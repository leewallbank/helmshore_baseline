Public Class mainfrm
    Dim first_letter As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim days As Integer = Weekday(Now)
        date_picker.Value = DateAdd(DateInterval.Day, -days, Now)
    End Sub

    Private Sub retnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles retnbtn.Click
        disable_buttons()
        ProgressBar1.Value = 5
        retn_month = Format(date_picker.Value, "MM_MMM")
        retn_day = Format(date_picker.Value, "dd_ddd")
        'get all clients that start with CMEC
        param2 = "select _rowid, name from Client where name like 'CMEC%'"
        Dim cl_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("No CMEC clients found")
            Exit Sub
        End If
        'get all csids for these clients
        Dim no_of_clients = no_of_rows
        Dim idx As Integer
        Dim file_name As String = ""
        Dim files_found As Boolean = False
        For idx = 0 To no_of_clients - 1
            ProgressBar1.Value = (idx / no_of_clients) * 100
            Application.DoEvents()
            Dim cl_id As Integer = cl_dataset.Tables(0).Rows(idx).Item(0)
            Dim cl_name As String = Trim(cl_dataset.Tables(0).Rows(idx).Item(1))
            param2 = "select _rowid, schemeID from clientScheme where clientID = " & cl_id
            Dim csid_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim csid_rows As Integer = no_of_rows
            Dim idx2 As Integer
            For idx2 = 0 To csid_rows - 1
                Dim cs_id As Integer = csid_dataset.Tables(0).Rows(idx2).Item(0)
                Dim sch_id As Integer = csid_dataset.Tables(0).Rows(idx2).Item(1)
                'get remittance number
                param2 = "select _rowid from Remit where clientschemeID = " & cs_id & _
                " and date = '" & Format(date_picker.Value, "yyyy-MM-dd") & "'"
                Dim remit_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    Continue For
                End If
                Dim remit_idx As Integer
                Dim remit_rows As Integer = no_of_rows
                For remit_idx = 0 To remit_rows - 1
                    Dim remit_no As Integer = remit_dataset.Tables(0).Rows(remit_idx).Item(0)
                    'get scheme name
                    param2 = "select name from Scheme where _rowid = " & sch_id
                    Dim sch_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read scheme number = " & sch_id)
                        Exit Sub
                    End If
                    Dim sch_name As String = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                    file_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                                                       retn_day & "\Remittances\" & Format(remit_no, "#") & "\Returns\"
                    new_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                                                                       retn_day & "\Remittances\" & cl_name & "-" & sch_name
                    'create directories for returns
                    Dim dir_name As String = new_path & " Nulla Bona\"
                    Try
                        Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                        If System.IO.Directory.Exists(dir_name) = False Then
                            di = System.IO.Directory.CreateDirectory(dir_name)
                        Else
                            'System.IO.Directory.Delete(dir_name, True)
                            di = System.IO.Directory.CreateDirectory(dir_name)
                        End If
                    Catch ex As Exception
                        MsgBox("Unable to create folder")
                        End
                    End Try
                    Dim retn_codeid As Integer
                    Dim cl_ref As String = ""

                    For Each foundFile As String In My.Computer.FileSystem.GetFiles _
           (file_path, FileIO.SearchOption.SearchTopLevelOnly, "*.pdf")
                        Dim start_idx As Integer
                        For start_idx = foundFile.Length To 1 Step -1
                            If Mid(foundFile, start_idx, 1) = "\" Then
                                Exit For
                            End If
                        Next
                        file_name = Microsoft.VisualBasic.Right(foundFile, foundFile.Length - start_idx)
                        start_idx = InStr(file_name, "-")
                        files_found = True
                        Dim name_sur As String = ""
                        Try
                            cl_ref = Microsoft.VisualBasic.Left(file_name, start_idx - 1)
                        Catch ex As Exception
                            Continue For
                        End Try
                        'get return code for this client ref
                        param2 = "select return_codeID, status_open_closed from Debtor " & _
                        " where client_ref = '" & cl_ref & "' and return_remitID = " & remit_no
                        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
                        If no_of_rows = 0 Then
                            If InStr(cl_ref, "___") > 0 Then    ' D.Dicks 12.03.2012
                                cl_ref = Replace(cl_ref, "___", " & ")
                            End If
                            cl_ref = Replace(cl_ref, "_", "/")
                            param2 = "select return_codeID, status_open_closed from Debtor " & _
                                                                                " where client_ref = '" & cl_ref & "' and return_remitID = " & remit_no
                            Dim debtor2_dataset As DataSet = get_dataset("onestep", param2)
                            If no_of_rows > 0 Then
                                debtor_dataset = debtor2_dataset
                            Else
                                If InStr(cl_ref, "//") > 0 Then
                                    cl_ref = Replace(cl_ref, "//", "/ ")
                                Else
                                    cl_ref = Replace(cl_ref, "/", " ")
                                End If
                                param2 = "select return_codeID, status_open_closed from Debtor " & _
                                                        " where client_ref = '" & cl_ref & "' and return_remitID = " & remit_no
                                Dim debtor3_dataset As DataSet = get_dataset("onestep", param2)
                                If no_of_rows = 0 Then
                                    MsgBox("Unable to find case for client number = " & cl_ref)
                                    Exit Sub
                                End If
                                debtor_dataset = debtor3_dataset
                            End If
                        End If
                        Dim status_open_closed As String = debtor_dataset.Tables(0).Rows(0).Item(1)
                        If status_open_closed = "O" Then
                            Continue For
                        End If
                        Try
                            retn_codeid = debtor_dataset.Tables(0).Rows(0).Item(0)
                        Catch ex As Exception
                            retn_codeid = 0
                        End Try
                        'get fee category
                        If retn_codeid > 0 Then
                            param2 = "select fee_category from codeReturns where _rowid = " & retn_codeid
                            Dim retn_dataset As DataSet = get_dataset("onestep", param2)
                            If no_of_rows = 0 Then
                                MsgBox("Unable to read Code Returns table for code = " & retn_codeid)
                                Exit Sub
                            End If
                            Dim fee_cat As Integer = retn_dataset.Tables(0).Rows(0).Item(0)
                            If fee_cat = 3 Then
                                Dim new_file As String = dir_name & file_name
                                Try
                                    My.Computer.FileSystem.CopyFile(foundFile, new_file)
                                Catch ex As Exception

                                End Try

                            End If
                        End If
                    Next
                Next
            Next
        Next
        If files_found Then
            MsgBox("All Nulla Bona reports copied to Nulla Bona directories")
        Else
            MsgBox("No reports were found")
        End If
        Me.Close()
    End Sub



    Private Sub disable_buttons()
        exitbtn.Enabled = False
        retnbtn.Enabled = False
    End Sub
End Class

Public Class mainform



    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline, caption As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim propaddr As String = ""
        Dim curraddr As String = ""
        Dim bill_no As String = ""
        Dim name As String = ""
        Dim idx, idx2, idx3 As Integer
        Dim lines As Integer = 0
        Dim debt_amt As Decimal
        Dim orig_amt As Decimal
        Dim clref As String = ""
        Dim addrline As String = ""
        Dim amtstring As String = ""
        Dim comments As String = Nothing
        Dim offence_from_date As Date = Nothing
        Dim offence_to_date As Date = Nothing
        Dim note_line As String

        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "Client ref|Name|Current Address|Debt Amount|OffenceFrom|OffenceTo|Comments" & vbNewLine
        outfile = outline

        'look for Account Number
        For idx = 0 To lines - 1
            If idx = 0 Then
                caption = Mid(line(idx), 1, 15)
            Else
                caption = Mid(line(idx), 2, 15)
            End If
            If caption = "" Then
                Continue For
            End If
            If caption = "Invoice Number:" Then
                clref = Trim(Mid(line(idx), 17, 30))
                'get details for this invoice number
                For idx2 = idx + 1 To lines - 1
                    caption = Trim(Mid(line(idx2), 2, 20))
                    Select Case caption
                        Case Is = "Customer Name:"
                            name = Trim(Mid(line(idx2), 20, 30))
                        Case Is = "Address:"
                            curraddr = Trim(Mid(line(idx2), 20, 30))
                            If Len(curraddr) > 3 Then
                                For idx3 = idx2 + 1 To lines - 1
                                    caption = Trim(Mid(line(idx3), 2, 20))
                                    If caption = "Invoice Description:" Then
                                        idx2 = idx3 - 1
                                        Exit For
                                    End If
                                    addrline = Trim(Mid(line(idx3), 20, 30))
                                    If Len(addrline) > 3 Then
                                        curraddr = curraddr & "," & addrline
                                    End If
                                Next
                            End If
                        Case Is = "Invoice Description:"
                            For idx3 = idx2 + 1 To lines - 1
                                caption = Trim(Mid(line(idx3), 2, 20))
                                If caption = "Original Invoice Tot" Then
                                    idx2 = idx3 - 1
                                    Exit For
                                End If
                                Dim body As String = Trim(Mid(line(idx3), 20, 32))
                                If LCase(Microsoft.VisualBasic.Left(body, 4)) = "from" Then
                                    Dim to_idx As Integer = InStr(body, "to")
                                    If to_idx = 0 Then
                                        to_idx = InStr(body, "-")
                                    Else
                                        to_idx += 1
                                    End If
                                    If to_idx = 0 Then
                                        errorfile = errorfile & "Line  " & idx2 & " - Invalid to date" & vbNewLine
                                    Else
                                        body = Microsoft.VisualBasic.Right(body, body.Length - to_idx)
                                        Try
                                            offence_to_date = CDate(body)
                                        Catch ex As Exception
                                            errorfile = errorfile & "Line  " & idx2 & " - Invalid to date" & vbNewLine
                                        End Try
                                    End If
                                    If offence_from_date = Nothing Then
                                        body = Trim(Mid(line(idx3), 26, to_idx - 6))
                                        Try
                                            offence_from_date = CDate(body)
                                        Catch ex As Exception
                                            Try
                                                body = Microsoft.VisualBasic.Right(body, body.Length - 1)
                                                offence_from_date = CDate(body)
                                            Catch ex2 As Exception
                                                errorfile = errorfile & "Line  " & idx2 & " - Invalid from date" & vbNewLine
                                            End Try
                                        End Try
                                    End If
                                End If
                                note_line = Trim(Mid(line(idx3), 20, 35))
                                If Len(note_line) > 3 Then
                                    comments = comments & " " & note_line & ";"
                                End If
                            Next
                        Case Is = "Original Invoice Tot"
                            amtstring = Trim(Mid(line(idx2), 30, 15))
                            orig_amt = Microsoft.VisualBasic.Right(amtstring, amtstring.Length - 1)
                            comments = comments & " " & "Original Inv Tot:" & Format(orig_amt, "fixed") & ";"
                        Case Is = "CURRENT BALANCE:"
                            amtstring = Trim(Mid(line(idx2), 20, 30))
                            debt_amt = amtstring
                            idx = idx2
                            Exit For
                    End Select
                Next
            End If
            'validate case details
            If clref = Nothing Or Microsoft.VisualBasic.Left(clref, 3) = "   " Then
                errorfile = errorfile & "Line  " & idx2 & " - No client reference" & vbNewLine
            End If
            'save case in outline
            outfile = outfile & clref & "|" & name & "|" & curraddr & "|" & debt_amt & "|"
            If offence_from_date = Nothing Then
                outfile = outfile & "|"
            Else
                outfile = outfile & offence_from_date & "|"
            End If
            If offence_to_date = Nothing Then
                outfile = outfile & "|"
            Else
                outfile = outfile & offence_to_date & "|"
            End If
            Dim comments2 As String = ""
            Dim comments3 As String = comments
            If comments <> Nothing Then
                If comments.Length <= 250 Then
                    outfile = outfile & comments & vbNewLine
                Else
                    While comments3.Length > 250
                        Dim len As Integer = Microsoft.VisualBasic.Len(comments3)
                        Dim idx6 As Integer
                        For idx6 = 250 To 1 Step -1
                            If Mid(comments3, idx6, 1) = ";" Then
                                Exit For
                            End If
                        Next
                        comments2 = comments2 & Microsoft.VisualBasic.Left(comments3, idx6)
                        Dim idx7 As Integer
                        For idx7 = idx6 To 250
                            comments2 = comments2 & " "
                        Next
                        comments3 = Microsoft.VisualBasic.Right(comments3, len - idx6)
                    End While
                    outfile = outfile & comments2 & comments3 & vbNewLine
                End If
            End If
            name = ""
            curraddr = ""
            clref = ""
            debt_amt = Nothing
            comments = Nothing
            offence_from_date = Nothing
            offence_to_date = Nothing
        Next
        
        viewbtn.Enabled = True
        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
End Class

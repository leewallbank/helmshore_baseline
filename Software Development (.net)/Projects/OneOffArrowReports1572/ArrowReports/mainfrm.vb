Public Class mainfrm
    '020810 activitynextcontactdate and activitystatus removed from activity file
    '30112010 remove chr(13) from address lines and phone numbers
    '15.5.2013 add accounts management clientID = 1765
    '6.8.2013 allow running of remits for specific remit date

    Dim address, add_line1, add_line2, add_line3, add_line4, add_line5, add_pcode, saved_fname As String
    Dim csid, csid_idx As Integer
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim allreports As Boolean = False
    Dim csid_no_of_rows As Integer
    Dim csid_table(200) As Integer

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub actbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles actbtn.Click
        disable_buttons()
        ProgressBar1.Value = 5
        write_activity_report()
        enable_buttons()
    End Sub
    Sub write_activity_report()
        Dim activity_file As String = "UniqueTransactionID|ArrowKey|ActivityDate|ActivityType|" & _
        "ActivityComment|" & vbNewLine
        'get last reported values from arrow_report_dates
        Me.Arrow_report_datesTableAdapter.FillBy1(Me.Arrow_report_datesDataSet.Arrow_report_dates)
        Dim last_note_id As Long = Me.Arrow_report_datesDataSet.Tables(0).Rows(0).Item(2)
        Dim latest_note_id As Long = last_note_id
        For csid_idx = 0 To csid_no_of_rows
            Try
                ProgressBar1.Value = (csid_idx / csid_no_of_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            csid = csid_table(csid_idx)
            param1 = "onestep"
            param2 = "select _rowid, client_ref from Debtor where clientschemeID = " & csid
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            Dim debtor, idx, idx2, tran_id As Integer
            Dim cl_ref, act_type, act_comment As String
            Dim act_date As Date
            Dim debtor_rows As Integer = no_of_rows
            For idx = 0 To debtor_rows - 1
                Application.DoEvents()
                debtor = debtor_dataset.Tables(0).Rows(idx).Item(0)
                'get all notes
                param2 = "select _rowid, _createdDate, type, text from Note where debtorID=" & debtor & _
                " and _rowid > " & last_note_id & _
                " order by _rowid"
                Dim note_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 0 Then
                    Continue For
                End If

                Dim note_rows As Integer = no_of_rows
                For idx2 = 0 To note_rows - 1
                    tran_id = note_dataset.Tables(0).Rows(idx2).Item(0)
                    cl_ref = Trim(debtor_dataset.Tables(0).Rows(idx).Item(1))
                    act_date = note_dataset.Tables(0).Rows(idx2).Item(1)
                    act_type = note_dataset.Tables(0).Rows(idx2).Item(2)
                    Try
                        act_comment = Trim(note_dataset.Tables(0).Rows(idx2).Item(3))
                        act_comment = Trim(remove_chars(act_comment))
                    Catch ex As Exception
                        act_comment = ""
                    End Try
                    activity_file = activity_file & tran_id & "|" & cl_ref & "|" & _
                    Format(act_date, "dd/MM/yyyy") & "|" & act_type & "|" & act_comment & "|" & vbNewLine
                Next
                If tran_id > latest_note_id Then
                    latest_note_id = tran_id
                End If
            Next
        Next
        'save report details for next time
        If last_note_id <> latest_note_id Then
            Me.Arrow_report_datesTableAdapter.InsertQuery1(Now, latest_note_id)
        End If
        With SaveFileDialog1
            .Title = "Save files"
            .Filter = "txt|*.txt"
            .DefaultExt = ".txt"
            .OverwritePrompt = True
            .FileName = "Rossendales_ActivityReport_" & Format(Now, "ddMMyyyy") & ".txt"
            .Title = "Select folder for saving files"
        End With
        If SaveFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("Files not created")
            enable_buttons()
            Exit Sub
        End If

        Dim fname As String = SaveFileDialog1.FileName
        If allreports Then
            Dim idx3 As Integer
            For idx3 = fname.Length To 1 Step -1
                If Mid(fname, idx3, 1) = "\" Then
                    Exit For
                End If
            Next
            saved_fname = Microsoft.VisualBasic.Left(fname, idx3)
        End If
        My.Computer.FileSystem.WriteAllText(fname, activity_file, False, ascii)
        If allreports = False Then
            MsgBox("File saved ok")
            Me.Close()
        End If
    End Sub
    Function remove_chars(ByVal text As String) As String
        Dim idx As Integer
        Dim new_text As String = ""
        For idx = 1 To text.Length
            If Mid(text, idx, 1) = Chr(10) Or Mid(text, idx, 1) = Chr(9) Or _
            Mid(text, idx, 1) = Chr(13) Or Mid(text, idx, 1) = "|" Then
                new_text = new_text & " "
            Else
                new_text = new_text & Mid(text, idx, 1)
            End If
        Next
        Return new_text
    End Function
    Sub disable_buttons()
        ' allbtn.Enabled = False
        actbtn.Enabled = False
        transbtn.Enabled = False
        exitbtn.Enabled = False
    End Sub
    Sub enable_buttons()
        'allbtn.Enabled = True
        actbtn.Enabled = True
        transbtn.Enabled = True
        exitbtn.Enabled = True
    End Sub

    Private Sub transbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles transbtn.Click
        disable_buttons()
        ProgressBar1.Value = 5
        If MsgBox("Run for specific remit date?", MsgBoxStyle.YesNo, "Specific Remit Date") = MsgBoxResult.Yes Then
            RemitDatefrm.ShowDialog()
        End If
        write_transactions_report()
        enable_buttons()
    End Sub
    Sub write_transactions_report()
        Dim trans_file As String = "UniqueTransactionID|ServicerAccountReference|ArrowKey|" & _
        "TransactionDate|TransactionGroup|TransactionType|TransactionAmount|" & vbNewLine
        'Me.Arrow_report_datesTableAdapter.FillBy(Me.Arrow_report_datesDataSet.Arrow_report_dates)

        'get last reported values from arrow_report_dates
        Dim last_debtor_id As Integer = 0
        Dim last_remit_id As Integer = 0
        Dim latest_debtor_id As Integer = last_debtor_id
        Dim latest_remit_id As Integer = last_remit_id

        For csid_idx = 0 To csid_no_of_rows
            Try
                ProgressBar1.Value = (csid_idx / csid_no_of_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            csid = csid_table(csid_idx)
            param1 = "onestep"
            param2 = "select _rowid, client_ref from Debtor where clientschemeID = " & csid & _
            " and _rowid > " & last_debtor_id & _
            " order by _rowid"
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            Dim debtor, idx, idx2, tran_id, sourceid, typeid As Integer
            Dim cl_ref As String = ""
            Dim tran_type As String = ""
            Dim tran_group As String = ""
            Dim status_date As Date
            Dim tran_amt As Decimal
            Dim debtor_rows As Integer = no_of_rows
            For idx = 0 To debtor_rows - 1
                If remit_date <> Nothing Then
                    Exit For
                End If
                Application.DoEvents()
                debtor = debtor_dataset.Tables(0).Rows(idx).Item(0)
                cl_ref = Trim(debtor_dataset.Tables(0).Rows(idx).Item(1))
                'get all new debt
                param2 = "select _rowid, type, date, fee_amount, remited_fee from Fee" & _
                " where debtorID = " & debtor
                Dim fee_dataset As DataSet = get_dataset(param1, param2)
                Dim fee_idx As Integer = no_of_rows - 1
                For idx2 = 0 To fee_idx
                    'write out fee record
                    tran_id = fee_dataset.Tables(0).Rows(idx2).Item(0)
                    tran_type = Trim(fee_dataset.Tables(0).Rows(idx2).Item(1))
                    If tran_type = "Debt" Then
                        tran_type = "Original Debt"
                        'ignore transaction fees
                        Continue For
                    ElseIf tran_type <> "Costs" Then
                        Continue For
                    End If
                    tran_amt = fee_dataset.Tables(0).Rows(idx2).Item(3) * -1
                    Dim remitted_amt As Decimal = fee_dataset.Tables(0).Rows(idx2).Item(4)
                    Dim fee_date As Date = fee_dataset.Tables(0).Rows(idx2).Item(2)
                    trans_file = trans_file & tran_id & "|" & debtor & "|" & cl_ref & "|" & _
                                Format(fee_date, "dd/MM/yyyy") & "|" & tran_type & "|" & "" & "|" & _
                                Format(tran_amt, "0.00") & "|" & vbNewLine
                Next
                latest_debtor_id = debtor
            Next
            'get all payments in remits since last one
            'if remit_date has a value then use that
            If remit_date = Nothing Then
                param2 = "select _rowid, status_date, amount_sourceID, amount_typeID, amount, status, " & _
                           "status_remitID, debtorID, split_debt, split_costs, date from Payment" & _
                           " where status = 'R'  and status_remitID > " & last_remit_id & _
                           " and clientschemeID = " & csid
            Else
                param2 = "select _rowid, status_date, amount_sourceID, amount_typeID, amount, status, " & _
                           "status_remitID, debtorID, split_debt, split_costs, date from Payment" & _
                           " where status = 'R'  and status_date = '" & Format(remit_date, "yyyy-MM-dd") & "'" & _
                           " and clientschemeID = " & csid
            End If
           

            Dim payment_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim payment_rows As Integer = no_of_rows
            For idx2 = 0 To payment_rows - 1
                tran_id = payment_dataset.Tables(0).Rows(idx2).Item(0)
                status_date = payment_dataset.Tables(0).Rows(idx2).Item(1)

                'If Format(status_date, "yyyy-MM-dd") >= Format(CDate("Jul 1 , 2013"), "yyyy-MM-dd") Then
                '    Continue For
                'End If
                sourceid = payment_dataset.Tables(0).Rows(idx2).Item(2)
                typeid = payment_dataset.Tables(0).Rows(idx2).Item(3)
                debtor = payment_dataset.Tables(0).Rows(idx2).Item(7)
                Dim transactionDate As Date = payment_dataset.Tables(0).Rows(idx2).Item(10)
                param2 = "select direct, source from PaySource where _rowid = " & sourceid
                Dim paysource_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    MsgBox("Error reading PaySource database for " & sourceid)
                    Exit Sub
                End If
                If paysource_dataset.Tables(0).Rows(0).Item(0) = "Y" Then
                    tran_group = "PaymentDirect"
                Else
                    tran_group = "Payment"
                End If
                'get transaction type from PayType
                param2 = "select desc_short from PayType where _rowid = " & typeid
                Dim paytype_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    MsgBox("Error reading PayType database for " & typeid)
                    Exit Sub
                End If
                tran_type = Trim(paysource_dataset.Tables(0).Rows(0).Item(1))
                If tran_type <> "Direct Debit" Then
                    tran_type = Trim(paytype_dataset.Tables(0).Rows(0).Item(0))
                End If
                'tran_amt = payment_dataset.Tables(0).Rows(idx2).Item(4)
                'use debt amount not payments amount
                tran_amt = payment_dataset.Tables(0).Rows(idx2).Item(8)
                tran_amt = tran_amt + payment_dataset.Tables(0).Rows(idx2).Item(9)
                Dim remitted_amt As Decimal
                If payment_dataset.Tables(0).Rows(idx2).Item(5) = "W" Then
                    remitted_amt = 0
                Else
                    remitted_amt = tran_amt
                End If
                'get client-ref from debtor table
                param2 = "select client_ref from Debtor where _rowid = " & debtor
                Dim debtor2_dataset As DataSet = get_dataset(param1, param2)
                cl_ref = Trim(debtor2_dataset.Tables(0).Rows(0).Item(0))
                trans_file = trans_file & tran_id & "|" & debtor & "|" & cl_ref & "|" & _
                Format(transactionDate, "dd/MM/yyyy") & "|" & tran_group & "|" & tran_type & "|" & _
                Format(tran_amt, "0.00") & "|" & vbNewLine
                Dim remit_id As Integer = payment_dataset.Tables(0).Rows(idx2).Item(6)
                If remit_id > latest_remit_id Then
                    latest_remit_id = remit_id
                End If
            Next
        Next
        'save report details for next time
        'If remit_date = Nothing Then
        '    If last_remit_id <> latest_remit_id Or last_debtor_id <> latest_debtor_id Then
        '        Me.Arrow_report_datesTableAdapter.InsertQuery(Now, 0, latest_remit_id, latest_debtor_id)
        '    End If
        'End If

        Dim fname As String
        If allreports = False Then
            With SaveFileDialog1
                .Title = "Save files"
                .Filter = "txt|*.txt"
                .DefaultExt = ".txt"
                .OverwritePrompt = True
                .FileName = "Rossendales_TransactionsReport_" & Format(Now, "ddMMyyyy") & ".txt"
                .Title = "Select folder for saving files"
            End With
            If SaveFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
                MsgBox("Files not created")
                enable_buttons()
                Exit Sub
            Else
                fname = SaveFileDialog1.FileName
            End If
        Else
            fname = saved_fname & "Rossendales_TransactionsReport_" & Format(Now, "ddMMyyyy") & ".txt"
        End If
        My.Computer.FileSystem.WriteAllText(fname, trans_file, False, ascii)
        MsgBox("File saved ok")
        Me.Close()
    End Sub

    Private Sub invbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()
        ProgressBar1.Value = 5
        write_inventory_report()
        enable_buttons()
    End Sub
    Sub write_inventory_report()
        Dim inv_file As String = "ServiceAccountReference|ArrowKey|DebtType|" & _
        "Title|FirstName|Surname|DateOfBirth|AddressLine1|AddressLine2|AddressLine3|" & _
        "AdddressLine4|AddressLine5|PostCode|HomePhoneNumber|WorkPhoneNumber|MobilePhoneNumber|" & _
        "FaxNumber|EmailAdddress|MaritalStatus|EmploymentStatus|DateOfDebtPlacement|" & _
        "OpeningBalance|AmountPaid|CurrentBalance|CommissionRate|CommissionAmount|DirectPayments|" & _
        "LastPaymentDate|LastPaymentAmount|LastContactDate|DateClosed|AccountStatus|ClosedStatus|" & _
        "WriteOffReason|WriteOffDate|WriteOffAmount|ArrangementInstalmentAmount|" & _
        "ArrangementFirstInstalmentDueDate|ArrangementInstalmentFrequency|" & _
        "ArrowGlobalLegalEntity|3PDMAllocationDate|3PDMName|3PDMContactDetails|CCARequestdate|" & _
        "CCARequestDetails|SARRequestDate|SARRequestDetails|RegulatoryComplaintsDate|" & _
        "RegulatoryComplaintDetails|" & vbNewLine
        Dim inv_file2 As String = inv_file
        Dim inv_file3 As String = inv_file
        For csid_idx = 0 To csid_no_of_rows
            ProgressBar1.Value = (csid_idx / csid_no_of_rows) * 100
            If csid_idx = 10 Then
                inv_file2 = inv_file
                inv_file = "ServiceAccountReference|ArrowKey|DebtType|" & _
        "Title|FirstName|Surname|DateOfBirth|AddressLine1|AddressLine2|AddressLine3|" & _
        "AdddressLine4|AddressLine5|PostCode|HomePhoneNumber|WorkPhoneNumber|MobilePhoneNumber|" & _
        "FaxNumber|EmailAdddress|MaritalStatus|EmploymentStatus|DateOfDebtPlacement|" & _
        "OpeningBalance|AmountPaid|CurrentBalance|CommissionRate|CommissionAmount|DirectPayments|" & _
        "LastPaymentDate|LastPaymentAmount|LastContactDate|DateClosed|AccountStatus|ClosedStatus|" & _
        "WriteOffReason|WriteOffDate|WriteOffAmount|ArrangementInstalmentAmount|" & _
        "ArrangementFirstInstalmentDueDate|ArrangementInstalmentFrequency|" & _
        "ArrowGlobalLegalEntity|3PDMAllocationDate|3PDMName|3PDMContactDetails|CCARequestdate|" & _
        "CCARequestDetails|SARRequestDate|SARRequestDetails|RegulatoryComplaintsDate|" & _
        "RegulatoryComplaintDetails|" & vbNewLine
            End If
            If csid_idx = 25 Then
                inv_file3 = inv_file
                inv_file = "ServiceAccountReference|ArrowKey|DebtType|" & _
        "Title|FirstName|Surname|DateOfBirth|AddressLine1|AddressLine2|AddressLine3|" & _
        "AdddressLine4|AddressLine5|PostCode|HomePhoneNumber|WorkPhoneNumber|MobilePhoneNumber|" & _
        "FaxNumber|EmailAdddress|MaritalStatus|EmploymentStatus|DateOfDebtPlacement|" & _
        "OpeningBalance|AmountPaid|CurrentBalance|CommissionRate|CommissionAmount|DirectPayments|" & _
        "LastPaymentDate|LastPaymentAmount|LastContactDate|DateClosed|AccountStatus|ClosedStatus|" & _
        "WriteOffReason|WriteOffDate|WriteOffAmount|ArrangementInstalmentAmount|" & _
        "ArrangementFirstInstalmentDueDate|ArrangementInstalmentFrequency|" & _
        "ArrowGlobalLegalEntity|3PDMAllocationDate|3PDMName|3PDMContactDetails|CCARequestdate|" & _
        "CCARequestDetails|SARRequestDate|SARRequestDetails|RegulatoryComplaintsDate|" & _
        "RegulatoryComplaintDetails|" & vbNewLine
            End If
            csid = csid_table(csid_idx)
            'get commission rate
            param1 = "onestep"
            param2 = "select fee_comm1 from ClientScheme where _rowid = " & csid
            Dim cs_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                MsgBox("Clientscheme not found for csid = " & csid)
                Exit Sub
            End If
            Dim comm_rate As Decimal = cs_dataset.Tables(0).Rows(0).Item(0)

            param2 = "select _rowid, client_ref, clientschemeID, name_title, name_fore, name_sur, dateOfBirth, address," & _
            " add_postcode, add_phone, add_fax, empPhone, addEmail, empName, _createdDate, debt_amount, " & _
            " debtPaid, debt_balance, status, status_open_closed, return_date, " & _
            " arrange_amount, arrange_started, arrange_interval, return_codeID" & _
            " from Debtor where clientschemeID = " & csid
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            Dim debtor, idx, arr_interval As Integer
            Dim cl_ref, sch_name, title, first_name, surname, home_phone, phone2, emp_phone, fax, email As String
            Dim emp_status, status, open_closed As String
            Dim last_payment_date, dob, created_date, return_date, last_contact, arr_started As Date
            Dim last_payment_amt, opening_bal, amt_paid, cur_bal, dir_payments, arr_amt As Decimal
            Dim debtor_rows As Integer = no_of_rows
            For idx = 0 To debtor_rows - 1
                Application.DoEvents()
                debtor = debtor_dataset.Tables(0).Rows(idx).Item(0)
                cl_ref = Trim(debtor_dataset.Tables(0).Rows(idx).Item(1))
                csid = debtor_dataset.Tables(0).Rows(idx).Item(2)
                param2 = "select schemeID from ClientScheme where _rowid = " & csid
                Dim csid_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    MsgBox("error reading clientscheme for " & csid)
                    Exit Sub
                End If
                param2 = "select name from Scheme where _rowid = " & csid_dataset.Tables(0).Rows(0).Item(0)
                Dim sch_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    MsgBox("error reading scheme for client scheme = " & csid)
                    Exit Sub
                End If
                sch_name = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                Try
                    title = Trim(debtor_dataset.Tables(0).Rows(idx).Item(3))
                Catch ex As Exception
                    title = Nothing
                End Try
                Try
                    first_name = Trim(debtor_dataset.Tables(0).Rows(idx).Item(4))
                Catch ex As Exception
                    first_name = Nothing
                End Try

                surname = Trim(debtor_dataset.Tables(0).Rows(idx).Item(5))
                Try
                    dob = debtor_dataset.Tables(0).Rows(idx).Item(6)
                Catch ex As Exception
                    dob = Nothing
                End Try

                'get payments
                param2 = "select amount, date, amount_sourceID from Payment" & _
                " where debtorID = " & debtor & " and amount <> 0 and (status = 'W' or status = 'R')" & _
                " order by date desc"
                Dim payment_dataset As DataSet = get_dataset(param1, param2)
                dir_payments = 0
                If no_of_rows = 0 Then
                    last_payment_date = Nothing
                    last_payment_amt = Nothing
                Else
                    last_payment_date = payment_dataset.Tables(0).Rows(0).Item(1)
                    last_payment_amt = payment_dataset.Tables(0).Rows(0).Item(0)
                    'sum up direct payments
                    Dim idx2 As Integer
                    Dim pay_rows As Integer = no_of_rows
                    For idx2 = 0 To pay_rows - 1
                        Dim sourceid As Integer = payment_dataset.Tables(0).Rows(idx2).Item(2)
                        param2 = "select direct from PaySource where _rowid = " & sourceid
                        Dim paysource_dataset As DataSet = get_dataset(param1, param2)
                        If no_of_rows <> 1 Then
                            MsgBox("Unable to read paysource = " & sourceid)
                            Exit Sub
                        End If
                        If paysource_dataset.Tables(0).Rows(0).Item(0) = "Y" Then
                            dir_payments += payment_dataset.Tables(0).Rows(idx2).Item(0)
                        End If
                    Next

                End If

                inv_file = inv_file & debtor & "|" & cl_ref & "|" & sch_name & "|" & _
                title & "|" & first_name & "|" & surname & "|"

                If dob = Nothing Then
                    inv_file = inv_file & ""
                Else
                    inv_file = inv_file & Format(dob, "dd/MM/yyyy")
                End If
                address = Trim(debtor_dataset.Tables(0).Rows(idx).Item(7))
                Try
                    add_pcode = Trim(debtor_dataset.Tables(0).Rows(idx).Item(8))
                Catch ex As Exception
                    add_pcode = ""
                End Try

                get_address_lines()
                Try
                    home_phone = Trim(debtor_dataset.Tables(0).Rows(idx).Item(9))
                    home_phone = remove_cr(home_phone)
                Catch ex As Exception
                    home_phone = ""
                End Try
                Try
                    phone2 = Trim(debtor_dataset.Tables(0).Rows(idx).Item(10))
                    phone2 = remove_cr(phone2)
                Catch ex As Exception
                    phone2 = ""
                End Try
                Try
                    emp_phone = Trim(debtor_dataset.Tables(0).Rows(idx).Item(11))
                    emp_phone = remove_cr(emp_phone)
                Catch ex As Exception
                    emp_phone = ""
                End Try
                fax = ""
                Try
                    email = Trim(debtor_dataset.Tables(0).Rows(idx).Item(12))
                    email = remove_cr(email)
                Catch ex As Exception
                    email = ""
                End Try
                emp_status = ""
                Try
                    Dim emp_name As String = Trim(debtor_dataset.Tables(0).Rows(idx).Item(13))
                    If emp_name.Length > 0 Then
                        emp_status = "Employed"
                    End If
                Catch ex As Exception

                End Try
                If emp_phone <> "" Then
                    emp_status = "Employed"
                End If

                created_date = debtor_dataset.Tables(0).Rows(idx).Item(14)
                opening_bal = debtor_dataset.Tables(0).Rows(idx).Item(15)
                amt_paid = debtor_dataset.Tables(0).Rows(idx).Item(16)
                cur_bal = opening_bal - amt_paid
                status = debtor_dataset.Tables(0).Rows(idx).Item(18)
                open_closed = debtor_dataset.Tables(0).Rows(idx).Item(19)
                Dim comm_amt As Decimal = (amt_paid * comm_rate) / 100
                Try
                    return_date = debtor_dataset.Tables(0).Rows(idx).Item(20)
                Catch ex As Exception
                    return_date = Nothing
                End Try

                If open_closed = "O" And status <> "C" Then
                    return_date = Nothing
                End If
                'get last letter date
                param2 = "select max(_createdDate) from Note where type= 'Letter' and debtorID = " & debtor
                Dim note_dataset As DataSet = get_dataset(param1, param2)
                Try
                    last_contact = note_dataset.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    last_contact = Nothing
                End Try
                If last_contact = Nothing Then
                    'get last letter stage
                    param2 = "select _createdDate, text from Note where type = 'Stage' and debtorID = " & debtor & _
                    " order by _rowid desc"
                    Dim note2_dataset As DataSet = get_dataset(param1, param2)
                    If no_of_rows = 0 Then
                        last_contact = Nothing
                    Else
                        Dim idx2 As Integer
                        Dim note_rows As Integer = no_of_rows - 1
                        For idx2 = 0 To note_rows
                            If InStr(note2_dataset.Tables(0).Rows(idx2).Item(1), "Letter") > 0 Then
                                last_contact = note2_dataset.Tables(0).Rows(idx2).Item(0)
                                Exit For
                            End If
                        Next
                    End If
                End If
                Dim arr_frequency As String = ""
                If open_closed = "O" And status = "A" Then
                    arr_amt = debtor_dataset.Tables(0).Rows(idx).Item(21)
                    Try
                        arr_started = debtor_dataset.Tables(0).Rows(idx).Item(22)
                        arr_interval = debtor_dataset.Tables(0).Rows(idx).Item(23)
                    Catch ex As Exception
                        arr_started = Nothing
                        arr_interval = 0
                    End Try
                    Select Case arr_interval
                        Case 0
                            arr_frequency = ""
                        Case 7
                            arr_frequency = "Weekly"
                        Case 14
                            arr_frequency = "Fortnightly"
                        Case 28 To 31
                            arr_frequency = "Monthly"
                        Case 90
                            arr_frequency = "Quarterly"
                        Case Else
                            arr_frequency = "Every " & arr_interval & " Days"
                    End Select
                End If
                
                inv_file = inv_file & "|" & add_line1 & "|" & add_line2 & "|" & add_line3 & "|" & add_line4 & _
                "|" & add_line5 & "|" & add_pcode & "|" & home_phone & "|" & emp_phone & "|" & phone2 & "|" & fax & "|" & _
                email & "|" & "" & "|" & emp_status & "|" & Format(created_date, "dd/MM/yyyy") & "|" & _
                Format(opening_bal, "0.00") & "|" & Format(amt_paid, "F") & "|" & Format(cur_bal, "0.00") & "|" & _
                Format(comm_rate, "0.00") & "|" & Format(comm_amt, "0.00") & "|" & Format(dir_payments, "0.00") & "|"
                If last_payment_date = Nothing Then
                    inv_file = inv_file & ""
                Else
                    inv_file = inv_file & Format(last_payment_date, "dd/MM/yyyy")
                End If

                inv_file = inv_file & "|" & Format(last_payment_amt, "0.00") & "|"
                If last_contact = Nothing Then
                    inv_file = inv_file & ""
                Else
                    inv_file = inv_file & Format(last_contact, "dd/MM/yyyy")
                End If

                If return_date = Nothing Then
                    inv_file = inv_file & "|" & ""
                Else
                    inv_file = inv_file & "|" & Format(return_date, "dd/MM/yyyy")
                End If
                If open_closed = "O" Then
                    inv_file = inv_file & "|" & "Open"
                Else
                    inv_file = inv_file & "|" & "Closed"
                End If
                Dim status_text As String = ""
                Select Case status
                    Case "S"
                        If amt_paid > 0 Then
                            status_text = "PaidinFull"
                        Else
                            status_text = "Successful"
                        End If
                    Case "C"
                        status_text = "Cancelled"
                    Case "F"
                        status_text = "FullyPaid"
                    Case "A"
                        status_text = "Arrangement"
                    Case "T"
                        status_text = "Trace"
                    Case "L"
                        status_text = "Live"
                    Case "X"
                        status_text = "Expired"
                    Case Else
                        status_text = "Unknown"
                End Select
                inv_file = inv_file & "|" & status_text & "|"

                'write off amount is balance left
                Dim wo_date As Date = Nothing
                Dim wo_amt As Decimal
                Dim wo_reason As String = Nothing

                wo_date = Nothing
                wo_amt = debtor_dataset.Tables(0).Rows(idx).Item(17)
                If wo_amt > 0 And open_closed = "C" Then
                    wo_date = return_date
                    Dim retn_code As Integer
                    Try
                        retn_code = debtor_dataset.Tables(0).Rows(idx).Item(24)
                    Catch ex As Exception
                        retn_code = 0
                    End Try
                    If retn_code > 0 Then
                        'get return reason from codereturns table
                        param2 = "select reason_short from CodeReturns where _rowid = " & retn_code
                        Dim retn_dataset As DataSet = get_dataset(param1, param2)
                        If no_of_rows = 0 Then
                            MsgBox("Can't find return reason for return code = " & retn_code)
                            Exit Sub
                        End If
                        wo_reason = retn_dataset.Tables(0).Rows(0).Item(0)
                    End If
                End If

                If Microsoft.VisualBasic.Left(wo_reason, 6) = "Settle" And wo_amt > 0 And open_closed = "C" Then
                    inv_file = inv_file & Trim(wo_reason) & "|"
                    If wo_date = Nothing Then
                        inv_file = inv_file & ""
                    Else
                        inv_file = inv_file & Format(wo_date, "dd/MM/yyyy")

                    End If
                    inv_file = inv_file & "|" & Format(wo_amt, "0.00") & "|"
                Else
                    inv_file = inv_file & "" & "|" & "" & "|"
                End If

                If open_closed = "O" And status = "A" Then
                    inv_file = inv_file & Format(arr_amt, "0.00") & "|"
                    If arr_started = Nothing Then
                        inv_file = inv_file & ""
                    Else
                        inv_file = inv_file & Format(arr_started, "dd/MM/yyyy")
                    End If
                Else
                    inv_file = inv_file & "" & "|" & ""
                End If
                inv_file = inv_file & "|" & "" & "|" & arr_frequency & "|"

                'get buyername from notes
                param2 = "select text from Note where (type = 'Note' or type = 'Client note') " & _
                " and debtorID = " & debtor & " order by _rowid desc"
                Dim note3_dataset As DataSet = get_dataset(param1, param2)
                Dim idx3 As Integer
                Dim note3_rows As Integer = no_of_rows - 1
                Dim text As String
                Dim buyer_name As String = ""
                For idx3 = 0 To note3_rows
                    text = note3_dataset.Tables(0).Rows(idx3).Item(0)
                    Dim start_idx As Integer = InStr(text, "BuyerName:")
                    If start_idx > 0 Then
                        Dim idx4 As Integer
                        For idx4 = start_idx + 10 To text.Length
                            If Mid(text, idx4, 1) = ";" Then
                                Exit For
                            End If
                        Next
                        buyer_name = Mid(text, start_idx + 10, idx4 - start_idx - 10)
                        Exit For
                    End If
                Next
                inv_file = inv_file & buyer_name & "|||||||||" & vbNewLine
            Next
        Next
        Dim fname, fname2, fname3 As String
        If allreports = False Then
            With SaveFileDialog1
                .Title = "Save files"
                .Filter = "txt|*.txt"
                .DefaultExt = ".txt"
                .OverwritePrompt = True
                .FileName = "Rossendales_InventoryReport_" & Format(Now, "ddMMyyyy") & ".txt"
                .Title = "Select folder for saving files"
            End With
            If SaveFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
                MsgBox("Files not created")
                enable_buttons()
                Exit Sub
            Else
                fname = SaveFileDialog1.FileName
                Dim idx3 As Integer
                For idx3 = fname.Length To 1 Step -1
                    If Mid(fname, idx3, 1) = "\" Then
                        Exit For
                    End If
                Next
                saved_fname = Microsoft.VisualBasic.Left(fname, idx3)
                fname2 = saved_fname & "Rossendales_InventoryReport2_" & Format(Now, "ddMMyyyy") & ".txt"
                fname3 = saved_fname & "Rossendales_InventoryReport3_" & Format(Now, "ddMMyyyy") & ".txt"
            End If
        Else
            fname = saved_fname & "Rossendales_InventoryReport1_" & Format(Now, "ddMMyyyy") & ".txt"
            fname2 = saved_fname & "Rossendales_InventoryReport2_" & Format(Now, "ddMMyyyy") & ".txt"
            fname3 = saved_fname & "Rossendales_InventoryReport3_" & Format(Now, "ddMMyyyy") & ".txt"
        End If

        Try
            My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
        Catch
            MsgBox("Can't save file")
            Exit Sub
        End Try
        If inv_file2.Length > 0 Then
            Try
                My.Computer.FileSystem.WriteAllText(fname2, inv_file2, False, ascii)
            Catch
                MsgBox("Can't save file2")
                Exit Sub
            End Try
        End If
        If inv_file3.Length > 0 Then
            Try
                My.Computer.FileSystem.WriteAllText(fname3, inv_file3, False, ascii)
            Catch
                MsgBox("Can't save file3")
                Exit Sub
            End Try
        End If
        If allreports Then
            MsgBox("Files saved ok")
        Else
            MsgBox("File saved ok")
        End If

        Me.Close()
    End Sub
    Sub get_address_lines()
        Dim idx, add_idx As Integer
        add_line1 = ""
        add_line2 = ""
        add_line3 = ""
        add_line4 = ""
        add_line5 = ""
        add_idx = 1
        For idx = 1 To address.Length
            If Mid(address, idx, 1) = Chr(9) Or Mid(address, idx, 1) = Chr(10) Then
                add_idx += 1
            ElseIf Mid(address, idx, 1) <> Chr(13) Then
                Select Case add_idx
                    Case 1
                        add_line1 = add_line1 & Mid(address, idx, 1)
                    Case 2
                        add_line2 = add_line2 & Mid(address, idx, 1)
                    Case 3
                        add_line3 = add_line3 & Mid(address, idx, 1)
                    Case 4
                        add_line4 = add_line4 & Mid(address, idx, 1)
                    Case 5
                        add_line5 = add_line5 & Mid(address, idx, 1)
                End Select
            End If
        Next
        If add_line2 = add_pcode Then
            add_line2 = ""
        End If
        If add_line3 = add_pcode Then
            add_line3 = ""
        End If
        If add_line4 = add_pcode Then
            add_line4 = ""
        End If
        If add_line5 = add_pcode Then
            add_line5 = ""
        End If
    End Sub
    Function remove_cr(ByVal chars As String) As String
        Dim new_chars As String = ""
        Dim idx As Integer
        For idx = 1 To chars.Length
            If Mid(chars, idx, 1) <> Chr(10) And Mid(chars, idx, 1) <> Chr(13) Then
                new_chars = new_chars & Mid(chars, idx, 1)
            End If
        Next
        Return new_chars
    End Function

    Private Sub mainfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        conn.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'get a list of client scheme ids
        '09.05.2011 add guernsey
        '26.09.2011 and arrowRML
        '15.02.2013 add accounts management cl=1765
        param1 = "onestep"
        param2 = "select _rowid from ClientScheme where clientID = 1572"
        Dim csid_dataset As DataSet = get_dataset(param1, param2)
        csid_no_of_rows = no_of_rows - 1
        Dim idx As Integer
        For idx = 0 To csid_no_of_rows
            csid_table(idx) = csid_dataset.Tables(0).Rows(idx).Item(0)
        Next
    End Sub

    Private Sub allbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()
        allreports = True
        ProgressBar1.Value = 5
        write_activity_report()
        ProgressBar1.Value = 5
        write_transactions_report()
        ProgressBar1.Value = 5
        'write_inventory_report()
        enable_buttons()
        allreports = False
    End Sub

End Class

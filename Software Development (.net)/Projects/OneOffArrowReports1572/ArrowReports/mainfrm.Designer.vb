<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.transbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.actbtn = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.Arrow_report_datesDataSet = New ArrowReports.Arrow_report_datesDataSet
        Me.Arrow_report_datesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Arrow_report_datesTableAdapter = New ArrowReports.Arrow_report_datesDataSetTableAdapters.Arrow_report_datesTableAdapter
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        CType(Me.Arrow_report_datesDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Arrow_report_datesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'transbtn
        '
        Me.transbtn.Location = New System.Drawing.Point(112, 100)
        Me.transbtn.Name = "transbtn"
        Me.transbtn.Size = New System.Drawing.Size(118, 23)
        Me.transbtn.TabIndex = 2
        Me.transbtn.Text = "Transaction report"
        Me.transbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(205, 265)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 4
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'actbtn
        '
        Me.actbtn.Location = New System.Drawing.Point(112, 46)
        Me.actbtn.Name = "actbtn"
        Me.actbtn.Size = New System.Drawing.Size(118, 23)
        Me.actbtn.TabIndex = 1
        Me.actbtn.Text = "Activity Report"
        Me.actbtn.UseVisualStyleBackColor = True
        Me.actbtn.Visible = False
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 265)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 5
        '
        'Arrow_report_datesDataSet
        '
        Me.Arrow_report_datesDataSet.DataSetName = "Arrow_report_datesDataSet"
        Me.Arrow_report_datesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Arrow_report_datesBindingSource
        '
        Me.Arrow_report_datesBindingSource.DataMember = "Arrow_report_dates"
        Me.Arrow_report_datesBindingSource.DataSource = Me.Arrow_report_datesDataSet
        '
        'Arrow_report_datesTableAdapter
        '
        Me.Arrow_report_datesTableAdapter.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(88, 175)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(173, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Inventory report now run separately"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(88, 152)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(163, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Activity report now run separately"
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(332, 312)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.actbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.transbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Arrow Reports"
        CType(Me.Arrow_report_datesDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Arrow_report_datesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents transbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents actbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents Arrow_report_datesDataSet As ArrowReports.Arrow_report_datesDataSet
    Friend WithEvents Arrow_report_datesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Arrow_report_datesTableAdapter As ArrowReports.Arrow_report_datesDataSetTableAdapters.Arrow_report_datesTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label

End Class

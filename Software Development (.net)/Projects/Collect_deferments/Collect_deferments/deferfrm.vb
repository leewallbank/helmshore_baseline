Public Class deferfrm

    Private Sub deferfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        defer_dg.CommitEdit(DataGridViewDataErrorContexts.CurrentCellChange)
        Dim defer_idx As Integer
        Form1.Collect_DefermentsTableAdapter.DeleteQuery(Month(run_date), Year(run_date))
        Dim defer_rows As Integer = defer_dg.Rows.Count - 1
        For defer_idx = 0 To defer_rows
            Dim csid As Integer = defer_dg.Rows(defer_idx).Cells(0).Value
            Dim defer_no As Integer = defer_dg.Rows(defer_idx).Cells(2).Value
            If defer_no > 0 Then
                Form1.Collect_DefermentsTableAdapter.InsertQuery(csid, run_date, defer_no, Form1.inv_tbox.Text, vat_rate, Form1.defer_tb.Text)
            End If
        Next
    End Sub

    Private Sub deferfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        defer_dg.Rows.Clear()
        param2 = "select _rowid, schemeID from clientScheme where clientID = 1572"
        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        Dim cs_rows As Integer = no_of_rows - 1
        Dim cs_idx As Integer
        For cs_idx = 0 To cs_rows
            Dim cs_id As Integer = cs_ds.Tables(0).Rows(cs_idx).Item(0)
            Dim sch_id As Integer = cs_ds.Tables(0).Rows(cs_idx).Item(1)
            param2 = "select name from scheme where _rowid = " & sch_id
            Dim sch_ds As DataSet = get_dataset("onestep", param2)
            Dim sch_name As String = sch_ds.Tables(0).Rows(0).Item(0)
            defer_dg.Rows.Add(cs_id, sch_name)
        Next
    End Sub
End Class
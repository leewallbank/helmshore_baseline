<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class deferfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.defer_dg = New System.Windows.Forms.DataGridView
        Me.cs_id = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.sch_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.defer_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.defer_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'defer_dg
        '
        Me.defer_dg.AllowUserToAddRows = False
        Me.defer_dg.AllowUserToDeleteRows = False
        Me.defer_dg.AllowUserToOrderColumns = True
        Me.defer_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.defer_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cs_id, Me.sch_name, Me.defer_no})
        Me.defer_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.defer_dg.Location = New System.Drawing.Point(0, 0)
        Me.defer_dg.Name = "defer_dg"
        Me.defer_dg.Size = New System.Drawing.Size(386, 526)
        Me.defer_dg.TabIndex = 0
        '
        'cs_id
        '
        Me.cs_id.HeaderText = "CSID"
        Me.cs_id.Name = "cs_id"
        Me.cs_id.Visible = False
        '
        'sch_name
        '
        Me.sch_name.HeaderText = "Scheme"
        Me.sch_name.Name = "sch_name"
        Me.sch_name.ReadOnly = True
        Me.sch_name.Width = 200
        '
        'defer_no
        '
        Me.defer_no.HeaderText = "No of Deferments"
        Me.defer_no.Name = "defer_no"
        '
        'deferfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(386, 526)
        Me.Controls.Add(Me.defer_dg)
        Me.Name = "deferfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add deferment numbers"
        CType(Me.defer_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents defer_dg As System.Windows.Forms.DataGridView
    Friend WithEvents cs_id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sch_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents defer_no As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

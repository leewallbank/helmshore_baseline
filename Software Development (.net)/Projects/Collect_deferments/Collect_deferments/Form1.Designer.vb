<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.run_dtp = New System.Windows.Forms.DateTimePicker
        Me.runbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.defer_tb = New System.Windows.Forms.TextBox
        Me.inv_lbl = New System.Windows.Forms.Label
        Me.inv_tbox = New System.Windows.Forms.TextBox
        Me.FeesSQLDataSet = New Collect_deferments.FeesSQLDataSet
        Me.Collect_DefermentsTableAdapter = New Collect_deferments.FeesSQLDataSetTableAdapters.Collect_DefermentsTableAdapter
        Me.Vat_DatesTableAdapter = New Collect_deferments.FeesSQLDataSetTableAdapters.Vat_DatesTableAdapter
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'run_dtp
        '
        Me.run_dtp.Location = New System.Drawing.Point(119, 54)
        Me.run_dtp.Name = "run_dtp"
        Me.run_dtp.Size = New System.Drawing.Size(133, 20)
        Me.run_dtp.TabIndex = 0
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(148, 168)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(75, 23)
        Me.runbtn.TabIndex = 2
        Me.runbtn.Text = "Run report"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(313, 320)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 4
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(157, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Report Month"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(145, 248)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(85, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Deferment value"
        '
        'defer_tb
        '
        Me.defer_tb.Location = New System.Drawing.Point(160, 264)
        Me.defer_tb.Name = "defer_tb"
        Me.defer_tb.ReadOnly = True
        Me.defer_tb.Size = New System.Drawing.Size(56, 20)
        Me.defer_tb.TabIndex = 3
        '
        'inv_lbl
        '
        Me.inv_lbl.AutoSize = True
        Me.inv_lbl.Location = New System.Drawing.Point(168, 102)
        Me.inv_lbl.Name = "inv_lbl"
        Me.inv_lbl.Size = New System.Drawing.Size(59, 13)
        Me.inv_lbl.TabIndex = 7
        Me.inv_lbl.Text = "Invoice No"
        '
        'inv_tbox
        '
        Me.inv_tbox.Location = New System.Drawing.Point(139, 118)
        Me.inv_tbox.Name = "inv_tbox"
        Me.inv_tbox.Size = New System.Drawing.Size(100, 20)
        Me.inv_tbox.TabIndex = 1
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Collect_DefermentsTableAdapter
        '
        Me.Collect_DefermentsTableAdapter.ClearBeforeFill = True
        '
        'Vat_DatesTableAdapter
        '
        Me.Vat_DatesTableAdapter.ClearBeforeFill = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(419, 376)
        Me.Controls.Add(Me.inv_tbox)
        Me.Controls.Add(Me.inv_lbl)
        Me.Controls.Add(Me.defer_tb)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Controls.Add(Me.run_dtp)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Collect deferments"
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents run_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents FeesSQLDataSet As Collect_deferments.FeesSQLDataSet
    Friend WithEvents Collect_DefermentsTableAdapter As Collect_deferments.FeesSQLDataSetTableAdapters.Collect_DefermentsTableAdapter
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents defer_tb As System.Windows.Forms.TextBox
    Friend WithEvents inv_lbl As System.Windows.Forms.Label
    Friend WithEvents inv_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Vat_DatesTableAdapter As Collect_deferments.FeesSQLDataSetTableAdapters.Vat_DatesTableAdapter
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog

End Class

Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Vat_DatesTableAdapter.Fill(Me.FeesSQLDataSet.Vat_Dates)
        run_dtp.Value = DateAdd(DateInterval.Month, -1, Now)
        defer_tb.Text = "�15.00"
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        inv_tbox.Text = Trim(inv_tbox.Text)
        If inv_tbox.Text.Length < 3 Then
            MsgBox("Enter invoice number")
        Else
            run_date = run_dtp.Value
            'make it 1st of month
            Dim days As Integer = Format(run_date, "dd") - 1
            run_date = DateAdd(DateInterval.Day, -days, run_date)
            'get vat rate
            Dim vat_rows As Integer = Me.FeesSQLDataSet.Vat_Dates.Rows.Count - 1
            Dim vat_idx As Integer
            For vat_idx = 0 To vat_rows
                Dim vat_date As Date = Me.FeesSQLDataSet.Vat_Dates.Rows(vat_idx).Item(0)
                If Format(run_date, "yyyy-MM-dd") > Format(vat_date, "yyyy-MM-dd") Then
                    vat_rate = Me.FeesSQLDataSet.Vat_Dates.Rows(vat_idx).Item(1)
                    Exit For
                End If
            Next
            deferfrm.ShowDialog()
            Dim filename As String
            'now run crystal report
            Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
            Dim RD303report = New RD303
            Dim myArrayList1 As ArrayList = New ArrayList()
            myArrayList1.Add(run_date)
            SetCurrentValuesForParameterField1(RD303report, myArrayList1)
            Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
            myConnectionInfo.ServerName = "DebtRecovery"
            myConnectionInfo.DatabaseName = "DebtRecovery"
            myConnectionInfo.UserID = "vbnet"
            myConnectionInfo.Password = "tenbv"

            myConnectionInfo2.ServerName = "RossFeeTable"
            myConnectionInfo2.DatabaseName = "FeesSQL"
            myConnectionInfo2.UserID = "sa"
            myConnectionInfo2.Password = "sa"
            SetDBLogonForReport(myConnectionInfo, myConnectionInfo2, RD303report)
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "PDF files |*.pdf"
                .DefaultExt = ".pdf"
                .OverwritePrompt = True
                .FileName = "RD303 SLC Deferment Invoice.pdf"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                Application.DoEvents()
                RD303report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
                RD303report.close()
                MsgBox("Completed")
                Me.Close()
            End If
            Me.Close()
        End If
    End Sub

   
End Class

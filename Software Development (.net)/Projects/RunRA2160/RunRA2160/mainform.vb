Public Class mainform

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub artbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles artbtn.Click
        disable_buttons()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2160report = New RA2160
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(remittbox.Text)
        SetCurrentValuesForParameterField1(RA2160report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2160report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2160 remit Description report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2160report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2160report.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
        Me.Close()
    End Sub

    Private Sub brtbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()
        enable_buttons()
    End Sub

    Private Sub disable_buttons()
        artbtn.Enabled = False
        exitbtn.Enabled = False
    End Sub
    Private Sub enable_buttons()
        artbtn.Enabled = True
        exitbtn.Enabled = True
    End Sub

End Class
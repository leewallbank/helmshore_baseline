<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.exitbtn = New System.Windows.Forms.Button
        Me.datelbl = New System.Windows.Forms.Label
        Me.workbtn = New System.Windows.Forms.Button
        Me.dispbtn = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.testbtn = New System.Windows.Forms.Button
        Me.prodbtn = New System.Windows.Forms.Button
        Me.invbtn = New System.Windows.Forms.Button
        Me.Employed_bailiffs_calendarDataSet = New Employed_Bailiffs_RTD.Employed_bailiffs_calendarDataSet
        Me.Employed_bailiffs_calendarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Employed_bailiffs_calendarTableAdapter = New Employed_Bailiffs_RTD.Employed_bailiffs_calendarDataSetTableAdapters.Employed_bailiffs_calendarTableAdapter
        CType(Me.Employed_bailiffs_calendarDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Employed_bailiffs_calendarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(239, 353)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 7
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'datelbl
        '
        Me.datelbl.AutoSize = True
        Me.datelbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.datelbl.Location = New System.Drawing.Point(106, 20)
        Me.datelbl.Name = "datelbl"
        Me.datelbl.Size = New System.Drawing.Size(55, 16)
        Me.datelbl.TabIndex = 1
        Me.datelbl.Text = "Label1"
        '
        'workbtn
        '
        Me.workbtn.Location = New System.Drawing.Point(97, 73)
        Me.workbtn.Name = "workbtn"
        Me.workbtn.Size = New System.Drawing.Size(101, 23)
        Me.workbtn.TabIndex = 1
        Me.workbtn.Text = "Set working days"
        Me.workbtn.UseVisualStyleBackColor = True
        '
        'dispbtn
        '
        Me.dispbtn.Location = New System.Drawing.Point(97, 168)
        Me.dispbtn.Name = "dispbtn"
        Me.dispbtn.Size = New System.Drawing.Size(114, 23)
        Me.dispbtn.TabIndex = 2
        Me.dispbtn.Text = "Display Commissions"
        Me.dispbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(27, 353)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 6
        Me.ProgressBar1.Visible = False
        '
        'testbtn
        '
        Me.testbtn.Enabled = False
        Me.testbtn.Location = New System.Drawing.Point(28, 223)
        Me.testbtn.Name = "testbtn"
        Me.testbtn.Size = New System.Drawing.Size(99, 23)
        Me.testbtn.TabIndex = 3
        Me.testbtn.Text = "Update in Test"
        Me.testbtn.UseVisualStyleBackColor = True
        '
        'prodbtn
        '
        Me.prodbtn.Location = New System.Drawing.Point(177, 223)
        Me.prodbtn.Name = "prodbtn"
        Me.prodbtn.Size = New System.Drawing.Size(137, 23)
        Me.prodbtn.TabIndex = 4
        Me.prodbtn.Text = "Update in Production"
        Me.prodbtn.UseVisualStyleBackColor = True
        '
        'invbtn
        '
        Me.invbtn.Enabled = False
        Me.invbtn.Location = New System.Drawing.Point(97, 292)
        Me.invbtn.Name = "invbtn"
        Me.invbtn.Size = New System.Drawing.Size(101, 23)
        Me.invbtn.TabIndex = 5
        Me.invbtn.Text = "Print Invoices"
        Me.invbtn.UseVisualStyleBackColor = True
        '
        'Employed_bailiffs_calendarDataSet
        '
        Me.Employed_bailiffs_calendarDataSet.DataSetName = "Employed_bailiffs_calendarDataSet"
        Me.Employed_bailiffs_calendarDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Employed_bailiffs_calendarBindingSource
        '
        Me.Employed_bailiffs_calendarBindingSource.DataMember = "Employed_bailiffs calendar"
        Me.Employed_bailiffs_calendarBindingSource.DataSource = Me.Employed_bailiffs_calendarDataSet
        '
        'Employed_bailiffs_calendarTableAdapter
        '
        Me.Employed_bailiffs_calendarTableAdapter.ClearBeforeFill = True
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(412, 411)
        Me.Controls.Add(Me.invbtn)
        Me.Controls.Add(Me.prodbtn)
        Me.Controls.Add(Me.testbtn)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.dispbtn)
        Me.Controls.Add(Me.workbtn)
        Me.Controls.Add(Me.datelbl)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Employed Bailiffs RTD"
        CType(Me.Employed_bailiffs_calendarDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Employed_bailiffs_calendarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents datelbl As System.Windows.Forms.Label
    Friend WithEvents workbtn As System.Windows.Forms.Button
    Friend WithEvents dispbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents testbtn As System.Windows.Forms.Button
    Friend WithEvents prodbtn As System.Windows.Forms.Button
    Friend WithEvents invbtn As System.Windows.Forms.Button
    Friend WithEvents Employed_bailiffs_calendarDataSet As Employed_Bailiffs_RTD.Employed_bailiffs_calendarDataSet
    Friend WithEvents Employed_bailiffs_calendarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Employed_bailiffs_calendarTableAdapter As Employed_Bailiffs_RTD.Employed_bailiffs_calendarDataSetTableAdapters.Employed_bailiffs_calendarTableAdapter

End Class

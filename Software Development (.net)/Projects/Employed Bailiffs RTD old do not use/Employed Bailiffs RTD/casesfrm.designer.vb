<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class casesfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.debtor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.van_fees = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.case_van_status = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.el_van_fees = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.levy_fees = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.levy_comm = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.swp_fees = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.visit1_fees = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.visit2_fees = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.visit3_fees = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.debtor, Me.van_fees, Me.case_van_status, Me.el_van_fees, Me.levy_fees, Me.levy_comm, Me.swp_fees, Me.visit1_fees, Me.visit2_fees, Me.visit3_fees})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(940, 537)
        Me.DataGridView1.TabIndex = 0
        '
        'debtor
        '
        Me.debtor.HeaderText = "Debtor"
        Me.debtor.Name = "debtor"
        Me.debtor.ReadOnly = True
        '
        'van_fees
        '
        Me.van_fees.HeaderText = "Van Fees"
        Me.van_fees.Name = "van_fees"
        Me.van_fees.ReadOnly = True
        '
        'case_van_status
        '
        Me.case_van_status.HeaderText = " "
        Me.case_van_status.Name = "case_van_status"
        Me.case_van_status.ReadOnly = True
        Me.case_van_status.Width = 20
        '
        'el_van_fees
        '
        Me.el_van_fees.HeaderText = "Eligible Van Fees"
        Me.el_van_fees.Name = "el_van_fees"
        Me.el_van_fees.ReadOnly = True
        '
        'levy_fees
        '
        Me.levy_fees.HeaderText = "Levy Fees"
        Me.levy_fees.Name = "levy_fees"
        Me.levy_fees.ReadOnly = True
        '
        'levy_comm
        '
        Me.levy_comm.HeaderText = "Levy Commission"
        Me.levy_comm.Name = "levy_comm"
        Me.levy_comm.ReadOnly = True
        '
        'swp_fees
        '
        Me.swp_fees.HeaderText = "SWP Comm"
        Me.swp_fees.Name = "swp_fees"
        Me.swp_fees.ReadOnly = True
        '
        'visit1_fees
        '
        Me.visit1_fees.HeaderText = "Visit1 Fees"
        Me.visit1_fees.Name = "visit1_fees"
        Me.visit1_fees.ReadOnly = True
        '
        'visit2_fees
        '
        Me.visit2_fees.HeaderText = "Visit2 Fees"
        Me.visit2_fees.Name = "visit2_fees"
        Me.visit2_fees.ReadOnly = True
        '
        'visit3_fees
        '
        Me.visit3_fees.HeaderText = "Visit3 Fees"
        Me.visit3_fees.Name = "visit3_fees"
        Me.visit3_fees.ReadOnly = True
        '
        'casesfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(940, 537)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "casesfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "casesfrm"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents debtor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents van_fees As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents case_van_status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents el_van_fees As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents levy_fees As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents levy_comm As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents swp_fees As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents visit1_fees As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents visit2_fees As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents visit3_fees As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

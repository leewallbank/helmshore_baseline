Public Class mainform

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        openbtn.Enabled = False
        exitbtn.Enabled = False
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If Not fileok Then
            MsgBox("File NOT Opened")
            openbtn.Enabled = True
            exitbtn.Enabled = True
            Exit Sub
        End If
        'do manipulation here
        Dim file As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim idx, idx2 As Integer
        Dim lines As Integer = 0
        Dim clref As String = ""
        Dim debtor As Integer
        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'insert debtor at front of file
        For idx = 0 To lines - 1
            Dim orig_line As String = Microsoft.VisualBasic.Right(line(idx), line(idx).Length)
            If orig_line.Length < 10 Then
                Continue For
            End If
            'get clref
            For idx2 = 1 To 50
                If Mid(line(idx), idx2, 1) = "|" Then
                    clref = Mid(line(idx), 1, idx2 - 1)
                    Exit For
                End If
            Next
            'validate case details
            If clref = Nothing Or Microsoft.VisualBasic.Left(clref, 3) = "   " Then
                errorfile = errorfile & "Line  " & idx2 & " - No client reference" & vbNewLine
                Continue For
            End If

            'get debtor ID from onestep
            param1 = "onestep"
            param2 = "select _rowid from Debtor where client_ref = '" & clref & "'" & _
            " and clientschemeID = 2106"
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                errorfile = errorfile & "Line  " & idx & " - client reference " & clref & _
                " not found on onestep" & vbNewLine
            Else
                'save case in outline
                debtor = debtor_dataset.Tables(0).Rows(0).Item(0)
                outfile = outfile & debtor & "|" & orig_line & vbNewLine
            End If
        Next
        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        filename = filename_prefix & "_preprocess.txt"
        My.Computer.FileSystem.WriteAllText(filename, outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText("error.txt", errorfile, False)
            MsgBox("Errors found - see error.txt")
        Else
            MsgBox("No errors found - file at " & filename)
        End If
        Me.Close()
    End Sub

End Class

Public Class monthfrm

    Private Sub monthfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            Monthly_statsTableAdapter.UpdateQuery(live_no, arr_no, arr_val, live_val, csid, start_year, start_month)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub


    Private Sub monthfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Monthly_statsDataSet.Monthly_stats' table. You can move, or remove it, as needed.
        Me.Monthly_statsTableAdapter.Fill(Me.Monthly_statsDataSet.Monthly_stats)
        Label1.Text = "Rushcliffe "
        If csid = 1541 Then
            Label1.Text = Label1.Text & " CTAX "
        Else
            Label1.Text = Label1.Text & " NNDR "
        End If
        Label1.Text = Label1.Text & " for " & Format(start_date, "MMM yyyy")
        Me.Monthly_statsTableAdapter.FillBy(Me.Monthly_statsDataSet.Monthly_stats, csid, start_year, start_month)
        DataGridView1.Rows.Clear()
        If Monthly_statsDataSet.Tables(0).Rows.Count = 0 Then
            Try
                Monthly_statsTableAdapter.InsertQuery(csid, start_year, start_month, 0, 0, 0, 0)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
            live_no = 0
            live_val = 0
            arr_no = 0
            arr_val = 0
        Else
            live_no = Monthly_statsDataSet.Tables(0).Rows(0).Item(3)
            live_val = Monthly_statsDataSet.Tables(0).Rows(0).Item(6)
            arr_no = Monthly_statsDataSet.Tables(0).Rows(0).Item(4)
            arr_val = Monthly_statsDataSet.Tables(0).Rows(0).Item(5)
        End If

        DataGridView1.Rows.Add(live_no, Format(live_val, "######0.00"), arr_no, Format(arr_val, "######0.00"))

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub DataGridView1_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValidated
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        If e.ColumnIndex = 0 Then
            live_no = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        ElseIf e.ColumnIndex = 1 Then
            live_val = DataGridView1.Rows(e.RowIndex).Cells(1).Value
        ElseIf e.ColumnIndex = 2 Then
            arr_no = DataGridView1.Rows(e.RowIndex).Cells(2).Value
        ElseIf e.ColumnIndex = 3 Then
            arr_val = DataGridView1.Rows(e.RowIndex).Cells(3).Value
        End If
    End Sub

    Private Sub DataGridView1_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DataGridView1.CellValidating
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        If e.ColumnIndex = 0 Then
            Try
                live_no = e.FormattedValue
                If live_no <> e.FormattedValue Then
                    MsgBox("live number can't contain a decimal")
                    e.Cancel = True
                End If
            Catch ex As Exception
                MsgBox("live number must be numeric")
                e.Cancel = True
            End Try
        ElseIf e.ColumnIndex = 1 Then
            Try
                live_val = e.FormattedValue
            Catch ex As Exception
                MsgBox("live value must be numeric")
                e.Cancel = True
            End Try
        ElseIf e.ColumnIndex = 2 Then
            Try
                arr_no = e.FormattedValue
                If arr_no <> e.FormattedValue Then
                    MsgBox("Arrangement number can't contain a decimal")
                    e.Cancel = True
                End If
            Catch ex As Exception
                MsgBox("Arrangement number must be numeric")
                e.Cancel = True
            End Try
        ElseIf e.ColumnIndex = 3 Then
            Try
                arr_val = e.FormattedValue
            Catch ex As Exception
                MsgBox("Arrangement value must be numeric")
                e.Cancel = True
            End Try
        End If
    End Sub


    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Monthly_statsBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.Monthly_statsBindingSource.EndEdit()
        Me.Monthly_statsTableAdapter.Update(Me.Monthly_statsDataSet.Monthly_stats)

    End Sub
End Class
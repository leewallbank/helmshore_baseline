<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class monthfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.dglive_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dglive_val = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgArr_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dgarr_val = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Label1 = New System.Windows.Forms.Label
        Me.exitbtn = New System.Windows.Forms.Button
        Me.Monthly_statsDataSet = New RushcliffeMonthly.Monthly_statsDataSet
        Me.Monthly_statsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Monthly_statsTableAdapter = New RushcliffeMonthly.Monthly_statsDataSetTableAdapters.Monthly_statsTableAdapter
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Monthly_statsDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Monthly_statsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dglive_no, Me.dglive_val, Me.dgArr_no, Me.dgarr_val})
        Me.DataGridView1.Location = New System.Drawing.Point(1, 69)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(395, 78)
        Me.DataGridView1.TabIndex = 0
        '
        'dglive_no
        '
        Me.dglive_no.HeaderText = "Live No"
        Me.dglive_no.Name = "dglive_no"
        Me.dglive_no.Width = 75
        '
        'dglive_val
        '
        Me.dglive_val.HeaderText = "Live Val"
        Me.dglive_val.Name = "dglive_val"
        '
        'dgArr_no
        '
        Me.dgArr_no.HeaderText = "Arrangement No"
        Me.dgArr_no.Name = "dgArr_no"
        Me.dgArr_no.Width = 75
        '
        'dgarr_val
        '
        Me.dgarr_val.HeaderText = "Arrangement Val"
        Me.dgarr_val.Name = "dgarr_val"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(39, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Label1"
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(253, 23)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(166, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "EXIT"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'Monthly_statsDataSet
        '
        Me.Monthly_statsDataSet.DataSetName = "Monthly_statsDataSet"
        Me.Monthly_statsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Monthly_statsBindingSource
        '
        Me.Monthly_statsBindingSource.DataMember = "Monthly_stats"
        Me.Monthly_statsBindingSource.DataSource = Me.Monthly_statsDataSet
        '
        'Monthly_statsTableAdapter
        '
        Me.Monthly_statsTableAdapter.ClearBeforeFill = True
        '
        'monthfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(447, 151)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "monthfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "monthfrm"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Monthly_statsDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Monthly_statsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dglive_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dglive_val As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgArr_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgarr_val As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents Monthly_statsDataSet As RushcliffeMonthly.Monthly_statsDataSet
    Friend WithEvents Monthly_statsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Monthly_statsTableAdapter As RushcliffeMonthly.Monthly_statsDataSetTableAdapters.Monthly_statsTableAdapter
End Class

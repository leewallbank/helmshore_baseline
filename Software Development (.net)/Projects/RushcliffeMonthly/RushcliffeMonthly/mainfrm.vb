Public Class mainfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub ctaxbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ctaxbtn.Click
        csid = 1541
        monthfrm.ShowDialog()
    End Sub

    Private Sub nndrbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nndrbtn.Click
        csid = 1542
        monthfrm.ShowDialog()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        start_year = Format(Now, "yyyy")
        start_month = Format(Now, "%M")
        start_date = CDate("01." & start_month & "." & start_year)
        Label1.Text = "Update " & Format(start_date, "MMM yyyy")
    End Sub
End Class

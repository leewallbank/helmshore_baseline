Imports System.Configuration
Imports System.IO
Public Class Form1

    Dim file_name As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        remit_dtp.Value = DateAdd(DateInterval.Day, -Weekday(Now), Now)
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        Dim prod_run As Boolean = False
        Dim env_str As String = ""
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then prod_run = True
        param2 = "select clientSchemeID, _rowid, split_debt, split_costs from Remit where date = '" & Format(remit_dtp.Value, "yyyy-MM-dd") & "'"
        Dim remit_ds As DataSet = get_dataset("onestep", param2)
        Dim remit_rows As Integer = no_of_rows - 1
        Dim idx As Integer
        Dim dir_path As String = "\\rossvr02\Shared\Company\High Court Enforcement\"
        If Not prod_run Then dir_path = "H:\temp\"

        Dim dir_name As String = ""
        For idx = 0 To remit_rows
            Try
                ProgressBar1.Value = (idx / remit_rows) * 100
            Catch ex As Exception
                ProgressBar1.Value = 0
            End Try
            Application.DoEvents()
            Dim cs_ID As Integer = remit_ds.Tables(0).Rows(idx).Item(0)
            param2 = "select schemeID, clientID from clientScheme where _rowid = " & cs_ID & _
                " and branchID = 4"
            Dim cs_ds As DataSet = get_dataset("onestep", param2)
            Dim cs_rows As Integer = no_of_rows - 1
            Dim idx2 As Integer
            For idx2 = 0 To cs_rows
                Dim sch_ID As Integer = cs_ds.Tables(0).Rows(idx2).Item(0)
                param2 = "select name from Scheme where _rowid = " & sch_ID
                Dim sch_ds As DataSet = get_dataset("onestep", param2)
                If InStr(LCase(sch_ds.Tables(0).Rows(0).Item(0)), "high") = 0 Then
                    Continue For
                End If
                Dim cl_ID As Integer = cs_ds.Tables(0).Rows(idx2).Item(1)
                param2 = "select name from Client where _rowid = " & cl_ID
                Dim cl_ds As DataSet = get_dataset("onestep", param2)
                Dim cl_name As String = Trim(cl_ds.Tables(0).Rows(0).Item(0))

                Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
                'run RA1620 crystal report if debt +costs > 0
                If remit_ds.Tables(0).Rows(idx).Item(2) + remit_ds.Tables(0).Rows(idx).Item(3) > 0 Then
                    Dim RA1620report = New RA1620
                    Dim myArrayList1 As ArrayList = New ArrayList()
                    myArrayList1.Add(cl_ID)
                    SetCurrentValuesForParameterField1(RA1620report, myArrayList1)
                    myArrayList1.Add(Format(remit_dtp.Value, "yyyy-MM-dd"))
                    SetCurrentValuesForParameterField2(RA1620report, myArrayList1)
                    myConnectionInfo.ServerName = "DebtRecovery"
                    myConnectionInfo.DatabaseName = "DebtRecovery"
                    myConnectionInfo.UserID = "vbnet"
                    myConnectionInfo.Password = "tenbv"
                    SetDBLogonForReport(myConnectionInfo, RA1620report)

                    'create directories if not there
                    dir_name = dir_path & "Weekly Remittances\" & cl_name & "\" & Format(remit_dtp.Value, "yyyy-MM-dd")
                    Try
                        Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                        If System.IO.Directory.Exists(dir_name) = False Then
                            di = System.IO.Directory.CreateDirectory(dir_name)
                        End If
                    Catch ex As Exception
                        MsgBox("Unable to create folder")
                        End
                    End Try

                    file_name = dir_name & "\RA1620 High Court Remittance.pdf"

                    RA1620report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, file_name)
                    RA1620report.close()
                End If
                'see if RA1618 is required
                Dim remitID As Integer = remit_ds.Tables(0).Rows(idx).Item(1)
                param2 = "select description, value_line from RemitDetail where remitID = " & remitID & _
                " and value_line > 0"
                Dim remit2_ds As DataSet = get_dataset("onestep", param2)
                Dim remit2_rows As Integer = no_of_rows - 1
                Dim idx3 As Integer
                Dim ra1618_reqd As Boolean = False
                For idx3 = 0 To remit2_rows
                    If InStr(remit2_ds.Tables(0).Rows(idx3).Item(0), "HMCTS - Fi-Fa Fee") > 0 Then
                        ra1618_reqd = True
                        Exit For
                    End If
                Next
                If ra1618_reqd Then
                    Dim RA1618report = New RA1618
                    Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
                    Dim myArrayList2 As ArrayList = New ArrayList()
                    myArrayList2.Add(cl_ID)
                    SetCurrentValuesForParameterField1(RA1618report, myArrayList2)
                    myArrayList2.Add(Format(remit_dtp.Value, "yyyy-MM-dd"))
                    SetCurrentValuesForParameterField2(RA1618report, myArrayList2)
                    myConnectionInfo2.ServerName = "DebtRecovery"
                    myConnectionInfo2.DatabaseName = "DebtRecovery"
                    myConnectionInfo2.UserID = "vbnet"
                    myConnectionInfo2.Password = "tenbv"
                    SetDBLogonForReport(myConnectionInfo2, RA1618report)

                    'create directories if not there
                    dir_name = dir_path & "Weekly Court Fee Invoices\" & cl_name & "\" & Format(remit_dtp.Value, "yyyy-MM-dd")
                    Try
                        Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                        If System.IO.Directory.Exists(dir_name) = False Then
                            di = System.IO.Directory.CreateDirectory(dir_name)
                        End If
                    Catch ex As Exception
                        MsgBox("Unable to create folder")
                        End
                    End Try
                    file_name = dir_name & "\RA1618 High Court Fee Invoice.pdf"
                    RA1618report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, file_name)
                    RA1618report.close()
                End If
                Dim ra1619_reqd As Boolean = False
                'see if any return fees for client remit
                param2 = "select _rowid from Debtor where return_remitID = " & remit_ds.Tables(0).Rows(idx).Item(1) & _
                " and return_fee > 0"
                Dim debt_ds As DataSet = get_dataset("onestep", param2)
                If no_of_rows > 0 Then
                    ra1619_reqd = True
                End If

                If ra1619_reqd Then
                    Dim RA1619report = New RA1619
                    Dim myConnectionInfo3 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
                    Dim myArrayList3 As ArrayList = New ArrayList()
                    myArrayList3.Add(cl_ID)
                    SetCurrentValuesForParameterField1(RA1619report, myArrayList3)
                    myArrayList3.Add(Format(remit_dtp.Value, "yyyy-MM-dd"))
                    SetCurrentValuesForParameterField2(RA1619report, myArrayList3)
                    myConnectionInfo3.ServerName = "DebtRecovery"
                    myConnectionInfo3.DatabaseName = "DebtRecovery"
                    myConnectionInfo3.UserID = "vbnet"
                    myConnectionInfo3.Password = "tenbv"
                    SetDBLogonForReport(myConnectionInfo3, RA1619report)

                    'create directories if not there
                    dir_name = dir_path & "Weekly Abortive Fee Invoices\" & cl_name & "\" & Format(remit_dtp.Value, "yyyy-MM-dd")
                    Try
                        Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                        If System.IO.Directory.Exists(dir_name) = False Then
                            di = System.IO.Directory.CreateDirectory(dir_name)
                        End If
                    Catch ex As Exception
                        MsgBox("Unable to create folder")
                        End
                    End Try
                    file_name = dir_name & "\RA1619 High Court Abortive Fee Invoice.pdf"
                    RA1619report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, file_name)
                    RA1619report.close()

                    'now run RA1619CC report
                    Dim RA1619CCreport = New RA1619CC
                    Dim myConnectionInfo4 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
                    Dim myArrayList4 As ArrayList = New ArrayList()
                    myArrayList4.Add(cl_ID)
                    SetCurrentValuesForParameterField1(RA1619CCreport, myArrayList4)
                    myArrayList4.Add(Format(remit_dtp.Value, "yyyy-MM-dd"))
                    SetCurrentValuesForParameterField2(RA1619CCreport, myArrayList4)
                    myConnectionInfo4.ServerName = "DebtRecovery"
                    myConnectionInfo4.DatabaseName = "DebtRecovery"
                    myConnectionInfo4.UserID = "vbnet"
                    myConnectionInfo4.Password = "tenbv"
                    SetDBLogonForReport(myConnectionInfo4, RA1619CCreport)
                    file_name = dir_name & "\RA1619CC High Court Abortive Fee Invoice.pdf"
                    RA1619CCreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, file_name)
                    RA1619CCreport.close()
                End If
            Next
        Next
        MsgBox("Reports created")
        Me.Close()
    End Sub
End Class

Public Class daysfrm

    Private Sub daysfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        'get a list of employed bailiffs
        param1 = "onestep"
        param2 = "Bailiff"
        param3 = "01_rowid 2name_sur 3name_fore"
        param4 = _
        " where _rowid = 2548 or _rowid = 2551 or _rowid = 2554"
        param5 = "order by 2"
        ret_code = get_table(param1, param2, param3, param4, param5)
        If ret_code <> 0 Then
            MessageBox.Show("Error accessing onestep - ret code = " & ret_code)
            Me.Close()
            Exit Sub
        End If

        'fill datagrid
        Dim idx As Integer
        DataGridView1.Rows.Clear()
        For idx = 1 To no_of_rows
            Dim bail_id = table_array(idx, 1)
            Dim bail_name = Trim(table_array(idx, 2)) & ", " & Trim(table_array(idx, 3))
            'check if already exists on table
            Employed_bailiffsTableAdapter.FillBy(Employed_bailiffsDataSet.Employed_bailiffs, bail_id, start_month, start_year)
            Dim bail_days As Decimal
            Try
                bail_days = Employed_bailiffsDataSet.Employed_bailiffs.Rows(0).Item(3)
            Catch
                bail_days = 20
                Employed_bailiffsTableAdapter.InsertQuery(bail_id, start_month, start_year, Format(bail_days, "##.#"), 0)
            End Try
            DataGridView1.Rows.Add(bail_id, bail_name, bail_days)
        Next
    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.ColumnIndex = 2 And e.RowIndex >= 0 Then
            Dim bail_days, bail_id As Decimal
            Try
                bail_days = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            Catch
                MessageBox.Show("Days must be numeric")
                Exit Sub
            End Try
            Try
                bail_id = DataGridView1.Rows(e.RowIndex).Cells(0).Value
                Employed_bailiffsTableAdapter.UpdateQuery(bail_days, bail_id, start_month, start_year)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
End Class
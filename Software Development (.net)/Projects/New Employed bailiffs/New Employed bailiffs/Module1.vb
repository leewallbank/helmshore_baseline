Module Module1
    Public start_date, end_date, start_date_14 As Date
    Public start_month, start_year As String
    Public row As DataRow
    Public bail_no, max_bail_no, tot_pif As Integer
    Public doc_array() As String, doc2_array As String
    Public bail_array() As bail_struct
    Public case_array() As case_struct
    Public vat_rate As Decimal
    Public date_for_month As Date
    Public conn1 As Boolean
    Public Sub open_print_form()
        Dim idx As Integer
        Dim debtor, levy_fees, case_van_status, van_fees, el_van_fees, levy_comm, visit1_comm, visit2_comm, swp_comm As String
        PrintForm.txtDocument.Text = doc_array(bail_no) & vbNewLine
        PrintForm.txtDocument.Text = PrintForm.txtDocument.Text & "Invoice number is " & bail_array(bail_no).inv_no & vbNewLine & vbNewLine
        PrintForm.txtDocument.Text = PrintForm.txtDocument.Text & "Debtor       Van Fees          Eligible       Levy Fees       Levy Comm       Visit1 Comm       Visit2 Comm       SWP Comm" & vbNewLine
        For idx = 0 To tot_pif
            If bail_array(bail_no).bail_id = case_array(idx).bail_id Then
                debtor = case_array(idx).debtor
                case_van_status = Microsoft.VisualBasic.Left(case_array(idx).status, 1)
                van_fees = Format(case_array(idx).van_fees, "�#####0.00")
                Dim l_van_fees As Integer = Microsoft.VisualBasic.Len(van_fees) * 2
                el_van_fees = Format(case_array(idx).el_van_fees, "�#####0.00")
                Dim l_el_van_fees As Integer = Microsoft.VisualBasic.Len(el_van_fees) * 2
                levy_fees = Format(case_array(idx).levy_fees, "�######0.00")
                Dim l_levy_fees As Integer = Microsoft.VisualBasic.Len(levy_fees) * 2
                levy_comm = Format(case_array(idx).levy_comm, "�#####0.00")
                Dim l_levy_comm As Integer = Microsoft.VisualBasic.Len(levy_comm) * 2
                visit1_comm = Format(case_array(idx).visit1_comm, "�#####0.00")
                Dim l_visit1 As Integer = Microsoft.VisualBasic.Len(visit1_comm) * 2
                visit2_comm = Format(case_array(idx).visit2_comm, "�#####0.00")
                Dim l_visit2 As Integer = Microsoft.VisualBasic.Len(visit2_comm) * 2
                swp_comm = (Format(case_array(idx).swp_comm, "�#####0.00"))
                PrintForm.txtDocument.Text = PrintForm.txtDocument.Text & debtor & Space(5) & _
                van_fees & Space(18 - l_van_fees) & case_van_status & Space(5) & el_van_fees & Space(20 - l_el_van_fees) & _
                levy_fees & Space(25 - l_levy_fees) & levy_comm & Space(30 - l_levy_comm) & _
                visit1_comm & Space(26 - l_visit1) & visit2_comm & Space(28 - l_visit2) & swp_comm & vbNewLine
            End If
        Next
        PrintForm.ShowDialog()
    End Sub
    Public Structure bail_struct
        Dim bail_id As Integer
        Dim bail_name As String
        Dim pif As Integer
        Dim deficit_bf As Decimal
        Dim deficit_cf As Decimal
        Dim amt_payable As Decimal
        Dim sage_acc As String
        Dim percent As Decimal
        Dim inv_no As String
    End Structure
    Public Structure case_struct
        Dim bail_id As Integer
        Dim debtor As Integer
        Dim status As String
        Dim levy_fees As Decimal
        Dim van_fees As Decimal
        Dim el_van_fees As Decimal
        Dim levy_comm As Decimal
        Dim visit1_comm As Decimal
        Dim visit2_comm As Decimal
        Dim swp_comm As Decimal
        Dim date_allocated As Date
        Dim date_visited As Date
    End Structure
   
End Module

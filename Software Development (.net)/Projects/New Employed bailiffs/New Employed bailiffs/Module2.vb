Module Module2
    ''get all impact bailiffs
    '    param1 = "onestep"
    '    param2 = "select _rowid, name_sur, name_fore from Bailiff where agent_type = 'B' " & _
    '    " and _rowid = 2606 and internalExternal = 'E' and status = 'O' and left(typeSub,6) = 'Impact' order by name_sur"
    'Dim bail_dataset As DataSet = get_dataset(param1, param2)
    'Dim bail_name As String = ""
    'Dim bail_id As Integer
    ''DataGridView1.Rows.Clear()
    'Dim bidx As Integer = 0
    'Dim bailiff_rows As Integer = no_of_rows
    '    bail_no = 0
    '    max_bail_no = 0
    '    tot_pif = 0
    'Dim max_rows As Integer = 1000
    '    ReDim doc_array(no_of_rows)
    '    ReDim bail_array(no_of_rows)
    '    ReDim case_array(max_rows)
    '    For Each row In bail_dataset.Tables(0).Rows
    '        doctext.Text = "                           Employed Bailiff Commission Run for Month " & Format(start_date, "MMM yyyy") & vbNewLine & vbNewLine
    '        mainfrm.ProgressBar1.Value = bidx / bailiff_rows * 100
    'Dim pif As Integer = 0
    'Dim tot_levy_comm As Decimal = 0
    'Dim tot_van_fees As Decimal = 0
    'Dim tot_unpaid_van As Decimal = 0
    'Dim tot_unpaid_van_comm As Decimal = 0
    'Dim tot_unpaid_swp As Decimal = 0
    'Dim tot_unpaid_levy As Decimal = 0
    'Dim tot_unpaid_visit1 As Decimal = 0
    'Dim tot_unpaid_visit2 As Decimal = 0
    'Dim first_visit_no As Integer = 0
    'Dim second_visit_no As Integer = 0
    'Dim tot_first_visit_comm As Decimal = 0
    'Dim tot_second_visit_comm As Decimal = 0
    'Dim swp_no As Integer = 0
    'Dim tot_swp_comm As Decimal = 0
    'Dim bail_deficit_cf As Decimal = 0
    'Dim tot_already_paid As Decimal = 0
    '        bail_id = bail_dataset.Tables(0).Rows(bidx).Item(0)
    '        bail_array(bail_no).bail_id = bail_id

    '        bail_name = Trim(bail_dataset.Tables(0).Rows(bidx).Item(2)) & " " & Trim(bail_dataset.Tables(0).Rows(bidx).Item(1))
    '        bail_array(bail_no).bail_name = bail_name
    '        bail_array(bail_no).sage_acc = "vb" & LCase(Microsoft.VisualBasic.Left(bail_dataset.Tables(0).Rows(bidx).Item(1), 4) & _
    '            Microsoft.VisualBasic.Left(bail_dataset.Tables(0).Rows(bidx).Item(2), 1))
    '        bidx += 1
    ''get number of days and deficit from last month
    '        param1 = "Employed"
    '        param2 = "select bail_days, bail_deficit_bf from Employed_bailiffs " & _
    '        " where bail_id = " & bail_id & " and bail_year = " & start_year & _
    '        " and bail_month = " & start_month
    'Dim emp_dataset As DataSet = get_dataset(param1, param2)
    '        If no_of_rows = 0 Then
    '            MessageBox.Show("Last month has not been run yet")
    '            Me.Close()
    '            Exit Sub
    '        End If
    'Dim bail_days As Integer = emp_dataset.Tables(0).Rows(0).Item(0)
    'Dim bail_deficit_bf As Decimal = emp_dataset.Tables(0).Rows(0).Item(1)
    '        doctext.Text = doctext.Text & bail_name & " " & Format(bail_id, "#") & _
    '                    "   Number of working days = " & Format(bail_days, "#") & vbNewLine
    'Dim net_target As Decimal = bail_days * 200
    'Dim target As Decimal = net_target + bail_deficit_bf
    '        bail_array(bail_no).deficit_bf = bail_deficit_bf
    'Dim target2 As Decimal = (bail_days * 351) + bail_deficit_bf
    '        doctext.Text = doctext.Text & "Target commission " & bail_days & " days @ �200 = " & _
    '        Format(net_target, "�#0.00")
    '        doctext.Text = doctext.Text & "    Plus deficit B/F of " & Format(bail_deficit_bf, "�#0.00") & _
    '        "  = " & Format(target, "�#0.00") & vbNewLine & vbNewLine

    ''get all visits made 30 days before start of last month
    'Dim earliest_visit As Date = DateAdd(DateInterval.Day, -30, start_date)
    '        param1 = "onestep"
    '        param2 = "select debtorID, date_visited from Visit " & _
    '        "where  bailiffID = " & bail_id & _
    '        " and date_visited > '" & Format(earliest_visit, "yyyy.MM.dd") & "'" & _
    '        " order by debtorID, date_visited desc"
    'Dim visit_dataset As DataSet = get_dataset(param1, param2)
    '        If no_of_rows = 0 Then
    '            Continue For
    '        End If
    'Dim debtorid As Integer = 0
    'Dim row2 As DataRow
    'Dim vidx As Integer = 0
    'Dim date_visited As Date
    'Dim last_debtorid As Integer = 0
    '        For Each row2 In visit_dataset.Tables(0).Rows
    '            debtorid = visit_dataset.Tables(0).Rows(vidx).Item(0)
    '            date_visited = visit_dataset.Tables(0).Rows(vidx).Item(1)
    '            vidx += 1
    '            If debtorid = last_debtorid Then
    '                Continue For
    '            End If
    '            last_debtorid = debtorid
    'Dim levy_fees As Decimal = 0
    'Dim levy_comm As Decimal = 0
    'Dim other_fc_fees As Decimal = 0
    'Dim van_fees As Decimal = 0
    'Dim el_van_fees As Decimal = 0
    'Dim already_paid As Decimal = 0
    'Dim swp_comm As Decimal = 0
    'Dim visit1_comm As Decimal = 0
    'Dim visit2_comm As Decimal = 0
    'Dim case_van_status As String = " "
    ''ignore if there is a later visit date
    '            param1 = "onestep"
    '            param2 = "select max(date_visited) from Visit where debtorID = " & debtorid & _
    '            " and date_visited is not null "
    'Dim visit3_dataset As DataSet = get_dataset(param1, param2)
    'Dim last_date_visited As Date
    '            Try
    '                last_date_visited = visit3_dataset.Tables(0).Rows(0).Item(0)
    '            Catch ex As Exception
    '                Continue For
    '            End Try
    '            If Format(last_date_visited, "yyyy.MM.dd") <> Format(date_visited, "yyyy.MM.dd") Then
    '                Continue For
    '            End If

    ''check debtor status is fully paid/successful
    '            param1 = "onestep"
    '            param2 = "select _rowid from Debtor " & _
    '            "where _rowid = " & debtorid & " and (status = 'S' or status = 'F')"
    'Dim debtor_dataset As DataSet = get_dataset(param1, param2)
    '            If no_of_rows = 0 Then
    '                Continue For
    '            End If

    ''ignore if a payment > end date exists or a waiting payment
    ''include waiting if status date less than month end date
    '            param1 = "onestep"
    '            param2 = "select debtorID from Payment where debtorID = " & debtorid & _
    '            " and status = 'W' " & _
    '            " and status_date > '" & Format(end_date, "yyyy.MM.dd") & "'"
    'Dim payment2_dataset As DataSet = get_dataset(param1, param2)
    '            If no_of_rows > 0 Then
    '                Continue For
    '            End If

    ''check a payment was remitted last month or clear date for waiting last month
    '            param1 = "onestep"
    '            param2 = "select status_date, amount, date from Payment " & _
    '                " where debtorID = " & debtorid & " and " & _
    '                " (status = 'W' or status = 'R') " & _
    '                " order by status_date desc"
    'Dim payment_dataset As DataSet = get_dataset(param1, param2)
    'Dim status_date, payment_date, visit_date30 As Date
    'Dim pidx As Integer
    '            Try
    '                status_date = payment_dataset.Tables(0).Rows(0).Item(0)
    '            Catch ex As Exception
    '                Continue For
    '            End Try
    '            status_date = payment_dataset.Tables(0).Rows(0).Item(0)
    '            If status_date < start_date Or status_date > end_date Then
    '                Continue For
    '            End If

    ''get latest status/payment date (ignore if payment amount is zero)
    '            For pidx = 0 To no_of_rows - 1
    '                If payment_dataset.Tables(0).Rows(pidx).Item(1) = 0 Then
    '                    Continue For
    '                End If
    '                payment_date = payment_dataset.Tables(0).Rows(pidx).Item(2)
    '                visit_date30 = DateAdd("d", +30, date_visited)
    '                Exit For
    '            Next

    '            If payment_date > visit_date30 Then
    '                Continue For
    '            End If

    ''get latest allocation date for debtor
    '            param1 = "onestep"
    '            param2 = "select max(date_allocated) from Visit where debtorID = " & debtorid & _
    '            " and bailiffID = " & bail_id

    'Dim visita_dataset As DataSet = get_dataset(param1, param2)
    'Dim date_allocated As Date
    '            Try
    '                date_allocated = visita_dataset.Tables(0).Rows(0).Item(0)
    '            Catch ex As Exception
    '                Continue For
    '            End Try

    ''get fees added
    ''include all van fees
    '            param1 = "onestep"
    '            param2 = "select _rowid, fee_amount,type,fee_remit_col from Fee where debtorID = " & debtorid & _
    '            " and (bailiffID = " & bail_id & " or (bailiffID is not null and fee_remit_col = 4))  and fee_amount <> 0"
    'Dim fee_dataset As DataSet = get_dataset(param1, param2)
    'Dim row3 As DataRow
    'Dim fidx As Integer = 0
    '            For Each row3 In fee_dataset.Tables(0).Rows
    '                If fee_dataset.Tables(0).Rows(fidx).Item(3) <> 3 And _
    '                fee_dataset.Tables(0).Rows(fidx).Item(3) <> 4 Then
    '                    fidx += 1
    '                    Continue For
    '                End If
    ''check that fee was remitted after allocation date
    'Dim feeid As Integer = fee_dataset.Tables(0).Rows(fidx).Item(0)
    ''param1 = "Fees"
    ''param2 = "select sum(Amount) from FeePayments where feeId = " & feeid '& _
    ' ''" and RemitedDate >='" & Format(date_allocated, "yyyy.MM.dd") & "'"
    ''Dim feepayments_datset As DataSet = get_dataset(param1, param2)
    'Dim fees As Decimal
    '                Try
    '                    fees = fee_dataset.Tables(0).Rows(fidx).Item(1)
    '                Catch
    '                    fidx += 1
    '                    Continue For
    '                End Try
    'Dim fee_type As String = LCase(fee_dataset.Tables(0).Rows(fidx).Item(2))
    '                If InStr(fee_type, "levy") > 0 Then
    '                    levy_fees = fees
    ''see if levy fees already paid
    '                    If get_paid(debtorid, 1) > 0 Then
    '                        fidx += 1
    '                        Continue For
    '                    End If
    '                    If levy_fees > 33 Then
    '                        tot_levy_comm += 10
    '                        levy_comm = 10
    '                    Else
    '                        levy_comm = (levy_fees * 0.3)
    '                        tot_levy_comm = tot_levy_comm + levy_comm
    '                    End If
    '                ElseIf fee_dataset.Tables(0).Rows(fidx).Item(3) = 3 Then
    '                    other_fc_fees += fees
    '                    If InStr(fee_type, "visit") > 0 Then
    '                        If InStr(fee_type, "1") > 0 Then
    '                            If get_paid(debtorid, 2) = 0 Then
    '                                tot_first_visit_comm += 3
    '                                visit1_comm = 3
    '                                tot_unpaid_visit1 += 3
    '                                first_visit_no += 1
    '                            End If
    '                        ElseIf InStr(fee_type, "2") > 0 Then
    '                            If get_paid(debtorid, 3) = 0 Then
    '                                second_visit_no += 1
    '                                tot_second_visit_comm += 3.5
    '                                visit2_comm = 3.5
    '                                tot_unpaid_visit1 += 3.5
    '                            End If
    '                        End If
    '                    ElseIf InStr(fee_type, "walk") > 0 Then
    '                        If get_paid(debtorid, 5) = 0 Then
    '                            swp_no += 1
    '                            tot_swp_comm += 2
    '                            swp_comm = 2
    '                            tot_unpaid_swp += 2
    '                        End If
    '                    End If
    '                ElseIf fee_dataset.Tables(0).Rows(fidx).Item(3) = 4 Then
    'Dim case_van_fees As Decimal = fees
    '                    van_fees += case_van_fees
    '                    tot_van_fees += case_van_fees
    ''see if van fees already paid
    '                    If get_paid(debtorid, 13) = 0 Then
    '                        If get_paid(debtorid, 12) > 0 Then
    '                            el_van_fees += case_van_fees
    '                            case_van_status = "P"
    '                        Else
    '                            tot_unpaid_van += case_van_fees
    '                            el_van_fees += case_van_fees
    '                            case_van_status = "U"
    '                        End If
    '                    Else
    '                        If get_paid(debtorid, 12) > 0 Then
    '                            el_van_fees += case_van_fees
    '                            case_van_status = "P"
    '                        Else
    '                            tot_unpaid_van += case_van_fees / 2
    '                            el_van_fees += case_van_fees / 2
    '                            case_van_status = "H"
    '                        End If
    '                    End If
    ''If get_paid(debtorid, 12) = 0 Then
    ''    If get_paid(debtorid, 13) > 0 Then
    ''        tot_unpaid_van += case_van_fees / 2
    ''        el_van_fees += case_van_fees / 2
    ''        case_van_status = "H"
    ''    Else
    ''        tot_unpaid_van += case_van_fees
    ''        el_van_fees += case_van_fees
    ''        case_van_status = "U"
    ''    End If
    ''Else
    ''    el_van_fees += case_van_fees
    ''    case_van_status = "P"
    ''End If
    '                    End If
    '                    fidx += 1

    '            Next
    ''get amount already paid
    ''param1 = "Fees"
    ''param2 = "select sum(PaymentAmount) from BailiffPayments where CaseId = " & debtorid & _
    ''" and (PaymentTypeID = 12 or PaymentTypeId = 13)"
    ''Dim feepay2_dataset As DataSet = get_dataset(param1, param2)
    ''Try
    ''    already_paid += feepay2_dataset.Tables(0).Rows(0).Item(0)
    ''    tot_already_paid += feepay2_dataset.Tables(0).Rows(0).Item(0)
    ''Catch ex As Exception

    ''End Try
    'Dim debtor As String = debtorid

    ''store values in table for display later
    '            case_array(tot_pif).bail_id = bail_id
    '            case_array(tot_pif).debtor = debtor
    '            case_array(tot_pif).van_fees = van_fees
    '            case_array(tot_pif).el_van_fees = el_van_fees
    '            case_array(tot_pif).levy_fees = levy_fees
    '            case_array(tot_pif).levy_comm = levy_comm
    '            case_array(tot_pif).visit1_comm = visit1_comm
    '            case_array(tot_pif).visit2_comm = visit2_comm
    '            case_array(tot_pif).swp_comm = swp_comm
    '            case_array(tot_pif).date_allocated = date_allocated
    '            case_array(tot_pif).date_visited = date_visited
    '            If van_fees = 0 Then
    '                case_van_status = "Z"
    '            End If
    '            case_array(tot_pif).status = case_van_status
    '            pif += 1
    '            tot_pif += 1
    '        Next

End Module

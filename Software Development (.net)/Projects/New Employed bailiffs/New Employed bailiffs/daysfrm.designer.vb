<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class daysfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.Employed_bailiffsDataSet = New New_Employed_bailiffs.Employed_bailiffsDataSet
        Me.Employed_bailiffsTableAdapter = New New_Employed_bailiffs.Employed_bailiffsDataSetTableAdapters.Employed_bailiffsTableAdapter
        Me.bail_id = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bail_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bail_days = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Employed_bailiffsDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.bail_id, Me.bail_name, Me.bail_days})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(511, 423)
        Me.DataGridView1.TabIndex = 0
        '
        'Employed_bailiffsDataSet
        '
        Me.Employed_bailiffsDataSet.DataSetName = "Employed_bailiffsDataSet"
        Me.Employed_bailiffsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Employed_bailiffsTableAdapter
        '
        Me.Employed_bailiffsTableAdapter.ClearBeforeFill = True
        '
        'bail_id
        '
        Me.bail_id.HeaderText = "Bailiff ID"
        Me.bail_id.Name = "bail_id"
        Me.bail_id.ReadOnly = True
        Me.bail_id.Width = 50
        '
        'bail_name
        '
        Me.bail_name.HeaderText = "Name"
        Me.bail_name.Name = "bail_name"
        Me.bail_name.ReadOnly = True
        Me.bail_name.Width = 200
        '
        'bail_days
        '
        Me.bail_days.HeaderText = "Working days"
        Me.bail_days.Name = "bail_days"
        Me.bail_days.Width = 50
        '
        'daysfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(511, 423)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "daysfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "working days"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Employed_bailiffsDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Employed_bailiffsDataSet As New_Employed_bailiffs.Employed_bailiffsDataSet
    Friend WithEvents Employed_bailiffsTableAdapter As New_Employed_bailiffs.Employed_bailiffsDataSetTableAdapters.Employed_bailiffsTableAdapter
    Friend WithEvents bail_id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bail_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bail_days As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

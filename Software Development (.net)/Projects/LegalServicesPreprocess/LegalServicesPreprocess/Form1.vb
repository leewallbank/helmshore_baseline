Public Class Form1
    Dim new_file As String = ""
    'Dim outcome_file As String
    Dim input_file As String
    Dim write_audit As Boolean = False
    Dim audit_file, no_changes_file As String
    Dim record As String = ""
    Dim filename, filename_id, filename_id_tag As String
    Dim first_error As Boolean = True
    Dim first_change As Boolean = True
    Dim on_onestep, equity_check As Boolean
    Dim contrib_id As Double
    Dim no_changes As Integer = 0
    Dim changes As Integer = 0
    Dim applicant_id, summons_no, maat_id, first_name, surname, dob, ni_no, outcome, appeal_type, rep_status As String
    Dim mthly_contrib_amt_str, upfront_contrib_amt_str, income_contrib_cap_str, income_uplift_applied As String
    Dim case_type, in_court_custody, debt_addr1, debt_addr2, debt_addr3, debt_addr4, debt_postcode As String
    Dim postal_addr1, postal_addr2, postal_addr3, postal_addr4, postal_postcode, landline, mobile, email As String
    Dim equity_amt, appl_equity_amt, partner_equity_amt, cap_amt, asset_no As Integer
    Dim valid_recs As Integer = 0
    Dim rej_recs As Integer = 0
    Dim no_of_errors As Integer = 0
    Dim percent_owned_partner, percent_owned_appl As Integer
    Dim asset_table(100, 3)
    Dim rep_order_withdrawal_date As Date
    Dim future_effective_date As Boolean
    Dim mthly_contrib_amt, upfront_contrib_amt, income_contrib_cap, final_defence_cost As Decimal
    Dim equity_amt_verified, cap_asset_type, cap_amt_verified, comments, residential_code, residential_desc As String
    Dim allowable_cap_threshold, effective_date, pref_pay_method, pref_pay_date, prop_type_code, prop_type_desc As String
    Dim bank_acc_name, bank_acc_no, bank_sort_code, emp_status, rep_order_withdrawal_date_str, hardship_appl_recvd As String

    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click
        conn_open = False
        Try
            With OpenFileDialog1
                .Title = "Read XML file"
                .Filter = "XML file|*.xml"
                .FileName = ""
                .CheckFileExists = True
            End With
            Dim xml_dataSet As New DataSet()
            Dim doc As New Xml.XmlDocument()

            Dim contrib_file As String = "Applicant ID" & "|" & "SummonsNum" & "|" & "MAAT ID" & _
                        "|" & "First name" & "|" & "Surname" & "|" & "Date of birth" & "|" & "NI No" & _
                        "|" & "Monthly Amount" & "|" & "Upfront Amt" & "|" & "Income contribution cap" & "|" & _
                        "Debt addr1" & "|" & "Debt addr2" & "|" & "Debt addr3" & "|" & "Debt addr4" & _
                        "|" & "Debt postcode" & "|" & "Curr addr1" & "|" & "Curr addr2" & _
                        "|" & "Curr addr3" & "|" & "Curr addr4" & "|" & "Curr postcode" & "|" & _
                        "Landline" & "|" & "Mobile" & "|" & "Email" & "|" & _
                         "Effective date" & "|" & "Comments" & vbNewLine
            Dim appeal_file As String = contrib_file
            Dim equity_file As String = contrib_file
            no_changes_file = "Maat ID" & vbNewLine

            input_file = contrib_file

            Dim idx As Integer = 0
            If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                Try
                    filename = OpenFileDialog1.FileName
                    Dim ln2 As Integer = Microsoft.VisualBasic.Len(filename)

                    new_file = Microsoft.VisualBasic.Left(filename, ln2 - 4) & "_error.txt"
                    My.Computer.FileSystem.WriteAllText(new_file, "Error messages " & Now, False)

                    new_file = Microsoft.VisualBasic.Left(filename, ln2 - 4) & "_changes.txt"
                    Dim change_message As String = "Debtor" & vbTab & "Maat ID" & vbTab & "Field" & _
                    vbTab & "Onestep Value" & vbTab & "New value" & vbNewLine
                    My.Computer.FileSystem.WriteAllText(new_file, change_message, False)
                    write_eff_date("debtor|effective_date" & vbNewLine)
                    'new_file = Microsoft.VisualBasic.Left(filename, ln2 - 4) & "_outcome.txt"
                    'My.Computer.FileSystem.WriteAllText(new_file, change_message, False)
                    reset_fields()
                    Dim rdr_name As String = ""
                    Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
                    reader.Read()
                    ProgressBar1.Value = 5
                    Dim record_count As Integer = 0
                    While (reader.ReadState <> Xml.ReadState.EndOfFile)
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        Try
                            rdr_name = reader.Name
                        Catch
                            Continue While
                        End Try

                        'Note ReadElementContentAsString moves focus to next element so don't need read

                        ProgressBar1.Value = record_count
                        Select Case rdr_name
                            Case "FILENAME"
                                filename_id_tag = reader.Item(0)
                                filename_id = reader.ReadElementContentAsString
                            Case "CONTRIBUTIONS"
                                record_count += 1
                                If record_count > 100 Then
                                    record_count = 0
                                End If
                                If idx > 0 Then
                                    end_of_record()
                                    input_file = input_file & record & vbNewLine
                                    If validate_record() = False Then
                                        rej_recs += 1
                                        audit_file = audit_file & maat_id & "|rejected" & vbNewLine
                                    Else
                                        valid_recs += 1
                                        If on_onestep = False Then
                                            If outcome = "APPEAL" Or _
                                            UCase(Microsoft.VisualBasic.Left(case_type, 6)) = "APPEAL" Then
                                                audit_file = audit_file & maat_id & "|appeal" & vbNewLine
                                                appeal_file = appeal_file & record & vbNewLine
                                            ElseIf equity_check = True Then
                                                audit_file = audit_file & maat_id & "|equity" & vbNewLine
                                                equity_file = equity_file & record & vbNewLine
                                                'ElseIf income_uplift_applied = "Yes" Then
                                                '    audit_file = audit_file & maat_id & "|sanction" & vbNewLine
                                                '    sanction_file = sanction_file & record & vbNewLine
                                            Else
                                                audit_file = audit_file & maat_id & "|contrib" & vbNewLine
                                                contrib_file = contrib_file & record & vbNewLine
                                            End If
                                        End If
                                        'reset_fields()
                                    End If
                                    '020810 reset fields moved here to include rejected
                                    reset_fields()
                                End If
                                idx += 1
                                contrib_id = reader.Item(0)
                                reader.Read()
                            Case "APPLICANT_ID"
                                applicant_id = reader.ReadElementContentAsString
                            Case "MAAT_ID"
                                maat_id = reader.ReadElementContentAsString
                                asset_no = 0
                                If maat_id = 1584096 Then
                                    asset_no = 0
                                End If
                            Case "ARREST_SUMMONS_NUMBER"
                                summons_no = reader.ReadElementContentAsString
                            Case "IN_COURT_CUSTODY"
                                in_court_custody = reader.ReadElementContentAsString
                                If in_court_custody.Length > 0 Then
                                    comments = comments & "In Court Custody:" & in_court_custody & ";"
                                End If
                            Case "REP_ORDER_WITHDRAWAL_DATE"
                                rep_order_withdrawal_date_str = reader.ReadElementContentAsString
                                Try
                                    rep_order_withdrawal_date = rep_order_withdrawal_date_str
                                Catch ex As Exception
                                    rep_order_withdrawal_date_str = ""
                                End Try
                                If IsDate(rep_order_withdrawal_date) Then
                                    comments = comments & "Rep-Order Withdrawal Date:" & _
                                    Format(rep_order_withdrawal_date, "dd.MM.yyyy") & ";"
                                End If
                            Case "FIRST_NAME"
                                first_name = reader.ReadElementContentAsString
                            Case "LAST_NAME"
                                surname = reader.ReadElementContentAsString
                            Case "DOB"
                                dob = reader.ReadElementContentAsString
                            Case "NI_NUMBER"
                                ni_no = reader.ReadElementContentAsString
                            Case "MONTHLY_CONTRIBUTION"
                                mthly_contrib_amt_str = reader.ReadElementContentAsString
                                mthly_contrib_amt = mthly_contrib_amt_str      ' pounds not pence/ 100
                            Case "UPFRONT_CONTRIBUTION"
                                upfront_contrib_amt_str = reader.ReadElementContentAsString
                                upfront_contrib_amt = upfront_contrib_amt_str     'pounds not pence/ 100
                            Case "INCOME_CONTRIBUTION_CAP"
                                income_contrib_cap_str = reader.ReadElementContentAsString
                                income_contrib_cap = income_contrib_cap_str      'pounds not pence/ 100
                            Case "INCOME_UPLIFT_APPLIED"
                                income_uplift_applied = reader.ReadElementContentAsString
                                comments = comments & "Inc uplift applied:" & income_uplift_applied & ";"
                            Case "DESCRIPTION"
                                case_type = reader.ReadElementContentAsString
                            Case "HOME_ADDRESS"
                                reader.Read()
                                While reader.Name <> "HOME_ADDRESS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "LINE1"
                                            debt_addr1 = reader.ReadElementContentAsString
                                        Case "LINE2"
                                            debt_addr2 = reader.ReadElementContentAsString
                                        Case "LINE3"
                                            debt_addr3 = reader.ReadElementContentAsString
                                        Case "CITY"
                                            debt_addr4 = reader.ReadElementContentAsString
                                        Case "POSTCODE"
                                            debt_postcode = reader.ReadElementContentAsString
                                        Case "COUNTRY"
                                            Dim country As String = reader.ReadElementContentAsString
                                            If country <> "GB" Then
                                                debt_addr4 = debt_addr4 & " " & Trim(country)
                                            End If
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "POSTAL_ADDRESS"
                                reader.Read()
                                While reader.Name <> "POSTAL_ADDRESS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "LINE1"
                                            postal_addr1 = reader.ReadElementContentAsString
                                        Case "LINE2"
                                            postal_addr2 = reader.ReadElementContentAsString
                                        Case "LINE3"
                                            postal_addr3 = reader.ReadElementContentAsString
                                        Case "CITY"
                                            postal_addr4 = reader.ReadElementContentAsString
                                        Case "POSTCODE"
                                            postal_postcode = reader.ReadElementContentAsString
                                        Case "COUNTRY"
                                            Dim country As String = reader.ReadElementContentAsString
                                            If country <> "GB" Then
                                                postal_addr4 = postal_addr4 & " " & Trim(country)
                                            End If
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "CASE_TYPE"
                                Try
                                    case_type = reader.ReadElementContentAsString
                                Catch
                                End Try
                                If case_type <> "" Then
                                    comments = comments & "Case type:" & case_type & ";"
                                    Continue While
                                End If
                                reader.Read()
                                While reader.Name <> "CASE_TYPE"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "DESCRIPTION"
                                            case_type = Trim(reader.ReadElementContentAsString)
                                            comments = comments & "Case type:" & case_type & ";"
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "LANDLINE"
                                landline = reader.ReadElementContentAsString
                            Case "MOBILE"
                                mobile = reader.ReadElementContentAsString
                            Case "EMAIL"
                                email = reader.ReadElementContentAsString
                            Case "RESIDENTIAL_STATUS"
                                reader.Read()
                                While reader.Name <> "RESIDENTIAL_STATUS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "CODE"
                                            residential_code = reader.ReadElementContentAsString
                                        Case "DESCRIPTION"
                                            residential_desc = reader.ReadElementContentAsString
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "PROPERTY_TYPE"
                                reader.Read()
                                While reader.Name <> "PROPERTY_TYPE"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "CODE"
                                            prop_type_code = reader.ReadElementContentAsString
                                        Case "DESCRIPTION"
                                            prop_type_desc = reader.ReadElementContentAsString
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "EQUITY_AMOUNT"
                                equity_amt = reader.ReadElementContentAsString
                            Case "PERCENTAGE_OWNED_APPLICANT"
                                percent_owned_appl = reader.ReadElementContentAsString
                            Case "PERCENTAGE_OWNED_PARTNER"
                                percent_owned_partner = reader.ReadElementContentAsString
                            Case "APPLICANT_EQUITY_AMOUNT"
                                appl_equity_amt = reader.ReadElementContentAsString
                            Case "PARTNER_EQUITY_AMOUNT"
                                partner_equity_amt = reader.ReadElementContentAsString
                            Case "EQUITY_AMOUNT_VERIFIED"
                                If equity_amt_verified <> "No" Then
                                    equity_amt_verified = reader.ReadElementContentAsString
                                    remove_chrs(equity_amt_verified)
                                Else
                                    reader.Read()
                                End If
                            Case "REP_STATUS"
                                reader.Read()
                                While reader.Name <> "REP_STATUS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "CODE"
                                            rep_status = reader.ReadElementContentAsString
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "APPEAL_TYPE"
                                reader.Read()
                                While reader.Name <> "APPEAL_TYPE"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "CODE"
                                            appeal_type = reader.ReadElementContentAsString
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "CROWN_COURT_OUTCOME"
                                reader.Read()
                                While reader.Name <> "CROWN_COURT_OUTCOME"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "CODE"
                                            outcome = reader.ReadElementContentAsString
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "FINAL_DEFENCE_COST"
                                final_defence_cost = reader.ReadElementContentAsString
                            Case "CAPITAL"
                                reader.Read()
                                While reader.Name <> "CAPITAL"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "CAPITAL_ASSET_TYPE"
                                            reader.Read()
                                            While reader.Name <> "CAPITAL_ASSET_TYPE"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "DESCRIPTION"
                                                        asset_no += 1
                                                        Dim temp As String = reader.ReadElementContentAsString
                                                        remove_chrs(temp)
                                                        Try
                                                            asset_table(asset_no, 1) = temp
                                                        Catch ex As Exception
                                                            MsgBox("asset table dimension requires increasing")
                                                            End
                                                        End Try
                                                    Case Else
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case "CAPITAL_AMOUNT"
                                            asset_table(asset_no, 2) = reader.ReadElementContentAsString
                                        Case "CAPITAL_AMOUNT_VERIFIED"
                                            cap_amt_verified = reader.ReadElementContentAsString
                                            remove_chrs(cap_amt_verified)
                                            asset_table(asset_no, 3) = cap_amt_verified
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "ALLOWABLE_CAPITAL_THRESHOLD"
                                allowable_cap_threshold = reader.ReadElementContentAsString
                                remove_chrs(allowable_cap_threshold)
                            Case "EFFECTIVE_DATE"
                                effective_date = reader.ReadElementContentAsString
                                'if effective date in the future - add to notes 
                                If effective_date > Now Then
                                    future_effective_date = True
                                    comments = comments & "Effective Date:" & effective_date & ";"
                                End If
                            Case "PREFERRED_PAYMENT_METHOD"
                                reader.Read()
                                While reader.Name <> "PREFERRED_PAYMENT_METHOD"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "DESCRIPTION"
                                            pref_pay_method = reader.ReadElementContentAsString
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "PREFERRED_PAYMENT_DAY"
                                pref_pay_date = reader.ReadElementContentAsString
                            Case "BANK_ACCOUNT_NAME"
                                bank_acc_name = reader.ReadElementContentAsString
                                If bank_acc_name.Length > 0 Then
                                    comments = comments & "Bank Acc Name:" & bank_acc_name & ";"
                                End If
                            Case "BANK_ACCOUNT_NO"
                                bank_acc_no = reader.ReadElementContentAsString
                                If bank_acc_no.Length > 0 Then
                                    comments = comments & "Bank Acc No:" & bank_acc_no & ";"
                                End If
                            Case "SORT_CODE"
                                bank_sort_code = reader.ReadElementContentAsString
                                If bank_sort_code.Length > 0 Then
                                    comments = comments & "Bank Sort Code:" & bank_sort_code & ";"
                                End If
                            Case "EMPLOYMENT_STATUS"
                                Try
                                    emp_status = reader.ReadElementContentAsString
                                Catch
                                End Try
                                If emp_status <> "" Then
                                    comments = comments & "Emp status:" & emp_status & ";"
                                    Continue While
                                End If
                                While reader.Name <> "EMPLOYMENT_STATUS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "DESCRIPTION"
                                            emp_status = reader.ReadElementContentAsString
                                            comments = comments & "Emp status:" & emp_status & ";"
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case Else
                                If maat_id.Length > 1 And _
                                rdr_name <> "CAPITAL_LIST" And _
                                rdr_name <> "BANK_DETAILS" And _
                                 rdr_name <> "EQUITY_LIST" And _
                                rdr_name <> "EQUITY" Then
                                    MsgBox("what is this? " & rdr_name)
                                End If
                                reader.Read()
                        End Select
                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Else
                MsgBox("XML file not processed")
                Exit Sub
            End If
            'write file including last applicant
            end_of_record()

            input_file = input_file & record & vbNewLine
            If validate_record() = False Then
                rej_recs += 1
                audit_file = audit_file & maat_id & "|rejected" & vbNewLine
            Else
                valid_recs += 1
                If on_onestep = False Then
                    If outcome = "APPEAL" Or _
                    UCase(Microsoft.VisualBasic.Left(case_type, 6)) = "APPEAL" Then
                        audit_file = audit_file & maat_id & "|appeal" & vbNewLine
                        appeal_file = appeal_file & record & vbNewLine
                    ElseIf equity_check = True Then
                        audit_file = audit_file & maat_id & "|equity" & vbNewLine
                        equity_file = equity_file & record & vbNewLine
                        'ElseIf income_uplift_applied = "Yes" Then
                        '    audit_file = audit_file & maat_id & "|sanction" & vbNewLine
                        '    sanction_file = sanction_file & record & vbNewLine
                    Else
                        audit_file = audit_file & maat_id & "|contrib" & vbNewLine
                        contrib_file = contrib_file & record & vbNewLine
                    End If
                End If
            End If

            Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_appeal_preprocess.txt"
            My.Computer.FileSystem.WriteAllText(new_file, appeal_file, False)
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_equity_preprocess.txt"
            My.Computer.FileSystem.WriteAllText(new_file, equity_file, False)
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_input.txt"
            My.Computer.FileSystem.WriteAllText(new_file, input_file, False)
            If write_audit Then
                new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_audit.txt"
                My.Computer.FileSystem.WriteAllText(new_file, audit_file, False)
            End If
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_contrib_preprocess.txt"
            My.Computer.FileSystem.WriteAllText(new_file, contrib_file, False)
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_no_changes.txt"
            My.Computer.FileSystem.WriteAllText(new_file, no_changes_file, False)
            'new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_sanction_preprocess.txt"
            'My.Computer.FileSystem.WriteAllText(new_file, sanction_file, False)

            'write acknowledgement file in xml
            '17.01.2011
            '21.01.2010 changed back
            Dim ack_file As String = "CONTRIBUTIONS_FILE_ACK_" & Format(Now, "yyyyMMddHHmm") & ".xml"
            'Dim ack_file As String = Microsoft.VisualBasic.Left(filename, ln - 4) & "_CONTRIBUTIONS_FILE_ACK.xml"
            Dim writer As New Xml.XmlTextWriter(ack_file, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)
            writer.Formatting = Xml.Formatting.Indented
            'acknowledgement file includes all records not rejected
            Dim tot_valid_recs As Integer = valid_recs
            Write_ack(writer, filename_id, filename_id_tag, tot_valid_recs, rej_recs, no_of_errors)
            writer.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        ProgressBar1.Value = 100
        MsgBox("Cases with no changes = " & no_changes & vbNewLine & "Changes = " & changes)
        Me.Close()
    End Sub

    Private Function validate_record() As Boolean
        'If mthly_contrib_amt = 0 Then
        '    Return (True)
        'End If
        equity_check = False
        Dim orig_no_of_errors As Integer = no_of_errors
        If maat_id = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = ""
            error_table(no_of_errors).error_text = "invalid maat id"
        End If

        If outcome <> "" Then
            If outcome <> "CONVICTED" And outcome <> "PART CONVICTED" And outcome <> "APPEAL" And outcome <> "ABANDONED" _
                    And outcome <> "AQUITTED" And outcome <> "DISMISSED" And outcome <> "DISMISSED" _
                    And outcome <> "PART SUCCESS" And outcome <> "UNSUCCESSFUL" And outcome <> "SUCCESSFUL" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid crown court outcome"
            End If
        End If

        If appeal_type <> "" Then
            If appeal_type <> "ASE" And appeal_type <> "ACV" And appeal_type <> "ACS" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid appeal type"
            End If
        End If

        If rep_status <> "" Then
            If rep_status <> "CURR" And rep_status <> "ERR" And rep_status <> "SUSP" _
            And rep_status <> "FI" And rep_status <> "NOT SENT FOR TRIAL" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid rep type"
            End If
        End If

        'check effective date not in the future
        'If effective_date > Now Then
        '    no_of_errors += 1
        '    error_table(no_of_errors).contrib_id = contrib_id
        '    error_table(no_of_errors).applicant_id = applicant_id
        '    error_table(no_of_errors).maat_id = maat_id
        '    error_table(no_of_errors).error_text = "invalid effective date"
        'End If

        If on_onestep Then
            Return (True)
        End If
        If first_name = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid first name"
        End If

        If surname = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid surname"
        End If

        If mthly_contrib_amt_str = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid monthly contribution"
        ElseIf mthly_contrib_amt = 0 Then
            If outcome = "CONVICTED" Or outcome = "PART CONVICTED" Then
                equity_check = True
            ElseIf outcome <> "APPEAL" And UCase(Microsoft.VisualBasic.Left(case_type, 6)) <> "APPEAL" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid monthly contribution"
            End If
        End If

        'If summons_no = "" Then
        'no_of_errors += 1
        'error_table(no_of_errors).contrib_id = contrib_id
        'error_table(no_of_errors).applicant_id = applicant_id
        'error_table(no_of_errors).maat_id = maat_id
        'error_table(no_of_errors).error_text = "invalid summons number"
        'End If

        If case_type = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid case type"
        End If

        If in_court_custody <> "Yes" And in_court_custody <> "No" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid in court custody"
        End If

        If emp_status = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid employment status"
        End If

        If effective_date = "" And future_effective_date = False Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid effective date"
        End If

        If income_contrib_cap_str = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid income contribution cap"
        End If

        If income_uplift_applied <> "Yes" And income_uplift_applied <> "No" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid income uplift applied"
        End If

        'If residential_code = "" Then
        '    no_of_errors += 1
        '    error_table(no_of_errors).contrib_id = contrib_id
        '    error_table(no_of_errors).applicant_id = applicant_id
        '    error_table(no_of_errors).maat_id = maat_id
        '    error_table(no_of_errors).error_text = "invalid residential code"
        'End If

        'If residential_desc = "" Then
        '    no_of_errors += 1
        '    error_table(no_of_errors).contrib_id = contrib_id
        '    error_table(no_of_errors).applicant_id = applicant_id
        '    error_table(no_of_errors).maat_id = maat_id
        '    error_table(no_of_errors).error_text = "invalid residential description"
        'End If
        If equity_amt_verified <> "" Then
            If equity_amt_verified <> "Yes" And equity_amt_verified <> "No" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid equity amount verified"
            End If
        End If

        If cap_amt_verified <> "" Then
            If cap_amt_verified <> "Yes" And cap_amt_verified <> "No" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid capital amount verified"
            End If
        End If

        If orig_no_of_errors = no_of_errors Then
            audit_file = audit_file & maat_id & "|no errors" & vbNewLine
            Return (True)
        Else
            equity_check = False
            audit_file = audit_file & maat_id & "|errors" & vbNewLine
            If no_of_errors = UBound(error_table) Then
                ReDim Preserve error_table(no_of_errors + 10)
            End If
            Return (False)
        End If

    End Function

    Private Sub Write_ack(ByVal writer As Xml.XmlWriter, ByVal filename_id As String, _
    ByVal filename_id_tag As String, ByVal valid_recs As Integer, ByVal rej_recs As Integer, _
    ByVal no_of_errors As Integer)
        writer.WriteStartElement("CONTRIBUTIONS_FILE_ACK")
        writer.WriteStartElement("FILENAME")
        writer.WriteAttributeString("ID", filename_id_tag)
        writer.WriteString(filename_id)
        writer.WriteEndElement()
        writer.WriteElementString("DATE_LOADED", Format(Now, "dd-MM-yyyy"))
        writer.WriteElementString("NO_OF_RECORDS_LOADED", valid_recs)
        writer.WriteElementString("NO_OF_RECORDS_REJECTED", rej_recs)

        Dim idx As Integer
        Dim contrib_id As Double = 0
        Dim last_contrib_id As Double = 0
        Dim applicant_id As Double = 0
        Dim maat_id As Double = 0
        Dim error_text As String = ""
        For idx = 1 To no_of_errors
            If idx = 1 Then
                writer.WriteStartElement("REJECTED_RECORD_LIST")
            End If
            contrib_id = error_table(idx).contrib_id
            If contrib_id <> last_contrib_id Then
                last_contrib_id = contrib_id
                writer.WriteStartElement("CONTRIBUTIONS")
                writer.WriteAttributeString("ID", contrib_id)
                applicant_id = error_table(idx).applicant_id
                maat_id = error_table(idx).maat_id
                writer.WriteElementString("APPLICANT_ID", applicant_id)
                writer.WriteElementString("MAAT_ID", maat_id)
            End If

            error_text = error_table(idx).error_text
            writer.WriteElementString("ERROR_TEXT", error_text)

            If idx = no_of_errors Then
                writer.WriteEndElement()
                writer.WriteEndElement()
            ElseIf contrib_id <> error_table(idx + 1).contrib_id Then
                writer.WriteEndElement()
            End If
        Next
        writer.WriteEndElement()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub end_of_record()
        Try
            If applicant_id = "" Then
                write_error("No applicant found")
                Exit Sub
            End If
            If applicant_id = "" Then
                write_error("No maat found")
                Exit Sub
            End If

            If equity_amt <> -9999 Then
                comments = comments & "Equity amt:" & equity_amt & ";"
            End If
            If appl_equity_amt <> -9999 Then
                comments = comments & "Applicant Equity Amount:" & appl_equity_amt & ";"
            End If
            If partner_equity_amt <> -9999 Then
                comments = comments & "Partner Equity Amount:" & partner_equity_amt & ";"
            End If
            comments = comments & "Equity Amt Verified:" & equity_amt_verified & ";"

            If allowable_cap_threshold <> "" Then
                comments = comments & "Allowable cap threshold:" & allowable_cap_threshold & ";"
            End If

            If outcome <> "" Then
                comments = comments & "Outcome:" & outcome & ";"
            End If

            If residential_code <> "" Then
                comments = comments & "Residential Code:" & Trim(residential_code) & ";"
            End If
            If residential_desc <> "" Then
                comments = comments & "Residential Description:" & Trim(residential_desc) & ";"
            End If

            If prop_type_code <> "" Then
                comments = comments & "Property Type Code:" & Trim(prop_type_code) & ";"
            End If
            If prop_type_desc <> "" Then
                comments = comments & "Property Type Description:" & Trim(prop_type_desc) & ";"
            End If
            If percent_owned_appl <> Nothing Then
                comments = comments & "Percent Owned Applicant:" & percent_owned_appl & ";"
            End If
            If percent_owned_partner <> Nothing Then
                comments = comments & "Percent Owned Partner:" & percent_owned_partner & ";"
            End If
            Dim idx3 As Integer
            Try
                For idx3 = 1 To asset_no
                    comments = comments & "Cap asset type:" & asset_table(idx3, 1) & ";"
                    comments = comments & "Cap amt:" & asset_table(idx3, 2) & ";"
                    comments = comments & "Cap amt verified:" & asset_table(idx3, 3) & ";"
                Next
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            asset_no = 0

            If landline = "" Then
                landline = mobile
                mobile = ""
            End If
            'use final costs if appeal type and mthly amt=0
            If outcome = "APPEAL" Or UCase(Microsoft.VisualBasic.Left(case_type, 6)) = "APPEAL" _
            And mthly_contrib_amt = 0 And upfront_contrib_amt = 0 Then
                upfront_contrib_amt = final_defence_cost
                comments = comments & "Final Defence Costs:" & Format(final_defence_cost, "F") & ";"
            End If
            record = applicant_id & "|" & summons_no & "|" & maat_id & "|" & first_name & "|" & _
                                        surname & "|" & dob & "|" & ni_no & "|" & mthly_contrib_amt & "|" & upfront_contrib_amt & "|" & _
                                        income_contrib_cap & "|" & debt_addr1 & "|" & debt_addr2 & "|" & debt_addr3 & "|" & _
                                        debt_addr4 & "|" & debt_postcode & "|" & postal_addr1 & "|" & postal_addr2 & "|" & _
                                        postal_addr3 & "|" & postal_addr4 & "|" & postal_postcode & "|" & landline & "|" & mobile & "|" & email & _
                                        "|"
            If future_effective_date Then
                record = record & ""
            Else
                record = record & effective_date
            End If
            'check if maat-id already exists on onestep
            param1 = "onestep"
            param2 = "select  clientschemeID, _rowid from Debtor" & _
            " where  client_ref = '" & maat_id & "'"
            'instead of hard-coding csid, use csid to get client-id, if =909 then it is LCS
            '"' and  (clientschemeID = 1892 or clientschemeID = 2109 or clientschemeID = 2110)"
            Dim debtor1_dataset As DataSet = get_dataset(param1, param2)
            Dim debtor As Integer = 0
            If no_of_rows > 0 Then
                Dim idx As Integer
                Dim csid_no As Integer = no_of_rows - 1
                For idx = 0 To csid_no
                    Dim csid As Integer = debtor1_dataset.Tables(0).Rows(idx).Item(0)
                    param2 = "select clientID from ClientScheme where _rowid = " & csid
                    Dim csid_datatset As DataSet = get_dataset(param1, param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read clientscheme for csid = " & csid)
                        Exit Sub
                    End If

                    If csid_datatset.Tables(0).Rows(0).Item(0) <> 909 Then
                        Continue For
                    Else
                        debtor = debtor1_dataset.Tables(0).Rows(idx).Item(1)
                        Exit For
                    End If
                Next
            End If

            If no_of_rows = 0 Or debtor = 0 Then
                on_onestep = False
                audit_file = audit_file & maat_id & "|not on onestep" & vbNewLine
                Dim comments2 As String = ""
                Dim comments3 As String = comments
                If comments.Length <= 250 Then
                    record = record & "|" & comments
                Else
                    While comments3.Length > 250
                        Dim len As Integer = Microsoft.VisualBasic.Len(comments3)
                        Dim idx As Integer
                        For idx = 250 To 1 Step -1
                            If Mid(comments3, idx, 1) = ";" Then
                                Exit For
                            End If
                        Next
                        comments2 = comments2 & Microsoft.VisualBasic.Left(comments3, idx)
                        Dim idx2 As Integer
                        For idx2 = idx To 250
                            comments2 = comments2 & " "
                        Next
                        comments3 = Microsoft.VisualBasic.Right(comments3, len - idx)
                    End While
                    record = record & "|" & comments2 & comments3
                End If
                Exit Sub
            End If
            record = record & "|" & comments

            'case exists on onestep so look for changes

            audit_file = audit_file & maat_id & "|on onestep" & vbNewLine
            Dim change_found As Boolean = False
            on_onestep = True
            param2 = "select _rowid, offence_number, offence_court, debt_amount, debt_costs, " & _
           " offenceValue, empNI, add_phone, add_fax, addEmail, prevReference, add_postcode, " & _
           " address, clientschemeID from Debtor" & _
           " where _rowid = " & debtor
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to read debtorID = " & debtor)
                Exit Sub
            End If
            'see if prevref has changed
            Dim orig_prevref As String
            Try
                orig_prevref = debtor_dataset.Tables(0).Rows(0).Item(10)
            Catch
                orig_prevref = ""
            End Try
            If applicant_id <> orig_prevref Then
                write_change(debtor & "|" & maat_id & "|" & "Prev Reference(Applicant)" & "|" & _
                                orig_prevref & "|" & applicant_id & vbNewLine)
                change_found = True
            End If

            'see if postcode has changed
            Dim orig_postcode As String
            Try
                orig_postcode = Trim(debtor_dataset.Tables(0).Rows(0).Item(11))
            Catch
                orig_postcode = ""
            End Try
            If LCase(debt_postcode) <> LCase(orig_postcode) Then
                Dim orig_home_addr = debtor_dataset.Tables(0).Rows(0).Item(12)
                Dim idx As Integer
                Dim addr As String = ""
                For idx = 1 To Microsoft.VisualBasic.Len(Trim(orig_home_addr))
                    If Mid(orig_home_addr, idx, 1) <> Chr(10) And _
                    Mid(orig_home_addr, idx, 1) <> Chr(13) Then
                        addr = addr & Mid(orig_home_addr, idx, 1)
                    Else
                        addr = addr & " "
                    End If
                Next
                write_change(debtor & "|" & maat_id & "|" & "Current Address" & "|" & _
                                addr & "|" & debt_addr1 & " " & debt_addr2 & " " & debt_addr3 & " " & _
                                debt_addr4 & " " & debt_postcode & vbNewLine)
                change_found = True
            End If

            'see if summons number has changed
            Dim orig_summons_no As String
            Try
                orig_summons_no = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
            Catch
                orig_summons_no = ""
            End Try
            If LCase(summons_no) <> LCase(orig_summons_no) Then
                write_change(debtor & "|" & maat_id & "|" & "Summons number" & "|" & _
                orig_summons_no & "|" & summons_no & vbNewLine)
                change_found = True
            End If

            'see if effective date has changed
            Dim orig_effective_date As Date
            Dim orig_effective_date_found As Boolean = True
            Try
                orig_effective_date = debtor_dataset.Tables(0).Rows(0).Item(2)
            Catch ex As Exception
                orig_effective_date_found = False
            End Try
            If orig_effective_date_found Then
                If effective_date <> orig_effective_date Then
                    Dim os_date As Date = orig_effective_date
                    write_change(debtor & "|" & maat_id & "|" & "Effective date(LO date)" & _
                    "|" & Format(os_date, "dd-MM-yyyy") & "|" & effective_date & vbNewLine)
                    change_found = True
                End If
            Else
                If future_effective_date Then
                    Dim eff_text As String = ""
                    param2 = "select text from Note where debtorID = " & debtor & _
                        " and type = 'Client note' order by _rowid desc"
                    Dim note_dataset As DataSet = get_dataset(param1, param2)
                    For idx3 = 0 To no_of_rows - 1
                        eff_text = note_dataset.Tables(0).Rows(idx3).Item(0)
                        Dim start_idx As Integer = InStr(eff_text, "Effective Date:")
                        If start_idx > 0 Then
                            eff_text = Mid(eff_text, start_idx + 15, 10)
                            Exit For
                        End If
                    Next
                    If eff_text.Length > 0 Then
                        If effective_date <> CDate(eff_text) Then
                            write_change(debtor & "|" & maat_id & "|" & "Effective date(LO date)" & _
                                                   "|" & CDate(eff_text) & "|" & effective_date & vbNewLine)
                            write_eff_date(debtor & "|" & effective_date & vbNewLine)
                            change_found = True
                        End If
                    End If
                Else
                    If effective_date <> "" Then
                        write_change(debtor & "|" & maat_id & "|" & "Effective date(LO date)" & _
                                            "|" & " " & "|" & effective_date & vbNewLine)
                        change_found = True
                    End If
                End If
            End If
            'see if debt amt has changed
            Dim test_amt As Decimal
            If upfront_contrib_amt_str = "" Then
                test_amt = 0
            Else
                test_amt = upfront_contrib_amt_str   ' pounds not pence/ 100
            End If
            Dim debt_amt As Decimal = debtor_dataset.Tables(0).Rows(0).Item(3)
            If test_amt <> debt_amt Then
                'no change if final costs = costs on onestep
                If debt_amt <> final_defence_cost Then
                    write_change(debtor & "|" & maat_id & "|" & "Debt amount" & "|" & _
                                    Format(debt_amt, "#.##") & "|" & Format(test_amt, "#.##") & vbNewLine)
                    change_found = True
                End If
            End If

            'see if debt costs has changed
            If mthly_contrib_amt_str = "" Then
                test_amt = 0
            Else
                test_amt = mthly_contrib_amt_str    'pounds not pence / 100
            End If
            Dim debt_costs As Decimal = debtor_dataset.Tables(0).Rows(0).Item(4)
            If test_amt <> debt_costs Then
                write_change(debtor & "|" & maat_id & "|" & "Debt Costs" & "|" & _
                                Format(debt_costs, "#.##") & "|" & test_amt & vbNewLine)
                change_found = True
            End If

            'see if offence value has changed
            If income_contrib_cap_str = "" Then
                test_amt = 0
            Else
                test_amt = income_contrib_cap_str     'pounds not pence / 100
            End If
            Dim offence_value As Decimal = debtor_dataset.Tables(0).Rows(0).Item(5)
            If test_amt <> offence_value Then
                write_change(debtor & "|" & maat_id & "|" & "Offence value" & "|" & _
                Format(offence_value, "#.##") & "|" & Format(test_amt, "#.##") & vbNewLine)
                change_found = True
            End If

            'see if NIno has changed
            Dim orig_ni_no As String
            Try
                orig_ni_no = debtor_dataset.Tables(0).Rows(0).Item(6)
            Catch
                orig_ni_no = ""
            End Try
            If LCase(ni_no) <> LCase(orig_ni_no) Then
                write_change(debtor & "|" & maat_id & "|" & "NI No" & "|" & orig_ni_no & "|" & ni_no & vbNewLine)
                change_found = True
            End If

            'check if phone number has changed
            Dim orig_phone_no As String
            Try
                orig_phone_no = Trim(debtor_dataset.Tables(0).Rows(0).Item(7))
            Catch
                orig_phone_no = ""
            End Try

            Dim orig_fax_no As String
            Try
                orig_fax_no = Trim(debtor_dataset.Tables(0).Rows(0).Item(8))
            Catch
                orig_fax_no = ""
            End Try
            If landline <> "" Then
                If landline <> orig_phone_no And _
                landline <> orig_fax_no Then
                    write_change(debtor & "|" & maat_id & "|" & "Phone No" & "|" & orig_phone_no & "|" & landline & vbNewLine)
                    change_found = True
                End If
            End If
            If mobile <> "" Then
                If mobile <> orig_phone_no And _
                mobile <> orig_fax_no Then
                    write_change(debtor & "|" & maat_id & "|" & "Mobile No" & "|" & orig_fax_no & "|" & mobile & vbNewLine)
                    change_found = True
                End If
            End If

            'check if email has changed
            Dim orig_email As String
            Try
                orig_email = debtor_dataset.Tables(0).Rows(0).Item(9)
            Catch
                orig_email = ""
            End Try
            If email <> orig_email Then
                write_change(debtor & "|" & maat_id & "|" & "Email" & "|" & orig_email & "|" & email & vbNewLine)
                change_found = True
            End If
            Dim idx4 As Integer
            Dim note_text As String = ""

            'see if outcome has changed
            'removed on 11.01.2011 due to acquitted appearing in text from debtor
            'put back 23.03.2011 now looking for specific client note
            Dim note_outcome As String = ""
            If outcome <> "" Then
                param2 = "select text from Note where debtorID = " & debtor & _
                " and type = 'Client note' order by _rowid desc"
                Dim note_dataset As DataSet = get_dataset(param1, param2)
                For idx3 = 0 To no_of_rows - 1
                    note_text = note_dataset.Tables(0).Rows(idx3).Item(0)
                    If InStr(note_text, "OUTCOME:") > 0 Then
                        For idx4 = 9 To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_outcome = Mid(note_text, 9, idx4 - 9)
                                Exit For
                            End If
                        Next
                        Exit For
                    End If
                Next
            End If
            If outcome <> "" Then
                If outcome <> note_outcome Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Outcome" & "|" & "" & "|" & outcome & vbNewLine)
                    write_note(debtor & "|" & "OUTCOME:" & outcome)
                End If
            End If

            'if case-type is appeal and clientscheme <> 2109(appeal scheme) then write to outcome file
            If UCase(Microsoft.VisualBasic.Left(case_type, 6)) = "APPEAL" And _
            debtor_dataset.Tables(0).Rows(0).Item(13) <> 2109 Then
                change_found = True
                write_outcome(debtor & "|" & maat_id & "|" & "Case type Appeal" & "|" & " " & "|" & case_type & vbNewLine)
            End If

            'see if bank account no has changed
            Dim bank_note As String = ""
            If bank_acc_no.Length > 0 Then
                Dim note_acc_no = ""
                param2 = "select text from Note where debtorID = " & debtor & _
                          "  and (type = 'Note' or type = 'Client note') order by _rowid desc"
                Dim note_dataset As DataSet = get_dataset(param1, param2)
                For idx3 = 0 To no_of_rows - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "bank acc no:")
                    If start_idx > 0 Then
                        start_idx += 12
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_acc_no = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_acc_no = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If
                    End If
                    If note_acc_no <> "" Then
                        Exit For
                    End If
                Next
                If bank_acc_no <> note_acc_no Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Bank Acc No" & "|" & note_acc_no & "|" & bank_acc_no & vbNewLine)
                    bank_note = debtor & "|" & "BANK DETAILS;Bank Acc No:" & bank_acc_no & ";"
                End If
            End If

            'see if bank sort code has changed
            If bank_sort_code.Length > 0 Then
                Dim note_sort_code = ""
                param2 = "select text from Note where debtorID = " & debtor & _
                         "  and (type = 'Note' or type = 'Client note') order by _rowid desc"
                Dim note_dataset As DataSet = get_dataset(param1, param2)
                For idx3 = 0 To no_of_rows - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "bank sortcode:")
                    If start_idx = 0 Then
                        start_idx = InStr(note_text, "bank sort code:") + 1
                    End If
                    If start_idx > 1 Then
                        start_idx += 14
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_sort_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_sort_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If
                    End If
                    If note_sort_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(bank_sort_code) <> LCase(note_sort_code) Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Bank Sortcode" & "|" & note_sort_code & "|" & bank_sort_code & vbNewLine)
                    If bank_note = "" Then
                        bank_note = debtor & "|" & "BANK DETAILS;Bank Acc No:" & bank_acc_no & ";"
                    End If
                    bank_note = bank_note & "Bank Sortcode:" & bank_sort_code & ";"
                ElseIf bank_note <> "" Then
                    bank_note = bank_note & "Bank Sortcode:" & bank_sort_code & ";"
                End If
            ElseIf bank_note <> "" Then
                bank_note = bank_note & "Bank Sortcode:" & bank_sort_code & ";"
            End If

            'see if bank acc name has changed
            If bank_acc_name.Length > 0 Then
                Dim note_acc_name As String = ""
                param2 = "select text from Note where debtorID = " & debtor & _
                                " order by _rowid desc"
                Dim note_dataset As DataSet = get_dataset(param1, param2)
                For idx3 = 0 To no_of_rows - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "bank acc name:")
                    If start_idx > 0 Then
                        start_idx += 14
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_acc_name = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_acc_name = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If
                    End If
                    If note_acc_name <> "" Then
                        Exit For
                    End If
                Next
                If LCase(bank_acc_name) <> LCase(note_acc_name) Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Bank Acc Name" & "|" & note_acc_name & "|" & bank_acc_name & vbNewLine)
                    If bank_note = "" Then
                        If bank_note = "" Then
                            bank_note = debtor & "|" & "BANK DETAILS;Bank Acc No:" & _
                            bank_acc_no & ";Bank Sortcode:" & bank_sort_code & ";"
                        End If
                    End If
                    bank_note = bank_note & "Bank Acc Name:" & bank_acc_name
                    write_note(bank_note)
                ElseIf bank_note <> "" Then
                    bank_note = bank_note & "Bank Acc Name:" & bank_acc_name
                    write_note(bank_note)
                End If
            ElseIf bank_note <> "" Then
                bank_note = bank_note & "Bank Acc Name:" & bank_acc_name
                write_note(bank_note)
            End If

            'see if Rep-Order Withdrawal Date is set
            If rep_order_withdrawal_date <> Nothing Then
                change_found = True
                write_outcome(debtor & "|" & maat_id & "|" & "Rep-order Withdrawal Date" & "||" & _
                Format(rep_order_withdrawal_date, "dd.MMM.yyyy") & "|" & vbNewLine)
                write_note(debtor & "|" & "REP-ORDER-WITHDRAWN-DATE:" & Format(rep_order_withdrawal_date, "dd.MMM.yyyy"))
            End If

            'see if final costs entered
            If final_defence_cost <> Nothing Then
                If debt_amt <> final_defence_cost And _
                    debt_costs <> final_defence_cost Then
                    write_change(debtor & "|" & maat_id & "|" & "Final Costs" & "|" & " " & "|" & _
                    Format(final_defence_cost, "F") & vbNewLine)
                    write_note(debtor & "|" & "FINAL-DEFENCE-COSTS:" & Format(final_defence_cost, "F"))
                    change_found = True
                End If
            End If
            'see if inc-uplift-applied has changed
            Dim note_inc_uplift_applied As String = ""
            param2 = "select text from Note where debtorID = " & debtor & _
                     " and (type = 'Client note' or type = 'Note') order by _rowid desc"
            Dim note2_dataset As DataSet = get_dataset(param1, param2)
            For idx3 = 0 To no_of_rows - 1
                note_text = LCase(note2_dataset.Tables(0).Rows(idx3).Item(0))
                Dim start_idx As Integer = InStr(note_text, "income-uplift-applied:")
                If start_idx > 0 Then
                    start_idx += 22
                Else
                    start_idx = InStr(LCase(note_text), "inc uplift applied:")
                    If start_idx > 0 Then
                        start_idx += 19
                    End If
                End If
                If start_idx > 0 Then
                    note_inc_uplift_applied = UCase(Mid(note_text, start_idx, 1))
                    If note_inc_uplift_applied = "Y" Then
                        note_inc_uplift_applied = "Yes"
                    Else
                        note_inc_uplift_applied = "No"
                    End If
                End If
                If note_inc_uplift_applied <> "" Then
                    Exit For
                End If
            Next
            If note_inc_uplift_applied = "" Then
                note_inc_uplift_applied = "No"
            End If
            If income_uplift_applied <> note_inc_uplift_applied Then
                write_change(debtor & "|" & maat_id & "|" & "Income Sanction" & "|" & _
                income_uplift_applied & "|" & note_inc_uplift_applied & vbNewLine)
                write_note(debtor & "|" & "INCOME-UPLIFT-APPLIED:" & income_uplift_applied)
                change_found = True
            End If

            'see if equity amount verified has changed
            Dim note_equity_amt_verified As String = ""
            param2 = "select text from Note where debtorID = " & debtor & _
                     " and (type = 'Client note' or type = 'Note') order by _rowid desc"
            Dim note3_dataset As DataSet = get_dataset(param1, param2)
            For idx3 = 0 To no_of_rows - 1
                note_text = LCase(note3_dataset.Tables(0).Rows(idx3).Item(0))
                Dim start_idx As Integer = InStr(note_text, "equity amt verified:")
                If start_idx > 0 Then
                    start_idx += 20
                End If
                If start_idx > 0 Then
                    note_equity_amt_verified = UCase(Mid(note_text, start_idx, 1))
                    If note_equity_amt_verified = "Y" Then
                        note_equity_amt_verified = "Yes"
                    Else
                        note_equity_amt_verified = "No"
                    End If
                End If
                If note_equity_amt_verified <> "" Then
                    Exit For
                End If
            Next
            If note_equity_amt_verified = "" Then
                note_equity_amt_verified = "No"
            End If
            If equity_amt_verified = "" Then
                equity_amt_verified = "No"
            End If
            If equity_amt_verified <> note_equity_amt_verified Then
                write_change(debtor & "|" & maat_id & "|" & "Equity Amt Verified" & "|" & _
                equity_amt_verified & "|" & note_equity_amt_verified & vbNewLine)
                write_note(debtor & "|" & "Equity Amt Verified:" & equity_amt_verified)
                change_found = True
            End If

            'see if equity amount has changed
            Dim note_equity_amt As Decimal = -9999
            param2 = "select text from Note where debtorID = " & debtor & _
                     " and (type = 'Client note' or type = 'Note') order by _rowid desc"
            Dim note4_dataset As DataSet = get_dataset(param1, param2)
            For idx3 = 0 To no_of_rows - 1
                note_text = note4_dataset.Tables(0).Rows(idx3).Item(0)
                Dim start_idx As Integer = InStr(note_text, "Equity Amt:")
                If start_idx > 0 Then
                    start_idx += 11
                End If
                If start_idx > 0 Then
                    For idx4 = start_idx To note_text.Length
                        If Mid(note_text, idx4, 1) = ";" Then
                            note_equity_amt = Mid(note_text, start_idx, idx4 - start_idx)
                            Exit For
                        End If
                    Next
                    If idx4 > note_text.Length Then
                        note_equity_amt = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                    End If
                End If
                If note_equity_amt <> -9999 Then
                    Exit For
                End If
            Next
            If equity_amt <> note_equity_amt Then
                If equity_amt <> -9999 Or note_equity_amt <> -9999 Then
                    write_change(debtor & "|" & maat_id & "|" & "Equity Amt" & "|" & _
                                    equity_amt & "|" & note_equity_amt & vbNewLine)
                    write_note(debtor & "|" & "Equity Amt:" & equity_amt)
                    change_found = True
                End If
            End If

            'see if applicant equity amount has changed
            Dim note_appl_equity_amt As Decimal = -9999
            param2 = "select text from Note where debtorID = " & debtor & _
                     " and (type = 'Client note' or type = 'Note') order by _rowid desc"
            Dim note5_dataset As DataSet = get_dataset(param1, param2)
            For idx3 = 0 To no_of_rows - 1
                note_text = note5_dataset.Tables(0).Rows(idx3).Item(0)
                Dim start_idx As Integer = InStr(note_text, "Applicant Equity Amount:")
                If start_idx > 0 Then
                    start_idx += 24
                End If
                If start_idx > 0 Then
                    For idx4 = start_idx To note_text.Length
                        If Mid(note_text, idx4, 1) = ";" Then
                            note_appl_equity_amt = Mid(note_text, start_idx, idx4 - start_idx)
                            Exit For
                        End If
                    Next
                    If idx4 > note_text.Length Then
                        note_appl_equity_amt = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                    End If
                End If
                If note_appl_equity_amt <> -9999 Then
                    Exit For
                End If
            Next
            If appl_equity_amt <> note_appl_equity_amt Then
                If appl_equity_amt <> -9999 Or note_appl_equity_amt <> -9999 Then
                    write_change(debtor & "|" & maat_id & "|" & "Applicant Equity Amount" & "|" & _
                                    appl_equity_amt & "|" & note_appl_equity_amt & vbNewLine)
                    write_note(debtor & "|" & "Applicant Equity Amount:" & appl_equity_amt)
                    change_found = True
                End If
            End If

            'see if partner equity amount has changed
            Dim note_part_equity_amt As Decimal = -9999
            param2 = "select text from Note where debtorID = " & debtor & _
                     " and (type = 'Client note' or type = 'Note') order by _rowid desc"
            Dim note6_dataset As DataSet = get_dataset(param1, param2)
            For idx3 = 0 To no_of_rows - 1
                note_text = note6_dataset.Tables(0).Rows(idx3).Item(0)
                Dim start_idx As Integer = InStr(note_text, "Partner Equity Amount:")
                If start_idx > 0 Then
                    start_idx += 22
                End If
                If start_idx > 0 Then
                    For idx4 = start_idx To note_text.Length
                        If Mid(note_text, idx4, 1) = ";" Then
                            note_part_equity_amt = Mid(note_text, start_idx, idx4 - start_idx)
                            Exit For
                        End If
                    Next
                    If idx4 > note_text.Length Then
                        note_part_equity_amt = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                    End If
                End If
                If note_part_equity_amt <> -9999 Then
                    Exit For
                End If
            Next
            If partner_equity_amt <> note_part_equity_amt Then
                If partner_equity_amt <> -9999 Or note_part_equity_amt <> -9999 Then
                    write_change(debtor & "|" & maat_id & "|" & "Partner Equity Amount" & "|" & _
                                    partner_equity_amt & "|" & note_part_equity_amt & vbNewLine)
                    write_note(debtor & "|" & "Partner Equity Amount:" & partner_equity_amt)
                    change_found = True
                End If
            End If

            If Not change_found Then
                audit_file = audit_file & maat_id & "|no changes found" & vbNewLine
                no_changes_file = no_changes_file & maat_id & vbNewLine
                no_changes += 1
            Else
                audit_file = audit_file & maat_id & "|changes found" & vbNewLine
                changes += 1
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub write_error(ByVal error_message As String)
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_error.txt"
        error_message = error_message & vbNewLine
        My.Computer.FileSystem.WriteAllText(new_file, error_message, True)
    End Sub
    Private Sub write_eff_date(ByVal change_message As String)
        Static Dim date_cnt As Integer
        date_cnt += 1
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_eff_date.txt"
        If date_cnt = 1 Then
            My.Computer.FileSystem.WriteAllText(new_file, change_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
        End If
    End Sub
    Private Sub write_change(ByVal change_message As String)
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_changes.txt"
        My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
    End Sub
    Private Sub write_outcome(ByVal change_message As String)
        'Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        'new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_outcome.txt"
        'My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
    End Sub
    Private Sub write_note(ByVal change_message As String)
        Static Dim note_count As Integer
        change_message = change_message & ";from feed on " & Format(Now, "dd.MM.yyyy") & vbNewLine
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_note.txt"
        note_count += 1
        If note_count = 1 Then
            My.Computer.FileSystem.WriteAllText(new_file, change_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
        End If
    End Sub
    Private Sub remove_chrs(ByRef in_field As String)
        Dim idx2 As Integer
        Dim out_field As String = ""
        For idx2 = 1 To Microsoft.VisualBasic.Len(in_field)
            If Mid(in_field, idx2, 1) <> Chr(9) And _
              Mid(in_field, idx2, 1) <> Chr(10) And _
            Mid(in_field, idx2, 1) <> Chr(13) Then
                out_field = out_field & Mid(in_field, idx2, 1)
            End If
        Next
        in_field = out_field
    End Sub

    Private Sub reset_fields()
        applicant_id = ""
        summons_no = ""
        maat_id = ""
        first_name = ""
        surname = ""
        dob = ""
        ni_no = ""
        mthly_contrib_amt_str = ""
        mthly_contrib_amt = Nothing
        upfront_contrib_amt_str = ""
        upfront_contrib_amt = Nothing
        final_defence_cost = Nothing
        income_contrib_cap_str = ""
        income_uplift_applied = ""
        case_type = ""
        in_court_custody = ""
        debt_addr1 = ""
        debt_addr2 = ""
        debt_addr3 = ""
        debt_addr4 = ""
        debt_postcode = ""
        postal_addr1 = ""
        postal_addr2 = ""
        postal_addr3 = ""
        postal_addr4 = ""
        postal_postcode = ""
        landline = ""
        mobile = ""
        email = ""
        equity_amt = -9999
        partner_equity_amt = -9999
        appl_equity_amt = -9999
        equity_amt_verified = ""
        cap_asset_type = ""
        cap_amt = 0
        cap_amt_verified = ""
        allowable_cap_threshold = ""
        effective_date = ""
        future_effective_date = False
        pref_pay_method = ""
        pref_pay_date = ""
        bank_acc_name = ""
        bank_acc_no = ""
        bank_sort_code = ""
        emp_status = ""
        rep_order_withdrawal_date_str = ""
        rep_order_withdrawal_date = Nothing
        hardship_appl_recvd = ""
        comments = ""
        outcome = ""
        rep_status = ""
        appeal_type = ""
        residential_code = ""
        residential_desc = ""
        prop_type_code = ""
        prop_type_desc = ""
        percent_owned_appl = Nothing
        percent_owned_partner = Nothing
    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '11.01.2011 check for outcome on notes removed
        '17.01.2011 ack file name changed
        '21.01.2011 ack file name changed back
        '10.02.2011 income uplift applied - no to go to separate income sanction scheme
        '22.03.2011 new fields added and new file created to load notes into onestep
        ' also clientID=909 used instead of list of csids.
        ' also income sanction cases now going to ccmt
    End Sub
End Class

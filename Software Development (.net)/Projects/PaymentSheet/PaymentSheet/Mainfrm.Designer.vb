<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.exitbtn = New System.Windows.Forms.Button
        Me.pay_dg = New System.Windows.Forms.DataGridView
        Me.pay_id = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pay_agent_from = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pay_agent2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pay_amount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.pay_debtorID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.addbtn = New System.Windows.Forms.Button
        Me.tr_lbl = New System.Windows.Forms.Label
        Me.log_lbl = New System.Windows.Forms.Label
        Me.disp_lbl = New System.Windows.Forms.Label
        Me.excelbtn = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.todaybtn = New System.Windows.Forms.Button
        Me.FeesSQLDataSet = New PaymentSheet.FeesSQLDataSet
        Me.PaymentSheetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PaymentSheetTableAdapter = New PaymentSheet.FeesSQLDataSetTableAdapters.PaymentSheetTableAdapter
        CType(Me.pay_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PaymentSheetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(719, 378)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 4
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'pay_dg
        '
        Me.pay_dg.AllowUserToAddRows = False
        Me.pay_dg.AllowUserToDeleteRows = False
        Me.pay_dg.AllowUserToOrderColumns = True
        Me.pay_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.pay_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.pay_id, Me.pay_agent_from, Me.pay_agent2, Me.pay_amount, Me.pay_debtorID})
        Me.pay_dg.Location = New System.Drawing.Point(33, 98)
        Me.pay_dg.Name = "pay_dg"
        Me.pay_dg.ReadOnly = True
        Me.pay_dg.Size = New System.Drawing.Size(464, 312)
        Me.pay_dg.TabIndex = 3
        '
        'pay_id
        '
        Me.pay_id.HeaderText = "Pay Id"
        Me.pay_id.Name = "pay_id"
        Me.pay_id.ReadOnly = True
        Me.pay_id.Visible = False
        '
        'pay_agent_from
        '
        Me.pay_agent_from.HeaderText = "Transferred From"
        Me.pay_agent_from.Name = "pay_agent_from"
        Me.pay_agent_from.ReadOnly = True
        '
        'pay_agent2
        '
        Me.pay_agent2.HeaderText = "Transferred To"
        Me.pay_agent2.Name = "pay_agent2"
        Me.pay_agent2.ReadOnly = True
        '
        'pay_amount
        '
        Me.pay_amount.HeaderText = "Amount"
        Me.pay_amount.Name = "pay_amount"
        Me.pay_amount.ReadOnly = True
        '
        'pay_debtorID
        '
        Me.pay_debtorID.HeaderText = "DebtorID"
        Me.pay_debtorID.Name = "pay_debtorID"
        Me.pay_debtorID.ReadOnly = True
        '
        'addbtn
        '
        Me.addbtn.Location = New System.Drawing.Point(191, 33)
        Me.addbtn.Name = "addbtn"
        Me.addbtn.Size = New System.Drawing.Size(113, 23)
        Me.addbtn.TabIndex = 0
        Me.addbtn.Text = "Add new entry"
        Me.addbtn.UseVisualStyleBackColor = True
        '
        'tr_lbl
        '
        Me.tr_lbl.AutoSize = True
        Me.tr_lbl.Location = New System.Drawing.Point(40, 20)
        Me.tr_lbl.Name = "tr_lbl"
        Me.tr_lbl.Size = New System.Drawing.Size(77, 13)
        Me.tr_lbl.TabIndex = 5
        Me.tr_lbl.Text = "Transfers from:"
        '
        'log_lbl
        '
        Me.log_lbl.AutoSize = True
        Me.log_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.log_lbl.Location = New System.Drawing.Point(66, 41)
        Me.log_lbl.Name = "log_lbl"
        Me.log_lbl.Size = New System.Drawing.Size(24, 13)
        Me.log_lbl.TabIndex = 7
        Me.log_lbl.Text = "log"
        '
        'disp_lbl
        '
        Me.disp_lbl.AutoSize = True
        Me.disp_lbl.Location = New System.Drawing.Point(147, 82)
        Me.disp_lbl.Name = "disp_lbl"
        Me.disp_lbl.Size = New System.Drawing.Size(39, 13)
        Me.disp_lbl.TabIndex = 8
        Me.disp_lbl.Text = "Label1"
        '
        'excelbtn
        '
        Me.excelbtn.Location = New System.Drawing.Point(573, 98)
        Me.excelbtn.Name = "excelbtn"
        Me.excelbtn.Size = New System.Drawing.Size(125, 49)
        Me.excelbtn.TabIndex = 2
        Me.excelbtn.Text = "Yesterdays payments to excel"
        Me.excelbtn.UseVisualStyleBackColor = True
        '
        'todaybtn
        '
        Me.todaybtn.Location = New System.Drawing.Point(573, 193)
        Me.todaybtn.Name = "todaybtn"
        Me.todaybtn.Size = New System.Drawing.Size(125, 49)
        Me.todaybtn.TabIndex = 3
        Me.todaybtn.Text = "Todays payments to excel"
        Me.todaybtn.UseVisualStyleBackColor = True
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PaymentSheetBindingSource
        '
        Me.PaymentSheetBindingSource.DataMember = "PaymentSheet"
        Me.PaymentSheetBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'PaymentSheetTableAdapter
        '
        Me.PaymentSheetTableAdapter.ClearBeforeFill = True
        '
        'Mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(824, 438)
        Me.Controls.Add(Me.todaybtn)
        Me.Controls.Add(Me.excelbtn)
        Me.Controls.Add(Me.disp_lbl)
        Me.Controls.Add(Me.log_lbl)
        Me.Controls.Add(Me.tr_lbl)
        Me.Controls.Add(Me.addbtn)
        Me.Controls.Add(Me.pay_dg)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "Mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Payment Sheet"
        CType(Me.pay_dg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PaymentSheetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents pay_dg As System.Windows.Forms.DataGridView
    Friend WithEvents addbtn As System.Windows.Forms.Button
    Friend WithEvents tr_lbl As System.Windows.Forms.Label
    Friend WithEvents FeesSQLDataSet As PaymentSheet.FeesSQLDataSet
    Friend WithEvents PaymentSheetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PaymentSheetTableAdapter As PaymentSheet.FeesSQLDataSetTableAdapters.PaymentSheetTableAdapter
    Friend WithEvents log_lbl As System.Windows.Forms.Label
    Friend WithEvents disp_lbl As System.Windows.Forms.Label
    Friend WithEvents pay_id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pay_agent_from As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pay_agent2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pay_amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pay_debtorID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents excelbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents todaybtn As System.Windows.Forms.Button
End Class

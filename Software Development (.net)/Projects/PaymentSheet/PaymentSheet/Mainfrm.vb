Public Class Mainfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim date_14_days_ago As Date = DateAdd(DateInterval.Day, -14, Now)
        Me.PaymentSheetTableAdapter.DeleteQuery1(date_14_days_ago)

        disp_lbl.Text = "My entries for today.  Double_click an entry to update/delete"
        log_user = My.User.Name
        Dim slash_idx As Integer = InStr(log_user, "\")
        If slash_idx > 0 Then
            log_user = Microsoft.VisualBasic.Right(log_user, log_user.length - slash_idx)
        End If
        log_lbl.Text = log_user
        populate_pay_grid()
    End Sub
    


    Private Sub addbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addbtn.Click
        addfrm.ShowDialog()
    End Sub

    Private Sub pay_dg_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles pay_dg.CellContentClick

    End Sub

    Private Sub pay_dg_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles pay_dg.CellDoubleClick
        selected_pay_id = pay_dg.Rows(e.RowIndex).Cells(0).Value
        selected_transfer_to = pay_dg.Rows(e.RowIndex).Cells(2).Value
        upd_row = e.RowIndex
        updatefrm.ShowDialog()
    End Sub

    Private Sub excelbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles excelbtn.Click
        Dim start_date As Date = DateAdd(DateInterval.Day, -1, Now)
        Dim end_date As Date = Now
        start_date = CDate(Format(start_date, "yyyy MM dd") & " 0:0:0")
        end_date = CDate(Format(end_date, "yyyy MM dd") & " 0:0:0")
        PaymentSheetTableAdapter.FillBy1(FeesSQLDataSet.PaymentSheet, Format(start_date, "yyyy-MM-dd"), Format(end_date, "yyyy-MM-dd"))
        Dim idx As Integer
        Dim file As String = "Agent,Tranfer to,Amount,DebtorID" & vbNewLine
        For idx = 0 To Me.FeesSQLDataSet.PaymentSheet.Rows.Count - 1
            file = file & Me.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(3) & ","
            file = file & Me.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(4) & ","
            file = file & "�" & Format(Me.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(5), "f") & ","
            file = file & Me.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(6) & vbNewLine
        Next
        If idx = 0 Then
            MessageBox.Show("There are no payments")
            Exit Sub
        End If
        With SaveFileDialog1
            .Title = "Save to excel"
            .Filter = "excel|*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "payment_transfers_" & Format(start_date, "dd.MM.yyyy") & ".xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
        End If
    End Sub

    Private Sub todaybtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles todaybtn.Click
        Dim start_date As Date = Now
        start_date = CDate(Format(start_date, "yyyy MM dd") & " 0:0:0")
        PaymentSheetTableAdapter.FillBy2(FeesSQLDataSet.PaymentSheet, Format(start_date, "yyyy-MM-dd"))
        Dim idx As Integer
        Dim file As String = "Agent,Tranfer to,Amount,DebtorID" & vbNewLine
        For idx = 0 To Me.FeesSQLDataSet.PaymentSheet.Rows.Count - 1
            file = file & Me.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(3) & ","
            file = file & Me.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(4) & ","
            file = file & "�" & Format(Me.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(5), "f") & ","
            file = file & Me.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(6) & vbNewLine
        Next
        If idx = 0 Then
            MessageBox.Show("There are no payments")
            Exit Sub
        End If
        With SaveFileDialog1
            .Title = "Save to excel"
            .Filter = "excel|*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "payment_transfers_" & Format(Now, "dd.MM.yyyy") & ".xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
        End If
    End Sub
End Class
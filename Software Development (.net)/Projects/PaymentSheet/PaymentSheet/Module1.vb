Module Module1
    Public log_user, selected_pay_id, selected_transfer_to As String
    Public conn_open As Boolean
    Public upd_row As Integer
    Public Sub populate_pay_grid()
        Mainfrm.PaymentSheetTableAdapter.FillBy(Mainfrm.FeesSQLDataSet.PaymentSheet, log_user, Format(Now, "dd/MM/yyyy"))
        Mainfrm.pay_dg.Rows.Clear()
        Dim idx As Integer
        For idx = 0 To Mainfrm.FeesSQLDataSet.PaymentSheet.Rows.Count - 1
            Dim pay_id As Integer = Mainfrm.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(0)
            Dim agent_from As String = Mainfrm.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(3)
            Dim agent2 As String = Mainfrm.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(4)
            Dim amount As Decimal = Mainfrm.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(5)
            Dim debtorID As Integer = Mainfrm.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(6)
            Mainfrm.pay_dg.Rows.Add(pay_id, agent_from, agent2, Format(amount, "c"), debtorID)
        Next
    End Sub
End Module

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class addfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.quitbtn = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.transfer_to_cbox = New System.Windows.Forms.ComboBox
        Me.amount_tbox = New System.Windows.Forms.TextBox
        Me.debtor_tbox = New System.Windows.Forms.TextBox
        Me.transfer_to_lbl = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.transfer_from_lbl = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'quitbtn
        '
        Me.quitbtn.Location = New System.Drawing.Point(29, 194)
        Me.quitbtn.Name = "quitbtn"
        Me.quitbtn.Size = New System.Drawing.Size(135, 23)
        Me.quitbtn.TabIndex = 3
        Me.quitbtn.Text = "Exit without Saving"
        Me.quitbtn.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(376, 194)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(100, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "Save and Exit"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'transfer_to_cbox
        '
        Me.transfer_to_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.transfer_to_cbox.FormattingEnabled = True
        Me.transfer_to_cbox.Location = New System.Drawing.Point(43, 71)
        Me.transfer_to_cbox.Name = "transfer_to_cbox"
        Me.transfer_to_cbox.Size = New System.Drawing.Size(121, 21)
        Me.transfer_to_cbox.TabIndex = 0
        '
        'amount_tbox
        '
        Me.amount_tbox.Location = New System.Drawing.Point(213, 72)
        Me.amount_tbox.Name = "amount_tbox"
        Me.amount_tbox.Size = New System.Drawing.Size(100, 20)
        Me.amount_tbox.TabIndex = 1
        '
        'debtor_tbox
        '
        Me.debtor_tbox.Location = New System.Drawing.Point(349, 71)
        Me.debtor_tbox.Name = "debtor_tbox"
        Me.debtor_tbox.Size = New System.Drawing.Size(100, 20)
        Me.debtor_tbox.TabIndex = 2
        '
        'transfer_to_lbl
        '
        Me.transfer_to_lbl.AutoSize = True
        Me.transfer_to_lbl.Location = New System.Drawing.Point(63, 55)
        Me.transfer_to_lbl.Name = "transfer_to_lbl"
        Me.transfer_to_lbl.Size = New System.Drawing.Size(65, 13)
        Me.transfer_to_lbl.TabIndex = 5
        Me.transfer_to_lbl.Text = "Transfer To:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(238, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Amount:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(373, 55)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Debtor ID:"
        '
        'transfer_from_lbl
        '
        Me.transfer_from_lbl.AutoSize = True
        Me.transfer_from_lbl.Location = New System.Drawing.Point(185, 9)
        Me.transfer_from_lbl.Name = "transfer_from_lbl"
        Me.transfer_from_lbl.Size = New System.Drawing.Size(39, 13)
        Me.transfer_from_lbl.TabIndex = 8
        Me.transfer_from_lbl.Text = "Label4"
        '
        'addfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(498, 266)
        Me.Controls.Add(Me.transfer_from_lbl)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.transfer_to_lbl)
        Me.Controls.Add(Me.debtor_tbox)
        Me.Controls.Add(Me.amount_tbox)
        Me.Controls.Add(Me.transfer_to_cbox)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.quitbtn)
        Me.Name = "addfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add payment to sheet"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents quitbtn As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents transfer_to_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents amount_tbox As System.Windows.Forms.TextBox
    Friend WithEvents debtor_tbox As System.Windows.Forms.TextBox
    Friend WithEvents transfer_to_lbl As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents transfer_from_lbl As System.Windows.Forms.Label
End Class

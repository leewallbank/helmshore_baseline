Public Class addfrm

    Private Sub addfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        transfer_from_lbl.Text = "Transfer from " & log_user
        amount_tbox.Text = ""
        debtor_tbox.Text = ""
        param2 = "select login_name from Bailiff where agent_type = 'P'" & _
        " and status = 'O' and name_fore is not null order by login_name"
        Dim bail_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to read agents from onestep")
            Exit Sub
        End If
        transfer_to_cbox.Items.Clear()
        Dim idx As Integer
        Dim bail_rows As Integer = no_of_rows - 1
        For idx = 0 To bail_rows
            Dim phone_agent As String = bail_dataset.Tables(0).Rows(idx).Item(0)
            If Microsoft.VisualBasic.Left(phone_agent, 1) = "-" Then
                Continue For
            End If
            If log_user <> phone_agent Then
                transfer_to_cbox.Items.Add(phone_agent)
            End If
        Next
    End Sub

    Private Sub quitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitbtn.Click
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        If transfer_to_cbox.SelectedIndex < 0 Then
            MsgBox("Select who to transfer to")
            Exit Sub
        End If
        Dim amount As Decimal
        Try
            amount = amount_tbox.Text
        Catch ex As Exception
            MsgBox("Amount should be numeric")
            Exit Sub
        End Try
        If amount <= 0 Then
            MsgBox("Amount should be more than zero")
        End If
        Dim debtor As Integer
        Try
            debtor = debtor_tbox.Text
        Catch ex As Exception
            MsgBox("debtorID should be a valid case number on onestep")
            Exit Sub
        End Try
        param2 = "select _rowid from Debtor where _rowid = " & debtor
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("DebtorID does not exist on onestep")
            Exit Sub
        End If
        Try
            Mainfrm.PaymentSheetTableAdapter.InsertQuery(log_user, Now, log_user, transfer_to_cbox.Text, Format(amount, "f"), debtor_tbox.Text)
            populate_pay_grid()
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        Me.Close()
    End Sub
End Class
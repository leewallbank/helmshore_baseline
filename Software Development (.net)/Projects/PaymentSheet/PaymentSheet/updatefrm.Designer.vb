<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class updatefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.transfer_to_lbl = New System.Windows.Forms.Label
        Me.upd_debtor_tbox = New System.Windows.Forms.TextBox
        Me.upd_amount_tbox = New System.Windows.Forms.TextBox
        Me.upd_transfer_to_cbox = New System.Windows.Forms.ComboBox
        Me.upd_lbl = New System.Windows.Forms.Label
        Me.quitbtn = New System.Windows.Forms.Button
        Me.updbtn = New System.Windows.Forms.Button
        Me.delbtn = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(435, 76)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(56, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Debtor ID:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(300, 76)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Amount:"
        '
        'transfer_to_lbl
        '
        Me.transfer_to_lbl.AutoSize = True
        Me.transfer_to_lbl.Location = New System.Drawing.Point(125, 76)
        Me.transfer_to_lbl.Name = "transfer_to_lbl"
        Me.transfer_to_lbl.Size = New System.Drawing.Size(65, 13)
        Me.transfer_to_lbl.TabIndex = 0
        Me.transfer_to_lbl.Text = "Transfer To:"
        '
        'upd_debtor_tbox
        '
        Me.upd_debtor_tbox.Location = New System.Drawing.Point(411, 92)
        Me.upd_debtor_tbox.Name = "upd_debtor_tbox"
        Me.upd_debtor_tbox.Size = New System.Drawing.Size(100, 20)
        Me.upd_debtor_tbox.TabIndex = 10
        '
        'upd_amount_tbox
        '
        Me.upd_amount_tbox.Location = New System.Drawing.Point(275, 93)
        Me.upd_amount_tbox.Name = "upd_amount_tbox"
        Me.upd_amount_tbox.Size = New System.Drawing.Size(100, 20)
        Me.upd_amount_tbox.TabIndex = 9
        '
        'upd_transfer_to_cbox
        '
        Me.upd_transfer_to_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.upd_transfer_to_cbox.FormattingEnabled = True
        Me.upd_transfer_to_cbox.Location = New System.Drawing.Point(105, 92)
        Me.upd_transfer_to_cbox.Name = "upd_transfer_to_cbox"
        Me.upd_transfer_to_cbox.Size = New System.Drawing.Size(121, 21)
        Me.upd_transfer_to_cbox.TabIndex = 8
        '
        'upd_lbl
        '
        Me.upd_lbl.AutoSize = True
        Me.upd_lbl.Location = New System.Drawing.Point(48, 20)
        Me.upd_lbl.Name = "upd_lbl"
        Me.upd_lbl.Size = New System.Drawing.Size(39, 13)
        Me.upd_lbl.TabIndex = 14
        Me.upd_lbl.Text = "Label1"
        '
        'quitbtn
        '
        Me.quitbtn.Location = New System.Drawing.Point(38, 199)
        Me.quitbtn.Name = "quitbtn"
        Me.quitbtn.Size = New System.Drawing.Size(152, 23)
        Me.quitbtn.TabIndex = 3
        Me.quitbtn.Text = "Exit without change"
        Me.quitbtn.UseVisualStyleBackColor = True
        '
        'updbtn
        '
        Me.updbtn.Location = New System.Drawing.Point(475, 199)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(127, 23)
        Me.updbtn.TabIndex = 4
        Me.updbtn.Text = "Update and Exit"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'delbtn
        '
        Me.delbtn.Location = New System.Drawing.Point(275, 20)
        Me.delbtn.Name = "delbtn"
        Me.delbtn.Size = New System.Drawing.Size(142, 23)
        Me.delbtn.TabIndex = 5
        Me.delbtn.Text = "Delete this entry"
        Me.delbtn.UseVisualStyleBackColor = True
        '
        'updatefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(641, 266)
        Me.Controls.Add(Me.delbtn)
        Me.Controls.Add(Me.updbtn)
        Me.Controls.Add(Me.quitbtn)
        Me.Controls.Add(Me.upd_lbl)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.transfer_to_lbl)
        Me.Controls.Add(Me.upd_debtor_tbox)
        Me.Controls.Add(Me.upd_amount_tbox)
        Me.Controls.Add(Me.upd_transfer_to_cbox)
        Me.Name = "updatefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Update/Delete payment  transfer"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents transfer_to_lbl As System.Windows.Forms.Label
    Friend WithEvents upd_debtor_tbox As System.Windows.Forms.TextBox
    Friend WithEvents upd_amount_tbox As System.Windows.Forms.TextBox
    Friend WithEvents upd_transfer_to_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents upd_lbl As System.Windows.Forms.Label
    Friend WithEvents quitbtn As System.Windows.Forms.Button
    Friend WithEvents updbtn As System.Windows.Forms.Button
    Friend WithEvents delbtn As System.Windows.Forms.Button
End Class

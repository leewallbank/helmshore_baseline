Public Class Form1
    Dim new_file As String = ""
    Dim record As String = ""
    Dim filename As String
    Dim first_error As Boolean = True
    Dim first_change As Boolean = True
    Dim applicant, summons_no, maat_id, first_name, surname, dob, ni_no As String
    Dim mthly_contrib_amt_str, upfront_contrib_amt_str, income_contrib_cap_str, income_uplift_applied As String
    Dim case_type, in_court_custody, debt_addr1, debt_addr2, debt_addr3, debt_addr4, debt_postcode
    Dim postal_addr1, postal_addr2, postal_addr3, postal_addr4, postal_postcode, landline, mobile, email As String
    Dim equity_amt, cap_amt As Integer
    Dim mthly_contrib_amt, upfront_contrib_amt, income_contrib_cap As Decimal
    Dim equity_amt_verified, cap_asset_type, cap_amt_verified, comments As String
    Dim allowable_cap_threshold, effective_date, pref_pay_method, pref_pay_date As String
    Dim bank_acc_name, bank_acc_no, sort_code, emp_status, rep_order, withdrawal_date, hardship_appl_recvd As String

    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click

        Try
            With OpenFileDialog1
                .Title = "Read XML file"
                .Filter = "XML file|*.xml"
                .FileName = ""
                .InitialDirectory = "c:\"
                .CheckFileExists = True
            End With
            Dim xml_dataSet As New DataSet()
            Dim doc As New Xml.XmlDocument()

            Dim file As String = "Applicant ID" & "|" & "SummonsNum" & "|" & "MAAT ID" & _
                        "|" & "First name" & "|" & "Surname" & "|" & "Date of birth" & "|" & "NI No" & _
                        "|" & "Monthly Amount" & "|" & "Upfront Amt" & "|" & "Income contribution cap" & "|" & _
                        "Debt addr1" & "|" & "Debt addr2" & "|" & "Debt addr3" & "|" & "Debt addr4" & _
                        "|" & "Debt postcode" & "|" & "Curr addr1" & "|" & "Curr addr2" & _
                        "|" & "Curr addr3" & "|" & "Curr addr4" & "|" & "Curr postcode" & "|" & _
                        "Landline" & "|" & "Mobile" & "|" & "Email" & "|" & _
                         "Effective date" & "|" & "Comments" & vbNewLine

            Dim idx As Integer = 0
            If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                Try
                    filename = OpenFileDialog1.FileName
                    Dim ln2 As Integer = Microsoft.VisualBasic.Len(filename)

                    new_file = Microsoft.VisualBasic.Left(filename, ln2 - 4) & "_error.txt"
                    My.Computer.FileSystem.WriteAllText(new_file, "Error messages " & Now, False)

                    new_file = Microsoft.VisualBasic.Left(filename, ln2 - 4) & "_changes.txt"
                    Dim change_message As String = "Debtor" & vbTab & "Field" & vbTab & "Onestep Value" & vbTab & _
                    "New value" & vbNewLine
                    My.Computer.FileSystem.WriteAllText(new_file, change_message, False)

                    reset_fields()
                    Dim rdr_name As String = ""
                    Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
                    While (reader.ReadState <> Xml.ReadState.EndOfFile)
                        If idx = 0 Then
                            reader.Read()
                        End If

                        Try
                            rdr_name = reader.Name
                        Catch
                            Continue While
                        End Try
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        Select Case rdr_name
                            Case "APPLICANT_ID"
                                If idx > 0 Then
                                    end_of_record()
                                    file = file & record & vbNewLine
                                    reset_fields()
                                End If
                                applicant = reader.ReadElementContentAsString
                                idx += 1
                            Case "MAAT_ID"
                                maat_id = reader.ReadElementContentAsString
                            Case "ARREST_SUMMONS_NUMBER"
                                summons_no = reader.ReadElementContentAsString
                            Case "FIRST_NAME"
                                first_name = reader.ReadElementContentAsString
                            Case "LAST_NAME"
                                surname = reader.ReadElementContentAsString
                            Case "DOB"
                                dob = reader.ReadElementContentAsString
                            Case "NI_NUMBER"
                                ni_no = reader.ReadElementContentAsString
                            Case "MONTHLY_CONTRIBUTION"
                                mthly_contrib_amt_str = reader.ReadElementContentAsString
                                mthly_contrib_amt = mthly_contrib_amt_str / 100
                            Case "UPFRONT_CONTRIBUTION"
                                upfront_contrib_amt_str = reader.ReadElementContentAsString
                                upfront_contrib_amt = upfront_contrib_amt_str / 100
                            Case "INCOME_CONTRIBUTION_CAP"
                                income_contrib_cap_str = reader.ReadElementContentAsString
                                income_contrib_cap = income_contrib_cap_str / 100
                            Case "INCOME_UPLIFT_APPLIED"
                                income_uplift_applied = reader.ReadElementContentAsString
                                comments = comments & "Inc uplift applied = " & income_uplift_applied & ":"
                            Case "DESCRIPTION"
                                case_type = reader.ReadElementContentAsString
                            Case "IN_COURT_CUSTODY"
                                in_court_custody = reader.ReadElementContentAsString
                            Case "HOME_ADDRESS"
                                reader.Read()
                                While reader.Name <> "HOME_ADDRESS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "LINE1"
                                            debt_addr1 = reader.ReadElementContentAsString
                                        Case "LINE2"
                                            debt_addr2 = reader.ReadElementContentAsString
                                        Case "LINE3"
                                            debt_addr3 = reader.ReadElementContentAsString
                                        Case "CITY"
                                            debt_addr4 = reader.ReadElementContentAsString
                                        Case "POSTCODE"
                                            debt_postcode = reader.ReadElementContentAsString
                                            'Case "COUNTRY"
                                            'home_addr = home_addr & " " & reader.ReadElementContentAsString
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "POSTAL_ADDRESS"
                                reader.Read()
                                While reader.Name <> "POSTAL_ADDRESS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "LINE1"
                                            postal_addr1 = reader.ReadElementContentAsString
                                        Case "LINE2"
                                            postal_addr2 = reader.ReadElementContentAsString
                                        Case "LINE3"
                                            postal_addr3 = reader.ReadElementContentAsString
                                        Case "CITY"
                                            postal_addr4 = reader.ReadElementContentAsString
                                        Case "POSTCODE"
                                            postal_postcode = reader.ReadElementContentAsString
                                            'Case "COUNTRY"
                                            'postal_addr = postal_addr & " " & reader.ReadElementContentAsString
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "CASE_TYPE"
                                Try
                                    case_type = reader.ReadElementContentAsString
                                Catch
                                End Try
                                If case_type <> "" Then
                                    comments = comments & "Case type = " & case_type & ":"
                                    Continue While
                                End If
                                reader.Read()
                                While reader.Name <> "CASE_TYPE"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "DESCRIPTION"
                                            case_type = Trim(reader.ReadElementContentAsString)
                                            comments = comments & "Case type = " & case_type & ":"
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "LANDLINE"
                                landline = reader.ReadElementContentAsString
                            Case "MOBILE"
                                mobile = reader.ReadElementContentAsString
                            Case "EMAIL"
                                email = reader.ReadElementContentAsString
                            Case "EQUITY_AMOUNT"
                                equity_amt = equity_amt + reader.ReadElementContentAsString
                            Case "EQUITY_AMOUNT_VERIFIED"
                                If equity_amt_verified <> "No" Then
                                    equity_amt_verified = reader.ReadElementContentAsString
                                    remove_chrs(equity_amt_verified)
                                End If
                            Case "CAPITAL_AMOUNT"
                                cap_amt = cap_amt + reader.ReadElementContentAsString
                            Case "CAPITAL_AMOUNT_VERIFIED"
                                If cap_amt_verified <> "No" Then
                                    cap_amt_verified = reader.ReadElementContentAsString
                                    remove_chrs(cap_amt_verified)
                                End If
                            Case "CAPITAL_ASSES_TYPE"
                                reader.Read()
                                While reader.Name <> "CAPITAL_ASSES_TYPE"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "DESCRIPTION"
                                            Dim temp As String = reader.ReadElementContentAsString
                                            remove_chrs(temp)
                                            If cap_asset_type = "" Then
                                                cap_asset_type = temp
                                            Else
                                                cap_asset_type = cap_asset_type & " & " & temp
                                            End If
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "ALLOWABLE_CAPITAL_THRESHOLD"
                                allowable_cap_threshold = reader.ReadElementContentAsString
                                remove_chrs(allowable_cap_threshold)
                            Case "EFFECTIVE_DATE"
                                effective_date = reader.ReadElementContentAsString
                            Case "PREFERRED_PAYMENT_METHOD"
                                reader.Read()
                                While reader.Name <> "PREFERRED_PAYMENT_METHOD"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "DESCRIPTION"
                                            pref_pay_method = reader.ReadElementContentAsString
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "PREFERRED_PAYMENT_DAY"
                                pref_pay_date = reader.ReadElementContentAsString
                            Case "ACCOUNT_NAME"
                                bank_acc_name = reader.ReadElementContentAsString
                            Case "ACCOUNT_NO"
                                bank_acc_no = reader.ReadElementContentAsString
                            Case "SORT_CODE"
                                sort_code = reader.ReadElementContentAsString
                            Case "EMPLOYMENT_STATUS"
                                Try
                                    emp_status = reader.ReadElementContentAsString
                                Catch
                                End Try
                                If emp_status <> "" Then
                                    comments = comments & "Emp status = " & emp_status & ":"
                                    Continue While
                                End If
                                While reader.Name <> "EMPLOYMENT_STATUS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "DESCRIPTION"
                                            emp_status = reader.ReadElementContentAsString
                                            comments = comments & "Emp status = " & emp_status & ":"
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case Else
                                reader.Read()
                        End Select
                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Else
                MsgBox("XML file not processed")
                Exit Sub
            End If
            'write file including last applicant
            end_of_record()
            file = file & record & vbNewLine
            Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_preprocess.txt"
            My.Computer.FileSystem.WriteAllText(new_file, file, False)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        MsgBox("file saved as " & new_file)
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub
    Private Sub end_of_record()
        Try
            If applicant = "" Then
                write_error("No applicant found")
                Exit Sub
            End If
            If equity_amt > 0 Then
                comments = comments & "Equity amt = " & equity_amt & ":"
                comments = comments & "Equity amt verified = " & equity_amt_verified & ":"
            End If

            If cap_amt > 0 Then
                comments = comments & "Cap asset type = " & cap_asset_type & ":"
                comments = comments & "Cap amt = " & cap_amt & ":"
                comments = comments & "Cap amt verified = " & cap_amt_verified & ":"
                comments = comments & "Allowable cap threshold = " & allowable_cap_threshold & ":"
            End If
            If landline = "" Then
                landline = mobile
                mobile = ""
            End If

            'check if maat-id already exists on onestep
            param1 = "onestep"
            param2 = "select _rowid, offence_number, offence_court, debt_amount, debt_costs, " & _
            " offenceValue, empNI, add_phone, add_fax, addEmail, prevReference, add_postcode, address from Debtor" & _
            " where client_ref = '" & maat_id & _
            "' and clientschemeID = 1892"
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                record = applicant & "|" & summons_no & "|" & maat_id & "|" & first_name & "|" & _
                surname & "|" & dob & "|" & ni_no & "|" & mthly_contrib_amt & "|" & upfront_contrib_amt & "|" & _
                income_contrib_cap & "|" & debt_addr1 & "|" & debt_addr2 & "|" & debt_addr3 & "|" & _
                debt_addr4 & "|" & debt_postcode & "|" & postal_addr1 & "|" & postal_addr2 & "|" & _
                postal_addr3 & "|" & postal_addr4 & "|" & postal_postcode & "|" & landline & "|" & mobile & "|" & email & _
                "|" & effective_date
                Dim comments2 As String = comments
                If Microsoft.VisualBasic.Len(comments) > 250 Then
                    Dim len As Integer = Microsoft.VisualBasic.Len(comments)
                    Dim idx As Integer
                    For idx = 250 To 1 Step -1
                        If Mid(comments, idx, 1) = ":" Then
                            Exit For
                        End If
                    Next
                    comments2 = Microsoft.VisualBasic.Left(comments, idx)
                    Dim idx2 As Integer
                    For idx2 = idx To 250
                        comments2 = comments2 & " "
                    Next
                    comments2 = comments2 & Microsoft.VisualBasic.Right(comments, len - idx)
                End If
                record = record & "|" & comments2
                Exit Sub
            End If

            'case exists on onestep so look for changes
            If no_of_rows > 1 Then
                write_error("Maat ID = " & maat_id & " has multiple cases on onestep")
                Exit Sub
            End If
            Dim debtor As String = debtor_dataset.Tables(0).Rows(0).Item(0)

            'see if prevref has changed
            Dim orig_prevref As String
            Try
                orig_prevref = debtor_dataset.Tables(0).Rows(0).Item(10)
            Catch
                orig_prevref = ""
            End Try
            If applicant <> orig_prevref Then
                write_change(debtor & vbTab & "Prev Reference(Applicant)" & "|" & _
                                orig_prevref & "|" & applicant & vbNewLine)
            End If

            'see if postcode has changed
            Dim orig_postcode As String
            Try
                orig_postcode = Trim(debtor_dataset.Tables(0).Rows(0).Item(11))
            Catch
                orig_postcode = ""
            End Try
            If LCase(debt_postcode) <> LCase(orig_postcode) Then
                Dim orig_home_addr = debtor_dataset.Tables(0).Rows(0).Item(12)
                Dim idx As Integer
                Dim addr As String = ""
                For idx = 1 To Microsoft.VisualBasic.Len(Trim(orig_home_addr))
                    If Mid(orig_home_addr, idx, 1) <> Chr(10) And _
                    Mid(orig_home_addr, idx, 1) <> Chr(13) Then
                        addr = addr & Mid(orig_home_addr, idx, 1)
                    Else
                        addr = addr & " "
                    End If
                Next
                write_change(debtor & "|" & "Current Address" & "|" & _
                                addr & "|" & debt_addr1 & " " & debt_addr2 & " " & debt_addr3 & " " & _
                                debt_addr4 & " " & debt_postcode & vbNewLine)
            End If

            'see if summons number has changed
            Dim orig_summons_no As String
            Try
                orig_summons_no = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
            Catch
                orig_summons_no = ""
            End Try
            If LCase(summons_no) <> LCase(orig_summons_no) Then
                write_change(debtor & vbTab & "Summons number" & "|" & _
                orig_summons_no & "|" & summons_no & vbNewLine)
            End If

            'see if effective date has changed
            If effective_date <> debtor_dataset.Tables(0).Rows(0).Item(2) Then
                Dim os_date As Date = debtor_dataset.Tables(0).Rows(0).Item(2)
                write_change(debtor & "|" & "Effective date(LO date)" & _
                "|" & Format(os_date, "dd-MM-yyyy") & "|" & effective_date & vbNewLine)
            End If

            'see if debt amt has changed
            Dim test_amt As Decimal
            If upfront_contrib_amt_str = "" Then
                test_amt = 0
            Else
                test_amt = upfront_contrib_amt_str / 100
            End If
            Dim debt_amt As Decimal = debtor_dataset.Tables(0).Rows(0).Item(3)

            If test_amt <> debt_amt Then
                write_change(debtor & "|" & "Debt amount" & "|" & _
                Format(debt_amt, "#.##") & "|" & Format(test_amt, "#.##") & vbNewLine)
            End If

            'see if debt costs has changed
            If mthly_contrib_amt_str = "" Then
                test_amt = 0
            Else
                test_amt = mthly_contrib_amt_str / 100
            End If
            Dim debt_costs As Decimal = debtor_dataset.Tables(0).Rows(0).Item(4)
            If test_amt <> debt_costs Then
                write_change(debtor & "|" & "Debt Costs" & "|" & _
                Format(debt_costs, "#.##") & "|" & test_amt & vbNewLine)
            End If

            'see if offence value has changed
            If income_contrib_cap_str = "" Then
                test_amt = 0
            Else
                test_amt = income_contrib_cap_str / 100
            End If
            Dim offence_value As Decimal = debtor_dataset.Tables(0).Rows(0).Item(5)
            If test_amt <> offence_value Then
                write_change(debtor & "|" & "Offence value" & "|" & _
                Format(offence_value, "#.##") & "|" & Format(test_amt, "#.##") & vbNewLine)
            End If

            'see if NIno has changed
            Dim orig_ni_no As String
            Try
                orig_ni_no = debtor_dataset.Tables(0).Rows(0).Item(6)
            Catch
                orig_ni_no = ""
            End Try
            If ni_no <> orig_ni_no Then
                write_change(debtor & vbTab & "NI No" & "|" & orig_ni_no & "|" & ni_no & vbNewLine)
            End If

            'check if phone number has changed
            Dim orig_phone_no As String
            Try
                orig_phone_no = Trim(debtor_dataset.Tables(0).Rows(0).Item(7))
            Catch
                orig_phone_no = ""
            End Try

            Dim orig_fax_no As String
            Try
                orig_fax_no = Trim(debtor_dataset.Tables(0).Rows(0).Item(8))
            Catch
                orig_fax_no = ""
            End Try
            If landline <> "" Then
                If landline <> orig_phone_no And _
                landline <> orig_fax_no Then
                    write_change(debtor & "|" & "Phone No" & "|" & orig_phone_no & "|" & landline & vbNewLine)
                End If
            End If
            If mobile <> "" Then
                If mobile <> orig_phone_no And _
                mobile <> orig_fax_no Then
                    write_change(debtor & "|" & "Mobile No" & "|" & orig_fax_no & "|" & mobile & vbNewLine)
                End If
            End If

            'check if email has changed
            Dim orig_email As String
            Try
                orig_email = debtor_dataset.Tables(0).Rows(0).Item(9)
            Catch
                orig_email = ""
            End Try
            If email <> orig_email Then
                write_change(debtor & "|" & "Email" & "|" & orig_email & "|" & email & vbNewLine)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub write_error(ByVal error_message As String)
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_error.txt"
        error_message = error_message & vbNewLine
        My.Computer.FileSystem.WriteAllText(new_file, error_message, True)
    End Sub
    Private Sub write_change(ByVal change_message As String)
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_changes.txt"
        My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
    End Sub
    Private Sub remove_chrs(ByRef in_field As String)
        Dim idx2 As Integer
        Dim out_field As String = ""
        For idx2 = 1 To Microsoft.VisualBasic.Len(in_field)
            If Mid(in_field, idx2, 1) <> Chr(9) And _
              Mid(in_field, idx2, 1) <> Chr(10) And _
            Mid(in_field, idx2, 1) <> Chr(13) Then
                out_field = out_field & Mid(in_field, idx2, 1)
            End If
        Next
        in_field = out_field
    End Sub

    Private Sub reset_fields()
        applicant = ""
        summons_no = ""
        maat_id = ""
        first_name = ""
        surname = ""
        dob = ""
        ni_no = ""
        mthly_contrib_amt_str = ""
        upfront_contrib_amt_str = ""
        income_contrib_cap_str = ""
        income_uplift_applied = ""
        case_type = ""
        in_court_custody = ""
        debt_addr1 = ""
        debt_addr2 = ""
        debt_addr3 = ""
        debt_addr4 = ""
        debt_postcode = ""
        postal_addr1 = ""
        postal_addr2 = ""
        postal_addr3 = ""
        postal_addr4 = ""
        postal_postcode = ""
        landline = ""
        mobile = ""
        email = ""
        equity_amt = 0
        equity_amt_verified = ""
        cap_asset_type = ""
        cap_amt = 0
        cap_amt_verified = ""
        allowable_cap_threshold = ""
        effective_date = ""
        pref_pay_method = ""
        pref_pay_date = ""
        bank_acc_name = ""
        bank_acc_no = ""
        sort_code = ""
        emp_status = ""
        rep_order = ""
        withdrawal_date = ""
        hardship_appl_recvd = ""
        comments = ""
    End Sub
End Class

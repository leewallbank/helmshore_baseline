Public Class disp_clientfrm

    Private Sub disp_clientfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        populate_client_table(cmpny_no)
        cl_no = 0
        Dim idx As Integer
        DataGridView1.Rows.Clear()
        For idx = 1 To cl_rows
            DataGridView1.Rows.Add(client_table(idx, 1), (Trim(client_table(idx, 2))))
            updmastfrm.ProgressBar1.Value = idx / cl_rows * 100
        Next
    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        cl_no = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        Me.Close()
    End Sub
End Class
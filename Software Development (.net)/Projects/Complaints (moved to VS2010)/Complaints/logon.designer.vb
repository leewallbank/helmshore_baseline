<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class logon
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Inv_textLabel As System.Windows.Forms.Label
        Dim Inv_passwordLabel As System.Windows.Forms.Label
        Dim cmpny_lbl As System.Windows.Forms.Label
        Me.exitbtn = New System.Windows.Forms.Button
        Me.savebtn = New System.Windows.Forms.Button
        Me.inv_passwordtextbox = New System.Windows.Forms.TextBox
        Me.Inv_textComboBox = New System.Windows.Forms.ComboBox
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmpny_cbox = New System.Windows.Forms.ComboBox
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.InvestigatorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InvestigatorsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
        Inv_textLabel = New System.Windows.Forms.Label
        Inv_passwordLabel = New System.Windows.Forms.Label
        cmpny_lbl = New System.Windows.Forms.Label
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Inv_textLabel
        '
        Inv_textLabel.AutoSize = True
        Inv_textLabel.Location = New System.Drawing.Point(102, 36)
        Inv_textLabel.Name = "Inv_textLabel"
        Inv_textLabel.Size = New System.Drawing.Size(93, 13)
        Inv_textLabel.TabIndex = 1
        Inv_textLabel.Text = "Select Your Name"
        '
        'Inv_passwordLabel
        '
        Inv_passwordLabel.AutoSize = True
        Inv_passwordLabel.Location = New System.Drawing.Point(107, 153)
        Inv_passwordLabel.Name = "Inv_passwordLabel"
        Inv_passwordLabel.Size = New System.Drawing.Size(83, 13)
        Inv_passwordLabel.TabIndex = 3
        Inv_passwordLabel.Text = "Enter password:"
        '
        'cmpny_lbl
        '
        cmpny_lbl.AutoSize = True
        cmpny_lbl.Location = New System.Drawing.Point(107, 96)
        cmpny_lbl.Name = "cmpny_lbl"
        cmpny_lbl.Size = New System.Drawing.Size(51, 13)
        cmpny_lbl.TabIndex = 5
        cmpny_lbl.Text = "Company"
        '
        'exitbtn
        '
        Me.exitbtn.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.exitbtn.Location = New System.Drawing.Point(248, 246)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 4
        Me.exitbtn.Text = "&Quit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'savebtn
        '
        Me.savebtn.Location = New System.Drawing.Point(105, 246)
        Me.savebtn.Name = "savebtn"
        Me.savebtn.Size = New System.Drawing.Size(75, 23)
        Me.savebtn.TabIndex = 3
        Me.savebtn.Text = "&Enter"
        Me.savebtn.UseVisualStyleBackColor = True
        '
        'inv_passwordtextbox
        '
        Me.inv_passwordtextbox.Location = New System.Drawing.Point(100, 178)
        Me.inv_passwordtextbox.Name = "inv_passwordtextbox"
        Me.inv_passwordtextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.inv_passwordtextbox.Size = New System.Drawing.Size(100, 20)
        Me.inv_passwordtextbox.TabIndex = 2
        '
        'Inv_textComboBox
        '
        Me.Inv_textComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Inv_textComboBox.FormattingEnabled = True
        Me.Inv_textComboBox.Location = New System.Drawing.Point(100, 52)
        Me.Inv_textComboBox.Name = "Inv_textComboBox"
        Me.Inv_textComboBox.Size = New System.Drawing.Size(121, 21)
        Me.Inv_textComboBox.TabIndex = 0
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'cmpny_cbox
        '
        Me.cmpny_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmpny_cbox.Enabled = False
        Me.cmpny_cbox.FormattingEnabled = True
        Me.cmpny_cbox.Location = New System.Drawing.Point(100, 112)
        Me.cmpny_cbox.Name = "cmpny_cbox"
        Me.cmpny_cbox.Size = New System.Drawing.Size(121, 21)
        Me.cmpny_cbox.TabIndex = 1
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'InvestigatorsBindingSource
        '
        Me.InvestigatorsBindingSource.DataMember = "Investigators"
        Me.InvestigatorsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'InvestigatorsTableAdapter
        '
        Me.InvestigatorsTableAdapter.ClearBeforeFill = True
        '
        'logon
        '
        Me.AcceptButton = Me.savebtn
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(352, 327)
        Me.Controls.Add(cmpny_lbl)
        Me.Controls.Add(Me.cmpny_cbox)
        Me.Controls.Add(Me.Inv_textComboBox)
        Me.Controls.Add(Me.inv_passwordtextbox)
        Me.Controls.Add(Me.savebtn)
        Me.Controls.Add(Inv_passwordLabel)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Inv_textLabel)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "logon"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = " Complaints System Log On"
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents savebtn As System.Windows.Forms.Button
    Friend WithEvents inv_passwordtextbox As System.Windows.Forms.TextBox
    Friend WithEvents Inv_textComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents InvestigatorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents InvestigatorsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
    Friend WithEvents cmpny_cbox As System.Windows.Forms.ComboBox
End Class

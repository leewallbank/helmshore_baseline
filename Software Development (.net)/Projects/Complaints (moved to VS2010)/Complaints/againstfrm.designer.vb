<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class againstfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.branch_dg = New System.Windows.Forms.DataGridView
        Me.ComplaintagainstBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.Complaint_againstBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Complaint_againstTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
        Me.against_code_dg = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.against_name_dg = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.branch_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComplaintagainstBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Complaint_againstBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'branch_dg
        '
        Me.branch_dg.AllowUserToDeleteRows = False
        Me.branch_dg.AllowUserToOrderColumns = True
        Me.branch_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.branch_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.against_code_dg, Me.against_name_dg})
        Me.branch_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.branch_dg.Location = New System.Drawing.Point(0, 0)
        Me.branch_dg.Name = "branch_dg"
        Me.branch_dg.Size = New System.Drawing.Size(476, 286)
        Me.branch_dg.TabIndex = 0
        '
        'ComplaintagainstBindingSource
        '
        Me.ComplaintagainstBindingSource.DataMember = "Complaint_against"
        Me.ComplaintagainstBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Complaint_againstBindingSource
        '
        Me.Complaint_againstBindingSource.DataMember = "Complaint_against"
        Me.Complaint_againstBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Complaint_againstTableAdapter
        '
        Me.Complaint_againstTableAdapter.ClearBeforeFill = True
        '
        'against_code_dg
        '
        Me.against_code_dg.HeaderText = "Against Code"
        Me.against_code_dg.Name = "against_code_dg"
        Me.against_code_dg.ReadOnly = True
        Me.against_code_dg.Visible = False
        '
        'against_name_dg
        '
        Me.against_name_dg.HeaderText = "Against Name"
        Me.against_name_dg.Name = "against_name_dg"
        Me.against_name_dg.Width = 200
        '
        'againstfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(476, 286)
        Me.Controls.Add(Me.branch_dg)
        Me.Name = "againstfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Compaint made against"
        CType(Me.branch_dg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComplaintagainstBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Complaint_againstBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents branch_dg As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents Complaint_againstBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaint_againstTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
    Friend WithEvents ComplaintagainstBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents against_code_dg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents against_name_dg As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

Public Class displayfrm3

    Private Sub displayfrm3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If admin_user = False Then
            updbtn.Enabled = False
        End If


        Comp_noTextBox.Text = comp_no
        'If env_str = "Prod" Then
        updatecmpfrm.Stage_EscalationTableAdapter.Fill(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Stage_Escalation)
        load_data()
        'Else
        'load2_data()
        'End If

    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Comp_noTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Comp_noTextBox.TextChanged

    End Sub
    Private Sub load2_data()
        'new load without table adapters
        param2 = "select comp_recpt_code,comp_entered_by, comp_date, comp_text, " & _
                    " comp_response, comp_recvd_code, comp_case_no," & _
                    "comp_client_no, comp_cat_code, comp_cat_number,comp_against_code," & _
                    "comp_against_code2,comp_allocated_to,comp_action, comp_founded," & _
                    "comp_stage_no, comp_cor_code,comp_priority,comp_against_agent,comp_against_agent2," & _
                    "comp_old_comp_no,comp_costs_cancel_value, comp_compensation,comp_response_no," & _
                    "comp_stage2_start_date,comp_stage2_completed_date,comp_stage2_completed_by, " & _
                    "comp_gender, comp_ethnicity, comp_ack_letter_date, comp_holding_letter_date,  " & _
                    "comp_stage2_ack_letter_date, comp_stage2_holding_letter_date," & _
                    "comp_stage3_start_date ,comp_stage3_completed_by, comp_stage3_completed_date," & _
                    "Comp_referred_to_solicitor,Comp_referred_to_insurer, Comp_monetary_risk, " & _
                    "Comp_legal_insurance_flag, comp_cancel_costs_reason, Comp_category, " & _
                    "comp_branch_no, Comp_insurance_type_no, " & _
                    " Comp_liability_flag, Comp_part_founded_agent_no, comp_feedback_no, " & _
                    " Comp_type_code, comp_completed_by, comp_completion_date, comp_hold_code, " & _
                    " comp_prof_indemnity_no, comp_public_liability_no, comp_cmpny_no, " & _
                    " comp_stage2_founded, comp_stage3_founded FROM Complaints WHERE comp_no = " & comp_no
        Dim comp_ds As DataSet = get_dataset("Complaints", param2)
        If no_of_rows = 0 Then
            MsgBox("There is no complaint number " & comp_no)
            Me.Close()
            Exit Sub
        End If
        Dim comp_cmpny_no As Integer = 0
        Try
            comp_cmpny_no = comp_ds.Tables(0).Rows(0).Item(53)
        Catch ex As Exception
            comp_cmpny_no = 0
        End Try
        If comp_cmpny_no <> cmpny_no And Not super_user Then
            MsgBox("Complaint number " & comp_no & " does not exist for " & cmpny_name)
            Me.Close()
            Exit Sub
        End If

        param2 = "select cmpny_name from Complaint_companies" & _
               " where cmpny_code = " & comp_cmpny_no
        Dim cmpny_ds As DataSet = get_dataset("Complaints", param2)
        cmpny_lbl.Text = cmpny_ds.Tables(0).Rows(0).Item(0)

        recpt_code = comp_ds.Tables(0).Rows(0).Item(0)
        param2 = "select recpt_text from Receipt_type " & _
        " where recpt_code = " & recpt_code
        Dim recpt_ds As DataSet = get_dataset("complaints", param2)
        Try
            Recpt_textTextBox.Text = recpt_ds.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            Recpt_textTextBox.Text = "????!"
        End Try

        Dim ent_code As Integer
        ent_code = comp_ds.Tables(0).Rows(0).Item(1)
        param2 = "select inv_text from Investigators where inv_code = " & ent_code
        Dim inv_ds As DataSet = get_dataset("Complaints", param2)
        Try
            entered_bytextbox.Text = inv_ds.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            entered_bytextbox.Text = "Unknown"
        End Try

        recvd_datetextbox.Text = CDate(comp_ds.Tables(0).Rows(0).Item(2))

        Comp_textTextBox.Text = comp_ds.Tables(0).Rows(0).Item(3)
        Try
            Comp_responseTextBox.Text = comp_ds.Tables(0).Rows(0).Item(4)
        Catch ex As Exception
            Comp_responseTextBox.Text = ""
        End Try

        recvd_from = comp_ds.Tables(0).Rows(0).Item(5)
        param2 = "select recvd_text from Received_from " & _
        " where recvd_from = " & recvd_from
        Dim recvd_ds As DataSet = get_dataset("complaints", param2)
        Try
            Recvd_textTextBox.Text = recvd_ds.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            Recvd_textTextBox.Text = "Unknown"
        End Try

        Comp_case_noTextBox.Text = comp_ds.Tables(0).Rows(0).Item(6)

        cl_no = comp_ds.Tables(0).Rows(0).Item(7)
        populate_client_table(comp_cmpny_no)
        Dim idx As Integer
        cl_name = ""
        For idx = 1 To cl_rows
            If cl_no = client_table(idx, 1) Then
                cl_name = Trim(client_table(idx, 2))
            End If
        Next
        cltextbox.Text = cl_name

        cat_code = comp_ds.Tables(0).Rows(0).Item(8)
        cat_number = comp_ds.Tables(0).Rows(0).Item(9)
        Comp_cat_codeTextBox.Text = cat_code
        param2 = "select cat_text from complaint_categories " & _
        " where cat_code = '" & cat_code & "'" & _
        " and cat_number = " & cat_number
        Dim cat_ds As DataSet = get_dataset("complaints", param2)
        cat_numbertextbox.Text = Format(cat_number, "n") & " " & cat_ds.Tables(0).Rows(0).Item(0)
        
        against_code = comp_ds.Tables(0).Rows(0).Item(10)
        param2 = "select against_text from Complaint_against " & _
        " where against_code = " & against_code
        Dim ag_ds As DataSet = get_dataset("complaints", param2)
        Against_textTextBox.Text = ag_ds.Tables(0).Rows(0).Item(0)

        Try
            against2_code = comp_ds.Tables(0).Rows(0).Item(11)
        Catch ex As Exception
            against2_code = 0
        End Try
        If against2_code > 0 Then
            param2 = "select against_text from Complaint_against " & _
                    " where against_code = " & against2_code
            Dim ag2_ds As DataSet = get_dataset("complaints", param2)
            Against2_textTextBox.Text = ag2_ds.Tables(0).Rows(0).Item(0)
        Else
            Against2_textTextBox.Text = ""
        End If

        alloc_to_code = comp_ds.Tables(0).Rows(0).Item(12)
        param2 = "select inv_text from Investigators where inv_code = " & alloc_to_code
        Dim inv2_ds As DataSet = get_dataset("Complaints", param2)
        Inv_textTextBox.Text = inv2_ds.Tables(0).Rows(0).Item(0)

        action_code = comp_ds.Tables(0).Rows(0).Item(13)
        If action_code = 0 Then
            Action_nameTextBox.Text = ""
        Else
            param2 = "select action_name from Actions " & _
            " where action_code = " & action_code
            Dim act_ds As DataSet = get_dataset("Complaints", param2)
            Action_nameTextBox.Text = act_ds.Tables(0).Rows(0).Item(0)
        End If

        Select Case comp_ds.Tables(0).Rows(0).Item(14)
            Case "U"
                openrbtn.Checked = True
            Case "N"
                unfoundedrbtn.Checked = True
            Case "Y"
                foundedrbtn.Checked = True
            Case "P"
                part_foundedrbtn.Checked = True
        End Select

        stage_no = comp_ds.Tables(0).Rows(0).Item(15)

        cor_code = comp_ds.Tables(0).Rows(0).Item(16)
        If cor_code = 0 Then
            Cor_nameTextBox.Text = "N/A"
        Else
            param2 = " select cor_name from Corrective_actions " & _
            " where cor_code = " & cor_code
            Dim cor_ds As DataSet = get_dataset("complaints", param2)
            Cor_nameTextBox.Text = cor_ds.Tables(0).Rows(0).Item(0)
        End If

        orig_priority = comp_ds.Tables(0).Rows(0).Item(17)
        agent_no = comp_ds.Tables(0).Rows(0).Item(18)
        Try
            agent2_no = comp_ds.Tables(0).Rows(0).Item(19)
        Catch ex As Exception
            agent2_no = 0
        End Try
        Try
            old_comp_no = comp_ds.Tables(0).Rows(0).Item(20)
        Catch ex As Exception
            old_comp_no = 0
        End Try
        Try
            costs_cancel = comp_ds.Tables(0).Rows(0).Item(21)
        Catch ex As Exception
            costs_cancel = 0
        End Try
        Try
            compensation = comp_ds.Tables(0).Rows(0).Item(22)
        Catch ex As Exception
            compensation = 0
        End Try
        Try
            resp_forgotten = comp_ds.Tables(0).Rows(0).Item(23)
        Catch ex As Exception
            resp_forgotten = 0
        End Try
        Try
            stage2_start_date = comp_ds.Tables(0).Rows(0).Item(24)
        Catch ex As Exception
            stage2_start_date = Nothing
        End Try
        Try
            stage2_completed_date = comp_ds.Tables(0).Rows(0).Item(25)
        Catch ex As Exception
            stage2_completed_date = Nothing
        End Try
        Try
            stage2_completed_by = comp_ds.Tables(0).Rows(0).Item(26)
        Catch ex As Exception
            stage2_completed_by = 0
        End Try
        Try
            gender_textbox.Text = comp_ds.Tables(0).Rows(0).Item(27)
        Catch ex As Exception
            gender_textbox.Text = "Unknown"
        End Try
        If gender_textbox.Text = "U" Then
            gender_textbox.Text = "Unknown"
        Else
            If gender_textbox.Text = "N" Then
                gender_textbox.Text = "N/A"
            End If
        End If

        Try
            ethnicity = comp_ds.Tables(0).Rows(0).Item(28)
        Catch ex As Exception
            ethnicity = 0
        End Try
        param2 = "select eth_name from Ethnicity " & _
        " where eth_code = " & ethnicity
        Dim eth_ds As DataSet = get_dataset("Complaints", param2)
        eth_textbox.Text = eth_ds.Tables(0).Rows(0).Item(0)

        Try
            ack_date = comp_ds.Tables(0).Rows(0).Item(29)
        Catch ex As Exception
            ack_date = CDate("1800,1,1")
        End Try
        Try
            holding_date = comp_ds.Tables(0).Rows(0).Item(30)
        Catch ex As Exception
            holding_date = CDate("1800,1,1")
        End Try
        Try
            stage2_ack_date = comp_ds.Tables(0).Rows(0).Item(31)
        Catch ex As Exception
            stage2_ack_date = CDate("1800,1,1")
        End Try
        Try
            stage2_holding_date = comp_ds.Tables(0).Rows(0).Item(32)
        Catch ex As Exception
            stage2_holding_date = CDate("1800,1,1")
        End Try
        Try
            stage3_start_date = comp_ds.Tables(0).Rows(0).Item(33)
        Catch ex As Exception
            stage3_start_date = CDate("1800,1,1")
        End Try
        Try
            stage3_completed_by = comp_ds.Tables(0).Rows(0).Item(34)
        Catch ex As Exception
            stage3_completed_by = 0
        End Try
        Try
            stage3_completed_date = comp_ds.Tables(0).Rows(0).Item(35)
        Catch ex As Exception
            stage3_completed_date = CDate("1800,1,1")
        End Try
        Try
            referred_to_solicitor = comp_ds.Tables(0).Rows(0).Item(36)
        Catch ex As Exception
            referred_to_solicitor = CDate("1800,1,1")
        End Try
        Try
            referred_to_insurer = comp_ds.Tables(0).Rows(0).Item(37)
        Catch ex As Exception
            referred_to_insurer = CDate("1800,1,1")
        End Try
        Try
            monetary_risk = comp_ds.Tables(0).Rows(0).Item(38)
        Catch ex As Exception
            monetary_risk = -999
        End Try
        Try
            legal_flag = comp_ds.Tables(0).Rows(0).Item(39)
        Catch ex As Exception
            legal_flag = "N"
        End Try
        Try
            cancel_costs_reason = comp_ds.Tables(0).Rows(0).Item(40)
        Catch ex As Exception
            cancel_costs_reason = ""
        End Try
        Try
            category = comp_ds.Tables(0).Rows(0).Item(41)
        Catch ex As Exception
            category = 0
        End Try
        Try
            branch_no = comp_ds.Tables(0).Rows(0).Item(42)
        Catch ex As Exception
            branch_no = 0
        End Try
        Try
            insurance_type_no = comp_ds.Tables(0).Rows(0).Item(43)
        Catch ex As Exception
            insurance_type_no = -1
        End Try
        liability_flag = ""
        If insurance_type_no >= 0 Then
            Try
                liability_flag = comp_ds.Tables(0).Rows(0).Item(44)
            Catch ex As Exception
                MsgBox("Check liability on insurance is correct")
            End Try
        End If
        If orig_founded = "P" Then
            Try
                orig_pf_agent_no = comp_ds.Tables(0).Rows(0).Item(45)
            Catch ex As Exception
                orig_pf_agent_no = 0
            End Try
        End If
        feedback_no = 0
        Try
            feedback_no = comp_ds.Tables(0).Rows(0).Item(46)
        Catch ex As Exception

        End Try
        Try
            type_code = comp_ds.Tables(0).Rows(0).Item(47)
        Catch ex As Exception
            type_code = 0
        End Try
        completed = False
        Try
            If comp_ds.Tables(0).Rows(0).Item(48) > 0 Then
                completed = True
                compby_code = comp_ds.Tables(0).Rows(0).Item(48)
                completed_datetextbox.Text = CDate(comp_ds.Tables(0).Rows(0).Item(49))
            End If
        Catch ex As Exception
            compby_code = 0
        End Try
        
        Try
            hold_code = comp_ds.Tables(0).Rows(0).Item(50)
        Catch ex As Exception

        End Try
        Try
            prof_indemnity_no = comp_ds.Tables(0).Rows(0).Item(51)
        Catch ex As Exception
            prof_indemnity_no = 0
        End Try
        Try
            public_liability_no = comp_ds.Tables(0).Rows(0).Item(52)
        Catch ex As Exception
            public_liability_no = 0
        End Try
    End Sub

    Private Sub load_data()
        
        Me.ComplaintsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaints, comp_no)
        Try
            recpt_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(2)
        Catch ex As Exception
            MsgBox("There is no complaint number " & comp_no)
            Me.Close()
            Exit Sub
        End Try
        Dim comp_cmpny_no As Integer = 0
        Try
            comp_cmpny_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(57)
        Catch ex As Exception
            comp_cmpny_no = 0
        End Try

        If comp_cmpny_no <> cmpny_no And Not super_user Then
            MsgBox("This complaint number " & comp_no & " is for a different company")
            Me.Close()
            Exit Sub
        End If
        param2 = "select cmpny_name from Complaint_companies" & _
        " where cmpny_code = " & comp_cmpny_no
        Dim cmpny_ds As DataSet = get_dataset("Complaints", param2)
        cmpny_lbl.Text = cmpny_ds.Tables(0).Rows(0).Item(0)

        recvd_datetextbox.Text = CDate(PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(1))
        Me.Receipt_typeTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Receipt_type, recpt_code)
        Try
            Recpt_textTextBox.Text = PraiseAndComplaintsSQLDataSet.Receipt_type.Rows(0).Item(1)
        Catch ex As Exception
            Recpt_textTextBox.Text = "????!"
        End Try

        recvd_from = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(3)
        Me.Received_fromTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Received_from, recvd_from)
        Recvd_textTextBox.Text = PraiseAndComplaintsSQLDataSet.Received_from.Rows(0).Item(1)
        Comp_case_noTextBox.Text = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(4)
        against_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(6)

        Me.Complaint_againstTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaint_against, against_code)
        Against_textTextBox.Text = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(0).Item(1)
        Try
            against2_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(41)
            Me.Complaint_againstTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaint_against, against2_code)
            Against2_textTextBox.Text = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(0).Item(1)
        Catch ex As Exception
            against2_code = 0
            Against2_textTextBox.Text = ""
        End Try
        
        cl_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(5)
        populate_client_table(comp_cmpny_no)
        Dim idx As Integer
        cl_name = ""
        For idx = 1 To cl_rows
            If cl_no = client_table(idx, 1) Then
                cl_name = Trim(client_table(idx, 2))
            End If
        Next
        cltextbox.Text = cl_name
        'entered by
        Dim ent_code As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(11)
        Me.InvestigatorsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Investigators, ent_code)

        entered_bytextbox.Text = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)

        'category description
        cat_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(8)
        Comp_cat_codeTextBox.Text = cat_code
        cat_number = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(9)
        updatecmpfrm.Complaint_categoriesTableAdapter.Fill3(cat_table, cat_code, cat_number)
        cat_numbertextbox.Text = cat_table.Rows(0).Item(1) & " " & cat_table.Rows(0).Item(2)

        Try
            gender_textbox.Text = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(32)
        Catch ex As Exception
            gender_textbox.Text = "Unknown"
        End Try
        If gender_textbox.Text = "U" Then
            gender_textbox.Text = "Unknown"
        Else
            If gender_textbox.Text = "N" Then
                gender_textbox.Text = "N/A"
            End If
        End If
        Try
            ethnicity = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(33)
        Catch ex As Exception
            ethnicity = 0
        End Try
        Try
            ack_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(34)
        Catch ex As Exception
            ack_date = CDate("1800,1,1")
        End Try
        Try
            holding_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(35)
        Catch ex As Exception
            holding_date = CDate("1800,1,1")
        End Try
        Try
            stage2_ack_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(36)
        Catch ex As Exception
            stage2_ack_date = CDate("1800,1,1")
        End Try
        Try
            stage2_holding_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(37)
        Catch ex As Exception
            stage2_holding_date = CDate("1800,1,1")
        End Try
        Try
            stage3_start_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(38)
        Catch ex As Exception
            stage3_start_date = CDate("1800,1,1")
        End Try
        Try
            stage3_completed_by = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(39)
        Catch ex As Exception
            stage3_completed_by = 0
        End Try
        Try
            stage3_completed_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(40)
        Catch ex As Exception
            stage3_completed_date = CDate("1800,1,1")
        End Try
        Try
            referred_to_solicitor = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(43)
        Catch ex As Exception
            referred_to_solicitor = CDate("1800,1,1")
        End Try
        Try
            referred_to_insurer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(44)
        Catch ex As Exception
            referred_to_insurer = CDate("1800,1,1")
        End Try
        Try
            monetary_risk = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(45)
        Catch ex As Exception
            monetary_risk = -999
        End Try
        Try
            legal_flag = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(46)
        Catch ex As Exception
            legal_flag = "N"
        End Try
        Try
            cancel_costs_reason = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(47)
        Catch ex As Exception
            cancel_costs_reason = ""
        End Try
        Try
            category = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(48)
        Catch ex As Exception
            category = 0
        End Try
        Select Case category
            Case 0
                category_tbox.Text = ""
            Case 1
                category_tbox.Text = "Fast Track"
            Case 2
                category_tbox.Text = "Normal"
            Case 3
                category_tbox.Text = "Special Handling"
        End Select

        branch_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(49)
        add_compfrm.Complaint_branchesTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaint_branches, branch_no)
        branch_tbox.Text = Me.PraiseAndComplaintsSQLDataSet.Complaint_branches.Rows(0).Item(1)

        Try
            prof_indemnity_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(55)
        Catch ex As Exception
            prof_indemnity_no = 0
        End Try

        Try
            public_liability_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(56)
        Catch ex As Exception
            public_liability_no = 0
        End Try

        add_compfrm.EthnicityTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Ethnicity)
        For idx = 0 To Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows.Count - 1
            Dim eth_code As Integer = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(0)
            Dim eth_desc As String = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(1)
            If ethnicity = eth_code Then
                eth_textbox.Text = eth_desc
                Exit For
            End If
        Next

        agent_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(7)
        agent_TextBox.Text = ""
        If agent_no = 0 Then
            agent_TextBox.Text = ""
        Else

            'get agent from bailiff table
            If agent_no = 9999 Then
                agent_TextBox.Text = "Various"
            Else
                If comp_cmpny_no = 0 Then
                    param1 = "onestep"
                    param2 = "select name_fore, name_sur, status from Bailiff" & _
                    " where _rowid = " & agent_no
                    Dim bail_dataset As DataSet = get_dataset(param1, param2)
                    If no_of_rows = 1 Then
                        Dim forename As String
                        Try
                            forename = Trim(bail_dataset.Tables(0).Rows(0).Item(0))
                        Catch ex As Exception
                            forename = ""
                        End Try
                        If forename.Length = 0 Then
                            agent_TextBox.Text = Trim(bail_dataset.Tables(0).Rows(0).Item(1))
                        Else
                            agent_TextBox.Text = Trim(bail_dataset.Tables(0).Rows(0).Item(1)) & ", " & _
                            forename
                        End If
                    Else
                        agent_TextBox.Text = ""
                    End If
                    If bail_dataset.Tables(0).Rows(0).Item(2) = "C" Then
                        agent_TextBox.Text = agent_TextBox.Text & " (C)"
                    End If
                End If

            End If
        End If
        Try
            agent2_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(42)
        Catch ex As Exception
            agent2_no = 0
        End Try
        agent2_TextBox.Text = ""
        If agent2_no = 0 Then
            agent2_TextBox.Text = ""
        Else
            'get agent from bailiff table
            If agent2_no = 9999 Then
                agent2_TextBox.Text = "Various"
            Else
                If comp_cmpny_no = 0 Then
                    param1 = "onestep"
                    param2 = "select name_fore, name_sur, status from Bailiff" & _
                    " where _rowid = " & agent2_no
                    Dim bail_dataset As DataSet = get_dataset(param1, param2)
                    If no_of_rows = 1 Then
                        Dim forename As String
                        Try
                            forename = Trim(bail_dataset.Tables(0).Rows(0).Item(0))
                        Catch ex As Exception
                            forename = ""
                        End Try
                        If forename.Length = 0 Then
                            agent2_TextBox.Text = Trim(bail_dataset.Tables(0).Rows(0).Item(1))
                        Else
                            agent2_TextBox.Text = Trim(bail_dataset.Tables(0).Rows(0).Item(1)) & ", " & _
                            forename
                        End If
                    Else
                        agent2_TextBox.Text = ""
                    End If
                    If bail_dataset.Tables(0).Rows(0).Item(2) = "C" Then
                        agent2_TextBox.Text = agent2_TextBox.Text & " (C)"
                    End If
                End If
                
            End If
        End If

        inv_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(12)
        Me.InvestigatorsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Investigators, inv_code)
        Inv_textTextBox.Text = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)

        'completed/not completed
        If PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(15) = 0 Then
            notcomprbtn.Checked = True
            completed_datetextbox.Visible = False
            compbyTextBox.Visible = False
            completedbylbl.Visible = False
            completeddatelbl.Visible = False
        Else
            comprbtn.Checked = True
            completed_datetextbox.Visible = True
            completedbylbl.Visible = True
            completeddatelbl.Visible = True
            completed_datetextbox.Text = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(14)
            compbyTextBox.Visible = True
            Dim compby_code As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(15)
            Me.InvestigatorsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Investigators, compby_code)
            compbyTextBox.Text = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
        End If

        'founded/unfounded
        Select Case PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(18)
            Case "U"
                openrbtn.Checked = True
            Case "N"
                unfoundedrbtn.Checked = True
            Case "Y"
                foundedrbtn.Checked = True
            Case "P"
                part_foundedrbtn.Checked = True
        End Select

        'checked/not checked
        'If ComplaintsDataSet.Tables(0).Rows(0).Item(17) = 0 Then
        '    Comp_checked_dateDateTimePicker.Visible = False
        '    checkedlbl.Visible = False
        '    checkedbylbl.Visible = False
        '    checkedbytextbox.Visible = False
        '    notchkdrbtn.Checked = True
        'Else
        '    Comp_checked_dateDateTimePicker.Visible = True
        '    checkedlbl.Visible = True
        '    checkedbylbl.Visible = True
        '    checkedbytextbox.Visible = True
        '    checkedrbtn.Checked = True
        '    Dim checked_by_code As Integer = ComplaintsDataSet.Tables(0).Rows(0).Item(17)
        '    Me.InvestigatorsTableAdapter.FillBy(Me.InvestigatorsDataSet.Investigators, checked_by_code)
        '    checkedbytextbox.Text = InvestigatorsDataSet.Tables(0).Rows(0).Item(1)
        'End If

        'action
        action_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(20)
        Me.ActionsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Actions, action_code)
        Action_nameTextBox.Text = PraiseAndComplaintsSQLDataSet.Actions.Rows(0).Item(1)

        If PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(15) = 0 Then
            days_TextBox.Visible = False
            days_Label.Visible = False
        Else
            days_TextBox.Visible = True
            days_Label.Visible = True
            days_TextBox.Text = DateDiff("d", PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(1), _
                     PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(14)) - _
            (DateDiff("w", PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(1), _
                     PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(14)) * 2)
            If Weekday(PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(1)) > _
                Weekday(PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(14)) Then
                days_TextBox.Text = days_TextBox.Text - 2
            End If
        End If
        doc_textbox.Text = populate_document_combobox_disp()
        If doc_textbox.Text = 0 Then
            doc_ListBox.Visible = False
        Else
            doc_ListBox.Visible = True
        End If
        Try
            stage2_start_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(29)
        Catch ex As Exception
            stage2_start_date = CDate("1800,1,1")
        End Try
        Try
            stage2_completed_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(30)
        Catch ex As Exception
            stage2_completed_date = CDate("1800,1,1")
        End Try
        Try
            stage2_completed_by = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(31)
        Catch ex As Exception
            stage2_completed_by = 0
        End Try
        'check if stage 1 acknowledgement letter has been sent
        ack_letter_sent.Text = ""
        If ack_date <> CDate("1800,1,1") Then
            ack_letter_sent.Text = Format(ack_date, "dd/MM/yyyy")
        End If

        case_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(4)
        Dim dob_str As String = "N/A"
        debt_typelbl.Text = ""
        If case_no > 0 Then
            If ack_date = CDate("1800,1,1") Then
                If comp_cmpny_no = 0 Then
                    param1 = "onestep"
                    param2 = "select text, _createdDate from Note" & _
                    " where debtorID = " & case_no & " and type = 'Letter'" & _
                    " order by _createdDate"
                    Dim note_dataset As DataSet = get_dataset(param1, param2)
                    For idx = 0 To no_of_rows - 1
                        Dim created_date As Date = note_dataset.Tables(0).Rows(idx).Item(1)
                        If InStr(note_dataset.Tables(0).Rows(idx).Item(0), "Complaint_Ack") > 0 And _
                         created_date >= CDate(recvd_datetextbox.Text) And _
                        InStr(note_dataset.Tables(0).Rows(idx).Item(0), "Stage 2") = 0 Then
                            ack_letter_sent.Text = FormatDateTime(note_dataset.Tables(0).Rows(idx).Item(1), DateFormat.ShortDate)
                            Exit For
                        End If
                    Next
                End If
            End If
            'also get dob and csid and client-ref
            clref_lbl.Text = ""
            If comp_cmpny_no = 0 Then
                param1 = "onestep"
                param2 = "select dateOfBirth, clientschemeID, client_ref from Debtor where _rowid = " & case_no
                Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 1 Then
                    Dim dob As Date
                    Try
                        dob = debtor_dataset.Tables(0).Rows(0).Item(0)
                    Catch ex As Exception
                        dob_str = "unknown"
                    End Try
                    If dob_str <> "unknown" Then
                        dob_str = Format(dob, "dd/MM/yyyy")
                    End If
                    clref_lbl.Text = "Client ref:" & debtor_dataset.Tables(0).Rows(0).Item(2)
                    'get debt type
                    Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(1)
                    param2 = "select schemeID from ClientScheme where _rowid = " & csid
                    Dim csid_dataset As DataSet = get_dataset(param1, param2)
                    Dim sch_id As Integer = csid_dataset.Tables(0).Rows(0).Item(0)
                    param2 = "select name from Scheme where _rowid = " & sch_id
                    Dim sch_dataset As DataSet = get_dataset(param1, param2)
                    debt_typelbl.Text = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                End If
            End If
            
        End If
        dob_textbox.Text = dob_str

        'check if stage 1 holding letter has been sent
        stage_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(21)
        hold_letter_sent.Text = ""
        If holding_date <> CDate("1800,1,1") Then
            hold_letter_sent.Text = Format(holding_date, "dd/MM/yyyy")
        Else
            If case_no > 0 Then
                If check_held_letter(case_no, recvd_datetextbox.Text) = "Y" Then
                    If stage_no < 2 Then
                        hold_letter_sent.Text = FormatDateTime(parm_hold_date, DateFormat.ShortDate)
                    ElseIf stage2_start_date <> CDate("1800,1,1") Then
                        If parm_hold_date < stage2_start_date Then
                            hold_letter_sent.Text = FormatDateTime(parm_hold_date, DateFormat.ShortDate)
                        End If
                    End If
                End If
            End If
        End If

        'get stage1 escalation reason
        'complaint escalation
        updatecmpfrm.Complaint_EscalationTableAdapter.FillBy(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaint_Escalation, comp_no, 1)
        If updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaint_Escalation.Rows.Count > 0 Then
            stage1_esc_code = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaint_Escalation.Rows(0).Item(2)
        Else
            stage1_esc_code = 0
        End If

        updatecmpfrm.Complaint_EscalationTableAdapter.FillBy(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaint_Escalation, comp_no, 2)
        If updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaint_Escalation.Rows.Count > 0 Then
            stage2_esc_code = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaint_Escalation.Rows(0).Item(2)
        Else
            stage2_esc_code = 0
        End If

        'fill stage 1 and stage2  escalation combobox with values from table
        stage1_esc_tbox.Text = ""
        stage2_esc_tbox.Text = ""
        For idx = 0 To updatecmpfrm.PraiseAndComplaintsSQLDataSet.Stage_Escalation.Rows.Count - 1
            If stage1_esc_code = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Stage_Escalation.Rows(idx).Item(0) Then
                stage1_esc_tbox.Text = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Stage_Escalation.Rows(idx).Item(1)
            End If
            If stage2_esc_code = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Stage_Escalation.Rows(idx).Item(0) Then
                stage2_esc_tbox.Text = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Stage_Escalation.Rows(idx).Item(1)
            End If
        Next

        'check if stage 2 acknowledgement letter has been sent
        stage2_ack_letter_sent.Text = ""
        If stage2_ack_date <> CDate("1800,1,1") Then
            stage2_ack_letter_sent.Text = Format(stage2_ack_date, "dd/MM/yyyy")
        Else
            If case_no > 0 Then
                If comp_cmpny_no = 0 Then
                    param1 = "onestep"
                    param2 = "select text, _createdDate from Note" & _
                    " where debtorID = " & case_no & " and type = 'Letter'" & _
                    " order by _createdDate"

                    Dim compare_date As Date
                    If stage2_start_date <> CDate("1800,1,1") Then
                        compare_date = stage2_start_date
                    Else
                        compare_date = CDate(recvd_datetextbox.Text)
                    End If
                    Dim note_dataset As DataSet = get_dataset(param1, param2)
                    For idx = 0 To no_of_rows - 1
                        Dim created_date As Date = CDate(note_dataset.Tables(0).Rows(idx).Item(1))
                        If InStr(note_dataset.Tables(0).Rows(idx).Item(0), "Complaint_Ack") > 0 And _
                        created_date >= compare_date And _
                        InStr(note_dataset.Tables(0).Rows(idx).Item(0), "Stage 2") > 0 Then
                            stage2_ack_letter_sent.Text = FormatDateTime(note_dataset.Tables(0).Rows(idx).Item(1), DateFormat.ShortDate)
                        End If
                    Next
                End If
                
            End If
        End If

        'check if stage 2 holding letter has been sent
        stage2_hold_letter_sent.Text = ""
        If stage2_holding_date <> CDate("1800,1,1") Then
            stage2_hold_letter_sent.Text = Format(stage2_holding_date, "dd/MM/yyyy")
        Else
            If case_no > 0 And stage2_start_date <> CDate("1800,1,1") Then
                If check_held_letter(case_no, stage2_start_date) = "Y" Then
                    stage2_hold_letter_sent.Text = FormatDateTime(parm_hold_date, DateFormat.ShortDate)
                End If
            End If
        End If
        'stage
        stage2_gbox.Visible = False
        stage2_founded_gbox.Visible = False
        stage2_esc_gbox.Visible = False
        stage2_letter_gbox.visible = False

        stage3_gbox.Visible = False
        stage3_founded_gbox.Visible = False
        stage_TextBox.Text = "Stage " & stage_no

        If stage_no > 1 Then
            'founded/unfounded
            Dim stage2_founded As String = "U"
            Try
                stage2_founded = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(58)
            Catch ex As Exception

            End Try
            Select Case stage2_founded
                Case "U"
                    stage2_openrbtn.Checked = True
                Case "N"
                    stage2_unfoundedrbtn.Checked = True
                Case "Y"
                    stage2_foundedrbtn.Checked = True
                Case "P"
                    stage2_part_foundedrbtn.Checked = True
            End Select
            stage2_gbox.Visible = True
            stage2_founded_gbox.Visible = True
            stage2_esc_gbox.Visible = True
            stage2_letter_gbox.Visible = True
            stage2_start_datepicker.Visible = False
            stage2_completed_datepicker.Visible = False
            stage2_completed_by_textbox.Text = ""
            If stage2_start_date <> CDate("1800,1,1") Then
                stage2_start_datepicker.Visible = True
                stage2_start_datepicker.Text = stage2_start_date
                If stage2_completed_by > 0 Then
                    Me.InvestigatorsTableAdapter.FillBy(PraiseAndComplaintsSQLDataSet.Investigators, stage2_completed_by)
                    stage2_completed_by_textbox.Text = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
                    stage2_completed_datepicker.Text = stage2_completed_date
                    stage2_completed_datepicker.Visible = True
                End If
            End If
        End If
        If stage_no = 3 Then
            Dim stage3_founded As String = "U"
            Try
                stage3_founded = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(59)
            Catch ex As Exception

            End Try
            Select Case stage3_founded
                Case "U"
                    stage3_openrbtn.Checked = True
                Case "N"
                    stage3_unfoundedrbtn.Checked = True
                Case "Y"
                    Stage3_foundedrbtn.Checked = True
                Case "P"
                    stage3_part_foundedrbtn.Checked = True
            End Select
            stage3_gbox.Visible = True
            stage3_founded_gbox.Visible = True
            stage3_start_datepicker.Visible = False
            stage3_completed_datepicker.Visible = False
            stage3_completed_by_textbox.Text = ""
            If stage3_start_date <> CDate("1800,1,1") Then
                stage3_start_datepicker.Visible = True
                stage3_start_datepicker.Text = stage3_start_date
                If stage3_completed_by > 0 Then
                    Me.InvestigatorsTableAdapter.FillBy(PraiseAndComplaintsSQLDataSet.Investigators, stage3_completed_by)
                    stage3_completed_by_textbox.Text = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
                    stage3_completed_datepicker.Text = stage3_completed_date
                    stage3_completed_datepicker.Visible = True
                End If
            End If
        End If
        'corrective action
        cor_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(22)
        If cor_code = 0 Then
            Cor_nameTextBox.Text = "N/A"
        Else
            Me.Corrective_actionsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Corrective_actions, cor_code)
            Cor_nameTextBox.Text = PraiseAndComplaintsSQLDataSet.Corrective_actions.Rows(0).Item(1)
        End If

        'hold reason
        hold_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(23)
        If hold_code = 0 Then
            Hold_name_TextBox.Text = "N/A"
        Else
            Hold_reasonTableAdapter.FillBy(PraiseAndComplaintsSQLDataSet.Hold_reason, hold_code)
            Hold_name_TextBox.Text = PraiseAndComplaintsSQLDataSet.Hold_reason.Rows(0).Item(1)
        End If

        'priority
        If PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(24) Then
            prioritytbox.Text = "PRIORITY"
        Else
            prioritytbox.Text = "not a priority"
        End If

        'text boxes
        Comp_textTextBox.Text = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(13)
        Try
            Comp_responseTextBox.Text = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(19)
        Catch
            Comp_responseTextBox.Text = ""
        End Try

        'old comp no
        Try
            old_comp_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(25)
        Catch ex As Exception
            old_comp_no = 0
        End Try

        If old_comp_no = 0 Then
            old_comp_noTextBox.Text = ""
        Else
            old_comp_noTextBox.Text = old_comp_no
        End If

        'costs cancelled
        Try
            costs_cancel = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(26)
        Catch ex As Exception
            costs_cancel = 0
        End Try
        If costs_cancel = 0 Then
            costs_cancel_tbox.Text = " "
            cancel_reasonbtn.Visible = False
        Else
            costs_cancel_tbox.Text = Format(costs_cancel, "�#0.00")
            cancel_reasonbtn.Visible = True
        End If

        'compensation
        Try
            compensation = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(27)
        Catch ex As Exception
            compensation = 0
        End Try
        If compensation = 0 Then
            compensation_tbox.Text = " "
        Else
            compensation_tbox.Text = Format(compensation, "�#0.00")
        End If

        'legal 
        orig_legal_rbtn = False
        legal_rbtn.Checked = False
        solicitor_datepicker.Visible = False
        monetary_risktbox.Text = ""
        monetary_risktbox.Visible = False

        If legal_flag = "Y" Then
            orig_legal_rbtn = True
            legal_rbtn.Checked = True
            If referred_to_solicitor = CDate("1800,1,1") Then
                solicitor_datepicker.Visible = False
            Else
                solicitor_datepicker.Visible = True
                solicitor_datepicker.Value = referred_to_solicitor
            End If
        End If

        'insurance
        ins_datepicker.Visible = False
        orig_ins_rbtn = False
        ins_rbtn.Checked = False
        orig_liability = False
        liability_rbtn.Checked = False
        act_not_tbox.Text = ""
        pi_cbox.Visible = False
        pl_cbox.Visible = False
        
        Try
            insurance_type_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(50)
        Catch ex As Exception
            insurance_type_no = -1
        End Try
        If insurance_type_no >= 0 Then
            pi_cbox.Visible = True
            pl_cbox.Visible = True
            orig_ins_rbtn = True
            ins_rbtn.Checked = True
            If referred_to_insurer = CDate("1800,1,1") Then
                ins_datepicker.Visible = False
            Else
                ins_datepicker.Visible = True
                ins_datepicker.Value = referred_to_insurer
            End If
            liability_rbtn.Checked = False
            If liability_flag = "Y" Then
                orig_liability = True
                liability_rbtn.Checked = True
            End If
            Select Case insurance_type_no
                Case 0
                    act_not_tbox.Text = "unknown"
                Case 1
                    act_not_tbox.Text = "Actual Notification"
                Case 2
                    act_not_tbox.Text = "Bordereau"
            End Select
            If prof_indemnity_no = 1 Then
                orig_pi_cbox = True
                pi_cbox.Checked = True
            Else
                orig_pi_cbox = False
                pi_cbox.Checked = False
            End If
            If public_liability_no = 1 Then
                orig_pl_cbox = True
                pl_cbox.Checked = True
            Else
                orig_pl_cbox = False
                pl_cbox.Checked = False
            End If
        End If


        If legal_rbtn.Checked Or ins_rbtn.Checked Then
            monetary_risktbox.Visible = True
            If monetary_risk >= 0 Then
                monetary_risktbox.Text = Format(monetary_risk, "�#0.00")
            End If
        End If

        'feedback
        Try
            feedback_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(53)
        Catch ex As Exception
            feedback_no = 0
        End Try
        updatecmpfrm.Complaints_feedbackTableAdapter.FillBy(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_feedback, feedback_no)
        feedback_tbox.Text = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_feedback.Rows(0).Item(1)

        'type code
        Try
            type_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(54)
        Catch ex As Exception
            type_code = 0
        End Try
        type_code_tbox.Text = ""
        If type_code > 0 Then
            updatecmpfrm.Complaints_type_codeTableAdapter.FillBy(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_type_code, type_code)
            type_code_tbox.Text = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_type_code.Rows(0).Item(1)
        End If
        'set hold label
        hold_lbl.Text = ""
        updatecmpfrm.Complaints_HoldsTableAdapter.FillBy2(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds, comp_no)
        If updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows.Count > 0 Then
            'get stage hold still open
            Dim hold_stage_no As Integer = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows(0).Item(1)
            hold_lbl.Text = "Stage " & hold_stage_no & " ON HOLD"
        End If

    End Sub


    Private Sub Comp_noTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Comp_noTextBox.Validating
        ErrorProvider1.SetError(Comp_noTextBox, "")
        Try
            comp_no = Comp_noTextBox.Text
        Catch ex As Exception
            MsgBox("Complaint no should be numeric")
            e.Cancel = True
        End Try
    End Sub

    Private Sub dispbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dispbtn.Click
        If env_str = "Prod" And comp_no <= 160 Then
            MsgBox("First complaint in Production is 161")
            Me.Close()
            Exit Sub
        End If
        load_data()
        Comp_noTextBox.SelectAll()
    End Sub

    Private Sub updbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updbtn.Click
        updatecmpfrm.ShowDialog()
        Me.Close()
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub doc_ListBox_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles doc_ListBox.DoubleClick
        file_path = "r:\complaints\" & comp_no & "\" & doc_ListBox.Text
        If Microsoft.VisualBasic.Right(doc_ListBox.Text, 3) = "tif" Then
            tiffrm.ShowDialog()
        Else
            Dim strWordpadFilename As String = "C:\program files\windows nt\accessories\wordpad " & """" & Trim(file_path) & """"
            ' start up wordpad & display the current file being imported
            Dim RetVal As Integer = Shell(strWordpadFilename, 1)    ' Run wordpad.
        End If
    End Sub

    Private Sub doc_ListBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles doc_ListBox.SelectedIndexChanged

    End Sub

    Private Sub hold_letter_sent_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hold_letter_sent.TextChanged

    End Sub

    Private Sub GroupBox3_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox3.Enter

    End Sub

    Private Sub cancel_reasonbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancel_reasonbtn.Click
        costs_reason_mode = "D"
        costs_reasonfrm.ShowDialog()
    End Sub

    Private Sub agent_TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agent_TextBox.TextChanged

    End Sub

    Private Sub legalgbox_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub ins_rbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ins_rbtn.CheckedChanged
        ins_rbtn.Checked = orig_ins_rbtn
    End Sub

    Private Sub liability_rbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles liability_rbtn.CheckedChanged
        liability_rbtn.Checked = orig_liability
    End Sub

    Private Sub stage1_esc_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage1_esc_tbox.TextChanged

    End Sub

    Private Sub TabPage5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabPage5.Click

    End Sub

    Private Sub pl_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pl_cbox.CheckedChanged
        pl_cbox.Checked = orig_pl_cbox
    End Sub

    Private Sub pi_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pi_cbox.CheckedChanged
        pi_cbox.Checked = orig_pi_cbox
    End Sub

    Private Sub legal_rbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles legal_rbtn.CheckedChanged
        legal_rbtn.Checked = orig_legal_rbtn
    End Sub

    Private Sub hold_lbl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hold_lbl.Click

    End Sub
End Class
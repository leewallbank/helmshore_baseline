Public Class branchfrm

    Private Sub branchfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.Complaint_branchesTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_branches)
        param2 = "select branch_code, branch_name from Complaint_branches order by branch_code"
        Dim branch_ds As DataSet = get_dataset("Complaints", param2)
        Dim branch_rows As Integer = no_of_rows - 1
        Dim branch_idx As Integer
        For branch_idx = 0 To branch_rows
            branch_cbox.Items.Add(branch_ds.Tables(0).Rows(branch_idx).Item(1))
        Next
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub
End Class
Public Class actionsfrm
    Dim entered_action_name As String
    Dim entered_action_code As Integer

    Private Sub actionsfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        actions_dg.EndEdit()
    End Sub
    Private Sub actionsfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.ActionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Actions)
        param2 = "select action_code, action_name from Actions " & _
        " order by action_code"
        Dim actions_ds As DataSet = get_dataset("complaints", param2)
        actions_dg.Rows.Clear()
        For Each row In actions_ds.Tables(0).Rows
            actions_dg.Rows.Add(row(0), row(1))
        Next
    End Sub

    Private Sub actions_dg_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles actions_dg.CellValueChanged
        If e.ColumnIndex = 1 And e.RowIndex >= 0 Then

            Dim action_code As Integer = actions_dg.Rows(e.RowIndex).Cells(0).Value
            Dim action_name As String = actions_dg.Rows(e.RowIndex).Cells(1).Value
            If actions_dg.Rows(e.RowIndex).IsNewRow Then Return
            Try
                If action_code = 0 Then  'new action
                    If action_name.Length = 0 Then
                        Return
                    End If
                    upd_txt = "insert into Actions (action_name) values('" & action_name & "')"
                    update_sql(upd_txt)
                    log_text = "Action inserted - " & entered_action_name
                    log_type = "master"
                    add_log(log_type, log_text)
                Else
                    'amendment
                    'Me.ActionsTableAdapter.UpdateQuery(action_name, action_code)
                    upd_txt = "update Actions set action_name = '" & action_name & "'" & _
                    " where action_code = " & action_code
                    update_sql(upd_txt)
                    log_text = "Action amended - " & action_code & _
                                                      " from " & entered_action_name & " to " & action_name
                    'updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
                    log_type = "master"
                    add_log(log_type, log_text)
                End If
               
            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate action entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub actions_dg_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles actions_dg.Leave

    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles actions_dg.RowEnter

        entered_action_code = actions_dg.Rows(e.RowIndex).Cells(0).Value
        entered_action_name = actions_dg.Rows(e.RowIndex).Cells(1).Value

    End Sub

    Private Sub actions_dg_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles actions_dg.RowsRemoved
        If e.RowIndex > 0 Then
            Try
                Dim delete_OK As Boolean = True
                'Me.ActionsTableAdapter.DeleteQuery(action_code)
                'don't allow deletion if used in a complaint or = 'N/A'
                If entered_action_name = "N/A" Then
                    MsgBox("Can't delete N\A")
                    delete_OK = False
                Else
                    param2 = "select count(*) from Complaints where comp_action = " & entered_action_code
                    Dim action_ds As DataSet = get_dataset("complaints", param2)
                    If action_ds.Tables(0).Rows(0).Item(0) > 0 Then
                        MessageBox.Show("Can't delete " & entered_action_name & " as it's used in a complaint")
                        delete_OK = False
                    End If
                End If
                If delete_OK Then
                    upd_txt = "delete from Actions " & _
                             " where action_code = " & entered_action_code
                    update_sql(upd_txt)
                    log_text = "Action deleted - " & entered_action_code & " name " & entered_action_name
                    'updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
                    log_type = "master"
                    add_log(log_type, log_text)
                Else
                    Me.Close()
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
End Class
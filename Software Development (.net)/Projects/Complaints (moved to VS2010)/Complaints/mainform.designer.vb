<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(mainfrm))
        Me.addbtn = New System.Windows.Forms.Button
        Me.updbtn = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.updmastbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.logbtn = New System.Windows.Forms.Button
        Me.nextbtn = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'addbtn
        '
        Me.addbtn.Location = New System.Drawing.Point(52, 21)
        Me.addbtn.Name = "addbtn"
        Me.addbtn.Size = New System.Drawing.Size(118, 23)
        Me.addbtn.TabIndex = 0
        Me.addbtn.Text = "&Add a complaint"
        Me.addbtn.UseVisualStyleBackColor = True
        '
        'updbtn
        '
        Me.updbtn.Location = New System.Drawing.Point(52, 64)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(118, 23)
        Me.updbtn.TabIndex = 1
        Me.updbtn.Text = "&Update complaint"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(52, 160)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(118, 23)
        Me.Button3.TabIndex = 3
        Me.Button3.Text = "&Display complaints"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'updmastbtn
        '
        Me.updmastbtn.Location = New System.Drawing.Point(52, 210)
        Me.updmastbtn.Name = "updmastbtn"
        Me.updmastbtn.Size = New System.Drawing.Size(118, 23)
        Me.updmastbtn.TabIndex = 5
        Me.updmastbtn.Text = "Update &Master Table"
        Me.updmastbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(207, 314)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 8
        Me.exitbtn.Text = "&Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 314)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 7
        Me.ProgressBar1.Visible = False
        '
        'logbtn
        '
        Me.logbtn.Location = New System.Drawing.Point(52, 259)
        Me.logbtn.Name = "logbtn"
        Me.logbtn.Size = New System.Drawing.Size(118, 23)
        Me.logbtn.TabIndex = 6
        Me.logbtn.Text = "&Complaints Audit"
        Me.logbtn.UseVisualStyleBackColor = True
        '
        'nextbtn
        '
        Me.nextbtn.Location = New System.Drawing.Point(52, 111)
        Me.nextbtn.Name = "nextbtn"
        Me.nextbtn.Size = New System.Drawing.Size(139, 23)
        Me.nextbtn.TabIndex = 2
        Me.nextbtn.Text = "&Next Complaint to allocate"
        Me.nextbtn.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(207, 160)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 49)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Update SQL"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(302, 404)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.nextbtn)
        Me.Controls.Add(Me.logbtn)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.updmastbtn)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.updbtn)
        Me.Controls.Add(Me.addbtn)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Complaints Main Menu"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents addbtn As System.Windows.Forms.Button
    Friend WithEvents updbtn As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents updmastbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents logbtn As System.Windows.Forms.Button
    Friend WithEvents nextbtn As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button

End Class

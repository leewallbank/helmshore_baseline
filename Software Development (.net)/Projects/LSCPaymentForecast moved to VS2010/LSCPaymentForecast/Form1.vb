Public Class Form1

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
    End Sub
    Private Sub load_table()
        Dim start_date, end_date As Date
        Dim wkday As Integer = Weekday(Now)
        start_date = DateAdd(DateInterval.Day, 2 - wkday, Now)
        start_date = CDate(Format(start_date, "yyyy-MM-dd") & " 00:00:00")
        end_date = DateAdd(DateInterval.Day, 6, start_date)

        'get all arrangement cases for ccmt csid = 1892 where arrangeNext date is in week
        param2 = "select _rowid, arrange_initial_amount, arrange_initial_due, " & _
        " arrange_started, arrange_amount, arrange_interval, debt_balance " & _
        " from Debtor where clientSchemeID = 1892" & _
        " and status = 'A' and status_open_closed = 'O'" & _
        " and arrange_next >= '" & Format(start_date, "yyyy-MM-dd") & "'" & _
        " and  arrange_next <= '" & Format(end_date, "yyyy-MM-dd") & "'"
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            'MsgBox("No debtors found on ccmt")
            Exit Sub
        End If
        Dim debtor_rows As Integer = no_of_rows
        Dim idx As Integer
        For idx = 0 To debtor_rows - 1
            ProgressBar1.Value = (idx / debtor_rows) * 100
            Dim debtorID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(0)

            Dim arr_amt As Decimal = debtor_dataset.Tables(0).Rows(idx).Item(4)
            Dim debt_bal As Decimal = debtor_dataset.Tables(0).Rows(idx).Item(6)
            If arr_amt > debt_bal Then
                arr_amt = debt_bal
            End If
            Try
                Me.LSC_Payment_ForecastTableAdapter.InsertQuery(debtorID, Now, arr_amt)
            Catch ex As Exception
                Try
                    Me.LSC_Payment_ForecastTableAdapter.UpdateQuery(Now, arr_amt, debtorID)
                Catch ex2 As Exception
                    'MsgBox("Unable to store record for " & debtorID)
                    Exit Sub
                End Try
            End Try
        Next
        'MsgBox("report completed")
        Me.Close()
    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        load_table()
    End Sub
End Class

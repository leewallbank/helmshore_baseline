Public Class mainform
    Public filename, outfile, errorfile As String
    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline, caption As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim name As String
        Dim idx As Integer
        Dim lines As Integer = 0
        

        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next


        'write out headings
        outline = "Acc Ref|Case No|Curr Bal|name" & vbNewLine
        outfile = outline

        'look for reference no
        Dim case_no As String = ""
        Dim cl_ref As String = ""
        Dim cl_bal As String = ""
        Dim idx2 As Integer
        Dim num_ref As Decimal
        For idx = 1 To lines - 1
            caption = Mid(line(idx), 3, 3)
            If caption = "Acc" Then
                cl_bal = ""
                case_no = ""
                cl_ref = ""
                name = ""
                For idx2 = idx + 2 To idx + 20
                    Dim acc_found As Boolean = True
                    cl_ref = Mid(line(idx2), 4, 8)
                    Try
                        num_ref = cl_ref
                    Catch ex As Exception
                        acc_found = False
                    End Try
                    If acc_found Then
                        name = Mid(line(idx2), 17, 20)
                        Exit For
                    End If
                Next
                For idx2 = idx2 + 1 To idx2 + 20
                    If Mid(line(idx2), 87, 3) = "Cur" Then
                        cl_bal = Mid(line(idx2), 103, 12)
                    End If
                    If Mid(line(idx2), 87, 3) = "Ref" Then
                        case_no = Mid(line(idx2), 103, 8)
                        Exit For
                    End If
                Next
                outfile = outfile & cl_ref & "|" & case_no & "|" & cl_bal & "|" & name & vbNewLine
                'see if there is another case
                For idx2 = idx2 + 1 To lines - 1
                    caption = Mid(line(idx2), 3, 3)
                    If caption = "Acc" Then
                        idx = idx2 - 1
                        Exit For
                    Else
                        If Mid(line(idx2), 87, 3) = "Cur" Then
                            cl_bal = Mid(line(idx2), 103, 12)
                            Dim idx3 As Integer
                            For idx3 = idx2 + 1 To idx2 + 20
                                If Mid(line(idx3), 87, 3) = "Ref" Then
                                    case_no = Mid(line(idx3), 103, 8)
                                    Exit For
                                End If
                            Next
                            outfile = outfile & cl_ref & "|" & case_no & "|" & cl_bal & "|" & name & vbNewLine
                        End If
                    End If
                Next
            End If
        Next

        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
        MessageBox.Show("Completed")
    End Sub

   
End Class

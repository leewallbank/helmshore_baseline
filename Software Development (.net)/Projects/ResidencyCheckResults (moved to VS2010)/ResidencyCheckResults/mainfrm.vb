Public Class mainfrm
    Dim outfile1 As String = ""
    Dim outfile2 As String = ""
    Dim outfile3 As String = ""
    Dim outfile4 As String = ""

    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click
        exitbtn.Enabled = False
        sheet_name = InputBox("Enter sheet name", "Enter sheet name")
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "XLS files|*.xls"
            .FileName = ""
            .CheckFileExists = True
        End With
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MessageBox.Show("No file Selected")
            Exit Sub
        End If
        load_vals(OpenFileDialog1.FileName)
        If finalrow = 0 Then
            MsgBox("No rows found in spreadsheet")
            exitbtn.Enabled = True
            Exit Sub
        End If
        exitbtn.Enabled = False
        readbtn.Enabled = False
        Try
            Dim rowidx As Integer
            For rowidx = 1 To finalrow
                ProgressBar1.Value = rowidx / finalrow * 100
                If rowidx = 1 Then
                    'write out headers
                    outfile1 = "Our Ref,Res score" & vbNewLine
                    outfile2 = outfile1
                    outfile3 = outfile1
                    outfile4 = "Our Ref,Phone No" & vbNewLine
                Else
                    If vals(rowidx, 1) = " " Then
                        Exit For
                    End If
                    If vals(rowidx, 1) = 1 Then
                        outfile1 = outfile1 & vals(rowidx, 3) & ",Residency score:" & vals(rowidx, 1) & vbNewLine
                    ElseIf vals(rowidx, 1) = 2 Then
                        outfile2 = outfile2 & vals(rowidx, 3) & ",Residency score:" & vals(rowidx, 1) & vbNewLine
                    ElseIf vals(rowidx, 1) = 3 Then
                        outfile3 = outfile3 & vals(rowidx, 3) & ",Residency score:" & vals(rowidx, 1) & vbNewLine
                    End If
                    Dim ph_no As String = Trim(vals(rowidx, 2))
                    Dim ph_no_found As Boolean = False
                    If ph_no.Length > 4 And ph_no <> "Ex-directory" Then
                        ph_no_found = True
                        outfile4 = outfile4 & vals(rowidx, 3) & "," & ph_no & vbNewLine
                    End If
                    'get linkid from onestep
                    Dim d As Integer
                    Try
                        d = vals(rowidx, 3)
                    Catch ex As Exception
                        MsgBox("Debtor is blank on rowid = " & rowidx)
                        Me.Close()
                        Exit Sub
                    End Try
                    param2 = "select linkID from Debtor where _rowid = " & d
                    Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read debtor for rowid = " & vals(rowidx, 3))
                        Exit Sub
                    End If
                    Dim linkid As Integer
                    Try
                        linkid = debtor_dataset.Tables(0).Rows(0).Item(0)
                    Catch ex As Exception
                        linkid = 0
                    End Try
                    If linkid = 0 Then
                        Continue For
                    End If
                    'get all debtors apart from leading case
                    param2 = "select _rowid from Debtor where linkID = " & linkid & _
                    " and _rowid <> " & vals(rowidx, 3)
                    Dim link_dataset As DataSet = get_dataset("onestep", param2)
                    Dim idx2 As Integer
                    For idx2 = 0 To no_of_rows - 1
                        Dim debtor As Integer = link_dataset.Tables(0).Rows(idx2).Item(0)
                        If vals(rowidx, 1) = 1 Then
                            outfile1 = outfile1 & debtor & ",Residency score:" & vals(rowidx, 1) & vbNewLine
                        ElseIf vals(rowidx, 1) = 2 Then
                            outfile2 = outfile2 & debtor & ",Residency score:" & vals(rowidx, 1) & vbNewLine
                        ElseIf vals(rowidx, 1) = 3 Then
                            outfile3 = outfile3 & debtor & ",Residency score:" & vals(rowidx, 1) & vbNewLine
                        End If
                        If ph_no_found Then
                            outfile4 = outfile4 & debtor & "," & ph_no & vbNewLine
                        End If
                    Next
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Exit Sub
        End Try
        Dim idx As Integer
        Dim filename_prefix As String = ""
        For idx = Len(OpenFileDialog1.FileName) To 1 Step -1
            If Mid(OpenFileDialog1.FileName, idx, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(OpenFileDialog1.FileName, idx - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_res_scores1.csv", outfile1, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_res_scores2.csv", outfile2, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_res_score_3.csv", outfile3, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_phone_numbers.csv", outfile4, False)
        MessageBox.Show("Files created successfully")
        Me.Close()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub


    Private Sub filebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        
    End Sub
End Class

Public Class retnfrm

    Private Sub retnfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        param1 = "onestep"
        param2 = "select _rowid from CodeReturns order by _rowid"
        Dim retn_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to find any return codes")
            Exit Sub
        End If
        Dim idx, retn_code As Integer
        Dim retn_group As String
        Dim retn_rows As Integer = no_of_rows - 1
        For idx = 0 To retn_rows
            retn_code = retn_dataset.Tables(0).Rows(idx).Item(0)
            'get retn_group from table
            ReturnCodesTableAdapter.FillBy(FeesSQLDataSet.ReturnCodes, retn_code)
            Try
                retn_group = FeesSQLDataSet.Tables(0).Rows(0).Item(1)
            Catch ex As Exception
                retn_group = ""
            End Try
            DataGridView1.Rows.Add(retn_code, retn_group)
        Next

    End Sub

    Private Sub DataGridView1_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DataGridView1.CellValidating
        If e.ColumnIndex = 1 Then
            Dim first_2_letters As String = _
            Microsoft.VisualBasic.Left(DataGridView1.Rows(e.RowIndex).Cells(1).EditedFormattedValue, 2)
            first_2_letters = UCase(first_2_letters)
            If first_2_letters <> "NU" And _
            first_2_letters <> "NB" And _
            first_2_letters <> "AB" And _
            first_2_letters <> "AT" And _
            first_2_letters <> "OT" And _
            first_2_letters <> "CL" And _
            first_2_letters <> "PA" And _
            first_2_letters.Length > 0 Then
                MsgBox("Invalid return group - only NU,NB,AB,AT,CL,PA,OT")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.ColumnIndex = 1 And e.RowIndex >= 0 Then
            Dim retn_code As Integer
            Dim retn_group As String
            retn_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            retn_group = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            Select Case UCase(Microsoft.VisualBasic.Left(retn_group, 2))
                Case "NB", "NU"
                    retn_group = "Nulla Bona"
                Case "AB"
                    retn_group = "Abscond"
                Case "AT"
                    retn_group = "Attachment"
                Case "CL", "OT"
                    retn_group = "Other"
                Case "PA"
                    retn_group = "Paid"
                Case Else
                    retn_group = ""
            End Select
            ReturnCodesTableAdapter.FillBy(FeesSQLDataSet.ReturnCodes, retn_code)
            If FeesSQLDataSet.ReturnCodes.Rows.Count = 0 Then
                Try
                    Me.ReturnCodesTableAdapter.InsertQuery(retn_code, retn_group)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            Else
                Try
                    Me.ReturnCodesTableAdapter.UpdateQuery(retn_group, retn_code)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End If

        End If
    End Sub
End Class
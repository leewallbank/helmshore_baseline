Public Class mainfrm
    Dim first_letter As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim days As Integer = Weekday(Now)
        date_picker.Value = DateAdd(DateInterval.Day, -days, Now)
    End Sub

    Private Sub csbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles csbtn.Click
        first_letter = InputBox("Enter first letter of client", "First Letter of Client")
        first_letter = UCase(Microsoft.VisualBasic.Left(Trim(first_letter), 1))
        If first_letter.Length = 0 Then
            MsgBox("NO Letter entered")
            Exit Sub
        End If
        param1 = "onestep"
        param2 = "Select _rowid, name from Client where name like '" & first_letter & "%" & "'" & _
        " order by name"
        Dim client_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("There are no clients starting with " & first_letter)
            Exit Sub
        End If
        Dim idx As Integer
        cl_rows = no_of_rows - 1
        For idx = 0 To cl_rows
            If idx = UBound(client_array, 2) Then
                ReDim Preserve client_array(2, idx + 20)
            End If
            client_array(1, idx) = client_dataset.Tables(0).Rows(idx).Item(0)
            client_array(2, idx) = Trim(client_dataset.Tables(0).Rows(idx).Item(1))
        Next
        clientfrm.ShowDialog()
        If selected_csid = 0 Then
            MsgBox("No client scheme has been selected")
            Exit Sub
        Else
            If MsgBox("Client Scheme selected is " & client_name & " - " & scheme_name, MsgBoxStyle.YesNo, "Client Scheme verification") = MsgBoxResult.No Then
                Exit Sub
            End If
        End If
        date_picker.Enabled = True
        Label1.Text = client_name & " - " & scheme_name
        retnbtn.Enabled = True
    End Sub

    Private Sub retnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles retnbtn.Click
        retn_month = Format(date_picker.Value, "MM_MMM")
        retn_day = Format(date_picker.Value, "dd_ddd")
        'get remittance number
        param2 = "select _rowid from Remit where clientschemeID = " & selected_csid & _
        " and date = '" & Format(date_picker.Value, "yyyy-MM-dd") & "'"
        Dim remit_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("There are no remits for this client on " & date_picker.Value)
            Exit Sub
        End If
        Dim remit_no As Integer = remit_dataset.Tables(0).Rows(0).Item(0)
        ' 05.01.2011 D.J.Dicks - The following line will not work correctly if the remit and the run date
        ' are on two different years NOW gives you the current date
        'file_path = "O:\DebtRecovery\Archives\" & Format(Now, "yyyy") & "\" & retn_month & "\" & _
        '                           retn_day & "\Remittances\" & Format(remit_no, "#") & "\Returns\"
        file_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                           retn_day & "\Remittances\" & Format(remit_no, "#") & "\Returns\"

        Dim file_name, client_ref As String
        'create directories for returns
        Dim dir_name As String = file_path & "Nulla Bona"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create folder")
            End
        End Try
        dir_name = file_path & "Abscond"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create folder")
            End
        End Try
        dir_name = file_path & "Attachment"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create folder")
            End
        End Try
        dir_name = file_path & "Client request - other"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create folder")
            End
        End Try

        Dim debtor, retn_codeid As Integer
        Dim files_found As Boolean = False
        For Each foundFile As String In My.Computer.FileSystem.GetFiles _
        (file_path, FileIO.SearchOption.SearchAllSubDirectories, "*.pdf")
            'get debtor number
            files_found = True
            debtor = Mid(foundFile, foundFile.Length - 10, 7)
            'get return code for this debtor
            param2 = "select return_codeID, client_ref, status_open_closed from Debtor " & _
            " where _rowid = " & debtor
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to find case number" & debtor)
                Exit Sub
            End If
            Dim status_open_closed As String = debtor_dataset.Tables(0).Rows(0).Item(2)
            If status_open_closed = "O" Then
                Continue For
            End If
            Try
                retn_codeid = debtor_dataset.Tables(0).Rows(0).Item(0)
            Catch ex As Exception
                retn_codeid = 0
            End Try
            'get retn_group for non-zero retn_codeid
            Dim retn_group As String = ""
            If retn_codeid > 0 Then
                retnfrm.ReturnCodesTableAdapter.FillBy(retnfrm.FeesSQLDataSet.ReturnCodes, retn_codeid)
                Try
                    retn_group = retnfrm.FeesSQLDataSet.Tables(0).Rows(0).Item(1)
                Catch ex As Exception
                    MsgBox("Update return code group for " & retn_codeid & " before producing reports")
                    Exit Sub
                End Try
            Else
                MsgBox("Unable to find return code for case " & debtor)
            End If
            If retn_group = "Other" Then
                retn_group = "Client request - other"
            End If
            client_ref = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
            file_name = file_path & retn_group & "\" & client_ref & "-" & debtor & ".pdf"
            My.Computer.FileSystem.CopyFile(foundFile, file_name)
        Next
        If files_found Then
            MsgBox("All reports moved to directories")
        Else
            MsgBox("No reports were found")
        End If
        Me.Close()
    End Sub

    Private Sub mainbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mainbtn.Click
        retnfrm.ShowDialog()
    End Sub
End Class

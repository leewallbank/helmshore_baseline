Public Class mainfrm
    Private RA863report As New RA863P4
    Private Const PARAMETER_FIELD_NAME1 As String = "parm_debtor"
    Dim myArrayList1 As ArrayList = New ArrayList()
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim days As Integer = Weekday(Now)
        date_picker.Value = DateAdd(DateInterval.Day, -days, Now)
        'populate sch_combobox
        sch_combobox.SelectedIndex = 0
    End Sub


    Private Sub retnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles retnbtn.Click
        retn_month = Format(date_picker.Value, "MM_MMM")
        retn_day = Format(date_picker.Value, "dd_ddd")
        'get remittance number
        Dim selected_csid As Integer
        Select Case sch_combobox.SelectedIndex
            Case 0
                selected_csid = 254  'CTAX
            Case 1
                selected_csid = 3969  'CTAX-TCE
            Case 2
                selected_csid = 256 'NNDR
            Case 3
                selected_csid = 3970 'NNDR-TCE-FT
        End Select
        param1 = "onestep"
        param2 = "select _rowid from Remit where clientschemeID = " & selected_csid & _
        " and date = '" & Format(date_picker.Value, "yyyy-MM-dd") & "'"
        Dim remit_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("There are no remits for this client on " & date_picker.Value)
            Exit Sub
        End If
        Dim remit_no As Integer = remit_dataset.Tables(0).Rows(0).Item(0)
        'get all returned cases for this remit
        Dim debtor As Integer
        Dim client_ref As String
        param2 = "select _rowid, client_ref from Debtor " & _
        " where return_remitID = " & remit_no & _
        " and status_open_closed = 'C' and status = 'C' order by _rowid "
        Dim debtor_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("No cases found for this remit")
            Exit Sub
        End If
        exitbtn.Enabled = False
        sch_combobox.Enabled = False
        date_picker.Enabled = False
        retnbtn.Enabled = False

        Dim debtor_rows As Integer = no_of_rows
        Dim idx As Integer
        Dim fname As String = ""
        For idx = 0 To debtor_rows - 1
            Try
                ProgressBar1.Value = (idx / debtor_rows) * 100
            Catch ex As Exception
                ProgressBar1.Value = 0
            End Try
            Application.DoEvents()
            'get debtor number
            debtor = debtor_dataset.Tables(0).Rows(idx).Item(0)
            client_ref = Trim(debtor_dataset.Tables(0).Rows(idx).Item(1))
            'remove any slashes from client_ref
            client_ref = Replace(client_ref, "/", " ")
            If idx = 0 Then
                With SaveFileDialog1
                    .Title = "Save files"
                    .Filter = "pdf|*.pdf"
                    .DefaultExt = ".pdf"
                    .OverwritePrompt = True
                    .FileName = "Wakefield returns.pdf"
                    .Title = "Select folder for saving files"
                End With
                If SaveFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
                    MsgBox("Files not created")
                    exitbtn.Enabled = True
                    Exit Sub
                Else
                    fname = SaveFileDialog1.FileName
                    Dim idx2 As Integer
                    For idx2 = fname.Length To 1 Step -1
                        If Mid(fname, idx2, 1) = "\" Then
                            file_path = Microsoft.VisualBasic.Left(fname, idx2)
                            Exit For
                        End If
                    Next
                End If
            End If
            setup_param(debtor, client_ref)
        Next
        write_report(fname)
        MsgBox("Reports generated")

        Me.Close()
    End Sub
    Sub setup_param(ByVal debtor As Integer, ByVal cl_ref As String)
        myArrayList1.Add(debtor)
        SetCurrentValuesForParameterField1(RA863Report, myArrayList1)
    End Sub
    Sub write_report(ByVal fname As String)
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA863Report)
        RA863Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fname)
        RA863Report.close()
    End Sub
    Private Sub SetCurrentValuesForParameterField1(ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myArrayList As ArrayList)
        Dim currentParameterValues As CrystalDecisions.Shared.ParameterValues = New CrystalDecisions.Shared.ParameterValues()
        For Each submittedValue As Object In myArrayList
            Dim myParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue()
            myParameterDiscreteValue.Value = submittedValue.ToString()
            currentParameterValues.Add(myParameterDiscreteValue)
        Next
        Dim myParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        Dim myParameterFieldDefinition As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME1)
        myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)
    End Sub
    Public Sub SetDBLogonForReport(ByVal myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo, ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub
End Class

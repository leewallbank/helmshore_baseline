Public Class mainform



    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If

        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline, caption As String
        Dim save_case As String = ""
        Dim linetext As String = ""
        Dim line(0) As String
        Dim propaddr As String = ""
        Dim curraddr As String = ""
        Dim last_curraddr As String = ""
        Dim yr_per As String = ""
        Dim name As String = ""
        Dim last_name As String = ""
        Dim name2 As String = ""
        Dim tel1 As String = ""
        Dim tel2 As String = ""
        Dim lonum As String = ""
        Dim last_lonum As String = ""
        Dim idx, idx2, idx3 As Integer
        Dim lines As Integer = 0
        Dim debt_amt, war_amt As Decimal
        Dim lodate, fromdate, todate As Date
        Dim clref As String = ""
        Dim last_clref As String = ""
        Dim addrline As String = ""
        Dim amtstring As String = ""
        Dim comments As String = Nothing

        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "Client ref|Name|Name2|DebtAddress|Current Address|LO Number|LO Date|Warrant Amt|Debt Amount|Yr period|Comments" & vbNewLine
        outfile = outline

        'look for DT030 - start of case details
        For idx = 1 To lines - 1
            Dim lstring As String
            caption = Mid(line(idx), 2, 5)
            'get details for this case
            Select Case caption
                Case Is = "DT030"
                    clref = Mid(line(idx), 10, 8)
                    lonum = Mid(line(idx), 47, 7)
                Case Is = "DT040"
                    name = Trim(Mid(line(idx), 8, 36))
                    If Mid(line(idx), 45, 1) = "&" Then
                        name2 = Trim(Mid(line(idx), 47, 34))
                    Else
                        lstring = Trim(Mid(line(idx), 45, 36))
                        If Len(lstring) > 1 Then
                            curraddr = lstring
                        End If
                    End If
                Case Is = "DT050"
                    lstring = Trim(Mid(line(idx), 8, 36))
                    If Len(curraddr) > 1 And Len(lstring) > 1 Then
                        curraddr = curraddr & ", " & lstring
                    ElseIf Len(lstring) > 1 Then
                        curraddr = lstring
                    End If
                    lstring = Trim(Mid(line(idx), 45, 36))
                    If Len(curraddr) > 1 And Len(lstring) > 1 Then
                        curraddr = curraddr & ", " & lstring
                    ElseIf Len(lstring) > 1 Then
                        curraddr = lstring
                    End If

                Case Is = "DT060"
                    lstring = Trim(Mid(line(idx), 8, 36))
                    If Len(curraddr) > 1 And Len(lstring) > 1 Then
                        curraddr = curraddr & ", " & lstring
                    ElseIf Len(lstring) > 1 Then
                        curraddr = lstring
                    End If
                    lstring = Trim(Mid(line(idx), 45, 36))
                    If Len(curraddr) > 1 And Len(lstring) > 1 Then
                        curraddr = curraddr & ", " & lstring
                    ElseIf Len(lstring) > 1 Then
                        curraddr = lstring
                    End If
                Case Is = "DT080"
                    lstring = Trim(Mid(line(idx), 8, 36))
                    If Len(lstring) > 1 Then
                        propaddr = lstring
                    End If
                    lstring = Trim(Mid(line(idx), 45, 36))
                    If Len(propaddr) > 1 And Len(lstring) > 1 Then
                        propaddr = propaddr & ", " & lstring
                    ElseIf Len(lstring) > 1 Then
                        propaddr = lstring
                    End If
                Case Is = "DT090"
                    lstring = Trim(Mid(line(idx), 8, 36))
                    If Len(propaddr) > 1 And Len(lstring) > 1 Then
                        propaddr = propaddr & ", " & lstring
                    ElseIf Len(lstring) > 1 Then
                        propaddr = lstring
                    End If
                    lstring = Trim(Mid(line(idx), 45, 36))
                    If Len(propaddr) > 1 And Len(lstring) > 1 Then
                        propaddr = propaddr & ", " & lstring
                    ElseIf Len(lstring) > 1 Then
                        propaddr = lstring
                    End If
                    lstring = Trim(Mid(line(idx), 82, 10))
                    If Len(propaddr) > 1 And Len(lstring) > 1 Then
                        propaddr = propaddr & ", " & lstring
                    ElseIf Len(lstring) > 1 Then
                        propaddr = lstring
                    End If
                Case Is = "DT100"
                    For idx2 = 10 To 30
                        If Mid(line(idx), idx2, 1) = "|" Then
                            Exit For
                        End If
                    Next
                    For idx3 = idx2 + 1 To idx2 + 20
                        If Mid(line(idx), idx3, 1) = "|" Then
                            Exit For
                        End If
                    Next
                    lstring = Mid(line(idx), idx2 + 1, idx3 - idx2 - 1)
                    If Not IsDate(lstring) Then
                        errorfile = errorfile & "Line  " & idx & " DT100 - Invalid lo date" & vbNewLine
                    Else
                        lodate = lstring
                    End If
                Case Is = "DT110"
                    yr_per = Mid(line(idx), 8, 7)
                    lstring = ""
                    For idx2 = 16 To 31
                        If Mid(line(idx), idx2, 1) <> " " Then
                            lstring = lstring & Mid(line(idx), idx2, 1)
                        End If
                    Next
                    lstring = Microsoft.VisualBasic.Right(lstring, Len(lstring) - 1)
                    If Not IsNumeric(lstring) Then
                        errorfile = errorfile & "Line  " & idx & " DT110 - Invalid warrant amount" & vbNewLine
                    Else
                        war_amt = lstring
                    End If
                Case Is = "DT140"
                    lstring = ""
                    For idx2 = 42 To 56
                        If Mid(line(idx), idx2, 1) <> " " Then
                            lstring = lstring & Mid(line(idx), idx2, 1)
                        End If
                    Next
                    lstring = Microsoft.VisualBasic.Right(lstring, Len(lstring) - 1)
                    If Not IsNumeric(lstring) Then
                        errorfile = errorfile & "Line  " & idx & " DT140 - Invalid debt amount" & vbNewLine
                    Else
                        debt_amt = lstring
                    End If
            End Select
            If caption = "DT999" Then

                'validate case details
                If clref = Nothing Or Microsoft.VisualBasic.Left(clref, 3) = "   " Then
                    errorfile = errorfile & "Line  " & idx & " - No client reference" & vbNewLine
                End If

                'check if this case has same clref and lonum as previous case
                'if so, this is a joint name so amend previous case details
                If clref = last_clref And lonum = last_lonum Then
                    name2 = name
                    comments = name & " has address " & curraddr
                    curraddr = last_curraddr
                    name = last_name
                    outfile = outfile & clref & "|" & name & "|" & name2 & "|" & propaddr & "|" _
                     & curraddr & "|" & lonum & "|" & lodate & "|" & war_amt _
                       & "|" & debt_amt & "|" & yr_per & "|" & comments & vbNewLine
                    save_case = ""
                Else
                    comments = ""
                    If Len(save_case) > 0 Then
                        outfile = outfile & save_case
                    End If
                    save_case = clref & "|" & name & "|" & name2 & "|" & propaddr & "|" _
                                   & curraddr & "|" & lonum & "|" & lodate & "|" & war_amt _
                                   & "|" & debt_amt & "|" & yr_per & "|" & "|" & comments & vbNewLine
                End If
               

                'save clref and lonum to compare to next case
                last_clref = clref
                last_lonum = lonum
                'save name and curraddr as may be required for a joint case
                last_name = name
                last_curraddr = curraddr

                name = ""
                name2 = ""
                propaddr = ""
                yr_per = ""
                tel1 = ""
                tel2 = ""
                curraddr = ""
                lonum = ""
                lodate = Nothing
                debt_amt = Nothing
                war_amt = Nothing
                comments = Nothing
                fromdate = Nothing
                todate = Nothing
            End If
        Next

        'write out last case
        outfile = outfile & save_case

        viewbtn.Enabled = True
        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText("error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
End Class

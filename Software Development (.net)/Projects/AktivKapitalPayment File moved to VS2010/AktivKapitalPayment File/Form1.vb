Imports System.IO
Public Class Form1

    Dim remit_date As Date
    Dim myArrayList1 As ArrayList = New ArrayList()
    Private Const PARAMETER_FIELD_NAME1 As String = "remit no"
    Dim file_name As String
    Dim remit_found As Boolean = False
    Dim first_remit As Boolean = True
    Dim filepath As String = ""
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'get last Saturday date
        remit_date = Now
        Dim wkday As Integer = Weekday(Now)
        remit_date = DateAdd(DateInterval.Day, -wkday, Now)
        remit_datetimepicker.Value = remit_date
    End Sub

    Private Sub writebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles writebtn.Click
        exitbtn.Enabled = False
        writebtn.Enabled = False
        Application.DoEvents()
        'get all csid for Aktiv Kapital
        '3.4.2012 process each clients in one file
        'AI is client 1227
        '22.4.2013 now have all clients in one file
        process_client()
        'process_client(1227, "AI")
        'FI is client 1228
        'myArrayList1.Clear()
        'process_client(1228, "FI")
        'AS is client 1647
        'myArrayList1.Clear()
        'process_client(1647, "AS")
        If remit_found = False Then
            MsgBox("No remits found for " & Format(remit_date, "dd/MM/yyyy"))
        Else
            MsgBox("Report created")
            Me.Close()
        End If
    End Sub
    Private Sub process_client()
        param2 = "select _rowid from ClientScheme where clientID = 1227 or clientID = 1228 or clientID = 1647"
        Dim csid_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            Exit Sub
        End If
        remit_date = remit_datetimepicker.Value
        Dim idx, csid_rows As Integer
        csid_rows = no_of_rows - 1
        Dim remitID As Integer
        For idx = 0 To csid_rows
            Dim csID As Integer = csid_dataset.Tables(0).Rows(idx).Item(0)
            param2 = "select _rowid from Remit where clientschemeID = " & csID & _
            " and date = '" & Format(remit_date, "yyyy-MM-dd") & "'"
            Dim remit_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim remit_rows As Integer = no_of_rows - 1
            Dim remit_idx As Integer
            For remit_idx = 0 To remit_rows
                remitID = remit_dataset.Tables(0).Rows(remit_idx).Item(0)
                If first_remit Then
                    remit_found = True
                    first_remit = False
                    With SaveFileDialog1
                        .Title = "Save file"
                        .Filter = "CSV files |*.csv"
                        .DefaultExt = ".csv"
                        .OverwritePrompt = True
                        .FileName = "AktivKapital_payment.csv"
                    End With
                    If SaveFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
                        MsgBox("No payment files created")
                        Exit Sub
                    End If
                Else
                    file_name = SaveFileDialog1.FileName
                End If
                myArrayList1.Add(remitID)
            Next
        Next
        If csid_rows < 0 Or first_remit Then
            Exit Sub
        End If
        Dim RD197Report = New RD197
        SetCurrentValuesForParameterField1(RD197Report, myArrayList1)
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD197Report)
        RD197Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.CharacterSeparatedValues, file_name)
        RD197Report.close()
        'read in file ignore first 14 columns apart from first line
        Dim txt_doc As String = My.Computer.FileSystem.ReadAllText(file_name)
        Dim idx2, comma_count As Integer
        Dim new_txt As String = ""
        For idx2 = 1 To txt_doc.Length
            If Mid(txt_doc, idx2, 1) <> ControlChars.Quote Then
                new_txt = new_txt & Mid(txt_doc, idx2, 1)
            End If
        Next
        txt_doc = new_txt
        new_txt = ""
        For idx2 = 1 To txt_doc.Length
            If Mid(txt_doc, idx2, 1) = "," Then
                comma_count += 1
                If comma_count = 14 Then
                    If new_txt.Length = 0 Then
                        new_txt = Microsoft.VisualBasic.Left(txt_doc, idx2 - 1) & vbNewLine
                    End If
                    'save to end of carriage return
                    Dim idx3 As Integer
                    For idx3 = idx2 To txt_doc.Length
                        If Mid(txt_doc, idx3, 1) = Chr(13) Then
                            new_txt = new_txt & Mid(txt_doc, idx2 + 1, idx3 - idx2 + 1)
                            Exit For
                        End If
                    Next
                    idx2 = idx3 + 1
                    comma_count = 0
                End If
            End If
        Next
        My.Computer.FileSystem.WriteAllText(file_name, new_txt, False, System.Text.Encoding.ASCII)
    End Sub
    Private Sub SetCurrentValuesForParameterField1(ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myArrayList As ArrayList)
        Dim currentParameterValues As CrystalDecisions.Shared.ParameterValues = New CrystalDecisions.Shared.ParameterValues()
        For Each submittedValue As Object In myArrayList
            Dim myParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue()
            myParameterDiscreteValue.Value = submittedValue.ToString()
            currentParameterValues.Add(myParameterDiscreteValue)
        Next
        Dim myParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        Dim myParameterFieldDefinition As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME1)
        myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)
    End Sub
    Public Sub SetDBLogonForReport(ByVal myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo, ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub writebtn_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles writebtn.Leave
        exitbtn.Enabled = True
        writebtn.Enabled = True
    End Sub
End Class

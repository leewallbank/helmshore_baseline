<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.allbtn = New System.Windows.Forms.Button
        Me.transbtn = New System.Windows.Forms.Button
        Me.invbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.actbtn = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.Arrow_Guernsey_report_datesDataSet = New ArrowGuernseyReports.Arrow_Guernsey_report_datesDataSet
        Me.Arrow_Guernsey_report_datesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Arrow_Guernsey_report_datesTableAdapter = New ArrowGuernseyReports.Arrow_Guernsey_report_datesDataSetTableAdapters.Arrow_Guernsey_report_datesTableAdapter
        CType(Me.Arrow_Guernsey_report_datesDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Arrow_Guernsey_report_datesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'allbtn
        '
        Me.allbtn.Location = New System.Drawing.Point(83, 34)
        Me.allbtn.Name = "allbtn"
        Me.allbtn.Size = New System.Drawing.Size(126, 23)
        Me.allbtn.TabIndex = 0
        Me.allbtn.Text = "All Guernsey Reports"
        Me.allbtn.UseVisualStyleBackColor = True
        '
        'transbtn
        '
        Me.transbtn.Location = New System.Drawing.Point(83, 143)
        Me.transbtn.Name = "transbtn"
        Me.transbtn.Size = New System.Drawing.Size(149, 23)
        Me.transbtn.TabIndex = 2
        Me.transbtn.Text = "Guernsey Transaction report"
        Me.transbtn.UseVisualStyleBackColor = True
        '
        'invbtn
        '
        Me.invbtn.Location = New System.Drawing.Point(83, 192)
        Me.invbtn.Name = "invbtn"
        Me.invbtn.Size = New System.Drawing.Size(149, 23)
        Me.invbtn.TabIndex = 3
        Me.invbtn.Text = "Guernsey Inventory Report"
        Me.invbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(205, 265)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 4
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'actbtn
        '
        Me.actbtn.Location = New System.Drawing.Point(83, 101)
        Me.actbtn.Name = "actbtn"
        Me.actbtn.Size = New System.Drawing.Size(134, 23)
        Me.actbtn.TabIndex = 1
        Me.actbtn.Text = "Guernsey Activity Report"
        Me.actbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 265)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 5
        '
        'Arrow_Guernsey_report_datesDataSet
        '
        Me.Arrow_Guernsey_report_datesDataSet.DataSetName = "Arrow_Guernsey_report_datesDataSet"
        Me.Arrow_Guernsey_report_datesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Arrow_Guernsey_report_datesBindingSource
        '
        Me.Arrow_Guernsey_report_datesBindingSource.DataMember = "Arrow_Guernsey_report_dates"
        Me.Arrow_Guernsey_report_datesBindingSource.DataSource = Me.Arrow_Guernsey_report_datesDataSet
        '
        'Arrow_Guernsey_report_datesTableAdapter
        '
        Me.Arrow_Guernsey_report_datesTableAdapter.ClearBeforeFill = True
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(348, 312)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.actbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.invbtn)
        Me.Controls.Add(Me.transbtn)
        Me.Controls.Add(Me.allbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Arrow Guernsey Reports"
        CType(Me.Arrow_Guernsey_report_datesDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Arrow_Guernsey_report_datesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents allbtn As System.Windows.Forms.Button
    Friend WithEvents transbtn As System.Windows.Forms.Button
    Friend WithEvents invbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents actbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents Arrow_Guernsey_report_datesDataSet As ArrowGuernseyReports.Arrow_Guernsey_report_datesDataSet
    Friend WithEvents Arrow_Guernsey_report_datesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Arrow_Guernsey_report_datesTableAdapter As ArrowGuernseyReports.Arrow_Guernsey_report_datesDataSetTableAdapters.Arrow_Guernsey_report_datesTableAdapter
End Class

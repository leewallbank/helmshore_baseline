<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class smsfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.drbtn = New System.Windows.Forms.RadioButton
        Me.srbtn = New System.Windows.Forms.RadioButton
        Me.exitbtn = New System.Windows.Forms.Button
        Me.qivoxrbtn = New System.Windows.Forms.RadioButton
        Me.SuspendLayout()
        '
        'drbtn
        '
        Me.drbtn.AutoSize = True
        Me.drbtn.Checked = True
        Me.drbtn.Location = New System.Drawing.Point(61, 31)
        Me.drbtn.Name = "drbtn"
        Me.drbtn.Size = New System.Drawing.Size(54, 17)
        Me.drbtn.TabIndex = 0
        Me.drbtn.TabStop = True
        Me.drbtn.Text = "Dialler"
        Me.drbtn.UseVisualStyleBackColor = True
        '
        'srbtn
        '
        Me.srbtn.AutoSize = True
        Me.srbtn.Location = New System.Drawing.Point(61, 73)
        Me.srbtn.Name = "srbtn"
        Me.srbtn.Size = New System.Drawing.Size(48, 17)
        Me.srbtn.TabIndex = 1
        Me.srbtn.Text = "SMS"
        Me.srbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(61, 173)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "Run"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'qivoxrbtn
        '
        Me.qivoxrbtn.AutoSize = True
        Me.qivoxrbtn.Location = New System.Drawing.Point(61, 112)
        Me.qivoxrbtn.Name = "qivoxrbtn"
        Me.qivoxrbtn.Size = New System.Drawing.Size(52, 17)
        Me.qivoxrbtn.TabIndex = 3
        Me.qivoxrbtn.Text = "Qivox"
        Me.qivoxrbtn.UseVisualStyleBackColor = True
        '
        'smsfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(234, 254)
        Me.Controls.Add(Me.qivoxrbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.srbtn)
        Me.Controls.Add(Me.drbtn)
        Me.Name = "smsfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Dialler or SMS"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents drbtn As System.Windows.Forms.RadioButton
    Friend WithEvents srbtn As System.Windows.Forms.RadioButton
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents qivoxrbtn As System.Windows.Forms.RadioButton
End Class

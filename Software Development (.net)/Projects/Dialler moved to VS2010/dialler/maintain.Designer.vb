<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class maintainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.SchemeIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Scheme = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CollectDiallerSchemeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FeesSQLDataSet = New dialler.FeesSQLDataSet
        Me.exitbtn = New System.Windows.Forms.Button
        Me.CollectDiallerSchemeTableAdapter = New dialler.FeesSQLDataSetTableAdapters.CollectDiallerSchemeTableAdapter
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CollectDiallerSchemeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SchemeIDDataGridViewTextBoxColumn, Me.Scheme})
        Me.DataGridView1.DataSource = Me.CollectDiallerSchemeBindingSource
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(343, 611)
        Me.DataGridView1.TabIndex = 0
        '
        'SchemeIDDataGridViewTextBoxColumn
        '
        Me.SchemeIDDataGridViewTextBoxColumn.DataPropertyName = "SchemeID"
        Me.SchemeIDDataGridViewTextBoxColumn.HeaderText = "SchemeID"
        Me.SchemeIDDataGridViewTextBoxColumn.Name = "SchemeIDDataGridViewTextBoxColumn"
        Me.SchemeIDDataGridViewTextBoxColumn.Width = 60
        '
        'Scheme
        '
        Me.Scheme.HeaderText = "Scheme"
        Me.Scheme.Name = "Scheme"
        Me.Scheme.ReadOnly = True
        Me.Scheme.Width = 200
        '
        'CollectDiallerSchemeBindingSource
        '
        Me.CollectDiallerSchemeBindingSource.DataMember = "CollectDiallerScheme"
        Me.CollectDiallerSchemeBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(435, 432)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'CollectDiallerSchemeTableAdapter
        '
        Me.CollectDiallerSchemeTableAdapter.ClearBeforeFill = True
        '
        'maintainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(343, 611)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.DataGridView1)
        Me.MinimizeBox = False
        Me.Name = "maintainfrm"
        Me.Text = "maintain"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CollectDiallerSchemeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents FeesSQLDataSet As dialler.FeesSQLDataSet
    Friend WithEvents CollectDiallerSchemeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents CollectDiallerSchemeTableAdapter As dialler.FeesSQLDataSetTableAdapters.CollectDiallerSchemeTableAdapter
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents SchemeIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Scheme As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class collect_stage
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.exitbtn = New System.Windows.Forms.Button
        Me.stagelbl = New System.Windows.Forms.Label
        Me.stagerb1 = New System.Windows.Forms.RadioButton
        Me.stagerb2 = New System.Windows.Forms.RadioButton
        Me.stagerb3 = New System.Windows.Forms.RadioButton
        Me.stagerb4 = New System.Windows.Forms.RadioButton
        Me.stagerb5 = New System.Windows.Forms.RadioButton
        Me.stagerb6 = New System.Windows.Forms.RadioButton
        Me.createbtn = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.maintainbtn = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar
        Me.letterb1 = New System.Windows.Forms.RadioButton
        Me.FeesSQLDataSet = New dialler.FeesSQLDataSet
        Me.Dialler_clientsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dialler_clientsTableAdapter = New dialler.FeesSQLDataSetTableAdapters.Dialler_clientsTableAdapter
        Me.StatusStrip1.SuspendLayout()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dialler_clientsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(266, 281)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 8
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'stagelbl
        '
        Me.stagelbl.AutoSize = True
        Me.stagelbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.stagelbl.Location = New System.Drawing.Point(175, 12)
        Me.stagelbl.Name = "stagelbl"
        Me.stagelbl.Size = New System.Drawing.Size(49, 16)
        Me.stagelbl.TabIndex = 0
        Me.stagelbl.Text = "Label1"
        '
        'stagerb1
        '
        Me.stagerb1.AutoSize = True
        Me.stagerb1.Checked = True
        Me.stagerb1.Location = New System.Drawing.Point(39, 41)
        Me.stagerb1.Name = "stagerb1"
        Me.stagerb1.Size = New System.Drawing.Size(65, 17)
        Me.stagerb1.TabIndex = 0
        Me.stagerb1.TabStop = True
        Me.stagerb1.Text = "Stage 1 "
        Me.stagerb1.UseVisualStyleBackColor = True
        '
        'stagerb2
        '
        Me.stagerb2.AutoSize = True
        Me.stagerb2.Location = New System.Drawing.Point(39, 75)
        Me.stagerb2.Name = "stagerb2"
        Me.stagerb2.Size = New System.Drawing.Size(62, 17)
        Me.stagerb2.TabIndex = 1
        Me.stagerb2.Text = "Stage 2"
        Me.stagerb2.UseVisualStyleBackColor = True
        '
        'stagerb3
        '
        Me.stagerb3.AutoSize = True
        Me.stagerb3.Location = New System.Drawing.Point(39, 116)
        Me.stagerb3.Name = "stagerb3"
        Me.stagerb3.Size = New System.Drawing.Size(62, 17)
        Me.stagerb3.TabIndex = 2
        Me.stagerb3.Text = "Stage 3"
        Me.stagerb3.UseVisualStyleBackColor = True
        '
        'stagerb4
        '
        Me.stagerb4.AutoSize = True
        Me.stagerb4.Location = New System.Drawing.Point(39, 155)
        Me.stagerb4.Name = "stagerb4"
        Me.stagerb4.Size = New System.Drawing.Size(62, 17)
        Me.stagerb4.TabIndex = 3
        Me.stagerb4.Text = "Stage 4"
        Me.stagerb4.UseVisualStyleBackColor = True
        '
        'stagerb5
        '
        Me.stagerb5.AutoSize = True
        Me.stagerb5.Location = New System.Drawing.Point(39, 198)
        Me.stagerb5.Name = "stagerb5"
        Me.stagerb5.Size = New System.Drawing.Size(78, 17)
        Me.stagerb5.TabIndex = 4
        Me.stagerb5.Text = "Stage PSD"
        Me.stagerb5.UseVisualStyleBackColor = True
        '
        'stagerb6
        '
        Me.stagerb6.AutoSize = True
        Me.stagerb6.Location = New System.Drawing.Point(39, 241)
        Me.stagerb6.Name = "stagerb6"
        Me.stagerb6.Size = New System.Drawing.Size(142, 17)
        Me.stagerb6.TabIndex = 5
        Me.stagerb6.Text = "Stage Awaiting Doorstep"
        Me.stagerb6.UseVisualStyleBackColor = True
        '
        'createbtn
        '
        Me.createbtn.Location = New System.Drawing.Point(165, 281)
        Me.createbtn.Name = "createbtn"
        Me.createbtn.Size = New System.Drawing.Size(75, 23)
        Me.createbtn.TabIndex = 7
        Me.createbtn.Text = "Create file"
        Me.createbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(36, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Select Stage"
        '
        'maintainbtn
        '
        Me.maintainbtn.Location = New System.Drawing.Point(42, 281)
        Me.maintainbtn.Name = "maintainbtn"
        Me.maintainbtn.Size = New System.Drawing.Size(75, 23)
        Me.maintainbtn.TabIndex = 6
        Me.maintainbtn.Text = "Maintain List"
        Me.maintainbtn.UseVisualStyleBackColor = True
        Me.maintainbtn.Visible = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripProgressBar1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 324)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(372, 22)
        Me.StatusStrip1.TabIndex = 9
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripProgressBar1
        '
        Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
        Me.ToolStripProgressBar1.Size = New System.Drawing.Size(100, 16)
        '
        'letterb1
        '
        Me.letterb1.AutoSize = True
        Me.letterb1.Location = New System.Drawing.Point(178, 75)
        Me.letterb1.Name = "letterb1"
        Me.letterb1.Size = New System.Drawing.Size(83, 17)
        Me.letterb1.TabIndex = 10
        Me.letterb1.TabStop = True
        Me.letterb1.Text = "Letter Stage"
        Me.letterb1.UseVisualStyleBackColor = True
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dialler_clientsBindingSource
        '
        Me.Dialler_clientsBindingSource.DataMember = "Dialler_clients"
        Me.Dialler_clientsBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'Dialler_clientsTableAdapter
        '
        Me.Dialler_clientsTableAdapter.ClearBeforeFill = True
        '
        'stage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(372, 346)
        Me.Controls.Add(Me.letterb1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.maintainbtn)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.createbtn)
        Me.Controls.Add(Me.stagerb6)
        Me.Controls.Add(Me.stagerb5)
        Me.Controls.Add(Me.stagerb4)
        Me.Controls.Add(Me.stagerb3)
        Me.Controls.Add(Me.stagerb2)
        Me.Controls.Add(Me.stagerb1)
        Me.Controls.Add(Me.stagelbl)
        Me.Controls.Add(Me.exitbtn)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "stage"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Stage"
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dialler_clientsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents stagelbl As System.Windows.Forms.Label
    Friend WithEvents stagerb1 As System.Windows.Forms.RadioButton
    Friend WithEvents stagerb2 As System.Windows.Forms.RadioButton
    Friend WithEvents stagerb3 As System.Windows.Forms.RadioButton
    Friend WithEvents stagerb4 As System.Windows.Forms.RadioButton
    Friend WithEvents stagerb5 As System.Windows.Forms.RadioButton
    Friend WithEvents stagerb6 As System.Windows.Forms.RadioButton
    Friend WithEvents createbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents maintainbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
    Friend WithEvents letterb1 As System.Windows.Forms.RadioButton
    Friend WithEvents FeesSQLDataSet As dialler.FeesSQLDataSet
    Friend WithEvents Dialler_clientsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dialler_clientsTableAdapter As dialler.FeesSQLDataSetTableAdapters.Dialler_clientsTableAdapter
End Class

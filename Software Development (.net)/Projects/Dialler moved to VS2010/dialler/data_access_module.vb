Imports System.Configuration
Imports System.Data.SqlClient
Module data_access_module
    Public table_array(,)
    Public conn_open As Boolean
    Public no_of_rows, last_rowid As Integer
    Public os_con As New Odbc.OdbcConnection()
    Public conn2 As New OleDb.OleDbConnection
    Public conn As New Odbc.OdbcConnection()

    Function get_table(ByVal database_name As String, ByVal table_name As String, Optional ByVal columns As String = Nothing, _
                Optional ByVal where_clause As String = Nothing, Optional ByVal order_clause As String = Nothing) As Integer
        Dim idx2 As Integer
        Dim error_found As Boolean = True
        If Left(columns, 8) <> "01_rowid" And Len(columns) > 0 Then
            MessageBox.Show("Where clause must start 01_rowid")
            Return 1
        End If
        If database_name = "Fees" Then
            os_con.ConnectionString = _
               "Dsn=RossFeeTable;description=FeesSQL Database;uid=sa;app=Microsoft� Visual Studio� 2005;wsid=DELL3703;database=FeesSQL"
        Else
            'conn.ConnectionString = _
            '   "Integrated Security=True;Dsn=DebtRecovery - DDSybase;na=192.168.19.26,14100;db=DebtRecovery;uid=thirdpartyB;password=thirdpartyB;"
            'test for reports database
            If conn_open = False Then
                os_con.ConnectionString = _
                              "Integrated Security=True;Dsn=DebtRecovery-Real;db=DebtRecovery;uid=thirdparty;password=thirdparty;"
                conn_open = True
                os_con.Open()
            End If

        End If
        Try
            Dim select_string As String

            select_string = "SELECT * from " & table_name & " " & where_clause & order_clause


            Dim adapter As New Odbc.OdbcDataAdapter(select_string, os_con)
            Dim dataset As New DataSet()
            adapter.Fill(dataset)
            no_of_rows = dataset.Tables(0).Rows.Count
            If no_of_rows = 0 Then
                Return 100
            End If

            If table_name = "Debtor" Then
                last_rowid = no_of_rows
            Else
                last_rowid = dataset.Tables(0).Rows(no_of_rows - 1).Item(0)
            End If

            Dim lastcol As Integer = dataset.Tables(0).Columns.Count

            Dim table As DataTable
            Dim row As DataRow
            Dim col As DataColumn
            Dim idx As Integer = 0

            ReDim table_array(last_rowid, lastcol)
            For Each table In dataset.Tables
                error_found = False
                For Each row In table.Rows
                    idx2 = 0
                    If table_name = "Debtor" Then
                        idx += 1
                    Else
                        idx = row.Item(0).ToString
                    End If
                    For Each col In table.Columns
                        Try
                            Dim col_no As Integer
                            col_no = InStr(1, columns, col.ColumnName)
                            If col_no > 0 Then
                                idx2 = Mid(columns, col_no - 2, 2)
                            Else
                                idx2 += 1
                            End If
                            If col_no > 0 Or columns = Nothing Then
                                Try
                                    table_array(idx, idx2) = row.Item(col).ToString
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try

                            End If
                        Catch ex As Exception
                            MsgBox(ex.Message)
                        End Try
                    Next
                Next
            Next
            Return 0
        Catch ex As Exception
            If error_found Then
                MessageBox.Show(ex.Message)
                Return 1
            Else
                If idx2 = 0 Then
                    Return 100
                Else

                End If
            End If
        Finally
            'conn.Close()
        End Try

    End Function
    Function get_dataset(ByVal database_name As String, Optional ByVal select_string As String = Nothing) As DataSet
        Dim dataset As New DataSet
        Try
            If database_name = "onestep" Then
                'If conn_open = False Then
                '    'conn.ConnectionString = _
                '    '   "Integrated Security=True;Dsn=DebtRecovery - DDSybase;na=192.168.19.26,14100;db=DebtRecovery;uid=thirdpartyB;password=thirdpartyB;"
                '    'production version 14.07.2011
                '    'conn.ConnectionString = _
                '    '   "Integrated Security=True;Dsn=DebtRecovery - DDSybase;db=DebtRecovery;uid=thirdpartyB;password=thirdpartyB;"

                '    'test for reports database
                '    'conn.ConnectionString = _
                '    '                      "Integrated Security=True;Dsn=DebtRecovery;db=DebtRecovery;uid=vbnet;password=tenbv;"
                '    conn.ConnectionString = _
                '       "Integrated Security=True;Dsn=DebtRecovery-Real;db=DebtRecovery-Real;uid=thirdparty;password=thirdparty;"
                '    conn.Open()
                '    conn_open = True
                'End If
                'Dim adapter As New Odbc.OdbcDataAdapter(select_string, conn)

                'adapter.Fill(dataset)
                'conn.Close()
                If os_con.State = ConnectionState.Closed Then
                    Connect_os_Db()
                End If
                Dim adapter As New Odbc.OdbcDataAdapter(select_string, os_con)
                adapter.Fill(dataset)
            Else
                conn2.ConnectionString = ""
                If database_name = "Fees" Then
                    conn2.ConnectionString = _
                     "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=FeesSQL;Data Source=ROSSVR01\SQL2005"
                ElseIf database_name = "TestFees" Then
                    conn2.ConnectionString = _
                    "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=TestFeesSQL;Data Source=ROSSVR01\SQL2005"
                ElseIf database_name = "Complaints" Then
                    conn2.ConnectionString = _
                       "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=PraiseAndComplaintsSQL;Data Source=ROSSVR01\SQL2005"
                ElseIf database_name = "Employed" Then
                    conn2.ConnectionString = _
                   "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=r:\Access tables\Employed_bailiffs.mdb;Persist Security Info=False"
                ElseIf database_name = "LSC" Then
                    conn2.ConnectionString = _
                    "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=rpt_user;Password=resu_tpr;Initial Catalog=LSCreporting;Data Source=ROSSVR01\SQL2005"
                End If
                conn2.Open()
                Dim adapter As New OleDb.OleDbDataAdapter(select_string, conn2)
                adapter.Fill(dataset)
                conn2.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Try
            no_of_rows = dataset.Tables(0).Rows.Count
        Catch
            no_of_rows = 0
        End Try
        Return dataset

    End Function
    Public Sub Connect_os_Db()

        Try
            If Not IsNothing(os_con) Then
                'This is only necessary following an exception...
                If os_con.State = ConnectionState.Open Then os_con.Close()
            End If
            os_con.ConnectionString = ""
            os_con.ConnectionString = ConfigurationManager.ConnectionStrings("DebtRecovery").ConnectionString
            os_con.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings("DebtRecovery").ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings("DebtRecovery").ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub
    Sub update_sql(ByVal upd_txt As String)
        Try
            If conn.State = ConnectionState.Closed Then
                Connect_sqlDb()
            End If
            Dim ODBCCMD As Odbc.OdbcCommand

            'Define attachment to database table specifics
            ODBCCMD = New Odbc.OdbcCommand
            With ODBCCMD
                .Connection = conn
                .CommandText = upd_txt
                .ExecuteNonQuery()
            End With

            'Clean up the connection
            ODBCCMD = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub Connect_sqlDb()

        Try
            If Not IsNothing(conn) Then
                'This is only necessary following an exception...
                If conn.State = ConnectionState.Open Then conn.Close()
            End If
            conn.ConnectionString = ""
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("FeesSQL").ConnectionString
            conn.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings("FeesSQL").ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings("feesSQL").ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub
End Module

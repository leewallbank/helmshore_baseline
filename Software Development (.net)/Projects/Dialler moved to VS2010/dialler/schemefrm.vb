Public Class schemefrm

    Private Sub schemefrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim row As DataGridViewRow
        Dim idx As Integer = 0
        ReDim csid_table(100)
        csid_rows = 0
        selected_scheme_rows = 0
        For Each row In DataGridView1.Rows
            If DataGridView1.Rows(idx).Cells(0).Value = True Then
                Dim cs_id As Integer = DataGridView1.Rows(idx).Cells(1).Value
                If cs_id <> 0 Then
                    csid_table(csid_rows) = DataGridView1.Rows(idx).Cells(1).Value
                    csid_rows += 1
                Else
                    selected_schemes(selected_scheme_rows) = DataGridView1.Rows(idx).Cells(2).Value
                    selected_scheme_rows += 1
                End If

            End If
            idx += 1
        Next

    End Sub

    Private Sub schemefrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FeesSQLDataSet.Dialler_bailiff_clients' table. You can move, or remove it, as needed.
        Me.Dialler_bailiff_clientsTableAdapter.Fill(Me.FeesSQLDataSet.Dialler_bailiff_clients)

        'get schemes for client
        Dim scheme_names(100), csid_array(100)
        If selected_client > 0 Then
            param2 = "select schemeID, _rowid, branchID from ClientScheme where clientID = " & selected_client
            If collect_cases Then
                If selected_client = 1275 Then 'HMRC
                    param2 = param2 & " and branchID = 8"
                Else
                    param2 = param2 & " and branchID = 2"
                End If
            Else
                param2 = param2 & " and branchID <> 2"
            End If
            Dim csid_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("There are no schemes for this client")
                Me.Close()
                Exit Sub
            End If
            Dim idx As Integer
            DataGridView1.Rows.Clear()
            Dim csid_rows As Integer = no_of_rows - 1
            For idx = 0 To csid_rows
                Dim csid As Integer = csid_dataset.Tables(0).Rows(idx).Item(1)
                param2 = "select name from Scheme where _rowid = " & csid_dataset.Tables(0).Rows(idx).Item(0)
                Dim sch_dataset As DataSet = get_dataset("onestep", param2)
                Dim sch_name As String = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                csid_array(idx) = csid
                scheme_names(idx) = sch_name
            Next
            'Array.Sort(scheme_names, csid_array, 0, csid_rows + 1)
            'Array.Sort(branch_array, csid_array, 0, csid_rows + 1)
            For idx = 0 To csid_rows
                DataGridView1.Rows.Add(False, csid_array(idx), scheme_names(idx))
            Next
        Else
            Dim idx As Integer
            param2 = "select distinct name from Scheme where _rowid = 12 or _rowid = 14 or _rowid=6" & _
            " or _rowid=23 or _rowid=49 order by name"
            Dim sch_dataset As DataSet = get_dataset("onestep", param2)
            Dim sch_rows As Integer = no_of_rows
            selected_scheme_rows = 0
            For idx = 0 To sch_rows - 1
                Dim sch_name As String = Trim(sch_dataset.Tables(0).Rows(idx).Item(0))
                DataGridView1.Rows.Add(False, 0, sch_name)
            Next

        End If
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        'selected_csid = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        'Me.Close()
    End Sub

    Private Sub Dialler_bailiff_clientsBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.Dialler_bailiff_clientsBindingSource.EndEdit()
        Me.Dialler_bailiff_clientsTableAdapter.Update(Me.FeesSQLDataSet.Dialler_bailiff_clients)

    End Sub
End Class
Imports System.IO
Module Module1
    Public mode, param1, param2, param3, param4, ret_code As String
    Public sch_rows, selected_client, csid_rows, UU_selected_index As Integer
    Public selected_client_name As String
    Public stage_form, no_stages_found, collect_cases As Boolean
    Public agent_no As Integer
    Public outline As String = ""
    Public outfile As String = ""
    Public ccmt_offence_val As Integer
    Public ascii As New System.Text.ASCIIEncoding()
    Public selected_stage_name As String = ""
    Public selected_stage_rows As Integer
    Public cs_table(,), client_table(,), sch_table(,), stage_table(,), csid_table(100), selected_stages(50)
    Public selected_schemes(50) As String
    Public selected_scheme_rows As Integer
    Public sch_group As String
    Public cancelled, mobile_picked, sms_picked, qivox_picked As Boolean
    Dim upd_txt As String
    Public prod_run As Boolean = False
    Public hmrc_case As Boolean = False

    Function found_pc_sector(ByVal test_pcode As String) As Boolean
        Dim pc_found As Boolean = False
        Dim idx As Integer = 0
        Dim int As Integer = 0
        Dim row As DataRow
        Dim pcode, next_char, test_pcode_sector As String
        Dim test_length As Integer = test_pcode.Length - 3
        If test_length < 0 Then
            test_length = 1
        End If
        test_pcode_sector = Microsoft.VisualBasic.Left(test_pcode, test_length)
        For Each row In postcodefrm.FeesSQLDataSet.Out_of_area.Rows
            pcode = Trim(postcodefrm.FeesSQLDataSet.Out_of_area.Rows(idx).Item(1))
            If Microsoft.VisualBasic.Left(test_pcode, pcode.Length) = pcode Then
                Dim pcode_numeric As Boolean = True
                Try
                    int = Microsoft.VisualBasic.Right(pcode, 1)
                Catch ex As Exception
                    pcode_numeric = False
                End Try
                If test_pcode_sector.Length <= pcode.Length Then
                    idx += 1
                    Continue For
                End If
                next_char = Mid(test_pcode_sector, pcode.Length + 1, 1)
                If pcode_numeric Then
                    'found if next is a letter
                    Try
                        int = next_char
                    Catch ex As Exception
                        pc_found = True
                        Exit For
                    End Try
                Else
                    'found if next is a number
                    Dim letter_found As Boolean = False
                    Try
                        int = next_char
                    Catch ex As Exception
                        letter_found = True
                    End Try
                    If letter_found = False Then
                        pc_found = True
                        Exit For
                    End If
                End If
            End If
            idx += 1
        Next
        Return pc_found
    End Function
    Sub produce_dialler_file(ByVal param2 As String)
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("No cases found")
            Exit Sub
        End If
        Dim idx, debtor_rows As Integer
        '16.12.2014 SMS file amended
        If sms_picked Then
            'outfile = "OurRef,Forename,Surname,Mobile1,Mobile2,Client,StageName, LastDate" & vbNewLine
            outfile = "Our_Reference,Forename,Surname,TelNumber,SecondNumber,ThirdNumber,FourthNumber" & vbNewLine
        ElseIf qivox_picked Then
            outfile = "OurRef,Name,Mobile1,Mobile2" & vbNewLine
        Else
            outfile = "Our_Reference,Title,Forename,Surname,Address,Client,Scheme,TelNumber," & _
                                      "SecondNumber,ThirdNumber,FourthNumber,Balance,ClientID,Name2,Created_Date" & vbNewLine
        End If
        Dim sourcefname As String
        If sms_picked Then
            sourcefname = "RossendalesCollect_" & Format(Now, "yyyyMMdd") & ".csv"
        ElseIf qivox_picked Then
            sourcefname = "Qivox" & Format(Now, "ddMMyyyy") & ".csv"
        Else
            sourcefname = "dialler.csv"
        End If
        'write out file
        With collect_stage.SaveFileDialog1
            .Title = "Save file"
            .Filter = "CSV files |*.csv"
            .DefaultExt = ".csv"
            .OverwritePrompt = True
            .FileName = sourcefname
        End With
        If Not collect_stage.SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            MsgBox("No file name selected for save ")
            Return
        End If

        
        Dim FileName As String = Path.GetFileName(collect_stage.SaveFileDialog1.FileName)
        My.Computer.FileSystem.WriteAllText(collect_stage.SaveFileDialog1.FileName, outfile, False, ascii)

        outline = ""
        debtor_rows = no_of_rows
        Dim phone_array(90000) As String
        Dim outline_array(90000) As String
        Dim outline_idx As Integer = 0
        postcodefrm.Out_of_areaTableAdapter.Fill(postcodefrm.FeesSQLDataSet.Out_of_area)
        For idx = 0 To debtor_rows - 1
            Application.DoEvents()
            mainform.ProgressBar1.Value = (idx / debtor_rows) * 100
            If mode = "F" Then
                ooafrm.ProgressBar1.Value = (idx / debtor_rows) * 100
            ElseIf stage_form = False Then
                mainform.ProgressBar1.Value = (idx / debtor_rows) * 100
            Else
                collect_stage.ToolStripProgressBar1.Value = (idx / debtor_rows) * 100
            End If
            Dim created_date As Date = CDate(debtor_dataset.Tables(0).Rows(idx).Item(15))
            'ignore cases <5 days old
            'for hmrc cases ignore case < 3 days old
            'for UU cases ignore case < 2 days old
            Dim maxDays As Integer = 5
            If hmrc_case Then
                maxDays = 3
            ElseIf mode = "UU" Then
                maxDays = 2
            End If

            If DateDiff(DateInterval.Day, created_date, Now) < maxDays Then
                Continue For
            End If
            Dim debtor As Integer = debtor_dataset.Tables(0).Rows(idx).Item(0)

            Dim csid As Integer = debtor_dataset.Tables(0).Rows(idx).Item(5)
            Dim csid_idx As Integer
            If csid_rows > 0 Then
                Dim csid_found As Boolean = False
                For csid_idx = 0 To csid_rows
                    If csid_table(csid_idx) = csid Then
                        csid_found = True
                        Exit For
                    End If
                Next
                If csid_found = False Then
                    Continue For
                End If
            End If
            param2 = "select branchID, clientID, schemeID from ClientScheme where _rowid = " & csid
            Dim csid_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows <> 1 Then
                MsgBox("Unable to read clientScheme for csid = " & csid)
                Exit Sub
            End If
            Dim cl_ID = csid_dataset.Tables(0).Rows(0).Item(1)
            If cl_ID = 1 Or cl_ID = 2 Or cl_ID = 24 Then
                Continue For
            End If
            Dim branchID As Integer = csid_dataset.Tables(0).Rows(0).Item(0)

            Dim sch_ID As Integer = csid_dataset.Tables(0).Rows(0).Item(2)
            Dim sch_name As String
            Select Case mode
                Case "1"  'lsc campaign 1 - payment due 5-13 days
                    If cl_ID <> 909 Then
                        Continue For
                    End If
                    If sch_ID <> 727 Then
                        Continue For
                    End If
                    Dim sub_status As Integer
                    Try
                        sub_status = debtor_dataset.Tables(0).Rows(idx).Item(17)
                    Catch ex As Exception
                        sub_status = 0
                    End Try
                    'ignore if sub status is :=
                    '5 AOE
                    '11 HCE
                    '60 External HCE
                    '29 HighCourt Enforcement
                    '10 Charging order
                    '7 Clamping order
                    '19 App withdrawn
                    '21 ICO Satisfied
                    '9 Third Party Debt Order
                    '26 Defendant refund
                    '27 LAA for Approval
                    If sub_status = 5 Or sub_status = 11 Or sub_status = 60 Or sub_status = 29 Or _
                    sub_status = 10 Or sub_status = 7 Or sub_status = 19 Or sub_status = 21 Or _
                    sub_status = 9 Or sub_status = 26 Or sub_status = 27 Then
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "PreTrialContribution" And stage_name <> "InSancContribution" Then
                        Continue For
                    End If
                    Dim arr_next As Date
                    Try
                        arr_next = debtor_dataset.Tables(0).Rows(idx).Item(16)
                    Catch
                        Continue For
                    End Try
                    Dim due_in_days As Integer = DateDiff(DateInterval.Day, CDate(Now), arr_next)
                    If due_in_days < 5 Or due_in_days > 13 Then
                        Continue For
                    End If
                    'see if payment in last 7 days
                    param2 = "select date from Payment where debtorID = " & debtor & _
                    " and amount > 0 and (status = 'R' or status = 'W') order by date desc"
                    Dim pay_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows > 0 Then
                        Dim last_pay_date As Date
                        Try
                            last_pay_date = pay_dataset.Tables(0).Rows(0).Item(0)
                        Catch ex As Exception
                            Continue For
                        End Try
                        If DateDiff(DateInterval.Day, last_pay_date, Now) < 8 Then
                            Continue For
                        End If
                    End If
                    'if debtor is in prison - ignore
                    param2 = "select InPrison from Debtor where DebtorID = " & debtor
                    Dim debt_ds As DataSet = get_dataset("LSC", param2)
                    If no_of_rows = 1 Then
                        Dim inprison As String
                        Try
                            inprison = Microsoft.VisualBasic.Left(debt_ds.Tables(0).Rows(0).Item(0), 1)
                            If inprison = "Y" Then
                                Continue For
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                Case "2"  'lsc campaign 2 - payment due 14-21 days
                    If cl_ID <> 909 Then
                        Continue For
                    End If
                    If sch_ID <> 727 Then
                        Continue For
                    End If
                    Dim sub_status As Integer
                    Try
                        sub_status = debtor_dataset.Tables(0).Rows(idx).Item(17)
                    Catch ex As Exception
                        sub_status = 0
                    End Try
                    'ignore if sub status is :=
                    '5 AOE
                    '11 HCE
                    '60 External HCE
                    '29 HighCourt Enforcement
                    '10 Charging order
                    '7 Clamping order
                    '19 App withdrawn
                    '21 ICO Satisfied
                    '9 Third Party Debt Order
                    '26 Defendant refund
                    '27 LAA for Approval
                    If sub_status = 5 Or sub_status = 11 Or sub_status = 60 Or sub_status = 29 Or _
                    sub_status = 10 Or sub_status = 7 Or sub_status = 19 Or sub_status = 21 Or _
                    sub_status = 9 Or sub_status = 26 Or sub_status = 27 Then
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "PreTrialContribution" And stage_name <> "InSancContribution" Then
                        Continue For
                    End If
                    Dim arr_next As Date
                    Try
                        arr_next = debtor_dataset.Tables(0).Rows(idx).Item(16)
                    Catch
                        Continue For
                    End Try

                    Dim due_in_days As Integer = DateDiff(DateInterval.Day, CDate(Now), arr_next)
                    If due_in_days < 14 Or due_in_days > 21 Then
                        Continue For
                    End If
                    'see if payment in last 7 days
                    param2 = "select date from Payment where debtorID = " & debtor & _
                    " and amount > 0 and (status = 'R' or status = 'W') order by date desc"
                    Dim pay_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows > 0 Then
                        Dim last_pay_date As Date
                        Try
                            last_pay_date = pay_dataset.Tables(0).Rows(0).Item(0)
                        Catch ex As Exception
                            Continue For
                        End Try
                        If DateDiff(DateInterval.Day, last_pay_date, Now) < 8 Then
                            Continue For
                        End If
                    End If
                    'if debtor is in prison - ignore
                    param2 = "select InPrison from Debtor where DebtorID = " & debtor
                    Dim debt_ds As DataSet = get_dataset("LSC", param2)
                    If no_of_rows = 1 Then
                        Dim inprison As String
                        Try
                            inprison = Microsoft.VisualBasic.Left(debt_ds.Tables(0).Rows(0).Item(0), 1)
                            If inprison = "Y" Then
                                Continue For
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                Case "3"  'lsc campaign 3 - defaulted
                    If cl_ID <> 909 Then
                        Continue For
                    End If
                    If sch_ID <> 727 Then
                        Continue For
                    End If
                    Dim sub_status As Integer
                    Try
                        sub_status = debtor_dataset.Tables(0).Rows(idx).Item(17)
                    Catch ex As Exception
                        sub_status = 0
                    End Try
                    'ignore if sub status is :=
                    '5 AOE
                    '11 HCE
                    '60 External HCE
                    '29 HighCourt Enforcement
                    '10 Charging order
                    '7 Clamping order
                    '19 App withdrawn
                    '21 ICO Satisfied
                    '9 Third Party Debt Order
                    '26 Defendant refund
                    '27 LAA for Approval
                    If sub_status = 5 Or sub_status = 11 Or sub_status = 60 Or sub_status = 29 Or _
                    sub_status = 10 Or sub_status = 7 Or sub_status = 19 Or sub_status = 21 Or _
                    sub_status = 9 Or sub_status = 26 Or sub_status = 27 Then
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "PreTrial Enforcement" And stage_name <> "Default Letter Sent" _
                    And stage_name <> "Awaiting Outcome" Then
                        Continue For
                    End If
                    'see if payment in last 7 days
                    param2 = "select date from Payment where debtorID = " & debtor & _
                    " and amount > 0 and (status = 'R' or status = 'W') order by date desc"
                    Dim pay_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows > 0 Then
                        Dim last_pay_date As Date
                        Try
                            last_pay_date = pay_dataset.Tables(0).Rows(0).Item(0)
                        Catch ex As Exception
                            Continue For
                        End Try
                        If DateDiff(DateInterval.Day, last_pay_date, Now) < 8 Then
                            Continue For
                        End If
                    End If
                    'if debtor is in prison - ignore
                    param2 = "select InPrison from Debtor where DebtorID = " & debtor
                    Dim debt_ds As DataSet = get_dataset("LSC", param2)
                    If no_of_rows = 1 Then
                        Dim inprison As String
                        Try
                            inprison = Microsoft.VisualBasic.Left(debt_ds.Tables(0).Rows(0).Item(0), 1)
                            If inprison = "Y" Then
                                Continue For
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                Case "4"  'lsc campaign 4 - FDC cases
                    If cl_ID <> 909 Then
                        Continue For
                    End If
                    If sch_ID <> 727 And sch_ID <> 779 Then
                        Continue For
                    End If
                    Dim sub_status As Integer
                    Try
                        sub_status = debtor_dataset.Tables(0).Rows(idx).Item(17)
                    Catch ex As Exception
                        sub_status = 0
                    End Try
                    'ignore if sub status is :=
                    '5 AOE
                    '11 HCE
                    '60 External HCE
                    '29 HighCourt Enforcement
                    '10 Charging order
                    '7 Clamping order
                    '19 App withdrawn
                    '21 ICO Satisfied
                    '9 Third Party Debt Order
                    '26 Defendant refund
                    '27 LAA for Approval
                    If sub_status = 5 Or sub_status = 11 Or sub_status = 60 Or sub_status = 29 Or _
                    sub_status = 10 Or sub_status = 7 Or sub_status = 19 Or sub_status = 21 Or _
                    sub_status = 9 Or sub_status = 26 Or sub_status = 27 Then
                        Continue For
                    End If
                    'extra omissions
                    '28 CCO Letter sent
                    '23 land registry
                    '24 Experian
                    '84 K&E Completed
                    '83 Gone for QC
                    If sub_status = 28 Or sub_status = 23 Or sub_status = 24 Or sub_status = 84 Or _
                             sub_status = 8310 Then
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "FDC Being Processed" And stage_name <> "FDC Letter Sent" _
                    And stage_name <> "FDC Recd More Info" _
                    And stage_name <> "PosTrial Enforcement" _
                    And stage_name <> "FDC Default" And stage_name <> "FDC Letter Returned" Then
                        Continue For
                    End If

                    'If stage_name = "FDC Letter Sent" Then
                    '    'has to be at stage for 7 days
                    '    param2 = "select _createdDate, text from Note where type = 'Stage' " & _
                    '    " and debtorID = " & debtor & " order by _createdDate desc"
                    '    Dim note_ds As DataSet = get_dataset("onestep", param2)
                    '    Dim note_text As String = note_ds.Tables(0).Rows(0).Item(1)
                    '    Dim note_date As Date
                    '    Try
                    '        note_date = note_ds.Tables(0).Rows(0).Item(0)
                    '    Catch ex As Exception
                    '        MsgBox(ex.Message)
                    '        Continue For
                    '    End Try
                    '    note_date = DateAdd(DateInterval.Day, 7, note_date)
                    '    If note_date > Now Then
                    '        Continue For
                    '    End If
                    'End If
                    'see if payment in last 7 days
                    param2 = "select date from Payment where debtorID = " & debtor & _
                    " and amount > 0 and (status = 'R' or status = 'W') order by date desc"
                    Dim pay_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows > 0 Then
                        Dim last_pay_date As Date
                        Try
                            last_pay_date = pay_dataset.Tables(0).Rows(0).Item(0)
                        Catch ex As Exception
                            Continue For
                        End Try
                        If DateDiff(DateInterval.Day, last_pay_date, Now) < 8 Then
                            Continue For
                        End If
                    End If
                    'if debtor is in prison - ignore
                    param2 = "select InPrison from Debtor where DebtorID = " & debtor
                    Dim debt_ds As DataSet = get_dataset("LSC", param2)
                    If no_of_rows = 1 Then
                        Dim inprison As String
                        Try
                            inprison = Microsoft.VisualBasic.Left(debt_ds.Tables(0).Rows(0).Item(0), 1)
                            If inprison = "Y" Then
                                Continue For
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                Case "5"  'lsc campaign 5 - crystallized cases
                    If cl_ID <> 909 Then
                        Continue For
                    End If
                    If sch_ID <> 727 Then
                        Continue For
                    End If
                    Dim sub_status As Integer
                    Try
                        sub_status = debtor_dataset.Tables(0).Rows(idx).Item(17)
                    Catch ex As Exception
                        sub_status = 0
                    End Try
                    'ignore if sub status is :=
                    '5 AOE
                    '11 HCE
                    '60 External HCE
                    '29 HighCourt Enforcement
                    '10 Charging order
                    '7 Clamping order
                    '19 App withdrawn
                    '21 ICO Satisfied
                    '9 Third Party Debt Order
                    '26 Defendant refund
                    '27 LAA for Approval
                    If sub_status = 5 Or sub_status = 11 Or sub_status = 60 Or sub_status = 29 Or _
                    sub_status = 10 Or sub_status = 7 Or sub_status = 19 Or sub_status = 21 Or _
                    sub_status = 9 Or sub_status = 26 Or sub_status = 27 Then
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "Awaiting FDCs" Then
                        Continue For
                    End If

                    'see if payment in last 7 days
                    param2 = "select date from Payment where debtorID = " & debtor & _
                    " and amount > 0 and (status = 'R' or status = 'W') order by date desc"
                    Dim pay_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows > 0 Then
                        Dim last_pay_date As Date
                        Try
                            last_pay_date = pay_dataset.Tables(0).Rows(0).Item(0)
                        Catch ex As Exception
                            Continue For
                        End Try
                        If DateDiff(DateInterval.Day, last_pay_date, Now) < 8 Then
                            Continue For
                        End If
                    End If


                    'if debtor is in prison - ignore
                    param2 = "select InPrison from Debtor where DebtorID = " & debtor
                    Dim debt_ds As DataSet = get_dataset("LSC", param2)
                    If no_of_rows = 1 Then
                        Dim inprison As String
                        Try
                            inprison = Microsoft.VisualBasic.Left(debt_ds.Tables(0).Rows(0).Item(0), 1)
                            If inprison = "Y" Then
                                Continue For
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                Case "6" ' return stage cmec
                    If branchID <> 1 Then
                        Continue For
                    End If
                    param2 = "select work_type from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    Dim work_type As Integer = scheme_dataset.Tables(0).Rows(0).Item(0)
                    If work_type <> 12 Then
                        Continue For
                    End If

                    'if allocated Ok if 3031 to 3033
                    Dim bail_current As String = debtor_dataset.Tables(0).Rows(idx).Item(16)
                    If bail_current = "Y" Then
                        Dim alloc_bail As Integer = debtor_dataset.Tables(0).Rows(idx).Item(17)
                        If alloc_bail < 3031 Or alloc_bail > 3033 Then
                            Continue For
                        End If
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "(Return)" And _
                    stage_name <> "Collect Pre Return" And _
                    stage_name <> "Collect-Contact Card" And _
                    stage_name <> "Pre Committal Letter" Then
                        Continue For
                    End If
                Case "7"  'lsc campaign CCO value
                    If cl_ID <> 909 Then
                        Continue For
                    End If
                    If sch_ID <> 727 And sch_ID <> 779 Then
                        Continue For
                    End If
                    Dim sub_status As Integer
                    Try
                        sub_status = debtor_dataset.Tables(0).Rows(idx).Item(17)
                    Catch ex As Exception
                        sub_status = 0
                    End Try
                    'ignore if sub status is not CCO Letter sent (28)
                   
                    If sub_status <> 28 Then
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                   
                    If stage_name <> "FDC Letter Sent" And stage_name <> "FDC Default" And _
                    stage_name <> "FDC Letter Returned" And stage_name <> "PosTrial Enforcement" Then
                        Continue For
                    End If
                    If stage_name = "FDC Letter Sent" Then
                        'has to be at stage for 7 days
                        param2 = "select _createdDate, text from Note where type = 'Stage' " & _
                        " and debtorID = " & debtor & " order by _createdDate desc"
                        Dim note_ds As DataSet = get_dataset("onestep", param2)
                        Dim note_text As String = note_ds.Tables(0).Rows(0).Item(1)
                        Dim note_date As Date
                        Try
                            note_date = note_ds.Tables(0).Rows(0).Item(0)
                        Catch ex As Exception
                            MsgBox(ex.Message)
                            Continue For
                        End Try
                        note_date = DateAdd(DateInterval.Day, 7, note_date)
                        If note_date > Now Then
                            Continue For
                        End If
                    End If
                    'has to be at stage for at least 28 days
                    'param2 = "select _createdDate, text from Note where type = 'Stage' " & _
                    '" and debtorID = " & debtor & " order by _createdDate desc"
                    'Dim note_ds As DataSet = get_dataset("onestep", param2)
                    'Dim note_text As String = note_ds.Tables(0).Rows(0).Item(1)
                    'Dim note_idx As Integer = InStr(note_text, "chg")
                    'If note_idx > 0 Then  'note found so check date
                    '    Dim note_date As Date
                    '    Try
                    '        note_date = note_ds.Tables(0).Rows(0).Item(0)
                    '    Catch ex As Exception
                    '        MsgBox(ex.Message)
                    '        Continue For
                    '    End Try
                    '    note_date = DateAdd(DateInterval.Day, 28, note_date)
                    '    If note_date > Now Then
                    '        Continue For
                    '    End If
                    'End If
                    'if debtor is in prison - ignore
                    param2 = "select InPrison from Debtor where DebtorID = " & debtor
                    Dim debt_ds As DataSet = get_dataset("LSC", param2)
                    If no_of_rows = 1 Then
                        Dim inprison As String
                        Try
                            inprison = Microsoft.VisualBasic.Left(debt_ds.Tables(0).Rows(0).Item(0), 1)
                            If inprison = "Y" Then
                                Continue For
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                Case "8"   'levy and van fees not paid and no broken arrangement
                    If branchID <> 1 Then
                        Continue For
                    End If
                    param2 = "select type from Fee where debtorID = " & debtor & _
                                       " and fee_amount > 0 and (fee_remit_col = 3 or fee_remit_col = 4)"
                    Dim fee_dataset As DataSet = get_dataset("onestep", param2)
                    Dim fee_idx As Integer
                    Dim levy_found As Boolean = False
                    Dim van_fees_found As Boolean = False
                    For fee_idx = 0 To no_of_rows - 1
                        Dim fee_type As String = LCase(fee_dataset.Tables(0).Rows(fee_idx).Item(0))
                        If InStr(fee_type, "lev") > 0 Then
                            levy_found = True
                        ElseIf InStr(fee_type, "van") > 0 Then
                            van_fees_found = True
                        End If
                    Next
                    If levy_found = False Or van_fees_found = False Then
                        Continue For
                    End If
                    'arrangebroken count should be zero
                    If debtor_dataset.Tables(0).Rows(idx).Item(20) > 0 Then
                        Continue For
                    End If
                Case "9"   'levy and van fees and broken arrangement
                    If branchID <> 1 Then
                        Continue For
                    End If
                    param2 = "select type from Fee where debtorID = " & debtor & _
                                       " and fee_amount > 0 and (fee_remit_col = 3 or fee_remit_col = 4)"
                    Dim fee_dataset As DataSet = get_dataset("onestep", param2)
                    Dim fee_idx As Integer
                    Dim levy_found As Boolean = False
                    Dim van_fees_found As Boolean = False
                    For fee_idx = 0 To no_of_rows - 1
                        Dim fee_type As String = LCase(fee_dataset.Tables(0).Rows(fee_idx).Item(0))
                        If InStr(fee_type, "lev") > 0 Then
                            levy_found = True
                        ElseIf InStr(fee_type, "van") > 0 Then
                            van_fees_found = True
                        End If
                    Next
                    If levy_found = False Or van_fees_found = False Then
                        Continue For
                    End If
                    'arrangebroken count should be > zero
                    If debtor_dataset.Tables(0).Rows(idx).Item(20) = 0 Then
                        Continue For
                    End If
                Case "10"   'levy and van fees and paid
                    If branchID <> 1 Then
                        Continue For
                    End If
                    param2 = "select type from Fee where debtorID = " & debtor & _
                                       " and fee_amount > 0 and (fee_remit_col = 3 or fee_remit_col = 4)"
                    Dim fee_dataset As DataSet = get_dataset("onestep", param2)
                    Dim fee_idx As Integer
                    Dim levy_found As Boolean = False
                    Dim van_fees_found As Boolean = False
                    For fee_idx = 0 To no_of_rows - 1
                        Dim fee_type As String = LCase(fee_dataset.Tables(0).Rows(fee_idx).Item(0))
                        If InStr(fee_type, "lev") > 0 Then
                            levy_found = True
                        ElseIf InStr(fee_type, "van") > 0 Then
                            van_fees_found = True
                        End If
                    Next
                    If levy_found = False Or van_fees_found = False Then
                        Continue For
                    End If
                Case "11"   'employed bailiffs
                    'check if visit at van stage
                    Dim include_case As Boolean = False
                    param2 = "select stageName, bailiffID from Visit where debtorID = " & debtor
                    Dim visit_dataset As DataSet = get_dataset("onestep", param2)
                    Dim visit_idx As Integer
                    For visit_idx = 0 To no_of_rows - 1
                        Dim stage_name As String
                        Try
                            stage_name = visit_dataset.Tables(0).Rows(visit_idx).Item(0)
                        Catch ex As Exception
                            Continue For
                        End Try
                        If InStr(LCase(stage_name), "van") > 0 Then
                            'see if visit was by an employed bailiff
                            Dim bailID As Integer
                            Try
                                bailID = visit_dataset.Tables(0).Rows(visit_idx).Item(1)
                            Catch ex As Exception
                                Continue For
                            End Try
                            param2 = "select typeSub from Bailiff where _rowid = " & bailID
                            Dim bail_ds As DataSet = get_dataset("onestep", param2)
                            Try
                                If bail_ds.Tables(0).Rows(0).Item(0) = "Employed" Then
                                    include_case = True
                                    Exit For
                                End If
                            Catch ex As Exception

                            End Try
                        End If
                    Next
                    If Not include_case Then
                        Continue For
                    End If
                Case "12"
                    'arrow scheme group
                    If cl_ID <> 1160 And cl_ID <> 986 And cl_ID <> 1301 Then
                        Continue For
                    End If
                    param2 = "select name from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    sch_name = scheme_dataset.Tables(0).Rows(0).Item(0)
                    If InStr(sch_name, sch_group) = 0 Then
                        If Left(sch_name, sch_group.Length) <> Left(sch_group, sch_group.Length) Then
                            Continue For
                        End If
                    End If
                Case "13"
                    'CPW scheme group
                    If cl_ID <> 1414 Then
                        Continue For
                    End If
                    param2 = "select name from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    sch_name = scheme_dataset.Tables(0).Rows(0).Item(0)
                    If InStr(sch_name, sch_group) = 0 Then
                        If Left(sch_name, sch_group.Length) <> Left(sch_group, sch_group.Length) Then
                            Continue For
                        End If
                    End If
                Case "14"
                    'High Court cases to outcall (like RA1714)
                    If cl_ID <> 1526 And cl_ID <> 1608 And cl_ID <> 1611 Then
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "FurtherVanAttendance" And stage_name <> "High Court Officer" And _
                    stage_name <> "Van Attendance" Then
                        Continue For
                    End If
                    'ignore cases allocated unless agent type is B
                    If debtor_dataset.Tables(0).Rows(idx).Item(18) = "Y" Then  'bail_current
                        Dim bailiffID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(19)
                        param2 = "select agent_type from Bailiff where _rowid = " & bailiffID
                        Dim bail_ds As DataSet = get_dataset("onestep", param2)
                        If bail_ds.Tables(0).Rows(0).Item(0) <> "B" Then
                            Continue For
                        End If
                    End If
                Case "15"  'vulnerable collect and HMRC
                    If branchID <> 2 And branchID <> 8 Then
                        Continue For
                    End If
                    param2 = "select isVulnerable from Debtor where _rowid = " & debtor
                    Dim vul_ds As DataSet = get_dataset("onestep", param2)
                    If vul_ds.Tables(0).Rows(0).Item(0) = "N" Then
                        Continue For
                    End If
                Case "16"  'TMA - R scheme
                    If branchID <> 2 Then
                        Continue For
                    End If
                    param2 = "select name from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    sch_name = scheme_dataset.Tables(0).Rows(0).Item(0)
                    If Microsoft.VisualBasic.Left(sch_name, 3) <> "TMA" Then
                        Continue For
                    End If
                Case "17"  'HBOP - M scheme
                    If branchID <> 2 Then
                        Continue For
                    End If
                    param2 = "select name from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    sch_name = scheme_dataset.Tables(0).Rows(0).Item(0)
                    If sch_name <> "HBOP - M" Then
                        Continue For
                    End If
                Case "18"
                    'leeds tma fees >=�149
                Case "19"
                    'collect TMA cases
                    If branchID <> 2 Then
                        Continue For
                    End If
                    param2 = "select name from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    sch_name = LCase(scheme_dataset.Tables(0).Rows(0).Item(0))
                    If InStr(sch_name, "tma") = 0 Then
                        Continue For
                    End If
                Case "20"
                    'HMRC
                    If branchID <> 8 Then
                        Continue For
                    End If
                    'get age of case
                    Dim caseAge As Integer = DateDiff(DateInterval.Day, created_date, Now)
                    If Agefrm.age1rbtn.Checked Then
                        If caseAge > 28 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age2rbtn.Checked Then
                        If caseAge < 29 Or caseAge > 56 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age3rbtn.Checked Then
                        If caseAge < 57 Or caseAge > 90 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age4rbtn.Checked Then
                        If caseAge < 90 Or caseAge > 180 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age5rbtn.Checked Then
                        If caseAge < 181 Then
                            Continue For
                        End If
                    End If
                Case "21"
                    'pre comm letter1
                    If branchID <> 1 Then
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "Pre Comm Letter 1" Then
                        Continue For
                    End If
                Case "22"
                    'pre comm letter2
                    If branchID <> 1 Then
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "Pre Comm Letter 2" Then
                        Continue For
                    End If
                Case "23"
                    'further van not RTD
                    If branchID <> 1 Then
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "FurtherVanAttendance" Then
                        Continue For
                    End If
                    param2 = "select work_type from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    Dim work_type As Integer = scheme_dataset.Tables(0).Rows(0).Item(0)
                    If work_type = 16 Or work_type = 20 Then
                        Continue For
                    End If
                Case "24"
                    'further van ONLY RTD
                    If branchID <> 1 Then
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "FurtherVanAttendance" Then
                        Continue For
                    End If
                    param2 = "select work_type from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    Dim work_type As Integer = scheme_dataset.Tables(0).Rows(0).Item(0)
                    If work_type <> 16 And work_type <> 20 Then
                        Continue For
                    End If
                Case "25"  'financial services - Arrow, CPW, 247 moneybox, motormile, Aktiv Kap, student loands, Cash comverters
                    If branchID <> 2 Then
                        Continue For
                    End If
                    If cl_ID <> 1765 And cl_ID <> 1160 And cl_ID <> 986 And cl_ID <> 1301 _
                    And cl_ID <> 1414 And cl_ID <> 1807 And cl_ID <> 1792 And cl_ID <> 1227 _
                    And cl_ID <> 1228 And cl_ID <> 1647 And cl_ID <> 1572 And cl_ID <> 1314 Then
                        Continue For
                    End If
                Case "26"  'DVLA
                    If cl_ID <> 1805 Then
                        Continue For
                    End If
                    'get age of case
                    Dim caseAge As Integer = DateDiff(DateInterval.Day, created_date, Now)
                    If Agefrm.age1rbtn.Checked Then
                        If caseAge > 28 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age2rbtn.Checked Then
                        If caseAge < 29 Or caseAge > 57 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age3rbtn.Checked Then
                        If caseAge < 58 Or caseAge > 85 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age4rbtn.Checked Then
                        If caseAge < 86 Or caseAge > 114 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age5rbtn.Checked Then
                        If caseAge < 115 Or caseAge > 143 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age6rbtn.Checked Then
                        If caseAge < 144 Or caseAge > 172 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age7rbtn.Checked Then
                        If caseAge < 173 Or caseAge > 201 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age8rbtn.Checked Then
                        If caseAge < 202 Then
                            Continue For
                        End If
                    End If
                Case "27"  'Arrow by case age
                    'arrow scheme group
                    If cl_ID <> 1160 And cl_ID <> 986 And cl_ID <> 1301 Then
                        Continue For
                    End If
                    'get age of case
                    Dim caseAge As Integer = DateDiff(DateInterval.Day, created_date, Now)
                    If Agefrm.age1rbtn.Checked Then
                        If caseAge > 28 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age2rbtn.Checked Then
                        If caseAge < 29 Or caseAge > 56 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age3rbtn.Checked Then
                        If caseAge < 57 Or caseAge > 90 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age4rbtn.Checked Then
                        If caseAge < 90 Or caseAge > 180 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age5rbtn.Checked Then
                        If caseAge < 181 Then
                            Continue For
                        End If
                    End If
                Case "28"  'Lowell by case age
                    If cl_ID <> 1815 Then
                        Continue For
                    End If
                    'get age of case
                    Dim caseAge As Integer = DateDiff(DateInterval.Day, created_date, Now)
                    If Agefrm.age1rbtn.Checked Then
                        If caseAge > 28 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age2rbtn.Checked Then
                        If caseAge < 29 Or caseAge > 56 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age3rbtn.Checked Then
                        If caseAge < 57 Or caseAge > 90 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age4rbtn.Checked Then
                        If caseAge < 90 Or caseAge > 180 Then
                            Continue For
                        End If
                    ElseIf Agefrm.age5rbtn.Checked Then
                        If caseAge < 181 Then
                            Continue For
                        End If
                    End If
                Case "29"  'DVLA broken arrangemments
                    If cl_ID <> 1805 Then
                        Continue For
                    End If

                Case "A"  'arrangements started > 3 months
                    If branchID <> 2 Then
                        Continue For
                    End If
                Case "B"   'payment or broken
                    If branchID <> 2 Then
                        Continue For
                    End If
                    If cl_ID = 1523 Then
                        Continue For
                    End If
                Case "C"   'choose client
                    If cl_ID <> selected_client Then
                        Continue For
                    End If
                    If cl_ID = 909 Then
                        Dim sub_status As Integer
                        Try
                            sub_status = debtor_dataset.Tables(0).Rows(idx).Item(16)
                        Catch ex As Exception
                            sub_status = 0
                        End Try
                        If sub_status = 60 Then 'External HCE
                            Continue For
                        End If
                    End If
                Case "D"
                    'hmrc paid or broken arrangement
                    If branchID <> 8 Then
                        Continue For
                    End If
                Case "E"   'ccmt campaigns
                    If csid <> 1892 Then
                        Continue For
                    End If
                    Dim sub_status As Integer
                    Try
                        sub_status = debtor_dataset.Tables(0).Rows(idx).Item(16)
                    Catch ex As Exception
                        sub_status = 0
                    End Try
                    If sub_status = 60 Then 'External HCE
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    Dim select_case As Boolean = False
                    If stage_name = "Awaiting Outcome" Or stage_name = "Awaiting FDCs" _
                    Or stage_name = "FDC Value Received" Then
                        If sub_status = 18 Or sub_status = 20 Then
                            select_case = True
                        End If
                    End If
                    If select_case = False Then
                        If stage_name <> "Default Letter Sent" And stage_name <> "PreTrial Enforcement" Then
                            Continue For
                        End If
                    End If

                Case "F"
                    If branchID <> 1 Or cl_ID = 55 Then  'ignore Savills
                        Continue For
                    End If
                    'check postcode sector
                    Dim pcode As String
                    Try
                        pcode = debtor_dataset.Tables(0).Rows(idx).Item(13)
                    Catch ex As Exception
                        pcode = ""
                    End Try
                    If pcode.Length > 0 Then
                        If found_pc_sector(pcode) = False Then
                            Continue For
                        End If
                    Else
                        Continue For
                    End If
                    param2 = "select name from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    Dim scheme_name As String = Trim(scheme_dataset.Tables(0).Rows(0).Item(0))
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If Microsoft.VisualBasic.Left(scheme_name, 4) = "NNDR" And stage_name <> "(Return)" Then
                        Continue For
                    End If
                Case "G"  'CMEC OOA
                    'CSA/CMEC/CMECS
                    'If cl_ID < 1294 Or cl_ID > 1331 Then
                    '    Continue For
                    'End If
                    'If cl_ID > 1300 And cl_ID < 1325 Then
                    '    Continue For
                    'End If
                    param2 = "select work_type from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    If scheme_dataset.Tables(0).Rows(0).Item(0) <> 12 Then
                        Continue For
                    End If
                    'check postcode sector
                    Dim pcode As String
                    Try
                        pcode = debtor_dataset.Tables(0).Rows(idx).Item(13)
                    Catch ex As Exception
                        pcode = ""
                    End Try
                    If pcode.Length > 0 Then
                        If found_pc_sector(pcode) = False Then
                            Continue For
                        End If
                    Else
                        Continue For
                    End If
                Case "H"  'Hit team
                    If branchID <> 1 Then
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    'van attendance and awaiting van approval only for Cornwall
                    If stage_name = "Van Attendance" Or stage_name = "AwaitingVanApproval" Then
                        If (cl_ID = 949 Or cl_ID = 955 Or cl_ID = 163 Or cl_ID = 160 Or cl_ID = 211 _
                                            Or cl_ID = 153 Or cl_ID = 154 Or cl_ID = 648 Or cl_ID = 155 Or cl_ID = 161 Or cl_ID = 381 _
                                            Or cl_ID = 628) Then
                        Else
                            Continue For  'not Cornwall
                        End If
                    Else
                        If stage_name <> "FurtherVanAttendance" And stage_name <> "(Return)" Then
                            Continue For
                        End If
                    End If

                    If (cl_ID = 50 Or cl_ID = 54 Or cl_ID = 82 Or cl_ID = 141) And stage_name = "FurtherVanAttendance" Then
                        Continue For  'Liverpool,Manchester leeds and trafford
                    End If

                    If sch_ID = 41 Or sch_ID = 676 Or sch_ID = 773 Then
                        Continue For  'RTD
                    End If
                    param2 = "select name from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    Dim scheme_name As String = Trim(scheme_dataset.Tables(0).Rows(0).Item(0))
                    If Microsoft.VisualBasic.Left(scheme_name, 4) = "NNDR" And stage_name <> "(Return)" Then
                        Continue For
                    End If
                    'check postcode sector
                    Dim pcode As String
                    Try
                        pcode = debtor_dataset.Tables(0).Rows(idx).Item(13)
                    Catch ex As Exception
                        pcode = ""
                    End Try
                    If pcode.Length > 0 Then
                        If found_pc_sector(pcode) = True Then
                            Continue For
                        End If
                    End If
                    'check that debtor does not just have a levy fee - must have van as well
                    param2 = "select type from Fee where debtorID = " & debtor & _
                    " and fee_amount > 0 and (fee_remit_col = 3 or fee_remit_col = 4)"
                    Dim fee_dataset As DataSet = get_dataset("onestep", param2)
                    Dim fee_idx As Integer
                    Dim levy_found As Boolean = False
                    Dim van_fees_found As Boolean = False
                    For fee_idx = 0 To no_of_rows - 1
                        Dim fee_type As String = LCase(fee_dataset.Tables(0).Rows(fee_idx).Item(0))
                        If InStr(fee_type, "lev") > 0 Then
                            levy_found = True
                        ElseIf InStr(fee_type, "van") > 0 Then
                            van_fees_found = True
                        End If
                    Next
                    If levy_found And Not van_fees_found Then
                        Continue For
                    End If
                Case "I"
                    'CSA/CMEC/CMECS
                    param2 = "select work_type from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    If scheme_dataset.Tables(0).Rows(0).Item(0) <> 12 Then
                        Continue For
                    End If
                    'If cl_ID < 1294 Or cl_ID > 1331 Then
                    '    Continue For
                    'End If
                    'If cl_ID > 1300 And cl_ID < 1325 Then
                    '    Continue For
                    'End If
                    ''include cases with a broken arrangement
                    'Dim include_case As Boolean = False
                    'Dim broken_arr As String = debtor_dataset.Tables(0).Rows(idx).Item(16)
                    'If broken_arr = "Y" Then
                    '    include_case = True
                    'End If
                    'If Not include_case Then
                    '    include cases with fees>=300
                    '    param2 = "select sum(fee_amount) from Fee where debtorID = " & debtor & _
                    '     " and fee_remit_col > 2"
                    '    Dim fee_dataset As DataSet = get_dataset("onestep", param2)
                    '    Dim fee_amt As Decimal = fee_dataset.Tables(0).Rows(0).Item(0)
                    '    If fee_amt >= 300 Then
                    '        include_case = True
                    '    End If
                    'End If
                    'If Not include_case Then
                    '    'include cases at van stage or later 
                    '    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    '    param2 = "select name, sort from Stage where _rowid = " & stageID
                    '    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    '    If no_of_rows <> 1 Then
                    '        MsgBox("Unable to read stage name for stageID = " & stageID)
                    '        Exit Sub
                    '    End If
                    '    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    '    Dim sort_no As Decimal = Trim(stage_dataset.Tables(0).Rows(0).Item(1))
                    '    If sort_no < 500 Then
                    '        Continue For
                    '    End If
                    '    If stage_name = "(Return)" Then
                    '        Continue For
                    '    End If
                    '    'check if visit at van stage
                    '    param2 = "select stageName from Visit where debtorID = " & debtor & _
                    '    " and date_visited is not null"
                    '    Dim visit_dataset As DataSet = get_dataset("onestep", param2)
                    '    Dim visit_idx As Integer
                    '    For visit_idx = 0 To no_of_rows - 1
                    '        If InStr(LCase(visit_dataset.Tables(0).Rows(visit_idx).Item(0)), "van") > 0 Then
                    '            include_case = True
                    '            Exit For
                    '        End If
                    '    Next
                    '    If Not include_case Then
                    '        Continue For
                    '    End If
                    'End If
                Case "J"
                    'fees only branch 1
                    If branchID <> 1 Then
                        Continue For
                    End If

                Case "K"  'csa cases
                    param2 = "select name from Client where _rowid = " & cl_ID
                    Dim client_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read client for clID = " & cl_ID)
                        Exit Sub
                    End If
                    Dim client_name As String = Trim(client_dataset.Tables(0).Rows(0).Item(0))
                    If InStr(client_name, "CSA") = 0 And _
                    InStr(client_name, "CMEC") = 0 Then
                        Continue For
                    End If
                    'check that 7 day letter sent > 5days ago or van on way letter sent
                    param2 = " select _createdDate, text from Note where type = 'Letter'" & _
                    " and debtorID = " & debtor
                    Dim note_dataset As DataSet = get_dataset(param1, param2)
                    If no_of_rows = 0 Then
                        outline = ""
                        Exit For
                    End If
                    Dim letter_found As Boolean = False
                    Dim idx3 As Integer
                    For idx3 = 0 To no_of_rows - 1
                        Dim note_text As String = note_dataset.Tables(0).Rows(idx3).Item(1)
                        If InStr(note_text, "VanOnWay") > 0 Then
                            letter_found = True
                            Exit For
                        End If
                        If InStr(note_text, "7Day") > 0 Then
                            Dim note_date As Date = note_dataset.Tables(0).Rows(idx3).Item(0)
                            If DateDiff(DateInterval.Day, note_date, Now) > 5 Then
                                letter_found = True
                                Exit For
                            End If
                        End If
                    Next
                    If letter_found = False Then
                        Continue For
                    End If
                Case "O" 'other clients
                    If branchID <> 2 Or cl_ID = 1045 Or cl_ID = 1159 Or cl_ID = 1227 Or cl_ID = 1228 _
                    Or cl_ID = 1160 Or cl_ID = 1245 Or cl_ID = 1301 Or cl_ID = 1351 Or cl_ID = 1293 Then
                        Continue For
                    End If
                    collect_stage.Dialler_clientsTableAdapter.FillBy(collect_stage.FeesSQLDataSet.Dialler_clients, cl_ID)
                    If collect_stage.FeesSQLDataSet.Dialler_clients.Rows.Count <> 0 Then
                        Continue For
                    End If
                Case "P"  'priority clients
                    If branchID <> 2 Then
                        Continue For
                    End If
                    collect_stage.Dialler_clientsTableAdapter.FillBy(collect_stage.FeesSQLDataSet.Dialler_clients, cl_ID)
                    If collect_stage.FeesSQLDataSet.Dialler_clients.Rows.Count = 0 Then
                        Continue For
                    End If
                    If collect_stage.FeesSQLDataSet.Dialler_clients.Rows(0).Item(1) = False Then
                        Continue For
                    End If
                Case "R"  'RTD cases
                    If branchID <> 1 Then
                        Continue For
                    End If
                    If sch_ID <> 41 And sch_ID <> 676 And sch_ID <> 773 Then
                        Continue For  'RTD
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "FurtherVanAttendance" And stage_name <> "Van Attendance" And _
                    stage_name <> "F C - Bailiff" And stage_name <> "AwaitingVanApproval" And _
                    stage_name <> "(Return)" Then
                        Continue For
                    End If
                    If stage_name <> "(Return)" Then
                        Dim last_date_found As Boolean = True
                        Try
                            Dim last_date As Date = debtor_dataset.Tables(0).Rows(idx).Item(14)
                        Catch ex As Exception
                            last_date_found = False
                        End Try
                        If last_date_found Then
                            Continue For
                        End If
                    End If
                    'check postcode sector
                    Dim pcode As String
                    Try
                        pcode = debtor_dataset.Tables(0).Rows(idx).Item(13)
                    Catch ex As Exception
                        pcode = ""
                    End Try
                    If pcode.Length > 0 Then
                        If found_pc_sector(pcode) = True Then
                            Continue For
                        End If
                    End If
                Case "S"  'special hit team
                    If branchID <> 1 Then
                        Continue For
                    End If
                    If cl_ID <> 50 And cl_ID <> 54 And cl_ID <> 82 And cl_ID <> 141 Then
                        Continue For  'Liverpool,Manchester leeds and trafford 
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "FurtherVanAttendance" And stage_name <> "Van Attendance" Then
                        Continue For
                    End If
                    If sch_ID = 41 Or sch_ID = 676 Or sch_ID = 773 Then
                        Continue For  'RTD
                    End If
                    'check postcode sector
                    Dim pcode As String
                    Try
                        pcode = debtor_dataset.Tables(0).Rows(idx).Item(13)
                    Catch ex As Exception
                        pcode = ""
                    End Try
                    If pcode.Length > 0 Then
                        If found_pc_sector(pcode) = True Then
                            Continue For
                        End If
                    End If
                    'check that debtor does not just have a levy fee - must have van as well
                    param2 = "select type from Fee where debtorID = " & debtor & _
                    " and fee_amount > 0 and (fee_remit_col = 3 or fee_remit_col = 4)"
                    Dim fee_dataset As DataSet = get_dataset("onestep", param2)
                    Dim fee_idx As Integer
                    Dim levy_found As Boolean = False
                    Dim van_fees_found As Boolean = False
                    For fee_idx = 0 To no_of_rows - 1
                        Dim fee_type As String = LCase(fee_dataset.Tables(0).Rows(fee_idx).Item(0))
                        If InStr(fee_type, "lev") > 0 Then
                            levy_found = True
                        ElseIf InStr(fee_type, "van") > 0 Then
                            van_fees_found = True
                        End If
                    Next
                    If levy_found And Not van_fees_found Then
                        Continue For
                    End If
                Case "T"  'top20 clients
                    If branchID <> 2 Then
                        Continue For
                    End If
                    collect_stage.Dialler_clientsTableAdapter.FillBy(collect_stage.FeesSQLDataSet.Dialler_clients, cl_ID)
                    If collect_stage.FeesSQLDataSet.Dialler_clients.Rows.Count = 0 Then
                        Continue For
                    End If
                    If collect_stage.FeesSQLDataSet.Dialler_clients.Rows(0).Item(1) = True Then
                        Continue For
                    End If
                Case "U"
                    'stacked cases with no link in arrangement
                    Dim linkID As Integer
                    Try
                        linkID = debtor_dataset.Tables(0).Rows(idx).Item(16)
                    Catch ex As Exception
                        linkID = 0
                    End Try
                    If linkID > 0 Then
                        'see if link in arrangement
                        param2 = "select _rowid from Debtor where linkID = " & linkID & _
                        " and status = 'A' and _rowid<> " & debtor
                        Dim link_dataset As DataSet = get_dataset("onestep", param2)
                        If no_of_rows > 0 Then
                            Continue For
                        End If
                    End If
                    'ignore NNDR cases
                    param2 = "select work_type from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    If scheme_dataset.Tables(0).Rows(0).Item(0) = 3 Then
                        Continue For
                    End If
                Case "UU"
                    'United Utilities scheme group
                    If cl_ID <> 1662 Then
                        Continue For
                    End If
                    'check client scheme is required
                    Select Case UU_selected_index
                        Case 0  'Domestic
                            If csid <> 3393 And csid <> 3394 And csid <> 3391 And csid <> 3392 And csid <> 3396 Then
                                Continue For
                            End If
                        Case 1  'Commercial
                            If csid <> 3399 And csid <> 3400 And csid <> 3397 And csid <> 3398 And csid <> 3395 Then
                                Continue For
                            End If
                        Case 2
                            'Domestic ignore
                            If csid = 3393 Or csid = 3394 Or csid = 3391 Or csid = 3392 Or csid = 3396 Then
                                Continue For
                            End If
                            'Commercial ignore
                            If csid = 3399 Or csid = 3400 Or csid = 3397 Or csid = 3398 Or csid = 3395 Then
                                Continue For
                            End If
                    End Select

                Case "W"  'enforcement top clients
                    If branchID = 2 Then
                        Continue For
                    End If
                    schemefrm.Dialler_bailiff_clientsTableAdapter.FillBy(schemefrm.FeesSQLDataSet.Dialler_bailiff_clients, cl_ID)
                    If schemefrm.FeesSQLDataSet.Dialler_bailiff_clients.Rows.Count = 0 Then
                        Continue For
                    End If
                    If selected_scheme_rows > 0 Then
                        Dim sch_found As Boolean = False
                        param2 = "select name from Scheme where _rowid = " & sch_ID
                        Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                        If no_of_rows = 0 Then
                            MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                            Exit Sub
                        End If
                        Dim scheme_name As String = Trim(scheme_dataset.Tables(0).Rows(0).Item(0))
                        Dim sch_idx As Integer
                        For sch_idx = 0 To selected_scheme_rows - 1
                            If scheme_name = selected_schemes(sch_idx) Then
                                sch_found = True
                                Exit For
                            End If
                        Next
                        If Not sch_found Then
                            Continue For
                        End If
                    End If
                Case "X"  'return stage inc HMRC
                    If branchID <> 2 And branchID <> 8 Then
                        Continue For
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "(Return)" Then
                        Continue For
                    End If
                Case "Y"  'return stage not cmec
                    If branchID <> 1 Then
                        Continue For
                    End If
                    'if allocated Ok if 3031 to 3033

                    Dim bail_current As String = debtor_dataset.Tables(0).Rows(idx).Item(16)
                    If bail_current = "Y" Then
                        Dim alloc_bail As Integer = debtor_dataset.Tables(0).Rows(idx).Item(17)
                        If alloc_bail < 3031 Or alloc_bail > 3033 Then
                            Continue For
                        End If
                    End If
                    Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                    param2 = "select name from Stage where _rowid = " & stageID
                    Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read stage name for stageID = " & stageID)
                        Exit Sub
                    End If
                    Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                    If stage_name <> "(Return)" And _
                    stage_name <> "Collect Pre Return" And _
                    stage_name <> "Collect-Contact Card" Then
                        Continue For
                    End If
                    param2 = "select work_type from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    Dim work_type As Integer = scheme_dataset.Tables(0).Rows(0).Item(0)
                    If work_type = 12 Then
                        Continue For
                    End If

                Case "Z"  'NNDR cases
                    If branchID <> 1 Then
                        Continue For
                    End If
                    param2 = "select work_type from Scheme where _rowid = " & sch_ID
                    Dim scheme_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                        Exit Sub
                    End If
                    If scheme_dataset.Tables(0).Rows(0).Item(0) <> 3 Then
                        Continue For
                    End If
            End Select

            'legal services - just at  PreTrialEnforcement
            If selected_client = 909 And ccmt_offence_val = 0 Then
                Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                param2 = "select name from Stage where _rowid = " & stageID
                Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows <> 1 Then
                    MsgBox("Unable to read stage name for stageID = " & stageID)
                    Exit Sub
                End If
                Dim stage_name As String = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
                If stage_name <> "PreTrial Enforcement" Then
                    Continue For
                End If
            End If
            If selected_stage_rows > 0 Then
                Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                param2 = "select name from Stage where _rowid = " & stageID
                Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows <> 1 Then
                    MsgBox("Unable to read stage name for stageID = " & stageID)
                    Exit Sub
                End If
                Dim stage_name As String = stage_dataset.Tables(0).Rows(0).Item(0)
                Dim stage_found As Boolean = False
                Dim stage_idx As Integer
                For stage_idx = 0 To selected_stage_rows - 1
                    If stage_name = selected_stages(stage_idx) Then
                        stage_found = True
                        Exit For
                    End If
                Next
                If Not stage_found Then
                    Continue For
                End If
            End If

            If selected_stage_name <> "" Then
                Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                param2 = "select name from Stage where _rowid = " & stageID
                Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows <> 1 Then
                    MsgBox("Unable to read stage name for stageID = " & stageID)
                    Exit Sub
                End If
                Dim stage_name As String = stage_dataset.Tables(0).Rows(0).Item(0)
                If InStr(stage_name, selected_stage_name) = 0 Then
                    'some schemes have stage letter 1 instead of stage 1
                    Dim test_stage_name As String = "Stage Letter " & Right(selected_stage_name, 1)
                    If InStr(stage_name, test_stage_name) = 0 Then
                        If selected_stage_name = "Stage 1" Then
                            If InStr(stage_name, "Notice of Assignment") = 0 Then
                                Continue For
                            End If
                        Else
                            Continue For
                        End If
                    End If
                Else
                    'If selected_stage_name = "Stage 1" Or _
                    'selected_stage_name = "Letter" Then
                    '    'for stage 1 and letter stages, check letter sent at least 5 days ago
                    '    param2 = "select _createdDate from Note " & _
                    '    " where debtorID = " & debtor & _
                    '    " and type = 'Letter'" & _
                    '    " order by _rowid"
                    '    Dim note_dataset As DataSet = get_dataset("onestep", param2)
                    '    If no_of_rows = 0 Then
                    '        Continue For
                    '    Else
                    '        Dim note_date As Date = note_dataset.Tables(0).Rows(0).Item(0)
                    '        If DateDiff(DateInterval.Day, note_date, Now) < 5 Then
                    '            Continue For
                    '        End If
                    '    End If
                    'End If
                End If
                'if letter stage selected - ignore names with stage in
                If selected_stage_name = "Letter" Then
                    If InStr(stage_name, "Stage") > 0 Then
                        Continue For
                    End If
                End If
            End If
            'if branch=2 or 8 then ignore vulnerable debtors (unless mode=15!)
            If mode <> "15" And (branchID = 2 Or branchID = 8) Then
                param2 = "select isVulnerable from Debtor where _rowid = " & debtor
                Dim vul_ds As DataSet = get_dataset("onestep", param2)
                If vul_ds.Tables(0).Rows(0).Item(0) = "Y" Then
                    Continue For
                End If
            End If
            'if SMS ignore cases sent SMS message in last 7 days
            Dim last_sage_date As Date = Nothing
            If sms_picked Then
                param2 = "select _createdDate, text from note" & _
                " where debtorID = " & debtor & _
                " and type = 'Note'" & _
                " and text = 'VoiceSage Contact Message Sent'" & _
                " order by _createdDate desc"
                Dim sms_ds As DataSet = get_dataset("onestep", param2)
                If no_of_rows > 0 Then
                    last_sage_date = sms_ds.Tables(0).Rows(0).Item(0)
                    If DateDiff(DateInterval.Day, last_sage_date, Now) < 8 Then
                        Continue For
                    End If

                End If
            End If
            '7.3.14 if mode is A (arrangements > 3 months) check for entry in table
            'ignore if selected less than 90 days ok
            If mode = "A" Then
                param2 = "select max(dialler_date) from Dialler_Arrangement_Cases" & _
                                " where dialler_debtorID = " & debtor
                Dim arr_ds As DataSet = Nothing
                Try
                    arr_ds = get_dataset("Fees", param2)
                Catch ex As Exception

                End Try

                Dim dialler_date As Date
                Try
                    dialler_date = arr_ds.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    dialler_date = Nothing
                End Try
                If dialler_date <> Nothing Then
                    If Format(dialler_date, "yyyy-MM-dd") <> Format(Now, "yyyy-MM-dd") And _
                        DateDiff(DateInterval.Day, dialler_date, Now) < 90 Then
                        Continue For
                    End If
                End If
                'save case in table if prod run
                If prod_run Then
                    upd_txt = "insert into Dialler_Arrangement_Cases (dialler_date,dialler_DebtorID,dialler_user)" & _
                    " values('" & Format(Now, "dd/MMM/yyyy HH:mm:ss") & "'," & debtor & ",'" & My.User.Name & "')"
                    update_sql(upd_txt)
                End If
            End If
            param2 = "select name from Client where _rowid = " & cl_ID
            Dim cl_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to read client for clID = " & cl_ID)
                Exit Sub
            End If

            Dim cl_name As String = Trim(cl_dataset.Tables(0).Rows(0).Item(0))

            param2 = "select name from Scheme where _rowid = " & sch_ID
            Dim sch_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
                Exit Sub
            End If


            sch_name = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
            Dim ph1, ph2, ph3, ph4 As String
            Dim title, forename, surname, name2, address As String
            Try
                title = Trim(debtor_dataset.Tables(0).Rows(idx).Item(1))
            Catch ex As Exception
                title = ""
            End Try
            Try
                forename = Trim(debtor_dataset.Tables(0).Rows(idx).Item(2))
            Catch ex As Exception
                forename = ""
            End Try
            Try
                surname = Trim(debtor_dataset.Tables(0).Rows(idx).Item(3))
            Catch ex As Exception
                surname = ""
            End Try
            Try
                address = Trim(debtor_dataset.Tables(0).Rows(idx).Item(4))
            Catch ex As Exception
                address = ""
            End Try
            Try
                name2 = Trim(debtor_dataset.Tables(0).Rows(idx).Item(11))
            Catch ex As Exception
                name2 = ""
            End Try
            Try
                ph1 = Trim(debtor_dataset.Tables(0).Rows(idx).Item(6))
            Catch ex As Exception
                ph1 = ""
            End Try
            If mobile_picked And ph1 <> "" Then
                If Microsoft.VisualBasic.Left(ph1, 2) <> "07" And _
                Microsoft.VisualBasic.Left(ph1, 3) <> "447" And _
                Microsoft.VisualBasic.Left(ph1, 4) <> "4407" Then
                    ph1 = ""
                End If
                If Microsoft.VisualBasic.Left(ph1, 4) = "4407" Then
                    ph1 = "447" & Microsoft.VisualBasic.Right(ph1, ph1.Length - 4)
                End If
            End If
            Try
                ph2 = Trim(debtor_dataset.Tables(0).Rows(idx).Item(7))
            Catch ex As Exception
                ph2 = ""
            End Try
            If mobile_picked And ph2 <> "" Then
                If Microsoft.VisualBasic.Left(ph2, 2) <> "07" And _
                Microsoft.VisualBasic.Left(ph2, 3) <> "447" And _
                Microsoft.VisualBasic.Left(ph2, 4) <> "4407" Then
                    ph2 = ""
                End If
                If Microsoft.VisualBasic.Left(ph2, 4) = "4407" Then
                    ph2 = "447" & Microsoft.VisualBasic.Right(ph2, ph2.Length - 4)
                End If
            End If
            
            
            If ph1 = ph2 Then
                ph2 = ""
            End If
            If mobile_picked And smsfrm.srbtn.Checked = False Then
                ph3 = ""
            Else
                Try
                    ph3 = Trim(debtor_dataset.Tables(0).Rows(idx).Item(8))
                Catch ex As Exception
                    ph3 = ""
                End Try
            End If
            If mobile_picked And ph3 <> "" Then
                If Microsoft.VisualBasic.Left(ph3, 2) <> "07" And _
                Microsoft.VisualBasic.Left(ph3, 3) <> "447" And _
                Microsoft.VisualBasic.Left(ph3, 4) <> "4407" Then
                    ph3 = ""
                End If
                If Microsoft.VisualBasic.Left(ph3, 4) = "4407" Then
                    ph3 = "447" & Microsoft.VisualBasic.Right(ph3, ph3.Length - 4)
                End If
            End If
            If mobile_picked And smsfrm.srbtn.Checked = False Then
                ph4 = ""
            Else
                Try
                    ph4 = Trim(debtor_dataset.Tables(0).Rows(idx).Item(9))
                Catch ex As Exception
                    ph4 = ""
                End Try
            End If
            If mobile_picked And ph4 <> "" Then
                If Microsoft.VisualBasic.Left(ph4, 2) <> "07" And _
                Microsoft.VisualBasic.Left(ph4, 3) <> "447" And _
                Microsoft.VisualBasic.Left(ph4, 4) <> "4407" Then
                    ph4 = ""
                End If
                If Microsoft.VisualBasic.Left(ph4, 4) = "4407" Then
                    ph4 = "447" & Microsoft.VisualBasic.Right(ph4, ph4.Length - 4)
                End If
            End If
            'shuffle phone numbers along
            If ph1.Length < 4 Then
                ph1 = ph2
                ph2 = ph3
                ph3 = ph4
                ph4 = ""
                If ph1.Length < 4 Then
                    ph1 = ph2
                    ph2 = ph3
                    ph3 = ""
                End If
                If ph1.Length < 4 Then
                    ph1 = ph2
                    ph2 = ""
                End If
            End If
            If ph2.Length < 4 Then
                ph2 = ph3
                ph3 = ph4
                ph4 = ""
                If ph2.Length < 4 Then
                    ph2 = ph3
                    ph3 = ""
                End If
            Else
                If ph3.Length < 4 Then
                    ph3 = ph4
                    ph4 = ""
                End If
            End If
            'remove chr(10) and chr(13) 
            If ph1.Length < 4 Then
                Continue For
            End If
            ph1 = remove_chars(ph1, True)
            ph2 = remove_chars(ph2, True)
            ph3 = remove_chars(ph3, True)
            ph4 = remove_chars(ph4, True)
            surname = remove_chars(surname, False)
            forename = remove_chars(forename, False)
            title = remove_chars(title, False)
            address = remove_chars(address, False)
            name2 = remove_chars(name2, False)
            Dim fullname As String = ""
            If title.Length > 0 Then
                fullname = title & " "
            End If
            If forename.Length > 0 Then
                fullname &= forename & " "
            End If
            fullname &= surname
            If sms_picked Then
                Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(12)
                param2 = "select name from Stage where _rowid = " & stageID
                Dim stage_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows <> 1 Then
                    MsgBox("Unable to read stage name for stageID = " & stageID)
                    Exit Sub
                End If
                Dim stage_name As String = stage_dataset.Tables(0).Rows(0).Item(0)
                '16.12.2014 SMS file changed
                'outline &= debtor & "," & forename & "," & surname & "," & ph1 & "," & ph2 & "," & cl_name & _
                ' "," & stage_name & ","
                'If last_sage_date <> Nothing Then
                '    outline &= Format(last_sage_date, "dd/MM/yyyy")
                'End If
                outline = outline & debtor & "," & forename & "," & surname & "," & _
                                             ph1 & "," & ph2 & "," & ph3 & "," & ph4 
            ElseIf qivox_picked Then
                outline &= debtor & "," & fullname & "," & ph1 & "," & ph2
            Else
                outline = outline & debtor & "," & title & "," & forename & "," & surname & "," & _
                            address & "," & cl_name & "," & sch_name & "," & _
                            ph1 & "," & ph2 & "," & ph3 & "," & ph4 & "," & _
                            Format(debtor_dataset.Tables(0).Rows(idx).Item(10), "fixed") & "," & _
                            cl_ID & "," & name2 & "," & Format(created_date, "d/MM/yyyy")
            End If

            Try
                outline_array(outline_idx) = outline
            Catch ex As Exception
                ReDim Preserve outline_array(outline_idx + 200)
                ReDim Preserve phone_array(outline_idx + 200)
                outline_array(outline_idx) = outline
            End Try

            phone_array(outline_idx) = ph1
            outline_idx += 1
            outline = ""
        Next
        Array.Sort(phone_array, outline_array, 0, outline_idx)
        Dim phone_no As String = ""

        Dim savedOutline As String = ""
        For idx = 0 To outline_idx - 1
            If idx = outline_idx - 1 Then
                Dim test As Integer
                test = 0
            End If
            'ToolStripProgressBar1.Value = (idx / outline_idx) * 100
            If phone_no = phone_array(idx) Then
                Dim sortDebtorID As Integer = Microsoft.VisualBasic.Left(outline_array(idx), 7)
                Dim savedDebtorID As Integer = Microsoft.VisualBasic.Left(savedOutline, 7)
                If sortDebtorID > savedDebtorID Then
                    savedOutline = outline_array(idx) & vbNewLine
                End If
                If idx <> outline_idx - 1 Then
                    Continue For
                End If
            End If
            If Len(savedOutline) > 0 Then
                Try
                    My.Computer.FileSystem.WriteAllText(collect_stage.SaveFileDialog1.FileName, savedOutline, True, ascii)
                Catch ex As Exception
                    MsgBox("Limit of cases reached")
                    MsgBox(ex.Message)
                    Exit For
                End Try
            End If
            savedOutline = outline_array(idx) & vbNewLine

            If idx = outline_idx - 1 Then 'last entry
                If phone_no <> phone_array(idx) Then
                    'write out last
                    If Len(savedOutline) > 0 Then
                        Try
                            My.Computer.FileSystem.WriteAllText(collect_stage.SaveFileDialog1.FileName, savedOutline, True, ascii)
                        Catch ex As Exception
                            MsgBox("Limit of cases reached")
                            MsgBox(ex.Message)
                        End Try
                    End If
                End If
            End If
            phone_no = phone_array(idx)
        Next

        'If Len(outfile) = 0 Then
        '    MessageBox.Show("There are no cases selected")
        'Else
        '    'If collect_stage.SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
        '    My.Computer.FileSystem.WriteAllText(FileName, outfile, False, ascii)
        '    If outfile2_required Then

        '        My.Computer.FileSystem.WriteAllText(file2_name, outfile2, False, ascii)
        '        MsgBox("2 files saved")
        '    End If
        'End If

        MsgBox("file saved")

        selected_stage_name = ""
        collect_stage.ToolStripProgressBar1.Visible = False
    End Sub
    Public Function remove_chars(ByVal ph As String, ByVal phone_number As Boolean) As String
        Dim ph_out As String = ""
        Dim padChar As String
        If phone_number Then
            padChar = ""
        Else
            padChar = " "
        End If
        If ph.Length < 2 And phone_number Then
            Return ("")
        End If
        Dim idx3 As Integer
        For idx3 = 1 To ph.Length
            If Mid(ph, idx3, 1) = vbCr Or _
                Mid(ph, idx3, 1) = "," Or _
                Mid(ph, idx3, 1) = Chr(10) Or _
                Mid(ph, idx3, 1) = Chr(13) Or _
               Mid(ph, idx3, 1) = vbLf Then
                ph_out = ph_out & padChar
            Else
                ph_out = ph_out & Mid(ph, idx3, 1)
            End If
        Next
        If phone_number Then
            ph_out = Replace(ph_out, " ", "")
        End If
        Return (Trim(ph_out))
    End Function
End Module
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class categoryfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cancelbtn = New System.Windows.Forms.Button
        Me.createbtn = New System.Windows.Forms.Button
        Me.cat_combobox = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'cancelbtn
        '
        Me.cancelbtn.Location = New System.Drawing.Point(24, 191)
        Me.cancelbtn.Name = "cancelbtn"
        Me.cancelbtn.Size = New System.Drawing.Size(75, 23)
        Me.cancelbtn.TabIndex = 1
        Me.cancelbtn.Text = "Cancel"
        Me.cancelbtn.UseVisualStyleBackColor = True
        '
        'createbtn
        '
        Me.createbtn.Location = New System.Drawing.Point(184, 191)
        Me.createbtn.Name = "createbtn"
        Me.createbtn.Size = New System.Drawing.Size(75, 23)
        Me.createbtn.TabIndex = 2
        Me.createbtn.Text = "Create File"
        Me.createbtn.UseVisualStyleBackColor = True
        '
        'cat_combobox
        '
        Me.cat_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cat_combobox.FormattingEnabled = True
        Me.cat_combobox.Items.AddRange(New Object() {"A 185, 806", "D 13,776", "J 17,892", "H 7,427"})
        Me.cat_combobox.Location = New System.Drawing.Point(77, 90)
        Me.cat_combobox.Name = "cat_combobox"
        Me.cat_combobox.Size = New System.Drawing.Size(121, 21)
        Me.cat_combobox.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(108, 74)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Category"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(31, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(228, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Select category (value shown is offence value)"
        '
        'categoryfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cat_combobox)
        Me.Controls.Add(Me.createbtn)
        Me.Controls.Add(Me.cancelbtn)
        Me.Name = "categoryfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Choose Category"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cancelbtn As System.Windows.Forms.Button
    Friend WithEvents createbtn As System.Windows.Forms.Button
    Friend WithEvents cat_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class

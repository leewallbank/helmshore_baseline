Module loadexcel
    Public sheet_name As String
    Public vals(1, 1) As String
    Public finalrow, finalcol As Integer
    Public Sub load_vals(ByVal myss As String)

        Dim row, col As Integer
        Dim xl = CreateObject("Excel.Application")
        xl.Workbooks.open(myss)
        Try
            xl.worksheets(sheet_name).activate()
        Catch ex As Exception
            MessageBox.Show("Unable to read spreadsheet")
            xl.workbooks.close()
            Exit Sub
        End Try

        finalrow = xl.activesheet.usedrange.rows.count
        finalcol = xl.activesheet.usedrange.columns.count
        ReDim vals(finalrow, finalcol)
        Dim idx As Integer = 0
        mainfrm.ProgressBar1.Value = 5
        Application.DoEvents()
        For row = 1 To finalrow
            Try
                mainfrm.ProgressBar1.Value = (row / finalrow) * 100
            Catch ex As Exception

            End Try

            Application.DoEvents()
            For col = 1 To finalcol
                idx += 1
                If xl.activesheet.cells(row, col).value = Nothing Then
                    vals(row, col) = " "
                Else
                    vals(row, col) = xl.activesheet.cells(row, col).value.ToString
                End If
            Next
        Next
        xl.workbooks.close()
        xl = Nothing
        mainfrm.ProgressBar1.Value = 5
    End Sub
End Module

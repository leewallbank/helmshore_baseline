Module data_access_module
    Public table_array(,)
    Public no_of_rows, last_rowid, ret_code As Integer
    Public conn As New Odbc.OdbcConnection()
    Public conn2 As New OleDb.OleDbConnection
    Public param1, param2, param3, param4, param5 As String
   
    Function get_dataset(ByVal database_name As String, Optional ByVal select_string As String = Nothing) As DataSet
        Dim dataset As New DataSet
        Try
            If database_name = "onestep" Then
                If conn_open = False Then
                    conn.ConnectionString = _
                                       "Integrated Security=True;Dsn=DebtRecovery;db=DebtRecovery;uid=vbnet;password=tenbv;"
                    Try
                        conn.Open()
                    Catch ex As Exception
                        MsgBox("Unable to open onestep connection")
                    End Try
                    conn_open = True
                End If
                Dim adapter As New Odbc.OdbcDataAdapter(select_string, conn)

                adapter.Fill(dataset)
                'conn.Close()
            Else
                conn2.ConnectionString = ""
                If database_name = "Fees" Then
                    conn2.ConnectionString = _
                     "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=FeesSQL;Data Source=ROSSVR01\SQL2005"
                ElseIf database_name = "TestFees" Then
                    conn2.ConnectionString = _
                    "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=TestFeesSQL;Data Source=ROSSVR01\SQL2005"
                ElseIf database_name = "Complaints" Then
                    conn2.ConnectionString = _
                       "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=PraiseAndComplaintsSQL;Data Source=ROSSVR01\SQL2005"
                ElseIf database_name = "Employed" Then
                    conn2.ConnectionString = _
                   "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=r:\Access tables\Employed_bailiffs.mdb;Persist Security Info=False"
                End If
                conn2.Open()
                Dim adapter As New OleDb.OleDbDataAdapter(select_string, conn2)
                adapter.Fill(dataset)
                conn2.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Try
            no_of_rows = dataset.Tables(0).Rows.Count
        Catch
            no_of_rows = 0
        End Try
        Return dataset

    End Function
   
End Module

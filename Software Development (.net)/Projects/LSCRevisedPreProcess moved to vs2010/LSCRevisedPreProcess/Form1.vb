
Imports System.Xml.Schema

Public Class Form1

    Dim xml_valid As Boolean = True
    Dim new_file As String = ""
    'Dim outcome_file As String
    Dim input_file, input_file2 As String
    Dim write_audit As Boolean = False
    Dim audit_file, no_changes_file As String
    Dim record As String = ""
    Dim filename, filename_id, header_id As String
    Dim first_error As Boolean = True
    Dim first_change As Boolean = True
    Dim on_onestep, equity_check As Boolean
    Dim contrib_id As Double
    Dim no_changes As Integer = 0
    Dim changes As Integer = 0
    Dim spaces As Integer = 250
    Dim test_date As Date
    Dim applicant_id, summons_no, maat_id, first_name, surname, dob, ni_no, outcome, outcome2, appeal_type, rep_status As String
    Dim mthly_contrib_amt_str, upfront_contrib_amt_str, income_contrib_cap_str, income_uplift_applied As String
    Dim case_type, in_court_custody, debt_addr1, debt_addr2, debt_addr3, debt_addr4, debt_postcode As String
    Dim postal_addr1, postal_addr2, postal_addr3, postal_addr4, postal_postcode, landline, mobile, email As String
    Dim equity_amt, appl_equity_amt, partner_equity_amt, cap_amt, asset_no, inc_ev_no As Integer
    Dim ci_code, ci_desc, case_type_code, rep_status_desc As String
    Dim has_partner, partner_first_name, partner_last_name, partner_nino, partner_emp_status, partner_emp_code As String
    Dim contrary_interest, disability_declaration, disability_description, nfa, si, hardship_result As String
    Dim assessment_reason, assessment_code, bedroom_count, undeclared_property, no_capital_declared, imprisoned, sentence_date_str As String
    Dim declared_value, declared_mortgage, verified_mortgage, verified_market_value As Decimal
    Dim all_evidence_date, uplift_applied_date, uplift_removed_date, sentence_date As Date
    Dim effective_date, rep_status_date, committal_date, hardship_review_date, assessment_date, partner_dob As Date
    Dim valid_recs As Integer = 0
    Dim rej_recs As Integer = 0
    Dim no_of_errors As Integer = 0
    Dim percent_owned_partner, percent_owned_appl As Integer
    Dim asset_table(100, 5)
    Dim inc_evidence_table(30, 4) As String
    Dim rep_order_withdrawal_date As Date
    Dim future_effective_date As Boolean
    Dim earlier_contrib_found As Boolean
    Dim mthly_contrib_amt, upfront_contrib_amt, income_contrib_cap As Decimal
    Dim comments2 As String
    Dim equity_amt_verified, cap_asset_type, cap_amt_verified, comments, residential_code, residential_desc As String
    Dim allowable_cap_threshold, effective_date_str, pref_pay_method, pref_pay_code, pref_pay_day, prop_type_code, prop_type_desc As String
    Dim bank_acc_name, bank_acc_no, bank_sort_code, emp_status, emp_code, rep_order_withdrawal_date_str, hardship_appl_recvd As String

    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click
        conn_open = False
        readbtn.Enabled = False
        exitbtn.Enabled = False
        Try
            With OpenFileDialog1
                .Title = "Read XML file"
                .Filter = "XML file|*.xml"
                .FileName = ""
                .CheckFileExists = True
            End With
            Dim xml_dataSet As New DataSet()
            Dim doc As New Xml.XmlDocument()

            Dim contrib_file As String = "Applicant ID" & "|" & "SummonsNum" & "|" & "MAAT ID" & _
                        "|" & "First name" & "|" & "Surname" & "|" & "Date of birth" & "|" & "NI No" & _
                        "|" & "Monthly Amount" & "|" & "Upfront Amt" & "|" & "Income contribution cap" & "|" & _
                        "Debt addr1" & "|" & "Debt addr2" & "|" & "Debt addr3" & "|" & "Debt addr4" & _
                        "|" & "Debt postcode" & "|" & "Curr addr1" & "|" & "Curr addr2" & _
                        "|" & "Curr addr3" & "|" & "Curr addr4" & "|" & "Curr postcode" & "|" & _
                        "Landline" & "|" & "Mobile" & "|" & "Email" & "|" & _
                         "Effective date" & "|" & "Comments" & vbNewLine
            Dim appeal_file As String = contrib_file
            Dim equity_file As String = contrib_file
            no_changes_file = "Maat ID" & vbNewLine

            input_file = contrib_file
            input_file2 = contrib_file

            'MsgBox("Using test client")

            Dim idx As Integer = 0

            If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                validate_xml_file()
                If xml_valid = False Then
                    MsgBox("XML file has failed validation - see error file")
                    If MsgBox("Do you want to continue processing the file?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                        Me.Close()
                        Exit Sub
                    End If
                End If
                Try
                    filename = OpenFileDialog1.FileName
                    Dim ln2 As Integer = Microsoft.VisualBasic.Len(filename)

                    new_file = Microsoft.VisualBasic.Left(filename, ln2 - 4) & "_error.txt"
                    My.Computer.FileSystem.WriteAllText(new_file, "Error messages " & Now, False)

                    new_file = Microsoft.VisualBasic.Left(filename, ln2 - 4) & "_changes.txt"
                    Dim change_message As String = "Debtor" & vbTab & "Maat ID" & vbTab & "Field" & _
                    vbTab & "Onestep Value" & vbTab & "New value" & vbNewLine
                    My.Computer.FileSystem.WriteAllText(new_file, change_message, False)
                    write_eff_date("debtor|effective_date" & vbNewLine)
                    reset_fields()
                    Dim rdr_name As String = ""
                    Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
                    reader.Read()
                    ProgressBar1.Value = 5
                    Dim record_count As Integer = 0
                    While (reader.ReadState <> Xml.ReadState.EndOfFile)
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        Try
                            rdr_name = reader.Name
                        Catch
                            Continue While
                        End Try

                        'Note ReadElementContentAsString moves focus to next element so don't need read
                        ProgressBar1.Value = record_count
                        Application.DoEvents()
                        Select Case rdr_name
                            Case "header"
                                header_id = reader.Item(0)
                                'MsgBox("TEST RUN ONLY - header and MAAT ID")
                                'get last header id
                                Me.LSC_preprocess_last_Header_IDTableAdapter.Fill(Me.FeesSQLDataSet1.LSC_preprocess_last_Header_ID)
                                If Me.FeesSQLDataSet1.LSC_preprocess_last_Header_ID.Rows.Count = 0 Then
                                    Try
                                        Me.LSC_preprocess_last_Header_IDTableAdapter.InsertQuery(header_id, Now, log_user)
                                    Catch ex As Exception
                                        MsgBox(ex.Message)
                                    End Try
                                Else
                                    Dim last_header_id As Integer = Me.FeesSQLDataSet1.LSC_preprocess_last_Header_ID.Rows(0).Item(0)
                                    Dim header_id_num As Integer = header_id
                                    If header_id_num < last_header_id Then
                                        MsgBox("Header ID on file is before last Header ID" & vbNewLine & _
                                        "This file Header ID is " & header_id_num & vbNewLine & _
                                        "Previous Header ID was " & last_header_id)
                                        If MsgBox("Do you want to carry on", MsgBoxStyle.YesNo, "Carry on regardless") = MsgBoxResult.No Then
                                            Me.Close()
                                            Exit Sub
                                        End If
                                    Else
                                        If header_id_num <> last_header_id Then
                                            Try
                                                Me.LSC_preprocess_last_Header_IDTableAdapter.InsertQuery(header_id, Now, log_user)
                                            Catch ex As Exception
                                                MsgBox(ex.Message)
                                            End Try
                                        End If
                                    End If
                                End If
                                reader.Read()
                            Case "filename"
                                filename_id = reader.ReadElementContentAsString
                            Case "CONTRIBUTIONS"
                                record_count += 1
                                If record_count > 100 Then
                                    record_count = 0
                                End If
                                If idx > 0 Then
                                    end_of_record()
                                    Try
                                        input_file = input_file & record & vbNewLine
                                    Catch ex As Exception
                                        input_file2 = input_file
                                        input_file = record & vbNewLine
                                    End Try

                                    If validate_record() = False Then
                                        rej_recs += 1
                                        audit_file = audit_file & maat_id & "|rejected" & vbNewLine
                                    Else
                                        valid_recs += 1
                                        If on_onestep = False Then
                                            If outcome = "APPEAL" Or _
                                            UCase(Microsoft.VisualBasic.Left(case_type, 6)) = "APPEAL" Then
                                                audit_file = audit_file & maat_id & "|appeal" & vbNewLine
                                                appeal_file = appeal_file & record & vbNewLine
                                            ElseIf equity_check = True Then
                                                audit_file = audit_file & maat_id & "|equity" & vbNewLine
                                                equity_file = equity_file & record & vbNewLine
                                            Else
                                                audit_file = audit_file & maat_id & "|contrib" & vbNewLine
                                                contrib_file = contrib_file & record & vbNewLine
                                            End If
                                        End If
                                    End If
                                    reset_fields()
                                End If
                                idx += 1
                                contrib_id = reader.Item(0)
                                reader.Read()
                            Case "maat_id"
                                maat_id = reader.ReadElementContentAsString
                                asset_no = 0
                                inc_ev_no = 0
                                'get last contrib id
                                'Dim last_contrib_id As Integer = 0
                                'Me.LSC_preprocess_last_IDTableAdapter1.FillBy(Me.FeesSQLDataSet1.LSC_preprocess_last_ID, maat_id)
                                'If Me.FeesSQLDataSet1.LSC_preprocess_last_ID.Rows.Count > 0 Then
                                '    last_contrib_id = Me.FeesSQLDataSet1.LSC_preprocess_last_ID.Rows(0).Item(1)
                                'End If
                                'earlier_contrib_found = False
                                'If contrib_id < last_contrib_id And last_contrib_id > 0 Then
                                '    earlier_contrib_found = True
                                'End If
                                'If earlier_contrib_found = False Then
                                '    Try
                                '        Me.LSC_preprocess_last_IDTableAdapter1.InsertQuery(maat_id, contrib_id, Now, log_user)
                                '    Catch ex As Exception
                                '        Try
                                '            Me.LSC_preprocess_last_IDTableAdapter1.DeleteQuery(maat_id)
                                '            Me.LSC_preprocess_last_IDTableAdapter1.InsertQuery(maat_id, contrib_id, Now, log_user)
                                '        Catch ex2 As Exception
                                '            MsgBox(ex2.Message)
                                '        End Try
                                '    End Try
                                'End If
                            Case "applicant"
                                applicant_id = reader.Item(0)
                                reader.Read()
                            Case "firstName"
                                first_name = reader.ReadElementContentAsString
                            Case "lastName"
                                surname = reader.ReadElementContentAsString
                            Case "dob"
                                dob = reader.ReadElementContentAsString
                                Try
                                    Dim dob_date As Date = dob
                                    dob = Format(dob_date, "dd-MM-yyyy")
                                Catch ex As Exception
                                    dob = Nothing
                                End Try
                            Case "ni_number"
                                ni_no = reader.ReadElementContentAsString
                            Case "landline"
                                landline = reader.ReadElementContentAsString
                            Case "mobile"
                                mobile = reader.ReadElementContentAsString
                            Case "email"
                                email = reader.ReadElementContentAsString
                            Case "noFixedAbode"
                                nfa = reader.ReadElementContentAsString
                                comments = comments & "No Fixed Abode:" & nfa & ";"
                                space_comments()
                            Case "specialInvestigation"
                                si = reader.ReadElementContentAsString
                                comments = comments & "Special Investigation:" & si & ";"
                                space_comments()
                            Case "homeAddress"
                                reader.Read()
                                While reader.Name <> "homeAddress"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "line1"
                                            debt_addr1 = reader.ReadElementContentAsString
                                        Case "line2"
                                            debt_addr2 = reader.ReadElementContentAsString
                                        Case "line3"
                                            debt_addr3 = reader.ReadElementContentAsString
                                        Case "city"
                                            debt_addr4 = reader.ReadElementContentAsString
                                        Case "postcode"
                                            debt_postcode = reader.ReadElementContentAsString
                                        Case "country"
                                            Dim country As String = reader.ReadElementContentAsString
                                            If country <> "GB" Then
                                                debt_addr4 = debt_addr4 & " " & Trim(country)
                                            End If
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "postalAddress"
                                reader.Read()
                                While reader.Name <> "postalAddress"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "line1"
                                            postal_addr1 = reader.ReadElementContentAsString
                                        Case "line2"
                                            postal_addr2 = reader.ReadElementContentAsString
                                        Case "line3"
                                            postal_addr3 = reader.ReadElementContentAsString
                                        Case "city"
                                            postal_addr4 = reader.ReadElementContentAsString
                                        Case "postcode"
                                            postal_postcode = reader.ReadElementContentAsString
                                        Case "country"
                                            Dim country As String = reader.ReadElementContentAsString
                                            If country <> "GB" Then
                                                postal_addr4 = postal_addr4 & " " & Trim(country)
                                            End If
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "employmentStatus"
                                reader.Read()
                                While reader.Name <> "employmentStatus"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "description"
                                            emp_status = reader.ReadElementContentAsString
                                            comments = comments & "Emp status:" & emp_status & ";"
                                            space_comments()
                                        Case "code"
                                            emp_code = reader.ReadElementContentAsString
                                            comments = comments & "Emp Code:" & emp_code & ";"
                                            space_comments()
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "partner"
                                reader.Read()
                                While reader.Name <> "partner"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "hasPartner"
                                            has_partner = reader.ReadElementContentAsString
                                            comments = comments & "Has Partner:" & has_partner & ";"
                                            space_comments()
                                        Case "contraryInterest"
                                            contrary_interest = reader.ReadElementContentAsString
                                            comments = comments & "Contrary Interest:" & contrary_interest & ";"
                                            space_comments()
                                        Case "ciDetails"
                                            reader.Read()
                                            While reader.Name <> "ciDetails"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "code"
                                                        ci_code = reader.ReadElementContentAsString
                                                        comments = comments & "Contrary Interest Code:" & ci_code & ";"
                                                        space_comments()
                                                    Case "description"
                                                        ci_desc = reader.ReadElementContentAsString
                                                        comments = comments & "Contrary Interest Description:" & ci_desc & ";"
                                                        space_comments()
                                                    Case Else
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "partnerDetails"
                                reader.Read()
                                While reader.Name <> "partnerDetails"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "firstName"
                                            partner_first_name = reader.ReadElementContentAsString
                                            comments = comments & "Partner First Name:" & partner_first_name & ";"
                                            space_comments()
                                        Case "lastName"
                                            partner_last_name = reader.ReadElementContentAsString
                                            comments = comments & "Partner Last Name:" & partner_last_name & ";"
                                            space_comments()
                                        Case "dob"
                                            partner_dob = reader.ReadElementContentAsString
                                            comments = comments & "Partner DOB:" & Format(partner_dob, "dd/MM/yyyy") & ";"
                                            space_comments()
                                        Case "niNumber"
                                            partner_nino = reader.ReadElementContentAsString
                                            comments = comments & "Partner NINO:" & partner_nino & ";"
                                            space_comments()
                                        Case "employmentStatus"
                                            reader.Read()
                                            While reader.Name <> "employmentStatus"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "description"
                                                        partner_emp_status = reader.ReadElementContentAsString
                                                        comments = comments & "Partner Emp Status:" & partner_emp_status & ";"
                                                        space_comments()
                                                    Case "code"
                                                        partner_emp_code = reader.ReadElementContentAsString
                                                        comments = comments & "Partner Emp Code:" & partner_emp_code & ";"
                                                        space_comments()
                                                    Case Else
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "disabilitySummary"
                                reader.Read()
                                While reader.Name <> "disabilitySummary"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "declaration"
                                            disability_declaration = reader.ReadElementContentAsString
                                            comments = comments & "Disability Declaration:" & disability_declaration & ";"
                                            space_comments()
                                        Case "disability"
                                            reader.Read()
                                            While reader.Name <> "disability"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "description"
                                                        disability_description = reader.ReadElementContentAsString
                                                        comments = comments & "Disability Description:" & disability_description & ";"
                                                        space_comments()
                                                    Case Else
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "caseType"
                                reader.Read()
                                While reader.Name <> "caseType"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "description"
                                            case_type = Trim(reader.ReadElementContentAsString)
                                            comments = comments & "Case type:" & case_type & ";"
                                            space_comments()
                                        Case "code"
                                            case_type_code = reader.ReadElementContentAsString
                                            comments = comments & "Case Type Code:" & case_type_code & ";"
                                            space_comments()
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "repStatus"
                                reader.Read()
                                While reader.Name <> "repStatus"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "status"
                                            rep_status = reader.ReadElementContentAsString
                                            comments = comments & "Rep Status:" & rep_status & ";"
                                            space_comments()
                                        Case "description"
                                            rep_status_desc = reader.ReadElementContentAsString
                                            comments = comments & "Rep Status Description:" & rep_status_desc & ";"
                                            space_comments()
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "repStatusDate"
                                rep_status_date = reader.ReadElementContentAsString
                                comments = comments & "Rep Status Date:" & Format(rep_status_date, "dd/MM/yyyy") & ";"
                                space_comments()
                            Case "arrestSummonsNumber"
                                summons_no = reader.ReadElementContentAsString
                            Case "inCourtCustody"
                                in_court_custody = reader.ReadElementContentAsString
                                If in_court_custody.Length > 0 Then
                                    If LCase(Microsoft.VisualBasic.Left(in_court_custody, 1)) = "n" Then
                                        in_court_custody = "No"
                                    End If
                                    If LCase(Microsoft.VisualBasic.Left(in_court_custody, 1)) = "y" Then
                                        in_court_custody = "Yes"
                                    End If
                                    comments = comments & "In Court Custody:" & in_court_custody & ";"
                                    space_comments()
                                End If
                            Case "imprisoned"
                                imprisoned = reader.ReadElementContentAsString
                                If imprisoned.Length > 0 Then
                                    If LCase(Microsoft.VisualBasic.Left(imprisoned, 1)) = "n" Then
                                        imprisoned = "No"
                                    End If
                                    If LCase(Microsoft.VisualBasic.Left(imprisoned, 1)) = "y" Then
                                        imprisoned = "Yes"
                                    End If
                                    comments = comments & "InPrison:" & imprisoned & ";"
                                    space_comments()
                                End If
                            Case "sentenceDate"
                                sentence_date_str = reader.ReadElementContentAsString
                                Try
                                    sentence_date = CDate(sentence_date_str)
                                    comments = comments & "SentenceOrderDate:" & Format(sentence_date, "dd/MM/yyyy") & ";"
                                    space_comments()
                                Catch ex As Exception
                                    sentence_date = Nothing
                                End Try
                            Case "committalDate"
                                committal_date = reader.ReadElementContentAsString
                                comments = comments & "Committal Date:" & Format(committal_date, "dd/MM/yyyy") & ";"
                                space_comments()
                            Case "ccHardship"
                                reader.Read()
                                While reader.Name <> "ccHardship"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "reviewDate"
                                            hardship_review_date = reader.ReadElementContentAsString
                                            comments = comments & "Hardship Review Date:" & Format(hardship_review_date, "dd/MM/yyyy") & ";"
                                            space_comments()
                                        Case "reviewResult"
                                            hardship_result = reader.ReadElementContentAsString
                                            comments = comments & "Hardship Review Result:" & hardship_result & ";"
                                            space_comments()
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "effectiveDate"
                                effective_date_str = reader.ReadElementContentAsString
                                Try
                                    effective_date = effective_date_str
                                Catch ex As Exception
                                    effective_date = Nothing
                                End Try
                                'if effective date in the future - add to notes 
                                If effective_date <> Nothing Then
                                    If effective_date > Now Then
                                        future_effective_date = True
                                        comments = comments & "Effective Date:" & Format(effective_date, "dd/MM/yyyy") & ";"
                                        space_comments()
                                    End If
                                End If
                            Case "monthlyContribution"
                                mthly_contrib_amt_str = reader.ReadElementContentAsString
                                mthly_contrib_amt = mthly_contrib_amt_str      ' pounds not pence/ 100
                            Case "upfrontContribution"
                                upfront_contrib_amt_str = reader.ReadElementContentAsString
                                upfront_contrib_amt = upfront_contrib_amt_str     'pounds not pence/ 100
                            Case "incomeContributionCap"
                                income_contrib_cap_str = reader.ReadElementContentAsString
                                income_contrib_cap = income_contrib_cap_str      'pounds not pence/ 100
                            Case "assessmentReason"
                                reader.Read()
                                While reader.Name <> "assessmentReason"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "description"
                                            assessment_reason = reader.ReadElementContentAsString
                                            comments = comments & "Assessment Reason:" & assessment_reason & ";"
                                            space_comments()
                                        Case "code"
                                            assessment_code = reader.ReadElementContentAsString
                                            comments = comments & "Assessment Code:" & assessment_code & ";"
                                            space_comments()
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "assessmentDate"
                                assessment_date = reader.ReadElementContentAsString
                                comments = comments & "Assessment Date:" & Format(assessment_date, "dd/MM/yyyy") & ";"
                                space_comments()
                            Case "bedRoomCount"
                                bedroom_count = reader.ReadElementContentAsString
                                comments = comments & "Bedroom Count:" & bedroom_count & ";"
                                space_comments()
                            Case "undeclaredProperty"
                                undeclared_property = reader.ReadElementContentAsString
                                comments = comments & "Undeclared Property:" & undeclared_property & ";"
                                space_comments()
                            Case "residentialStatus"
                                reader.Read()
                                While reader.Name <> "residentialStatus"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "code"
                                            residential_code = reader.ReadElementContentAsString
                                            comments = comments & "Residential Code:" & Trim(residential_code) & ";"
                                            space_comments()
                                        Case "description"
                                            residential_desc = reader.ReadElementContentAsString
                                            comments = comments & "Residential Description:" & Trim(residential_desc) & ";"
                                            space_comments()
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "propertyType"
                                reader.Read()
                                While reader.Name <> "propertyType"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "code"
                                            prop_type_code = reader.ReadElementContentAsString
                                            comments = comments & "Property Type Code:" & Trim(prop_type_code) & ";"
                                            space_comments()
                                        Case "description"
                                            prop_type_desc = reader.ReadElementContentAsString
                                            comments = comments & "Property Type Description:" & Trim(prop_type_desc) & ";"
                                            space_comments()
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "percentageApplicantOwned"
                                percent_owned_appl = reader.ReadElementContentAsString
                                comments = comments & "Percent Owned Applicant:" & percent_owned_appl & ";"
                                space_comments()
                            Case "percentagePartnerOwned"
                                percent_owned_partner = reader.ReadElementContentAsString
                                comments = comments & "Percent Owned Partner:" & percent_owned_partner & ";"
                                space_comments()
                            Case "applicantEquityAmount"
                                appl_equity_amt = reader.ReadElementContentAsString
                                comments = comments & "Applicant Equity Amount:" & appl_equity_amt & ";"
                                space_comments()
                            Case "partnerEquityAmount"
                                partner_equity_amt = reader.ReadElementContentAsString
                                comments = comments & "Partner Equity Amount:" & partner_equity_amt & ";"
                                space_comments()
                            Case "declaredMortgage"
                                declared_mortgage = reader.ReadElementContentAsString
                                comments = comments & "Declared Mortgage:" & declared_mortgage & ";"
                                space_comments()
                            Case "declaredValue"
                                declared_value = reader.ReadElementContentAsString
                                comments = comments & "Declared Value:" & declared_value & ";"
                                space_comments()
                            Case "verifiedMortgage"
                                verified_mortgage = reader.ReadElementContentAsString
                                comments = comments & "Verified Mortgage:" & verified_mortgage & ";"
                                space_comments()
                            Case "verifiedValue"
                                verified_market_value = reader.ReadElementContentAsString
                                comments = comments & "Verified Market Value:" & verified_market_value & ";"
                                space_comments()
                                'comments = comments & "Equity amt:" & equity_amt & ";"
                                'space_comments()
                            Case "equityVerified"
                                equity_amt_verified = LCase(reader.ReadElementContentAsString)
                                If Microsoft.VisualBasic.Left(equity_amt_verified, 1) = "n" Then
                                    equity_amt_verified = "No"
                                End If
                                If Microsoft.VisualBasic.Left(equity_amt_verified, 1) = "y" Then
                                    equity_amt_verified = "Yes"
                                End If
                                comments = comments & "Equity Amt Verified:" & equity_amt_verified & ";"
                                space_comments()
                            Case "capitalSummary"
                                reader.Read()
                                While reader.Name <> "capitalSummary"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "noCapitalDeclared"
                                            no_capital_declared = reader.ReadElementContentAsString
                                            comments = comments & "No Capital Declared:" & no_capital_declared & ";"
                                            space_comments()
                                        Case "additionalProperties"
                                            comments = comments & "Additional Properties:" & reader.ReadElementContentAsString & ";"
                                            space_comments()
                                        Case "undeclaredProperties"
                                            comments = comments & "Undeclared Properties:" & reader.ReadElementContentAsString & ";"
                                            space_comments()
                                        Case "totalCapitalAssets"
                                            comments = comments & "Total Capital Assets:" & reader.ReadElementContentAsString & ";"
                                            space_comments()
                                        Case "allEvidenceDate"
                                            all_evidence_date = reader.ReadElementContentAsString
                                            comments = comments & "All Evidence Date:" & Format(all_evidence_date, "dd/MM/yyyy") & ";"
                                            space_comments()
                                        Case "capAllowanceWithheld"
                                            comments = comments & "Cap Allowance Withheld:" & reader.ReadElementContentAsString & ";"
                                            space_comments()
                                        Case "capAllowanceRestore"
                                            comments = comments & "Cap Allowance Restore:" & reader.ReadElementContentAsString & ";"
                                            space_comments()
                                        Case "assetList"
                                            reader.Read()
                                            While reader.Name <> "assetList"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "description"
                                                        asset_no += 1
                                                        Dim temp As String = reader.ReadElementContentAsString
                                                        remove_chrs(temp)
                                                        Try
                                                            asset_table(asset_no, 1) = temp
                                                        Catch ex As Exception
                                                            MsgBox("asset table dimension requires increasing")
                                                            End
                                                        End Try
                                                        asset_table(asset_no, 2) = ""
                                                        asset_table(asset_no, 3) = ""
                                                        asset_table(asset_no, 4) = ""
                                                        asset_table(asset_no, 5) = ""
                                                    Case "amount"
                                                        asset_table(asset_no, 2) = reader.ReadElementContentAsString
                                                    Case "verified"
                                                        cap_amt_verified = reader.ReadElementContentAsString
                                                        remove_chrs(cap_amt_verified)
                                                        asset_table(asset_no, 3) = cap_amt_verified
                                                    Case "dateVerified"
                                                        test_date = reader.ReadElementContentAsString
                                                        asset_table(asset_no, 4) = Format(test_date, "dd/MM/yyyy")
                                                    Case "evidenceReceivedDate"
                                                        test_date = reader.ReadElementContentAsString
                                                        asset_table(asset_no, 5) = Format(test_date, "dd/MM/yyyy")
                                                    Case Else
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "repOrderWithdrawalDate"
                                rep_order_withdrawal_date_str = reader.ReadElementContentAsString
                                Try
                                    rep_order_withdrawal_date = rep_order_withdrawal_date_str
                                Catch ex As Exception
                                    rep_order_withdrawal_date_str = ""
                                End Try
                                If IsDate(rep_order_withdrawal_date) Then
                                    comments = comments & "Rep-Order-Withdrawn-Date:" & _
                                    Format(rep_order_withdrawal_date, "dd/MM/yyyy") & ";"
                                    space_comments()
                                End If
                            Case "incomeEvidenceList"
                                reader.Read()
                                Dim new_comment As String = ""
                                While reader.Name <> "incomeEvidenceList"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        If reader.Name = "incomeEvidence" And reader.NodeType = Xml.XmlNodeType.EndElement Then
                                            If new_comment.Length <= 250 Then
                                                comments = comments & new_comment
                                                space_comments()
                                            Else
                                                get_comment_break(new_comment)
                                            End If
                                            new_comment = ""
                                        End If
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "evidence"
                                            inc_ev_no += 1
                                            inc_evidence_table(inc_ev_no, 1) = reader.ReadElementContentAsString
                                            inc_evidence_table(inc_ev_no, 2) = ""
                                            inc_evidence_table(inc_ev_no, 3) = ""
                                            inc_evidence_table(inc_ev_no, 4) = ""
                                            new_comment = new_comment & "Inc Evidence:" & inc_evidence_table(inc_ev_no, 1) & ";"
                                        Case "mandatory"
                                            inc_evidence_table(inc_ev_no, 2) = reader.ReadElementContentAsString
                                            new_comment = new_comment & "Inc Evidence Mandatory:" & inc_evidence_table(inc_ev_no, 2) & ";"
                                        Case "otherText"
                                            Try
                                                inc_evidence_table(inc_ev_no, 3) = reader.ReadElementContentAsString
                                                remove_chrs(inc_evidence_table(inc_ev_no, 3))
                                                new_comment = new_comment & "Inc Evidence Other Text:" & inc_evidence_table(inc_ev_no, 3) & ";"
                                            Catch ex As Exception
                                                MsgBox(ex.Message)
                                            End Try
                                        Case "dateReceived"
                                            test_date = reader.ReadElementContentAsString
                                            inc_evidence_table(inc_ev_no, 4) = Format(test_date, "dd/MM/yyyy")
                                            new_comment = new_comment & "Inc Evidence Date Received:" & Format(test_date, "dd/MM/yyyy") & ";"
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "incomeUpliftApplied"
                                income_uplift_applied = reader.ReadElementContentAsString
                                comments = comments & "Inc uplift applied:" & income_uplift_applied & ";"
                                space_comments()
                            Case "upliftAppliedDate"
                                uplift_applied_date = reader.ReadElementContentAsString
                                comments = comments & "uplift applied date:" & Format(uplift_applied_date, "dd/MM/yyyy") & ";"
                                space_comments()
                            Case "upliftRemovedDate"
                                uplift_removed_date = reader.ReadElementContentAsString
                                comments = comments & "uplift removed date:" & Format(uplift_removed_date, "dd/MM/yyyy") & ";"
                                space_comments()
                            Case "ccOutcomes"
                                reader.Read()
                                While reader.Name <> "ccOutcomes" And reader.Name <> "CONTRIBUTIONS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        If reader.Name = "ccOutcome" And reader.NodeType = Xml.XmlNodeType.EndElement Then
                                            space_comments()
                                        End If
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "code"
                                            If outcome = "" Then
                                                outcome = reader.ReadElementContentAsString
                                                comments = comments & "OUTCOME:" & outcome & ";"
                                            Else
                                                outcome2 = reader.ReadElementContentAsString
                                                comments = comments & "OUTCOME:" & outcome2 & ";"
                                            End If
                                        Case "date"
                                            Try
                                                test_date = reader.ReadElementContentAsString
                                            Catch ex As Exception
                                                reader.Read()
                                                Continue While
                                            End Try
                                            comments = comments & "Outcome Date:" & Format(test_date, "dd/MM/yyyy") & ";"
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                                'Case "FINAL_DEFENCE_COST"
                                '    final_defence_cost = reader.ReadElementContentAsString

                                'Case "ALLOWABLE_CAPITAL_THRESHOLD"
                                '    allowable_cap_threshold = reader.ReadElementContentAsString
                                '    remove_chrs(allowable_cap_threshold)

                            Case "preferredPaymentMethod"
                                reader.Read()
                                While reader.Name <> "preferredPaymentMethod"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "description"
                                            pref_pay_method = reader.ReadElementContentAsString
                                            comments = comments & "Preferred Payment Method:" & pref_pay_method & ";"
                                            space_comments()
                                        Case "code"
                                            pref_pay_code = reader.ReadElementContentAsString
                                            comments = comments & "Preferred Payment Code:" & pref_pay_code & ";"
                                            space_comments()
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "preferredPaymentDay"
                                pref_pay_day = reader.ReadElementContentAsString
                                comments = comments & "Preferred Payment Day:" & pref_pay_day & ";"
                                space_comments()
                            Case "accountName"
                                bank_acc_name = reader.ReadElementContentAsString
                                If bank_acc_name.Length > 0 Then
                                    comments = comments & "Bank Acc Name:" & bank_acc_name & ";"
                                    space_comments()
                                End If
                            Case "accountNo"
                                bank_acc_no = reader.ReadElementContentAsString
                                If bank_acc_no.Length > 0 Then
                                    comments = comments & "Bank Acc No:" & bank_acc_no & ";"
                                    space_comments()
                                End If
                            Case "sortCode"
                                bank_sort_code = reader.ReadElementContentAsString
                                If bank_sort_code.Length > 0 Then
                                    comments = comments & "Bank Sort Code:" & bank_sort_code & ";"
                                    space_comments()
                                End If

                            Case Else
                                If maat_id.Length > 1 And _
                                rdr_name <> "application" And _
                                rdr_name <> "assessment" And _
                                rdr_name <> "bankDetails" And _
                                rdr_name <> "equity" Then
                                    MsgBox("what is this? " & rdr_name)
                                End If

                                reader.Read()
                        End Select
                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Else
                MsgBox("XML file not processed")
                Exit Sub
            End If
            'write file including last applicant
            end_of_record()
            input_file = input_file & record & vbNewLine
            If validate_record() = False Then
                rej_recs += 1
                audit_file = audit_file & maat_id & "|rejected" & vbNewLine
            Else
                valid_recs += 1
                If on_onestep = False Then
                    If outcome = "APPEAL" Or _
                    UCase(Microsoft.VisualBasic.Left(case_type, 6)) = "APPEAL" Then
                        audit_file = audit_file & maat_id & "|appeal" & vbNewLine
                        appeal_file = appeal_file & record & vbNewLine
                    ElseIf equity_check = True Then
                        audit_file = audit_file & maat_id & "|equity" & vbNewLine
                        equity_file = equity_file & record & vbNewLine
                    Else
                        audit_file = audit_file & maat_id & "|contrib" & vbNewLine
                        contrib_file = contrib_file & record & vbNewLine
                    End If
                End If
            End If
            Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_appeal_preprocess.txt"
            My.Computer.FileSystem.WriteAllText(new_file, appeal_file, False)
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_equity_preprocess.txt"
            My.Computer.FileSystem.WriteAllText(new_file, equity_file, False)
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_input.txt"
            My.Computer.FileSystem.WriteAllText(new_file, input_file, False)
            If write_audit Then
                new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_audit.txt"
                My.Computer.FileSystem.WriteAllText(new_file, audit_file, False)
            End If
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_contrib_preprocess.txt"
            My.Computer.FileSystem.WriteAllText(new_file, contrib_file, False)
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_no_changes.txt"
            My.Computer.FileSystem.WriteAllText(new_file, no_changes_file, False)

            'write acknowledgement file in xml
            Dim ack_file As String = "CONTRIBUTIONS_FILE_ACK_" & Format(Now, "yyyyMMddHHmm") & ".xml"
            Dim writer As New Xml.XmlTextWriter(ack_file, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)
            writer.Formatting = Xml.Formatting.Indented
            'acknowledgement file includes all records not rejected
            Dim tot_valid_recs As Integer = valid_recs
            Write_ack(writer, filename_id, header_id, tot_valid_recs, rej_recs, no_of_errors)
            writer.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        ProgressBar1.Value = 100
        MsgBox("Cases with no changes = " & no_changes & vbNewLine & "Changes = " & changes)
        Me.Close()
    End Sub
    Private Sub space_comments()
        'Try
        '    'Dim len As Integer = comments.Length
        '    'comments = comments & Space(spaces - comments.Length)
        '    'spaces += 250
        'Catch ex As Exception
        '    MsgBox("Out of comment space")
        'End Try
        Dim spaces As Integer
        Try

            spaces = (Int((comments.Length / 250)) + 1) * 250
            comments = comments & Space(spaces - comments.Length)
        Catch ex As Exception
            MsgBox("error in spacing comments")
        End Try

    End Sub
    Private Function validate_record() As Boolean
        equity_check = False
        Dim orig_no_of_errors As Integer = no_of_errors
        If maat_id = "" Then ' D.J.Dicks 09.12.2011 Why is this in "Or maat_id <> 2968467"
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = Nothing
            error_table(no_of_errors).error_text = "invalid maat id"
        End If

        If earlier_contrib_found = True Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "MAAT ID contrib ID is less than previous"
        End If

        If outcome <> "" Then
            If outcome <> "CONVICTED" And outcome <> "PART CONVICTED" And outcome <> "APPEAL" And outcome <> "ABANDONED" _
                    And outcome <> "AQUITTED" And outcome <> "DISMISSED" And outcome <> "DISMISSED" _
                    And outcome <> "PART SUCCESS" And outcome <> "UNSUCCESSFUL" And outcome <> "SUCCESSFUL" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid crown court outcome"
            End If
        End If

        If appeal_type <> "" Then
            If appeal_type <> "ASE" And appeal_type <> "ACV" And appeal_type <> "ACS" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid appeal type"
            End If
        End If

        If rep_status <> "" Then
            If rep_status <> "CURR" And rep_status <> "ERR" And rep_status <> "SUSP" _
            And rep_status <> "FI" And rep_status <> "NOT SENT FOR TRIAL" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid rep type"
            End If
        End If

        If on_onestep Then
            Return (True)
        End If
        If first_name = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid first name"
        End If

        If surname = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid surname"
        End If

        If mthly_contrib_amt_str = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid monthly contribution"
        ElseIf mthly_contrib_amt = 0 Then
            If outcome = "CONVICTED" Or outcome = "PART CONVICTED" Then
                equity_check = True
            ElseIf outcome <> "APPEAL" And UCase(Microsoft.VisualBasic.Left(case_type, 6)) <> "APPEAL" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid monthly contribution"
            End If
        End If

        If case_type = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid case type"
        End If

        If in_court_custody <> "Yes" And in_court_custody <> "No" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid in court custody"
        End If

        If emp_status = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid employment status"
        End If

        If effective_date = Nothing And future_effective_date = False Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid effective date"
        End If

        If income_contrib_cap_str = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid income contribution cap"
        End If

        'If income_uplift_applied <> "Yes" And income_uplift_applied <> "No" Then
        '    no_of_errors += 1
        '    error_table(no_of_errors).contrib_id = contrib_id
        '    error_table(no_of_errors).applicant_id = applicant_id
        '    error_table(no_of_errors).maat_id = maat_id
        '    error_table(no_of_errors).error_text = "invalid income uplift applied"
        'End If

        If equity_amt_verified <> "" Then
            If equity_amt_verified <> "Yes" And equity_amt_verified <> "No" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid equity amount verified"
            End If
        End If

        If cap_amt_verified <> "" Then
            If LCase(cap_amt_verified) <> "yes" And cap_amt_verified <> "no" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid capital amount verified"
            End If
        End If

        If orig_no_of_errors = no_of_errors Then
            audit_file = audit_file & maat_id & "|no errors" & vbNewLine
            Return (True)
        Else
            equity_check = False
            audit_file = audit_file & maat_id & "|errors" & vbNewLine
            If no_of_errors = UBound(error_table) Then
                ReDim Preserve error_table(no_of_errors + 10)
            End If
            Return (False)
        End If

    End Function

    Private Sub Write_ack(ByVal writer As Xml.XmlWriter, ByVal filename_id As String, _
    ByVal filename_id_tag As String, ByVal valid_recs As Integer, ByVal rej_recs As Integer, _
    ByVal no_of_errors As Integer)
        writer.WriteStartElement("CONTRIBUTIONS_FILE_ACK")
        writer.WriteStartElement("FILENAME")
        writer.WriteAttributeString("ID", filename_id_tag)
        writer.WriteString(filename_id)
        writer.WriteEndElement()
        writer.WriteElementString("DATE_LOADED", Format(Now, "dd-MM-yyyy"))
        writer.WriteElementString("NO_OF_RECORDS_LOADED", valid_recs)
        writer.WriteElementString("NO_OF_RECORDS_REJECTED", rej_recs)

        Dim idx As Integer
        Dim contrib_id As Double = 0
        Dim last_contrib_id As Double = 0
        Dim applicant_id As Double = 0
        Dim maat_id As Double = 0
        Dim error_text As String = ""
        For idx = 1 To no_of_errors
            If idx = 1 Then
                writer.WriteStartElement("REJECTED_RECORD_LIST")
            End If
            contrib_id = error_table(idx).contrib_id
            If contrib_id <> last_contrib_id Then
                last_contrib_id = contrib_id
                writer.WriteStartElement("CONTRIBUTIONS")
                writer.WriteAttributeString("ID", contrib_id)
                applicant_id = error_table(idx).applicant_id
                maat_id = error_table(idx).maat_id
                writer.WriteElementString("APPLICANT_ID", applicant_id)
                writer.WriteElementString("MAAT_ID", maat_id)
            End If

            error_text = error_table(idx).error_text
            writer.WriteElementString("ERROR_TEXT", error_text)

            If idx = no_of_errors Then
                writer.WriteEndElement()
                writer.WriteEndElement()
            ElseIf contrib_id <> error_table(idx + 1).contrib_id Then
                writer.WriteEndElement()
            End If
        Next
        writer.WriteEndElement()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub end_of_record()

        Try
            If applicant_id = "" Then
                write_error("No applicant found")
                Exit Sub
            End If
            If maat_id = "" Then
                write_error("No maat found")
                Exit Sub
            End If

            If allowable_cap_threshold <> "" Then
                comments = comments & "Allowable cap threshold:" & allowable_cap_threshold & ";"
                space_comments()
            End If

            'If outcome <> "" Then
            '    comments = comments & "Outcome:" & outcome & ";"
            '    space_comments()
            'End If

            Dim idx3 As Integer
            Try
                For idx3 = 1 To asset_no
                    comments = comments & "Cap asset type:" & asset_table(idx3, 1) & ";"
                    comments = comments & "Cap amt:" & asset_table(idx3, 2) & ";"
                    comments = comments & "Cap amt verified:" & asset_table(idx3, 3) & ";"
                    If asset_table(idx3, 4) <> "" Then
                        comments = comments & "Cap amt Date Verified:" & asset_table(idx3, 4) & ";"
                    End If
                    If asset_table(idx3, 5) <> "" Then
                        comments = comments & "Cap amt Evidence Received Date:" & asset_table(idx3, 5) & ";"
                    End If
                    space_comments()
                Next
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try



            If landline = "" Then
                landline = mobile
                mobile = ""
            End If
            'use final costs if appeal type and mthly amt=0
            'If outcome = "APPEAL" Or UCase(Microsoft.VisualBasic.Left(case_type, 6)) = "APPEAL" _
            'And mthly_contrib_amt = 0 And upfront_contrib_amt = 0 Then
            '    upfront_contrib_amt = final_defence_cost
            '    comments = comments & "Final Defence Costs:" & Format(final_defence_cost, "F") & ";"
            '    space_comments()
            'End If
            record = applicant_id & "|" & summons_no & "|" & maat_id & "|" & first_name & "|" & _
                                        surname & "|" & dob & "|" & ni_no & "|" & mthly_contrib_amt & "|" & upfront_contrib_amt & "|" & _
                                        income_contrib_cap & "|" & debt_addr1 & "|" & debt_addr2 & "|" & debt_addr3 & "|" & _
                                        debt_addr4 & "|" & debt_postcode & "|" & postal_addr1 & "|" & postal_addr2 & "|" & _
                                        postal_addr3 & "|" & postal_addr4 & "|" & postal_postcode & "|" & landline & "|" & mobile & "|" & email & _
                                        "|"
            If future_effective_date Then
                record = record & ""
            Else
                record = record & Format(effective_date, "dd-MM-yyyy")
            End If
            'check if maat-id already exists on onestep
            param1 = "onestep"
            param2 = "select  clientschemeID, _rowid from Debtor" & _
            " where  client_ref = '" & maat_id & "'"
            Dim debtor1_dataset As DataSet = get_dataset(param1, param2)
            Dim debtor As Integer = 0
            If no_of_rows > 0 Then
                Dim idx As Integer
                Dim csid_no As Integer = no_of_rows - 1
                For idx = 0 To csid_no
                    Dim csid As Integer = debtor1_dataset.Tables(0).Rows(idx).Item(0)
                    param2 = "select clientID from ClientScheme where _rowid = " & csid
                    Dim csid_datatset As DataSet = get_dataset(param1, param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read clientscheme for csid = " & csid)
                        Exit Sub
                    End If

                    'use <> 24 for test client; 909 for production client
                    If csid_datatset.Tables(0).Rows(0).Item(0) <> 909 Then
                        Continue For
                    Else
                        debtor = debtor1_dataset.Tables(0).Rows(idx).Item(1)
                        Exit For
                    End If
                Next
            End If

            If no_of_rows = 0 Or debtor = 0 Then
                on_onestep = False
                audit_file = audit_file & maat_id & "|not on onestep" & vbNewLine
                Dim comments2 As String = ""
                Dim comments3 As String = comments
                'If comments.Length <= 250 Then
                record = record & "|" & comments
                'Else
                '    While comments3.Length > 250
                '        Dim len As Integer = Microsoft.VisualBasic.Len(comments3)
                '        Dim idx As Integer
                '        For idx = 250 To 1 Step -1
                '            If Mid(comments3, idx, 1) = ";" Then
                '                Exit For
                '            End If
                '        Next
                '        comments2 = comments2 & Microsoft.VisualBasic.Left(comments3, idx)
                '        Dim idx2 As Integer
                '        For idx2 = idx To 250
                '            comments2 = comments2 & " "
                '        Next
                '        comments3 = Microsoft.VisualBasic.Right(comments3, len - idx)
                '    End While
                'record = record & "|" & comments2 & comments3
                'end If
                Exit Sub
            End If

            'case exists on onestep so look for changes
            audit_file = audit_file & maat_id & "|on onestep" & vbNewLine
            Dim change_found As Boolean = False
            on_onestep = True
            param2 = "select _rowid, offence_number, offence_court, debt_amount, debt_costs, " & _
           " offenceValue, empNI, add_phone, add_fax, addEmail, prevReference, add_postcode, " & _
           " address, clientschemeID from Debtor" & _
           " where _rowid = " & debtor
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to read debtorID = " & debtor)
                Exit Sub
            End If

            'see if prevref has changed
            Dim orig_prevref As String
            Try
                orig_prevref = debtor_dataset.Tables(0).Rows(0).Item(10)
            Catch
                orig_prevref = ""
            End Try
            If applicant_id <> orig_prevref Then
                write_change(debtor & "|" & maat_id & "|" & "Prev Reference(Applicant)" & "|" & _
                                orig_prevref & "|" & applicant_id & vbNewLine)
                change_found = True
            End If

            'see if postcode has changed
            Dim orig_postcode As String
            Try
                orig_postcode = Trim(debtor_dataset.Tables(0).Rows(0).Item(11))
            Catch
                orig_postcode = ""
            End Try
            If debt_postcode.Length > 0 Then
                If Trim(LCase(Microsoft.VisualBasic.Left(debt_postcode, 3)) <> LCase(Microsoft.VisualBasic.Left(orig_postcode, 3))) Then
                    Dim orig_home_addr = debtor_dataset.Tables(0).Rows(0).Item(12)
                    Dim idx As Integer
                    Dim addr As String = ""
                    For idx = 1 To Microsoft.VisualBasic.Len(Trim(orig_home_addr))
                        If Mid(orig_home_addr, idx, 1) <> Chr(10) And _
                        Mid(orig_home_addr, idx, 1) <> Chr(13) Then
                            addr = addr & Mid(orig_home_addr, idx, 1)
                        Else
                            addr = addr & " "
                        End If
                    Next
                    write_change(debtor & "|" & maat_id & "|" & "Current Address" & "|" & _
                                    addr & "|" & debt_addr1 & " " & debt_addr2 & " " & debt_addr3 & " " & _
                                    debt_addr4 & " " & debt_postcode & vbNewLine)
                    change_found = True
                End If
            End If

            'see if summons number has changed
            Dim orig_summons_no As String
            Try
                orig_summons_no = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
            Catch
                orig_summons_no = ""
            End Try
            If Trim(LCase(summons_no)) <> LCase(orig_summons_no) Then
                write_change(debtor & "|" & maat_id & "|" & "Summons number" & "|" & _
                orig_summons_no & "|" & summons_no & vbNewLine)
                change_found = True
            End If

            'get notes into dataset
            param2 = "select text from Note where debtorID = " & debtor & _
                            " and type = 'Client note' order by _rowid desc"
            Dim note_dataset As DataSet = get_dataset(param1, param2)
            Dim note_text As String = ""
            Dim no_of_notes As String = no_of_rows

            'see if effective date has changed
            Dim orig_effective_date As Date
            Dim orig_effective_date_found As Boolean = True
            Try
                orig_effective_date = debtor_dataset.Tables(0).Rows(0).Item(2)
            Catch ex As Exception
                orig_effective_date_found = False
            End Try
            If orig_effective_date_found Then
                If effective_date <> orig_effective_date Then
                    Dim os_date As Date = orig_effective_date
                    write_change(debtor & "|" & maat_id & "|" & "Effective date(LO date)" & _
                    "|" & Format(os_date, "dd-MM-yyyy") & "|" & Format(effective_date, "dd-MM-yyyy") & vbNewLine)
                    change_found = True
                End If
            Else
                If future_effective_date Then
                    Dim eff_text As String = ""
                    For idx3 = 0 To no_of_rows - 1
                        eff_text = note_dataset.Tables(0).Rows(idx3).Item(0)
                        Dim start_idx As Integer = InStr(eff_text, "Effective Date:")
                        If start_idx > 0 Then
                            eff_text = Mid(eff_text, start_idx + 15, 10)
                            Exit For
                        End If
                    Next
                    If eff_text.Length > 0 Then
                        If effective_date <> CDate(eff_text) Then
                            write_change(debtor & "|" & maat_id & "|" & "Effective date(LO date)" & _
                                                   "|" & CDate(eff_text) & "|" & Format(effective_date, "dd-MM-yyyy") & vbNewLine)
                            write_eff_date(debtor & "|" & effective_date_str & vbNewLine)
                            change_found = True
                        End If
                    End If
                Else
                    If effective_date <> Nothing Then
                        write_change(debtor & "|" & maat_id & "|" & "Effective date(LO date)" & _
                                            "|" & " " & "|" & Format(effective_date, "dd-MM-yyyy") & vbNewLine)
                        change_found = True
                    End If
                End If
            End If

            'see if debt amt has changed
            Dim test_amt As Decimal
            If upfront_contrib_amt_str = "" Then
                test_amt = 0
            Else
                test_amt = upfront_contrib_amt_str   ' pounds not pence/ 100
            End If
            Dim debt_amt As Decimal = debtor_dataset.Tables(0).Rows(0).Item(3)
            If test_amt <> debt_amt Then
                'no change if final costs = costs on onestep
                'If debt_amt <> final_defence_cost Then
                write_change(debtor & "|" & maat_id & "|" & "Debt amount" & "|" & _
                                Format(debt_amt, "#.##") & "|" & Format(test_amt, "#.##") & vbNewLine)
                change_found = True
                ' End If
            End If

            'see if debt costs has changed
            If mthly_contrib_amt_str = "" Then
                test_amt = 0
            Else
                test_amt = mthly_contrib_amt_str    'pounds not pence / 100
            End If
            Dim debt_costs As Decimal = debtor_dataset.Tables(0).Rows(0).Item(4)
            If test_amt <> debt_costs Then
                write_change(debtor & "|" & maat_id & "|" & "Debt Costs" & "|" & _
                                Format(debt_costs, "#.##") & "|" & test_amt & vbNewLine)
                change_found = True
            End If

            'see if offence value has changed
            If income_contrib_cap_str = "" Then
                test_amt = 0
            Else
                test_amt = income_contrib_cap_str     'pounds not pence / 100
            End If
            Dim offence_value As Decimal = debtor_dataset.Tables(0).Rows(0).Item(5)
            If test_amt <> offence_value Then
                write_change(debtor & "|" & maat_id & "|" & "Offence value" & "|" & _
                Format(offence_value, "#.##") & "|" & Format(test_amt, "#.##") & vbNewLine)
                change_found = True
            End If

            'see if NIno has changed
            Dim orig_ni_no As String
            Try
                orig_ni_no = debtor_dataset.Tables(0).Rows(0).Item(6)
            Catch
                orig_ni_no = ""
            End Try
            If LCase(ni_no) <> LCase(orig_ni_no) Then
                write_change(debtor & "|" & maat_id & "|" & "NI No" & "|" & orig_ni_no & "|" & ni_no & vbNewLine)
                change_found = True
            End If

            'check if phone number has changed
            Dim orig_phone_no As String
            Try
                orig_phone_no = Trim(debtor_dataset.Tables(0).Rows(0).Item(7))
            Catch
                orig_phone_no = ""
            End Try

            Dim orig_fax_no As String
            Try
                orig_fax_no = Trim(debtor_dataset.Tables(0).Rows(0).Item(8))
            Catch
                orig_fax_no = ""
            End Try
            If landline <> "" Then
                If landline <> orig_phone_no And _
                landline <> orig_fax_no Then
                    write_change(debtor & "|" & maat_id & "|" & "Phone No" & "|" & orig_phone_no & "|" & landline & vbNewLine)
                    change_found = True
                End If
            End If
            If mobile <> "" Then
                If mobile <> orig_phone_no And _
                mobile <> orig_fax_no Then
                    write_change(debtor & "|" & maat_id & "|" & "Mobile No" & "|" & orig_fax_no & "|" & mobile & vbNewLine)
                    change_found = True
                End If
            End If

            'check if email has changed
            Dim orig_email As String
            Try
                orig_email = debtor_dataset.Tables(0).Rows(0).Item(9)
            Catch
                orig_email = ""
            End Try
            If email <> orig_email Then
                write_change(debtor & "|" & maat_id & "|" & "Email" & "|" & orig_email & "|" & email & vbNewLine)
                change_found = True
            End If
            Dim idx4 As Integer

            'see if outcomes have changed
            If outcome <> "" Then
                Dim note_outcome As String = ""
                Dim note2_outcome As String = ""
                Dim first_outcome_found As Boolean = False
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "outcome:")
                    If start_idx > 0 Then
                        start_idx += 8
                    End If
                    If start_idx > 0 Then
                        If first_outcome_found = False Then
                            For idx4 = start_idx To note_text.Length
                                If Mid(note_text, idx4, 1) = ";" Then
                                    note_outcome = Mid(note_text, start_idx, idx4 - start_idx)
                                    Exit For
                                End If
                            Next
                            'continue if there is a second outcome
                            If outcome2 = "" Then
                                Exit For
                            End If
                            first_outcome_found = True
                        Else
                            For idx4 = start_idx To note_text.Length
                                If Mid(note_text, idx4, 1) = ";" Then
                                    note2_outcome = Mid(note_text, start_idx, idx4 - start_idx)
                                    Exit For
                                End If
                            Next
                            Exit For
                        End If
                    End If
                Next
                If LCase(outcome) <> note_outcome And LCase(outcome) <> note2_outcome Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Outcome" & "|" & "" & "|" & outcome & vbNewLine)
                    write_note(debtor & "|" & "OUTCOME:" & outcome)
                End If
                If outcome2 <> "" Then
                    If LCase(outcome2) <> note_outcome And LCase(outcome2) <> note2_outcome Then
                        change_found = True
                        write_outcome(debtor & "|" & maat_id & "|" & "Outcome" & "|" & "" & "|" & outcome2 & vbNewLine)
                        write_note(debtor & "|" & "OUTCOME:" & outcome2)
                    End If
                End If
            End If


            'if case-type is appeal and clientscheme <> 2109(appeal scheme) then write to outcome file
            If UCase(Microsoft.VisualBasic.Left(case_type, 6)) = "APPEAL" And _
            debtor_dataset.Tables(0).Rows(0).Item(13) <> 2109 Then
                change_found = True
                write_outcome(debtor & "|" & maat_id & "|" & "Case type Appeal" & "|" & " " & "|" & case_type & vbNewLine)
            End If

            'see if bank account no has changed
            Dim bank_note As String = ""
            If bank_acc_no.Length > 0 Then
                Dim note_acc_no = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "bank acc no:")
                    If start_idx > 0 Then
                        start_idx += 12
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_acc_no = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_acc_no = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If
                    End If
                    If note_acc_no <> "" Then
                        Exit For
                    End If
                Next
                If bank_acc_no <> note_acc_no Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Bank Acc No" & "|" & note_acc_no & "|" & bank_acc_no & vbNewLine)
                    bank_note = debtor & "|" & "BANK DETAILS;Bank Acc No:" & bank_acc_no & ";"
                End If
            End If

            'see if bank sort code has changed
            If bank_sort_code.Length > 0 Then
                Dim note_sort_code = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "bank sortcode:")
                    If start_idx = 0 Then
                        start_idx = InStr(note_text, "bank sort code:") + 1
                    End If
                    If start_idx > 1 Then
                        start_idx += 14
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_sort_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_sort_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If
                    End If
                    If note_sort_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(bank_sort_code) <> LCase(note_sort_code) Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Bank Sortcode" & "|" & note_sort_code & "|" & bank_sort_code & vbNewLine)
                    If bank_note = "" Then
                        bank_note = debtor & "|" & "BANK DETAILS;Bank Acc No:" & bank_acc_no & ";"
                    End If
                    bank_note = bank_note & "Bank Sortcode:" & bank_sort_code & ";"
                ElseIf bank_note <> "" Then
                    bank_note = bank_note & "Bank Sortcode:" & bank_sort_code & ";"
                End If
            ElseIf bank_note <> "" Then
                bank_note = bank_note & "Bank Sortcode:" & bank_sort_code & ";"
            End If

            'see if bank acc name has changed
            If bank_acc_name.Length > 0 Then
                Dim note_acc_name As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "bank acc name:")
                    If start_idx > 0 Then
                        start_idx += 14
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_acc_name = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_acc_name = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If
                    End If
                    If note_acc_name <> "" Then
                        Exit For
                    End If
                Next
                If LCase(bank_acc_name) <> LCase(note_acc_name) Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Bank Acc Name" & "|" & note_acc_name & "|" & bank_acc_name & vbNewLine)
                    If bank_note = "" Then
                        If bank_note = "" Then
                            bank_note = debtor & "|" & "BANK DETAILS;Bank Acc No:" & _
                            bank_acc_no & ";Bank Sortcode:" & bank_sort_code & ";"
                        End If
                    End If
                    bank_note = bank_note & "Bank Acc Name:" & bank_acc_name
                    write_note(bank_note)
                ElseIf bank_note <> "" Then
                    bank_note = bank_note & "Bank Acc Name:" & bank_acc_name
                    write_note(bank_note)
                End If
            ElseIf bank_note <> "" Then
                bank_note = bank_note & "Bank Acc Name:" & bank_acc_name
                write_note(bank_note)
            End If

            'see if Rep-Order Withdrawal Date has changed
            If rep_order_withdrawal_date <> Nothing Then
                Dim note_date As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "rep-order-withdrawn-date:")
                    If start_idx > 0 Then
                        start_idx += 25
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_date = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_date = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If
                    End If
                    If note_date <> "" Then
                        Exit For
                    End If
                Next
                If note_date = "" Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Rep-order-Withdrawn-Date" & "||" & _
                    Format(rep_order_withdrawal_date, "dd.MMM.yyyy") & "|" & vbNewLine)
                    write_note(debtor & "|" & "REP-ORDER-WITHDRAWN-DATE:" & Format(rep_order_withdrawal_date, "dd.MMM.yyyy"))
                Else
                    If rep_order_withdrawal_date <> CDate(note_date) Then
                        change_found = True
                        write_outcome(debtor & "|" & maat_id & "|" & "Rep-order-Withdrawn-Date" & "||" & _
                        Format(rep_order_withdrawal_date, "dd.MMM.yyyy") & "|" & vbNewLine)
                        write_note(debtor & "|" & "REP-ORDER-WITHDRAWN-DATE:" & Format(rep_order_withdrawal_date, "dd.MMM.yyyy"))
                    End If
                End If
            End If

            'see if final costs entered
            'If final_defence_cost <> Nothing Then
            '    If debt_amt <> final_defence_cost And _
            '        debt_costs <> final_defence_cost Then
            '        write_change(debtor & "|" & maat_id & "|" & "Final Costs" & "|" & " " & "|" & _
            '        Format(final_defence_cost, "F") & vbNewLine)
            '        write_note(debtor & "|" & "FINAL-DEFENCE-COSTS:" & Format(final_defence_cost, "F"))
            '        change_found = True
            '    End If
            'End If

            'see if inc-uplift-applied has changed
            Dim note_inc_uplift_applied As String = ""
            For idx3 = 0 To no_of_notes - 1
                note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                Dim start_idx As Integer = InStr(note_text, "income-uplift-applied:")
                If start_idx > 0 Then
                    start_idx += 22
                Else
                    start_idx = InStr(LCase(note_text), "inc uplift applied:")
                    If start_idx > 0 Then
                        start_idx += 19
                    End If
                End If
                If start_idx > 0 Then
                    note_inc_uplift_applied = UCase(Mid(note_text, start_idx, 1))
                    If note_inc_uplift_applied = "Y" Then
                        note_inc_uplift_applied = "Yes"
                    Else
                        note_inc_uplift_applied = "No"
                    End If
                End If
                If note_inc_uplift_applied <> "" Then
                    Exit For
                End If
            Next
            If note_inc_uplift_applied = "" Then
                note_inc_uplift_applied = "No"
            End If
            If income_uplift_applied <> "" And income_uplift_applied <> note_inc_uplift_applied Then
                write_change(debtor & "|" & maat_id & "|" & "Income Uplift Applied" & "|" & _
                income_uplift_applied & "|" & note_inc_uplift_applied & vbNewLine)
                write_note(debtor & "|" & "INCOME-UPLIFT-APPLIED:" & income_uplift_applied)
                change_found = True
            End If

            'see if equity amount verified has changed
            Dim note_equity_amt_verified As String = ""
            For idx3 = 0 To no_of_notes - 1
                note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                Dim start_idx As Integer = InStr(note_text, "equity amt verified:")
                If start_idx > 0 Then
                    start_idx += 20
                End If
                If start_idx > 0 Then
                    note_equity_amt_verified = UCase(Mid(note_text, start_idx, 1))
                    If note_equity_amt_verified = "Y" Then
                        note_equity_amt_verified = "Yes"
                    Else
                        note_equity_amt_verified = "No"
                    End If
                End If
                If note_equity_amt_verified <> "" Then
                    Exit For
                End If
            Next
            If note_equity_amt_verified = "" Then
                note_equity_amt_verified = "No"
            End If
            If equity_amt_verified = "" Then
                equity_amt_verified = "No"
            End If
            If equity_amt_verified <> note_equity_amt_verified Then
                write_change(debtor & "|" & maat_id & "|" & "Equity Amt Verified" & "|" & _
                equity_amt_verified & "|" & note_equity_amt_verified & vbNewLine)
                write_note(debtor & "|" & "Equity Amt Verified:" & equity_amt_verified)
                change_found = True
            End If

            'see if equity amount has changed
            Dim note_equity_amt As Decimal = -9999
            For idx3 = 0 To no_of_notes - 1
                note_text = note_dataset.Tables(0).Rows(idx3).Item(0)
                Dim start_idx As Integer = InStr(note_text, "Equity Amt:")
                If start_idx > 0 Then
                    start_idx += 11
                End If
                If start_idx > 0 Then
                    For idx4 = start_idx To note_text.Length
                        If Mid(note_text, idx4, 1) = ";" Then
                            note_equity_amt = Mid(note_text, start_idx, idx4 - start_idx)
                            Exit For
                        End If
                    Next
                    If idx4 > note_text.Length Then
                        note_equity_amt = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                    End If
                End If
                If note_equity_amt <> -9999 Then
                    Exit For
                End If
            Next
            If equity_amt <> note_equity_amt And equity_amt <> -9999 Then
                If note_equity_amt <> -9999 Then
                    write_change(debtor & "|" & maat_id & "|" & "Equity Amt" & "|" & _
                                    equity_amt & "|" & note_equity_amt & vbNewLine)
                    write_note(debtor & "|" & "Equity Amt:" & equity_amt)
                    change_found = True
                End If
            End If

            'see if applicant equity amount has changed
            Dim note_appl_equity_amt As Decimal = -9999
            For idx3 = 0 To no_of_notes - 1
                note_text = note_dataset.Tables(0).Rows(idx3).Item(0)
                Dim start_idx As Integer = InStr(note_text, "Applicant Equity Amount:")
                If start_idx > 0 Then
                    start_idx += 24
                End If
                If start_idx > 0 Then
                    For idx4 = start_idx To note_text.Length
                        If Mid(note_text, idx4, 1) = ";" Then
                            note_appl_equity_amt = Mid(note_text, start_idx, idx4 - start_idx)
                            Exit For
                        End If
                    Next
                    If idx4 > note_text.Length Then
                        note_appl_equity_amt = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                    End If
                End If
                If note_appl_equity_amt <> -9999 Then
                    Exit For
                End If
            Next
            If appl_equity_amt <> note_appl_equity_amt And appl_equity_amt <> -9999 Then
                If note_appl_equity_amt <> -9999 Then
                    write_change(debtor & "|" & maat_id & "|" & "Applicant Equity Amount" & "|" & _
                                    appl_equity_amt & "|" & note_appl_equity_amt & vbNewLine)
                    write_note(debtor & "|" & "Applicant Equity Amount:" & appl_equity_amt)
                    change_found = True
                End If
            End If

            'see if partner equity amount has changed
            Dim note_part_equity_amt As Decimal = -9999
            For idx3 = 0 To no_of_notes - 1
                note_text = note_dataset.Tables(0).Rows(idx3).Item(0)
                Dim start_idx As Integer = InStr(note_text, "Partner Equity Amount:")
                If start_idx > 0 Then
                    start_idx += 22
                End If
                If start_idx > 0 Then
                    For idx4 = start_idx To note_text.Length
                        If Mid(note_text, idx4, 1) = ";" Then
                            note_part_equity_amt = Mid(note_text, start_idx, idx4 - start_idx)
                            Exit For
                        End If
                    Next
                    If idx4 > note_text.Length Then
                        note_part_equity_amt = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                    End If
                End If
                If note_part_equity_amt <> -9999 Then
                    Exit For
                End If
            Next
            If partner_equity_amt <> note_part_equity_amt And partner_equity_amt <> -9999 Then
                If note_part_equity_amt <> -9999 Then
                    write_change(debtor & "|" & maat_id & "|" & "Partner Equity Amount" & "|" & _
                                    partner_equity_amt & "|" & note_part_equity_amt & vbNewLine)
                    write_note(debtor & "|" & "Partner Equity Amount:" & partner_equity_amt)
                    change_found = True
                End If
            End If

            'see if Has Partner has changed
            If has_partner <> "" Then
                Dim note_has_partner As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "has partner:")
                    If start_idx > 0 Then
                        start_idx += 12
                    End If
                    If start_idx > 0 Then
                        note_has_partner = Mid(note_text, start_idx, 1)
                        If note_has_partner = "y" Then
                            note_has_partner = "yes"
                        Else
                            note_has_partner = "no"
                        End If
                    End If
                    If note_has_partner <> "" Then
                        Exit For
                    End If
                Next

                If has_partner <> note_has_partner Then
                    write_note(debtor & "|" & "Has Partner:" & has_partner)
                    change_found = True
                End If
            End If

            'see if ci_code has changed
            If ci_code <> "" Then
                Dim note_ci_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "contrary interest code:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_ci_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_ci_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_ci_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(ci_code) <> note_ci_code Then
                    write_note(debtor & "|" & "Contrary Interest Code:" & ci_code)
                    change_found = True
                End If
            End If

            'see if ci_desc has changed
            If ci_desc <> "" Then
                Dim note_ci_desc As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "contrary interest description:")
                    If start_idx > 0 Then
                        start_idx += 30
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_ci_desc = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_ci_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_ci_desc <> "" Then
                        Exit For
                    End If
                Next
                If LCase(ci_desc) <> note_ci_desc Then
                    write_note(debtor & "|" & "Contrary Interest Description:" & ci_desc)
                    change_found = True
                End If
            End If

            'see if declared value has changed
            If declared_value <> Nothing Then
                Dim note_declared_value As Decimal = Nothing
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "declared value:")
                    If start_idx > 0 Then
                        start_idx += 15
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_declared_value = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_declared_value = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_declared_value <> Nothing Then
                        Exit For
                    End If
                Next
                If declared_value <> note_declared_value Then
                    write_note(debtor & "|" & "Declared value:" & declared_value)
                    change_found = True
                End If
            End If

            'see if no fixed abode has changed
            Dim note_nfa As String = ""
            If nfa <> "" Then
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "no fixed abode:")
                    If start_idx > 0 Then
                        start_idx += 15
                    End If
                    If start_idx > 0 Then
                        note_nfa = Mid(note_text, start_idx, 1)
                        If note_nfa = "y" Then
                            note_nfa = "yes"
                        Else
                            note_nfa = "no"
                        End If
                    End If
                    If note_nfa <> "" Then
                        Exit For
                    End If
                Next
                If nfa <> note_nfa Then
                    write_note(debtor & "|" & "No Fixed Abode:" & nfa)
                    change_found = True
                End If
            End If

            'see if special investigation has changed
            If si <> "" Then
                Dim note_si As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "special investigation:")
                    If start_idx > 0 Then
                        start_idx += 22
                    End If
                    If start_idx > 0 Then
                        note_si = Mid(note_text, start_idx, 1)
                        If note_si = "y" Then
                            note_si = "yes"
                        Else
                            note_si = "no"
                        End If
                    End If
                    If note_si <> "" Then
                        Exit For
                    End If
                Next
                If si <> note_si Then
                    write_note(debtor & "|" & "Special Investigation:" & si)
                    change_found = True
                End If
            End If

            'see if emp status has changed
            If emp_status <> "" Then
                Dim note_emp_status As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "emp status:")
                    If start_idx > 0 Then
                        start_idx += 11
                    End If
                    If start_idx > 0 And InStr(note_text, "partner emp status:") = 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_emp_status = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_emp_status = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_emp_status <> "" Then
                        Exit For
                    End If
                Next
                If LCase(emp_status) <> note_emp_status Then
                    write_note(debtor & "|" & "Emp Status:" & emp_status)
                    change_found = True
                End If
            End If

            'see if emp code has changed
            If emp_code <> "" Then
                Dim note_emp_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "emp code:")
                    If start_idx > 0 Then
                        start_idx += 9
                    End If
                    If start_idx > 0 And InStr(note_text, "partner emp code") = 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_emp_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_emp_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_emp_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(emp_code) <> note_emp_code Then
                    write_note(debtor & "|" & "Emp Code:" & emp_code)
                    change_found = True
                End If
            End If

            'see if Disability Declaration has changed
            If disability_declaration <> "" Then
                Dim note_disability_declaration As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "disability declaration:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_disability_declaration = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_disability_declaration = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_disability_declaration <> "" Then
                        Exit For
                    End If
                Next
                If LCase(disability_declaration) <> note_disability_declaration Then
                    write_note(debtor & "|" & "Disability Declaration:" & disability_declaration)
                    change_found = True
                End If
            End If

            'see if Disability Description has changed
            If disability_description <> "" Then
                Dim note_disability_description As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "disability description:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_disability_description = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                    End If
                    If note_disability_description <> "" Then
                        If LCase(disability_description) = note_disability_description Then
                            Exit For
                        Else
                            note_text = Microsoft.VisualBasic.Right(note_text, note_text.Length - start_idx)
                            start_idx = InStr(note_text, "disability description:")
                            If start_idx > 0 Then
                                start_idx += 23
                            End If
                            If start_idx > 0 Then
                                For idx4 = start_idx To note_text.Length
                                    If Mid(note_text, idx4, 1) = ";" Then
                                        note_disability_description = Mid(note_text, start_idx, idx4 - start_idx)
                                        Exit For
                                    End If
                                Next
                            End If
                            Exit For
                        End If
                    End If
                Next
                If LCase(disability_description) <> note_disability_description Then
                    write_note(debtor & "|" & "Disability Description:" & disability_description)
                    change_found = True
                End If
            End If

            'see if case-type has changed
            If case_type <> "" Then
                Dim note_case_type As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "case type:")
                    If start_idx > 0 Then
                        start_idx += 10
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_case_type = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_case_type = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_case_type <> "" Then
                        Exit For
                    End If
                Next
                If LCase(case_type) <> note_case_type Then
                    write_note(debtor & "|" & "Case type:" & case_type)
                    change_found = True
                End If
            End If

            'see if case-type code has changed
            If case_type_code <> "" Then
                Dim note_case_type_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "case type code:")
                    If start_idx > 0 Then
                        start_idx += 15
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_case_type_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_case_type_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_case_type_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(case_type_code) <> note_case_type_code Then
                    write_note(debtor & "|" & "Case type Code:" & case_type_code)
                    change_found = True
                End If
            End If
            'see if Rep Status has changed
            If rep_status <> "" Then
                Dim note_rep_status As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "rep status:")
                    If start_idx > 0 Then
                        start_idx += 11
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_rep_status = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_rep_status = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_rep_status <> "" Then
                        Exit For
                    End If
                Next
                If note_rep_status = "" Then
                    write_note(debtor & "|" & "Rep Status:" & rep_status)
                    change_found = True
                Else
                    If LCase(rep_status) <> note_rep_status Then
                        write_note(debtor & "|" & "Rep Status:" & rep_status)
                        change_found = True
                    End If
                End If
            End If

            'see if Rep Status Description has changed
            If rep_status_desc <> "" Then
                Dim note_rep_status_desc As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "rep status description:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_rep_status_desc = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_rep_status_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_rep_status_desc <> "" Then
                        Exit For
                    End If
                Next
                If note_rep_status_desc = "" Then
                    write_note(debtor & "|" & "Rep Status Description:" & rep_status_desc)
                    change_found = True
                Else
                    If LCase(rep_status_desc) <> note_rep_status_desc Then
                        write_note(debtor & "|" & "Rep Status Description:" & rep_status_desc)
                        change_found = True
                    End If
                End If
            End If

            'see if Rep Status Date has changed
            If rep_status_date <> Nothing Then
                Dim note_rep_status_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "rep status date:")
                    If start_idx > 0 Then
                        start_idx += 16
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_rep_status_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_rep_status_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_rep_status_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_rep_status_date_str)
                    If rep_status_date <> test_date Then
                        write_note(debtor & "|" & "Rep Status Date:" & Format(rep_status_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Rep Status Date:" & Format(rep_status_date, "dd/MM/yyyy"))
                End Try
            End If

            'see if in court custody has changed
            If in_court_custody <> "" Then
                Dim note_in_court_custody As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "in court custody:")
                    If start_idx > 0 Then
                        start_idx += 17
                    End If
                    If start_idx > 0 Then
                        note_in_court_custody = Mid(note_text, start_idx, 1)
                        If note_in_court_custody = "y" Then
                            note_in_court_custody = "yes"
                        Else
                            note_in_court_custody = "no"
                        End If
                    End If
                    If note_in_court_custody <> "" Then
                        Exit For
                    End If
                Next
                If LCase(in_court_custody) <> note_in_court_custody Then
                    write_note(debtor & "|" & "In Court Custody:" & in_court_custody)
                    change_found = True
                End If
            End If

            'see if imprisoned has changed
            If imprisoned <> "" Then
                Dim note_imprisoned As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "inprison:")
                    If start_idx > 0 Then
                        start_idx += 9
                    End If
                    If start_idx > 0 Then
                        note_imprisoned = Mid(note_text, start_idx, 1)
                        If note_imprisoned = "y" Then
                            note_imprisoned = "yes"
                        Else
                            note_imprisoned = "no"
                        End If
                    End If
                    If note_imprisoned <> "" Then
                        Exit For
                    End If
                Next
                If LCase(imprisoned) <> note_imprisoned Then
                    write_note(debtor & "|" & "InPrison:" & imprisoned)
                    change_found = True
                End If
            End If

            'see if sentence date has changed
            If sentence_date <> Nothing Then
                Dim note_sentence_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "sentenceorderdate:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_sentence_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_sentence_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_sentence_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_sentence_date_str)
                    If sentence_date <> test_date Then
                        write_note(debtor & "|" & "SentenceOrderDate:" & Format(sentence_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "SentenceOrderDate:" & Format(sentence_date, "dd/MM/yyyy"))
                End Try
            End If

            'see if committal Date has changed
            If committal_date <> Nothing Then
                Dim note_committal_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "committal date:")
                    If start_idx > 0 Then
                        start_idx += 15
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_committal_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_committal_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_committal_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_committal_date_str)
                    If committal_date <> test_date Then
                        write_note(debtor & "|" & "Committal Date:" & Format(committal_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Committal Date:" & Format(committal_date, "dd/MM/yyyy"))
                End Try
            End If

            'see if hardship review Date has changed
            If hardship_review_date <> Nothing Then
                Dim note_hardship_review_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "hardship review date:")
                    If start_idx > 0 Then
                        start_idx += 21
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_hardship_review_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_hardship_review_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_hardship_review_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_hardship_review_date_str)
                    If hardship_review_date <> test_date Then
                        write_note(debtor & "|" & "Hardship Review Date:" & Format(hardship_review_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Hardship Review Date:" & Format(hardship_review_date, "dd/MM/yyyy"))
                End Try
            End If


            'see if Hardship Result has changed
            If hardship_result <> "" Then
                Dim note_hardship_result As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "hardship review result:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_hardship_result = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_hardship_result = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_hardship_result <> "" Then
                        Exit For
                    End If
                Next
                If LCase(hardship_result) <> note_hardship_result Then
                    write_note(debtor & "|" & "Hardship Review Result:" & hardship_result)
                    change_found = True
                End If
            End If

            'see if assessment reason has changed
            If assessment_reason <> "" Then
                Dim note_assessment_reason As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "assessment reason:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_assessment_reason = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_assessment_reason = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_assessment_reason <> "" Then
                        Exit For
                    End If
                Next
                If LCase(assessment_reason) <> note_assessment_reason Then
                    write_note(debtor & "|" & "Assessment Reason:" & assessment_reason)
                    change_found = True
                End If
            End If

            'see if assessment code has changed
            If assessment_code <> "" Then
                Dim note_assessment_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "assessment code:")
                    If start_idx > 0 Then
                        start_idx += 16
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_assessment_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_assessment_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_assessment_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(assessment_code) <> note_assessment_code Then
                    write_note(debtor & "|" & "Assessment Code:" & assessment_code)
                    change_found = True
                End If
            End If
            'see if assessment Date has changed
            If assessment_date <> Nothing Then
                Dim note_assessment_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "assessment date:")
                    If start_idx > 0 Then
                        start_idx += 16
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_assessment_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_assessment_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_assessment_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_assessment_date_str)
                    If assessment_date <> test_date Then
                        write_note(debtor & "|" & "Assessment Date:" & Format(assessment_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Assessment Date:" & Format(assessment_date, "dd/MM/yyyy"))
                End Try
            End If


            'see if bedroom count has changed
            If bedroom_count <> "" Then
                Dim note_bedroom_count As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "bedroom count:")
                    If start_idx > 0 Then
                        start_idx += 14
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_bedroom_count = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_bedroom_count = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_bedroom_count <> "" Then
                        Exit For
                    End If
                Next
                If LCase(bedroom_count) <> note_bedroom_count Then
                    write_note(debtor & "|" & "Bedroom Count:" & bedroom_count)
                    change_found = True
                End If
            End If

            'see if undeclared property has changed
            If undeclared_property <> "" Then
                Dim note_undeclared_property As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "undeclared property:")
                    If start_idx > 0 Then
                        start_idx += 20
                    End If
                    If start_idx > 0 Then
                        note_undeclared_property = Mid(note_text, start_idx, 1)
                        If note_undeclared_property = "y" Then
                            note_undeclared_property = "yes"
                        Else
                            note_undeclared_property = "no"
                        End If
                    End If
                    If note_undeclared_property <> "" Then
                        Exit For
                    End If
                Next
                If LCase(undeclared_property) <> note_undeclared_property Then
                    write_note(debtor & "|" & "Undeclared Property:" & undeclared_property)
                    change_found = True
                End If
            End If

            'see if pcent owned applicant has changed
            If percent_owned_appl <> -999 Then
                Dim note_percent_owned_appl As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "percent owned applicant:")
                    If start_idx > 0 Then
                        start_idx += 24
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_percent_owned_appl = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_percent_owned_appl = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_percent_owned_appl <> "" Then
                        Exit For
                    End If
                Next
                Dim test_number As Decimal
                Try
                    test_number = note_percent_owned_appl
                    If percent_owned_appl <> test_number Then
                        write_note(debtor & "|" & "Percent Owned Applicant:" & percent_owned_appl)
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Percent Owned Applicant:" & percent_owned_appl)
                    change_found = True
                End Try

            End If

            'see if pcent owned partner has changed
            If percent_owned_partner <> -999 Then
                Dim note_percent_owned_partner As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "percent owned partner:")
                    If start_idx > 0 Then
                        start_idx += 22
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_percent_owned_partner = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_percent_owned_partner = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_percent_owned_partner <> "" Then
                        Exit For
                    End If
                Next
                Dim test_number As Decimal
                Try
                    test_number = note_percent_owned_partner
                    If percent_owned_partner <> test_number Then
                        write_note(debtor & "|" & "Percent Owned Partner:" & percent_owned_partner)
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Percent Owned Partner:" & percent_owned_partner)
                    change_found = True
                End Try
            End If

            'see if declared mortgage has changed
            If declared_mortgage <> -99999 Then
                Dim note_declared_mortgage As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "declared mortgage:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_declared_mortgage = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_declared_mortgage = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_declared_mortgage <> "" Then
                        Exit For
                    End If
                Next
                Dim test_number As Decimal
                Try
                    test_number = note_declared_mortgage
                    If declared_mortgage <> test_number Then
                        write_note(debtor & "|" & "Declared Mortgage:" & declared_mortgage)
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Declared Mortgage:" & declared_mortgage)
                    change_found = True
                End Try
            End If

            'see if verified mortgage has changed
            If verified_mortgage <> -99999 Then
                Dim note_verified_mortgage As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "verified mortgage:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_verified_mortgage = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_verified_mortgage = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_verified_mortgage <> "" Then
                        Exit For
                    End If
                Next
                Dim test_number As Decimal
                Try
                    test_number = note_verified_mortgage
                    If verified_mortgage <> test_number Then
                        write_note(debtor & "|" & "Verified Mortgage:" & verified_mortgage)
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Verified Mortgage:" & verified_mortgage)
                    change_found = True
                End Try
            End If

            'see if verified market value has changed
            If verified_market_value <> -99999 Then
                Dim note_verified_market_value As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "verified market value:")
                    If start_idx > 0 Then
                        start_idx += 22
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_verified_market_value = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_verified_market_value = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_verified_market_value <> "" Then
                        Exit For
                    End If
                Next
                Dim test_number As Decimal
                Try
                    test_number = note_verified_market_value
                    If verified_market_value <> test_number Then
                        write_note(debtor & "|" & "Verified Market Value:" & verified_market_value)
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Verified Market Value:" & verified_market_value)
                    change_found = True
                End Try
            End If

            'see if no capital declared has changed
            If no_capital_declared <> "" Then
                Dim note_no_capital_declared As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "no capital declared:")
                    If start_idx > 0 Then
                        start_idx += 20
                    End If
                    If start_idx > 0 Then
                        note_no_capital_declared = Mid(note_text, start_idx, 1)
                        If note_no_capital_declared = "y" Then
                            note_no_capital_declared = "yes"
                        Else
                            note_no_capital_declared = "no"
                        End If
                    End If
                    If note_no_capital_declared <> "" Then
                        Exit For
                    End If
                Next
                If LCase(no_capital_declared) <> note_no_capital_declared Then
                    write_note(debtor & "|" & "No Capital Declared:" & no_capital_declared)
                    change_found = True
                End If
            End If

            'see if contrary interest has changed
            If contrary_interest <> "" Then
                Dim note_contrary_interest As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "contrary interest:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        note_contrary_interest = Mid(note_text, start_idx, 1)
                        If note_contrary_interest = "y" Then
                            note_contrary_interest = "yes"
                        Else
                            note_contrary_interest = "no"
                        End If
                    End If
                    If note_contrary_interest <> "" Then
                        Exit For
                    End If
                Next
                If LCase(contrary_interest) <> note_contrary_interest Then
                    write_note(debtor & "|" & "Contrary Interest:" & contrary_interest)
                    change_found = True
                End If
            End If

            'see if partner first name has changed
            If partner_first_name <> "" Then
                Dim note_partner_first_name As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "partner first name:")
                    If start_idx > 0 Then
                        start_idx += 19
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_partner_first_name = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_partner_first_name = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_partner_first_name <> "" Then
                        Exit For
                    End If
                Next
                If LCase(partner_first_name) <> note_partner_first_name Then
                    write_note(debtor & "|" & "Partner First Name:" & partner_first_name)
                    change_found = True
                End If
            End If

            'see if partner last name has changed
            If partner_last_name <> "" Then
                Dim note_partner_last_name As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "partner last name:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_partner_last_name = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_partner_last_name = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_partner_last_name <> "" Then
                        Exit For
                    End If
                Next
                If LCase(partner_last_name) <> note_partner_last_name Then
                    write_note(debtor & "|" & "Partner Last Name:" & partner_last_name)
                    change_found = True
                End If
            End If

            'see if partner dob has changed
            If partner_dob <> Nothing Then
                Dim note_partner_dob_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "partner dob:")
                    If start_idx > 0 Then
                        start_idx += 12
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_partner_dob_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_partner_dob_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_partner_dob_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_partner_dob_str)
                    If test_date <> partner_dob Then
                        write_note(debtor & "|" & "Partner DOB:" & Format(partner_dob, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Partner DOB:" & Format(partner_dob, "dd/MM/yyyy"))
                    change_found = True
                End Try

            End If

            'see if partner nino has changed
            If partner_nino <> "" Then
                Dim note_partner_nino As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "partner nino:")
                    If start_idx > 0 Then
                        start_idx += 13
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_partner_nino = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_partner_nino = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_partner_nino <> "" Then
                        Exit For
                    End If
                Next
                If LCase(partner_nino) <> note_partner_nino Then
                    write_note(debtor & "|" & "Partner NINO:" & partner_nino)
                    change_found = True
                End If
            End If

            'see if partner emp status has changed
            If partner_emp_status <> "" Then
                Dim note_partner_emp_status As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "partner emp status:")
                    If start_idx > 0 Then
                        start_idx += 19
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_partner_emp_status = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_partner_emp_status = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_partner_emp_status <> "" Then
                        Exit For
                    End If
                Next
                If LCase(partner_emp_status) <> note_partner_emp_status Then
                    write_note(debtor & "|" & "Partner Emp Status:" & partner_emp_status)
                    change_found = True
                End If
            End If

            'see if partner emp code has changed
            If partner_emp_code <> "" Then
                Dim note_partner_emp_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "partner emp code:")
                    If start_idx > 0 Then
                        start_idx += 17
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_partner_emp_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_partner_emp_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_partner_emp_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(partner_emp_code) <> note_partner_emp_code Then
                    write_note(debtor & "|" & "Partner Emp Code:" & partner_emp_code)
                    change_found = True
                End If
            End If
            'see if residential code has changed
            If residential_code <> "" Then
                Dim note_residential_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "residential code:")
                    If start_idx > 0 Then
                        start_idx += 17
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_residential_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_residential_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_residential_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(residential_code) <> note_residential_code Then
                    write_note(debtor & "|" & "Residential Code:" & residential_code)
                    change_found = True
                End If
            End If

            'see if residential description has changed
            If residential_desc <> "" Then
                Dim note_residential_desc As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "residential description:")
                    If start_idx > 0 Then
                        start_idx += 24
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_residential_desc = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_residential_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_residential_desc <> "" Then
                        Exit For
                    End If
                Next
                If LCase(residential_desc) <> note_residential_desc Then
                    write_note(debtor & "|" & "Residential Description:" & residential_desc)
                    change_found = True
                End If
            End If

            'see if property_type_code has changed
            If prop_type_code <> "" Then
                Dim note_prop_type_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "property type code:")
                    If start_idx > 0 Then
                        start_idx += 19
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_prop_type_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_prop_type_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_prop_type_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(prop_type_code) <> note_prop_type_code Then
                    write_note(debtor & "|" & "Property Type Code:" & prop_type_code)
                    change_found = True
                End If
            End If

            'see if property_type_description has changed
            If prop_type_desc <> "" Then
                Dim note_prop_type_desc As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "property type description:")
                    If start_idx > 0 Then
                        start_idx += 26
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_prop_type_desc = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_prop_type_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_prop_type_desc <> "" Then
                        Exit For
                    End If
                Next
                If LCase(prop_type_desc) <> note_prop_type_desc Then
                    write_note(debtor & "|" & "Property Type Description:" & prop_type_desc)
                    change_found = True
                End If
            End If

            'see if all evidence date has changed
            If all_evidence_date <> Nothing Then
                Dim note_all_evidence_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "all evidence date:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_all_evidence_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_all_evidence_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_all_evidence_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_all_evidence_date_str)
                    If all_evidence_date <> note_all_evidence_date_str Then
                        write_note(debtor & "|" & "All Evidence Date:" & Format(all_evidence_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "All Evidence Date:" & Format(all_evidence_date, "dd/MM/yyyy"))
                    change_found = True
                End Try

            End If

            'see if preferred payment day has changed
            If pref_pay_day <> "" Then
                Dim note_pref_pay_day As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "preferred payment day:")
                    If start_idx > 0 Then
                        start_idx += 22
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_pref_pay_day = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_pref_pay_day = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_pref_pay_day <> "" Then
                        Exit For
                    End If
                Next
                If LCase(pref_pay_day) <> note_pref_pay_day Then
                    write_note(debtor & "|" & "Preferred Payment Day:" & pref_pay_day)
                    change_found = True
                End If
            End If

            'see if preferred payment method has changed
            If pref_pay_method <> "" Then
                Dim note_pref_pay_method As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "preferred payment method:")
                    If start_idx > 0 Then
                        start_idx += 25
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_pref_pay_method = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_pref_pay_method = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_pref_pay_method <> "" Then
                        Exit For
                    End If
                Next
                If LCase(pref_pay_method) <> note_pref_pay_method Then
                    write_note(debtor & "|" & "Preferred Payment Method:" & pref_pay_method)
                    change_found = True
                End If
            End If

            'see if preferred payment method code has changed
            If pref_pay_code <> "" Then
                Dim note_pref_pay_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "preferred payment code:")
                    If start_idx > 0 Then
                        start_idx += 22
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_pref_pay_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_pref_pay_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_pref_pay_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(pref_pay_code) <> note_pref_pay_code Then
                    write_note(debtor & "|" & "Preferred Payment Code:" & pref_pay_method)
                    change_found = True
                End If
            End If

            'see if income evidence has changed
            If inc_ev_no > 0 Then
                Dim note_evidence(30) As String
                Dim note_inc_ev_no As Integer = 0

                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "inc evidence:")
                    If start_idx > 0 Then
                        start_idx += 13
                    End If
                    If start_idx > 0 Then
                        note_inc_ev_no += 1
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_evidence(note_inc_ev_no) = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        note_text = Microsoft.VisualBasic.Right(note_text, note_text.Length - start_idx)
                        start_idx = InStr(note_text, "inc evidence:")
                        While start_idx <> 0
                            start_idx += 13
                            note_inc_ev_no += 1
                            For idx4 = start_idx To note_text.Length
                                If Mid(note_text, idx4, 1) = ";" Then
                                    note_evidence(note_inc_ev_no) = Mid(note_text, start_idx, idx4 - start_idx)
                                    Exit For
                                End If
                            Next
                            note_text = Microsoft.VisualBasic.Right(note_text, note_text.Length - start_idx)
                            start_idx = InStr(note_text, "inc evidence:")
                        End While
                    End If
                Next
                For idx3 = 1 To inc_ev_no
                    Dim inc_ev_found As Boolean = False
                    For idx4 = 1 To note_inc_ev_no
                        If LCase(inc_evidence_table(idx3, 1)) = LCase(note_evidence(idx4)) Then
                            inc_ev_found = True
                            Exit For
                        End If
                    Next
                    If inc_ev_found = False Then
                        Dim note_txt As String = debtor & "|" & "Inc Evidence:" & inc_evidence_table(idx3, 1)
                        If inc_evidence_table(idx3, 2) <> "" Then
                            note_txt = note_txt & ";" & "Inc Evidence Mandatory:" & inc_evidence_table(idx3, 2)
                        End If
                        If inc_evidence_table(idx3, 3) <> "" Then
                            note_txt = note_txt & ";" & "Inc Evidence Other Text:" & inc_evidence_table(idx3, 3)
                        End If
                        If inc_evidence_table(idx3, 4) <> "" Then
                            test_date = inc_evidence_table(idx3, 4)
                            note_txt = note_txt & ";" & "Inc Evidence Date Received:" & Format(test_date, "dd/MM/yyyy")
                        End If
                        write_note(note_txt)
                        change_found = True
                    End If
                Next
            End If


            'see if assets have changed
            If asset_no > 0 Then
                Dim note_asset(100) As String
                Dim note_asset_no As Integer = 0
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "cap amt:")
                    While start_idx > 0
                        start_idx += 8
                        note_asset_no += 1
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_asset(note_asset_no) = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        note_text = Microsoft.VisualBasic.Right(note_text, note_text.Length - start_idx)
                        start_idx = InStr(note_text, "cap amt:")
                    End While
                Next
                For idx3 = 1 To asset_no
                    Dim asset_found As Boolean = False
                    For idx4 = 1 To note_asset_no
                        If asset_table(idx3, 2) = note_asset(idx4) Then
                            asset_found = True
                            Exit For
                        End If
                    Next
                    If asset_found = False Then
                        Dim note_txt As String = debtor & "|" & "Cap asset type:" & asset_table(idx3, 1)
                        If asset_table(idx3, 2) <> "" Then
                            note_txt = note_txt & ";" & "Cap amt:" & asset_table(idx3, 2)
                        End If
                        If asset_table(idx3, 3) <> "" Then
                            note_txt = note_txt & ";" & "Cap amt verified:" & asset_table(idx3, 3)
                        End If
                        If asset_table(idx3, 4) <> "" Then
                            test_date = asset_table(idx3, 4)
                            note_txt = note_txt & ";" & "Cap amt Date Verified:" & Format(test_date, "dd/MM/yyyy")
                        End If
                        If asset_table(idx3, 5) <> "" Then
                            test_date = asset_table(idx3, 5)
                            note_txt = note_txt & ";" & "Cap amt Evidence Received Date:" & Format(test_date, "dd/MM/yyyy")
                        End If
                        write_note(note_txt)
                        change_found = True
                    End If
                Next
            End If

            'see if uplift applied date has changed
            If uplift_applied_date <> Nothing Then
                Dim note_uplift_applied_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "uplift applied date:")
                    If start_idx > 0 Then
                        start_idx += 20
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_uplift_applied_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_uplift_applied_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_uplift_applied_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_uplift_applied_date_str)
                    If test_date <> uplift_applied_date Then
                        write_note(debtor & "|" & "uplift applied date:" & Format(uplift_applied_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "uplift applied date:" & Format(uplift_applied_date, "dd/MM/yyyy"))
                    change_found = True
                End Try
            End If

            'see if uplift removed date has changed
            If uplift_removed_date <> Nothing Then
                Dim note_uplift_removed_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "uplift removed date:")
                    If start_idx > 0 Then
                        start_idx += 20
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_uplift_removed_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_uplift_removed_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_uplift_removed_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_uplift_removed_date_str)
                    If test_date <> uplift_removed_date Then
                        write_note(debtor & "|" & "uplift removed date:" & Format(uplift_removed_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "uplift removed date:" & Format(uplift_removed_date, "dd/MM/yyyy"))
                    change_found = True
                End Try

            End If


            'ANY CHANGES FOUND?
            If Not change_found Then
                audit_file = audit_file & maat_id & "|no changes found" & vbNewLine
                no_changes_file = no_changes_file & maat_id & vbNewLine
                no_changes += 1
            Else
                audit_file = audit_file & maat_id & "|changes found" & vbNewLine
                changes += 1
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub write_error(ByVal error_message As String)
        Static Dim error_no As Integer
        error_no += 1
        Dim ln As Integer = Microsoft.VisualBasic.Len(OpenFileDialog1.FileName)
        new_file = Microsoft.VisualBasic.Left(OpenFileDialog1.FileName, ln - 4) & "_error.txt"
        error_message = error_message & vbNewLine
        If error_no = 1 Then
            My.Computer.FileSystem.WriteAllText(new_file, error_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(new_file, error_message, True)
        End If

    End Sub
    Private Sub write_eff_date(ByVal change_message As String)
        Static Dim date_cnt As Integer
        date_cnt += 1
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_eff_date.txt"
        If date_cnt = 1 Then
            My.Computer.FileSystem.WriteAllText(new_file, change_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
        End If
    End Sub
    Private Sub write_change(ByVal change_message As String)
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_changes.txt"
        My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
    End Sub
    Private Sub write_outcome(ByVal change_message As String)
        'Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        'new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_outcome.txt"
        'My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
    End Sub
    Private Sub write_note(ByVal change_message As String)
        Static Dim note_count As Integer
        change_message = change_message & ";from feed on " & Format(Now, "dd.MM.yyyy") & vbNewLine
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_note.txt"
        note_count += 1
        If note_count = 1 Then
            My.Computer.FileSystem.WriteAllText(new_file, change_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
        End If
    End Sub
    Private Sub remove_chrs(ByRef in_field As String)
        Dim idx2 As Integer
        Dim out_field As String = ""
        For idx2 = 1 To Microsoft.VisualBasic.Len(in_field)
            If Mid(in_field, idx2, 1) <> Chr(9) And _
              Mid(in_field, idx2, 1) <> Chr(10) And _
            Mid(in_field, idx2, 1) <> Chr(13) Then
                out_field = out_field & Mid(in_field, idx2, 1)
            End If
        Next
        in_field = out_field
    End Sub

    Private Sub reset_fields()
        applicant_id = ""
        summons_no = ""
        maat_id = ""
        first_name = ""
        surname = ""
        dob = ""
        ni_no = ""
        mthly_contrib_amt_str = ""
        mthly_contrib_amt = Nothing
        upfront_contrib_amt_str = ""
        upfront_contrib_amt = Nothing
        'final_defence_cost = Nothing
        income_contrib_cap_str = ""
        income_uplift_applied = ""
        case_type = ""
        in_court_custody = ""
        imprisoned = ""
        sentence_date = Nothing
        debt_addr1 = ""
        debt_addr2 = ""
        debt_addr3 = ""
        debt_addr4 = ""
        debt_postcode = ""
        postal_addr1 = ""
        postal_addr2 = ""
        postal_addr3 = ""
        postal_addr4 = ""
        postal_postcode = ""
        landline = ""
        mobile = ""
        email = ""
        equity_amt = -9999
        partner_equity_amt = -9999
        appl_equity_amt = -9999
        equity_amt_verified = ""
        cap_asset_type = ""
        cap_amt = 0
        cap_amt_verified = ""
        allowable_cap_threshold = ""
        effective_date_str = ""
        effective_date = Nothing
        future_effective_date = False
        pref_pay_method = ""
        pref_pay_code = ""
        pref_pay_day = ""
        bank_acc_name = ""
        bank_acc_no = ""
        bank_sort_code = ""
        emp_status = ""
        emp_code = ""
        rep_order_withdrawal_date_str = ""
        rep_order_withdrawal_date = Nothing
        hardship_appl_recvd = ""
        comments = ""
        outcome = ""
        outcome2 = ""
        rep_status = ""
        appeal_type = ""
        residential_code = ""
        residential_desc = ""
        prop_type_code = ""
        prop_type_desc = ""
        percent_owned_appl = -999
        percent_owned_partner = -999
        has_partner = ""
        partner_first_name = ""
        partner_last_name = ""
        partner_dob = Nothing
        partner_nino = ""
        partner_emp_status = ""
        partner_emp_code = ""
        contrary_interest = ""
        disability_declaration = ""
        disability_description = ""
        declared_value = Nothing
        rep_status_date = Nothing
        committal_date = Nothing
        hardship_review_date = Nothing
        hardship_result = ""
        si = ""
        nfa = ""
        assessment_reason = ""
        assessment_code = ""
        assessment_date = Nothing
        bedroom_count = ""
        undeclared_property = ""
        declared_mortgage = -99999
        verified_mortgage = -99999
        verified_market_value = -99999
        no_capital_declared = ""
        all_evidence_date = Nothing
        uplift_applied_date = Nothing
        uplift_removed_date = Nothing
        ci_code = ""
        ci_desc = ""
        case_type_code = ""
        rep_status_desc = ""
    End Sub
    Private Sub get_comment_break(ByVal new_comment As String)
        Dim idx, idx2 As Integer
        'break comments into 250 chunks broken at ;
        Dim no_of_250s As Integer = Int(new_comment.Length / 250) + 1
        For idx = 1 To no_of_250s
            For idx2 = 250 To 1 Step -1
                If Mid(new_comment, idx2, 1) = ";" Then
                    Exit For
                End If
            Next
            comments = comments & Microsoft.VisualBasic.Left(new_comment, idx2)
            new_comment = Microsoft.VisualBasic.Right(new_comment, new_comment.Length - idx2)
            If new_comment.Length <= 250 Then
                comments = comments & new_comment & Space(250 - new_comment.Length)
                Exit For
            End If
        Next
    End Sub

    Private Sub validate_xml_file()
        'open file as text first to remove any � signs
        Dim txt_doc As String
        txt_doc = My.Computer.FileSystem.ReadAllText(OpenFileDialog1.FileName)
        txt_doc = Replace(txt_doc, "�", "")
        My.Computer.FileSystem.WriteAllText(OpenFileDialog1.FileName, txt_doc, False)
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(OpenFileDialog1.FileName)
        myDocument.Schemas.Add("", "R:\vb.net\LSC XSD\contribution_file.xsd")
        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
    End Sub

    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_error("Error: " & e.Message)
            Case XmlSeverityType.Warning
                write_error("Warning " & e.Message)
        End Select
    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        log_user = My.User.Name
        Dim slash_idx As Integer = InStr(log_user, "\")
        If slash_idx > 0 Then
            log_user = Microsoft.VisualBasic.Right(log_user, log_user.Length - slash_idx)
        End If
    End Sub

    Private Sub LSC_preprocess_last_Header_IDBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

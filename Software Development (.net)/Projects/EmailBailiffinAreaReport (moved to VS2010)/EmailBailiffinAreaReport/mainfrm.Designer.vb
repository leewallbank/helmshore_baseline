<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.allbtn = New System.Windows.Forms.Button
        Me.somebtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.Label1 = New System.Windows.Forms.Label
        Me.maintainbtn = New System.Windows.Forms.Button
        Me.FeesSQLDataSet = New EmailBailiffinAreaReport.FeesSQLDataSet
        Me.Bailiff_in_areaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Bailiff_in_areaTableAdapter = New EmailBailiffinAreaReport.FeesSQLDataSetTableAdapters.Bailiff_in_areaTableAdapter
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Bailiff_in_areaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'allbtn
        '
        Me.allbtn.Location = New System.Drawing.Point(100, 128)
        Me.allbtn.Name = "allbtn"
        Me.allbtn.Size = New System.Drawing.Size(115, 23)
        Me.allbtn.TabIndex = 1
        Me.allbtn.Text = "Send all emails"
        Me.allbtn.UseVisualStyleBackColor = True
        '
        'somebtn
        '
        Me.somebtn.Location = New System.Drawing.Point(66, 189)
        Me.somebtn.Name = "somebtn"
        Me.somebtn.Size = New System.Drawing.Size(170, 23)
        Me.somebtn.TabIndex = 2
        Me.somebtn.Text = "Send emails not sent this week"
        Me.somebtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(205, 290)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 3
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 290)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(23, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Label1"
        '
        'maintainbtn
        '
        Me.maintainbtn.Location = New System.Drawing.Point(100, 72)
        Me.maintainbtn.Name = "maintainbtn"
        Me.maintainbtn.Size = New System.Drawing.Size(115, 23)
        Me.maintainbtn.TabIndex = 0
        Me.maintainbtn.Text = "Maintain email list"
        Me.maintainbtn.UseVisualStyleBackColor = True
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Bailiff_in_areaBindingSource
        '
        Me.Bailiff_in_areaBindingSource.DataMember = "Bailiff_in_area"
        Me.Bailiff_in_areaBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'Bailiff_in_areaTableAdapter
        '
        Me.Bailiff_in_areaTableAdapter.ClearBeforeFill = True
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(325, 341)
        Me.Controls.Add(Me.maintainbtn)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.somebtn)
        Me.Controls.Add(Me.allbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Email Bailiff in Area Report (RA889)"
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Bailiff_in_areaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents allbtn As System.Windows.Forms.Button
    Friend WithEvents somebtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents maintainbtn As System.Windows.Forms.Button
    Friend WithEvents FeesSQLDataSet As EmailBailiffinAreaReport.FeesSQLDataSet
    Friend WithEvents Bailiff_in_areaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Bailiff_in_areaTableAdapter As EmailBailiffinAreaReport.FeesSQLDataSetTableAdapters.Bailiff_in_areaTableAdapter

End Class

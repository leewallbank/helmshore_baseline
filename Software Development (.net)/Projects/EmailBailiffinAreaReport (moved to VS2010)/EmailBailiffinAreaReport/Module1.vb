Module Module1
    Public error_path, error_message, error_file, file_path, file_name, cl_name, orig_cc As String
    Public log_path, log_file, log_message, cl_name_parm, sch_name_parm As String
    Public send_all, double_click As Boolean
    Public csid_parm, cc_no_parm, branch As Integer
    Public cc_all As String
    Public wc_date As Date
    Public conn1_open As Boolean
    Public first_letters As String

    Sub write_error()
        Dim dir_info As IO.DirectoryInfo
        Try
            If IO.Directory.Exists(error_path) = False Then
                dir_info = IO.Directory.CreateDirectory(error_path)
            End If
            'save document
        Catch ex As Exception
            MsgBox("Unable to save error file")
        End Try
        My.Computer.FileSystem.WriteAllText(error_file, error_message, True)

    End Sub
    Sub write_log()
        Dim dir_info As IO.DirectoryInfo
        Try
            If IO.Directory.Exists(log_path) = False Then
                dir_info = IO.Directory.CreateDirectory(log_path)
            End If
            'save document
        Catch ex As Exception
            MsgBox("Unable to save log file")
        End Try
        My.Computer.FileSystem.WriteAllText(log_file, log_message, True)

    End Sub
    Sub remove_chars(ByRef str As String)
        Dim idx As Integer
        Dim strout As String = ""
        Dim lstr As Integer = Microsoft.VisualBasic.Len(str)
        For idx = 1 To lstr
            If Mid(str, idx, 1) <> " " And Mid(str, idx, 1) <> "&" And Mid(str, idx, 1) <> "/" _
            And Mid(str, idx, 1) <> "." And Mid(str, idx, 1) <> "'" Then
                strout = strout & Mid(str, idx, 1)
            End If
        Next
        str = strout
    End Sub
End Module

Imports System.Collections
Public Class mainfrm
    Dim log_user As String

    Dim sch_name As String = ""
    Dim subject As String = ""
    Dim fromaddress As String
    Dim next_toaddress As String
    Dim body As String
    Dim error_found As Boolean = False
    Dim csid_idx As Integer = 0
    Private RA88963report As RA889
    Private Const PARAMETER_FIELD_NAME1 As String = "client_parm"
    Dim myArrayList1 As ArrayList = New ArrayList()
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub sendallbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allbtn.Click
        disable_buttons()
        send_all = True
        If MsgBox("Sending bailiff in area reports to ALL clients", MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton2, "Send Emails") = MsgBoxResult.Ok Then
            Dim retn_no As Integer = prepare_emails()
            If retn_no = 0 Then
                MsgBox("ALL emails sent OK")
            Else
                If retn_no = 99 Then
                    MsgBox("No emails sent")
                Else
                    MsgBox("Emails sent but see error log at h:\email in area")
                End If
            End If
        Else
            MsgBox("No emails sent")
        End If
        log_message = "Finished" & vbNewLine
        write_log()
        Me.Close()
    End Sub
    Private Function prepare_emails()
        Dim toaddress As String = ""
        fromaddress = "crystal@rossendales.com"
        Dim oWSH = CreateObject("WScript.Shell")
        log_user = oWSH.ExpandEnvironmentStrings("%USERNAME%")
        Dim ccaddress As String = log_user & "@rossendales.com"
        body = "Good Morning"
        If Format(Now, "HH") > 12 Then
            body = "Good afternoon"
        End If

        body = body & vbNewLine & "Please find attached bailiff in area report."
        body = body & vbNewLine & vbNewLine & "Regards" & vbNewLine
        body = body & "Client Liaison Team" & vbNewLine & _
                    "        Rossendales" & vbNewLine & _
                    "        PO Box 324" & vbNewLine & _
                    "        Rossendale" & vbNewLine & _
                    "        BB4 0GE   " & vbNewLine & _
                    "Tel: 01706 830323"
        cc_all = InputBox("Send a CC for emails to ", "EMAIL to send CC to. Blank out for no CC", ccaddress)
        error_file = error_path & "error_log_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"
        Dim this_week As String = Format(Now, "dd.MM.yyyy")
        'Dim row As DataRow
        Dim idx As Integer = 0
        Dim final_warning_given As Boolean = False
        Dim cl_rows As Integer
        Dim email_idx As Integer = -1
        Dim cl_id As Integer
        Dim row As DataRow
        Me.Bailiff_in_areaTableAdapter.Fill(Me.FeesSQLDataSet.Bailiff_in_area)
        cl_rows = FeesSQLDataSet.Bailiff_in_area.Rows.Count
        For Each row In FeesSQLDataSet.Bailiff_in_area.Rows
            ProgressBar1.Value = (idx / cl_rows) * 100
            cl_id = FeesSQLDataSet.Bailiff_in_area.Rows(idx).Item(0)
            Dim last_date_str As String
            Try
                last_date_str = FeesSQLDataSet.Bailiff_in_area.Rows(idx).Item(2)
            Catch
                last_date_str = " "
            End Try
            If Not send_all Then
                If Microsoft.VisualBasic.Len(Trim(last_date_str)) > 0 Then
                    Dim last_date As Date = CDate(last_date_str)
                    If last_date >= DateAdd(DateInterval.Day, -1, wc_date) Then
                        idx += 1
                        Continue For
                    End If
                End If
            End If
            toaddress = FeesSQLDataSet.Bailiff_in_area.Rows(idx).Item(1)

            If Not final_warning_given Then
                final_warning_given = True
                If send_all Then
                    If MsgBox("Send ALL emails to clients", MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton2, "-----Last chance to stop emails-----") = MsgBoxResult.Cancel Then
                        Return 99
                    End If
                Else
                    If MsgBox("Send unsent emails to clients", MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton2, "-----Last chance to stop emails-----") = MsgBoxResult.Cancel Then
                        Return 99
                    End If
                End If
            End If

            
            param1 = "onestep"
            param2 = "select name from Client where _rowid = " & cl_id
            Dim cl_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows <> 1 Then
                MsgBox("Client name not found for client ID = " & cl_id)
                Continue For
            End If
            cl_name = Trim(cl_dataset.Tables(0).Rows(0).Item(0))
            log_message = "Looking at " & cl_id & " for " & toaddress & vbNewLine
            write_log()
            'check there is at least one bailiff with 50 or more cases 
            'param2 = "select _rowid from ClientScheme where clientID = " & cl_id & _
            '" and branchID = 1"
            'Dim cs_dataset As DataSet = get_dataset(param1, param2)
            'If no_of_rows = 0 Then
            '    MsgBox("There are no schemes for " & cl_name)
            '    idx += 1
            '    Continue For
            'End If
            'Dim cs_rows As Integer = no_of_rows - 1
            'Dim idx2 As Integer
            'Dim cases_found As Boolean = False
            'If cs_rows = 0 Then
            '    Continue For
            'End If
            'Dim csid_array(10) As Integer
            'For idx2 = 0 To 10
            '    csid_array(idx2) = 0
            'Next
            'Dim csid As Integer
            'Dim bailiff_table(9999)
            'Dim idx3 As Integer
            'For idx2 = 0 To cs_rows
            '    '27.04.2011 need to accumulate for client not client scheme
            '    csid = cs_dataset.Tables(0).Rows(idx2).Item(0)
            '    param2 = "select bailiffID, bail_current from Debtor " & _
            '    " where clientschemeID = " & csid & _
            '    " and status_open_closed = 'O'"
            '    Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            '    Dim no_of_cases As Integer = no_of_rows - 1
            '    For idx3 = 0 To no_of_cases
            '        If debtor_dataset.Tables(0).Rows(idx3).Item(1) = "Y" Then
            '            Dim bailiff_id As Integer = debtor_dataset.Tables(0).Rows(idx3).Item(0)
            '            If bailiff_id <> 72 Then
            '                bailiff_table(bailiff_id) += 1
            '                If bailiff_table(bailiff_id) > 49 Then
            '                    param2 = "select internalExternal from Bailiff " & _
            '                             " where _rowid = " & bailiff_id & _
            '                             " and agent_type = 'B' and hasPen = 'Y'"
            '                    Dim bail_dataset As DataSet = get_dataset(param1, param2)
            '                    If no_of_rows = 1 Then
            '                        cases_found = True
            '                        Exit For
            '                    End If
            '                End If
            '            End If
            '        End If
            '    Next
            'Next
            'If cases_found Then
            send_email(toaddress, cl_id)
            'End If
            idx += 1
        Next
        If error_found Then
            Return 10
        Else
            If final_warning_given Then
                Return 0
            Else
                Return 99
            End If
        End If
    End Function
    Sub send_email(ByVal toaddress As String, ByVal cl_id As Integer)
        'run crystal report for client
        Dim RA889Report = New RA889
        myArrayList1.Add(cl_id)
        SetCurrentValuesForParameterField1(RA889Report, myArrayList1)
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA889Report)
        file_name = "H:\RA889 Enforcement Agents in area report.txt"
        
        'first run as txt file
        RA889Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Text, file_name)
        'read report to see if blank
        'read file into array
        Dim txt_file As String = My.Computer.FileSystem.ReadAllText(file_name)
        Dim idx, lines As Integer
        Dim line(20) As String
        Dim linetext As String = ""
        For idx = 1 To txt_file.Length
            If Mid(txt_file, idx, 2) = vbNewLine Then
                line(lines) = linetext
                linetext = ""
                lines += 1
                If lines = 20 Then
                    Exit For
                End If
            Else
                linetext = linetext + Mid(txt_file, idx, 1)
            End If
        Next
        'ignore if report is blank
        Try
            If line(10).Length < 10 Then
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try
        log_message = "running RA889 for " & cl_id & " for " & toaddress & vbNewLine
        write_log()
        file_name = "H:\RA889 Enforcement Agents in area report.pdf"
        RA889Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, file_name)
        RA889Report.close()
        log_message = "RA889 completed for " & cl_id & " for " & toaddress & vbNewLine
        write_log()
        subject = "Enforcement Agents in Area report for " & cl_name
        If email(toaddress, fromaddress, subject, body, file_name) = 0 Then
            Try
                Bailiff_in_areaTableAdapter.UpdateQuery(Now, toaddress, cl_id)
            Catch ex As Exception
                error_message = "Last date not updated for client scheme " & _
                cl_name & " " & sch_name & vbNewLine
                write_error()
                MsgBox("Last date not set for client scheme " & cl_name & " " & sch_name)
                error_found = True
            End Try
        Else
            error_found = True
        End If
    End Sub
    Private Sub SetCurrentValuesForParameterField1(ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myArrayList As ArrayList)
        Dim currentParameterValues As CrystalDecisions.Shared.ParameterValues = New CrystalDecisions.Shared.ParameterValues()
        For Each submittedValue As Object In myArrayList
            Dim myParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue()
            myParameterDiscreteValue.Value = submittedValue.ToString()
            currentParameterValues.Add(myParameterDiscreteValue)
        Next
        Dim myParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        Dim myParameterFieldDefinition As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME1)
        myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)
    End Sub
    Public Sub SetDBLogonForReport(ByVal myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo, ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        wc_date = DateAdd(DateInterval.Day, -Weekday(Now, FirstDayOfWeek.Sunday) + 2, Now)
        Label1.Text = "Report for W/C " & Format(wc_date, "dd/MM/yyyy")
        error_path = "h:\email in area\"
        error_file = error_path & "error_log_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"
        log_path = "h:\email in area\"
        log_file = error_path & "log_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"
    End Sub

    Private Sub somebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles somebtn.Click
        disable_buttons()
        send_all = False
        Dim retn_no As Integer = prepare_emails()
        If retn_no = 0 Then
            MsgBox("emails sent OK")
        Else
            If retn_no = 99 Then
                MsgBox("No emails sent")
            Else
                MsgBox("Emails sent but see error log at h:\email in area")
            End If
        End If
        log_message = "Finished" & vbNewLine
        write_log()
        Me.Close()
    End Sub

    Private Sub maintainbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles maintainbtn.Click
        disable_buttons()
        clientsfrm.ShowDialog()
        enable_buttons()
    End Sub
    Private Sub disable_buttons()
        allbtn.Enabled = False
        somebtn.Enabled = False
        exitbtn.Enabled = False
        maintainbtn.Enabled = False
    End Sub
    Private Sub enable_buttons()
        allbtn.Enabled = True
        somebtn.Enabled = True
        exitbtn.Enabled = True
        maintainbtn.Enabled = True
    End Sub
End Class

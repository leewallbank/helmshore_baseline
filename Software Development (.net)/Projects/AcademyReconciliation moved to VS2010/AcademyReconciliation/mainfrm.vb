Public Class mainfrm
    Dim reconfile1, reconfile2, reconfile3, reconfile4, cl_ref, our_ref, file_summons_no, cl_ref2, file_orig_debt As String
    Dim file_cl_os As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub reconcile()
        exitbtn.Enabled = False
        selectbtn.Enabled = False
        debtor_rows = 0
        Dim filename As String = ""
        Dim filetext As String = ""
        Dim linetext As String = ""
        reconfile1 = "Not on onestep" & vbNewLine
        reconfile2 = "On Onestep not on your list" & vbNewLine & _
        "Client Ref" & vbTab & "OurRef" & vbTab & "Summons No" & vbTab & "LO Date" & vbTab & "open/closed" & vbTab & "status" & vbTab & _
        "Debt amount" & vbTab & "Client balance" & vbNewLine
        reconfile3 = "Matched with differences" & vbNewLine & _
        "Client Ref" & vbTab & "Summons No" & vbTab & "OurRef" & vbTab & "LO Date" & vbTab & "open/closed" & vbTab & "status" & vbTab & _
        "Field" & vbTab & "academy value" & vbTab & "onestep value" & vbTab & "Last Payment date" & vbNewLine
        reconfile4 = "Matched without differences" & vbNewLine & _
                "Client Ref" & vbTab & "Summons No" & vbTab & "OurRef" & vbTab & "LO Date" & vbNewLine

        Dim fileok As Boolean = True
        Dim line(0) As String
        Try
            With OpenFileDialog1
                .Title = "Read Academy file"
                .Filter = "txt file|*.txt"
                .FileName = ""
                .CheckFileExists = True
            End With
            If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                Try
                    filename = OpenFileDialog1.FileName
                    filetext = My.Computer.FileSystem.ReadAllText(filename)
                Catch ex As Exception
                    MsgBox("Unable to read file")
                    fileok = False
                End Try
            Else
                fileok = False
            End If

            If fileok = False Then
                MsgBox("Unable to read file")
                exitbtn.Enabled = True
                selectbtn.Enabled = True
                Exit Sub
            End If

            ProgressBar1.Value = 5

            Dim lines As Integer

            'read file into array
            Dim idx As Integer
            For idx = 1 To Len(filetext) - 1
                If Mid(filetext, idx, 2) = vbNewLine Then
                    ReDim Preserve line(lines)
                    line(lines) = linetext
                    linetext = ""
                    lines += 1
                Else
                    linetext = linetext + Mid(filetext, idx, 1)
                End If
            Next

            'process each line
            For idx = 1 To lines - 1
                Try
                    ProgressBar1.Value = (idx / (lines - 1)) * 100
                Catch ex As Exception

                End Try

                Application.DoEvents()
                'ignore header and sub-header
                If Mid(line(idx), 2, 1) <> "D" Then
                    Continue For
                End If
                'get client ref
                cl_ref = Trim(Mid(line(idx), 4, 20))
                If cl_ref.Length = 0 Then
                    Continue For
                End If
                our_ref = Trim(Mid(line(idx), 25, 8))
                'If cl_ref = "941040064" Then
                '    our_ref = ""
                'End If
                file_summons_no = Trim(Mid(line(idx), 91, 20))
                'If file_summons_no = "32606" Then
                '    our_ref = ""
                'End If
                file_lo_date = Mid(line(idx), 34, 10)
                Dim test_date As Date
                Try
                    test_date = file_lo_date
                Catch ex As Exception
                    file_lo_date = Nothing
                End Try
                file_orig_debt = Mid(line(idx), 45, 12)
                file_cl_os = Mid(line(idx), 58, 12)
                param1 = "onestep"
                'ignore our_ref as other numbers are showing there
                our_ref = ""
                If our_ref.Length > 0 Then
                    'check if case is on onestep using our_ref
                    param2 = "select _rowid, clientschemeID from Debtor where _rowid = " & our_ref & _
                    " order by status_open_closed desc"
                    Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                    If no_of_rows = 0 Then
                        'not on onestep
                        reconfile1 = reconfile1 & line(idx) & vbNewLine
                        Continue For
                    Else
                        'on onestep
                        our_ref = debtor_dataset.Tables(0).Rows(0).Item(0)
                        'check it's one of the csids in cs_table
                        Dim idx2 As Integer
                        Dim debtor_ok As Boolean = False
                        For idx2 = 1 To sch_rows
                            If debtor_dataset.Tables(0).Rows(0).Item(1) = cs_table(idx2) Then
                                debtor_ok = True
                                Exit For
                            End If
                        Next
                        If debtor_ok = False Then
                            'not on onestep
                            reconfile1 = reconfile1 & line(idx) & vbNewLine
                        Else
                            'look for differences
                            check_debtor(our_ref)
                        End If
                    End If
                Else
                    'check if case is on onestep using cl_ref
                    cl_ref2 = Trim(cl_ref) & " " & Trim(file_summons_no)
                    param2 = "select _rowid, clientschemeID, debt_original from Debtor where client_ref = '" & cl_ref & "'" & _
                    " order by status_open_closed desc"
                    Dim debtor_dataset As DataSet = get_dataset(param1, param2)

                    If no_of_rows = 0 Then
                        'check using cl ref + summons number
                        param2 = "select _rowid, clientschemeID, debt_original from Debtor where client_ref = '" & cl_ref2 & "'"
                        Dim debtor2_dataset As DataSet = get_dataset(param1, param2)
                        If no_of_rows = 0 Then
                            'not on onestep
                            reconfile1 = reconfile1 & Trim(line(idx)) & vbNewLine
                        Else
                            Dim idx2, idx3, debtors_found As Integer
                            'check it's one of the csids in cs_table
                            Dim case_table(50, 2) As Decimal
                            debtors_found = 0
                            For idx2 = 0 To no_of_rows - 1
                                For idx3 = 1 To sch_rows
                                    Try
                                        If debtor2_dataset.Tables(0).Rows(idx2).Item(1) = cs_table(idx3) Then
                                            debtors_found += 1
                                            Try
                                                case_table(debtors_found, 1) = debtor2_dataset.Tables(0).Rows(idx2).Item(0)
                                            Catch ex As Exception
                                                max_debtors += 500
                                                ReDim Preserve debtor_table(max_debtors)
                                            End Try

                                            case_table(debtors_found, 2) = debtor2_dataset.Tables(0).Rows(idx2).Item(2)
                                            Exit For
                                        End If
                                    Catch ex As Exception
                                        MsgBox(ex.Message)
                                    End Try
                                Next
                            Next
                        End If
                    Else
                        Dim idx2, idx3, debtors_found As Integer
                        'check it's one of the csids in cs_table
                        Dim case_table(50, 2) As Decimal
                        debtors_found = 0
                        For idx2 = 0 To no_of_rows - 1
                            For idx3 = 1 To sch_rows
                                Try
                                    If debtor_dataset.Tables(0).Rows(idx2).Item(1) = cs_table(idx3) Then
                                        debtors_found += 1
                                        Try
                                            case_table(debtors_found, 1) = debtor_dataset.Tables(0).Rows(idx2).Item(0)
                                        Catch ex As Exception
                                            max_debtors += 500
                                            ReDim Preserve debtor_table(max_debtors)
                                        End Try
                                        case_table(debtors_found, 2) = debtor_dataset.Tables(0).Rows(idx2).Item(2)
                                        Exit For
                                    End If
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            Next
                        Next
                        If debtors_found = 0 Then
                            'not on onestep
                            reconfile1 = reconfile1 & line(idx) & vbNewLine
                            Continue For
                        Else
                            If debtors_found > 1 Then
                                Dim correct_debtor_found As Boolean = False
                                For idx2 = 1 To debtors_found
                                    'see if summons numbers match
                                    our_ref = case_table(idx2, 1)
                                    param2 = "select offence_number from Debtor where _rowid = " & our_ref
                                    Dim debtor4_dataset As DataSet = get_dataset("onestep", param2)
                                    If no_of_rows = 0 Then
                                        MsgBox("Unable to read debtor = " & our_ref)
                                        Exit Sub
                                    End If
                                    Dim summons_no As String = ""
                                    Try
                                        summons_no = debtor4_dataset.Tables(0).Rows(0).Item(0)
                                    Catch ex As Exception
                                        Continue For
                                    End Try
                                    If summons_no <> "" Then
                                        If summons_no = file_summons_no Then
                                            check_debtor(our_ref)
                                            correct_debtor_found = True
                                            Exit For
                                        End If
                                    End If
                                Next
                                If correct_debtor_found = False Then
                                    For idx2 = 1 To debtors_found
                                        If file_lo_date <> Nothing Then
                                            our_ref = case_table(idx2, 1)
                                            param2 = "select offence_court from Debtor where _rowid = " & our_ref
                                            Dim debtor3_dataset As DataSet = get_dataset("onestep", param2)
                                            If no_of_rows = 0 Then
                                                MsgBox("Unable to read debtor = " & our_ref)
                                                Exit Sub
                                            End If
                                            Dim os_lo_date As Date = Nothing
                                            Try
                                                os_lo_date = debtor3_dataset.Tables(0).Rows(0).Item(0)
                                            Catch ex As Exception

                                            End Try
                                            If os_lo_date <> Nothing Then
                                                If Format(CDate(os_lo_date), "dd.MM.yyyy") = Format(CDate(file_lo_date), "dd.MM.yyyy") Then

                                                    check_debtor(our_ref)
                                                    correct_debtor_found = True
                                                    Exit For
                                                End If
                                            End If
                                        End If
                                    Next
                                End If

                                If correct_debtor_found = False Then
                                    For idx2 = 1 To debtors_found
                                        If case_table(idx2, 2) = file_orig_debt Then
                                            our_ref = case_table(idx2, 1)
                                            check_debtor(our_ref)
                                            correct_debtor_found = True
                                            Exit For
                                        End If
                                    Next
                                    'not on onestep
                                    reconfile1 = reconfile1 & line(idx) & vbNewLine
                                    Continue For
                                End If
                            Else
                                'look for differences
                                our_ref = case_table(1, 1)
                                check_debtor(our_ref)
                            End If
                        End If
                    End If
                End If
            Next

            Dim filename_prefix As String = ""
            For idx = Len(filename) To 1 Step -1
                If Mid(filename, idx, 1) = "." Then
                    filename_prefix = Microsoft.VisualBasic.Left(filename, idx - 1)
                    Exit For
                End If
            Next
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_recon1.txt", reconfile1, False)
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_recon3.txt", reconfile3, False)
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_recon4.txt", reconfile4, False)
            If MsgBox("Create reconfile2 - cases on onestep not on file", MsgBoxStyle.OkCancel, "Case on onestep not on file") = MsgBoxResult.Ok Then
                create_reconfile2()
                My.Computer.FileSystem.WriteAllText(filename_prefix & "_recon2.txt", reconfile2, False)
                MsgBox("Reconciliation completed")
            Else
                MsgBox("Reconciliation completed apart from reconfile2")
            End If

            Me.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub check_debtor(ByVal our_ref As Integer)
        Try
            Dim diff_found As Boolean = False
            debtor_rows += 1
            debtor_table(debtor_rows) = our_ref
            param2 = "select max(date) from Payment" & _
                            " where debtorID = " & our_ref & _
                            " and (status = 'W' or status = 'R')" & _
                            " and amount > 0"
            Dim pay_ds As DataSet = get_dataset("onestep", param2)
            Dim lastPaymentdate As Date = Nothing
            Try
                If no_of_rows = 1 Then
                    lastPaymentdate = CDate(pay_ds.Tables(0).Rows(0).Item(0))
                End If
            Catch ex As Exception

            End Try
           
            param2 = "select offence_court, status_open_closed, status, client_ref, debt_original from Debtor where _rowid = " & our_ref
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If cl_ref <> Trim(debtor_dataset.Tables(0).Rows(0).Item(3)) Then
                
                write_file3(debtor_dataset, "Client reference", cl_ref, debtor_dataset.Tables(0).Rows(0).Item(3), lastPaymentdate)
                diff_found = True
            End If
            Dim lo_date As String = Nothing
            Try
                lo_date = debtor_dataset.Tables(0).Rows(0).Item(0)
            Catch ex As Exception

            End Try
            If lo_date = Nothing Then
                diff_found = True
            Else

                If Format(CDate(lo_date), "dd.MM.yyyy") <> Format(CDate(file_lo_date), "dd.MM.yyyy") Then
                    write_file3(debtor_dataset, "LO Date", Format(CDate(file_lo_date), "dd.MM.yyyy"), Format(CDate(lo_date), "dd.MM.yyyy"), lastPaymentdate)
                    diff_found = True
                End If
                If file_orig_debt <> debtor_dataset.Tables(0).Rows(0).Item(4) Then
                    write_file3(debtor_dataset, "Original debt", file_orig_debt, debtor_dataset.Tables(0).Rows(0).Item(4), lastPaymentdate)
                    diff_found = True
                End If
            End If


            'get client balance outstanding
            param2 = "select fee_amount, remited_fee from Fee where debtorID = " & our_ref & " and type = 'Debt'"
            Dim fee_dataset As DataSet = get_dataset(param1, param2)
            Dim cl_os As Decimal
            If no_of_rows = 0 Then
                cl_os = 0
            Else
                cl_os = fee_dataset.Tables(0).Rows(0).Item(0) - fee_dataset.Tables(0).Rows(0).Item(1)
            End If
            If file_cl_os <> cl_os Then
                write_file3(debtor_dataset, "Balance OS", file_cl_os, cl_os, lastPaymentdate)
                diff_found = True
            End If
            If Not diff_found Then
                write_file4(cl_ref, file_summons_no, our_ref)
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub write_file3(ByVal debtor_dataset As DataSet, ByVal field As String, ByVal academy_value As String, ByVal onestep_value As String, ByVal lastPaymentdate As Date)

        reconfile3 = reconfile3 & cl_ref & vbTab & file_summons_no & vbTab & our_ref & vbTab & file_lo_date & _
      vbTab & debtor_dataset.Tables(0).Rows(0).Item(1) & vbTab & debtor_dataset.Tables(0).Rows(0).Item(2) & _
      vbTab & field & vbTab & academy_value & vbTab & onestep_value & vbTab
        If lastPaymentdate <> Nothing Then
            reconfile3 &= Format(lastPaymentdate, "dd.MM.yyyy")
        End If

        reconfile3 &= vbNewLine

    End Sub

    Private Sub write_file4(ByVal cl_ref As String, ByVal summons_no As String, ByVal our_ref As Integer)

        reconfile4 = reconfile4 & cl_ref & vbTab & summons_no & vbTab & our_ref & vbTab & file_lo_date & vbTab & vbNewLine

    End Sub
    Private Sub create_reconfile2()
        'get onestep cases for selected schemes and see if already matched
        Dim idx, idx2, idx3, debtor As Integer
        ProgressBar1.Value = 5
        param1 = "onestep"
        For idx = 1 To sch_rows
            param2 = "select _rowid, client_ref, status, debt_amount, offence_number, offence_court from Debtor " & _
            " where clientschemeID = " & cs_table(idx) & _
            " and status_open_closed = 'O' order by _rowid"
            Dim debtor2_dataset As DataSet = get_dataset(param1, param2)
            Dim debtor2_rows As Integer = debtor2_dataset.Tables(0).Rows.Count - 1
            For idx2 = 0 To debtor2_rows
                Try
                    ProgressBar1.Value = (idx2 / debtor2_rows) * 100
                Catch ex As Exception

                End Try

                debtor = debtor2_dataset.Tables(0).Rows(idx2).Item(0)
                'see if debtor is in debtor table (previously matched)
                Dim debtor_found As Boolean = False
                Try
                    For idx3 = 1 To debtor_rows
                        If debtor_table(idx3) = debtor Then
                            debtor_found = True
                            Exit For
                        End If
                    Next
                    If Not debtor_found Then
                        write_recon2file(debtor, idx2, debtor2_dataset)
                    End If
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Next
        Next

    End Sub
    Private Sub write_recon2file(ByVal debtor As Integer, ByVal rowidx As Integer, ByVal debtor_dataset As DataSet)
        Dim cl_ref As String = debtor_dataset.Tables(0).Rows(rowidx).Item(1)
        Dim open_closed As String = "O"
        Dim status As String = debtor_dataset.Tables(0).Rows(rowidx).Item(2)
        Dim debt_amt As Decimal = debtor_dataset.Tables(0).Rows(rowidx).Item(3)
        Dim summons_no As String = ""
        Try
            summons_no = debtor_dataset.Tables(0).Rows(rowidx).Item(4)
        Catch ex As Exception

        End Try
        Dim lo_date As Date = Nothing
        Try
            lo_date = CDate(debtor_dataset.Tables(0).Rows(rowidx).Item(5))
        Catch ex As Exception
            lo_date = Nothing
        End Try
        param2 = "select fee_amount, remited_fee from Fee where debtorID = " & debtor & " and type = 'Debt'"
        Dim fee_dataset As DataSet = get_dataset(param1, param2)
        Dim cl_os As Decimal
        If no_of_rows = 0 Then
            cl_os = 0
        Else
            Try
                cl_os = fee_dataset.Tables(0).Rows(0).Item(0) - fee_dataset.Tables(0).Rows(0).Item(1)
            Catch ex As Exception

            End Try

        End If
        reconfile2 = reconfile2 & cl_ref & vbTab & debtor & vbTab & summons_no & vbTab
        If lo_date <> Nothing Then
            reconfile2 &= Format(lo_date, "dd.MM.yyyy")
        End If

        reconfile2 &= vbTab & open_closed & vbTab & status & vbTab & Format(debt_amt, "#0.00") & _
              vbTab & Format(cl_os, "#0.00") & vbNewLine
    End Sub
    Private Sub selectbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles selectbtn.Click
        conn_open = False
        clientfrm.ShowDialog()
        If MsgBox("Reconcile " & cl_name, MsgBoxStyle.OkCancel, "Academy reconciliation") = MsgBoxResult.Ok Then
            schemefrm.ShowDialog()
            If sch_rows = 0 Then
                MsgBox("No scheme selected so no reconciliation has been made")
            Else
                reconcile()
            End If
        Else
            MsgBox("No reconciliation has been made")
            Me.Close()
        End If
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        branch_cbox.Text = 1
    End Sub
End Class

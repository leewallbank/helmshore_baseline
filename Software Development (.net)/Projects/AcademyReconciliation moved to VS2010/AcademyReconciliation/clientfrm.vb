Public Class clientfrm

    Private Sub clientfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DataGridView1.Rows.Clear()
        'get clients from onestep
        param1 = "onestep"
        param2 = "select _rowid, name from Client order by name"
        Dim cl_dataset As DataSet = get_dataset(param1, param2)
        Dim cl_id As Integer
        Dim cl_rows As Integer = no_of_rows
        Dim cl_name As String
        'check branch and display branch 
        Dim idx As Integer = 0
        For idx = 0 To cl_rows - 1
            cl_id = cl_dataset.Tables(0).Rows(idx).Item(0)
            param2 = "select branchID from ClientScheme where clientID = " & cl_id
            Dim cs_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            If cs_dataset.Tables(0).Rows(0).Item(0) = mainfrm.branch_cbox.Text Then
                cl_name = Trim(cl_dataset.Tables(0).Rows(idx).Item(1))
                DataGridView1.Rows.Add(cl_id, cl_name)
            End If
        Next
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        Me.Close()
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter
        cl_id = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        cl_name = DataGridView1.Rows(e.RowIndex).Cells(1).Value
    End Sub
End Class
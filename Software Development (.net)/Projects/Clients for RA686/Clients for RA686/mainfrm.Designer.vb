<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.cl_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cl_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FeesSQLDataSet = New Clients_for_RA686.FeesSQLDataSet
        Me.Clients_for_RA686BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Clients_for_RA686TableAdapter = New Clients_for_RA686.FeesSQLDataSetTableAdapters.clients_for_RA686TableAdapter
        Me.ClientsForRA686BindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.exitbtn = New System.Windows.Forms.Button
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Clients_for_RA686BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ClientsForRA686BindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cl_no, Me.cl_name})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(568, 593)
        Me.DataGridView1.TabIndex = 1
        '
        'cl_no
        '
        Me.cl_no.HeaderText = "Client No"
        Me.cl_no.Name = "cl_no"
        '
        'cl_name
        '
        Me.cl_name.HeaderText = "Client name"
        Me.cl_name.Name = "cl_name"
        Me.cl_name.ReadOnly = True
        Me.cl_name.Width = 400
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Clients_for_RA686BindingSource
        '
        Me.Clients_for_RA686BindingSource.DataMember = "clients for RA686"
        Me.Clients_for_RA686BindingSource.DataSource = Me.FeesSQLDataSet
        '
        'Clients_for_RA686TableAdapter
        '
        Me.Clients_for_RA686TableAdapter.ClearBeforeFill = True
        '
        'ClientsForRA686BindingSource
        '
        Me.ClientsForRA686BindingSource.DataMember = "clients for RA686"
        Me.ClientsForRA686BindingSource.DataSource = Me.FeesSQLDataSet
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(411, 224)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(121, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "Exit without changes"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(568, 593)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Clients for RA686"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Clients_for_RA686BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ClientsForRA686BindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents FeesSQLDataSet As Clients_for_RA686.FeesSQLDataSet
    Friend WithEvents Clients_for_RA686BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Clients_for_RA686TableAdapter As Clients_for_RA686.FeesSQLDataSetTableAdapters.clients_for_RA686TableAdapter
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents ClientsForRA686BindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents cl_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cl_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents exitbtn As System.Windows.Forms.Button

End Class

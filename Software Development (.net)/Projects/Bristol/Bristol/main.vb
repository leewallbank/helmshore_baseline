Public Class mainform



    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim propaddr As String = ""
        Dim curraddr As String = ""
        Dim name As String = ""
        Dim idx, idx2, cl_idx As Integer
        Dim lines As Integer = 0
        Dim amt(5) As Decimal
        Dim lodate(5), fromdate, todate As Date
        Dim clref(5) As String

        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "Client ref|Name|CurrentAddress|Debt Address|Amount|LO Date|From date|To date" & vbNewLine
        outfile = outline

        'look for A/C Ref
        For idx = 1 To lines - 1
            Dim amtstring As String
            If Mid(line(idx), 3, 7) = "A/C Ref" Then
                'end of case found - get client ref, amt and lo date
                ReDim clref(5)
                ReDim amt(5)
                ReDim lodate(5)
                Dim lostring As String
                clref(0) = Mid(line(idx + 3), 2, 15)
                amtstring = Mid(line(idx + 3), 43, 11)
                If Not IsNumeric(amtstring) Then
                    errorfile = errorfile & "Line  " & idx + 3 & " - amount not numeric - " _
                             & amtstring & vbNewLine
                Else
                    amt(0) = amtstring
                End If
                lostring = Mid(line(idx + 3), 69, 9)
                If Not IsDate(lostring) Then
                    errorfile = errorfile & "Line  " & idx + 3 & " - LO Date not valid - " _
                                                 & lostring & vbNewLine
                Else
                    lodate(0) = lostring
                End If
                fromdate = Nothing
                todate = Nothing
                name = ""
                curraddr = ""
                propaddr = ""


                'check for more client ref, amt and lo date
                For cl_idx = 1 To 5
                    If idx + cl_idx + 3 < lines Then
                        If Mid(line(idx + cl_idx + 3), 3, 11) <> "-----------" And _
                           Mid(line(idx + cl_idx + 3), 3, 3) <> "" Then
                            clref(cl_idx) = Mid(line(idx + cl_idx + 3), 2, 15)
                            amtstring = Mid(line(idx + cl_idx + 3), 43, 11)
                            If Not IsNumeric(amtstring) Then
                                errorfile = errorfile & "Line  " & idx + 3 & " - amount not numeric - " _
                                         & amtstring & vbNewLine
                            Else
                                amt(cl_idx) = amtstring
                            End If
                            lodate(cl_idx) = Mid(line(idx + cl_idx + 3), 69, 9)
                        Else
                            Exit For
                        End If
                    End If
                Next
                'get from line
                For idx2 = idx To idx - 20 Step -1
                    If Mid(line(idx2), 3, 4) = "From" Then
                        Dim tostring As String
                        fromdate = Mid(line(idx2), 8, 9)
                        tostring = Mid(line(idx2), 23, 9)
                        If tostring <> Nothing Then
                            todate = tostring

                        End If
                        'get property address
                    ElseIf Mid(line(idx2), 3, 8) = "Property" Then
                        propaddr = Mid(line(idx2), 21, 59)
                        If propaddr = "As correspondence address" Then
                            propaddr = ""
                        End If
                        'get current address
                    ElseIf Mid(line(idx2), 3, 7) = "Current" Then
                        curraddr = Mid(line(idx2), 21, 59)
                        'check for continuation on next line
                        If Mid(line(idx2 + 1), 3, 10) = "          " Then
                            curraddr = curraddr & " " & Mid(line(idx2 + 1), 21, 30)
                        End If
                        'get name
                    ElseIf Mid(line(idx2), 3, 2) = "WG" Then
                        name = Mid(line(idx2 - 1), 3, 60)
                        Exit For
                    End If
                Next
                'validate case details
                If clref(0) = Nothing Or Microsoft.VisualBasic.Left(clref(0), 3) = "   " Then
                    errorfile = errorfile & "Line  " & idx2 & " - No client reference" & vbNewLine
                End If

                'save case in outline
                outfile = outfile & clref(0) & "|" & name & "|" & curraddr & "|" & propaddr & "|" _
                    & amt(0) & "|" & lodate(0) & "|" & fromdate & "|"
                If todate = Nothing Then
                    outfile = outfile & " " & "|"
                Else
                    outfile = outfile & todate & "|"
                End If
                outfile = outfile & vbNewLine
                'check for multiple cases
                For idx2 = 1 To 5
                    If clref(idx2) <> Nothing Then
                        outfile = outfile & clref(idx2) & "|" & name & "|" & curraddr & "|" & propaddr & "|" _
                        & amt(idx2) & "|" & lodate(idx2) & "|" & fromdate & "|"
                        If todate = Nothing Then
                            outfile = outfile & " " & "|"
                        Else
                            outfile = outfile & todate & "|"
                        End If
                        outfile = outfile & vbNewLine
                    Else
                        Exit For
                    End If
                Next
            End If
        Next

        viewbtn.Enabled = True
        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText("error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
End Class

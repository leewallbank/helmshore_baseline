Public Class mainfrm

    Private Sub loadbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles loadbtn.Click
        exitbtn.Enabled = False
        ProgressBar1.Value = 5
        'check batch not already in table
        Dim batch_no As Integer = 0
        Dim batch_date As DateTime
        Try
            batch_no = InputBox("Enter batch no", "Enter batch number")
        Catch ex As Exception
            If batch_no = 0 Then
                Exit Sub
            End If
            MessageBox.Show(ex.Message)
        End Try
        Dim result As Integer = check_batch(batch_no)
        If result > 0 Then
            MessageBox.Show("Batch " & batch_no & " has already been loaded")
            Exit Sub
        End If
        Dim date_valid As Boolean = False
        Do Until date_valid
            Try
                date_valid = True
                Dim temp As String
                temp = InputBox("Enter batch date (dd.mm.YYYY)", "Enter batch date")
                If Len(temp) = 0 Then
                    Exit Sub
                End If
                batch_date = temp
            Catch ex As Exception
                MessageBox.Show("Invalid date entered")
                date_valid = False
            End Try
        Loop

        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "XLS files|*.xls"
            .FileName = ""
            .CheckFileExists = True
        End With
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MessageBox.Show("No file Selected")
            Exit Sub
        End If
        load_vals(OpenFileDialog1.FileName, batch_no)
        If finalrow = 0 Then
            Exit Sub
        End If
        'insert details into table
        Try
            Dim rowidx, colidx As Integer
            For rowidx = 2 To finalrow
                If vals(rowidx, 4) = "L" _
                Or vals(rowidx, 4) = "M" Then
                    If vals(rowidx, 31) <> "Y" Then
                        Continue For
                    End If
                End If
                Dim name As String = ""
                Dim address As String = ""
                Dim res_score As Integer = 0
                If vals(rowidx, 4) = "M" Then
                    For colidx = 32 To 36
                        name = name & " " & Trim(vals(rowidx, colidx))
                    Next
                    For colidx = 37 To 46
                        address = address & " " & Trim(vals(rowidx, colidx))
                    Next
                    If vals(rowidx, 50) = " " Then
                        res_score = 0
                    Else
                        res_score = vals(rowidx, 50)
                    End If
                End If
                insert_batch(batch_no, batch_date, vals(rowidx, 2), vals(rowidx, 4), _
                    res_score, name, address)
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            exitbtn.Enabled = True
            Exit Sub
        End Try
        MessageBox.Show("Batch entered successfully")
        Dim next_batch As Integer = TraceTableAdapter.ScalarQuery().Value + 1
        Label1.Text = "Next batch to load is " & next_batch
        Label2.Text = "Sheet name should be BATCH" & next_batch
        exitbtn.Enabled = True
        ProgressBar1.Value = 100
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub


    Private Sub delbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles delbtn.Click
        Dim batch_no As Integer = 0
        Try
            batch_no = InputBox("Enter batch no", "Enter batch number")

            delete_batch(batch_no)
        Catch ex As Exception
            If batch_no = 0 Then
                Exit Sub
            End If
            MessageBox.Show(ex.Message)
            Exit Sub
        End Try
        MessageBox.Show("Batch deleted")
        Dim next_batch As Integer = TraceTableAdapter.ScalarQuery().Value + 1
        Label1.Text = "Next batch to load is " & next_batch
        Label2.Text = "Sheet name should be BATCH" & next_batch
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim next_batch As Integer = TraceTableAdapter.ScalarQuery().Value + 1
        Label1.Text = "Next batch to load is " & next_batch
        Label2.Text = "Sheet name should be BATCH" & next_batch
    End Sub

    
End Class

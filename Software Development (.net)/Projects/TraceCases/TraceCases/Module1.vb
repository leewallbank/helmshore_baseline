Module Module1

    Public row_count, debtor, branch_no, branch, parm_csid, sortcode, bankacc As Integer
    Public branch_date As Date

    Public Sub delete_batch(ByVal batch_no As Integer)
        Dim traceconn As New OleDb.OleDbConnection

        Dim previousConnectionState As ConnectionState
        previousConnectionState = traceconn.State
        If traceconn.State = ConnectionState.Closed Then
            traceconn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=r:\Access tables\Trace.mdb;Persist Security Info=False"
            traceconn.Open()
        End If
        Dim query As String
        query = "delete from Trace where Batch_no = ?"
        Dim cmd As New OleDb.OleDbCommand(query, traceconn)
        Dim parameter As New OleDb.OleDbParameter
        parameter.ParameterName = "Batch_no"
        parameter.OleDbType = OleDb.OleDbType.Integer
        parameter.Direction = ParameterDirection.Input
        parameter.Value = batch_no
        cmd.Parameters.Add(parameter)
        cmd.ExecuteNonQuery()
        traceconn.Close()
    End Sub
    Public Function check_batch(ByVal batch_no As Integer) As Integer
        Dim traceconn As New OleDb.OleDbConnection
        traceconn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=r:\Access tables\trace.mdb;Persist Security Info=False"
        traceconn.Open()

        Dim query As String = "select count(*) from Trace where Batch_no = ?"
        Dim cmd As New OleDb.OleDbCommand(query, traceconn)
        cmd.CommandType = CommandType.Text
        Dim parameter As New OleDb.OleDbParameter
        parameter.ParameterName = "Batch_no"
        parameter.OleDbType = OleDb.OleDbType.Integer
        parameter.Direction = ParameterDirection.Input
        parameter.Value = batch_no
        cmd.Parameters.Add(parameter)

        Dim rdr As OleDb.OleDbDataReader
        rdr = cmd.ExecuteReader()
        If rdr.HasRows Then
            Using rdr
                While rdr.Read
                    row_count = rdr.GetValue(0)
                End While
            End Using
        Else
            row_count = 0
        End If
        traceconn.Close()
        Return row_count
    End Function
    Public Sub insert_batch(ByVal batch_no As Integer, ByVal batch_date As Date, ByVal debtor As Integer, ByVal result As String, ByVal residencyscore As Integer, ByVal debtorname As String, ByVal debtoraddress As String)
        Dim traceconn As New OleDb.OleDbConnection

        Dim previousConnectionState As ConnectionState
        previousConnectionState = traceconn.State
        If traceconn.State = ConnectionState.Closed Then
            traceconn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=r:\Access tables\Trace.mdb;Persist Security Info=False"
            traceconn.Open()
        End If

        Dim query As String

        query = "insert into Trace values(?,?,?,?,?,?,?) "

        Dim parm1, parm2, parm3, parm4, parm5, parm6, parm7 As New OleDb.OleDbParameter

        Dim cmd2 As New OleDb.OleDbCommand(query, traceconn)
        cmd2.CommandType = CommandType.Text
        parm1.ParameterName = "Batch_no"
        parm1.OleDbType = OleDb.OleDbType.Integer
        parm1.Direction = ParameterDirection.Input
        parm1.Value = batch_no
        cmd2.Parameters.Add(parm1)
        parm2.ParameterName = "Batch_date"
        parm2.OleDbType = OleDb.OleDbType.Date
        parm2.Direction = ParameterDirection.Input
        parm2.Value = batch_date
        cmd2.Parameters.Add(parm2)
        parm3.ParameterName = "Debtor"
        parm3.OleDbType = OleDb.OleDbType.Integer
        parm3.Direction = ParameterDirection.Input
        parm3.Value = debtor
        cmd2.Parameters.Add(parm3)
        parm4.ParameterName = "Result"
        parm4.OleDbType = OleDb.OleDbType.VarChar
        parm4.Direction = ParameterDirection.Input
        parm4.Value = result
        cmd2.Parameters.Add(parm4)
        parm5.ParameterName = "ResidencyScore"
        parm5.OleDbType = OleDb.OleDbType.Integer
        parm5.Direction = ParameterDirection.Input
        parm5.Value = residencyscore
        cmd2.Parameters.Add(parm5)
        parm6.ParameterName = "DebtorName"
        parm6.OleDbType = OleDb.OleDbType.VarChar
        parm6.Direction = ParameterDirection.Input
        parm6.Value = debtorname
        cmd2.Parameters.Add(parm6)
        parm7.ParameterName = "DebtorAddress"
        parm7.OleDbType = OleDb.OleDbType.VarChar
        parm7.Direction = ParameterDirection.Input
        parm7.Value = debtoraddress
        cmd2.Parameters.Add(parm7)
        cmd2.ExecuteNonQuery()
        traceconn.Close()
    End Sub

End Module

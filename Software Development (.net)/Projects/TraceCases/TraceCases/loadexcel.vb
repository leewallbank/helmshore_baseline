Module loadexcel

    Public vals(1, 1) As String
    Public finalrow, bar_value As Integer
    Public Sub load_vals(ByVal myss As String, ByVal batch_no As Integer)
        Dim row, col, finalcol As Integer
        Dim xl = CreateObject("Excel.Application")
        xl.Workbooks.open(myss)
        Dim sheetname As String
        
        sheetname = "BATCH" & batch_no

        Try
            xl.worksheets(sheetname).activate()
        Catch ex As Exception
            MessageBox.Show("Unable to read spreadsheet")
            xl.workbooks.close()
            End
        End Try

        finalrow = xl.activesheet.usedrange.rows.count
        finalcol = xl.activesheet.usedrange.columns.count
        ReDim vals(finalrow, finalcol)
        Dim idx As Integer = 0
        For row = 2 To finalrow
            bar_value += 1
            If bar_value > 100 Then
                bar_value = 5
            End If
            mainfrm.ProgressBar1.Value = bar_value
            For col = 1 To finalcol
                idx += 1
                If xl.activesheet.cells(row, col).value = Nothing Then
                    vals(row, col) = " "
                Else
                    vals(row, col) = xl.activesheet.cells(row, col).value.ToString
                End If
            Next
        Next
        xl.workbooks.close()
        xl = Nothing
    End Sub
End Module

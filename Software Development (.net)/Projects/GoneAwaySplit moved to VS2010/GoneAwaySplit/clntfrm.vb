Public Class clntfrm

    Private Sub clntfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'populate datagridview1
        param1 = "onestep"
        param2 = "select name, _rowid from Client " & _
        "  order by name"
        Dim cl_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("No clients found")
            Exit Sub
        End If
        Dim cl_rows As Integer = no_of_rows
        Dim idx As Integer
        For idx = 0 To cl_rows - 1
            Dim cl_name As String = cl_dataset.Tables(0).Rows(idx).Item(0)
            Dim cl_ID As Integer = cl_dataset.Tables(0).Rows(idx).Item(1)
            
            DataGridView1.Rows.Add(cl_ID, cl_name)
        Next
    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        selected_cl_id = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        selected_cl_name = DataGridView1.Rows(e.RowIndex).Cells(1).Value
        Me.Close()
    End Sub
End Class
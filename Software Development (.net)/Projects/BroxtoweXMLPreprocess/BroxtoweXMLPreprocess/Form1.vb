Public Class Form1
    '160810 change amount due to currentbalance
    Dim new_file As String = ""
    Dim record As String = ""
    Dim filename, saved_comments, saved_name2 As String
    Dim first_error As Boolean = True
    Dim account_number, last_account_number, forename, surname As String
    Dim last_postcode, comments, recovery_control As String
    Dim lo_date, from_date, end_date As Date
    Dim match_count As Integer
    Dim tot_matches As Integer = 0
    Dim prop_addr1, prop_addr2, prop_addr3, prop_addr4, prop_addr5, prop_postcode As String
    Dim postal_addr1, postal_addr2, postal_addr3, postal_addr4, postal_addr5, postal_postcode As String
    Dim lo_amount, last_lo_amount, amt_due, curr_bal, costs_bal, tot_costs_raised As Decimal
    Dim mobile, telephone, email As String

    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click
        Try
            With OpenFileDialog1
                .Title = "Read XML file"
                .Filter = "XML file|*.xml"
                .FileName = ""
                .CheckFileExists = True
            End With
            Dim xml_dataSet As New DataSet()
            Dim doc As New Xml.XmlDocument()

            Dim file As String = "AccountNumber" & "|" & "AmountDue" & "|" & "CourtHearingDate" & "|" & "LiabilityOrderAmount" & _
                        "|" & "PersonForenames" & "|" & "PersonSurname" & "|" & "PostalAdd1" & "|" & _
                        "PostalAdd2" & "|" & "PostalAdd3" & "|" & "PostalAdd4" & "|" & _
                        "PostalAdd5" & "|" & "PostalPostcode" & "|" & "PropertyAdd1" & "|" & "PropertyAdd2" & _
                        "|" & "PropertyAdd3" & "|" & "PropertyAdd4" & "|" & "PropertyAdd5" & _
                        "|" & "PropertyPostcode" & "|" & "ChargeStartDate" & "|" & "ChargeEndDate" & "|" & "YearPeriod" & _
                        "|Mobile|Telephone|email|" & "Comments" & "|" & "Name2|" & vbNewLine

            If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                Try
                    filename = OpenFileDialog1.FileName
                    Dim ln2 As Integer = Microsoft.VisualBasic.Len(filename)
                    new_file = Microsoft.VisualBasic.Left(filename, ln2 - 4) & "_error.txt"
                    My.Computer.FileSystem.WriteAllText(new_file, "Error messages " & Now, False)
                    reset_fields()
                    Dim rdr_name As String = ""
                    Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
                    reader.Read()
                    ProgressBar1.Value = 5
                    Dim record_count As Integer = 0
                    Dim first_account As Boolean = True
                    While (reader.ReadState <> Xml.ReadState.EndOfFile)
                        Try
                            rdr_name = reader.Name
                        Catch
                            Continue While
                        End Try
                        If reader.NodeType > 1 And rdr_name <> "BailiffExtract" Then
                            reader.Read()
                            Continue While
                        End If

                        If rdr_name = "root" _
                        Or rdr_name = "Module" Then
                            reader.Read()
                            Continue While
                        End If
                        'Note ReadElementContentAsString moves focus to next element so don't need read
                        ProgressBar1.Value = record_count
                        Select Case rdr_name
                            Case "AccountNumber"
                                record_count += 1
                                If record_count > 100 Then
                                    record_count = 0
                                End If
                                account_number = reader.ReadElementContentAsString
                            Case "CurrentBalance"  '160810 was AmountDue"
                                amt_due = reader.ReadElementContentAsString
                            Case "RecoveryControl"
                                recovery_control = reader.ReadElementContentAsString
                                recovery_control = Microsoft.VisualBasic.Left(recovery_control, 5) & _
                                Microsoft.VisualBasic.Right(recovery_control, 2)
                                'Case "CurrentBalance"
                                'Try
                                '    curr_bal = reader.ReadElementContentAsString
                                'Catch ex As Exception
                                '    curr_bal = Nothing
                                'End Try
                                'If curr_bal <> Nothing Then
                                '    comments = comments & "CurrentBalance:" & Format(curr_bal, "0.00") & ";"
                                'End If
                            Case "CostsBalance"
                                Try
                                    costs_bal = reader.ReadElementContentAsString
                                Catch ex As Exception
                                    costs_bal = Nothing
                                End Try
                                If costs_bal <> Nothing Then
                                    comments = comments & "CostsBalance:" & Format(costs_bal, "0.00") & ";"
                                End If
                            Case "TotalCostsRaised"
                                Try
                                    tot_costs_raised = reader.ReadElementContentAsString
                                Catch ex As Exception
                                    tot_costs_raised = Nothing
                                End Try
                                If tot_costs_raised <> Nothing Then
                                    comments = comments & "TotalCostsRaised:" & Format(tot_costs_raised, "0.00") & ";"
                                End If
                            Case "CourtHearingDate"
                                lo_date = reader.ReadElementContentAsString
                            Case "LiabilityOrderAmount"
                                lo_amount = reader.ReadElementContentAsString
                            Case "PersonForenames"
                                forename = Trim(reader.ReadElementContentAsString)
                            Case "PersonSurname"
                                surname = Trim(reader.ReadElementContentAsString)
                            Case "PersonPostalAdd1"
                                postal_addr1 = reader.ReadElementContentAsString
                            Case "PersonPostalAdd2"
                                postal_addr2 = reader.ReadElementContentAsString
                            Case "PersonPostalAdd3"
                                postal_addr3 = reader.ReadElementContentAsString
                            Case "PersonPostalAdd4"
                                postal_addr4 = reader.ReadElementContentAsString
                            Case "PersonPostalAdd5"
                                postal_addr5 = reader.ReadElementContentAsString
                            Case "PersonPostalPostcode"
                                postal_postcode = reader.ReadElementContentAsString
                            Case "PropertyAdd1"
                                prop_addr1 = reader.ReadElementContentAsString
                            Case "PropertyAdd2"
                                prop_addr2 = reader.ReadElementContentAsString
                            Case "PropertyAdd3"
                                prop_addr3 = reader.ReadElementContentAsString
                            Case "PropertyAdd4"
                                prop_addr4 = reader.ReadElementContentAsString
                            Case "PropertyAdd5"
                                prop_addr5 = reader.ReadElementContentAsString
                            Case "PropertyAddressPostcode"
                                prop_postcode = reader.ReadElementContentAsString
                            Case "ChargeStartDate"
                                from_date = reader.ReadElementContentAsString
                            Case "ChargeEndDate"
                                end_date = reader.ReadElementContentAsString
                            Case "ACMobile"
                                mobile = reader.ReadElementContentAsString
                            Case "ACTelephone"
                                telephone = reader.ReadElementContentAsString
                            Case "ACEmailAddress"
                                email = reader.ReadElementContentAsString
                            Case "BailiffExtract"
                                Dim nodeType As Integer = reader.NodeType
                                If account_number = "" And nodeType = 1 Then
                                    reader.Read()
                                Else
                                    If Not first_account Then
                                        If account_number = last_account_number And _
                                                lo_amount = last_lo_amount And _
                                            last_postcode = prop_postcode Then
                                            'file = file & record & forename & " " & surname & vbNewLine
                                            match_count += 1
                                            tot_matches += 1
                                            If match_count = 1 Then
                                                saved_name2 = forename & " " & surname
                                            Else
                                                If match_count = 2 Then
                                                    saved_comments = saved_comments & "other names:" & forename & " " & surname & ";"
                                                Else
                                                    saved_comments = saved_comments & forename & " " & surname & ";"
                                                End If

                                            End If
                                            'record = ""
                                        Else
                                            If record <> "" Then
                                                file = file & record & saved_comments & "|" & saved_name2 & vbNewLine
                                            End If
                                            save_record()
                                            match_count = 0
                                        End If
                                    Else
                                        first_account = False
                                        save_record()
                                        match_count = 0
                                    End If
                                    last_account_number = account_number
                                    last_lo_amount = lo_amount
                                    last_postcode = prop_postcode
                                    reset_fields()
                                    reader.Read()
                                End If
                            Case Else
                                reader.Read()
                        End Select
                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Else
                MsgBox("XML file not processed")
                Exit Sub
            End If
            'write out last record
            If record <> "" Then
                file = file & record & saved_comments & "|" & saved_name2 & vbNewLine
            End If

            'write file 
            Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_preprocess.txt"
            My.Computer.FileSystem.WriteAllText(new_file, file, False)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        ProgressBar1.Value = 100
        MsgBox("file saved as " & new_file)
        MsgBox("Records amalgamted = " & tot_matches)
        Me.Close()
    End Sub


    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub
    Private Sub save_record()
        record = account_number & "|" & amt_due & "|" & lo_date & "|" & lo_amount & "|" & _
        forename & "|" & surname & "|" & postal_addr1 & "|" & postal_addr2 & "|" & _
        postal_addr3 & "|" & postal_addr4 & "|" & postal_addr5 & "|" & postal_postcode & "|" _
        & prop_addr1 & "|" & prop_addr2 & "|" & prop_addr3 & "|" & prop_addr4 & "|" & _
        prop_addr5 & "|" & prop_postcode & "|" & from_date & "|" & end_date & "|" & recovery_control & "|" & _
        mobile & "|" & telephone & "|" & email & "|"
        saved_comments = comments
        saved_name2 = ""
    End Sub
    Private Sub write_error(ByVal error_message As String)
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_error.txt"
        error_message = error_message & vbNewLine
        My.Computer.FileSystem.WriteAllText(new_file, error_message, True)
    End Sub
    Private Sub write_change(ByVal change_message As String)
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_changes.txt"
        My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
    End Sub
    Private Sub remove_chrs(ByRef in_field As String)
        Dim idx2 As Integer
        Dim out_field As String = ""
        For idx2 = 1 To Microsoft.VisualBasic.Len(in_field)
            If Mid(in_field, idx2, 1) <> Chr(9) And _
              Mid(in_field, idx2, 1) <> Chr(10) And _
            Mid(in_field, idx2, 1) <> Chr(13) Then
                out_field = out_field & Mid(in_field, idx2, 1)
            End If
        Next
        in_field = out_field
    End Sub

    Private Sub reset_fields()
        account_number = ""
        lo_date = Nothing
        lo_amount = Nothing
        amt_due = Nothing
        recovery_control = ""
        curr_bal = Nothing
        costs_bal = Nothing
        tot_costs_raised = Nothing
        forename = ""
        surname = ""
        prop_addr1 = ""
        prop_addr2 = ""
        prop_addr3 = ""
        prop_addr4 = ""
        prop_addr5 = ""
        prop_postcode = ""
        postal_addr1 = ""
        postal_addr2 = ""
        postal_addr3 = ""
        postal_addr4 = ""
        postal_addr5 = ""
        postal_postcode = ""
        from_date = Nothing
        end_date = Nothing
        comments = ""
        mobile = ""
        telephone = ""
        email = ""
    End Sub

End Class

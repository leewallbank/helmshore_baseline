<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.savebtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.run_datetime_picker = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.FeesSQLDataSet = New SavePenformStatistics.FeesSQLDataSet
        Me.PenformStatisticsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PenformStatisticsTableAdapter = New SavePenformStatistics.FeesSQLDataSetTableAdapters.PenformStatisticsTableAdapter
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PenformStatisticsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'savebtn
        '
        Me.savebtn.Location = New System.Drawing.Point(104, 137)
        Me.savebtn.Name = "savebtn"
        Me.savebtn.Size = New System.Drawing.Size(75, 23)
        Me.savebtn.TabIndex = 0
        Me.savebtn.Text = "Save stats"
        Me.savebtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(193, 213)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'run_datetime_picker
        '
        Me.run_datetime_picker.Location = New System.Drawing.Point(91, 77)
        Me.run_datetime_picker.Name = "run_datetime_picker"
        Me.run_datetime_picker.Size = New System.Drawing.Size(121, 20)
        Me.run_datetime_picker.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(114, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Run for"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 213)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 4
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PenformStatisticsBindingSource
        '
        Me.PenformStatisticsBindingSource.DataMember = "PenformStatistics"
        Me.PenformStatisticsBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'PenformStatisticsTableAdapter
        '
        Me.PenformStatisticsTableAdapter.ClearBeforeFill = True
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.run_datetime_picker)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.savebtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Save penforms statistics"
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PenformStatisticsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents savebtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents run_datetime_picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents FeesSQLDataSet As SavePenformStatistics.FeesSQLDataSet
    Friend WithEvents PenformStatisticsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PenformStatisticsTableAdapter As SavePenformStatistics.FeesSQLDataSetTableAdapters.PenformStatisticsTableAdapter

End Class

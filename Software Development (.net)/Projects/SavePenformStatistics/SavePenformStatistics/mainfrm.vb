Public Class mainfrm
    Public start_year, start_month As Integer
    Public start_date As Date
    Public work_type_array(1000) As Integer
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        start_year = Year(Now)
        start_month = Month(Now) - 1
        If start_month = 0 Then
            start_month = 12
            start_year -= 1
        End If
        start_date = CDate(start_year & "." & start_month & "." & "1")
        Label1.Text = "Run for " & Format(start_date, "MMM yyyy")
        run_datetime_picker.Value = start_date
    End Sub

    Private Sub run_datetime_picker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles run_datetime_picker.Validated

    End Sub

    Private Sub run_datetime_picker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles run_datetime_picker.ValueChanged
        start_date = run_datetime_picker.Value
        Label1.Text = "Run for " & Format(start_date, "MMM yyyy")
        start_month = Format(start_date, "MM")
        start_year = Format(start_date, "yyyy")
    End Sub

    Private Sub savebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles savebtn.Click
        param1 = "onestep"

        Dim idx As Integer
        Dim end_date As Date
        Dim end_year As Integer = start_year
        Dim end_month As Integer = start_month + 1
        If end_month = 13 Then
            end_month = 1
            end_year += 1
        End If
        end_date = CDate(end_year & "." & end_month & "." & "1")
        param2 = "select caseNumber from PenForm " & _
        " where _createdDate >= '" & Format(start_date, "yyyy-MM-dd") & "'" & _
        " and _createdDate < '" & Format(end_date, "yyyy-MM-dd") & "'"
        Dim pf_dataset As DataSet = get_dataset(param1, param2)
        Dim pf_rows As Integer = no_of_rows - 1
        Dim last_csid As Integer = 0
        Dim last_sch_no, last_work_type As Integer
        Dim sch_no, work_type As Integer
        ProgressBar1.Value = 5
        For idx = 0 To pf_rows
            ProgressBar1.Value = (idx / pf_rows) * 100
            Application.DoEvents()
            Dim debtor As Integer
            Try
                debtor = pf_dataset.Tables(0).Rows(idx).Item(0)
            Catch ex As Exception
                Continue For
            End Try

            param2 = "select clientschemeID from Debtor " & _
            " where _rowid = " & debtor
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(0)
            If csid = last_csid Then
                work_type_array(last_work_type) += 1
                Continue For
            End If
            param2 = "select schemeID from ClientScheme" & _
            " where _rowid = " & csid
            Dim csid_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                MsgBox("SchemeID not found for clientschemeID = " & csid)
                Exit Sub
            End If
            sch_no = csid_dataset.Tables(0).Rows(0).Item(0)
            param2 = "select work_type from Scheme where _rowid = " & sch_no
            Dim sch_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                MsgBox("work type not found for schemeID = " & sch_no)
                Exit Sub
            End If
            work_type = sch_dataset.Tables(0).Rows(0).Item(0)
            last_csid = csid
            work_type_array(work_type) += 1
            last_sch_no = sch_no
            last_work_type = work_type
        Next
        'save totals in table
        Dim pf_val As Integer
        For idx = 1 To 1000
            work_type = idx
            pf_val = work_type_array(idx)
            If pf_val = 0 Then
                Continue For
            End If
            Me.PenformStatisticsTableAdapter.InsertQuery(start_year, start_month, work_type, pf_val)
        Next
        MsgBox("Completed")
        Me.Close()
    End Sub

    Private Sub PenformStatisticsBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.PenformStatisticsBindingSource.EndEdit()
        Me.PenformStatisticsTableAdapter.Update(Me.FeesSQLDataSet.PenformStatistics)

    End Sub
End Class

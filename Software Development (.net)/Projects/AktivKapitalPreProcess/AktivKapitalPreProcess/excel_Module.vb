Module excel_Module
    Public filename, errorfile As String
    Public vals(1, 1) As String
    Public finalrow, finalcol As Integer
    Public Sub load_vals(ByVal filename As String)
        Dim row, col As Integer
        Dim xl = CreateObject("Excel.Application")
        xl.Workbooks.open(filename)
        xl.worksheets("Sheet1").activate()
        finalrow = xl.activesheet.usedrange.rows.count
        finalcol = xl.activesheet.usedrange.columns.count
        ReDim vals(finalrow, finalcol)
        Dim idx As Integer = 0
        Try
            For row = 1 To finalrow
                mainform.ProgressBar1.Value = (row / finalrow) * 100
                Application.DoEvents()
                For col = 1 To finalcol
                    idx += 1
                    If xl.activesheet.cells(row, col).value = Nothing Then
                        vals(row, col) = ""
                    Else
                        vals(row, col) = xl.activesheet.cells(row, col).value.ToString
                    End If
                    If vals(row, 1) = Nothing Then
                        Exit For
                    End If
                Next
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        
        xl.workbooks.close()
        xl = Nothing
    End Sub
End Module

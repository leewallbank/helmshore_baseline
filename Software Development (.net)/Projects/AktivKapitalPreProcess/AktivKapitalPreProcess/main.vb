Public Class mainform
    Dim outline As String = ""
    Dim outfile_asset, outfile_first, outfile_sb_asset, outfile_sb_first, outfile_zug, outfile_sb_zug As String
    Dim debt_amt As Decimal
    Dim client_found As Integer = 0
    Dim lodate As Date
    Dim clref As String = ""
    Dim off_number, batch_no, off_location As String
    Dim off_court As Date
    Dim title, forename, surname As String
    Dim dob As Date
    Dim addr1, addr2, addr3, addr4, pcode As String
    Dim d_addr1, d_addr2, d_addr3, d_addr4, d_addr5, d_pcode As String
    Dim phone, emp_phone, fax As String
    Dim notes As String
    Dim joint_name As String
    Dim joint_address As String
    Dim new_format As Boolean
    Dim prev_addr As String
    Dim statute_barred As String
    Dim ignore_row As Boolean
    Dim FileContents() As String
    Dim InputLineArray() As String

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click

        'If MsgBox("New format?", MsgBoxStyle.YesNo, "New Format") = MsgBoxResult.Yes Then
        new_format = True
        'Else
        'new_format = False
        'End If
        Dim filetext As String
        Dim fileok As Boolean = True
        disable_buttons()
        message_tbox.Text = "Opening File"
        Application.DoEvents()
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "csv files|*.csv*"

            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                'load_vals(filename)
            Catch ex As Exception
                MsgBox("Unable to read file - message " & ex.Message)
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            message_tbox.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            message_tbox.Text = "File NOT Opened"
        End If
        enable_buttons()
        Application.DoEvents()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        '18.10.2011 col H type of debt2, col BA statue barred, col BB score
        reformbtn.Enabled = False
        errbtn.Enabled = False
    End Sub
    Private Sub disable_buttons()
        exitbtn.Enabled = False
        openbtn.Enabled = False
    End Sub
    Private Sub enable_buttons()
        exitbtn.Enabled = True
        openbtn.Enabled = True
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        disable_buttons()
        'batch_no = Nothing
        'batch_no = InputBox("Enter Batch Number", "Batch Numner")
        'If batch_no = Nothing Then
        '    MsgBox("Incorrect batch number")
        '    enable_buttons()
        '    Exit Sub
        'End If
        Dim idx, idx2 As Integer
        notes = ""
        joint_name = ""
        joint_address = ""
        prev_addr = ""
        Dim test_date As Date
        Dim test_amt As Decimal
        reformbtn.Enabled = False
        message_tbox.Text = "Reformatting file"
        Application.DoEvents()
        'write out headings
        outfile_sb_asset = "Client Ref|OffenceNumber|OffenceCourt|CurrentBalance|Title|Forename|Surname|DOB|" & _
                    "Add1|Add2|Add3|Add4|Pcode|Phone|EmpPhone|Fax|DebtAdd1|DebtAdd2|DebtAdd3|" & _
                    "DebtAdd4|DebtAdd5|DebtPC|Batch No|StatuteBarred|OffenceLocation|Notes" & vbNewLine
        outfile_sb_asset = outfile_sb_asset
        outfile_sb_first = outfile_sb_asset
        outfile_sb_zug = outfile_sb_asset

        outfile_asset = "Client Ref|OffenceNumber|OffenceCourt|CurrentBalance|Title|Forename|Surname|DOB|" & _
                "Add1|Add2|Add3|Add4|Pcode|Phone|EmpPhone|Fax|DebtAdd1|DebtAdd2|DebtAdd3|" & _
                "DebtAdd4|DebtAdd5|DebtPC|Batch No|Notes" & vbNewLine
        outfile_first = outfile_asset
        outfile_zug = outfile_asset

        FileContents = System.IO.File.ReadAllLines(filename)


        finalrow = UBound(FileContents)
        finalcol = 53
        Dim colval As String
        For idx = 1 To finalrow
            InputLineArray = FileContents(idx).Split(",")
            ignore_row = False
            Try
                ProgressBar1.Value = (idx / finalrow) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            For idx2 = 1 To finalcol
                If ignore_row = True Then
                    Continue For
                End If
                If new_format Then
                    process_row_new_format(idx)
                    Continue For
                End If
                'old format
                colval = Trim(vals(idx, idx2))
                Select Case idx2
                    Case 1
                        notes = "ClientCode:" & colval & ";"
                    Case 2
                        notes = notes & "ClientName:" & colval & ";"
                    Case 3
                        client_found = 0
                        If colval = "Aktiv Kapital First Investment Limited" Then
                            client_found = 1
                        ElseIf colval = "Aktiv Kapital Asset Investments Limited" Then
                            client_found = 2
                        ElseIf colval = "Aktiv Kapital Portfolio AS Zug Branch" Then
                            client_found = 3
                        Else
                            errorfile = errorfile & "Invalid Master Client - " & colval & " Line " & idx
                        End If
                        notes = notes & "MasterClient:" & colval & ";"
                    Case 4
                        clref = colval
                    Case 5
                        off_number = colval
                    Case 6
                        Try
                            off_court = colval
                        Catch ex As Exception
                            off_court = Nothing
                        End Try
                    Case 7
                        notes = notes & "DebtType:" & colval & ";"
                    Case 8
                        debt_amt = colval
                    Case 9
                        notes = notes & "DefaultBal:" & colval & ";"
                    Case 10
                        If colval.Length > 0 Then
                            Try
                                test_date = colval
                            Catch ex As Exception
                                test_date = Nothing
                            End Try
                            '19.6.2014 code moved inside if/end if
                            If test_date <> Nothing Then
                                notes = notes & "DefaultDate:" & Format(test_date, "dd/MM/yyyy") & ";"
                            End If
                        End If
                        
                    Case 11
                        title = colval
                    Case 12
                        forename = colval
                    Case 13
                        surname = colval
                    Case 14
                        Try
                            dob = colval
                        Catch ex As Exception
                            dob = Nothing
                        End Try
                    Case 15
                        addr1 = colval
                    Case 16
                        addr2 = colval
                    Case 17
                        addr3 = colval
                    Case 18
                        addr4 = colval
                    Case 19
                        pcode = colval
                    Case 20
                        phone = colval
                    Case 21
                        emp_phone = colval
                    Case 22
                        fax = colval
                    Case 23
                        joint_name = ""
                        If colval.Length > 0 Then
                            joint_name = colval
                        End If
                    Case 24
                        If colval.Length > 0 Then
                            joint_name = Trim(joint_name & " " & colval)
                        End If
                    Case 25
                        If colval.Length > 0 Then
                            joint_name = Trim(joint_name & " " & colval)
                        End If
                        If joint_name.Length > 0 Then
                            notes = notes & "JointName:" & joint_name & ";"
                        End If
                    Case 26
                        If colval.Length > 0 Then
                            Try
                                test_date = colval
                            Catch ex As Exception
                                test_date = Nothing
                            End Try
                            If test_date <> Nothing Then
                                notes = notes & "JointDOB:" & Format(test_date, "dd/MM/yyyy") & ";"
                            End If
                        End If
                    Case 27
                        If colval.Length > 0 Then
                            joint_address = colval
                        Else
                            joint_address = ""
                        End If
                    Case 28
                        If colval.Length > 0 Then
                            joint_address = Trim(joint_address & " " & colval)
                        End If
                    Case 29
                        If colval.Length > 0 Then
                            joint_address = Trim(joint_address & " " & colval)
                        End If
                    Case 30
                        If colval.Length > 0 Then
                            joint_address = Trim(joint_address & " " & colval)
                        End If
                    Case 31
                        If colval.Length > 0 Then
                            joint_address = Trim(joint_address & " " & colval)
                        End If
                        If joint_address.Length > 0 Then
                            notes = notes & "JointAddress:" & joint_address & ";"
                        End If
                    Case 32
                        If colval.Length > 0 Then
                            notes = notes & "JointTelNo:" & colval & ";"
                        End If
                    Case 33
                        If colval.Length > 0 Then
                            notes = notes & "JointEmpTelNo:" & colval & ";"
                        End If
                    Case 34
                        If colval.Length > 0 Then
                            notes = notes & "JointOtherTelNo:" & colval & ";"
                        End If
                    Case 35
                        Try
                            test_date = colval
                        Catch ex As Exception
                            test_date = Nothing
                        End Try
                        If test_date <> Nothing Then
                            notes = notes & "LastPayDate:" & Format(test_date, "dd/MM/yyyy") & ";"
                        End If
                    Case 36
                        If colval.Length > 0 Then
                            Try
                                test_amt = colval
                            Catch ex As Exception
                                test_amt = Nothing
                            End Try
                            If test_amt <> Nothing Then
                                notes = notes & "LastPayVal:" & Format(test_amt, "fixed") & ";"
                            End If
                        End If
                    Case 37
                        If colval.Length > 0 Then
                            Try
                                test_date = colval
                            Catch ex As Exception
                                test_date = Nothing
                            End Try
                            If test_date <> Nothing Then
                                notes = notes & "ContractStartDate:" & Format(test_date, "dd/MM/yyyy") & ";"
                            End If
                        End If
                    Case 38
                        If colval.Length > 0 Then
                            notes = notes & "CourtCode:" & colval & ";"
                        End If
                    Case 39
                        If colval.Length > 0 Then
                            notes = notes & "CaseNumber:" & colval & ";"
                        End If
                    Case 40
                        If colval.Length > 0 Then
                            Try
                                test_date = colval
                            Catch ex As Exception
                                test_date = Nothing
                            End Try
                            If test_date <> Nothing Then
                                notes = notes & "JudgementDate:" & Format(test_date, "dd/MM/yyyy") & ";"
                            End If
                        End If
                    Case 41
                        If colval.Length > 0 Then
                            prev_addr = colval
                        Else
                            prev_addr = ""
                        End If
                    Case 42
                        If colval.Length > 0 Then
                            prev_addr = Trim(prev_addr & " " & colval)
                        End If
                    Case 43
                        If colval.Length > 0 Then
                            prev_addr = Trim(prev_addr & " " & colval)
                        End If
                    Case 44
                        If colval.Length > 0 Then
                            prev_addr = Trim(prev_addr & " " & colval)
                        End If
                    Case 45
                        If colval.Length > 0 Then
                            prev_addr = Trim(prev_addr & " " & colval)
                        End If
                        If prev_addr.Length > 0 Then
                            notes = notes & "PrevAddr:" & prev_addr & ";"
                        End If
                    Case 46
                        d_addr1 = colval
                    Case 47
                        d_addr2 = colval
                    Case 48
                        d_addr3 = colval
                    Case 49
                        d_addr4 = colval
                    Case 50
                        d_addr5 = colval
                    Case 51
                        d_pcode = colval
                End Select
            Next
            If ignore_row Then
                Exit For
            End If
            'shuffle addresses up
            If addr2.Length = 0 Then
                addr2 = addr3
                addr3 = addr4
                addr4 = ""
                If addr2.Length = 0 Then
                    addr2 = addr3
                    addr3 = ""
                End If
            End If
            If addr3.Length = 0 Then
                addr3 = addr4
                addr4 = ""
            End If
            If d_addr2.Length = 0 Then
                d_addr2 = d_addr3
                d_addr3 = d_addr4
                d_addr4 = d_addr5
                d_addr5 = ""
                If d_addr2.Length = 0 Then
                    d_addr2 = addr3
                    d_addr3 = d_addr4
                    d_addr4 = ""
                End If
            End If
            If d_addr3.Length = 0 Then
                d_addr3 = d_addr4
                d_addr4 = d_addr5
                d_addr5 = ""
            End If
            Try
                outline = clref & "|" & off_number & "|"
                If off_court = Nothing Then
                    outline = outline & "|"
                Else
                    outline = outline & Format(off_court, "dd/MM/yyyy") & "|"
                End If
                outline = outline & Format(debt_amt, "fixed") & "|" & title & "|" & forename & "|" & surname & "|"
                If dob = Nothing Then
                    outline = outline & "|"
                Else
                    outline = outline & Format(dob, "dd/MM/yyyy") & "|"
                End If
                outline = outline & addr1 & "|" & addr2 & "|" & addr3 & "|" & addr4 & "|" & _
                pcode & "|" & phone & "|" & emp_phone & "|" & fax & "|" & d_addr1 & "|" & _
                d_addr2 & "|" & d_addr3 & "|" & d_addr4 & "|" & d_addr5 & "|" & d_pcode & "|" & batch_no
                If new_format Then
                    outline = outline & "|" & statute_barred & "|" & off_location
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            Dim notes2 As String = ""
            Dim notes3 As String = notes
            If notes.Length <= 250 Or new_format Then
                If client_found = 2 Then
                    If statute_barred.Length > 0 Then
                        outfile_sb_asset = outfile_sb_asset & outline & "|" & notes & vbNewLine
                    Else
                        outfile_asset = outfile_asset & outline & "|" & notes & vbNewLine
                    End If
                ElseIf client_found = 1 Then
                    If statute_barred.Length > 0 Then
                        outfile_sb_first = outfile_sb_first & outline & "|" & notes & vbNewLine
                    Else
                        outfile_first = outfile_first & outline & "|" & notes & vbNewLine
                    End If
                ElseIf client_found = 3 Then
                    If statute_barred.Length > 0 Then
                        outfile_sb_zug = outfile_sb_zug & outline & "|" & notes & vbNewLine
                    Else
                        outfile_zug = outfile_zug & outline & "|" & notes & vbNewLine
                    End If
                End If

            Else
                While notes3.Length > 250
                    Dim len As Integer = Microsoft.VisualBasic.Len(notes3)
                    Dim idx3 As Integer
                    For idx3 = 250 To 1 Step -1
                        If Mid(notes3, idx3, 1) = ";" Then
                            Exit For
                        End If
                    Next
                    notes2 = notes2 & Microsoft.VisualBasic.Left(notes3, idx3)
                    Dim idx5 As Integer
                    For idx5 = idx3 To 250
                        notes2 = notes2 & " "
                    Next
                    notes3 = Microsoft.VisualBasic.Right(notes3, len - idx3)
                End While
                If client_found = 2 Then
                    outfile_asset = outfile_asset & outline & "|" & notes2 & notes3 & vbNewLine
                ElseIf client_found = 1 Then
                    outfile_first = outfile_first & outline & "|" & notes2 & notes3 & vbNewLine
                ElseIf client_found = 3 Then
                    outfile_zug = outfile_zug & outline & "|" & notes2 & notes3 & vbNewLine
                End If
            End If
        Next


        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        'write out files in batches of 750
        Dim limit As Integer = 750
        filename = filename_prefix & "_asset_preprocess"
        Dim outfile As String = outfile_asset
        write_file(limit, filename, outfile)

        filename = filename_prefix & "_first_preprocess"
        outfile = outfile_first
        write_file(limit, filename, outfile)

        filename = filename_prefix & "_zug_preprocess"
        outfile = outfile_zug
        write_file(limit, filename, outfile)

        filename = filename_prefix & "_asset_sb_preprocess"
        outfile = outfile_sb_asset
        write_file(limit, filename, outfile)

        filename = filename_prefix & "_first_sb_preprocess"
        outfile = outfile_sb_first
        write_file(limit, filename, outfile)

        filename = filename_prefix & "_zug_sb_preprocess"
        outfile = outfile_sb_zug
        write_file(limit, filename, outfile)
        'My.Computer.FileSystem.WriteAllText(filename_prefix & "_asset_preprocess.txt", outfile_asset, False, System.Text.Encoding.ASCII)
        'My.Computer.FileSystem.WriteAllText(filename_prefix & "_first_preprocess.txt", outfile_first, False, System.Text.Encoding.ASCII)
        'My.Computer.FileSystem.WriteAllText(filename_prefix & "_zug_preprocess.txt", outfile_zug, False, System.Text.Encoding.ASCII)
        'My.Computer.FileSystem.WriteAllText(filename_prefix & "_asset_sb_preprocess.txt", outfile_sb_asset, False, System.Text.Encoding.ASCII)
        'My.Computer.FileSystem.WriteAllText(filename_prefix & "_first_sb_preprocess.txt", outfile_sb_first, False, System.Text.Encoding.ASCII)
        'My.Computer.FileSystem.WriteAllText(filename_prefix & "_zug_sb_preprocess.txt", outfile_sb_zug, False, System.Text.Encoding.ASCII)
        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText("error.txt", errorfile, False)
            errbtn.Enabled = True
            enable_buttons()
            message_tbox.Text = "Errors found"
        Else
            message_tbox.Text = "No errors found"
            MsgBox("Files written")
            Me.Close()
        End If
    End Sub
    Private Sub write_file(ByVal limit As Integer, ByVal filename As String, ByVal outfile As String)
        Dim LineNumber As Integer, FileCount As Integer = 1, RowCount As Integer = 0
        Dim OutputFile As String = ""
        Dim FileContents() As String = Split(outfile, vbNewLine)

        If UBound(FileContents) = 0 Then
            Return
        End If
        For Each InputLine As String In FileContents
            OutputFile &= InputLine & vbCrLf
            LineNumber += 1
            RowCount += 1

            If RowCount > limit Then ' Not equal so as not to include the header

                WriteFile(filename & FileCount.ToString & ".txt", OutputFile)
                FileCount += 1

                OutputFile = FileContents(0) & vbCrLf ' Add header again
                RowCount = 1
            End If

        Next InputLine

        WriteFile(filename & FileCount.ToString & ".txt", OutputFile)

    End Sub
    Public Sub WriteFile(ByVal FileName As String, ByVal Content As String)

        Using Writer As IO.StreamWriter = New IO.StreamWriter(FileName, False, System.Text.Encoding.ASCII)
            Writer.Write(Content)
        End Using
    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
    Private Sub process_row_new_format(ByVal idx As Integer)
        Dim idx2 As Integer
        Dim colval As String
        Dim test_date As Date
        Dim test_amt As Decimal
        Dim spaces As Integer = 250
        For idx2 = 1 To finalcol
            Try
                'colval = Trim(vals(idx, idx2))
                colval = Trim(InputLineArray(idx2 - 1))
                'remove any quotes
                Dim letIDX As Integer
                Dim new_colval As String = ""
                For letIDX = 1 To colval.Length
                    If Mid(colval, letIDX, 1) <> Chr(34) Then
                        new_colval &= Mid(colval, letIDX, 1)
                    End If
                Next
                colval = new_colval
                Select Case idx2
                    Case 1
                        If colval = "" Then
                            ignore_row = True
                            Exit For
                        End If

                        notes = "ClientCode:" & colval & ";"
                        notes = notes & Space(spaces - notes.Length)
                        spaces += 250
                    Case 2
                        notes = notes & "ClientName:" & colval & ";"
                        notes = notes & Space(spaces - notes.Length)
                        spaces += 250
                    Case 3
                        client_found = 0
                        If colval = "Aktiv Kapital First Investment Limited" Then
                            client_found = 1
                        ElseIf colval = "Aktiv Kapital Asset Investments Limited" Then
                            client_found = 2
                        ElseIf colval = "Aktiv Kapital Portfolio AS Zug Branch" Then
                            client_found = 3
                        Else
                            errorfile = errorfile & "Invalid Master Client - " & colval & " Line " & idx & vbNewLine
                        End If
                        notes = notes & "MasterClient:" & colval & ";"
                        notes = notes & Space(spaces - notes.Length)
                        spaces += 250
                    Case 4
                        clref = colval

                    Case 5
                        off_number = colval
                    Case 6
                        Try
                            off_court = colval
                        Catch ex As Exception
                            off_court = Nothing
                        End Try
                    Case 7
                        'notes = notes & "DebtType:" & colval & ";"
                        'notes = notes & Space(spaces - notes.Length)
                        'spaces += 250
                        off_location = colval
                    Case 8
                        If colval.Length > 0 Then
                            notes = notes & "DebtType2:" & colval & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        End If
                    Case 9
                        Try
                            debt_amt = colval
                        Catch ex As Exception
                            errorfile = errorfile & "Invalid debt amount " & colval & " on line - " & idx & vbNewLine
                        End Try

                    Case 10
                        notes = notes & "DefaultBal:" & colval & ";"
                        notes = notes & Space(spaces - notes.Length)
                        spaces += 250
                    Case 11
                        If colval.Length > 0 Then
                            Try
                                test_date = colval
                            Catch ex As Exception
                                test_date = Nothing
                            End Try
                        End If
                        If test_date <> Nothing Then
                            notes = notes & "DefaultDate:" & Format(test_date, "dd/MM/yyyy") & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        End If
                    Case 12
                        title = colval
                    Case 13
                        forename = colval
                    Case 14
                        surname = colval
                    Case 15
                        Try
                            dob = colval
                        Catch ex As Exception
                            dob = Nothing
                        End Try
                    Case 16
                        addr1 = colval
                    Case 17
                        addr2 = colval
                    Case 18
                        addr3 = colval
                    Case 19
                        addr4 = colval
                    Case 20
                        pcode = colval
                    Case 21
                        phone = colval
                    Case 22
                        emp_phone = colval
                    Case 23
                        fax = colval
                    Case 24
                        joint_name = ""
                        If colval.Length > 0 Then
                            joint_name = colval
                        End If
                    Case 25
                        If colval.Length > 0 Then
                            joint_name = Trim(joint_name & " " & colval)
                        End If
                    Case 26
                        If colval.Length > 0 Then
                            joint_name = Trim(joint_name & " " & colval)
                        End If
                        If joint_name.Length > 0 Then
                            notes = notes & "JointName:" & joint_name & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        End If
                    Case 27
                        If colval.Length > 0 Then
                            Try
                                test_date = colval
                            Catch ex As Exception
                                test_date = Nothing
                            End Try
                            If test_date <> Nothing Then
                                notes = notes & "JointDOB:" & Format(test_date, "dd/MM/yyyy") & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                        End If
                    Case 28
                        If colval.Length > 0 Then
                            joint_address = colval
                        Else
                            joint_address = ""
                        End If
                    Case 29
                        If colval.Length > 0 Then
                            joint_address = Trim(joint_address & " " & colval)
                        End If
                    Case 30
                        If colval.Length > 0 Then
                            joint_address = Trim(joint_address & " " & colval)
                        End If
                    Case 31
                        If colval.Length > 0 Then
                            joint_address = Trim(joint_address & " " & colval)
                        End If
                    Case 32
                        If colval.Length > 0 Then
                            joint_address = Trim(joint_address & " " & colval)
                        End If
                        If joint_address.Length > 0 Then
                            notes = notes & "JointAddress:" & joint_address & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        End If
                    Case 33
                        If colval.Length > 0 Then
                            notes = notes & "JointTelNo:" & colval & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        End If
                    Case 34
                        If colval.Length > 0 Then
                            notes = notes & "JointEmpTelNo:" & colval & ";"
                        End If
                    Case 35
                        If colval.Length > 0 Then
                            notes = notes & "JointOtherTelNo:" & colval & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        End If
                    Case 36
                        Try
                            test_date = colval
                        Catch ex As Exception
                            test_date = Nothing
                        End Try
                        If test_date <> Nothing Then
                            notes = notes & "LastPayDate:" & Format(test_date, "dd/MM/yyyy") & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        End If
                    Case 37
                        If colval.Length > 0 Then
                            Try
                                test_amt = colval
                            Catch ex As Exception
                                test_amt = Nothing
                            End Try
                            If test_amt <> Nothing Then
                                notes = notes & "LastPayVal:" & Format(test_amt, "fixed") & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                        End If
                    Case 38
                        If colval.Length > 0 Then
                            Try
                                test_date = colval
                            Catch ex As Exception
                                test_date = Nothing
                            End Try
                            If test_date <> Nothing Then
                                notes = notes & "ContractStartDate:" & Format(test_date, "dd/MM/yyyy") & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                        End If
                    Case 39
                        If colval.Length > 0 Then
                            notes = notes & "CourtCode:" & colval & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        End If
                    Case 40
                        If colval.Length > 0 Then
                            notes = notes & "CaseNumber:" & colval & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        End If
                    Case 41
                        If colval.Length > 0 Then
                            Try
                                test_date = colval
                            Catch ex As Exception
                                test_date = Nothing
                            End Try
                            If test_date <> Nothing Then
                                notes = notes & "JudgementDate:" & Format(test_date, "dd/MM/yyyy") & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                        End If
                    Case 42
                        If colval.Length > 0 Then
                            prev_addr = colval
                        Else
                            prev_addr = ""
                        End If
                    Case 43
                        If colval.Length > 0 Then
                            prev_addr = Trim(prev_addr & " " & colval)
                        End If
                    Case 44
                        If colval.Length > 0 Then
                            prev_addr = Trim(prev_addr & " " & colval)
                        End If
                    Case 45
                        If colval.Length > 0 Then
                            prev_addr = Trim(prev_addr & " " & colval)
                        End If
                    Case 46
                        If colval.Length > 0 Then
                            prev_addr = Trim(prev_addr & " " & colval)
                        End If
                        If prev_addr.Length > 0 Then
                            notes = notes & "PrevAddr:" & prev_addr & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        End If
                    Case 47
                        d_addr1 = colval
                    Case 48
                        d_addr2 = colval
                    Case 49
                        d_addr3 = colval
                    Case 50
                        d_addr4 = colval
                    Case 51
                        d_addr5 = colval
                    Case 52
                        d_pcode = colval
                    Case 53
                        statute_barred = ""
                        If colval.Length > 0 Then
                            statute_barred = colval
                        End If
                        'Case 54
                        '    If colval.Length > 0 Then
                        '        notes = notes & "Score:" & colval & ";"
                        '        notes = notes & Space(spaces - notes.Length)
                        '        spaces += 250
                        '    End If
                End Select
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Next
    End Sub
End Class

Module data_access_module
    Public table_array(,)
    Public no_of_rows, last_rowid As Integer

    Function get_table(ByVal use_rowid As Boolean, ByVal database_name As String, ByVal table_name As String, ByVal no_of_parms As Integer, Optional ByVal columns As String = Nothing, _
                Optional ByVal where_clause As String = Nothing, Optional ByVal parm_array(,) As String = Nothing) As Integer
        Dim idx2 As Integer
        Dim conn As New Odbc.OdbcConnection()
        Dim error_found As Boolean = True
        If Left(columns, 8) <> "01_rowid" And Len(columns) > 0 Then
            MessageBox.Show("Columns clause must start 01_rowid")
            Return 1
        End If
        If database_name = "Fees" Then
            conn.ConnectionString = _
               "Dsn=RossFeeTable;description=FeesSQL Database;uid=sa;app=Microsoft� Visual Studio� 2005;wsid=DELL3703;database=FeesSQL"
        Else
            conn.ConnectionString = _
               "Integrated Security=True;Dsn=DebtRecovery - DDSybase;na=192.10.200.26,14100;db=DebtRecovery;uid=thirdpartyB;password=thirdpartyB;"
        End If
        Try
            Dim select_string As String = "SELECT * from " & table_name & " " & where_clause
            conn.Open()
            Dim adapter As New Odbc.OdbcDataAdapter(select_string, conn)
            Dim dataset As New DataSet()
            Dim idx As Integer
            For idx = 0 To no_of_parms - 1
                Dim data_type As SqlDbType
                If parm_array(idx, 1) = "date" Then
                    Dim parm_date As Date = parm_array(idx, 0)
                    data_type = SqlDbType.Date
                    adapter.SelectCommand.Parameters.AddWithValue(idx, data_type).Value = parm_date
                Else
                    data_type = SqlDbType.Char
                    adapter.SelectCommand.Parameters.AddWithValue(idx, data_type).Value = parm_array(idx, 0)
                End If

            Next

            adapter.Fill(dataset)
            no_of_rows = dataset.Tables(0).Rows.Count
            If no_of_rows = 0 Then
                Return 100
            End If

            If use_rowid = False Then
                last_rowid = no_of_rows
            Else
                last_rowid = dataset.Tables(0).Rows(no_of_rows - 1).Item(0)
            End If

            Dim lastcol As Integer = dataset.Tables(0).Columns.Count

            Dim table As DataTable
            Dim row As DataRow
            Dim col As DataColumn
            idx = 0

            ReDim table_array(last_rowid, lastcol)
            For Each table In dataset.Tables
                error_found = False
                For Each row In table.Rows
                    idx2 = 0
                    If use_rowid = False Then
                        idx += 1
                    Else
                        idx = row.Item(0).ToString
                    End If
                    For Each col In table.Columns
                        Dim col_no As Integer
                        col_no = InStr(1, columns, col.ColumnName)
                        If col_no > 0 Then
                            idx2 = Mid(columns, col_no - 2, 2)
                        Else
                            idx2 += 1
                        End If
                        If col_no > 0 Or columns = Nothing Then
                            table_array(idx, idx2) = row.Item(col).ToString
                        End If

                    Next
                Next
            Next

            Return 0
        Catch ex As Exception
            If error_found Then
                MessageBox.Show(ex.Message)
                Return 1
            Else
                If idx2 = 0 Then
                    Return 100
                Else

                End If
            End If
        Finally
            conn.Close()
        End Try

    End Function
End Module

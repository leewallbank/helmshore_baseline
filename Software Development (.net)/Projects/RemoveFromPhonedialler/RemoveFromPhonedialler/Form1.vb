Public Class Form1

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'get notes in last couple of hours
        Dim database As String = "DebtRecovery"
        Dim columns As String = "01_rowid 02debtorID 03type 04text"
        Dim start_date As Date = DateAdd(DateInterval.Hour, -2, Now)

        Dim where As String = "where _createdDate > ?" _
        & " and (type ='Trace' or type = 'Arrangement'" _
        & " or type = 'Cancelled' or type = 'Hold' or type = 'Removed bailiff' " _
        & " or type = 'Note')  "

        Dim parm_array(1, 1) As String
        parm_array(0, 0) = start_date
        parm_array(0, 1) = "date"

        get_table(False, database, "Note", 1, columns, where, parm_array)
        Dim note_table(no_of_rows, 4)
        note_table = table_array
        Dim idx As Integer

        'get debtor record
        columns = "01_rowid, 02add_phone"
        Dim file As String = Nothing
        Dim note_rows As Integer = no_of_rows
        For idx = 1 To note_rows
            'if type is note - only include bailiff allocation
            If note_table(idx, 3) = "Note" Then
                If Microsoft.VisualBasic.Left(note_table(idx, 4), 9) <> "Bailiff (" Then
                    Continue For
                End If
            End If
            where = " where _rowid = " & note_table(idx, 2)
            get_table(False, database, "Debtor", 0, columns, where)
            If table_array(1, 2) = Nothing Then
                Continue For  'no phone number
            End If
            Dim ph_number As String = Trim(table_array(1, 2))
            ph_number = Replace(ph_number, " ", "")
            file = file & ph_number & "," & table_array(1, 1) & vbNewLine
        Next
        'now get recent payments
        columns = "01_rowid, 02debtorID"
        where = " where _createdDate > ?"
        get_table(False, database, "Payment", 1, columns, where, parm_array)

        Dim payment_table(no_of_rows, 2)
        payment_table = table_array

        'get debtor record
        Dim payment_rows As Integer = no_of_rows
        columns = "01_rowid, 02add_phone 03status"
        For idx = 1 To payment_rows
            where = " where _rowid = " & payment_table(idx, 2)
            get_table(False, database, "Debtor", 0, columns, where)
            If table_array(1, 2) = Nothing Then
                Continue For  'no phone number
            End If
            If table_array(1, 3) <> "F" Then
                Continue For  'not fully paid
            End If
            Dim ph_number As String = Trim(table_array(1, 2))
            ph_number = Replace(ph_number, " ", "")
            file = file & ph_number & "," & table_array(1, 1) & vbNewLine
        Next

        Dim filename As String = "\\192.10.200.230\remove\removetest.txt"
        My.Computer.FileSystem.WriteAllText(filename, file, False)
        'write log
        filename = "\\192.10.200.230\remove\removelog.txt"
        file = "remove file run at " & Now & vbNewLine
        My.Computer.FileSystem.WriteAllText(filename, file, True)

        Me.Close()




    End Sub
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class addfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.quitbtn = New System.Windows.Forms.Button
        Me.updbtn = New System.Windows.Forms.Button
        Me.name_tbox = New System.Windows.Forms.TextBox
        Me.debtor_tbox = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.transfer_from_lbl = New System.Windows.Forms.Label
        Me.recvd_dtp = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.phone_tbox = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.reason_cbox = New System.Windows.Forms.ComboBox
        Me.onestep_name_tbox = New System.Windows.Forms.TextBox
        Me.notes_tbox = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.namebtn = New System.Windows.Forms.Button
        Me.client_lbl = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'quitbtn
        '
        Me.quitbtn.Location = New System.Drawing.Point(29, 293)
        Me.quitbtn.Name = "quitbtn"
        Me.quitbtn.Size = New System.Drawing.Size(135, 23)
        Me.quitbtn.TabIndex = 7
        Me.quitbtn.Text = "Exit without Saving"
        Me.quitbtn.UseVisualStyleBackColor = True
        '
        'updbtn
        '
        Me.updbtn.Location = New System.Drawing.Point(813, 293)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(100, 23)
        Me.updbtn.TabIndex = 8
        Me.updbtn.Text = "Save and Exit"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'name_tbox
        '
        Me.name_tbox.Location = New System.Drawing.Point(463, 72)
        Me.name_tbox.Name = "name_tbox"
        Me.name_tbox.Size = New System.Drawing.Size(152, 20)
        Me.name_tbox.TabIndex = 3
        '
        'debtor_tbox
        '
        Me.debtor_tbox.Location = New System.Drawing.Point(335, 72)
        Me.debtor_tbox.Name = "debtor_tbox"
        Me.debtor_tbox.Size = New System.Drawing.Size(100, 20)
        Me.debtor_tbox.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(350, 56)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "DebtorID:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(496, 56)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Debtor Name:"
        '
        'transfer_from_lbl
        '
        Me.transfer_from_lbl.AutoSize = True
        Me.transfer_from_lbl.Location = New System.Drawing.Point(185, 9)
        Me.transfer_from_lbl.Name = "transfer_from_lbl"
        Me.transfer_from_lbl.Size = New System.Drawing.Size(39, 13)
        Me.transfer_from_lbl.TabIndex = 8
        Me.transfer_from_lbl.Text = "Label4"
        '
        'recvd_dtp
        '
        Me.recvd_dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.recvd_dtp.Location = New System.Drawing.Point(29, 73)
        Me.recvd_dtp.Name = "recvd_dtp"
        Me.recvd_dtp.Size = New System.Drawing.Size(152, 20)
        Me.recvd_dtp.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(38, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(143, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Date and Time call received:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(242, 55)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Phone:"
        '
        'phone_tbox
        '
        Me.phone_tbox.Location = New System.Drawing.Point(210, 72)
        Me.phone_tbox.Name = "phone_tbox"
        Me.phone_tbox.Size = New System.Drawing.Size(100, 20)
        Me.phone_tbox.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(639, 55)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(81, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Reason for call:"
        '
        'reason_cbox
        '
        Me.reason_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.reason_cbox.FormattingEnabled = True
        Me.reason_cbox.Items.AddRange(New Object() {"Agent", "Bailiff", "Company", "Fees", "Instalments"})
        Me.reason_cbox.Location = New System.Drawing.Point(642, 71)
        Me.reason_cbox.Name = "reason_cbox"
        Me.reason_cbox.Size = New System.Drawing.Size(121, 21)
        Me.reason_cbox.TabIndex = 4
        '
        'onestep_name_tbox
        '
        Me.onestep_name_tbox.Location = New System.Drawing.Point(463, 124)
        Me.onestep_name_tbox.Name = "onestep_name_tbox"
        Me.onestep_name_tbox.ReadOnly = True
        Me.onestep_name_tbox.Size = New System.Drawing.Size(152, 20)
        Me.onestep_name_tbox.TabIndex = 17
        '
        'notes_tbox
        '
        Me.notes_tbox.Location = New System.Drawing.Point(29, 194)
        Me.notes_tbox.Multiline = True
        Me.notes_tbox.Name = "notes_tbox"
        Me.notes_tbox.Size = New System.Drawing.Size(530, 78)
        Me.notes_tbox.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(230, 178)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 13)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Notes:"
        '
        'namebtn
        '
        Me.namebtn.Location = New System.Drawing.Point(335, 124)
        Me.namebtn.Name = "namebtn"
        Me.namebtn.Size = New System.Drawing.Size(93, 23)
        Me.namebtn.TabIndex = 5
        Me.namebtn.Text = "Onestep name"
        Me.namebtn.UseVisualStyleBackColor = True
        '
        'client_lbl
        '
        Me.client_lbl.AutoSize = True
        Me.client_lbl.Location = New System.Drawing.Point(460, 18)
        Me.client_lbl.Name = "client_lbl"
        Me.client_lbl.Size = New System.Drawing.Size(39, 13)
        Me.client_lbl.TabIndex = 20
        Me.client_lbl.Text = "Label7"
        '
        'addfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(939, 366)
        Me.Controls.Add(Me.client_lbl)
        Me.Controls.Add(Me.namebtn)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.notes_tbox)
        Me.Controls.Add(Me.onestep_name_tbox)
        Me.Controls.Add(Me.reason_cbox)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.phone_tbox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.recvd_dtp)
        Me.Controls.Add(Me.transfer_from_lbl)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.debtor_tbox)
        Me.Controls.Add(Me.name_tbox)
        Me.Controls.Add(Me.updbtn)
        Me.Controls.Add(Me.quitbtn)
        Me.Name = "addfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add Call"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents quitbtn As System.Windows.Forms.Button
    Friend WithEvents updbtn As System.Windows.Forms.Button
    Friend WithEvents name_tbox As System.Windows.Forms.TextBox
    Friend WithEvents debtor_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents transfer_from_lbl As System.Windows.Forms.Label
    Friend WithEvents recvd_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents phone_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents reason_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents onestep_name_tbox As System.Windows.Forms.TextBox
    Friend WithEvents notes_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents namebtn As System.Windows.Forms.Button
    Friend WithEvents client_lbl As System.Windows.Forms.Label
End Class

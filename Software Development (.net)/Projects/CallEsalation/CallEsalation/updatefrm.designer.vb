<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class updatefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.quitbtn = New System.Windows.Forms.Button
        Me.updbtn = New System.Windows.Forms.Button
        Me.delbtn = New System.Windows.Forms.Button
        Me.upd_reason_cbox = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.upd_phone_tbox = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.upd_recvd_dtp = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.upd_debtor_tbox = New System.Windows.Forms.TextBox
        Me.upd_name_tbox = New System.Windows.Forms.TextBox
        Me.upd_agent_tbox = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.upd_notes_tbox = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.complete_btn = New System.Windows.Forms.Button
        Me.upd_comp_by_tbox = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.upd_comp_date_tbox = New System.Windows.Forms.TextBox
        Me.resolution_tbox = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.justified_cbox = New System.Windows.Forms.ComboBox
        Me.upd_onestep_name_tbox = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.feedback_tbox = New System.Windows.Forms.TextBox
        Me.call_id_lbl = New System.Windows.Forms.Label
        Me.checkbtn = New System.Windows.Forms.Button
        Me.checked_date_tbox = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.checked_by_tbox = New System.Windows.Forms.TextBox
        Me.upd_namebtn = New System.Windows.Forms.Button
        Me.upd_client_lbl = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'quitbtn
        '
        Me.quitbtn.Location = New System.Drawing.Point(61, 410)
        Me.quitbtn.Name = "quitbtn"
        Me.quitbtn.Size = New System.Drawing.Size(152, 23)
        Me.quitbtn.TabIndex = 13
        Me.quitbtn.Text = "Exit without change"
        Me.quitbtn.UseVisualStyleBackColor = True
        '
        'updbtn
        '
        Me.updbtn.Location = New System.Drawing.Point(742, 410)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(127, 23)
        Me.updbtn.TabIndex = 14
        Me.updbtn.Text = "Update and Exit"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'delbtn
        '
        Me.delbtn.Location = New System.Drawing.Point(351, 20)
        Me.delbtn.Name = "delbtn"
        Me.delbtn.Size = New System.Drawing.Size(142, 23)
        Me.delbtn.TabIndex = 12
        Me.delbtn.Text = "Delete this entry"
        Me.delbtn.UseVisualStyleBackColor = True
        '
        'upd_reason_cbox
        '
        Me.upd_reason_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.upd_reason_cbox.FormattingEnabled = True
        Me.upd_reason_cbox.Items.AddRange(New Object() {"Agent", "Bailiff", "Company", "Fees", "Instalments"})
        Me.upd_reason_cbox.Location = New System.Drawing.Point(742, 86)
        Me.upd_reason_cbox.Name = "upd_reason_cbox"
        Me.upd_reason_cbox.Size = New System.Drawing.Size(121, 21)
        Me.upd_reason_cbox.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(739, 68)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(81, 13)
        Me.Label5.TabIndex = 24
        Me.Label5.Text = "Reason for call:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(351, 68)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Phone:"
        '
        'upd_phone_tbox
        '
        Me.upd_phone_tbox.Location = New System.Drawing.Point(317, 86)
        Me.upd_phone_tbox.Name = "upd_phone_tbox"
        Me.upd_phone_tbox.Size = New System.Drawing.Size(100, 20)
        Me.upd_phone_tbox.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(136, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(143, 13)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Date and Time call received:"
        '
        'upd_recvd_dtp
        '
        Me.upd_recvd_dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.upd_recvd_dtp.Location = New System.Drawing.Point(139, 86)
        Me.upd_recvd_dtp.Name = "upd_recvd_dtp"
        Me.upd_recvd_dtp.Size = New System.Drawing.Size(152, 20)
        Me.upd_recvd_dtp.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(614, 69)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(73, 13)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Debtor Name:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(458, 69)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(53, 13)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "DebtorID:"
        '
        'upd_debtor_tbox
        '
        Me.upd_debtor_tbox.Location = New System.Drawing.Point(437, 85)
        Me.upd_debtor_tbox.Name = "upd_debtor_tbox"
        Me.upd_debtor_tbox.Size = New System.Drawing.Size(100, 20)
        Me.upd_debtor_tbox.TabIndex = 3
        '
        'upd_name_tbox
        '
        Me.upd_name_tbox.Location = New System.Drawing.Point(571, 85)
        Me.upd_name_tbox.Name = "upd_name_tbox"
        Me.upd_name_tbox.Size = New System.Drawing.Size(152, 20)
        Me.upd_name_tbox.TabIndex = 4
        '
        'upd_agent_tbox
        '
        Me.upd_agent_tbox.Location = New System.Drawing.Point(12, 85)
        Me.upd_agent_tbox.Name = "upd_agent_tbox"
        Me.upd_agent_tbox.ReadOnly = True
        Me.upd_agent_tbox.Size = New System.Drawing.Size(100, 20)
        Me.upd_agent_tbox.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(46, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "Agent:"
        '
        'upd_notes_tbox
        '
        Me.upd_notes_tbox.Location = New System.Drawing.Point(12, 165)
        Me.upd_notes_tbox.Multiline = True
        Me.upd_notes_tbox.Name = "upd_notes_tbox"
        Me.upd_notes_tbox.Size = New System.Drawing.Size(331, 86)
        Me.upd_notes_tbox.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(136, 149)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 29
        Me.Label3.Text = "Notes:"
        '
        'complete_btn
        '
        Me.complete_btn.Location = New System.Drawing.Point(461, 163)
        Me.complete_btn.Name = "complete_btn"
        Me.complete_btn.Size = New System.Drawing.Size(100, 23)
        Me.complete_btn.TabIndex = 8
        Me.complete_btn.Text = "Complete Call"
        Me.complete_btn.UseVisualStyleBackColor = True
        '
        'upd_comp_by_tbox
        '
        Me.upd_comp_by_tbox.Location = New System.Drawing.Point(393, 214)
        Me.upd_comp_by_tbox.Name = "upd_comp_by_tbox"
        Me.upd_comp_by_tbox.ReadOnly = True
        Me.upd_comp_by_tbox.Size = New System.Drawing.Size(100, 20)
        Me.upd_comp_by_tbox.TabIndex = 31
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(403, 198)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(75, 13)
        Me.Label8.TabIndex = 32
        Me.Label8.Text = "Completed By:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(543, 198)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(86, 13)
        Me.Label9.TabIndex = 33
        Me.Label9.Text = "Completed Date:"
        '
        'upd_comp_date_tbox
        '
        Me.upd_comp_date_tbox.Location = New System.Drawing.Point(529, 214)
        Me.upd_comp_date_tbox.Name = "upd_comp_date_tbox"
        Me.upd_comp_date_tbox.ReadOnly = True
        Me.upd_comp_date_tbox.Size = New System.Drawing.Size(100, 20)
        Me.upd_comp_date_tbox.TabIndex = 34
        '
        'resolution_tbox
        '
        Me.resolution_tbox.Location = New System.Drawing.Point(378, 283)
        Me.resolution_tbox.Multiline = True
        Me.resolution_tbox.Name = "resolution_tbox"
        Me.resolution_tbox.ReadOnly = True
        Me.resolution_tbox.Size = New System.Drawing.Size(251, 77)
        Me.resolution_tbox.TabIndex = 11
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(403, 266)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(60, 13)
        Me.Label10.TabIndex = 36
        Me.Label10.Text = "Resolution:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(675, 197)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(48, 13)
        Me.Label11.TabIndex = 38
        Me.Label11.Text = "Justified:"
        '
        'justified_cbox
        '
        Me.justified_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.justified_cbox.Enabled = False
        Me.justified_cbox.FormattingEnabled = True
        Me.justified_cbox.Items.AddRange(New Object() {"Yes", "No"})
        Me.justified_cbox.Location = New System.Drawing.Point(661, 213)
        Me.justified_cbox.Name = "justified_cbox"
        Me.justified_cbox.Size = New System.Drawing.Size(85, 21)
        Me.justified_cbox.TabIndex = 9
        '
        'upd_onestep_name_tbox
        '
        Me.upd_onestep_name_tbox.Location = New System.Drawing.Point(567, 118)
        Me.upd_onestep_name_tbox.Name = "upd_onestep_name_tbox"
        Me.upd_onestep_name_tbox.ReadOnly = True
        Me.upd_onestep_name_tbox.Size = New System.Drawing.Size(152, 20)
        Me.upd_onestep_name_tbox.TabIndex = 40
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(739, 266)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(58, 13)
        Me.Label12.TabIndex = 42
        Me.Label12.Text = "Feedback:"
        '
        'feedback_tbox
        '
        Me.feedback_tbox.Location = New System.Drawing.Point(678, 283)
        Me.feedback_tbox.Multiline = True
        Me.feedback_tbox.Name = "feedback_tbox"
        Me.feedback_tbox.ReadOnly = True
        Me.feedback_tbox.Size = New System.Drawing.Size(198, 68)
        Me.feedback_tbox.TabIndex = 12
        '
        'call_id_lbl
        '
        Me.call_id_lbl.AutoSize = True
        Me.call_id_lbl.Location = New System.Drawing.Point(12, 20)
        Me.call_id_lbl.Name = "call_id_lbl"
        Me.call_id_lbl.Size = New System.Drawing.Size(37, 13)
        Me.call_id_lbl.TabIndex = 43
        Me.call_id_lbl.Text = "call ID"
        '
        'checkbtn
        '
        Me.checkbtn.Location = New System.Drawing.Point(88, 280)
        Me.checkbtn.Name = "checkbtn"
        Me.checkbtn.Size = New System.Drawing.Size(100, 23)
        Me.checkbtn.TabIndex = 10
        Me.checkbtn.Text = "Check Call"
        Me.checkbtn.UseVisualStyleBackColor = True
        '
        'checked_date_tbox
        '
        Me.checked_date_tbox.Location = New System.Drawing.Point(172, 340)
        Me.checked_date_tbox.Name = "checked_date_tbox"
        Me.checked_date_tbox.ReadOnly = True
        Me.checked_date_tbox.Size = New System.Drawing.Size(100, 20)
        Me.checked_date_tbox.TabIndex = 12
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(186, 324)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(79, 13)
        Me.Label13.TabIndex = 47
        Me.Label13.Text = "Checked Date:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(46, 324)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(68, 13)
        Me.Label14.TabIndex = 46
        Me.Label14.Text = "Checked By:"
        '
        'checked_by_tbox
        '
        Me.checked_by_tbox.Location = New System.Drawing.Point(36, 340)
        Me.checked_by_tbox.Name = "checked_by_tbox"
        Me.checked_by_tbox.ReadOnly = True
        Me.checked_by_tbox.Size = New System.Drawing.Size(100, 20)
        Me.checked_by_tbox.TabIndex = 11
        '
        'upd_namebtn
        '
        Me.upd_namebtn.Location = New System.Drawing.Point(437, 118)
        Me.upd_namebtn.Name = "upd_namebtn"
        Me.upd_namebtn.Size = New System.Drawing.Size(100, 23)
        Me.upd_namebtn.TabIndex = 6
        Me.upd_namebtn.Text = "Onestep name"
        Me.upd_namebtn.UseVisualStyleBackColor = True
        '
        'upd_client_lbl
        '
        Me.upd_client_lbl.AutoSize = True
        Me.upd_client_lbl.Location = New System.Drawing.Point(568, 30)
        Me.upd_client_lbl.Name = "upd_client_lbl"
        Me.upd_client_lbl.Size = New System.Drawing.Size(32, 13)
        Me.upd_client_lbl.TabIndex = 48
        Me.upd_client_lbl.Text = "client"
        '
        'updatefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(888, 465)
        Me.Controls.Add(Me.upd_client_lbl)
        Me.Controls.Add(Me.upd_namebtn)
        Me.Controls.Add(Me.checked_date_tbox)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.checked_by_tbox)
        Me.Controls.Add(Me.checkbtn)
        Me.Controls.Add(Me.call_id_lbl)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.feedback_tbox)
        Me.Controls.Add(Me.upd_onestep_name_tbox)
        Me.Controls.Add(Me.justified_cbox)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.resolution_tbox)
        Me.Controls.Add(Me.upd_comp_date_tbox)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.upd_comp_by_tbox)
        Me.Controls.Add(Me.complete_btn)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.upd_notes_tbox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.upd_agent_tbox)
        Me.Controls.Add(Me.upd_reason_cbox)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.upd_phone_tbox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.upd_recvd_dtp)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.upd_debtor_tbox)
        Me.Controls.Add(Me.upd_name_tbox)
        Me.Controls.Add(Me.delbtn)
        Me.Controls.Add(Me.updbtn)
        Me.Controls.Add(Me.quitbtn)
        Me.Name = "updatefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Update/Delete calls"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents quitbtn As System.Windows.Forms.Button
    Friend WithEvents updbtn As System.Windows.Forms.Button
    Friend WithEvents delbtn As System.Windows.Forms.Button
    Friend WithEvents upd_reason_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents upd_phone_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents upd_recvd_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents upd_debtor_tbox As System.Windows.Forms.TextBox
    Friend WithEvents upd_name_tbox As System.Windows.Forms.TextBox
    Friend WithEvents upd_agent_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents upd_notes_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents complete_btn As System.Windows.Forms.Button
    Friend WithEvents upd_comp_by_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents upd_comp_date_tbox As System.Windows.Forms.TextBox
    Friend WithEvents resolution_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents justified_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents upd_onestep_name_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents feedback_tbox As System.Windows.Forms.TextBox
    Friend WithEvents call_id_lbl As System.Windows.Forms.Label
    Friend WithEvents checkbtn As System.Windows.Forms.Button
    Friend WithEvents checked_date_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents checked_by_tbox As System.Windows.Forms.TextBox
    Friend WithEvents upd_namebtn As System.Windows.Forms.Button
    Friend WithEvents upd_client_lbl As System.Windows.Forms.Label
End Class

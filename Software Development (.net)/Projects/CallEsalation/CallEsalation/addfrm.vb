Public Class addfrm

    Private Sub addfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        client_lbl.Text = ""
        namebtn.Enabled = False
        recvd_dtp.Focus()
        transfer_from_lbl.Text = "Calls taken by  " & log_user
        recvd_dtp.CustomFormat = "dd MM yyyy HH:mm"
        name_tbox.Text = ""
        debtor_tbox.Text = ""
        param2 = "select login_name from Bailiff where agent_type = 'P'" & _
        " and status = 'O' and name_fore is not null order by login_name"
        Dim bail_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to read agents from onestep")
            Exit Sub
        End If
        
    End Sub

    Private Sub quitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitbtn.Click
        Me.Close()
    End Sub

    Private Sub updbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updbtn.Click
        If recvd_dtp.Value > Now Then
            MsgBox("Time can't be in the future")
            Exit Sub
        End If
        If recvd_dtp.Value < DateAdd(DateInterval.Day, -1, Now) Then
            MsgBox("Date can't be before yesterday")
            Exit Sub
        End If
        If phone_tbox.Text.Length < 5 Then
            MsgBox("Invalid phone number entered")
            Exit Sub
        End If

        Dim debtor As Integer
        Try
            debtor = debtor_tbox.Text
        Catch ex As Exception
            MsgBox("debtorID should be a valid case number on onestep")
            Exit Sub
        End Try
        If name_tbox.Text.Length < 5 Then
            MsgBox("Debtor name should be entered")
            Exit Sub
        End If
        If reason_cbox.SelectedIndex < 0 Then
            MsgBox("reason for call should be entered")
            Exit Sub
        End If
        param2 = "select _rowid from Debtor where _rowid = " & debtor
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("DebtorID does not exist on onestep")
            Exit Sub
        End If

        Try
            Mainfrm.CallEscalationTableAdapter.InsertQuery(log_user, Now, recvd_dtp.Value, debtor_tbox.Text, log_user, name_tbox.Text, phone_tbox.Text, reason_cbox.Text, "", null_date, "", "", null_date, "", notes_tbox.Text, "", 0)
            populate_call_grid()
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        Me.Close()
    End Sub

    Private Sub debtor_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles debtor_tbox.TextChanged

    End Sub

    Private Sub debtor_tbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles debtor_tbox.Validated
        Dim debtor As Integer
        Try
            debtor = debtor_tbox.Text
        Catch ex As Exception
            MsgBox("debtorID should be a valid case number on onestep")
            Exit Sub
        End Try
        param2 = "select _rowid, name_fore, name_sur, name2, clientSchemeID from Debtor where _rowid = " & debtor
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("DebtorID does not exist on onestep")
            Exit Sub
        End If
        Dim onestep_name As String
        Try
            onestep_name = debtor_dataset.Tables(0).Rows(0).Item(1)
        Catch ex As Exception
            onestep_name = ""
        End Try
        Try
            onestep_name = Trim(onestep_name & " " & debtor_dataset.Tables(0).Rows(0).Item(2))
        Catch ex As Exception
            
        End Try
        client_lbl.Text = get_cl_name(debtor_dataset.Tables(0).Rows(0).Item(4))
        onestep_name_tbox.Text = onestep_name
        namebtn.Enabled = True
    End Sub
   

    Private Sub name_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles name_tbox.TextChanged

    End Sub

    Private Sub namebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles namebtn.Click
        name_tbox.Text = onestep_name_tbox.Text
    End Sub
End Class
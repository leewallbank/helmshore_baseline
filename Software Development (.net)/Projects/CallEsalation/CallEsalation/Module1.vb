Module Module1
    Public log_user, selected_call_id, selected_transfer_to, mode As String
    Public conn_open, complete_call, check_call As Boolean
    Public upd_row As Integer
    Public null_date As Date = CDate("1900 1 1")
    Public Sub populate_call_grid()
        If mode = "O" Then
            Mainfrm.CallEscalationTableAdapter.FillBy(Mainfrm.FeesSQLDataSet1.CallEscalation, Format(null_date, "yyyy MM dd"), "0")
        ElseIf mode = "U" Then
            Mainfrm.CallEscalationTableAdapter.FillBy1(Mainfrm.FeesSQLDataSet1.CallEscalation, Format(null_date, "yyyy MM dd"), "0")
        Else
            Mainfrm.CallEscalationTableAdapter.FillBy2(Mainfrm.FeesSQLDataSet1.CallEscalation, "0")
        End If
        Mainfrm.call_dg.Rows.Clear()
        Dim idx As Integer
        For idx = 0 To Mainfrm.FeesSQLDataSet1.CallEscalation.Rows.Count - 1
            Dim call_id As Integer = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(idx).Item(0)
            Dim recvd_date As Date = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(idx).Item(18)
            Dim debtorID As Integer = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(idx).Item(3)
            Dim agent As String = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(idx).Item(4)
            Dim name As String = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(idx).Item(5)
            Dim phone As String = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(idx).Item(6)
            Dim reason As String = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(idx).Item(7)
            Dim notes As String = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(idx).Item(14)
            Dim notes2 As String = ""
            'replace linefeed by space
            Dim idx2 As Integer
            For idx2 = 1 To notes.Length
                If Mid(notes, idx2, 1) = Chr(10) Or Mid(notes, idx2, 1) = Chr(13) Then
                    notes2 = notes2 & " "
                Else
                    notes2 = notes2 & Mid(notes, idx2, 1)
                End If
            Next
            Mainfrm.call_dg.Rows.Add(call_id, Format(recvd_date, "dd/MM/yyyy HH:mm"), debtorID, agent, name, phone, reason, notes2)
        Next
    End Sub
    Public Function get_cl_name(ByVal csID As Integer) As String

        param2 = "select clientID from clientScheme where _rowid = " & csID
        Dim cs_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to read client scheme for cs_id = " & csID)
            Return ("")
        End If
        param2 = "select name from client where _rowid = " & cs_dataset.Tables(0).Rows(0).Item(0)
        Dim cl_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to read client table")
            Return ("")
        End If
        Return (cl_dataset.Tables(0).Rows(0).Item(0))

    End Function
End Module

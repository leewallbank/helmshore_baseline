<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.exitbtn = New System.Windows.Forms.Button
        Me.call_dg = New System.Windows.Forms.DataGridView
        Me.addbtn = New System.Windows.Forms.Button
        Me.log_lbl = New System.Windows.Forms.Label
        Me.disp_lbl = New System.Windows.Forms.Label
        Me.excelbtn = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.openbtn = New System.Windows.Forms.Button
        Me.uncheckedbtn = New System.Windows.Forms.Button
        Me.allbtn = New System.Windows.Forms.Button
        Me.FeesSQLDataSet1 = New CallEsalation.FeesSQLDataSet
        Me.CallEscalationTableAdapter = New CallEsalation.FeesSQLDataSetTableAdapters.CallEscalationTableAdapter
        Me.esc_id = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.esc_recvd_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.esc_debtorID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.esc_agent = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.esc_debtor_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.esc_phone = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.esc_reason = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.esc_notes = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.call_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(814, 440)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 5
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'call_dg
        '
        Me.call_dg.AllowUserToAddRows = False
        Me.call_dg.AllowUserToDeleteRows = False
        Me.call_dg.AllowUserToOrderColumns = True
        Me.call_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.call_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.esc_id, Me.esc_recvd_date, Me.esc_debtorID, Me.esc_agent, Me.esc_debtor_name, Me.esc_phone, Me.esc_reason, Me.esc_notes})
        Me.call_dg.Location = New System.Drawing.Point(22, 98)
        Me.call_dg.Name = "call_dg"
        Me.call_dg.ReadOnly = True
        Me.call_dg.Size = New System.Drawing.Size(915, 336)
        Me.call_dg.TabIndex = 3
        '
        'addbtn
        '
        Me.addbtn.Location = New System.Drawing.Point(188, 31)
        Me.addbtn.Name = "addbtn"
        Me.addbtn.Size = New System.Drawing.Size(113, 23)
        Me.addbtn.TabIndex = 0
        Me.addbtn.Text = "Add new entry"
        Me.addbtn.UseVisualStyleBackColor = True
        '
        'log_lbl
        '
        Me.log_lbl.AutoSize = True
        Me.log_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.log_lbl.Location = New System.Drawing.Point(66, 41)
        Me.log_lbl.Name = "log_lbl"
        Me.log_lbl.Size = New System.Drawing.Size(24, 13)
        Me.log_lbl.TabIndex = 7
        Me.log_lbl.Text = "log"
        '
        'disp_lbl
        '
        Me.disp_lbl.AutoSize = True
        Me.disp_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.disp_lbl.Location = New System.Drawing.Point(147, 82)
        Me.disp_lbl.Name = "disp_lbl"
        Me.disp_lbl.Size = New System.Drawing.Size(45, 13)
        Me.disp_lbl.TabIndex = 8
        Me.disp_lbl.Text = "Label1"
        '
        'excelbtn
        '
        Me.excelbtn.Location = New System.Drawing.Point(776, 32)
        Me.excelbtn.Name = "excelbtn"
        Me.excelbtn.Size = New System.Drawing.Size(89, 21)
        Me.excelbtn.TabIndex = 4
        Me.excelbtn.Text = "Write  to excel"
        Me.excelbtn.UseVisualStyleBackColor = True
        '
        'openbtn
        '
        Me.openbtn.Location = New System.Drawing.Point(345, 31)
        Me.openbtn.Name = "openbtn"
        Me.openbtn.Size = New System.Drawing.Size(75, 23)
        Me.openbtn.TabIndex = 1
        Me.openbtn.Text = "Open Calls"
        Me.openbtn.UseVisualStyleBackColor = True
        '
        'uncheckedbtn
        '
        Me.uncheckedbtn.Location = New System.Drawing.Point(448, 31)
        Me.uncheckedbtn.Name = "uncheckedbtn"
        Me.uncheckedbtn.Size = New System.Drawing.Size(117, 23)
        Me.uncheckedbtn.TabIndex = 2
        Me.uncheckedbtn.Text = "Unchecked Calls"
        Me.uncheckedbtn.UseVisualStyleBackColor = True
        '
        'allbtn
        '
        Me.allbtn.Location = New System.Drawing.Point(607, 31)
        Me.allbtn.Name = "allbtn"
        Me.allbtn.Size = New System.Drawing.Size(75, 23)
        Me.allbtn.TabIndex = 3
        Me.allbtn.Text = "All calls"
        Me.allbtn.UseVisualStyleBackColor = True
        '
        'FeesSQLDataSet1
        '
        Me.FeesSQLDataSet1.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'CallEscalationTableAdapter
        '
        Me.CallEscalationTableAdapter.ClearBeforeFill = True
        '
        'esc_id
        '
        Me.esc_id.HeaderText = "Call Id"
        Me.esc_id.Name = "esc_id"
        Me.esc_id.ReadOnly = True
        Me.esc_id.Width = 50
        '
        'esc_recvd_date
        '
        Me.esc_recvd_date.HeaderText = "Received DateTime"
        Me.esc_recvd_date.Name = "esc_recvd_date"
        Me.esc_recvd_date.ReadOnly = True
        Me.esc_recvd_date.Width = 120
        '
        'esc_debtorID
        '
        Me.esc_debtorID.HeaderText = "DebtorID"
        Me.esc_debtorID.Name = "esc_debtorID"
        Me.esc_debtorID.ReadOnly = True
        '
        'esc_agent
        '
        Me.esc_agent.HeaderText = "Agent"
        Me.esc_agent.Name = "esc_agent"
        Me.esc_agent.ReadOnly = True
        '
        'esc_debtor_name
        '
        Me.esc_debtor_name.HeaderText = "Debtor Name"
        Me.esc_debtor_name.Name = "esc_debtor_name"
        Me.esc_debtor_name.ReadOnly = True
        '
        'esc_phone
        '
        Me.esc_phone.HeaderText = "Phone No"
        Me.esc_phone.Name = "esc_phone"
        Me.esc_phone.ReadOnly = True
        '
        'esc_reason
        '
        Me.esc_reason.HeaderText = "Reason"
        Me.esc_reason.Name = "esc_reason"
        Me.esc_reason.ReadOnly = True
        '
        'esc_notes
        '
        Me.esc_notes.HeaderText = "Notes"
        Me.esc_notes.Name = "esc_notes"
        Me.esc_notes.ReadOnly = True
        Me.esc_notes.Width = 200
        '
        'Mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(949, 475)
        Me.Controls.Add(Me.allbtn)
        Me.Controls.Add(Me.uncheckedbtn)
        Me.Controls.Add(Me.openbtn)
        Me.Controls.Add(Me.excelbtn)
        Me.Controls.Add(Me.disp_lbl)
        Me.Controls.Add(Me.log_lbl)
        Me.Controls.Add(Me.addbtn)
        Me.Controls.Add(Me.call_dg)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "Mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Call Escalation"
        CType(Me.call_dg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents call_dg As System.Windows.Forms.DataGridView
    Friend WithEvents addbtn As System.Windows.Forms.Button
    Friend WithEvents FeesSQLDataSet As CallEsalation.FeesSQLDataSet
    Friend WithEvents log_lbl As System.Windows.Forms.Label
    Friend WithEvents disp_lbl As System.Windows.Forms.Label
    Friend WithEvents excelbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents FeesSQLDataSet1 As CallEsalation.FeesSQLDataSet
    Friend WithEvents CallEscalationTableAdapter As CallEsalation.FeesSQLDataSetTableAdapters.CallEscalationTableAdapter
    Friend WithEvents openbtn As System.Windows.Forms.Button
    Friend WithEvents uncheckedbtn As System.Windows.Forms.Button
    Friend WithEvents allbtn As System.Windows.Forms.Button
    Friend WithEvents esc_id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents esc_recvd_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents esc_debtorID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents esc_agent As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents esc_debtor_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents esc_phone As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents esc_reason As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents esc_notes As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

Public Class Mainfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mode = "O"
        disp_lbl.Text = "All Open calls. Double_click an entry to update/delete"
        log_user = My.User.Name
        Dim slash_idx As Integer = InStr(log_user, "\")
        If slash_idx > 0 Then
            log_user = Microsoft.VisualBasic.Right(log_user, log_user.length - slash_idx)
        End If
        log_lbl.Text = log_user
        populate_call_grid()
    End Sub
    


    Private Sub addbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addbtn.Click
        addfrm.ShowDialog()
    End Sub

    Private Sub pay_dg_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles call_dg.CellContentClick

    End Sub

    Private Sub pay_dg_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles call_dg.CellDoubleClick
        selected_call_id = call_dg.Rows(e.RowIndex).Cells(0).Value
        selected_transfer_to = call_dg.Rows(e.RowIndex).Cells(2).Value
        upd_row = e.RowIndex
        updatefrm.ShowDialog()
    End Sub

    Private Sub excelbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles excelbtn.Click

        Dim idx As Integer = 0
        Dim file As String = "Call Id,Created Date,Created By,Agent,DebtorID, debtor name,Client,Phone,Reason," & _
        "Resolution,Completed Date,Completed By,Checked date, Checked by,Justified,feedback,Notes" & vbNewLine
        Dim row As DataGridViewRow
        For Each row In call_dg.Rows
            Dim call_id As Integer = call_dg.Rows(idx).Cells(0).Value
            CallEscalationTableAdapter.FillBy3(FeesSQLDataSet1.CallEscalation, call_id)
            file = file & call_id & ","
            file = file & call_dg.Rows(idx).Cells(1).Value & ","  'created date
            file = file & Me.FeesSQLDataSet1.CallEscalation.Rows(0).Item(1) & "," 'created by
            file = file & call_dg.Rows(idx).Cells(3).Value & "," 'agent
            file = file & call_dg.Rows(idx).Cells(2).Value & "," 'debtorID
            param2 = "select clientSchemeID from Debtor where _rowid = " & call_dg.Rows(idx).Cells(2).Value
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            Dim cl_name As String
            If no_of_rows = 0 Then
                cl_name = ""
            Else
                cl_name = get_cl_name(debtor_dataset.Tables(0).Rows(0).Item(0))
            End If
            Dim name As String = call_dg.Rows(idx).Cells(4).Value & "," 'debtor name
            name = Replace(name, ",", " ")
            file = file & name & ","
            cl_name = Replace(cl_name, ",", " ")
            file = file & cl_name & ","
            file = file & call_dg.Rows(idx).Cells(5).Value & "," 'phone
            file = file & call_dg.Rows(idx).Cells(6).Value & "," 'reason
            Dim resolution As String = Me.FeesSQLDataSet1.CallEscalation.Rows(0).Item(8)
            resolution = remove_chars(resolution)
            file = file & resolution & ","
            If Me.FeesSQLDataSet1.CallEscalation.Rows(0).Item(10) = "" Then
                file = file & ",,"
            Else
                file = file & Format(Me.FeesSQLDataSet1.CallEscalation.Rows(0).Item(9), "dd/MM/yyyy") & _
                "," & Me.FeesSQLDataSet1.CallEscalation.Rows(0).Item(10) & "," 'comp date comp by
            End If
            If Me.FeesSQLDataSet1.CallEscalation.Rows(0).Item(12) = null_date Then
                file = file & ",,"
            Else
                file = file & Format(Me.FeesSQLDataSet1.CallEscalation.Rows(0).Item(12), "dd/MM/yyyy") & _
                "," & Me.FeesSQLDataSet1.CallEscalation.Rows(0).Item(11) & "," 'checked date checked by
            End If
            file = file & Me.FeesSQLDataSet1.CallEscalation.Rows(0).Item(13) & "," 'justified
            Dim feedback As String = Me.FeesSQLDataSet1.CallEscalation.Rows(0).Item(15) & "," 'feedback
            feedback = remove_chars(feedback)
            file = file & feedback
            file = file & call_dg.Rows(idx).Cells(7).Value & "," & vbNewLine 'notes
            idx += 1
        Next
        If idx = 0 Then
            MessageBox.Show("There are no calls")
            Exit Sub
        End If
        With SaveFileDialog1
            .Title = "Save to excel"
            .Filter = "excel|*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "escalated_calls_" & Format(Now, "dd.MM.yyyy") & ".xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
        End If
    End Sub
    Function remove_chars(ByVal text As String) As String
        Dim idx As Integer
        Dim new_text As String = ""
        For idx = 1 To text.Length
            If Mid(text, idx, 1) = Chr(10) Or Mid(text, idx, 1) = Chr(9) Or _
            Mid(text, idx, 1) = Chr(13) Or Mid(text, idx, 1) = "|" Then
                new_text = new_text & " "
            Else
                new_text = new_text & Mid(text, idx, 1)
            End If
        Next
        Return new_text
    End Function
    Private Sub todaybtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim start_date As Date = Now
        start_date = CDate(Format(start_date, "yyyy MM dd") & " 0:0:0")
        'PaymentSheetTableAdapter.FillBy2(FeesSQLDataSet.PaymentSheet, Format(start_date, "yyyy-MM-dd"))
        Dim idx As Integer
        Dim file As String = "Agent,Tranfer to,Amount,DebtorID" & vbNewLine
        'For idx = 0 To Me.FeesSQLDataSet.PaymentSheet.Rows.Count - 1
        '    file = file & Me.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(3) & ","
        '    file = file & Me.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(4) & ","
        '    file = file & "�" & Format(Me.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(5), "f") & ","
        '    file = file & Me.FeesSQLDataSet.PaymentSheet.Rows(idx).Item(6) & vbNewLine
        'Next
        If idx = 0 Then
            MessageBox.Show("There are no payments")
            Exit Sub
        End If
        With SaveFileDialog1
            .Title = "Save to excel"
            .Filter = "excel|*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "payment_transfers_" & Format(Now, "dd.MM.yyyy") & ".xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
        End If
    End Sub

    
    
    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        mode = "O"
        disp_lbl.Text = "All Open calls. Double_click an entry to update/delete"
        populate_call_grid()
    End Sub

    Private Sub uncheckedbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uncheckedbtn.Click
        disp_lbl.Text = "All Unchecked calls. Double_click an entry to update/delete"
        mode = "U"
        populate_call_grid()
    End Sub

    Private Sub allbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allbtn.Click
        disp_lbl.Text = "All calls. Double_click an entry to update/delete"
        mode = "A"
        populate_call_grid()
    End Sub
End Class
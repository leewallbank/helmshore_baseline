Public Class updatefrm

    Private Sub updatefrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        upd_namebtn.Enabled = False
        check_call = False
        call_id_lbl.Text = "Call ID:" & selected_call_id
        upd_recvd_dtp.CustomFormat = "dd MM yyyy HH:mm"
        upd_recvd_dtp.Text = Mainfrm.call_dg.Rows(upd_row).Cells(1).Value
        upd_agent_tbox.Text = Mainfrm.call_dg.Rows(upd_row).Cells(3).Value
        upd_debtor_tbox.Text = Mainfrm.call_dg.Rows(upd_row).Cells(2).Value
        param2 = "select clientSchemeID from Debtor where _rowid = " & upd_debtor_tbox.Text
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("DebtorID does not exist on onestep")
            Exit Sub
        End If
        upd_client_lbl.Text = get_cl_name(debtor_dataset.Tables(0).Rows(0).Item(0))

        upd_name_tbox.Text = Mainfrm.call_dg.Rows(upd_row).Cells(4).Value
        upd_phone_tbox.Text = Mainfrm.call_dg.Rows(upd_row).Cells(5).Value
        upd_reason_cbox.Text = Mainfrm.call_dg.Rows(upd_row).Cells(6).Value
        upd_notes_tbox.Text = Mainfrm.call_dg.Rows(upd_row).Cells(7).Value
        Mainfrm.CallEscalationTableAdapter.FillBy3(Mainfrm.FeesSQLDataSet1.CallEscalation, selected_call_id)
        Dim completed_by As String = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(0).Item(10)
        Dim checked_date As Date = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(0).Item(12)
        If checked_date = null_date Then
            check_call = False
            checked_by_tbox.Text = ""
            checked_date_tbox.Text = ""
        Else
            check_call = True
            checked_by_tbox.Text = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(0).Item(11)
            checked_date_tbox.Text = Format(Now, "dd/MM/yyyy")
        End If
        If completed_by <> "" Then
            complete_call = True
            upd_recvd_dtp.Enabled = False
            upd_agent_tbox.Enabled = False
            upd_name_tbox.Enabled = False
            upd_debtor_tbox.Enabled = False
            upd_phone_tbox.Enabled = False
            upd_reason_cbox.Enabled = False
            upd_comp_by_tbox.Text = completed_by
            upd_comp_date_tbox.Text = Format(Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(0).Item(9), "dd/MM/yyyy")
            justified_cbox.Text = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(0).Item(13)
            resolution_tbox.Text = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(0).Item(8)
            feedback_tbox.Text = Mainfrm.FeesSQLDataSet1.CallEscalation.Rows(0).Item(15)
            feedback_tbox.ReadOnly = False
            justified_cbox.Enabled = True
            resolution_tbox.ReadOnly = False
        Else
            upd_recvd_dtp.Enabled = True
            upd_agent_tbox.Enabled = True
            upd_name_tbox.Enabled = True
            upd_debtor_tbox.Enabled = True
            upd_phone_tbox.Enabled = True
            upd_reason_cbox.Enabled = True
            complete_call = False
            upd_comp_by_tbox.Text = ""
            upd_comp_date_tbox.Text = ""
            feedback_tbox.ReadOnly = True
            justified_cbox.Enabled = False
            resolution_tbox.ReadOnly = True
        End If

        upd_phone_tbox.Focus()
    End Sub

    Private Sub quitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitbtn.Click
        Me.Close()
    End Sub

    Private Sub updbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updbtn.Click
        Dim debtor As Integer
        Try
            debtor = upd_debtor_tbox.Text
        Catch ex As Exception
            MsgBox("debtorID should be a valid case number on onestep")
            Exit Sub
        End Try
        param2 = "select _rowid from Debtor where _rowid = " & debtor
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("DebtorID does not exist on onestep")
            Exit Sub
        End If
        If upd_name_tbox.Text.Length < 2 Then
            MsgBox("Debtor name should not be blank")
            Exit Sub
        End If
        If complete_call Then
            If Trim(resolution_tbox.Text).Length = 0 Then
                MsgBox("Resolution should be entered")
                resolution_tbox.Focus()
                Exit Sub
            End If
            If justified_cbox.SelectedIndex < 0 Then
                MsgBox("Justified must be set to yes or No")
                justified_cbox.Focus()
                Exit Sub
            End If
        End If
        Try
            Mainfrm.CallEscalationTableAdapter.UpdateQuery(upd_recvd_dtp.Value, upd_debtor_tbox.Text, upd_agent_tbox.Text, upd_name_tbox.Text, upd_phone_tbox.Text, upd_reason_cbox.Text, selected_call_id)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        If complete_call Then
            Dim justified_str As String
            If justified_cbox.SelectedIndex = 0 Then
                justified_str = "yes"
            Else
                justified_str = "no"
            End If
            Try
                Mainfrm.CallEscalationTableAdapter.UpdateQuery1(upd_comp_date_tbox.Text, upd_comp_by_tbox.Text, justified_str, feedback_tbox.Text, resolution_tbox.Text, selected_call_id)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
        End If
        If check_call Then
            Try
                Mainfrm.CallEscalationTableAdapter.UpdateQuery3(checked_date_tbox.Text, checked_by_tbox.Text, selected_call_id)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
        End If
        populate_call_grid()
        Me.Close()
    End Sub

    Private Sub delbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles delbtn.Click
        Try
            Mainfrm.CallEscalationTableAdapter.UpdateQuery2(log_user, Now, selected_call_id)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        populate_call_grid()
        Me.Close()
    End Sub

    Private Sub updphone_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_phone_tbox.TextChanged

    End Sub

    Private Sub complete_btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles complete_btn.Click
        If complete_call Then
            MsgBox("This call has already been completed")
            Exit Sub
        End If
        complete_call = True
        upd_comp_by_tbox.Text = log_user
        upd_comp_date_tbox.Text = Format(Now, "dd/MM/yyyy")
        resolution_tbox.ReadOnly = False
        justified_cbox.Enabled = True
        feedback_tbox.ReadOnly = False
        resolution_tbox.Focus()
    End Sub

    Private Sub upd_debtor_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_debtor_tbox.TextChanged

    End Sub

    Private Sub upd_debtor_tbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles upd_debtor_tbox.Validated
        Dim debtor As Integer
        Try
            debtor = upd_debtor_tbox.Text
        Catch ex As Exception
            MsgBox("debtorID should be a valid case number on onestep")
            Exit Sub
        End Try
        param2 = "select _rowid, name_fore, name_sur, name2, clientSchemeID from Debtor where _rowid = " & debtor
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("DebtorID does not exist on onestep")
            Exit Sub
        End If
        Dim onestep_name As String
        Try
            onestep_name = debtor_dataset.Tables(0).Rows(0).Item(1)
        Catch ex As Exception
            onestep_name = ""
        End Try
        Try
            onestep_name = Trim(onestep_name & " " & debtor_dataset.Tables(0).Rows(0).Item(2))
        Catch ex As Exception

        End Try
        upd_namebtn.Enabled = True
        upd_onestep_name_tbox.Text = onestep_name
        upd_client_lbl.Text = get_cl_name(debtor_dataset.Tables(0).Rows(0).Item(4))
    End Sub

    Private Sub checkbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles checkbtn.Click
        If complete_call = False Then
            MsgBox("Need to complete call before checking")
            Exit Sub
        End If
        If check_call Then
            MsgBox("This call has already been checked")
            Exit Sub
        End If
        If log_user = upd_comp_by_tbox.Text Then
            MsgBox("Can't check calls you have completed yourself")
            Exit Sub
        End If
        check_call = True
        checked_by_tbox.Text = log_user
        checked_date_tbox.Text = Format(Now, "dd/MM/yyyy")
    End Sub

    Private Sub namebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_namebtn.Click
        upd_name_tbox.Text = upd_onestep_name_tbox.Text
    End Sub
End Class
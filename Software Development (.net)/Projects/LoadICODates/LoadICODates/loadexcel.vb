Module loadexcel

    Public vals(1, 1) As String
    Public finalrow As Integer
    Public Sub load_vals(ByVal myss As String)
        Dim row, col, finalcol As Integer
        Dim xl = CreateObject("Excel.Application")
        xl.Workbooks.open(myss)
        Dim sheetname As String

        sheetname = "DateExtract"

        Try
            xl.worksheets(sheetname).activate()
        Catch ex As Exception
            MessageBox.Show("Unable to read spreadsheet")
            xl.workbooks.close()
            Exit Sub
        End Try

        finalrow = xl.activesheet.usedrange.rows.count
        finalcol = xl.activesheet.usedrange.columns.count
        ReDim vals(finalrow, finalcol)
        Dim idx As Integer = 0
        For row = 1 To finalrow
            mainform.ProgressBar1.Value = row / finalrow * 100
            For col = 1 To finalcol
                idx += 1
                If xl.activesheet.cells(row, col).value = Nothing Then
                    vals(row, col) = " "
                Else
                    vals(row, col) = xl.activesheet.cells(row, col).value.ToString
                End If
            Next
        Next
        xl.workbooks.close()
        xl = Nothing
    End Sub
End Module

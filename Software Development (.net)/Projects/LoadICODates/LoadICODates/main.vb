Public Class mainform
    Public outfile As String
    Dim rowidx As Integer
    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        ProgressBar1.Value = 0
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Files(*.xls;*.xlsx)|*.xls;*.xlsx" '|All files (*.*)|*.*"
            .FileName = ""
            .CheckFileExists = True
        End With
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MessageBox.Show("No file Selected")
            Exit Sub
        End If
        load_vals(OpenFileDialog1.FileName)
        Dim idx, debtorid As Integer
        Dim ico_date As Date
        For idx = 1 To finalrow
            Try
                debtorid = vals(idx, 1)
            Catch ex As Exception
                Continue For
            End Try
            Try
                ico_date = vals(idx, 4)
            Catch ex As Exception
                MsgBox("Invalid ICO date for case number " & debtorid)
                Me.Close()
                Exit Sub
            End Try
            outfile = outfile & debtorid & "|SentenceOrderDate:" & Format(ico_date, "dd/MM/yyyy") & _
            ";from spreadsheet on " & Format(Now, "dd/MM/yyyy") & vbNewLine
        Next

        Dim namelength = Len(OpenFileDialog1.FileName)
        Dim filename_prefix As String = ""
        filename_prefix = Microsoft.VisualBasic.Left(OpenFileDialog1.FileName, namelength - 4)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_ICO_dates.txt", outfile, False)
        MsgBox("Load file produced")
        Me.Close()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub

    Private Sub checkbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

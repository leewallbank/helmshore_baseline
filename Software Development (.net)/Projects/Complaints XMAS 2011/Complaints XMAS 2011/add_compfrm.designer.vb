<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class add_compfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Comp_recpt_codeLabel As System.Windows.Forms.Label
        Dim Comp_dateLabel As System.Windows.Forms.Label
        Dim Comp_recvd_codeLabel As System.Windows.Forms.Label
        Dim Comp_case_noLabel As System.Windows.Forms.Label
        Dim Comp_against_codeLabel As System.Windows.Forms.Label
        Dim Comp_cat_codeLabel As System.Windows.Forms.Label
        Dim Comp_client_noLabel1 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Me.Comp_recpt_codeComboBox = New System.Windows.Forms.ComboBox
        Me.ReceipttypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.dontsavebtn = New System.Windows.Forms.Button
        Me.savebtn = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Comp_dateDateTimePicker = New System.Windows.Forms.DateTimePicker
        Me.Comp_recvd_codeComboBox = New System.Windows.Forms.ComboBox
        Me.ReceivedfromBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.case_noTextBox = New System.Windows.Forms.TextBox
        Me.Comp_against_codeComboBox = New System.Windows.Forms.ComboBox
        Me.ComplaintagainstBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cl_combobox = New System.Windows.Forms.ComboBox
        Me.catcombobox = New System.Windows.Forms.ComboBox
        Me.ComplaintcategoriesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.codeComboBox = New System.Windows.Forms.ComboBox
        Me.compTextBox = New System.Windows.Forms.TextBox
        Me.agentComboBox = New System.Windows.Forms.ComboBox
        Me.addbtn = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.doc_textbox = New System.Windows.Forms.TextBox
        Me.prioritycbox = New System.Windows.Forms.CheckBox
        Me.InvComboBox = New System.Windows.Forms.ComboBox
        Me.InvestigatorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ActionsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ActionsTableAdapter
        Me.InvestigatorsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
        Me.Received_fromTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
        Me.Receipt_typeTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Receipt_typeTableAdapter
        Me.Complaint_againstTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
        Me.ComplaintsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComplaintsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ComplaintsTableAdapter
        Me.Complaint_categoriesTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_categoriesTableAdapter
        Me.Test_complaintsTableAdapter = New Complaints.test_complaintsDataSetTableAdapters.test_complaintsTableAdapter
        Me.Test_complaintsDataSet = New Complaints.test_complaintsDataSet
        Me.old_comp_no_tbox = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.gender_combobox = New System.Windows.Forms.ComboBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.eth_combobox = New System.Windows.Forms.ComboBox
        Me.EthnicityBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label9 = New System.Windows.Forms.Label
        Me.EthnicityTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.EthnicityTableAdapter
        Me.debt_typelbl = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.ack_gbox = New System.Windows.Forms.GroupBox
        Me.ack_cbox = New System.Windows.Forms.CheckBox
        Me.ack_datepicker = New System.Windows.Forms.DateTimePicker
        Me.comp_against2_codecombobox = New System.Windows.Forms.ComboBox
        Me.agent2_combobox = New System.Windows.Forms.ComboBox
        Me.legalcbox = New System.Windows.Forms.CheckBox
        Me.legalgbox = New System.Windows.Forms.GroupBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.monetary_risktbox = New System.Windows.Forms.TextBox
        Me.ins_datepicker = New System.Windows.Forms.DateTimePicker
        Me.solicitor_datepicker = New System.Windows.Forms.DateTimePicker
        Comp_recpt_codeLabel = New System.Windows.Forms.Label
        Comp_dateLabel = New System.Windows.Forms.Label
        Comp_recvd_codeLabel = New System.Windows.Forms.Label
        Comp_case_noLabel = New System.Windows.Forms.Label
        Comp_against_codeLabel = New System.Windows.Forms.Label
        Comp_cat_codeLabel = New System.Windows.Forms.Label
        Comp_client_noLabel1 = New System.Windows.Forms.Label
        Label2 = New System.Windows.Forms.Label
        Label3 = New System.Windows.Forms.Label
        Label4 = New System.Windows.Forms.Label
        Label5 = New System.Windows.Forms.Label
        Label6 = New System.Windows.Forms.Label
        Label11 = New System.Windows.Forms.Label
        Label12 = New System.Windows.Forms.Label
        CType(Me.ReceipttypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceivedfromBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComplaintagainstBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComplaintcategoriesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Test_complaintsDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EthnicityBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ack_gbox.SuspendLayout()
        Me.legalgbox.SuspendLayout()
        Me.SuspendLayout()
        '
        'Comp_recpt_codeLabel
        '
        Comp_recpt_codeLabel.AutoSize = True
        Comp_recpt_codeLabel.Location = New System.Drawing.Point(370, 40)
        Comp_recpt_codeLabel.Name = "Comp_recpt_codeLabel"
        Comp_recpt_codeLabel.Size = New System.Drawing.Size(85, 13)
        Comp_recpt_codeLabel.TabIndex = 10
        Comp_recpt_codeLabel.Text = "Form of Receipt:"
        '
        'Comp_dateLabel
        '
        Comp_dateLabel.AutoSize = True
        Comp_dateLabel.Location = New System.Drawing.Point(10, 40)
        Comp_dateLabel.Name = "Comp_dateLabel"
        Comp_dateLabel.Size = New System.Drawing.Size(82, 13)
        Comp_dateLabel.TabIndex = 9
        Comp_dateLabel.Text = "Date Received:"
        '
        'Comp_recvd_codeLabel
        '
        Comp_recvd_codeLabel.AutoSize = True
        Comp_recvd_codeLabel.Location = New System.Drawing.Point(62, 130)
        Comp_recvd_codeLabel.Name = "Comp_recvd_codeLabel"
        Comp_recvd_codeLabel.Size = New System.Drawing.Size(68, 13)
        Comp_recvd_codeLabel.TabIndex = 11
        Comp_recvd_codeLabel.Text = "Complainant:"
        '
        'Comp_case_noLabel
        '
        Comp_case_noLabel.AutoSize = True
        Comp_case_noLabel.Location = New System.Drawing.Point(563, 130)
        Comp_case_noLabel.Name = "Comp_case_noLabel"
        Comp_case_noLabel.Size = New System.Drawing.Size(51, 13)
        Comp_case_noLabel.TabIndex = 12
        Comp_case_noLabel.Text = "Case No:"
        '
        'Comp_against_codeLabel
        '
        Comp_against_codeLabel.AutoSize = True
        Comp_against_codeLabel.Location = New System.Drawing.Point(484, 201)
        Comp_against_codeLabel.Name = "Comp_against_codeLabel"
        Comp_against_codeLabel.Size = New System.Drawing.Size(103, 13)
        Comp_against_codeLabel.TabIndex = 14
        Comp_against_codeLabel.Text = "Comp made against:"
        '
        'Comp_cat_codeLabel
        '
        Comp_cat_codeLabel.AutoSize = True
        Comp_cat_codeLabel.Location = New System.Drawing.Point(36, 313)
        Comp_cat_codeLabel.Name = "Comp_cat_codeLabel"
        Comp_cat_codeLabel.Size = New System.Drawing.Size(52, 13)
        Comp_cat_codeLabel.TabIndex = 15
        Comp_cat_codeLabel.Text = "Category:"
        '
        'Comp_client_noLabel1
        '
        Comp_client_noLabel1.AutoSize = True
        Comp_client_noLabel1.Location = New System.Drawing.Point(27, 217)
        Comp_client_noLabel1.Name = "Comp_client_noLabel1"
        Comp_client_noLabel1.Size = New System.Drawing.Size(65, 13)
        Comp_client_noLabel1.TabIndex = 18
        Comp_client_noLabel1.Text = "Client name:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Location = New System.Drawing.Point(214, 313)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(52, 13)
        Label2.TabIndex = 21
        Label2.Text = "Code No:"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Location = New System.Drawing.Point(305, 363)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(93, 13)
        Label3.TabIndex = 22
        Label3.Text = "Complaints officer:"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Location = New System.Drawing.Point(80, 366)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(103, 13)
        Label4.TabIndex = 24
        Label4.Text = "Details of Complaint:"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Location = New System.Drawing.Point(153, 251)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(61, 13)
        Label5.TabIndex = 26
        Label5.Text = "Staff/bailiff:"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Location = New System.Drawing.Point(454, 366)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(93, 13)
        Label6.TabIndex = 49
        Label6.Text = "No of Documents:"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Location = New System.Drawing.Point(326, 268)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(146, 13)
        Label11.TabIndex = 59
        Label11.Text = "Secondary complaint against:"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Location = New System.Drawing.Point(354, 220)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(118, 13)
        Label12.TabIndex = 64
        Label12.Text = "Main complaint against:"
        '
        'Comp_recpt_codeComboBox
        '
        Me.Comp_recpt_codeComboBox.DataSource = Me.ReceipttypeBindingSource
        Me.Comp_recpt_codeComboBox.DisplayMember = "recpt_text"
        Me.Comp_recpt_codeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Comp_recpt_codeComboBox.FormattingEnabled = True
        Me.Comp_recpt_codeComboBox.Location = New System.Drawing.Point(478, 40)
        Me.Comp_recpt_codeComboBox.Name = "Comp_recpt_codeComboBox"
        Me.Comp_recpt_codeComboBox.Size = New System.Drawing.Size(121, 21)
        Me.Comp_recpt_codeComboBox.TabIndex = 1
        '
        'ReceipttypeBindingSource
        '
        Me.ReceipttypeBindingSource.DataMember = "Receipt_type"
        Me.ReceipttypeBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'dontsavebtn
        '
        Me.dontsavebtn.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.dontsavebtn.Location = New System.Drawing.Point(27, 553)
        Me.dontsavebtn.Name = "dontsavebtn"
        Me.dontsavebtn.Size = New System.Drawing.Size(141, 23)
        Me.dontsavebtn.TabIndex = 21
        Me.dontsavebtn.Text = "DO NOT SAVE and Exit"
        Me.dontsavebtn.UseVisualStyleBackColor = True
        '
        'savebtn
        '
        Me.savebtn.Location = New System.Drawing.Point(768, 553)
        Me.savebtn.Name = "savebtn"
        Me.savebtn.Size = New System.Drawing.Size(94, 23)
        Me.savebtn.TabIndex = 22
        Me.savebtn.Text = "Save and Exit"
        Me.savebtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(314, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "COMPLAINT FORM"
        '
        'Comp_dateDateTimePicker
        '
        Me.Comp_dateDateTimePicker.Location = New System.Drawing.Point(98, 40)
        Me.Comp_dateDateTimePicker.Name = "Comp_dateDateTimePicker"
        Me.Comp_dateDateTimePicker.Size = New System.Drawing.Size(131, 20)
        Me.Comp_dateDateTimePicker.TabIndex = 0
        '
        'Comp_recvd_codeComboBox
        '
        Me.Comp_recvd_codeComboBox.DataSource = Me.ReceivedfromBindingSource
        Me.Comp_recvd_codeComboBox.DisplayMember = "recvd_text"
        Me.Comp_recvd_codeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Comp_recvd_codeComboBox.FormattingEnabled = True
        Me.Comp_recvd_codeComboBox.Location = New System.Drawing.Point(35, 146)
        Me.Comp_recvd_codeComboBox.Name = "Comp_recvd_codeComboBox"
        Me.Comp_recvd_codeComboBox.Size = New System.Drawing.Size(121, 21)
        Me.Comp_recvd_codeComboBox.TabIndex = 4
        '
        'ReceivedfromBindingSource
        '
        Me.ReceivedfromBindingSource.DataMember = "Received_from"
        Me.ReceivedfromBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'case_noTextBox
        '
        Me.case_noTextBox.Location = New System.Drawing.Point(534, 147)
        Me.case_noTextBox.Name = "case_noTextBox"
        Me.case_noTextBox.Size = New System.Drawing.Size(100, 20)
        Me.case_noTextBox.TabIndex = 7
        '
        'Comp_against_codeComboBox
        '
        Me.Comp_against_codeComboBox.DataSource = Me.ComplaintagainstBindingSource
        Me.Comp_against_codeComboBox.DisplayMember = "against_text"
        Me.Comp_against_codeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Comp_against_codeComboBox.FormattingEnabled = True
        Me.Comp_against_codeComboBox.Location = New System.Drawing.Point(478, 217)
        Me.Comp_against_codeComboBox.MaxLength = 7
        Me.Comp_against_codeComboBox.Name = "Comp_against_codeComboBox"
        Me.Comp_against_codeComboBox.Size = New System.Drawing.Size(121, 21)
        Me.Comp_against_codeComboBox.TabIndex = 9
        '
        'ComplaintagainstBindingSource
        '
        Me.ComplaintagainstBindingSource.DataMember = "Complaint_against"
        Me.ComplaintagainstBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'cl_combobox
        '
        Me.cl_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cl_combobox.FormattingEnabled = True
        Me.cl_combobox.Location = New System.Drawing.Point(98, 214)
        Me.cl_combobox.Name = "cl_combobox"
        Me.cl_combobox.Size = New System.Drawing.Size(233, 21)
        Me.cl_combobox.TabIndex = 8
        '
        'catcombobox
        '
        Me.catcombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.catcombobox.FormattingEnabled = True
        Me.catcombobox.Items.AddRange(New Object() {"A", "B", "C"})
        Me.catcombobox.Location = New System.Drawing.Point(107, 310)
        Me.catcombobox.Name = "catcombobox"
        Me.catcombobox.Size = New System.Drawing.Size(94, 21)
        Me.catcombobox.TabIndex = 13
        '
        'ComplaintcategoriesBindingSource
        '
        Me.ComplaintcategoriesBindingSource.DataMember = "Complaint_categories"
        Me.ComplaintcategoriesBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'codeComboBox
        '
        Me.codeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.codeComboBox.FormattingEnabled = True
        Me.codeComboBox.Location = New System.Drawing.Point(272, 313)
        Me.codeComboBox.Name = "codeComboBox"
        Me.codeComboBox.Size = New System.Drawing.Size(516, 21)
        Me.codeComboBox.TabIndex = 14
        '
        'compTextBox
        '
        Me.compTextBox.AcceptsReturn = True
        Me.compTextBox.AllowDrop = True
        Me.compTextBox.Location = New System.Drawing.Point(27, 382)
        Me.compTextBox.Multiline = True
        Me.compTextBox.Name = "compTextBox"
        Me.compTextBox.Size = New System.Drawing.Size(246, 144)
        Me.compTextBox.TabIndex = 15
        '
        'agentComboBox
        '
        Me.agentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.agentComboBox.FormattingEnabled = True
        Me.agentComboBox.Location = New System.Drawing.Point(98, 265)
        Me.agentComboBox.Name = "agentComboBox"
        Me.agentComboBox.Size = New System.Drawing.Size(174, 21)
        Me.agentComboBox.TabIndex = 10
        '
        'addbtn
        '
        Me.addbtn.Location = New System.Drawing.Point(465, 396)
        Me.addbtn.Name = "addbtn"
        Me.addbtn.Size = New System.Drawing.Size(205, 23)
        Me.addbtn.TabIndex = 17
        Me.addbtn.Text = "Add a document"
        Me.addbtn.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'doc_textbox
        '
        Me.doc_textbox.Location = New System.Drawing.Point(553, 363)
        Me.doc_textbox.Name = "doc_textbox"
        Me.doc_textbox.ReadOnly = True
        Me.doc_textbox.Size = New System.Drawing.Size(46, 20)
        Me.doc_textbox.TabIndex = 48
        Me.doc_textbox.TabStop = False
        '
        'prioritycbox
        '
        Me.prioritycbox.AutoSize = True
        Me.prioritycbox.Location = New System.Drawing.Point(374, 470)
        Me.prioritycbox.Name = "prioritycbox"
        Me.prioritycbox.Size = New System.Drawing.Size(77, 17)
        Me.prioritycbox.TabIndex = 19
        Me.prioritycbox.Text = "PRIORITY"
        Me.prioritycbox.UseVisualStyleBackColor = True
        '
        'InvComboBox
        '
        Me.InvComboBox.DataSource = Me.InvestigatorsBindingSource
        Me.InvComboBox.DisplayMember = "inv_text"
        Me.InvComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.InvComboBox.FormattingEnabled = True
        Me.InvComboBox.Location = New System.Drawing.Point(298, 395)
        Me.InvComboBox.Name = "InvComboBox"
        Me.InvComboBox.Size = New System.Drawing.Size(121, 21)
        Me.InvComboBox.TabIndex = 16
        '
        'InvestigatorsBindingSource
        '
        Me.InvestigatorsBindingSource.DataMember = "Investigators"
        Me.InvestigatorsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSetBindingSource
        '
        'PraiseAndComplaintsSQLDataSetBindingSource
        '
        Me.PraiseAndComplaintsSQLDataSetBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        Me.PraiseAndComplaintsSQLDataSetBindingSource.Position = 0
        '
        'ActionsTableAdapter
        '
        Me.ActionsTableAdapter.ClearBeforeFill = True
        '
        'InvestigatorsTableAdapter
        '
        Me.InvestigatorsTableAdapter.ClearBeforeFill = True
        '
        'Received_fromTableAdapter
        '
        Me.Received_fromTableAdapter.ClearBeforeFill = True
        '
        'Receipt_typeTableAdapter
        '
        Me.Receipt_typeTableAdapter.ClearBeforeFill = True
        '
        'Complaint_againstTableAdapter
        '
        Me.Complaint_againstTableAdapter.ClearBeforeFill = True
        '
        'ComplaintsBindingSource
        '
        Me.ComplaintsBindingSource.DataMember = "Complaints"
        Me.ComplaintsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'ComplaintsTableAdapter
        '
        Me.ComplaintsTableAdapter.ClearBeforeFill = True
        '
        'Complaint_categoriesTableAdapter
        '
        Me.Complaint_categoriesTableAdapter.ClearBeforeFill = True
        '
        'Test_complaintsTableAdapter
        '
        Me.Test_complaintsTableAdapter.ClearBeforeFill = True
        '
        'Test_complaintsDataSet
        '
        Me.Test_complaintsDataSet.DataSetName = "test_complaintsDataSet"
        Me.Test_complaintsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'old_comp_no_tbox
        '
        Me.old_comp_no_tbox.Location = New System.Drawing.Point(690, 396)
        Me.old_comp_no_tbox.Name = "old_comp_no_tbox"
        Me.old_comp_no_tbox.Size = New System.Drawing.Size(100, 20)
        Me.old_comp_no_tbox.TabIndex = 18
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(685, 363)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(110, 13)
        Me.Label7.TabIndex = 51
        Me.Label7.Text = "Old Complaint number"
        '
        'gender_combobox
        '
        Me.gender_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.gender_combobox.FormattingEnabled = True
        Me.gender_combobox.Items.AddRange(New Object() {"N/A", "F", "M", "Unknown"})
        Me.gender_combobox.Location = New System.Drawing.Point(178, 146)
        Me.gender_combobox.Name = "gender_combobox"
        Me.gender_combobox.Size = New System.Drawing.Size(88, 21)
        Me.gender_combobox.TabIndex = 5
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(196, 130)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(42, 13)
        Me.Label8.TabIndex = 53
        Me.Label8.Text = "Gender"
        '
        'eth_combobox
        '
        Me.eth_combobox.DataSource = Me.EthnicityBindingSource
        Me.eth_combobox.DisplayMember = "eth_name"
        Me.eth_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.eth_combobox.FormattingEnabled = True
        Me.eth_combobox.Location = New System.Drawing.Point(298, 146)
        Me.eth_combobox.Name = "eth_combobox"
        Me.eth_combobox.Size = New System.Drawing.Size(198, 21)
        Me.eth_combobox.TabIndex = 6
        '
        'EthnicityBindingSource
        '
        Me.EthnicityBindingSource.DataMember = "Ethnicity"
        Me.EthnicityBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(361, 130)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(50, 13)
        Me.Label9.TabIndex = 55
        Me.Label9.Text = "Ethnicity:"
        '
        'EthnicityTableAdapter
        '
        Me.EthnicityTableAdapter.ClearBeforeFill = True
        '
        'debt_typelbl
        '
        Me.debt_typelbl.AutoSize = True
        Me.debt_typelbl.Location = New System.Drawing.Point(720, 150)
        Me.debt_typelbl.Name = "debt_typelbl"
        Me.debt_typelbl.Size = New System.Drawing.Size(10, 13)
        Me.debt_typelbl.TabIndex = 56
        Me.debt_typelbl.Text = " "
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(704, 150)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(0, 13)
        Me.Label10.TabIndex = 57
        '
        'ack_gbox
        '
        Me.ack_gbox.Controls.Add(Me.ack_cbox)
        Me.ack_gbox.Controls.Add(Me.ack_datepicker)
        Me.ack_gbox.Location = New System.Drawing.Point(478, 457)
        Me.ack_gbox.Name = "ack_gbox"
        Me.ack_gbox.Size = New System.Drawing.Size(162, 69)
        Me.ack_gbox.TabIndex = 20
        Me.ack_gbox.TabStop = False
        Me.ack_gbox.Text = "Stage 1 Acknowledgement"
        '
        'ack_cbox
        '
        Me.ack_cbox.AutoSize = True
        Me.ack_cbox.Location = New System.Drawing.Point(33, 19)
        Me.ack_cbox.Name = "ack_cbox"
        Me.ack_cbox.Size = New System.Drawing.Size(76, 17)
        Me.ack_cbox.TabIndex = 80
        Me.ack_cbox.Text = "Letter sent"
        Me.ack_cbox.UseVisualStyleBackColor = True
        '
        'ack_datepicker
        '
        Me.ack_datepicker.Location = New System.Drawing.Point(19, 36)
        Me.ack_datepicker.Name = "ack_datepicker"
        Me.ack_datepicker.Size = New System.Drawing.Size(117, 20)
        Me.ack_datepicker.TabIndex = 79
        '
        'comp_against2_codecombobox
        '
        Me.comp_against2_codecombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comp_against2_codecombobox.FormattingEnabled = True
        Me.comp_against2_codecombobox.Location = New System.Drawing.Point(478, 265)
        Me.comp_against2_codecombobox.Name = "comp_against2_codecombobox"
        Me.comp_against2_codecombobox.Size = New System.Drawing.Size(121, 21)
        Me.comp_against2_codecombobox.TabIndex = 11
        '
        'agent2_combobox
        '
        Me.agent2_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.agent2_combobox.FormattingEnabled = True
        Me.agent2_combobox.Location = New System.Drawing.Point(622, 265)
        Me.agent2_combobox.Name = "agent2_combobox"
        Me.agent2_combobox.Size = New System.Drawing.Size(173, 21)
        Me.agent2_combobox.TabIndex = 12
        '
        'legalcbox
        '
        Me.legalcbox.AutoSize = True
        Me.legalcbox.Location = New System.Drawing.Point(723, 25)
        Me.legalcbox.Name = "legalcbox"
        Me.legalcbox.Size = New System.Drawing.Size(104, 17)
        Me.legalcbox.TabIndex = 2
        Me.legalcbox.Text = "Legal/Insurance"
        Me.legalcbox.UseVisualStyleBackColor = True
        '
        'legalgbox
        '
        Me.legalgbox.Controls.Add(Me.Label15)
        Me.legalgbox.Controls.Add(Me.Label14)
        Me.legalgbox.Controls.Add(Me.Label13)
        Me.legalgbox.Controls.Add(Me.monetary_risktbox)
        Me.legalgbox.Controls.Add(Me.ins_datepicker)
        Me.legalgbox.Controls.Add(Me.solicitor_datepicker)
        Me.legalgbox.Location = New System.Drawing.Point(681, 48)
        Me.legalgbox.Name = "legalgbox"
        Me.legalgbox.Size = New System.Drawing.Size(181, 199)
        Me.legalgbox.TabIndex = 3
        Me.legalgbox.TabStop = False
        Me.legalgbox.Text = "Legal/Insurance"
        Me.legalgbox.Visible = False
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(32, 146)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(75, 13)
        Me.Label15.TabIndex = 68
        Me.Label15.Text = "Monetary Risk"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(32, 86)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(95, 13)
        Me.Label14.TabIndex = 68
        Me.Label14.Text = "Referred to Insurer"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(32, 25)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(100, 13)
        Me.Label13.TabIndex = 3
        Me.Label13.Text = "Referred to Solicitor"
        '
        'monetary_risktbox
        '
        Me.monetary_risktbox.Location = New System.Drawing.Point(19, 162)
        Me.monetary_risktbox.Name = "monetary_risktbox"
        Me.monetary_risktbox.Size = New System.Drawing.Size(100, 20)
        Me.monetary_risktbox.TabIndex = 2
        '
        'ins_datepicker
        '
        Me.ins_datepicker.Checked = False
        Me.ins_datepicker.Location = New System.Drawing.Point(19, 102)
        Me.ins_datepicker.Name = "ins_datepicker"
        Me.ins_datepicker.ShowCheckBox = True
        Me.ins_datepicker.Size = New System.Drawing.Size(139, 20)
        Me.ins_datepicker.TabIndex = 1
        '
        'solicitor_datepicker
        '
        Me.solicitor_datepicker.Checked = False
        Me.solicitor_datepicker.Location = New System.Drawing.Point(19, 41)
        Me.solicitor_datepicker.Name = "solicitor_datepicker"
        Me.solicitor_datepicker.ShowCheckBox = True
        Me.solicitor_datepicker.Size = New System.Drawing.Size(139, 20)
        Me.solicitor_datepicker.TabIndex = 0
        '
        'add_compfrm
        '
        Me.AcceptButton = Me.savebtn
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.dontsavebtn
        Me.ClientSize = New System.Drawing.Size(893, 615)
        Me.Controls.Add(Me.legalgbox)
        Me.Controls.Add(Me.legalcbox)
        Me.Controls.Add(Label12)
        Me.Controls.Add(Me.agent2_combobox)
        Me.Controls.Add(Me.comp_against2_codecombobox)
        Me.Controls.Add(Label11)
        Me.Controls.Add(Me.ack_gbox)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.debt_typelbl)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.eth_combobox)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.gender_combobox)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.old_comp_no_tbox)
        Me.Controls.Add(Me.InvComboBox)
        Me.Controls.Add(Me.prioritycbox)
        Me.Controls.Add(Me.doc_textbox)
        Me.Controls.Add(Label6)
        Me.Controls.Add(Me.addbtn)
        Me.Controls.Add(Label5)
        Me.Controls.Add(Me.agentComboBox)
        Me.Controls.Add(Me.compTextBox)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Label3)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.codeComboBox)
        Me.Controls.Add(Me.catcombobox)
        Me.Controls.Add(Me.cl_combobox)
        Me.Controls.Add(Comp_client_noLabel1)
        Me.Controls.Add(Comp_cat_codeLabel)
        Me.Controls.Add(Comp_against_codeLabel)
        Me.Controls.Add(Me.Comp_against_codeComboBox)
        Me.Controls.Add(Comp_case_noLabel)
        Me.Controls.Add(Me.case_noTextBox)
        Me.Controls.Add(Comp_recvd_codeLabel)
        Me.Controls.Add(Me.Comp_recvd_codeComboBox)
        Me.Controls.Add(Comp_dateLabel)
        Me.Controls.Add(Me.Comp_dateDateTimePicker)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.savebtn)
        Me.Controls.Add(Me.dontsavebtn)
        Me.Controls.Add(Comp_recpt_codeLabel)
        Me.Controls.Add(Me.Comp_recpt_codeComboBox)
        Me.Name = "add_compfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add a new Complaint"
        CType(Me.ReceipttypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceivedfromBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComplaintagainstBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComplaintcategoriesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Test_complaintsDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EthnicityBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ack_gbox.ResumeLayout(False)
        Me.ack_gbox.PerformLayout()
        Me.legalgbox.ResumeLayout(False)
        Me.legalgbox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Comp_recpt_codeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents dontsavebtn As System.Windows.Forms.Button
    Friend WithEvents savebtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Comp_dateDateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Comp_recvd_codeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents case_noTextBox As System.Windows.Forms.TextBox
    'Friend WithEvents Comp_client_noComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Comp_against_codeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents cl_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents catcombobox As System.Windows.Forms.ComboBox
    Friend WithEvents codeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents compTextBox As System.Windows.Forms.TextBox
    Friend WithEvents agentComboBox As System.Windows.Forms.ComboBox
    'Friend WithEvents DepartmentsDataSet As Complaints.DepartmentsDataSet
    'Friend WithEvents DepartmentsBindingSource As System.Windows.Forms.BindingSource
    'Friend WithEvents DepartmentsTableAdapter As Complaints.DepartmentsDataSetTableAdapters.DepartmentsTableAdapter
    Friend WithEvents addbtn As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents doc_textbox As System.Windows.Forms.TextBox
    Friend WithEvents prioritycbox As System.Windows.Forms.CheckBox
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents ActionsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ActionsTableAdapter
    Friend WithEvents InvestigatorsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
    Friend WithEvents InvComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Received_fromTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
    Friend WithEvents Receipt_typeTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Receipt_typeTableAdapter
    Friend WithEvents ReceipttypeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ReceivedfromBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ComplaintagainstBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaint_againstTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
    Friend WithEvents ComplaintsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ComplaintsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ComplaintsTableAdapter
    Friend WithEvents ComplaintcategoriesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaint_categoriesTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_categoriesTableAdapter
    Friend WithEvents PraiseAndComplaintsSQLDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Test_complaintsTableAdapter As Complaints.test_complaintsDataSetTableAdapters.test_complaintsTableAdapter
    Friend WithEvents Test_complaintsDataSet As Complaints.test_complaintsDataSet
    Friend WithEvents InvestigatorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents old_comp_no_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents gender_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents eth_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents EthnicityBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents EthnicityTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.EthnicityTableAdapter
    Friend WithEvents debt_typelbl As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents ack_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents ack_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents ack_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents agent2_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents comp_against2_codecombobox As System.Windows.Forms.ComboBox
    Friend WithEvents legalcbox As System.Windows.Forms.CheckBox
    Friend WithEvents legalgbox As System.Windows.Forms.GroupBox
    Friend WithEvents monetary_risktbox As System.Windows.Forms.TextBox
    Friend WithEvents ins_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents solicitor_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label

    
End Class

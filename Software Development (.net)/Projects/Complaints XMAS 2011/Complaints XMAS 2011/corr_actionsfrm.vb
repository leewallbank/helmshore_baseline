Public Class corr_actionsfrm

    Private Sub corr_actionsfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Corrective_actions' table. You can move, or remove it, as needed.
        Me.Corrective_actionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Corrective_actions)
        'TODO: This line of code loads data into the 'Corrective_actionsDataSet.Corrective_actions' table. You can move, or remove it, as needed.
        corr_action_orig_no_rows = DataGridView1.RowCount
    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.ColumnIndex = 1 And e.RowIndex >= 0 Then
            corr_action_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            corr_action_name = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            If DataGridView1.Rows(e.RowIndex).IsNewRow Then Return
            Try
                Me.Corrective_actionsTableAdapter.UpdateQuery(corr_action_name, corr_action_code)
                log_text = "Corrective action amended - " & corr_action_code & _
                                                  " from " & orig_text & " to " & corr_action_name
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate Corrective action entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter

        corr_action_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        orig_text = DataGridView1.Rows(e.RowIndex).Cells(1).Value

    End Sub

    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles DataGridView1.RowsRemoved
        If e.RowIndex > 0 Then
            Try
                Try
                    Me.Corrective_actionsTableAdapter.DeleteQuery(corr_action_code)
                Catch
                    MessageBox.Show("Can't delete as it's used in a complaint")
                End Try
                log_text = "Corrective action deleted - " & corr_action_code & " name " & orig_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
End Class
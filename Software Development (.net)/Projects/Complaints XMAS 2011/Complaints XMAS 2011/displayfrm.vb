Public Class displayfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub findbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles findbtn.Click
        disable_buttons()
        get_letter = True
        start_search()
        enable_buttons()
    End Sub
    Private Sub start_search()
        ErrorProvider1.SetError(recvd_fromcombobox, "")
        ErrorProvider1.SetError(cat_combobox, "")
        ErrorProvider1.SetError(dept_combobox, "")
        ErrorProvider1.SetError(stage_combobox, "")
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5

        Dim resp
        search_csid_no = 0
        If allrbtn.Checked = True Then
            search_no = 1
        ElseIf caserbtn.Checked = True Then
            Try
                resp = InputBox("Enter case number")
                If resp Is "" Then
                    ProgressBar1.Visible = False
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox("Case number is not numeric")
                ProgressBar1.Visible = False
                Exit Sub
            End Try
            case_no = resp
            search_no = 2
        ElseIf clrbtn.Checked = True Then
            disp_clientfrm.ShowDialog()
            If cl_no = 0 Then
                MessageBox.Show("No client has been selected")
                ProgressBar1.Visible = False
                Exit Sub
            Else
                'select scheme if required
                If MsgBox("Select all schemes?", MsgBoxStyle.YesNo, "Client Scheme selection") = MsgBoxResult.No Then
                    schform.ShowDialog()
                End If
            End If
            search_no = 3
            'ElseIf agentrbtn.Checked = True Then
            '    agent_dispfrm.ShowDialog()
            '    If agent1 = -1 Then
            '        MessageBox.Show("No agent has been selected")
            '        ProgressBar1.Visible = False
            '        Exit Sub
            '    End If
            '    search_no = 4

            'ElseIf deptrbtn.Checked = True Then
            '    Try
            '        dept_code = InputBox("Enter department number")
            '    Catch ex As Exception
            '        MsgBox("Department number is not numeric")
            '        Exit Sub
            '    End Try
            '    search_no = 5
        ElseIf invrbtn.Checked = True Then
            investdispfrm.ShowDialog()
            If inv_code = 0 Then
                MessageBox.Show("No investigator has been selected")
                ProgressBar1.Visible = False
                Exit Sub
            End If
            search_no = 7
        End If
        founded_search = "A"
        If openrbtn.Checked = True Then
            founded_search = "U"
        ElseIf Foundedrbtn.Checked = True Then
            founded_search = "Y"
        ElseIf unfoundedrbtn.Checked = True Then
            founded_search = "N"
        End If

        held_search = "A"
        'If heldrbtn.Checked = True Then
        '    held_search = "Y"
        'ElseIf notheldrbtn.Checked = True Then
        '    held_search = "N"
        'End If

        completed_search = "A"
        If completedrbtn.Checked = True Then
            completed_search = "Y"
        ElseIf notcomprbtn.Checked = True Then
            completed_search = "N"
        End If
        selected_comp_no = 0
        displayfrm2.ShowDialog()
        ProgressBar1.Visible = False
    End Sub


    Private Sub dispbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dispbtn.Click
        Dim resp
        Try
            resp = InputBox("Enter complaint number")
            If resp Is "" Then
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("Complaint number not numeric")
            Exit Sub
        End Try
        If Len(resp) = 0 Then
            MsgBox("Complaint number not numeric")
            Exit Sub
        End If
        comp_no = resp
        displayfrm3.ShowDialog()
    End Sub

    Private Sub displayfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Receipt_type' table. You can move, or remove it, as needed.
        Me.Receipt_typeTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Receipt_type)
        search_recvd_from = -1
        cat_text = ""
        cat_combobox.SelectedIndex = -1
        Me.Received_fromTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Received_from)
        recvd_fromcombobox.SelectedIndex = -1
        Me.Complaint_againstTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_against)
        dept_combobox.SelectedIndex = -1
        search_dept = ""
        debt_typecombobox.SelectedIndex = 0

        agentcombobox.Items.Clear()
        param1 = "onestep"
        param2 = "select _rowid, name_fore, name_sur, status from Bailiff" & _
        " where _rowid <> 2057 and _rowid <> 2037 and _rowid <> 2063" & _
        " order by name_sur"
        Dim bail_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 1 Then
            Exit Sub
        End If

        ReDim search_bailiff_table(no_of_rows, 2)

        Dim idx As Integer
        Dim idx2 As Integer = 0
        Dim bailiff_name As String
        For idx = 0 To no_of_rows - 1
            Try
                bailiff_name = Trim(bail_dataset.Tables(0).Rows(idx).Item(2))
            Catch ex As Exception
                Continue For
            End Try
            If bailiff_name.Length = 0 Then
                Continue For
            End If
            idx2 += 1
            Dim forename As String
            Try
                forename = Trim(bail_dataset.Tables(0).Rows(idx).Item(1))
            Catch ex As Exception
                forename = ""
            End Try
            bailiff_name = bailiff_name & ", " & forename

            If bail_dataset.Tables(0).Rows(idx).Item(3) = "C" Then
                bailiff_name = bailiff_name & " (C)"
            End If
            search_bailiff_table(idx2, 2) = Trim(bailiff_name)
            search_bailiff_table(idx2, 1) = bail_dataset.Tables(0).Rows(idx).Item(0)
        Next
        agent_rows = idx2

        For idx = 1 To idx2
            agentcombobox.Items.Add(search_bailiff_table(idx, 2))
        Next
        agentcombobox.Items.Add("Various")
        agentcombobox.SelectedIndex = -1
        agentcombobox.Text = " "
        agent1 = -1
        agent2 = -1

        'add receipt type combobox
        displayfrm3.Receipt_typeTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Receipt_type)
        receipt_typecombobox.SelectedIndex = -1
        search_receipt_type = ""
    End Sub

    Private Sub agentrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub against_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recvd_fromcombobox.SelectedIndexChanged

    End Sub

    Private Sub recvd_fromcombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles recvd_fromcombobox.Validated

    End Sub

    Private Sub cat_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cat_combobox.SelectedIndexChanged

    End Sub

    Private Sub cat_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cat_combobox.Validated

    End Sub

    Private Sub cat_combobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cat_combobox.Validating
        If Microsoft.VisualBasic.Len(Trim(cat_combobox.Text)) = 0 Then
            cat_text = ""
        Else
            cat_text = cat_combobox.Text
            If cat_text <> "A" And cat_text <> "B" And cat_text <> "C" Then
                ErrorProvider1.SetError(cat_combobox, "Category must be A,B or C or blank")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub recvd_fromcombobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles recvd_fromcombobox.Validating
        recvd_text = recvd_fromcombobox.Text
        If Microsoft.VisualBasic.Len(Trim(recvd_text)) = 0 Then
            recvd_from = -1
        Else
            If recvd_fromcombobox.SelectedIndex = -1 Then
                ErrorProvider1.SetError(recvd_fromcombobox, "Pick one from list or leave blank")
                e.Cancel = True
            Else
                search_recvd_from = recvd_fromcombobox.SelectedIndex
            End If
        End If
    End Sub

    Private Sub agentcombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentcombobox.Validated
        If search_agent_no = 0 Then
            Exit Sub
        End If
        Dim idx As Integer
        If agentcombobox.SelectedItem = "Various" Then
            agent1 = 9999
        Else
            For idx = 1 To agent_rows
                If search_bailiff_table(idx, 2) = agentcombobox.SelectedItem Then
                    If agent1 = -1 Then
                        agent1 = search_bailiff_table(idx, 1)
                    Else
                        agent2 = search_bailiff_table(idx, 1)
                        Exit For
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub agentcombobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles agentcombobox.Validating
        Dim agent_text As String = agentcombobox.Text
        agent1 = -1
        agent2 = -1
        If Microsoft.VisualBasic.Len(Trim(agent_text)) = 0 Then
            search_agent_no = 0
        Else
            If agentcombobox.SelectedIndex = -1 Then
                ErrorProvider1.SetError(recvd_fromcombobox, "Pick one from list or leave blank")
                e.Cancel = True
            Else
                search_agent_no = agentcombobox.SelectedIndex
            End If
        End If
    End Sub

    Private Sub dept_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles dept_combobox.Validated
    End Sub


    Private Sub dept_combobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles dept_combobox.Validating
        search_dept = dept_combobox.Text
        If Microsoft.VisualBasic.Len(Trim(search_dept)) = 0 Then
            search_dept = ""
        Else
            If search_dept.Length > 0 Then
                If dept_combobox.SelectedIndex = -1 Then
                    ErrorProvider1.SetError(dept_combobox, "Pick one from list or leave blank")
                    e.Cancel = True
                End If
            End If
        End If
    End Sub

    Private Sub find2btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles find2btn.Click
        disable_buttons()
        get_letter = False
        start_search()
        enable_buttons()
    End Sub

    Private Sub disable_buttons()
        findbtn.Enabled = False
        find2btn.Enabled = False
        exitbtn.Enabled = False
    End Sub
    Private Sub enable_buttons()
        findbtn.Enabled = True
        find2btn.Enabled = True
        exitbtn.Enabled = True
    End Sub

    Private Sub stage_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage_combobox.SelectedIndexChanged

    End Sub

    Private Sub stage_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage_combobox.Validated
        If Microsoft.VisualBasic.Len(Trim(stage_combobox.Text)) = 0 Then
            search_stage_text = ""
        Else
            search_stage_text = stage_combobox.Text
        End If
    End Sub

    Private Sub stage_combobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage_combobox.Validating
        If Microsoft.VisualBasic.Len(Trim(stage_combobox.Text)) > 0 Then
            If stage_combobox.SelectedIndex < 0 Then
                ErrorProvider1.SetError(stage_combobox, "Stage must be blank or one from the list")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub recvd_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recvd_cbox.CheckedChanged
        If recvd_cbox.Checked Then
            recvd_datepicker.Visible = True
        Else
            recvd_datepicker.Visible = False
        End If
    End Sub

    Private Sub comp_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comp_cbox.CheckedChanged
        If comp_cbox.Checked = True Then
            comp_datepicker.Visible = True
        Else
            comp_datepicker.Visible = False
        End If
    End Sub

    Private Sub dept_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dept_combobox.SelectedIndexChanged

    End Sub

    Private Sub receipt_typecombobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles receipt_typecombobox.SelectedIndexChanged

    End Sub

    Private Sub receipt_typecombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles receipt_typecombobox.Validated
        If Microsoft.VisualBasic.Len(Trim(receipt_typecombobox.Text)) = 0 Then
            search_receipt_type = ""
        Else
            search_receipt_type = receipt_typecombobox.Text
        End If
    End Sub

    Private Sub debt_typecombobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles debt_typecombobox.SelectedIndexChanged

    End Sub

    Private Sub debt_typecombobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles debt_typecombobox.Validating

    End Sub

    Private Sub receipt_typecombobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles receipt_typecombobox.Validating
        If Microsoft.VisualBasic.Len(Trim(receipt_typecombobox.Text)) > 0 Then
            If receipt_typecombobox.SelectedIndex < 0 Then
                ErrorProvider1.SetError(receipt_typecombobox, "Receipt Type must be blank or one from the list")
                e.Cancel = True
            End If
        End If
    End Sub
End Class
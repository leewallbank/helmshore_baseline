Public Class displayfrm2
    Dim gtot_cases As Integer
    Private Sub displayfrm2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim name As String = LCase(Trim(displayfrm.name_textbox.Text))
            name = LCase(Trim(name))
            If admin_user = False Then
                updbtn.Enabled = False
            End If
            Dim no_of_complaints As Integer = ComplaintsTableAdapter.ScalarQuery()
            Select Case search_no
                Case 1
                    ComplaintsTableAdapter.Fill(PraiseAndComplaintsSQLDataSet.Complaints)
                Case 2
                    ComplaintsTableAdapter.Fill2(PraiseAndComplaintsSQLDataSet.Complaints, case_no)
                    held_search = "A"
                Case 3
                    ComplaintsTableAdapter.Fill3(PraiseAndComplaintsSQLDataSet.Complaints, cl_no)
                    held_search = "A"
                Case 4
                    ComplaintsTableAdapter.FillBy1(PraiseAndComplaintsSQLDataSet.Complaints, agent1, agent2)
                    held_search = "A"
                Case 5
                    'Me.ComplaintsTableAdapter.Fill5(Me.ComplaintsDataSet.Complaints, dept_code)
                Case 6
                    ComplaintsTableAdapter.Fill6(PraiseAndComplaintsSQLDataSet.Complaints)
                    held_search = "A"
                Case 7
                    ComplaintsTableAdapter.FillBy2(PraiseAndComplaintsSQLDataSet.Complaints, inv_code)
                    held_search = "A"
            End Select
            'remove any rows not required for completed, founded or letter
            Dim row As DataRow
            Dim idx As Integer = 0
            Dim idx2 As Integer = 0
            gtot_cases = 0
            DataGridView1.Rows.Clear()
            file = "Investigator,Complaint No,Case No,Recvd date,Stage 1 completed,Stage 2 recvd," & _
            "Stage 2 completed,Stage 3 recvd, Stage 3 completed,Days taken,Days left," & _
            "client,Complainant,against,agent,category,hold letter,Founded" & vbNewLine
            Dim tot_rows As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows.Count
            Dim days_taken, days_left, client_name, type, agent, against, category, hold As String
            For Each row In PraiseAndComplaintsSQLDataSet.Complaints.Rows
                displayfrm.ProgressBar1.Value = (idx / tot_rows) * 100
                Application.DoEvents()
                Dim disp_Comp_no As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(0)
                Dim recvd_date As Date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1)
                Dim comp_case_no As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(4)
                Dim founded As String = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(18)
                Dim comp_completed_by As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(15)
                alloc_to_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(12)
                InvestigatorsTableAdapter.FillBy(PraiseAndComplaintsSQLDataSet.Investigators, alloc_to_code)
                Dim disp_comp_officer As String = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
                InvestigatorsTableAdapter.Fill(PraiseAndComplaintsSQLDataSet.Investigators)
                stage_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(21)
                Dim stage2_start_date As Date = Nothing
                Dim stage2_completed_by As Integer = 0
                Dim stage2_completed_date As Date = Nothing
                Dim stage3_start_date As Date = Nothing
                Dim stage3_completed_by As Integer = 0
                Dim stage3_completed_date As Date = Nothing
                Select Case stage_no
                    Case 2
                        Try
                            stage2_start_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(29)
                        Catch ex As Exception
                            stage2_start_date = Nothing
                        End Try
                        Try
                            stage2_completed_by = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(30)
                        Catch ex As Exception
                            stage2_completed_by = 0
                        End Try
                        Try
                            stage2_completed_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(31)
                        Catch ex As Exception
                            stage2_completed_date = Nothing
                        End Try
                    Case 3
                        Try
                            stage2_start_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(29)
                        Catch ex As Exception
                            stage2_start_date = Nothing
                        End Try
                        Try
                            stage2_completed_by = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(30)
                        Catch ex As Exception
                            stage2_completed_by = 0
                        End Try
                        Try
                            stage2_completed_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(31)
                        Catch ex As Exception
                            stage2_completed_date = Nothing
                        End Try
                        Try
                            stage3_start_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(38)
                        Catch ex As Exception
                            stage3_start_date = Nothing
                        End Try
                        Try
                            stage3_completed_by = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(39)
                        Catch ex As Exception
                            stage3_completed_by = 0
                        End Try
                        Try
                            stage3_completed_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(40)
                        Catch ex As Exception
                            stage3_completed_date = Nothing
                        End Try
                End Select
                Try
                    legal_ins_flag = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(46)
                Catch ex As Exception
                    legal_ins_flag = "N"
                End Try

                'ignore any stage 0 complaints
                If stage_no = 0 Then
                    idx += 1
                    Continue For
                End If
                'founded/unfounded
                If founded_search <> "A" Then
                    If founded <> founded_search Then
                        idx += 1
                        Continue For
                    End If
                End If
                If founded = "U" Then founded = " "

                'category
                If cat_text <> "" Then
                    If cat_text <> PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(8) Then
                        idx += 1
                        Continue For
                    End If
                End If

                'received from
                If search_recvd_from >= 0 Then
                    If search_recvd_from + 1 <> PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(3) Then
                        idx += 1
                        Continue For
                    End If
                End If

                'received against
                against = ""
                against_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(6)
                Complaint_againstTableAdapter.FillBy(PraiseAndComplaintsSQLDataSet.Complaint_against, against_code)
                against = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(0).Item(1)
                If search_dept.Length > 0 Then
                    If search_dept <> against Then
                        idx += 1
                        Continue For
                    End If
                End If

                'receipt type
                If search_receipt_type.Length > 0 Then
                    Dim receipt_type As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(2)
                    displayfrm3.Receipt_typeTableAdapter.FillBy(displayfrm3.PraiseAndComplaintsSQLDataSet.Receipt_type, receipt_type)
                    Dim receipt_type_text As String = displayfrm3.PraiseAndComplaintsSQLDataSet.Receipt_type.Rows(0).Item(1)
                    If search_receipt_type <> receipt_type_text Then
                        idx += 1
                        Continue For
                    End If
                End If
                'get agent
                agent = ""
                agent_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(7)
                If agent1 > 0 And agent1 <> agent_no Then
                    If agent2 > 0 Then
                        If agent2 <> agent_no Then
                            idx += 1
                            Continue For
                        End If
                    Else
                        idx += 1
                        Continue For
                    End If
                End If

                'check stage
                If search_stage_text <> "" Then
                    If search_stage_text = "Legal/Insurance" Then
                        If legal_ins_flag = "N" Then
                            idx += 1
                            Continue For
                        End If
                    Else
                        If stage_no = 1 And (search_stage_text = "Stage 2" Or search_stage_text = "Stage 3") _
                        Or (stage_no = 2 And search_stage_text = "Stage 3") Then
                            idx += 1
                            Continue For
                        End If
                    End If
                End If
                'completed/not completed stage
                If completed_search <> "A" Then
                    'need to check stage completed if a stage is selected
                    If search_stage_text = "" Then
                        If completed_search = "Y" Then
                            Select Case stage_no
                                Case 1
                                    If comp_completed_by = 0 Then
                                        idx += 1
                                        Continue For
                                    End If
                                Case 2
                                    If comp_completed_by = 0 Or stage2_completed_by = 0 Then
                                        idx += 1
                                        Continue For
                                    End If
                                Case 3
                                    If comp_completed_by = 0 Or stage2_completed_by = 0 Or stage3_completed_by = 0 Then
                                        idx += 1
                                        Continue For
                                    End If
                            End Select
                        End If
                        If completed_search = "N" Then
                            Select Case stage_no
                                Case 1
                                    If comp_completed_by > 0 Then
                                        idx += 1
                                        Continue For
                                    End If
                                Case 2
                                    If comp_completed_by > 0 And stage2_completed_by > 0 Then
                                        idx += 1
                                        Continue For
                                    End If
                                Case 3
                                    If comp_completed_by > 0 And stage2_completed_by > 0 And stage3_completed_by > 0 Then
                                        idx += 1
                                        Continue For
                                    End If
                            End Select
                        End If
                    Else
                        Select Case search_stage_text
                            Case "Stage 1"
                                If (completed_search = "Y" And comp_completed_by = 0) Or _
                                  (completed_search = "N" And comp_completed_by > 0) Then
                                    idx += 1
                                    Continue For
                                End If
                            Case "Stage 2"
                                If (completed_search = "Y" And stage2_completed_by = 0) Or _
                                  (completed_search = "N" And stage2_completed_by > 0) Then
                                    idx += 1
                                    Continue For
                                End If
                            Case "Stage 3"
                                If (completed_search = "Y" And stage3_completed_by = 0) Or _
                                  (completed_search = "N" And stage3_completed_by > 0) Then
                                    idx += 1
                                    Continue For
                                End If
                        End Select

                    End If
                End If
                'if stage 2 need to check all/open/closed
                'now stage 2 same as other stages
                'If stage_no = 2 Then
                '    If search_stage_text <> "Stage 2 all" Then
                '        Dim stage2_closed As Boolean = False
                '        stage2_closed = True
                '        Try
                '            Dim stage2_completed_date As Date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(31)
                '        Catch ex As Exception
                '            stage2_closed = False
                '        End Try
                '        Select Case search_stage_text
                '            Case "Stage 2 open"
                '                If stage2_closed = True Then
                '                    idx += 1
                '                    Continue For
                '                End If
                '            Case "Stage 2 closed"
                '                If stage2_closed = False Then
                '                    idx += 1
                '                    Continue For
                '                End If
                '        End Select
                '    End If
                'End If
                If get_letter Then
                    hold = check_held_letter(comp_case_no, recvd_date)
                Else
                    hold = "?"
                End If
                'if name entered check name matches
                If name.Length > 0 Then
                    If comp_case_no > 0 Then
                        param2 = "select name_sur from Debtor " & _
                        " where _rowid = " & comp_case_no
                        Dim name_dataset As DataSet = get_dataset(param1, param2)
                        If no_of_rows = 0 Then
                            idx += 1
                            Continue For
                        End If
                        Dim name_found As String = Trim(LCase(name_dataset.Tables(0).Rows(0).Item(0)))
                        If name_found.Length > name.Length Then
                            If Microsoft.VisualBasic.Left(name_found, name.Length) <> name Then
                                idx += 1
                                Continue For
                            End If
                        Else
                            If Microsoft.VisualBasic.Left(name, name_found.Length) <> name_found Then
                                idx += 1
                                Continue For
                            End If
                        End If
                    End If
                End If
                'if scheme entered need to check if case number is on complaint
                If search_csid_no > 0 And comp_case_no > 0 Then
                    param2 = "select _rowid from Debtor where _rowid = " & comp_case_no & _
                    " and clientschemeID = " & search_csid_no
                    Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                    If no_of_rows = 0 Then
                        idx += 1
                        Continue For
                    End If
                End If
                'if debt type entered check it
                If displayfrm.debt_typecombobox.SelectedIndex > 0 Then
                    If comp_case_no = 0 Then
                        idx += 1
                        Continue For
                    End If
                    param2 = "select clientschemeID from Debtor where _rowid = " & comp_case_no
                    Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                    If no_of_rows = 1 Then
                        Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(0)
                        param2 = "select schemeID from ClientScheme where _rowid =  " & csid
                        Dim cs_dataset As DataSet = get_dataset(param1, param2)
                        If no_of_rows = 1 Then
                            Dim sch_id As Integer = cs_dataset.Tables(0).Rows(0).Item(0)
                            param2 = "select work_type from Scheme where _rowid = " & sch_id
                            Dim sch_dataset As DataSet = get_dataset(param1, param2)
                            If no_of_rows = 1 Then
                                Dim work_type As Integer = sch_dataset.Tables(0).Rows(0).Item(0)
                                Select Case displayfrm.debt_typecombobox.SelectedIndex
                                    Case 1 'CTAX
                                        If work_type <> 2 Then
                                            idx += 1
                                            Continue For
                                        End If
                                    Case 2 'NNDR
                                        If work_type <> 3 Then
                                            idx += 1
                                            Continue For
                                        End If
                                    Case 3 'RTD
                                        If work_type <> 16 And work_type <> 20 Then
                                            idx += 1
                                            Continue For
                                        End If
                                End Select
                            End If

                        End If
                    End If
                End If
                'if recvd date entered just select complaints received in that month
                'need to select all stages received in that month
                If displayfrm.recvd_cbox.Checked Then
                    Dim compare_date As Date
                    Dim include_comp As Boolean = False
                    Select Case search_stage_text
                        Case ""
                            compare_date = recvd_date
                            If Month(compare_date) = Month(displayfrm.recvd_datepicker.Value) _
                                                And Year(compare_date) = Year(displayfrm.recvd_datepicker.Value) Then
                                include_comp = True
                                Exit Select
                            End If
                            Try
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(29)
                            Catch ex As Exception
                                Exit Select
                            End Try
                            If Month(compare_date) = Month(displayfrm.recvd_datepicker.Value) _
                                                And Year(compare_date) = Year(displayfrm.recvd_datepicker.Value) Then
                                include_comp = True
                                Exit Select
                            End If
                            Try
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(38)
                            Catch ex As Exception
                                Exit Select
                            End Try
                            If Month(compare_date) = Month(displayfrm.recvd_datepicker.Value) _
                                                And Year(compare_date) = Year(displayfrm.recvd_datepicker.Value) Then
                                include_comp = True
                            End If
                        Case "Stage 1"
                            compare_date = recvd_date
                            If Month(compare_date) = Month(displayfrm.recvd_datepicker.Value) _
                                                And Year(compare_date) = Year(displayfrm.recvd_datepicker.Value) Then
                                include_comp = True
                            End If
                        Case "Stage 2"
                            compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(29)
                            If Month(compare_date) = Month(displayfrm.recvd_datepicker.Value) _
                                                And Year(compare_date) = Year(displayfrm.recvd_datepicker.Value) Then
                                include_comp = True
                            End If
                        Case "Stage 3"
                            compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(38)
                            If Month(compare_date) = Month(displayfrm.recvd_datepicker.Value) _
                            And Year(compare_date) = Year(displayfrm.recvd_datepicker.Value) Then
                                include_comp = True
                            End If
                    End Select
                    If include_comp = False Then
                        idx += 1
                        Continue For
                    End If
                End If
                'if comp date entered just select complaints resolved in that month
                If displayfrm.comp_cbox.Checked Then
                    Dim compare_date As Date = Nothing
                    Dim include_comp As Boolean = False
                    Select Case search_stage_text
                        Case ""
                            If comp_completed_by > 0 Then
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(14)
                                If Month(compare_date) = Month(displayfrm.comp_datepicker.Value) _
                                                  And Year(compare_date) = Year(displayfrm.comp_datepicker.Value) Then
                                    include_comp = True
                                    Exit Select
                                End If
                            End If
                            If stage2_completed_by > 0 Then
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(31)
                                If Month(compare_date) = Month(displayfrm.comp_datepicker.Value) _
                                          And Year(compare_date) = Year(displayfrm.comp_datepicker.Value) Then
                                    include_comp = True
                                    Exit Select
                                End If
                            End If
                            If stage3_completed_by > 0 Then
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(40)
                                If Month(compare_date) = Month(displayfrm.comp_datepicker.Value) _
                                                                          And Year(compare_date) = Year(displayfrm.comp_datepicker.Value) Then
                                    include_comp = True
                                End If
                            End If
                        Case "Stage 1"
                            If comp_completed_by > 0 Then
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(14)
                                If Month(compare_date) = Month(displayfrm.comp_datepicker.Value) _
                                                  And Year(compare_date) = Year(displayfrm.comp_datepicker.Value) Then
                                    include_comp = True
                                End If
                            End If
                        Case "Stage 2"
                            If stage2_completed_by > 0 Then
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(31)
                                If Month(compare_date) = Month(displayfrm.comp_datepicker.Value) _
                                          And Year(compare_date) = Year(displayfrm.comp_datepicker.Value) Then
                                    include_comp = True
                                End If
                            End If
                        Case "Stage 3"
                            If stage3_completed_by > 0 Then
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(40)
                                If Month(compare_date) = Month(displayfrm.comp_datepicker.Value) _
                                                                          And Year(compare_date) = Year(displayfrm.comp_datepicker.Value) Then
                                    include_comp = True
                                End If
                            End If
                    End Select
                    If include_comp = False Then
                        idx += 1
                        Continue For
                    End If
                End If
                'If held_search <> "A" Then
                '    If hold <> held_search Then
                '        idx += 1
                '        Continue For
                '    End If
                'End If
                'If search_no = 6 Then
                '    'only include complaints 6+ days old with no hold letter
                '    'or 17+ days old with held letter
                '    If hold = "N" And DateDiff("d", recvd_date, Now) < 6 Then
                '        idx += 1
                '        Continue For
                '    End If
                '    If hold = "Y" And DateDiff("d", recvd_date, Now) < 17 Then
                '        idx += 1
                '        Continue For
                '    End If
                'End If
                Dim stage1_comp_date_string As String = ""
                If comp_completed_by > 0 Then
                    stage1_comp_date_string = Format(PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(14), "yyyy-MM-dd")
                    days_taken = DateDiff("d", PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1), _
                         PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(14)) - _
                    (DateDiff("w", PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1), _
                         PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(14)) * 2)
                    If Weekday(PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1)) > _
                        Weekday(PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(14)) Then
                        days_taken = days_taken - 2
                    End If
                    If days_taken = 0 Then days_taken = 1
                Else
                    days_taken = DateDiff("d", PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1), Now) - _
                    (DateDiff("w", PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1), Now) * 2)
                    If Weekday(PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1)) > Weekday(Now) Then
                        days_taken = days_taken - 2
                    End If
                End If
                'get client name
                client_name = ""
                Dim cl_turn_days As Integer = 10
                If PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(9) = 101 Then
                    cl_turn_days = 40
                ElseIf PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(5) > 0 Then
                    Dim cl_no As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(5)
                    param1 = "onestep"
                    param2 = "select name from Client" & _
                    " where _rowid = " & cl_no
                    Dim cl_dataset As DataSet = get_dataset(param1, param2)
                    If no_of_rows = 1 Then
                        client_name = cl_dataset.Tables(0).Rows(0).Item(0)
                    Else
                        client_name = ""
                    End If
                    If client_turnfrm.Client_turnaroundTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Client_turnaround, cl_no) > 0 Then
                        cl_turn_days = Me.PraiseAndComplaintsSQLDataSet.Client_turnaround.Rows(0).Item(1)
                    End If
                End If
                'days left
                If comp_completed_by > 0 Or legal_ins_flag = "Y" Then
                    days_left = ""
                Else
                    days_left = cl_turn_days - days_taken
                End If
                'get type
                type = ""
                'received from
                Dim recvd_from2 As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(3)
                Received_fromTableAdapter.FillBy(PraiseAndComplaintsSQLDataSet.Received_from, recvd_from2)
                type = PraiseAndComplaintsSQLDataSet.Received_from.Rows(0).Item(1)

                'get category
                category = ""
                category = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(8) & " " _
                           & PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(9)


                If agent_no = 9999 Then
                    agent = "Various"
                Else
                    If agent_no > 0 Then
                        param1 = "onestep"
                        param2 = "select name_sur, name_fore, status from Bailiff" & _
                        " where _rowid = " & agent_no
                        Dim bail_dataset As DataSet = get_dataset(param1, param2)
                        If no_of_rows = 1 Then
                            Dim forename As String
                            Try
                                forename = Trim(bail_dataset.Tables(0).Rows(0).Item(1))
                            Catch ex As Exception
                                forename = ""
                            End Try
                            agent = Trim(bail_dataset.Tables(0).Rows(0).Item(0)) & _
                                                                 ", " & forename
                            If bail_dataset.Tables(0).Rows(0).Item(2) = "C" Then
                                agent = agent & " (C)"
                            End If
                            agent = Replace(agent, ",", " ")
                        Else
                            agent = ""
                        End If
                    Else
                        dept_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(10)
                        'If dept_code > 0 Then
                        '    updatecmpfrm.DepartmentsTableAdapter.FillBy(updatecmpfrm.DepartmentsDataSet.Departments, dept_code)
                        '    agent = updatecmpfrm.DepartmentsDataSet.Tables(0).Rows(0).Item(1)
                        'End If
                    End If
                End If
                idx += 1
                Dim stage2_start_date_string As String = ""
                Dim stage2_comp_date_string As String = ""
                Dim stage3_start_date_string As String = ""
                Dim stage3_comp_date_string As String = ""
                If stage_no = 2 Then
                    stage2_start_date_string = Format(stage2_start_date, "yyyy-MM-dd")
                    If stage2_completed_date <> Nothing Then
                        stage2_comp_date_string = Format(stage2_completed_date, "yyyy-MM-dd")
                    End If
                End If
                If stage_no = 3 Then
                    stage2_start_date_string = Format(stage2_start_date, "yyyy-MM-dd")
                    If stage2_completed_date <> Nothing Then
                        stage2_comp_date_string = Format(stage2_completed_date, "yyyy-MM-dd")
                    End If
                    stage3_start_date_string = Format(stage3_start_date, "yyyy-MM-dd")
                    If stage3_completed_date <> Nothing Then
                        stage3_comp_date_string = Format(stage3_completed_date, "yyyy-MM-dd")
                    End If
                End If
                gtot_cases += 1
                DataGridView1.Rows.Add(disp_comp_officer, disp_Comp_no, comp_case_no, legal_ins_flag, Format(recvd_date, "yyyy-MM-dd"), _
                stage1_comp_date_string, stage2_start_date_string, stage2_comp_date_string, _
                stage3_start_date_string, stage3_comp_date_string, days_taken, days_left, _
                client_name, type, against, agent, category, hold, founded)
                file = file & disp_comp_officer & "," & disp_Comp_no & "," & comp_case_no & "," & recvd_date & "," & _
                stage1_comp_date_string & "," & stage2_start_date_string & "," & stage2_comp_date_string & "," & _
                stage3_start_date_string & "," & stage3_comp_date_string & "," & days_taken & "," & days_left & "," & client_name & "," & type & "," & _
                 against & "," & agent & "," & category & "," & hold & "," & founded & vbNewLine

                If search_no = 6 Then
                    mainfrm.ProgressBar1.Value = (idx / no_of_complaints) * 100
                Else
                    displayfrm.ProgressBar1.Value = (idx / no_of_complaints) * 100
                End If
            Next
            Me.Text = "Display Complaints List - " & gtot_cases & " cases"
            If DataGridView1.RowCount = 0 Then
                If search_no = 4 Then
                    MsgBox("There are no complaints for " & search_agent_name)
                Else
                    MsgBox("There are no complaints for selected criteria")
                End If

                Me.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub DataGridView1_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellEnter
        selected_comp_no = DataGridView1.Rows(e.RowIndex).Cells(1).Value
    End Sub

    Private Sub showbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dispbtn.Click
        comp_no = selected_comp_no
        displayfrm3.ShowDialog()
    End Sub

    
    Private Sub updbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updbtn.Click
        comp_no = selected_comp_no
        updatecmpfrm.ShowDialog()
    End Sub

    Private Sub ComplaintsBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.ComplaintsBindingSource.EndEdit()
        Me.ComplaintsTableAdapter.Update(Me.PraiseAndComplaintsSQLDataSet.Complaints)

    End Sub

    Private Sub excelbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles excelbtn.Click
        With SaveFileDialog1
            .Title = "Save to excel"
            .Filter = "excel|*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "complaints.xls"
        End With
        If Len(file) = 0 Then
            MessageBox.Show("There are no cases selected")
        Else
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
            End If
        End If
    End Sub
End Class
Public Class againstfrm

    Private Sub againstfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Complaint_against' table. You can move, or remove it, as needed.
        Me.Complaint_againstTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_against)
        'TODO: This line of code loads data into the 'Complaint_againstDataSet.Complaint_against' table. You can move, or remove it, as needed.
        against_orig_no_rows = DataGridView1.RowCount
    End Sub
    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.ColumnIndex = 1 And e.RowIndex >= 0 Then
            against_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            against_text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            If DataGridView1.Rows(e.RowIndex).IsNewRow Then Return
            If e.RowIndex < 5 Then Return
            Try
                Me.Complaint_againstTableAdapter.UpdateQuery(against_text, against_code)
                log_text = "Complaint against amended - " & against_code & _
                                                  " from " & orig_text & " to " & against_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate against type entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter

        against_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        orig_text = DataGridView1.Rows(e.RowIndex).Cells(1).Value

    End Sub
    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles DataGridView1.RowsRemoved
        If e.RowIndex > 3 Then
            Try
                Try
                    Me.Complaint_againstTableAdapter.DeleteQuery(against_code)
                Catch
                    MessageBox.Show("Can't delete as it's used in a complaint")
                End Try
                log_text = "Complaint against deleted - " & against_code & " name " & orig_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If

    End Sub

    Private Sub Complaint_againstBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.Complaint_againstBindingSource.EndEdit()
        Me.Complaint_againstTableAdapter.Update(Me.PraiseAndComplaintsSQLDataSet.Complaint_against)

    End Sub
End Class
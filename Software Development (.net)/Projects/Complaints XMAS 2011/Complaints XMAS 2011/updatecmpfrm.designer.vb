<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class updatecmpfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Comp_noLabel As System.Windows.Forms.Label
        Dim Comp_dateLabel As System.Windows.Forms.Label
        Dim Comp_case_noLabel As System.Windows.Forms.Label
        Dim Comp_cat_numberLabel As System.Windows.Forms.Label
        Dim Comp_textLabel As System.Windows.Forms.Label
        Dim Recpt_textLabel As System.Windows.Forms.Label
        Dim Recvd_textLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Comp_against_codeLabel As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Comp_entered_byLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Comp_priorityLabel As System.Windows.Forms.Label
        Dim Comp_old_comp_noLabel As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label19 As System.Windows.Forms.Label
        Dim Label21 As System.Windows.Forms.Label
        Dim Label22 As System.Windows.Forms.Label
        Dim Label25 As System.Windows.Forms.Label
        Me.updbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.recpt_combobox = New System.Windows.Forms.ComboBox
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cl_ComboBox = New System.Windows.Forms.ComboBox
        Me.Cat_textComboBox = New System.Windows.Forms.ComboBox
        Me.agentComboBox = New System.Windows.Forms.ComboBox
        Me.invComboBox = New System.Windows.Forms.ComboBox
        Me.entered_byTextBox = New System.Windows.Forms.TextBox
        Me.notcomprbtn = New System.Windows.Forms.RadioButton
        Me.comprbtn = New System.Windows.Forms.RadioButton
        Me.compdatetimepicker = New System.Windows.Forms.DateTimePicker
        Me.foundedrbtn = New System.Windows.Forms.RadioButton
        Me.unfoundedrbtn = New System.Windows.Forms.RadioButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.openrbtn = New System.Windows.Forms.RadioButton
        Me.compdatelbl = New System.Windows.Forms.Label
        Me.compbylbl = New System.Windows.Forms.Label
        Me.cat_codeComboBox = New System.Windows.Forms.ComboBox
        Me.recvd_textComboBox = New System.Windows.Forms.ComboBox
        Me.comp_case_noTextBox = New System.Windows.Forms.TextBox
        Me.comp_against_codeComboBox = New System.Windows.Forms.ComboBox
        Me.Comp_dateTextBox = New System.Windows.Forms.TextBox
        Me.Comp_noTextBox = New System.Windows.Forms.TextBox
        Me.Comp_textTextBox = New System.Windows.Forms.TextBox
        Me.Comp_responseTextBox = New System.Windows.Forms.TextBox
        Me.actions_ComboBox = New System.Windows.Forms.ComboBox
        Me.addbtn = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.stage_ComboBox = New System.Windows.Forms.ComboBox
        Me.stage_Label = New System.Windows.Forms.Label
        Me.cor_nameComboBox = New System.Windows.Forms.ComboBox
        Me.doc_ListBox = New System.Windows.Forms.ListBox
        Me.doc_textbox = New System.Windows.Forms.TextBox
        Me.hold_name_ComboBox = New System.Windows.Forms.ComboBox
        Me.reason_label = New System.Windows.Forms.Label
        Me.deldocbtn = New System.Windows.Forms.Button
        Me.Comp_priorityCheckBox = New System.Windows.Forms.CheckBox
        Me.prevcomplbl = New System.Windows.Forms.Label
        Me.compbycombobox = New System.Windows.Forms.ComboBox
        Me.old_comp_notextbox = New System.Windows.Forms.TextBox
        Me.costs_cancel_tbox = New System.Windows.Forms.TextBox
        Me.compensation_tbox = New System.Windows.Forms.TextBox
        Me.respcbox = New System.Windows.Forms.CheckBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.stage2_gbox = New System.Windows.Forms.GroupBox
        Me.stage2_completed_by_combobox = New System.Windows.Forms.ComboBox
        Me.stage2_completed_datepicker = New System.Windows.Forms.DateTimePicker
        Me.stage2_start_datepicker = New System.Windows.Forms.DateTimePicker
        Me.stage1_gbox = New System.Windows.Forms.GroupBox
        Me.Test_LogDataSet = New Complaints_XMAS_2011.DataSet1
        Me.Test_LogTableAdapter = New Complaints_XMAS_2011.DataSet1TableAdapters.LogTableAdapter
        Me.gender_combobox = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.eth_combobox = New System.Windows.Forms.ComboBox
        Me.dob_textbox = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.debt_typelbl = New System.Windows.Forms.Label
        Me.ack_datepicker = New System.Windows.Forms.DateTimePicker
        Me.ack_cbox = New System.Windows.Forms.CheckBox
        Me.holding_cbox = New System.Windows.Forms.CheckBox
        Me.holding_datepicker = New System.Windows.Forms.DateTimePicker
        Me.ack_gbox = New System.Windows.Forms.GroupBox
        Me.holding_gbox = New System.Windows.Forms.GroupBox
        Me.stage2_ack_gbox = New System.Windows.Forms.GroupBox
        Me.stage2_ack_datepicker = New System.Windows.Forms.DateTimePicker
        Me.stage2_ack_cbox = New System.Windows.Forms.CheckBox
        Me.stage2_holding_gbox = New System.Windows.Forms.GroupBox
        Me.stage2_holding_datepicker = New System.Windows.Forms.DateTimePicker
        Me.stage2_holding_cbox = New System.Windows.Forms.CheckBox
        Me.stage3_gbox = New System.Windows.Forms.GroupBox
        Me.stage3_completed_by_combobox = New System.Windows.Forms.ComboBox
        Me.stage3_completed_datepicker = New System.Windows.Forms.DateTimePicker
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.stage3_start_datepicker = New System.Windows.Forms.DateTimePicker
        Me.Label18 = New System.Windows.Forms.Label
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.Received_fromTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
        Me.Complaint_againstBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Complaint_againstTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
        Me.InvestigatorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InvestigatorsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
        Me.ActionsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ActionsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ActionsTableAdapter
        Me.Corrective_actionsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Corrective_actionsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Corrective_actionsTableAdapter
        Me.Detail_historyBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Detail_historyTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.detail_historyTableAdapter
        Me.Complaint_categoriesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Complaint_categoriesTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_categoriesTableAdapter
        Me.Receipt_typeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Receipt_typeTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Receipt_typeTableAdapter
        Me.Hold_reasonBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Hold_reasonTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Hold_reasonTableAdapter
        Me.ResponseBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ResponseTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.responseTableAdapter
        Me.ComplaintsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComplaintsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ComplaintsTableAdapter
        Me.comp_against2_codecombobox = New System.Windows.Forms.ComboBox
        Me.agent2_combobox = New System.Windows.Forms.ComboBox
        Me.Complaints_resp_clientsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Complaints_resp_clientsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaints_resp_clientsTableAdapter
        Me.legalcbox = New System.Windows.Forms.CheckBox
        Me.legalgbox = New System.Windows.Forms.GroupBox
        Me.inscbox = New System.Windows.Forms.CheckBox
        Me.solicitor_cbox = New System.Windows.Forms.CheckBox
        Me.monetary_risktbox = New System.Windows.Forms.TextBox
        Me.ins_datepicker = New System.Windows.Forms.DateTimePicker
        Me.solicitor_datepicker = New System.Windows.Forms.DateTimePicker
        Me.costs_reasonbtn = New System.Windows.Forms.Button
        Comp_noLabel = New System.Windows.Forms.Label
        Comp_dateLabel = New System.Windows.Forms.Label
        Comp_case_noLabel = New System.Windows.Forms.Label
        Comp_cat_numberLabel = New System.Windows.Forms.Label
        Comp_textLabel = New System.Windows.Forms.Label
        Recpt_textLabel = New System.Windows.Forms.Label
        Recvd_textLabel = New System.Windows.Forms.Label
        Label1 = New System.Windows.Forms.Label
        Comp_against_codeLabel = New System.Windows.Forms.Label
        Label5 = New System.Windows.Forms.Label
        Label3 = New System.Windows.Forms.Label
        Comp_entered_byLabel = New System.Windows.Forms.Label
        Label2 = New System.Windows.Forms.Label
        Label4 = New System.Windows.Forms.Label
        Label6 = New System.Windows.Forms.Label
        Label7 = New System.Windows.Forms.Label
        Comp_priorityLabel = New System.Windows.Forms.Label
        Comp_old_comp_noLabel = New System.Windows.Forms.Label
        Label10 = New System.Windows.Forms.Label
        Label11 = New System.Windows.Forms.Label
        Label19 = New System.Windows.Forms.Label
        Label21 = New System.Windows.Forms.Label
        Label22 = New System.Windows.Forms.Label
        Label25 = New System.Windows.Forms.Label
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.stage2_gbox.SuspendLayout()
        Me.stage1_gbox.SuspendLayout()
        CType(Me.Test_LogDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ack_gbox.SuspendLayout()
        Me.holding_gbox.SuspendLayout()
        Me.stage2_ack_gbox.SuspendLayout()
        Me.stage2_holding_gbox.SuspendLayout()
        Me.stage3_gbox.SuspendLayout()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Complaint_againstBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Corrective_actionsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Detail_historyBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Complaint_categoriesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Receipt_typeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Hold_reasonBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ResponseBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Complaints_resp_clientsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.legalgbox.SuspendLayout()
        Me.SuspendLayout()
        '
        'Comp_noLabel
        '
        Comp_noLabel.AutoSize = True
        Comp_noLabel.Location = New System.Drawing.Point(33, 9)
        Comp_noLabel.Name = "Comp_noLabel"
        Comp_noLabel.Size = New System.Drawing.Size(70, 13)
        Comp_noLabel.TabIndex = 4
        Comp_noLabel.Text = "complaint no:"
        '
        'Comp_dateLabel
        '
        Comp_dateLabel.AutoSize = True
        Comp_dateLabel.Location = New System.Drawing.Point(149, 9)
        Comp_dateLabel.Name = "Comp_dateLabel"
        Comp_dateLabel.Size = New System.Drawing.Size(77, 13)
        Comp_dateLabel.TabIndex = 2
        Comp_dateLabel.Text = "Date received:"
        '
        'Comp_case_noLabel
        '
        Comp_case_noLabel.AutoSize = True
        Comp_case_noLabel.Location = New System.Drawing.Point(256, 101)
        Comp_case_noLabel.Name = "Comp_case_noLabel"
        Comp_case_noLabel.Size = New System.Drawing.Size(49, 13)
        Comp_case_noLabel.TabIndex = 4
        Comp_case_noLabel.Text = "Case no:"
        '
        'Comp_cat_numberLabel
        '
        Comp_cat_numberLabel.AutoSize = True
        Comp_cat_numberLabel.Location = New System.Drawing.Point(25, 166)
        Comp_cat_numberLabel.Name = "Comp_cat_numberLabel"
        Comp_cat_numberLabel.Size = New System.Drawing.Size(64, 13)
        Comp_cat_numberLabel.TabIndex = 8
        Comp_cat_numberLabel.Text = "Cat number:"
        '
        'Comp_textLabel
        '
        Comp_textLabel.AutoSize = True
        Comp_textLabel.Location = New System.Drawing.Point(123, 230)
        Comp_textLabel.Name = "Comp_textLabel"
        Comp_textLabel.Size = New System.Drawing.Size(103, 13)
        Comp_textLabel.TabIndex = 13
        Comp_textLabel.Text = "Details of Complaint:"
        '
        'Recpt_textLabel
        '
        Recpt_textLabel.AutoSize = True
        Recpt_textLabel.Location = New System.Drawing.Point(406, 9)
        Recpt_textLabel.Name = "Recpt_textLabel"
        Recpt_textLabel.Size = New System.Drawing.Size(80, 13)
        Recpt_textLabel.TabIndex = 14
        Recpt_textLabel.Text = "Form of receipt:"
        '
        'Recvd_textLabel
        '
        Recvd_textLabel.AutoSize = True
        Recvd_textLabel.Location = New System.Drawing.Point(517, 9)
        Recvd_textLabel.Name = "Recvd_textLabel"
        Recvd_textLabel.Size = New System.Drawing.Size(68, 13)
        Recvd_textLabel.TabIndex = 16
        Recvd_textLabel.Text = "Complainant:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(388, 101)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(36, 13)
        Label1.TabIndex = 18
        Label1.Text = "Client:"
        '
        'Comp_against_codeLabel
        '
        Comp_against_codeLabel.AutoSize = True
        Comp_against_codeLabel.Location = New System.Drawing.Point(697, 73)
        Comp_against_codeLabel.Name = "Comp_against_codeLabel"
        Comp_against_codeLabel.Size = New System.Drawing.Size(103, 13)
        Comp_against_codeLabel.TabIndex = 20
        Comp_against_codeLabel.Text = "Comp made against:"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Location = New System.Drawing.Point(862, 73)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(61, 13)
        Label5.TabIndex = 27
        Label5.Text = "Staff/bailiff:"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Location = New System.Drawing.Point(670, 166)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(93, 13)
        Label3.TabIndex = 28
        Label3.Text = "Complaints officer:"
        '
        'Comp_entered_byLabel
        '
        Comp_entered_byLabel.AutoSize = True
        Comp_entered_byLabel.Location = New System.Drawing.Point(272, 9)
        Comp_entered_byLabel.Name = "Comp_entered_byLabel"
        Comp_entered_byLabel.Size = New System.Drawing.Size(61, 13)
        Comp_entered_byLabel.TabIndex = 29
        Comp_entered_byLabel.Text = "Entered by:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Location = New System.Drawing.Point(494, 230)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(107, 13)
        Label2.TabIndex = 14
        Label2.Text = "Complaint Response:"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Location = New System.Drawing.Point(547, 559)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(94, 13)
        Label4.TabIndex = 44
        Label4.Text = "Action to Resolve:"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Location = New System.Drawing.Point(604, 544)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(179, 13)
        Label6.TabIndex = 48
        Label6.Text = "Recommendation/Corrective Action:"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Location = New System.Drawing.Point(47, 414)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(93, 13)
        Label7.TabIndex = 51
        Label7.Text = "No of Documents:"
        '
        'Comp_priorityLabel
        '
        Comp_priorityLabel.AccessibleRole = System.Windows.Forms.AccessibleRole.TitleBar
        Comp_priorityLabel.AutoSize = True
        Comp_priorityLabel.Location = New System.Drawing.Point(811, 166)
        Comp_priorityLabel.Name = "Comp_priorityLabel"
        Comp_priorityLabel.Size = New System.Drawing.Size(58, 13)
        Comp_priorityLabel.TabIndex = 62
        Comp_priorityLabel.Text = "PRIORITY"
        '
        'Comp_old_comp_noLabel
        '
        Comp_old_comp_noLabel.AutoSize = True
        Comp_old_comp_noLabel.Location = New System.Drawing.Point(763, 230)
        Comp_old_comp_noLabel.Name = "Comp_old_comp_noLabel"
        Comp_old_comp_noLabel.Size = New System.Drawing.Size(70, 13)
        Comp_old_comp_noLabel.TabIndex = 64
        Comp_old_comp_noLabel.Text = "Old comp no:"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Location = New System.Drawing.Point(540, 607)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(86, 13)
        Label10.TabIndex = 66
        Label10.Text = "Costs Cancelled:"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Location = New System.Drawing.Point(688, 607)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(77, 13)
        Label11.TabIndex = 68
        Label11.Text = "Compensation:"
        '
        'Label19
        '
        Label19.AutoSize = True
        Label19.Location = New System.Drawing.Point(576, 101)
        Label19.Name = "Label19"
        Label19.Size = New System.Drawing.Size(118, 13)
        Label19.TabIndex = 80
        Label19.Text = "Main complaint against:"
        '
        'Label21
        '
        Label21.AutoSize = True
        Label21.Location = New System.Drawing.Point(567, 136)
        Label21.Name = "Label21"
        Label21.Size = New System.Drawing.Size(127, 13)
        Label21.TabIndex = 79
        Label21.Text = "Secondary comp against:"
        '
        'Label22
        '
        Label22.AutoSize = True
        Label22.Location = New System.Drawing.Point(709, 562)
        Label22.Name = "Label22"
        Label22.Size = New System.Drawing.Size(91, 13)
        Label22.TabIndex = 81
        Label22.Text = "Corrective Action:"
        '
        'Label25
        '
        Label25.AutoSize = True
        Label25.Location = New System.Drawing.Point(21, 123)
        Label25.Name = "Label25"
        Label25.Size = New System.Drawing.Size(78, 13)
        Label25.TabIndex = 85
        Label25.Text = "Monetary Risk:"
        '
        'updbtn
        '
        Me.updbtn.Location = New System.Drawing.Point(824, 661)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(112, 23)
        Me.updbtn.TabIndex = 37
        Me.updbtn.Text = "Update and Exit"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(22, 661)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(124, 23)
        Me.exitbtn.TabIndex = 36
        Me.exitbtn.Text = "Exit without Updating"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'recpt_combobox
        '
        Me.recpt_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.recpt_combobox.FormattingEnabled = True
        Me.recpt_combobox.Location = New System.Drawing.Point(396, 40)
        Me.recpt_combobox.Name = "recpt_combobox"
        Me.recpt_combobox.Size = New System.Drawing.Size(90, 21)
        Me.recpt_combobox.TabIndex = 0
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'cl_ComboBox
        '
        Me.cl_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cl_ComboBox.FormattingEnabled = True
        Me.cl_ComboBox.Location = New System.Drawing.Point(325, 128)
        Me.cl_ComboBox.Name = "cl_ComboBox"
        Me.cl_ComboBox.Size = New System.Drawing.Size(235, 21)
        Me.cl_ComboBox.TabIndex = 6
        '
        'Cat_textComboBox
        '
        Me.Cat_textComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Cat_textComboBox.FormattingEnabled = True
        Me.Cat_textComboBox.Location = New System.Drawing.Point(118, 193)
        Me.Cat_textComboBox.Name = "Cat_textComboBox"
        Me.Cat_textComboBox.Size = New System.Drawing.Size(489, 21)
        Me.Cat_textComboBox.TabIndex = 12
        '
        'agentComboBox
        '
        Me.agentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.agentComboBox.FormattingEnabled = True
        Me.agentComboBox.Location = New System.Drawing.Point(837, 98)
        Me.agentComboBox.Name = "agentComboBox"
        Me.agentComboBox.Size = New System.Drawing.Size(121, 21)
        Me.agentComboBox.TabIndex = 8
        '
        'invComboBox
        '
        Me.invComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.invComboBox.FormattingEnabled = True
        Me.invComboBox.Location = New System.Drawing.Point(651, 193)
        Me.invComboBox.Name = "invComboBox"
        Me.invComboBox.Size = New System.Drawing.Size(121, 21)
        Me.invComboBox.TabIndex = 13
        '
        'entered_byTextBox
        '
        Me.entered_byTextBox.Location = New System.Drawing.Point(275, 40)
        Me.entered_byTextBox.Name = "entered_byTextBox"
        Me.entered_byTextBox.ReadOnly = True
        Me.entered_byTextBox.Size = New System.Drawing.Size(100, 20)
        Me.entered_byTextBox.TabIndex = 30
        Me.entered_byTextBox.TabStop = False
        '
        'notcomprbtn
        '
        Me.notcomprbtn.AutoSize = True
        Me.notcomprbtn.Checked = True
        Me.notcomprbtn.Location = New System.Drawing.Point(6, 22)
        Me.notcomprbtn.Name = "notcomprbtn"
        Me.notcomprbtn.Size = New System.Drawing.Size(95, 17)
        Me.notcomprbtn.TabIndex = 12
        Me.notcomprbtn.TabStop = True
        Me.notcomprbtn.Text = "Not Completed"
        Me.notcomprbtn.UseVisualStyleBackColor = True
        '
        'comprbtn
        '
        Me.comprbtn.AutoSize = True
        Me.comprbtn.Location = New System.Drawing.Point(6, 45)
        Me.comprbtn.Name = "comprbtn"
        Me.comprbtn.Size = New System.Drawing.Size(75, 17)
        Me.comprbtn.TabIndex = 0
        Me.comprbtn.Text = "Completed"
        Me.comprbtn.UseVisualStyleBackColor = True
        '
        'compdatetimepicker
        '
        Me.compdatetimepicker.Location = New System.Drawing.Point(159, 36)
        Me.compdatetimepicker.Name = "compdatetimepicker"
        Me.compdatetimepicker.Size = New System.Drawing.Size(128, 20)
        Me.compdatetimepicker.TabIndex = 21
        '
        'foundedrbtn
        '
        Me.foundedrbtn.AutoSize = True
        Me.foundedrbtn.Location = New System.Drawing.Point(6, 42)
        Me.foundedrbtn.Name = "foundedrbtn"
        Me.foundedrbtn.Size = New System.Drawing.Size(67, 17)
        Me.foundedrbtn.TabIndex = 33
        Me.foundedrbtn.Text = "Founded"
        Me.foundedrbtn.UseVisualStyleBackColor = True
        '
        'unfoundedrbtn
        '
        Me.unfoundedrbtn.AutoSize = True
        Me.unfoundedrbtn.Location = New System.Drawing.Point(6, 67)
        Me.unfoundedrbtn.Name = "unfoundedrbtn"
        Me.unfoundedrbtn.Size = New System.Drawing.Size(78, 17)
        Me.unfoundedrbtn.TabIndex = 34
        Me.unfoundedrbtn.Text = "Unfounded"
        Me.unfoundedrbtn.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.comprbtn)
        Me.GroupBox1.Controls.Add(Me.notcomprbtn)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 16)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(120, 84)
        Me.GroupBox1.TabIndex = 19
        Me.GroupBox1.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.openrbtn)
        Me.GroupBox2.Controls.Add(Me.foundedrbtn)
        Me.GroupBox2.Controls.Add(Me.unfoundedrbtn)
        Me.GroupBox2.Location = New System.Drawing.Point(359, 533)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(133, 100)
        Me.GroupBox2.TabIndex = 31
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Founded/Unfounded"
        '
        'openrbtn
        '
        Me.openrbtn.AutoSize = True
        Me.openrbtn.Checked = True
        Me.openrbtn.Location = New System.Drawing.Point(6, 19)
        Me.openrbtn.Name = "openrbtn"
        Me.openrbtn.Size = New System.Drawing.Size(59, 17)
        Me.openrbtn.TabIndex = 33
        Me.openrbtn.TabStop = True
        Me.openrbtn.Text = "Not set"
        Me.openrbtn.UseVisualStyleBackColor = True
        '
        'compdatelbl
        '
        Me.compdatelbl.AutoSize = True
        Me.compdatelbl.Location = New System.Drawing.Point(185, 16)
        Me.compdatelbl.Name = "compdatelbl"
        Me.compdatelbl.Size = New System.Drawing.Size(86, 13)
        Me.compdatelbl.TabIndex = 0
        Me.compdatelbl.Text = "Completed Date:"
        '
        'compbylbl
        '
        Me.compbylbl.AutoSize = True
        Me.compbylbl.Location = New System.Drawing.Point(197, 63)
        Me.compbylbl.Name = "compbylbl"
        Me.compbylbl.Size = New System.Drawing.Size(74, 13)
        Me.compbylbl.TabIndex = 14
        Me.compbylbl.Text = "Completed by:"
        '
        'cat_codeComboBox
        '
        Me.cat_codeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cat_codeComboBox.FormattingEnabled = True
        Me.cat_codeComboBox.Items.AddRange(New Object() {"A Minor", "B Medial", "C Major"})
        Me.cat_codeComboBox.Location = New System.Drawing.Point(28, 193)
        Me.cat_codeComboBox.Name = "cat_codeComboBox"
        Me.cat_codeComboBox.Size = New System.Drawing.Size(84, 21)
        Me.cat_codeComboBox.TabIndex = 11
        '
        'recvd_textComboBox
        '
        Me.recvd_textComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.recvd_textComboBox.FormattingEnabled = True
        Me.recvd_textComboBox.Location = New System.Drawing.Point(520, 39)
        Me.recvd_textComboBox.Name = "recvd_textComboBox"
        Me.recvd_textComboBox.Size = New System.Drawing.Size(121, 21)
        Me.recvd_textComboBox.TabIndex = 1
        '
        'comp_case_noTextBox
        '
        Me.comp_case_noTextBox.Location = New System.Drawing.Point(247, 129)
        Me.comp_case_noTextBox.Name = "comp_case_noTextBox"
        Me.comp_case_noTextBox.Size = New System.Drawing.Size(72, 20)
        Me.comp_case_noTextBox.TabIndex = 5
        '
        'comp_against_codeComboBox
        '
        Me.comp_against_codeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comp_against_codeComboBox.FormattingEnabled = True
        Me.comp_against_codeComboBox.Location = New System.Drawing.Point(700, 98)
        Me.comp_against_codeComboBox.Name = "comp_against_codeComboBox"
        Me.comp_against_codeComboBox.Size = New System.Drawing.Size(121, 21)
        Me.comp_against_codeComboBox.TabIndex = 7
        '
        'Comp_dateTextBox
        '
        Me.Comp_dateTextBox.Location = New System.Drawing.Point(142, 41)
        Me.Comp_dateTextBox.Name = "Comp_dateTextBox"
        Me.Comp_dateTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Comp_dateTextBox.TabIndex = 41
        Me.Comp_dateTextBox.TabStop = False
        '
        'Comp_noTextBox
        '
        Me.Comp_noTextBox.Location = New System.Drawing.Point(32, 39)
        Me.Comp_noTextBox.Name = "Comp_noTextBox"
        Me.Comp_noTextBox.ReadOnly = True
        Me.Comp_noTextBox.Size = New System.Drawing.Size(81, 20)
        Me.Comp_noTextBox.TabIndex = 42
        Me.Comp_noTextBox.TabStop = False
        '
        'Comp_textTextBox
        '
        Me.Comp_textTextBox.Location = New System.Drawing.Point(26, 246)
        Me.Comp_textTextBox.Multiline = True
        Me.Comp_textTextBox.Name = "Comp_textTextBox"
        Me.Comp_textTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Comp_textTextBox.Size = New System.Drawing.Size(321, 114)
        Me.Comp_textTextBox.TabIndex = 15
        '
        'Comp_responseTextBox
        '
        Me.Comp_responseTextBox.Location = New System.Drawing.Point(396, 246)
        Me.Comp_responseTextBox.Multiline = True
        Me.Comp_responseTextBox.Name = "Comp_responseTextBox"
        Me.Comp_responseTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Comp_responseTextBox.Size = New System.Drawing.Size(318, 114)
        Me.Comp_responseTextBox.TabIndex = 16
        '
        'actions_ComboBox
        '
        Me.actions_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.actions_ComboBox.FormattingEnabled = True
        Me.actions_ComboBox.Location = New System.Drawing.Point(503, 577)
        Me.actions_ComboBox.Name = "actions_ComboBox"
        Me.actions_ComboBox.Size = New System.Drawing.Size(191, 21)
        Me.actions_ComboBox.TabIndex = 32
        '
        'addbtn
        '
        Me.addbtn.Location = New System.Drawing.Point(32, 366)
        Me.addbtn.Name = "addbtn"
        Me.addbtn.Size = New System.Drawing.Size(123, 23)
        Me.addbtn.TabIndex = 21
        Me.addbtn.Text = "Add a document"
        Me.addbtn.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'stage_ComboBox
        '
        Me.stage_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.stage_ComboBox.FormattingEnabled = True
        Me.stage_ComboBox.Items.AddRange(New Object() {"Stage 0", "Stage 1", "Stage 2", "Stage 3"})
        Me.stage_ComboBox.Location = New System.Drawing.Point(791, 295)
        Me.stage_ComboBox.Name = "stage_ComboBox"
        Me.stage_ComboBox.Size = New System.Drawing.Size(121, 21)
        Me.stage_ComboBox.TabIndex = 19
        '
        'stage_Label
        '
        Me.stage_Label.AutoSize = True
        Me.stage_Label.Location = New System.Drawing.Point(747, 303)
        Me.stage_Label.Name = "stage_Label"
        Me.stage_Label.Size = New System.Drawing.Size(38, 13)
        Me.stage_Label.TabIndex = 47
        Me.stage_Label.Text = "Stage:"
        '
        'cor_nameComboBox
        '
        Me.cor_nameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cor_nameComboBox.FormattingEnabled = True
        Me.cor_nameComboBox.Location = New System.Drawing.Point(703, 578)
        Me.cor_nameComboBox.Name = "cor_nameComboBox"
        Me.cor_nameComboBox.Size = New System.Drawing.Size(121, 21)
        Me.cor_nameComboBox.TabIndex = 33
        '
        'doc_ListBox
        '
        Me.doc_ListBox.FormattingEnabled = True
        Me.doc_ListBox.Location = New System.Drawing.Point(18, 437)
        Me.doc_ListBox.Name = "doc_ListBox"
        Me.doc_ListBox.Size = New System.Drawing.Size(194, 56)
        Me.doc_ListBox.TabIndex = 22
        '
        'doc_textbox
        '
        Me.doc_textbox.Location = New System.Drawing.Point(142, 411)
        Me.doc_textbox.Name = "doc_textbox"
        Me.doc_textbox.ReadOnly = True
        Me.doc_textbox.Size = New System.Drawing.Size(46, 20)
        Me.doc_textbox.TabIndex = 49
        Me.doc_textbox.TabStop = False
        '
        'hold_name_ComboBox
        '
        Me.hold_name_ComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.hold_name_ComboBox.FormattingEnabled = True
        Me.hold_name_ComboBox.Location = New System.Drawing.Point(812, 322)
        Me.hold_name_ComboBox.Name = "hold_name_ComboBox"
        Me.hold_name_ComboBox.Size = New System.Drawing.Size(100, 21)
        Me.hold_name_ComboBox.TabIndex = 20
        '
        'reason_label
        '
        Me.reason_label.AutoSize = True
        Me.reason_label.Location = New System.Drawing.Point(745, 330)
        Me.reason_label.Name = "reason_label"
        Me.reason_label.Size = New System.Drawing.Size(47, 13)
        Me.reason_label.TabIndex = 61
        Me.reason_label.Text = "Reason:"
        '
        'deldocbtn
        '
        Me.deldocbtn.Location = New System.Drawing.Point(32, 499)
        Me.deldocbtn.Name = "deldocbtn"
        Me.deldocbtn.Size = New System.Drawing.Size(123, 23)
        Me.deldocbtn.TabIndex = 29
        Me.deldocbtn.Text = "Delete a document"
        Me.deldocbtn.UseVisualStyleBackColor = True
        '
        'Comp_priorityCheckBox
        '
        Me.Comp_priorityCheckBox.Location = New System.Drawing.Point(824, 191)
        Me.Comp_priorityCheckBox.Name = "Comp_priorityCheckBox"
        Me.Comp_priorityCheckBox.Size = New System.Drawing.Size(36, 24)
        Me.Comp_priorityCheckBox.TabIndex = 14
        '
        'prevcomplbl
        '
        Me.prevcomplbl.AutoSize = True
        Me.prevcomplbl.Location = New System.Drawing.Point(159, 529)
        Me.prevcomplbl.Name = "prevcomplbl"
        Me.prevcomplbl.Size = New System.Drawing.Size(107, 13)
        Me.prevcomplbl.TabIndex = 64
        Me.prevcomplbl.Text = "Previously completed"
        Me.prevcomplbl.Visible = False
        '
        'compbycombobox
        '
        Me.compbycombobox.FormattingEnabled = True
        Me.compbycombobox.Location = New System.Drawing.Point(159, 83)
        Me.compbycombobox.Name = "compbycombobox"
        Me.compbycombobox.Size = New System.Drawing.Size(121, 21)
        Me.compbycombobox.TabIndex = 22
        '
        'old_comp_notextbox
        '
        Me.old_comp_notextbox.Location = New System.Drawing.Point(764, 246)
        Me.old_comp_notextbox.Name = "old_comp_notextbox"
        Me.old_comp_notextbox.Size = New System.Drawing.Size(100, 20)
        Me.old_comp_notextbox.TabIndex = 17
        '
        'costs_cancel_tbox
        '
        Me.costs_cancel_tbox.Location = New System.Drawing.Point(535, 623)
        Me.costs_cancel_tbox.Name = "costs_cancel_tbox"
        Me.costs_cancel_tbox.Size = New System.Drawing.Size(100, 20)
        Me.costs_cancel_tbox.TabIndex = 34
        '
        'compensation_tbox
        '
        Me.compensation_tbox.Location = New System.Drawing.Point(683, 624)
        Me.compensation_tbox.Name = "compensation_tbox"
        Me.compensation_tbox.Size = New System.Drawing.Size(100, 20)
        Me.compensation_tbox.TabIndex = 35
        '
        'respcbox
        '
        Me.respcbox.AutoSize = True
        Me.respcbox.Location = New System.Drawing.Point(736, 272)
        Me.respcbox.Name = "respcbox"
        Me.respcbox.Size = New System.Drawing.Size(225, 17)
        Me.respcbox.TabIndex = 18
        Me.respcbox.Text = "Response to complaint - forgotten incident"
        Me.respcbox.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(6, 16)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(56, 13)
        Me.Label13.TabIndex = 70
        Me.Label13.Text = "Start date:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 68)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(86, 13)
        Me.Label14.TabIndex = 71
        Me.Label14.Text = "Completed Date:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(6, 40)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(74, 13)
        Me.Label15.TabIndex = 72
        Me.Label15.Text = "Completed by:"
        '
        'stage2_gbox
        '
        Me.stage2_gbox.Controls.Add(Me.stage2_completed_by_combobox)
        Me.stage2_gbox.Controls.Add(Me.stage2_completed_datepicker)
        Me.stage2_gbox.Controls.Add(Me.Label13)
        Me.stage2_gbox.Controls.Add(Me.Label14)
        Me.stage2_gbox.Controls.Add(Me.stage2_start_datepicker)
        Me.stage2_gbox.Controls.Add(Me.Label15)
        Me.stage2_gbox.Location = New System.Drawing.Point(578, 366)
        Me.stage2_gbox.Name = "stage2_gbox"
        Me.stage2_gbox.Size = New System.Drawing.Size(232, 92)
        Me.stage2_gbox.TabIndex = 27
        Me.stage2_gbox.TabStop = False
        Me.stage2_gbox.Text = "Stage 2"
        '
        'stage2_completed_by_combobox
        '
        Me.stage2_completed_by_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.stage2_completed_by_combobox.FormattingEnabled = True
        Me.stage2_completed_by_combobox.Location = New System.Drawing.Point(98, 37)
        Me.stage2_completed_by_combobox.Name = "stage2_completed_by_combobox"
        Me.stage2_completed_by_combobox.Size = New System.Drawing.Size(121, 21)
        Me.stage2_completed_by_combobox.TabIndex = 1
        '
        'stage2_completed_datepicker
        '
        Me.stage2_completed_datepicker.Location = New System.Drawing.Point(98, 64)
        Me.stage2_completed_datepicker.Name = "stage2_completed_datepicker"
        Me.stage2_completed_datepicker.Size = New System.Drawing.Size(128, 20)
        Me.stage2_completed_datepicker.TabIndex = 2
        '
        'stage2_start_datepicker
        '
        Me.stage2_start_datepicker.Location = New System.Drawing.Point(98, 11)
        Me.stage2_start_datepicker.Name = "stage2_start_datepicker"
        Me.stage2_start_datepicker.Size = New System.Drawing.Size(128, 20)
        Me.stage2_start_datepicker.TabIndex = 0
        '
        'stage1_gbox
        '
        Me.stage1_gbox.Controls.Add(Me.GroupBox1)
        Me.stage1_gbox.Controls.Add(Me.compdatelbl)
        Me.stage1_gbox.Controls.Add(Me.compdatetimepicker)
        Me.stage1_gbox.Controls.Add(Me.compbylbl)
        Me.stage1_gbox.Controls.Add(Me.compbycombobox)
        Me.stage1_gbox.Location = New System.Drawing.Point(22, 543)
        Me.stage1_gbox.Name = "stage1_gbox"
        Me.stage1_gbox.Size = New System.Drawing.Size(321, 112)
        Me.stage1_gbox.TabIndex = 30
        Me.stage1_gbox.TabStop = False
        Me.stage1_gbox.Text = "Stage 1"
        '
        'Test_LogDataSet
        '
        Me.Test_LogDataSet.DataSetName = "Test_LogDataSet"
        Me.Test_LogDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Test_LogTableAdapter
        '
        Me.Test_LogTableAdapter.ClearBeforeFill = True
        '
        'gender_combobox
        '
        Me.gender_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.gender_combobox.FormattingEnabled = True
        Me.gender_combobox.Items.AddRange(New Object() {"F", "M", "N/A", "Unknown"})
        Me.gender_combobox.Location = New System.Drawing.Point(652, 38)
        Me.gender_combobox.Name = "gender_combobox"
        Me.gender_combobox.Size = New System.Drawing.Size(75, 21)
        Me.gender_combobox.TabIndex = 2
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(660, 9)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(45, 13)
        Me.Label12.TabIndex = 71
        Me.Label12.Text = "Gender:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(90, 101)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(50, 13)
        Me.Label20.TabIndex = 74
        Me.Label20.Text = "Ethnicity:"
        '
        'eth_combobox
        '
        Me.eth_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.eth_combobox.FormattingEnabled = True
        Me.eth_combobox.Location = New System.Drawing.Point(26, 127)
        Me.eth_combobox.Name = "eth_combobox"
        Me.eth_combobox.Size = New System.Drawing.Size(200, 21)
        Me.eth_combobox.TabIndex = 4
        '
        'dob_textbox
        '
        Me.dob_textbox.Location = New System.Drawing.Point(764, 39)
        Me.dob_textbox.Name = "dob_textbox"
        Me.dob_textbox.ReadOnly = True
        Me.dob_textbox.Size = New System.Drawing.Size(76, 20)
        Me.dob_textbox.TabIndex = 3
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(745, 9)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(111, 13)
        Me.Label16.TabIndex = 77
        Me.Label16.Text = "Debtor's Date of Birth:"
        '
        'Label17
        '
        Me.Label17.Location = New System.Drawing.Point(134, 166)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(45, 13)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Code:"
        '
        'debt_typelbl
        '
        Me.debt_typelbl.AutoSize = True
        Me.debt_typelbl.Location = New System.Drawing.Point(256, 152)
        Me.debt_typelbl.Name = "debt_typelbl"
        Me.debt_typelbl.Size = New System.Drawing.Size(10, 13)
        Me.debt_typelbl.TabIndex = 78
        Me.debt_typelbl.Text = " "
        '
        'ack_datepicker
        '
        Me.ack_datepicker.Location = New System.Drawing.Point(19, 36)
        Me.ack_datepicker.Name = "ack_datepicker"
        Me.ack_datepicker.Size = New System.Drawing.Size(117, 20)
        Me.ack_datepicker.TabIndex = 79
        '
        'ack_cbox
        '
        Me.ack_cbox.AutoSize = True
        Me.ack_cbox.Location = New System.Drawing.Point(33, 19)
        Me.ack_cbox.Name = "ack_cbox"
        Me.ack_cbox.Size = New System.Drawing.Size(76, 17)
        Me.ack_cbox.TabIndex = 80
        Me.ack_cbox.Text = "Letter sent"
        Me.ack_cbox.UseVisualStyleBackColor = True
        '
        'holding_cbox
        '
        Me.holding_cbox.AutoSize = True
        Me.holding_cbox.Location = New System.Drawing.Point(32, 19)
        Me.holding_cbox.Name = "holding_cbox"
        Me.holding_cbox.Size = New System.Drawing.Size(76, 17)
        Me.holding_cbox.TabIndex = 81
        Me.holding_cbox.Text = "Letter sent"
        Me.holding_cbox.UseVisualStyleBackColor = True
        '
        'holding_datepicker
        '
        Me.holding_datepicker.Location = New System.Drawing.Point(21, 42)
        Me.holding_datepicker.Name = "holding_datepicker"
        Me.holding_datepicker.Size = New System.Drawing.Size(117, 20)
        Me.holding_datepicker.TabIndex = 82
        '
        'ack_gbox
        '
        Me.ack_gbox.Controls.Add(Me.ack_cbox)
        Me.ack_gbox.Controls.Add(Me.ack_datepicker)
        Me.ack_gbox.Location = New System.Drawing.Point(247, 374)
        Me.ack_gbox.Name = "ack_gbox"
        Me.ack_gbox.Size = New System.Drawing.Size(151, 69)
        Me.ack_gbox.TabIndex = 23
        Me.ack_gbox.TabStop = False
        Me.ack_gbox.Text = "Stage 1 Acknowledgement"
        '
        'holding_gbox
        '
        Me.holding_gbox.Controls.Add(Me.holding_cbox)
        Me.holding_gbox.Controls.Add(Me.holding_datepicker)
        Me.holding_gbox.Location = New System.Drawing.Point(245, 449)
        Me.holding_gbox.Name = "holding_gbox"
        Me.holding_gbox.Size = New System.Drawing.Size(153, 74)
        Me.holding_gbox.TabIndex = 24
        Me.holding_gbox.TabStop = False
        Me.holding_gbox.Text = "Stage 1 Holding Letter"
        '
        'stage2_ack_gbox
        '
        Me.stage2_ack_gbox.Controls.Add(Me.stage2_ack_datepicker)
        Me.stage2_ack_gbox.Controls.Add(Me.stage2_ack_cbox)
        Me.stage2_ack_gbox.Location = New System.Drawing.Point(409, 374)
        Me.stage2_ack_gbox.Name = "stage2_ack_gbox"
        Me.stage2_ack_gbox.Size = New System.Drawing.Size(151, 66)
        Me.stage2_ack_gbox.TabIndex = 25
        Me.stage2_ack_gbox.TabStop = False
        Me.stage2_ack_gbox.Text = "Stage 2 Acknowledgement"
        '
        'stage2_ack_datepicker
        '
        Me.stage2_ack_datepicker.Location = New System.Drawing.Point(19, 37)
        Me.stage2_ack_datepicker.Name = "stage2_ack_datepicker"
        Me.stage2_ack_datepicker.Size = New System.Drawing.Size(119, 20)
        Me.stage2_ack_datepicker.TabIndex = 81
        '
        'stage2_ack_cbox
        '
        Me.stage2_ack_cbox.AutoSize = True
        Me.stage2_ack_cbox.Location = New System.Drawing.Point(25, 19)
        Me.stage2_ack_cbox.Name = "stage2_ack_cbox"
        Me.stage2_ack_cbox.Size = New System.Drawing.Size(76, 17)
        Me.stage2_ack_cbox.TabIndex = 81
        Me.stage2_ack_cbox.Text = "Letter sent"
        Me.stage2_ack_cbox.UseVisualStyleBackColor = True
        '
        'stage2_holding_gbox
        '
        Me.stage2_holding_gbox.Controls.Add(Me.stage2_holding_datepicker)
        Me.stage2_holding_gbox.Controls.Add(Me.stage2_holding_cbox)
        Me.stage2_holding_gbox.Location = New System.Drawing.Point(409, 457)
        Me.stage2_holding_gbox.Name = "stage2_holding_gbox"
        Me.stage2_holding_gbox.Size = New System.Drawing.Size(151, 66)
        Me.stage2_holding_gbox.TabIndex = 26
        Me.stage2_holding_gbox.TabStop = False
        Me.stage2_holding_gbox.Text = "Stage 2 Holding Letter"
        '
        'stage2_holding_datepicker
        '
        Me.stage2_holding_datepicker.Location = New System.Drawing.Point(14, 37)
        Me.stage2_holding_datepicker.Name = "stage2_holding_datepicker"
        Me.stage2_holding_datepicker.Size = New System.Drawing.Size(124, 20)
        Me.stage2_holding_datepicker.TabIndex = 81
        '
        'stage2_holding_cbox
        '
        Me.stage2_holding_cbox.AutoSize = True
        Me.stage2_holding_cbox.Location = New System.Drawing.Point(32, 19)
        Me.stage2_holding_cbox.Name = "stage2_holding_cbox"
        Me.stage2_holding_cbox.Size = New System.Drawing.Size(76, 17)
        Me.stage2_holding_cbox.TabIndex = 81
        Me.stage2_holding_cbox.Text = "Letter sent"
        Me.stage2_holding_cbox.UseVisualStyleBackColor = True
        '
        'stage3_gbox
        '
        Me.stage3_gbox.Controls.Add(Me.stage3_completed_by_combobox)
        Me.stage3_gbox.Controls.Add(Me.stage3_completed_datepicker)
        Me.stage3_gbox.Controls.Add(Me.Label8)
        Me.stage3_gbox.Controls.Add(Me.Label9)
        Me.stage3_gbox.Controls.Add(Me.stage3_start_datepicker)
        Me.stage3_gbox.Controls.Add(Me.Label18)
        Me.stage3_gbox.Location = New System.Drawing.Point(579, 465)
        Me.stage3_gbox.Name = "stage3_gbox"
        Me.stage3_gbox.Size = New System.Drawing.Size(232, 92)
        Me.stage3_gbox.TabIndex = 28
        Me.stage3_gbox.TabStop = False
        Me.stage3_gbox.Text = "Stage 3"
        '
        'stage3_completed_by_combobox
        '
        Me.stage3_completed_by_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.stage3_completed_by_combobox.FormattingEnabled = True
        Me.stage3_completed_by_combobox.Location = New System.Drawing.Point(98, 37)
        Me.stage3_completed_by_combobox.Name = "stage3_completed_by_combobox"
        Me.stage3_completed_by_combobox.Size = New System.Drawing.Size(121, 21)
        Me.stage3_completed_by_combobox.TabIndex = 1
        '
        'stage3_completed_datepicker
        '
        Me.stage3_completed_datepicker.Location = New System.Drawing.Point(97, 64)
        Me.stage3_completed_datepicker.Name = "stage3_completed_datepicker"
        Me.stage3_completed_datepicker.Size = New System.Drawing.Size(128, 20)
        Me.stage3_completed_datepicker.TabIndex = 2
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(6, 16)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(56, 13)
        Me.Label8.TabIndex = 70
        Me.Label8.Text = "Start date:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 68)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(86, 13)
        Me.Label9.TabIndex = 71
        Me.Label9.Text = "Completed Date:"
        '
        'stage3_start_datepicker
        '
        Me.stage3_start_datepicker.Location = New System.Drawing.Point(98, 11)
        Me.stage3_start_datepicker.Name = "stage3_start_datepicker"
        Me.stage3_start_datepicker.Size = New System.Drawing.Size(128, 20)
        Me.stage3_start_datepicker.TabIndex = 0
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(6, 40)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(74, 13)
        Me.Label18.TabIndex = 72
        Me.Label18.Text = "Completed by:"
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Received_fromTableAdapter
        '
        Me.Received_fromTableAdapter.ClearBeforeFill = True
        '
        'Complaint_againstBindingSource
        '
        Me.Complaint_againstBindingSource.DataMember = "Complaint_against"
        Me.Complaint_againstBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Complaint_againstTableAdapter
        '
        Me.Complaint_againstTableAdapter.ClearBeforeFill = True
        '
        'InvestigatorsBindingSource
        '
        Me.InvestigatorsBindingSource.DataMember = "Investigators"
        Me.InvestigatorsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'InvestigatorsTableAdapter
        '
        Me.InvestigatorsTableAdapter.ClearBeforeFill = True
        '
        'ActionsBindingSource
        '
        Me.ActionsBindingSource.DataMember = "Actions"
        Me.ActionsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'ActionsTableAdapter
        '
        Me.ActionsTableAdapter.ClearBeforeFill = True
        '
        'Corrective_actionsBindingSource
        '
        Me.Corrective_actionsBindingSource.DataMember = "Corrective actions"
        Me.Corrective_actionsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Corrective_actionsTableAdapter
        '
        Me.Corrective_actionsTableAdapter.ClearBeforeFill = True
        '
        'Detail_historyBindingSource
        '
        Me.Detail_historyBindingSource.DataMember = "detail_history"
        Me.Detail_historyBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Detail_historyTableAdapter
        '
        Me.Detail_historyTableAdapter.ClearBeforeFill = True
        '
        'Complaint_categoriesBindingSource
        '
        Me.Complaint_categoriesBindingSource.DataMember = "Complaint_categories"
        Me.Complaint_categoriesBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Complaint_categoriesTableAdapter
        '
        Me.Complaint_categoriesTableAdapter.ClearBeforeFill = True
        '
        'Receipt_typeBindingSource
        '
        Me.Receipt_typeBindingSource.DataMember = "Receipt_type"
        Me.Receipt_typeBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Receipt_typeTableAdapter
        '
        Me.Receipt_typeTableAdapter.ClearBeforeFill = True
        '
        'Hold_reasonBindingSource
        '
        Me.Hold_reasonBindingSource.DataMember = "Hold_reason"
        Me.Hold_reasonBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Hold_reasonTableAdapter
        '
        Me.Hold_reasonTableAdapter.ClearBeforeFill = True
        '
        'ResponseBindingSource
        '
        Me.ResponseBindingSource.DataMember = "response"
        Me.ResponseBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'ResponseTableAdapter
        '
        Me.ResponseTableAdapter.ClearBeforeFill = True
        '
        'ComplaintsBindingSource
        '
        Me.ComplaintsBindingSource.DataMember = "Complaints"
        Me.ComplaintsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'ComplaintsTableAdapter
        '
        Me.ComplaintsTableAdapter.ClearBeforeFill = True
        '
        'comp_against2_codecombobox
        '
        Me.comp_against2_codecombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comp_against2_codecombobox.FormattingEnabled = True
        Me.comp_against2_codecombobox.Location = New System.Drawing.Point(700, 133)
        Me.comp_against2_codecombobox.Name = "comp_against2_codecombobox"
        Me.comp_against2_codecombobox.Size = New System.Drawing.Size(121, 21)
        Me.comp_against2_codecombobox.TabIndex = 9
        '
        'agent2_combobox
        '
        Me.agent2_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.agent2_combobox.FormattingEnabled = True
        Me.agent2_combobox.Location = New System.Drawing.Point(837, 133)
        Me.agent2_combobox.Name = "agent2_combobox"
        Me.agent2_combobox.Size = New System.Drawing.Size(121, 21)
        Me.agent2_combobox.TabIndex = 10
        '
        'Complaints_resp_clientsBindingSource
        '
        Me.Complaints_resp_clientsBindingSource.DataMember = "Complaints_resp_clients"
        Me.Complaints_resp_clientsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Complaints_resp_clientsTableAdapter
        '
        Me.Complaints_resp_clientsTableAdapter.ClearBeforeFill = True
        '
        'legalcbox
        '
        Me.legalcbox.AutoSize = True
        Me.legalcbox.Location = New System.Drawing.Point(865, 370)
        Me.legalcbox.Name = "legalcbox"
        Me.legalcbox.Size = New System.Drawing.Size(104, 17)
        Me.legalcbox.TabIndex = 82
        Me.legalcbox.Text = "Legal/Insurance"
        Me.legalcbox.UseVisualStyleBackColor = True
        '
        'legalgbox
        '
        Me.legalgbox.Controls.Add(Me.inscbox)
        Me.legalgbox.Controls.Add(Me.solicitor_cbox)
        Me.legalgbox.Controls.Add(Label25)
        Me.legalgbox.Controls.Add(Me.monetary_risktbox)
        Me.legalgbox.Controls.Add(Me.ins_datepicker)
        Me.legalgbox.Controls.Add(Me.solicitor_datepicker)
        Me.legalgbox.Location = New System.Drawing.Point(824, 394)
        Me.legalgbox.Name = "legalgbox"
        Me.legalgbox.Size = New System.Drawing.Size(145, 175)
        Me.legalgbox.TabIndex = 83
        Me.legalgbox.TabStop = False
        Me.legalgbox.Text = "Legal/Insurance"
        '
        'inscbox
        '
        Me.inscbox.AutoSize = True
        Me.inscbox.Location = New System.Drawing.Point(7, 74)
        Me.inscbox.Name = "inscbox"
        Me.inscbox.Size = New System.Drawing.Size(117, 17)
        Me.inscbox.TabIndex = 87
        Me.inscbox.Text = "Referred to Insurer:"
        Me.inscbox.UseVisualStyleBackColor = True
        '
        'solicitor_cbox
        '
        Me.solicitor_cbox.AutoSize = True
        Me.solicitor_cbox.Location = New System.Drawing.Point(7, 20)
        Me.solicitor_cbox.Name = "solicitor_cbox"
        Me.solicitor_cbox.Size = New System.Drawing.Size(119, 17)
        Me.solicitor_cbox.TabIndex = 86
        Me.solicitor_cbox.Text = "Referred to Solicitor"
        Me.solicitor_cbox.UseVisualStyleBackColor = True
        '
        'monetary_risktbox
        '
        Me.monetary_risktbox.Location = New System.Drawing.Point(24, 143)
        Me.monetary_risktbox.Name = "monetary_risktbox"
        Me.monetary_risktbox.Size = New System.Drawing.Size(100, 20)
        Me.monetary_risktbox.TabIndex = 85
        '
        'ins_datepicker
        '
        Me.ins_datepicker.Location = New System.Drawing.Point(3, 92)
        Me.ins_datepicker.Name = "ins_datepicker"
        Me.ins_datepicker.Size = New System.Drawing.Size(134, 20)
        Me.ins_datepicker.TabIndex = 84
        '
        'solicitor_datepicker
        '
        Me.solicitor_datepicker.Location = New System.Drawing.Point(0, 40)
        Me.solicitor_datepicker.Name = "solicitor_datepicker"
        Me.solicitor_datepicker.Size = New System.Drawing.Size(134, 20)
        Me.solicitor_datepicker.TabIndex = 0
        '
        'costs_reasonbtn
        '
        Me.costs_reasonbtn.Location = New System.Drawing.Point(503, 661)
        Me.costs_reasonbtn.Name = "costs_reasonbtn"
        Me.costs_reasonbtn.Size = New System.Drawing.Size(155, 23)
        Me.costs_reasonbtn.TabIndex = 84
        Me.costs_reasonbtn.Text = "Reason for Cancelled Costs"
        Me.costs_reasonbtn.UseVisualStyleBackColor = True
        '
        'updatecmpfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(981, 696)
        Me.Controls.Add(Me.costs_reasonbtn)
        Me.Controls.Add(Me.legalgbox)
        Me.Controls.Add(Me.legalcbox)
        Me.Controls.Add(Label22)
        Me.Controls.Add(Me.agent2_combobox)
        Me.Controls.Add(Me.comp_against2_codecombobox)
        Me.Controls.Add(Label19)
        Me.Controls.Add(Label21)
        Me.Controls.Add(Me.stage3_gbox)
        Me.Controls.Add(Me.prevcomplbl)
        Me.Controls.Add(Me.stage2_holding_gbox)
        Me.Controls.Add(Me.stage2_ack_gbox)
        Me.Controls.Add(Me.holding_gbox)
        Me.Controls.Add(Me.ack_gbox)
        Me.Controls.Add(Me.debt_typelbl)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.dob_textbox)
        Me.Controls.Add(Me.eth_combobox)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.gender_combobox)
        Me.Controls.Add(Me.stage1_gbox)
        Me.Controls.Add(Me.stage2_gbox)
        Me.Controls.Add(Me.respcbox)
        Me.Controls.Add(Label11)
        Me.Controls.Add(Me.compensation_tbox)
        Me.Controls.Add(Label10)
        Me.Controls.Add(Me.costs_cancel_tbox)
        Me.Controls.Add(Me.old_comp_notextbox)
        Me.Controls.Add(Comp_old_comp_noLabel)
        Me.Controls.Add(Comp_priorityLabel)
        Me.Controls.Add(Me.Comp_priorityCheckBox)
        Me.Controls.Add(Me.deldocbtn)
        Me.Controls.Add(Me.reason_label)
        Me.Controls.Add(Me.hold_name_ComboBox)
        Me.Controls.Add(Me.doc_ListBox)
        Me.Controls.Add(Me.doc_textbox)
        Me.Controls.Add(Label7)
        Me.Controls.Add(Me.cor_nameComboBox)
        Me.Controls.Add(Label6)
        Me.Controls.Add(Me.stage_Label)
        Me.Controls.Add(Me.stage_ComboBox)
        Me.Controls.Add(Me.addbtn)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Me.actions_ComboBox)
        Me.Controls.Add(Me.Comp_responseTextBox)
        Me.Controls.Add(Me.Comp_textTextBox)
        Me.Controls.Add(Me.Comp_noTextBox)
        Me.Controls.Add(Me.Comp_dateTextBox)
        Me.Controls.Add(Me.comp_against_codeComboBox)
        Me.Controls.Add(Me.comp_case_noTextBox)
        Me.Controls.Add(Me.recvd_textComboBox)
        Me.Controls.Add(Me.cat_codeComboBox)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.entered_byTextBox)
        Me.Controls.Add(Comp_entered_byLabel)
        Me.Controls.Add(Me.invComboBox)
        Me.Controls.Add(Label3)
        Me.Controls.Add(Label5)
        Me.Controls.Add(Me.agentComboBox)
        Me.Controls.Add(Comp_against_codeLabel)
        Me.Controls.Add(Me.Cat_textComboBox)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.cl_ComboBox)
        Me.Controls.Add(Me.recpt_combobox)
        Me.Controls.Add(Recvd_textLabel)
        Me.Controls.Add(Recpt_textLabel)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.updbtn)
        Me.Controls.Add(Comp_textLabel)
        Me.Controls.Add(Comp_cat_numberLabel)
        Me.Controls.Add(Comp_case_noLabel)
        Me.Controls.Add(Comp_dateLabel)
        Me.Controls.Add(Comp_noLabel)
        Me.Name = "updatecmpfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Update a complaint"
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.stage2_gbox.ResumeLayout(False)
        Me.stage2_gbox.PerformLayout()
        Me.stage1_gbox.ResumeLayout(False)
        Me.stage1_gbox.PerformLayout()
        CType(Me.Test_LogDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ack_gbox.ResumeLayout(False)
        Me.ack_gbox.PerformLayout()
        Me.holding_gbox.ResumeLayout(False)
        Me.holding_gbox.PerformLayout()
        Me.stage2_ack_gbox.ResumeLayout(False)
        Me.stage2_ack_gbox.PerformLayout()
        Me.stage2_holding_gbox.ResumeLayout(False)
        Me.stage2_holding_gbox.PerformLayout()
        Me.stage3_gbox.ResumeLayout(False)
        Me.stage3_gbox.PerformLayout()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Complaint_againstBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Corrective_actionsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Detail_historyBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Complaint_categoriesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Receipt_typeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Hold_reasonBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ResponseBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Complaints_resp_clientsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.legalgbox.ResumeLayout(False)
        Me.legalgbox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents updbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents recpt_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents cl_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Cat_textComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents agentComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents invComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents entered_byTextBox As System.Windows.Forms.TextBox
    Friend WithEvents comprbtn As System.Windows.Forms.RadioButton
    Friend WithEvents notcomprbtn As System.Windows.Forms.RadioButton
    Friend WithEvents unfoundedrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents foundedrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents compdatetimepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents openrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents compbylbl As System.Windows.Forms.Label
    Friend WithEvents compdatelbl As System.Windows.Forms.Label
    Friend WithEvents cat_codeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents recvd_textComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents comp_case_noTextBox As System.Windows.Forms.TextBox
    Friend WithEvents comp_against_codeComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents Comp_dateTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Comp_noTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Comp_textTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Comp_responseTextBox As System.Windows.Forms.TextBox
    Friend WithEvents actions_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents addbtn As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents stage_Label As System.Windows.Forms.Label
    Friend WithEvents stage_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents cor_nameComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents doc_ListBox As System.Windows.Forms.ListBox
    Friend WithEvents doc_textbox As System.Windows.Forms.TextBox
    Friend WithEvents hold_name_ComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents reason_label As System.Windows.Forms.Label
    Friend WithEvents deldocbtn As System.Windows.Forms.Button
    Friend WithEvents Comp_priorityCheckBox As System.Windows.Forms.CheckBox
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents Received_fromTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
    Friend WithEvents Complaint_againstBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaint_againstTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
    Friend WithEvents InvestigatorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InvestigatorsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
    Friend WithEvents ActionsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ActionsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ActionsTableAdapter
    Friend WithEvents Corrective_actionsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Corrective_actionsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Corrective_actionsTableAdapter
    Friend WithEvents Detail_historyBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Detail_historyTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.detail_historyTableAdapter
    Friend WithEvents Complaint_categoriesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaint_categoriesTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_categoriesTableAdapter
    Friend WithEvents Receipt_typeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Receipt_typeTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Receipt_typeTableAdapter
    Friend WithEvents Hold_reasonBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Hold_reasonTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Hold_reasonTableAdapter
    Friend WithEvents ResponseBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ResponseTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.responseTableAdapter
    Friend WithEvents prevcomplbl As System.Windows.Forms.Label
    Friend WithEvents compbycombobox As System.Windows.Forms.ComboBox
    Friend WithEvents Test_LogDataSet As Complaints.Test_LogDataSet
    Friend WithEvents Test_LogTableAdapter As Complaints.Test_LogDataSetTableAdapters.Test_LogTableAdapter
    Friend WithEvents ComplaintsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents old_comp_notextbox As System.Windows.Forms.TextBox
    Friend WithEvents compensation_tbox As System.Windows.Forms.TextBox
    Friend WithEvents costs_cancel_tbox As System.Windows.Forms.TextBox
    Friend WithEvents respcbox As System.Windows.Forms.CheckBox
    Friend WithEvents ComplaintsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ComplaintsTableAdapter
    Friend WithEvents stage2_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents stage2_completed_by_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents stage2_completed_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents stage2_start_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents stage1_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents gender_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents eth_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents dob_textbox As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents debt_typelbl As System.Windows.Forms.Label
    Friend WithEvents ack_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents ack_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents holding_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents holding_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents holding_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents ack_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents stage2_ack_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents stage2_holding_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents stage2_holding_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents stage2_holding_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents stage2_ack_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents stage2_ack_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents stage3_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents stage3_completed_by_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents stage3_completed_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents stage3_start_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents agent2_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents comp_against2_codecombobox As System.Windows.Forms.ComboBox
    Friend WithEvents Complaints_resp_clientsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaints_resp_clientsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaints_resp_clientsTableAdapter
    Friend WithEvents legalcbox As System.Windows.Forms.CheckBox
    Friend WithEvents legalgbox As System.Windows.Forms.GroupBox
    Friend WithEvents monetary_risktbox As System.Windows.Forms.TextBox
    Friend WithEvents ins_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents solicitor_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents solicitor_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents inscbox As System.Windows.Forms.CheckBox
    Friend WithEvents costs_reasonbtn As System.Windows.Forms.Button
End Class

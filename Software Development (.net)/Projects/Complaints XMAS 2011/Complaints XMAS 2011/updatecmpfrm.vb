Public Class updatecmpfrm

    Private Sub updatecmpfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Complaints_resp_clients' table. You can move, or remove it, as needed.
        Me.Complaints_resp_clientsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaints_resp_clients)
        Try
            Dim idx As Integer
            ErrorProvider1.SetError(agentComboBox, "")
            ErrorProvider1.SetError(Cat_textComboBox, "")
            ErrorProvider1.SetError(updbtn, "")
            ErrorProvider1.SetError(actions_ComboBox, "")
            ErrorProvider1.SetError(GroupBox2, "")
            ErrorProvider1.SetError(stage2_gbox, "")
            'use test database for test logid
            If log_code = test_logid Then
                add_compfrm.Test_complaintsTableAdapter.FillBy(add_compfrm.Test_complaintsDataSet.test_complaints, comp_no)
            Else
                Me.ComplaintsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaints, comp_no)
            End If
            Try
                If log_code = test_logid Then
                    recpt_code = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(2)
                Else
                    recpt_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(2)
                End If
            Catch
                MsgBox("There is no complaint number " & comp_no)
                Me.Close()
                Exit Sub
            End Try

            'get data for complaint from test or live
            add_compfrm.EthnicityTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Ethnicity)
            Dim ent_code As Integer
            If log_code = test_logid Then
                ent_code = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(11)
                Comp_dateTextBox.Text = CDate(add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(1))
                orig_details = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(13)
                Try
                    orig_resp_details = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(19)
                Catch ex As Exception
                    orig_resp_details = ""
                End Try
                recvd_from = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(3)
                case_no = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(4)
                cl_no = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(5)
                cat_code = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(8)
                cat_number = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(9)
                against_code = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(6)
                Try
                    against2_code = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(41)
                Catch ex As Exception
                    against2_code = 0
                End Try
                alloc_to_code = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(12)
                action_code = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(20)
                orig_founded = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(18)
                cl_no = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(5)
                case_no = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(4)
                stage_no = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(21)
                cor_code = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(22)
                orig_priority = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(24)
                agent_no = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(7)
                Try
                    agent2_no = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(42)
                Catch ex As Exception
                    agent2_no = 0
                End Try

                Try
                    old_comp_no = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(25)
                Catch ex As Exception
                    old_comp_no = 0
                End Try

                Try
                    costs_cancel = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(26)
                Catch ex As Exception
                    costs_cancel = 0
                End Try
                Try
                    compensation = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(27)
                Catch ex As Exception
                    compensation = 0
                End Try
                Try
                    resp_forgotten = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(28)
                Catch ex As Exception
                    resp_forgotten = 0
                End Try
                Try
                    stage2_start_date = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(29)
                Catch ex As Exception
                    stage2_start_date = Nothing
                End Try
                Try
                    stage2_completed_date = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(31)
                Catch ex As Exception
                    stage2_completed_date = Nothing
                End Try
                Try
                    stage2_completed_by = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(30)
                Catch ex As Exception
                    stage2_completed_by = 0
                End Try
                Try
                    gender = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(32)
                Catch ex As Exception
                    gender = "Unknown"
                End Try
                Try
                    ethnicity = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(33)
                Catch ex As Exception
                    ethnicity = 0
                End Try
                Try
                    ack_date = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(34)
                Catch ex As Exception
                    ack_date = CDate("1800,1,1")
                End Try
                Try
                    holding_date = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(35)
                Catch ex As Exception
                    holding_date = CDate("1800,1,1")
                End Try
                Try
                    stage2_ack_date = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(36)
                Catch ex As Exception
                    stage2_ack_date = CDate("1800,1,1")
                End Try
                Try
                    stage2_holding_date = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(37)
                Catch ex As Exception
                    stage2_holding_date = CDate("1800,1,1")
                End Try
                Try
                    stage3_start_date = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(38)
                Catch ex As Exception
                    stage3_start_date = CDate("1800,1,1")
                End Try
                Try
                    stage3_completed_by = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(39)
                Catch ex As Exception
                    stage3_completed_by = 0
                End Try
                Try
                    stage3_completed_date = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(40)
                Catch ex As Exception
                    stage3_completed_date = CDate("1800,1,1")
                End Try
                Try
                    referred_to_solicitor = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(43)
                Catch ex As Exception
                    referred_to_solicitor = CDate("1800,1,1")
                End Try
                Try
                    referred_to_insurer = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(44)
                Catch ex As Exception
                    referred_to_insurer = CDate("1800,1,1")
                End Try
                Try
                    monetary_risk = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(45)
                Catch ex As Exception
                    monetary_risk = -999
                End Try
                Try
                    legal_ins_flag = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(46)
                Catch ex As Exception
                    legal_ins_flag = "N"
                End Try
                Try
                    cancel_costs_reason = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(47)
                Catch ex As Exception
                    cancel_costs_reason = ""
                End Try
            Else
                ent_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(11)
                Comp_dateTextBox.Text = CDate(PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(1))
                orig_details = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(13)
                Try
                    orig_resp_details = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(19)
                Catch ex As Exception
                    orig_resp_details = ""
                End Try
                recvd_from = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(3)
                case_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(4)
                cl_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(5)
                cat_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(8)
                cat_number = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(9)
                against_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(6)
                Try
                    against2_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(41)
                Catch ex As Exception
                    against2_code = 0
                End Try
                alloc_to_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(12)
                action_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(20)
                orig_founded = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(18)
                cl_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(5)
                case_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(4)
                stage_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(21)
                cor_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(22)
                orig_priority = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(24)
                agent_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(7)
                Try
                    agent2_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(42)
                Catch ex As Exception
                    agent2_no = 0
                End Try
                Try
                    old_comp_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(25)
                Catch ex As Exception
                    old_comp_no = 0
                End Try

                Try
                    costs_cancel = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(26)
                Catch ex As Exception
                    costs_cancel = 0
                End Try
                Try
                    compensation = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(27)
                Catch ex As Exception
                    compensation = 0
                End Try
                Try
                    resp_forgotten = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(28)
                Catch ex As Exception
                    resp_forgotten = 0
                End Try
                Try
                    stage2_start_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(29)
                Catch ex As Exception
                    stage2_start_date = Nothing
                End Try
                Try
                    stage2_completed_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(31)
                Catch ex As Exception
                    stage2_completed_date = Nothing
                End Try
                Try
                    stage2_completed_by = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(30)
                Catch ex As Exception
                    stage2_completed_by = 0
                End Try
                Try
                    gender = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(32)
                Catch ex As Exception
                    gender = "Unknown"
                End Try
                Try
                    ethnicity = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(33)
                Catch ex As Exception
                    ethnicity = 0
                End Try
                Try
                    ack_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(34)
                Catch ex As Exception
                    ack_date = CDate("1800,1,1")
                End Try
                Try
                    holding_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(35)
                Catch ex As Exception
                    holding_date = CDate("1800,1,1")
                End Try
                Try
                    stage2_ack_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(36)
                Catch ex As Exception
                    stage2_ack_date = CDate("1800,1,1")
                End Try
                Try
                    stage2_holding_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(37)
                Catch ex As Exception
                    stage2_holding_date = CDate("1800,1,1")
                End Try
                Try
                    stage3_start_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(38)
                Catch ex As Exception
                    stage3_start_date = CDate("1800,1,1")
                End Try
                Try
                    stage3_completed_by = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(39)
                Catch ex As Exception
                    stage3_completed_by = 0
                End Try
                Try
                    stage3_completed_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(40)
                Catch ex As Exception
                    stage3_completed_date = CDate("1800,1,1")
                End Try
                Try
                    referred_to_solicitor = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(43)
                Catch ex As Exception
                    referred_to_solicitor = CDate("1800,1,1")
                End Try
                Try
                    referred_to_insurer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(44)
                Catch ex As Exception
                    referred_to_insurer = CDate("1800,1,1")
                End Try
                Try
                    monetary_risk = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(45)
                Catch ex As Exception
                    monetary_risk = -999
                End Try
                Try
                    legal_ins_flag = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(46)
                Catch ex As Exception
                    legal_ins_flag = "N"
                End Try
                Try
                    cancel_costs_reason = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(47)
                Catch ex As Exception
                    cancel_costs_reason = ""
                End Try
            End If

            'save original values
            orig_entered_date = Comp_dateTextBox.Text
            orig_recpt_code = recpt_code
            orig_recvd_from = recvd_from
            orig_case_no = case_no
            orig_cl_no = cl_no
            orig_agent_no = agent_no
            orig_agent2_no = agent2_no
            orig_alloc_to_code = alloc_to_code
            orig_action_code = action_code
            orig_cor_code = cor_code
            orig_old_comp_no = old_comp_no
            orig_costs_cancel = costs_cancel
            orig_compensation = compensation
            orig_resp_forgotten = resp_forgotten
            orig_stage_no = stage_no
            orig_stage2_start_date = stage2_start_date
            orig_stage2_completed_date = stage2_completed_date
            orig_stage2_completed_by = stage2_completed_by
            orig_stage3_start_date = stage3_start_date
            orig_stage3_completed_date = stage3_completed_date
            orig_stage3_completed_by = stage3_completed_by
            orig_legal_ins_flag = legal_ins_flag
            orig_referred_to_solicitor = referred_to_solicitor
            orig_referred_to_insurer = referred_to_insurer
            orig_monetary_risk = monetary_risk
            Select Case gender
                Case "U"
                    gender = "Unknown"
                Case "N"
                    gender = "N/A"
            End Select
            gender_combobox.Text = gender
            orig_gender = gender
            eth_combobox.Items.Clear()
            For idx = 0 To Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows.Count - 1
                Dim eth_code As Integer = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(0)
                Dim eth_desc As String = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(1)
                eth_combobox.Items.Add(eth_desc)
                If ethnicity = eth_code Then
                    eth_combobox.Text = eth_desc
                End If
            Next
            ethnicity_desc = eth_combobox.Text
            orig_ethnicity_desc = eth_combobox.Text

            Me.Hold_reasonTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Hold_reason)
            Me.Corrective_actionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Corrective_actions)
            Me.ActionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Actions)
            Me.ResponseTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.response)
            doc_change_made = False

            'entered by
            Me.InvestigatorsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Investigators, ent_code)
            entered_byTextBox.Text = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)

            Me.Complaint_againstTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_against)
            Me.Complaint_categoriesTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_categories)

            Comp_noTextBox.Text = comp_no
            Comp_textTextBox.Text = orig_details

            Comp_responseTextBox.Text = orig_resp_details

            'receipt type
            Me.Receipt_typeTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Receipt_type, recpt_code)
            recpt_text = PraiseAndComplaintsSQLDataSet.Receipt_type.Rows(0).Item(1)
            orig_recpt_text = recpt_text
            Me.Receipt_typeTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Receipt_type)
            recpt_combobox.Items.Clear()
            idx = 0
            For Each row In PraiseAndComplaintsSQLDataSet.Receipt_type.Rows
                recpt_combobox.Items.Add(PraiseAndComplaintsSQLDataSet.Receipt_type.Rows(idx).Item(1))
                idx += 1
            Next
            recpt_combobox.Text = recpt_text

            'received from

            Me.Received_fromTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Received_from, recvd_from)
            recvd_text = PraiseAndComplaintsSQLDataSet.Received_from.Rows(0).Item(1)
            orig_recvd_text = recvd_text
            Me.Received_fromTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Received_from)
            recvd_textComboBox.Items.Clear()
            idx = 0
            For Each row In PraiseAndComplaintsSQLDataSet.Received_from.Rows
                recvd_textComboBox.Items.Add(PraiseAndComplaintsSQLDataSet.Received_from.Rows(idx).Item(1))
                idx += 1
            Next
            recvd_textComboBox.Text = recvd_text

            'case no and client no
            comp_case_noTextBox.Text = case_no
            'if non-zero case no get dob from onestep
            Dim dob_str As String = "N/A"
            If case_no > 0 Then
                param1 = "onestep"
                param2 = "select dateOfBirth, clientschemeID from Debtor where _rowid = " & case_no
                Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 1 Then
                    Dim dob As Date
                    Try
                        dob = debtor_dataset.Tables(0).Rows(0).Item(0)
                    Catch ex As Exception
                        dob_str = "unknown"
                    End Try
                    If dob_str <> "unknown" Then
                        dob_str = Format(dob, "dd/MM/yyyy")
                    End If
                    'get debt type
                    Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(1)
                    param2 = "select schemeID from ClientScheme where _rowid = " & csid
                    Dim csid_dataset As DataSet = get_dataset(param1, param2)
                    Dim sch_id As Integer = csid_dataset.Tables(0).Rows(0).Item(0)
                    param2 = "select name from Scheme where _rowid = " & sch_id
                    Dim sch_dataset As DataSet = get_dataset(param1, param2)
                    debt_typelbl.Text = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                End If
            End If
            dob_textbox.Text = dob_str
            'category
            orig_cat_code = cat_code
            orig_cat_number = cat_number
            Me.Complaint_categoriesTableAdapter.Fill2(cat_table, cat_code)

            Dim cat_text As String

            Select Case cat_code
                Case "A" : cat_codeComboBox.SelectedIndex = 0
                Case "B" : cat_codeComboBox.SelectedIndex = 1
                Case "C" : cat_codeComboBox.SelectedIndex = 2
            End Select

            idx = 0
            Cat_textComboBox.Items.Clear()
            For Each row In cat_table.Rows
                cat_text = cat_table.Rows(idx).Item(1) & " " & cat_table.Rows(idx).Item(2)
                Cat_textComboBox.Items.Add(cat_text)
                If cat_number = 99 Then
                    Cat_textComboBox.Text = "99 Contentious"
                ElseIf cat_number = cat_table.Rows(idx).Item(1) Then
                    Cat_textComboBox.SelectedIndex = idx
                End If
                idx += 1
            Next

            'against code
            orig_against_code = against_code
            Me.Complaint_againstTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaint_against, against_code)
            against_text = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(0).Item(1)
            orig_against_text = against_text
            Me.Complaint_againstTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_against)
            comp_against_codeComboBox.Items.Clear()
            idx = 0
            For Each row In PraiseAndComplaintsSQLDataSet.Complaint_against.Rows
                comp_against_codeComboBox.Items.Add(PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(idx).Item(1))
                If against_text = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(idx).Item(1) Then
                    comp_against_codeComboBox.SelectedIndex = idx
                    comp_against_codeComboBox.Text = against_text
                End If
                idx += 1
            Next

            'against2 code
            orig_against2_code = against2_code
            If against2_code > 0 Then
                Me.Complaint_againstTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaint_against, against2_code)
                against2_text = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(0).Item(1)
            Else
                against2_text = ""
            End If

            Me.Complaint_againstTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_against)
            comp_against2_codecombobox.Items.Clear()
            idx = 0
            For Each row In PraiseAndComplaintsSQLDataSet.Complaint_against.Rows
                comp_against2_codecombobox.Items.Add(PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(idx).Item(1))
                If against2_text = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(idx).Item(1) Then
                    comp_against2_codecombobox.SelectedIndex = idx
                    comp_against2_codecombobox.Text = against2_text
                End If
                idx += 1
            Next
            
            orig_against2_text = against2_text

            agentComboBox.Items.Clear()
            populate_agentcombo()

            agent2_combobox.Items.Clear()
            populate_agent2combo()

            'investigating officer
            Me.InvestigatorsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Investigators, alloc_to_code)
            inv_text = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
            orig_inv_text = inv_text
            Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
            invComboBox.Items.Clear()
            idx = 0
            For Each row In PraiseAndComplaintsSQLDataSet.Investigators.Rows
                invComboBox.Items.Add(PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(1))
                idx += 1
            Next
            invComboBox.Text = inv_text

            'action
            If action_code <> 0 Then
                Me.ActionsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Actions, action_code)
                Try
                    action_name = PraiseAndComplaintsSQLDataSet.Actions.Rows(0).Item(1)
                Catch ex As Exception
                    action_name = "N/A"
                End Try
                orig_action_text = action_name
            End If
            Me.ActionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Actions)
            actions_ComboBox.Items.Clear()
            idx = 0
            For Each row In PraiseAndComplaintsSQLDataSet.Actions.Rows
                actions_ComboBox.Items.Add(PraiseAndComplaintsSQLDataSet.Actions.Rows(idx).Item(1))
                idx += 1
            Next
            If action_code <> 0 Then
                actions_ComboBox.Text = action_name
            Else
                actions_ComboBox.Text = "N/A"
                actions_ComboBox.SelectedItem = -1
            End If

            'completion date
            Dim completed_date As Date
            completed = False
            If log_code = test_logid Then
                If add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(15) > 0 Then
                    completed = True
                    completed_date = CDate(add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(14))
                End If
            Else
                If PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(15) > 0 Then
                    completed = True
                    completed_date = CDate(PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(14))
                End If
            End If
            orig_completed = completed
            compbycombobox.Items.Clear()
            Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
            Dim row2 As DataRow
            idx = 0
            For Each row2 In PraiseAndComplaintsSQLDataSet.Investigators.Rows
                compbycombobox.Items.Add(PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(1))
                idx += 1
            Next
            If completed Then
                compdatetimepicker.Value = completed_date
                orig_comp_date = completed_date
                compdatetimepicker.Visible = True
                comprbtn.Checked = True
                If log_code = test_logid Then
                    compby_code = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(15)
                Else
                    compby_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(15)
                End If

                Me.InvestigatorsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Investigators, compby_code)
                compbycombobox.Text = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
                orig_compby = compbycombobox.Text
                compbycombobox.Visible = True
            Else
                notcomprbtn.Checked = True
                compdatetimepicker.Visible = False
                compbycombobox.Visible = False
                compbycombobox.SelectedIndex = -1
            End If

            'founded/unfounded
            new_founded = ""
            Select Case orig_founded
                Case "Y"
                    foundedrbtn.Checked = True
                Case "N"
                    unfoundedrbtn.Checked = True
                Case "U"
                    openrbtn.Checked = True
            End Select

            'checkedby

            'checked = True
            'If ComplaintsDataSet.Tables(0).Rows(0).Item(17) = 0 Then
            '    checked = False
            'End If
            'orig_checked = checked
            'If checked Then
            '    chkdrbtn.Checked = True
            '    checkeddatetimepicker.Visible = True
            '    checkeddatelbl.Visible = True
            '    checkedbylbl.Visible = True
            '    checkeddatetimepicker.Value = CDate(ComplaintsDataSet.Tables(0).Rows(0).Item(16))
            '    Dim checked_code As Integer = ComplaintsDataSet.Tables(0).Rows(0).Item(17)
            '    Me.InvestigatorsTableAdapter.FillBy(Me.InvestigatorsDataSet.Investigators, checked_code)
            '    checkedbytextbox.Text = InvestigatorsDataSet.Tables(0).Rows(0).Item(1)
            '    checkedbytextbox.Visible = True
            '    checkeddatetimepicker.Enabled = False
            '    checkedbytextbox.ReadOnly = True
            'Else
            '    notchkdrbtn.Checked = True
            '    checkeddatelbl.Visible = False
            '    checkeddatetimepicker.Visible = False
            '    checkedbylbl.Visible = False
            '    checkedbytextbox.Visible = False
            '    checkedbytextbox.Visible = False
            'End If

            cl_ComboBox.Items.Clear()
            If case_no = 0 Then
                populate_client_table()
                For idx = 1 To cl_rows
                    cl_ComboBox.Items.Add(client_table(idx, 2))
                    If cl_no = client_table(idx, 1) Then
                        cl_name = client_table(idx, 2)
                    End If
                Next
            Else
                param2 = "select name from Client" & _
                " where _rowid = " & cl_no
                Dim cl_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    MsgBox("Error reading client " & cl_no)
                    Exit Sub
                End If
                cl_name = Trim(cl_dataset.Tables(0).Rows(0).Item(0))
                cl_ComboBox.Items.Add(cl_name)
                cl_ComboBox.SelectedItem = cl_name
            End If

            If cl_no = 0 Then
                cl_ComboBox.SelectedItem = -1
            Else
                cl_ComboBox.SelectedItem = cl_name
            End If

            'no of documents
            doc_textbox.Text = populate_document_combobox_upd()
            orig_no_docs = doc_textbox.Text
            If doc_textbox.Text = 0 Then
                doc_ListBox.Visible = False
                deldocbtn.Enabled = False
            Else
                doc_ListBox.Visible = True
                deldocbtn.Enabled = True
            End If

            'check if stage 1 acknowledgement letter has been sent
            If ack_date <> CDate("1800,1,1") Then
                ack_datepicker.Visible = True
                ack_cbox.Checked = True
                ack_datepicker.Value = ack_date
            Else
                ack_datepicker.Visible = False
                ack_cbox.Checked = False
                If case_no > 0 Then
                    param1 = "onestep"
                    param2 = "select text, _createdDate from Note" & _
                    " where debtorID = " & case_no & " and type = 'Letter'" & _
                    " order by _createdDate"
                    Dim note_dataset As DataSet = get_dataset(param1, param2)
                    For idx = 0 To no_of_rows - 1
                        Dim created_date As Date = note_dataset.Tables(0).Rows(idx).Item(1)
                        Dim note_text As String = note_dataset.Tables(0).Rows(idx).Item(0)
                        If InStr(note_text, "Complaint_Ack") > 0 And _
                        created_date >= CDate(Comp_dateTextBox.Text) And _
                        InStr(note_text, "Stage") = 0 Then
                            ack_datepicker.Text = FormatDateTime(note_dataset.Tables(0).Rows(idx).Item(1), DateFormat.ShortDate)
                            ack_datepicker.Visible = True
                            ack_cbox.Checked = True
                            ack_date = ack_datepicker.Value
                        End If
                    Next
                End If
            End If
            orig_ack_date = ack_date

            'check if stage 1 holding letter has been sent
            If holding_date <> CDate("1800,1,1") Then
                holding_datepicker.Visible = True
                holding_cbox.Checked = True
                holding_datepicker.Value = holding_date
            Else
                holding_datepicker.Visible = False
                holding_cbox.Checked = False
                If case_no > 0 Then
                    If check_held_letter(case_no, Comp_dateTextBox.Text) = "Y" Then
                        If stage_no < 2 Then
                            holding_datepicker.Text = FormatDateTime(parm_hold_date, DateFormat.ShortDate)
                            holding_datepicker.Visible = True
                            holding_cbox.Checked = True
                            holding_date = holding_datepicker.Value
                        ElseIf stage2_start_date <> CDate("1800,1,1") Then
                            If parm_hold_date < stage2_start_date Then
                                holding_datepicker.Text = FormatDateTime(parm_hold_date, DateFormat.ShortDate)
                                holding_datepicker.Visible = True
                                holding_cbox.Checked = True
                                holding_date = holding_datepicker.Value
                            End If
                        End If

                    End If
                End If
            End If

            orig_holding_date = holding_date

            'check if stage 2 acknowledgement letter has been sent
            If stage2_ack_date <> CDate("1800,1,1") Then
                stage2_ack_datepicker.Visible = True
                stage2_ack_cbox.Checked = True
                stage2_ack_datepicker.Value = stage2_ack_date
            Else
                stage2_ack_datepicker.Visible = False
                stage2_ack_cbox.Checked = False
                If case_no > 0 Then
                    param1 = "onestep"
                    param2 = "select text, _createdDate from Note" & _
                    " where debtorID = " & case_no & " and type = 'Letter'" & _
                    " order by _createdDate"
                    Dim compare_date As Date
                    If stage2_start_date <> Nothing Then
                        compare_date = stage2_start_date
                    Else
                        compare_date = CDate(Comp_dateTextBox.Text)
                    End If
                    Dim note_dataset As DataSet = get_dataset(param1, param2)
                    For idx = 0 To no_of_rows - 1
                        Dim created_date As Date = note_dataset.Tables(0).Rows(idx).Item(1)
                        Dim note_text As String = note_dataset.Tables(0).Rows(idx).Item(0)
                        If InStr(note_text, "Complaint_Ack") > 0 And _
                        created_date >= compare_date And _
                        InStr(note_text, "Stage 2") > 0 Then
                            stage2_ack_datepicker.Text = FormatDateTime(note_dataset.Tables(0).Rows(idx).Item(1), DateFormat.ShortDate)
                            stage2_ack_datepicker.Visible = True
                            stage2_ack_cbox.Checked = True
                            stage2_ack_date = stage2_ack_datepicker.Value
                        End If
                    Next
                End If
            End If
            orig_stage2_ack_date = stage2_ack_date

            'check if stage 2 holding letter has been sent
            If stage2_holding_date <> CDate("1800,1,1") Then
                stage2_holding_datepicker.Visible = True
                stage2_holding_cbox.Checked = True
                stage2_holding_datepicker.Value = stage2_holding_date
            Else
                stage2_holding_datepicker.Visible = False
                stage2_holding_cbox.Checked = False
                If case_no > 0 And stage2_start_date <> Nothing Then
                    If check_held_letter(case_no, stage2_start_date) = "Y" Then
                        stage2_holding_datepicker.Text = FormatDateTime(parm_hold_date, DateFormat.ShortDate)
                        stage2_holding_datepicker.Visible = True
                        stage2_holding_cbox.Checked = True
                    End If
                End If
            End If

            orig_stage2_holding_date = stage2_holding_date

            'stage
            stage_ComboBox.Text = "Stage " & stage_no

            'corrective action
            If cor_code > 0 Then
                Me.Corrective_actionsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Corrective_actions, cor_code)
                cor_name = PraiseAndComplaintsSQLDataSet.Corrective_actions.Rows(0).Item(1)
                orig_cor_text = cor_name
            End If
            Me.Corrective_actionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Corrective_actions)
            cor_nameComboBox.Items.Clear()
            idx = 0
            For Each row In PraiseAndComplaintsSQLDataSet.Corrective_actions.Rows
                cor_nameComboBox.Items.Add(PraiseAndComplaintsSQLDataSet.Corrective_actions.Rows(idx).Item(1))
                idx += 1
            Next
            If cor_code = 0 Then
                cor_nameComboBox.Text = "N/A"
                cor_nameComboBox.SelectedItem = -1
            Else
                cor_nameComboBox.Text = cor_name
            End If

            'hold reason
            If holding_cbox.Checked = True Then
                hold_name_ComboBox.Visible = False
                reason_label.Visible = False
                hold_code = 1
                orig_hold_name = "N/A"
            Else
                reason_label.Visible = True
                hold_name_ComboBox.Visible = True
                If log_code = test_logid Then
                    hold_code = add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(23)
                Else
                    hold_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(23)
                End If
                Me.Hold_reasonTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Hold_reason)
                hold_name_ComboBox.Items.Clear()
                idx = 0
                For Each row In PraiseAndComplaintsSQLDataSet.Hold_reason.Rows
                    hold_name_ComboBox.Items.Add(PraiseAndComplaintsSQLDataSet.Hold_reason.Rows(idx).Item(1))
                    idx += 1
                Next
                Me.Hold_reasonTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Hold_reason, hold_code)
                Try
                    hold_name_ComboBox.Text = Me.PraiseAndComplaintsSQLDataSet.Hold_reason.Rows(0).Item(1)
                Catch ex As Exception
                    hold_name_ComboBox.Text = ""
                End Try

                orig_hold_name = hold_name_ComboBox.Text
            End If
            orig_hold_code = hold_code

            'priority
            Comp_priorityCheckBox.Checked = orig_priority

            'if complaint is completed or was previously completed can only change response,stage and priority
            'can now also change case number
            Dim prev_completed = True
            Dim comp_date As Date
            Try
                If log_code = test_logid Then
                    comp_date = CDate(add_compfrm.Test_complaintsDataSet.test_complaints.Rows(0).Item(14))
                Else
                    comp_date = CDate(PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(14))
                End If
            Catch
                prev_completed = False
            End Try
            If comp_date < CDate("Jan 1 2000") Then
                prev_completed = False
            End If

            'stage > 1 can now update details box
            If prev_completed = True Then
                recpt_combobox.Enabled = False
                recvd_textComboBox.Enabled = False
                'comp_case_noTextBox.Enabled = False
                cl_ComboBox.Enabled = False
                comp_against_codeComboBox.Enabled = False
                agentComboBox.Enabled = False
                'cat_codeComboBox.Enabled = False
                'Cat_textComboBox.Enabled = False
                If stage_no < 2 Then
                    Comp_textTextBox.Enabled = False
                Else
                    Comp_textTextBox.Enabled = True
                End If
            Else
                recpt_combobox.Enabled = True
                recvd_textComboBox.Enabled = True
                comp_case_noTextBox.Enabled = True
                cl_ComboBox.Enabled = True
                comp_against_codeComboBox.Enabled = True
                agentComboBox.Enabled = True
                cat_codeComboBox.Enabled = True
                Cat_textComboBox.Enabled = True
                Comp_textTextBox.Enabled = True
            End If
            If prev_completed = True And completed = False Then
                prevcomplbl.Visible = True
            Else
                prevcomplbl.Visible = False
            End If

            'old comp no
            old_comp_notextbox.Text = orig_old_comp_no

            'costs cancelled
            If costs_cancel = 0 Then
                costs_cancel_tbox.Text = " "
                costs_reasonbtn.Visible = False
            Else
                costs_cancel_tbox.Text = Format(costs_cancel, "�#0.00")
                costs_reasonbtn.Visible = True
            End If

            'compensation
            If compensation = 0 Then
                compensation_tbox.Text = " "
            Else
                compensation_tbox.Text = Format(compensation, "�#0.00")
            End If

            'response forgotten
            If orig_resp_forgotten = 0 Then
                respcbox.Checked = False
            Else
                respcbox.Checked = True
            End If

            'stage 2 group box
            If stage_no > 1 Then
                stage2_start_datepicker.Visible = True
                stage2_start_datepicker.Text = stage2_start_date
                stage2_completed_datepicker.Visible = True
                stage2_completed_by_combobox.Visible = True
                Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
                Dim row As DataRow
                Dim idx2 As Integer
                stage2_completed_by_combobox.Items.Clear()
                For Each row In Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows
                    stage2_completed_by_combobox.Items.Add(PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx2).Item(1))
                    If Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx2).Item(0) = stage2_completed_by Then
                        stage2_completed_by_combobox.Text = Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx2).Item(1)
                    End If
                    idx2 += 1
                Next
                If stage2_completed_by > 0 Then
                    stage2_completed_datepicker.Text = stage2_completed_date
                Else
                    stage2_completed_datepicker.Visible = False
                    stage2_completed_by_combobox.SelectedItem = -1
                End If
            Else
                stage2_start_datepicker.Visible = False
                stage2_completed_datepicker.Visible = False
                stage2_completed_by_combobox.Visible = False
                stage2_start_date = Nothing
                stage2_completed_date = Nothing
                stage2_completed_by = 0
            End If

            'stage 3 group box
            If stage_no > 2 Then
                stage3_start_datepicker.Visible = True
                stage3_start_datepicker.Text = stage3_start_date
                stage3_completed_datepicker.Visible = True
                stage3_completed_by_combobox.Visible = True
                Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
                Dim row As DataRow
                Dim idx2 As Integer
                stage3_completed_by_combobox.Items.Clear()
                For Each row In Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows
                    stage3_completed_by_combobox.Items.Add(PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx2).Item(1))
                    If Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx2).Item(0) = stage3_completed_by Then
                        stage3_completed_by_combobox.Text = Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx2).Item(1)
                    End If
                    idx2 += 1
                Next
                If stage3_completed_by > 0 Then
                    stage3_completed_datepicker.Text = stage3_completed_date
                Else
                    stage3_completed_datepicker.Visible = False
                    stage3_completed_by_combobox.SelectedItem = -1
                End If
            Else
                stage3_start_datepicker.Visible = False
                stage3_completed_datepicker.Visible = False
                stage3_completed_by_combobox.Visible = False
                stage3_start_date = Nothing
                stage3_completed_date = Nothing
                stage3_completed_by = 0
            End If

            'legal group box
            If legal_ins_flag = "N" Then
                legalcbox.Checked = False
                legalgbox.Visible = False
                solicitor_datepicker.Value = Now
                ins_datepicker.Value = Now
            Else
                legalcbox.Checked = True
                legalgbox.Visible = True
                If referred_to_solicitor = CDate("1800,1,1") Then
                    solicitor_cbox.Checked = False
                    solicitor_datepicker.Visible = False
                    solicitor_datepicker.Value = Now
                Else
                    solicitor_cbox.Checked = True
                    solicitor_datepicker.Visible = True
                    solicitor_datepicker.Value = referred_to_solicitor
                End If
                If referred_to_insurer = CDate("1800,1,1") Then
                    inscbox.Checked = False
                    ins_datepicker.Visible = False
                    ins_datepicker.Value = Now
                Else
                    inscbox.Checked = True
                    ins_datepicker.Visible = True
                    ins_datepicker.Value = referred_to_insurer
                End If
                If monetary_risk = -999 Then
                    monetary_risktbox.Text = ""
                Else
                    monetary_risktbox.Text = Format(monetary_risk, "�#0.00")
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub populate_agentcombo()
        Try
            Dim idx As Integer
            dept_code = 0
            Dim where_clause As String
            If comp_against_codeComboBox.SelectedIndex = 2 Then
                agent_intext = "I"
            ElseIf comp_against_codeComboBox.SelectedIndex = 3 Then
                agent_intext = "E"
            Else
                agent_intext = " "
            End If
            If agent_intext <> " " Then
                where_clause = " where agent_type = 'B' and internalExternal = '" & agent_intext & "'"
            Else
                where_clause = " where agent_type <> 'B' "
            End If
            populate_bailiff_table(where_clause)
            
            For idx = 1 To agent_rows
                agentComboBox.Items.Add(bailiff_table(idx, 2))
                If agent_no = bailiff_table(idx, 1) Then
                    agentComboBox.Text = bailiff_table(idx, 2)
                End If
            Next
            agentComboBox.Items.Add("Various")
            If agent_no = 9999 Then
                agentComboBox.Text = "Various"
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub
    Private Sub populate_agent2combo()
        Try
            Dim idx As Integer
            Dim where_clause As String
            If comp_against2_codecombobox.SelectedIndex = 2 Then
                agent_intext = "I"
            ElseIf comp_against2_codecombobox.SelectedIndex = 3 Then
                agent_intext = "E"
            Else
                agent_intext = " "
            End If
            If agent_intext <> " " Then
                where_clause = " where agent_type = 'B' and internalExternal = '" & agent_intext & "'"
            Else
                where_clause = " where agent_type <> 'B' "
            End If
            populate_bailiff_table(where_clause)

            For idx = 1 To agent_rows
                agent2_combobox.Items.Add(bailiff_table(idx, 2))
                If agent2_no = bailiff_table(idx, 1) Then
                    agent2_combobox.Text = bailiff_table(idx, 2)
                End If
            Next
            agent2_combobox.Items.Add("Various")
            If agent2_no = 9999 Then
                agent2_combobox.Text = "Various"
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub
    Private Sub populate_departments()
        'Me.DepartmentsTableAdapter.Fill(Me.DepartmentsDataSet.Departments)
        'agentComboBox.Items.Clear()
        'Dim idx As Integer = 0
        'Dim row As DataRow
        'dept_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(10)
        'For Each row In DepartmentsDataSet.Tables(0).Rows
        '    agentComboBox.Items.Add(DepartmentsDataSet.Tables(0).Rows(idx).Item(1))
        '    If DepartmentsDataSet.Tables(0).Rows(idx).Item(0) = dept_code _
        '    And orig_against_code > 0 Then
        '        agentComboBox.Text = DepartmentsDataSet.Tables(0).Rows(idx).Item(1)
        '    End If
        '    idx += 1
        'Next
        'dept_rows = idx
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Comp_case_noTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Cat_textComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cat_textComboBox.SelectedIndexChanged

    End Sub


    Private Sub Recvd_textComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub agentComboBox_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentComboBox.Enter

    End Sub

    Private Sub agentComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agentComboBox.SelectedIndexChanged

    End Sub

    Private Sub cl_ComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cl_ComboBox.SelectedIndexChanged

    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub GroupBox1_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupBox1.Validated
        If comprbtn.Checked = True Then
            completed = True
            compdatetimepicker.Visible = True
            compbycombobox.Visible = True
            If orig_completed = False Then
                compdatetimepicker.Value = Now
                compbycombobox.SelectedIndex = -1
                compbycombobox.Text = "Not yet known"
            End If
            Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
            Dim row As DataRow
            Dim idx As Integer
            compbycombobox.Items.Clear()
            For Each row In Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows
                compbycombobox.Items.Add(PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(1))
                idx += 1
            Next
        Else
            completed = False
            compdatetimepicker.Visible = False
            compbycombobox.Visible = False
        End If
    End Sub

    Private Sub compdatetimepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles compdatetimepicker.Validating
        
        ErrorProvider1.SetError(compdatetimepicker, "")
        If compdatetimepicker.Value > Now Then
            ErrorProvider1.SetError(compdatetimepicker, "Completion Date can't be in the future")
            e.Cancel = True
        ElseIf compdatetimepicker.Value < Comp_dateTextBox.Text Then
            ErrorProvider1.SetError(compdatetimepicker, "Completion Date must be after date received")
            e.Cancel = True
        ElseIf stage_no > 1 And stage2_completed_by > 0 And compdatetimepicker.Value > stage2_completed_datepicker.Text Then
            ErrorProvider1.SetError(compdatetimepicker, "Completion date must not be after stage 2 completion date")
            e.Cancel = True
        End If
    End Sub

    Private Sub compdatetimepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles compdatetimepicker.ValueChanged

    End Sub

    Private Sub GroupBox3_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    
    Private Sub GroupBox2_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupBox2.Validated
        If foundedrbtn.Checked = True Then
            new_founded = "Y"
        ElseIf unfoundedrbtn.Checked = True Then
            new_founded = "N"
        Else
            new_founded = "U"
        End If
    End Sub

    Private Sub compbytextbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub updbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updbtn.Click
        Try
            Dim change_made As Boolean = False
            Dim stage2_change_made As Boolean = False
            Dim stage2_completed_change_made As Boolean = False
            Dim stage3_change_made As Boolean = False
            Dim stage3_completed_change_made As Boolean = False

            ErrorProvider1.SetError(Comp_dateTextBox, "")
            ErrorProvider1.SetError(agentComboBox, "")
            ErrorProvider1.SetError(Cat_textComboBox, "")
            ErrorProvider1.SetError(updbtn, "")
            ErrorProvider1.SetError(actions_ComboBox, "")
            ErrorProvider1.SetError(GroupBox1, "")
            ErrorProvider1.SetError(GroupBox2, "")
            ErrorProvider1.SetError(compbycombobox, "")
            ErrorProvider1.SetError(Comp_textTextBox, "")
            ErrorProvider1.SetError(Comp_responseTextBox, "")
            ErrorProvider1.SetError(stage2_gbox, "")
            ErrorProvider1.SetError(stage3_gbox, "")

            If Len(agentComboBox.Text) = 0 And _
            (comp_against_codeComboBox.SelectedIndex > 1 _
            And comp_against_codeComboBox.SelectedIndex < 5) Then
                ErrorProvider1.SetError(agentComboBox, "Select Agent or Various")
                agentComboBox.Focus()
                Exit Sub
            End If

            If Len(Cat_textComboBox.Text) = 0 Then
                ErrorProvider1.SetError(Cat_textComboBox, "Select category code")
                agentComboBox.Focus()
                Exit Sub
            End If

            'If orig_completed And comprbtn.Checked = False And orig_checked And chkdrbtn.Checked = True Then
            '    ErrorProvider1.SetError(updbtn, "Need to change to not checked if you are changing to not complete")
            '    Exit Sub
            'End If

            If orig_founded = "U" And (foundedrbtn.Checked Or unfoundedrbtn.Checked) _
            And comprbtn.Checked = False Then
                ErrorProvider1.SetError(GroupBox2, "Can only set Founded/unfounded when complaint completed")
                Exit Sub
            End If

            If openrbtn.Checked And comprbtn.Checked = True Then
                ErrorProvider1.SetError(GroupBox2, "Need to set Founded/unfounded when complaint is completed")
                Exit Sub
            End If

            If comprbtn.Checked = True And compbycombobox.SelectedIndex = -1 Then
                ErrorProvider1.SetError(compbycombobox, "Need to set who completed the complaint")
                Exit Sub
            End If

            If compbycombobox.Text = "Not yet known" Then
                ErrorProvider1.SetError(compbycombobox, "Please select who completed the complaint")
                Exit Sub
            End If

            If openrbtn.Checked = False And comprbtn.Checked = False Then
                MsgBox("Founded/unfounded will be changed to not set")
                new_founded = "U"
                openrbtn.Checked = True
                Exit Sub
            End If

            If (orig_founded = "N" Or orig_founded = "Y") And openrbtn.Checked _
                    And comprbtn.Checked = True Then
                ErrorProvider1.SetError(GroupBox2, "Can only reset Founded/unfounded when complaint not completed")
                Exit Sub
            End If

            If orig_founded <> "Y" And new_founded = "Y" And actions_ComboBox.Text = "N/A" Then
                ErrorProvider1.SetError(actions_ComboBox, "Action needs to be entered")
                Exit Sub
            End If

            If Microsoft.VisualBasic.Len(Comp_textTextBox.Text) > 500 Then
                ErrorProvider1.SetError(Comp_textTextBox, "Maximum length of text is 500 characters")
                Exit Sub
            End If

            If Microsoft.VisualBasic.Len(Comp_responseTextBox.Text) > 500 Then
                ErrorProvider1.SetError(Comp_responseTextBox, "Maximum length of response is 500 characters")
                Exit Sub
            End If

            If stage_no > 2 And stage2_start_datepicker.Visible = False Then
                ErrorProvider1.SetError(stage2_gbox, "Enter stage 2 before a higher stage")
                Exit Sub
            End If

            If stage_no > 2 And stage2_completed_by = 0 Then
                ErrorProvider1.SetError(stage2_gbox, "Enter stage 2 completed by before moving on to next stage")
                Exit Sub
            End If

            If stage_no > 1 And stage2_completed_by > 0 And CDate(stage2_start_date) > CDate(stage2_completed_date) Then
                ErrorProvider1.SetError(stage2_gbox, "Stage 2 completed date must be on or after start date")
                Exit Sub
            End If

            If stage_no > 2 And stage3_completed_by > 0 And Format(CDate(stage3_start_date), "yyyy-MM-dd") > _
            Format(CDate(stage3_completed_date), "yyyy-MM-dd") Then
                ErrorProvider1.SetError(stage3_gbox, "Stage 3 completed date must be on or after start date")
                Exit Sub
            End If

            If stage_no = 3 And stage3_start_date = CDate("01 01 1800") Then
                ErrorProvider1.SetError(stage3_gbox, "stage 3 start date must be entered")
                Exit Sub
            End If
            'write log for changes made
            log_type = "update complaint"
            If orig_entered_date <> Comp_dateTextBox.Text Then
                log_text = "Received date changed from " & orig_entered_date & " to " & Comp_dateTextBox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If recpt_code <> orig_recpt_code Then
                log_text = "Form of Receipt changed from " & orig_recpt_text & " to " & recpt_combobox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If recvd_from <> orig_recvd_from Then
                log_text = "Received from changed from " & orig_recvd_text & " to " & recvd_textComboBox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If case_no <> orig_case_no Then
                log_text = "Case number changed from " & orig_case_no & _
                                          " to " & case_no
                add_log(log_type, log_text)
                change_made = True
            End If

            If cl_no <> orig_cl_no Then
                log_text = "Client number changed from " & orig_cl_no & _
                                          " to " & cl_no
                add_log(log_type, log_text)
                change_made = True
            End If

            If against_code <> orig_against_code Then
                log_text = "Complaint Against changed from " & orig_against_text & " to " & comp_against_codeComboBox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If agent_no <> orig_agent_no Then
                log_text = "Agent changed from " & orig_agent_no & _
                                          " to " & agent_no
                add_log(log_type, log_text)
                change_made = True
            End If

            'If dept_code <> PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(10) Then
            '    log_text = "Department changed from " & PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(10) & _
            '                              " to " & dept_code
            '    updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "update complaint", comp_no, log_text)
            '    change_made = True
            'End If
            If against2_code <> orig_against2_code Then
                log_text = "Complaint Against2 changed from " & orig_against2_text & " to " & comp_against2_codecombobox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If agent2_no <> orig_agent2_no Then
                log_text = "Agent2 changed from " & orig_agent2_no & _
                                          " to " & agent2_no
                add_log(log_type, log_text)
                change_made = True
            End If

            If cat_code <> orig_cat_code Or _
                cat_number <> orig_cat_number Then
                log_text = "Category changed from " & orig_cat_code & _
                                 orig_cat_number & " to " & cat_code & cat_number
                add_log(log_type, log_text)
                change_made = True
            End If

            If alloc_to_code <> orig_alloc_to_code Then
                log_text = "Investigator changed from " & orig_inv_text & " to " & invComboBox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If action_code <> orig_action_code Then
                log_text = "Action changed from " & orig_action_text & " to " & actions_ComboBox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If Comp_textTextBox.Text <> orig_details Then
                log_text = "Complaint details changed "
                add_log(log_type, log_text)
                If log_code <> test_logid Then
                    Detail_historyTableAdapter.InsertQuery(comp_no, Now, log_code, orig_details)
                End If
                change_made = True
            End If

            If Comp_responseTextBox.Text <> orig_resp_details Then
                log_text = "Complaint response changed "
                add_log(log_type, log_text)
                If log_code <> test_logid Then
                    ResponseTableAdapter.InsertQuery(comp_no, Now, log_code, orig_resp_details)
                End If
                change_made = True
            End If

            If stage_no <> orig_stage_no Then
                log_text = "Stage changed from " & orig_stage_no & " to " & stage_no
                add_log(log_type, log_text)
                change_made = True
            End If

            If cor_code <> orig_cor_code Then
                log_text = "Corrective action changed from " & orig_cor_text & " to " & cor_nameComboBox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If hold_code <> orig_hold_code Then
                log_text = "Hold reason changed from " & orig_hold_name & " to " & hold_name_ComboBox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If Comp_priorityCheckBox.Checked <> orig_priority Then
                log_text = "Priority changed from " & orig_priority & _
                                          " to " & Comp_priorityCheckBox.Checked
                add_log(log_type, log_text)
                change_made = True
            End If

            If old_comp_no <> orig_old_comp_no Then
                log_text = "Old complaint number changed from " & orig_old_comp_no & _
                                                      " to " & old_comp_no
                add_log(log_type, log_text)
                change_made = True
            End If

            If costs_cancel <> orig_costs_cancel Then
                log_text = "Cancel costs changed from " & Format(orig_costs_cancel, "�#0.00") & _
                                                      " to " & Format(costs_cancel, "�#0.00")
                add_log(log_type, log_text)
                change_made = True
            End If

            If compensation <> orig_compensation Then
                log_text = "Compensation changed from " & Format(orig_compensation, "�#0.00") & _
                                                      " to " & Format(compensation, "�#0.00")
                add_log(log_type, log_text)
                change_made = True
            End If

            If resp_forgotten <> orig_resp_forgotten Then
                If orig_resp_forgotten = 0 Then
                    log_text = "Response forgotten set"
                Else
                    log_text = "Response forgotten reset"
                End If
                add_log(log_type, log_text)
                change_made = True
            End If

            If stage2_start_date <> orig_stage2_start_date Then
                log_text = "Stage2 start date changed from " & Format(orig_stage2_start_date, "dd/MM/yyyy") & _
                                                      " to " & Format(stage2_start_date, "dd/MM/yyyy")
                add_log(log_type, log_text)
                stage2_change_made = True
            End If

            If stage_no > 1 And stage2_completed_by > 0 And stage2_completed_date <> orig_stage2_completed_date Then
                log_text = "Stage2 completed date changed from " & Format(orig_stage2_completed_date, "dd/MM/yyyy") & _
                                                      " to " & Format(stage2_completed_date, "dd/MM/yyyy")
                add_log(log_type, log_text)
                stage2_completed_change_made = True
            End If

            If gender <> orig_gender Then
                log_text = "Gender changed from " & orig_gender & _
                                                      " to " & gender
                add_log(log_type, log_text)
                change_made = True
            End If

            If ethnicity_desc <> orig_ethnicity_desc Then
                log_text = "Ethnicity changed from " & orig_ethnicity_desc & _
                                                                      " to " & ethnicity_desc
                add_log(log_type, log_text)
                change_made = True
                'get new ethnicity_code
                Dim idx As Integer
                For idx = 0 To Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows.Count - 1
                    Dim eth_code As Integer = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(0)
                    Dim eth_desc As String = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(1)
                    If ethnicity_desc = eth_desc Then
                        ethnicity = eth_code
                        Exit For
                    End If
                Next
            End If
            If ack_cbox.Checked Then
                ack_date = ack_datepicker.Value
            End If
            If ack_date <> orig_ack_date Then
                log_text = "Acknowledgement Date changed from "
                If orig_ack_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(orig_ack_date, "dd/MM/yyyy")
                End If
                log_text = log_text & " to "
                If ack_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(ack_date, "dd/MM/yyyy")
                End If
                add_log(log_type, log_text)
                change_made = True
            End If
            If holding_cbox.Checked Then
                holding_date = holding_datepicker.Value
            End If
            If holding_date <> orig_holding_date Then
                log_text = "Holding Date changed from "
                If orig_holding_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(orig_holding_date, "dd/MM/yyyy")
                End If
                log_text = log_text & " to "
                If holding_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(holding_date, "dd/MM/yyyy")
                End If
                add_log(log_type, log_text)
                change_made = True
            End If
            If stage2_ack_cbox.Checked Then
                stage2_ack_date = stage2_ack_datepicker.Value
            End If
            If stage2_ack_date <> orig_stage2_ack_date Then
                log_text = "Stage 2 Acknowledgement Date changed from "
                If orig_stage2_ack_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(orig_stage2_ack_date, "dd/MM/yyyy")
                End If
                log_text = log_text & " to "
                If stage2_ack_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(stage2_ack_date, "dd/MM/yyyy")
                End If
                add_log(log_type, log_text)
                change_made = True
            End If
            If stage2_holding_cbox.Checked Then
                stage2_holding_date = stage2_holding_datepicker.Value
            End If
            If stage2_holding_date <> orig_stage2_holding_date Then
                log_text = "Holding Date changed from "
                If orig_stage2_holding_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(orig_stage2_holding_date, "dd/MM/yyyy")
                End If
                log_text = log_text & " to "
                If stage2_holding_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(stage2_holding_date, "dd/MM/yyyy")
                End If
                add_log(log_type, log_text)
                change_made = True
            End If

            If stage_no > 2 And stage3_start_date <> orig_stage3_start_date Then
                log_text = "Stage3 start date changed from " & Format(orig_stage3_start_date, "dd/MM/yyyy") & _
                                                      " to " & Format(stage3_start_date, "dd/MM/yyyy")
                add_log(log_type, log_text)
                stage3_change_made = True
            End If

            If stage_no > 2 And stage3_completed_by > 0 And stage3_completed_date <> orig_stage3_completed_date Then
                log_text = "Stage3 completed date changed from " & Format(orig_stage3_completed_date, "dd/MM/yyyy") & _
                                                      " to " & Format(stage3_completed_date, "dd/MM/yyyy")
                add_log(log_type, log_text)
                stage3_completed_change_made = True
            End If

            'changing from completed_by > 1 to not yet known (1) treat as reset back to zero
            If orig_stage2_completed_by > 1 And stage2_completed_by = 1 Then
                stage2_completed_by = 0
            End If

            If stage_no > 1 And stage2_completed_by <> orig_stage2_completed_by Then
                log_text = "Stage2 completed by changed from " & orig_stage2_completed_by & _
                                                      " to " & stage2_completed_by
                add_log(log_type, log_text)
                stage2_completed_change_made = True
            End If

            If stage_no > 2 And stage3_completed_by <> orig_stage3_completed_by Then
                log_text = "Stage3 completed by changed from " & orig_stage3_completed_by & _
                                                      " to " & stage3_completed_by
                add_log(log_type, log_text)
                stage3_completed_change_made = True
            End If
            'update complaints database
            If change_made Then
                'save on test database for test logid
                If log_code = test_logid Then
                    Try
                        add_compfrm.Test_complaintsTableAdapter.UpdateQuery1(Comp_dateTextBox.Text, recpt_code, _
                        recvd_from, case_no, cl_no, _
                        against_code, agent_no, cat_code, _
                        cat_number, dept_code, alloc_to_code, _
                        Comp_textTextBox.Text, Comp_responseTextBox.Text, _
                        action_code, stage_no, cor_code, hold_code, Comp_priorityCheckBox.Checked, _
                         old_comp_no, costs_cancel, compensation, resp_forgotten, gender, ethnicity, _
                         ack_date, holding_date, stage2_ack_date, stage2_holding_date, against2_code, agent2_no, comp_no)
                    Catch ex2 As Exception
                        MessageBox.Show(ex2.Message)
                        Me.Close()
                        Exit Sub
                    End Try
                Else
                    Try
                        ComplaintsTableAdapter.UpdateQuery1(Comp_dateTextBox.Text, recpt_code, _
                        recvd_from, case_no, cl_no, _
                        against_code, agent_no, cat_code, _
                        cat_number, dept_code, alloc_to_code, _
                        Comp_textTextBox.Text, Comp_responseTextBox.Text, _
                        action_code, stage_no, cor_code, hold_code, Comp_priorityCheckBox.Checked, _
                        old_comp_no, costs_cancel, compensation, resp_forgotten, gender, ethnicity, _
                        ack_date, holding_date, stage2_ack_date, stage2_holding_date, against2_code, agent2_no, comp_no)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                        Me.Close()
                        Exit Sub
                    End Try
                End If
            End If
            'check if completed has changed
            If orig_completed <> completed Then
                If completed = False Then
                    compby_code = 0
                End If
                'save on test database for test logid
                If log_code = test_logid Then
                    add_compfrm.Test_complaintsTableAdapter.UpdateQuery2(compdatetimepicker.Text, compby_code, comp_no)
                Else
                    ComplaintsTableAdapter.UpdateQuery2(compdatetimepicker.Text, compby_code, comp_no)
                End If
                If completed Then
                    log_text = "Complaint changed to completed by " & compbycombobox.Text
                    'check if one of the clients to notify
                    If cl_no > 0 Then
                        Me.Complaints_resp_clientsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaints_resp_clients, cl_no)
                        If Me.PraiseAndComplaintsSQLDataSet.Complaints_resp_clients.Rows.Count = 1 Then
                            MsgBox("Don't forget to send details to the client")
                        End If
                    End If
                Else
                    log_text = "Complaint changed to not completed "
                End If
                add_log(log_type, log_text)
                change_made = True
            End If
            'check if completed date or completed by has changed
            Dim completed_changed As Boolean = False
            If completed And orig_completed Then
                If orig_comp_date <> compdatetimepicker.Text Then
                    log_text = "Completed date changed from " & orig_comp_date & _
                                                          " to " & CDate(compdatetimepicker.Text)
                    add_log(log_type, log_text)
                    completed_changed = True
                End If
                If orig_compby <> compbycombobox.Text Then
                    log_text = "Completed by changed from " & orig_compby & _
                                                                          " to " & compbycombobox.Text
                    add_log(log_type, log_text)
                    completed_changed = True
                End If
                If completed_changed Then
                    'save on test database for test logid
                    If log_code = test_logid Then
                        add_compfrm.Test_complaintsTableAdapter.UpdateQuery2(compdatetimepicker.Text, compby_code, comp_no)
                    Else
                        ComplaintsTableAdapter.UpdateQuery2(compdatetimepicker.Text, compby_code, comp_no)
                    End If
                End If
            End If

            'check if founded has changed
            If orig_founded <> new_founded And Not (new_founded = Nothing) Then
                If new_founded = "Y" Then
                    log_text = "Complaint set to founded"
                ElseIf new_founded = "N" Then
                    log_text = "Complaint set to not founded"
                Else
                    log_text = "Complaint set to founded not set"
                End If

                'save on test database for test logid
                If log_code = test_logid Then
                    add_compfrm.Test_complaintsTableAdapter.UpdateQuery3(new_founded, comp_no)
                Else
                    ComplaintsTableAdapter.UpdateQuery3(new_founded, comp_no)
                End If
                add_log(log_type, log_text)
                change_made = True
            End If

            'see if checked by has changed
            'Dim checked_by As Integer
            'If checked <> orig_checked Then
            '    If checked Then
            '        log_text = "Complaint changed to checked"
            '        checked_by = log_code
            '    Else
            '        log_text = "Complaint changed to not checked"
            '        checked_by = 0
            '    End If
            '    'save on test database for test logid
            '    If log_code = test_logid Then
            '        ComplaintsTableAdapter.UpdateQuery4(checkeddatetimepicker.Value, checked_by, comp_no)
            '    Else
            '        ComplaintsTableAdapter.UpdateQuery4(checkeddatetimepicker.Value, checked_by, comp_no)
            '    End If
            '    add_log(log_type, log_text)
            '    change_made = True
            'End If

            'see if stage2 has changed
            If stage2_change_made Then
                If log_code = test_logid Then
                    add_compfrm.Test_complaintsTableAdapter.UpdateQuery4(stage2_start_date, comp_no)
                Else
                    ComplaintsTableAdapter.UpdateQuery4(stage2_start_date, comp_no)
                End If
            End If
            If stage3_change_made Then
                If log_code = test_logid Then
                    add_compfrm.Test_complaintsTableAdapter.UpdateQuery6(stage3_start_date, comp_no)
                Else
                    ComplaintsTableAdapter.UpdateQuery6(stage3_start_date, comp_no)
                End If
            End If
            If stage2_completed_change_made Or (stage_no = 1 And orig_stage_no > 1) Then
                If stage_no = 1 Then
                    stage2_completed_by = 0
                End If
                If log_code = test_logid Then
                    add_compfrm.Test_complaintsTableAdapter.UpdateQuery5(stage2_completed_by, stage2_completed_date, comp_no)
                Else
                    ComplaintsTableAdapter.UpdateQuery5(stage2_completed_by, stage2_completed_date, comp_no)
                End If
            End If

            If stage3_completed_change_made Or (stage_no = 2 And orig_stage_no > 2) Then
                If stage_no = 2 Then
                    stage3_completed_by = 0
                End If
                If log_code = test_logid Then
                    add_compfrm.Test_complaintsTableAdapter.UpdateQuery7(stage3_completed_by, stage3_completed_date, comp_no)
                Else
                    ComplaintsTableAdapter.UpdateQuery7(stage3_completed_by, stage3_completed_date, comp_no)
                End If
            End If

            'see if legal has changed
            Dim legal_change As Boolean = False
            If legal_ins_flag <> orig_legal_ins_flag Then
                legal_change = True
                If legal_ins_flag = "N" Then
                    referred_to_solicitor = CDate("1800,1,1")
                    referred_to_insurer = CDate("1800,1,1")
                    monetary_risk = -999
                    log_text = "Legal Insurance changed from Yes to No"
                Else
                    log_text = "Legal Insurance changed from No to Yes"
                End If
                add_log(log_type, log_text)
            End If
            If legal_ins_flag = "Y" Then
                If orig_referred_to_solicitor <> referred_to_solicitor Then
                    legal_change = True
                    log_text = "Referred to solicitor date changed from "
                    If orig_referred_to_solicitor = CDate("1800,1,1") Then
                        log_text = log_text & "blank"
                    Else
                        log_text = log_text & orig_referred_to_solicitor
                    End If
                    log_text = log_text & " to " & CDate(solicitor_datepicker.Text)
                    add_log(log_type, log_text)
                End If
                If orig_referred_to_insurer <> referred_to_insurer Then
                    legal_change = True
                    log_text = "Referred to insurer date changed from "
                    If orig_referred_to_insurer = CDate("1800,1,1") Then
                        log_text = log_text & "blank"
                    Else
                        log_text = log_text & orig_referred_to_insurer
                    End If
                    log_text = log_text & " to " & CDate(ins_datepicker.Text)
                    add_log(log_type, log_text)
                End If
                If orig_monetary_risk <> monetary_risk Then
                    legal_change = True
                    log_text = "Monetary Risk changed from "
                    If orig_monetary_risk = -999 Then
                        log_text = log_text & "blank"
                    Else
                        log_text = log_text & orig_monetary_risk
                    End If
                    log_text = log_text & " to "
                    If monetary_risk = -999 Then
                        log_text = log_text & "blank"
                    Else
                        log_text = log_text & Format(monetary_risk, "�#0.00")
                    End If
                    add_log(log_type, log_text)
                End If
            End If
            If legal_change = True Then
                
                If log_code = test_logid Then
                    add_compfrm.Test_complaintsTableAdapter.UpdateQuery9(legal_ins_flag, referred_to_solicitor, referred_to_insurer, monetary_risk, comp_no)
                Else
                    ComplaintsTableAdapter.UpdateQuery9(legal_ins_flag, referred_to_solicitor, referred_to_insurer, monetary_risk, comp_no)
                End If
            End If

            If cancel_costs_reason_change = True Then
                If cancel_costs_reason = "" Then
                    log_text = "Cancel Costs reason removed"
                Else
                    log_text = "Cancel Costs reason changed"
                End If

                add_log(log_type, log_text)
                If log_code = test_logid Then
                    add_compfrm.Test_complaintsTableAdapter.UpdateQuery10(cancel_costs_reason, comp_no)
                Else
                    ComplaintsTableAdapter.UpdateQuery10(cancel_costs_reason, comp_no)
                End If
            End If
            If change_made Or doc_change_made Or completed_changed Or stage2_change_made Or stage2_completed_change_made _
                Or stage3_change_made Or stage3_completed_change_made Or legal_change Or cancel_costs_reason_change Then
                MsgBox("Complaint has been updated")
            Else
                MsgBox("No changes have been made")
            End If

                Me.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub agentComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentComboBox.Validated
        Dim idx As Integer
        agent_no = 0
        If agentComboBox.SelectedItem = "Various" Then
            agent_no = 9999
        Else
            For idx = 1 To agent_rows
                If bailiff_table(idx, 2) = agentComboBox.SelectedItem Then
                    agent_no = bailiff_table(idx, 1)
                    Exit For
                End If
            Next
        End If
        dept_code = 0
        orig_against_code = against_code
    End Sub

    Private Sub Cat_textComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cat_textComboBox.Validated
        If Len(Cat_textComboBox.SelectedItem) = 0 Then
            cat_number = 0
        Else
            orig_cat_code = Microsoft.VisualBasic.Left(cat_codeComboBox.SelectedItem, 1)
            'cat_number is number before the space
            If Mid(Cat_textComboBox.SelectedItem, 2, 1) = " " Then
                cat_number = Trim(Microsoft.VisualBasic.Left(Cat_textComboBox.SelectedItem, 2))
            Else
                cat_number = Trim(Microsoft.VisualBasic.Left(Cat_textComboBox.SelectedItem, 3))
            End If
        End If
        If orig_cat_number <> cat_number And _
        (cat_number = 99 Or orig_cat_number = 99) Then
            MsgBox("Stage will be reset")
            If cat_number = 99 Then
                stage_Label.Visible = False
                stage_ComboBox.Visible = False
                stage_no = 0
            Else
                stage_Label.Visible = True
                stage_ComboBox.Visible = True
                stage_ComboBox.Text = "Stage 1"
                stage_no = 1
            End If
        End If
    End Sub

    Private Sub Cat_textComboBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Cat_textComboBox.Validating
        
    End Sub

    Private Sub invComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles invComboBox.SelectedIndexChanged

    End Sub

    Private Sub invComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles invComboBox.Validated
        Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
        Try
            alloc_to_code = PraiseAndComplaintsSQLDataSet.Investigators.Rows(invComboBox.SelectedIndex).Item(0)
        Catch
            alloc_to_code = 1
        End Try
    End Sub

    Private Sub cat_codeComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cat_codeComboBox.SelectedIndexChanged

    End Sub

    Private Sub cat_codeComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cat_codeComboBox.Validated
        Dim cat_table As New PraiseAndComplaintsSQLDataSet.Complaint_categoriesDataTable
        cat_code = Microsoft.VisualBasic.Left(cat_codeComboBox.SelectedItem, 1)

        If cat_code <> orig_cat_code Then
            orig_cat_code = ""
        End If
        Me.Complaint_categoriesTableAdapter.Fill2(cat_table, cat_code)
        Dim row As DataRow
        Dim cat_text As String
        Dim idx As Integer = 0
        Cat_textComboBox.Items.Clear()
        For Each row In cat_table.Rows
            cat_text = cat_table.Rows(idx).Item(1) & " " & cat_table.Rows(idx).Item(2)
            Cat_textComboBox.Items.Add(cat_text)
            If cat_number = cat_table.Rows(idx).Item(1) And orig_cat_code <> "" Then
                If cat_number < 99 Then
                    Cat_textComboBox.SelectedIndex = cat_number - 1
                Else
                    Select Case cat_number
                        Case 99
                            Cat_textComboBox.Text = "Contentious"
                        Case 100
                            Cat_textComboBox.Text = "Request Report"
                        Case 101
                            Cat_textComboBox.Text = "SAR"
                    End Select
                End If
            End If
            idx += 1
        Next

    End Sub

    Private Sub recpt_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recpt_combobox.SelectedIndexChanged

    End Sub

    Private Sub recpt_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles recpt_combobox.Validated

        recpt_code = PraiseAndComplaintsSQLDataSet.Receipt_type.Rows(recpt_combobox.SelectedIndex).Item(0)

    End Sub

    Private Sub recvd_textComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles recvd_textComboBox.Validated
        recvd_from = PraiseAndComplaintsSQLDataSet.Received_from.Rows(recvd_textComboBox.SelectedIndex).Item(0)
    End Sub

    Private Sub comp_case_noTextBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles comp_case_noTextBox.Validated
        case_no = comp_case_noTextBox.Text
        If case_no > 0 Then
            Dim idx As Integer
            Dim dob_str As String = "N/A"
            param1 = "onestep"
            param2 = "select dateOfBirth, clientschemeID from Debtor where _rowid = " & case_no
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 1 Then
                Dim dob As Date
                Try
                    dob = debtor_dataset.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    dob_str = "unknown"
                End Try
                If dob_str <> "unknown" Then
                    dob_str = Format(dob, "dd/MM/yyyy")
                End If
                dob_textbox.Text = dob_str
                'get debt type
                Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(1)
                param2 = "select schemeID from ClientScheme where _rowid = " & csid
                Dim csid_dataset As DataSet = get_dataset(param1, param2)
                Dim sch_id As Integer = csid_dataset.Tables(0).Rows(0).Item(0)
                param2 = "select name from Scheme where _rowid = " & sch_id
                Dim sch_dataset As DataSet = get_dataset(param1, param2)
                debt_typelbl.Text = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                'reset ack letter and holding letter dates
                If ack_date <> CDate("1800,1,1") Then
                    ack_datepicker.Visible = True
                    ack_cbox.Checked = True
                    ack_datepicker.Value = ack_date
                Else
                    ack_datepicker.Visible = False
                    ack_cbox.Checked = False
                    If case_no > 0 Then
                        param1 = "onestep"
                        param2 = " select text, _createdDate from Note" & _
                        " where debtorID = " & case_no & " and type = 'Letter'" & _
                        " and _createdDate >= '" & Format(CDate(Comp_dateTextBox.Text), "yyyy-MM-dd") & "'" & _
                        " order by _rowid"
                        Dim note_dataset As DataSet = get_dataset(param1, param2)
                        For idx = 0 To no_of_rows - 1
                            Dim note_text As String = note_dataset.Tables(0).Rows(idx).Item(0)
                            If InStr(note_text, "Complaint_Ack") > 0 And _
                                InStr(note_text, "Stage 2") = 0 Then
                                ack_datepicker.Text = FormatDateTime(note_dataset.Tables(0).Rows(idx).Item(1), DateFormat.ShortDate)
                                ack_datepicker.Visible = True
                                ack_cbox.Checked = True
                                ack_date = ack_datepicker.Value
                            End If
                        Next
                    End If
                End If
                If holding_date <> CDate("1800,1,1") Then
                    holding_datepicker.Visible = True
                    holding_cbox.Checked = True
                    holding_datepicker.Value = holding_date
                Else
                    holding_datepicker.Visible = False
                    holding_cbox.Checked = False
                    If check_held_letter(case_no, Comp_dateTextBox.Text) = "Y" Then
                        holding_datepicker.Text = FormatDateTime(parm_hold_date, DateFormat.ShortDate)
                        holding_datepicker.Visible = True
                        holding_cbox.Checked = True
                        holding_date = holding_datepicker.Value
                    End If
                End If
            End If
            If stage2_ack_date <> CDate("1800,1,1") Then
                stage2_ack_datepicker.Visible = True
                stage2_ack_cbox.Checked = True
                stage2_ack_datepicker.Value = stage2_ack_date
            Else
                stage2_ack_datepicker.Visible = False
                stage2_ack_cbox.Checked = False
                If case_no > 0 Then
                    param1 = "onestep"
                    param2 = " select text, _createdDate from Note" & _
                    " where debtorID = " & case_no & " and type = 'Letter'"
                    If stage2_start_date <> Nothing Then
                        param2 = param2 & " and _createdDate >= '" & Format(stage2_start_date, "yyyy-MM-dd") & "'"
                    Else
                        param2 = param2 & " and _createdDate >= '" & Format(CDate(Comp_dateTextBox.Text), "yyyy-MM-dd") & "'"
                    End If
                    param2 = param2 & " order by _rowid"
                    Dim note_dataset As DataSet = get_dataset(param1, param2)
                    For idx = 0 To no_of_rows - 1
                        Dim note_text As String = note_dataset.Tables(0).Rows(idx).Item(0)
                        If InStr(note_text, "Complaint_Ack") > 0 And _
                            InStr(note_text, "Stage 2") > 0 Then
                            stage2_ack_datepicker.Text = FormatDateTime(note_dataset.Tables(0).Rows(idx).Item(1), DateFormat.ShortDate)
                            stage2_ack_datepicker.Visible = True
                            stage2_ack_cbox.Checked = True
                            stage2_ack_date = stage2_ack_datepicker.Value
                        End If
                    Next
                End If
            End If
        End If
    End Sub

    Private Sub comp_case_noTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles comp_case_noTextBox.Validating
        Try
            ErrorProvider1.SetError(comp_case_noTextBox, "")
            If Len(Trim(comp_case_noTextBox.Text)) = 0 Then
                comp_case_noTextBox.Text = 0
            End If
            Try
                If comp_case_noTextBox.Text = 0 Then
                    populate_client_table()
                    Dim idx2 As Integer
                    For idx2 = 1 To cl_rows
                        cl_ComboBox.Items.Add(client_table(idx2, 2))
                    Next
                    Exit Sub
                End If
            Catch ex As Exception
                ErrorProvider1.SetError(comp_case_noTextBox, "Case number must be numeric")
                e.Cancel = True
                Exit Sub
            End Try

            'check case nunber is on onestep
            param1 = "onestep"
            param2 = "select clientschemeID from Debtor" & _
            " where _rowid = " & comp_case_noTextBox.Text
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows <> 1 Then
                ErrorProvider1.SetError(comp_case_noTextBox, "Case number is not on Onestep")
                e.Cancel = True
                Exit Sub
            End If

            Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(0)

            'get client ID
            param2 = "select clientID from ClientScheme" & _
            " where _rowid = " & csid
            Dim csid_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows <> 1 Then
                ErrorProvider1.SetError(comp_case_noTextBox, "Client scheme is not on Onestep")
                Exit Sub
            End If

            cl_no = csid_dataset.Tables(0).Rows(0).Item(0)
            'get client name
            param1 = "onestep"
            param2 = "select name from Client" & _
            " where _rowid = " & cl_no
            Dim cl_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows <> 1 Then
                MsgBox("Unable to get client name for " & cl_no)
                Exit Sub
            End If
            cl_name = Trim(cl_dataset.Tables(0).Rows(0).Item(0))

            cl_ComboBox.Items.Clear()
            cl_ComboBox.Items.Add(cl_name)
            cl_ComboBox.SelectedItem = cl_name
            comp_against_codeComboBox.Focus()
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub comp_case_noTextBox_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comp_case_noTextBox.TextChanged

    End Sub

    Private Sub cl_ComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cl_ComboBox.Validated
        Dim idx As Integer
        For idx = 1 To cl_rows
            If client_table(idx, 2) = cl_ComboBox.SelectedItem Then
                cl_no = client_table(idx, 1)
                Exit For
            End If
        Next
    End Sub

    Private Sub comp_against_codeComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comp_against_codeComboBox.SelectedIndexChanged

    End Sub

    Private Sub comp_against_codeComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles comp_against_codeComboBox.Validated
        Try
            against_code = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(comp_against_codeComboBox.SelectedIndex).Item(0)
            If against_code <> orig_against_code Then
                orig_against_code = -1
            Else
                Exit Sub
            End If
            agentComboBox.Items.Clear()
            agentComboBox.Text = ""
            Dim where_clause As String
            If comp_against_codeComboBox.SelectedIndex = 2 Then
                agent_intext = "I"
            ElseIf comp_against_codeComboBox.SelectedIndex = 3 Then
                agent_intext = "E"
            Else
                agent_intext = " "
            End If
            If agent_intext <> " " Then
                where_clause = " where agent_type = 'B' and internalExternal = '" & agent_intext & "'"
            Else
                where_clause = " where (agent_type ='P' or agent_type = 'A') "
            End If
            populate_bailiff_table(where_clause)
            Dim idx As Integer
            For idx = 1 To agent_rows
                agentComboBox.Items.Add(bailiff_table(idx, 2))
            Next
            agentComboBox.Items.Add("Various")
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub Comp_dateTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub notcomprbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles notcomprbtn.CheckedChanged

    End Sub

    Private Sub response_TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub chkdrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If completed And Not orig_completed Then
            compbycombobox.SelectedIndex = -1
        End If
    End Sub

    Private Sub GroupBox1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles GroupBox1.Validating

        
    End Sub

    Private Sub actions_ComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles actions_ComboBox.SelectedIndexChanged

    End Sub

    Private Sub actions_ComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles actions_ComboBox.Validated
        Me.ActionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Actions)
        action_code = PraiseAndComplaintsSQLDataSet.Actions.Rows(actions_ComboBox.SelectedIndex).Item(0)
    End Sub

    Private Sub addbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addbtn.Click
        Try
            OpenFileDialog1.FileName = ""
            OpenFileDialog1.Filter = "Text,word or tif|*.txt; *.doc;*.tif"
            If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim fname As String = OpenFileDialog1.FileName
                Dim dirname As String = comp_no
                save_document(fname, dirname)
                doc_textbox.Text = populate_document_combobox_upd()
                Dim f2name As String = ""
                Dim idx As Integer
                Dim flen As Integer = Microsoft.VisualBasic.Len(fname)
                For idx = flen To 1 Step -1
                    If Mid(fname, idx, 1) = "\" Then
                        f2name = Microsoft.VisualBasic.Right(fname, flen - idx)
                        Exit For
                    End If
                Next
                log_text = "Document added " & f2name
                log_type = "Document"
                add_log(log_type, log_text)
                doc_change_made = True

                If doc_textbox.Text = 0 Then
                    doc_ListBox.Visible = False
                Else
                    doc_ListBox.Visible = True
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub Comp_responseTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Comp_responseTextBox.TextChanged

    End Sub

    Private Sub stage_ComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage_ComboBox.SelectedIndexChanged

    End Sub

    Private Sub stage_ComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage_ComboBox.Validated
        stage_no = Microsoft.VisualBasic.Right(stage_ComboBox.Text, 1)
        If stage_no = 2 Then
            stage2_start_datepicker.Visible = True
            If orig_stage_no = 1 Then
                stage2_start_date = Now
                stage2_start_datepicker.Text = Now
                stage2_completed_by = 0
                stage2_completed_date = Now
                stage2_completed_datepicker.Text = Now
            Else
                If orig_stage_no > 2 Then
                    stage2_completed_datepicker.Visible = True
                    stage2_completed_by_combobox.Visible = True
                End If
            End If
            Comp_textTextBox.Enabled = True
        ElseIf stage_no = 3 Then
            stage3_start_datepicker.Visible = True
            If orig_stage_no = 2 Then
                stage3_start_date = Now
                stage3_start_datepicker.Text = Now
                stage3_completed_by = 0
                stage3_completed_date = Now
                stage3_completed_datepicker.Text = Now
            End If
            Comp_textTextBox.Enabled = True
        End If

    End Sub

    Private Sub cor_nameComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cor_nameComboBox.SelectedIndexChanged

    End Sub

    Private Sub cor_nameComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cor_nameComboBox.Validated
        cor_code = PraiseAndComplaintsSQLDataSet.Corrective_actions.Rows(cor_nameComboBox.SelectedIndex).Item(0)
    End Sub

    Private Sub hold_name_ComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles hold_name_ComboBox.Validated
        Try
            hold_code = PraiseAndComplaintsSQLDataSet.Actions.Rows(hold_name_ComboBox.SelectedIndex).Item(0)
        Catch
            hold_code = 1
        End Try
    End Sub

    Private Sub doc_ListBox_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles doc_ListBox.DoubleClick
        file_path = "r:\complaints\" & comp_no & "\" & doc_ListBox.Text

        If Microsoft.VisualBasic.Right(doc_ListBox.Text, 3) = "tif" Then
            tiffrm.ShowDialog()
        Else
            Dim strWordpadFilename As String = "C:\program files\windows nt\accessories\wordpad " & """" & Trim(file_path) & """"
            ' start up wordpad & display the current file being imported
            Dim RetVal As Integer = Shell(strWordpadFilename, 1)    ' Run wordpad.
        End If
        
    End Sub

    Private Sub deldocbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deldocbtn.Click
        deldocfrm.ShowDialog()
        'no of documents
        doc_textbox.Text = populate_document_combobox_upd()

        If doc_textbox.Text = 0 Then
            doc_ListBox.Visible = False
        Else
            doc_ListBox.Visible = True
        End If
    End Sub


    Private Sub compbycombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles compbycombobox.Validated
        Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
        Try
            compby_code = PraiseAndComplaintsSQLDataSet.Investigators.Rows(compbycombobox.SelectedIndex).Item(0)
        Catch
            compby_code = 0
        End Try

    End Sub

    Private Sub old_comp_notextbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles old_comp_notextbox.Validating
        ErrorProvider1.SetError(old_comp_notextbox, "")
        If Microsoft.VisualBasic.Len(Trim(old_comp_notextbox.Text)) = 0 Then
            old_comp_no = 0
        Else
            Try
                old_comp_no = old_comp_notextbox.Text
            Catch ex As Exception
                ErrorProvider1.SetError(old_comp_notextbox, "Old complaint no must be numeric")
                e.Cancel = True
                Exit Sub
            End Try
            If old_comp_no >= 9999 Then
                ErrorProvider1.SetError(old_comp_notextbox, "Old complaint no must be less than 9999")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub costs_canc_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles costs_cancel_tbox.TextChanged

    End Sub

    Private Sub costs_cancel_tbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles costs_cancel_tbox.Validated
        If costs_cancel > 0 Then
            costs_reasonbtn.Visible = True
        Else
            costs_reasonbtn.Visible = False
            cancel_costs_reason = ""
            cancel_costs_reason_change = True
        End If
    End Sub

    Private Sub costs_canc_tbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles costs_cancel_tbox.Validating
        ErrorProvider1.SetError(costs_cancel_tbox, "")
        If Microsoft.VisualBasic.Len(Trim(costs_cancel_tbox.Text)) = 0 Then
            costs_cancel = 0
        Else
            Try
                costs_cancel = costs_cancel_tbox.Text
            Catch ex As Exception
                ErrorProvider1.SetError(costs_cancel_tbox, "Cancelled costs must be numeric")
                e.Cancel = True
            End Try
        End If

    End Sub

    Private Sub compensation_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles compensation_tbox.TextChanged

    End Sub

    Private Sub compensation_tbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles compensation_tbox.Validating
        ErrorProvider1.SetError(compensation_tbox, "")
        If Microsoft.VisualBasic.Len(Trim(compensation_tbox.Text)) = 0 Then
            compensation = 0
        Else
            Try
                compensation = compensation_tbox.Text
            Catch ex As Exception
                ErrorProvider1.SetError(compensation_tbox, "Compensation must be numeric")
                e.Cancel = True
            End Try
        End If
    End Sub

    Private Sub respcbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles respcbox.CheckedChanged

    End Sub

    Private Sub respcbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles respcbox.Validated
        If respcbox.Checked = True Then
            resp_forgotten = 1
        Else
            resp_forgotten = 0
        End If
    End Sub

    Private Sub stage2_completed_by_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage2_completed_by_combobox.SelectedIndexChanged

    End Sub

    Private Sub stage2_completed_by_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage2_completed_by_combobox.Validated
        If stage2_completed_by_combobox.SelectedIndex = -1 Then
            Exit Sub
        End If
        stage2_completed_by = PraiseAndComplaintsSQLDataSet.Investigators.Rows(stage2_completed_by_combobox.SelectedIndex).Item(0)
        If stage2_completed_by > 0 Then
            stage2_completed_datepicker.Visible = True
            stage2_completed_date = Now
        End If
    End Sub

    Private Sub stage2_completed_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage2_completed_datepicker.Validated
        stage2_completed_date = stage2_completed_datepicker.Text
        
    End Sub

    Private Sub stage2_completed_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage2_completed_datepicker.Validating
        If stage2_completed_datepicker.Text > Now Then
            ErrorProvider1.SetError(stage2_gbox, "Completed date must not be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub stage2_completed_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage2_completed_datepicker.ValueChanged

    End Sub

    Private Sub stage2_start_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage2_start_datepicker.Validated
        stage2_start_date = stage2_start_datepicker.Text
    End Sub

    Private Sub stage2_start_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage2_start_datepicker.Validating
        ErrorProvider1.SetError(stage2_gbox, "")
        If stage2_start_datepicker.Text > Now Then
            ErrorProvider1.SetError(stage2_gbox, "Start date must not be in the future")
            e.Cancel = True
        ElseIf CDate(stage2_start_datepicker.Text) < CDate(Comp_dateTextBox.Text) Then
            ErrorProvider1.SetError(stage2_gbox, "Start date must not be before received date")
            e.Cancel = True
        End If
    End Sub

    Private Sub stage2_start_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage2_start_datepicker.ValueChanged

    End Sub

    Private Sub GroupBox2_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub compbycombobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles compbycombobox.SelectedIndexChanged

    End Sub

    Private Sub stage2_completed_by_combobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage2_completed_by_combobox.Validating
        If stage2_completed_by_combobox.Text = "Not yet known" And orig_stage2_completed_by = 0 Then
            ErrorProvider1.SetError(stage2_completed_by_combobox, "Please select who completed stage 2")
            e.Cancel = True
            Exit Sub
        End If
    End Sub

    Private Sub gender_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gender_combobox.SelectedIndexChanged

    End Sub

    Private Sub gender_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles gender_combobox.Validated
        gender = gender_combobox.Text
    End Sub

    Private Sub eth_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles eth_combobox.SelectedIndexChanged

    End Sub

    Private Sub eth_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles eth_combobox.Validated
        ethnicity_desc = eth_combobox.Text
    End Sub


    Private Sub ack_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ack_cbox.CheckedChanged

    End Sub

    Private Sub ack_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles ack_cbox.Validated
        If ack_cbox.Checked Then
            ack_datepicker.Visible = True
            ack_datepicker.Value = Now
        Else
            ack_datepicker.Visible = False
            ack_date = CDate("1800,1,1")
        End If
    End Sub

    Private Sub ack_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles ack_datepicker.Validated
        ack_date = ack_datepicker.Value
    End Sub

    Private Sub ack_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ack_datepicker.Validating
        ErrorProvider1.SetError(ack_gbox, "")
        If ack_datepicker.Text > Now Then
            ErrorProvider1.SetError(ack_gbox, "Start date must not be in the future")
            e.Cancel = True
        ElseIf CDate(ack_datepicker.Text) < CDate(Comp_dateTextBox.Text) Then
            ErrorProvider1.SetError(ack_gbox, "Start date must not be before received date")
            e.Cancel = True
        End If
    End Sub

    Private Sub holding_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles holding_datepicker.Validating
        ErrorProvider1.SetError(holding_gbox, "")
        If holding_datepicker.Text > Now Then
            ErrorProvider1.SetError(holding_gbox, "Start date must not be in the future")
            e.Cancel = True
        ElseIf CDate(holding_datepicker.Text) < CDate(Comp_dateTextBox.Text) Then
            ErrorProvider1.SetError(holding_gbox, "Start date must not be before received date")
            e.Cancel = True
        End If
    End Sub

    Private Sub holding_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles holding_cbox.Validated
        If holding_cbox.Checked Then
            holding_datepicker.Visible = True
            holding_datepicker.Value = Now
        Else
            holding_datepicker.Visible = False
            holding_date = CDate("1800,1,1")
        End If
    End Sub

    Private Sub holding_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles holding_datepicker.ValueChanged

    End Sub

    Private Sub ack_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ack_datepicker.ValueChanged

    End Sub

    Private Sub stage2_ack_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage2_ack_cbox.CheckedChanged

    End Sub

    Private Sub stage2_ack_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage2_ack_cbox.Validated
        If stage2_ack_cbox.Checked Then
            stage2_ack_datepicker.Visible = True
            stage2_ack_datepicker.Value = Now
        Else
            stage2_ack_datepicker.Visible = False
            stage2_ack_date = CDate("1800,1,1")
        End If
    End Sub

    Private Sub holding_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles holding_cbox.CheckedChanged

    End Sub

    Private Sub stage2_holding_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage2_holding_cbox.CheckedChanged

    End Sub

    Private Sub stage2_holding_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage2_holding_cbox.Validated
        If stage2_holding_cbox.Checked Then
            stage2_holding_datepicker.Visible = True
            stage2_holding_datepicker.Value = Now
        Else
            stage2_holding_datepicker.Visible = False
            stage2_holding_date = CDate("1800,1,1")
        End If
    End Sub

    Private Sub stage3_start_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage3_start_datepicker.Validated
        stage3_start_date = stage3_start_datepicker.Text
    End Sub

    Private Sub stage3_start_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage3_start_datepicker.Validating
        ErrorProvider1.SetError(stage3_gbox, "")
        If stage3_start_datepicker.Text > Now Then
            ErrorProvider1.SetError(stage3_gbox, "Start date must not be in the future")
            e.Cancel = True
        ElseIf CDate(stage3_start_datepicker.Text) < CDate(stage2_completed_datepicker.Text) Then
            ErrorProvider1.SetError(stage3_gbox, "Start date must not be before stage 2 completed date")
            e.Cancel = True
        End If
    End Sub

    Private Sub stage3_completed_by_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage3_completed_by_combobox.Validated
        If stage3_completed_by_combobox.SelectedIndex = -1 Then
            Exit Sub
        End If
        stage3_completed_by = PraiseAndComplaintsSQLDataSet.Investigators.Rows(stage3_completed_by_combobox.SelectedIndex).Item(0)
        If stage3_completed_by > 0 Then
            stage3_completed_datepicker.Visible = True
            stage3_completed_date = Now
        End If
    End Sub

    Private Sub stage3_completed_by_combobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage3_completed_by_combobox.Validating
        If stage3_completed_by_combobox.Text = "Not yet known" And orig_stage3_completed_by = 0 Then
            ErrorProvider1.SetError(stage3_completed_by_combobox, "Please select who completed stage 3")
            e.Cancel = True
            Exit Sub
        End If
    End Sub

    Private Sub stage3_completed_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage3_completed_datepicker.Validated
        stage3_completed_date = stage3_completed_datepicker.Text
    End Sub

    Private Sub stage3_completed_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage3_completed_datepicker.Validating
        If stage3_completed_datepicker.Text > Now Then
            ErrorProvider1.SetError(stage3_gbox, "Completed date must not be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub stage3_start_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage3_start_datepicker.ValueChanged

    End Sub

    Private Sub comp_against2_codecombobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comp_against2_codecombobox.SelectedIndexChanged

    End Sub

    Private Sub comp_against2_codecombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles comp_against2_codecombobox.Validated
        Try
            against2_code = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(comp_against2_codecombobox.SelectedIndex).Item(0)
            If against2_code <> orig_against2_code Then
                orig_against2_code = -1
            Else
                Exit Sub
            End If
            agent2_combobox.Items.Clear()
            agent2_combobox.Text = ""
            Dim where_clause As String
            If comp_against2_codecombobox.SelectedIndex = 2 Then
                agent_intext = "I"
            ElseIf comp_against2_codecombobox.SelectedIndex = 3 Then
                agent_intext = "E"
            Else
                agent_intext = " "
            End If
            If agent_intext <> " " Then
                where_clause = " where agent_type = 'B' and internalExternal = '" & agent_intext & "'"
            Else
                where_clause = " where (agent_type ='P' or agent_type = 'A') "
            End If
            populate_bailiff_table(where_clause)
            Dim idx As Integer
            For idx = 1 To agent_rows
                agent2_combobox.Items.Add(bailiff_table(idx, 2))
            Next
            agent2_combobox.Items.Add("Various")
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub agent2_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agent2_combobox.SelectedIndexChanged

    End Sub

    Private Sub agent2_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles agent2_combobox.Validated
        Dim idx As Integer
        agent2_no = 0
        If agent2_combobox.SelectedItem = "Various" Then
            agent2_no = 9999
        Else
            For idx = 1 To agent_rows
                If bailiff_table(idx, 2) = agent2_combobox.SelectedItem Then
                    agent2_no = bailiff_table(idx, 1)
                    Exit For
                End If
            Next
        End If
        orig_against2_code = against2_code
    End Sub

    Private Sub Comp_dateTextBox_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Comp_dateTextBox.TextChanged

    End Sub

    Private Sub Comp_dateTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Comp_dateTextBox.Validating
        ErrorProvider1.SetError(Comp_dateTextBox, "")
        If Not IsDate(Comp_dateTextBox.Text) Then
            ErrorProvider1.SetError(Comp_dateTextBox, "Invalid date")
            e.Cancel = True
        End If
    End Sub

    Private Sub legalcbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles legalcbox.CheckedChanged

    End Sub

   
    Private Sub legalcbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles legalcbox.Validated
        If legalcbox.Checked = True Then
            legal_ins_flag = "Y"
            legalgbox.Visible = True
            If referred_to_solicitor = CDate("1800,1,1") Then
                solicitor_cbox.Checked = False
                solicitor_datepicker.Visible = False
            Else
                solicitor_cbox.Checked = True
                solicitor_datepicker.Visible = True
                solicitor_datepicker.Value = referred_to_solicitor
            End If
            If referred_to_insurer = CDate("1800,1,1") Then
                inscbox.Checked = False
                ins_datepicker.Visible = False
            Else
                inscbox.Checked = True
                ins_datepicker.Visible = True
                ins_datepicker.Value = referred_to_insurer
            End If
            If monetary_risk = -999 Then
                monetary_risktbox.Text = ""
            Else
                monetary_risktbox.Text = Format(monetary_risk, "�#0.00")
            End If
        Else
            legal_ins_flag = "N"
            legalgbox.Visible = False
            solicitor_cbox.Checked = False
            solicitor_datepicker.Visible = False
            inscbox.Checked = False
            ins_datepicker.Visible = False
        End If

    End Sub

    Private Sub solicitor_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles solicitor_cbox.CheckedChanged

    End Sub

    Private Sub solicitor_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles solicitor_cbox.Validated
        If solicitor_cbox.Checked = True Then
            solicitor_datepicker.Visible = True
            If referred_to_solicitor = CDate("1800,1,1") Then
                referred_to_solicitor = Now
            End If
        Else
            solicitor_datepicker.Visible = False
        End If
    End Sub

    Private Sub inscbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles inscbox.CheckedChanged

    End Sub

    Private Sub inscbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles inscbox.Validated
        If inscbox.Checked = True Then
            ins_datepicker.Visible = True
            If referred_to_insurer = CDate("1800,1,1") Then
                referred_to_insurer = Now
            End If
        Else
            ins_datepicker.Visible = False
        End If
    End Sub

    Private Sub solicitor_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles solicitor_datepicker.Validated
        referred_to_solicitor = Format(solicitor_datepicker.Value, "yyyy-MM-dd")
    End Sub

    Private Sub solicitor_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles solicitor_datepicker.Validating
        ErrorProvider1.SetError(solicitor_datepicker, "")
        If solicitor_datepicker.Text < Comp_dateTextBox.Text Then
            ErrorProvider1.SetError(solicitor_datepicker, "Date can't be before entered date")
            e.Cancel = True
        ElseIf solicitor_datepicker.Text > Now Then
            ErrorProvider1.SetError(solicitor_datepicker, "Date can't be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub solicitor_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles solicitor_datepicker.ValueChanged

    End Sub

    Private Sub ins_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles ins_datepicker.Validated
        referred_to_insurer = Format(ins_datepicker.Value, "yyyy-MM-dd")
    End Sub

    Private Sub ins_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ins_datepicker.Validating
        ErrorProvider1.SetError(ins_datepicker, "")
        If ins_datepicker.Text < Comp_dateTextBox.Text Then
            ErrorProvider1.SetError(ins_datepicker, "Date can't be before entered date")
            e.Cancel = True
        ElseIf ins_datepicker.Text > Now Then
            ErrorProvider1.SetError(ins_datepicker, "Date can't be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub ins_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ins_datepicker.ValueChanged

    End Sub

    Private Sub monetary_risktbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles monetary_risktbox.TextChanged

    End Sub

    Private Sub monetary_risktbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles monetary_risktbox.Validating
        ErrorProvider1.SetError(monetary_risktbox, "")
        If Trim(monetary_risktbox.Text).Length = 0 Then
            monetary_risk = -999
        Else
            If Not IsNumeric(monetary_risktbox.Text) Then
                ErrorProvider1.SetError(monetary_risktbox, "Value must be numeric")
                e.Cancel = True
            ElseIf monetary_risktbox.Text < 0 Then
                ErrorProvider1.SetError(monetary_risktbox, "Value must be positive")
                e.Cancel = True
            Else
                monetary_risk = monetary_risktbox.Text
            End If
        End If
    End Sub

    Private Sub costs_reasonbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles costs_reasonbtn.Click
        costs_reason_mode = "U"
        costs_reasonfrm.ShowDialog()
    End Sub
End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class againstfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.ComplaintagainstBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.Complaint_againstBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Complaint_againstTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
        Me.cl_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AgainsttextDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AgainstcodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComplaintagainstBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Complaint_againstBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AgainstcodeDataGridViewTextBoxColumn, Me.AgainsttextDataGridViewTextBoxColumn, Me.cl_no})
        Me.DataGridView1.DataSource = Me.ComplaintagainstBindingSource
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(447, 286)
        Me.DataGridView1.TabIndex = 0
        '
        'ComplaintagainstBindingSource
        '
        Me.ComplaintagainstBindingSource.DataMember = "Complaint_against"
        Me.ComplaintagainstBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Complaint_againstBindingSource
        '
        Me.Complaint_againstBindingSource.DataMember = "Complaint_against"
        Me.Complaint_againstBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Complaint_againstTableAdapter
        '
        Me.Complaint_againstTableAdapter.ClearBeforeFill = True
        '
        'cl_no
        '
        Me.cl_no.HeaderText = "Client No"
        Me.cl_no.Name = "cl_no"
        '
        'AgainsttextDataGridViewTextBoxColumn
        '
        Me.AgainsttextDataGridViewTextBoxColumn.DataPropertyName = "against_text"
        Me.AgainsttextDataGridViewTextBoxColumn.HeaderText = "Text"
        Me.AgainsttextDataGridViewTextBoxColumn.Name = "AgainsttextDataGridViewTextBoxColumn"
        Me.AgainsttextDataGridViewTextBoxColumn.Width = 300
        '
        'AgainstcodeDataGridViewTextBoxColumn
        '
        Me.AgainstcodeDataGridViewTextBoxColumn.DataPropertyName = "against_code"
        Me.AgainstcodeDataGridViewTextBoxColumn.HeaderText = "Code"
        Me.AgainstcodeDataGridViewTextBoxColumn.Name = "AgainstcodeDataGridViewTextBoxColumn"
        Me.AgainstcodeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'againstfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(447, 286)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "againstfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Compaint made against"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComplaintagainstBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Complaint_againstBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents Complaint_againstBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaint_againstTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
    Friend WithEvents ComplaintagainstBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AgainstcodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AgainsttextDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cl_no As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class recvd_fromfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.Received_fromBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Received_fromTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
        Me.ReceivedfromBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.RecvdfromDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.RecvdtextDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Received_fromBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceivedfromBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.RecvdfromDataGridViewTextBoxColumn, Me.RecvdtextDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.ReceivedfromBindingSource
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(249, 346)
        Me.DataGridView1.TabIndex = 0
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Received_fromBindingSource
        '
        Me.Received_fromBindingSource.DataMember = "Received_from"
        Me.Received_fromBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Received_fromTableAdapter
        '
        Me.Received_fromTableAdapter.ClearBeforeFill = True
        '
        'ReceivedfromBindingSource
        '
        Me.ReceivedfromBindingSource.DataMember = "Received_from"
        Me.ReceivedfromBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'RecvdfromDataGridViewTextBoxColumn
        '
        Me.RecvdfromDataGridViewTextBoxColumn.DataPropertyName = "recvd_from"
        Me.RecvdfromDataGridViewTextBoxColumn.HeaderText = "recvd_from"
        Me.RecvdfromDataGridViewTextBoxColumn.Name = "RecvdfromDataGridViewTextBoxColumn"
        Me.RecvdfromDataGridViewTextBoxColumn.ReadOnly = True
        '
        'RecvdtextDataGridViewTextBoxColumn
        '
        Me.RecvdtextDataGridViewTextBoxColumn.DataPropertyName = "recvd_text"
        Me.RecvdtextDataGridViewTextBoxColumn.HeaderText = "recvd_text"
        Me.RecvdtextDataGridViewTextBoxColumn.Name = "RecvdtextDataGridViewTextBoxColumn"
        '
        'recvd_fromfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(249, 346)
        Me.Controls.Add(Me.DataGridView1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "recvd_fromfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Complainant"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Received_fromBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceivedfromBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents Received_fromBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Received_fromTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
    Friend WithEvents RecvdfromDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RecvdtextDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ReceivedfromBindingSource As System.Windows.Forms.BindingSource
End Class

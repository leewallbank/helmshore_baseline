Public Class updmastfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub catbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles catbtn.Click
        categoriesfrm.ShowDialog()
        Dim idx As Integer
        For idx = comp_cat_orig_no_rows To categoriesfrm.DataGridView1.RowCount - 1
            cat_code = categoriesfrm.DataGridView1.Rows(idx - 1).Cells(0).Value
            cat_number = categoriesfrm.DataGridView1.Rows(idx - 1).Cells(1).Value
            cat_text = categoriesfrm.DataGridView1.Rows(idx - 1).Cells(2).Value
            Try
                categoriesfrm.Complaint_categoriesTableAdapter.InsertQuery(cat_code, cat_number, cat_text)
                log_text = "Category added - " & cat_code & " " & cat_number & " " & cat_text
                LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    Private Sub recbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recbtn.Click
        recpt_typefrm.ShowDialog()
        Dim idx As Integer

        For idx = recpt_orig_no_rows To recpt_typefrm.DataGridView1.RowCount
            Try
                If recpt_typefrm.DataGridView1.Rows(idx).Cells(1).Value.ToString.Length = 0 Then
                    Continue For
                End If
            Catch ex As Exception
                Continue For
            End Try
            recpt_text = recpt_typefrm.DataGridView1.Rows(idx).Cells(1).Value
            Try
                recpt_typefrm.Receipt_typeTableAdapter.InsertQuery(recpt_text)
                log_text = "Receipt type added - " & recpt_text
                LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    Private Sub recvdbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recvdbtn.Click
        recvd_fromfrm.ShowDialog()
        Dim idx As Integer

        For idx = recvd_from_orig_no_rows To recvd_fromfrm.DataGridView1.RowCount
            Try
                If recvd_fromfrm.DataGridView1.Rows(idx).Cells(1).Value.ToString.Length = 0 Then
                    Continue For
                End If
            Catch ex As Exception
                Continue For
            End Try
            recvd_text = recvd_fromfrm.DataGridView1.Rows(idx).Cells(1).Value
            Try
                recvd_fromfrm.Received_fromTableAdapter.InsertQuery(recvd_text)
                log_text = "Received from added - " & recvd_text
                LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    Private Sub againstbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles againstbtn.Click
        againstfrm.ShowDialog()
        Dim idx As Integer
        For idx = against_orig_no_rows To againstfrm.DataGridView1.RowCount - 1
            If againstfrm.DataGridView1.Rows(idx - 1).Cells(1).Value = Nothing Then
                Exit Sub
            End If
            against_text = againstfrm.DataGridView1.Rows(idx - 1).Cells(1).Value
            Try
                againstfrm.Complaint_againstTableAdapter.InsertQuery(against_text)
                log_text = "Complaint against added - " & against_text
                LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    Private Sub deptbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'deptfrm.ShowDialog()
        'Dim idx As Integer
        'For idx = dept_orig_no_rows To deptfrm.DataGridView1.RowCount - 1
        '    If deptfrm.DataGridView1.Rows(idx - 1).Cells(1).Value = Nothing Then
        '        Exit Sub
        '    End If
        '    dept_text = deptfrm.DataGridView1.Rows(idx - 1).Cells(1).Value
        '    Try
        '        deptfrm.DepartmentsTableAdapter.InsertQuery(dept_text)
        '        log_text = "Department added - " & dept_text
        '        LogTableAdapter.InsertQuery(log_user, Now, "master", 0, log_text)
        '    Catch ex As Exception
        '        MessageBox.Show(ex.Message)
        '    End Try
        'Next
    End Sub

    Private Sub investbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles investbtn.Click
        investfrm.ShowDialog()
        Dim idx As Integer

        For idx = inv_orig_no_rows To investfrm.DataGridView1.RowCount - 1
            If investfrm.DataGridView1.Rows(idx - 1).Cells(1).Value = Nothing Then
                Exit Sub
            End If
            inv_text = investfrm.DataGridView1.Rows(idx - 1).Cells(1).Value
            Dim admin As Boolean
            Try
                admin = investfrm.DataGridView1.Rows(idx - 1).Cells(2).Value
            Catch
                admin = False
            End Try
            Try
                investfrm.InvestigatorsTableAdapter.InsertQuery(inv_text, "password", admin)
                log_text = "Investigator added - " & inv_text
                LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub



    Private Sub updmastfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Log' table. You can move, or remove it, as needed.
        Me.LogTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Log)
        'TODO: This line of code loads data into the 'LogDataSet.Log' table. You can move, or remove it, as needed.
        Me.LogTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Log)

    End Sub

    Private Sub actionsbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles actionsbtn.Click
        actionsfrm.ShowDialog()
        Dim idx As Integer
        For idx = action_orig_no_rows To actionsfrm.DataGridView1.RowCount - 1
            If actionsfrm.DataGridView1.Rows(idx - 1).Cells(1).Value = Nothing Then
                Exit Sub
            End If
            action_name = actionsfrm.DataGridView1.Rows(idx - 1).Cells(1).Value
            Try
                actionsfrm.ActionsTableAdapter.InsertQuery(action_name)
                log_text = "Action added - " & action_name
                LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    Private Sub resetbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles resetbtn.Click
        resetfrm.ShowDialog()
    End Sub

    Private Sub corractbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles corractbtn.Click
        corr_actionsfrm.ShowDialog()
        Dim idx As Integer
        For idx = corr_action_orig_no_rows To corr_actionsfrm.DataGridView1.RowCount - 1
            If corr_actionsfrm.DataGridView1.Rows(idx - 1).Cells(1).Value = Nothing Then
                Exit Sub
            End If
            corr_action_name = corr_actionsfrm.DataGridView1.Rows(idx - 1).Cells(1).Value
            Try
                corr_actionsfrm.Corrective_actionsTableAdapter.InsertQuery(corr_action_name)
                log_text = "Corrective action added - " & corr_action_name
                LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    Private Sub cl_turnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cl_turnbtn.Click
        ProgressBar1.Value = 10
        client_turnfrm.ShowDialog()
        ProgressBar1.Visible = False
    End Sub

    Private Sub notifybtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles notifybtn.Click
        ProgressBar1.Value = 10
        NotifyClientfrm.ShowDialog()
        ProgressBar1.Visible = False
    End Sub
End Class
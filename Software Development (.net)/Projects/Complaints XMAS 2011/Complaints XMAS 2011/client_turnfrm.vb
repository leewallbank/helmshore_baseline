Public Class client_turnfrm

    Private Sub client_turnfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        updmastfrm.ProgressBar1.Visible = True
        DataGridView1.Rows.Clear()
        populate_client_table()
        Dim idx As Integer
        Dim turn_days As Integer
        For idx = 1 To cl_rows
            Me.Client_turnaroundTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Client_turnaround, client_table(idx, 1))
            Try
                turn_days = PraiseAndComplaintsSQLDataSet.Client_turnaround.Rows(0).Item(1)
            Catch
                turn_days = 10
            End Try
            DataGridView1.Rows.Add(client_table(idx, 1), (Trim(client_table(idx, 2))), turn_days)
            updmastfrm.ProgressBar1.Value = idx / cl_rows * 100
        Next
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.ColumnIndex = 2 And e.RowIndex >= 0 Then
            Dim cl_no As Integer = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            Dim Turn_days As Integer
            Try
                Turn_days = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            Catch
                MessageBox.Show("Days must be numeric")
                Exit Sub
            End Try
            Try
                If Me.Client_turnaroundTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Client_turnaround, cl_no) > 0 Then
                    Me.Client_turnaroundTableAdapter.UpdateQuery(Turn_days, cl_no)
                Else
                    Me.Client_turnaroundTableAdapter.InsertQuery(cl_no, Turn_days)
                End If

                log_text = "Client code - " & cl_no & " turnaround days changed to " & Turn_days
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate client turnaround days entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
End Class
Imports System
Imports System.IO
Module Module1
    Public recpt_orig_no_rows, comp_cat_orig_no_rows, cat_number, recvd_from, selected_comp_no, hold_code As Integer
    Public recvd_from_orig_no_rows, recpt_code, against_orig_no_rows, action_orig_no_rows, cor_code, resp_forgotten, orig_resp_forgotten As Integer
    Public dept_orig_no_rows, dept_code, ret_code, inv_orig_no_rows, against_code, against2_code, orig_cat_number As Integer
    Public cat_code, cat_text, recpt_text, recvd_text, against_text, against2_text, dept_text, inv_text, orig_hold_name As String
    Public param1, param2, param3, param4, param5, param6, cl_name, log_user, log_text, orig_text, cor_name, corr_action_name As String
    Public admin_user, search_agent_surname, search_agent_forename, search_agent_name, orig_recpt_text, orig_recvd_text As String
    Public orig_against_text, orig_against2_text, orig_inv_text, orig_action_text, orig_cor_text, file_path, orig_compby As String
    Public orig_no_docs, agent1, agent2, compby_code, invest_no_rows, orig_recpt_code, orig_recvd_from, orig_case_no As Integer
    Public test_logid As Integer = 16
    Public costs_reason_mode, first_letters As String
    Public get_letter, cancel_costs_reason_change As Boolean
    Public monetary_risk, orig_monetary_risk As Decimal
    Public referred_to_solicitor, referred_to_insurer, orig_referred_to_solicitor, orig_referred_to_insurer As Date
    Public stage2_completed_by, orig_stage2_completed_by, orig_stage3_completed_by, ethnicity As Integer
    Public exception_string, exception_stacktrace, gender, orig_gender, ethnicity_desc, orig_ethnicity_desc As String
    Public costs_cancel, orig_costs_cancel, compensation, orig_compensation As Decimal
    Public orig_cl_no, orig_agent_no, orig_agent2_no, orig_alloc_to_code, orig_action_code, orig_cor_code, orig_hold_code, stage3_completed_by As Integer
    Public doc_change_made, orig_priority As Boolean
    Public orig_comp_date, stage2_start_date, stage2_completed_date, orig_stage2_start_date, orig_stage2_completed_date As Date
    Public orig_stage3_start_date, orig_stage3_completed_date, orig_entered_date As Date
    Public row As DataRow
    Public search_receipt_type As String
    Public ack_date, holding_date, orig_ack_date, orig_holding_date, stage2_ack_date, stage2_holding_date As Date
    Public orig_stage2_ack_date, orig_stage2_holding_date, parm_hold_date, stage3_start_date, stage3_completed_date As Date
    Public search_dept, search_stage_text, legal_ins_flag, orig_legal_ins_flag, cancel_costs_reason As String
    Public last_against_code, last_against2_code, search_recvd_from, search_agent_no, search_dept_code, old_comp_no, orig_old_comp_no As Integer
    Public inv_code, cl_rows, cl_no, agent_no, agent2_no, agent_rows, inv_count, alloc_to_code, stage_no, corr_action_code, corr_action_orig_no_rows As Integer
    Public debtor_table(,), client_table(,), cs_table(,), bailiff_table(,), search_bailiff_table(,)
    Public password_reset, completed, orig_completed, orig_checked, checked As Boolean
    Public comp_no, case_no, log_code, orig_against_code, orig_against2_code, dept_rows, search_no, search_csid_no, action_code, orig_stage_no As Integer
    Public new_founded, founded_search, held_search, completed_search, action_name, agent_intext, log_type, file As String
    Public cat_table As New Complaints_XMAS_2011.DataSet1.Complaint_categoriesDataTable
    Public inv_password, user_password, orig_cat_code, orig_details, orig_resp_details, orig_founded, para_name As String
    Public Sub populate_client_table()
        param1 = "onestep"
        param2 = "select _rowid, name from Client " & _
        " where _rowid <> 1 and _rowid <> 2 and _rowid <> 24 " & _
        " order by name"
        Dim client_dataset As DataSet = get_dataset(param1, param2)
        ReDim client_table(no_of_rows, 2)
        Dim idx As Integer
        For idx = 0 To no_of_rows - 1
            Dim cl_no As Integer = client_dataset.Tables(0).Rows(idx).Item(0)
            Dim cl_name = Trim(client_dataset.Tables(0).Rows(idx).Item(1))
            client_table(idx + 1, 1) = cl_no
            client_table(idx + 1, 2) = cl_name
        Next
        cl_rows = no_of_rows
    End Sub
    Public Sub populate_bailiff_table(ByVal where_clause As String)
        where_clause = where_clause & " and _rowid <> 2057 and _rowid <> 2037 and _rowid <> 2063"
        param1 = "onestep"
        param2 = "select distinct _rowid, name_fore, name_sur, status from Bailiff" & _
        where_clause & " order by name_sur"
        Dim bail_dataset As DataSet = get_dataset(param1, param2)
        ReDim bailiff_table(no_of_rows, no_of_rows)

        Dim idx As Integer
        Dim idx2 As Integer = 0
        Dim bailiff_name As String
        For idx = 0 To no_of_rows - 1
            Try
                bailiff_name = Trim(bail_dataset.Tables(0).Rows(idx).Item(2))
            Catch ex As Exception
                Continue For
            End Try
            If bailiff_name.Length = 0 Then
                Continue For
            End If
            idx2 += 1
            Dim bail_forename As String
            Try
                bail_forename = Trim(bail_dataset.Tables(0).Rows(idx).Item(1))
            Catch ex As Exception
                bail_forename = ""
            End Try
            If bail_forename <> "" Then
                bailiff_name = bailiff_name & ", " & bail_forename
            End If
            If bail_dataset.Tables(0).Rows(idx).Item(3) = "C" Then
                bailiff_name = bailiff_name & " (C)"
            End If
            bailiff_table(idx2, 2) = bailiff_name
            bailiff_table(idx2, 1) = bail_dataset.Tables(0).Rows(idx).Item(0)
        Next
        agent_rows = idx2

    End Sub
    Public Sub save_document(ByVal filename As String, ByVal dirname As String)
        Dim path As String
        If log_code = test_logid Then
            path = "r:\complaints\Test\" & dirname
        Else
            path = "r:\complaints\" & dirname
        End If
        Dim newfile As String = path & "\"
        Dim dir_info As DirectoryInfo
        Dim idx As Integer
        For idx = Len(filename) To 1 Step -1
            If Mid(filename, idx, 1) = "\" Then
                newfile = newfile & Right(filename, Len(filename) - idx)
                Exit For
            End If
        Next
        'create directory
        Try
            If Directory.Exists(path) = False Then
                dir_info = Directory.CreateDirectory(path)
            End If
            'save document
            My.Computer.FileSystem.CopyFile(filename, newfile, True)
        Catch ex As Exception
            MsgBox("Unable to save document")
        End Try
    End Sub
    Public Sub delete_temp_directory()
        Dim path As String = "r:\complaints\T" & log_code
        Try
            Directory.Delete(path)
        Catch ex As Exception

        End Try
    End Sub
    Public Sub rename_directory(ByVal dirname As String)
        Dim newdirname As String
        If log_code = test_logid Then
            newdirname = "r:\complaints\Test\" & comp_no
        Else
            newdirname = "r:\complaints\" & comp_no
        End If
        Dim path As String = "r:\complaints\T" & log_code
        If Directory.Exists(path) Then
            Try
                Directory.Move(dirname, newdirname)
            Catch ex As Exception
                MessageBox.Show("Unable to rename temp directory for documents")
            End Try
        End If
    End Sub
    Public Function populate_document_combobox_disp() As Integer
        Dim path As String
        If log_code = test_logid Then
            path = "r:\complaints\Test\" & comp_no
        Else
            path = "r:\complaints\" & comp_no
        End If
        displayfrm3.doc_ListBox.Items.Clear()
        If Directory.Exists(path) = False Then
            Exit Function
        End If
        Dim dir As New DirectoryInfo(path)
        Dim f As FileInfo
        Dim idx As Integer = 0
        For Each f In dir.GetFiles()
            If Microsoft.VisualBasic.Right(f.Name, 4) <> ".del" Then
                displayfrm3.doc_ListBox.Items.Add(f.Name)
                displayfrm3.doc_ListBox.Text = f.Name
                idx += 1
            End If
        Next
        Return idx
    End Function
    Public Function populate_document_combobox_del() As Integer
        Dim path As String
        If log_code = test_logid Then
            path = "r:\complaints\Test\" & comp_no
        Else
            path = "r:\complaints\" & comp_no
        End If
        deldocfrm.doc_ListBox.Items.Clear()
        If Directory.Exists(path) = False Then
            Exit Function
        End If
        Dim dir As New DirectoryInfo(path)
        Dim f As FileInfo
        Dim idx As Integer = 0
        For Each f In dir.GetFiles()
            If Microsoft.VisualBasic.Right(f.Name, 4) <> ".del" Then
                deldocfrm.doc_ListBox.Items.Add(f.Name)
                deldocfrm.doc_ListBox.Text = f.Name
                idx += 1
            End If
        Next
        Return idx
    End Function

    Public Function populate_document_combobox_upd() As Integer
        Dim path As String
        If log_code = test_logid Then
            path = "r:\complaints\Test\" & comp_no
        Else
            path = "r:\complaints\" & comp_no
        End If
        updatecmpfrm.doc_ListBox.Items.Clear()
        If Directory.Exists(path) = False Then
            Exit Function
        End If
        Dim dir As New DirectoryInfo(path)
        Dim f As FileInfo
        Dim idx As Integer = 0
        For Each f In dir.GetFiles()
            If Microsoft.VisualBasic.Right(f.Name, 4) <> ".del" Then
                updatecmpfrm.doc_ListBox.Items.Add(f.Name)
                updatecmpfrm.doc_ListBox.Text = f.Name
                idx += 1
            End If
        Next
        Return idx
    End Function

    Public Function check_held_letter(ByVal case_no As Integer, ByVal comp_date As Date) As String
        Dim retn_code As String = "N"
        param1 = "onestep"
        param2 = "select _rowid, text, _createdBy, _createdDate from Note" & _
        " where debtorID = " & case_no & " and type = 'Letter'"

        Dim note_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            retn_code = "N"
        Else
            Dim idx As Integer
            For idx = 0 To no_of_rows - 1
                Dim created_date As Date = note_dataset.Tables(0).Rows(idx).Item(3)
                If created_date < comp_date Then
                    Continue For
                End If
                If InStr(note_dataset.Tables(0).Rows(idx).Item(1), "Complaint_Hold") > 0 Then
                    retn_code = "Y"
                    parm_hold_date = created_date
                    Exit For
                End If
            Next
        End If
        Return retn_code
    End Function
    Sub add_log(ByVal log_type As String, ByVal log_text As String)
        If log_code = test_logid Then
            updatecmpfrm.Test_LogTableAdapter.InsertQuery(log_code, Now, log_type, comp_no, log_text)
        Else
            updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, log_type, comp_no, log_text)
        End If
    End Sub

    Sub error_log()
        Dim path As String
        path = "C:\complaints"

        Dim newfile As String = path & "\error_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"
        Dim dir_info As DirectoryInfo
        'create directory
        Try
            If Directory.Exists(path) = False Then
                dir_info = Directory.CreateDirectory(path)
            End If
            'save document
            Dim outfile As String = "Complaint number " & comp_no & vbNewLine
            outfile = outfile & exception_string & vbNewLine
            My.Computer.FileSystem.WriteAllText(newfile, outfile, False)
        Catch ex As Exception
            MsgBox("Unable to save log file")
        End Try
        Application.Exit()
    End Sub
End Module

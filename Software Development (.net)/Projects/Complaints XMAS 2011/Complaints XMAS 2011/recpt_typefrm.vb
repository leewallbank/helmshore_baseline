Public Class recpt_typefrm

    Private Sub recpt_typefrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Complaints' table. You can move, or remove it, as needed.
        Me.ComplaintsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaints)

        Me.Receipt_typeTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Receipt_type)
        recpt_orig_no_rows = DataGridView1.NewRowIndex
    End Sub
    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.ColumnIndex = 1 And e.RowIndex >= 0 Then
            recpt_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            recpt_text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            If DataGridView1.Rows(e.RowIndex).IsNewRow Then Return
            Try
                Me.Receipt_typeTableAdapter.UpdateQuery(recpt_text, recpt_code)
                log_text = "Receipt type amended - " & recpt_code & _
                                           " from " & orig_text & " to " & recpt_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate receipt type entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter
        'If e.RowIndex > 0 Then
        recpt_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        orig_text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
        'End If
    End Sub
    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles DataGridView1.RowsRemoved
        If e.RowIndex > 0 Then
            Try
                Try
                    Me.Receipt_typeTableAdapter.DeleteQuery(recpt_code)
                Catch
                    MessageBox.Show("Can't delete as there are complaints with this receipt type")
                    Exit Sub
                End Try
                log_text = "Receipt type deleted - " & recpt_code & " name " & orig_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
                'End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If

    End Sub

End Class
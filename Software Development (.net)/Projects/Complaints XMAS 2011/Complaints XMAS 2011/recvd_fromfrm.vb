Public Class recvd_fromfrm

    Private Sub recvdfrom_frm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Received_from' table. You can move, or remove it, as needed.
        
        
        'TODO: This line of code loads data into the 'Received_fromDataSet.Received_from' table. You can move, or remove it, as needed.
        Me.Received_fromTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Received_from)
        recvd_from_orig_no_rows = DataGridView1.NewRowIndex
    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.ColumnIndex = 1 And e.RowIndex >= 0 Then
            recvd_from = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            recvd_text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            If DataGridView1.Rows(e.RowIndex).IsNewRow Then Return
            Try
                Me.Received_fromTableAdapter.UpdateQuery(recvd_text, recvd_from)
                log_text = "Complainant from amended - " & recvd_from & _
                                              " from " & orig_text & " to " & recvd_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate complainant entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter

        recvd_from = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        orig_text = DataGridView1.Rows(e.RowIndex).Cells(1).Value

    End Sub

    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles DataGridView1.RowsRemoved
        If e.RowIndex > 0 Then
            Try
                Try
                    Me.Received_fromTableAdapter.DeleteQuery(recvd_from)
                Catch
                    MessageBox.Show("can't delete as it's used in a complaint")
                End Try
                log_text = "Complainant deleted - " & recvd_from & " name " & orig_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If

    End Sub

    
    Private Sub Received_fromBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.Received_fromBindingSource.EndEdit()
        Me.Received_fromTableAdapter.Update(Me.PraiseAndComplaintsSQLDataSet.Received_from)

    End Sub
End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class costs_reasonfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.costs_reasontbox = New System.Windows.Forms.TextBox
        Me.exitbtn = New System.Windows.Forms.Button
        Me.exit_no_savebtn = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'costs_reasontbox
        '
        Me.costs_reasontbox.Location = New System.Drawing.Point(0, 0)
        Me.costs_reasontbox.Multiline = True
        Me.costs_reasontbox.Name = "costs_reasontbox"
        Me.costs_reasontbox.Size = New System.Drawing.Size(280, 219)
        Me.costs_reasontbox.TabIndex = 0
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(183, 240)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(84, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit and Save"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'exit_no_savebtn
        '
        Me.exit_no_savebtn.Location = New System.Drawing.Point(12, 240)
        Me.exit_no_savebtn.Name = "exit_no_savebtn"
        Me.exit_no_savebtn.Size = New System.Drawing.Size(105, 23)
        Me.exit_no_savebtn.TabIndex = 2
        Me.exit_no_savebtn.Text = "Exit without saving"
        Me.exit_no_savebtn.UseVisualStyleBackColor = True
        '
        'costs_reasonfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(279, 289)
        Me.Controls.Add(Me.exit_no_savebtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.costs_reasontbox)
        Me.Name = "costs_reasonfrm"
        Me.Text = "Cancel Costs Reason"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents costs_reasontbox As System.Windows.Forms.TextBox
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents exit_no_savebtn As System.Windows.Forms.Button
End Class

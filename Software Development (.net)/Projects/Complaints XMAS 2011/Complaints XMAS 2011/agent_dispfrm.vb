Public Class agent_dispfrm

    Private Sub agent_dispfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim agent_name As String = ""
        agent1 = -1
        agent2 = -1
        param1 = "onestep"
        param2 = "Bailiff"
        param3 = "01_rowid 2name_sur 3name_fore"
        param4 = "where status = 'O' and _rowid <> 2057 and _rowid <> 2037 and _rowid <> 2063 and _rowid <> 2144"
        param5 = " order by 2"
        ret_code = get_table(param1, param2, param3, param4, param5)
        If ret_code <> 0 Then
            Exit Sub
        End If

        Dim idx As Integer
        Dim last_agent As String = ""
        DataGridView1.Rows.Clear()
        For idx = 1 To no_of_rows
            If table_array(idx, 3) = Nothing Then
                idx += 1
                Continue For
            End If
            agent_name = Trim(table_array(idx, 2)) & ", " & Trim(table_array(idx, 3))
            agent_name = LCase(agent_name)

            Dim idx2 As Integer
            For idx2 = 1 To Len(agent_name)
                If Mid(agent_name, idx2, 2) = ", " Then
                    agent_name = UCase(Microsoft.VisualBasic.Left(agent_name, 1)) & _
                            Mid(agent_name, 2, idx2) & _
                          UCase(Mid(agent_name, idx2 + 2, 1)) & _
                          Microsoft.VisualBasic.Right(agent_name, Len(agent_name) - idx2 - 2)
                    Exit For
                End If
            Next
            If Len(Trim(agent_name)) > 0 Then
                If last_agent <> agent_name Then
                    DataGridView1.Rows.Add(agent_name)
                End If
                last_agent = agent_name
            End If
        Next
    End Sub


    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        search_agent_name = DataGridView1.Rows(e.RowIndex).Cells(0).Value.ToString
        Dim idx As Integer
        For idx = 1 To Len(search_agent_name)
            'If Mid(search_agent_name, idx, 1) = "," Then
            '    search_agent_surname = Microsoft.VisualBasic.Left(search_agent_name, idx - 1)
            '    search_agent_forename = Microsoft.VisualBasic.Right(search_agent_name, Len(search_agent_name) - idx)
            '    Exit For
            'End If
        Next
        'get agent numbers

        Dim table_agent_name As String
        
        For idx = 1 To no_of_rows
            If table_array(idx, 3) = Nothing Then
                idx += 1
                Continue For
            End If
            table_agent_name = Trim(table_array(idx, 2)) & ", " & Trim(table_array(idx, 3))
            If LCase(table_agent_name) = LCase(search_agent_name) Then
                If agent1 = -1 Then
                    agent1 = table_array(idx, 1)
                Else
                    agent2 = table_array(idx, 1)
                    Exit For
                End If
            End If
        Next
        Me.Close()
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class
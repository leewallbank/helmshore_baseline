<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class corr_actionsfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.CorrectiveActionsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.Corrective_actionsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Corrective_actionsTableAdapter
        Me.CorcodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CornameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CorrectiveActionsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CorcodeDataGridViewTextBoxColumn, Me.CornameDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.CorrectiveActionsBindingSource
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(402, 266)
        Me.DataGridView1.TabIndex = 0
        '
        'CorrectiveActionsBindingSource
        '
        Me.CorrectiveActionsBindingSource.DataMember = "Corrective actions"
        Me.CorrectiveActionsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSetBindingSource
        '
        'PraiseAndComplaintsSQLDataSetBindingSource
        '
        Me.PraiseAndComplaintsSQLDataSetBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        Me.PraiseAndComplaintsSQLDataSetBindingSource.Position = 0
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Corrective_actionsTableAdapter
        '
        Me.Corrective_actionsTableAdapter.ClearBeforeFill = True
        '
        'CorcodeDataGridViewTextBoxColumn
        '
        Me.CorcodeDataGridViewTextBoxColumn.DataPropertyName = "cor_code"
        Me.CorcodeDataGridViewTextBoxColumn.HeaderText = "Code"
        Me.CorcodeDataGridViewTextBoxColumn.Name = "CorcodeDataGridViewTextBoxColumn"
        Me.CorcodeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'CornameDataGridViewTextBoxColumn
        '
        Me.CornameDataGridViewTextBoxColumn.DataPropertyName = "cor_name"
        Me.CornameDataGridViewTextBoxColumn.HeaderText = "Text"
        Me.CornameDataGridViewTextBoxColumn.Name = "CornameDataGridViewTextBoxColumn"
        Me.CornameDataGridViewTextBoxColumn.Width = 250
        '
        'corr_actionsfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(402, 266)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "corr_actionsfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Corrective Actions"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CorrectiveActionsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents CorrectiveActionsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Corrective_actionsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Corrective_actionsTableAdapter
    Friend WithEvents CorcodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CornameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

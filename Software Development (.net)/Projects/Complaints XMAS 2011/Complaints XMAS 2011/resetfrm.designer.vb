<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class resetfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Inv_textLabel As System.Windows.Forms.Label
        Me.Inv_textComboBox = New System.Windows.Forms.ComboBox
        Me.resetbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.InvestigatorsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
        Me.InvestigatorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Inv_textLabel = New System.Windows.Forms.Label
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Inv_textLabel
        '
        Inv_textLabel.AutoSize = True
        Inv_textLabel.Location = New System.Drawing.Point(126, 82)
        Inv_textLabel.Name = "Inv_textLabel"
        Inv_textLabel.Size = New System.Drawing.Size(98, 13)
        Inv_textLabel.TabIndex = 1
        Inv_textLabel.Text = "Select Investigator:"
        '
        'Inv_textComboBox
        '
        Me.Inv_textComboBox.DataSource = Me.InvestigatorsBindingSource
        Me.Inv_textComboBox.DisplayMember = "inv_text"
        Me.Inv_textComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Inv_textComboBox.FormattingEnabled = True
        Me.Inv_textComboBox.Location = New System.Drawing.Point(129, 108)
        Me.Inv_textComboBox.Name = "Inv_textComboBox"
        Me.Inv_textComboBox.Size = New System.Drawing.Size(121, 21)
        Me.Inv_textComboBox.TabIndex = 0
        '
        'resetbtn
        '
        Me.resetbtn.Location = New System.Drawing.Point(240, 210)
        Me.resetbtn.Name = "resetbtn"
        Me.resetbtn.Size = New System.Drawing.Size(116, 23)
        Me.resetbtn.TabIndex = 2
        Me.resetbtn.Text = "Reset password"
        Me.resetbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.exitbtn.Location = New System.Drawing.Point(12, 210)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(107, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit without reset"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(24, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(181, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Password will be reset to ""password"""
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'InvestigatorsTableAdapter
        '
        Me.InvestigatorsTableAdapter.ClearBeforeFill = True
        '
        'InvestigatorsBindingSource
        '
        Me.InvestigatorsBindingSource.DataMember = "Investigators"
        Me.InvestigatorsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'resetfrm
        '
        Me.AcceptButton = Me.resetbtn
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.exitbtn
        Me.ClientSize = New System.Drawing.Size(456, 266)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.resetbtn)
        Me.Controls.Add(Inv_textLabel)
        Me.Controls.Add(Me.Inv_textComboBox)
        Me.Name = "resetfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reset password"
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Inv_textComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents resetbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents InvestigatorsTableAdapter As Complaints_XMAS_2011.DataSet1TableAdapters.InvestigatorsTableAdapter
    Friend WithEvents InvestigatorsBindingSource As System.Windows.Forms.BindingSource
End Class

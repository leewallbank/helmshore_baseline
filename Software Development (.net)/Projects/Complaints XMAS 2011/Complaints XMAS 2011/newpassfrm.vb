Public Class newpassfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub pass1txtbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles pass1txtbox.Validating
        ErrorProvider1.SetError(pass1txtbox, "")
        If Len(pass1txtbox.Text) < 6 Then
            ErrorProvider1.SetError(pass1txtbox, "Password must be at least 6 characters long")
            e.Cancel = True
        Else
            If Len(pass1txtbox.Text) > 10 Then
                ErrorProvider1.SetError(pass1txtbox, "Password must be at no more than 10 characters long")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub pass2txtbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles pass2txtbox.Validating
        
    End Sub

    Private Sub savebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles savebtn.Click
        ErrorProvider1.SetError(pass2txtbox, "")
        If pass1txtbox.Text <> pass2txtbox.Text Then
            ErrorProvider1.SetError(pass1txtbox, "Passwords were not the same - please re-enter")
            pass1txtbox.Text = ""
            pass2txtbox.Text = ""
            pass1txtbox.Focus()
        Else
            password_reset = True
            inv_password = pass1txtbox.Text
            Me.Close()
        End If
    End Sub

    Private Sub newpassfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub pass1txtbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pass1txtbox.TextChanged

    End Sub
End Class
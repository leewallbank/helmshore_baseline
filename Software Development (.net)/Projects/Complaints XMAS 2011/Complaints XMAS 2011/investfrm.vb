Public Class investfrm

    Private Sub investfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Complaints' table. You can move, or remove it, as needed.
        Me.ComplaintsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaints)
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Investigators' table. You can move, or remove it, as needed.
        Me.InvestigatorsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Investigators)
        'TODO: This line of code loads data into the 'InvestigatorsDataSet.Investigators' table. You can move, or remove it, as needed.
        inv_orig_no_rows = DataGridView1.RowCount
    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.RowIndex >= 0 Then
            inv_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            inv_text = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            Dim admin As Boolean
            Try
                admin = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            Catch
                admin = False
            End Try
            DataGridView1.Rows(e.RowIndex).Cells(3).Value = "password"
            If DataGridView1.Rows(e.RowIndex).IsNewRow Then Return
            Try
                Me.InvestigatorsTableAdapter.UpdateQuery(inv_text, admin, inv_code)
                log_text = "Investigator amended - " & inv_code & _
                                                 " from " & orig_text & " to " & inv_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate Investigator entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter

        inv_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        orig_text = DataGridView1.Rows(e.RowIndex).Cells(1).Value

    End Sub

    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles DataGridView1.RowsRemoved
        If e.RowIndex > 0 Then
            Try
                Try
                    Me.InvestigatorsTableAdapter.DeleteQuery(inv_code)
                Catch ex As Exception
                    MessageBox.Show("Can't delete as complaints are allocated")
                    Exit Sub
                End Try
                log_text = "Investigator deleted - " & inv_code & " name " & orig_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)

            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub InvestigatorsBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.InvestigatorsBindingSource.EndEdit()
        Me.InvestigatorsTableAdapter.Update(Me.PraiseAndComplaintsSQLDataSet.Investigators)

    End Sub
End Class
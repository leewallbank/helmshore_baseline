<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class displayfrm2
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.dispbtn = New System.Windows.Forms.Button
        Me.updbtn = New System.Windows.Forms.Button
        Me.excelbtn = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.comp_officer = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.disp_comp_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.comp_case_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.legal = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.recvd_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.completed_string = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.stage2_recvd_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.stage2_comp_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.stage3_recvd_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.stage3_comp_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.days_taken = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.days_left = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Client = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Type = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Against = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Agent = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Category = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Hold = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Founded = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.ComplaintsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComplaintsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ComplaintsTableAdapter
        Me.InvestigatorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InvestigatorsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
        Me.Received_fromBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Received_fromTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
        Me.Complaint_againstBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Complaint_againstTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Received_fromBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Complaint_againstBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.comp_officer, Me.disp_comp_no, Me.comp_case_no, Me.legal, Me.recvd_date, Me.completed_string, Me.stage2_recvd_date, Me.stage2_comp_date, Me.stage3_recvd_date, Me.stage3_comp_date, Me.days_taken, Me.days_left, Me.Client, Me.Type, Me.Against, Me.Agent, Me.Category, Me.Hold, Me.Founded})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(1007, 591)
        Me.DataGridView1.TabIndex = 0
        '
        'dispbtn
        '
        Me.dispbtn.Location = New System.Drawing.Point(722, 288)
        Me.dispbtn.Name = "dispbtn"
        Me.dispbtn.Size = New System.Drawing.Size(129, 23)
        Me.dispbtn.TabIndex = 1
        Me.dispbtn.Text = "Display Complaint"
        Me.dispbtn.UseVisualStyleBackColor = True
        '
        'updbtn
        '
        Me.updbtn.Location = New System.Drawing.Point(722, 334)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(129, 23)
        Me.updbtn.TabIndex = 2
        Me.updbtn.Text = "Update Complaint"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'excelbtn
        '
        Me.excelbtn.Location = New System.Drawing.Point(722, 377)
        Me.excelbtn.Name = "excelbtn"
        Me.excelbtn.Size = New System.Drawing.Size(129, 23)
        Me.excelbtn.TabIndex = 3
        Me.excelbtn.Text = "Write to excel"
        Me.excelbtn.UseVisualStyleBackColor = True
        '
        'comp_officer
        '
        Me.comp_officer.HeaderText = "Complaints Officer"
        Me.comp_officer.Name = "comp_officer"
        Me.comp_officer.ReadOnly = True
        Me.comp_officer.Width = 95
        '
        'disp_comp_no
        '
        Me.disp_comp_no.HeaderText = "Complaint No"
        Me.disp_comp_no.Name = "disp_comp_no"
        Me.disp_comp_no.ReadOnly = True
        Me.disp_comp_no.Width = 37
        '
        'comp_case_no
        '
        Me.comp_case_no.HeaderText = "Case ID"
        Me.comp_case_no.Name = "comp_case_no"
        Me.comp_case_no.ReadOnly = True
        Me.comp_case_no.Width = 50
        '
        'legal
        '
        Me.legal.HeaderText = "Legal"
        Me.legal.Name = "legal"
        Me.legal.ReadOnly = True
        Me.legal.Width = 40
        '
        'recvd_date
        '
        Me.recvd_date.HeaderText = "Stage 1 Date Recvd"
        Me.recvd_date.Name = "recvd_date"
        Me.recvd_date.ReadOnly = True
        Me.recvd_date.Width = 70
        '
        'completed_string
        '
        Me.completed_string.HeaderText = "Stage 1 Date Comp"
        Me.completed_string.Name = "completed_string"
        Me.completed_string.ReadOnly = True
        Me.completed_string.Width = 70
        '
        'stage2_recvd_date
        '
        Me.stage2_recvd_date.HeaderText = "Stage 2 Date Recvd"
        Me.stage2_recvd_date.Name = "stage2_recvd_date"
        Me.stage2_recvd_date.ReadOnly = True
        Me.stage2_recvd_date.Width = 70
        '
        'stage2_comp_date
        '
        Me.stage2_comp_date.HeaderText = "Stage 2 Date Comp"
        Me.stage2_comp_date.Name = "stage2_comp_date"
        Me.stage2_comp_date.ReadOnly = True
        Me.stage2_comp_date.Width = 70
        '
        'stage3_recvd_date
        '
        Me.stage3_recvd_date.HeaderText = "Stage 3 Date Recvd"
        Me.stage3_recvd_date.Name = "stage3_recvd_date"
        Me.stage3_recvd_date.ReadOnly = True
        Me.stage3_recvd_date.Width = 70
        '
        'stage3_comp_date
        '
        Me.stage3_comp_date.HeaderText = "Stage 3 Date Comp"
        Me.stage3_comp_date.Name = "stage3_comp_date"
        Me.stage3_comp_date.ReadOnly = True
        Me.stage3_comp_date.Width = 70
        '
        'days_taken
        '
        Me.days_taken.HeaderText = "Days Taken"
        Me.days_taken.Name = "days_taken"
        Me.days_taken.ReadOnly = True
        Me.days_taken.Width = 35
        '
        'days_left
        '
        Me.days_left.HeaderText = "Days Left"
        Me.days_left.Name = "days_left"
        Me.days_left.ReadOnly = True
        Me.days_left.Width = 35
        '
        'Client
        '
        Me.Client.HeaderText = "Client"
        Me.Client.Name = "Client"
        Me.Client.ReadOnly = True
        Me.Client.Width = 140
        '
        'Type
        '
        Me.Type.HeaderText = "Complainant"
        Me.Type.Name = "Type"
        Me.Type.ReadOnly = True
        Me.Type.Width = 76
        '
        'Against
        '
        Me.Against.HeaderText = "Against"
        Me.Against.Name = "Against"
        Me.Against.ReadOnly = True
        Me.Against.Width = 80
        '
        'Agent
        '
        Me.Agent.HeaderText = "Staff/bailiff"
        Me.Agent.Name = "Agent"
        Me.Agent.ReadOnly = True
        Me.Agent.Width = 110
        '
        'Category
        '
        Me.Category.HeaderText = "Cat"
        Me.Category.Name = "Category"
        Me.Category.ReadOnly = True
        Me.Category.Width = 30
        '
        'Hold
        '
        Me.Hold.HeaderText = "Hold Letter"
        Me.Hold.Name = "Hold"
        Me.Hold.ReadOnly = True
        Me.Hold.Width = 38
        '
        'Founded
        '
        Me.Founded.HeaderText = "Fnded"
        Me.Founded.Name = "Founded"
        Me.Founded.ReadOnly = True
        Me.Founded.Width = 40
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComplaintsBindingSource
        '
        Me.ComplaintsBindingSource.DataMember = "Complaints"
        Me.ComplaintsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'ComplaintsTableAdapter
        '
        Me.ComplaintsTableAdapter.ClearBeforeFill = True
        '
        'InvestigatorsBindingSource
        '
        Me.InvestigatorsBindingSource.DataMember = "Investigators"
        Me.InvestigatorsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'InvestigatorsTableAdapter
        '
        Me.InvestigatorsTableAdapter.ClearBeforeFill = True
        '
        'Received_fromBindingSource
        '
        Me.Received_fromBindingSource.DataMember = "Received_from"
        Me.Received_fromBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Received_fromTableAdapter
        '
        Me.Received_fromTableAdapter.ClearBeforeFill = True
        '
        'Complaint_againstBindingSource
        '
        Me.Complaint_againstBindingSource.DataMember = "Complaint_against"
        Me.Complaint_againstBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Complaint_againstTableAdapter
        '
        Me.Complaint_againstTableAdapter.ClearBeforeFill = True
        '
        'displayfrm2
        '
        Me.AcceptButton = Me.dispbtn
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1007, 591)
        Me.Controls.Add(Me.excelbtn)
        Me.Controls.Add(Me.updbtn)
        Me.Controls.Add(Me.dispbtn)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "displayfrm2"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Display Complaints List"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Received_fromBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Complaint_againstBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents dispbtn As System.Windows.Forms.Button
    Friend WithEvents updbtn As System.Windows.Forms.Button
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents ComplaintsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ComplaintsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ComplaintsTableAdapter
    Friend WithEvents InvestigatorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InvestigatorsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
    Friend WithEvents Received_fromBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Received_fromTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
    Friend WithEvents Complaint_againstBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaint_againstTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
    Friend WithEvents excelbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents comp_officer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents disp_comp_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents comp_case_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents legal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents recvd_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents completed_string As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents stage2_recvd_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents stage2_comp_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents stage3_recvd_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents stage3_comp_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents days_taken As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents days_left As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Client As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Against As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Agent As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Category As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Hold As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Founded As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

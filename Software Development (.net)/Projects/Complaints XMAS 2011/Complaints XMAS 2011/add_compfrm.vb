Public Class add_compfrm

    Private Sub add_compfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            Me.EthnicityTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Ethnicity)
            eth_combobox.Text = "N/A"
            ethnicity_desc = "N/A"
            gender_combobox.Text = "N/A"
            last_against_code = 0
            Me.Complaint_againstTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_against)
            Me.Receipt_typeTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Receipt_type)
            'delete any temporary documents
            delete_temp_directory()

            Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)

            Me.Received_fromTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Received_from)

            Me.Receipt_typeTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Receipt_type)

            populate_client_table()
            Dim idx As Integer
            For idx = 1 To cl_rows
                cl_combobox.Items.Add(client_table(idx, 2))
            Next
            Comp_recpt_codeComboBox.SelectedIndex = -1
            Comp_recvd_codeComboBox.SelectedIndex = -1
            Comp_against_codeComboBox.SelectedIndex = -1
            comp_against2_codecombobox.SelectedIndex = -1
            'populate comp_against2_codecombobox
            Dim row As DataRow
            idx = 0
            For Each row In Me.PraiseAndComplaintsSQLDataSet.Complaint_against.Rows
                comp_against2_codecombobox.Items.Add(Me.PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(idx).Item(1))
                idx += 1
            Next
            case_no = 0
            case_noTextBox.Text = 0
            cl_combobox.SelectedIndex = -1
            agentComboBox.SelectedIndex = -1
            catcombobox.SelectedIndex = -1
            codeComboBox.SelectedIndex = -1
            compTextBox.Text = ""
            alloc_to_code = 1
            InvComboBox.SelectedIndex = -1
            against_code = 1
            doc_textbox.Text = 0
            prioritycbox.Checked = False
            old_comp_no_tbox.Text = " "
            old_comp_no = 0
            ack_datepicker.Visible = False
            ack_cbox.Checked = False
            legalcbox.Checked = False
            monetary_risktbox.Text = ""
            solicitor_datepicker.Checked = False
            ins_datepicker.Checked = False
            legalgbox.Visible = False
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub case_noTextBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles case_noTextBox.Validated

    End Sub


    Private Sub case_noTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles case_noTextBox.Validating
        Try
            ErrorProvider1.SetError(case_noTextBox, "")
            case_no = 0
            If Len(Trim(case_noTextBox.Text)) = 0 Then
                populate_client_table()
                Dim idx2 As Integer
                For idx2 = 1 To cl_rows
                    cl_combobox.Items.Add(client_table(idx2, 2))
                Next
                Exit Sub
            End If
            If case_noTextBox.Text = 0 Then
                populate_client_table()
                Dim idx2 As Integer
                For idx2 = 1 To cl_rows
                    cl_combobox.Items.Add(client_table(idx2, 2))
                Next
                Exit Sub
            End If

            If Not IsNumeric(case_noTextBox.Text) Then
                ErrorProvider1.SetError(case_noTextBox, "Case number must be numeric")
                e.Cancel = True
                Exit Sub
            End If
            case_no = case_noTextBox.Text
            'check case number is on onestep
            param1 = "onestep"
            param2 = " select _rowid, clientschemeID from Debtor" & _
            " where _rowid = " & case_noTextBox.Text
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows <> 1 Then
                ErrorProvider1.SetError(case_noTextBox, "Case number is not on Onestep")
                Exit Sub
            End If
            Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(1)

            'get client number
            param2 = "select _rowid, clientID, schemeID from ClientScheme" & _
            " where _rowid = " & csid

            Dim csid_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows <> 1 Then
                ErrorProvider1.SetError(case_noTextBox, "Client scheme is not on Onestep")
                Exit Sub
            End If

            cl_no = csid_dataset.tables(0).rows(0).item(1)
            'get client name
            Dim idx As Integer
            For idx = 1 To cl_rows
                If client_table(idx, 1) = cl_no Then
                    cl_name = client_table(idx, 2)
                    Exit For
                End If
            Next
            cl_combobox.Items.Clear()
            cl_combobox.Items.Add(cl_name)
            cl_combobox.SelectedItem = cl_name
            Comp_against_codeComboBox.Focus()

            'get debt type
            Dim sch_id As Integer = csid_dataset.Tables(0).Rows(0).Item(2)
            param2 = "select name from Scheme where _rowid = " & sch_id
            Dim sch_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows <> 1 Then
                MsgBox("Unable to find scheme no " & sch_id)
            End If
            debt_typelbl.Text = Trim(sch_dataset.Tables(0).Rows(0).Item(0))

        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub


    Private Sub savebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles savebtn.Click
        Try
            ErrorProvider1.SetError(Comp_recpt_codeComboBox, "")
            If Len(Comp_recpt_codeComboBox.Text) = 0 Then
                ErrorProvider1.SetError(Comp_recpt_codeComboBox, "Receipt type must be entered")
                Comp_recpt_codeComboBox.Focus()
                Exit Sub
            End If
            ErrorProvider1.SetError(Comp_recvd_codeComboBox, "")
            If Len(Comp_recvd_codeComboBox.Text) = 0 Then
                ErrorProvider1.SetError(Comp_recvd_codeComboBox, "Received from must be entered")
                Comp_recvd_codeComboBox.Focus()
                Exit Sub
            End If

            ErrorProvider1.SetError(Comp_against_codeComboBox, "")
            If Len(Comp_against_codeComboBox.Text) = 0 Then
                ErrorProvider1.SetError(Comp_against_codeComboBox, "Complaint made against must be entered")
                Comp_against_codeComboBox.Focus()
                Exit Sub
            End If

            ErrorProvider1.SetError(agentComboBox, "")
            If Len(agentComboBox.Text) = 0 And _
                (Comp_against_codeComboBox.SelectedIndex > 1 And _
                 Comp_against_codeComboBox.SelectedIndex < 5) Then
                ErrorProvider1.SetError(agentComboBox, "Complaint against name must be entered")
                agentComboBox.Focus()
                Exit Sub
            End If

            ErrorProvider1.SetError(catcombobox, "")
            If Len(catcombobox.Text) = 0 Then
                ErrorProvider1.SetError(catcombobox, "Category must be entered")
                catcombobox.Focus()
                Exit Sub
            End If

            ErrorProvider1.SetError(catcombobox, "")
            If Len(codeComboBox.Text) = 0 Then
                ErrorProvider1.SetError(codeComboBox, "Category code must be entered")
                codeComboBox.Focus()
                Exit Sub
            End If

            ErrorProvider1.SetError(compTextBox, "")
            If Len(compTextBox.Text) > 500 Then
                ErrorProvider1.SetError(compTextBox, "Maximum of 500 characters")
                compTextBox.Focus()
                Exit Sub
            End If

            Dim code_no As Integer
            If codeComboBox.Text = "99 Contentious" Then
                stage_no = 0
                code_no = 99
            Else
                stage_no = 1
                If Mid(codeComboBox.Text, 2, 1) = " " Then
                    code_no = Microsoft.VisualBasic.Left(codeComboBox.Text, 2)
                Else
                    code_no = Microsoft.VisualBasic.Left(codeComboBox.Text, 3)
                End If
            End If

            'get ethnicity code
            Dim idx As Integer
            For idx = 0 To Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows.Count - 1
                Dim eth_code As Integer = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(0)
                Dim eth_desc As String = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(1)
                If ethnicity_desc = eth_desc Then
                    ethnicity = eth_code
                    Exit For
                End If
            Next
            'insert complaint
            'save on test database for test logid
            If log_code = test_logid Then
                Try
                    Test_complaintsTableAdapter.InsertQuery(Comp_dateDateTimePicker.Text, recpt_code, _
                    recvd_from, case_no, cl_no, _
                    against_code, agent_no, _
                    Microsoft.VisualBasic.Left(catcombobox.Text, 1), code_no, _
                    dept_code, log_code, alloc_to_code, compTextBox.Text, " ", _
                    0, "U", 0, 3, stage_no, 1, 1, prioritycbox.Checked, old_comp_no, _
                    gender_combobox.Text, ethnicity, against2_code, agent2_no)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    Me.Close()
                    Exit Sub
                End Try
                comp_no = Me.Test_complaintsTableAdapter.SelectQuery(log_code)
                'update ack date if entered
                If ack_cbox.Checked = True Then
                    Test_complaintsTableAdapter.UpdateQuery8(ack_datepicker.Value, comp_no)
                End If
                'update legal details 
                If legalcbox.Checked = True Then
                    Dim solicitor_date, ins_date As Date
                    If solicitor_datepicker.Checked = True Then
                        solicitor_date = Format(solicitor_datepicker.Value, "yyyy-MM-dd")
                    Else
                        solicitor_date = CDate("1800,1,1")
                    End If
                    If ins_datepicker.Checked = True Then
                        ins_date = Format(ins_datepicker.Value, "yyyy-MM-dd")
                    Else
                        ins_date = CDate("1800,1,1")
                    End If
                    Test_complaintsTableAdapter.UpdateQuery9("Y", solicitor_date, ins_date, monetary_risk, comp_no)
                End If
            Else
                Try
                    ComplaintsTableAdapter.InsertQuery(Comp_dateDateTimePicker.Text, recpt_code, _
                    recvd_from, case_no, cl_no, _
                    against_code, agent_no, _
                    Microsoft.VisualBasic.Left(catcombobox.Text, 1), code_no, _
                    dept_code, log_code, alloc_to_code, compTextBox.Text, " ", 0, "U", 0, 3, _
                    stage_no, 1, 1, prioritycbox.Checked, old_comp_no, gender_combobox.Text, ethnicity, against2_code, agent2_no)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    Me.Close()
                    Exit Sub
                End Try
                comp_no = Me.ComplaintsTableAdapter.ScalarQuery2(log_code)
                'update ack date if entered
                If ack_cbox.Checked = True Then
                    ComplaintsTableAdapter.UpdateQuery8(ack_datepicker.Value, comp_no)
                End If
                'update legal details 
                If legalcbox.Checked = True Then
                    Dim solicitor_date, ins_date As Date
                    If solicitor_datepicker.Checked = True Then
                        solicitor_date = Format(solicitor_datepicker.Value, "yyyy-MM-dd")
                    Else
                        solicitor_date = CDate("1800,1,1")
                    End If
                    If ins_datepicker.Checked = True Then
                        ins_date = Format(ins_datepicker.Value, "yyyy-MM-dd")
                    Else
                        ins_date = CDate("1800,1,1")
                    End If
                    ComplaintsTableAdapter.UpdateQuery9("Y", solicitor_date, ins_date, monetary_risk, comp_no)
                End If
            End If
            'display complaint no

            Try
                MsgBox("Complaint number is " & comp_no)

            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try

            'rename directory for any documents saved
            Dim dirname As String = "r:\complaints\T" & log_code
            rename_directory(dirname)
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub Comp_dateDateTimePicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Comp_dateDateTimePicker.Validating
        ErrorProvider1.SetError(Comp_dateDateTimePicker, "")
        If Comp_dateDateTimePicker.Text < DateAdd("d", -200, Now) Then
            ErrorProvider1.SetError(Comp_dateDateTimePicker, "Date can only go back 200 days")
            e.Cancel = True
        ElseIf Comp_dateDateTimePicker.Text > Now Then
            ErrorProvider1.SetError(Comp_dateDateTimePicker, "Date can't be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub Comp_recpt_codeComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Comp_recpt_codeComboBox.Validated
        'get recpt code number
        ErrorProvider1.SetError(Comp_recpt_codeComboBox, "")
        recpt_code = PraiseAndComplaintsSQLDataSet.Receipt_type.Rows(Comp_recpt_codeComboBox.SelectedIndex).Item(0)
    End Sub


    Private Sub dontsavebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dontsavebtn.Click
        Me.Close()
    End Sub


    Private Sub catcombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles catcombobox.Validated
        Try
            ErrorProvider1.SetError(catcombobox, "")
            Dim cat_table As New PraiseAndComplaintsSQLDataSet.Complaint_categoriesDataTable
            Dim cat_code As String = Microsoft.VisualBasic.Left(catcombobox.SelectedItem, 1)
            Me.Complaint_categoriesTableAdapter.Fill2(cat_table, cat_code)
            Dim row As DataRow
            Dim cat_text As String
            Dim idx As Integer = 0
            codeComboBox.Items.Clear()
            For Each row In cat_table.Rows
                cat_text = cat_table.Rows(idx).Item(1) & " " & cat_table.Rows(idx).Item(2)
                codeComboBox.Items.Add(cat_text)
                idx += 1
            Next
            codeComboBox.SelectedIndex = -1
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub


    Private Sub Comp_against_codeComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Comp_against_codeComboBox.Validated
        Try
            against_code = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(Comp_against_codeComboBox.SelectedIndex).Item(0)
            If against_code <> last_against_code Then
                last_against_code = against_code
            Else
                Exit Sub
            End If

            agentComboBox.Items.Clear()
            against_code = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(Comp_against_codeComboBox.SelectedIndex).Item(0)
            Dim where_clause As String
            If Comp_against_codeComboBox.SelectedIndex = 2 Then
                agent_intext = "I"
                where_clause = " where agent_type = 'B'"
            ElseIf Comp_against_codeComboBox.SelectedIndex = 3 Then
                agent_intext = "E"
                where_clause = " where agent_type = 'B'"
            Else
                agent_intext = " "
                where_clause = " where (agent_type = 'P' or agent_type = 'A')"
            End If

            If agent_intext <> " " Then
                where_clause = where_clause & " and internalExternal = '" & agent_intext & "'"
            End If
            populate_bailiff_table(where_clause)
            Dim idx As Integer

            For idx = 1 To agent_rows
                agentComboBox.Items.Add(bailiff_table(idx, 2))
            Next
            agentComboBox.Items.Add("Various")
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    'Private Sub populate_departments()
    '    Me.DepartmentsTableAdapter.Fill(Me.DepartmentsDataSet.Departments)
    '    agentComboBox.Items.Clear()
    '    Dim idx As Integer = 0
    '    Dim row As DataRow
    '    For Each row In DepartmentsDataSet.Tables(0).Rows
    '        agentComboBox.Items.Add(DepartmentsDataSet.Tables(0).Rows(idx).Item(1))
    '        idx += 1
    '    Next
    '    dept_rows = idx
    'End Sub
    Private Sub Comp_recvd_codeComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Comp_recvd_codeComboBox.Validated
        'get recvd from number
        recvd_from = PraiseAndComplaintsSQLDataSet.Received_from.Rows(Comp_recvd_codeComboBox.SelectedIndex).Item(0)
    End Sub


    Private Sub cl_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cl_combobox.Validated
        Dim idx As Integer
        For idx = 1 To cl_rows
            If client_table(idx, 2) = cl_combobox.SelectedItem Then
                cl_no = client_table(idx, 1)
                Exit For
            End If
        Next
    End Sub


    Private Sub agentComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentComboBox.Validated
        Dim idx As Integer
        agent_no = 0
        ErrorProvider1.SetError(agentComboBox, "")
        If agentComboBox.SelectedItem = "Various" Then
            agent_no = 9999
        Else
            For idx = 1 To agent_rows
                If bailiff_table(idx, 2) = agentComboBox.SelectedItem Then
                    agent_no = bailiff_table(idx, 1)
                    Exit For
                End If
            Next
        End If
        dept_code = 0
        Exit Sub

        agent_no = 0
    End Sub

    Private Sub Comp_recpt_codeComboBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Comp_recpt_codeComboBox.Validating
        ErrorProvider1.SetError(Comp_recpt_codeComboBox, "")
        If Len(Comp_recpt_codeComboBox.Text) = 0 Then
            ErrorProvider1.SetError(Comp_recpt_codeComboBox, "You must select a form of receipt")
            e.Cancel = True
        End If
    End Sub

    Private Sub Comp_recvd_codeComboBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Comp_recvd_codeComboBox.Validating
        ErrorProvider1.SetError(Comp_recvd_codeComboBox, "")
        If Len(Comp_recvd_codeComboBox.Text) = 0 Then
            ErrorProvider1.SetError(Comp_recvd_codeComboBox, "You must select received from")
            e.Cancel = True
        End If
    End Sub

    Private Sub addbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addbtn.Click
        OpenFileDialog1.FileName = ""
        OpenFileDialog1.Filter = "Text,word or tif|*.txt; *.doc;*.tif"
        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim fname As String = OpenFileDialog1.FileName
            Dim dirname As String = "T" & log_code
            save_document(fname, dirname)
        End If
        doc_textbox.Text = doc_textbox.Text + 1

    End Sub

    Private Sub compTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles compTextBox.TextChanged

    End Sub

    Private Sub Comp_against_codeComboBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Comp_against_codeComboBox.Validating
        ErrorProvider1.SetError(Comp_against_codeComboBox, "")
        If Len(Comp_against_codeComboBox.Text) = 0 Then
            ErrorProvider1.SetError(Comp_against_codeComboBox, "You must select who the complaint is against")
            e.Cancel = True
        End If
    End Sub

    Private Sub codeComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles codeComboBox.Validated
        ErrorProvider1.SetError(codeComboBox, "")
    End Sub

    Private Sub InvComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles InvComboBox.Validated
        Try
            alloc_to_code = PraiseAndComplaintsSQLDataSet.Investigators.Rows(InvComboBox.SelectedIndex).Item(0)
        Catch
            alloc_to_code = 1
        End Try
    End Sub

    Private Sub old_comp_no_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles old_comp_no_tbox.Validating
        ErrorProvider1.SetError(old_comp_no_tbox, "")
        If Microsoft.VisualBasic.Len(Trim(old_comp_no_tbox.Text)) = 0 Then
            old_comp_no = 0
            Exit Sub
        Else
            Try
                old_comp_no = Trim(old_comp_no_tbox.Text)
            Catch ex As Exception
                ErrorProvider1.SetError(old_comp_no_tbox, "Old complaint number must be blank or numeric")
                e.Cancel = True
            End Try
        End If
        If old_comp_no > 9999 Then
            ErrorProvider1.SetError(old_comp_no_tbox, "Old complaint number too high (up to 9999)")
            e.Cancel = True
        End If
    End Sub

    Private Sub eth_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles eth_combobox.SelectedIndexChanged

    End Sub

    Private Sub eth_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles eth_combobox.Validated
        ethnicity_desc = eth_combobox.Text
    End Sub

    Private Sub case_noTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles case_noTextBox.TextChanged

    End Sub

    Private Sub ack_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ack_cbox.CheckedChanged

    End Sub

    Private Sub ack_cbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ack_cbox.Validating
        If ack_cbox.Checked = True Then
            ack_datepicker.Visible = True
        Else
            ack_datepicker.Visible = False
        End If
    End Sub

    Private Sub ack_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ack_datepicker.Validating
        ErrorProvider1.SetError(ack_datepicker, "")
        If Format(ack_datepicker.Value, "yyyy.MM.dd") < Format(Comp_dateDateTimePicker.Value, "yyyy.MM.dd") Then
            ErrorProvider1.SetError(ack_datepicker, "Acknowledgement date can't be before complaint entry date")
            e.Cancel = True
        End If
        If Format(ack_datepicker.Value, "yyyy.MM.dd") > Format(Now, "yyyy.MM.dd") Then
            ErrorProvider1.SetError(ack_datepicker, "Acknowledgement date can't be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub Comp_against_codeComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Comp_against_codeComboBox.SelectedIndexChanged

    End Sub

    Private Sub agentComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agentComboBox.SelectedIndexChanged

    End Sub

    Private Sub comp_against2_codecombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles comp_against2_codecombobox.Validated
        If comp_against2_codecombobox.SelectedIndex < 0 Then
            Exit Sub
        End If
        Try
            against2_code = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(comp_against2_codecombobox.SelectedIndex).Item(0)
            If against2_code <> last_against2_code Then
                last_against2_code = against2_code
            Else
                Exit Sub
            End If

            agent2_combobox.Items.Clear()
            against2_code = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(comp_against2_codecombobox.SelectedIndex).Item(0)
            Dim where_clause As String
            If comp_against2_codecombobox.SelectedIndex = 2 Then
                agent_intext = "I"
                where_clause = " where agent_type = 'B'"
            ElseIf comp_against2_codecombobox.SelectedIndex = 3 Then
                agent_intext = "E"
                where_clause = " where agent_type = 'B'"
            Else
                agent_intext = " "
                where_clause = " where (agent_type = 'P' or agent_type = 'A')"
            End If

            If agent_intext <> " " Then
                where_clause = where_clause & " and internalExternal = '" & agent_intext & "'"
            End If
            populate_bailiff_table(where_clause)
            Dim idx As Integer

            For idx = 1 To agent_rows
                agent2_combobox.Items.Add(bailiff_table(idx, 2))
            Next
            agent2_combobox.Items.Add("Various")
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub agent2_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles agent2_combobox.Validated
        Dim idx As Integer
        agent2_no = 0
        ErrorProvider1.SetError(agent2_combobox, "")
        If agent2_combobox.SelectedItem = "Various" Then
            agent2_no = 9999
        Else
            For idx = 1 To agent_rows
                If bailiff_table(idx, 2) = agent2_combobox.SelectedItem Then
                    agent2_no = bailiff_table(idx, 1)
                    Exit For
                End If
            Next
        End If
        dept_code = 0
        Exit Sub

        agent2_no = 0
    End Sub

    Private Sub agent2_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agent2_combobox.SelectedIndexChanged

    End Sub

    Private Sub codeComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles codeComboBox.SelectedIndexChanged

    End Sub

    Private Sub codeComboBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles codeComboBox.Validating
        ErrorProvider1.SetError(codeComboBox, "")
        If codeComboBox.SelectedItem = "99 Contentious" Then
            ErrorProvider1.SetError(codeComboBox, "Contentious is no longer valid")
            e.Cancel = True
        End If
    End Sub


    Private Sub legalcbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles legalcbox.CheckedChanged

    End Sub

    Private Sub legalcbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles legalcbox.Validated
        If legalcbox.Checked = True Then
            legalgbox.Visible = True
            monetary_risk = -999
        Else
            legalgbox.Visible = False
        End If
    End Sub

    Private Sub legal_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles solicitor_datepicker.Validating
        ErrorProvider1.SetError(solicitor_datepicker, "")
        If solicitor_datepicker.Checked = True Then
            If Format(CDate(solicitor_datepicker.Text), "yyyy-MM-dd") < Format(Comp_dateDateTimePicker.Value, "yyyy-MM-dd") Then
                ErrorProvider1.SetError(solicitor_datepicker, "Date can't be before entered date")
                e.Cancel = True
            ElseIf solicitor_datepicker.Text > Now Then
                ErrorProvider1.SetError(solicitor_datepicker, "Date can't be in the future")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub legal_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles solicitor_datepicker.ValueChanged

    End Sub

    Private Sub ins_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ins_datepicker.Validating
        ErrorProvider1.SetError(ins_datepicker, "")
        If ins_datepicker.Checked = True Then
            If Format(CDate(ins_datepicker.Text), "yyyy-MM-dd") < Format(Comp_dateDateTimePicker.Value, "yyyy-MM-dd") Then
                ErrorProvider1.SetError(ins_datepicker, "Date can't be before entered date")
                e.Cancel = True
            ElseIf ins_datepicker.Text > Now Then
                ErrorProvider1.SetError(ins_datepicker, "Date can't be in the future")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub monetary_risktbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles monetary_risktbox.TextChanged

    End Sub

    Private Sub monetary_risktbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles monetary_risktbox.Validating
        ErrorProvider1.SetError(monetary_risktbox, "")
        If Microsoft.VisualBasic.Len(Trim(monetary_risktbox.Text)) = 0 Then
            monetary_risk = -999
        Else
            If Not IsNumeric(monetary_risktbox.Text) Then
                ErrorProvider1.SetError(monetary_risktbox, "Please enter valid amount")
                e.Cancel = True
            Else
                monetary_risk = monetary_risktbox.Text
                If monetary_risk < 0 Then
                    ErrorProvider1.SetError(monetary_risktbox, "Please enter positive amount")
                    e.Cancel = True
                End If
            End If
        End If
    End Sub

    Private Sub ins_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ins_datepicker.ValueChanged

    End Sub

    Private Sub Comp_dateDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Comp_dateDateTimePicker.ValueChanged

    End Sub
End Class



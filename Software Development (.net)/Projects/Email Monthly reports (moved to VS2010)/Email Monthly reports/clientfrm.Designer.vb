<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class clientfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.client = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.scheme = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.last_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.email = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cc1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cc2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.csid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cc_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.client, Me.scheme, Me.last_date, Me.email, Me.cc1, Me.cc2, Me.csid, Me.cc_no})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(999, 498)
        Me.DataGridView1.TabIndex = 0
        '
        'client
        '
        Me.client.HeaderText = "Client"
        Me.client.Name = "client"
        Me.client.ReadOnly = True
        Me.client.Width = 200
        '
        'scheme
        '
        Me.scheme.HeaderText = "Scheme"
        Me.scheme.Name = "scheme"
        Me.scheme.ReadOnly = True
        '
        'last_date
        '
        Me.last_date.HeaderText = "Last date"
        Me.last_date.Name = "last_date"
        Me.last_date.Width = 75
        '
        'email
        '
        Me.email.HeaderText = "Email"
        Me.email.Name = "email"
        Me.email.Width = 175
        '
        'cc1
        '
        Me.cc1.HeaderText = "CC1"
        Me.cc1.Name = "cc1"
        Me.cc1.Width = 175
        '
        'cc2
        '
        Me.cc2.HeaderText = "CC2"
        Me.cc2.Name = "cc2"
        Me.cc2.Width = 165
        '
        'csid
        '
        Me.csid.HeaderText = "CSID"
        Me.csid.Name = "csid"
        Me.csid.Visible = False
        '
        'cc_no
        '
        Me.cc_no.HeaderText = "No of CCs"
        Me.cc_no.Name = "cc_no"
        Me.cc_no.ReadOnly = True
        Me.cc_no.Width = 30
        '
        'clientfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(999, 498)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "clientfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Double-click on client scheme to maintain list of CCs"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents client As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents scheme As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents last_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents email As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cc1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cc2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents csid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cc_no As System.Windows.Forms.DataGridViewTextBoxColumn

End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainform
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.allbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.maintainbtn = New System.Windows.Forms.Button
        Me.somebtn = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.FeesSQLDataSet = New Email_Monthly_reports.FeesSQLDataSet
        Me.Client_email_ccs_sentBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Client_email_ccs_sentTableAdapter = New Email_Monthly_reports.FeesSQLDataSetTableAdapters.client_email_ccs_sentTableAdapter
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Client_email_ccs_sentBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'allbtn
        '
        Me.allbtn.Location = New System.Drawing.Point(93, 131)
        Me.allbtn.Name = "allbtn"
        Me.allbtn.Size = New System.Drawing.Size(113, 23)
        Me.allbtn.TabIndex = 0
        Me.allbtn.Text = "Send ALL emails"
        Me.allbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(217, 276)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(111, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Label1"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(16, 276)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 3
        '
        'maintainbtn
        '
        Me.maintainbtn.Location = New System.Drawing.Point(93, 70)
        Me.maintainbtn.Name = "maintainbtn"
        Me.maintainbtn.Size = New System.Drawing.Size(113, 23)
        Me.maintainbtn.TabIndex = 4
        Me.maintainbtn.Text = "Maintain email list"
        Me.maintainbtn.UseVisualStyleBackColor = True
        '
        'somebtn
        '
        Me.somebtn.Location = New System.Drawing.Point(57, 193)
        Me.somebtn.Name = "somebtn"
        Me.somebtn.Size = New System.Drawing.Size(179, 23)
        Me.somebtn.TabIndex = 5
        Me.somebtn.Text = "Send emails not sent this month"
        Me.somebtn.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(26, 22)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Label2"
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Client_email_ccs_sentBindingSource
        '
        Me.Client_email_ccs_sentBindingSource.DataMember = "client email ccs sent"
        Me.Client_email_ccs_sentBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'Client_email_ccs_sentTableAdapter
        '
        Me.Client_email_ccs_sentTableAdapter.ClearBeforeFill = True
        '
        'mainform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 323)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.somebtn)
        Me.Controls.Add(Me.maintainbtn)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.allbtn)
        Me.Name = "mainform"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Email Monthly Reports"
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Client_email_ccs_sentBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents allbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents maintainbtn As System.Windows.Forms.Button
    Friend WithEvents somebtn As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents FeesSQLDataSet As Email_Monthly_reports.FeesSQLDataSet
    Friend WithEvents Client_email_ccs_sentBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Client_email_ccs_sentTableAdapter As Email_Monthly_reports.FeesSQLDataSetTableAdapters.client_email_ccs_sentTableAdapter


End Class

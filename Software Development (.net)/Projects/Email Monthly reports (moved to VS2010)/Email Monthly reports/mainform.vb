Public Class mainform
    Dim log_user As String
    Dim cl_name As String = ""
    Dim next_cl_no, next_csid As Integer
    Dim sch_name As String = ""
    Dim subject As String = ""
    Dim fromaddress As String
    Dim next_toaddress As String
    Dim body As String
    Dim att2 As String = " "
    Dim att3 As String = " "
    Dim att4 As String = " "
    Dim att5 As String = " "
    Dim att6 As String = " "
    Dim att7 As String = " "
    Dim att8 As String = " "
    Dim att9 As String = " "
    Dim error_found As Boolean = False
    Dim csid_array(100)
    Dim csid_idx As Integer = 0
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        prod_run = False
        Dim env_str As String = ""
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try

        If env_str = "Prod" Then prod_run = True
        If Not prod_run Then
            MsgBox("Warning - test run only")
        End If

        conn_open = False
        error_path = "h:\email clients\"
        error_file = error_path & "error_log_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"
        log_path = "h:\email clients\"
        log_file = error_path & "log_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"
        Dim oWSH = CreateObject("WScript.Shell")
        log_user = oWSH.ExpandEnvironmentStrings("%USERNAME%")
        Label1.Text = "Monthly reports for " & Format(Now, "MMM yyyy")
        If branch = 1 Then
            Label2.Text = "Bailiff"
        ElseIf branch = 2 Then
            Label2.Text = "Collect"
        ElseIf branch = 10 Then
            Label2.Text = "Marston"
        Else
            MsgBox("Unknown Branch")
            Me.Close()
        End If

    End Sub

    Private Sub allbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allbtn.Click
        allbtn.Enabled = False
        maintainbtn.Enabled = False
        somebtn.Enabled = False
        exitbtn.Enabled = False
        send_all = True
        If MsgBox("Sending emails to ALL clients", MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton2, "Send Emails") = MsgBoxResult.Ok Then
            Dim retn_no As Integer = prepare_emails()
            If retn_no = 0 Then
                MsgBox("ALL emails sent OK")
            Else
                If retn_no = 99 Then
                    MsgBox("No emails sent")
                Else
                    MsgBox("Emails sent but see error log at h:\email clients")
                End If
            End If
        Else
            MsgBox("No emails sent")
        End If
        allbtn.Enabled = True
        somebtn.Enabled = True
        exitbtn.Enabled = True
        maintainbtn.Enabled = True
    End Sub

    Function prepare_emails()
        'first send emails for main (first) name plus cc_all
        'then for cc1, then for cc2 etc
        'this is so can send multiple schemes to same name
        'there are different ccs for same first name so can't send ccs same time as main name.

        Dim toaddress As String = ""
        Dim ccaddress As String
        If branch = 10 Then
            fromaddress = log_user & "@marstongroup.co.uk"
            ccaddress = log_user & "@marstongroup.co.uk"
            fromaddress = "clientreports@marstongroup.co.uk"
        Else
            fromaddress = log_user & "@rossendales.com"
            ccaddress = log_user & "@rossendales.com"
            fromaddress = "crystal@rossendales.com"
        End If

        body = "Good Morning"
        If Format(Now, "HH") > 12 Then
            body = "Good afternoon"
        End If

        body = body & vbNewLine & "Please find attached monthly statistics produced from our Onestep system."


        'new message for this month
        'body = body & vbNewLine
        'body = body & "Please ignore previous One Step Month End reports which were inadvertently sent to you earlier today." & vbNewLine
        'body = body & "The correct reports are now attached dated the 6th August 2010." & vbNewLine

        'body = body & "Sincere apologies for the inconvenience." & vbNewLine
       

        If branch = 1 Then
            'body = body & vbNewLine
            'body &= "Please note, the attached reports were generated on 1st April 2014" & vbNewLine & _
            '            "prior to cases transitioning to new schemes. The May 2014 reports will include the new schemes. "
            'MsgBox("WARNING - adding transition note and using reports in EndPeriod_Original")
            body = body & vbNewLine & vbNewLine & "Regards" & vbNewLine
            body = body & "Client Liaison Team" & vbNewLine & _
                    "        Rossendales" & vbNewLine & _
                    "        PO Box 324" & vbNewLine & _
                    "        Rossendale" & vbNewLine & _
                    "        BB4 0GE   " & vbNewLine & _
                    "Tel: 01706 830323" & vbNewLine

        ElseIf branch = 2 Then
            body = body & vbNewLine & vbNewLine & "Regards" & vbNewLine
            body = body & "Rossendales Collect Limited" & vbNewLine & _
            "        Rossendales" & vbNewLine & _
            "        PO Box 324" & vbNewLine & _
            "        Rossendale" & vbNewLine & _
            "        BB4 0GE   " & vbNewLine & _
            "Tel: 0844 701 3991"
        Else
            body = body & vbNewLine & vbNewLine & "Regards" & vbNewLine
            body = body & "Marston Group Limited" & vbNewLine & _
                       "        PO Box 323" & vbNewLine & _
                       "        Rossendale" & vbNewLine & _
                       "        BB4 0GD   " & vbNewLine & _
                       "Tel: 0845 076 6262"
        End If

        cc_all = InputBox("Send a CC for emails to ", "EMAIL to send CC to. Blank out for no CC", ccaddress)
        error_file = error_path & "error_log_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"
        Dim this_month As String = Format(Now, "MM") & "_" & Format(Now, "MMM")
        file_path = "O:\DebtRecovery\Archives\" & Format(Now, "yyyy") & "\" & this_month & "\00_EndPeriod\" & _
           "forClientSchemes\"
        'If branch = 1 Then
        '    file_path = "O:\DebtRecovery\Archives\" & Format(Now, "yyyy") & "\" & this_month & "\00_EndPeriod_Original\" & _
        '               "forClientSchemes\"
        'End If
        Dim row As DataRow
        Dim idx As Integer = 0
        Dim final_warning_given As Boolean = False
        Dim csid As Integer
        Dim last_toaddress As String = ""
        Dim cl_rows As Integer
        Dim email_array(20) As email_struct
        Dim email_cl_id(20) As Integer
        Dim email_idx As Integer = -1
        Dim cl_id, sch_id As Integer
        'send main name first
        ccform.Client_emailsTableAdapter.FillBy1(ccform.FeesSQLDataSet.client_emails)
        cl_rows = ccform.FeesSQLDataSet.client_emails.Rows.Count
        For Each row In ccform.FeesSQLDataSet.client_emails.Rows
            ProgressBar1.Value = idx / cl_rows * 100
            Dim last_date_str As String
            Try
                last_date_str = ccform.FeesSQLDataSet.client_emails.Rows(idx).Item(2)
            Catch
                last_date_str = " "
            End Try
            If Not send_all Then
                If Microsoft.VisualBasic.Len(Trim(last_date_str)) > 0 Then
                    Dim last_date As Date = CDate(last_date_str)
                    If Month(last_date) = Month(Now) Then
                        idx += 1
                        Continue For
                    End If
                End If
            End If
            toaddress = Trim(ccform.FeesSQLDataSet.client_emails.Rows(idx).Item(1))

            If Not final_warning_given Then
                final_warning_given = True
                If send_all Then
                    If MsgBox("Send ALL emails to clients", MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton2, "-----Last chance to stop emails-----") = MsgBoxResult.Cancel Then
                        Return 99
                    End If
                Else
                    If MsgBox("Send unsent emails to clients", MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton2, "-----Last chance to stop emails-----") = MsgBoxResult.Cancel Then
                        Return 99
                    End If
                End If
            End If
            If toaddress <> last_toaddress And email_idx >= 0 Then
                'sort email_array by cl_no
                Array.Sort(email_cl_id, email_array, 0, email_idx + 1)
                Dim idx2 As Integer
                For idx2 = 0 To email_idx
                    csid = email_array(idx2).csid
                    cl_id = email_cl_id(idx2)
                    sch_id = email_array(idx2).sch_id
                    Dim send_now As Boolean = False
                    If idx2 = email_idx Or _
                        (idx2 < email_idx And email_cl_id(idx2 + 1) <> cl_id) Then
                        send_now = True
                    End If
                    send_email(csid, last_toaddress, cl_id, sch_id, send_now)
                Next
                email_idx = -1
                ReDim email_array(20), email_cl_id(20)
                att2 = " "
                att3 = " "
                att4 = " "
                att5 = " "
                att6 = " "
                att7 = " "
                att8 = " "
                att9 = " "
            End If
            csid = ccform.FeesSQLDataSet.client_emails.Rows(idx).Item(0)
            toaddress = Trim(ccform.FeesSQLDataSet.client_emails.Rows(idx).Item(1))
            last_toaddress = toaddress
            param1 = "onestep"
            param2 = "select clientID, schemeID from ClientScheme where _rowid = " & csid & _
            " and branchID = " & branch
            Dim csid_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                idx += 1
                Continue For
            End If

            email_idx += 1
            If UBound(email_array) < email_idx Then
                ReDim Preserve email_array(email_idx + 20)
                ReDim Preserve email_cl_id(email_idx + 20)
            End If

            cl_id = csid_dataset.Tables(0).Rows(0).Item(0)
            sch_id = csid_dataset.Tables(0).Rows(0).Item(1)
            email_cl_id(email_idx) = cl_id
            email_array(email_idx).csid = csid
            email_array(email_idx).sch_id = sch_id

            csid_idx += 1
            If UBound(csid_array) < csid_idx Then
                ReDim Preserve csid_array(csid_idx + 50)
            End If
            csid_array(csid_idx) = csid
            idx += 1
            If idx > 5 And Not prod_run Then
                Exit For
            End If
        Next

        'send last one if required
        If email_idx >= 0 Then
            'sort email_array by cl_no
            Array.Sort(email_cl_id, email_array, 0, email_idx + 1)
            Dim idx2 As Integer
            For idx2 = 0 To email_idx
                csid = email_array(idx2).csid
                cl_id = email_cl_id(idx2)
                sch_id = email_array(idx2).sch_id
                Dim send_now As Boolean = False
                If idx2 = email_idx Or _
                  (idx2 < email_idx And email_cl_id(idx2 + 1) <> cl_id) Then
                    send_now = True
                End If
                send_email(csid, last_toaddress, cl_id, sch_id, send_now)
            Next
            email_idx = -1
            ReDim email_array(20), email_cl_id(20)
            att2 = " "
            att3 = " "
            att4 = " "
            att5 = " "
            att6 = " "
            att7 = " "
            att8 = " "
            att9 = " "
        End If
        ProgressBar1.Value = 0

        'send ccs
        cc_all = " "  'only needs to be sent once
        last_toaddress = ""
        ccform.Client_email_ccsTableAdapter.FillBy1(ccform.FeesSQLDataSet.client_email_ccs)
        cl_rows = ccform.FeesSQLDataSet.client_email_ccs.Rows.Count
        idx = 0
        For Each row In ccform.FeesSQLDataSet.client_email_ccs.Rows
            ProgressBar1.Value = idx / cl_rows * 100
            toaddress = Trim(ccform.FeesSQLDataSet.client_email_ccs.Rows(idx).Item(1))
            If Not prod_run Then
                toaddress = "jblundell@rossendales.com"
            End If
            csid = ccform.FeesSQLDataSet.client_email_ccs.Rows(idx).Item(0)
            Dim csid_found As Boolean = False
            'do not send if not send all and already sent this month
            '12.2.2014 not good enough to check if csid sent - need to check if cc sent
            'If Not send_all Then
            '    'see if csid already sent

            '    Dim idx2 As Integer
            '    For idx2 = 1 To csid_idx
            '        If csid_array(idx2) = csid Then
            '            csid_found = True
            '            Exit For
            '        End If
            '    Next
            'End If
            'If csid_found = False And Not send_all Then  'only send ccs for ones where main emails have been sent
            '    idx += 1
            '    Continue For
            'End If
            'do not send if already sent this month - may be re-starting after error
            'sent ccs are stored in table
            If Not send_all Then
                Dim send_cc As Boolean = True
                Client_email_ccs_sentTableAdapter.FillBy(Me.FeesSQLDataSet.client_email_ccs_sent, csid, toaddress)
                Dim sent_cc_idx As Integer
                For sent_cc_idx = 0 To Me.FeesSQLDataSet.client_email_ccs_sent.Rows.Count - 1
                    Dim sent_date As Date = Me.FeesSQLDataSet.client_email_ccs_sent.Rows(sent_cc_idx).Item(3)
                    If Format(sent_date, "MM yyyy") = Format(Now, "MM yyyy") Then
                        send_cc = False
                        Exit For
                    End If
                Next
                If Not send_cc Then
                    idx += 1
                    Continue For
                End If

            End If
            If toaddress <> last_toaddress And email_idx >= 0 Then
                'sort email_array by cl_no
                Array.Sort(email_cl_id, email_array, 0, email_idx + 1)
                Dim idx2 As Integer
                For idx2 = 0 To email_idx
                    csid = email_array(idx2).csid
                    cl_id = email_cl_id(idx2)
                    sch_id = email_array(idx2).sch_id
                    Dim send_now As Boolean = False
                    If idx2 = email_idx Or _
                        (idx2 < email_idx And email_cl_id(idx2 + 1) <> cl_id) Then
                        send_now = True
                    End If
                    send_email(csid, last_toaddress, cl_id, sch_id, send_now)
                Next
                email_idx = -1
                ReDim email_array(20), email_cl_id(20)
                att2 = " "
                att3 = " "
                att4 = " "
                att5 = " "
                att6 = " "
                att7 = " "
                att8 = " "
                att9 = " "
            End If
            csid = ccform.FeesSQLDataSet.client_email_ccs.Rows(idx).Item(0)
            toaddress = Trim(ccform.FeesSQLDataSet.client_email_ccs.Rows(idx).Item(1))
            last_toaddress = toaddress

            param1 = "onestep"
            param2 = "select clientID, schemeID from ClientScheme where _rowid = " & csid & _
            " and branchID = " & branch
            Dim csid_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                idx += 1
                Continue For
            End If

            email_idx += 1
            If UBound(email_array) < email_idx Then
                ReDim Preserve email_array(email_idx + 20)
                ReDim Preserve email_cl_id(email_idx + 20)
            End If
            cl_id = csid_dataset.Tables(0).Rows(0).Item(0)
            sch_id = csid_dataset.Tables(0).Rows(0).Item(1)
            email_cl_id(email_idx) = cl_id
            email_array(email_idx).csid = csid
            email_array(email_idx).sch_id = sch_id

            csid_idx += 1
            If UBound(csid_array) < csid_idx Then
                ReDim Preserve csid_array(csid_idx + 50)
            End If
            idx += 1
            If idx > 5 And Not prod_run Then
                Exit For
            End If
        Next

        'send last one if required
        If email_idx >= 0 Then
            'sort email_array by cl_no
            Array.Sort(email_cl_id, email_array, 0, email_idx + 1)
            Dim idx2 As Integer
            For idx2 = 0 To email_idx
                csid = email_array(idx2).csid
                cl_id = email_cl_id(idx2)
                sch_id = email_array(idx2).sch_id
                Dim send_now As Boolean = False
                If idx2 = email_idx Or _
                   (idx2 < email_idx And email_cl_id(idx2 + 1) <> cl_id) Then
                    send_now = True
                End If
                send_email(csid, last_toaddress, cl_id, sch_id, send_now)
            Next
        End If
        ProgressBar1.Value = 0
        If error_found Then
            Return 10
        Else
            If final_warning_given Then
                Return 0
            Else
                Return 99
            End If
        End If
    End Function

    Sub send_email(ByVal csid As Integer, ByVal toaddress As String, ByVal cl_id As Integer, ByVal sch_id As Integer, ByVal send_now As Boolean)
        'If cl_id <> 73 Then
        '    Exit Sub
        'End If
        'get client name
        param2 = "select name from Client where _rowid = " & cl_id
        Dim cl_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            error_message = "ClientID " & cl_id & " not found on onestep" & vbNewLine
            write_error()
            Exit Sub
        End If
        cl_name = Trim(cl_dataset.Tables(0).Rows(0).Item(0))
        
        remove_chars(cl_name)
        Microsoft.VisualBasic.Replace(cl_name, " ", "")

        'get scheme name
        param2 = "select name from Scheme where _rowid = " & sch_id
        Dim sch_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            error_message = "SchemeID " & sch_id & " not found on onestep" & vbNewLine
            write_error()
            Exit Sub
        End If
        sch_name = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
        remove_chars(sch_name)
        'get filename for this clientscheme
        client_document = cl_name & "-" & cl_id & "\" & sch_name & "-" & sch_id & ".pdf"
        file_name = file_path & client_document

        If send_now = False Then
            If att2 = " " Then
                att2 = file_name
            ElseIf att3 = " " Then
                att3 = file_name
            ElseIf att4 = " " Then
                att4 = file_name
            ElseIf att5 = " " Then
                att5 = file_name
            ElseIf att6 = " " Then
                att6 = file_name
            ElseIf att7 = " " Then
                att7 = file_name
            ElseIf att8 = " " Then
                att8 = file_name
            ElseIf att9 = " " Then
                att9 = file_name
            Else
                send_now = True
            End If
        End If
        If send_now = False Then
            Try
                If prod_run Then
                    ccform.Client_emailsTableAdapter.UpdateQuery1(Now, csid)
                End If
            Catch ex As Exception
                error_message = "Last date not updated for client scheme " & _
                cl_name & " " & sch_name & vbNewLine
                write_error()
                MsgBox("Last date not set for client scheme " & cl_name & " " & sch_name)
                error_found = True
            End Try
            'log_message = client_document & " sent to " & toaddress
            'log_message = log_message & vbNewLine
            'write_log()
            Exit Sub
        End If
        subject = "Monthly reports for " & cl_name & "-" & cl_id
        If Not prod_run Then
            toaddress = "JBlundell@rossendales.com"
            fromaddress = "crystal@rossendales.com"
        End If
        If email(toaddress, fromaddress, subject, body, file_name, att2, att3, att4, att5, att6, att7, att8, att9) = 0 Then
            Try
                If prod_run Then
                    ccform.Client_emailsTableAdapter.UpdateQuery1(Now, csid)
                End If
                'save entry in ccs_sent table
                If cc_all = " " Then
                    Me.Client_email_ccs_sentTableAdapter.InsertQuery(csid, toaddress, Now)
                End If
            Catch ex As Exception
                error_message = "Last date not updated for client scheme " & _
                cl_name & " " & sch_name & vbNewLine
                write_error()
                MsgBox("Last date not set for client scheme " & cl_name & " " & sch_name)
                error_found = True
            End Try
        Else
            error_found = True
        End If
        att2 = " "
        att3 = " "
        att4 = " "
        att5 = " "
        att6 = " "
        att7 = " "
        att8 = " "
        att9 = " "
    End Sub

    Private Sub maintainbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles maintainbtn.Click
        allbtn.Enabled = False
        somebtn.Enabled = False
        exitbtn.Enabled = False
        maintainbtn.Enabled = False
        clientfrm.ShowDialog()
        allbtn.Enabled = True
        somebtn.Enabled = True
        exitbtn.Enabled = True
        maintainbtn.Enabled = True
    End Sub

    Private Sub somebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles somebtn.Click
        allbtn.Enabled = False
        somebtn.Enabled = False
        exitbtn.Enabled = False
        maintainbtn.Enabled = False
        send_all = False
        Dim retn_no As Integer = prepare_emails()
        If retn_no = 0 Then
            MsgBox("emails sent OK")
        Else
            If retn_no = 99 Then
                MsgBox("No emails sent")
            Else
                MsgBox("Emails sent but see error log at h:\email clients")
            End If
        End If
        allbtn.Enabled = True
        somebtn.Enabled = True
        exitbtn.Enabled = True
        maintainbtn.Enabled = True
    End Sub

    Private Sub Client_email_ccs_sentBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.Client_email_ccs_sentBindingSource.EndEdit()
        Me.Client_email_ccs_sentTableAdapter.Update(Me.FeesSQLDataSet.client_email_ccs_sent)

    End Sub
End Class

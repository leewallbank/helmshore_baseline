Public Class ccform

    Private Sub ccform_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'delete all ccs and then insert all
        Client_email_ccsTableAdapter.DeleteQuery(csid_parm)
        Dim datarow As DataGridViewRow
        Dim idx As Integer = 0
        cc_no_parm = 0
        For Each datarow In ccDataGridView.Rows
            Dim cc_cc As String
            Try
                cc_cc = ccDataGridView.Rows(idx).Cells(0).Value
            Catch ex As Exception
                idx += 1
                Continue For
            End Try
            If cc_cc = Nothing Then
                idx += 1
                Continue For
            End If
            Client_email_ccsTableAdapter.InsertQuery(csid_parm, cc_cc)
            idx += 1
            cc_no_parm += 1
        Next
    End Sub

    Private Sub ccform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Client_email_ccsTableAdapter.FillBy(FeesSQLDataSet.client_email_ccs, csid_parm)
        'populate datagridview
        ccDataGridView.Rows.Clear()
        Dim datarow As DataRow
        Dim idx As Integer = 0
        For Each datarow In FeesSQLDataSet.client_email_ccs.Rows
            ccDataGridView.Rows.Add(FeesSQLDataSet.client_email_ccs.Rows(idx).Item(1))
            idx += 1
        Next
        cl_lbl.Text = cl_name_parm
        sch_lbl.Text = sch_name_parm
    End Sub

    Private Sub ccDataGridView_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles ccDataGridView.CellValidating
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Len(Trim(e.FormattedValue.ToString)) = 0 Then
            Exit Sub
        End If
        If InStr(e.FormattedValue.ToString, """") > 0 Then
            MsgBox("Email address must not contain quotes")
            e.Cancel = True
            Exit Sub
        End If
        If InStr(e.FormattedValue.ToString, ",") > 0 Then
            MsgBox("Email address must not contain a comma")
            e.Cancel = True
        End If
        If InStr(e.FormattedValue.ToString, "@") = 0  Then
            MsgBox("Email address must contain @")
            e.Cancel = True
        End If
    End Sub

    Private Sub Client_email_ccsBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.Client_email_ccsBindingSource.EndEdit()
        Me.Client_email_ccsTableAdapter.Update(Me.FeesSQLDataSet.client_email_ccs)

    End Sub
End Class
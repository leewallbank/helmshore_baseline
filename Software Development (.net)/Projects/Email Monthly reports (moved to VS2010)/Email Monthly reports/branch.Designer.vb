<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class branchfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.bailbtn = New System.Windows.Forms.Button
        Me.collectbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.marstonbtn = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'bailbtn
        '
        Me.bailbtn.Location = New System.Drawing.Point(56, 29)
        Me.bailbtn.Name = "bailbtn"
        Me.bailbtn.Size = New System.Drawing.Size(135, 23)
        Me.bailbtn.TabIndex = 0
        Me.bailbtn.Text = "Rossendales Bailiffs"
        Me.bailbtn.UseVisualStyleBackColor = True
        '
        'collectbtn
        '
        Me.collectbtn.Location = New System.Drawing.Point(56, 79)
        Me.collectbtn.Name = "collectbtn"
        Me.collectbtn.Size = New System.Drawing.Size(135, 23)
        Me.collectbtn.TabIndex = 1
        Me.collectbtn.Text = "Rossendales Collect"
        Me.collectbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(189, 204)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 3
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'marstonbtn
        '
        Me.marstonbtn.Location = New System.Drawing.Point(56, 129)
        Me.marstonbtn.Name = "marstonbtn"
        Me.marstonbtn.Size = New System.Drawing.Size(135, 23)
        Me.marstonbtn.TabIndex = 2
        Me.marstonbtn.Text = "Marston"
        Me.marstonbtn.UseVisualStyleBackColor = True
        '
        'branchfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.marstonbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.collectbtn)
        Me.Controls.Add(Me.bailbtn)
        Me.Name = "branchfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Select Branch"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents bailbtn As System.Windows.Forms.Button
    Friend WithEvents collectbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents marstonbtn As System.Windows.Forms.Button
End Class

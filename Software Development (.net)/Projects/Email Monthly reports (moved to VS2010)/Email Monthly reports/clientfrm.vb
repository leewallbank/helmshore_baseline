Public Class clientfrm
    Private Sub clientfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'populate datagrid
        DataGridView1.Rows.Clear()
        'get clients
        param1 = "onestep"
        param2 = "select name, _rowid from Client " & _
        " where _rowid <> 1 and _rowid <> 2 and _rowid <> 24 order by name"
        Dim cl_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            error_message = "Clients not found on onestep" & vbNewLine
            write_error()
            Exit Sub
        End If
        Dim cl_rows As Integer = no_of_rows - 1
        Dim cl_no As Integer
        Dim idx As Integer
        Dim client_name, scheme_name, email, cc1, cc2, last_date_str As String
        For idx = 0 To cl_rows
            mainform.ProgressBar1.Value = idx / cl_rows * 100
            cl_no = cl_dataset.Tables(0).Rows(idx).Item(1)
            client_name = Trim(cl_dataset.Tables(0).Rows(idx).Item(0))
            'get csid
            param2 = "select _rowid, schemeID from ClientScheme where clientID = " & cl_no & _
            "  and branchID = " & branch
            Dim csid_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim sch_no, csid, cc_no As Integer
            Dim idx2 As Integer
            Dim csid_rows As Integer = no_of_rows - 1
            For idx2 = 0 To csid_rows
                Dim remit_found As Boolean = False
                cc_no = 0
                csid = csid_dataset.Tables(0).Rows(idx2).Item(0)
                'ignore if there are no remittances 
                param2 = "select count(*) from Remit where clientschemeID = " & csid
                Dim remit_dataset As DataSet = get_dataset(param1, param2)
                If remit_dataset.Tables(0).Rows(0).Item(0) > 0 Then
                    remit_found = True
                End If
                sch_no = csid_dataset.Tables(0).Rows(idx2).Item(1)
                'get scheme name
                param2 = "select name from Scheme where _rowid = " & sch_no
                Dim sch_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 0 Then
                    error_message = "SchemeID " & sch_no & " not found on onestep" & vbNewLine
                    write_error()
                    Exit Sub
                End If

                scheme_name = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                ccform.Client_emailsTableAdapter.FillBy(ccform.FeesSQLDataSet.client_emails, csid)
                Try
                    last_date_str = ccform.FeesSQLDataSet.client_emails.Rows(0).Item(2)
                Catch
                    last_date_str = " "
                End Try
                If last_date_str > " " Then
                    Try
                        last_date_str = Format(CDate(last_date_str), "dd/MM/yyyy")
                    Catch
                        last_date_str = " "
                    End Try
                End If

                'get from email address from table
                Try
                    email = ccform.FeesSQLDataSet.client_emails.Rows(0).Item(1)
                Catch ex As Exception
                    email = " "
                End Try
                If remit_found = False And email = " " Then
                    Continue For
                End If
                'see if there are any ccs
                ccform.Client_email_ccsTableAdapter.FillBy(ccform.FeesSQLDataSet.client_email_ccs, csid)
                Dim row As DataRow
                Dim idx3 As Integer = 0
                cc1 = " "
                cc2 = " "
                For Each row In ccform.FeesSQLDataSet.client_email_ccs.Rows
                    If idx3 = 0 Then
                        cc1 = ccform.FeesSQLDataSet.client_email_ccs.Rows(0).Item(1)
                    ElseIf idx3 = 1 Then
                        cc2 = ccform.FeesSQLDataSet.client_email_ccs.Rows(1).Item(1)
                    End If
                    cc_no += 1
                    idx3 += 1
                Next
                DataGridView1.Rows.Add(client_name, scheme_name, last_date_str, email, cc1, cc2, csid, cc_no, branch)
            Next
        Next
        mainform.ProgressBar1.Value = 0
    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Len(Trim(DataGridView1.Rows(e.RowIndex).Cells(3).Value)) = 0 Then
            MsgBox("Add main email address before any ccs")
            Exit Sub
        End If
        double_click = True
        csid_parm = DataGridView1.Rows(e.RowIndex).Cells(6).Value
        cl_name_parm = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        sch_name_parm = DataGridView1.Rows(e.RowIndex).Cells(1).Value
        ccform.ShowDialog()
        DataGridView1.Rows(e.RowIndex).Cells(7).Value = cc_no_parm
        ccform.Client_email_ccsTableAdapter.FillBy(ccform.FeesSQLDataSet.client_email_ccs, csid_parm)
        Dim row As DataRow
        Dim idx3 As Integer = 0
        Dim cc1 As String = " "
        Dim cc2 As String = " "
        For Each row In ccform.FeesSQLDataSet.client_email_ccs.Rows
            If idx3 = 0 Then
                cc1 = ccform.FeesSQLDataSet.client_email_ccs.Rows(0).Item(1)
            ElseIf idx3 = 1 Then
                cc2 = ccform.FeesSQLDataSet.client_email_ccs.Rows(1).Item(1)
            End If
            idx3 += 1
        Next
        DataGridView1.Rows(e.RowIndex).Cells(4).Value = cc1
        DataGridView1.Rows(e.RowIndex).Cells(5).Value = cc2
        double_click = False
    End Sub

    Private Sub DataGridView1_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DataGridView1.CellValidating
        If e.RowIndex < 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = 3 Then  'email
            If Microsoft.VisualBasic.Len(Trim(e.FormattedValue.ToString)) = 0 _
            And DataGridView1.Rows(e.RowIndex).Cells(7).Value > 0 Then
                MsgBox("Remove ccs before removing main email address")
                e.Cancel = True
                Exit Sub
            End If
            If Microsoft.VisualBasic.Len(Trim(e.FormattedValue.ToString)) = 0 Then
                Exit Sub
            End If
            If InStr(e.FormattedValue.ToString, """") > 0 Then
                MsgBox("Email address must not contain quotes")
                e.Cancel = True
                Exit Sub
            End If
            If InStr(e.FormattedValue.ToString, ",") > 0 Then
                MsgBox("Email address must not contain a comma")
                e.Cancel = True
            End If
            If InStr(e.FormattedValue.ToString, "@") = 0 Then
                MsgBox("Email address must contain a @")
                e.Cancel = True
                Exit Sub
            End If
        End If

        If e.ColumnIndex = 2 Then
            Dim last_date_str As String = e.FormattedValue.ToString
            If Microsoft.VisualBasic.Len(Trim(last_date_str)) = 0 Then
                Exit Sub
            End If
            Try
                Dim last_date As Date = CDate(last_date_str)
            Catch
                MsgBox("Invalid date")
                e.Cancel = True
            End Try
        End If
        If e.ColumnIndex = 4 Or e.ColumnIndex = 5 Then
            orig_cc = Trim(DataGridView1.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
            If Microsoft.VisualBasic.Len(Trim(e.FormattedValue.ToString)) > 0 _
            And Microsoft.VisualBasic.Len(Trim(DataGridView1.Rows(e.RowIndex).Cells(3).Value)) = 0 Then
                MsgBox("Enter main email address before any ccs")
                e.Cancel = True
            End If
            If Microsoft.VisualBasic.Len(Trim(e.FormattedValue.ToString)) = 0 Then
                Exit Sub
            End If
            If InStr(e.FormattedValue.ToString, """") > 0 Then
                MsgBox("Email address must not contain quotes")
                e.Cancel = True
                Exit Sub
            End If
            If InStr(e.FormattedValue.ToString, ",") > 0 Then
                MsgBox("Email address must not contain a comma")
                e.Cancel = True
            End If
            If InStr(e.FormattedValue.ToString, "@") = 0 Then
                MsgBox("Email address must contain @")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.RowIndex < 0 Or double_click Then
            Exit Sub
        End If
        Dim csid As Integer = DataGridView1.Rows(e.RowIndex).Cells(6).Value
        ccform.Client_emailsTableAdapter.FillBy(ccform.FeesSQLDataSet.client_emails, csid)
        Try
            csid = ccform.FeesSQLDataSet.client_emails.Rows(0).Item(0)
        Catch
            ccform.Client_emailsTableAdapter.InsertQuery(csid, " ")
        End Try
        If e.ColumnIndex = 2 Then
            Dim last_date_str As String = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            Try
                Dim last_date As Date = CDate(last_date_str)
            Catch
                last_date_str = Nothing
            End Try
            Try
                ccform.Client_emailsTableAdapter.UpdateQuery1(last_date_str, csid)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
        If e.ColumnIndex = 3 Then
            Dim email As String = DataGridView1.Rows(e.RowIndex).Cells(3).Value
            Try
                ccform.Client_emailsTableAdapter.UpdateQuery(email, csid)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End If
        If e.ColumnIndex = 4 Then
            Dim cc1 As String = DataGridView1.Rows(e.RowIndex).Cells(4).Value
            If Microsoft.VisualBasic.Len(orig_cc) > 0 Then
                If Microsoft.VisualBasic.Len(Trim(cc1)) = 0 Then
                    ccform.Client_email_ccsTableAdapter.DeleteQuery1(csid, orig_cc)
                    ccform.Client_email_ccsTableAdapter.FillBy(ccform.FeesSQLDataSet.client_email_ccs, csid)
                    Dim row As DataRow
                    Dim idx3 As Integer = 0
                    Dim cc2 As String = " "
                    For Each row In ccform.FeesSQLDataSet.client_email_ccs.Rows
                        If idx3 = 0 Then
                            cc1 = ccform.FeesSQLDataSet.client_email_ccs.Rows(0).Item(1)
                        ElseIf idx3 = 1 Then
                            cc2 = ccform.FeesSQLDataSet.client_email_ccs.Rows(1).Item(1)
                        End If
                        idx3 += 1
                    Next
                    DataGridView1.Rows(e.RowIndex).Cells(4).Value = cc1
                    DataGridView1.Rows(e.RowIndex).Cells(5).Value = cc2
                    DataGridView1.Rows(e.RowIndex).Cells(7).Value = idx3
                Else
                    If cc1 = orig_cc Then
                        Exit Sub
                    End If
                    Try
                        ccform.Client_email_ccsTableAdapter.UpdateQuery(cc1, csid, orig_cc)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                End If
            Else
                Try
                    ccform.Client_email_ccsTableAdapter.InsertQuery(csid, cc1)
                    DataGridView1.Rows(e.RowIndex).Cells(7).Value += 1
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            End If

        End If
        If e.ColumnIndex = 5 Then
            Dim cc2 As String = DataGridView1.Rows(e.RowIndex).Cells(5).Value
            If Microsoft.VisualBasic.Len(orig_cc) > 0 Then
                If Microsoft.VisualBasic.Len(Trim(cc2)) = 0 Then
                    ccform.Client_email_ccsTableAdapter.DeleteQuery1(csid, orig_cc)
                    DataGridView1.Rows(e.RowIndex).Cells(7).Value -= 1
                    ccform.Client_email_ccsTableAdapter.FillBy(ccform.FeesSQLDataSet.client_email_ccs, csid)
                    Dim row As DataRow
                    Dim idx3 As Integer = 0
                    For Each row In ccform.FeesSQLDataSet.client_email_ccs.Rows
                        If idx3 = 1 Then
                            cc2 = ccform.FeesSQLDataSet.client_email_ccs.Rows(1).Item(1)
                        End If
                        idx3 += 1
                    Next
                    DataGridView1.Rows(e.RowIndex).Cells(5).Value = cc2
                Else
                    If cc2 = orig_cc Then
                        Exit Sub
                    End If
                    Try
                        ccform.Client_email_ccsTableAdapter.UpdateQuery(cc2, csid, orig_cc)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try

                End If
            Else
                Try
                    ccform.Client_email_ccsTableAdapter.InsertQuery(csid, cc2)
                    DataGridView1.Rows(e.RowIndex).Cells(7).Value += 1
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            End If
        End If

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class
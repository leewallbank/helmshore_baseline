<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rd231btn = New System.Windows.Forms.Button
        Me.RD253btn = New System.Windows.Forms.Button
        Me.allbtn = New System.Windows.Forms.Button
        Me.RD230btn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.startDateDTP = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.endDateDTP = New System.Windows.Forms.DateTimePicker
        Me.rd229btn = New System.Windows.Forms.Button
        Me.rd232btn = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'rd231btn
        '
        Me.rd231btn.Location = New System.Drawing.Point(188, 193)
        Me.rd231btn.Name = "rd231btn"
        Me.rd231btn.Size = New System.Drawing.Size(92, 23)
        Me.rd231btn.TabIndex = 1
        Me.rd231btn.Text = "Run RD231"
        Me.rd231btn.UseVisualStyleBackColor = True
        '
        'RD253btn
        '
        Me.RD253btn.Location = New System.Drawing.Point(188, 336)
        Me.RD253btn.Name = "RD253btn"
        Me.RD253btn.Size = New System.Drawing.Size(92, 23)
        Me.RD253btn.TabIndex = 3
        Me.RD253btn.Text = "Run RD253"
        Me.RD253btn.UseVisualStyleBackColor = True
        '
        'allbtn
        '
        Me.allbtn.Location = New System.Drawing.Point(188, 70)
        Me.allbtn.Name = "allbtn"
        Me.allbtn.Size = New System.Drawing.Size(92, 23)
        Me.allbtn.TabIndex = 0
        Me.allbtn.Text = "All Reports"
        Me.allbtn.UseVisualStyleBackColor = True
        '
        'RD230btn
        '
        Me.RD230btn.Location = New System.Drawing.Point(188, 132)
        Me.RD230btn.Name = "RD230btn"
        Me.RD230btn.Size = New System.Drawing.Size(92, 23)
        Me.RD230btn.TabIndex = 2
        Me.RD230btn.Text = "Run RD230"
        Me.RD230btn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(460, 397)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 5
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'startDateDTP
        '
        Me.startDateDTP.Location = New System.Drawing.Point(63, 25)
        Me.startDateDTP.Name = "startDateDTP"
        Me.startDateDTP.Size = New System.Drawing.Size(130, 20)
        Me.startDateDTP.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(103, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Start date"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 397)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(294, 23)
        Me.ProgressBar1.TabIndex = 8
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(135, 116)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(238, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Open as xls, remove blank rows and save as csv"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(310, 9)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 13)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "End date"
        '
        'endDateDTP
        '
        Me.endDateDTP.Location = New System.Drawing.Point(270, 25)
        Me.endDateDTP.Name = "endDateDTP"
        Me.endDateDTP.Size = New System.Drawing.Size(130, 20)
        Me.endDateDTP.TabIndex = 12
        '
        'rd229btn
        '
        Me.rd229btn.Location = New System.Drawing.Point(188, 256)
        Me.rd229btn.Name = "rd229btn"
        Me.rd229btn.Size = New System.Drawing.Size(92, 23)
        Me.rd229btn.TabIndex = 14
        Me.rd229btn.Text = "Run RD229"
        Me.rd229btn.UseVisualStyleBackColor = True
        '
        'rd232btn
        '
        Me.rd232btn.Location = New System.Drawing.Point(188, 295)
        Me.rd232btn.Name = "rd232btn"
        Me.rd232btn.Size = New System.Drawing.Size(92, 23)
        Me.rd232btn.TabIndex = 15
        Me.rd232btn.Text = "Run RD232"
        Me.rd232btn.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(135, 177)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(238, 13)
        Me.Label4.TabIndex = 16
        Me.Label4.Text = "Open as xls, remove blank rows and save as csv"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(135, 240)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(238, 13)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "Open as xls, remove blank rows and save as csv"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(575, 449)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.rd232btn)
        Me.Controls.Add(Me.rd229btn)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.endDateDTP)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.startDateDTP)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.RD230btn)
        Me.Controls.Add(Me.allbtn)
        Me.Controls.Add(Me.RD253btn)
        Me.Controls.Add(Me.rd231btn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Lowell reports"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents rd231btn As System.Windows.Forms.Button
    Friend WithEvents RD253btn As System.Windows.Forms.Button
    Friend WithEvents allbtn As System.Windows.Forms.Button
    Friend WithEvents RD230btn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents startDateDTP As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents endDateDTP As System.Windows.Forms.DateTimePicker
    Friend WithEvents rd229btn As System.Windows.Forms.Button
    Friend WithEvents rd232btn As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label

End Class

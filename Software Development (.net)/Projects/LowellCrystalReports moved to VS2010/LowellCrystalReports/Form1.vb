Public Class Form1
    Dim all_reports As Boolean = False
    Dim filepath, filename As String
    Private Sub rd231btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd231btn.Click
        run_RD231()
    End Sub
    Private Sub run_RD231()
        disable_buttons()
        ProgressBar1.Value = 50
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD231report = New RD231P
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(startDateDTP.Value)
        SetCurrentValuesForParameterField1(RD231report, myArrayList1)
        myArrayList1.Add(endDateDTP.Value)
        SetCurrentValuesForParameterField2(RD231report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD231report)
        If Not all_reports Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".csv"
                .OverwritePrompt = True
                .FileName = "RD231 TDX Lowell non-financial update.xls"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
            End If
        Else
            filename = filepath & "RD231 TDX Lowell non-financial update.xls"
        End If
        ProgressBar1.Value = 60
        Application.DoEvents()
        RD231report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
        RD231report.close()
        If Not all_reports Then
            MsgBox("Completed")
            Me.Close()
        End If
    End Sub
    Private Sub RD230btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RD230btn.Click
        run_RD230()
    End Sub
    Private Sub run_RD230()
        disable_buttons()
        ProgressBar1.Value = 10
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD230report = New RD230P
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(startDateDTP.Value)
        SetCurrentValuesForParameterField1(RD230report, myArrayList1)
        myArrayList1.Add(endDateDTP.Value)
        SetCurrentValuesForParameterField2(RD230report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD230report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RD230 TDX Lowell Payment file.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = SaveFileDialog1.FileName
            Dim idx As Integer
            For idx = filename.Length To 1 Step -1
                If Mid(filename, idx, 1) = "\" Then
                    Exit For
                End If
            Next
            filepath = Microsoft.VisualBasic.Left(filename, idx)
            ProgressBar1.Value = 20
            Application.DoEvents()
            RD230report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
            RD230report.close()
            If Not all_reports Then
                MsgBox("Completed")
                Me.Close()
            End If
        Else
            MsgBox("Report not saved")
            Me.Close()
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub run_RD229()
        disable_buttons()
        ProgressBar1.Value = 30
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD229report = New RD229P
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(startDateDTP.Value)
        SetCurrentValuesForParameterField1(RD229report, myArrayList1)
        myArrayList1.Add(endDateDTP.Value)
        SetCurrentValuesForParameterField2(RD229report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD229report)
        If Not all_reports Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = "RD229 TDX Lowell Write-off file.xls"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
            Else
                MsgBox("Report not saved")
                Me.Close()
                Exit Sub
            End If
        Else
            filename = filepath & "RD229 TDX Lowell Write-off file.xls"
        End If

        ProgressBar1.Value = 40
        Application.DoEvents()
        RD229report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
        RD229report.close()
        If Not all_reports Then
            MsgBox("Completed")
            Me.Close()
        End If

    End Sub
    Private Sub run_RD232()
        disable_buttons()
        ProgressBar1.Value = 70
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD232Preport = New RD232P
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(startDateDTP.Value)
        SetCurrentValuesForParameterField1(RD232Preport, myArrayList1)
        myArrayList1.Add(endDateDTP.Value)
        SetCurrentValuesForParameterField2(RD232Preport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD232Preport)
        If Not all_reports Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = "RD232 TDX Lowell Recon file.xls"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
            End If
        Else
            filename = filepath & "RD232 TDX Lowell Recon file.xls"
        End If

        ProgressBar1.Value = 80
        Application.DoEvents()
        RD232Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
        RD232Preport.close()
        If Not all_reports Then
            MsgBox("Completed")
            Me.Close()
        End If
       
    End Sub

    Private Sub RD253btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RD253btn.Click
        run_RD253()
    End Sub
    Private Sub run_RD253()
        disable_buttons()
        ProgressBar1.Value = 90
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD253LPreport = New RD253LP
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(startDateDTP.Value)
        SetCurrentValuesForParameterField1(RD253LPreport, myArrayList1)
        myArrayList1.Add(endDateDTP.Value)
        SetCurrentValuesForParameterField2(RD253LPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253LPreport)
        If Not all_reports Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "PDF files |*.pdf"
                .DefaultExt = ".pdf"
                .OverwritePrompt = True
                .FileName = "RD253LP TDX Lowell non-direct Invoice.pdf"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
            Else
                MsgBox("Report not saved")
                Me.Close()
                Exit Sub
            End If
        Else
            filename = filepath & "RD253LP TDX Lowell non-direct Invoice.pdf"
        End If

        ProgressBar1.Value = 95
        Application.DoEvents()
        RD253LPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD253LPreport.close()

        Dim RD253LDPreport = New RD253LDP
        myArrayList1.Add(startDateDTP.Value)
        SetCurrentValuesForParameterField1(RD253LDPreport, myArrayList1)
        myArrayList1.Add(endDateDTP.Value)
        SetCurrentValuesForParameterField2(RD253LDPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253LPreport)
       
        filename = filepath & "RD253LDP TDX Lowell Direct Invoice.pdf"

        ProgressBar1.Value = 60
        Application.DoEvents()
        RD253LDPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD253LDPreport.close()

        If Not all_reports Then
            MsgBox("Completed")
            Me.Close()
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        startDateDTP.Value = DateAdd(DateInterval.Day, -Weekday(Now), Now)
    End Sub

    Private Sub allbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allbtn.Click
        disable_buttons()
        all_reports = True
        run_RD230()
        run_RD229()
        run_RD231()
        run_RD232()
        run_RD253()

        MsgBox("Completed")
        Me.Close()

    End Sub
    Private Sub disable_buttons()
        allbtn.Enabled = False
        rd231btn.Enabled = False
        RD230btn.Enabled = False
        RD253btn.Enabled = False
        rd229btn.Enabled = False
        exitbtn.Enabled = False
        rd232btn.Enabled = False
        startDateDTP.Enabled = False
    End Sub

    Private Sub RD253Dbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub rd229btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd229btn.Click
        run_RD229()
    End Sub

    Private Sub rd232btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd232btn.Click
        run_RD232()
    End Sub
End Class

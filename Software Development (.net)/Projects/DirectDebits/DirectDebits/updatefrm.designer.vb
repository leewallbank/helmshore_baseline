<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class updatefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.quitbtn = New System.Windows.Forms.Button
        Me.updbtn = New System.Windows.Forms.Button
        Me.delbtn = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.upd_phone_tbox = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.upd_recvd_dtp = New System.Windows.Forms.DateTimePicker
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.upd_debtor_tbox = New System.Windows.Forms.TextBox
        Me.upd_name_tbox = New System.Windows.Forms.TextBox
        Me.upd_agent_tbox = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.complete_btn = New System.Windows.Forms.Button
        Me.upd_comp_by_tbox = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.upd_comp_date_tbox = New System.Windows.Forms.TextBox
        Me.upd_onestep_name_tbox = New System.Windows.Forms.TextBox
        Me.dd_id_lbl = New System.Windows.Forms.Label
        Me.checkbtn = New System.Windows.Forms.Button
        Me.checked_date_tbox = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.checked_by_tbox = New System.Windows.Forms.TextBox
        Me.upd_namebtn = New System.Windows.Forms.Button
        Me.upd_client_lbl = New System.Windows.Forms.Label
        Me.upd_freq_cbox = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.upd_amount_tbox = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.upd_est_dtp = New System.Windows.Forms.DateTimePicker
        Me.Label15 = New System.Windows.Forms.Label
        Me.bounced_dtp = New System.Windows.Forms.DateTimePicker
        Me.bounced_cbox = New System.Windows.Forms.CheckBox
        Me.outcome_cbox = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.bounced_outcome_tbox = New System.Windows.Forms.TextBox
        Me.Button1 = New System.Windows.Forms.Button
        Me.bal_lbl = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'quitbtn
        '
        Me.quitbtn.Location = New System.Drawing.Point(61, 410)
        Me.quitbtn.Name = "quitbtn"
        Me.quitbtn.Size = New System.Drawing.Size(152, 23)
        Me.quitbtn.TabIndex = 20
        Me.quitbtn.Text = "Exit without change"
        Me.quitbtn.UseVisualStyleBackColor = True
        '
        'updbtn
        '
        Me.updbtn.Location = New System.Drawing.Point(742, 410)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(127, 23)
        Me.updbtn.TabIndex = 21
        Me.updbtn.Text = "Update and Exit"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'delbtn
        '
        Me.delbtn.Location = New System.Drawing.Point(351, 20)
        Me.delbtn.Name = "delbtn"
        Me.delbtn.Size = New System.Drawing.Size(142, 23)
        Me.delbtn.TabIndex = 12
        Me.delbtn.Text = "Delete this entry"
        Me.delbtn.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(351, 68)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 23
        Me.Label4.Text = "Phone:"
        '
        'upd_phone_tbox
        '
        Me.upd_phone_tbox.Location = New System.Drawing.Point(317, 86)
        Me.upd_phone_tbox.Name = "upd_phone_tbox"
        Me.upd_phone_tbox.Size = New System.Drawing.Size(100, 20)
        Me.upd_phone_tbox.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(136, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(143, 13)
        Me.Label1.TabIndex = 22
        Me.Label1.Text = "Date and Time call received:"
        '
        'upd_recvd_dtp
        '
        Me.upd_recvd_dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.upd_recvd_dtp.Location = New System.Drawing.Point(139, 86)
        Me.upd_recvd_dtp.Name = "upd_recvd_dtp"
        Me.upd_recvd_dtp.Size = New System.Drawing.Size(152, 20)
        Me.upd_recvd_dtp.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(614, 69)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(73, 13)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Debtor Name:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(458, 69)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(53, 13)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "DebtorID:"
        '
        'upd_debtor_tbox
        '
        Me.upd_debtor_tbox.Location = New System.Drawing.Point(437, 85)
        Me.upd_debtor_tbox.Name = "upd_debtor_tbox"
        Me.upd_debtor_tbox.Size = New System.Drawing.Size(100, 20)
        Me.upd_debtor_tbox.TabIndex = 3
        '
        'upd_name_tbox
        '
        Me.upd_name_tbox.Location = New System.Drawing.Point(571, 85)
        Me.upd_name_tbox.Name = "upd_name_tbox"
        Me.upd_name_tbox.Size = New System.Drawing.Size(152, 20)
        Me.upd_name_tbox.TabIndex = 5
        '
        'upd_agent_tbox
        '
        Me.upd_agent_tbox.Location = New System.Drawing.Point(12, 85)
        Me.upd_agent_tbox.Name = "upd_agent_tbox"
        Me.upd_agent_tbox.ReadOnly = True
        Me.upd_agent_tbox.Size = New System.Drawing.Size(100, 20)
        Me.upd_agent_tbox.TabIndex = 0
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(46, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(38, 13)
        Me.Label2.TabIndex = 27
        Me.Label2.Text = "Agent:"
        '
        'complete_btn
        '
        Me.complete_btn.Location = New System.Drawing.Point(90, 283)
        Me.complete_btn.Name = "complete_btn"
        Me.complete_btn.Size = New System.Drawing.Size(100, 23)
        Me.complete_btn.TabIndex = 13
        Me.complete_btn.Text = "Complete DD"
        Me.complete_btn.UseVisualStyleBackColor = True
        '
        'upd_comp_by_tbox
        '
        Me.upd_comp_by_tbox.Location = New System.Drawing.Point(22, 334)
        Me.upd_comp_by_tbox.Name = "upd_comp_by_tbox"
        Me.upd_comp_by_tbox.ReadOnly = True
        Me.upd_comp_by_tbox.Size = New System.Drawing.Size(100, 20)
        Me.upd_comp_by_tbox.TabIndex = 14
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(35, 318)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(75, 13)
        Me.Label8.TabIndex = 32
        Me.Label8.Text = "Completed By:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(172, 318)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(86, 13)
        Me.Label9.TabIndex = 33
        Me.Label9.Text = "Completed Date:"
        '
        'upd_comp_date_tbox
        '
        Me.upd_comp_date_tbox.Location = New System.Drawing.Point(158, 334)
        Me.upd_comp_date_tbox.Name = "upd_comp_date_tbox"
        Me.upd_comp_date_tbox.ReadOnly = True
        Me.upd_comp_date_tbox.Size = New System.Drawing.Size(100, 20)
        Me.upd_comp_date_tbox.TabIndex = 15
        '
        'upd_onestep_name_tbox
        '
        Me.upd_onestep_name_tbox.Location = New System.Drawing.Point(567, 118)
        Me.upd_onestep_name_tbox.Name = "upd_onestep_name_tbox"
        Me.upd_onestep_name_tbox.ReadOnly = True
        Me.upd_onestep_name_tbox.Size = New System.Drawing.Size(152, 20)
        Me.upd_onestep_name_tbox.TabIndex = 40
        '
        'dd_id_lbl
        '
        Me.dd_id_lbl.AutoSize = True
        Me.dd_id_lbl.Location = New System.Drawing.Point(12, 20)
        Me.dd_id_lbl.Name = "dd_id_lbl"
        Me.dd_id_lbl.Size = New System.Drawing.Size(37, 13)
        Me.dd_id_lbl.TabIndex = 43
        Me.dd_id_lbl.Text = "DD ID"
        '
        'checkbtn
        '
        Me.checkbtn.Location = New System.Drawing.Point(354, 273)
        Me.checkbtn.Name = "checkbtn"
        Me.checkbtn.Size = New System.Drawing.Size(100, 23)
        Me.checkbtn.TabIndex = 16
        Me.checkbtn.Text = "Check DD"
        Me.checkbtn.UseVisualStyleBackColor = True
        '
        'checked_date_tbox
        '
        Me.checked_date_tbox.Location = New System.Drawing.Point(438, 333)
        Me.checked_date_tbox.Name = "checked_date_tbox"
        Me.checked_date_tbox.ReadOnly = True
        Me.checked_date_tbox.Size = New System.Drawing.Size(100, 20)
        Me.checked_date_tbox.TabIndex = 18
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(452, 317)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(79, 13)
        Me.Label13.TabIndex = 47
        Me.Label13.Text = "Checked Date:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(314, 317)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(68, 13)
        Me.Label14.TabIndex = 46
        Me.Label14.Text = "Checked By:"
        '
        'checked_by_tbox
        '
        Me.checked_by_tbox.Location = New System.Drawing.Point(302, 333)
        Me.checked_by_tbox.Name = "checked_by_tbox"
        Me.checked_by_tbox.ReadOnly = True
        Me.checked_by_tbox.Size = New System.Drawing.Size(100, 20)
        Me.checked_by_tbox.TabIndex = 17
        '
        'upd_namebtn
        '
        Me.upd_namebtn.Location = New System.Drawing.Point(437, 118)
        Me.upd_namebtn.Name = "upd_namebtn"
        Me.upd_namebtn.Size = New System.Drawing.Size(100, 23)
        Me.upd_namebtn.TabIndex = 4
        Me.upd_namebtn.Text = "Onestep name"
        Me.upd_namebtn.UseVisualStyleBackColor = True
        '
        'upd_client_lbl
        '
        Me.upd_client_lbl.AutoSize = True
        Me.upd_client_lbl.Location = New System.Drawing.Point(568, 20)
        Me.upd_client_lbl.Name = "upd_client_lbl"
        Me.upd_client_lbl.Size = New System.Drawing.Size(32, 13)
        Me.upd_client_lbl.TabIndex = 48
        Me.upd_client_lbl.Text = "client"
        '
        'upd_freq_cbox
        '
        Me.upd_freq_cbox.FormattingEnabled = True
        Me.upd_freq_cbox.Items.AddRange(New Object() {"Weekly", "Fortnightly", "Monthly"})
        Me.upd_freq_cbox.Location = New System.Drawing.Point(12, 197)
        Me.upd_freq_cbox.Name = "upd_freq_cbox"
        Me.upd_freq_cbox.Size = New System.Drawing.Size(100, 21)
        Me.upd_freq_cbox.TabIndex = 7
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(33, 181)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 13)
        Me.Label3.TabIndex = 52
        Me.Label3.Text = "Frequency:"
        '
        'upd_amount_tbox
        '
        Me.upd_amount_tbox.Location = New System.Drawing.Point(760, 86)
        Me.upd_amount_tbox.Name = "upd_amount_tbox"
        Me.upd_amount_tbox.Size = New System.Drawing.Size(100, 20)
        Me.upd_amount_tbox.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(783, 69)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 13)
        Me.Label5.TabIndex = 51
        Me.Label5.Text = "Amount:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(226, 183)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(53, 13)
        Me.Label10.TabIndex = 54
        Me.Label10.Text = "Outcome:"
        '
        'upd_est_dtp
        '
        Me.upd_est_dtp.Location = New System.Drawing.Point(424, 199)
        Me.upd_est_dtp.Name = "upd_est_dtp"
        Me.upd_est_dtp.Size = New System.Drawing.Size(128, 20)
        Me.upd_est_dtp.TabIndex = 10
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(707, 211)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(79, 13)
        Me.Label15.TabIndex = 60
        Me.Label15.Text = "Bounced Date:"
        '
        'bounced_dtp
        '
        Me.bounced_dtp.Location = New System.Drawing.Point(686, 227)
        Me.bounced_dtp.Name = "bounced_dtp"
        Me.bounced_dtp.Size = New System.Drawing.Size(128, 20)
        Me.bounced_dtp.TabIndex = 12
        '
        'bounced_cbox
        '
        Me.bounced_cbox.AutoSize = True
        Me.bounced_cbox.Location = New System.Drawing.Point(710, 179)
        Me.bounced_cbox.Name = "bounced_cbox"
        Me.bounced_cbox.Size = New System.Drawing.Size(75, 17)
        Me.bounced_cbox.TabIndex = 11
        Me.bounced_cbox.Text = "Bounced?"
        Me.bounced_cbox.UseVisualStyleBackColor = True
        '
        'outcome_cbox
        '
        Me.outcome_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.outcome_cbox.FormattingEnabled = True
        Me.outcome_cbox.Items.AddRange(New Object() {"DD Set", "DD Not Set", "Answer Phone Message left"})
        Me.outcome_cbox.Location = New System.Drawing.Point(158, 199)
        Me.outcome_cbox.Name = "outcome_cbox"
        Me.outcome_cbox.Size = New System.Drawing.Size(194, 21)
        Me.outcome_cbox.TabIndex = 8
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(687, 273)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(99, 13)
        Me.Label12.TabIndex = 62
        Me.Label12.Text = "Bounced Outcome:"
        '
        'bounced_outcome_tbox
        '
        Me.bounced_outcome_tbox.Location = New System.Drawing.Point(617, 289)
        Me.bounced_outcome_tbox.Multiline = True
        Me.bounced_outcome_tbox.Name = "bounced_outcome_tbox"
        Me.bounced_outcome_tbox.Size = New System.Drawing.Size(252, 65)
        Me.bounced_outcome_tbox.TabIndex = 19
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(424, 170)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(144, 23)
        Me.Button1.TabIndex = 9
        Me.Button1.Text = "Re-Calc Completion date"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'bal_lbl
        '
        Me.bal_lbl.AutoSize = True
        Me.bal_lbl.Location = New System.Drawing.Point(568, 47)
        Me.bal_lbl.Name = "bal_lbl"
        Me.bal_lbl.Size = New System.Drawing.Size(45, 13)
        Me.bal_lbl.TabIndex = 63
        Me.bal_lbl.Text = "balance"
        '
        'updatefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(888, 465)
        Me.Controls.Add(Me.bal_lbl)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.bounced_outcome_tbox)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.outcome_cbox)
        Me.Controls.Add(Me.bounced_cbox)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.bounced_dtp)
        Me.Controls.Add(Me.upd_est_dtp)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.upd_freq_cbox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.upd_amount_tbox)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.upd_client_lbl)
        Me.Controls.Add(Me.upd_namebtn)
        Me.Controls.Add(Me.checked_date_tbox)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.checked_by_tbox)
        Me.Controls.Add(Me.checkbtn)
        Me.Controls.Add(Me.dd_id_lbl)
        Me.Controls.Add(Me.upd_onestep_name_tbox)
        Me.Controls.Add(Me.upd_comp_date_tbox)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.upd_comp_by_tbox)
        Me.Controls.Add(Me.complete_btn)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.upd_agent_tbox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.upd_phone_tbox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.upd_recvd_dtp)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.upd_debtor_tbox)
        Me.Controls.Add(Me.upd_name_tbox)
        Me.Controls.Add(Me.delbtn)
        Me.Controls.Add(Me.updbtn)
        Me.Controls.Add(Me.quitbtn)
        Me.Name = "updatefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Update/Delete DDs"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents quitbtn As System.Windows.Forms.Button
    Friend WithEvents updbtn As System.Windows.Forms.Button
    Friend WithEvents delbtn As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents upd_phone_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents upd_recvd_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents upd_debtor_tbox As System.Windows.Forms.TextBox
    Friend WithEvents upd_name_tbox As System.Windows.Forms.TextBox
    Friend WithEvents upd_agent_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents complete_btn As System.Windows.Forms.Button
    Friend WithEvents upd_comp_by_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents upd_comp_date_tbox As System.Windows.Forms.TextBox
    Friend WithEvents upd_onestep_name_tbox As System.Windows.Forms.TextBox
    Friend WithEvents dd_id_lbl As System.Windows.Forms.Label
    Friend WithEvents checkbtn As System.Windows.Forms.Button
    Friend WithEvents checked_date_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents checked_by_tbox As System.Windows.Forms.TextBox
    Friend WithEvents upd_namebtn As System.Windows.Forms.Button
    Friend WithEvents upd_client_lbl As System.Windows.Forms.Label
    Friend WithEvents upd_freq_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents upd_amount_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents upd_est_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents bounced_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents bounced_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents outcome_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents bounced_outcome_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents bal_lbl As System.Windows.Forms.Label
End Class

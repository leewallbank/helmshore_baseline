<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.exitbtn = New System.Windows.Forms.Button
        Me.dd_dg = New System.Windows.Forms.DataGridView
        Me.addbtn = New System.Windows.Forms.Button
        Me.log_lbl = New System.Windows.Forms.Label
        Me.disp_lbl = New System.Windows.Forms.Label
        Me.excelbtn = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.openbtn = New System.Windows.Forms.Button
        Me.uncheckedbtn = New System.Windows.Forms.Button
        Me.allbtn = New System.Windows.Forms.Button
        Me.FeesSQLDataSet1 = New DirectDebits.FeesSQLDataSet
        Me.DirectDebitsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DirectDebitsTableAdapter = New DirectDebits.FeesSQLDataSetTableAdapters.DirectDebitsTableAdapter
        Me.dd_id = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dd_recvd_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dd_agent = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dd_debtorID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dd_debtor_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dd_phone = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dd_outcome = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dd_amount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dd_frequency = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dd_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DirectDebitsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(814, 440)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 5
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'dd_dg
        '
        Me.dd_dg.AllowUserToAddRows = False
        Me.dd_dg.AllowUserToDeleteRows = False
        Me.dd_dg.AllowUserToOrderColumns = True
        Me.dd_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dd_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.dd_id, Me.dd_recvd_date, Me.dd_agent, Me.dd_debtorID, Me.dd_debtor_name, Me.dd_phone, Me.dd_outcome, Me.dd_amount, Me.dd_frequency})
        Me.dd_dg.Location = New System.Drawing.Point(12, 98)
        Me.dd_dg.Name = "dd_dg"
        Me.dd_dg.ReadOnly = True
        Me.dd_dg.Size = New System.Drawing.Size(933, 336)
        Me.dd_dg.TabIndex = 3
        '
        'addbtn
        '
        Me.addbtn.Location = New System.Drawing.Point(188, 31)
        Me.addbtn.Name = "addbtn"
        Me.addbtn.Size = New System.Drawing.Size(113, 23)
        Me.addbtn.TabIndex = 0
        Me.addbtn.Text = "Add new entry"
        Me.addbtn.UseVisualStyleBackColor = True
        '
        'log_lbl
        '
        Me.log_lbl.AutoSize = True
        Me.log_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.log_lbl.Location = New System.Drawing.Point(66, 41)
        Me.log_lbl.Name = "log_lbl"
        Me.log_lbl.Size = New System.Drawing.Size(24, 13)
        Me.log_lbl.TabIndex = 7
        Me.log_lbl.Text = "log"
        '
        'disp_lbl
        '
        Me.disp_lbl.AutoSize = True
        Me.disp_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.disp_lbl.Location = New System.Drawing.Point(147, 82)
        Me.disp_lbl.Name = "disp_lbl"
        Me.disp_lbl.Size = New System.Drawing.Size(45, 13)
        Me.disp_lbl.TabIndex = 8
        Me.disp_lbl.Text = "Label1"
        '
        'excelbtn
        '
        Me.excelbtn.Location = New System.Drawing.Point(776, 32)
        Me.excelbtn.Name = "excelbtn"
        Me.excelbtn.Size = New System.Drawing.Size(89, 21)
        Me.excelbtn.TabIndex = 4
        Me.excelbtn.Text = "Write  to excel"
        Me.excelbtn.UseVisualStyleBackColor = True
        '
        'openbtn
        '
        Me.openbtn.Location = New System.Drawing.Point(345, 31)
        Me.openbtn.Name = "openbtn"
        Me.openbtn.Size = New System.Drawing.Size(75, 23)
        Me.openbtn.TabIndex = 1
        Me.openbtn.Text = "Open DDs"
        Me.openbtn.UseVisualStyleBackColor = True
        '
        'uncheckedbtn
        '
        Me.uncheckedbtn.Location = New System.Drawing.Point(448, 31)
        Me.uncheckedbtn.Name = "uncheckedbtn"
        Me.uncheckedbtn.Size = New System.Drawing.Size(117, 23)
        Me.uncheckedbtn.TabIndex = 2
        Me.uncheckedbtn.Text = "Unchecked DDs"
        Me.uncheckedbtn.UseVisualStyleBackColor = True
        '
        'allbtn
        '
        Me.allbtn.Location = New System.Drawing.Point(607, 31)
        Me.allbtn.Name = "allbtn"
        Me.allbtn.Size = New System.Drawing.Size(75, 23)
        Me.allbtn.TabIndex = 3
        Me.allbtn.Text = "All DDs"
        Me.allbtn.UseVisualStyleBackColor = True
        '
        'FeesSQLDataSet1
        '
        Me.FeesSQLDataSet1.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DirectDebitsBindingSource
        '
        Me.DirectDebitsBindingSource.DataMember = "DirectDebits"
        Me.DirectDebitsBindingSource.DataSource = Me.FeesSQLDataSet1
        '
        'DirectDebitsTableAdapter
        '
        Me.DirectDebitsTableAdapter.ClearBeforeFill = True
        '
        'dd_id
        '
        Me.dd_id.HeaderText = "DD Id"
        Me.dd_id.Name = "dd_id"
        Me.dd_id.ReadOnly = True
        Me.dd_id.Width = 50
        '
        'dd_recvd_date
        '
        Me.dd_recvd_date.HeaderText = "Received DateTime"
        Me.dd_recvd_date.Name = "dd_recvd_date"
        Me.dd_recvd_date.ReadOnly = True
        Me.dd_recvd_date.Width = 120
        '
        'dd_agent
        '
        Me.dd_agent.HeaderText = "Agent"
        Me.dd_agent.Name = "dd_agent"
        Me.dd_agent.ReadOnly = True
        '
        'dd_debtorID
        '
        Me.dd_debtorID.HeaderText = "DebtorID"
        Me.dd_debtorID.Name = "dd_debtorID"
        Me.dd_debtorID.ReadOnly = True
        '
        'dd_debtor_name
        '
        Me.dd_debtor_name.HeaderText = "Debtor Name"
        Me.dd_debtor_name.Name = "dd_debtor_name"
        Me.dd_debtor_name.ReadOnly = True
        '
        'dd_phone
        '
        Me.dd_phone.HeaderText = "Phone No"
        Me.dd_phone.Name = "dd_phone"
        Me.dd_phone.ReadOnly = True
        '
        'dd_outcome
        '
        Me.dd_outcome.HeaderText = "Outcome"
        Me.dd_outcome.Name = "dd_outcome"
        Me.dd_outcome.ReadOnly = True
        '
        'dd_amount
        '
        Me.dd_amount.HeaderText = "Amount"
        Me.dd_amount.Name = "dd_amount"
        Me.dd_amount.ReadOnly = True
        '
        'dd_frequency
        '
        Me.dd_frequency.HeaderText = "Frequency"
        Me.dd_frequency.Name = "dd_frequency"
        Me.dd_frequency.ReadOnly = True
        '
        'Mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(957, 488)
        Me.Controls.Add(Me.allbtn)
        Me.Controls.Add(Me.uncheckedbtn)
        Me.Controls.Add(Me.openbtn)
        Me.Controls.Add(Me.excelbtn)
        Me.Controls.Add(Me.disp_lbl)
        Me.Controls.Add(Me.log_lbl)
        Me.Controls.Add(Me.addbtn)
        Me.Controls.Add(Me.dd_dg)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "Mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Direct Debits"
        CType(Me.dd_dg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DirectDebitsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents dd_dg As System.Windows.Forms.DataGridView
    Friend WithEvents addbtn As System.Windows.Forms.Button
    Friend WithEvents FeesSQLDataSet As DirectDebits.FeesSQLDataSet
    Friend WithEvents log_lbl As System.Windows.Forms.Label
    Friend WithEvents disp_lbl As System.Windows.Forms.Label
    Friend WithEvents excelbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents FeesSQLDataSet1 As DirectDebits.FeesSQLDataSet
    Friend WithEvents openbtn As System.Windows.Forms.Button
    Friend WithEvents uncheckedbtn As System.Windows.Forms.Button
    Friend WithEvents allbtn As System.Windows.Forms.Button
    Friend WithEvents DirectDebitsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DirectDebitsTableAdapter As DirectDebits.FeesSQLDataSetTableAdapters.DirectDebitsTableAdapter
    Friend WithEvents dd_id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dd_recvd_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dd_agent As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dd_debtorID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dd_debtor_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dd_phone As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dd_outcome As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dd_amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dd_frequency As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

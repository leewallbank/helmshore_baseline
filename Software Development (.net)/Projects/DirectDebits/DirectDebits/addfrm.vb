Public Class addfrm
    Private Sub addfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bal_lbl.Text = ""
        client_lbl.Text = ""
        namebtn.Enabled = False
        recvd_dtp.Focus()
        agent_lbl.Text = "Calls taken by  " & log_user
        recvd_dtp.CustomFormat = "dd MM yyyy HH:mm"
        name_tbox.Text = ""
        debtor_tbox.Text = ""
        phone_tbox.Text = ""
        amount_tbox.Text = ""
        freq_cbox.SelectedIndex = -1
        onestep_name_tbox.Text = ""
        est_dtp.Visible = False
        param2 = "select login_name from Bailiff where agent_type = 'P'" & _
        " and status = 'O' and name_fore is not null order by login_name"
        Dim bail_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to read agents from onestep")
            Exit Sub
        End If

    End Sub

    Private Sub quitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitbtn.Click
        Me.Close()
    End Sub

    Private Sub updbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updbtn.Click
        If recvd_dtp.Value > Now Then
            MsgBox("Time can't be in the future")
            Exit Sub
        End If
        If recvd_dtp.Value < DateAdd(DateInterval.Day, -1, Now) Then
            MsgBox("Date can't be before yesterday")
            Exit Sub
        End If
        If phone_tbox.Text.Length < 5 Then
            MsgBox("Invalid phone number entered")
            Exit Sub
        End If

        Dim debtor As Integer
        Try
            debtor = debtor_tbox.Text
        Catch ex As Exception
            MsgBox("debtorID should be a valid case number on onestep")
            Exit Sub
        End Try
        If name_tbox.Text.Length < 5 Then
            MsgBox("Debtor name should be entered")
            Exit Sub
        End If
        Dim amount As Decimal
        If Trim(amount_tbox.Text).Length = 0 Then
            amount = 0
        Else
            If Not IsNumeric(amount_tbox.Text) Then
                MsgBox("Amount should be numeric")
                Exit Sub
            Else
                amount = amount_tbox.Text
            End If
        End If
        If amount < 0 Then
            MsgBox("Amount should not be negative")
            Exit Sub
        End If

        param2 = "select _rowid from Debtor where _rowid = " & debtor
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("DebtorID does not exist on onestep")
            Exit Sub
        End If
        Try
            Mainfrm.DirectDebitsTableAdapter.InsertQuery(log_user, Now, recvd_dtp.Value, debtor_tbox.Text, log_user, name_tbox.Text, phone_tbox.Text, "", "", null_date, "", null_date, "", Format(amount, "f"), freq_cbox.Text, Format(est_dtp.Value, "dd/MM/yyyy"), "", null_date, "", "", null_date)
            populate_dd_grid()
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        Me.Close()
    End Sub

    Private Sub debtor_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles debtor_tbox.TextChanged

    End Sub

    Private Sub debtor_tbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles debtor_tbox.Validated
        Dim debtor As Integer
        Try
            debtor = debtor_tbox.Text
        Catch ex As Exception
            MsgBox("debtorID should be a valid case number on onestep")
            Exit Sub
        End Try
        param2 = "select _rowid, name_fore, name_sur, name2, clientSchemeID, debt_balance from Debtor where _rowid = " & debtor
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("DebtorID does not exist on onestep")
            Exit Sub
        End If
        Dim onestep_name As String
        Try
            onestep_name = debtor_dataset.Tables(0).Rows(0).Item(1)
        Catch ex As Exception
            onestep_name = ""
        End Try
        Try
            onestep_name = Trim(onestep_name & " " & debtor_dataset.Tables(0).Rows(0).Item(2))
        Catch ex As Exception

        End Try
        client_lbl.Text = get_cl_name(debtor_dataset.Tables(0).Rows(0).Item(4))
        bal_lbl.Text = "Balance: " & Format(debtor_dataset.Tables(0).Rows(0).Item(5), "c")
        onestep_name_tbox.Text = onestep_name
        namebtn.Enabled = True
    End Sub


    Private Sub name_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles name_tbox.TextChanged

    End Sub

    Private Sub namebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles namebtn.Click
        name_tbox.Text = onestep_name_tbox.Text
    End Sub

    Private Sub freq_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles freq_cbox.SelectedIndexChanged

    End Sub

    Private Sub freq_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles freq_cbox.Validated
        Dim est_comp_date As Date = calc_est_comp_date(amount_tbox.Text, freq_cbox.Text, debtor_tbox.Text)
        If est_comp_date <> null_date Then
            est_dtp.Value = est_comp_date
            est_dtp.Visible = True
        End If
    End Sub

    Private Sub estbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles estbtn.Click
        Try
            est_dtp.Value = calc_est_comp_date(amount_tbox.Text, freq_cbox.Text, debtor_tbox.Text)
        Catch ex As Exception
            Exit Sub
        End Try
        est_dtp.Visible = True
    End Sub
End Class
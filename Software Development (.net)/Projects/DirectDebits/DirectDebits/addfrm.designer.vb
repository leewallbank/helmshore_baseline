<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class addfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.quitbtn = New System.Windows.Forms.Button
        Me.updbtn = New System.Windows.Forms.Button
        Me.name_tbox = New System.Windows.Forms.TextBox
        Me.debtor_tbox = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.agent_lbl = New System.Windows.Forms.Label
        Me.recvd_dtp = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.phone_tbox = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.onestep_name_tbox = New System.Windows.Forms.TextBox
        Me.namebtn = New System.Windows.Forms.Button
        Me.amount_tbox = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.freq_cbox = New System.Windows.Forms.ComboBox
        Me.client_lbl = New System.Windows.Forms.Label
        Me.est_dtp = New System.Windows.Forms.DateTimePicker
        Me.estbtn = New System.Windows.Forms.Button
        Me.bal_lbl = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'quitbtn
        '
        Me.quitbtn.Location = New System.Drawing.Point(41, 282)
        Me.quitbtn.Name = "quitbtn"
        Me.quitbtn.Size = New System.Drawing.Size(135, 23)
        Me.quitbtn.TabIndex = 9
        Me.quitbtn.Text = "Exit without Saving"
        Me.quitbtn.UseVisualStyleBackColor = True
        '
        'updbtn
        '
        Me.updbtn.Location = New System.Drawing.Point(827, 282)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(100, 23)
        Me.updbtn.TabIndex = 10
        Me.updbtn.Text = "Save and Exit"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'name_tbox
        '
        Me.name_tbox.Location = New System.Drawing.Point(464, 106)
        Me.name_tbox.Name = "name_tbox"
        Me.name_tbox.Size = New System.Drawing.Size(152, 20)
        Me.name_tbox.TabIndex = 4
        '
        'debtor_tbox
        '
        Me.debtor_tbox.Location = New System.Drawing.Point(336, 106)
        Me.debtor_tbox.Name = "debtor_tbox"
        Me.debtor_tbox.Size = New System.Drawing.Size(100, 20)
        Me.debtor_tbox.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(351, 90)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "DebtorID:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(497, 90)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Debtor Name:"
        '
        'agent_lbl
        '
        Me.agent_lbl.AutoSize = True
        Me.agent_lbl.Location = New System.Drawing.Point(188, 18)
        Me.agent_lbl.Name = "agent_lbl"
        Me.agent_lbl.Size = New System.Drawing.Size(34, 13)
        Me.agent_lbl.TabIndex = 8
        Me.agent_lbl.Text = "agent"
        '
        'recvd_dtp
        '
        Me.recvd_dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.recvd_dtp.Location = New System.Drawing.Point(30, 107)
        Me.recvd_dtp.Name = "recvd_dtp"
        Me.recvd_dtp.Size = New System.Drawing.Size(152, 20)
        Me.recvd_dtp.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(39, 90)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(143, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "Date and Time call received:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(243, 89)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 13)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "Phone:"
        '
        'phone_tbox
        '
        Me.phone_tbox.Location = New System.Drawing.Point(211, 106)
        Me.phone_tbox.Name = "phone_tbox"
        Me.phone_tbox.Size = New System.Drawing.Size(100, 20)
        Me.phone_tbox.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(666, 89)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(46, 13)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "Amount:"
        '
        'onestep_name_tbox
        '
        Me.onestep_name_tbox.Location = New System.Drawing.Point(464, 132)
        Me.onestep_name_tbox.Name = "onestep_name_tbox"
        Me.onestep_name_tbox.ReadOnly = True
        Me.onestep_name_tbox.Size = New System.Drawing.Size(152, 20)
        Me.onestep_name_tbox.TabIndex = 17
        '
        'namebtn
        '
        Me.namebtn.Location = New System.Drawing.Point(336, 132)
        Me.namebtn.Name = "namebtn"
        Me.namebtn.Size = New System.Drawing.Size(93, 23)
        Me.namebtn.TabIndex = 3
        Me.namebtn.Text = "Onestep name"
        Me.namebtn.UseVisualStyleBackColor = True
        '
        'amount_tbox
        '
        Me.amount_tbox.Location = New System.Drawing.Point(643, 106)
        Me.amount_tbox.Name = "amount_tbox"
        Me.amount_tbox.Size = New System.Drawing.Size(100, 20)
        Me.amount_tbox.TabIndex = 5
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(795, 90)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(60, 13)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "Frequency:"
        '
        'freq_cbox
        '
        Me.freq_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.freq_cbox.FormattingEnabled = True
        Me.freq_cbox.Items.AddRange(New Object() {"Weekly", "Fortnightly", "Monthly"})
        Me.freq_cbox.Location = New System.Drawing.Point(783, 107)
        Me.freq_cbox.Name = "freq_cbox"
        Me.freq_cbox.Size = New System.Drawing.Size(121, 21)
        Me.freq_cbox.TabIndex = 6
        '
        'client_lbl
        '
        Me.client_lbl.AutoSize = True
        Me.client_lbl.Location = New System.Drawing.Point(461, 18)
        Me.client_lbl.Name = "client_lbl"
        Me.client_lbl.Size = New System.Drawing.Size(32, 13)
        Me.client_lbl.TabIndex = 20
        Me.client_lbl.Text = "client"
        '
        'est_dtp
        '
        Me.est_dtp.Location = New System.Drawing.Point(25, 177)
        Me.est_dtp.Name = "est_dtp"
        Me.est_dtp.Size = New System.Drawing.Size(157, 20)
        Me.est_dtp.TabIndex = 8
        '
        'estbtn
        '
        Me.estbtn.Location = New System.Drawing.Point(30, 148)
        Me.estbtn.Name = "estbtn"
        Me.estbtn.Size = New System.Drawing.Size(152, 23)
        Me.estbtn.TabIndex = 7
        Me.estbtn.Text = "Calc Est Completion date"
        Me.estbtn.UseVisualStyleBackColor = True
        '
        'bal_lbl
        '
        Me.bal_lbl.AutoSize = True
        Me.bal_lbl.Location = New System.Drawing.Point(461, 49)
        Me.bal_lbl.Name = "bal_lbl"
        Me.bal_lbl.Size = New System.Drawing.Size(21, 13)
        Me.bal_lbl.TabIndex = 23
        Me.bal_lbl.Text = "bal"
        '
        'addfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(939, 338)
        Me.Controls.Add(Me.bal_lbl)
        Me.Controls.Add(Me.estbtn)
        Me.Controls.Add(Me.est_dtp)
        Me.Controls.Add(Me.freq_cbox)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.amount_tbox)
        Me.Controls.Add(Me.client_lbl)
        Me.Controls.Add(Me.namebtn)
        Me.Controls.Add(Me.onestep_name_tbox)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.phone_tbox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.recvd_dtp)
        Me.Controls.Add(Me.agent_lbl)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.debtor_tbox)
        Me.Controls.Add(Me.name_tbox)
        Me.Controls.Add(Me.updbtn)
        Me.Controls.Add(Me.quitbtn)
        Me.Name = "addfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Add Call"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents quitbtn As System.Windows.Forms.Button
    Friend WithEvents updbtn As System.Windows.Forms.Button
    Friend WithEvents name_tbox As System.Windows.Forms.TextBox
    Friend WithEvents debtor_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents agent_lbl As System.Windows.Forms.Label
    Friend WithEvents recvd_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents phone_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents onestep_name_tbox As System.Windows.Forms.TextBox
    Friend WithEvents namebtn As System.Windows.Forms.Button
    Friend WithEvents amount_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents freq_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents client_lbl As System.Windows.Forms.Label
    Friend WithEvents est_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents estbtn As System.Windows.Forms.Button
    Friend WithEvents bal_lbl As System.Windows.Forms.Label
End Class

Public Class updatefrm

    Private Sub updatefrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        upd_namebtn.Enabled = False
        bal_lbl.Text = ""
        check_call = False
        dd_id_lbl.Text = "Call ID:" & selected_dd_id
        upd_recvd_dtp.CustomFormat = "dd MM yyyy HH:mm"
        upd_recvd_dtp.Text = Mainfrm.dd_dg.Rows(upd_row).Cells(1).Value
        upd_agent_tbox.Text = Mainfrm.dd_dg.Rows(upd_row).Cells(2).Value
        upd_debtor_tbox.Text = Mainfrm.dd_dg.Rows(upd_row).Cells(3).Value
        param2 = "select clientSchemeID, debt_balance from Debtor where _rowid = " & upd_debtor_tbox.Text
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("DebtorID does not exist on onestep")
            Exit Sub
        End If
        bal_lbl.Text = "Balance: " & Format(debtor_dataset.Tables(0).Rows(0).Item(1), "c")
        upd_client_lbl.Text = get_cl_name(debtor_dataset.Tables(0).Rows(0).Item(0))
        upd_name_tbox.Text = Mainfrm.dd_dg.Rows(upd_row).Cells(4).Value
        upd_phone_tbox.Text = Mainfrm.dd_dg.Rows(upd_row).Cells(5).Value
        upd_amount_tbox.Text = Mainfrm.dd_dg.Rows(upd_row).Cells(7).Value
        upd_freq_cbox.Text = Mainfrm.dd_dg.Rows(upd_row).Cells(8).Value
        outcome_cbox.Text = Mainfrm.dd_dg.Rows(upd_row).Cells(6).Value
        Mainfrm.DirectDebitsTableAdapter.FillBy3(Mainfrm.FeesSQLDataSet1.DirectDebits, selected_dd_id)
        Dim pif_date As Date = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(0).Item(16)
        upd_est_dtp.Value = pif_date
        If pif_date <> null_date Then
            upd_est_dtp.Visible = True
        Else
            upd_est_dtp.Visible = False
        End If
        
        Dim bounced_date As Date = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(0).Item(18)
        bounced_outcome_tbox.Text = ""
        If bounced_date = null_date Then
            bounced_cbox.Checked = False
            bounced_dtp.Visible = False
            bounced_outcome_tbox.Visible = False
        Else
            bounced_cbox.Checked = True
            bounced_dtp.Visible = True
            bounced_dtp.Value = bounced_date
            bounced_outcome_tbox.Visible = True
            Try
                bounced_outcome_tbox.Text = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(0).Item(19)
            Catch ex As Exception
                bounced_outcome_tbox.Text = ""
            End Try

        End If
        Dim completed_by As String = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(0).Item(11)
        Dim checked_date As Date = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(0).Item(12)
        If checked_date = null_date Then
            check_call = False
            checked_by_tbox.Text = ""
            checked_date_tbox.Text = ""
        Else
            check_call = True
            checked_by_tbox.Text = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(0).Item(13)
            checked_date_tbox.Text = Format(Now, "dd/MM/yyyy")
        End If
        If completed_by <> "" Then
            complete_call = True
            upd_recvd_dtp.Enabled = False
            upd_agent_tbox.Enabled = False
            upd_name_tbox.Enabled = False
            upd_debtor_tbox.Enabled = False
            upd_phone_tbox.Enabled = False
            upd_comp_by_tbox.Text = completed_by
            upd_comp_date_tbox.Text = Format(Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(0).Item(10), "dd/MM/yyyy")

        Else
            upd_recvd_dtp.Enabled = True
            upd_agent_tbox.Enabled = True
            upd_name_tbox.Enabled = True
            upd_debtor_tbox.Enabled = True
            upd_phone_tbox.Enabled = True
            complete_call = False
            upd_comp_by_tbox.Text = ""
            upd_comp_date_tbox.Text = ""
        End If

        upd_phone_tbox.Focus()
    End Sub

    Private Sub quitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitbtn.Click
        Me.Close()
    End Sub

    Private Sub updbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updbtn.Click
        Dim debtor As Integer
        Try
            debtor = upd_debtor_tbox.Text
        Catch ex As Exception
            MsgBox("debtorID should be a valid case number on onestep")
            Exit Sub
        End Try
        param2 = "select _rowid from Debtor where _rowid = " & debtor
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("DebtorID does not exist on onestep")
            Exit Sub
        End If
        If upd_name_tbox.Text.Length < 2 Then
            MsgBox("Debtor name should not be blank")
            Exit Sub
        End If
        Dim amount As Decimal
        If Trim(upd_amount_tbox.Text).Length = 0 Then
            amount = 0
        Else
            If Not IsNumeric(upd_amount_tbox.Text) Then
                MsgBox("Amount should be numeric")
                Exit Sub
            Else
                amount = upd_amount_tbox.Text
            End If
        End If
        If amount < 0 Then
            MsgBox("Amount should not be negative")
            Exit Sub
        End If
        Dim pif_date As Date = null_date
        Dim bounced_date As Date = null_date
        If bounced_cbox.Checked Then
            bounced_date = bounced_dtp.Value
            If bounced_date < Format(upd_recvd_dtp.Value, "yyyy-MM-dd") Then
                MsgBox("Bounced date should not be before DD created date")
                Exit Sub
            Else
                If bounced_date > Now Then
                    MsgBox("Bounced date should not be in the future")
                    Exit Sub
                End If
            End If
        End If
        If complete_call Then

        End If
        Try
            Mainfrm.DirectDebitsTableAdapter.UpdateQuery(upd_recvd_dtp.Value, upd_debtor_tbox.Text, _
            upd_agent_tbox.Text, upd_name_tbox.Text, upd_phone_tbox.Text, outcome_cbox.Text, _
            Format(amount, "f"), upd_freq_cbox.Text, Format(upd_est_dtp.Value, "dd/MM/yyyy"), "", _
            Format(bounced_date, "dd/MM/yyyy"), bounced_outcome_tbox.Text, selected_dd_id)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        If complete_call Then

            Try
                Mainfrm.DirectDebitsTableAdapter.UpdateQuery1(upd_comp_date_tbox.Text, upd_comp_by_tbox.Text, selected_dd_id)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
        End If
        If check_call Then
            Try
                Mainfrm.DirectDebitsTableAdapter.UpdateQuery3(checked_date_tbox.Text, checked_by_tbox.Text, selected_dd_id)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
        End If
        populate_dd_grid()
        Me.Close()
    End Sub

    Private Sub delbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles delbtn.Click
        Try
            Mainfrm.DirectDebitsTableAdapter.UpdateQuery2(log_user, Now, selected_dd_id)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        populate_dd_grid()
        Me.Close()
    End Sub

    Private Sub updphone_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_phone_tbox.TextChanged

    End Sub

    Private Sub complete_btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles complete_btn.Click
        If complete_call Then
            MsgBox("This DD has already been completed")
            Exit Sub
        End If
        complete_call = True
        upd_comp_by_tbox.Text = log_user
        upd_comp_date_tbox.Text = Format(Now, "dd/MM/yyyy")
    End Sub

    Private Sub upd_debtor_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_debtor_tbox.TextChanged

    End Sub

    Private Sub upd_debtor_tbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles upd_debtor_tbox.Validated
        Dim debtor As Integer
        Try
            debtor = upd_debtor_tbox.Text
        Catch ex As Exception
            MsgBox("debtorID should be a valid case number on onestep")
            Exit Sub
        End Try
        param2 = "select _rowid, name_fore, name_sur, name2, clientSchemeID from Debtor where _rowid = " & debtor
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("DebtorID does not exist on onestep")
            Exit Sub
        End If
        Dim onestep_name As String
        Try
            onestep_name = debtor_dataset.Tables(0).Rows(0).Item(1)
        Catch ex As Exception
            onestep_name = ""
        End Try
        Try
            onestep_name = Trim(onestep_name & " " & debtor_dataset.Tables(0).Rows(0).Item(2))
        Catch ex As Exception

        End Try
        upd_namebtn.Enabled = True
        upd_onestep_name_tbox.Text = onestep_name
        upd_client_lbl.Text = get_cl_name(debtor_dataset.Tables(0).Rows(0).Item(4))
    End Sub

    Private Sub checkbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles checkbtn.Click
        If complete_call = False Then
            MsgBox("Need to complete DD before checking")
            Exit Sub
        End If
        If check_call Then
            MsgBox("This DD has already been checked")
            Exit Sub
        End If
        If log_user = upd_comp_by_tbox.Text Then
            MsgBox("Can't check DDs you have completed yourself")
            Exit Sub
        End If
        check_call = True
        checked_by_tbox.Text = log_user
        checked_date_tbox.Text = Format(Now, "dd/MM/yyyy")
    End Sub

    Private Sub namebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_namebtn.Click
        upd_name_tbox.Text = upd_onestep_name_tbox.Text
    End Sub

    Private Sub bounced_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bounced_cbox.CheckedChanged

    End Sub

    Private Sub bounced_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles bounced_cbox.Validated
        If bounced_cbox.Checked Then
            bounced_dtp.Visible = True
            bounced_dtp.Focus()
            bounced_outcome_tbox.Visible = True
        Else
            bounced_dtp.Visible = False
            bounced_outcome_tbox.Visible = False
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Try
            upd_est_dtp.Value = calc_est_comp_date(upd_amount_tbox.Text, upd_freq_cbox.Text, upd_debtor_tbox.Text)
        Catch ex As Exception
            Exit Sub
        End Try
        upd_est_dtp.Visible = True

    End Sub

    Private Sub freq_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_freq_cbox.SelectedIndexChanged

    End Sub
End Class
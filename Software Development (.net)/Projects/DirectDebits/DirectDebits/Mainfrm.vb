Public Class Mainfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       
        mode = "O"
        disp_lbl.Text = "All Open Direct Debits. Double_click an entry to update/delete"
        log_user = My.User.Name
        Dim slash_idx As Integer = InStr(log_user, "\")
        If slash_idx > 0 Then
            log_user = Microsoft.VisualBasic.Right(log_user, log_user.length - slash_idx)
        End If
        log_lbl.Text = log_user
        populate_dd_grid()
    End Sub



    Private Sub addbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addbtn.Click
        addfrm.ShowDialog()
    End Sub

    Private Sub pay_dg_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dd_dg.CellContentClick

    End Sub

    Private Sub pay_dg_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dd_dg.CellDoubleClick
        selected_dd_id = dd_dg.Rows(e.RowIndex).Cells(0).Value
        selected_transfer_to = dd_dg.Rows(e.RowIndex).Cells(2).Value
        upd_row = e.RowIndex
        updatefrm.ShowDialog()
    End Sub

    Private Sub excelbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles excelbtn.Click

        Dim idx As Integer = 0
        Dim file As String = "DD Id,Created Date,Created By,Agent,DebtorID, debtor name,Client,Phone,Outcome," & _
        "Amount,Frequency,Completed Date,Completed By,Checked date, Checked by,Est Completion Date,Bounced Date,Bounced Outcome" & vbNewLine
        Dim row As DataGridViewRow
        For Each row In dd_dg.Rows
            Dim dd_id As Integer = dd_dg.Rows(idx).Cells(0).Value
            DirectDebitsTableAdapter.FillBy3(FeesSQLDataSet1.DirectDebits, dd_id)
            file = file & dd_id & ","
            file = file & dd_dg.Rows(idx).Cells(1).Value & ","  'created date
            file = file & Me.FeesSQLDataSet1.DirectDebits.Rows(0).Item(1) & "," 'created by
            file = file & dd_dg.Rows(idx).Cells(2).Value & "," 'agent
            file = file & dd_dg.Rows(idx).Cells(3).Value & "," 'debtorID
            param2 = "select clientSchemeID from Debtor where _rowid = " & dd_dg.Rows(idx).Cells(3).Value
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            Dim cl_name As String
            If no_of_rows = 0 Then
                cl_name = ""
            Else
                cl_name = get_cl_name(debtor_dataset.Tables(0).Rows(0).Item(0))
            End If
            Dim name As String = dd_dg.Rows(idx).Cells(4).Value & "," 'debtor name
            name = Replace(name, ",", " ")
            file = file & name & ","
            cl_name = Replace(cl_name, ",", " ")
            file = file & cl_name & ","
            file = file & dd_dg.Rows(idx).Cells(5).Value & "," 'phone
            file = file & dd_dg.Rows(idx).Cells(6).Value & "," 'outcome
            file = file & dd_dg.Rows(idx).Cells(7).Value & "," 'amount
            file = file & dd_dg.Rows(idx).Cells(8).Value & "," 'frequency
            If Me.FeesSQLDataSet1.DirectDebits.Rows(0).Item(10) = null_date Then
                file = file & ",,"
            Else
                file = file & Format(Me.FeesSQLDataSet1.DirectDebits.Rows(0).Item(10), "dd/MM/yyyy") & _
                "," & Me.FeesSQLDataSet1.DirectDebits.Rows(0).Item(11) & "," 'comp date comp by
            End If
            If Me.FeesSQLDataSet1.DirectDebits.Rows(0).Item(12) = null_date Then
                file = file & ",,"
            Else
                file = file & Format(Me.FeesSQLDataSet1.DirectDebits.Rows(0).Item(12), "dd/MM/yyyy") & _
                "," & Me.FeesSQLDataSet1.DirectDebits.Rows(0).Item(13) & "," 'checked date checked by
            End If
            If Me.FeesSQLDataSet1.DirectDebits.Rows(0).Item(16) = null_date Then 'est completion date
                file = file & ","
            Else
                file = file & Format(Me.FeesSQLDataSet1.DirectDebits.Rows(0).Item(16), "dd/MM/yyyy") & ","
            End If
            If Me.FeesSQLDataSet1.DirectDebits.Rows(0).Item(18) = null_date Then 'bounced date
                file = file & ",,"
            Else
                file = file & Format(Me.FeesSQLDataSet1.DirectDebits.Rows(0).Item(18), "dd/MM/yyyy") & ","
                Dim bounced_outcome As String = ""
                Try
                    bounced_outcome = Me.FeesSQLDataSet1.DirectDebits.Rows(0).Item(19)  ' bounced outcome
                Catch ex As Exception

                End Try
                bounced_outcome = remove_chars(bounced_outcome)
                file = file & bounced_outcome & ","
            End If
            file = file & vbNewLine
            idx += 1
        Next
        If idx = 0 Then
            MessageBox.Show("There are no calls")
            Exit Sub
        End If
        With SaveFileDialog1
            .Title = "Save to excel"
            .Filter = "excel|*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "direct_debits_" & Format(Now, "dd.MM.yyyy") & ".xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
        End If
    End Sub
    Function remove_chars(ByVal text As String) As String
        Dim idx As Integer
        Dim new_text As String = ""
        For idx = 1 To text.Length
            If Mid(text, idx, 1) = Chr(10) Or Mid(text, idx, 1) = Chr(9) Or _
            Mid(text, idx, 1) = Chr(13) Or Mid(text, idx, 1) = "|" Then
                new_text = new_text & " "
            Else
                new_text = new_text & Mid(text, idx, 1)
            End If
        Next
        Return new_text
    End Function

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        mode = "O"
        disp_lbl.Text = "All Open calls. Double_click an entry to update/delete"
        populate_dd_grid()
    End Sub

    Private Sub uncheckedbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles uncheckedbtn.Click
        disp_lbl.Text = "All Unchecked calls. Double_click an entry to update/delete"
        mode = "U"
        populate_dd_grid()
    End Sub

    Private Sub allbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allbtn.Click
        disp_lbl.Text = "All calls. Double_click an entry to update/delete"
        mode = "A"
        populate_dd_grid()
    End Sub

End Class
Module Module1
    Public log_user, selected_dd_id, selected_transfer_to, mode As String
    Public conn_open, complete_call, check_call As Boolean
    Public upd_row As Integer
    Public null_date As Date = CDate("1900 1 1")
    Public Sub populate_dd_grid()
        If mode = "O" Then
            Mainfrm.DirectDebitsTableAdapter.FillBy(Mainfrm.FeesSQLDataSet1.DirectDebits, Format(null_date, "yyyy MM dd"), Format(null_date, "yyyy MM dd"))
        ElseIf mode = "U" Then
            Mainfrm.DirectDebitsTableAdapter.FillBy1(Mainfrm.FeesSQLDataSet1.DirectDebits, Format(null_date, "yyyy MM dd"), Format(null_date, "yyyy MM dd"))
        Else
            Mainfrm.DirectDebitsTableAdapter.FillBy2(Mainfrm.FeesSQLDataSet1.DirectDebits, Format(null_date, "yyyy MM dd"))
        End If
        Mainfrm.dd_dg.Rows.Clear()
        Dim idx As Integer
        For idx = 0 To Mainfrm.FeesSQLDataSet1.DirectDebits.Rows.Count - 1
            Dim dd_id As Integer = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(idx).Item(0)
            Dim recvd_date As Date = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(idx).Item(3)
            Dim debtorID As Integer = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(idx).Item(4)
            Dim agent As String = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(idx).Item(5)
            Dim name As String = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(idx).Item(6)
            Dim phone As String = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(idx).Item(7)
            Dim amount As Decimal = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(idx).Item(14)
            Dim amt_str As String = ""
            If amount > 0 Then
                amt_str = Format(amount, "C")
            End If
            Dim outcome = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(idx).Item(8)
            Dim frequency As String = Mainfrm.FeesSQLDataSet1.DirectDebits.Rows(idx).Item(15)
            Mainfrm.dd_dg.Rows.Add(dd_id, Format(recvd_date, "dd/MM/yyyy HH:mm"), agent, debtorID, name, phone, outcome, amt_str, frequency)
        Next
    End Sub
    Public Function calc_est_comp_date(ByVal amt As Decimal, ByVal freq As String, ByVal debtor As Integer) As Date
        Dim est_date As Date
        Dim days As Integer
        param2 = "select  debt_balance from Debtor where _rowid = " & debtor
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("DebtorID does not exist on onestep")
            Exit Function
        End If
        Dim bal = debtor_dataset.Tables(0).Rows(0).Item(0)
        Select Case freq
            Case "Weekly"
                days = (bal / amt) * 7

            Case "Fortnightly"
                days = (bal / amt) * 14

            Case "Monthly"
                days = (bal / amt) * 30

        End Select
        est_date = DateAdd(DateInterval.Day, days, Now)
        Return (est_date)
    End Function
    Public Function get_cl_name(ByVal csID As Integer) As String

        param2 = "select clientID from clientScheme where _rowid = " & csID
        Dim cs_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to read client scheme for cs_id = " & csID)
            Return ("")
        End If
        param2 = "select name from client where _rowid = " & cs_dataset.Tables(0).Rows(0).Item(0)
        Dim cl_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to read client table")
            Return ("")
        End If
        Return (cl_dataset.Tables(0).Rows(0).Item(0))

    End Function
End Module

Imports System.IO
Public Class Form1

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        override_tbox.Text = "0.00"
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        runbtn.Enabled = False
        exitbtn.Enabled = False
        Dim filename As String
        'RD253V2
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD253V2report = New RD253V2
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(remit_dtp.Value)
        SetCurrentValuesForParameterField1(RD253V2report, myArrayList1)
        myArrayList1.Add(override_tbox.Text)
        SetCurrentValuesForParameterField2(RD253V2report, myArrayList1)
        myArrayList1.Add(invref_tbox.Text)
        SetCurrentValuesForParameterField3(RD253V2report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253V2report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RD253V2 DVLA Invoice.pdf"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = SaveFileDialog1.FileName
            RD253V2report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
            RD253V2report.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
        Me.Close()
    End Sub
End Class
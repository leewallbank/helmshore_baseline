<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.run20btn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.otherbtn = New System.Windows.Forms.Button
        Me.ctaxbtn = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'run20btn
        '
        Me.run20btn.Location = New System.Drawing.Point(66, 55)
        Me.run20btn.Name = "run20btn"
        Me.run20btn.Size = New System.Drawing.Size(133, 23)
        Me.run20btn.TabIndex = 0
        Me.run20btn.Text = "Run reports Branch 20"
        Me.run20btn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(190, 262)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 262)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'otherbtn
        '
        Me.otherbtn.Location = New System.Drawing.Point(66, 113)
        Me.otherbtn.Name = "otherbtn"
        Me.otherbtn.Size = New System.Drawing.Size(133, 23)
        Me.otherbtn.TabIndex = 3
        Me.otherbtn.Text = "Run reports Other"
        Me.otherbtn.UseVisualStyleBackColor = True
        '
        'ctaxbtn
        '
        Me.ctaxbtn.Location = New System.Drawing.Point(66, 179)
        Me.ctaxbtn.Name = "ctaxbtn"
        Me.ctaxbtn.Size = New System.Drawing.Size(133, 23)
        Me.ctaxbtn.TabIndex = 4
        Me.ctaxbtn.Text = "BR 1/21 CTAX"
        Me.ctaxbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(73, 163)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(112, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Loaded Post 6.4.2014"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 337)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ctaxbtn)
        Me.Controls.Add(Me.otherbtn)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.run20btn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CMEC STATS"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents run20btn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents otherbtn As System.Windows.Forms.Button
    Friend WithEvents ctaxbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class

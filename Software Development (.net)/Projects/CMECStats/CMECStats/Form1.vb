Public Class Form1
    Dim record, file, record2, file2, file3, file4, file5, record3 As String
    Dim arr_table(30, 3)
    Dim hold_table(30, 5)
    Dim trace_table(30, 2)
    Dim all_table(30, 3)
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles run20btn.Click
        'get all CMEC cases - branch=20
        exitbtn.Enabled = False
        run20btn.Enabled = False
        otherbtn.Enabled = False
        ctaxbtn.Enabled = False
        Dim sch_name, cl_name As String
        param2 = "select _rowid, clientID, schemeID from ClientScheme where branchID = 20"
        Dim clsch_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("No client schemes found for branch=20")
            Exit Sub
        End If
        file = "DebtorID,Client,Scheme,Arrange Started,Arrange Broken,Debt balance,Arrange Amount,Arrange Interval,Agent,Date case loaded" & vbNewLine
        file2 = "DebtorID,Client,Scheme,Hold Started,Hold Ended,Client Request,Agent,Hold reason,Date case loaded" & vbNewLine
        file3 = "DebtorID,Client,Scheme,Trace Started,Trace Ended,Date case loaded" & vbNewLine
        file4 = "DebtorID, 30-59,60-79,80+, No of Visits" & vbNewLine
        file5 = "DebtorID,Def name,1st line of address,postcode,date entered,age of case,date assigned," & _
        "days assigned,visited,last pen date,last pen action,No of visits,original debt,OS balance" & vbNewLine
        'get list of client schemes
        Dim no_of_clientschemes As Integer = no_of_rows
        Dim idx As Integer
        For idx = 0 To no_of_clientschemes - 1
            
            Dim clID As Integer = clsch_dataset.Tables(0).Rows(idx).Item(1)
            If clID = 1 Or clID = 2 Or clID = 24 Then
                Continue For
            End If
            Dim csid As Integer = clsch_dataset.Tables(0).Rows(idx).Item(0)
            Dim schID As Integer = clsch_dataset.Tables(0).Rows(idx).Item(2)
            param2 = "select name from Scheme where _rowid = " & schID
            Dim sch_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Can't find client for " & clID)
                Exit Sub
            End If
            sch_name = sch_dataset.Tables(0).Rows(0).Item(0)
            'get client name
            param2 = "select name from Client where _rowid = " & clID
            Dim cl_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Can't find client for " & clID)
                Exit Sub
            End If
            cl_name = cl_dataset.Tables(0).Rows(0).Item(0)
            'get all cases 
            param2 = "select _rowid, status, debt_balance, arrange_amount, arrange_interval, _createdDate," & _
            " bail_visits, status_open_closed, bail_current, name_title, name_fore, name_sur, address," & _
            " add_postcode, bail_allocated, bailiffID, debt_original from Debtor" & _
            " where clientSchemeID = " & csid

            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim debtor_rows As Integer = no_of_rows
            Dim debtorIDX As Integer
            Dim arr_idx, hold_idx, trace_idx, all_idx As Integer
            For debtorIDX = 0 To debtor_rows - 1
                Try
                    ProgressBar1.Value = (debtorIDX / debtor_rows) * 100
                Catch ex As Exception

                End Try
                Application.DoEvents()
                'get all notes
                Dim debtorID As Integer = debtor_dataset.Tables(0).Rows(debtorIDX).Item(0)
                Dim created_date As Date = debtor_dataset.Tables(0).Rows(debtorIDX).Item(5)
                arr_idx = 0
                hold_idx = 0
                trace_idx = 0
                all_idx = 0
                Dim idx4 As Integer
                For idx4 = 0 To 30
                    trace_table(idx4, 1) = Nothing
                    trace_table(idx4, 2) = Nothing
                    all_table(idx4, 1) = Nothing
                    all_table(idx4, 2) = Nothing
                    all_table(idx4, 3) = Nothing
                Next
                param2 = "select type, text, _createdDate, _createdBy from Note where debtorID = " & debtorID & _
                " order by _rowid"
                Dim note_dataset As DataSet = get_dataset("onestep", param2)
                Dim note_rows As Integer = no_of_rows
                record = ""
                record2 = ""
                record3 = ""
                For idx4 = 0 To note_rows - 1
                    Dim note_type As String = note_dataset.Tables(0).Rows(idx4).Item(0)
                    Dim note_date As Date = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                    If note_type = "Arrangement" Then
                        If arr_idx > 0 And arr_table(arr_idx, 2) = Nothing Then
                            arr_table(arr_idx, 2) = note_date
                        End If
                        arr_idx += 1
                        arr_table(arr_idx, 1) = note_date
                        arr_table(arr_idx, 2) = Format(Now, "dd/MM/yyyy")
                        If all_idx = 0 Then
                            'first entry
                            all_idx += 1
                            all_table(all_idx, 1) = note_date
                            all_table(all_idx, 2) = Format(Now, "dd/MM/yyyy")
                        Else
                            Dim in_force As Boolean = False
                            'see if latest arrangement is still in force
                            If all_table(all_idx, 2) <> Nothing Then
                                If all_table(all_idx, 2) >= note_date Then
                                    'still in force so update end date
                                    in_force = True
                                    all_table(all_idx, 2) = Format(Now, "dd/MM/yyyy")
                                End If
                            End If
                            If Not in_force Then
                                'see if hold still in force
                                If all_table(all_idx, 3) = Nothing Then
                                    'not in force so add new entry
                                    all_idx += 1
                                    all_table(all_idx, 1) = note_date
                                    all_table(all_idx, 2) = Format(Now, "dd/MM/yyyy")
                                Else
                                    If all_table(all_idx, 3) > note_date Then
                                        'hold still in force so can update arr end date
                                        all_table(all_idx, 2) = Format(Now, "dd/MM/yyyy")
                                    Else
                                        'not in force so add new entry
                                        all_idx += 1
                                        all_table(all_idx, 1) = note_date
                                        all_table(all_idx, 2) = Format(Now, "dd/MM/yyyy")
                                    End If
                                End If
                            End If
                           
                        End If
                    End If
                    If note_type = "Broken" Or note_type = "Clear arrange" Then
                        arr_table(arr_idx, 2) = note_dataset.Tables(0).Rows(idx4).Item(2)
                        'update latest entry
                        all_table(all_idx, 2) = note_date
                    End If
                    If note_type = "Trace" Then
                        Try
                            If trace_table(trace_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy") Then
                                trace_table(trace_idx, 2) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                            End If
                        Catch ex As Exception

                        End Try

                        trace_idx += 1
                        trace_table(trace_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                    End If
                    If note_type = "Off trace" Then
                        trace_table(trace_idx, 2) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                    End If
                    If note_type = "Hold" Then
                        If Microsoft.VisualBasic.Left(note_dataset.Tables(0).Rows(idx4).Item(1), 3) <> "for" Then
                            Continue For
                        End If
                        If hold_idx > 0 Then
                            If note_dataset.Tables(0).Rows(idx4).Item(2) < CDate(hold_table(hold_idx, 3)) Then
                                hold_table(hold_idx, 3) = note_dataset.Tables(0).Rows(idx4).Item(2)
                            End If
                        End If
                        hold_idx += 1
                        hold_table(hold_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                        'if prev hold end date is after this hold start date then amend
                        If hold_idx > 1 Then
                            If hold_table(hold_idx - 1, 3) > hold_table(hold_idx, 1) Then
                                hold_table(hold_idx - 1, 3) = hold_table(hold_idx, 1)
                            End If
                        End If
                        If InStr(note_dataset.Tables(0).Rows(idx4).Item(1), "client request") > 0 Then
                            hold_table(hold_idx, 2) = "Y"
                        Else
                            hold_table(hold_idx, 2) = "N"
                        End If
                        hold_table(hold_idx, 4) = note_dataset.Tables(0).Rows(idx4).Item(3)
                        hold_table(hold_idx, 5) = note_dataset.Tables(0).Rows(idx4).Item(1)
                        hold_table(hold_idx, 5) = Replace(hold_table(hold_idx, 5), ",", " ")
                        remove_chrs(hold_table(hold_idx, 5))
                        'get hold days
                        Dim hold_end_date As Date = Nothing
                        Dim end_idx As Integer = InStr(note_dataset.Tables(0).Rows(idx4).Item(1), "days")
                        If end_idx > 0 Then
                            Dim days_str As String = Mid(note_dataset.Tables(0).Rows(idx4).Item(1), 4, end_idx - 4)
                            Dim days As Integer
                            Try
                                days = days_str
                            Catch ex As Exception
                                days = Nothing
                            End Try

                            If days <> Nothing Then
                                hold_end_date = DateAdd(DateInterval.Day, days, hold_table(hold_idx, 1))
                            End If
                            hold_table(hold_idx, 3) = Format(hold_end_date, "dd/MM/yyyy")
                        End If
                        If hold_table(hold_idx, 2) = "Y" Then
                            If all_idx = 0 Then
                                'first entry
                                all_idx += 1
                                all_table(all_idx, 1) = note_date
                                all_table(all_idx, 3) = Format(hold_end_date, "dd/MM/yyyy")
                            Else
                                'see if arrangement is in force
                                Dim in_force As Boolean = False
                                If all_table(all_idx, 2) <> Nothing Then
                                    If all_table(all_idx, 2) > note_date Then
                                        'arrangement still in force
                                        in_force = True
                                        all_table(all_idx, 3) = hold_end_date
                                    End If
                                End If
                                If Not in_force Then
                                    'see if hold is in force
                                    If all_table(all_idx, 3) <> Nothing Then
                                        If all_table(all_idx, 3) >= note_date Then
                                            'hold still in force
                                            all_table(all_idx, 3) = hold_end_date
                                        Else
                                            'no hold so add new entry
                                            all_idx += 1
                                            all_table(all_idx, 1) = note_date
                                            all_table(all_idx, 3) = Format(hold_end_date, "dd/MM/yyyy")
                                        End If
                                    Else
                                        'no hold so add new entry
                                        all_idx += 1
                                        all_table(all_idx, 1) = note_date
                                        all_table(all_idx, 3) = Format(hold_end_date, "dd/MM/yyyy")
                                    End If
                                End If
                            End If
                        End If
                    End If
                    If note_type = "Off hold" Then
                        hold_table(hold_idx, 3) = Format(note_dataset.Tables(0).Rows(idx4).Item(2))
                        'update last entry
                        If all_idx > 0 Then
                            all_table(all_idx, 3) = note_date
                        End If
                    End If
                Next
                Dim debt_bal, arr_amt As Decimal
                Dim arr_interval As Integer
                Dim status As String = debtor_dataset.Tables(0).Rows(debtorIDX).Item(1)
                If status = "A" And arr_idx > 0 Then
                    debt_bal = debtor_dataset.Tables(0).Rows(debtorIDX).Item(2)
                    arr_amt = debtor_dataset.Tables(0).Rows(debtorIDX).Item(3)
                    Try
                        arr_interval = debtor_dataset.Tables(0).Rows(debtorIDX).Item(4)
                    Catch ex As Exception
                        arr_interval = Nothing
                    End Try
                    'set last broken date to blank
                    arr_table(arr_idx, 2) = Nothing
                End If
                'date assigned
                Dim dateAssigned As Date = Nothing
                If debtor_dataset.Tables(0).Rows(debtorIDX).Item(8) = "Y" Then  'allocated case
                    Try
                        dateAssigned = debtor_dataset.Tables(0).Rows(debtorIDX).Item(14)
                    Catch ex As Exception

                    End Try
                End If
                For idx4 = arr_idx To 1 Step -1
                    Dim arr_start As Date = arr_table(idx4, 1)
                    Dim arr_broken As Date
                    Try
                        arr_broken = arr_table(idx4, 2)
                    Catch ex As Exception
                        arr_broken = Nothing
                    End Try
                    record = record & debtorID & "," & cl_name & "," & sch_name & "," & _
                            Format(arr_start, "dd/MM/yyyy") & ","
                    If arr_broken = Nothing Then
                        record = record & ","
                    Else
                        record = record & Format(arr_broken, "dd/MM/yyyy") & ","
                        End If
                        If idx4 = arr_idx And status = "A" Then
                            record = record & Format(debt_bal, "F") & "," & Format(arr_amt, "F") & _
                            "," & arr_interval
                        Else
                            record = record & ",,"
                        End If
                        record = record & "," & arr_table(idx4, 3) & "," & Format(created_date, "dd/MM/yyyy") & vbNewLine
                Next
                If record.Length > 0 Then
                    file = file & record
                End If

                For idx4 = hold_idx To 1 Step -1
                    Dim hold_start As Date = hold_table(idx4, 1)
                    Dim hold_end As Date
                    Try
                        hold_end = hold_table(idx4, 3)
                        If hold_end > Now Then
                            hold_end = Now
                        End If
                    Catch ex As Exception
                        hold_end = Nothing
                    End Try
                    record2 = record2 & debtorID & "," & cl_name & "," & sch_name & "," & _
                                           Format(hold_start, "dd/MM/yyyy") & ","
                    If hold_end = Nothing Then
                        record2 = record2 & ","
                    Else
                        record2 = record2 & Format(hold_end, "dd/MM/yyyy") & ","
                        End If
                        record2 = record2 & hold_table(idx4, 2) & "," & hold_table(idx4, 4) & "," & _
                        hold_table(idx4, 5) & "," & Format(created_date, "dd/MM/yyyy") & vbNewLine
                Next
                Dim ignoreDays As Integer = 0
                Dim ignoreAllocDays As Integer = 0
                For idx4 = trace_idx To 1 Step -1
                    Dim trace_start_date As Date = trace_table(idx4, 1)
                    Dim trace_end_date As Date = trace_table(idx4, 2)
                    record3 = record3 & debtorID & "," & cl_name & "," & sch_name & "," & _
                                           Format(trace_start_date, "dd/MM/yyyy") & ","
                    If Format(trace_end_date, "yyyy") = 1 Then
                        'ignoreDays += DateDiff(DateInterval.Day, trace_start_date, Now)
                        record3 = record3 & ","
                    Else
                        'ignoreDays += DateDiff(DateInterval.Day, trace_start_date, trace_end_date)
                        record3 = record3 & Format(trace_end_date, "dd/MM/yyyy") & ","
                    End If
                    record3 = record3 & Format(created_date, "dd/MM/yyyy") & vbNewLine
                Next
                'check all table to calc ignore days for case age
                For idx4 = 1 To all_idx
                    Dim end_date As Date = Nothing
                    If all_table(idx4, 2) <> Nothing Then
                        end_date = all_table(idx4, 2)
                    End If
                    If all_table(idx4, 3) <> Nothing Then
                        If end_date = Nothing Then
                            end_date = all_table(idx4, 3)
                        Else
                            If all_table(idx4, 3) > end_date Then
                                end_date = all_table(idx4, 3)
                            End If
                        End If
                    End If
                    If end_date > Now Then
                        end_date = Now
                    End If
                    ignoreDays += DateDiff(DateInterval.Day, all_table(idx4, 1), end_date)
                Next
                'check all table if case is assigned
                If dateAssigned <> Nothing Then
                    For idx4 = 1 To all_idx
                        'calc days to ignore after date assigned
                        Dim end_date As Date = Nothing
                        If all_table(idx4, 2) <> Nothing Then
                            end_date = all_table(idx4, 2)
                        End If
                        If all_table(idx4, 3) <> Nothing Then
                            If end_date = Nothing Then
                                end_date = all_table(idx4, 3)
                            Else
                                If all_table(idx4, 3) > end_date Then
                                    end_date = all_table(idx4, 3)
                                End If
                            End If
                        End If
                        If dateAssigned > end_date Then
                            Continue For
                        End If
                        If end_date > Now Then
                            end_date = Now
                        End If
                        If dateAssigned > all_table(idx4, 1) Then
                            ignoreAllocDays += DateDiff(DateInterval.Day, dateAssigned, end_date)
                        Else
                            ignoreAllocDays += DateDiff(DateInterval.Day, all_table(idx4, 1), end_date)
                        End If
                    Next
                End If

                If record2.Length > 0 Then
                    file2 = file2 & record2
                End If
                If record3.Length > 0 Then
                    file3 = file3 & record3
                End If
                'see if case age >= 30 days excluding days on hols, arrangement and trace
                If debtor_dataset.Tables(0).Rows(debtorIDX).Item(7) = "O" Then 'open case
                    Dim bailVisits As Integer = debtor_dataset.Tables(0).Rows(debtorIDX).Item(6)
                    Dim caseAge As Integer = DateDiff(DateInterval.Day, created_date, Now) - ignoreDays
                    If caseAge > 80 Then
                        file4 &= debtorID & ",,,Y," & bailVisits & vbNewLine
                    ElseIf caseAge >= 60 Then
                        file4 &= debtorID & ",,Y,," & bailVisits & vbNewLine
                    ElseIf caseAge >= 30 Then
                        file4 &= debtorID & ",Y,,," & bailVisits & vbNewLine
                    End If
                    'if assigned add to file 5
                    If debtor_dataset.Tables(0).Rows(debtorIDX).Item(8) = "Y" Then  'allocated case
                        Dim bailiffID As Integer = debtor_dataset.Tables(0).Rows(debtorIDX).Item(15)
                        param2 = "select count(*) from bailiff " & _
                        " where _rowid = " & bailiffID & _
                        " and agent_type = 'B' and add_mobile_name <> 'none'"
                        Dim bail_ds As DataSet = get_dataset("onestep", param2)
                        If bail_ds.Tables(0).Rows(0).Item(0) = 0 Then
                            Continue For
                        End If
                        Dim debtorName As String = ""
                        Try
                            debtorName = debtor_dataset.Tables(0).Rows(debtorIDX).Item(9)
                            debtorName &= " "
                        Catch ex As Exception

                        End Try
                        Try
                            debtorName &= debtor_dataset.Tables(0).Rows(debtorIDX).Item(10)
                            debtorName &= " "
                        Catch ex As Exception

                        End Try
                        Try
                            debtorName &= debtor_dataset.Tables(0).Rows(debtorIDX).Item(11)
                            debtorName &= " "
                        Catch ex As Exception

                        End Try
                        file5 &= debtorID & "," & debtorName & ","
                        '1st line of address
                        Dim addrIDX As Integer
                        Dim address As String = ""
                        Try
                            address = debtor_dataset.Tables(0).Rows(debtorIDX).Item(12)
                        Catch ex As Exception

                        End Try
                        For addrIDX = 1 To address.Length
                            If Mid(address, addrIDX, 1) = Chr(10) Or _
                            Mid(address, addrIDX, 1) = Chr(13) Or _
                            Mid(address, addrIDX, 1) = "," Then
                                Exit For
                            End If
                        Next
                        address = Microsoft.VisualBasic.Left(address, addrIDX - 1)
                        file5 &= address & ","
                        Dim postcode As String = ""
                        Try
                            postcode = debtor_dataset.Tables(0).Rows(debtorIDX).Item(13)
                        Catch ex As Exception

                        End Try
                        file5 &= postcode & "," & Format(created_date, "yyyy-MM-dd") & "," & caseAge & ","

                        file5 &= Format(dateAssigned, "yyyy-MM-dd") & ","
                        'no of days assigned excluding on hold or arrangemnt
                        Dim assignedDays As Integer = DateDiff(DateInterval.Day, dateAssigned, Now) - ignoreAllocDays
                        file5 &= assignedDays & ","
                        param2 = "select count(*) from Visit " & _
                        " where debtorID = " & debtorID & _
                        " and bailiffID = " & bailiffID & _
                        " and date_visited is not null"
                        Dim visit_ds As DataSet = get_dataset("onestep", param2)
                        Dim visits As Integer = visit_ds.Tables(0).Rows(0).Item(0)
                        If visits > 0 Then
                            file5 &= "Y,"
                        Else
                            file5 &= "N,"
                        End If
                        param2 = "select _createdDate, sendDelay, _rowID from penform " & _
                        " where caseNumber = " & debtorID & _
                        " and bailiffID = " & bailiffID & _
                        " order by _rowID desc"
                        Dim pen_ds As DataSet = get_dataset("onestep", param2)
                        Dim maxPenDate As Date = Nothing
                        Try
                            maxPenDate = pen_ds.Tables(0).Rows(0).Item(0)
                            Dim sendDelay As Integer = pen_ds.Tables(0).Rows(0).Item(1)
                            If sendDelay > 0 Then
                                maxPenDate = DateAdd(DateInterval.Minute, -sendDelay, maxPenDate)
                            End If
                        Catch ex As Exception

                        End Try
                        Dim answer As String = ""
                        If maxPenDate <> Nothing Then
                            file5 &= Format(maxPenDate, "dd/MM/yyyy")
                            Dim penRowID As Integer = pen_ds.Tables(0).Rows(0).Item(2)
                            param2 = "select answer from penformanswer " & _
                            " where penFormID = " & penRowID & _
                            " and questionExternal = 'Actions'"
                            Dim ans_ds As DataSet = get_dataset("onestep", param2)

                            Try
                                answer = ans_ds.Tables(0).Rows(0).Item(0)
                                answer = Replace(answer, ",", " ")
                            Catch ex As Exception

                            End Try
                        End If
                        file5 &= ","
                       
                        Dim orig_debt As Decimal = debtor_dataset.Tables(0).Rows(debtorIDX).Item(16)
                        debt_bal = debtor_dataset.Tables(0).Rows(debtorIDX).Item(2)
                        file5 &= answer & "," & visits & "," & orig_debt & "," & debt_bal & vbNewLine
                    End If
                End If
            Next
            'MsgBox("one cs only for test")
            'idx = no_of_clientschemes
        Next

        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "TXT files |*.txt"
            .DefaultExt = ".txt"
            .OverwritePrompt = True
            .FileName = "csa_arrangements.csv"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
            Dim start_idx As Integer = InStr(SaveFileDialog1.FileName, "_")
            SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, start_idx) & "holds.csv"
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file2, False)
            SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, start_idx) & "trace.csv"
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file3, False)
            SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, start_idx) & "caseage.csv"
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file4, False)
            SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, start_idx) & "allocated_cases.csv"
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file5, False)
            MsgBox("reports written")
        Else
            MsgBox("No reports written")
        End If
        Me.Close()
    End Sub
    Private Sub remove_chrs(ByRef in_field As String)
        Dim idx2 As Integer
        Dim out_field As String = ""
        For idx2 = 1 To Microsoft.VisualBasic.Len(in_field)
            If Mid(in_field, idx2, 1) <> Chr(9) And _
              Mid(in_field, idx2, 1) <> Chr(10) And _
            Mid(in_field, idx2, 1) <> Chr(13) Then
                out_field = out_field & Mid(in_field, idx2, 1)
            End If
        Next
        in_field = out_field
    End Sub

    Private Sub otherbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otherbtn.Click
        'get all CMEC cases - work-type=12
        exitbtn.Enabled = False
        run20btn.Enabled = False
        otherbtn.Enabled = False
        ctaxbtn.Enabled = False
        Dim sch_name, cl_name As String
        param2 = "select _rowid, name from Scheme where work_type = 12"
        Dim sch_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("No schemes with work-type = 12 found")
            Exit Sub
        End If
        file = "DebtorID,Client,Scheme,Arrange Started,Arrange Broken,Debt balance,Arrange Amount,Arrange Interval,Agent,Date case loaded" & vbNewLine
        file2 = "DebtorID,Client,Scheme,Hold Started,Hold Ended,Client Request,Agent,Hold reason,Date case loaded" & vbNewLine
        file3 = "DebtorID,Client,Scheme,Trace Started,Trace Ended,Date case loaded" & vbNewLine
        'get list of client schemes
        Dim no_of_schemes As Integer = no_of_rows
        Dim idx As Integer
        For idx = 0 To no_of_schemes - 1

            Dim schid As Integer = sch_dataset.Tables(0).Rows(idx).Item(0)
            sch_name = sch_dataset.Tables(0).Rows(idx).Item(1)
            param2 = "select _rowid, clientID from clientScheme where schemeID = " & schid
            Dim csid_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("No client schemes found")
                Exit Sub
                'Continue For
            End If
            Dim no_of_csids As Integer = no_of_rows
            Dim idx2 As Integer
            For idx2 = 0 To no_of_csids - 1
                'get client name

                Dim clID As Integer = csid_dataset.Tables(0).Rows(idx2).Item(1)
                'old CSA clients not required - or test
                If clID = 987 Or clID = 999 Or clID = 1000 Or clID = 1 Or clID = 2 Or clID = 24 Then
                    Continue For
                End If
                param2 = "select name from Client where _rowid = " & clID
                Dim cl_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    MsgBox("Can't find client for " & clID)
                    Exit Sub
                End If
                cl_name = cl_dataset.Tables(0).Rows(0).Item(0)
                'get all cases 
                Dim csid As Integer = csid_dataset.Tables(0).Rows(idx2).Item(0)
                param2 = "select _rowid, status, debt_balance, arrange_amount, arrange_interval, _createdDate from Debtor" & _
                " where clientSchemeID = " & csid

                Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    Continue For
                End If
                Dim debtor_rows As Integer = no_of_rows
                Dim idx3 As Integer
                Dim arr_idx, hold_idx, trace_idx As Integer
                For idx3 = 0 To debtor_rows - 1
                    Try
                        ProgressBar1.Value = (idx3 / debtor_rows) * 100
                    Catch ex As Exception

                    End Try
                    Application.DoEvents()
                    'get all notes
                    Dim debtorID As Integer = debtor_dataset.Tables(0).Rows(idx3).Item(0)
                    Dim created_date As Date = debtor_dataset.Tables(0).Rows(idx3).Item(5)
                    arr_idx = 0
                    hold_idx = 0
                    trace_idx = 0
                    Dim idx4 As Integer
                    For idx4 = 0 To 30
                        trace_table(idx4, 1) = Nothing
                        trace_table(idx4, 2) = Nothing
                    Next
                    'If debtorID = 5473481 Then
                    '    debtorID = 5473481
                    'End If

                    param2 = "select type, text, _createdDate, _createdBy from Note where debtorID = " & debtorID & _
                    " order by _rowid"
                    Dim note_dataset As DataSet = get_dataset("onestep", param2)
                    Dim note_rows As Integer = no_of_rows
                    record = ""
                    record2 = ""
                    record3 = ""
                    For idx4 = 0 To note_rows - 1
                        Dim note_type As String = note_dataset.Tables(0).Rows(idx4).Item(0)
                        If note_type = "Arrangement" Then
                            If arr_idx > 0 And arr_table(arr_idx, 2) = Nothing Then
                                arr_table(arr_idx, 2) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                            End If
                            arr_idx += 1
                            arr_table(arr_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                            arr_table(arr_idx, 2) = Nothing
                            arr_table(arr_idx, 3) = note_dataset.Tables(0).Rows(idx4).Item(3)
                        End If
                        If note_type = "Broken" Or note_type = "Clear arrange" Then
                            arr_table(arr_idx, 2) = note_dataset.Tables(0).Rows(idx4).Item(2)
                        End If
                        If note_type = "Trace" Then
                            Try
                                If trace_table(trace_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy") Then
                                    trace_table(trace_idx, 2) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                                End If
                            Catch ex As Exception

                            End Try

                            trace_idx += 1
                            trace_table(trace_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                        End If
                        If note_type = "Off trace" Then
                            trace_table(trace_idx, 2) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                        End If
                        If note_type = "Hold" Then
                            If Microsoft.VisualBasic.Left(note_dataset.Tables(0).Rows(idx4).Item(1), 3) <> "for" Then
                                Continue For
                            End If
                            If hold_idx > 0 Then
                                If note_dataset.Tables(0).Rows(idx4).Item(2) < CDate(hold_table(hold_idx, 3)) Then
                                    hold_table(hold_idx, 3) = note_dataset.Tables(0).Rows(idx4).Item(2)
                                End If
                            End If
                            hold_idx += 1
                            hold_table(hold_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                            If InStr(note_dataset.Tables(0).Rows(idx4).Item(1), "client request") > 0 Then
                                hold_table(hold_idx, 2) = "Y"
                            Else
                                hold_table(hold_idx, 2) = "N"
                            End If
                            hold_table(hold_idx, 4) = note_dataset.Tables(0).Rows(idx4).Item(3)
                            hold_table(hold_idx, 5) = note_dataset.Tables(0).Rows(idx4).Item(1)
                            hold_table(hold_idx, 5) = Replace(hold_table(hold_idx, 5), ",", " ")
                            remove_chrs(hold_table(hold_idx, 5))
                            'get hold days
                            Dim end_idx As Integer = InStr(note_dataset.Tables(0).Rows(idx4).Item(1), "days")
                            If end_idx > 0 Then
                                Dim days_str As String = Mid(note_dataset.Tables(0).Rows(idx4).Item(1), 4, end_idx - 4)
                                Dim days As Integer
                                Try
                                    days = days_str
                                Catch ex As Exception
                                    days = Nothing
                                End Try
                                Dim hold_end_date As Date = Nothing
                                If days <> Nothing Then
                                    hold_end_date = DateAdd(DateInterval.Day, days, hold_table(hold_idx, 1))
                                End If
                                hold_table(hold_idx, 3) = Format(hold_end_date, "dd/MM/yyyy")
                            End If
                           
                        End If
                        If note_type = "Off hold" Then
                            hold_table(hold_idx, 3) = Format(note_dataset.Tables(0).Rows(idx4).Item(2))
                        End If
                    Next
                    Dim debt_bal, arr_amt As Decimal
                    Dim arr_interval As Integer
                    Dim status As String = debtor_dataset.Tables(0).Rows(idx3).Item(1)
                    If status = "A" And arr_idx > 0 Then
                        debt_bal = debtor_dataset.Tables(0).Rows(idx3).Item(2)
                        arr_amt = debtor_dataset.Tables(0).Rows(idx3).Item(3)
                        Try
                            arr_interval = debtor_dataset.Tables(0).Rows(idx3).Item(4)
                        Catch ex As Exception
                            arr_interval = Nothing
                        End Try

                    End If
                    For idx4 = arr_idx To 1 Step -1
                        Dim arr_start As Date = arr_table(idx4, 1)
                        Dim arr_broken As Date
                        Try
                            arr_broken = arr_table(idx4, 2)
                        Catch ex As Exception
                            arr_broken = Nothing
                        End Try
                        record = record & debtorID & "," & cl_name & "," & sch_name & "," & _
                                Format(arr_start, "dd/MM/yyyy") & ","
                        If arr_broken = Nothing Then
                            record = record & ","
                        Else
                            record = record & Format(arr_broken, "dd/MM/yyyy") & ","
                        End If
                        If idx4 = arr_idx And status = "A" Then
                            record = record & Format(debt_bal, "F") & "," & Format(arr_amt, "F") & _
                            "," & arr_interval
                        Else
                            record = record & ",,"
                        End If
                        record = record & "," & arr_table(idx4, 3) & "," & Format(created_date, "dd/MM/yyyy") & vbNewLine
                    Next
                    If record.Length > 0 Then
                        file = file & record
                    End If

                    For idx4 = hold_idx To 1 Step -1
                        Dim hold_start As Date = hold_table(idx4, 1)
                        Dim hold_end As Date
                        Try
                            hold_end = hold_table(idx4, 3)
                        Catch ex As Exception
                            hold_end = Nothing
                        End Try
                        record2 = record2 & debtorID & "," & cl_name & "," & sch_name & "," & _
                                               Format(hold_start, "dd/MM/yyyy") & ","
                        If hold_end = Nothing Then
                            record2 = record2 & ","
                        Else
                            record2 = record2 & Format(hold_end, "dd/MM/yyyy") & ","
                        End If
                        record2 = record2 & hold_table(idx4, 2) & "," & hold_table(idx4, 4) & "," & _
                        hold_table(idx4, 5) & "," & Format(created_date, "dd/MM/yyyy") & vbNewLine
                    Next
                    For idx4 = trace_idx To 1 Step -1
                        Dim trace_start_date As Date = trace_table(idx4, 1)
                        Dim trace_end_date As Date = trace_table(idx4, 2)
                        record3 = record3 & debtorID & "," & cl_name & "," & sch_name & "," & _
                                               Format(trace_start_date, "dd/MM/yyyy") & ","
                        If Format(trace_end_date, "yyyy") = 1 Then
                            record3 = record3 & ","
                        Else
                            record3 = record3 & Format(trace_end_date, "dd/MM/yyyy") & ","
                        End If
                        record3 = record3 & Format(created_date, "dd/MM/yyyy") & vbNewLine
                    Next
                    If record2.Length > 0 Then
                        file2 = file2 & record2
                    End If
                    If record3.Length > 0 Then
                        file3 = file3 & record3
                    End If
                Next
            Next
        Next
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "TXT files |*.txt"
            .DefaultExt = ".txt"
            .OverwritePrompt = True
            .FileName = "csa_arrangements.csv"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
            Dim start_idx As Integer = InStr(SaveFileDialog1.FileName, "_")
            SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, start_idx) & "holds.csv"
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file2, False)
            SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, start_idx) & "trace.csv"
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file3, False)
            MsgBox("reports written")
        Else
            MsgBox("No reports written")
        End If
        Me.Close()
    End Sub

    Private Sub ctaxbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ctaxbtn.Click
        'get all branch 1 and 20 ctax cases loaded since 7.4.2014
        exitbtn.Enabled = False
        run20btn.Enabled = False
        otherbtn.Enabled = False
        ctaxbtn.Enabled = False
        Dim sch_name, cl_name As String
        param2 = "select _rowid, clientID, schemeID from ClientScheme where (branchID = 1 or branchID = 21)"
        Dim clsch_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("No client schemes found for branch=1 or 21")
            Exit Sub
        End If
        file = "DebtorID,Client,Scheme,Arrange Started,Arrange Broken,Debt balance,Arrange Amount,Arrange Interval,Agent,Date case loaded" & vbNewLine
        'get list of client schemes
        Dim no_of_clientschemes As Integer = no_of_rows
        Dim idx As Integer
        For idx = 0 To no_of_clientschemes - 1
            Dim clID As Integer = clsch_dataset.Tables(0).Rows(idx).Item(1)
            If clID = 1 Or clID = 2 Or clID = 24 Then
                Continue For
            End If
            Dim csid As Integer = clsch_dataset.Tables(0).Rows(idx).Item(0)
            Dim schID As Integer = clsch_dataset.Tables(0).Rows(idx).Item(2)
            param2 = "select name, work_type from Scheme where _rowid = " & schID
            Dim sch_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Can't find client for " & clID)
                Exit Sub
            End If
            'only CTAX cases
            If sch_dataset.Tables(0).Rows(0).Item(1) <> 2 Then
                Continue For
            End If
            sch_name = sch_dataset.Tables(0).Rows(0).Item(0)
            'get client name
            param2 = "select name from Client where _rowid = " & clID
            Dim cl_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Can't find client for " & clID)
                Exit Sub
            End If
            cl_name = cl_dataset.Tables(0).Rows(0).Item(0)
            'get all cases 
            'only need cases loaded since 7.4.14
            Dim startDate As Date = CDate("Apr6, 2014")
            param2 = "select _rowid, status, debt_balance, arrange_amount, arrange_interval, _createdDate," & _
            " bail_visits, status_open_closed from Debtor" & _
            " where clientSchemeID = " & csid & _
            " and _createdDate > '" & Format(startDate, "yyyy-MM-dd") & " '"
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim debtor_rows As Integer = no_of_rows
            Dim debtorIDX As Integer
            Dim arr_idx, hold_idx, trace_idx As Integer
            For debtorIDX = 0 To debtor_rows - 1
                Try
                    ProgressBar1.Value = (debtorIDX / debtor_rows) * 100
                Catch ex As Exception

                End Try
                Application.DoEvents()
                'get all notes
                Dim debtorID As Integer = debtor_dataset.Tables(0).Rows(debtorIDX).Item(0)
                Dim created_date As Date = debtor_dataset.Tables(0).Rows(debtorIDX).Item(5)

              
                arr_idx = 0
                hold_idx = 0
                trace_idx = 0
                Dim idx4 As Integer
                For idx4 = 0 To 30
                    trace_table(idx4, 1) = Nothing
                    trace_table(idx4, 2) = Nothing
                Next
                param2 = "select type, text, _createdDate, _createdBy from Note where debtorID = " & debtorID & _
                " order by _rowid"
                Dim note_dataset As DataSet = get_dataset("onestep", param2)
                Dim note_rows As Integer = no_of_rows
                record = ""
                record2 = ""
                record3 = ""
                For idx4 = 0 To note_rows - 1
                    Dim note_type As String = note_dataset.Tables(0).Rows(idx4).Item(0)
                    If note_type = "Arrangement" Then
                        If arr_idx > 0 And arr_table(arr_idx, 2) = Nothing Then
                            arr_table(arr_idx, 2) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                        End If
                        arr_idx += 1
                        arr_table(arr_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                        arr_table(arr_idx, 2) = Nothing
                        arr_table(arr_idx, 3) = note_dataset.Tables(0).Rows(idx4).Item(3)
                    End If
                    If note_type = "Broken" Or note_type = "Clear arrange" Then
                        arr_table(arr_idx, 2) = note_dataset.Tables(0).Rows(idx4).Item(2)
                    End If
                    If note_type = "Trace" Then
                        Try
                            If trace_table(trace_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy") Then
                                trace_table(trace_idx, 2) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                            End If
                        Catch ex As Exception

                        End Try

                        trace_idx += 1
                        trace_table(trace_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                    End If
                    If note_type = "Off trace" Then
                        trace_table(trace_idx, 2) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                    End If
                    If note_type = "Hold" Then
                        If Microsoft.VisualBasic.Left(note_dataset.Tables(0).Rows(idx4).Item(1), 3) <> "for" Then
                            Continue For
                        End If
                        If hold_idx > 0 Then
                            If note_dataset.Tables(0).Rows(idx4).Item(2) < CDate(hold_table(hold_idx, 3)) Then
                                hold_table(hold_idx, 3) = note_dataset.Tables(0).Rows(idx4).Item(2)
                            End If
                        End If
                        hold_idx += 1
                        hold_table(hold_idx, 1) = Format(note_dataset.Tables(0).Rows(idx4).Item(2), "dd/MM/yyyy")
                        If InStr(note_dataset.Tables(0).Rows(idx4).Item(1), "client request") > 0 Then
                            hold_table(hold_idx, 2) = "Y"
                        Else
                            hold_table(hold_idx, 2) = "N"
                        End If
                        hold_table(hold_idx, 4) = note_dataset.Tables(0).Rows(idx4).Item(3)
                        hold_table(hold_idx, 5) = note_dataset.Tables(0).Rows(idx4).Item(1)
                        hold_table(hold_idx, 5) = Replace(hold_table(hold_idx, 5), ",", " ")
                        remove_chrs(hold_table(hold_idx, 5))
                        'get hold days
                        Dim end_idx As Integer = InStr(note_dataset.Tables(0).Rows(idx4).Item(1), "days")
                        If end_idx > 0 Then
                            Dim days_str As String = Mid(note_dataset.Tables(0).Rows(idx4).Item(1), 4, end_idx - 4)
                            Dim days As Integer
                            Try
                                days = days_str
                            Catch ex As Exception
                                days = Nothing
                            End Try
                            Dim hold_end_date As Date = Nothing
                            If days <> Nothing Then
                                hold_end_date = DateAdd(DateInterval.Day, days, hold_table(hold_idx, 1))
                            End If
                            hold_table(hold_idx, 3) = Format(hold_end_date, "dd/MM/yyyy")
                        End If
                    End If
                    If note_type = "Off hold" Then
                        hold_table(hold_idx, 3) = Format(note_dataset.Tables(0).Rows(idx4).Item(2))
                    End If
                Next
                Dim debt_bal, arr_amt As Decimal
                Dim arr_interval As Integer
                Dim status As String = debtor_dataset.Tables(0).Rows(debtorIDX).Item(1)
                If status = "A" And arr_idx > 0 Then
                    debt_bal = debtor_dataset.Tables(0).Rows(debtorIDX).Item(2)
                    arr_amt = debtor_dataset.Tables(0).Rows(debtorIDX).Item(3)
                    Try
                        arr_interval = debtor_dataset.Tables(0).Rows(debtorIDX).Item(4)
                    Catch ex As Exception
                        arr_interval = Nothing
                    End Try

                End If
                Dim ignoreDays As Integer = 0
                For idx4 = arr_idx To 1 Step -1
                    Dim arr_start As Date = arr_table(idx4, 1)
                    Dim arr_broken As Date
                    Try
                        arr_broken = arr_table(idx4, 2)
                    Catch ex As Exception
                        arr_broken = Nothing
                    End Try
                    record = record & debtorID & "," & cl_name & "," & sch_name & "," & _
                            Format(arr_start, "dd/MM/yyyy") & ","
                    If arr_broken = Nothing Then
                        ignoreDays += DateDiff(DateInterval.Day, arr_start, Now)
                        record = record & ","
                    Else
                        ignoreDays += DateDiff(DateInterval.Day, arr_start, arr_broken)
                        record = record & Format(arr_broken, "dd/MM/yyyy") & ","
                    End If
                    If idx4 = arr_idx And status = "A" Then
                        record = record & Format(debt_bal, "F") & "," & Format(arr_amt, "F") & _
                        "," & arr_interval
                    Else
                        record = record & ",,"
                    End If
                    record = record & "," & arr_table(idx4, 3) & "," & Format(created_date, "dd/MM/yyyy") & vbNewLine
                Next
                If record.Length > 0 Then
                    file = file & record
                End If

                For idx4 = hold_idx To 1 Step -1
                    Dim hold_start As Date = hold_table(idx4, 1)
                    Dim hold_end As Date
                    Try
                        hold_end = hold_table(idx4, 3)
                    Catch ex As Exception
                        hold_end = Nothing
                    End Try
                    record2 = record2 & debtorID & "," & cl_name & "," & sch_name & "," & _
                                           Format(hold_start, "dd/MM/yyyy") & ","
                    If hold_end = Nothing Then
                        ignoreDays += DateDiff(DateInterval.Day, hold_start, Now)
                        record2 = record2 & ","
                    Else
                        ignoreDays += DateDiff(DateInterval.Day, hold_start, hold_end)
                        record2 = record2 & Format(hold_end, "dd/MM/yyyy") & ","
                    End If
                    record2 = record2 & hold_table(idx4, 2) & "," & hold_table(idx4, 4) & "," & _
                    hold_table(idx4, 5) & "," & Format(created_date, "dd/MM/yyyy") & vbNewLine
                Next
                For idx4 = trace_idx To 1 Step -1
                    Dim trace_start_date As Date = trace_table(idx4, 1)
                    Dim trace_end_date As Date = trace_table(idx4, 2)
                    record3 = record3 & debtorID & "," & cl_name & "," & sch_name & "," & _
                                           Format(trace_start_date, "dd/MM/yyyy") & ","
                    If Format(trace_end_date, "yyyy") = 1 Then
                        ignoreDays += DateDiff(DateInterval.Day, trace_start_date, Now)
                        record3 = record3 & ","
                    Else
                        ignoreDays += DateDiff(DateInterval.Day, trace_start_date, trace_end_date)
                        record3 = record3 & Format(trace_end_date, "dd/MM/yyyy") & ","
                    End If
                    record3 = record3 & Format(created_date, "dd/MM/yyyy") & vbNewLine
                Next
                If record2.Length > 0 Then
                    file2 = file2 & record2
                End If
                If record3.Length > 0 Then
                    file3 = file3 & record3
                End If
                'see if case age >= 30 days excluding days on hols, arrangement and trace
                If debtor_dataset.Tables(0).Rows(debtorIDX).Item(7) = "O" Then 'open case
                    Dim bailVisits As Integer = debtor_dataset.Tables(0).Rows(debtorIDX).Item(6)
                    Dim caseAge As Integer = DateDiff(DateInterval.Day, created_date, Now) - ignoreDays
                    If caseAge > 80 Then
                        file4 &= debtorID & ",,,Y," & bailVisits & vbNewLine
                    ElseIf caseAge >= 60 Then
                        file4 &= debtorID & ",,Y,," & bailVisits & vbNewLine
                    ElseIf caseAge >= 30 Then
                        file4 &= debtorID & ",Y,,," & bailVisits & vbNewLine
                    End If
                End If
            Next
        Next

        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "TXT files |*.txt"
            .DefaultExt = ".txt"
            .OverwritePrompt = True
            .FileName = "ctax_arrangements.csv"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
            'Dim start_idx As Integer = InStr(SaveFileDialog1.FileName, "_")
            'SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, start_idx) & "holds.csv"
            'My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file2, False)
            'SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, start_idx) & "trace.csv"
            'My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file3, False)
            'SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, start_idx) & "caseage.csv"
            'My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file4, False)
            MsgBox("reports written")
        Else
            MsgBox("No reports written")
        End If
        Me.Close()
    End Sub
End Class

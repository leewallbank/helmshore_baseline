Public Class Form1
    Dim filepath As String = "H:\temp\"
    Dim filename As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        'get all LSC cases with writ received date= writ_dtp
        exitbtn.Enabled = False
        runbtn.Enabled = False
        param2 = "select _rowid, clientSchemeID from Debtor" & _
        " where writReceivedDate = '" & Format(writ_dtp.Value, "yyyy-MM-dd") & "'"

        Dim debt_ds As DataSet = get_dataset("onestep", param2)
        Dim debt_rows As Integer = no_of_rows - 1
        Dim debt_idx As Integer
        Dim reports_written As Integer = 0
        For debt_idx = 0 To debt_rows
            Try
                ProgressBar1.Value = (debt_idx / debt_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            Dim cs_id As Integer = debt_ds.Tables(0).Rows(debt_idx).Item(1)
            param2 = "select clientID from clientScheme where _rowid = " & cs_id
            Dim cs_ds As DataSet = get_dataset("onestep", param2)
            If cs_ds.Tables(0).Rows(0).Item(0) <> 909 Then
                Continue For
            End If
            'check notes are in place
            Dim debtorID As Integer = debt_ds.Tables(0).Rows(debt_idx).Item(0)
            param2 = "select text, _createdDate from Note where debtorID = " & debtorID & _
            " and text like 'Total found%less%'" & _
            " order by _createdDate desc"

            Dim note_ds As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim created_date As Date = note_ds.Tables(0).Rows(0).Item(1)
            If Format(created_date, "yyyy-MM-dd") <> Format(writ_dtp.Value, "yyyy-MM-dd") Then
                Continue For
            End If
            Dim txt As String = note_ds.Tables(0).Rows(0).Item(0)
            Dim start_colon As Integer = InStr(txt, ":")
            If start_colon = 0 Then
                Continue For
            End If
            Dim start_semi_colon As Integer = InStr(txt, ";")
            If start_semi_colon = 0 Or start_semi_colon < start_colon Then
                Continue For
            End If
            Dim cap As Decimal
            Try
                cap = Mid(txt, start_colon + 1, start_semi_colon - start_colon - 1)
            Catch ex As Exception
                Continue For
            End Try
            reports_written += 1

            Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
            Dim RA1393Preport = New RA1393P
            Dim myArrayList1 As ArrayList = New ArrayList()
            myArrayList1.Add(debtorID)
            SetCurrentValuesForParameterField1(RA1393Preport, myArrayList1)
            myArrayList1.Add(cap)
            SetCurrentValuesForParameterField2(RA1393Preport, myArrayList1)
            myConnectionInfo.ServerName = "DebtRecovery"
            myConnectionInfo.DatabaseName = "DebtRecovery"
            myConnectionInfo.UserID = "vbnet"
            myConnectionInfo.Password = "tenbv"

            Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
            myConnectionInfo2.ServerName = "LSC_reporting"
            myConnectionInfo2.DatabaseName = "LSCReporting"
            myConnectionInfo2.UserID = "rpt_user"
            myConnectionInfo2.Password = "resu_tpr"
            SetDBLogonForReport(myConnectionInfo, myConnectionInfo2, RA1393Preport)
            filename = filepath & debtorID & ".pdf"
            RA1393Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
            RA1393Preport.close()
        Next
        MsgBox(reports_written & " letters written")
        Me.Close()
    End Sub
End Class

Public Class Form1

    Dim equity_max = 20
    Dim tot_ver_cap_and_equity, add_prop_equity As String
    Dim equity_amt(equity_max) As String
    Dim overridden(equity_max) As String
    Dim equity_type(equity_max) As String
    Dim equity_ver(equity_max) As String
    Dim cap_amt(equity_max) As String
    Dim cap_ver(equity_max) As String
    Dim cap_asset_type(equity_max) As String
    Dim file As String
    Dim debtorID As Integer
    Dim no_of_assets As Integer
    Dim no_of_equities As Integer
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        'get all open live ccmt post con cases
        runbtn.Enabled = False
        exitbtn.Enabled = False
        param2 = "select  _rowid, client_ref, offencevalue from Debtor " & _
        " where _rowid = 5424099 and clientSchemeID = 2110 and status_open_closed = 'O' and status = 'L'"
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("No cases found")
            enable_buttons()
            Exit Sub
        End If
        Dim idx, idx2, idx3 As Integer
        Dim start_idx As Integer
        Dim text As String
        file = "Ross Ref|MAAT Ref|Equity Amt|Overridden|Equity Amt Verified|Equity Type|Contribution Cap Amt|" & _
        "Cap Amt|Cap Amt Verified|Cap asset Type|Amt over 30K" & vbNewLine

        Dim debtor_rows As Integer = no_of_rows - 1
        For idx = 0 To debtor_rows
            For idx2 = 1 To equity_max
                cap_asset_type(idx2) = ""
                cap_amt(idx2) = ""
                cap_ver(idx2) = ""
                equity_amt(idx2) = ""
                overridden(idx2) = ""
                equity_ver(idx2) = ""
                equity_type(idx2) = ""
            Next

            tot_ver_cap_and_equity = ""
            add_prop_equity = ""
            ProgressBar1.Value = (idx / (debtor_rows + 1)) * 100
            Application.DoEvents()
            debtorID = debtor_dataset.Tables(0).Rows(idx).Item(0)
            'get client notes
            param2 = "select text from Note where debtorID = " & debtorID & " and type = 'Client note'" & _
            " order by _rowid"
            Dim note_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("No notes found for case = " & debtorID)
            End If
            Dim amt_over_30k As Decimal
            no_of_assets = 0
            For idx2 = 0 To no_of_rows - 1
                text = note_dataset.Tables(0).Rows(idx2).Item(0)
                start_idx = InStr(text, "Additional Property Equity:")
                If start_idx <> 0 Then
                    start_idx += 27
                    For idx3 = start_idx To text.Length
                        If Mid(text, idx3, 1) = ";" Then
                            Exit For
                        End If
                    Next
                    add_prop_equity = Mid(text, start_idx, idx3 - start_idx)
                    equity_amt(0) = add_prop_equity
                    save_equity_amt()
                    equity_ver(0) = ""
                    save_equity_ver()
                    equity_type(0) = "Additional Property Equity"
                    save_equity_type()
                End If
                start_idx = InStr(text, "Total Verified Capital and Equity:")
                If start_idx <> 0 Then
                    start_idx += 34
                    For idx3 = start_idx To text.Length
                        If Mid(text, idx3, 1) = ";" Then
                            Exit For
                        End If
                    Next
                    tot_ver_cap_and_equity = Mid(text, start_idx, idx3 - start_idx)
                    cap_amt(0) = tot_ver_cap_and_equity
                    save_cap_amt_tot_ver()
                End If
                start_idx = InStr(text, "Equity Amt:")
                If start_idx <> 0 Then
                    start_idx += 11
                    For idx3 = start_idx To text.Length
                        If Mid(text, idx3, 1) = ";" Then
                            Exit For
                        End If
                    Next
                    If Mid(text, start_idx, 1) = "-" Then
                        equity_amt(0) = Mid(text, start_idx + 1, idx3 - start_idx - 1)
                    Else
                        equity_amt(0) = Mid(text, start_idx, idx3 - start_idx)
                    End If
                    save_equity_amt()
                    equity_type(0) = "Equity Amt"
                    save_equity_type()
                End If
                start_idx = InStr(text, "Equity Amt Verified:")
                If start_idx <> 0 Then
                    start_idx += 20
                    For idx3 = start_idx To text.Length
                        If Mid(text, idx3, 1) = ";" Then
                            Exit For
                        End If
                    Next
                    equity_ver(0) = Mid(text, start_idx, idx3 - start_idx)
                    save_equity_ver()
                End If
                start_idx = InStr(text, "Cap Amt:")
                While start_idx <> 0
                    start_idx += 8
                    For idx3 = start_idx To text.Length
                        If Mid(text, idx3, 1) = ";" Then
                            Exit For
                        End If
                    Next
                    If Mid(text, start_idx, 1) = "-" Then
                        cap_amt(0) = Mid(text, start_idx + 1, idx3 - start_idx - 1)
                    Else
                        cap_amt(0) = Mid(text, start_idx, idx3 - start_idx)
                    End If
                    save_cap_amt()
                    text = Microsoft.VisualBasic.Right(text, text.Length - idx3)
                    start_idx = InStr(text, "Cap Amt:")
                End While
                text = note_dataset.Tables(0).Rows(idx2).Item(0)
                start_idx = InStr(text, "Cap Amt Verified:")
                While start_idx <> 0
                    start_idx += 17
                    For idx3 = start_idx To text.Length
                        If Mid(text, idx3, 1) = ";" Then
                            Exit For
                        End If
                    Next
                    cap_ver(0) = Mid(text, start_idx, idx3 - start_idx)
                    save_cap_ver()
                    text = Microsoft.VisualBasic.Right(text, text.Length - idx3)
                    start_idx = InStr(text, "Cap Amt Verified:")
                End While
                text = note_dataset.Tables(0).Rows(idx2).Item(0)
                start_idx = InStr(text, "Cap Asset Type:")
                While start_idx <> 0
                    start_idx += 15
                    For idx3 = start_idx To text.Length
                        If Mid(text, idx3, 1) = ";" Then
                            Exit For
                        End If
                    Next
                    cap_asset_type(0) = Mid(text, start_idx, idx3 - start_idx)
                    save_cap_asset_type()
                    Try
                        text = Microsoft.VisualBasic.Right(text, text.Length - idx3)
                    Catch ex As Exception
                        start_idx = 0
                        Exit While
                    End Try
                    start_idx = InStr(text, "Cap Asset Type:")
                End While
            Next
            Dim idx4 As Integer
            amt_over_30k = 0
            Dim overridden_amt As Boolean = False
            For idx4 = no_of_equities To 1 Step -1
                Try
                    If overridden_amt = False Then
                        amt_over_30k = amt_over_30k + equity_amt(idx4)
                    End If
                Catch ex As Exception
                    Continue For
                End Try
                If equity_type(idx4) = "Equity Amt" Then
                    If overridden_amt = False Then
                        overridden(idx4) = "No"
                        overridden_amt = True
                    Else
                        overridden(idx4) = "Yes"
                    End If
                End If
            Next
            For idx4 = 1 To no_of_assets
                Try
                    amt_over_30k = amt_over_30k + cap_amt(idx4)
                Catch ex As Exception
                    Exit For
                End Try
            Next
            amt_over_30k -= 30000
            If amt_over_30k < 0 Then
                amt_over_30k = 0
            End If
            If tot_ver_cap_and_equity <> "" Then
                For idx4 = 1 To equity_max
                    If cap_amt(idx4) = tot_ver_cap_and_equity Then
                        amt_over_30k = tot_ver_cap_and_equity
                        Exit For
                    End If
                Next
            End If
            Dim maatID As String = debtor_dataset.Tables(0).Rows(idx).Item(1)
            Dim contrib_amt As Decimal = debtor_dataset.Tables(0).Rows(idx).Item(2)

            
            For idx4 = 1 To equity_max
                If equity_amt(idx4) <> "" Or cap_amt(idx4) <> "" Then
                    file = file & debtorID & "|" & maatID & "|" & _
                       equity_amt(idx4) & "|" & overridden(idx4) & "|" & equity_ver(idx4) & "|" & _
                       equity_type(idx4) & "|" & contrib_amt & "|" & cap_amt(idx4) & "|" & cap_ver(idx4) & _
                       "|" & cap_asset_type(idx4) & "|" & amt_over_30k & vbNewLine
                End If
            Next
        Next
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "TXT files |*.txt"
            .DefaultExt = ".txt"
            .OverwritePrompt = True
            .FileName = "capital_and_equity.txt"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
            MsgBox("report saved")
        Else
            MsgBox("report NOT saved")
        End If
        Me.Close()
    End Sub
    Private Sub save_cap_amt()
        Dim idx As Integer
        For idx = 1 To equity_max
            If cap_amt(idx) = "" Then
                cap_amt(idx) = cap_amt(0)
                no_of_assets = idx
                Exit For
            End If
        Next
    End Sub
    Private Sub save_cap_amt_tot_ver()
        Dim idx As Integer
        For idx = 1 To equity_max
            If cap_amt(idx) = "" Then
                cap_amt(idx) = cap_amt(0)
                cap_ver(idx) = "Yes"
                cap_asset_type(idx) = "Total Verified Cap and Equity"
                no_of_assets = idx
                Exit For
            End If
        Next
    End Sub
    Private Sub save_equity_type()
        Dim idx As Integer
        For idx = 1 To equity_max
            If equity_type(idx) = "" Then
                equity_type(idx) = equity_type(0)
                Exit For
            End If
        Next
    End Sub
    Private Sub save_equity_amt()
        Dim idx As Integer
        For idx = 1 To equity_max
            If equity_amt(idx) = "" Then
                equity_amt(idx) = equity_amt(0)
                no_of_equities = idx
                Exit For
            End If
        Next
    End Sub
    Private Sub save_cap_asset_type()
        Dim idx As Integer
        For idx = 1 To equity_max
            If cap_asset_type(idx) = "" Then
                cap_asset_type(idx) = cap_asset_type(0)
                Exit For
            End If
        Next
    End Sub
    Private Sub save_cap_ver()
        Dim idx As Integer
        For idx = 1 To equity_max
            If cap_ver(idx) = "" Then
                cap_ver(idx) = cap_ver(0)
                Exit For
            End If
        Next
    End Sub
    Private Sub save_equity_ver()
        Dim idx As Integer
        For idx = 1 To equity_max
            If equity_ver(idx) = "" Then
                equity_ver(idx) = equity_ver(0)
                Exit For
            End If
        Next
    End Sub
    Private Sub enable_buttons()
        exitbtn.Enabled = True
        runbtn.Enabled = True
    End Sub
End Class

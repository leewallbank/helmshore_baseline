﻿'---------------------------------------------------------------------
'  This file is part of the Microsoft .NET Framework SDK Code Samples.
' 
'  Copyright (C) Microsoft Corporation.  All rights reserved.
' 
'This source code is intended only as a supplement to Microsoft
'Development Tools and/or on-line documentation.  See these other
'materials for detailed information regarding Microsoft code samples.
' 
'THIS CODE AND INFORMATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY
'KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
'IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
'PARTICULAR PURPOSE.
'---------------------------------------------------------------------

Imports System.net.mail

Module Mailer
    Function email(ByVal toaddress, ByVal fromaddress, ByVal subject, ByVal body) As Integer
        Dim mailServerName As String = "rossvr03"
        Try
            Using message As New MailMessage(fromaddress, toaddress, subject, body)


                Dim mailClient As New SmtpClient(mailServerName)
                mailClient.UseDefaultCredentials = True
                message.CC.Add("crystal@rossendales.com")
                mailClient.Send(message)
            End Using
            Return (0)
        Catch ex As Exception

            Return (10)
        End Try
    End Function
End Module

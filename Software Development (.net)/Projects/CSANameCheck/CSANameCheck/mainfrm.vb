Public Class mainfrm

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'param2 = "select _rowid from Scheme where work_type = 12"
        'Dim sch2_dataset As DataSet = get_dataset("onestep", param2)
        'Dim sch_rows As Integer = no_of_rows
        'Dim sch_idx As Integer
        'For sch_idx = 0 To sch_rows - 1
        '    Dim sch2ID As Integer = sch2_dataset.Tables(0).Rows(sch_idx).Item(0)
        '    param2 = "select _rowid, clientID from clientScheme where schemeID = " & sch2ID
        '    Dim cs2_dataset As DataSet = get_dataset("onestep", param2)
        '    Dim cs2_rows As Integer = no_of_rows
        '    Dim cs2_idx As Integer
        '    For cs2_idx = 0 To cs2_rows - 1
        '        Dim clID As Integer = cs2_dataset.Tables(0).Rows(cs2_idx).Item(1)
        '        If clID = 1 Or clID = 2 Or clID = 24 Then
        '            Continue For
        '        End If
        '        Dim cs2ID As Integer = cs2_dataset.Tables(0).Rows(cs2_idx).Item(0)
        '        param2 = "select _rowid, clientSchemeID, name_sur, name_fore, address from Debtor" & _
        '        " where clientSchemeID = " & cs2ID
        '        Dim debt_dataset As DataSet = get_dataset("onestep", param2)
        '        Dim debt_rows As Integer = no_of_rows
        '        Dim idx As Integer
        '        For idx = 0 To debt_rows - 1
        '            Dim csID As Integer = debt_dataset.Tables(0).Rows(idx).Item(1)
        '            param2 = "select schemeID from clientScheme" & _
        '            " where _rowid = " & csID
        '            Dim cs_dataset As DataSet = get_dataset("onestep", param2)
        '            Dim schID As Integer = cs_dataset.Tables(0).Rows(0).Item(0)
        '            param2 = "select work_type from Scheme" & _
        '            " where _rowid = " & schID
        '            Dim sch_dataset As DataSet = get_dataset("onestep", param2)
        '            'check name against agents
        '            Dim surname As String = debt_dataset.Tables(0).Rows(idx).Item(2)
        '            Dim forename As String
        '            Try
        '                forename = debt_dataset.Tables(0).Rows(idx).Item(3)
        '            Catch ex As Exception
        '                forename = ""
        '            End Try

        '            surname = Replace(surname, "'", "")
        '            If forename = "" Then
        '                param2 = "select _rowid from Bailiff where name_sur = '" & surname & "'" & _
        '                                    " and status = 'O'"
        '            Else
        '                forename = Replace(forename, "'", "")
        '                param2 = "select _rowid from Bailiff where name_sur = '" & surname & "'" & _
        '                                    " and name_fore = '" & forename & "'" & " and status = 'O'"
        '            End If


        '            Dim bail_dataset As DataSet = get_dataset("onestep", param2)
        '            Dim bail_rows As Integer = no_of_rows
        '            Dim idx2 As Integer
        '            For idx2 = 0 To bail_rows - 1
        '                'send email out
        '                Dim bail_rowid As Integer = bail_dataset.Tables(0).Rows(idx2).Item(0)
        '                Dim debtorID As Integer = debt_dataset.Tables(0).Rows(idx).Item(0)
        '                Dim body As String = forename & " " & surname & " on last week's file from CSA may be the same as agent no " & bail_rowid & " on onestep" & vbNewLine
        '                Dim address As String = debt_dataset.Tables(0).Rows(idx).Item(4)
        '                body = body & "DebtorID is " & debtorID & vbNewLine
        '                body = body & "Address is: " & address
        '                If email("scowpe@rossendales.com", "crystal@rossendales.com", "Potential CSA case", body) <> 0 Then
        '                    MsgBox("Contact IT - error message EM123")
        '                    Exit Sub
        '                End If
        '            Next
        '        Next
        '    Next
        'Next
        'MsgBox("Completed")
        'Me.Close()
        'Exit Sub

        'look at cases loaded last week
        Dim start_date As Date = DateAdd(DateInterval.Day, -7, Now)
        Dim wkday As Integer = Weekday(start_date) - 1
        start_date = DateAdd(DateInterval.Day, -wkday, start_date)
        param2 = "select _rowid, clientSchemeID, name_sur, name_fore from Debtor" & _
        " where _createdDate > '" & Format(start_date, "yyyy-MM-dd") & "'"
        Dim debt_dataset As DataSet = get_dataset("onestep", param2)
        Dim debt_rows As Integer = no_of_rows
        Dim idx As Integer
        For idx = 0 To debt_rows - 1
            Dim csID As Integer = debt_dataset.Tables(0).Rows(idx).Item(1)
            param2 = "select schemeID, clientID from clientScheme" & _
            " where _rowid = " & csID
            Dim cs_dataset As DataSet = get_dataset("onestep", param2)
            Dim clID As Integer = cs_dataset.Tables(0).Rows(0).Item(1)
            If clID = 1 Or clID = 2 Or clID = 24 Then
                Continue For
            End If
            Dim schID As Integer = cs_dataset.Tables(0).Rows(0).Item(0)
            param2 = "select work_type from Scheme" & _
            " where _rowid = " & schID
            Dim sch_dataset As DataSet = get_dataset("onestep", param2)
            If sch_dataset.Tables(0).Rows(0).Item(0) = 12 Then
                'check name against agents
                Dim surname As String = debt_dataset.Tables(0).Rows(idx).Item(2)
                Dim forename As String
                Try
                    forename = debt_dataset.Tables(0).Rows(idx).Item(3)
                Catch ex As Exception
                    forename = ""
                End Try

                surname = Replace(surname, "'", "")
                If forename = "" Then
                    param2 = "select _rowid from Bailiff where name_sur = '" & surname & "'" & _
                                        " and status = 'O'"
                Else
                    forename = Replace(forename, "'", "")
                    param2 = "select _rowid from Bailiff where name_sur = '" & surname & "'" & _
                                        " and name_fore = '" & forename & "'" & " and status = 'O'"
                End If
                Dim bail_dataset As DataSet = get_dataset("onestep", param2)
                Dim bail_rows As Integer = no_of_rows
                Dim idx2 As Integer
                For idx2 = 0 To bail_rows - 1
                    'send email out
                    Dim debtorID As Integer = debt_dataset.Tables(0).Rows(idx).Item(0)
                    Dim bail_rowid As Integer = bail_dataset.Tables(0).Rows(idx2).Item(0)
                    Dim body As String = forename & " " & surname & " on last week's file from CSA may be the same as agent no " & bail_rowid & " on onestep" & vbNewLine
                    Dim address As String = debt_dataset.Tables(0).Rows(idx).Item(4)
                    body = body & "DebtorID is " & debtorID & vbNewLine
                    body = body & "Address is: " & address
                    email("jblundell@rossendales.com", "crystal@rossendales.com", "Potential CSA case", body)
                Next
            End If
        Next
        Me.Close()
    End Sub
End Class

Public Class adminfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub pwordbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pwordbtn.Click
        resetpwordfrm.ShowDialog()
    End Sub

    Private Sub userbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles userbtn.Click
        userfrm.ShowDialog()
    End Sub

    Private Sub resetbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles resetbtn.Click
        Try
            debtorID = InputBox("Enter Debtor ID", "Reset case to Work in Progress")
        Catch ex As Exception
            MsgBox("Invalid number")
            Me.Close()
            Return
        End Try
        'check case is at status > 2
        casefrm.LSC_Debtor_tableTableAdapter.FillBy(casefrm.FeesSQLDataSet.LSC_Debtor_table, debtorID)
        If casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows.Count = 0 Then
            MsgBox("Case " & debtorID & " is not on the system")
            Me.Close()
            Return
        End If
        If casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(10) < 2 Then
            MsgBox("Case " & debtorID & " is not completed")
            Me.Close()
            Return
        End If
        'update status to 1
        Try
            casefrm.LSC_Debtor_tableTableAdapter.UpdateQuery1(1, Nothing, debtorID)
        Catch ex As Exception

        End Try
        MsgBox("Debtor " & debtorID & " reset to WIP")
        log_text = "Case reset to WIP"
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Debtor", log_text, Now, log_code)
        Me.Close()
    End Sub

    Private Sub logbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles logbtn.Click
        Try
            debtorID = InputBox("Enter DebtorID", "Display log of chnages for case")
        Catch ex As Exception
            MsgBox("Invalid number")
            Exit Sub
        End Try
        logfrm.ShowDialog()

    End Sub
End Class
Public Class userfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub userfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'update table
        Dim user_rows As Integer = user_grid.Rows.Count
        Dim idx As Integer
        For idx = 0 To user_rows - 1
            Dim user_no As Integer
            Try
                user_no = user_grid.Rows(idx).Cells(0).Value
            Catch ex As Exception
                Continue For
            End Try
            If user_no = 0 Then
                Continue For
            End If
            Try
                'insert row if it does not exist
                logon.LSC_User_tableTableAdapter.FillBy(logon.FeesSQLDataSet.LSC_User_table, user_no)
                If logon.FeesSQLDataSet.LSC_User_table.Rows.Count = 0 Then
                    logon.LSC_User_tableTableAdapter.InsertQuery(user_grid.Rows(idx).Cells(1).Value, "password", _
                        user_grid.Rows(idx).Cells(2).Value, user_grid.Rows(idx).Cells(3).Value)
                Else
                    'update it if it does
                    logon.LSC_User_tableTableAdapter.UpdateQuery1(user_grid.Rows(idx).Cells(1).Value, _
                       user_grid.Rows(idx).Cells(2).Value, user_grid.Rows(idx).Cells(3).Value, user_no)
                End If
                
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Next
    End Sub

    Private Sub userfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.LSC_User_tableTableAdapter.Fill(Me.FeesSQLDataSet.LSC_User_table)

    End Sub

    
    Private Sub user_grid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles user_grid.CellContentClick

    End Sub

    
    Private Sub user_grid_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles user_grid.CellValueChanged
        If e.RowIndex < 0 Then
            Return
        End If
        If e.ColumnIndex = 1 Then
            user_grid.Rows(e.RowIndex).Cells(2).Value = False
            user_grid.Rows(e.RowIndex).Cells(3).Value = False
            user_grid.Rows(e.RowIndex).Cells(4).Value = "password"
        End If
    End Sub
End Class
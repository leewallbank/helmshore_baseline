<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class logon
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Inv_passwordLabel As System.Windows.Forms.Label
        Dim Inv_textLabel As System.Windows.Forms.Label
        Me.Inv_textComboBox = New System.Windows.Forms.ComboBox
        Me.inv_passwordtextbox = New System.Windows.Forms.TextBox
        Me.savebtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.LSC_User_tableBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FeesSQLDataSet = New CapitalandEquity.FeesSQLDataSet
        Me.LSC_User_tableTableAdapter = New CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_User_tableTableAdapter
        Me.LSC_Log_tableTableAdapter = New CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Log_tableTableAdapter
        Inv_passwordLabel = New System.Windows.Forms.Label
        Inv_textLabel = New System.Windows.Forms.Label
        CType(Me.LSC_User_tableBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Inv_passwordLabel
        '
        Inv_passwordLabel.AutoSize = True
        Inv_passwordLabel.Location = New System.Drawing.Point(81, 125)
        Inv_passwordLabel.Name = "Inv_passwordLabel"
        Inv_passwordLabel.Size = New System.Drawing.Size(83, 13)
        Inv_passwordLabel.TabIndex = 8
        Inv_passwordLabel.Text = "Enter password:"
        '
        'Inv_textLabel
        '
        Inv_textLabel.AutoSize = True
        Inv_textLabel.Location = New System.Drawing.Point(81, 27)
        Inv_textLabel.Name = "Inv_textLabel"
        Inv_textLabel.Size = New System.Drawing.Size(93, 13)
        Inv_textLabel.TabIndex = 5
        Inv_textLabel.Text = "Select Your Name"
        '
        'Inv_textComboBox
        '
        Me.Inv_textComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.Inv_textComboBox.FormattingEnabled = True
        Me.Inv_textComboBox.Location = New System.Drawing.Point(84, 68)
        Me.Inv_textComboBox.Name = "Inv_textComboBox"
        Me.Inv_textComboBox.Size = New System.Drawing.Size(121, 21)
        Me.Inv_textComboBox.TabIndex = 4
        '
        'inv_passwordtextbox
        '
        Me.inv_passwordtextbox.Location = New System.Drawing.Point(84, 155)
        Me.inv_passwordtextbox.Name = "inv_passwordtextbox"
        Me.inv_passwordtextbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.inv_passwordtextbox.Size = New System.Drawing.Size(100, 20)
        Me.inv_passwordtextbox.TabIndex = 6
        '
        'savebtn
        '
        Me.savebtn.Location = New System.Drawing.Point(50, 217)
        Me.savebtn.Name = "savebtn"
        Me.savebtn.Size = New System.Drawing.Size(75, 23)
        Me.savebtn.TabIndex = 7
        Me.savebtn.Text = "Enter"
        Me.savebtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.exitbtn.Location = New System.Drawing.Point(193, 217)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 9
        Me.exitbtn.Text = "Quit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'LSC_User_tableBindingSource
        '
        Me.LSC_User_tableBindingSource.DataMember = "LSC User table"
        Me.LSC_User_tableBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LSC_User_tableTableAdapter
        '
        Me.LSC_User_tableTableAdapter.ClearBeforeFill = True
        '
        'LSC_Log_tableTableAdapter
        '
        Me.LSC_Log_tableTableAdapter.ClearBeforeFill = True
        '
        'logon
        '
        Me.AcceptButton = Me.savebtn
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.Inv_textComboBox)
        Me.Controls.Add(Me.inv_passwordtextbox)
        Me.Controls.Add(Me.savebtn)
        Me.Controls.Add(Inv_passwordLabel)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Inv_textLabel)
        Me.Name = "logon"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Capital And Equity Logon"
        CType(Me.LSC_User_tableBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Inv_textComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents inv_passwordtextbox As System.Windows.Forms.TextBox
    Friend WithEvents savebtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents FeesSQLDataSet As CapitalandEquity.FeesSQLDataSet
    Friend WithEvents LSC_User_tableBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LSC_User_tableTableAdapter As CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_User_tableTableAdapter
    Friend WithEvents LSC_Log_tableTableAdapter As CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Log_tableTableAdapter
End Class

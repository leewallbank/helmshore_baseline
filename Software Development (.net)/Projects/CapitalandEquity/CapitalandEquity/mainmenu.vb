Public Class mainmenu

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub casebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles casebtn.Click
        debtorID = 0
        Try
            debtorID = InputBox("Enter case number", "ONESTEP case number entry")
        Catch ex As Exception
            MsgBox("invalid case number - " & debtorID)
            Exit Sub
        End Try
        moved_to_new_case = True
        casefrm.ShowDialog()
    End Sub

    Private Sub mainmenu_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        casefrm.LSC_Status_tableTableAdapter.Fill(casefrm.FeesSQLDataSet.LSC_Status_table)
        log_user_txt.Text = log_user
        'log_user = My.User.Name
        'log_user = Microsoft.VisualBasic.Right(log_user, log_user.Length - InStr(log_user, "\"))
        'log_user_txt.Text = log_user
        myallocbtn.Enabled = True
        all_allocbtn.Enabled = True
        casefrm.LSC_WIP_tableTableAdapter.FillBy(casefrm.FeesSQLDataSet.LSC_WIP_table, log_code)
        Dim alloc_no As Integer = casefrm.FeesSQLDataSet.LSC_WIP_table.Rows.Count
        If alloc_no = 0 Then
            myallocbtn.Enabled = False
            casefrm.LSC_WIP_tableTableAdapter.Fill(casefrm.FeesSQLDataSet.LSC_WIP_table)
            Dim all_alloc_no As Integer = casefrm.FeesSQLDataSet.LSC_WIP_table.Rows.Count
            If all_alloc_no = 0 Then
                all_allocbtn.Enabled = False
            End If
        End If
        Me.LSC_Query_tableTableAdapter.FillBy(Me.FeesSQLDataSet.LSC_Query_table, log_code)
        alloc_no = FeesSQLDataSet.LSC_Query_table.Rows.Count
        If alloc_no = 0 Then
            myquerybtn.Enabled = False
            Me.LSC_Query_tableTableAdapter.FillBy2(Me.FeesSQLDataSet.LSC_Query_table)
            Dim all_alloc_no As Integer = FeesSQLDataSet.LSC_Query_table.Rows.Count
            If all_alloc_no = 0 Then
                allquerybtn.Enabled = False
            End If
        End If


    End Sub

    Private Sub allbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        moved_to_new_case = True
        debtorID = 5261020
        casefrm.ShowDialog()
    End Sub

    Private Sub nextbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nextbtn.Click
        get_next_case()
        casefrm.ShowDialog()
    End Sub

    Private Sub myallocbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles myallocbtn.Click
        allocfrm.ShowDialog()
    End Sub

    Private Sub allallocbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles all_allocbtn.Click
        all_allocfrm.showdialog()
    End Sub

    Private Sub myquerybtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles myquerybtn.Click
        myqueryfrm.ShowDialog()
    End Sub

    Private Sub allquerybtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allquerybtn.Click
        allqueryfrm.ShowDialog()
    End Sub

    Private Sub reportbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reportbtn.Click
        reportfrm.open_queries_rb.Checked = True
        reportfrm.ShowDialog()
    End Sub

    Private Sub adminbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles adminbtn.Click
        adminfrm.ShowDialog()
    End Sub
End Class

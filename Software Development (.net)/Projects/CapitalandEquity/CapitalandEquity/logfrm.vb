Public Class logfrm

    Private Sub logfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.LSC_Log_tableTableAdapter.FillBy(Me.FeesSQLDataSet.LSC_Log_table, debtorID)
        Dim no_of_logs As Integer = Me.FeesSQLDataSet.LSC_Log_table.Rows.Count
        Dim idx As Integer
        log_grid.Rows.Clear()
        Me.Text = "Log for DebtorID = " & debtorID
        For idx = 0 To no_of_logs - 1
            Dim log_type As String = Me.FeesSQLDataSet.LSC_Log_table.Rows(idx).Item(2)
            Dim log_text As String = Me.FeesSQLDataSet.LSC_Log_table.Rows(idx).Item(3)
            Dim log_date As Date = Me.FeesSQLDataSet.LSC_Log_table.Rows(idx).Item(4)
            Dim log_code As Integer = Me.FeesSQLDataSet.LSC_Log_table.Rows(idx).Item(5)
            logon.LSC_User_tableTableAdapter.FillBy(logon.FeesSQLDataSet.LSC_User_table, log_code)
            Dim log_user As String = logon.FeesSQLDataSet.LSC_User_table.Rows(0).Item(1)
            log_grid.Rows.Add(Format(log_date, "dd/MM/yyyy HH:mm:ss"), log_user, log_type, log_text)
        Next
    End Sub
End Class
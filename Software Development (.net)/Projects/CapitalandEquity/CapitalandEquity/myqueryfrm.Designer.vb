<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class myqueryfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.query_grid = New System.Windows.Forms.DataGridView
        Me.query_debtorid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_text = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_response = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.query_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'query_grid
        '
        Me.query_grid.AllowUserToAddRows = False
        Me.query_grid.AllowUserToDeleteRows = False
        Me.query_grid.AllowUserToOrderColumns = True
        Me.query_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.query_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.query_debtorid, Me.query_no, Me.query_name, Me.query_date, Me.query_text, Me.query_response})
        Me.query_grid.Location = New System.Drawing.Point(0, 0)
        Me.query_grid.Name = "query_grid"
        Me.query_grid.Size = New System.Drawing.Size(955, 233)
        Me.query_grid.TabIndex = 0
        '
        'query_debtorid
        '
        Me.query_debtorid.HeaderText = "Debtor"
        Me.query_debtorid.Name = "query_debtorid"
        Me.query_debtorid.ReadOnly = True
        Me.query_debtorid.Width = 50
        '
        'query_no
        '
        Me.query_no.HeaderText = "Query No"
        Me.query_no.Name = "query_no"
        Me.query_no.Width = 40
        '
        'query_name
        '
        Me.query_name.HeaderText = "Name"
        Me.query_name.Name = "query_name"
        Me.query_name.ReadOnly = True
        Me.query_name.Width = 150
        '
        'query_date
        '
        Me.query_date.HeaderText = "Date Added"
        Me.query_date.Name = "query_date"
        Me.query_date.ReadOnly = True
        Me.query_date.Width = 70
        '
        'query_text
        '
        Me.query_text.HeaderText = "Text"
        Me.query_text.Name = "query_text"
        Me.query_text.ReadOnly = True
        Me.query_text.Width = 300
        '
        'query_response
        '
        Me.query_response.HeaderText = "Response"
        Me.query_response.Name = "query_response"
        Me.query_response.Width = 300
        '
        'myqueryfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(955, 266)
        Me.Controls.Add(Me.query_grid)
        Me.Name = "myqueryfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "My Open Queries (Double-click to display case)"
        CType(Me.query_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents query_grid As System.Windows.Forms.DataGridView
    Friend WithEvents query_debtorid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_text As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_response As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

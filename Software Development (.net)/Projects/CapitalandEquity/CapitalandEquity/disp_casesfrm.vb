Public Class disp_casesfrm

    Private Sub disp_casesfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = "Cases reported on " & Format(selected_report_date, "dd/MM/yyyy")
        disp_cases_grid.Rows.Clear()
        Dim idx As Integer
        casefrm.LSC_Debtor_tableTableAdapter.FillBy3(casefrm.FeesSQLDataSet.LSC_Debtor_table, selected_report_date)
        Dim no_of_cases As Integer = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows.Count
        For idx = 0 To no_of_cases - 1
            Dim rep_debtorid As Integer = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(0)
            Dim debtor_name As String = ""
            param2 = "select name_fore, name_sur from Debtor where _rowid = " & rep_debtorid
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 1 Then
                debtor_name = debtor_dataset.Tables(0).Rows(0).Item(0) & " " & _
                   debtor_dataset.Tables(0).Rows(0).Item(1)
            End If
            Dim completed_by_no As Integer = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(12)
            logon.LSC_User_tableTableAdapter.FillBy(logon.FeesSQLDataSet.LSC_User_table, completed_by_no)
            Dim completed_by As String = logon.FeesSQLDataSet.LSC_User_table.Rows(0).Item(1)
            Dim completed_date As Date = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(11)
            disp_cases_grid.Rows.Add(rep_debtorid, debtor_name, completed_by, Format(completed_date, "dd/MM/yyyy"))
        Next
    End Sub
End Class
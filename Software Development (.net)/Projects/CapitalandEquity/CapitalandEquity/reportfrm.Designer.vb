<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class reportfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.exitbtn = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.open_queries_rb = New System.Windows.Forms.RadioButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lsc_rept_rb = New System.Windows.Forms.RadioButton
        Me.runbtn = New System.Windows.Forms.Button
        Me.end_date_picker = New System.Windows.Forms.DateTimePicker
        Me.start_date_picker = New System.Windows.Forms.DateTimePicker
        Me.date_gbox = New System.Windows.Forms.GroupBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.prev_rep_rb = New System.Windows.Forms.RadioButton
        Me.GroupBox1.SuspendLayout()
        Me.date_gbox.SuspendLayout()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(415, 293)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'open_queries_rb
        '
        Me.open_queries_rb.AutoSize = True
        Me.open_queries_rb.Checked = True
        Me.open_queries_rb.Location = New System.Drawing.Point(15, 19)
        Me.open_queries_rb.Name = "open_queries_rb"
        Me.open_queries_rb.Size = New System.Drawing.Size(90, 17)
        Me.open_queries_rb.TabIndex = 3
        Me.open_queries_rb.TabStop = True
        Me.open_queries_rb.Text = "Open Queries"
        Me.open_queries_rb.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.prev_rep_rb)
        Me.GroupBox1.Controls.Add(Me.lsc_rept_rb)
        Me.GroupBox1.Controls.Add(Me.open_queries_rb)
        Me.GroupBox1.Location = New System.Drawing.Point(24, 29)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 141)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Adhoc Reports"
        '
        'lsc_rept_rb
        '
        Me.lsc_rept_rb.AutoSize = True
        Me.lsc_rept_rb.Location = New System.Drawing.Point(15, 42)
        Me.lsc_rept_rb.Name = "lsc_rept_rb"
        Me.lsc_rept_rb.Size = New System.Drawing.Size(114, 17)
        Me.lsc_rept_rb.TabIndex = 4
        Me.lsc_rept_rb.Text = "Create LSC Report"
        Me.lsc_rept_rb.UseVisualStyleBackColor = True
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(79, 293)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(75, 23)
        Me.runbtn.TabIndex = 1
        Me.runbtn.Text = "Run report"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'end_date_picker
        '
        Me.end_date_picker.Location = New System.Drawing.Point(27, 91)
        Me.end_date_picker.Name = "end_date_picker"
        Me.end_date_picker.Size = New System.Drawing.Size(123, 20)
        Me.end_date_picker.TabIndex = 6
        '
        'start_date_picker
        '
        Me.start_date_picker.Location = New System.Drawing.Point(27, 47)
        Me.start_date_picker.Name = "start_date_picker"
        Me.start_date_picker.Size = New System.Drawing.Size(123, 20)
        Me.start_date_picker.TabIndex = 7
        '
        'date_gbox
        '
        Me.date_gbox.Controls.Add(Me.Label2)
        Me.date_gbox.Controls.Add(Me.Label1)
        Me.date_gbox.Controls.Add(Me.end_date_picker)
        Me.date_gbox.Controls.Add(Me.start_date_picker)
        Me.date_gbox.Location = New System.Drawing.Point(277, 48)
        Me.date_gbox.Name = "date_gbox"
        Me.date_gbox.Size = New System.Drawing.Size(200, 129)
        Me.date_gbox.TabIndex = 8
        Me.date_gbox.TabStop = False
        Me.date_gbox.Text = "Dates"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(63, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(55, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Start Date"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(63, 75)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "End date"
        '
        'prev_rep_rb
        '
        Me.prev_rep_rb.AutoSize = True
        Me.prev_rep_rb.Location = New System.Drawing.Point(15, 69)
        Me.prev_rep_rb.Name = "prev_rep_rb"
        Me.prev_rep_rb.Size = New System.Drawing.Size(124, 17)
        Me.prev_rep_rb.TabIndex = 9
        Me.prev_rep_rb.TabStop = True
        Me.prev_rep_rb.Text = "Previous LSC reports"
        Me.prev_rep_rb.UseVisualStyleBackColor = True
        '
        'reportfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(522, 363)
        Me.Controls.Add(Me.date_gbox)
        Me.Controls.Add(Me.runbtn)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "reportfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Run Adhoc Reports"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.date_gbox.ResumeLayout(False)
        Me.date_gbox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents open_queries_rb As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lsc_rept_rb As System.Windows.Forms.RadioButton
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents end_date_picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents start_date_picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents date_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents prev_rep_rb As System.Windows.Forms.RadioButton
End Class

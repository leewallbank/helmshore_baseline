<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class resetpwordfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.resetbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.FeesSQLDataSet = New CapitalandEquity.FeesSQLDataSet
        Me.LSCUserTableBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LSC_User_tableTableAdapter = New CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_User_tableTableAdapter
        Me.user_cbox = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LSCUserTableBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'resetbtn
        '
        Me.resetbtn.Location = New System.Drawing.Point(47, 204)
        Me.resetbtn.Name = "resetbtn"
        Me.resetbtn.Size = New System.Drawing.Size(89, 23)
        Me.resetbtn.TabIndex = 0
        Me.resetbtn.Text = "Reset"
        Me.resetbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(375, 204)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LSCUserTableBindingSource
        '
        Me.LSCUserTableBindingSource.DataMember = "LSC User table"
        Me.LSCUserTableBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'LSC_User_tableTableAdapter
        '
        Me.LSC_User_tableTableAdapter.ClearBeforeFill = True
        '
        'user_cbox
        '
        Me.user_cbox.DataBindings.Add(New System.Windows.Forms.Binding("SelectedValue", Me.LSCUserTableBindingSource, "lsc_user_name", True))
        Me.user_cbox.DataSource = Me.LSCUserTableBindingSource
        Me.user_cbox.DisplayMember = "lsc_user_name"
        Me.user_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.user_cbox.FormattingEnabled = True
        Me.user_cbox.Location = New System.Drawing.Point(150, 86)
        Me.user_cbox.Name = "user_cbox"
        Me.user_cbox.Size = New System.Drawing.Size(192, 21)
        Me.user_cbox.TabIndex = 2
        Me.user_cbox.ValueMember = "lsc_user_name"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(193, 56)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Select person"
        '
        'resetpwordfrm
        '
        Me.AcceptButton = Me.resetbtn
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(477, 266)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.user_cbox)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.resetbtn)
        Me.Name = "resetpwordfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reset Password"
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LSCUserTableBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents resetbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents FeesSQLDataSet As CapitalandEquity.FeesSQLDataSet
    Friend WithEvents LSCUserTableBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LSC_User_tableTableAdapter As CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_User_tableTableAdapter
    Friend WithEvents user_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class

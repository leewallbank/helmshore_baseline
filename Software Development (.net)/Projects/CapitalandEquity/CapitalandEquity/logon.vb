Public Class logon
    Dim user_password, inv_text As String
    Dim user_count, changed_row As Integer
    Dim admin_user, user_enabled, user_admin As Boolean
    Private Sub logon_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.LSC_User_tableTableAdapter.Fill(Me.FeesSQLDataSet.LSC_User_table)
        Dim row As DataRow
        Dim idx As Integer = 0
        Inv_textComboBox.Items.Clear()
        For Each row In FeesSQLDataSet.LSC_User_table.Rows
            user_enabled = FeesSQLDataSet.LSC_User_table.Rows(idx).Item(4)
            If user_enabled Then
                inv_text = FeesSQLDataSet.LSC_User_table.Rows(idx).Item(1)
                Inv_textComboBox.Items.Add(inv_text)
            End If
            idx += 1
        Next
        Inv_textComboBox.SelectedIndex = -1
        Inv_textComboBox.SelectedText = ""
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub savebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles savebtn.Click
        log_user = My.User.Name
        If inv_passwordtextbox.Text <> user_password Then
            MsgBox("The password is not valid")
            user_count += 1
            If user_count > 5 Then
                MsgBox("Contact administrator to reset password")
                Dim oWSH = CreateObject("WScript.Shell")
                log_user = oWSH.ExpandEnvironmentStrings("%USERNAME%")
                log_user = My.User.Name
                Try
                    log_text = "5 invalid attempts at logon - " & Inv_textComboBox.Text & " by computer for " & log_user
                    LSC_Log_tableTableAdapter.InsertQuery(0, "logon", log_text, Now, log_code)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                Me.Close()
            End If
            inv_passwordtextbox.Focus()
            inv_passwordtextbox.SelectAll()
            Exit Sub
        End If
        log_user = Inv_textComboBox.Text
        If inv_passwordtextbox.Text = "password" Then
            password_reset = False
            newpassfrm.ShowDialog()
            If password_reset = False Then
                MsgBox("Password was not set")
                Me.Close()
            Else
                'update password on table
                Try
                    Try
                        LSC_User_tableTableAdapter.UpdateQuery(inv_password, log_code)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                        Exit Sub
                    End Try
                    log_text = "New password set for - " & Inv_textComboBox.Text
                    LSC_Log_tableTableAdapter.InsertQuery(0, "logon", log_text, Now, log_code)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End If
        End If
        'delete any wip that have expired - first where keep is not set
        casefrm.LSC_WIP_tableTableAdapter.DeleteQuery2(Format(Now, "yyyy-MM-dd"), False)
        'update keep to N where keep = Y and keep date before today
        casefrm.LSC_WIP_tableTableAdapter.UpdateQuery1(Format(Now, "yyyy-MM-dd"), False, Format(Now, "yyyy-MM-dd"), True)
        'disable admin if not admin user
        Me.LSC_User_tableTableAdapter.FillBy(Me.FeesSQLDataSet.LSC_User_table, log_code)
        If Me.FeesSQLDataSet.LSC_User_table.Rows(0).Item(3) = False Then
            mainmenu.adminbtn.Enabled = False
        End If
        mainmenu.ShowDialog()
        Me.Close()
    End Sub

    Private Sub Inv_textComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Inv_textComboBox.Validated
        Dim idx As Integer = 0
        Dim row As DataRow
        For Each row In FeesSQLDataSet.LSC_User_table.Rows
            If FeesSQLDataSet.LSC_User_table.Rows(idx).Item(1) = Inv_textComboBox.Text Then
                log_code = FeesSQLDataSet.LSC_User_table.Rows(idx).Item(0)
                log_user = FeesSQLDataSet.LSC_User_table.Rows(idx).Item(1)
                admin_user = FeesSQLDataSet.LSC_User_table.Rows(idx).Item(3)
                user_password = FeesSQLDataSet.LSC_User_table.Rows(idx).Item(2)
                user_enabled = FeesSQLDataSet.LSC_User_table.Rows(idx).Item(4)
                changed_row = idx
                Exit For
            End If
            idx += 1
        Next
        inv_passwordtextbox.Text = ""
    End Sub

    Private Sub Inv_textComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Inv_textComboBox.SelectedIndexChanged

    End Sub
End Class
Public Class resetpwordfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub resetpwordfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FeesSQLDataSet.LSC_User_table' table. You can move, or remove it, as needed.
        Me.LSC_User_tableTableAdapter.Fill(Me.FeesSQLDataSet.LSC_User_table)

    End Sub

    Private Sub resetbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles resetbtn.Click
        Dim selected_user As String = user_cbox.SelectedValue
        Me.LSC_User_tableTableAdapter.FillBy1(Me.FeesSQLDataSet.LSC_User_table, selected_user)
        Dim selected_log_code = Me.FeesSQLDataSet.LSC_User_table.Rows(0).Item(0)
        Try
            LSC_User_tableTableAdapter.UpdateQuery("password", selected_log_code)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Exit Sub
        End Try
        log_text = "Password reset for - " & selected_user
        logon.LSC_Log_tableTableAdapter.InsertQuery(0, "logon", log_text, Now, log_code)
        MsgBox("Password reset to 'password' for " & selected_user)
        Me.Close()
    End Sub
End Class
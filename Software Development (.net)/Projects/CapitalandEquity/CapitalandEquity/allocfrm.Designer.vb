<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class allocfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.wip_grid = New System.Windows.Forms.DataGridView
        Me.alloc_debtor = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.alloc_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.alloc_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.alloc_keep = New System.Windows.Forms.DataGridViewCheckBoxColumn
        CType(Me.wip_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'wip_grid
        '
        Me.wip_grid.AllowUserToAddRows = False
        Me.wip_grid.AllowUserToDeleteRows = False
        Me.wip_grid.AllowUserToOrderColumns = True
        Me.wip_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.wip_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.alloc_debtor, Me.alloc_name, Me.alloc_date, Me.alloc_keep})
        Me.wip_grid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wip_grid.Location = New System.Drawing.Point(0, 0)
        Me.wip_grid.Name = "wip_grid"
        Me.wip_grid.Size = New System.Drawing.Size(588, 266)
        Me.wip_grid.TabIndex = 0
        '
        'alloc_debtor
        '
        Me.alloc_debtor.HeaderText = "DebtorID"
        Me.alloc_debtor.Name = "alloc_debtor"
        Me.alloc_debtor.ReadOnly = True
        '
        'alloc_name
        '
        Me.alloc_name.HeaderText = "Name"
        Me.alloc_name.Name = "alloc_name"
        Me.alloc_name.ReadOnly = True
        Me.alloc_name.Width = 200
        '
        'alloc_date
        '
        Me.alloc_date.HeaderText = "Allocated Date"
        Me.alloc_date.Name = "alloc_date"
        Me.alloc_date.ReadOnly = True
        '
        'alloc_keep
        '
        Me.alloc_keep.HeaderText = "Keep another day"
        Me.alloc_keep.Name = "alloc_keep"
        '
        'allocfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(588, 266)
        Me.Controls.Add(Me.wip_grid)
        Me.Name = "allocfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cases allocated to me (Double-click to select)"
        CType(Me.wip_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents wip_grid As System.Windows.Forms.DataGridView
    Friend WithEvents alloc_debtor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents alloc_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents alloc_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents alloc_keep As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class

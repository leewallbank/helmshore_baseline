<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainmenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.casebtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.log_user_txt = New System.Windows.Forms.Label
        Me.myallocbtn = New System.Windows.Forms.Button
        Me.myquerybtn = New System.Windows.Forms.Button
        Me.all_allocbtn = New System.Windows.Forms.Button
        Me.allquerybtn = New System.Windows.Forms.Button
        Me.adminbtn = New System.Windows.Forms.Button
        Me.reportbtn = New System.Windows.Forms.Button
        Me.nextbtn = New System.Windows.Forms.Button
        Me.FeesSQLDataSet = New CapitalandEquity.FeesSQLDataSet
        Me.LSC_Query_tableBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LSC_Query_tableTableAdapter = New CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Query_tableTableAdapter
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LSC_Query_tableBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'casebtn
        '
        Me.casebtn.Location = New System.Drawing.Point(28, 154)
        Me.casebtn.Name = "casebtn"
        Me.casebtn.Size = New System.Drawing.Size(124, 23)
        Me.casebtn.TabIndex = 4
        Me.casebtn.Text = "Display Case details"
        Me.casebtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(317, 302)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 8
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'log_user_txt
        '
        Me.log_user_txt.AutoSize = True
        Me.log_user_txt.Location = New System.Drawing.Point(12, 9)
        Me.log_user_txt.Name = "log_user_txt"
        Me.log_user_txt.Size = New System.Drawing.Size(44, 13)
        Me.log_user_txt.TabIndex = 2
        Me.log_user_txt.Text = "log user"
        '
        'myallocbtn
        '
        Me.myallocbtn.Location = New System.Drawing.Point(28, 58)
        Me.myallocbtn.Name = "myallocbtn"
        Me.myallocbtn.Size = New System.Drawing.Size(97, 23)
        Me.myallocbtn.TabIndex = 0
        Me.myallocbtn.Text = "Allocated to me"
        Me.myallocbtn.UseVisualStyleBackColor = True
        '
        'myquerybtn
        '
        Me.myquerybtn.Location = New System.Drawing.Point(28, 99)
        Me.myquerybtn.Name = "myquerybtn"
        Me.myquerybtn.Size = New System.Drawing.Size(124, 23)
        Me.myquerybtn.TabIndex = 2
        Me.myquerybtn.Text = "My Open Queries"
        Me.myquerybtn.UseVisualStyleBackColor = True
        '
        'all_allocbtn
        '
        Me.all_allocbtn.Location = New System.Drawing.Point(218, 58)
        Me.all_allocbtn.Name = "all_allocbtn"
        Me.all_allocbtn.Size = New System.Drawing.Size(106, 23)
        Me.all_allocbtn.TabIndex = 1
        Me.all_allocbtn.Text = "All allocated cases"
        Me.all_allocbtn.UseVisualStyleBackColor = True
        '
        'allquerybtn
        '
        Me.allquerybtn.Location = New System.Drawing.Point(218, 99)
        Me.allquerybtn.Name = "allquerybtn"
        Me.allquerybtn.Size = New System.Drawing.Size(106, 23)
        Me.allquerybtn.TabIndex = 3
        Me.allquerybtn.Text = "All Open Queries"
        Me.allquerybtn.UseVisualStyleBackColor = True
        '
        'adminbtn
        '
        Me.adminbtn.Location = New System.Drawing.Point(218, 220)
        Me.adminbtn.Name = "adminbtn"
        Me.adminbtn.Size = New System.Drawing.Size(106, 23)
        Me.adminbtn.TabIndex = 7
        Me.adminbtn.Text = "Admin"
        Me.adminbtn.UseVisualStyleBackColor = True
        '
        'reportbtn
        '
        Me.reportbtn.Location = New System.Drawing.Point(28, 220)
        Me.reportbtn.Name = "reportbtn"
        Me.reportbtn.Size = New System.Drawing.Size(124, 23)
        Me.reportbtn.TabIndex = 6
        Me.reportbtn.Text = "Reports"
        Me.reportbtn.UseVisualStyleBackColor = True
        '
        'nextbtn
        '
        Me.nextbtn.Location = New System.Drawing.Point(218, 154)
        Me.nextbtn.Name = "nextbtn"
        Me.nextbtn.Size = New System.Drawing.Size(106, 23)
        Me.nextbtn.TabIndex = 5
        Me.nextbtn.Text = "Get next case"
        Me.nextbtn.UseVisualStyleBackColor = True
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LSC_Query_tableBindingSource
        '
        Me.LSC_Query_tableBindingSource.DataMember = "LSC Query table"
        Me.LSC_Query_tableBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'LSC_Query_tableTableAdapter
        '
        Me.LSC_Query_tableTableAdapter.ClearBeforeFill = True
        '
        'mainmenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(421, 362)
        Me.Controls.Add(Me.nextbtn)
        Me.Controls.Add(Me.reportbtn)
        Me.Controls.Add(Me.adminbtn)
        Me.Controls.Add(Me.allquerybtn)
        Me.Controls.Add(Me.all_allocbtn)
        Me.Controls.Add(Me.myquerybtn)
        Me.Controls.Add(Me.myallocbtn)
        Me.Controls.Add(Me.log_user_txt)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.casebtn)
        Me.Name = "mainmenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Capital and Equity"
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LSC_Query_tableBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents casebtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents log_user_txt As System.Windows.Forms.Label
    Friend WithEvents myallocbtn As System.Windows.Forms.Button
    Friend WithEvents myquerybtn As System.Windows.Forms.Button
    Friend WithEvents all_allocbtn As System.Windows.Forms.Button
    Friend WithEvents allquerybtn As System.Windows.Forms.Button
    Friend WithEvents adminbtn As System.Windows.Forms.Button
    Friend WithEvents reportbtn As System.Windows.Forms.Button
    Friend WithEvents nextbtn As System.Windows.Forms.Button
    Friend WithEvents FeesSQLDataSet As CapitalandEquity.FeesSQLDataSet
    Friend WithEvents LSC_Query_tableBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LSC_Query_tableTableAdapter As CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Query_tableTableAdapter

End Class

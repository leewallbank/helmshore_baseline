<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adminfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pwordbtn = New System.Windows.Forms.Button
        Me.resetbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.userbtn = New System.Windows.Forms.Button
        Me.logbtn = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'pwordbtn
        '
        Me.pwordbtn.Location = New System.Drawing.Point(53, 104)
        Me.pwordbtn.Name = "pwordbtn"
        Me.pwordbtn.Size = New System.Drawing.Size(117, 23)
        Me.pwordbtn.TabIndex = 1
        Me.pwordbtn.Text = "Reset password"
        Me.pwordbtn.UseVisualStyleBackColor = True
        '
        'resetbtn
        '
        Me.resetbtn.Location = New System.Drawing.Point(53, 159)
        Me.resetbtn.Name = "resetbtn"
        Me.resetbtn.Size = New System.Drawing.Size(117, 23)
        Me.resetbtn.TabIndex = 2
        Me.resetbtn.Text = "Reset case to WIP"
        Me.resetbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(205, 289)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 4
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'userbtn
        '
        Me.userbtn.Location = New System.Drawing.Point(53, 49)
        Me.userbtn.Name = "userbtn"
        Me.userbtn.Size = New System.Drawing.Size(117, 23)
        Me.userbtn.TabIndex = 0
        Me.userbtn.Text = "Update users"
        Me.userbtn.UseVisualStyleBackColor = True
        '
        'logbtn
        '
        Me.logbtn.Location = New System.Drawing.Point(53, 216)
        Me.logbtn.Name = "logbtn"
        Me.logbtn.Size = New System.Drawing.Size(117, 23)
        Me.logbtn.TabIndex = 3
        Me.logbtn.Text = "Display Log"
        Me.logbtn.UseVisualStyleBackColor = True
        '
        'adminfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 354)
        Me.Controls.Add(Me.logbtn)
        Me.Controls.Add(Me.userbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.resetbtn)
        Me.Controls.Add(Me.pwordbtn)
        Me.Name = "adminfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Admin Functions"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pwordbtn As System.Windows.Forms.Button
    Friend WithEvents resetbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents userbtn As System.Windows.Forms.Button
    Friend WithEvents logbtn As System.Windows.Forms.Button
End Class

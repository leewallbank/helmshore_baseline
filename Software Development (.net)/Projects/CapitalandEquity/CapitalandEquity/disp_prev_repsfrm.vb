Public Class disp_prev_repsfrm

    Private Sub disp_prev_repsfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        prev_reps_grid.Rows.Clear()
        Dim idx As Integer
        Dim report_date As Date
        casefrm.LSC_Debtor_tableTableAdapter.FillBy2(casefrm.FeesSQLDataSet.LSC_Debtor_table, reportfrm.start_date_picker.Value, reportfrm.end_date_picker.Value)
        Dim no_of_dates As Integer = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows.Count
        For idx = 0 To no_of_dates - 1
            If idx = 0 Then
                report_date = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(15)
            Else
                If report_date = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(15) Then
                    Continue For
                End If
                report_date = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(15)
            End If
            Dim no_of_cases As Integer = casefrm.LSC_Debtor_tableTableAdapter.ScalarQuery(report_date)
            prev_reps_grid.Rows.Add(Format(report_date, "dd/MM/yyyy"), no_of_cases)
        Next
    End Sub

    Private Sub prev_reps_grid_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles prev_reps_grid.CellDoubleClick
        'display list of cases
        selected_report_date = prev_reps_grid.Rows(e.RowIndex).Cells(0).Value
        disp_casesfrm.ShowDialog()
    End Sub
End Class
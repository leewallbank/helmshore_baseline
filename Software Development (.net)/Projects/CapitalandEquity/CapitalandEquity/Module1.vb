Module Module1
    Public debtorID, log_code, status_no, bank_details_idx, credit_score, no_of_bank_accts_decl As Integer
    Public param2, inv_password, log_user, log_text, match_str, prop_comments_entered As String
    Public display_mode, password_reset, moved_to_new_case, case_completed As Boolean
    Public tot_cap_assets, tot_calc_cap_assets As Decimal
    Public selected_report_date As Date
    Public Sub allocate_to_me()
        Try
            casefrm.LSC_WIP_tableTableAdapter.DeleteQuery1(debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Try
            casefrm.LSC_WIP_tableTableAdapter.InsertQuery(log_code, debtorID, Now, False)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        casefrm.wip_txt.Text = "Allocated to me"
        mainmenu.myallocbtn.Enabled = True
        log_text = "Allocated case"
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Allocate", log_text, Now, log_code)
        allow_grid_update()

        'check if a debtor record exists
        casefrm.LSC_Debtor_tableTableAdapter.FillBy(casefrm.FeesSQLDataSet.LSC_Debtor_table, debtorID)
        If casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows.Count = 0 Then
            Try
                casefrm.LSC_Debtor_tableTableAdapter.InsertQuery1(debtorID, 1, Now, log_code, True)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            Try
                casefrm.LSC_Debtor_tableTableAdapter.UpdateQuery21(1, debtorID)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            get_debtor_details()

        End If
        casefrm.status_txt.Text = "WIP"
        status_no = 1
        mainmenu.myallocbtn.Enabled = True
        mainmenu.all_allocbtn.Enabled = True
        casefrm.exp_check_cbox.Enabled = True
        casefrm.match_cbox.Enabled = True
        casefrm.undec_bank_cbox.Enabled = True
        casefrm.cred_score_ud.Enabled = True
        casefrm.cred_last_year_cbox.Enabled = True
        casefrm.cons_pos_cbox.Enabled = True
        casefrm.tot_cap_assets_cbox.Enabled = True
        casefrm.validatebtn.Enabled = True
        casefrm.completebtn.Enabled = False
        casefrm.tomebtn.Enabled = False
        casefrm.tot_cap_assets_cbox.Enabled = True
        casefrm.parents_cbox.Enabled = True
        casefrm.partner_cbox.Enabled = True
        casefrm.same_surname_cbox.Enabled = True
        casefrm.prop_comments_tbox.Enabled = True
        casefrm.bank_acct_ud.Enabled = True
    End Sub
    Public Sub get_debtor_details()
        casefrm.LSC_Debtor_tableTableAdapter.FillBy(casefrm.FeesSQLDataSet.LSC_Debtor_table, debtorID)
        casefrm.status_txt.Text = "Never Allocated"
        Dim exp_check As Boolean = True
        Dim debt_no_of_props As Integer = 0
        Dim debt_no_of_bank_accts As Integer = 0
        Dim debt_no_of_credit_agreements As Integer = 0
        match_str = "NA"
        credit_score = 0
        tot_cap_assets = 0
        no_of_bank_accts_decl = 0
        Dim cred_last_year As Boolean = False
        Dim undec_bank_acct As Boolean = False
        Dim cons_position As Boolean = False
        Dim prop_comments As String = ""
        Dim owned_by_parents As Boolean = False
        Dim owned_by_partner As Boolean = False
        Dim owned_same_surname As Boolean = False
        Dim comp_date As Date = Nothing
        If bank_details_idx > 0 Then
            no_of_bank_accts_decl = 1
        End If


        case_completed = False
        If casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows.Count = 1 Then
            status_no = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(10)
            If status_no > 1 Then
                case_completed = True
            End If
            casefrm.status_txt.Text = casefrm.FeesSQLDataSet.LSC_Status_table.Rows(status_no).Item(1)
            exp_check = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(1)
            Try
                match_str = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(2)
            Catch ex As Exception
                match_str = "NA"
            End Try
            Try
                no_of_bank_accts_decl = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(5)
            Catch ex As Exception
                no_of_bank_accts_decl = 0
            End Try
            Try
                credit_score = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(6)
            Catch ex As Exception
                credit_score = 0
            End Try
            Try
                cred_last_year = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(7)
            Catch ex As Exception
                cred_last_year = False
            End Try
            Try
                undec_bank_acct = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(8)
            Catch ex As Exception
                undec_bank_acct = False
            End Try
            Try
                cons_position = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(9)
            Catch ex As Exception
                cons_position = False
            End Try
            Try
                comp_date = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(11)
            Catch ex As Exception
                comp_date = Nothing
            End Try
            Try
                tot_cap_assets = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(18)
            Catch ex As Exception
                tot_cap_assets = 0
            End Try
            Try
                owned_by_parents = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(28)
            Catch ex As Exception
                owned_by_parents = False
            End Try
            Try
                owned_by_partner = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(29)
            Catch ex As Exception
                owned_by_partner = False
            End Try
            Try
                owned_same_surname = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(30)
            Catch ex As Exception
                owned_same_surname = False
            End Try
            Try
                prop_comments = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(31)
            Catch ex As Exception
                prop_comments = ""
            End Try
            Try
                no_of_bank_accts_decl = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(33)
            Catch ex As Exception

            End Try
        End If
        casefrm.exp_check_cbox.Checked = exp_check
        casefrm.cred_score_ud.Value = credit_score
        casefrm.cons_pos_cbox.Checked = cons_position
        casefrm.match_cbox.Text = match_str
        casefrm.undec_bank_cbox.Checked = undec_bank_acct
        casefrm.cred_last_year_cbox.Checked = cred_last_year
        casefrm.cons_pos_cbox.Checked = cons_position
        casefrm.parents_cbox.Checked = owned_by_parents
        casefrm.partner_cbox.Checked = owned_by_partner
        casefrm.same_surname_cbox.Checked = owned_same_surname
        casefrm.prop_comments_tbox.Text = prop_comments
        casefrm.bank_acct_ud.Value = no_of_bank_accts_decl
        casefrm.tot_cap_assets_cbox.Items.Clear()
        casefrm.tot_cap_assets_cbox.Items.Add(tot_cap_assets)
        casefrm.tot_cap_assets_cbox.Items.Add(tot_calc_cap_assets)
        casefrm.tot_cap_assets_cbox.SelectedIndex = 0
        If comp_date = Nothing Then
            casefrm.comp_date_txt.Text = ""
        Else
            casefrm.comp_date_txt.Text = "Completed on " & Format(comp_date, "dd/MM/yyyy")
        End If
        moved_to_new_case = False

    End Sub

    Public Sub get_next_case()
        param2 = "select _rowid from Debtor where status_open_closed = 'O'" & _
                " and status = 'L' and (clientschemeID = 1892 or clientschemeID = 2110)" & _
                " order by _createdDate"
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("No more cases found")
            Exit Sub
        End If
        moved_to_new_case = True
        case_completed = False
        casefrm.prop_grid.Rows.Clear()
        Dim debtor_rows As Integer = no_of_rows - 1
        Dim idx As Integer
        Dim case_found As Boolean = False
        For idx = 0 To debtor_rows
            casefrm.nextbtn.Enabled = False
            Application.DoEvents()
            debtorID = debtor_dataset.Tables(0).Rows(idx).Item(0)
            'check not allocated to anyone
            casefrm.LSC_WIP_tableTableAdapter.FillBy1(casefrm.FeesSQLDataSet.LSC_WIP_table, debtorID)
            Dim user_no As Integer
            Try
                user_no = casefrm.FeesSQLDataSet.LSC_WIP_table.Rows(0).Item(0)
            Catch ex As Exception
                case_found = True
            End Try
            If Not case_found Then
                Continue For
            End If
            'check not completed already
            casefrm.LSC_Debtor_tableTableAdapter.FillBy(casefrm.FeesSQLDataSet.LSC_Debtor_table, debtorID)
            Try
                Dim status As Integer = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(0).Item(10)
                If status > 1 Then
                    case_found = False
                    Continue For
                End If
            Catch ex As Exception

            End Try
            Exit For
        Next
        casefrm.wait_txt.Text = ""
        casefrm.nextbtn.Enabled = True
        If case_found Then
            allocate_to_me()
        Else
            MsgBox("No unallocated cases found")
        End If
    End Sub
    Public Sub allow_grid_update()
        Dim idx As Integer
        Dim no_of_rows As Integer = casefrm.prop_grid.Rows.Count
        casefrm.prop_grid.ReadOnly = False
        For idx = 0 To no_of_rows - 1
            casefrm.prop_grid.Rows(idx).Cells(6).ReadOnly = True
            casefrm.prop_grid.Rows(idx).Cells(7).ReadOnly = True
        Next
        no_of_rows = casefrm.bank_grid.Rows.Count
        casefrm.bank_grid.ReadOnly = False
        For idx = 0 To no_of_rows - 1
            casefrm.bank_grid.Rows(idx).Cells(3).ReadOnly = True
            casefrm.bank_grid.Rows(idx).Cells(4).ReadOnly = True
        Next
        no_of_rows = casefrm.cred_grid.Rows.Count
        casefrm.cred_grid.ReadOnly = False
        For idx = 0 To no_of_rows - 1
            casefrm.cred_grid.Rows(idx).Cells(7).ReadOnly = True
            casefrm.cred_grid.Rows(idx).Cells(8).ReadOnly = True
        Next
        no_of_rows = casefrm.query_grid.Rows.Count
        casefrm.query_grid.ReadOnly = False
        For idx = 0 To no_of_rows - 1
            casefrm.query_grid.Rows(idx).Cells(0).ReadOnly = True
            casefrm.query_grid.Rows(idx).Cells(2).ReadOnly = True
            casefrm.query_grid.Rows(idx).Cells(3).ReadOnly = True
            casefrm.query_grid.Rows(idx).Cells(6).ReadOnly = True
            casefrm.query_grid.Rows(idx).Cells(7).ReadOnly = True
        Next
    End Sub
End Module


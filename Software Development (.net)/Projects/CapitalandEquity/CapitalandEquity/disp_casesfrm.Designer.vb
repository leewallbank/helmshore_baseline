<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class disp_casesfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.disp_cases_grid = New System.Windows.Forms.DataGridView
        Me.rep_debtorID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.rep_debtor_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.rep_Completed_by = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.rep_completed_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.disp_cases_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'disp_cases_grid
        '
        Me.disp_cases_grid.AllowUserToAddRows = False
        Me.disp_cases_grid.AllowUserToDeleteRows = False
        Me.disp_cases_grid.AllowUserToOrderColumns = True
        Me.disp_cases_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.disp_cases_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.rep_debtorID, Me.rep_debtor_name, Me.rep_Completed_by, Me.rep_completed_date})
        Me.disp_cases_grid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.disp_cases_grid.Location = New System.Drawing.Point(0, 0)
        Me.disp_cases_grid.Name = "disp_cases_grid"
        Me.disp_cases_grid.ReadOnly = True
        Me.disp_cases_grid.Size = New System.Drawing.Size(627, 342)
        Me.disp_cases_grid.TabIndex = 0
        '
        'rep_debtorID
        '
        Me.rep_debtorID.HeaderText = "DebtorID"
        Me.rep_debtorID.Name = "rep_debtorID"
        Me.rep_debtorID.ReadOnly = True
        '
        'rep_debtor_name
        '
        Me.rep_debtor_name.HeaderText = "Name"
        Me.rep_debtor_name.Name = "rep_debtor_name"
        Me.rep_debtor_name.ReadOnly = True
        Me.rep_debtor_name.Width = 250
        '
        'rep_Completed_by
        '
        Me.rep_Completed_by.HeaderText = "Completed By"
        Me.rep_Completed_by.Name = "rep_Completed_by"
        Me.rep_Completed_by.ReadOnly = True
        '
        'rep_completed_date
        '
        Me.rep_completed_date.HeaderText = "Completed Date"
        Me.rep_completed_date.Name = "rep_completed_date"
        Me.rep_completed_date.ReadOnly = True
        '
        'disp_casesfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(627, 342)
        Me.Controls.Add(Me.disp_cases_grid)
        Me.Name = "disp_casesfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cases on report date"
        CType(Me.disp_cases_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents disp_cases_grid As System.Windows.Forms.DataGridView
    Friend WithEvents rep_debtorID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rep_debtor_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rep_Completed_by As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rep_completed_date As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

Public Class allqueryfrm

    Private Sub allqueryfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        mainmenu.LSC_Query_tableTableAdapter.FillBy2(mainmenu.FeesSQLDataSet.LSC_Query_table)
        queryall_grid.Rows.Clear()
        Dim no_of_queries As Integer = mainmenu.FeesSQLDataSet.LSC_Query_table.Rows.Count
        Dim idx As Integer
        For idx = 0 To no_of_queries - 1
            Dim query_debtorid As Integer = mainmenu.FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(0)
            Dim query_no As Integer = mainmenu.FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(1)
            Dim query_text As String = mainmenu.FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(2)
            Dim query_date As Date = mainmenu.FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(3)
            param2 = "select name_sur, name_fore from Debtor where _rowid = " & query_debtorid
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            Dim query_debtor_name As String = ""
            Try
                query_debtor_name = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
            Catch ex As Exception
            End Try
            Try
                query_debtor_name = query_debtor_name & " " & Trim(debtor_dataset.Tables(0).Rows(0).Item(0))
            Catch ex As Exception
            End Try
            Dim query_user_no As Integer = mainmenu.FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(4)
            logon.LSC_User_tableTableAdapter.FillBy(logon.FeesSQLDataSet.LSC_User_table, query_user_no)
            Dim query_user As String = logon.FeesSQLDataSet.LSC_User_table.Rows(0).Item(1)
            Dim query_response As String = ""
            Try
                query_response = mainmenu.FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(5)
            Catch ex As Exception

            End Try

            queryall_grid.Rows.Add(query_debtorid, query_no, query_debtor_name, query_text, Format(query_date, "dd/MM/yyyy"), query_user, query_response)
        Next
    End Sub

    Private Sub queryall_grid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles queryall_grid.CellContentClick

    End Sub


    Private Sub queryall_grid_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles queryall_grid.CellDoubleClick
        debtorID = queryall_grid.Rows(e.RowIndex).Cells(0).Value
        moved_to_new_case = True
        casefrm.ShowDialog()
        Me.Close()
    End Sub
End Class
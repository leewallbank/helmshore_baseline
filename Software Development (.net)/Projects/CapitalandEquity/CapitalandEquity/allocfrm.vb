Public Class allocfrm

    Private Sub allocfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        casefrm.LSC_WIP_tableTableAdapter.FillBy(casefrm.FeesSQLDataSet.LSC_WIP_table, log_code)
        wip_grid.Rows.Clear()
        Dim no_of_wips As Integer = casefrm.FeesSQLDataSet.LSC_WIP_table.Rows.Count
        Dim idx As Integer
        For idx = 0 To no_of_wips - 1
            Dim wip_debtorid As Decimal = casefrm.FeesSQLDataSet.LSC_WIP_table.Rows(idx).Item(1)
            Dim wip_date As Date = casefrm.FeesSQLDataSet.LSC_WIP_table.Rows(idx).Item(2)
            Dim wip_keep As Boolean = casefrm.FeesSQLDataSet.LSC_WIP_table.Rows(idx).Item(3)
            param2 = "select name_sur, name_fore from Debtor where _rowid = " & wip_debtorid
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            Dim wip_debtor_name As String = ""
            Try
                wip_debtor_name = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
            Catch ex As Exception

            End Try
            Try
                wip_debtor_name = wip_debtor_name & " " & Trim(debtor_dataset.Tables(0).Rows(0).Item(0))
            Catch ex As Exception

            End Try
            wip_grid.Rows.Add(wip_debtorid, wip_debtor_name, Format(wip_date, "dd/MM/yyyy"), wip_keep)
        Next
    End Sub

    Private Sub wip_grid_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles wip_grid.CellDoubleClick
        debtorID = wip_grid.Rows(e.RowIndex).Cells(0).Value
        moved_to_new_case = True
        casefrm.ShowDialog()
        Me.Close()
    End Sub

    Private Sub wip_grid_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles wip_grid.CellValueChanged
        If e.RowIndex < 0 Or e.ColumnIndex <> 3 Then
            Exit Sub
        End If
        Try
            casefrm.LSC_WIP_tableTableAdapter.UpdateQuery(wip_grid.Rows(e.RowIndex).Cells(3).Value, log_code, wip_grid.Rows(e.RowIndex).Cells(0).Value)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    
    Private Sub wip_grid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles wip_grid.CellContentClick

    End Sub
End Class
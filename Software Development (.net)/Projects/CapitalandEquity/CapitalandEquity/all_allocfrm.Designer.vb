<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class all_allocfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.wipall_grid = New System.Windows.Forms.DataGridView
        Me.all_debtorid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.all_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.all_allocated_to = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.all_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.all_keep = New System.Windows.Forms.DataGridViewCheckBoxColumn
        CType(Me.wipall_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'wipall_grid
        '
        Me.wipall_grid.AllowUserToAddRows = False
        Me.wipall_grid.AllowUserToDeleteRows = False
        Me.wipall_grid.AllowUserToOrderColumns = True
        Me.wipall_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.wipall_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.all_debtorid, Me.all_name, Me.all_allocated_to, Me.all_date, Me.all_keep})
        Me.wipall_grid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wipall_grid.Location = New System.Drawing.Point(0, 0)
        Me.wipall_grid.Name = "wipall_grid"
        Me.wipall_grid.Size = New System.Drawing.Size(643, 266)
        Me.wipall_grid.TabIndex = 0
        '
        'all_debtorid
        '
        Me.all_debtorid.HeaderText = "DebtorID"
        Me.all_debtorid.Name = "all_debtorid"
        Me.all_debtorid.ReadOnly = True
        Me.all_debtorid.Width = 50
        '
        'all_name
        '
        Me.all_name.HeaderText = "Debtor name"
        Me.all_name.Name = "all_name"
        Me.all_name.ReadOnly = True
        Me.all_name.Width = 250
        '
        'all_allocated_to
        '
        Me.all_allocated_to.HeaderText = "Allocated To"
        Me.all_allocated_to.Name = "all_allocated_to"
        Me.all_allocated_to.ReadOnly = True
        '
        'all_date
        '
        Me.all_date.HeaderText = "Allocated Date"
        Me.all_date.Name = "all_date"
        Me.all_date.ReadOnly = True
        Me.all_date.Width = 75
        '
        'all_keep
        '
        Me.all_keep.HeaderText = "Keep another day"
        Me.all_keep.Name = "all_keep"
        Me.all_keep.ReadOnly = True
        Me.all_keep.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.all_keep.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'all_allocfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(643, 266)
        Me.Controls.Add(Me.wipall_grid)
        Me.Name = "all_allocfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "All Allocated cases"
        CType(Me.wipall_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents wipall_grid As System.Windows.Forms.DataGridView
    Friend WithEvents all_debtorid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents all_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents all_allocated_to As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents all_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents all_keep As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class userfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.exitbtn = New System.Windows.Forms.Button
        Me.user_grid = New System.Windows.Forms.DataGridView
        Me.LSCUserTableBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FeesSQLDataSet = New CapitalandEquity.FeesSQLDataSet
        Me.LSC_User_tableTableAdapter = New CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_User_tableTableAdapter
        Me.LscusernoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.LscusernameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.LscuseradminDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.LscusrenabledDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.lsc_user_password = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.user_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LSCUserTableBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(682, 231)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 0
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'user_grid
        '
        Me.user_grid.AllowUserToDeleteRows = False
        Me.user_grid.AllowUserToOrderColumns = True
        Me.user_grid.AutoGenerateColumns = False
        Me.user_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.user_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LscusernoDataGridViewTextBoxColumn, Me.LscusernameDataGridViewTextBoxColumn, Me.LscuseradminDataGridViewCheckBoxColumn, Me.LscusrenabledDataGridViewCheckBoxColumn, Me.lsc_user_password})
        Me.user_grid.DataSource = Me.LSCUserTableBindingSource
        Me.user_grid.Location = New System.Drawing.Point(0, 0)
        Me.user_grid.Name = "user_grid"
        Me.user_grid.Size = New System.Drawing.Size(464, 244)
        Me.user_grid.TabIndex = 1
        '
        'LSCUserTableBindingSource
        '
        Me.LSCUserTableBindingSource.DataMember = "LSC User table"
        Me.LSCUserTableBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LSC_User_tableTableAdapter
        '
        Me.LSC_User_tableTableAdapter.ClearBeforeFill = True
        '
        'LscusernoDataGridViewTextBoxColumn
        '
        Me.LscusernoDataGridViewTextBoxColumn.DataPropertyName = "lsc_user_no"
        Me.LscusernoDataGridViewTextBoxColumn.HeaderText = "User_no"
        Me.LscusernoDataGridViewTextBoxColumn.Name = "LscusernoDataGridViewTextBoxColumn"
        Me.LscusernoDataGridViewTextBoxColumn.ReadOnly = True
        Me.LscusernoDataGridViewTextBoxColumn.Visible = False
        Me.LscusernoDataGridViewTextBoxColumn.Width = 50
        '
        'LscusernameDataGridViewTextBoxColumn
        '
        Me.LscusernameDataGridViewTextBoxColumn.DataPropertyName = "lsc_user_name"
        Me.LscusernameDataGridViewTextBoxColumn.HeaderText = "Name"
        Me.LscusernameDataGridViewTextBoxColumn.Name = "LscusernameDataGridViewTextBoxColumn"
        Me.LscusernameDataGridViewTextBoxColumn.Width = 200
        '
        'LscuseradminDataGridViewCheckBoxColumn
        '
        Me.LscuseradminDataGridViewCheckBoxColumn.DataPropertyName = "lsc_user_admin"
        Me.LscuseradminDataGridViewCheckBoxColumn.HeaderText = "Admin"
        Me.LscuseradminDataGridViewCheckBoxColumn.Name = "LscuseradminDataGridViewCheckBoxColumn"
        Me.LscuseradminDataGridViewCheckBoxColumn.Width = 50
        '
        'LscusrenabledDataGridViewCheckBoxColumn
        '
        Me.LscusrenabledDataGridViewCheckBoxColumn.DataPropertyName = "lsc_usr_enabled"
        Me.LscusrenabledDataGridViewCheckBoxColumn.HeaderText = "Enabled"
        Me.LscusrenabledDataGridViewCheckBoxColumn.Name = "LscusrenabledDataGridViewCheckBoxColumn"
        '
        'lsc_user_password
        '
        Me.lsc_user_password.DataPropertyName = "lsc_user_password"
        Me.lsc_user_password.HeaderText = "lsc_user_password"
        Me.lsc_user_password.Name = "lsc_user_password"
        Me.lsc_user_password.ReadOnly = True
        Me.lsc_user_password.Visible = False
        '
        'userfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(779, 266)
        Me.Controls.Add(Me.user_grid)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "userfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "maintain User List"
        CType(Me.user_grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LSCUserTableBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents user_grid As System.Windows.Forms.DataGridView
    Friend WithEvents FeesSQLDataSet As CapitalandEquity.FeesSQLDataSet
    Friend WithEvents LSCUserTableBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LSC_User_tableTableAdapter As CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_User_tableTableAdapter
    Friend WithEvents LscusernoDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LscusernameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LscuseradminDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents LscusrenabledDataGridViewCheckBoxColumn As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents lsc_user_password As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

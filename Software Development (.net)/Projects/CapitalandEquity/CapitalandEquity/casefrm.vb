Public Class casefrm
    Dim asset_idx, next_prop_no, prop_no_entered, bank_no_entered, cred_no_entered, next_bank_no, next_cred_no As Integer
    Dim query_no_entered, next_query_no As Integer
    Dim query_text_entered, query_resp_entered As String
    Dim asset_max As Integer = 50
    Dim cap_asset_type(asset_max) As String
    Dim cap_amt(asset_max) As String
    Dim cap_amt_verified(asset_max) As String
    Dim prop_address_entered, prop_type_entered, bank_name_entered, cred_type_entered, cred_joint_with_entered, likely_value_entered As String
    Dim cred_value_entered, cred_value_os_entered As String
    Dim bank_amt_entered As Decimal

    Dim debt_no_of_props As Integer = 0
    Dim debt_no_of_credit_agreements As Integer = 0
    Dim debt_no_of_bank_accts As Integer = 0
    Dim exp_check, undec_bank_acct, cred_last_year, cons_position As Boolean
    Dim ver_tot_cap As Decimal = 0
    Dim ver_tot_equity As Decimal = 0
    Dim lr_check_idx As Integer
    Dim lr_check_array(10, 4) As String
    Dim address_match_result As String
    Dim land_registry_result As String
    Dim experian_result As String
    Dim est_prop_value As String
    Dim poss_equity As String
    Dim add_prop_decl As Boolean

    Private Sub casefrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'insert any new rows in the table
        insert_new_rows()
    End Sub
    Private Sub insert_new_rows()
        completebtn.Enabled = False
        Dim row As DataGridViewRow
        Dim idx As Integer = 0
        For Each row In prop_grid.Rows
            Dim prop_no As Integer
            Dim address As String
            Try
                address = prop_grid.Rows(idx).Cells(2).FormattedValue
            Catch ex As Exception
                idx += 1
                Continue For
            End Try

            If Microsoft.VisualBasic.Len(Trim(address)) = 0 Then
                idx += 1
                Continue For
            End If
            Try
                prop_no = prop_grid.Rows(idx).Cells(0).Value
            Catch ex As Exception
                idx += 1
                Continue For
            End Try
            If prop_no = 0 Then
                prop_no = next_prop_no
                next_prop_no += 1
            Else
                idx += 1
                Continue For
            End If
            prop_grid.Rows(idx).Cells(0).Value = prop_no
            Dim prop_type As String = prop_grid.Rows(idx).Cells(3).Value
            Dim main_res As Boolean = prop_grid.Rows(idx).Cells(1).Value
            Dim lr_check As Boolean = prop_grid.Rows(idx).Cells(4).Value
            Dim likely_value As Decimal = Nothing
            Try
                likely_value = prop_grid.Rows(idx).Cells(5).Value
            Catch ex As Exception

            End Try
            Dim likely_value_str As String = Nothing
            If likely_value <> Nothing Then
                likely_value_str = Format(likely_value, "#.##")
            End If
            Dim temp_table As New FeesSQLDataSet.LSC_Property_tableDataTable
            LSC_Property_tableTableAdapter.FillBy1(Me.FeesSQLDataSet.LSC_Property_table, debtorID, prop_no)
            Try
                Dim test As Integer = temp_table.Rows(0).Item(0)
            Catch
                Try
                    Me.LSC_Property_tableTableAdapter.InsertQuery(debtorID, prop_no, main_res, address, prop_type, lr_check, likely_value_str, Format(Now, "dd/MM/yyyy"), log_code)
                Catch ex As Odbc.OdbcException
                    MessageBox.Show("Insert of Property failed")
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                prop_grid.Rows(idx).Cells(6).Value = Format(Now, "dd/MM/yyyy")
                prop_grid.Rows(idx).Cells(7).Value = log_user
            End Try
            idx += 1
        Next

        idx = 0
        For Each row In bank_grid.Rows
            Dim bank_no As Integer
            Dim bank_name As String
            Try
                bank_name = bank_grid.Rows(idx).Cells(1).FormattedValue
            Catch ex As Exception
                idx += 1
                Continue For
            End Try

            If Microsoft.VisualBasic.Len(Trim(bank_name)) = 0 Then
                idx += 1
                Continue For
            End If
            Try
                bank_no = bank_grid.Rows(idx).Cells(0).Value
            Catch ex As Exception
                idx += 1
                Continue For
            End Try
            If bank_no = 0 Then
                bank_no = next_bank_no
                next_bank_no += 1
            Else
                idx += 1
                Continue For
            End If
            bank_grid.Rows(idx).Cells(0).Value = bank_no
            Dim bank_value As Decimal = Nothing
            Try
                bank_value = bank_grid.Rows(idx).Cells(2).Value
            Catch ex As Exception

            End Try
            Dim bank_value_str As String = Nothing
            If bank_value <> Nothing Then
                bank_value_str = Format(bank_value, "#.##")
            End If
            Dim temp_table As New FeesSQLDataSet.LSC_bank_tableDataTable
            LSC_bank_tableTableAdapter.FillBy1(Me.FeesSQLDataSet.LSC_bank_table, debtorID, bank_no)
            Try
                Dim test As Integer = temp_table.Rows(0).Item(0)
            Catch
                Try
                    Me.LSC_bank_tableTableAdapter.InsertQuery(debtorID, bank_no, bank_name, bank_value_str, Format(Now, "dd/MM/yyyy"), log_code)
                Catch ex As Odbc.OdbcException
                    MessageBox.Show("Insert of bank Account failed")
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                bank_grid.Rows(idx).Cells(3).Value = Format(Now, "dd/MM/yyyy")
                bank_grid.Rows(idx).Cells(4).Value = log_user
            End Try
            idx += 1
        Next
        calc_ver_tot_cap()
        idx = 0
        For Each row In cred_grid.Rows
            Dim cred_no As Integer
            Dim cred_type As String
            Try
                cred_type = cred_grid.Rows(idx).Cells(1).FormattedValue
            Catch ex As Exception
                idx += 1
                Continue For
            End Try

            If Microsoft.VisualBasic.Len(Trim(cred_type)) = 0 Then
                idx += 1
                Continue For
            End If
            Try
                cred_no = cred_grid.Rows(idx).Cells(0).Value
            Catch ex As Exception
                idx += 1
                Continue For
            End Try
            If cred_no = 0 Then
                cred_no = next_cred_no
                next_cred_no += 1
            Else
                idx += 1
                Continue For
            End If
            cred_grid.Rows(idx).Cells(0).Value = cred_no
            Dim cred_default As Boolean = cred_grid.Rows(idx).Cells(2).Value
            Dim cred_joint As Boolean = cred_grid.Rows(idx).Cells(3).Value
            Dim cred_joint_with As String = cred_grid.Rows(idx).Cells(4).Value
            Dim cred_value As Decimal = Nothing
            Try
                cred_value = cred_grid.Rows(idx).Cells(5).Value
            Catch ex As Exception

            End Try
            Dim cred_value_str As String = Nothing
            If cred_value <> Nothing Then
                cred_value_str = Format(cred_value, "#.##")
            End If
            Dim cred_value_os As Decimal = Nothing
            Try
                cred_value_os = cred_grid.Rows(idx).Cells(6).Value
            Catch ex As Exception

            End Try
            Dim cred_value_os_str As String = Nothing
            If cred_value_os <> Nothing Then
                cred_value_os_str = Format(cred_value_os, "#.##")
            End If
            Dim temp_table As New FeesSQLDataSet.LSC_Credit_tableDataTable
            LSC_Credit_tableTableAdapter.FillBy(Me.FeesSQLDataSet.LSC_Credit_table, debtorID, cred_no)
            Try
                Dim test As Integer = temp_table.Rows(0).Item(0)
            Catch
                Try
                    Me.LSC_Credit_tableTableAdapter.InsertQuery(debtorID, cred_no, cred_type, cred_default, cred_joint, cred_joint_with, cred_value_str, cred_value_os_str, Format(Now, "dd/MM/yyyy"), log_code)
                Catch ex As Odbc.OdbcException
                    MessageBox.Show("Insert of credit agreement failed")
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                cred_grid.Rows(idx).Cells(7).Value = Format(Now, "dd/MM/yyyy")
                cred_grid.Rows(idx).Cells(8).Value = log_user
            End Try
            idx += 1
        Next
        calc_ver_tot_equity()
        idx = 0
        For Each row In query_grid.Rows
            Dim query_no As Integer
            Dim query_text As String
            Try
                query_text = query_grid.Rows(idx).Cells(1).FormattedValue
            Catch ex As Exception
                idx += 1
                Continue For
            End Try

            If Microsoft.VisualBasic.Len(Trim(query_text)) = 0 Then
                idx += 1
                Continue For
            End If
            Try
                query_no = query_grid.Rows(idx).Cells(0).Value
            Catch ex As Exception
                idx += 1
                Continue For
            End Try
            If query_no = 0 Then
                query_no = next_query_no
                next_query_no += 1
            Else
                idx += 1
                Continue For
            End If
            Dim query_response As String = query_grid.Rows(idx).Cells(4).Value
            query_grid.Rows(idx).Cells(0).Value = query_no
            Dim temp_table As New FeesSQLDataSet.LSC_Query_tableDataTable
            mainmenu.LSC_Query_tableTableAdapter.FillBy3(Me.FeesSQLDataSet.LSC_Query_table, debtorID, query_no)
            Try
                Dim test As Integer = temp_table.Rows(0).Item(0)
            Catch
                Try
                    mainmenu.LSC_Query_tableTableAdapter.InsertQuery(debtorID, query_no, query_text, Format(Now, "dd/MM/yyyy"), log_code, query_response, 0)
                    mainmenu.myquerybtn.Enabled = True
                    mainmenu.allquerybtn.Enabled = True
                Catch ex As Odbc.OdbcException
                    MessageBox.Show("Insert of query failed")
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                query_grid.Rows(idx).Cells(2).Value = Format(Now, "dd/MM/yyyy")
                query_grid.Rows(idx).Cells(3).Value = log_user
            End Try
            idx += 1
        Next
    End Sub


    Private Sub casefrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        wait_txt.Text = ""
        display_case_details()
    End Sub
    Public Sub display_case_details()
        prop_no_entered = 0
        param2 = "select status_open_closed, client_ref, name_sur, name_fore, address, dateOfBirth," & _
        "clientschemeID, _createdDate, last_stageID, debt_amount, debt_costs, isCompany, add_postcode" & _
        " from Debtor where _rowid = " & debtorID
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Case " & debtorID & "  does not exist on ONESTEP")
            Me.Close()
            Exit Sub
        End If
        Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(6)
        param2 = "select clientID from ClientScheme where _rowid = " & csid
        Dim csid_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to access client details for CSID = " & csid)
            Me.Close()
            Exit Sub
        End If
        If csid_dataset.Tables(0).Rows(0).Item(0) <> 909 Then
            MsgBox("Case " & debtorID & " is not an LSC case")
            Me.Close()
            Exit Sub
        End If
        Dim status_open_closed As String = debtor_dataset.Tables(0).Rows(0).Item(0)
        If status_open_closed = "C" Then
            onestep_status_txt.Text = "CLOSED"
            display_mode = True
        Else
            onestep_status_txt.Text = "OPEN"
            display_mode = False
        End If

        param2 = "select text from Note where (type = 'Note' or type = 'Client note')" & _
        " and debtorID = " & debtorID & _
        " order by _rowid"
        Dim note_dataset As DataSet = get_dataset("onestep", param2)
        Dim note_idx = no_of_rows - 1
        Dim idx, idx2 As Integer
        Dim note_txt As String
        Dim case_type As String = ""
        Dim outcome As String = ""
        Dim emp_status As String = ""
        Dim equity_amt As String = ""
        Dim residential_desc As String = ""
        Dim prop_type_desc As String = ""
        Dim appl_pcent As String = ""
        Dim partner_pcent As String = ""
        Dim decl_cap_assets As String = ""
        Dim decl_cap_assets_type As String = ""
        Dim partner_equity_amt As String = ""
        Dim appl_equity_amt As String = ""
        Dim equity_amt_verified As String = ""
        Dim bank_details(5) As String
        Dim final_costs As String = ""
        Dim final_vat As String = ""
        Dim lgfs_tot As String = ""
        Dim lgfs_vat As String = ""
        Dim agfs_tot As String = ""
        Dim agfs_vat As String = ""
        Dim so_date_str As String = ""
        Dim calc_date_str As String = ""
        Dim partner_first_name As String = ""
        Dim partner_last_name As String = ""
        Dim partner_dob As String = ""
        Dim partner_nino As String = ""
        Dim partner_emp_status As String = ""
        Dim inc_evidence As String = ""
        inc_evidence_grid.Rows.Clear()
        Dim tot_ver_cap_equity As String = ""
        experian_result = ""
        For idx = 0 To 5
            bank_details(idx) = ""
        Next
        bank_details_idx = 0
        add_prop_decl_txt.Text = "No"
        asset_idx = 0
        tot_calc_cap_assets = 0
        For idx = 0 To asset_max
            cap_asset_type(idx) = ""
            cap_amt(idx) = Nothing
            cap_amt_verified(idx) = ""
        Next
        For idx = 0 To 10
            lr_check_array(idx, 1) = ""
            lr_check_array(idx, 2) = ""
            lr_check_array(idx, 3) = ""
            lr_check_array(idx, 4) = ""
        Next
        lr_check_idx = 0
        For idx = 0 To note_idx
            Dim search_length As Integer
            note_txt = note_dataset.Tables(0).Rows(idx).Item(0)
            Dim start_idx = InStr(note_txt, "Case Type:")
            If start_idx > 0 Then
                search_length = 10
                case_type = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        If note_txt.Length - idx2 > 3 And Mid(note_txt, idx2, 3) = "Emp" Then
                            Exit For
                        Else
                            case_type = case_type & Mid(note_txt, idx2, 1)
                        End If
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "OUTCOME:")
            If start_idx = 0 Then
                start_idx = InStr(note_txt, "Outcome:")
            End If
            If start_idx > 0 Then
                outcome = ""
                search_length = 8
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        outcome = outcome & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Emp Status:")
            search_length = 11
            If start_idx = 0 Then
                start_idx = InStr(note_txt, "Employment Status:")
                search_length = 18
            End If
            If start_idx > 0 Then
                emp_status = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    ElseIf note_txt.Length - idx2 > 3 And Mid(note_txt, idx2, 4) = "Disp" Then
                        Exit For
                    Else
                        emp_status = emp_status & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Equity Amt:")
            If start_idx > 0 Then
                equity_amt = ""
                search_length = 11
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        equity_amt = equity_amt & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Total Verified Capital and Equity:")
            If start_idx > 0 Then
                tot_ver_cap_equity = ""
                search_length = 34
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        tot_ver_cap_equity = tot_ver_cap_equity & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            'residential description
            start_idx = InStr(note_txt, "Residential Description:")
            If start_idx > 0 Then
                residential_desc = ""
                search_length = 24
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        residential_desc = residential_desc & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            'property type description
            start_idx = InStr(note_txt, "Property Type Code:")
            If start_idx > 0 Then
                prop_type_desc = ""
                search_length = 19
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        prop_type_desc = prop_type_desc & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            'percent owned by applicant
            start_idx = InStr(note_txt, "Percent Owned Applicant:")
            If start_idx > 0 Then
                appl_pcent = ""
                search_length = 24
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        appl_pcent = appl_pcent & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            'percent owned by partner
            start_idx = InStr(note_txt, "Percent Owned Partner:")
            If start_idx > 0 Then
                partner_pcent = ""
                search_length = 22
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        partner_pcent = partner_pcent & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            'Declared Capital Assets:
            start_idx = InStr(note_txt, "Declared Capital Assets:")
            If start_idx > 0 Then
                search_length = 24
                For idx2 = start_idx + search_length To note_txt.Length
                    If Not IsNumeric(Mid(note_txt, idx2, 1)) And Mid(note_txt, idx2, 1) <> " " Then
                        Exit For
                    Else
                        decl_cap_assets = decl_cap_assets & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If

            'Capital Asset type:
            start_idx = InStr(note_txt, "Capital Asset Type:")
            If start_idx > 0 Then
                search_length = 19
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        decl_cap_assets_type = decl_cap_assets_type & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Equity Amt Verified:")
            If start_idx > 0 Then
                equity_amt_verified = ""
                search_length = 20
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        equity_amt_verified = equity_amt_verified & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Applicant Equity Amount:")
            If start_idx > 0 Then
                appl_equity_amt = ""
                search_length = 24
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        appl_equity_amt = appl_equity_amt & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Partner Equity Amount:")
            If start_idx > 0 Then
                partner_equity_amt = ""
                search_length = 22
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        partner_equity_amt = partner_equity_amt & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "BANK DETAILS")
            If start_idx > 0 Then
                bank_details(bank_details_idx) = note_txt
                bank_details_idx += 1
            End If
            start_idx = InStr(note_txt, "TitleNo:")
            If start_idx > 0 Then
                lr_check_idx += 1
                search_length = 8
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else

                        lr_check_array(lr_check_idx, 1) = lr_check_array(lr_check_idx, 1) & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Tenure:")
            If start_idx > 0 Then
                search_length = 7
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        lr_check_array(lr_check_idx, 2) = lr_check_array(lr_check_idx, 2) & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Proprietor:")
            If start_idx > 0 Then
                search_length = 11
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        lr_check_array(lr_check_idx, 3) = lr_check_array(lr_check_idx, 3) & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Charge:")
            If start_idx > 0 Then
                search_length = 7
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        lr_check_array(lr_check_idx, 4) = lr_check_array(lr_check_idx, 4) & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "AddressMatchResults:")
            If start_idx > 0 Then
                search_length = 20
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        address_match_result = address_match_result & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "LandRegistryResults:")
            If start_idx > 0 Then
                search_length = 20
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        land_registry_result = land_registry_result & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "ExperianResults:")
            If start_idx > 0 Then
                search_length = 16
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        experian_result = experian_result & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "EstPropertyValue:")
            If start_idx > 0 Then
                search_length = 17
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        est_prop_value = est_prop_value & Mid(note_txt, idx2, 1)
                    End If
                Next
                start_idx = InStr(note_txt, "PossibleEquity:")
                If start_idx > 0 Then
                    search_length = 15
                    For idx2 = start_idx + search_length To note_txt.Length
                        If Mid(note_txt, idx2, 1) = ";" Then
                            Exit For
                        Else
                            poss_equity = poss_equity & Mid(note_txt, idx2, 1)
                        End If
                    Next
                End If
            End If
            Dim last_start As Integer = get_assets(note_txt, idx)
            'see if more assets on same line
            Dim txt2 As String = note_txt
            While last_start > 0
                txt2 = Microsoft.VisualBasic.Right(txt2, txt2.Length - last_start - 5)
                last_start = get_assets(txt2, 1)
            End While
            start_idx = InStr(note_txt, "FinalCost:")
            If start_idx > 0 Then
                search_length = 10
                final_costs = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        final_costs = final_costs & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "FinalVat:")
            If start_idx > 0 Then
                search_length = 9
                final_vat = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        final_vat = final_vat & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "LGFS_Total:")
            If start_idx > 0 Then
                search_length = 11
                lgfs_tot = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        lgfs_tot = lgfs_tot & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "LGFS_Vat:")
            If start_idx > 0 Then
                search_length = 9
                lgfs_vat = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        lgfs_vat = lgfs_vat & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "AGFS_Total:")
            If start_idx > 0 Then
                search_length = 11
                agfs_tot = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        agfs_tot = agfs_tot & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "AGFS_Vat:")
            If start_idx > 0 Then
                search_length = 9
                agfs_vat = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        agfs_vat = agfs_vat & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "SentenceOrderDate:")
            If start_idx > 0 Then
                search_length = 18
                so_date_str = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        so_date_str = so_date_str & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "CalculationDate:")
            If start_idx > 0 Then
                search_length = 16
                calc_date_str = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        calc_date_str = calc_date_str & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Partner First Name:")
            If start_idx > 0 Then
                search_length = 19
                partner_first_name = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        partner_first_name = partner_first_name & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Partner Last Name:")
            If start_idx > 0 Then
                search_length = 18
                partner_last_name = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        partner_last_name = partner_last_name & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Partner DOB:")
            If start_idx > 0 Then
                search_length = 12
                partner_dob = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        partner_dob = partner_dob & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Partner NINO:")
            If start_idx > 0 Then
                search_length = 13
                partner_nino = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        partner_nino = partner_nino & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Partner Emp Status:")
            If start_idx > 0 Then
                search_length = 19
                partner_emp_status = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        partner_emp_status = partner_emp_status & Mid(note_txt, idx2, 1)
                    End If
                Next
            End If
            start_idx = InStr(note_txt, "Inc Evidence:")
            If start_idx > 0 Then
                search_length = 13
                inc_evidence = ""
                For idx2 = start_idx + search_length To note_txt.Length
                    If Mid(note_txt, idx2, 1) = ";" Then
                        Exit For
                    Else
                        inc_evidence = inc_evidence & Mid(note_txt, idx2, 1)
                    End If
                Next
                inc_evidence_grid.Rows.Add(inc_evidence)
            End If
        Next
        If IsNumeric(decl_cap_assets) Then
            If decl_cap_assets > 0 Then
                cap_amt(asset_idx) = decl_cap_assets
                tot_calc_cap_assets += decl_cap_assets
                cap_asset_type(asset_idx) = decl_cap_assets_type
                asset_idx += 1
            End If
        End If
        

        ver_tot_c_and_e_from_feed.Text = tot_ver_cap_equity

        casetype_txt.Text = case_type
        If InStr(LCase(case_type), "appeal") > 0 Then
            display_mode = True
        End If
        outcome_txt.Text = outcome
        If InStr(LCase(outcome), "acquitted") > 0 Or InStr(LCase(outcome), "aquitted") > 0 Then
            display_mode = True
        End If
        debtor_tbox.Text = debtorID
        maatid_txt.Text = debtor_dataset.Tables(0).Rows(0).Item(1)
        Dim debtor_name As String = ""
        Dim postcode As String = ""
        Try
            debtor_name = Trim(debtor_dataset.Tables(0).Rows(0).Item(3)) & " "
        Catch ex As Exception

        End Try
        Try
            debtor_name = Trim(debtor_name & debtor_dataset.Tables(0).Rows(0).Item(2))
        Catch ex As Exception

        End Try
        Try
            postcode = Trim(debtor_dataset.Tables(0).Rows(0).Item(12))
        Catch ex As Exception

        End Try
        name_tbox.Text = debtor_name
        address_tbox.Text = debtor_dataset.Tables(0).Rows(0).Item(4)
        address_tbox.Text = Replace(address_tbox.Text, Chr(10), vbNewLine)
        loaded_txt.Text = Format(debtor_dataset.Tables(0).Rows(0).Item(7), "dd/MM/yyyy")
        pcode_tbox.Text = postcode
        Dim dob As Date = Nothing
        Try
            dob = debtor_dataset.Tables(0).Rows(0).Item(5)
        Catch ex As Exception

        End Try
        dob_txt.Text = dob
        Dim stageID As Integer = debtor_dataset.Tables(0).Rows(0).Item(8)
        Dim stage_name As String = ""
        param2 = "select name from Stage where _rowid = " & stageID
        Dim stage_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to find stageID = " & stageID)
        Else
            stage_name = stage_dataset.Tables(0).Rows(0).Item(0)
        End If
        stage_txt.Text = stage_name
        emp_status_txt.Text = Trim(emp_status)

        'contributions
        Dim debt_amt, debt_costs As Decimal
        Dim iscompany As String
        debt_amt = debtor_dataset.Tables(0).Rows(0).Item(9)
        debt_costs = debtor_dataset.Tables(0).Rows(0).Item(10)
        iscompany = debtor_dataset.Tables(0).Rows(0).Item(11)
        'If iscompany = "Y" Then
        '    inc_contrib_txt.Text = Format(debt_amt, "�0.00")
        '    five_sixths_txt.Text = "Y"
        'Else
        '    inc_contrib_txt.Text = Format(debt_amt + debt_costs, "�0.00")
        '    five_sixths_txt.Text = "N"
        'End If

        'amount paid
        param2 = "select Sum(split_debt + split_costs) from Payment " & _
        " where status = 'R' and debtorID = " & debtorID
        Dim payment_dataset As DataSet = get_dataset("onestep", param2)
        Dim inc_paid As Decimal
        Try
            inc_paid = payment_dataset.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            inc_paid = 0
        End Try

        inc_paid_txt.Text = FormatCurrency(inc_paid)
        Dim amt_dec As Decimal
        Try
            amt_dec = equity_amt
        Catch ex As Exception
            amt_dec = 0
        End Try

        equity_amt_txt.Text = FormatCurrency(amt_dec)
        equity_amt_ver_txt.Text = equity_amt_verified
        Try
            amt_dec = partner_equity_amt
        Catch ex As Exception
            amt_dec = 0
        End Try

        partner_equity_amt_txt.Text = FormatCurrency(amt_dec)
        Try
            amt_dec = appl_equity_amt
        Catch ex As Exception
            amt_dec = 0
        End Try
        Try
            appl_equity_amt_txt.Text = FormatCurrency(appl_equity_amt)
        Catch ex As Exception
            appl_equity_amt_txt.Text = ""
        End Try

        residential_desc_txt.Text = residential_desc
        prop_type_desc_txt.Text = prop_type_desc
        appl_pcent_txt.Text = appl_pcent
        partner_pcent_txt.Text = partner_pcent

        'get crystallisation fees
        Dim crystallisation As String = ""
        param2 = "select sum(fee_amount) from Fee where debtorID = " & debtorID & _
        " and type = 'Crystalisation'"
        Dim fee_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows <> 0 Then
            Try
                crystallisation = fee_dataset.Tables(0).Rows(0).Item(0)
            Catch ex As Exception

            End Try
        End If

        Dim sixth_adj As String = ""
        param2 = "select sum(fee_amount) from Fee where debtorID = " & debtorID & _
        " and type = '6th Inst Adjust'"
        Dim fee2_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows <> 0 Then
            Try
                sixth_adj = fee2_dataset.Tables(0).Rows(0).Item(0)
            Catch ex As Exception

            End Try
        End If

        If display_mode Then
            mode_txt.Text = "DISPLAY ONLY"
        Else
            mode_txt.Text = "UPDATE ALLOWED"
        End If
        'populate asset_grid
        asset_grid.Rows.Clear()

        For idx = 0 To asset_idx - 1
            Dim cap_amt_dec As Decimal = cap_amt(idx)
            If cap_amt_dec < 0 Then
                Continue For
            End If
            asset_grid.Rows.Add("Onestep", cap_asset_type(idx), Format(cap_amt_dec, "�0.00"), _
            cap_amt_verified(idx))
        Next

        'populate lr grid
        lr_grid.Rows.Clear()
        For idx = 1 To lr_check_idx
            lr_grid.Rows.Add(lr_check_array(idx, 1), lr_check_array(idx, 2), lr_check_array(idx, 3), lr_check_array(idx, 4))
        Next
        'populate search grid
        search_grid.Rows.Clear()
        search_grid.Rows.Add(address_match_result, land_registry_result, experian_result, est_prop_value, poss_equity)

        'populate bank details grid
        bank_details_grid.Rows.Clear()
        For idx = 0 To bank_details_idx - 1
            bank_details_grid.Rows.Add(bank_details(idx))
        Next

        populate_prop_grid()

        populate_bank_grid()

        'populate credit agreements
        cred_grid.Rows.Clear()
        Me.LSC_Credit_tableTableAdapter.FillBy1(Me.FeesSQLDataSet.LSC_Credit_table, debtorID)
        Dim no_of_credit_agreements As Integer = FeesSQLDataSet.LSC_Credit_table.Rows.Count
        For idx = 0 To no_of_credit_agreements - 1
            Dim cred_no As Integer = FeesSQLDataSet.LSC_Credit_table.Rows(idx).Item(1)
            Dim cred_type As String = FeesSQLDataSet.LSC_Credit_table.Rows(idx).Item(2)
            Dim cred_default As Boolean = FeesSQLDataSet.LSC_Credit_table.Rows(idx).Item(3)
            Dim cred_joint As Boolean = FeesSQLDataSet.LSC_Credit_table.Rows(idx).Item(4)
            Dim cred_joint_with As String = ""
            Try
                cred_joint_with = FeesSQLDataSet.LSC_Credit_table.Rows(idx).Item(5)
            Catch ex As Exception

            End Try

            Dim cred_value_str As String = ""
            Dim cred_value As Decimal = Nothing
            Try
                cred_value = FeesSQLDataSet.LSC_Credit_table.Rows(idx).Item(6)
            Catch ex As Exception

            End Try
            If cred_value <> Nothing Then
                cred_value_str = FormatCurrency(cred_value)
            End If
            Dim cred_value_os_str As String = ""
            Dim cred_value_os As Decimal = Nothing
            Try
                cred_value_os = FeesSQLDataSet.LSC_Credit_table.Rows(idx).Item(7)
            Catch ex As Exception

            End Try
            If cred_value_os <> Nothing Then
                cred_value_os_str = FormatCurrency(cred_value_os)
            End If
            next_cred_no = cred_no
            Dim added_date As Date = FeesSQLDataSet.LSC_Credit_table.Rows(idx).Item(8)
            Dim user_code As Integer = FeesSQLDataSet.LSC_Credit_table.Rows(idx).Item(9)
            logon.LSC_User_tableTableAdapter.FillBy(Me.FeesSQLDataSet.LSC_User_table, user_code)
            Dim added_by As String = FeesSQLDataSet.LSC_User_table.Rows(0).Item(1)
            cred_grid.Rows.Add(cred_no, cred_type, cred_default, cred_joint, cred_joint_with, cred_value_str, cred_value_os_str, Format(added_date, "dd/MM/yyyy"), added_by)
        Next
        next_cred_no += 1
        calc_ver_tot_equity()
        ver_tot_equity_txt.Text = FormatCurrency(ver_tot_equity)

        'populate queries
        query_grid.Rows.Clear()
        mainmenu.LSC_Query_tableTableAdapter.FillBy1(Me.FeesSQLDataSet.LSC_Query_table, debtorID)
        Dim no_of_queries As Integer = FeesSQLDataSet.LSC_Query_table.Rows.Count
        For idx = 0 To no_of_queries - 1
            Dim query_no As Integer = FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(1)
            Dim query_text As String = FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(2)
            Dim query_date_added As Date = FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(3)
            Dim user_code As Integer = FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(4)
            logon.LSC_User_tableTableAdapter.FillBy(Me.FeesSQLDataSet.LSC_User_table, user_code)
            Dim added_by As String = FeesSQLDataSet.LSC_User_table.Rows(0).Item(1)
            Dim query_response As String = ""
            Try
                query_response = FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(5)
            Catch ex As Exception

            End Try

            Dim query_comp_date_str As String = ""
            Dim query_comp_date As Date = Nothing
            Dim query_completed As Boolean = False

            Try
                user_code = FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(7)
            Catch ex As Exception
                user_code = 0
            End Try
            Dim query_comp_by As String = ""
            If user_code > 0 Then
                query_completed = True
                logon.LSC_User_tableTableAdapter.FillBy(Me.FeesSQLDataSet.LSC_User_table, user_code)
                query_comp_by = FeesSQLDataSet.LSC_User_table.Rows(0).Item(1)
                Try
                    query_comp_date = FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(6)
                Catch ex As Exception

                End Try
                If query_comp_date <> Nothing Then
                    query_comp_date_str = Format(query_comp_date, "dd/MM/yyyy")
                End If
            End If
            next_query_no = query_no

            query_grid.Rows.Add(query_no, query_text, Format(query_date_added, "dd/MM/yyyy"), added_by, query_response, query_completed, query_comp_date_str, query_comp_by)
        Next
        next_query_no += 1

        'populate final costs grid
        final_costs_grid.Rows.Clear()
        final_costs_grid.Rows.Add(final_costs, final_vat, lgfs_tot, lgfs_vat, agfs_tot, agfs_vat)

        'populate crystallisation grid
        crystal_grid.Rows.Clear()
        Dim so_date As Date
        If so_date_str <> "" Then
            Try
                so_date = CDate(so_date_str)
                so_date_str = Format(so_date, "dd/MM/yyyy")
            Catch ex As Exception

            End Try
        End If
        Dim calc_date As Date
        If calc_date_str <> "" Then
            Try
                calc_date = CDate(calc_date_str)
                calc_date_str = Format(calc_date, "dd/MM/yyyy")
            Catch ex As Exception

            End Try
        End If
        crystal_grid.Rows.Add(so_date_str, calc_date_str, crystallisation, sixth_adj)

        'populate partner grid
        partner_grid.Rows.Clear()
        If partner_dob <> "" Then
            Try
                Dim test_date As Date = CDate(partner_dob)
                partner_dob = Format(test_date, "dd/MM/yyyy")
            Catch ex As Exception

            End Try
        End If
        partner_grid.Rows.Add(partner_first_name, partner_last_name, partner_dob, partner_nino, partner_emp_status)

        'see if currently in wip
        wip_txt.Text = ""
        Me.LSC_WIP_tableTableAdapter.FillBy1(Me.FeesSQLDataSet.LSC_WIP_table, debtorID)
        Dim wip_user_no As Integer = 0
        Try
            wip_user_no = FeesSQLDataSet.LSC_WIP_table.Rows(0).Item(0)
        Catch ex As Exception
            wip_txt.Text = "Not Allocated"
        End Try

        'only allow update if allocate to user
        prop_grid.ReadOnly = True
        cred_grid.ReadOnly = True
        bank_grid.ReadOnly = True
        query_grid.ReadOnly = True
        exp_check_cbox.Enabled = False
        match_cbox.Enabled = False
        undec_bank_cbox.Enabled = False
        cred_score_ud.Enabled = False
        cred_last_year_cbox.Enabled = False
        cons_pos_cbox.Enabled = False
        tot_cap_assets_cbox.Enabled = False
        parents_cbox.Enabled = False
        partner_cbox.Enabled = False
        same_surname_cbox.Enabled = False
        prop_comments_txt.Enabled = False
        bank_acct_ud.Enabled = False
        If display_mode = True Then
            tomebtn.Enabled = False
        Else
            tomebtn.Enabled = True
        End If

        If wip_txt.Text = "" Then
            If wip_user_no = log_code And display_mode = False Then
                wip_txt.Text = "Allocated to me"
                allow_grid_update()
                tomebtn.Enabled = False
                exp_check_cbox.Enabled = True
                match_cbox.Enabled = True
                undec_bank_cbox.Enabled = True
                cred_score_ud.Enabled = True
                cred_last_year_cbox.Enabled = True
                cons_pos_cbox.Enabled = True
                tot_cap_assets_cbox.Enabled = True
                parents_cbox.Enabled = True
                partner_cbox.Enabled = True
                same_surname_cbox.Enabled = True
                prop_comments_txt.Enabled = True
                bank_acct_ud.Enabled = True
                'see if any queries for this case
                mainmenu.LSC_Query_tableTableAdapter.FillBy1(mainmenu.FeesSQLDataSet.LSC_Query_table, debtorID)
            Else
                logon.LSC_User_tableTableAdapter.FillBy(Me.FeesSQLDataSet.LSC_User_table, wip_user_no)
                wip_txt.Text = "Allocated to " & FeesSQLDataSet.LSC_User_table.Rows(0).Item(1)
            End If
        End If

        'get details from new debtor table
        get_debtor_details()

        validatebtn.Enabled = False
        If status_no = 1 And wip_user_no = log_code Then
            validatebtn.Enabled = True
        End If
        completebtn.Enabled = False
        If status_no > 2 Then
            tomebtn.Enabled = False
        End If
        undec_bank_cbox.Checked = undec_bank_acct
    End Sub
    Private Sub populate_prop_grid()
        'populate property grid
        prop_grid.Rows.Clear()
        Dim idx As Integer
        Me.LSC_Property_tableTableAdapter.FillBy(Me.FeesSQLDataSet.LSC_Property_table, debtorID)
        Dim no_of_props As Integer = FeesSQLDataSet.LSC_Property_table.Rows.Count
        For idx = 0 To no_of_props - 1
            Dim address As String = FeesSQLDataSet.LSC_Property_table.Rows(idx).Item(2)
            Dim lr_check As Boolean
            Try
                lr_check = FeesSQLDataSet.LSC_Property_table.Rows(idx).Item(3)
            Catch ex As Exception

            End Try
            Dim likely_value_str As String = ""
            Dim likely_value As Decimal = Nothing
            Try
                likely_value = FeesSQLDataSet.LSC_Property_table.Rows(idx).Item(4)
            Catch ex As Exception

            End Try
            If likely_value <> Nothing Then
                likely_value_str = FormatCurrency(likely_value)
            End If
            Dim prop_no As Integer = FeesSQLDataSet.LSC_Property_table.Rows(idx).Item(1)
            next_prop_no = prop_no
            Dim added_date As Date = FeesSQLDataSet.LSC_Property_table.Rows(idx).Item(5)
            Dim user_code As Integer = FeesSQLDataSet.LSC_Property_table.Rows(idx).Item(6)
            Dim prop_type As String
            Try
                prop_type = FeesSQLDataSet.LSC_Property_table.Rows(idx).Item(7)
            Catch ex As Exception
                prop_type = ""
            End Try

            Dim main_res As Boolean = FeesSQLDataSet.LSC_Property_table.Rows(idx).Item(8)
            logon.LSC_User_tableTableAdapter.FillBy(Me.FeesSQLDataSet.LSC_User_table, user_code)
            Dim added_by As String = FeesSQLDataSet.LSC_User_table.Rows(0).Item(1)
            prop_grid.Rows.Add(prop_no, main_res, address, prop_type, lr_check, likely_value_str, Format(added_date, "dd/MM/yyyy"), added_by)
        Next
        next_prop_no += 1
    End Sub

    Private Sub populate_bank_grid()
        bank_grid.Rows.Clear()
        Me.LSC_bank_tableTableAdapter.FillBy(Me.FeesSQLDataSet.LSC_bank_table, debtorID)
        Dim no_of_banks As Integer = FeesSQLDataSet.LSC_bank_table.Rows.Count
        Dim idx As Integer
        For idx = 0 To no_of_banks - 1
            Dim bank_no As Integer = FeesSQLDataSet.LSC_bank_table.Rows(idx).Item(1)
            Dim bank_name As String = FeesSQLDataSet.LSC_bank_table.Rows(idx).Item(2)
            Dim bank_value_str As String = ""
            Dim bank_value As Decimal = Nothing
            Try
                bank_value = FeesSQLDataSet.LSC_bank_table.Rows(idx).Item(3)
            Catch ex As Exception

            End Try
            If bank_value <> Nothing Then
                bank_value_str = FormatCurrency(bank_value)
            End If
            next_bank_no = bank_no
            Dim added_date As Date = FeesSQLDataSet.LSC_bank_table.Rows(idx).Item(4)
            Dim user_code As Integer = FeesSQLDataSet.LSC_bank_table.Rows(idx).Item(5)
            logon.LSC_User_tableTableAdapter.FillBy(Me.FeesSQLDataSet.LSC_User_table, user_code)
            Dim added_by As String = FeesSQLDataSet.LSC_User_table.Rows(0).Item(1)
            bank_grid.Rows.Add(bank_no, bank_name, bank_value_str, Format(added_date, "dd/MM/yyyy"), added_by)
        Next
        next_bank_no += 1

        calc_ver_tot_cap()
        ver_tot_cap_txt.Text = FormatCurrency(ver_tot_cap)


    End Sub

    Private Sub calc_ver_tot_cap()
        ver_tot_cap = 0
        Dim no_of_banks As Integer = bank_grid.Rows.Count
        Dim idx As Integer
        For idx = 0 To no_of_banks - 1
            Dim bank_value As Decimal = 0
            Try
                bank_value = bank_grid.Rows(idx).Cells(2).Value
            Catch ex As Exception

            End Try
            If bank_value > 0 Then
                ver_tot_cap += bank_value
            End If
        Next
        ver_tot_cap_txt.Text = FormatCurrency(ver_tot_cap)
        Dim ver_tot_ce As Decimal = ver_tot_cap + ver_tot_equity - 30000
        If ver_tot_ce < 0 Then
            ver_tot_ce = 0
        End If
        ver_tot_ce_txt.Text = FormatCurrency(ver_tot_ce)
    End Sub
    Private Sub calc_ver_tot_equity()
        'sum of proprty values
        ver_tot_equity = 0
        Dim no_of_props As Integer = prop_grid.Rows.Count
        Dim idx As Integer
        For idx = 0 To no_of_props - 1
            Dim prop_value As Decimal = 0
            Try
                prop_value = prop_grid.Rows(idx).Cells(5).Value
            Catch ex As Exception

            End Try
            ver_tot_equity += prop_value
        Next

        'less any outstanding mortgages
        Dim no_of_creds As Integer = cred_grid.Rows.Count
        For idx = 0 To no_of_creds - 1
            If cred_grid.Rows(idx).Cells(1).Value <> "Mortgage" Then
                Continue For
            End If
            Dim mortgage_value_os As Decimal = 0
            Try
                mortgage_value_os = cred_grid.Rows(idx).Cells(6).Value
            Catch ex As Exception

            End Try
            ver_tot_equity -= mortgage_value_os
        Next
        ver_tot_equity_txt.Text = FormatCurrency(ver_tot_equity)
        Dim ver_tot_ce As Decimal = ver_tot_cap + ver_tot_equity - 30000
        If ver_tot_ce < 0 Then
            ver_tot_ce = 0
        End If
        ver_tot_ce_txt.Text = FormatCurrency(ver_tot_ce)
    End Sub
    Private Function get_assets(ByVal txt As String, ByVal row_number As Integer) As Integer
        Dim start_idx, search_length, idx2 As Integer
        Dim last_start As Integer = 0

        Static position As Integer
        If row_number = 0 Then
            position = 1
        End If
        start_idx = InStr(txt, "Cap Asset Type:")
        If start_idx > 0 And position = 1 Then
            position = 2
            If start_idx > last_start Then
                last_start = start_idx
            End If
            search_length = 15
            For idx2 = start_idx + search_length To txt.Length
                If Mid(txt, idx2, 1) = ";" Then
                    Exit For
                Else
                    cap_asset_type(asset_idx) = cap_asset_type(asset_idx) & Mid(txt, idx2, 1)
                End If
            Next
            If cap_asset_type(asset_idx) = "Additional Properties" Then
                add_prop_decl_txt.Text = "Yes"
            End If
        End If
        start_idx = InStr(txt, "Cap Amt:")
        If start_idx > 0 And position = 2 Then
            position = 3
            If start_idx > last_start Then
                last_start = start_idx
            End If
            search_length = 8
            For idx2 = start_idx + search_length To txt.Length
                If Mid(txt, idx2, 1) = ";" Then
                    Exit For
                Else
                    cap_amt(asset_idx) = cap_amt(asset_idx) & Mid(txt, idx2, 1)
                End If
            Next
            tot_cap_assets += cap_amt(asset_idx)
        End If
        start_idx = InStr(txt, "Cap Amt Verified:")
        search_length = 17
        If start_idx = 0 And debtorID = 5279087 Then
            start_idx = InStr(txt, "Verified:")
            search_length = 9
        End If
        If start_idx > 0 And position = 3 Then
            position = 1
            If start_idx > last_start Then
                last_start = start_idx
            End If

            For idx2 = start_idx + search_length To txt.Length
                If Mid(txt, idx2, 1) = ";" Then
                    Exit For
                Else
                    cap_amt_verified(asset_idx) = cap_amt_verified(asset_idx) & Mid(txt, idx2, 1)
                End If
            Next
            asset_idx += 1
        End If
        Return (last_start)
    End Function
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub asset_grid_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles asset_grid.CellValidating
        asset_ep.SetError(asset_grid, "")
        If e.ColumnIndex = 2 Then
            Dim asset_type As String = asset_grid.Rows(e.RowIndex).Cells(2).EditedFormattedValue
            If asset_type.Length = 0 Then
                asset_ep.SetError(asset_grid, "Invalid asset type")
                e.Cancel = True
            End If
            If asset_grid.Rows(e.RowIndex).Cells(1).Value <> "Onestep" Then
                asset_grid.Rows(e.RowIndex).Cells(1).Value = log_user
            End If
        End If
        If e.ColumnIndex = 3 Then
            Try
                Dim asset_amt As Decimal = asset_grid.Rows(e.RowIndex).Cells(3).EditedFormattedValue
            Catch ex As Exception
                asset_ep.SetError(asset_grid, "Invalid asset Amount")
                e.Cancel = True
            End Try

        End If
        If e.ColumnIndex = 4 Then
            Dim asset_amt_ver As String = LCase(asset_grid.Rows(e.RowIndex).Cells(4).EditedFormattedValue)
            If asset_amt_ver <> "no" And asset_amt_ver <> "yes" Then
                asset_ep.SetError(asset_grid, "Invalid asset verified")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub tomebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            LSC_WIP_tableTableAdapter.DeleteQuery1(debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        Try
            LSC_WIP_tableTableAdapter.InsertQuery(log_code, debtorID, Now, False)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        wip_txt.Text = "Allocated to me"
        mainmenu.myallocbtn.Enabled = True

        allow_grid_update()

        'check if a debtor record exists
        Me.LSC_Debtor_tableTableAdapter.FillBy(Me.FeesSQLDataSet.LSC_Debtor_table, debtorID)
        If Me.FeesSQLDataSet.LSC_Debtor_table.Rows.Count = 0 Then
            Try
                Me.LSC_Debtor_tableTableAdapter.InsertQuery1(debtorID, 1, Now, log_code, True)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Else
            Try
                Me.LSC_Debtor_tableTableAdapter.UpdateQuery21(1, debtorID)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            get_debtor_details()

        End If
        status_txt.Text = "WIP"
        status_no = 1
        mainmenu.myallocbtn.Enabled = True
        mainmenu.all_allocbtn.Enabled = True
        exp_check_cbox.Enabled = True
        match_cbox.Enabled = True
        undec_bank_cbox.Enabled = True
        cred_score_ud.Enabled = True
        cred_last_year_cbox.Enabled = True
        cons_pos_cbox.Enabled = True
        tot_cap_assets_cbox.Enabled = True
        validatebtn.Enabled = True
        completebtn.Enabled = False
    End Sub

    Private Sub nextbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nextbtn.Click
        wait_txt.Text = "WAIT"
        Application.DoEvents()
        get_next_case()
        display_case_details()
    End Sub

    Private Sub prop_grid_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles prop_grid.CellLeave
        
    End Sub
    


    Private Sub prop_grid_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles prop_grid.CellValidating
        ErrorProvider1.SetError(prop_grid, "")
        If prop_grid.ReadOnly Or moved_to_new_case Or prop_grid.Rows(e.RowIndex).IsNewRow Then
            Exit Sub
        End If

        If e.ColumnIndex = 2 Then
            If Microsoft.VisualBasic.Len(Trim(prop_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue)) = 0 Then
                ErrorProvider1.SetError(prop_grid, "Address can't be blank - use delete to remove property details")
                e.Cancel = True
            End If
        End If
        If e.ColumnIndex = 5 Then
            If Microsoft.VisualBasic.Len(Trim(prop_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue)) > 0 Then
                Dim likely_value As Decimal
                Try
                    likely_value = prop_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue
                Catch ex As Exception
                    ErrorProvider1.SetError(prop_grid, "Likely value not numeric or blank")
                    e.Cancel = True
                End Try
                If likely_value < 0 Then
                    ErrorProvider1.SetError(prop_grid, "Likely value can't be negative")
                    e.Cancel = True
                End If

            End If
        End If
    End Sub

    Private Sub prop_grid_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles prop_grid.CellValueChanged
        If debtorID = 0 Then
            Return
        End If
        If e.RowIndex < 0 Or moved_to_new_case Then
            calc_ver_tot_equity()
            Exit Sub
        End If
        If prop_grid.Rows(e.RowIndex).IsNewRow Then
            calc_ver_tot_equity()
            Exit Sub
        End If
        If prop_grid.Rows(e.RowIndex).Cells(0).Value = 0 Then
            Exit Sub
        End If
        If e.ColumnIndex = 1 Then
            Dim prop_no As Integer = prop_grid.Rows(e.RowIndex).Cells(0).Value
            If prop_no > 0 Then
                Try
                    LSC_Property_tableTableAdapter.UpdateQuery3(prop_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value, debtorID, prop_no)
                Catch ex As Exception
                    MsgBox(ex.Message)
                    Exit Sub
                End Try
                log_text = "Property main residence changed to " & prop_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value & " on property " & prop_address_entered
                logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update property", log_text, Now, log_code)
            End If
        End If
        If e.ColumnIndex = 2 Then
            Dim prop_no As Integer = prop_grid.Rows(e.RowIndex).Cells(0).Value
            If prop_no > 0 Then
                Try
                    LSC_Property_tableTableAdapter.UpdateQuery(prop_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value, debtorID, prop_no)
                Catch ex As Exception
                    MsgBox(ex.Message)
                    Exit Sub
                End Try
                log_text = "Property address changed from " & prop_address_entered & " to " & prop_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
                logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update property", log_text, Now, log_code)
            End If
        End If
        If e.ColumnIndex = 3 Then
            Dim prop_no As Integer = prop_grid.Rows(e.RowIndex).Cells(0).Value
            If prop_no > 0 Then
                Try
                    LSC_Property_tableTableAdapter.UpdateQuery4(prop_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value, debtorID, prop_no)
                Catch ex As Exception
                    MsgBox(ex.Message)
                    Exit Sub
                End Try
                log_text = "Property type changed from " & prop_type_entered & " to " & prop_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value & " on property " & prop_address_entered
                logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update property", log_text, Now, log_code)
            End If
        End If
        If e.ColumnIndex = 4 Then
            Dim prop_no As Integer = prop_grid.Rows(e.RowIndex).Cells(0).Value
            Try
                LSC_Property_tableTableAdapter.UpdateQuery1(prop_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value, debtorID, prop_no)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
            log_text = "LR check on property " & prop_address_entered & " changed to " & prop_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update property", log_text, Now, log_code)
        End If
        If e.ColumnIndex = 5 Then
            Dim prop_no As Integer = prop_grid.Rows(e.RowIndex).Cells(0).Value
            Dim likely_value As Decimal = 0
            Try
                likely_value = prop_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            Catch ex As Exception

            End Try

            Try
                LSC_Property_tableTableAdapter.UpdateQuery2(Format(likely_value, "F"), debtorID, prop_no)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
            calc_ver_tot_equity()
            log_text = "Likely value on property " & prop_address_entered & " changed from " & likely_value_entered & " to " & Format(likely_value, "�0.00")
            logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update property", log_text, Now, log_code)
        End If
    End Sub

    Private Sub prop_grid_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles prop_grid.RowEnter
        prop_no_entered = prop_grid.Rows(e.RowIndex).Cells(0).Value
        prop_address_entered = prop_grid.Rows(e.RowIndex).Cells(2).Value
        prop_type_entered = prop_grid.Rows(e.RowIndex).Cells(3).Value
        likely_value_entered = prop_grid.Rows(e.RowIndex).Cells(5).Value
        If Microsoft.VisualBasic.Left(likely_value_entered, 1) = "�" Then
            likely_value_entered = Microsoft.VisualBasic.Right(likely_value_entered, likely_value_entered.Length - 1)
        End If
        completebtn.Enabled = False
    End Sub

    Private Sub prop_grid_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles prop_grid.RowsRemoved

        Try
            If prop_no_entered < 1 Then
                Exit Sub
            End If

            If moved_to_new_case Then
                Exit Sub
            End If
            If prop_grid.Rows(e.RowIndex).IsNewRow Then
                Exit Sub
            End If
            If case_completed Then
                MsgBox("No changes can be made unless case is WIP - property not deleted - refresh case to see")
                Exit Sub
            End If
            LSC_Property_tableTableAdapter.DeleteQuery1(debtorID, prop_no_entered)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        calc_ver_tot_equity()
        log_text = "Property removed at address " & prop_address_entered
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Delete property", log_text, Now, log_code)

    End Sub

    Private Sub debt_prop_no_ud_Validated(ByVal sender As Object, ByVal e As System.EventArgs)
    End Sub


    Private Sub validatebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles validatebtn.Click
        insert_new_rows()

        If Not IsNumeric(tot_cap_assets_cbox.Text) Then
            MsgBox("Total Capital assets to be used must be numeric")
            Exit Sub
        End If

        If exp_check_cbox.Checked = True _
        And match_cbox.SelectedItem = "NA" Then
            MsgBox("Experian result has not been entered")
            Exit Sub
        End If

        Dim owners As Integer = 0
        If parents_cbox.Checked Then
            owners += 1
        End If
        If partner_cbox.Checked Then
            owners += 1
        End If
        If same_surname_cbox.Checked Then
            owners += 1
        End If
        If owners > 1 Then
            MsgBox("Only select one of owned by parents, partner and same surname")
            Exit Sub
        End If

        'check if any outstanding queries
        mainmenu.LSC_Query_tableTableAdapter.FillBy4(Me.FeesSQLDataSet.LSC_Query_table, debtorID)
        If FeesSQLDataSet.LSC_Query_table.Rows.Count > 0 Then
            MsgBox("Complete queries first")
            Exit Sub
        End If
        MsgBox("validation completed successfully")
        completebtn.Enabled = True
    End Sub

    Private Sub bank_grid_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles bank_grid.CellValidating
        ErrorProvider1.SetError(bank_grid, "")
        If bank_grid.ReadOnly Or moved_to_new_case Or bank_grid.Rows(e.RowIndex).IsNewRow Then
            Exit Sub
        End If
        If e.ColumnIndex = 1 Then
            If Microsoft.VisualBasic.Len(Trim(bank_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue)) = 0 Then
                ErrorProvider1.SetError(bank_grid, "Bank account name can't be blank - use delete to remove bank details")
                e.Cancel = True
            End If
        End If
        If e.ColumnIndex = 2 Then
            If Microsoft.VisualBasic.Len(Trim(bank_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue)) = 0 Then
                Exit Sub
            End If
            If Not IsNumeric(bank_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue) Then
                ErrorProvider1.SetError(bank_grid, "Bank Amount value not numeric")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub bank_grid_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles bank_grid.CellValueChanged

        If e.RowIndex < 0 Or moved_to_new_case Then
            calc_ver_tot_cap()
            Exit Sub
        End If
        If bank_grid.Rows(e.RowIndex).IsNewRow Then
            calc_ver_tot_cap()
            Exit Sub
        End If
        If bank_grid.Rows(e.RowIndex).Cells(0).Value = 0 Then
            Return
        End If
        If e.ColumnIndex = 1 Then
            Dim bank_no As Integer = bank_grid.Rows(e.RowIndex).Cells(0).Value
            If bank_no > 0 Then
                Try
                    LSC_bank_tableTableAdapter.UpdateQuery(bank_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value, debtorID, bank_no)
                Catch ex As Exception
                    MsgBox(ex.Message)
                    Exit Sub
                End Try
                log_text = "Bank account name changed from " & bank_name_entered & " to " & bank_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
                logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Bank", log_text, Now, log_code)
            End If
        ElseIf e.ColumnIndex = 2 Then
            Dim bank_amount As Decimal = 0
            Try
                bank_amount = bank_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            Catch ex As Exception

            End Try
            Try
                LSC_bank_tableTableAdapter.UpdateQuery1(bank_amount, debtorID, bank_grid.Rows(e.RowIndex).Cells(0).Value)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
            calc_ver_tot_cap()
            log_text = "Bank account " & bank_name_entered & " amount changed from " & bank_amt_entered & " to " & Format(bank_amount, "�0.00")
            logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Bank", log_text, Now, log_code)
        End If
    End Sub

    Private Sub bank_grid_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles bank_grid.RowEnter
        bank_no_entered = bank_grid.Rows(e.RowIndex).Cells(0).Value
        bank_name_entered = bank_grid.Rows(e.RowIndex).Cells(1).Value
        Try
            bank_amt_entered = bank_grid.Rows(e.RowIndex).Cells(2).Value
        Catch ex As Exception
            bank_amt_entered = Nothing
        End Try
        completebtn.Enabled = False
    End Sub

    Private Sub bank_grid_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles bank_grid.RowsRemoved
        Try
            If bank_no_entered < 1 Then
                Exit Sub
            End If
            If moved_to_new_case Then
                Exit Sub
            End If
            If case_completed Then
                MsgBox("No changes can be made unless case is WIP - bank not deleted - refresh case to see")
                Exit Sub
            End If
            LSC_bank_tableTableAdapter.DeleteQuery(debtorID, bank_no_entered)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        calc_ver_tot_cap()
        log_text = "Bank Account removed is " & bank_name_entered
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Delete bank account", log_text, Now, log_code)
    End Sub

    Private Sub cred_grid_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles cred_grid.CellValidating
        ErrorProvider1.SetError(cred_grid, "")
        If cred_grid.ReadOnly Or moved_to_new_case Or cred_grid.Rows(e.RowIndex).IsNewRow Then
            Exit Sub
        End If
        If e.ColumnIndex = 1 Then
            If Microsoft.VisualBasic.Len(Trim(cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue)) = 0 Then
                ErrorProvider1.SetError(cred_grid, "Credit Type can't be blank - use delete to remove credit type")
                e.Cancel = True
            End If
        End If
        If e.ColumnIndex = 4 Then
            If Microsoft.VisualBasic.Len(Trim(cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue)) > 0 And _
                cred_grid.Rows(e.RowIndex).Cells(3).EditedFormattedValue = False Then
                ErrorProvider1.SetError(cred_grid, "Joint with must be blank when joint flag is not set")
                e.Cancel = True
            End If
        End If
        If e.ColumnIndex = 5 Then
            If Microsoft.VisualBasic.Len(Trim(cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue)) = 0 Then
                Exit Sub
            End If
            If Not IsNumeric(cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue) Then
                ErrorProvider1.SetError(cred_grid, "Amount value not numeric or blank")
                e.Cancel = True
            End If
        End If
        If e.ColumnIndex = 6 Then
            If Microsoft.VisualBasic.Len(Trim(cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue)) = 0 Then
                Exit Sub
            End If
            If Not IsNumeric(cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue) Then
                ErrorProvider1.SetError(cred_grid, "Amount OS value not numeric or blank")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub cred_grid_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles cred_grid.CellValueChanged
        If debtorID = 0 Then
            Return
        End If
        If e.RowIndex < 0 Or moved_to_new_case Then
            calc_ver_tot_equity()
            Exit Sub
        End If
        If cred_grid.Rows(e.RowIndex).IsNewRow Then
            calc_ver_tot_equity()
            Exit Sub
        End If
        If cred_grid.Rows(e.RowIndex).Cells(0).Value = 0 Then
            Return
        End If
        If e.ColumnIndex = 1 Then
            Dim cred_no As Integer = cred_grid.Rows(e.RowIndex).Cells(0).Value
            If cred_no > 0 Then
                Try
                    LSC_Credit_tableTableAdapter.UpdateQuery(cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value, debtorID, cred_no)
                Catch ex As Exception
                    MsgBox(ex.Message)
                    Exit Sub
                End Try
                log_text = "Credit type changed from " & cred_type_entered & " to " & cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
                logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Credit", log_text, Now, log_code)
            End If
        End If
        If e.ColumnIndex = 2 Then
            Try
                LSC_Credit_tableTableAdapter.UpdateQuery1(cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value, debtorID, cred_grid.Rows(e.RowIndex).Cells(0).Value)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
            log_text = "Credit on " & cred_type_entered & " in default changed to " & cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Credit", log_text, Now, log_code)
        End If
        If e.ColumnIndex = 3 Then
            Try
                LSC_Credit_tableTableAdapter.UpdateQuery2(cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value, debtorID, cred_grid.Rows(e.RowIndex).Cells(0).Value)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
            log_text = "Credit on " & cred_type_entered & " joint changed to " & cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Credit", log_text, Now, log_code)
        End If
        If e.ColumnIndex = 4 Then
            Try
                LSC_Credit_tableTableAdapter.UpdateQuery3(cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value, debtorID, cred_grid.Rows(e.RowIndex).Cells(0).Value)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
            log_text = "Credit on " & cred_type_entered & " joint with changed from " & cred_joint_with_entered & " to " & cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Credit", log_text, Now, log_code)
        End If
        If e.ColumnIndex = 5 Then
            Dim cred_amount As Decimal = 0
            Try
                cred_amount = cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            Catch ex As Exception

            End Try

            Try
                LSC_Credit_tableTableAdapter.UpdateQuery4(cred_amount, debtorID, cred_grid.Rows(e.RowIndex).Cells(0).Value)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
            log_text = "Credit on " & cred_type_entered & " amount value changed from " & cred_value_entered & " to " & Format(cred_amount, "�0.00")
            logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Credit", log_text, Now, log_code)
        End If
        If e.ColumnIndex = 6 Then
            Dim cred_amount_os As Decimal = 0
            Try
                cred_amount_os = cred_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
            Catch ex As Exception

            End Try

            Try
                LSC_Credit_tableTableAdapter.UpdateQuery5(Format(cred_amount_os, "F"), debtorID, cred_grid.Rows(e.RowIndex).Cells(0).Value)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
            calc_ver_tot_equity()
            log_text = "Credit on " & cred_type_entered & " amount value OS changed from " & cred_value_os_entered & " to " & Format(cred_amount_os, "�0.00")
            logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Credit", log_text, Now, log_code)
        End If
    End Sub

    Private Sub cred_grid_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles cred_grid.RowEnter
        cred_no_entered = cred_grid.Rows(e.RowIndex).Cells(0).Value
        cred_type_entered = cred_grid.Rows(e.RowIndex).Cells(1).Value
        cred_joint_with_entered = cred_grid.Rows(e.RowIndex).Cells(4).Value
        cred_value_entered = cred_grid.Rows(e.RowIndex).Cells(5).Value
        cred_value_os_entered = cred_grid.Rows(e.RowIndex).Cells(6).Value
        completebtn.Enabled = False
    End Sub

    Private Sub cred_grid_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles cred_grid.RowsRemoved
        Try
            If cred_no_entered < 1 Then
                Exit Sub
            End If
            If moved_to_new_case Then
                Exit Sub
            End If
            If case_completed Then
                MsgBox("No changes can be made unless case is WIP - credit agreement not deleted - refresh case to see")
                Exit Sub
            End If
            LSC_Credit_tableTableAdapter.DeleteQuery(debtorID, cred_no_entered)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        calc_ver_tot_equity()
        log_text = "Credit agreement removed is " & cred_type_entered
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Delete credit", log_text, Now, log_code)
    End Sub

    Private Sub cred_grid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles cred_grid.CellContentClick

    End Sub

    Private Sub exp_check_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exp_check_cbox.CheckedChanged
        If moved_to_new_case Or debtorID = 0 Then
            Return
        End If
        Try
            LSC_Debtor_tableTableAdapter.UpdateQuery4(exp_check_cbox.Checked, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        log_text = "Experian checked changed to " & exp_check_cbox.Checked
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Debtor", log_text, Now, log_code)
    End Sub


    Private Sub undec_bank_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles undec_bank_cbox.CheckedChanged
        Try
            LSC_Debtor_tableTableAdapter.UpdateQuery6(undec_bank_cbox.Checked, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        log_text = "Undeclared bank account changed to " & undec_bank_cbox.Checked
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Debtor", log_text, Now, log_code)
    End Sub

    Private Sub cred_score_ud_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles cred_score_ud.Validating
        credit_score = cred_score_ud.Value
    End Sub

    Private Sub cred_score_ud_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cred_score_ud.ValueChanged
        If moved_to_new_case Then
            Return
        End If
        Try
            LSC_Debtor_tableTableAdapter.UpdateQuery7(cred_score_ud.Value, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        log_text = "Credit score changed from " & credit_score & " to " & cred_score_ud.Value
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Debtor", log_text, Now, log_code)
    End Sub

    Private Sub cred_last_year_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cred_last_year_cbox.CheckedChanged
        If moved_to_new_case Then
            Return
        End If
        Try
            LSC_Debtor_tableTableAdapter.UpdateQuery8(cred_last_year_cbox.Checked, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        log_text = "Credit in last 12 months changed"
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Debtor", log_text, Now, log_code)
    End Sub

    Private Sub cons_pos_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cons_pos_cbox.CheckedChanged
        If moved_to_new_case Then
            Return
        End If
        Try
            LSC_Debtor_tableTableAdapter.UpdateQuery9(cons_pos_cbox.Checked, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        log_text = "Consistent position changed to " & cons_pos_cbox.Checked
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Debtor", log_text, Now, log_code)
    End Sub


    Private Sub query_grid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles query_grid.CellContentClick

    End Sub

    Private Sub query_grid_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles query_grid.CellValidating
        ErrorProvider1.SetError(query_grid, "")
        If e.RowIndex < 0 Or moved_to_new_case Or query_grid.Rows(e.RowIndex).IsNewRow Then
            Exit Sub
        End If

        If e.ColumnIndex = 1 Then
            If Microsoft.VisualBasic.Len(Trim(query_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).EditedFormattedValue)) = 0 Then
                ErrorProvider1.SetError(query_grid, "Text can't be blank - use delete to remove a query")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub query_grid_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles query_grid.CellValueChanged
        If e.RowIndex < 0 Or moved_to_new_case Then
            Exit Sub
        End If
        If query_grid.Rows(e.RowIndex).IsNewRow Then
            Exit Sub
        End If
        If query_grid.Rows(e.RowIndex).Cells(0).Value = 0 Then
            Return
        End If
        If e.ColumnIndex = 1 Then
            Dim query_no As Integer = query_grid.Rows(e.RowIndex).Cells(0).Value
            If query_no > 0 Then
                Try
                    mainmenu.LSC_Query_tableTableAdapter.UpdateQuery1(query_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value, query_no, debtorID)
                Catch ex As Exception
                    MsgBox(ex.Message)
                    Exit Sub
                End Try
                log_text = "Query text changed from " & query_text_entered & " to " & query_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
                logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Query", log_text, Now, log_code)
            End If
        End If
        If e.ColumnIndex = 4 Then
            Dim query_no As Integer = query_grid.Rows(e.RowIndex).Cells(0).Value
            If query_no > 0 Then
                Try
                    mainmenu.LSC_Query_tableTableAdapter.UpdateQuery(query_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value, query_no, debtorID)
                Catch ex As Exception
                    MsgBox(ex.Message)
                    Exit Sub
                End Try
                log_text = "Query response changed from " & query_resp_entered & " to " & query_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
                logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Query", log_text, Now, log_code)
            End If
        End If
        If e.ColumnIndex = 5 Then
            Dim query_no As Integer = query_grid.Rows(e.RowIndex).Cells(0).Value
            If query_no > 0 Then
                If query_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value = True Then
                    Try
                        mainmenu.LSC_Query_tableTableAdapter.UpdateQuery2(Format(Now, "dd/MM/yyyy"), log_code, query_no, debtorID)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        Exit Sub
                    End Try
                Else
                    Try
                        mainmenu.LSC_Query_tableTableAdapter.UpdateQuery2(Format(Now, "dd/MM/yyyy"), 0, query_no, debtorID)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                        Exit Sub
                    End Try
                End If
                log_text = "Query completed changed to " & query_grid.Rows(e.RowIndex).Cells(e.ColumnIndex).Value & " on query no " & query_no_entered
                logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Query", log_text, Now, log_code)
            End If
        End If
    End Sub

    Private Sub query_grid_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles query_grid.RowEnter
        query_no_entered = query_grid.Rows(e.RowIndex).Cells(0).Value
        query_text_entered = query_grid.Rows(e.RowIndex).Cells(1).Value
        query_resp_entered = query_grid.Rows(e.RowIndex).Cells(4).Value
    End Sub


    Private Sub query_grid_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles query_grid.RowsRemoved
        Try
            If query_no_entered < 1 Or moved_to_new_case Then
                Exit Sub
            End If
            If case_completed Then
                MsgBox("No changes can be made unless case is WIP - query not deleted - refresh case to see")
                Exit Sub
            End If
            mainmenu.LSC_Query_tableTableAdapter.DeleteQuery(query_no_entered, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        calc_ver_tot_cap()
        log_text = "Query removed is " & query_text_entered
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Delete Query", log_text, Now, log_code)
    End Sub

    Private Sub completebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles completebtn.Click
        'update status 
        Dim status As Integer = 2
        Try
            'add assets to asset table
            Dim no_of_assets As Integer = asset_grid.Rows.Count - 1
            Dim idx As Integer
            'first delete any existing
            LSC_Asset_tableTableAdapter.DeleteQuery(debtorID)
            For idx = 0 To no_of_assets
                Dim asset_source As String = asset_grid.Rows(idx).Cells(0).Value
                Dim asset_type As String = asset_grid.Rows(idx).Cells(1).Value
                Dim asset_val As Decimal = Nothing
                Dim asset_val_str As String = Nothing
                Try
                    asset_val = asset_grid.Rows(idx).Cells(2).Value
                Catch ex As Exception

                End Try
                If asset_val <> Nothing Then
                    asset_val_str = Format(asset_val, "#.00")
                End If
                Dim asset_ver_str As String = asset_grid.Rows(idx).Cells(3).Value
                Dim asset_ver As Boolean
                If asset_ver_str = "Yes" Then
                    asset_ver = True
                Else
                    asset_ver = False
                End If
                Dim asset_ignore As Boolean = False 'asset_grid.Rows(idx).Cells(6).Value
                Try
                    LSC_Asset_tableTableAdapter.InsertQuery(debtorID, idx, asset_source, asset_type, asset_val_str, asset_ver, asset_ignore)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            Next
            Dim case_type As String = casetype_txt.Text
            Dim emp_status As String = emp_status_txt.Text
            Dim outcome As String = outcome_txt.Text
            Dim tot_cap_amt_decl As Decimal = tot_cap_assets_cbox.SelectedItem
            Dim tot_equity_decl As Decimal = equity_amt_txt.Text
            Dim ver_tot_cap_and_equity As Decimal = ver_tot_ce_txt.Text
            Dim residential_status As String = residential_desc_txt.Text
            Dim prop_type As String = prop_type_desc_txt.Text

            Dim cred_agreements, no_of_props, no_of_bank_accts As Integer
            cred_agreements = cred_grid.Rows.Count - 1
            no_of_props = prop_grid.Rows.Count - 1
            no_of_bank_accts = bank_grid.Rows.Count - 1
            Dim add_prop_decl As Boolean
            If add_prop_decl_txt.Text = "Yes" Then
                add_prop_decl = True
            Else
                add_prop_decl = False
            End If
            Dim lr_check As Boolean
            Try
                If lr_check_idx > 0 Then
                    lr_check = True
                Else
                    lr_check = False
                End If
            Catch ex As Exception
                lr_check = False
            End Try

            Dim owned_by_parents As Boolean = parents_cbox.Checked
            Dim owned_by_partner As Boolean = partner_cbox.Checked
            Dim owned_same_surname As Boolean = same_surname_cbox.Checked
            Dim prop_comments As String = prop_comments_txt.Text
            Dim no_of_bank_accts_decl As Integer = bank_acct_ud.Value

            LSC_Debtor_tableTableAdapter.UpdateQuery10(status, Format(Now, "dd/MM/yyyy"), log_code, _
            outcome, case_type, tot_cap_amt_decl, ver_tot_cap_and_equity, emp_status, residential_status, _
            prop_type, cred_agreements, no_of_props, no_of_bank_accts, _
            tot_equity_decl, add_prop_decl, exp_check_cbox.Checked, no_of_bank_accts_decl, debtorID)

            Dim appl_percent_str As String
            Dim appl_percent As Decimal
            Try
                appl_percent = appl_pcent_txt.Text
                appl_percent_str = Format(appl_percent, "#.00")
                LSC_Debtor_tableTableAdapter.UpdateQuery16(appl_percent_str, debtorID)
            Catch ex As Exception

            End Try

            Dim partner_percent_str As String
            Dim partner_percent As Decimal
            Try
                partner_percent = partner_pcent_txt.Text
                partner_percent_str = Format(partner_percent, "#.00")
                LSC_Debtor_tableTableAdapter.UpdateQuery17(partner_percent_str, debtorID)
            Catch ex As Exception

            End Try

        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        prop_grid.ReadOnly = True
        cred_grid.ReadOnly = True
        bank_grid.ReadOnly = True
        query_grid.ReadOnly = True
        exp_check_cbox.Enabled = False
        match_cbox.Enabled = False
        undec_bank_cbox.Enabled = False
        cred_score_ud.Enabled = False
        cred_last_year_cbox.Enabled = False
        cons_pos_cbox.Enabled = False
        status_txt.Text = "Pending"
        validatebtn.Enabled = False
        completebtn.Enabled = False
        case_completed = True
        parents_cbox.Enabled = False
        partner_cbox.Enabled = False
        same_surname_cbox.Enabled = False
        prop_comments_tbox.Enabled = False
        tot_cap_assets_cbox.Enabled = False
        bank_acct_ud.Enabled = False
        tomebtn.Enabled = True
        'delete from wip
        Try
            LSC_WIP_tableTableAdapter.DeleteQuery1(debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        wip_txt.Text = "Not Allocated"
        log_text = "Case completed"
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Completed Case", log_text, Now, log_code)
    End Sub

    Private Sub resetbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'update status back to 1 and completed_by to zero
        Try
            LSC_Debtor_tableTableAdapter.UpdateQuery11(1, 0, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        'update wip to user
        Try
            LSC_WIP_tableTableAdapter.InsertQuery(log_code, debtorID, Now, False)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        allow_grid_update()
        exp_check_cbox.Enabled = True
        match_cbox.Enabled = True
        undec_bank_cbox.Enabled = True
        cred_score_ud.Enabled = True
        cred_last_year_cbox.Enabled = True
        cons_pos_cbox.Enabled = True
        tot_cap_assets_cbox.Enabled = True
        status_txt.Text = "WIP"
        wip_txt.Text = "Allocated to me"
        mainmenu.myallocbtn.Enabled = True
        mainmenu.all_allocbtn.Enabled = True
        parents_cbox.Enabled = True
        partner_cbox.Enabled = True
        same_surname_cbox.Enabled = True
        prop_comments_tbox.Enabled = True
        tot_cap_assets_cbox.Enabled = True
        validatebtn.Enabled = True
        moved_to_new_case = False
    End Sub

    Private Sub asset_grid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles asset_grid.CellContentClick

    End Sub

    Private Sub tot_cap_assets_cbox_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles tot_cap_assets_cbox.Enter

    End Sub

    Private Sub tot_cap_assets_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tot_cap_assets_cbox.SelectedIndexChanged

    End Sub

    Private Sub tot_cap_assets_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles tot_cap_assets_cbox.Validated
        If moved_to_new_case Then
            Return
        End If
        Dim new_value As Decimal = tot_cap_assets_cbox.Text
        If new_value = tot_cap_assets Then Return
        Try
            Me.LSC_Debtor_tableTableAdapter.UpdateQuery19(new_value, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Return
        End Try

        log_text = "Total Capital assets changed from " & Format(tot_cap_assets, "�#.00") & " to " & Format(new_value, "�0.00")
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Debtor", log_text, Now, log_code)
        tot_cap_assets = tot_cap_assets_cbox.Text
    End Sub

    Private Sub tomebtn_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tomebtn.Click
        allocate_to_me()
    End Sub

    Private Sub match_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles match_cbox.Validated
        If moved_to_new_case Then
            Exit Sub
        End If
        If case_completed Then
            MsgBox("No changes can be made unless case is WIP ")
            Exit Sub
        End If

        Dim match_str_new As String = "NA"
        Select Case match_cbox.SelectedIndex
            Case 0
                match_str_new = "MATCH"
            Case 1
                match_str_new = "NO MATCH"
            Case 2
                match_str_new = "SURNAME MATCH"
        End Select
        Try
            LSC_Debtor_tableTableAdapter.UpdateQuery5(match_str_new, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        log_text = "Experian match changed from " & match_str & " to " & match_str_new
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Debtor", log_text, Now, log_code)
    End Sub

    Private Sub match_cbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles match_cbox.Validating
    End Sub

    Private Sub parents_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles parents_cbox.CheckedChanged
        If moved_to_new_case Then
            Return
        End If
        Try
            Me.LSC_Debtor_tableTableAdapter.UpdateQuery12(parents_cbox.Checked, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Return
        End Try
        log_text = "Owned by parents changed to " & parents_cbox.Checked
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Debtor", log_text, Now, log_code)
    End Sub

    Private Sub partner_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles partner_cbox.CheckedChanged
        If moved_to_new_case Then
            Return
        End If
        Try
            Me.LSC_Debtor_tableTableAdapter.UpdateQuery13(partner_cbox.Checked, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Return
        End Try
        log_text = "Owned by partner changed to " & partner_cbox.Checked
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Debtor", log_text, Now, log_code)
    End Sub

    Private Sub same_surname_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles same_surname_cbox.CheckedChanged
        If moved_to_new_case Then
            Return
        End If
        Try
            Me.LSC_Debtor_tableTableAdapter.UpdateQuery14(same_surname_cbox.Checked, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Return
        End Try
        log_text = "Owned by same surname changed to " & same_surname_cbox.Checked
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Debtor", log_text, Now, log_code)
    End Sub

    Private Sub prop_comments_tbox_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles prop_comments_tbox.Enter
        prop_comments_entered = prop_comments_tbox.Text
    End Sub

    Private Sub prop_comments_tbox_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles prop_comments_tbox.Leave
        If moved_to_new_case Then
            Return
        End If
        If prop_comments_entered = prop_comments_tbox.Text Then
            Return
        End If
        Try
            Me.LSC_Debtor_tableTableAdapter.UpdateQuery15(prop_comments_tbox.Text, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Return
        End Try
        log_text = "property comments changed to " & prop_comments_tbox.Text
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Debtor", log_text, Now, log_code)
    End Sub

    Private Sub prop_comments_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles prop_comments_tbox.TextChanged
        
    End Sub

    Private Sub bank_acct_ud_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles bank_acct_ud.Validating
        no_of_bank_accts_decl = bank_acct_ud.Value
    End Sub

    Private Sub bank_acct_ud_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bank_acct_ud.ValueChanged
        If moved_to_new_case Then
            Return
        End If
        Try
            Me.LSC_Debtor_tableTableAdapter.UpdateQuery18(bank_acct_ud.Value, debtorID)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        log_text = "No of bank accounts declared changed from " & no_of_bank_accts_decl & " to " & bank_acct_ud.Value
        logon.LSC_Log_tableTableAdapter.InsertQuery(debtorID, "Update Debtor", log_text, Now, log_code)
    End Sub

    Private Sub TabPage4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabPage4.Click

    End Sub

    Private Sub logbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles logbtn.Click
        insert_new_rows()
        logfrm.ShowDialog()
    End Sub

    Private Sub match_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles match_cbox.SelectedIndexChanged

    End Sub

    Private Sub tot_cap_assets_cbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles tot_cap_assets_cbox.Validating
        If moved_to_new_case Then
            Return
        End If
        ErrorProvider1.SetError(tot_cap_assets_cbox, "")
        Try
            Dim new_value As Decimal = tot_cap_assets_cbox.Text
        Catch ex As Exception
            ErrorProvider1.SetError(tot_cap_assets_cbox, "Must be numeric")
            e.Cancel = True
        End Try

    End Sub

    Private Sub prop_grid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles prop_grid.CellContentClick

    End Sub

    Private Sub bank_grid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles bank_grid.CellContentClick

    End Sub

    Private Sub TabPage3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TabPage3.Click

    End Sub

    Private Sub googlebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles googlebtn.Click
        googlefrm.ShowDialog()
    End Sub

    Private Sub outcome_txt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles outcome_txt.Click

    End Sub

    Private Sub add_prop_grid_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles partner_grid.CellContentClick

    End Sub
End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class casefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Label1 = New System.Windows.Forms.Label
        Me.maatid_txt = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.exitbtn = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.label5 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.loaded_txt = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.dob_txt = New System.Windows.Forms.Label
        Me.Casetype = New System.Windows.Forms.Label
        Me.casetype_txt = New System.Windows.Forms.Label
        Me.outcome = New System.Windows.Forms.Label
        Me.outcome_txt = New System.Windows.Forms.Label
        Me.stage = New System.Windows.Forms.Label
        Me.stage_txt = New System.Windows.Forms.Label
        Me.emp_status = New System.Windows.Forms.Label
        Me.emp_status_txt = New System.Windows.Forms.Label
        Me.inc_contrib_txt = New System.Windows.Forms.Label
        Me.inc_contrib = New System.Windows.Forms.Label
        Me.five_sixths_txt = New System.Windows.Forms.Label
        Me.five_sixths = New System.Windows.Forms.Label
        Me.inc_paid_txt = New System.Windows.Forms.Label
        Me.inc_paid = New System.Windows.Forms.Label
        Me.equity_amt_ver = New System.Windows.Forms.Label
        Me.equity_amt_txt = New System.Windows.Forms.Label
        Me.equity_amt_ver_txt = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.asset_grid = New System.Windows.Forms.DataGridView
        Me.source = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.asset_type = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.asset_amt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.asset_amt_ver = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.asset_ep = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.mode_txt = New System.Windows.Forms.Label
        Me.nextbtn = New System.Windows.Forms.Button
        Me.onestep_status_txt = New System.Windows.Forms.Label
        Me.status = New System.Windows.Forms.Label
        Me.partner_equity = New System.Windows.Forms.Label
        Me.partner_equity_amt_txt = New System.Windows.Forms.Label
        Me.appl_equity = New System.Windows.Forms.Label
        Me.appl_equity_amt_txt = New System.Windows.Forms.Label
        Me.final_costs_tab = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.prop_grid = New System.Windows.Forms.DataGridView
        Me.prop_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.main_residence = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.address = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.prop_type = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.lr_check = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.likely_value = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.added_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.added_by = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.bank_grid = New System.Windows.Forms.DataGridView
        Me.bank_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bank_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bank_amount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bank_added_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bank_added_by = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.cred_grid = New System.Windows.Forms.DataGridView
        Me.cred_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cred_type = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.cred_default = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.cred_joint = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.cred_joint_with = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.cred_value = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cred_value_os = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cred_date_added = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cred_added_by = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabPage5 = New System.Windows.Forms.TabPage
        Me.query_grid = New System.Windows.Forms.DataGridView
        Me.query_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_text = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_Date_added = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_added_by = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_response = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_comp_cbox = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.query_completed_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_completed_by = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabPage6 = New System.Windows.Forms.TabPage
        Me.lr_grid = New System.Windows.Forms.DataGridView
        Me.lr_title = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lr_tenure = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lr_prop = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.lr_charge = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabPage7 = New System.Windows.Forms.TabPage
        Me.search_grid = New System.Windows.Forms.DataGridView
        Me.search_1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.search_2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.search_3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.search_4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.search_5 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.TabPage8 = New System.Windows.Forms.TabPage
        Me.bank_details_grid = New System.Windows.Forms.DataGridView
        Me.bank_details = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabPage9 = New System.Windows.Forms.TabPage
        Me.final_costs_grid = New System.Windows.Forms.DataGridView
        Me.final_cost = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.final_vat = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.LGFS_tot = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.LGFS_vat = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AGFS_tot = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.AGFS_vat = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabPage10 = New System.Windows.Forms.TabPage
        Me.crystal_grid = New System.Windows.Forms.DataGridView
        Me.so_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.calc_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.crystallisation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.sixth_adj = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.wip_txt = New System.Windows.Forms.Label
        Me.status_txt = New System.Windows.Forms.Label
        Me.PostCode = New System.Windows.Forms.Label
        Me.validatebtn = New System.Windows.Forms.Button
        Me.completebtn = New System.Windows.Forms.Button
        Me.exp_check_cbox = New System.Windows.Forms.CheckBox
        Me.match_cbox = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.undec_bank_cbox = New System.Windows.Forms.CheckBox
        Me.cred_score_ud = New System.Windows.Forms.NumericUpDown
        Me.Label12 = New System.Windows.Forms.Label
        Me.cred_last_year_cbox = New System.Windows.Forms.CheckBox
        Me.cons_pos_cbox = New System.Windows.Forms.CheckBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.ver_tot_cap_txt = New System.Windows.Forms.Label
        Me.ver_tot_equity_txt = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.ver_tot_ce_txt = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.tot_cap_assets_cbox = New System.Windows.Forms.ComboBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.residential_desc_txt = New System.Windows.Forms.Label
        Me.prop_type_desc_txt = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.appl_pcent_txt = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.partner_pcent_txt = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.tomebtn = New System.Windows.Forms.Button
        Me.Label7 = New System.Windows.Forms.Label
        Me.add_prop_decl_txt = New System.Windows.Forms.Label
        Me.parents_cbox = New System.Windows.Forms.CheckBox
        Me.partner_cbox = New System.Windows.Forms.CheckBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.same_surname_cbox = New System.Windows.Forms.CheckBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.prop_comments_tbox = New System.Windows.Forms.TextBox
        Me.prop_comments_txt = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.bank_acct_ud = New System.Windows.Forms.NumericUpDown
        Me.Label25 = New System.Windows.Forms.Label
        Me.wait_txt = New System.Windows.Forms.Label
        Me.logbtn = New System.Windows.Forms.Button
        Me.googlebtn = New System.Windows.Forms.Button
        Me.LSCPropertyTableBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FeesSQLDataSet = New CapitalandEquity.FeesSQLDataSet
        Me.LSC_Property_tableTableAdapter = New CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Property_tableTableAdapter
        Me.LSC_WIP_tableTableAdapter = New CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_WIP_tableTableAdapter
        Me.LSC_Status_tableTableAdapter = New CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Status_tableTableAdapter
        Me.LSC_bank_tableTableAdapter = New CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_bank_tableTableAdapter
        Me.LSC_Credit_tableTableAdapter = New CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Credit_tableTableAdapter
        Me.LSC_Debtor_tableTableAdapter = New CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Debtor_tableTableAdapter
        Me.LSC_Asset_tableTableAdapter = New CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Asset_tableTableAdapter
        Me.comp_date_txt = New System.Windows.Forms.Label
        Me.address_tbox = New System.Windows.Forms.TextBox
        Me.debtor_tbox = New System.Windows.Forms.TextBox
        Me.pcode_tbox = New System.Windows.Forms.TextBox
        Me.name_tbox = New System.Windows.Forms.TextBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.ver_tot_c_and_e_from_feed = New System.Windows.Forms.Label
        Me.TabPage11 = New System.Windows.Forms.TabPage
        Me.partner_grid = New System.Windows.Forms.DataGridView
        Me.first_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.last_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dob = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.nino = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.partner_emp_status = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TabPage12 = New System.Windows.Forms.TabPage
        Me.inc_evidence_grid = New System.Windows.Forms.DataGridView
        Me.inc_evidence = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.asset_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.asset_ep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.final_costs_tab.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.prop_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.bank_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.cred_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage5.SuspendLayout()
        CType(Me.query_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage6.SuspendLayout()
        CType(Me.lr_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage7.SuspendLayout()
        CType(Me.search_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.TabPage8.SuspendLayout()
        CType(Me.bank_details_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage9.SuspendLayout()
        CType(Me.final_costs_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage10.SuspendLayout()
        CType(Me.crystal_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.cred_score_ud, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.bank_acct_ud, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LSCPropertyTableBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage11.SuspendLayout()
        CType(Me.partner_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage12.SuspendLayout()
        CType(Me.inc_evidence_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(91, 19)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Case Number"
        '
        'maatid_txt
        '
        Me.maatid_txt.AutoSize = True
        Me.maatid_txt.Location = New System.Drawing.Point(168, 45)
        Me.maatid_txt.Name = "maatid_txt"
        Me.maatid_txt.Size = New System.Drawing.Size(38, 13)
        Me.maatid_txt.TabIndex = 2
        Me.maatid_txt.Text = "maatid"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(168, 19)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "MAAT ID"
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(837, 474)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 15
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(513, 19)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Name"
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Location = New System.Drawing.Point(714, 19)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(45, 13)
        Me.label5.TabIndex = 8
        Me.label5.Text = "Address"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 19)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Loaded Date"
        '
        'loaded_txt
        '
        Me.loaded_txt.AutoSize = True
        Me.loaded_txt.Location = New System.Drawing.Point(12, 45)
        Me.loaded_txt.Name = "loaded_txt"
        Me.loaded_txt.Size = New System.Drawing.Size(39, 13)
        Me.loaded_txt.TabIndex = 10
        Me.loaded_txt.Text = "Label6"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(337, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(30, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "DOB"
        '
        'dob_txt
        '
        Me.dob_txt.AutoSize = True
        Me.dob_txt.Location = New System.Drawing.Point(325, 45)
        Me.dob_txt.Name = "dob_txt"
        Me.dob_txt.Size = New System.Drawing.Size(25, 13)
        Me.dob_txt.TabIndex = 12
        Me.dob_txt.Text = "dob"
        '
        'Casetype
        '
        Me.Casetype.AutoSize = True
        Me.Casetype.Location = New System.Drawing.Point(230, 91)
        Me.Casetype.Name = "Casetype"
        Me.Casetype.Size = New System.Drawing.Size(51, 13)
        Me.Casetype.TabIndex = 13
        Me.Casetype.Text = "Casetype"
        '
        'casetype_txt
        '
        Me.casetype_txt.AutoSize = True
        Me.casetype_txt.Location = New System.Drawing.Point(212, 119)
        Me.casetype_txt.Name = "casetype_txt"
        Me.casetype_txt.Size = New System.Drawing.Size(50, 13)
        Me.casetype_txt.TabIndex = 14
        Me.casetype_txt.Text = "casetype"
        '
        'outcome
        '
        Me.outcome.AutoSize = True
        Me.outcome.Location = New System.Drawing.Point(362, 93)
        Me.outcome.Name = "outcome"
        Me.outcome.Size = New System.Drawing.Size(50, 13)
        Me.outcome.TabIndex = 15
        Me.outcome.Text = "Outcome"
        '
        'outcome_txt
        '
        Me.outcome_txt.AutoSize = True
        Me.outcome_txt.Location = New System.Drawing.Point(362, 119)
        Me.outcome_txt.Name = "outcome_txt"
        Me.outcome_txt.Size = New System.Drawing.Size(48, 13)
        Me.outcome_txt.TabIndex = 16
        Me.outcome_txt.Text = "outcome"
        '
        'stage
        '
        Me.stage.AutoSize = True
        Me.stage.Location = New System.Drawing.Point(400, 19)
        Me.stage.Name = "stage"
        Me.stage.Size = New System.Drawing.Size(35, 13)
        Me.stage.TabIndex = 17
        Me.stage.Text = "Stage"
        '
        'stage_txt
        '
        Me.stage_txt.AutoSize = True
        Me.stage_txt.Location = New System.Drawing.Point(400, 45)
        Me.stage_txt.Name = "stage_txt"
        Me.stage_txt.Size = New System.Drawing.Size(33, 13)
        Me.stage_txt.TabIndex = 18
        Me.stage_txt.Text = "stage"
        '
        'emp_status
        '
        Me.emp_status.AutoSize = True
        Me.emp_status.Location = New System.Drawing.Point(476, 91)
        Me.emp_status.Name = "emp_status"
        Me.emp_status.Size = New System.Drawing.Size(61, 13)
        Me.emp_status.TabIndex = 19
        Me.emp_status.Text = "Emp Status"
        '
        'emp_status_txt
        '
        Me.emp_status_txt.AutoSize = True
        Me.emp_status_txt.Location = New System.Drawing.Point(476, 119)
        Me.emp_status_txt.Name = "emp_status_txt"
        Me.emp_status_txt.Size = New System.Drawing.Size(55, 13)
        Me.emp_status_txt.TabIndex = 20
        Me.emp_status_txt.Text = "empstatus"
        '
        'inc_contrib_txt
        '
        Me.inc_contrib_txt.AutoSize = True
        Me.inc_contrib_txt.Location = New System.Drawing.Point(13, 178)
        Me.inc_contrib_txt.Name = "inc_contrib_txt"
        Me.inc_contrib_txt.Size = New System.Drawing.Size(59, 13)
        Me.inc_contrib_txt.TabIndex = 21
        Me.inc_contrib_txt.Text = "inc_contrib"
        '
        'inc_contrib
        '
        Me.inc_contrib.AutoSize = True
        Me.inc_contrib.Location = New System.Drawing.Point(13, 151)
        Me.inc_contrib.Name = "inc_contrib"
        Me.inc_contrib.Size = New System.Drawing.Size(106, 13)
        Me.inc_contrib.TabIndex = 22
        Me.inc_contrib.Text = "Income Contributions"
        '
        'five_sixths_txt
        '
        Me.five_sixths_txt.AutoSize = True
        Me.five_sixths_txt.Location = New System.Drawing.Point(142, 178)
        Me.five_sixths_txt.Name = "five_sixths_txt"
        Me.five_sixths_txt.Size = New System.Drawing.Size(10, 13)
        Me.five_sixths_txt.TabIndex = 23
        Me.five_sixths_txt.Text = " "
        '
        'five_sixths
        '
        Me.five_sixths.AutoSize = True
        Me.five_sixths.Location = New System.Drawing.Point(142, 151)
        Me.five_sixths.Name = "five_sixths"
        Me.five_sixths.Size = New System.Drawing.Size(47, 13)
        Me.five_sixths.TabIndex = 24
        Me.five_sixths.Text = "5/6 paid"
        '
        'inc_paid_txt
        '
        Me.inc_paid_txt.AutoSize = True
        Me.inc_paid_txt.Location = New System.Drawing.Point(260, 178)
        Me.inc_paid_txt.Name = "inc_paid_txt"
        Me.inc_paid_txt.Size = New System.Drawing.Size(109, 13)
        Me.inc_paid_txt.TabIndex = 25
        Me.inc_paid_txt.Text = "Inc Contributions paid"
        '
        'inc_paid
        '
        Me.inc_paid.AutoSize = True
        Me.inc_paid.Location = New System.Drawing.Point(261, 151)
        Me.inc_paid.Name = "inc_paid"
        Me.inc_paid.Size = New System.Drawing.Size(82, 13)
        Me.inc_paid.TabIndex = 26
        Me.inc_paid.Text = "Inc Contrib Paid"
        '
        'equity_amt_ver
        '
        Me.equity_amt_ver.AutoSize = True
        Me.equity_amt_ver.Location = New System.Drawing.Point(796, 178)
        Me.equity_amt_ver.Name = "equity_amt_ver"
        Me.equity_amt_ver.Size = New System.Drawing.Size(95, 13)
        Me.equity_amt_ver.TabIndex = 27
        Me.equity_amt_ver.Text = "Equity Amt Verified"
        '
        'equity_amt_txt
        '
        Me.equity_amt_txt.AutoSize = True
        Me.equity_amt_txt.Location = New System.Drawing.Point(694, 191)
        Me.equity_amt_txt.Name = "equity_amt_txt"
        Me.equity_amt_txt.Size = New System.Drawing.Size(55, 13)
        Me.equity_amt_txt.TabIndex = 28
        Me.equity_amt_txt.Text = "equity amt"
        '
        'equity_amt_ver_txt
        '
        Me.equity_amt_ver_txt.AutoSize = True
        Me.equity_amt_ver_txt.Location = New System.Drawing.Point(795, 191)
        Me.equity_amt_ver_txt.Name = "equity_amt_ver_txt"
        Me.equity_amt_ver_txt.Size = New System.Drawing.Size(73, 13)
        Me.equity_amt_ver_txt.TabIndex = 29
        Me.equity_amt_ver_txt.Text = "equity amt ver"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(695, 178)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(84, 13)
        Me.Label10.TabIndex = 30
        Me.Label10.Text = "Total Equity Amt"
        '
        'asset_grid
        '
        Me.asset_grid.AllowUserToAddRows = False
        Me.asset_grid.AllowUserToDeleteRows = False
        Me.asset_grid.AllowUserToOrderColumns = True
        Me.asset_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.asset_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.source, Me.asset_type, Me.asset_amt, Me.asset_amt_ver})
        Me.asset_grid.Location = New System.Drawing.Point(3, 3)
        Me.asset_grid.Name = "asset_grid"
        Me.asset_grid.Size = New System.Drawing.Size(879, 178)
        Me.asset_grid.TabIndex = 31
        '
        'source
        '
        Me.source.HeaderText = "Source"
        Me.source.Name = "source"
        Me.source.ReadOnly = True
        '
        'asset_type
        '
        Me.asset_type.HeaderText = "Asset Type"
        Me.asset_type.Name = "asset_type"
        Me.asset_type.ReadOnly = True
        Me.asset_type.Width = 400
        '
        'asset_amt
        '
        Me.asset_amt.HeaderText = "Asset Amt"
        Me.asset_amt.Name = "asset_amt"
        Me.asset_amt.ReadOnly = True
        '
        'asset_amt_ver
        '
        Me.asset_amt_ver.HeaderText = "Verified"
        Me.asset_amt_ver.Name = "asset_amt_ver"
        Me.asset_amt_ver.ReadOnly = True
        Me.asset_amt_ver.Width = 50
        '
        'asset_ep
        '
        Me.asset_ep.ContainerControl = Me
        '
        'mode_txt
        '
        Me.mode_txt.AutoSize = True
        Me.mode_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mode_txt.Location = New System.Drawing.Point(12, 112)
        Me.mode_txt.Name = "mode_txt"
        Me.mode_txt.Size = New System.Drawing.Size(53, 20)
        Me.mode_txt.TabIndex = 33
        Me.mode_txt.Text = "mode"
        '
        'nextbtn
        '
        Me.nextbtn.Location = New System.Drawing.Point(144, 215)
        Me.nextbtn.Name = "nextbtn"
        Me.nextbtn.Size = New System.Drawing.Size(168, 23)
        Me.nextbtn.TabIndex = 1
        Me.nextbtn.Text = "Get next unallocated case"
        Me.nextbtn.UseVisualStyleBackColor = True
        '
        'onestep_status_txt
        '
        Me.onestep_status_txt.AutoSize = True
        Me.onestep_status_txt.Location = New System.Drawing.Point(246, 45)
        Me.onestep_status_txt.Name = "onestep_status_txt"
        Me.onestep_status_txt.Size = New System.Drawing.Size(49, 13)
        Me.onestep_status_txt.TabIndex = 35
        Me.onestep_status_txt.Text = "os status"
        '
        'status
        '
        Me.status.AutoSize = True
        Me.status.Location = New System.Drawing.Point(246, 19)
        Me.status.Name = "status"
        Me.status.Size = New System.Drawing.Size(66, 13)
        Me.status.TabIndex = 36
        Me.status.Text = "Status (O/C)"
        '
        'partner_equity
        '
        Me.partner_equity.AutoSize = True
        Me.partner_equity.Location = New System.Drawing.Point(569, 178)
        Me.partner_equity.Name = "partner_equity"
        Me.partner_equity.Size = New System.Drawing.Size(94, 13)
        Me.partner_equity.TabIndex = 37
        Me.partner_equity.Text = "Partner Equity Amt"
        '
        'partner_equity_amt_txt
        '
        Me.partner_equity_amt_txt.AutoSize = True
        Me.partner_equity_amt_txt.Location = New System.Drawing.Point(568, 191)
        Me.partner_equity_amt_txt.Name = "partner_equity_amt_txt"
        Me.partner_equity_amt_txt.Size = New System.Drawing.Size(91, 13)
        Me.partner_equity_amt_txt.TabIndex = 38
        Me.partner_equity_amt_txt.Text = "partner equity amt"
        '
        'appl_equity
        '
        Me.appl_equity.AutoSize = True
        Me.appl_equity.Location = New System.Drawing.Point(438, 178)
        Me.appl_equity.Name = "appl_equity"
        Me.appl_equity.Size = New System.Drawing.Size(104, 13)
        Me.appl_equity.TabIndex = 39
        Me.appl_equity.Text = "Applicant Equity Amt"
        '
        'appl_equity_amt_txt
        '
        Me.appl_equity_amt_txt.AutoSize = True
        Me.appl_equity_amt_txt.Location = New System.Drawing.Point(461, 191)
        Me.appl_equity_amt_txt.Name = "appl_equity_amt_txt"
        Me.appl_equity_amt_txt.Size = New System.Drawing.Size(58, 13)
        Me.appl_equity_amt_txt.TabIndex = 40
        Me.appl_equity_amt_txt.Text = "appl equity"
        '
        'final_costs_tab
        '
        Me.final_costs_tab.Controls.Add(Me.TabPage1)
        Me.final_costs_tab.Controls.Add(Me.TabPage2)
        Me.final_costs_tab.Controls.Add(Me.TabPage3)
        Me.final_costs_tab.Controls.Add(Me.TabPage5)
        Me.final_costs_tab.Controls.Add(Me.TabPage6)
        Me.final_costs_tab.Controls.Add(Me.TabPage7)
        Me.final_costs_tab.Controls.Add(Me.TabPage4)
        Me.final_costs_tab.Controls.Add(Me.TabPage8)
        Me.final_costs_tab.Controls.Add(Me.TabPage9)
        Me.final_costs_tab.Controls.Add(Me.TabPage10)
        Me.final_costs_tab.Controls.Add(Me.TabPage11)
        Me.final_costs_tab.Controls.Add(Me.TabPage12)
        Me.final_costs_tab.Location = New System.Drawing.Point(12, 503)
        Me.final_costs_tab.Name = "final_costs_tab"
        Me.final_costs_tab.SelectedIndex = 0
        Me.final_costs_tab.Size = New System.Drawing.Size(940, 213)
        Me.final_costs_tab.TabIndex = 16
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.prop_grid)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(932, 187)
        Me.TabPage1.TabIndex = 4
        Me.TabPage1.Text = "Property Details"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'prop_grid
        '
        Me.prop_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.prop_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.prop_no, Me.main_residence, Me.address, Me.prop_type, Me.lr_check, Me.likely_value, Me.added_date, Me.added_by})
        Me.prop_grid.Location = New System.Drawing.Point(3, 3)
        Me.prop_grid.Name = "prop_grid"
        Me.prop_grid.Size = New System.Drawing.Size(923, 188)
        Me.prop_grid.TabIndex = 0
        '
        'prop_no
        '
        Me.prop_no.HeaderText = "Prop No"
        Me.prop_no.Name = "prop_no"
        Me.prop_no.Visible = False
        '
        'main_residence
        '
        Me.main_residence.HeaderText = "Main Residence"
        Me.main_residence.Name = "main_residence"
        Me.main_residence.Width = 70
        '
        'address
        '
        Me.address.HeaderText = "Address"
        Me.address.Name = "address"
        Me.address.Width = 330
        '
        'prop_type
        '
        Me.prop_type.HeaderText = "Property Type"
        Me.prop_type.Items.AddRange(New Object() {"Detached", "Semi Detached", "Bungalow", "Terraced House", "Flat", "Maisonette", "Land", "Commercial", "N/A"})
        Me.prop_type.Name = "prop_type"
        Me.prop_type.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.prop_type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.prop_type.Width = 120
        '
        'lr_check
        '
        Me.lr_check.HeaderText = "LR Check"
        Me.lr_check.Name = "lr_check"
        Me.lr_check.Width = 50
        '
        'likely_value
        '
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.likely_value.DefaultCellStyle = DataGridViewCellStyle2
        Me.likely_value.HeaderText = "Likely value"
        Me.likely_value.Name = "likely_value"
        '
        'added_date
        '
        Me.added_date.HeaderText = "Added Date"
        Me.added_date.Name = "added_date"
        Me.added_date.ReadOnly = True
        '
        'added_by
        '
        Me.added_by.HeaderText = "Added By"
        Me.added_by.Name = "added_by"
        Me.added_by.ReadOnly = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.bank_grid)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(932, 187)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Bank Accounts"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'bank_grid
        '
        Me.bank_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.bank_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.bank_no, Me.bank_name, Me.bank_amount, Me.bank_added_date, Me.bank_added_by})
        Me.bank_grid.Location = New System.Drawing.Point(0, 0)
        Me.bank_grid.Name = "bank_grid"
        Me.bank_grid.Size = New System.Drawing.Size(469, 181)
        Me.bank_grid.TabIndex = 0
        '
        'bank_no
        '
        Me.bank_no.HeaderText = "Column1"
        Me.bank_no.Name = "bank_no"
        Me.bank_no.Visible = False
        '
        'bank_name
        '
        Me.bank_name.HeaderText = "name"
        Me.bank_name.Name = "bank_name"
        '
        'bank_amount
        '
        Me.bank_amount.HeaderText = "Amount"
        Me.bank_amount.Name = "bank_amount"
        '
        'bank_added_date
        '
        Me.bank_added_date.HeaderText = "Added Date"
        Me.bank_added_date.Name = "bank_added_date"
        Me.bank_added_date.ReadOnly = True
        '
        'bank_added_by
        '
        Me.bank_added_by.HeaderText = "Added By"
        Me.bank_added_by.Name = "bank_added_by"
        Me.bank_added_by.ReadOnly = True
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.cred_grid)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(932, 187)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Credit Agreements"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'cred_grid
        '
        Me.cred_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.cred_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cred_no, Me.cred_type, Me.cred_default, Me.cred_joint, Me.cred_joint_with, Me.cred_value, Me.cred_value_os, Me.cred_date_added, Me.cred_added_by})
        Me.cred_grid.Location = New System.Drawing.Point(3, 15)
        Me.cred_grid.Name = "cred_grid"
        Me.cred_grid.Size = New System.Drawing.Size(790, 169)
        Me.cred_grid.TabIndex = 0
        '
        'cred_no
        '
        Me.cred_no.HeaderText = "Column1"
        Me.cred_no.Name = "cred_no"
        Me.cred_no.Visible = False
        '
        'cred_type
        '
        Me.cred_type.HeaderText = "Type"
        Me.cred_type.Items.AddRange(New Object() {"Mortgage", "Loan", "Communications", "Utilities", "Finance", "Credit cards", "NA"})
        Me.cred_type.Name = "cred_type"
        Me.cred_type.Width = 120
        '
        'cred_default
        '
        Me.cred_default.HeaderText = "IN default"
        Me.cred_default.Name = "cred_default"
        Me.cred_default.Width = 50
        '
        'cred_joint
        '
        Me.cred_joint.HeaderText = "Joint"
        Me.cred_joint.Name = "cred_joint"
        Me.cred_joint.Width = 50
        '
        'cred_joint_with
        '
        Me.cred_joint_with.HeaderText = "Joint With"
        Me.cred_joint_with.Items.AddRange(New Object() {"Partner", "Parent", "Friend", "Sibling", "NA", "      "})
        Me.cred_joint_with.Name = "cred_joint_with"
        '
        'cred_value
        '
        Me.cred_value.HeaderText = "Value"
        Me.cred_value.Name = "cred_value"
        '
        'cred_value_os
        '
        Me.cred_value_os.HeaderText = "Value OS"
        Me.cred_value_os.Name = "cred_value_os"
        '
        'cred_date_added
        '
        Me.cred_date_added.HeaderText = "Date Added"
        Me.cred_date_added.Name = "cred_date_added"
        Me.cred_date_added.ReadOnly = True
        Me.cred_date_added.Width = 75
        '
        'cred_added_by
        '
        Me.cred_added_by.HeaderText = "Added by"
        Me.cred_added_by.Name = "cred_added_by"
        Me.cred_added_by.ReadOnly = True
        Me.cred_added_by.Width = 75
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.query_grid)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(932, 187)
        Me.TabPage5.TabIndex = 5
        Me.TabPage5.Text = "Queries"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'query_grid
        '
        Me.query_grid.AllowUserToOrderColumns = True
        Me.query_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.query_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.query_no, Me.query_text, Me.query_Date_added, Me.query_added_by, Me.query_response, Me.query_comp_cbox, Me.query_completed_date, Me.query_completed_by})
        Me.query_grid.Location = New System.Drawing.Point(-3, 0)
        Me.query_grid.Name = "query_grid"
        Me.query_grid.Size = New System.Drawing.Size(913, 187)
        Me.query_grid.TabIndex = 0
        '
        'query_no
        '
        Me.query_no.HeaderText = "query no"
        Me.query_no.Name = "query_no"
        Me.query_no.ReadOnly = True
        Me.query_no.Width = 40
        '
        'query_text
        '
        Me.query_text.HeaderText = "Text"
        Me.query_text.Name = "query_text"
        Me.query_text.Width = 250
        '
        'query_Date_added
        '
        Me.query_Date_added.HeaderText = "Date Added"
        Me.query_Date_added.Name = "query_Date_added"
        Me.query_Date_added.ReadOnly = True
        Me.query_Date_added.Width = 70
        '
        'query_added_by
        '
        Me.query_added_by.HeaderText = "Added By"
        Me.query_added_by.Name = "query_added_by"
        Me.query_added_by.ReadOnly = True
        Me.query_added_by.Width = 70
        '
        'query_response
        '
        Me.query_response.HeaderText = "Response"
        Me.query_response.Name = "query_response"
        Me.query_response.Width = 200
        '
        'query_comp_cbox
        '
        Me.query_comp_cbox.HeaderText = "Completed"
        Me.query_comp_cbox.Name = "query_comp_cbox"
        Me.query_comp_cbox.Width = 60
        '
        'query_completed_date
        '
        Me.query_completed_date.HeaderText = "Completed Date"
        Me.query_completed_date.Name = "query_completed_date"
        Me.query_completed_date.ReadOnly = True
        Me.query_completed_date.Width = 70
        '
        'query_completed_by
        '
        Me.query_completed_by.HeaderText = "Completed By"
        Me.query_completed_by.Name = "query_completed_by"
        Me.query_completed_by.ReadOnly = True
        Me.query_completed_by.Width = 70
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.lr_grid)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(932, 187)
        Me.TabPage6.TabIndex = 6
        Me.TabPage6.Text = "LR Check"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'lr_grid
        '
        Me.lr_grid.AllowUserToAddRows = False
        Me.lr_grid.AllowUserToDeleteRows = False
        Me.lr_grid.AllowUserToOrderColumns = True
        Me.lr_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.lr_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.lr_title, Me.lr_tenure, Me.lr_prop, Me.lr_charge})
        Me.lr_grid.Location = New System.Drawing.Point(0, 0)
        Me.lr_grid.Name = "lr_grid"
        Me.lr_grid.ReadOnly = True
        Me.lr_grid.Size = New System.Drawing.Size(908, 181)
        Me.lr_grid.TabIndex = 0
        '
        'lr_title
        '
        Me.lr_title.HeaderText = "Title"
        Me.lr_title.Name = "lr_title"
        Me.lr_title.ReadOnly = True
        '
        'lr_tenure
        '
        Me.lr_tenure.HeaderText = "tenure"
        Me.lr_tenure.Name = "lr_tenure"
        Me.lr_tenure.ReadOnly = True
        '
        'lr_prop
        '
        Me.lr_prop.HeaderText = "Proprietor"
        Me.lr_prop.Name = "lr_prop"
        Me.lr_prop.ReadOnly = True
        Me.lr_prop.Width = 300
        '
        'lr_charge
        '
        Me.lr_charge.HeaderText = "Charge"
        Me.lr_charge.Name = "lr_charge"
        Me.lr_charge.ReadOnly = True
        Me.lr_charge.Width = 350
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.search_grid)
        Me.TabPage7.Location = New System.Drawing.Point(4, 22)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage7.Size = New System.Drawing.Size(932, 187)
        Me.TabPage7.TabIndex = 7
        Me.TabPage7.Text = "Search results"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'search_grid
        '
        Me.search_grid.AllowUserToAddRows = False
        Me.search_grid.AllowUserToDeleteRows = False
        Me.search_grid.AllowUserToOrderColumns = True
        Me.search_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.search_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.search_1, Me.search_2, Me.search_3, Me.search_4, Me.search_5})
        Me.search_grid.Location = New System.Drawing.Point(0, 0)
        Me.search_grid.Name = "search_grid"
        Me.search_grid.ReadOnly = True
        Me.search_grid.Size = New System.Drawing.Size(879, 150)
        Me.search_grid.TabIndex = 0
        '
        'search_1
        '
        Me.search_1.HeaderText = "AddressMatch"
        Me.search_1.Name = "search_1"
        Me.search_1.ReadOnly = True
        '
        'search_2
        '
        Me.search_2.HeaderText = "LandRegistry"
        Me.search_2.Name = "search_2"
        Me.search_2.ReadOnly = True
        Me.search_2.Width = 150
        '
        'search_3
        '
        Me.search_3.HeaderText = "Experian"
        Me.search_3.Name = "search_3"
        Me.search_3.ReadOnly = True
        Me.search_3.Width = 250
        '
        'search_4
        '
        Me.search_4.HeaderText = "EstPropertyValue"
        Me.search_4.Name = "search_4"
        Me.search_4.ReadOnly = True
        '
        'search_5
        '
        Me.search_5.HeaderText = "PossibleEquity"
        Me.search_5.Name = "search_5"
        Me.search_5.ReadOnly = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.asset_grid)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(932, 187)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Declared Assets"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'TabPage8
        '
        Me.TabPage8.Controls.Add(Me.bank_details_grid)
        Me.TabPage8.Location = New System.Drawing.Point(4, 22)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage8.Size = New System.Drawing.Size(932, 187)
        Me.TabPage8.TabIndex = 8
        Me.TabPage8.Text = "Bank details from feed"
        Me.TabPage8.UseVisualStyleBackColor = True
        '
        'bank_details_grid
        '
        Me.bank_details_grid.AllowUserToAddRows = False
        Me.bank_details_grid.AllowUserToDeleteRows = False
        Me.bank_details_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.bank_details_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.bank_details})
        Me.bank_details_grid.Location = New System.Drawing.Point(3, 0)
        Me.bank_details_grid.Name = "bank_details_grid"
        Me.bank_details_grid.ReadOnly = True
        Me.bank_details_grid.Size = New System.Drawing.Size(879, 181)
        Me.bank_details_grid.TabIndex = 0
        '
        'bank_details
        '
        Me.bank_details.HeaderText = "bank Details"
        Me.bank_details.Name = "bank_details"
        Me.bank_details.ReadOnly = True
        Me.bank_details.Width = 800
        '
        'TabPage9
        '
        Me.TabPage9.Controls.Add(Me.final_costs_grid)
        Me.TabPage9.Location = New System.Drawing.Point(4, 22)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage9.Size = New System.Drawing.Size(932, 187)
        Me.TabPage9.TabIndex = 9
        Me.TabPage9.Text = "Final Costs"
        Me.TabPage9.UseVisualStyleBackColor = True
        '
        'final_costs_grid
        '
        Me.final_costs_grid.AllowUserToAddRows = False
        Me.final_costs_grid.AllowUserToDeleteRows = False
        Me.final_costs_grid.AllowUserToOrderColumns = True
        Me.final_costs_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.final_costs_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.final_cost, Me.final_vat, Me.LGFS_tot, Me.LGFS_vat, Me.AGFS_tot, Me.AGFS_vat})
        Me.final_costs_grid.Location = New System.Drawing.Point(0, 6)
        Me.final_costs_grid.Name = "final_costs_grid"
        Me.final_costs_grid.ReadOnly = True
        Me.final_costs_grid.Size = New System.Drawing.Size(683, 150)
        Me.final_costs_grid.TabIndex = 0
        '
        'final_cost
        '
        Me.final_cost.HeaderText = "Final Costs"
        Me.final_cost.Name = "final_cost"
        Me.final_cost.ReadOnly = True
        '
        'final_vat
        '
        Me.final_vat.HeaderText = "Final Vat"
        Me.final_vat.Name = "final_vat"
        Me.final_vat.ReadOnly = True
        '
        'LGFS_tot
        '
        Me.LGFS_tot.HeaderText = "LGFS Total"
        Me.LGFS_tot.Name = "LGFS_tot"
        Me.LGFS_tot.ReadOnly = True
        '
        'LGFS_vat
        '
        Me.LGFS_vat.HeaderText = "LGFS Vat"
        Me.LGFS_vat.Name = "LGFS_vat"
        Me.LGFS_vat.ReadOnly = True
        '
        'AGFS_tot
        '
        Me.AGFS_tot.HeaderText = "AGFS Total"
        Me.AGFS_tot.Name = "AGFS_tot"
        Me.AGFS_tot.ReadOnly = True
        '
        'AGFS_vat
        '
        Me.AGFS_vat.HeaderText = "AGFS Vat"
        Me.AGFS_vat.Name = "AGFS_vat"
        Me.AGFS_vat.ReadOnly = True
        '
        'TabPage10
        '
        Me.TabPage10.Controls.Add(Me.crystal_grid)
        Me.TabPage10.Location = New System.Drawing.Point(4, 22)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage10.Size = New System.Drawing.Size(932, 187)
        Me.TabPage10.TabIndex = 10
        Me.TabPage10.Text = "Crystallisation"
        Me.TabPage10.UseVisualStyleBackColor = True
        '
        'crystal_grid
        '
        Me.crystal_grid.AllowUserToAddRows = False
        Me.crystal_grid.AllowUserToDeleteRows = False
        Me.crystal_grid.AllowUserToOrderColumns = True
        Me.crystal_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.crystal_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.so_date, Me.calc_date, Me.crystallisation, Me.sixth_adj})
        Me.crystal_grid.Location = New System.Drawing.Point(-1, 6)
        Me.crystal_grid.Name = "crystal_grid"
        Me.crystal_grid.ReadOnly = True
        Me.crystal_grid.Size = New System.Drawing.Size(593, 150)
        Me.crystal_grid.TabIndex = 0
        '
        'so_date
        '
        Me.so_date.HeaderText = "SO date"
        Me.so_date.Name = "so_date"
        Me.so_date.ReadOnly = True
        '
        'calc_date
        '
        Me.calc_date.HeaderText = "Calc Date"
        Me.calc_date.Name = "calc_date"
        Me.calc_date.ReadOnly = True
        '
        'crystallisation
        '
        Me.crystallisation.HeaderText = "Crystallisation"
        Me.crystallisation.Name = "crystallisation"
        Me.crystallisation.ReadOnly = True
        '
        'sixth_adj
        '
        Me.sixth_adj.HeaderText = "6th Inst Adj"
        Me.sixth_adj.Name = "sixth_adj"
        Me.sixth_adj.ReadOnly = True
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'wip_txt
        '
        Me.wip_txt.AutoSize = True
        Me.wip_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.wip_txt.Location = New System.Drawing.Point(20, 81)
        Me.wip_txt.Name = "wip_txt"
        Me.wip_txt.Size = New System.Drawing.Size(35, 20)
        Me.wip_txt.TabIndex = 42
        Me.wip_txt.Text = "wip"
        '
        'status_txt
        '
        Me.status_txt.AutoSize = True
        Me.status_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.status_txt.Location = New System.Drawing.Point(356, 215)
        Me.status_txt.Name = "status_txt"
        Me.status_txt.Size = New System.Drawing.Size(59, 20)
        Me.status_txt.TabIndex = 43
        Me.status_txt.Text = "status"
        '
        'PostCode
        '
        Me.PostCode.AutoSize = True
        Me.PostCode.Location = New System.Drawing.Point(629, 19)
        Me.PostCode.Name = "PostCode"
        Me.PostCode.Size = New System.Drawing.Size(53, 13)
        Me.PostCode.TabIndex = 44
        Me.PostCode.Text = "PostCode"
        '
        'validatebtn
        '
        Me.validatebtn.Location = New System.Drawing.Point(335, 474)
        Me.validatebtn.Name = "validatebtn"
        Me.validatebtn.Size = New System.Drawing.Size(75, 23)
        Me.validatebtn.TabIndex = 12
        Me.validatebtn.Text = "Validate"
        Me.validatebtn.UseVisualStyleBackColor = True
        '
        'completebtn
        '
        Me.completebtn.Location = New System.Drawing.Point(495, 474)
        Me.completebtn.Name = "completebtn"
        Me.completebtn.Size = New System.Drawing.Size(75, 23)
        Me.completebtn.TabIndex = 13
        Me.completebtn.Text = "Complete"
        Me.completebtn.UseVisualStyleBackColor = True
        '
        'exp_check_cbox
        '
        Me.exp_check_cbox.AutoSize = True
        Me.exp_check_cbox.Checked = True
        Me.exp_check_cbox.CheckState = System.Windows.Forms.CheckState.Checked
        Me.exp_check_cbox.Location = New System.Drawing.Point(26, 397)
        Me.exp_check_cbox.Name = "exp_check_cbox"
        Me.exp_check_cbox.Size = New System.Drawing.Size(101, 17)
        Me.exp_check_cbox.TabIndex = 5
        Me.exp_check_cbox.Text = "Experian Check"
        Me.exp_check_cbox.UseVisualStyleBackColor = True
        '
        'match_cbox
        '
        Me.match_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.match_cbox.FormattingEnabled = True
        Me.match_cbox.Items.AddRange(New Object() {"MATCH", "NO MATCH", "SURNAME MATCH", "NA"})
        Me.match_cbox.Location = New System.Drawing.Point(146, 395)
        Me.match_cbox.Name = "match_cbox"
        Me.match_cbox.Size = New System.Drawing.Size(121, 21)
        Me.match_cbox.TabIndex = 6
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(144, 379)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(89, 13)
        Me.Label11.TabIndex = 55
        Me.Label11.Text = "Match/No Match"
        '
        'undec_bank_cbox
        '
        Me.undec_bank_cbox.AutoSize = True
        Me.undec_bank_cbox.Location = New System.Drawing.Point(307, 399)
        Me.undec_bank_cbox.Name = "undec_bank_cbox"
        Me.undec_bank_cbox.Size = New System.Drawing.Size(151, 17)
        Me.undec_bank_cbox.TabIndex = 7
        Me.undec_bank_cbox.Text = "Undeclared bank Account"
        Me.undec_bank_cbox.UseVisualStyleBackColor = True
        '
        'cred_score_ud
        '
        Me.cred_score_ud.Location = New System.Drawing.Point(479, 399)
        Me.cred_score_ud.Name = "cred_score_ud"
        Me.cred_score_ud.Size = New System.Drawing.Size(70, 20)
        Me.cred_score_ud.TabIndex = 8
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(479, 379)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(63, 13)
        Me.Label12.TabIndex = 58
        Me.Label12.Text = "Credit score"
        '
        'cred_last_year_cbox
        '
        Me.cred_last_year_cbox.AutoSize = True
        Me.cred_last_year_cbox.Location = New System.Drawing.Point(577, 402)
        Me.cred_last_year_cbox.Name = "cred_last_year_cbox"
        Me.cred_last_year_cbox.Size = New System.Drawing.Size(177, 17)
        Me.cred_last_year_cbox.TabIndex = 9
        Me.cred_last_year_cbox.Text = "Credit Agreement in last 12 mths"
        Me.cred_last_year_cbox.UseVisualStyleBackColor = True
        '
        'cons_pos_cbox
        '
        Me.cons_pos_cbox.AutoSize = True
        Me.cons_pos_cbox.Location = New System.Drawing.Point(779, 402)
        Me.cons_pos_cbox.Name = "cons_pos_cbox"
        Me.cons_pos_cbox.Size = New System.Drawing.Size(115, 17)
        Me.cons_pos_cbox.TabIndex = 10
        Me.cons_pos_cbox.Text = "Consistent Position"
        Me.cons_pos_cbox.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(26, 439)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(141, 15)
        Me.Label13.TabIndex = 61
        Me.Label13.Text = "Verified Total Capital"
        '
        'ver_tot_cap_txt
        '
        Me.ver_tot_cap_txt.AutoSize = True
        Me.ver_tot_cap_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ver_tot_cap_txt.Location = New System.Drawing.Point(177, 441)
        Me.ver_tot_cap_txt.Name = "ver_tot_cap_txt"
        Me.ver_tot_cap_txt.Size = New System.Drawing.Size(69, 13)
        Me.ver_tot_cap_txt.TabIndex = 62
        Me.ver_tot_cap_txt.Text = "ver tot cap"
        '
        'ver_tot_equity_txt
        '
        Me.ver_tot_equity_txt.AutoSize = True
        Me.ver_tot_equity_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ver_tot_equity_txt.Location = New System.Drawing.Point(418, 441)
        Me.ver_tot_equity_txt.Name = "ver_tot_equity_txt"
        Me.ver_tot_equity_txt.Size = New System.Drawing.Size(82, 13)
        Me.ver_tot_equity_txt.TabIndex = 64
        Me.ver_tot_equity_txt.Text = "ver tot equity"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(277, 439)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(135, 15)
        Me.Label15.TabIndex = 63
        Me.Label15.Text = "Verified Total Equity"
        '
        'ver_tot_ce_txt
        '
        Me.ver_tot_ce_txt.AutoSize = True
        Me.ver_tot_ce_txt.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ver_tot_ce_txt.Location = New System.Drawing.Point(675, 441)
        Me.ver_tot_ce_txt.Name = "ver_tot_ce_txt"
        Me.ver_tot_ce_txt.Size = New System.Drawing.Size(105, 13)
        Me.ver_tot_ce_txt.TabIndex = 66
        Me.ver_tot_ce_txt.Text = "ver tot c/e  - 30k"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(516, 439)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(156, 15)
        Me.Label16.TabIndex = 65
        Me.Label16.Text = "Verified Total C/E - 30K"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(492, 272)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(140, 13)
        Me.Label19.TabIndex = 72
        Me.Label19.Text = "Total Cap Assets to be used"
        '
        'tot_cap_assets_cbox
        '
        Me.tot_cap_assets_cbox.FormattingEnabled = True
        Me.tot_cap_assets_cbox.Location = New System.Drawing.Point(516, 294)
        Me.tot_cap_assets_cbox.Name = "tot_cap_assets_cbox"
        Me.tot_cap_assets_cbox.Size = New System.Drawing.Size(93, 21)
        Me.tot_cap_assets_cbox.TabIndex = 0
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(20, 272)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(115, 13)
        Me.Label14.TabIndex = 74
        Me.Label14.Text = "Residential Description"
        '
        'residential_desc_txt
        '
        Me.residential_desc_txt.AutoSize = True
        Me.residential_desc_txt.Location = New System.Drawing.Point(44, 294)
        Me.residential_desc_txt.Name = "residential_desc_txt"
        Me.residential_desc_txt.Size = New System.Drawing.Size(80, 13)
        Me.residential_desc_txt.TabIndex = 75
        Me.residential_desc_txt.Text = "residential desc"
        '
        'prop_type_desc_txt
        '
        Me.prop_type_desc_txt.AutoSize = True
        Me.prop_type_desc_txt.Location = New System.Drawing.Point(150, 294)
        Me.prop_type_desc_txt.Name = "prop_type_desc_txt"
        Me.prop_type_desc_txt.Size = New System.Drawing.Size(54, 13)
        Me.prop_type_desc_txt.TabIndex = 77
        Me.prop_type_desc_txt.Text = "prop desc"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(144, 272)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(102, 13)
        Me.Label18.TabIndex = 76
        Me.Label18.Text = "Property Description"
        '
        'appl_pcent_txt
        '
        Me.appl_pcent_txt.AutoSize = True
        Me.appl_pcent_txt.Location = New System.Drawing.Point(293, 294)
        Me.appl_pcent_txt.Name = "appl_pcent_txt"
        Me.appl_pcent_txt.Size = New System.Drawing.Size(57, 13)
        Me.appl_pcent_txt.TabIndex = 79
        Me.appl_pcent_txt.Text = "pcent appl"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(260, 272)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(113, 13)
        Me.Label20.TabIndex = 78
        Me.Label20.Text = "% Owned by Applicant"
        '
        'partner_pcent_txt
        '
        Me.partner_pcent_txt.AutoSize = True
        Me.partner_pcent_txt.Location = New System.Drawing.Point(406, 294)
        Me.partner_pcent_txt.Name = "partner_pcent_txt"
        Me.partner_pcent_txt.Size = New System.Drawing.Size(70, 13)
        Me.partner_pcent_txt.TabIndex = 81
        Me.partner_pcent_txt.Text = "pcent partner"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(383, 272)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(103, 13)
        Me.Label21.TabIndex = 80
        Me.Label21.Text = "% Owned by Partner"
        '
        'tomebtn
        '
        Me.tomebtn.Location = New System.Drawing.Point(12, 215)
        Me.tomebtn.Name = "tomebtn"
        Me.tomebtn.Size = New System.Drawing.Size(92, 23)
        Me.tomebtn.TabIndex = 82
        Me.tomebtn.Text = "Allocate To Me"
        Me.tomebtn.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(639, 272)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(141, 13)
        Me.Label7.TabIndex = 83
        Me.Label7.Text = "Additional Property Declared"
        '
        'add_prop_decl_txt
        '
        Me.add_prop_decl_txt.AutoSize = True
        Me.add_prop_decl_txt.Location = New System.Drawing.Point(657, 302)
        Me.add_prop_decl_txt.Name = "add_prop_decl_txt"
        Me.add_prop_decl_txt.Size = New System.Drawing.Size(72, 13)
        Me.add_prop_decl_txt.TabIndex = 84
        Me.add_prop_decl_txt.Text = "add prop decl"
        '
        'parents_cbox
        '
        Me.parents_cbox.AutoSize = True
        Me.parents_cbox.Location = New System.Drawing.Point(29, 349)
        Me.parents_cbox.Name = "parents_cbox"
        Me.parents_cbox.Size = New System.Drawing.Size(113, 17)
        Me.parents_cbox.TabIndex = 1
        Me.parents_cbox.Text = "Owned by Parents"
        Me.parents_cbox.UseVisualStyleBackColor = True
        '
        'partner_cbox
        '
        Me.partner_cbox.AutoSize = True
        Me.partner_cbox.Location = New System.Drawing.Point(153, 349)
        Me.partner_cbox.Name = "partner_cbox"
        Me.partner_cbox.Size = New System.Drawing.Size(111, 17)
        Me.partner_cbox.TabIndex = 2
        Me.partner_cbox.Text = "Owned by Partner"
        Me.partner_cbox.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(30, 250)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(203, 13)
        Me.Label8.TabIndex = 87
        Me.Label8.Text = "<---------------------DECLARED--------------------->"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(277, 250)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(203, 13)
        Me.Label9.TabIndex = 88
        Me.Label9.Text = "<---------------------DECLARED--------------------->"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(26, 324)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(236, 13)
        Me.Label17.TabIndex = 89
        Me.Label17.Text = "<----------------------------VERIFIED---------------------------->"
        '
        'same_surname_cbox
        '
        Me.same_surname_cbox.AutoSize = True
        Me.same_surname_cbox.Location = New System.Drawing.Point(280, 349)
        Me.same_surname_cbox.Name = "same_surname_cbox"
        Me.same_surname_cbox.Size = New System.Drawing.Size(145, 17)
        Me.same_surname_cbox.TabIndex = 3
        Me.same_surname_cbox.Text = "Owned by same surname"
        Me.same_surname_cbox.UseVisualStyleBackColor = True
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(277, 324)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(200, 13)
        Me.Label22.TabIndex = 91
        Me.Label22.Text = "<-----------------------VERIFIED--------------------->"
        '
        'prop_comments_tbox
        '
        Me.prop_comments_tbox.Location = New System.Drawing.Point(479, 356)
        Me.prop_comments_tbox.Name = "prop_comments_tbox"
        Me.prop_comments_tbox.Size = New System.Drawing.Size(447, 20)
        Me.prop_comments_tbox.TabIndex = 4
        '
        'prop_comments_txt
        '
        Me.prop_comments_txt.AutoSize = True
        Me.prop_comments_txt.Location = New System.Drawing.Point(479, 337)
        Me.prop_comments_txt.Name = "prop_comments_txt"
        Me.prop_comments_txt.Size = New System.Drawing.Size(98, 13)
        Me.prop_comments_txt.TabIndex = 93
        Me.prop_comments_txt.Text = "Property Comments"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(429, 151)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(243, 13)
        Me.Label23.TabIndex = 94
        Me.Label23.Text = "<---------------------------FROM FEED--------------------------->"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(683, 151)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(243, 13)
        Me.Label24.TabIndex = 95
        Me.Label24.Text = "<---------------------------FROM FEED--------------------------->"
        '
        'bank_acct_ud
        '
        Me.bank_acct_ud.Location = New System.Drawing.Point(826, 300)
        Me.bank_acct_ud.Maximum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.bank_acct_ud.Name = "bank_acct_ud"
        Me.bank_acct_ud.Size = New System.Drawing.Size(43, 20)
        Me.bank_acct_ud.TabIndex = 0
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(792, 272)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(106, 13)
        Me.Label25.TabIndex = 99
        Me.Label25.Text = "Bank Accts declared"
        '
        'wait_txt
        '
        Me.wait_txt.AutoSize = True
        Me.wait_txt.Location = New System.Drawing.Point(201, 199)
        Me.wait_txt.Name = "wait_txt"
        Me.wait_txt.Size = New System.Drawing.Size(35, 13)
        Me.wait_txt.TabIndex = 100
        Me.wait_txt.Text = "WAIT"
        '
        'logbtn
        '
        Me.logbtn.Location = New System.Drawing.Point(660, 474)
        Me.logbtn.Name = "logbtn"
        Me.logbtn.Size = New System.Drawing.Size(75, 23)
        Me.logbtn.TabIndex = 14
        Me.logbtn.Text = "Display Log"
        Me.logbtn.UseVisualStyleBackColor = True
        '
        'googlebtn
        '
        Me.googlebtn.Location = New System.Drawing.Point(44, 474)
        Me.googlebtn.Name = "googlebtn"
        Me.googlebtn.Size = New System.Drawing.Size(75, 23)
        Me.googlebtn.TabIndex = 11
        Me.googlebtn.Text = "Google Map"
        Me.googlebtn.UseVisualStyleBackColor = True
        '
        'LSCPropertyTableBindingSource
        '
        Me.LSCPropertyTableBindingSource.DataMember = "LSC Property table"
        Me.LSCPropertyTableBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LSC_Property_tableTableAdapter
        '
        Me.LSC_Property_tableTableAdapter.ClearBeforeFill = True
        '
        'LSC_WIP_tableTableAdapter
        '
        Me.LSC_WIP_tableTableAdapter.ClearBeforeFill = True
        '
        'LSC_Status_tableTableAdapter
        '
        Me.LSC_Status_tableTableAdapter.ClearBeforeFill = True
        '
        'LSC_bank_tableTableAdapter
        '
        Me.LSC_bank_tableTableAdapter.ClearBeforeFill = True
        '
        'LSC_Credit_tableTableAdapter
        '
        Me.LSC_Credit_tableTableAdapter.ClearBeforeFill = True
        '
        'LSC_Debtor_tableTableAdapter
        '
        Me.LSC_Debtor_tableTableAdapter.ClearBeforeFill = True
        '
        'LSC_Asset_tableTableAdapter
        '
        Me.LSC_Asset_tableTableAdapter.ClearBeforeFill = True
        '
        'comp_date_txt
        '
        Me.comp_date_txt.AutoSize = True
        Me.comp_date_txt.Location = New System.Drawing.Point(629, 225)
        Me.comp_date_txt.Name = "comp_date_txt"
        Me.comp_date_txt.Size = New System.Drawing.Size(60, 13)
        Me.comp_date_txt.TabIndex = 101
        Me.comp_date_txt.Text = "comp_date"
        '
        'address_tbox
        '
        Me.address_tbox.Location = New System.Drawing.Point(717, 42)
        Me.address_tbox.Multiline = True
        Me.address_tbox.Name = "address_tbox"
        Me.address_tbox.ReadOnly = True
        Me.address_tbox.Size = New System.Drawing.Size(195, 90)
        Me.address_tbox.TabIndex = 102
        '
        'debtor_tbox
        '
        Me.debtor_tbox.Location = New System.Drawing.Point(94, 42)
        Me.debtor_tbox.Name = "debtor_tbox"
        Me.debtor_tbox.ReadOnly = True
        Me.debtor_tbox.Size = New System.Drawing.Size(68, 20)
        Me.debtor_tbox.TabIndex = 103
        '
        'pcode_tbox
        '
        Me.pcode_tbox.Location = New System.Drawing.Point(632, 45)
        Me.pcode_tbox.Name = "pcode_tbox"
        Me.pcode_tbox.ReadOnly = True
        Me.pcode_tbox.Size = New System.Drawing.Size(67, 20)
        Me.pcode_tbox.TabIndex = 104
        '
        'name_tbox
        '
        Me.name_tbox.Location = New System.Drawing.Point(516, 45)
        Me.name_tbox.Name = "name_tbox"
        Me.name_tbox.ReadOnly = True
        Me.name_tbox.Size = New System.Drawing.Size(100, 20)
        Me.name_tbox.TabIndex = 105
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.Location = New System.Drawing.Point(744, 220)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(100, 15)
        Me.Label26.TabIndex = 106
        Me.Label26.Text = "Verified Total C/E"
        '
        'ver_tot_c_and_e_from_feed
        '
        Me.ver_tot_c_and_e_from_feed.AutoSize = True
        Me.ver_tot_c_and_e_from_feed.Location = New System.Drawing.Point(766, 235)
        Me.ver_tot_c_and_e_from_feed.Name = "ver_tot_c_and_e_from_feed"
        Me.ver_tot_c_and_e_from_feed.Size = New System.Drawing.Size(76, 13)
        Me.ver_tot_c_and_e_from_feed.TabIndex = 107
        Me.ver_tot_c_and_e_from_feed.Text = "ver tot c and e"
        '
        'TabPage11
        '
        Me.TabPage11.Controls.Add(Me.partner_grid)
        Me.TabPage11.Location = New System.Drawing.Point(4, 22)
        Me.TabPage11.Name = "TabPage11"
        Me.TabPage11.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage11.Size = New System.Drawing.Size(932, 187)
        Me.TabPage11.TabIndex = 11
        Me.TabPage11.Text = "Partner"
        Me.TabPage11.UseVisualStyleBackColor = True
        '
        'partner_grid
        '
        Me.partner_grid.AllowUserToAddRows = False
        Me.partner_grid.AllowUserToDeleteRows = False
        Me.partner_grid.AllowUserToOrderColumns = True
        Me.partner_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.partner_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.first_name, Me.last_name, Me.dob, Me.nino, Me.partner_emp_status})
        Me.partner_grid.Location = New System.Drawing.Point(3, 6)
        Me.partner_grid.Name = "partner_grid"
        Me.partner_grid.ReadOnly = True
        Me.partner_grid.Size = New System.Drawing.Size(801, 150)
        Me.partner_grid.TabIndex = 0
        '
        'first_name
        '
        Me.first_name.HeaderText = "First Name"
        Me.first_name.Name = "first_name"
        Me.first_name.ReadOnly = True
        '
        'last_name
        '
        Me.last_name.HeaderText = "Last name"
        Me.last_name.Name = "last_name"
        Me.last_name.ReadOnly = True
        Me.last_name.Width = 200
        '
        'dob
        '
        Me.dob.HeaderText = "DOB"
        Me.dob.Name = "dob"
        Me.dob.ReadOnly = True
        '
        'nino
        '
        Me.nino.HeaderText = "NINO"
        Me.nino.Name = "nino"
        Me.nino.ReadOnly = True
        '
        'partner_emp_status
        '
        Me.partner_emp_status.HeaderText = "Emp Status"
        Me.partner_emp_status.Name = "partner_emp_status"
        Me.partner_emp_status.ReadOnly = True
        Me.partner_emp_status.Width = 200
        '
        'TabPage12
        '
        Me.TabPage12.Controls.Add(Me.inc_evidence_grid)
        Me.TabPage12.Location = New System.Drawing.Point(4, 22)
        Me.TabPage12.Name = "TabPage12"
        Me.TabPage12.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage12.Size = New System.Drawing.Size(932, 187)
        Me.TabPage12.TabIndex = 12
        Me.TabPage12.Text = "Inc Evidence"
        Me.TabPage12.UseVisualStyleBackColor = True
        '
        'inc_evidence_grid
        '
        Me.inc_evidence_grid.AllowUserToAddRows = False
        Me.inc_evidence_grid.AllowUserToDeleteRows = False
        Me.inc_evidence_grid.AllowUserToOrderColumns = True
        Me.inc_evidence_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.inc_evidence_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.inc_evidence})
        Me.inc_evidence_grid.Location = New System.Drawing.Point(-1, 6)
        Me.inc_evidence_grid.Name = "inc_evidence_grid"
        Me.inc_evidence_grid.ReadOnly = True
        Me.inc_evidence_grid.Size = New System.Drawing.Size(573, 150)
        Me.inc_evidence_grid.TabIndex = 0
        '
        'inc_evidence
        '
        Me.inc_evidence.HeaderText = "Income Evidence"
        Me.inc_evidence.Name = "inc_evidence"
        Me.inc_evidence.ReadOnly = True
        Me.inc_evidence.Width = 450
        '
        'casefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(950, 728)
        Me.Controls.Add(Me.ver_tot_c_and_e_from_feed)
        Me.Controls.Add(Me.Label26)
        Me.Controls.Add(Me.name_tbox)
        Me.Controls.Add(Me.pcode_tbox)
        Me.Controls.Add(Me.debtor_tbox)
        Me.Controls.Add(Me.address_tbox)
        Me.Controls.Add(Me.comp_date_txt)
        Me.Controls.Add(Me.googlebtn)
        Me.Controls.Add(Me.logbtn)
        Me.Controls.Add(Me.wait_txt)
        Me.Controls.Add(Me.Label25)
        Me.Controls.Add(Me.bank_acct_ud)
        Me.Controls.Add(Me.Label24)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.prop_comments_txt)
        Me.Controls.Add(Me.prop_comments_tbox)
        Me.Controls.Add(Me.Label22)
        Me.Controls.Add(Me.same_surname_cbox)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.partner_cbox)
        Me.Controls.Add(Me.parents_cbox)
        Me.Controls.Add(Me.add_prop_decl_txt)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.tomebtn)
        Me.Controls.Add(Me.partner_pcent_txt)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.appl_pcent_txt)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.prop_type_desc_txt)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.residential_desc_txt)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.tot_cap_assets_cbox)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.ver_tot_ce_txt)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.ver_tot_equity_txt)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.ver_tot_cap_txt)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.cons_pos_cbox)
        Me.Controls.Add(Me.cred_last_year_cbox)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.cred_score_ud)
        Me.Controls.Add(Me.undec_bank_cbox)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.match_cbox)
        Me.Controls.Add(Me.exp_check_cbox)
        Me.Controls.Add(Me.completebtn)
        Me.Controls.Add(Me.validatebtn)
        Me.Controls.Add(Me.PostCode)
        Me.Controls.Add(Me.status_txt)
        Me.Controls.Add(Me.wip_txt)
        Me.Controls.Add(Me.final_costs_tab)
        Me.Controls.Add(Me.appl_equity_amt_txt)
        Me.Controls.Add(Me.appl_equity)
        Me.Controls.Add(Me.partner_equity_amt_txt)
        Me.Controls.Add(Me.partner_equity)
        Me.Controls.Add(Me.status)
        Me.Controls.Add(Me.onestep_status_txt)
        Me.Controls.Add(Me.nextbtn)
        Me.Controls.Add(Me.mode_txt)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.equity_amt_ver_txt)
        Me.Controls.Add(Me.equity_amt_txt)
        Me.Controls.Add(Me.equity_amt_ver)
        Me.Controls.Add(Me.inc_paid)
        Me.Controls.Add(Me.inc_paid_txt)
        Me.Controls.Add(Me.five_sixths)
        Me.Controls.Add(Me.five_sixths_txt)
        Me.Controls.Add(Me.inc_contrib)
        Me.Controls.Add(Me.inc_contrib_txt)
        Me.Controls.Add(Me.emp_status_txt)
        Me.Controls.Add(Me.emp_status)
        Me.Controls.Add(Me.stage_txt)
        Me.Controls.Add(Me.stage)
        Me.Controls.Add(Me.outcome_txt)
        Me.Controls.Add(Me.outcome)
        Me.Controls.Add(Me.casetype_txt)
        Me.Controls.Add(Me.Casetype)
        Me.Controls.Add(Me.dob_txt)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.loaded_txt)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.maatid_txt)
        Me.Controls.Add(Me.Label1)
        Me.Name = "casefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Onestep Case details"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.asset_grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.asset_ep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.final_costs_tab.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.prop_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.bank_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.cred_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage5.ResumeLayout(False)
        CType(Me.query_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage6.ResumeLayout(False)
        CType(Me.lr_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage7.ResumeLayout(False)
        CType(Me.search_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage8.ResumeLayout(False)
        CType(Me.bank_details_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage9.ResumeLayout(False)
        CType(Me.final_costs_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage10.ResumeLayout(False)
        CType(Me.crystal_grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.cred_score_ud, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.bank_acct_ud, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LSCPropertyTableBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage11.ResumeLayout(False)
        CType(Me.partner_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage12.ResumeLayout(False)
        CType(Me.inc_evidence_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents maatid_txt As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents loaded_txt As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dob_txt As System.Windows.Forms.Label
    Friend WithEvents Casetype As System.Windows.Forms.Label
    Friend WithEvents casetype_txt As System.Windows.Forms.Label
    Friend WithEvents outcome As System.Windows.Forms.Label
    Friend WithEvents outcome_txt As System.Windows.Forms.Label
    Friend WithEvents stage As System.Windows.Forms.Label
    Friend WithEvents stage_txt As System.Windows.Forms.Label
    Friend WithEvents emp_status As System.Windows.Forms.Label
    Friend WithEvents emp_status_txt As System.Windows.Forms.Label
    Friend WithEvents inc_contrib_txt As System.Windows.Forms.Label
    Friend WithEvents inc_contrib As System.Windows.Forms.Label
    Friend WithEvents five_sixths_txt As System.Windows.Forms.Label
    Friend WithEvents five_sixths As System.Windows.Forms.Label
    Friend WithEvents inc_paid_txt As System.Windows.Forms.Label
    Friend WithEvents inc_paid As System.Windows.Forms.Label
    Friend WithEvents equity_amt_ver As System.Windows.Forms.Label
    Friend WithEvents equity_amt_txt As System.Windows.Forms.Label
    Friend WithEvents equity_amt_ver_txt As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents asset_grid As System.Windows.Forms.DataGridView
    Friend WithEvents asset_ep As System.Windows.Forms.ErrorProvider
    Friend WithEvents mode_txt As System.Windows.Forms.Label
    Friend WithEvents nextbtn As System.Windows.Forms.Button
    Friend WithEvents status As System.Windows.Forms.Label
    Friend WithEvents onestep_status_txt As System.Windows.Forms.Label
    Friend WithEvents partner_equity As System.Windows.Forms.Label
    Friend WithEvents partner_equity_amt_txt As System.Windows.Forms.Label
    Friend WithEvents appl_equity_amt_txt As System.Windows.Forms.Label
    Friend WithEvents appl_equity As System.Windows.Forms.Label
    Friend WithEvents final_costs_tab As System.Windows.Forms.TabControl
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents FeesSQLDataSet As CapitalandEquity.FeesSQLDataSet
    Friend WithEvents LSCPropertyTableBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LSC_Property_tableTableAdapter As CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Property_tableTableAdapter
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents prop_grid As System.Windows.Forms.DataGridView
    Friend WithEvents LSC_WIP_tableTableAdapter As CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_WIP_tableTableAdapter
    Friend WithEvents wip_txt As System.Windows.Forms.Label
    Friend WithEvents status_txt As System.Windows.Forms.Label
    Friend WithEvents LSC_Status_tableTableAdapter As CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Status_tableTableAdapter
    Friend WithEvents PostCode As System.Windows.Forms.Label
    Friend WithEvents validatebtn As System.Windows.Forms.Button
    Friend WithEvents completebtn As System.Windows.Forms.Button
    Friend WithEvents bank_grid As System.Windows.Forms.DataGridView
    Friend WithEvents bank_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bank_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bank_amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bank_added_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bank_added_by As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LSC_bank_tableTableAdapter As CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_bank_tableTableAdapter
    Friend WithEvents LSC_Credit_tableTableAdapter As CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Credit_tableTableAdapter
    Friend WithEvents cred_grid As System.Windows.Forms.DataGridView
    Friend WithEvents exp_check_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents match_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents undec_bank_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cred_score_ud As System.Windows.Forms.NumericUpDown
    Friend WithEvents cred_last_year_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents cons_pos_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents ver_tot_cap_txt As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents ver_tot_equity_txt As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents ver_tot_ce_txt As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents query_grid As System.Windows.Forms.DataGridView
    Friend WithEvents query_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_text As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_Date_added As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_added_by As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_response As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_comp_cbox As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents query_completed_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_completed_by As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tot_cap_assets_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents residential_desc_txt As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents prop_type_desc_txt As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents appl_pcent_txt As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents partner_pcent_txt As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents tomebtn As System.Windows.Forms.Button
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents lr_grid As System.Windows.Forms.DataGridView
    Friend WithEvents lr_title As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lr_tenure As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lr_prop As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lr_charge As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents search_grid As System.Windows.Forms.DataGridView
    Friend WithEvents search_1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents search_2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents search_3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents search_4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents search_5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents add_prop_decl_txt As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents partner_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents parents_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents prop_comments_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents same_surname_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents prop_comments_txt As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents TabPage8 As System.Windows.Forms.TabPage
    Friend WithEvents bank_details_grid As System.Windows.Forms.DataGridView
    Friend WithEvents bank_details As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents bank_acct_ud As System.Windows.Forms.NumericUpDown
    Friend WithEvents LSC_Debtor_tableTableAdapter As CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Debtor_tableTableAdapter
    Friend WithEvents wait_txt As System.Windows.Forms.Label
    Friend WithEvents LSC_Asset_tableTableAdapter As CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Asset_tableTableAdapter
    Friend WithEvents logbtn As System.Windows.Forms.Button
    Friend WithEvents prop_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents main_residence As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents address As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents prop_type As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents lr_check As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents likely_value As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents added_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents added_by As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents googlebtn As System.Windows.Forms.Button
    Friend WithEvents comp_date_txt As System.Windows.Forms.Label
    Friend WithEvents cred_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cred_type As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents cred_default As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents cred_joint As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents cred_joint_with As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents cred_value As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cred_value_os As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cred_date_added As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cred_added_by As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents address_tbox As System.Windows.Forms.TextBox
    Friend WithEvents debtor_tbox As System.Windows.Forms.TextBox
    Friend WithEvents pcode_tbox As System.Windows.Forms.TextBox
    Friend WithEvents name_tbox As System.Windows.Forms.TextBox
    Friend WithEvents TabPage9 As System.Windows.Forms.TabPage
    Friend WithEvents final_costs_grid As System.Windows.Forms.DataGridView
    Friend WithEvents final_cost As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents final_vat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LGFS_tot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LGFS_vat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AGFS_tot As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AGFS_vat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TabPage10 As System.Windows.Forms.TabPage
    Friend WithEvents crystal_grid As System.Windows.Forms.DataGridView
    Friend WithEvents so_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents calc_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents crystallisation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sixth_adj As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents source As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents asset_type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents asset_amt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents asset_amt_ver As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents ver_tot_c_and_e_from_feed As System.Windows.Forms.Label
    Friend WithEvents TabPage11 As System.Windows.Forms.TabPage
    Friend WithEvents partner_grid As System.Windows.Forms.DataGridView
    Friend WithEvents first_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents last_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dob As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nino As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents partner_emp_status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TabPage12 As System.Windows.Forms.TabPage
    Friend WithEvents inc_evidence_grid As System.Windows.Forms.DataGridView
    Friend WithEvents inc_evidence As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

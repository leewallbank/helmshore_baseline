<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class allqueryfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.queryall_grid = New System.Windows.Forms.DataGridView
        Me.query_debtorid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_text = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_added_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_added_by = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.query_response = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.queryall_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'queryall_grid
        '
        Me.queryall_grid.AllowUserToAddRows = False
        Me.queryall_grid.AllowUserToDeleteRows = False
        Me.queryall_grid.AllowUserToOrderColumns = True
        Me.queryall_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.queryall_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.query_debtorid, Me.query_no, Me.query_name, Me.query_text, Me.query_added_date, Me.query_added_by, Me.query_response})
        Me.queryall_grid.Location = New System.Drawing.Point(-1, 0)
        Me.queryall_grid.Name = "queryall_grid"
        Me.queryall_grid.ReadOnly = True
        Me.queryall_grid.Size = New System.Drawing.Size(951, 150)
        Me.queryall_grid.TabIndex = 0
        '
        'query_debtorid
        '
        Me.query_debtorid.HeaderText = "DebtorID"
        Me.query_debtorid.Name = "query_debtorid"
        Me.query_debtorid.ReadOnly = True
        Me.query_debtorid.Width = 60
        '
        'query_no
        '
        Me.query_no.HeaderText = "Query No"
        Me.query_no.Name = "query_no"
        Me.query_no.ReadOnly = True
        Me.query_no.Width = 40
        '
        'query_name
        '
        Me.query_name.HeaderText = "Name"
        Me.query_name.Name = "query_name"
        Me.query_name.ReadOnly = True
        Me.query_name.Width = 80
        '
        'query_text
        '
        Me.query_text.FillWeight = 200.0!
        Me.query_text.HeaderText = "text"
        Me.query_text.Name = "query_text"
        Me.query_text.ReadOnly = True
        Me.query_text.Width = 300
        '
        'query_added_date
        '
        Me.query_added_date.HeaderText = "Added Date"
        Me.query_added_date.Name = "query_added_date"
        Me.query_added_date.ReadOnly = True
        Me.query_added_date.Width = 70
        '
        'query_added_by
        '
        Me.query_added_by.HeaderText = "Added By"
        Me.query_added_by.Name = "query_added_by"
        Me.query_added_by.ReadOnly = True
        Me.query_added_by.Width = 70
        '
        'query_response
        '
        Me.query_response.FillWeight = 200.0!
        Me.query_response.HeaderText = "Response"
        Me.query_response.Name = "query_response"
        Me.query_response.ReadOnly = True
        Me.query_response.Width = 280
        '
        'allqueryfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(951, 266)
        Me.Controls.Add(Me.queryall_grid)
        Me.Name = "allqueryfrm"
        Me.Text = "All Open Queries (Double-click to display case)"
        CType(Me.queryall_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents queryall_grid As System.Windows.Forms.DataGridView
    Friend WithEvents query_debtorid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_text As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_added_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_added_by As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents query_response As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

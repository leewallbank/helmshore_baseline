<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class logfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.log_grid = New System.Windows.Forms.DataGridView
        Me.log_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.log_user = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.log_type = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.log_text = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.FeesSQLDataSet = New CapitalandEquity.FeesSQLDataSet
        Me.LSC_Log_tableTableAdapter = New CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Log_tableTableAdapter
        CType(Me.log_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'log_grid
        '
        Me.log_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.log_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.log_date, Me.log_user, Me.log_type, Me.log_text})
        Me.log_grid.Location = New System.Drawing.Point(-3, 12)
        Me.log_grid.Name = "log_grid"
        Me.log_grid.Size = New System.Drawing.Size(900, 422)
        Me.log_grid.TabIndex = 0
        '
        'log_date
        '
        Me.log_date.HeaderText = "Date"
        Me.log_date.Name = "log_date"
        Me.log_date.ReadOnly = True
        Me.log_date.Width = 120
        '
        'log_user
        '
        Me.log_user.HeaderText = "Who"
        Me.log_user.Name = "log_user"
        Me.log_user.ReadOnly = True
        '
        'log_type
        '
        Me.log_type.HeaderText = "Type"
        Me.log_type.Name = "log_type"
        Me.log_type.ReadOnly = True
        Me.log_type.Width = 120
        '
        'log_text
        '
        Me.log_text.HeaderText = "text"
        Me.log_text.Name = "log_text"
        Me.log_text.ReadOnly = True
        Me.log_text.Width = 500
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LSC_Log_tableTableAdapter
        '
        Me.LSC_Log_tableTableAdapter.ClearBeforeFill = True
        '
        'logfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(909, 446)
        Me.Controls.Add(Me.log_grid)
        Me.Name = "logfrm"
        Me.Text = "logfrm"
        CType(Me.log_grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents log_grid As System.Windows.Forms.DataGridView
    Friend WithEvents FeesSQLDataSet As CapitalandEquity.FeesSQLDataSet
    Friend WithEvents LSC_Log_tableTableAdapter As CapitalandEquity.FeesSQLDataSetTableAdapters.LSC_Log_tableTableAdapter
    Friend WithEvents log_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents log_user As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents log_type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents log_text As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class disp_prev_repsfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.prev_reps_grid = New System.Windows.Forms.DataGridView
        Me.rep_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.rep_cases = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.prev_reps_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'prev_reps_grid
        '
        Me.prev_reps_grid.AllowUserToAddRows = False
        Me.prev_reps_grid.AllowUserToDeleteRows = False
        Me.prev_reps_grid.AllowUserToOrderColumns = True
        Me.prev_reps_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.prev_reps_grid.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.rep_date, Me.rep_cases})
        Me.prev_reps_grid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.prev_reps_grid.Location = New System.Drawing.Point(0, 0)
        Me.prev_reps_grid.Name = "prev_reps_grid"
        Me.prev_reps_grid.ReadOnly = True
        Me.prev_reps_grid.Size = New System.Drawing.Size(280, 266)
        Me.prev_reps_grid.TabIndex = 0
        '
        'rep_date
        '
        Me.rep_date.HeaderText = "Report date"
        Me.rep_date.Name = "rep_date"
        Me.rep_date.ReadOnly = True
        '
        'rep_cases
        '
        Me.rep_cases.HeaderText = "No of Cases"
        Me.rep_cases.Name = "rep_cases"
        Me.rep_cases.ReadOnly = True
        '
        'disp_prev_repsfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(280, 266)
        Me.Controls.Add(Me.prev_reps_grid)
        Me.Name = "disp_prev_repsfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Double-click to display cases"
        CType(Me.prev_reps_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents prev_reps_grid As System.Windows.Forms.DataGridView
    Friend WithEvents rep_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rep_cases As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

Public Class reportfrm
    Dim file1, file2, record As String
    Dim idx As Integer
    Private Sub reportfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        date_gbox.Visible = False
        Dim start_date, end_date As Date
        start_date = DateAdd(DateInterval.Day, -Weekday(Now) + 1, Now)
        end_date = Now
        start_date_picker.Value = start_date
        end_date_picker.Value = end_date
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click

        If open_queries_rb.Checked Then
            file1 = "DebtorID,MAATID,Query No,Query text,Added By,Added date" & vbNewLine
            mainmenu.LSC_Query_tableTableAdapter.FillBy2(mainmenu.FeesSQLDataSet.LSC_Query_table)
            Dim no_of_queries As Integer = mainmenu.FeesSQLDataSet.LSC_Query_table.Rows.Count

            For idx = 0 To no_of_queries - 1
                Dim query_debtorid As Integer = mainmenu.FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(0)
                Dim query_no As Integer = mainmenu.FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(1)
                Dim query_text As String = mainmenu.FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(2)
                query_text = Replace(query_text, ",", " ")
                Dim query_date As Date = mainmenu.FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(3)
                param2 = "select client_ref from Debtor where _rowid = " & query_debtorid
                Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    MsgBox("Unable to get MAATID for case = " & query_debtorid)
                End If
                Dim maat_id As String = debtor_dataset.Tables(0).Rows(0).Item(0)
                Dim query_user_no As Integer = mainmenu.FeesSQLDataSet.LSC_Query_table.Rows(idx).Item(4)
                logon.LSC_User_tableTableAdapter.FillBy(logon.FeesSQLDataSet.LSC_User_table, query_user_no)
                Dim query_user As String = logon.FeesSQLDataSet.LSC_User_table.Rows(0).Item(1)
                file1 = file1 & query_debtorid & "," & maat_id & "," & query_no & "," & query_text & _
                "," & query_user & "," & Format(query_date, "dd/MM/yyyy") & vbNewLine
            Next

            If Len(file1) = 0 Then
                MessageBox.Show("There are no open queries")
            Else
                With SaveFileDialog1
                    .Title = "Save to excel"
                    .Filter = "excel|*.xls"
                    .DefaultExt = ".xls"
                    .OverwritePrompt = True
                    .FileName = "queries.xls"
                End With
                If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                    My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file1, False)
                End If
                MsgBox("Open queries report generated")
                log_text = "Open Queries report run"
                logon.LSC_Log_tableTableAdapter.InsertQuery(0, "Run Reports", log_text, Now, log_code)
            End If
            Me.Close()
            Exit Sub
        End If
        If lsc_rept_rb.Checked Then
            If MsgBox("This extracts all PENDING cases and updates the status to COMPLETED", MsgBoxStyle.OkCancel, "Weekly Report") = MsgBoxResult.Cancel Then
                Me.Close()
                Exit Sub
            End If
            generate_lsc_report()
        ElseIf prev_rep_rb.Checked Then
            disp_prev_repsfrm.ShowDialog()
        End If
        Me.Close()
    End Sub
    Private Sub generate_lsc_report()
        '2 files one with total cap and equity >0 and one <=0
        'can add when fields added to debtor table
        file1 = "Date_Received,DebtorID,MAAT_ID,Case_Type,Crown_Court_Outcome,Income_Contribution_Liability," & _
        "Total_Income_Contribs_Paid,Total_of_Capital_Amount_Declared,Total Equity_Declared," & _
        "Total_Capital_and_Equity_Combined,Income_contribution_Cap,Contributor_based_on_declaration," & _
        "First_Name,Last_name,DOB,Appl_Employment_Status,Clients home/work number,Clients mobile number," & _
        "Home_Address,Postcode,Residential_Status,property_Type,Declared_Property_value," & _
        "Declared_Outstanding_Mortgage,Declared_Charges,Percentage_Owned_by_Applicant,Percentage_Owned_by_Partner," & _
        "Total_Ownership_for_Calculation,Additional_Property_Declared,Additional_Property_Address," & _
        "Equity_Declared_from_Additional_Property,Land_Registry_Check,Applicant_Property_Ownership_Evidenced," & _
        "Property_Owned_by_Parents,Property_Owned_by_Partner_Only,Property_Owned_by_Someone_with_same_surname," & _
        "Risk,Property Comments,Changes_to_MAAT,Property_Likely_Valuation,Additional_Property_Likely_Valuation,Experian_check," & _
        "Match_Type,Mortgage_Value,Decaration_to_stop,Equity_only,Bank_Accounts_Declared,Volume_of_bank_accounts_in_MAAT," & _
        "Volume_of_bank_accounts_checked,Accounts_in_credit_or_overdrawn,Undeclared_bank_accounts,Credit_Score,Credit_agreement_in_last_12_months," & _
        "Types_of_credit_agreements,Agreements_in_default,Joint_credit_agreement,Joint_with,Credit_position_consistent," & _
        "Partner_Declared,Partner_Name,Partner_NI_No,Partner_DOB,Checks_Indicate_Partner,Property_Owned_by_Partner," & _
        "Capital_Declared,Capital_Evidence_Relevant,Capital_Evidence_OS,Verified_Total_Capital," & _
        "Verified_Total_Equity,Total_Verified_less_30K" & vbNewLine
        file2 = file1
        'get all cases pending (status=2)
        casefrm.LSC_Debtor_tableTableAdapter.FillBy1(casefrm.FeesSQLDataSet.LSC_Debtor_table)
        Dim no_of_debtors As Integer = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows.Count
        For idx = 0 To no_of_debtors - 1
            Dim debtorid As Integer = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(0)
            param2 = "select client_ref, _createdDate, debt_amount, debt_costs, offenceValue, name_fore, name_sur," & _
            " dateOfBirth, add_phone, add_fax, empPhone, address, add_postcode from Debtor where _rowid = " & debtorid
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to read case number " & debtorid)
                Exit Sub
            End If
            Dim maat_id As String = debtor_dataset.Tables(0).Rows(0).Item(0)
            Dim date_received As Date = debtor_dataset.Tables(0).Rows(0).Item(1)
            Dim outcome As String = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(16)
            Dim case_type As String = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(17)
            Dim inc_contrib_liability As Decimal = debtor_dataset.Tables(0).Rows(0).Item(2) + _
            debtor_dataset.Tables(0).Rows(0).Item(3)
            'amount paid
            param2 = "select Sum(split_debt + split_costs) from Payment " & _
            " where status = 'R' and debtorID = " & debtorid
            Dim payment_dataset As DataSet = get_dataset("onestep", param2)
            Dim inc_paid As Decimal
            Try
                inc_paid = payment_dataset.Tables(0).Rows(0).Item(0)
            Catch ex As Exception
                inc_paid = 0
            End Try
            Dim tot_cap_amt_decl As Decimal = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(18)
            Dim tot_equity_decl As Decimal = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(25)
            Dim tot_ce_decl As Decimal = tot_cap_amt_decl + tot_equity_decl
            Dim ver_tot_cap_and_equity As Decimal = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(19)
            Dim inc_contrib_cap As Decimal = debtor_dataset.Tables(0).Rows(0).Item(4)
            Dim contributor As String
            If ver_tot_cap_and_equity > 0 Then
                If tot_cap_amt_decl = 0 Then
                    contributor = "Y-Income only"
                ElseIf tot_equity_decl = 0 Then
                    contributor = "Y-capital only"
                Else
                    contributor = "Y-income and capital"
                End If
            Else
                If tot_ce_decl = 0 Then
                    contributor = "N-None"
                Else
                    contributor = "N-Under �30K"
                End If
            End If
            Dim first_name As String = ""
            Try
                first_name = debtor_dataset.Tables(0).Rows(0).Item(5)
                first_name = Replace(first_name, ",", " ")
            Catch ex As Exception

            End Try
            Dim last_name As String = debtor_dataset.Tables(0).Rows(0).Item(6)
            last_name = Replace(last_name, ",", " ")
            Dim dob As Date = Nothing
            Dim dob_str As String = ""
            Try
                dob = debtor_dataset.Tables(0).Rows(0).Item(7)
            Catch ex As Exception

            End Try
            If dob <> Nothing Then
                dob_str = Format(dob, "dd/MM/yyyy")
            End If
            Dim emp_status As String = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(20)
            Dim add_phone As String = ""
            Try
                add_phone = Trim(debtor_dataset.Tables(0).Rows(0).Item(8))
            Catch ex As Exception

            End Try
            Dim add_fax As String = ""
            Try
                add_fax = Trim(debtor_dataset.Tables(0).Rows(0).Item(9))
            Catch ex As Exception

            End Try
            Dim emp_phone As String = ""
            Try
                emp_phone = Trim(debtor_dataset.Tables(0).Rows(0).Item(10))
            Catch ex As Exception

            End Try
            If add_phone = "" Then
                add_phone = emp_phone
            End If
            If add_fax = "" And Microsoft.VisualBasic.Left(add_phone, 2) = "07" Then
                add_fax = add_phone
                add_phone = ""
            End If
            Dim home_address As String = Trim(debtor_dataset.Tables(0).Rows(0).Item(11))
            home_address = remove_chars(home_address)
            Dim pcode As String = ""
            Try
                pcode = Trim(debtor_dataset.Tables(0).Rows(0).Item(12))
            Catch ex As Exception

            End Try
            Dim residential_status As String = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(21)
            Dim property_type As String = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(22)

            Dim decl_property_value As String = Nothing
            Dim decl_os_mortgage As String = Nothing
            Dim decl_charges As String = Nothing
            Dim percent_owned_by_applicant As Decimal
            Dim percent_owned_by_applicant_str As String = Nothing
            Try
                percent_owned_by_applicant = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(23)
                percent_owned_by_applicant_str = Format(percent_owned_by_applicant, "#.00")
            Catch ex As Exception

            End Try

            Dim percent_owned_by_partner As Decimal
            Dim percent_owned_by_partner_str As String = Nothing
            Try
                percent_owned_by_partner = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(24)
                percent_owned_by_partner_str = Format(percent_owned_by_partner, "#.00")
            Catch ex As Exception

            End Try

            Dim tot_ownership As String = Nothing
            Dim add_prop_decl As Boolean = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(26)
            Dim add_prop_decl_str As String
            If add_prop_decl Then
                add_prop_decl_str = "Yes"
            Else
                add_prop_decl_str = "No"
            End If
            Dim add_prop_address As String = Nothing
            Dim add_prop_equity As String = Nothing
            Dim lr_check As String
            If casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(27) Then
                lr_check = "Y"
            Else
                lr_check = "N"
            End If

            Dim appl_prop_evidenced As String = Nothing
            Dim owned_by_parents As String
            If casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(28) Then
                owned_by_parents = "Y"
            Else
                owned_by_parents = "N"
            End If
            Dim owned_by_partner As String
            If casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(29) Then
                owned_by_partner = "Y"
            Else
                owned_by_partner = "N"
            End If
            Dim owned_same_surname As String
            If casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(30) Then
                owned_same_surname = "Y"
            Else
                owned_same_surname = "N"
            End If
            Dim risk As String = Nothing
            Dim prop_comments As String
            Try
                prop_comments = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(31)
            Catch ex As Exception
                prop_comments = ""
            End Try

            Dim maat_changes As String = Nothing
            Dim prop_likely_value As Decimal = Nothing
            Dim prop_likely_value_str As String = Nothing
            casefrm.LSC_Property_tableTableAdapter.FillBy(casefrm.FeesSQLDataSet.LSC_Property_table, debtorid)
            Dim no_of_props As Integer = casefrm.FeesSQLDataSet.LSC_Property_table.Rows.Count
            Dim idx2 As Integer
            For idx2 = 0 To no_of_props - 1
                Dim likely_value As Decimal = Nothing
                Try
                    likely_value = casefrm.FeesSQLDataSet.LSC_Property_table.Rows(idx2).Item(4)
                    prop_likely_value += likely_value
                    prop_likely_value_str = Format(prop_likely_value, "�#.00")
                Catch ex As Exception

                End Try
            Next
            'if property type not set - use property type from main residence
            If property_type.Length = 0 And no_of_props > 0 Then
                casefrm.LSC_Property_tableTableAdapter.FillBy2(casefrm.FeesSQLDataSet.LSC_Property_table, debtorid, True)
                Try
                    property_type = casefrm.FeesSQLDataSet.LSC_Property_table.Rows(0).Item(7)
                Catch ex As Exception
                    property_type = ""
                End Try
            End If
            Dim add_prop_likely_value_str As String = Nothing
            Dim exp_check As String
            If casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(1) Then
                exp_check = "Y"
            Else
                exp_check = "N"
            End If
            Dim match_type As String = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(2)
            Dim mortgage_value_str As String = Nothing
            Dim mortgage_value As Decimal = Nothing
            Dim mortgage_value_os As Decimal = Nothing
            Dim type_cred_agreements As String = Nothing
            Dim agreements_in_default As String = Nothing
            Dim joint_cred_agreement As String = Nothing
            Dim joint_with As String = Nothing
            casefrm.LSC_Credit_tableTableAdapter.FillBy1(casefrm.FeesSQLDataSet.LSC_Credit_table, debtorid)
            Dim no_of_creds As Integer = casefrm.FeesSQLDataSet.LSC_Credit_table.Rows.Count
            For idx2 = 0 To no_of_creds - 1
                type_cred_agreements = type_cred_agreements & casefrm.FeesSQLDataSet.LSC_Credit_table.Rows(idx2).Item(2) & ";"
                If casefrm.FeesSQLDataSet.LSC_Credit_table.Rows(idx2).Item(3) Then
                    agreements_in_default = agreements_in_default & "Y;"
                Else
                    agreements_in_default = agreements_in_default & "N;"
                End If
                If casefrm.FeesSQLDataSet.LSC_Credit_table.Rows(idx2).Item(4) Then
                    joint_cred_agreement = joint_cred_agreement & "Y;"
                Else
                    joint_cred_agreement = joint_cred_agreement & "N;"
                End If
                joint_with = joint_with & casefrm.FeesSQLDataSet.LSC_Credit_table.Rows(idx2).Item(5) & ";"
                If casefrm.FeesSQLDataSet.LSC_Credit_table.Rows(idx2).Item(2) <> "Mortgage" Then
                    Continue For
                End If
                Dim mortgage As Decimal
                Try
                    mortgage = casefrm.FeesSQLDataSet.LSC_Credit_table.Rows(idx2).Item(6)
                    mortgage_value += mortgage

                Catch ex As Exception

                End Try
                Try
                    mortgage_value_os += casefrm.FeesSQLDataSet.LSC_Credit_table.Rows(idx2).Item(7)
                Catch ex As Exception

                End Try

            Next
            If mortgage_value <> Nothing Then
                mortgage_value_str = Format(mortgage_value, "�#.00")
            End If

            Dim ver_tot_equity As Decimal = Nothing
            Dim ver_tot_equity_str As String = Nothing
            If prop_likely_value <> Nothing Then
                ver_tot_equity = prop_likely_value
            End If
            If mortgage_value_os <> Nothing Then
                ver_tot_equity -= mortgage_value_os
            End If

            If ver_tot_equity <> Nothing Then
                ver_tot_equity_str = Format(ver_tot_equity, "�#.00")
            End If
            Dim decision_to_stop As String = Nothing
            Dim equity_only As String = Nothing
            Dim bank_accts_declared As String
            Dim vol_of_bank_accts_decl As Integer = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(32)
            If vol_of_bank_accts_decl = 0 Then
                bank_accts_declared = "N"
            Else
                bank_accts_declared = "Y"
            End If
            Dim vol_of_bank_accts_checked As Integer = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(5)
            Dim bank_accts_in_credit As String = Nothing
            casefrm.LSC_bank_tableTableAdapter.FillBy(casefrm.FeesSQLDataSet.LSC_bank_table, debtorid)
            Dim no_of_banks As Integer = casefrm.FeesSQLDataSet.LSC_bank_table.Rows.Count
            Dim ver_tot_cap_str As String = Nothing
            Dim ver_tot_cap As Decimal = Nothing
            Dim tot_bank_acct_value As Decimal = Nothing
            For idx2 = 0 To no_of_banks - 1
                Dim bank_val As Decimal = casefrm.FeesSQLDataSet.LSC_bank_table.Rows(idx2).Item(3)
                tot_bank_acct_value += bank_val
                If bank_val > 0 Then
                    ver_tot_cap += bank_val
                End If
            Next
            If tot_bank_acct_value < 0 Then
                bank_accts_in_credit = "OD"
            Else
                bank_accts_in_credit = "C"
            End If
            If ver_tot_cap <> Nothing Then
                ver_tot_cap_str = Format(ver_tot_cap, "�#.00")
            End If
            Dim undecl_bank_accts As String
            If casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(8) Then
                undecl_bank_accts = "Y"
            Else
                undecl_bank_accts = "N"
            End If
            Dim credit_score As String = Nothing
            Try
                credit_score = casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(6)
            Catch ex As Exception

            End Try
            Dim cred_agreement_last_year As String
            If casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(7) Then
                cred_agreement_last_year = "Y"
            Else
                cred_agreement_last_year = "N"
            End If
            Dim cred_pos_consistent As String
            If casefrm.FeesSQLDataSet.LSC_Debtor_table.Rows(idx).Item(9) Then
                cred_pos_consistent = "Y"
            Else
                cred_pos_consistent = "N"
            End If
            Dim partner_declared As String = Nothing
            Dim partner_name As String = Nothing
            Dim partner_ni_no As String = Nothing
            Dim partner_dob As String = Nothing
            Dim checks_indicate_partner As String = Nothing
            Dim prop_owned_by_partner_only As String = Nothing
            Dim decl_assets As String = Nothing
            casefrm.LSC_Asset_tableTableAdapter.FillBy(casefrm.FeesSQLDataSet.LSC_Asset_table, debtorid, False)
            Dim no_of_assets As Integer = casefrm.FeesSQLDataSet.LSC_Asset_table.Rows.Count
            For idx2 = 0 To no_of_assets - 1
                Dim asset_val As Decimal = casefrm.FeesSQLDataSet.LSC_Asset_table.Rows(idx2).Item(4)
                If asset_val = 0 Then
                    Continue For
                End If
                decl_assets = decl_assets & casefrm.FeesSQLDataSet.LSC_Asset_table.Rows(idx2).Item(3) & ":"
                decl_assets = decl_assets & Format(asset_val, "�#.00") & ":"
                If casefrm.FeesSQLDataSet.LSC_Asset_table.Rows(idx2).Item(5) Then
                    decl_assets = decl_assets & "Verified;"
                Else
                    decl_assets = decl_assets & "Not verified;"
                End If

            Next
            Dim cap_evidence_os As String = Nothing
            Dim cap_evidence_relevant As String = Nothing




            record = Format(date_received, "dd/MM/yyyy") & "," & debtorid & "," & maat_id & "," & _
            case_type & "," & outcome & "," & Format(inc_contrib_liability, "�#.00") & "," & _
            Format(inc_paid, "�#.00") & "," & Format(tot_cap_amt_decl, "�#.00") & "," & _
            Format(tot_equity_decl, "�#.00") & "," & Format(tot_ce_decl, "�#.00") & "," & _
            Format(inc_contrib_cap, "�#.00") & "," & contributor & "," & first_name & "," & _
            last_name & "," & dob_str & "," & emp_status & "," & add_phone & "," & add_fax & "," & home_address & "," & pcode & _
            "," & residential_status & "," & property_type & "," & decl_property_value & "," & _
            decl_os_mortgage & "," & decl_charges & "," & percent_owned_by_applicant_str & "," & _
            percent_owned_by_partner_str & "," & tot_ownership & "," & add_prop_decl_str & "," & _
            add_prop_address & "," & add_prop_equity & "," & lr_check & "," & appl_prop_evidenced & "," & _
            owned_by_parents & "," & owned_by_partner & "," & owned_same_surname & "," & _
            risk & "," & prop_comments & "," & maat_changes & "," & prop_likely_value_str & "," & _
            add_prop_likely_value_str & "," & exp_check & "," & match_type & "," & mortgage_value_str & _
            "," & decision_to_stop & "," & equity_only & "," & bank_accts_declared & "," & _
            vol_of_bank_accts_decl & "," & vol_of_bank_accts_checked & "," & bank_accts_in_credit & "," & _
            undecl_bank_accts & "," & credit_score & "," & cred_agreement_last_year & _
            "," & type_cred_agreements & "," & agreements_in_default & "," & joint_cred_agreement & "," & _
            joint_with & "," & cred_pos_consistent & "," & partner_declared & "," & partner_name & "," & _
            partner_ni_no & "," & partner_dob & "," & checks_indicate_partner & "," & _
            prop_owned_by_partner_only & "," & decl_assets & "," & cap_evidence_relevant & "," & _
            cap_evidence_os & "," & ver_tot_cap_str & "," & _
            ver_tot_equity_str & "," & Format(ver_tot_cap_and_equity, "�#.00")
            Dim status As Integer
            If ver_tot_cap_and_equity > 0 Then
                file2 = file2 & record & vbNewLine
                status = 4
            Else
                file1 = file1 & record & vbNewLine
                status = 3
            End If
            'update status and report date
            Try
                casefrm.LSC_Debtor_tableTableAdapter.UpdateQuery20(status, Format(Now, "dd/MM/yyyy"), debtorid)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        Next
        If Len(file1) = 0 Then
            MessageBox.Show("There are no cases to report on")
        Else
            Dim filename As String = "LSC_LT30K_report_" & Format(Now, "dd_MM_yyyy") & ".xls"
            With SaveFileDialog1
                .Title = "Save to excel"
                .Filter = "excel|*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = filename
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file1, False)
                For idx = SaveFileDialog1.FileName.Length To 1 Step -1
                    If Mid(SaveFileDialog1.FileName, idx, 1) = "\" Then
                        SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, idx)
                        SaveFileDialog1.FileName = SaveFileDialog1.FileName & "LSC_GT30K_report_" & Format(Now, "dd_MM_yyyy") & ".xls"
                        Exit For
                    End If
                Next
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file2, False)
            End If
            MsgBox("Weekly report generated")
            log_text = "Weekly report run"
            logon.LSC_Log_tableTableAdapter.InsertQuery(0, "Run Reports", log_text, Now, log_code)
        End If
    End Sub
    Private Function remove_chars(ByVal txt As String) As String
        Dim idx As Integer
        Dim new_txt As String = ""
        For idx = 1 To txt.Length
            Dim test_char As String = Mid(txt, idx, 1)
            If test_char <> Chr(10) And test_char <> Chr(13) Then
                new_txt = new_txt & test_char
            Else
                new_txt = Trim(new_txt) & " "
            End If
        Next
        Return (Trim(new_txt))
    End Function

    Private Sub prev_rep_rb_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles prev_rep_rb.CheckedChanged
        If prev_rep_rb.Checked Then
            date_gbox.Visible = True
        Else
            date_gbox.Visible = False
        End If
    End Sub
End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class newpassfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.pass1txtbox = New System.Windows.Forms.TextBox
        Me.pass2txtbox = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.exitbtn = New System.Windows.Forms.Button
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.savebtn = New System.Windows.Forms.Button
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pass1txtbox
        '
        Me.pass1txtbox.Location = New System.Drawing.Point(89, 65)
        Me.pass1txtbox.MaxLength = 20
        Me.pass1txtbox.Name = "pass1txtbox"
        Me.pass1txtbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.pass1txtbox.Size = New System.Drawing.Size(100, 20)
        Me.pass1txtbox.TabIndex = 0
        '
        'pass2txtbox
        '
        Me.pass2txtbox.Location = New System.Drawing.Point(89, 157)
        Me.pass2txtbox.MaxLength = 10
        Me.pass2txtbox.Name = "pass2txtbox"
        Me.pass2txtbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.pass2txtbox.Size = New System.Drawing.Size(100, 20)
        Me.pass2txtbox.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(83, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(106, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Enter New Password"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(93, 131)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(96, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Re-enter password"
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(257, 222)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "Quit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'savebtn
        '
        Me.savebtn.Location = New System.Drawing.Point(29, 222)
        Me.savebtn.Name = "savebtn"
        Me.savebtn.Size = New System.Drawing.Size(75, 23)
        Me.savebtn.TabIndex = 3
        Me.savebtn.Text = "Save"
        Me.savebtn.UseVisualStyleBackColor = True
        '
        'newpassfrm
        '
        Me.AcceptButton = Me.savebtn
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(344, 266)
        Me.Controls.Add(Me.savebtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.pass2txtbox)
        Me.Controls.Add(Me.pass1txtbox)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "newpassfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Enter New Password"
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents pass1txtbox As System.Windows.Forms.TextBox
    Friend WithEvents pass2txtbox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents savebtn As System.Windows.Forms.Button
End Class

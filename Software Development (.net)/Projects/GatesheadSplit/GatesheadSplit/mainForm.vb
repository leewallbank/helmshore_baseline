Public Class mainfrm

    Private Sub loadbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles splitbtn.Click
        Dim outfile1 As String = ""
        Dim outfile2 As String = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "XLS files|*.xls"
            .FileName = ""
            .CheckFileExists = True
        End With
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MessageBox.Show("No file Selected")
            Exit Sub
        End If
        load_vals(OpenFileDialog1.FileName)
        If finalrow = 0 Then
            Exit Sub
        End If
        'split into 2 files
        Try
            Dim rowidx, colidx As Integer
            For rowidx = 1 To finalrow
                ProgressBar1.Value = rowidx / finalrow * 100
                For colidx = 1 To finalcol
                    If rowidx = 1 Then
                        'write out headers
                        outfile1 = outfile1 & vals(rowidx, colidx) & vbTab
                        outfile2 = outfile2 & vals(rowidx, colidx) & vbTab
                        If colidx = finalcol Then
                            outfile1 = outfile1 & vbNewLine
                            outfile2 = outfile2 & vbNewLine
                        End If
                    Else
                        If vals(rowidx, 1) = " " Then
                            rowidx = finalrow
                            Exit For
                        End If
                        Dim lo_date As Date
                        If colidx = 15 Then
                            lo_date = vals(rowidx, 15)
                            vals(rowidx, 15) = Format(lo_date, "dd/MM/yyyy")
                        End If
                        
                        If vals(rowidx, 16) < 180 Then
                            outfile1 = outfile1 & vals(rowidx, colidx) & vbTab
                            If colidx = finalcol Then
                                outfile1 = outfile1 & vbNewLine
                            End If
                        Else
                            outfile2 = outfile2 & vals(rowidx, colidx) & vbTab
                            If colidx = finalcol Then
                                outfile2 = outfile2 & vbNewLine
                            End If
                        End If
                    End If
                Next
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Exit Sub
        End Try
        Dim idx As Integer
        Dim filename_prefix As String = ""
        For idx = Len(OpenFileDialog1.FileName) To 1 Step -1
            If Mid(OpenFileDialog1.FileName, idx, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(OpenFileDialog1.FileName, idx - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocessLT180.txt", outfile1, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocessGE180.txt", outfile2, False)
        MessageBox.Show("Files created successfully")
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub


    
End Class

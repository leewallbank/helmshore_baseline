Public Class maintainfrm

    Private Sub maintainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'Split_namesDataSet.split_names' table. You can move, or remove it, as needed.
        Me.Split_namesTableAdapter.Fill(Me.Split_namesDataSet.split_names)
        orig_no_rows = DataGridView1.RowCount
    End Sub
    Private Sub DataGridView1_CellValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        DataGridView1.Rows(e.RowIndex).ErrorText = String.Empty
    End Sub

    Private Sub DataGridView1_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs)

    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)

    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs)
        saved_name_id = DataGridView1.Rows(e.RowIndex).Cells(0).Value
    End Sub

    Private Sub DataGridView1_RowsAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsAddedEventArgs)

    End Sub

    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs)
        Try
            Me.Split_namesTableAdapter.DeleteQuery(saved_name_id)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub add_names()
        Dim idx As Integer
        Dim split_name As String

        For idx = orig_no_rows To DataGridView1.RowCount - 1
            split_name = DataGridView1.Rows(idx - 1).Cells(1).Value
            Try
                Me.Split_namesTableAdapter.InsertQuery(split_name)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        add_names()
        Me.Close()
    End Sub
End Class

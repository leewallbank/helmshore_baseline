Public Class mainform
    Dim outline As String = ""
    Dim not_on_onestep_file As String

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Excel files | *.xls"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                load_vals(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then

        Else
            MsgBox("File not opened")
            Exit Sub
        End If
        'do manipulation here

        Dim idx As Integer
        ProgressBar1.Value = 5
        For idx = 2 To finalrow
            ProgressBar1.Value = (idx / finalrow) * 100
            Application.DoEvents()
            Dim maatid As String = vals(idx, 1)
            Dim in_prison As String = vals(idx, 2)
            Dim param2 = "select _rowid, clientschemeID from Debtor where client_ref = '" & maatid & "'"
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                not_on_onestep_file = not_on_onestep_file & maatid & vbNewLine
                Continue For
            End If
            Dim debtor_rows As Integer = no_of_rows - 1
            Dim debtor_idx As Integer
            Dim debtor_found As Boolean = False
            Dim debtor As Integer
            For debtor_idx = 0 To debtor_rows
                Dim csid As Integer = debtor_dataset.Tables(0).Rows(debtor_idx).Item(1)
                param2 = "select _rowid from ClientScheme where _rowid = " & csid & _
                " and clientID = 909"
                Dim csid_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 1 Then
                    debtor_found = True
                    debtor = debtor_dataset.Tables(0).Rows(debtor_idx).Item(0)
                    Exit For
                End If
            Next
            If debtor_found = False Then
                not_on_onestep_file = not_on_onestep_file & maatid & vbNewLine
                Continue For
            End If
            param2 = "select text from Note where debtorID = " & debtor & _
             " order by _rowid desc"
            Dim note_dataset As DataSet = get_dataset("onestep", param2)
            Dim idx2 As Integer
            Dim in_prison_found As Boolean = False
            For idx2 = 0 To no_of_rows - 1
                Dim note_text As String = LCase(note_dataset.Tables(0).Rows(idx2).Item(0))
                Dim start_idx As Integer = InStr(note_text, "inprison:")
                If start_idx = 0 Then
                    Continue For
                End If

                Dim idx3 As Integer
                For idx3 = start_idx + 9 To note_text.Length
                    If Mid(note_text, idx3, 1) = ";" Then
                        Exit For
                    End If
                Next
                Dim current_in_prison As String = LCase(Mid(note_text, start_idx + 9, idx3 - start_idx - 9))
                If Microsoft.VisualBasic.Left(LCase(in_prison), 1) <> _
                Microsoft.VisualBasic.Left(LCase(current_in_prison), 1) Then
                    outfile = outfile & debtor & "|InPrison:" & in_prison & ";"
                    outfile = outfile & "from spreadsheet on " & Format(Now, "dd/MM/yyyy") & vbNewLine
                End If
                in_prison_found = True
                Exit For
            Next
            If in_prison_found = False Then
                outfile = outfile & debtor & "|InPrison:" & in_prison & ";"
                outfile = outfile & "from spreadsheet on " & Format(Now, "dd/MM/yyyy") & vbNewLine
            End If
            'Exit For
           

        Next
        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_note.txt", outfile, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_not_on_onestep.txt", not_on_onestep_file, False)
        MsgBox("reports written")
        Me.Close()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        
    End Sub

   

End Class

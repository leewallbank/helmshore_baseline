Public Class mainform



    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline, caption As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim propaddr As String = ""
        Dim curraddr As String = ""
        Dim bill_no As String = ""
        Dim name As String = ""
        Dim name2 As String = ""
        Dim tel1 As String = ""
        Dim tel2 As String = ""
        Dim lonum As String = ""
        Dim lostring As String = ""
        Dim idx, idx2, idx3 As Integer
        Dim lines As Integer = 0
        Dim debt_amt, war_amt As Decimal
        Dim lodate, fromdate, todate As Date
        Dim clref As String = ""
        Dim addrline As String = ""
        Dim amtstring As String = ""
        Dim comments As String = Nothing
        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "Client ref|Name|Name2|DebtAddress|tel1|Tel2|Current Address|LO Number|LO Date|Warrant Amt|From date|To date|Debt Amount|Comments" & vbNewLine
        outfile = outline

        'look for Account Number
        Dim total_owing_found As Boolean
        For idx = 0 To lines - 1
            caption = Mid(line(idx), 7, 14)
            If caption = "ccount Number:" Then
                'add a space to each line
                line(idx) = " " & line(idx)
                caption = Mid(line(idx), 7, 14)
            End If
            If caption = "Account Number" Then
                total_owing_found = False
                clref = Trim(Mid(line(idx), 42, 12))
                bill_no = Mid(line(idx), 51, 2)
                If Microsoft.VisualBasic.Right(bill_no, 1) = "/" Then
                    bill_no = "0" & Microsoft.VisualBasic.Left(bill_no, 1)
                End If
                comments = "Bill No: " & bill_no & "; "
                'get details for this account number
                For idx2 = idx + 1 To lines - 1
                    caption = Mid(line(idx2), 7, 14)
                    Select Case caption
                        Case Is = "Lead Name:    "
                            name = Mid(line(idx2), 42, 35)
                        Case Is = "Other(s):     "
                            name2 = Trim(Mid(line(idx2), 42, 35))
                        Case Is = "Property Addre"
                            propaddr = Mid(line(idx2), 42, 35)
                            addrline = ""
                            If Len(propaddr) > 3 Then
                                For idx3 = idx2 + 1 To idx2 + 7
                                    addrline = Trim(Mid(line(idx3), 42, 35))
                                    If Len(addrline) > 3 _
                                    And Mid(line(idx3), 7, 2) = "  " Then
                                        propaddr = propaddr & "," & addrline
                                    End If
                                Next
                            End If
                        Case Is = "Home Telephone"
                            tel1 = Mid(line(idx2), 42, 25)
                        Case Is = "Mobile Telepho"
                            tel2 = Mid(line(idx2), 42, 25)
                        Case Is = "Forwarding Add"
                            curraddr = Mid(line(idx2), 42, 35)
                            addrline = ""
                            If Len(curraddr) > 4 Then
                                For idx3 = idx2 + 1 To idx2 + 7
                                    addrline = Mid(line(idx3), 42, 35)
                                    If Len(addrline) > 3 _
                                    And Mid(line(idx3), 7, 2) = "  " Then
                                        curraddr = curraddr & "," & addrline
                                    End If
                                Next
                            End If
                        Case Is = "Summons/Liabil"
                            lonum = Mid(line(idx2), 42, 25)
                        Case Is = "Hearing Date: "
                            lostring = Mid(line(idx2), 42, 25)
                            If Not IsDate(lostring) Then
                                errorfile = errorfile & "Line  " & idx2 & " - LO Date not valid - " _
                                                             & lostring & vbNewLine
                            Else
                                lodate = lostring
                            End If
                        Case Is = "Amount Granted"
                            amtstring = Mid(line(idx2), 43, 10)
                            If Not IsNumeric(amtstring) Then
                                errorfile = errorfile & "Line  " & idx2 & " - Warrant amount not numeric - " _
                                         & amtstring & vbNewLine
                            Else
                                war_amt = amtstring
                            End If
                        Case Is = "Charge period("
                            For idx3 = idx2 + 2 To idx2 + 7
                                Dim tempdate As Date
                                Dim chargeline As String = ""
                                If Mid(line(idx3), 7, 2) <> "  " And _
                                    Mid(line(idx3), 7, 2) <> "" Then
                                    chargeline = Trim(Mid(line(idx3), 7, 35))
                                    comments = comments & "Charge period: " & chargeline & "; "
                                    amtstring = Mid(line(idx3), 47, 7)
                                    comments = comments & "Amt: " & amtstring & ";"
                                    lostring = Microsoft.VisualBasic.Left(chargeline, 10)
                                    If Not IsDate(lostring) Then
                                        errorfile = errorfile & "Line  " & idx3 & " - From Date not valid - " _
                                                                       & lostring & vbNewLine
                                    Else
                                        tempdate = lostring
                                        If fromdate = Nothing Then
                                            fromdate = tempdate
                                        Else
                                            If tempdate < fromdate Then
                                                fromdate = tempdate
                                            End If
                                        End If
                                    End If

                                    lostring = Mid(chargeline, 15, 10)
                                    If Not IsDate(lostring) Then
                                        errorfile = errorfile & "Line  " & idx3 & " - To Date not valid - " _
                                                                       & lostring & vbNewLine
                                    Else
                                        tempdate = lostring
                                        If todate = Nothing Then
                                            todate = tempdate
                                        Else
                                            If tempdate > todate Then
                                                todate = tempdate
                                            End If
                                        End If
                                    End If
                                End If
                            Next
                        Case Is = "Total Amount O"
                            total_owing_found = True
                            amtstring = Mid(line(idx2), 45, 10)
                            If Not IsNumeric(amtstring) Then
                                errorfile = errorfile & "Line  " & idx2 & " - Debt amount not numeric - " _
                                         & amtstring & vbNewLine
                            Else
                                debt_amt = amtstring
                            End If
                    End Select
                    If caption = "Account Number" Or idx2 = lines - 1 Then
                        idx = idx2 - 1
                        If idx2 = lines - 1 Then
                            idx = lines + 1
                        End If
                        Exit For
                    Else
                        'look for comments
                        If total_owing_found And caption <> "Total Amount O" Then
                            Dim extra_comments As String = Trim(Mid(line(idx2), 7, 90))
                            If extra_comments.Length > 3 Then
                                comments = comments & extra_comments & ";"
                            End If
                        End If
                    End If
                Next
            End If
            'validate case details
            If clref = Nothing Or Microsoft.VisualBasic.Left(clref, 3) = "   " Then
                If idx2 > 0 Then
                    errorfile = errorfile & "Line  " & idx2 & " - No client reference" & vbNewLine
                End If
                Continue For
            End If

            'save case in outline
            outfile = outfile & clref & "|" & name & "|" & name2 & "|" & propaddr & "|" _
            & tel1 & "|" & tel2 & "|" & curraddr & "|" & lonum & "|" & lodate & "|" & war_amt & _
            "|" & fromdate & "|" & todate & "|" & debt_amt & "|" & comments & vbNewLine
            name = ""
            name2 = ""
            propaddr = ""
            tel1 = ""
            tel2 = ""
            curraddr = ""
            bill_no = ""
            lonum = ""
            lodate = Nothing
            debt_amt = Nothing
            war_amt = Nothing
            comments = Nothing
            fromdate = Nothing
            todate = Nothing
        Next
        
        viewbtn.Enabled = True
        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class manfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.exitbtn = New System.Windows.Forms.Button
        Me.getbtn = New System.Windows.Forms.Button
        Me.sort_codelbl = New System.Windows.Forms.Label
        Me.acc_no_lbl = New System.Windows.Forms.Label
        Me.acc_name_lbl = New System.Windows.Forms.Label
        Me.acc_no_tbox = New System.Windows.Forms.TextBox
        Me.sort_code_tbox = New System.Windows.Forms.TextBox
        Me.acc_name_tbox = New System.Windows.Forms.TextBox
        Me.FeesSQLDataSet = New LCS_Bank_details.FeesSQLDataSet
        Me.LCS_Bank_detailsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LCS_Bank_detailsTableAdapter = New LCS_Bank_details.FeesSQLDataSetTableAdapters.LCS_Bank_detailsTableAdapter
        Me.maatlbl = New System.Windows.Forms.Label
        Me.LCS_logTableAdapter = New LCS_Bank_details.FeesSQLDataSetTableAdapters.LCS_logTableAdapter
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LCS_Bank_detailsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(205, 280)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'getbtn
        '
        Me.getbtn.Location = New System.Drawing.Point(74, 30)
        Me.getbtn.Name = "getbtn"
        Me.getbtn.Size = New System.Drawing.Size(110, 23)
        Me.getbtn.TabIndex = 4
        Me.getbtn.Text = "Get bank details"
        Me.getbtn.UseVisualStyleBackColor = True
        '
        'sort_codelbl
        '
        Me.sort_codelbl.AutoSize = True
        Me.sort_codelbl.Location = New System.Drawing.Point(100, 106)
        Me.sort_codelbl.Name = "sort_codelbl"
        Me.sort_codelbl.Size = New System.Drawing.Size(54, 13)
        Me.sort_codelbl.TabIndex = 5
        Me.sort_codelbl.Text = "Sort Code"
        '
        'acc_no_lbl
        '
        Me.acc_no_lbl.AutoSize = True
        Me.acc_no_lbl.Location = New System.Drawing.Point(100, 156)
        Me.acc_no_lbl.Name = "acc_no_lbl"
        Me.acc_no_lbl.Size = New System.Drawing.Size(64, 13)
        Me.acc_no_lbl.TabIndex = 6
        Me.acc_no_lbl.Text = "Account No"
        '
        'acc_name_lbl
        '
        Me.acc_name_lbl.AutoSize = True
        Me.acc_name_lbl.Location = New System.Drawing.Point(100, 206)
        Me.acc_name_lbl.Name = "acc_name_lbl"
        Me.acc_name_lbl.Size = New System.Drawing.Size(78, 13)
        Me.acc_name_lbl.TabIndex = 7
        Me.acc_name_lbl.Text = "Account Name"
        '
        'acc_no_tbox
        '
        Me.acc_no_tbox.Enabled = False
        Me.acc_no_tbox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.acc_no_tbox.Location = New System.Drawing.Point(74, 172)
        Me.acc_no_tbox.Name = "acc_no_tbox"
        Me.acc_no_tbox.Size = New System.Drawing.Size(100, 20)
        Me.acc_no_tbox.TabIndex = 8
        '
        'sort_code_tbox
        '
        Me.sort_code_tbox.Enabled = False
        Me.sort_code_tbox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sort_code_tbox.Location = New System.Drawing.Point(74, 122)
        Me.sort_code_tbox.Name = "sort_code_tbox"
        Me.sort_code_tbox.Size = New System.Drawing.Size(100, 20)
        Me.sort_code_tbox.TabIndex = 9
        '
        'acc_name_tbox
        '
        Me.acc_name_tbox.Enabled = False
        Me.acc_name_tbox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.acc_name_tbox.Location = New System.Drawing.Point(22, 222)
        Me.acc_name_tbox.Name = "acc_name_tbox"
        Me.acc_name_tbox.Size = New System.Drawing.Size(247, 20)
        Me.acc_name_tbox.TabIndex = 10
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LCS_Bank_detailsBindingSource
        '
        Me.LCS_Bank_detailsBindingSource.DataMember = "LCS_Bank_details"
        Me.LCS_Bank_detailsBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'LCS_Bank_detailsTableAdapter
        '
        Me.LCS_Bank_detailsTableAdapter.ClearBeforeFill = True
        '
        'maatlbl
        '
        Me.maatlbl.AutoSize = True
        Me.maatlbl.Location = New System.Drawing.Point(100, 76)
        Me.maatlbl.Name = "maatlbl"
        Me.maatlbl.Size = New System.Drawing.Size(45, 13)
        Me.maatlbl.TabIndex = 11
        Me.maatlbl.Text = "Maat ID"
        '
        'LCS_logTableAdapter
        '
        Me.LCS_logTableAdapter.ClearBeforeFill = True
        '
        'manfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(332, 327)
        Me.Controls.Add(Me.maatlbl)
        Me.Controls.Add(Me.acc_name_tbox)
        Me.Controls.Add(Me.sort_code_tbox)
        Me.Controls.Add(Me.acc_no_tbox)
        Me.Controls.Add(Me.acc_name_lbl)
        Me.Controls.Add(Me.acc_no_lbl)
        Me.Controls.Add(Me.sort_codelbl)
        Me.Controls.Add(Me.getbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "manfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "View LCS Bank details"
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LCS_Bank_detailsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents getbtn As System.Windows.Forms.Button
    Friend WithEvents FeesSQLDataSet As LCS_Bank_details.FeesSQLDataSet
    Friend WithEvents LCS_Bank_detailsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LCS_Bank_detailsTableAdapter As LCS_Bank_details.FeesSQLDataSetTableAdapters.LCS_Bank_detailsTableAdapter
    Friend WithEvents sort_codelbl As System.Windows.Forms.Label
    Friend WithEvents acc_no_lbl As System.Windows.Forms.Label
    Friend WithEvents acc_name_lbl As System.Windows.Forms.Label
    Friend WithEvents acc_no_tbox As System.Windows.Forms.TextBox
    Friend WithEvents sort_code_tbox As System.Windows.Forms.TextBox
    Friend WithEvents acc_name_tbox As System.Windows.Forms.TextBox
    Friend WithEvents maatlbl As System.Windows.Forms.Label
    Friend WithEvents LCS_logTableAdapter As LCS_Bank_details.FeesSQLDataSetTableAdapters.LCS_logTableAdapter

End Class

Public Class manfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub getbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles getbtn.Click
        Dim maat_id As Integer
        Try
            maat_id = InputBox("Enter Maat ID", "MAAT ID selection")
        Catch ex As Exception
            MsgBox("Invalid Maat ID")
            Exit Sub
        End Try
        If Not IsNumeric(maat_id) Then
            MsgBox("Invalid Maat ID")
            Exit Sub
        End If
        Me.LCS_Bank_detailsTableAdapter.FillBy(Me.FeesSQLDataSet.LCS_Bank_details, maat_id)
        maatlbl.Text = "Maat ID = " & maat_id
        If FeesSQLDataSet.LCS_Bank_details.Rows.Count = 0 Then
            MsgBox("Bank details not found - if case entered before 23.08.2010 ask IT to load")
            Exit Sub
        End If
        Dim sort_code As String = FeesSQLDataSet.LCS_Bank_details.Rows(0).Item(1)
        Dim acc_no As String = FeesSQLDataSet.LCS_Bank_details.Rows(0).Item(3)
        Dim acc_name As String = FeesSQLDataSet.LCS_Bank_details.Rows(0).Item(2)
        sort_code_tbox.Text = sort_code
        acc_name_tbox.Text = acc_name
        acc_no_tbox.Text = acc_no
        Dim log_user As String
        log_user = My.User.Name
        Try
            LCS_logTableAdapter.InsertQuery(maat_id, Now, log_user)
        Catch ex As Exception

        End Try

    End Sub

   

    Private Sub manfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class recvd_fromfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.recvd_from_dg = New System.Windows.Forms.DataGridView
        Me.ReceivedfromBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.Received_fromBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Received_fromTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
        Me.revd_from_dg = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.recvd_text_dg = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.recvd_from_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceivedfromBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Received_fromBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'recvd_from_dg
        '
        Me.recvd_from_dg.AllowUserToDeleteRows = False
        Me.recvd_from_dg.AllowUserToOrderColumns = True
        Me.recvd_from_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.recvd_from_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.revd_from_dg, Me.recvd_text_dg})
        Me.recvd_from_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.recvd_from_dg.Location = New System.Drawing.Point(0, 0)
        Me.recvd_from_dg.Name = "recvd_from_dg"
        Me.recvd_from_dg.Size = New System.Drawing.Size(326, 346)
        Me.recvd_from_dg.TabIndex = 0
        '
        'ReceivedfromBindingSource
        '
        Me.ReceivedfromBindingSource.DataMember = "Received_from"
        Me.ReceivedfromBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Received_fromBindingSource
        '
        Me.Received_fromBindingSource.DataMember = "Received_from"
        Me.Received_fromBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Received_fromTableAdapter
        '
        Me.Received_fromTableAdapter.ClearBeforeFill = True
        '
        'revd_from_dg
        '
        Me.revd_from_dg.HeaderText = "No"
        Me.revd_from_dg.Name = "revd_from_dg"
        Me.revd_from_dg.ReadOnly = True
        Me.revd_from_dg.Width = 50
        '
        'recvd_text_dg
        '
        Me.recvd_text_dg.HeaderText = "Text"
        Me.recvd_text_dg.Name = "recvd_text_dg"
        Me.recvd_text_dg.Width = 200
        '
        'recvd_fromfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(326, 346)
        Me.Controls.Add(Me.recvd_from_dg)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "recvd_fromfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Complainant"
        CType(Me.recvd_from_dg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceivedfromBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Received_fromBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents recvd_from_dg As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents Received_fromBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Received_fromTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
    Friend WithEvents ReceivedfromBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents revd_from_dg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents recvd_text_dg As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

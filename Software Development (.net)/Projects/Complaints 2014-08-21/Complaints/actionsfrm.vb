Public Class actionsfrm

    Private Sub actionsfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Actions' table. You can move, or remove it, as needed.
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Actions' table. You can move, or remove it, as needed.
        Me.ActionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Actions)
        'TODO: This line of code loads data into the 'ActionsDataSet.Actions' table. You can move, or remove it, as needed.
        action_orig_no_rows = DataGridView1.RowCount
    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.ColumnIndex = 1 And e.RowIndex >= 0 Then
            action_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            action_name = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            If DataGridView1.Rows(e.RowIndex).IsNewRow Then Return
            If e.RowIndex = 2 Then
                Return
            End If
            Try
                Me.ActionsTableAdapter.UpdateQuery(action_name, action_code)
                log_text = "Action amended - " & action_code & _
                                                  " from " & orig_text & " to " & action_name
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate action entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter

        action_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        orig_text = DataGridView1.Rows(e.RowIndex).Cells(1).Value

    End Sub

    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles DataGridView1.RowsRemoved
        If e.RowIndex > 0 And e.RowIndex <> 2 Then
            Try
                Try
                    Me.ActionsTableAdapter.DeleteQuery(action_code)
                Catch
                    MessageBox.Show("Can't delete as it's used in a complaint")
                End Try
                log_text = "Action deleted - " & action_code & " name " & orig_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
End Class
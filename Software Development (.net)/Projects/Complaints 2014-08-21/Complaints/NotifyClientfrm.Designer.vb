<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class NotifyClientfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.cl_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cl_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.Complaints_resp_clientsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Complaints_resp_clientsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaints_resp_clientsTableAdapter
        Me.findbtn = New System.Windows.Forms.Button
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Complaints_resp_clientsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cl_no, Me.cl_name})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(449, 698)
        Me.DataGridView1.TabIndex = 0
        '
        'cl_no
        '
        Me.cl_no.HeaderText = "Client No"
        Me.cl_no.Name = "cl_no"
        Me.cl_no.Width = 75
        '
        'cl_name
        '
        Me.cl_name.HeaderText = "Client Name"
        Me.cl_name.Name = "cl_name"
        Me.cl_name.Width = 300
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Complaints_resp_clientsBindingSource
        '
        Me.Complaints_resp_clientsBindingSource.DataMember = "Complaints_resp_clients"
        Me.Complaints_resp_clientsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Complaints_resp_clientsTableAdapter
        '
        Me.Complaints_resp_clientsTableAdapter.ClearBeforeFill = True
        '
        'findbtn
        '
        Me.findbtn.Location = New System.Drawing.Point(222, 0)
        Me.findbtn.Name = "findbtn"
        Me.findbtn.Size = New System.Drawing.Size(115, 23)
        Me.findbtn.TabIndex = 2
        Me.findbtn.Text = "Get client number"
        Me.findbtn.UseVisualStyleBackColor = True
        '
        'NotifyClientfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(449, 698)
        Me.Controls.Add(Me.findbtn)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "NotifyClientfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Notify Client"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Complaints_resp_clientsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents cl_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cl_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents Complaints_resp_clientsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaints_resp_clientsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaints_resp_clientsTableAdapter
    Friend WithEvents findbtn As System.Windows.Forms.Button
End Class

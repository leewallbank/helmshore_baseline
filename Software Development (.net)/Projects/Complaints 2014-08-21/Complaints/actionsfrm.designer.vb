<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class actionsfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.ActionsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.ActionsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ActionsTableAdapter
        Me.ActioncodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ActionnameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ActioncodeDataGridViewTextBoxColumn, Me.ActionnameDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.ActionsBindingSource
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(450, 368)
        Me.DataGridView1.TabIndex = 0
        '
        'ActionsBindingSource
        '
        Me.ActionsBindingSource.DataMember = "Actions"
        Me.ActionsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ActionsTableAdapter
        '
        'Me.ActionsTableAdapter.ClearBeforeFill = True
        '
        'ActioncodeDataGridViewTextBoxColumn
        '
        Me.ActioncodeDataGridViewTextBoxColumn.DataPropertyName = "action_code"
        Me.ActioncodeDataGridViewTextBoxColumn.HeaderText = "Code"
        Me.ActioncodeDataGridViewTextBoxColumn.Name = "ActioncodeDataGridViewTextBoxColumn"
        Me.ActioncodeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ActionnameDataGridViewTextBoxColumn
        '
        Me.ActionnameDataGridViewTextBoxColumn.DataPropertyName = "action_name"
        Me.ActionnameDataGridViewTextBoxColumn.HeaderText = "Text"
        Me.ActionnameDataGridViewTextBoxColumn.Name = "ActionnameDataGridViewTextBoxColumn"
        Me.ActionnameDataGridViewTextBoxColumn.Width = 300
        '
        'actionsfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(450, 368)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "actionsfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Actions"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents ActionsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ActionsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ActionsTableAdapter
    Friend WithEvents ActioncodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ActionnameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class calendarfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.EmployedbailiffsCalendarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FeesSQLDataSet = New EmployedBailiffsCalendar.FeesSQLDataSet
        Me.Employed_bailiffs_calendarTableAdapter = New EmployedBailiffsCalendar.FeesSQLDataSetTableAdapters.Employed_bailiffs_calendarTableAdapter
        Me.month = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.year = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Start_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.end_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.EmployedbailiffsCalendarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.month, Me.year, Me.Start_date, Me.end_date})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(463, 437)
        Me.DataGridView1.TabIndex = 0
        '
        'EmployedbailiffsCalendarBindingSource
        '
        Me.EmployedbailiffsCalendarBindingSource.DataMember = "Employed_bailiffs calendar"
        Me.EmployedbailiffsCalendarBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Employed_bailiffs_calendarTableAdapter
        '
        Me.Employed_bailiffs_calendarTableAdapter.ClearBeforeFill = True
        '
        'month
        '
        Me.month.HeaderText = "Month"
        Me.month.Name = "month"
        '
        'year
        '
        Me.year.HeaderText = "Year"
        Me.year.Name = "year"
        '
        'Start_date
        '
        Me.Start_date.HeaderText = "Start date"
        Me.Start_date.Name = "Start_date"
        '
        'end_date
        '
        Me.end_date.HeaderText = "End Date"
        Me.end_date.Name = "end_date"
        '
        'calendarfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(463, 437)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "calendarfrm"
        Me.Text = "calendarfrm"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.EmployedbailiffsCalendarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents FeesSQLDataSet As EmployedBailiffsCalendar.FeesSQLDataSet
    Friend WithEvents EmployedbailiffsCalendarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Employed_bailiffs_calendarTableAdapter As EmployedBailiffsCalendar.FeesSQLDataSetTableAdapters.Employed_bailiffs_calendarTableAdapter
    Friend WithEvents month As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents year As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Start_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents end_date As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

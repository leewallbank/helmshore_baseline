Public Class calendarfrm

    Private Sub calendarfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'delete all calendar and then insert all
        Employed_bailiffs_calendarTableAdapter.DeleteQuery()
        Dim datarow As DataGridViewRow
        Dim idx As Integer = 0
        For Each datarow In DataGridView1.Rows
            Dim month, year, start_date, end_date As String
            Try
                month = DataGridView1.Rows(idx).Cells(0).Value
            Catch ex As Exception
                idx += 1
                Continue For
            End Try
            If month = Nothing Then
                idx += 1
                Continue For
            End If
            year = DataGridView1.Rows(idx).Cells(1).Value
            start_date = DataGridView1.Rows(idx).Cells(2).Value
            end_date = DataGridView1.Rows(idx).Cells(3).Value
            Employed_bailiffs_calendarTableAdapter.InsertQuery(month, year, Start_date, end_date)
            idx += 1
        Next
    End Sub

    Private Sub calendarfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FeesSQLDataSet.Employed_bailiffs_calendar' table. You can move, or remove it, as needed.
        Me.Employed_bailiffs_calendarTableAdapter.Fill(Me.FeesSQLDataSet.Employed_bailiffs_calendar)

        'populate datagridview
        DataGridView1.Rows.Clear()
        Dim datarow As DataRow
        Dim idx As Integer = 0
        For Each datarow In FeesSQLDataSet.Employed_bailiffs_calendar.Rows
            DataGridView1.Rows.Add(FeesSQLDataSet.Employed_bailiffs_calendar.Rows(idx).Item(0), _
            FeesSQLDataSet.Employed_bailiffs_calendar.Rows(idx).Item(1), _
            Format(FeesSQLDataSet.Employed_bailiffs_calendar.Rows(idx).Item(2), "dd.MM.yyyy"), _
            Format(FeesSQLDataSet.Employed_bailiffs_calendar.Rows(idx).Item(3), "dd.MM.yyyy"))
            idx += 1
        Next
    End Sub
End Class
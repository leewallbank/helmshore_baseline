Public Class mainform
    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline, caption As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim propaddr As String = ""
        Dim curraddr As String = ""
        Dim name As String = ""
        Dim name2 As String = ""
        Dim lostring As String = ""
        Dim idx, idx2, idx3 As Integer
        Dim lines As Integer = 0
        Dim debt_amt, war_amt, saved_war_amt As Decimal
        Dim lodate, fromdate, saved_fromdate, todate As Date
        Dim clref As String = ""
        Dim lo_num As Integer = 0
        Dim addrline As String = ""
        Dim amtstring As String = ""
        Dim line_length As Integer
        Dim comments As String

        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "Client ref|Name|DebtAddress|Name2|Current Address|LO Date|Warrant Amt|From date|To date|Debt Amount|Summons No|Comments" & vbNewLine
        outfile = outline

        'look for Account No line 
        For idx = 1 To lines - 1
            caption = Mid(line(idx), 48, 10)
            If caption = "Account No" Then
                debt_amt = 0
                war_amt = 0
                name2 = ""
                comments = ""
                war_amt = 0
                clref = Mid(line(idx), 61, 9)
                idx += 1
                lo_num = Mid(line(idx), 61, 7)
                'look for Re: which is line before name
                For idx2 = idx To lines - 1
                    If Mid(line(idx2), 7, 3) = "Re:" Then
                        idx = idx2 + 1
                        line_length = Microsoft.VisualBasic.Len(line(idx))
                        name = Trim(Microsoft.VisualBasic.Right(line(idx), line_length - 1))
                        Exit For
                    End If
                Next
                'look for a joint name on the next line
                idx += 1
                line_length = Microsoft.VisualBasic.Len(line(idx))
                name2 = Trim(Microsoft.VisualBasic.Right(line(idx), line_length - 1))
                curraddr = ""
                'now get address lines 
                For idx2 = idx + 1 To lines - 1
                    If Mid(line(idx2), 7, 6) = "A Liab" Then
                        Exit For
                    End If
                    Dim trimmed_line As String = Trim(line(idx2))
                    line_length = Microsoft.VisualBasic.Len(trimmed_line)
                    If line_length > 6 Then
                        curraddr = curraddr & Trim(Microsoft.VisualBasic.Right(trimmed_line, line_length - 6)) & " "
                    End If
                Next
                'get lo date from next line
                idx = idx2 + 1
                lostring = Mid(line(idx), 53, 11)
                If IsDate(lostring) Then
                    lodate = CDate(lostring)
                Else
                    errorfile = errorfile & "Line  " & idx & " - LO date is not valid - " _
                                    '             & lostring & vbNewLine
                End If
                'get debt amount from 6 lines down
                idx = idx + 6
                amtstring = ""
                For idx2 = 11 To 27
                    If Mid(line(idx), idx2, 1) = " " Then
                        Exit For
                    End If
                    amtstring = amtstring & Mid(line(idx), idx2, 1)
                Next
                If Not IsNumeric(amtstring) Then
                    errorfile = errorfile & "Line  " & idx2 & " - Debt amount not numeric - " _
                             & amtstring & vbNewLine
                Else
                    debt_amt = amtstring
                End If
                'now look for property line
                propaddr = ""
                For idx2 = idx + 1 To lines - 1
                    If Mid(line(idx2), 7, 8) = "Property" Then
                        idx = idx2 + 2
                        Exit For
                    End If
                Next
                Dim blank_lines As Integer = 0
                Dim from_date_found As Boolean = False
                Dim prop_addr_found As Boolean = False
                Dim first_from_date As Boolean = True
                For idx2 = idx To lines - 1
                    caption = Mid(line(idx2), 48, 10)
                    If caption = "Account No" Then
                        Exit For
                    End If
                    'get from date and warrant date
                    Dim this_line_has_from_date As Boolean = False
                    For idx3 = 10 To 70
                        If Mid(line(idx2), idx3, 5) = "From " Then
                            If from_date_found = True Then
                                'add previous details to comments line
                                comments = comments & "Warrant amt " & saved_war_amt & " from " _
                                        & fromdate & " to " & todate & " "
                                prop_addr_found = True
                            End If
                            from_date_found = True
                            this_line_has_from_date = True
                            Exit For
                        End If
                    Next
                    If this_line_has_from_date Then
                        this_line_has_from_date = False
                        lostring = Mid(line(idx2), idx3 + 5, 10)
                        If IsDate(lostring) Then
                            fromdate = CDate(lostring)
                            If first_from_date Then
                                first_from_date = False
                                saved_fromdate = fromdate
                            End If
                        Else
                            errorfile = errorfile & "Line  " & idx2 & " - From date is not valid - " _
                                            '             & lostring & vbNewLine
                        End If
                        amtstring = ""
                        For idx3 = idx3 + 15 To 84
                            If IsNumeric(Mid(line(idx2), idx3, 1)) Or _
                            Mid(line(idx2), idx3, 1) = "." Then
                                amtstring = amtstring & Mid(line(idx2), idx3, 1)
                            End If
                        Next
                        If Not IsNumeric(amtstring) Then
                            errorfile = errorfile & "Line  " & idx2 & " - Warrant amount not numeric - " _
                                     & amtstring & vbNewLine
                        Else
                            war_amt += amtstring
                            saved_war_amt = amtstring
                        End If
                        'get to date on next line
                        For idx3 = 10 To 60
                            If Mid(line(idx2 + 1), idx3, 5) = "to   " Then
                                lostring = Mid(line(idx2 + 1), idx3 + 5, 10)
                                If IsDate(lostring) Then
                                    todate = CDate(lostring)
                                    Exit For
                                Else
                                    errorfile = errorfile & "Line  " & idx2 & " - To date is not valid - " _
                                                    '             & lostring & vbNewLine
                                End If
                            End If
                        Next
                    End If
                    
                    If prop_addr_found = True Then
                        Continue For
                    End If
                    Dim trimmed_line As String = Trim(line(idx2))
                    line_length = Microsoft.VisualBasic.Len(trimmed_line)
                    If line_length < 7 Then
                        blank_lines += 1
                        If blank_lines >= 2 Then
                            Exit For
                        End If
                        Continue For
                    End If
                    blank_lines = 0
                    Dim build_line As String = ""
                    For idx3 = 7 To 40
                        If Mid(trimmed_line, idx3, 1) = vbLf Or _
                            Mid(trimmed_line, idx3, 1) = vbCr Or _
                            Mid(trimmed_line, idx3, 1) = vbTab Then
                            Exit For
                        Else
                            build_line = build_line & Mid(trimmed_line, idx3, 1)
                        End If
                    Next
                    propaddr = propaddr & Trim(build_line) & " "
                Next

                If war_amt <> saved_war_amt Then
                    'add last warrant amount to comments
                    comments = comments & "Warrant amt " & saved_war_amt & " from " _
                                                    & fromdate & " to " & todate & vbCr
                End If
                'validate case details
                If clref = Nothing Then
                    errorfile = errorfile & "Line  " & idx2 & " - No client reference" & vbNewLine
                End If

                'save case in outline
                outfile = outfile & clref & "|" & name & "|" & propaddr & "|" & name2 _
                 & "|" & curraddr & "|" & lodate & "|" & war_amt & _
                  "|" & saved_fromdate & "|" & todate & "|" & debt_amt & "|" & lo_num & "|" & comments & vbNewLine
                name = ""
                name2 = ""
                propaddr = ""
                curraddr = ""
                lodate = Nothing
                debt_amt = Nothing
                war_amt = Nothing
                lo_num = Nothing
                fromdate = Nothing
                todate = Nothing

            End If
        Next
        viewbtn.Enabled = True
        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
    

    Private Sub checkbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

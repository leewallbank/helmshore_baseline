Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Runbtn.Click
        Dim start_date As Date = CDate("01 Feb 2012")
        Dim end_date As Date = CDate("29 Feb 2012")
        Dim clientID = 1245
        param2 = "select _rowid from clientScheme where clientID = " & clientID & _
        " and branchID = 2"
        Dim cs_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("No client schemes found for clientiD = " & clientID)
            Exit Sub
        End If
        Dim csid_rows As Integer = no_of_rows - 1
        Dim csid_idx As Integer
        For csid_idx = 0 To csid_rows
            Dim csid As Integer = cs_dataset.Tables(0).Rows(csid_idx).Item(0)
            param2 = "select _rowID, debt_balance, status_open_closed, return_date from Debtor where clientSchemeID = " & csid & _
            " and arrange_started is not null"
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim debt_rows As Integer = no_of_rows - 1
            Dim debt_idx As Integer
            For debt_idx = 0 To debt_rows
                ProgressBar1.Value = (debt_idx / debt_rows) * 100
                Application.DoEvents()
                Dim debtorID As Integer = debtor_dataset.Tables(0).Rows(debt_idx).Item(0)
                If debtor_dataset.Tables(0).Rows(debt_idx).Item(2) = "C" Then
                    Try
                        Dim return_date As Date = debtor_dataset.Tables(0).Rows(debt_idx).Item(3)
                        If return_date < start_date Then
                            Continue For
                        End If
                    Catch ex As Exception

                    End Try
                End If
                'If debtorID = 5931595 Then
                '    debtorID = 5931595
                'End If
                param2 = "select type, _createdDate, text from Note where debtorID = " & debtorID & _
                " and (type = 'Arrangement' or type = 'Clear arrange' or type = 'Broken')" & _
                " and _createddate < '" & Format(end_date, "yyyy-MM-dd") & "' order by _createdDate desc"
                Dim note_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    Continue For
                End If
                Dim idx As Integer
                Dim start_arr_date As Date
                Dim end_arr_date As Date = CDate("01 Jan 2100")
                Dim last_start_arr_date As Date = CDate("01 Jan 1900")
                Dim arr_array(10, 2) As Date
                Dim arr_idx As Integer = 0
                Dim no_of_notes As Integer = no_of_rows - 1
                For idx = 0 To no_of_notes
                    If note_dataset.Tables(0).Rows(idx).Item(0) = "Arrangement" Then
                        Dim note_text As String = note_dataset.Tables(0).Rows(idx).Item(2)
                        Dim start_idx As Integer = InStr(note_text, "due on ")
                        Dim test_date As Date
                        If start_idx > 0 Then
                            Try
                                test_date = Mid(note_text, start_idx + 7, 12)
                            Catch ex As Exception
                                Continue For
                            End Try
                        End If
                        start_arr_date = test_date
                        If start_arr_date > end_date Then
                            end_arr_date = note_dataset.Tables(0).Rows(idx).Item(1)
                            Continue For
                        End If
                        If Format(start_arr_date, "yyyy-MM-dd") = Format(last_start_arr_date, "yyyy-MM-dd") Then
                            Continue For
                        End If
                        last_start_arr_date = start_arr_date
                        arr_idx += 1
                        arr_array(arr_idx, 1) = start_arr_date
                        arr_array(arr_idx, 2) = end_arr_date
                        end_arr_date = start_arr_date  'end of any previous arrangement
                        If start_arr_date < start_date Then
                            Exit For
                        End If
                    Else
                        end_arr_date = note_dataset.Tables(0).Rows(idx).Item(1)
                        If Format(end_arr_date, "yyyy-MM-dd") = Format(start_arr_date, "yyyy-MM-dd") Then
                            end_arr_date = arr_array(arr_idx, 2)
                            arr_idx -= 1
                        End If
                        If end_arr_date < start_date Then
                            Exit For
                        End If
                    End If
                Next
                For idx = 1 To arr_idx
                    start_arr_date = arr_array(idx, 1)
                    Dim actual_start_arr_date As Date = start_arr_date
                    If start_arr_date < start_date Then
                        start_arr_date = start_date
                    End If
                    end_arr_date = arr_array(idx, 2)
                    Dim actual_end_arr_date As Date = end_arr_date
                    If end_arr_date > end_date Then
                        end_arr_date = end_date
                    End If
                    'get payments since arrangement start date
                    param2 = "select amount, date from Payment where debtorID = " & debtorID & _
                    " and (status = 'W' or status = 'R') and date >= '" & Format(start_arr_date, "yyyy-MM-dd") & "'"
                    Dim pay_dataset As DataSet = get_dataset("onestep", param2)
                    Dim tot_amount_paid As Decimal = 0
                    Dim amt_paid_in_period As Decimal = 0
                    Dim idx2 As Integer
                    Dim no_of_payments = no_of_rows - 1
                    For idx2 = 0 To no_of_payments
                        tot_amount_paid += pay_dataset.Tables(0).Rows(idx2).Item(0)
                        If pay_dataset.Tables(0).Rows(idx2).Item(1) <= end_arr_date Then
                            amt_paid_in_period += pay_dataset.Tables(0).Rows(idx2).Item(0)
                        End If
                    Next
                    'need to calculate balance at start of arrangement
                    Dim amount_owed As Decimal = debtor_dataset.Tables(0).Rows(debt_idx).Item(1) ' current balance
                    amount_owed += tot_amount_paid
                    Try
                        Me.ArrangementStatsTableAdapter1.InsertQuery(debtorID, Format(actual_start_arr_date, "dd/MM/yyyy"), Format(actual_end_arr_date, "dd/MM/yyyy"), amt_paid_in_period, amount_owed)
                    Catch ex As Exception
                        Try
                            Me.ArrangementStatsTableAdapter1.DeleteQuery(debtorID, Format(actual_start_arr_date, "dd/MM/yyyy"))
                            Me.ArrangementStatsTableAdapter1.InsertQuery(debtorID, Format(actual_start_arr_date, "dd/MM/yyyy"), Format(actual_end_arr_date, "dd/MM/yyyy"), amt_paid_in_period, amount_owed)
                        Catch ex2 As Exception
                            MsgBox(ex2.Message)
                        End Try
                    End Try
                Next
            Next
        Next
        MsgBox("stats saved")
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

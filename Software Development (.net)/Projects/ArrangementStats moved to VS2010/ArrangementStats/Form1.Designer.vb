<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Runbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.FeesSQLDataSet1 = New ArrangementStats.FeesSQLDataSet
        Me.ArrangementStatsTableAdapter1 = New ArrangementStats.FeesSQLDataSetTableAdapters.ArrangementStatsTableAdapter
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        CType(Me.FeesSQLDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Runbtn
        '
        Me.Runbtn.Location = New System.Drawing.Point(127, 85)
        Me.Runbtn.Name = "Runbtn"
        Me.Runbtn.Size = New System.Drawing.Size(75, 23)
        Me.Runbtn.TabIndex = 0
        Me.Runbtn.Text = "Run"
        Me.Runbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(227, 221)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'FeesSQLDataSet1
        '
        Me.FeesSQLDataSet1.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ArrangementStatsTableAdapter1
        '
        Me.ArrangementStatsTableAdapter1.ClearBeforeFill = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 221)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(326, 266)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.Runbtn)
        Me.Name = "Form1"
        Me.Text = "Arrangement stats"
        CType(Me.FeesSQLDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents FeesSQLDataSet As ArrangementStats.FeesSQLDataSet
    Friend WithEvents ArrangementStatsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ArrangementStatsTableAdapter As ArrangementStats.FeesSQLDataSetTableAdapters.ArrangementStatsTableAdapter
    Friend WithEvents FeesSQLDataSet1 As ArrangementStats.FeesSQLDataSet
    Friend WithEvents ArrangementStatsTableAdapter1 As ArrangementStats.FeesSQLDataSetTableAdapters.ArrangementStatsTableAdapter
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar

End Class

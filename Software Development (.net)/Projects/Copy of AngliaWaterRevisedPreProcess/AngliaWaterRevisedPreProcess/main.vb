Public Class mainform



    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim propaddr As String = ""
        Dim curraddr As String = ""
        Dim name As String = ""
        Dim idx, idx2 As Integer
        Dim lines As Integer = 0
        Dim debt_amt As Decimal
        Dim offence_court As Date
        Dim dob As Date
        Dim offence_reg As Date
        Dim offence_from As Date
        Dim offence_date As Date
        Dim clref As String = ""
        Dim title As String = ""
        Dim forename As String = ""
        Dim offence_number As Decimal
        Dim surname As String = ""
        Dim addr1 As String = ""
        Dim addr2 As String = ""
        Dim addr3 As String = ""
        Dim addr4 As String = ""
        Dim pcode As String = ""
        Dim phone As String = ""
        Dim phone2 As String = ""
        Dim email As String = ""
        Dim debt_addr1 As String = ""
        Dim debt_addr2 As String = ""
        Dim debt_addr3 As String = ""
        Dim debt_addr4 As String = ""
        Dim debt_addr5 As String = ""
        Dim name2 As String = ""
        Dim latepaymentCharge As Decimal = 0
        Dim code As String = ""
        Dim sep4482 As String = "|"
        Dim sep4481 As String = "*"
        Dim sep4483 As String = "`"
        Dim sep4486 As String = "~"
        Dim sep4487 As String = "#"
        Dim sep4484 As String = "%"
        Dim sep4496 As String = "<"
        Dim sep4485 As String = ">"
        Dim sep4491 As String = "$"
        Dim sep4489 As String = "?"
        Dim sep4492 As String = "^"
        Dim sep4490 As String = "!"
        Dim sep4480 As String = "�"
        Dim sep4479 As String = "{"
        Dim sep4493 As String = "}"
        Dim sep4495 As String = "["
        Dim sep4494 As String = "]"
        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "ClientRef|OffenceNumber|DebtAmount|title|forename|surname|Name2|curr_addr1" & _
        "|curr_addr2|curr_addr3|curr_addr4|curr_postcode|Phone|Phone2|Email|debt_addr1|" & _
        "debt_addr2|debt_addr3|debt_addr4|debt_addr5|OffenceCourt|OffenceReg|OffenceFrom|OffenceDate|" & _
        "DOB|LatePaymentCharge|comments" & vbNewLine
        outfile_new = outline
        outfile_upd = outline
        outfile_wdraw = outline
        outfile_vacated = outline
        Dim vacated As Boolean
        Dim outfile_upd2 As String = "DebtorID,ClientREf,New Debt, OnestepClientBalance" & vbNewLine
        'process cases delimited by a pipe
        For idx = 0 To lines - 1
            ProgressBar1.Value = (idx / lines) * 100
            Dim amtstring As String
            Dim lostring, field As String
            Dim line_length As Integer = line(idx).Length
            Dim start_field_idx As Integer = 1
            Dim end_field_idx As Integer = 0
            Dim field_no As Integer = 0
            Dim row_count As Integer = 0
            Dim calc_row_count As Integer = 0
            For idx2 = 1 To line_length
                'first line is date and row-count
                'ignore any row of length < 10
                If line(idx).Length < 10 Then
                    Continue For
                End If
                If idx = 0 Then
                    Try
                        row_count = Microsoft.VisualBasic.Right(line(0), line(0).Length - 9)
                    Catch ex As Exception
                        errorfile = errorfile & "Line  " & idx & " - row-count is not numeric" & vbNewLine
                    End Try
                    Exit For
                End If

                If Mid(line(idx), idx2, 1) = "|" Or idx2 = line_length Then
                    field_no += 1
                    end_field_idx = idx2
                    field = Mid(line(idx), start_field_idx, end_field_idx - start_field_idx)
                    If Microsoft.VisualBasic.Left(field, 1) = Chr(10) Then
                        field = Microsoft.VisualBasic.Right(field, field.Length - 1)
                    End If
                    field = Trim(field)
                    field = remove_chars(field)
                    Select Case field_no
                        Case 1
                            calc_row_count += 1
                            code = field
                            If code <> "N" And code <> "W" And code <> "U" Then
                                errorfile = errorfile & "Line  " & idx & " - code is not N, U or W - " _
                                                                         & code & vbNewLine
                            End If
                        Case 2
                            amtstring = field
                            If Not IsNumeric(amtstring) Then
                                errorfile = errorfile & "Line  " & idx & " - offence number not numeric - " _
                                         & amtstring & vbNewLine
                            Else
                                offence_number = amtstring
                            End If
                        Case 3
                            clref = field
                        Case 4
                            amtstring = field
                            If Not IsNumeric(amtstring) Then
                                errorfile = errorfile & "Line  " & idx & " - debt not numeric - " _
                                         & amtstring & vbNewLine
                            Else
                                debt_amt = amtstring
                                If code = "U" Then
                                    'get debt amount from onestep
                                    '18.11.2014 use debt balance
                                    param2 = "select D._rowid,CS.clientID " & _
                                    " from Debtor D, clientScheme CS" & _
                                    " where client_ref = '" & clref & "'" & _
                                    " and status_open_closed = 'O'" & _
                                    " and CS._rowID = D.clientschemeID " & _
                                    " and CS.clientID = 1245"
                                    Dim debt_ds As DataSet = get_dataset("onestep", param2)
                                    Dim debtIDX As Integer
                                    For debtIDX = 0 To debt_ds.Tables(0).Rows.Count - 1
                                        Dim debtorID As Integer = debt_ds.Tables(0).Rows(debtIDX).Item(0)
                                        Dim client_debtOS As Decimal = 0
                                        param2 = "select fee_amount, remited_fee from Fee " & _
                                        " where debtorID = " & debtorID & _
                                        " and fee_remit_col < 3"
                                        Dim fee_ds As DataSet = get_dataset("onestep", param2)
                                        Dim feeRow As DataRow
                                        For Each feeRow In fee_ds.Tables(0).Rows
                                            client_debtOS = client_debtOS + feeRow(0) - feeRow(1)
                                        Next
                                        If debt_amt <> client_debtOS Then
                                            outfile_upd2 &= debtorID & "," & clref & "," & debt_amt & "," & client_debtOS & vbNewLine
                                        End If
                                    Next
                                End If
                            End If
                        Case 5
                            If field.Length > 0 Then
                                comments = "BPCourtFlag:" & field & ";"
                                space_comments()
                            End If
                        Case 6
                            If field.Length > 0 Then
                                comments = comments & "CAConfirmedHomeownerDetails:" & field & ";"
                                space_comments()
                            End If
                        Case 7
                            If field.Length > 0 Then
                                comments = comments & "PRMoveIn:" & field & ";"
                                space_comments()
                            End If
                        Case 8
                            If field.Length > 0 Then
                                comments = comments & "PRMoveOut:" & field & ";"
                                space_comments()
                            End If
                        Case 9
                            title = field
                        Case 10
                            forename = field
                        Case 11
                            surname = field
                        Case 12
                            name2 = field
                        Case 13
                            addr1 = field
                        Case 14
                            addr2 = field
                        Case 15
                            addr3 = field
                        Case 16
                            addr4 = field
                        Case 17
                            pcode = field
                        Case 18
                            phone = field
                        Case 19
                            phone2 = field
                        Case 20
                            email = field
                        Case 21
                            debt_addr1 = field
                        Case 22
                            debt_addr2 = field
                        Case 23
                            debt_addr3 = field
                        Case 24
                            debt_addr4 = field
                        Case 25
                            debt_addr5 = field
                        Case 26
                            lostring = field
                            If lostring.Length > 0 Then
                                If Not IsDate(lostring) Then
                                    errorfile = errorfile & "Line  " & idx & " - offence from Date not valid - " _
                                                                 & lostring & vbNewLine
                                Else
                                    offence_from = lostring
                                End If
                            End If
                        Case 27
                            If field.Length > 0 Then
                                comments = comments & "CADCARequestType:" & field & ";"
                                space_comments()
                            End If
                        Case 28
                            vacated = False
                            If field.Length > 0 Then
                                comments = comments & "CAVacated:" & field & ";"
                                space_comments()
                                If field = "Y" Then
                                    vacated = True
                                End If
                            End If
                        Case 29
                            If field.Length > 0 Then
                                comments = comments & "CAInvoiceDate:" & field & ";"
                                space_comments()
                            End If
                        Case 30
                            If field.Length > 0 Then
                                comments = comments & "CALastBillAmount:" & field & ";"
                                space_comments()
                            End If
                        Case 31
                            If field.Length > 0 Then
                                comments = comments & "CALastBFAmount:" & field & ";"
                                space_comments()
                            End If
                        Case 32
                            If field.Length > 0 Then
                                comments = comments & "CALastBillVATAmount:" & field & ";"
                                space_comments()
                            End If
                        Case 33
                            lostring = field
                            If lostring.Length > 0 Then
                                If Not IsDate(lostring) Then
                                    errorfile = errorfile & "Line  " & idx & " - offence date Date not valid - " _
                                                                 & lostring & vbNewLine
                                Else
                                    offence_date = lostring
                                End If
                            End If
                        Case 34
                            If field.Length > 0 Then
                                comments = comments & "CTClaimNo:" & field & ";"
                                space_comments()
                            End If
                        Case 35
                            lostring = field
                            If lostring.Length > 0 Then
                                If Not IsDate(lostring) Then
                                    errorfile = errorfile & "Line  " & idx & " - offence court Date not valid - " _
                                                                 & lostring & vbNewLine
                                Else
                                    offence_court = lostring
                                End If
                            End If
                        Case 36
                            lostring = field
                            If lostring.Length > 0 Then
                                If Not IsDate(lostring) Then
                                    errorfile = errorfile & "Line  " & idx & " - offence reg Date not valid - " _
                                                                 & lostring & vbNewLine
                                Else
                                    offence_reg = lostring
                                End If
                            End If
                        Case 37
                            If field.Length > 0 Then
                                comments = comments & "CLaimPeriodStart:" & field & ";"
                                space_comments()
                            End If

                        Case 38
                            If field.Length > 0 Then
                                comments = comments & "CLaimPeriodEnd:" & field & ";"
                                space_comments()
                            End If

                        Case 39
                            If field.Length > 0 Then
                                comments = comments & "CATracedAddress:" & field & ";"
                                space_comments()
                            End If
                        Case 40
                            If field.Length > 0 Then
                                If Not IsDate(field) Then
                                    errorfile = errorfile & "Line  " & idx & " - Date of Birth not valid - " _
                                                                 & field & vbNewLine
                                Else
                                    dob = CDate(field)
                                End If
                            End If
                        Case 41
                            If field.Length > 0 Then
                                comments = comments & "NumberOfFailedArrangements:" & field & ";"
                                space_comments()
                            End If
                        Case 42
                            If field.Length > 0 Then
                                comments = comments & "CAISDefaultIssued:" & field & ";"
                                space_comments()
                            End If
                        Case 43
                            If field.Length > 0 Then
                                comments = comments & "FiledVisitCorrectCustomerContact:" & field & ";"
                                space_comments()
                            End If
                        Case 44
                            If field.Length > 0 Then
                                comments = comments & "MeasuredorUnmeasured:" & field & ";"
                                space_comments()
                            End If
                        Case 45
                            If field.Length > 0 Then
                                comments = comments & "MeterReading:" & field & ";"
                                space_comments()
                            End If
                        Case 46
                            If field.Length > 0 Then
                                comments = comments & "TypeofMeterReading:" & field & ";"
                                space_comments()
                            End If
                        Case 47
                            If field.Length > 0 Then
                                If Not IsDate(field) Then
                                    errorfile = errorfile & "Line  " & idx & " - Date of Meter reading not valid - " _
                                              & field & vbNewLine
                                Else
                                    comments = comments & "DateofMeterReading:" & Format(CDate(field), "dd/MM/yyyy") & ";"
                                    space_comments()
                                End If
                            End If
                        Case 48
                            If field.Length > 0 Then
                                'comments = comments & "LandlordIndicator:" & field & ";"
                                comments = comments & "AccountIndicator:"
                                Select Case field
                                    Case "Y"
                                        comments = comments & "Domestic Landlord;"
                                    Case "N"
                                        comments = comments & "Domestic;"
                                    Case "L"
                                        comments = comments & "Commercial Landlord;"
                                    Case "C"
                                        comments = comments & "Commercial;"
                                    Case Else
                                        comments = comments & "Unknown;"
                                End Select
                                space_comments()
                            End If
                        Case 49
                            If field.Length > 0 Then
                                comments = comments & "WithdrawalReason:" & field & ";"
                                space_comments()
                            End If
                        Case 50
                            If field.Length > 0 Then
                                comments = comments & "PaymentsSinceReferral:" & field & ";"
                                space_comments()
                            End If
                        Case 51
                            '18.11.2014 changed from Y/N to amount
                            'If field.Length > 0 Then
                            '    comments = comments & "AdminFeeLetterSent:" & field & ";"
                            '    space_comments()
                            'End If
                            If field.Length > 0 Then
                                comments = comments & "LatePaymentCharge:" & field & ";"
                                space_comments()
                                latepaymentcharge = field
                            End If

                        Case 52
                            If field.Length > 0 Then
                                comments = comments & "DCA.DCAID:" & field & ";"
                                space_comments()
                            End If
                    End Select
                    start_field_idx = idx2 + 1
                End If
            Next
            If idx = 0 Then
                Continue For
            End If
            If line(idx).Length < 10 Then
                Continue For
            End If
            'validate case details
            If clref = Nothing Then
                errorfile = errorfile & "Line  " & idx & " - No client reference" & vbNewLine
            End If

            'save case in outline
            Dim outrec As String = clref & "|" & offence_number & "|" & debt_amt & "|" & _
                title & "|" & forename & "|" & surname & "|" & name2 & "|" & addr1 & "|" & _
                addr2 & "|" & addr3 & "|" & addr4 & "|" & pcode & "|" & phone & "|" & _
                phone2 & "|" & email & "|" & debt_addr1 & "|" & debt_addr2 & "|" & debt_addr3 & "|" & _
                debt_addr4 & "|" & debt_addr5 & "|"

            If offence_court = Nothing Then
                outrec = outrec & "|"
            Else
                outrec = outrec & offence_court & "|"
            End If
            If offence_reg = Nothing Then
                outrec = outrec & "|"
            Else
                outrec = outrec & offence_reg & "|"
            End If
            If offence_from = Nothing Then
                outrec = outrec & "|"
            Else
                outrec = outrec & offence_from & "|"
            End If
            If offence_date = Nothing Then
                outrec = outrec & "|"
            Else
                outrec = outrec & offence_date & "|"
            End If
            If dob = Nothing Then
                outrec = outrec & "|"
            Else
                outrec = outrec & Format(dob, "dd/MM/yyyy") & "|"
            End If
            outrec &= latepaymentCharge & "|"
            Dim comments2 As String = ""
            Dim comments3 As String = comments
            'If comments.Length <= 250 Then
            outrec = outrec & comments
            'Else
            '    While comments3.Length > 250
            '        Dim len As Integer = Microsoft.VisualBasic.Len(comments3)
            '        Dim idx3 As Integer
            '        For idx3 = 250 To 1 Step -1
            '            If Mid(comments3, idx3, 1) = ";" Then
            '                Exit For
            '            End If
            '        Next
            '        comments2 = comments2 & Microsoft.VisualBasic.Left(comments3, idx3)
            '        For idx3 = idx3 To 250
            '            comments2 = comments2 & " "
            '        Next
            '        comments3 = Microsoft.VisualBasic.Right(comments3, len - idx3)
            '    End While
            '    outrec = outrec & comments2 & comments3
            'End If

            Select Case code
                Case "N"
                    If vacated Then
                        outfile_vacated = outfile_vacated & outrec & vbNewLine
                    Else
                        outfile_new = outfile_new & outrec & vbNewLine
                    End If
                Case "U"
                    outfile_upd = outfile_upd & outrec & vbNewLine
                Case "W"
                    outfile_wdraw = outfile_wdraw & outrec & vbNewLine
            End Select


            'clear fields
            name2 = ""
            title = ""
            forename = ""
            surname = ""
            addr1 = ""
            addr2 = ""
            addr3 = ""
            addr4 = ""
            phone = ""
            phone2 = ""
            email = ""
            pcode = ""
            debt_addr1 = ""
            debt_addr2 = ""
            debt_addr3 = ""
            debt_addr4 = ""
            debt_addr5 = ""
            comments = ""
            offence_court = Nothing
            offence_from = Nothing
            offence_date = Nothing
            clref = ""
            debt_amt = Nothing
            offence_number = Nothing
            offence_reg = Nothing
            dob = Nothing
            latepaymentCharge = 0
        Next

        viewbtn.Enabled = True
        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_ocupied.txt", outfile_new, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_update.txt", outfile_upd, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_update2.csv", outfile_upd2, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_withdraw.txt", outfile_wdraw, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_vacated.txt", outfile_vacated, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText("error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
            MsgBox("Files saved OK")
            Me.Close()
        End If
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
    Private Function remove_chars(ByVal field As String) As String
        field = Replace(field, "&", " ")
        field = Replace(field, "^", " ")
        field = Trim(field)
        Return field

    End Function
    Private Sub space_comments()
        Dim spaces As Integer
        Try

            spaces = (Int((comments.Length / 250)) + 1) * 250
            comments = comments & Space(spaces - comments.Length)
        Catch ex As Exception
            MsgBox("error in spacing comments")
        End Try

    End Sub
End Class

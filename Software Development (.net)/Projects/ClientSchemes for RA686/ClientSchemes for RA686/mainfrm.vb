Public Class mainfrm
    Dim changes As Boolean = True
    Private Sub mainfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'delete rows in table then insert from datagrid
        If changes = False Then
            MsgBox("No changes made")
            Exit Sub
        End If
        Client_schemes_for_RA686TableAdapter.DeleteQuery()
        Dim idx As Integer
        Dim cl_rows As Integer = DataGridView1.Rows.Count
        For idx = 0 To cl_rows - 1
            Dim cl_no As Integer = DataGridView1.Rows(idx).Cells(0).Value
            If cl_no > 0 Then
                Try
                    Client_schemes_for_RA686TableAdapter.InsertQuery(cl_no)
                Catch ex As Exception

                End Try

            End If
        Next
        MsgBox("Changes updated")
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FeesSQLDataSet.client_schemes_for_RA686' table. You can move, or remove it, as needed.
        Me.Client_schemes_for_RA686TableAdapter.Fill(Me.FeesSQLDataSet.client_schemes_for_RA686)
        Dim idx As Integer
        param1 = "onestep"
        Dim client_rows As Integer = FeesSQLDataSet.client_schemes_for_RA686.Rows.Count
        For idx = 0 To client_rows - 1
            Dim cl_no As Integer = FeesSQLDataSet.client_schemes_for_RA686.Rows(idx).Item(0)
            param2 = "select name from Client where _rowid = " & cl_no
            Dim cl_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                MsgBox("Client not found for cl_no = " & cl_no)
            End If
            Dim cl_name As String = Trim(cl_dataset.Tables(0).Rows(0).Item(0))
            DataGridView1.Rows.Add(cl_no, cl_name)
        Next
    End Sub

    Private Sub DataGridView1_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DataGridView1.CellValidating
        If e.ColumnIndex = 1 Then
            Exit Sub
        End If
        Try
            If e.FormattedValue = 0 Then
                Exit Sub
            End If
        Catch ex As Exception
            Exit Sub
        End Try
        
        param2 = "select name from Client where _rowid = " & e.FormattedValue
        Dim cl_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("Client number does nor exist on onestep")
            e.Cancel = True
            Exit Sub
        End If
        Dim cl_name As String = Trim(cl_dataset.Tables(0).Rows(0).Item(0))
        DataGridView1.Rows(e.RowIndex).Cells(1).Value = cl_name
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        changes = False
        Me.Close()
    End Sub

End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Arrow_report_datesDataSet = New ArrowActivityReport.Arrow_report_datesDataSet
        Me.Arrow_report_datesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Arrow_report_datesTableAdapter = New ArrowActivityReport.Arrow_report_datesDataSetTableAdapters.Arrow_report_datesTableAdapter
        CType(Me.Arrow_report_datesDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Arrow_report_datesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Arrow_report_datesDataSet
        '
        Me.Arrow_report_datesDataSet.DataSetName = "Arrow_report_datesDataSet"
        Me.Arrow_report_datesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Arrow_report_datesBindingSource
        '
        Me.Arrow_report_datesBindingSource.DataMember = "Arrow_report_dates"
        Me.Arrow_report_datesBindingSource.DataSource = Me.Arrow_report_datesDataSet
        '
        'Arrow_report_datesTableAdapter
        '
        Me.Arrow_report_datesTableAdapter.ClearBeforeFill = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(332, 266)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Arrow Activity report"
        CType(Me.Arrow_report_datesDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Arrow_report_datesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Arrow_report_datesDataSet As ArrowActivityReport.Arrow_report_datesDataSet
    Friend WithEvents Arrow_report_datesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Arrow_report_datesTableAdapter As ArrowActivityReport.Arrow_report_datesDataSetTableAdapters.Arrow_report_datesTableAdapter

End Class

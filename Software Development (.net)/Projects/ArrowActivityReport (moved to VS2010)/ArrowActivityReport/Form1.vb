Public Class Form1
    Dim log_file, log_name As String
    Dim csid_no_of_rows As Integer
    Dim csid_table(200) As Integer
    Dim csid, csid_idx As Integer
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim file1_written As Boolean = False
    Dim file2_written As Boolean = False
    Dim file3_written As Boolean = False
    Dim file4_written As Boolean = False
    Dim file5_written As Boolean = False
    Dim file6_written As Boolean = False
    Dim file7_written As Boolean = False
    Dim file8_written As Boolean = False
    Dim file9_written As Boolean = False
    Dim file10_written As Boolean = False
    Dim file11_written As Boolean = False
    Dim file12_written As Boolean = False
    Dim file13_written As Boolean = False
    Dim file14_written As Boolean = False
    Dim file15_written As Boolean = False
    Dim FName As String
    Dim act_file As String
    Dim out_of_space As Boolean = False
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        log_file = "Started Activity report at " & Format(Now, "yyyy-MM-dd HH:mm:ss") & vbNewLine
        log_name = "R:/Arrow Reports/ActivityReport_log_" & Format(Now, "ddMMyyyy") & ".txt"
        My.Computer.FileSystem.WriteAllText(log_name, log_file, False)
        '9.12.2014 add erudio cl=1874
        param1 = "onestep"
        param2 = "select _rowid from ClientScheme where clientID = 986 or clientID = 1160 or clientID = 1301 or clientID = 1765 or clientID = 1874"
        Dim csid_dataset As DataSet = get_dataset(param1, param2)
        csid_no_of_rows = no_of_rows - 1

        For csid_idx = 0 To csid_no_of_rows
            csid_table(csid_idx) = csid_dataset.Tables(0).Rows(csid_idx).Item(0)
        Next
        Dim activity_record As String = "UniqueTransactionID|ArrowKey|ActivityDate|ActivityType|" & _
                "ActivityComment|" & vbNewLine
        'get last reported values from arrow_report_dates
        Me.Arrow_report_datesTableAdapter.FillBy1(Me.Arrow_report_datesDataSet.Arrow_report_dates)
        Dim last_note_id As Long = Me.Arrow_report_datesDataSet.Tables(0).Rows(0).Item(2)
        Dim latest_note_id As Long = last_note_id
        For csid_idx = 0 To csid_no_of_rows
            csid = csid_table(csid_idx)
            param1 = "onestep"
            param2 = "select _rowid, client_ref from Debtor where clientschemeID = " & csid
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            Dim debtor, idx, idx2, tran_id As Integer
            Dim cl_ref, act_type, act_comment As String
            Dim act_date As Date
            Dim debtor_rows As Integer = no_of_rows
            For idx = 0 To debtor_rows - 1
                Application.DoEvents()
                debtor = debtor_dataset.Tables(0).Rows(idx).Item(0)
                'get all notes
                param2 = "select _rowid, _createdDate, type, text from Note where debtorID=" & debtor & _
                " and _rowid > " & last_note_id & _
                " order by _rowid"
                Dim note_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 0 Then
                    Continue For
                End If

                Dim note_rows As Integer = no_of_rows
                For idx2 = 0 To note_rows - 1
                    tran_id = note_dataset.Tables(0).Rows(idx2).Item(0)
                    cl_ref = Trim(debtor_dataset.Tables(0).Rows(idx).Item(1))
                    act_date = note_dataset.Tables(0).Rows(idx2).Item(1)
                    act_type = note_dataset.Tables(0).Rows(idx2).Item(2)
                    Try
                        act_comment = Trim(note_dataset.Tables(0).Rows(idx2).Item(3))
                        act_comment = Trim(remove_chars(act_comment))
                    Catch ex As Exception
                        act_comment = ""
                    End Try
                    activity_record &= tran_id & "|" & cl_ref & "|" & _
                               Format(act_date, "dd/MM/yyyy") & "|" & act_type & "|" & act_comment & "|" & vbNewLine
                    Try
                        act_file &= activity_record 
                    Catch ex As Exception
                        save_file()
                        act_file = "UniqueTransactionID|ArrowKey|ActivityDate|ActivityType|" & _
                                       "ActivityComment|" & vbNewLine & activity_record
                    End Try
                    activity_record = ""
                Next
                If tran_id > latest_note_id Then
                    latest_note_id = tran_id
                End If
            Next
        Next
        'save report details for next time
        If last_note_id <> latest_note_id Then
            Me.Arrow_report_datesTableAdapter.InsertQuery1(Now, latest_note_id)
        End If
        If Not out_of_space Then
            save_file()
            log_file = "Finished Inventory report at " & Format(Now, "yyyy-MM-dd HH:mm:ss") & vbNewLine
            My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
        End If

        Me.Close()
    End Sub
    Sub save_file()
        If file1_written = False Then
            file1_written = True
            Try
                FName = "R:/Arrow Reports/Rossendales_ActivityReport1_" & Format(Now, "ddMMyyyy") & ".txt"
                My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
            Catch
                log_file = "Can't save file1" & vbNewLine
                My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
            End Try
        Else
            If file2_written = False Then
                file2_written = True
                FName = "R:/Arrow Reports/Rossendales_ActivityReport2_" & Format(Now, "ddMMyyyy") & ".txt"
                Try
                    My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                Catch
                    log_file = "Can't save file2" & vbNewLine
                    My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                End Try
            Else
                If file3_written = False Then
                    file3_written = True
                    FName = "R:/Arrow Reports/Rossendales_ActivityReport3_" & Format(Now, "ddMMyyyy") & ".txt"
                    Try
                        My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                    Catch
                        log_file = "Can't save file3" & vbNewLine
                        My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                    End Try
                Else
                    If file4_written = False Then
                        file4_written = True
                        FName = "R:/Arrow Reports/Rossendales_ActivityReport4_" & Format(Now, "ddMMyyyy") & ".txt"
                        Try
                            My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                        Catch
                            log_file = "Can't save file4" & vbNewLine
                            My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                        End Try
                    Else
                        If file5_written = False Then
                            file5_written = True
                            FName = "R:/Arrow Reports/Rossendales_ActivityReport5_" & Format(Now, "ddMMyyyy") & ".txt"
                            Try
                                My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                            Catch
                                log_file = "Can't save file5" & vbNewLine
                                My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                            End Try
                        Else
                            If file6_written = False Then
                                file6_written = True
                                FName = "R:/Arrow Reports/Rossendales_ActivityReport6_" & Format(Now, "ddMMyyyy") & ".txt"
                                Try
                                    My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                                Catch
                                    log_file = "Can't save file6" & vbNewLine
                                    My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                End Try
                            Else
                                If file7_written = False Then
                                    file7_written = True
                                    FName = "R:/Arrow Reports/Rossendales_ActivityReport7_" & Format(Now, "ddMMyyyy") & ".txt"
                                    Try
                                        My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                                    Catch
                                        log_file = "Can't save file7" & vbNewLine
                                        My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                    End Try
                                Else
                                    If file8_written = False Then
                                        file8_written = True
                                        FName = "R:/Arrow Reports/Rossendales_ActivityReport8_" & Format(Now, "ddMMyyyy") & ".txt"
                                        Try
                                            My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                                        Catch
                                            log_file = "Can't save file8" & vbNewLine
                                            My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                        End Try
                                    Else
                                        If file9_written = False Then
                                            file9_written = True
                                            FName = "R:/Arrow Reports/Rossendales_ActivityReport9_" & Format(Now, "ddMMyyyy") & ".txt"
                                            Try
                                                My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                                            Catch
                                                log_file = "Can't save file9" & vbNewLine
                                                My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                            End Try
                                        Else
                                            If file10_written = False Then
                                                file10_written = True
                                                FName = "R:/Arrow Reports/Rossendales_ActivityReport10_" & Format(Now, "ddMMyyyy") & ".txt"
                                                Try
                                                    My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                                                Catch
                                                    log_file = "Can't save file10" & vbNewLine
                                                    My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                End Try
                                            Else
                                                If file11_written = False Then
                                                    file11_written = True
                                                    FName = "R:/Arrow Reports/Rossendales_ActivityReport11_" & Format(Now, "ddMMyyyy") & ".txt"
                                                    Try
                                                        My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                                                    Catch
                                                        log_file = "Can't save file11" & vbNewLine
                                                        My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                    End Try
                                                Else
                                                    If file12_written = False Then
                                                        file12_written = True
                                                        FName = "R:/Arrow Reports/Rossendales_ActivityReport12_" & Format(Now, "ddMMyyyy") & ".txt"
                                                        Try
                                                            My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                                                        Catch
                                                            log_file = "Can't save file12" & vbNewLine
                                                            My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                        End Try
                                                    Else
                                                        If file13_written = False Then
                                                            file13_written = True
                                                            FName = "R:/Arrow Reports/Rossendales_ActivityReport13_" & Format(Now, "ddMMyyyy") & ".txt"
                                                            Try
                                                                My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                                                            Catch
                                                                log_file = "Can't save file13" & vbNewLine
                                                                My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                            End Try
                                                        Else
                                                            If file14_written = False Then
                                                                file14_written = True
                                                                FName = "R:/Arrow Reports/Rossendales_ActivityReport14_" & Format(Now, "ddMMyyyy") & ".txt"
                                                                Try
                                                                    My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                                                                Catch
                                                                    log_file = "Can't save file14" & vbNewLine
                                                                    My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                                End Try
                                                            Else
                                                                If file15_written = False Then
                                                                    file15_written = True
                                                                    FName = "R:/Arrow Reports/Rossendales_ActivityReport15_" & Format(Now, "ddMMyyyy") & ".txt"
                                                                    Try
                                                                        My.Computer.FileSystem.WriteAllText(FName, act_file, False, ascii)
                                                                    Catch
                                                                        log_file = "Can't save file15" & vbNewLine
                                                                        My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                                    End Try
                                                                Else
                                                                    out_of_space = True
                                                                    log_file = "More files required - contact IT" & vbNewLine
                                                                    My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub
    Function remove_chars(ByVal text As String) As String
        Dim idx As Integer
        Dim new_text As String = ""
        For idx = 1 To text.Length
            If Mid(text, idx, 1) = Chr(10) Or Mid(text, idx, 1) = Chr(9) Or _
            Mid(text, idx, 1) = Chr(13) Or Mid(text, idx, 1) = "|" Then
                new_text = new_text & " "
            Else
                new_text = new_text & Mid(text, idx, 1)
            End If
        Next
        Return new_text
    End Function
End Class

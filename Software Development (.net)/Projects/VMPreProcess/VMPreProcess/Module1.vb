Module Module1
    Public filename, outfile_secondary, outfile_tertiary, errorfile, text_line As String
    Public name_array() As String
    Public fromdate, todate As Date
    Public fromdate_found, todate_found As Boolean
    Public name_count, saved_name_id, orig_no_rows, rep_tot_cases As Integer

    Public Sub delete_name(ByVal name_id As Integer)
        Dim traceconn As New OleDb.OleDbConnection

        Dim previousConnectionState As ConnectionState
        previousConnectionState = traceconn.State
        If traceconn.State = ConnectionState.Closed Then
            traceconn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=r:\Access tables\split_names.mdb;Persist Security Info=False"
            traceconn.Open()
        End If
        Dim query As String
        query = "delete from split_names where name_id = ?"
        Dim cmd As New OleDb.OleDbCommand(query, traceconn)
        Dim parameter As New OleDb.OleDbParameter
        parameter.ParameterName = "name_id"
        parameter.OleDbType = OleDb.OleDbType.Integer
        parameter.Direction = ParameterDirection.Input
        parameter.Value = name_id
        cmd.Parameters.Add(parameter)
        cmd.ExecuteNonQuery()
        traceconn.Close()
    End Sub
    Public Sub read_names()
        Dim nameconn As New OleDb.OleDbConnection
        nameconn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=r:\Access tables\split_names.mdb;Persist Security Info=False"
        nameconn.Open()
        Dim query As String = "select name_id, split_name from split_names"
        Dim cmd As New OleDb.OleDbCommand(query, nameconn)
        cmd.CommandType = CommandType.Text
        Dim rdr As OleDb.OleDbDataReader
        rdr = cmd.ExecuteReader()
        Dim idx As Integer = 0
        name_count = 0
        If rdr.HasRows Then
            Do While rdr.Read()
                Try
                    ReDim Preserve name_array(idx)
                    name_array(idx) = rdr.Item(1)
                Catch ex As Exception
                    MsgBox(ex.Message)
                    nameconn.Close()
                    Exit Sub
                End Try
                idx += 1
            Loop
        End If
        name_count = idx
    End Sub
    Public Sub insert_name(ByVal split_name As String)
        Dim nameconn As New OleDb.OleDbConnection

        Dim previousConnectionState As ConnectionState
        previousConnectionState = nameconn.State
        If nameconn.State = ConnectionState.Closed Then
            nameconn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=r:\Access tables\split_names.mdb;Persist Security Info=False"
            nameconn.Open()
        End If

        Dim query As String

        query = "insert into Trace values(?) "

        Dim parm1 As New OleDb.OleDbParameter

        Dim cmd2 As New OleDb.OleDbCommand(query, nameconn)
        cmd2.CommandType = CommandType.Text
        parm1.ParameterName = "split_name"
        parm1.OleDbType = OleDb.OleDbType.Integer
        parm1.Direction = ParameterDirection.Input
        parm1.Value = split_name
        cmd2.Parameters.Add(parm1)
        cmd2.ExecuteNonQuery()
        nameconn.Close()
    End Sub
    Public Sub check_names()
        Dim idx, idx2, line_length, name_length As Integer

        line_length = Microsoft.VisualBasic.Len(text_line)
        For idx = 0 To name_count - 1
            name_length = Microsoft.VisualBasic.Len(name_array(idx))
            For idx2 = 1 To line_length - name_length + 1
                If LCase(Mid(text_line, idx2, name_length)) = LCase(name_array(idx)) Then
                    text_line = Trim(Microsoft.VisualBasic.Left(text_line, idx2 - 1)) & " " _
                    & name_array(idx) & " " & _
                    Trim(Microsoft.VisualBasic.Right(text_line, line_length - idx2 - name_length + 1))
                    text_line = UCase(text_line)
                End If
            Next
        Next
    End Sub
End Module

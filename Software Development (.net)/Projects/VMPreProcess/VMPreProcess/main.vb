Public Class mainform
    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Tcsv files | *.csv"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline As String
        Dim linetext As String = ""
        Dim line(0) As String
       
        Dim idx, idx2 As Integer
        Dim lines As Integer = 0
        Dim clref As String = ""
        Dim comments As String = ""
        Dim firstname As String = ""

        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                If Mid(linetext, 2, 11) = ",,,,,,,,,,," Then
                    Continue For
                End If
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "ClientRef|OffenceNo|Title|FirstName|LastName|Address1|Address2" & _
        "|Address3|PostCode|OffenceLine1|OffenceLine2|OffenceLine3|OffenceLine4|PhoneNo|" & _
        "PhoneNo2|OffenceUpTo|DOB|DebtAmt|" & _
        "OffenceCourt|Notes" & vbNewLine
        outfile_secondary = outline
        outfile_tertiary = outline
        Dim field As String = ""
        Dim dte As Date
        Dim secondary_no As Integer = 0
        Dim tertiary_no As Integer = 0
        Dim file_type As String = ""
        'fields comma separated
        For idx = 1 To lines - 1
            'ignore first line as it is a heading
            Dim start_idx As Integer = 1
            Dim field_no As Integer = 1
            'ignore commas inside quotes
            Dim inside_quote As Boolean = False
            For field_no = 1 To 39
                For idx2 = start_idx To line(idx).Length
                    If Mid(line(idx), idx2, 1) = """" Then
                        If inside_quote Then
                            inside_quote = False
                        Else
                            inside_quote = True
                        End If
                    End If
                    If Mid(line(idx), idx2, 1) = "," And inside_quote = False Then
                        field = Trim(Mid(line(idx), start_idx, idx2 - start_idx))
                        start_idx = idx2 + 1
                        Exit For
                    End If
                Next
                Dim idx3 As Integer
                Dim new_field As String = ""
                For idx3 = 1 To field.Length
                    If Mid(field, idx3, 1) <> """" And _
                       Mid(field, idx3, 1) <> "," Then
                        new_field = new_field & Mid(field, idx3, 1)
                    End If
                Next
                field = new_field
                Select Case field_no
                    Case 1  'accountno - ignore
                    Case 2 'client ref
                        clref = field
                        outline = clref & "|"
                    Case 3 'offencenumber
                        outline = outline & field & "|"
                    Case 4 'title
                        outline = outline & field & "|"
                    Case 5 'firstname
                        firstname = field
                    Case 6 'initials
                        outline = outline & firstname & " " & field & "|"
                    Case 7 'lastname
                        outline = outline & field & "|"
                    Case 8 'addressline1
                        outline = outline & field & "|"
                    Case 9 'addressline2
                        outline = outline & field & "|"
                    Case 10 'addressline3
                        outline = outline & field & "|"
                    Case 11 'postcode-
                        outline = outline & field & "|"
                    Case 12 'offence line1
                        outline = outline & field & "|"
                    Case 13 'offence line2
                        outline = outline & field & "|"
                    Case 14 'offence line3
                        outline = outline & field & "|"
                    Case 15 'offence pcode
                        outline = outline & field & "|"
                    Case 16 'phone
                        outline = outline & field & "|"
                    Case 17 'phone2
                        outline = outline & field & "|"
                    Case 18 'phone3
                        If field.Length > 0 Then
                            comments = comments & "Phone3:" & field & ";"
                        End If
                    Case 19 'phone4
                        If field.Length > 0 Then
                            comments = comments & "Phone4:" & field & ";"
                        End If
                    Case 20 'offenceupto

                        If field.Length > 0 Then
                            dte = CDate(field)
                            outline = outline & Format(dte, "dd/MM/yyyy") & "|"
                        Else
                            outline = outline & "|"
                        End If
                    Case 21 'disconnection reason
                        If field.Length > 0 Then
                            comments = comments & "DisconnectionReason:" & field & ";"
                        End If
                    Case 22 'last paydate
                        If field.Length > 0 Then
                            dte = CDate(field)
                            comments = comments & "LastPayDate:" & Format(dte, "dd/MM/yyyy") & ";"
                        End If
                    Case 23 'dob
                        If field.Length > 2 Then
                            dte = CDate(field)
                            'ignore 1.1.1987
                            If dte = CDate("1987 01 01") Then
                                outline = outline & "|"
                            Else
                                outline = outline & Format(dte, "dd/MM/yyyy") & "|"
                            End If
                        Else
                            outline = outline & "|"
                        End If
                    Case 24 'franchise
                            If field.Length > 0 Then
                                comments = comments & "Franchise:" & field & ";"
                            End If
                    Case 25 'agency - ignore
                    Case 26 'clientcode -ignore
                    Case 27 'casemonth
                            If field.Length > 0 Then
                                comments = comments & "CaseMonth:" & field & ";"
                            End If
                    Case 28 'payerype
                            If field.Length > 0 Then
                                comments = comments & "PayerType:" & field & ";"
                            End If
                    Case 29 'totalpay
                            If field.Length > 0 Then
                                comments = comments & "TotalPay:" & field & ";"
                            End If
                    Case 30 'installdate
                            If field.Length > 2 Then
                                dte = CDate(field)
                                comments = comments & "InstallDdate:" & Format(dte, "dd/MM/yyyy") & ";"
                            End If
                    Case 31 'placement type
                            If field.Length > 0 Then
                                comments = comments & "PlacementType:" & field & ";"
                            End If
                    Case 32 'placement description
                            If field.Length > 0 Then
                                If field = "Tertiary" Then
                                    file_type = "T"
                                    tertiary_no += 1
                                ElseIf field = "Secondary" Then
                                    file_type = "S"
                                    secondary_no += 1
                                Else
                                    MsgBox("ERROR - Placement Desc not Secondary or Tertiary - Line No " & idx)
                                    Exit Sub
                                End If
                            End If
                    Case 33 'payment type
                            If field.Length > 0 Then
                                comments = comments & "PaymentType:" & field & ";"
                            End If
                    Case 34 'product combination
                            If field.Length > 0 Then
                                comments = comments & "ProductCombination:" & field & ";"
                            End If
                    Case 35 'account status
                            If field.Length > 0 Then
                                comments = comments & "AccountStatus:" & field & ";"
                            End If
                    Case 36 'original balance
                            outline = outline & field & "|"
                    Case 37 'arrears - ignore
                    Case 38 'filename - ignore
                    Case 39 'offencecourt
                            field = Trim(Mid(line(idx), start_idx, line(idx).Length - start_idx + 1))
                            If field.Length > 0 Then
                                new_field = ""
                                For idx3 = 1 To field.Length
                                    If Mid(field, idx3, 1) <> """" Then
                                        new_field = new_field & Mid(field, idx3, 1)
                                    End If
                                Next
                                field = new_field
                                dte = CDate(field)
                                outline = outline & Format(dte, "dd/MM/yyyy") & "|"
                            Else
                                outline = outline & "|"
                            End If
                End Select
            Next

            'validate case details
            If clref = Nothing Then
                errorfile = errorfile & "Line  " & idx & " - No client reference" & vbNewLine
            End If
            'save case in outline
            Dim comments2 As String = ""
            Dim comments3 As String = comments
            If comments.Length <= 250 Then
                If file_type = "T" Then
                    outfile_tertiary = outfile_tertiary & outline & comments & vbNewLine
                Else
                    outfile_secondary = outfile_secondary & outline & comments & vbNewLine
                End If

            Else
                While comments3.Length > 250
                    Dim len As Integer = Microsoft.VisualBasic.Len(comments3)
                    Dim idx5 As Integer
                    For idx5 = 250 To 1 Step -1
                        If Mid(comments3, idx5, 1) = ";" Then
                            Exit For
                        End If
                    Next
                    comments2 = comments2 & Microsoft.VisualBasic.Left(comments3, idx5)
                    Dim idx6 As Integer
                    For idx6 = idx5 To 250
                        comments2 = comments2 & " "
                    Next
                    comments3 = Microsoft.VisualBasic.Right(comments3, len - idx5)
                End While
                If file_type = "T" Then
                    outfile_tertiary = outfile_tertiary & outline & comments2 & comments3 & vbNewLine
                Else
                    outfile_secondary = outfile_secondary & outline & comments2 & comments3 & vbNewLine
                End If
            End If

            comments = ""
            firstname = ""
            outline = ""
        Next
        viewbtn.Enabled = True
        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_secondary_preprocess.txt", outfile_secondary, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_tertiary_preprocess.txt", outfile_tertiary, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
        MsgBox("Secondary = " & secondary_no & "  tertiary - " & tertiary_no)
        Me.Close()
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
    

    Private Sub checkbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

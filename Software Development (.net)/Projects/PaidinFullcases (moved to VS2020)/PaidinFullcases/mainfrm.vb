Imports System.Collections
Public Class mainfrm
    Private RA863report As RA863
    Private Const PARAMETER_FIELD_NAME1 As String = "parm_debtor"
    Dim myArrayList1 As ArrayList = New ArrayList()
    Dim folder_name, folder_name2, bail_type, xref_fname As String
    Dim start_date, end_date As Date
    Dim first_letter As String

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim days As Integer = -Weekday(Now)
        Date_picker.Value = DateAdd(DateInterval.Day, days, Now)

    End Sub

    Private Sub datesbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub filebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles filebtn.Click
        exitbtn.Enabled = False
        Dim start_day As String
        start_date = Date_picker.Value
        start_day = Format(start_date, "ddd")
        Dim end_date As Date = DateAdd(DateInterval.Day, 1, start_date)
        
        ProgressBar1.Value = 5
       
        param2 = "select _rowid, client_ref, return_date from Debtor " & _
        " where status_open_closed = 'C' and status = 'S'" & _
        " and clientschemeID = " & selected_csid & _
        " and return_date >='" & Format(start_date, "yyyy.MM.dd") & "'" & _
        " and return_date <'" & Format(end_date, "yyyy.MM.dd") & _
        "' order by client_ref"
        Dim debtor_dataset = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("There are no cases for this period")
            Exit Sub
        End If
        filebtn.Enabled = False
        Dim debtor_rows As Integer = no_of_rows
        Dim idx As Integer
        Dim fname As String = ""
        Dim first_case As Boolean = True
        For idx = 0 To debtor_rows - 1
            ProgressBar1.Value = (idx / debtor_rows) * 100
            Dim cl_ref As String
            cl_ref = Trim(debtor_dataset.tables(0).rows(idx).item(1).ToString)
            Dim debtor As Integer = debtor_dataset.tables(0).rows(idx).item(0)
            Dim retn_date As Date = debtor_dataset.tables(0).rows(idx).item(2)
            If first_case Then
                first_case = False
                With SaveFileDialog1
                    .Title = "Save files"
                    .Filter = "pdf|*.pdf"
                    .DefaultExt = ".pdf"
                    .OverwritePrompt = True
                    .FileName = debtor & "-" & cl_ref & ".pdf"
                    .Title = "Select folder for saving files (New folder NorthSomerset will be created)"
                End With
                If SaveFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
                    MsgBox("Files not created")
                    exitbtn.Enabled = True
                    Exit Sub
                Else
                    fname = SaveFileDialog1.FileName
                    Dim idx2 As Integer
                    For idx2 = fname.Length To 1 Step -1
                        If Mid(fname, idx2, 1) = "\" Then
                            fname = Microsoft.VisualBasic.Left(fname, idx2) & client_name & "\"
                            'check directory exists
                            Try
                                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(fname)
                                If System.IO.Directory.Exists(fname) = False Then
                                    di = System.IO.Directory.CreateDirectory(fname)
                                Else
                                    System.IO.Directory.Delete(fname, True)
                                    di = System.IO.Directory.CreateDirectory(fname)
                                End If
                            Catch ex As Exception
                                MsgBox("Unable to create folder")
                                End
                            End Try
                            Exit For
                        End If
                    Next
                End If
            End If
            write_report(debtor, cl_ref, fname)
            control_file = control_file & cl_ref & "-" & debtor & ".pdf," & cl_ref & "," & _
            Format(retn_date, "dd/MM/yyyy") & ",BAILRETR,CT" & vbNewLine
        Next

        'Dim control_file_name As String = fname
        'For idx = control_file_name.Length - 1 To 1 Step -1
        '    If Mid(control_file_name(idx), 1) = "\" Then
        '        Exit For
        '    End If
        'Next
        Dim control_file_name As String = fname & "\control_file.csv"
        My.Computer.FileSystem.WriteAllText(control_file_name, control_file, False)
        MsgBox("Files created in " & fname)
        Me.Close()
    End Sub
    Sub write_report(ByVal debtor As Integer, ByVal cl_ref As String, ByVal fname As String)
        Dim RA863Report = New RA863
        myArrayList1.Add(debtor)
        SetCurrentValuesForParameterField1(RA863Report, myArrayList1)
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA863Report)
        Dim file As String = fname & cl_ref & "-" & debtor & ".pdf"
        RA863Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, file)
        RA863Report.close()
    End Sub
    Public Sub SetDBLogonForReport(ByVal myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo, ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub
    Private Sub SetCurrentValuesForParameterField1(ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myArrayList As ArrayList)
        Dim currentParameterValues As CrystalDecisions.Shared.ParameterValues = New CrystalDecisions.Shared.ParameterValues()
        For Each submittedValue As Object In myArrayList
            Dim myParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue()
            myParameterDiscreteValue.Value = submittedValue.ToString()
            currentParameterValues.Add(myParameterDiscreteValue)
        Next
        Dim myParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        Dim myParameterFieldDefinition As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME1)
        myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)
    End Sub
   
    
    Private Sub clbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clbtn.Click
        first_letter = InputBox("Enter first letter of client", "First Letter of Client")
        first_letter = UCase(Microsoft.VisualBasic.Left(Trim(first_letter), 1))
        If first_letter.Length = 0 Then
            MsgBox("NO Letter entered")
            Exit Sub
        End If
        param1 = "onestep"
        param2 = "Select _rowid, name from Client where name like '" & first_letter & "%" & "'" & _
        " order by name"
        Dim client_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("There are no clients starting with " & first_letter)
            Exit Sub
        End If
        Dim idx As Integer
        cl_rows = no_of_rows - 1
        For idx = 0 To cl_rows
            client_array(1, idx) = client_dataset.Tables(0).Rows(idx).Item(0)
            client_array(2, idx) = Trim(client_dataset.Tables(0).Rows(idx).Item(1))
            If idx = UBound(client_array, 2) Then
                ReDim Preserve client_array(2, idx + 20)
            End If
        Next
        clientfrm.ShowDialog()
        If selected_csid = 0 Then
            MsgBox("No client scheme has been selected")
            Exit Sub
        Else
            If MsgBox("Client Scheme selected is " & client_name & " - " & scheme_name, MsgBoxStyle.YesNo, "Client Scheme verification") = MsgBoxResult.No Then
                Exit Sub
            End If
        End If
        date_picker.Enabled = True
        Label1.Text = client_name & " - " & scheme_name
        filebtn.Enabled = True
    End Sub
End Class

Module Module1
    Public error_file, error_message, error_path As String
    Public error_found As Boolean
    Public saved_bail_id, orig_no_rows, new_rows As Integer
    Public Sub SetDBLogonForReport(ByVal myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo, ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            If myTable.Name = "Bailiff" Then
                Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
            End If
        Next
    End Sub
    Public Sub SetDBLogonForReport2(ByVal myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo, ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            If myTable.Name <> "Bailiff" Then
                Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
            End If
        Next
    End Sub
    Sub write_error()
        Dim dir_info As IO.DirectoryInfo
        error_found = True
        Try
            If IO.Directory.Exists(error_path) = False Then
                dir_info = IO.Directory.CreateDirectory(error_path)
            End If
            'save document
        Catch ex As Exception
            MsgBox("Unable to save error file")
        End Try
        My.Computer.FileSystem.WriteAllText(error_file, error_message, True)
        error_message = ""
    End Sub
End Module

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class maintain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.bail_id = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bail_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.folder_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bail_type = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.BailiffComplaintsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Bailiff_complaintsDataSet = New BailiffComplaints.Bailiff_complaintsDataSet
        Me.Bailiff_ComplaintsTableAdapter = New BailiffComplaints.Bailiff_complaintsDataSetTableAdapters.Bailiff_ComplaintsTableAdapter
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BailiffComplaintsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Bailiff_complaintsDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.bail_id, Me.bail_name, Me.folder_name, Me.bail_type})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(542, 504)
        Me.DataGridView1.TabIndex = 0
        '
        'bail_id
        '
        Me.bail_id.HeaderText = "Bail ID"
        Me.bail_id.Name = "bail_id"
        Me.bail_id.Width = 75
        '
        'bail_name
        '
        Me.bail_name.HeaderText = "Bailiff name"
        Me.bail_name.Name = "bail_name"
        Me.bail_name.ReadOnly = True
        Me.bail_name.Width = 150
        '
        'folder_name
        '
        Me.folder_name.HeaderText = "Folder Name"
        Me.folder_name.Name = "folder_name"
        '
        'bail_type
        '
        Me.bail_type.HeaderText = "Type"
        Me.bail_type.Name = "bail_type"
        '
        'BailiffComplaintsBindingSource
        '
        Me.BailiffComplaintsBindingSource.DataMember = "Bailiff Complaints"
        Me.BailiffComplaintsBindingSource.DataSource = Me.Bailiff_complaintsDataSet
        '
        'Bailiff_complaintsDataSet
        '
        Me.Bailiff_complaintsDataSet.DataSetName = "Bailiff_complaintsDataSet"
        Me.Bailiff_complaintsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Bailiff_ComplaintsTableAdapter
        '
        Me.Bailiff_ComplaintsTableAdapter.ClearBeforeFill = True
        '
        'maintain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(542, 504)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "maintain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "maintain"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BailiffComplaintsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Bailiff_complaintsDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Bailiff_complaintsDataSet As BailiffComplaints.Bailiff_complaintsDataSet
    Friend WithEvents BailiffComplaintsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Bailiff_ComplaintsTableAdapter As BailiffComplaints.Bailiff_complaintsDataSetTableAdapters.Bailiff_ComplaintsTableAdapter
    Friend WithEvents bail_id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bail_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents folder_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bail_type As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

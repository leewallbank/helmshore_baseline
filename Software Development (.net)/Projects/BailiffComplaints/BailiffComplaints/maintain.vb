Public Class maintain

    Private Sub maintain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'get list of bailiffs and folder names
        Dim row As DataRow
        Dim idx As Integer = 0

        param1 = "onestep"
        param2 = "select _rowid, name_sur, name_fore from Bailiff" & _
        " where status = 'O' and agent_type = 'B' and hasPen = 'Y' and typeSub <> 'VO'" & _
        " order by name_sur, name_fore"

        Dim bailiff_dataset As DataSet = get_dataset(param1, param2)
        new_rows = 0
        orig_no_rows = 0
        Dim bail_no_rows As Integer = no_of_rows
        DataGridView1.Rows.Clear()
        For Each row In bailiff_dataset.Tables(0).Rows
            mainfrm.ProgressBar1.Value = idx / bail_no_rows * 100
            'check for override on access table
            Dim bail_id As Integer = bailiff_dataset.Tables(0).Rows(idx).Item(0)
            Me.Bailiff_ComplaintsTableAdapter.FillBy(Bailiff_complaintsDataSet.Bailiff_Complaints, bail_id)
            Dim override_found As Boolean = True
            Dim bail_name_str As String = ""
            Try
                bail_id = Bailiff_complaintsDataSet.Tables(0).Rows(0).Item(0)
            Catch ex As Exception
                override_found = False
            End Try
            If Not override_found Then
                idx += 1
                Continue For
            End If
            bail_name_str = Trim(bailiff_dataset.Tables(0).Rows(idx).Item(1))
            Try
                bail_name_str = bail_name_str & ", " & Trim(bailiff_dataset.Tables(0).Rows(idx).Item(2))
            Catch ex As Exception

            End Try
            Dim folder_name As String = Bailiff_complaintsDataSet.Tables(0).Rows(0).Item(1)
            Dim bail_type As String = Bailiff_complaintsDataSet.Tables(0).Rows(0).Item(2)
            DataGridView1.Rows.Add(bail_id, bail_name_str, folder_name, bail_type)
            idx += 1
            orig_no_rows += 1
        Next
        mainfrm.ProgressBar1.Value = 0
    End Sub

    Private Sub DataGridView1_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DataGridView1.CellValidating
        If e.ColumnIndex = 0 Then
            Dim new_bail_id As Integer
            Try
                new_bail_id = DataGridView1.Rows(e.RowIndex).Cells(0).EditedFormattedValue
            Catch ex As Exception
                new_bail_id = 0
            End Try
            If saved_bail_id > 0 And saved_bail_id <> new_bail_id Then
                MsgBox("Can't change bailiff ID - change back to " & saved_bail_id)
                e.Cancel = True
            End If
            If new_bail_id <> saved_bail_id And new_bail_id > 0 And saved_bail_id = 0 Then
                'check new_bail_id does not already exist in table
                Me.Bailiff_ComplaintsTableAdapter.FillBy(Bailiff_complaintsDataSet.Bailiff_Complaints, new_bail_id)
                Dim test As String = ""
                Dim found As Boolean = True
                Try
                    test = Bailiff_complaintsDataSet.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    found = False
                End Try
                If found Then
                    MsgBox("This bail ID already exists in the list")
                    e.Cancel = True
                    Exit Sub
                End If
                'check new_bail_id exists on onestep
                param2 = "select _rowid, name_sur, name_fore from Bailiff" & _
                        " where status = 'O' and agent_type = 'B' and hasPen = 'Y' and typeSub <> 'VO'" & _
                        " and _rowid = " & new_bail_id

                Dim bailiff_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 0 Then
                    MsgBox("This bailiff ID does not exist on ONESTEP")
                    e.Cancel = True
                    Exit Sub
                End If
                'get name
                Dim bail_name_str As String = Trim(bailiff_dataset.Tables(0).Rows(0).Item(1))
                Try
                    bail_name_str = bail_name_str & ", " & Trim(bailiff_dataset.Tables(0).Rows(0).Item(2))
                Catch ex As Exception
                End Try
                DataGridView1.Rows(e.RowIndex).Cells(1).Value = bail_name_str
            End If
        ElseIf e.ColumnIndex = 2 Then
            Dim folder_name As String
            folder_name = DataGridView1.Rows(e.RowIndex).Cells(2).EditedFormattedValue
            If Trim(folder_name).Length = 0 Then
                MsgBox("Please enter folder name")
                e.Cancel = True
            End If
        ElseIf e.ColumnIndex = 3 Then
            Dim bail_type As String = DataGridView1.Rows(e.RowIndex).Cells(3).EditedFormattedValue
            If UCase(bail_type) <> "FC" And UCase(bail_type) <> "VAN" Then
                MsgBox("Type must be FC or Van")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.RowIndex < 0 Then Return
        If DataGridView1.Rows(e.RowIndex).IsNewRow Then Return

        Dim bail_id As Integer

        bail_id = DataGridView1.Rows(e.RowIndex).Cells(0).EditedFormattedValue
        If e.ColumnIndex = 2 Then
            Dim folder_name As String
            folder_name = Trim(DataGridView1.Rows(e.RowIndex).Cells(2).Value)
            Try
                Me.Bailiff_ComplaintsTableAdapter.UpdateQuery(folder_name, bail_id)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        ElseIf e.ColumnIndex = 3 Then
            Dim bail_type As String = Trim(DataGridView1.Rows(e.RowIndex).Cells(3).Value)
            If Microsoft.VisualBasic.Left(UCase(bail_type), 1) = "F" Then
                bail_type = "FC"
            Else
                bail_type = "Van"
            End If
            Try
                Bailiff_ComplaintsTableAdapter.UpdateQuery1(bail_type, bail_id)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        saved_bail_id = DataGridView1.Rows(e.RowIndex).Cells(0).Value
    End Sub

    Private Sub DataGridView1_RowsAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsAddedEventArgs) Handles DataGridView1.RowsAdded
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        new_rows += 1
    End Sub

    

    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles DataGridView1.RowsRemoved
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        If saved_bail_id > 0 Then
            Try
                Me.Bailiff_ComplaintsTableAdapter.DeleteQuery(saved_bail_id)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
End Class
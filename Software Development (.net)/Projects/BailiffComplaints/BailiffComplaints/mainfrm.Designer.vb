<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.runbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Bailiff_complaintsDataSet = New BailiffComplaints.Bailiff_complaintsDataSet
        Me.Bailiff_ComplaintsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Bailiff_ComplaintsTableAdapter = New BailiffComplaints.Bailiff_complaintsDataSetTableAdapters.Bailiff_ComplaintsTableAdapter
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.mainbtn = New System.Windows.Forms.Button
        CType(Me.Bailiff_complaintsDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Bailiff_ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(93, 131)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(75, 23)
        Me.runbtn.TabIndex = 0
        Me.runbtn.Text = "Run reports"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(189, 190)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 3
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(48, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(170, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Save report RA778 in bailiff folders"
        '
        'Bailiff_complaintsDataSet
        '
        Me.Bailiff_complaintsDataSet.DataSetName = "Bailiff_complaintsDataSet"
        Me.Bailiff_complaintsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Bailiff_ComplaintsBindingSource
        '
        Me.Bailiff_ComplaintsBindingSource.DataMember = "Bailiff Complaints"
        Me.Bailiff_ComplaintsBindingSource.DataSource = Me.Bailiff_complaintsDataSet
        '
        'Bailiff_ComplaintsTableAdapter
        '
        Me.Bailiff_ComplaintsTableAdapter.ClearBeforeFill = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 190)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'mainbtn
        '
        Me.mainbtn.Location = New System.Drawing.Point(65, 74)
        Me.mainbtn.Name = "mainbtn"
        Me.mainbtn.Size = New System.Drawing.Size(131, 23)
        Me.mainbtn.TabIndex = 1
        Me.mainbtn.Text = "Maintain folder names"
        Me.mainbtn.UseVisualStyleBackColor = True
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(293, 266)
        Me.Controls.Add(Me.mainbtn)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bailiff Complaints reports"
        CType(Me.Bailiff_complaintsDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Bailiff_ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Bailiff_complaintsDataSet As BailiffComplaints.Bailiff_complaintsDataSet
    Friend WithEvents Bailiff_ComplaintsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Bailiff_ComplaintsTableAdapter As BailiffComplaints.Bailiff_complaintsDataSetTableAdapters.Bailiff_ComplaintsTableAdapter
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents mainbtn As System.Windows.Forms.Button


End Class

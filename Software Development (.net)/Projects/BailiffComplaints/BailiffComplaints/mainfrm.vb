Imports System.Collections
Public Class mainfrm
    Private RA778Report As RA778
    Private Const PARAMETER_FIELD_NAME As String = "parm_bailiff"
    Dim myArrayList As ArrayList = New ArrayList()
    Dim folder_name, folder_name2, bail_type As String
    Private Sub SetCurrentValuesForParameterField(ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myArrayList As ArrayList)
        Dim currentParameterValues As CrystalDecisions.Shared.ParameterValues = New CrystalDecisions.Shared.ParameterValues()
        For Each submittedValue As Object In myArrayList
            Dim myParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue()
            myParameterDiscreteValue.Value = submittedValue.ToString()
            currentParameterValues.Add(myParameterDiscreteValue)
        Next
        Dim myParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        Dim myParameterFieldDefinition As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME)

        myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)


    End Sub


    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        RA778Report = New RA778()
        error_path = "c:\bailiffcomplaints\"
        error_file = error_path & "error_file_" & Format(Now, "dd.MM.yyyy_HH.mm.ss") & ".txt"
        error_found = False
        error_message = ""
        'get list of bailiffs and folder names
        Dim row As DataRow
        Dim idx As Integer = 0

        param1 = "onestep"
        param2 = "select _rowid, name_sur, name_fore, internalExternal from Bailiff" & _
        " where status = 'O' and agent_type = 'B' and hasPen = 'Y' and typeSub <> 'VO'"

        Dim bailiff_dataset As DataSet = get_dataset(param1, param2)
        Dim bail_rows As Integer = no_of_rows
        Dim bail_id As Integer
        For Each row In bailiff_dataset.Tables(0).Rows
            ProgressBar1.Value = idx / no_of_rows * 100
            folder_name = ""
            folder_name2 = ""
            'check for override on access table
            bail_id = bailiff_dataset.Tables(0).Rows(idx).Item(0)
            myArrayList.Add(bail_id)
            Me.Bailiff_ComplaintsTableAdapter.FillBy(Bailiff_complaintsDataSet.Bailiff_Complaints, bail_id)
            Dim override_found As Boolean = True
            Try
                folder_name = Bailiff_complaintsDataSet.Tables(0).Rows(0).Item(1)
                bail_type = Bailiff_complaintsDataSet.Tables(0).Rows(0).Item(2)
            Catch ex As Exception
                override_found = False
            End Try
            If override_found Then
                folder_name2 = "XX"
                write_report(bail_id)
                idx += 1
                Continue For
            End If
            Try
                folder_name = Trim(bailiff_dataset.Tables(0).Rows(idx).Item(2))
                folder_name2 = Trim(Microsoft.VisualBasic.Left(bailiff_dataset.Tables(0).Rows(idx).Item(2), 1))
            Catch ex As Exception

            End Try
            Dim surname As String = Trim(bailiff_dataset.Tables(0).Rows(idx).Item(1))

            folder_name = folder_name & " " & surname

            folder_name = Trim(folder_name)

            folder_name2 = folder_name2 & " " & surname
            folder_name2 = Trim(folder_name2)
            If bailiff_dataset.Tables(0).Rows(idx).Item(3) = "I" Then
                bail_type = "FC"
            Else
                bail_type = "Van"
            End If

            write_report(bail_id)
            idx += 1
        Next
        If error_found Then
            MsgBox("Reports saved in bailiff folders - see errors in C:\bailiffcomplaints\")
        Else
            MsgBox("Reports saved in bailiff folders - no errors")
        End If
        ProgressBar1.Value = 0
    End Sub
    
    Sub write_report(ByVal bail_id As Integer)
        SetCurrentValuesForParameterField(RA778Report, myArrayList)
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        myConnectionInfo.ServerName = "DebtRecovery - DDSybase"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "thirdpartyB"
        myConnectionInfo.Password = "thirdpartyB"
        SetDBLogonForReport(myConnectionInfo, RA778Report)
        Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        myConnectionInfo2.ServerName = "RossFeeTable"
        myConnectionInfo2.DatabaseName = "PraiseAndComplaintsSQL"
        myConnectionInfo2.UserID = "sa"
        myConnectionInfo2.Password = "sa"
        SetDBLogonForReport2(myConnectionInfo2, RA778Report)
        Dim path As String = "\\rossvr02\shared\company\Enforce\Bailiff Performance\" & bail_type _
          & "\" & folder_name & "\"
        'check path exists for folder_name  (full first name)
        If IO.Directory.Exists(path) = False Then
            'try second folder name (just first initial
            path = "\\rossvr02\shared\company\Enforce\Bailiff Performance\" & bail_type _
          & "\" & folder_name2 & "\"
            If IO.Directory.Exists(path) = False Then
                'write error message
                error_message = "No folder found for " & folder_name & " (bail id = " & bail_id & ")" & vbNewLine
                write_error()
                Exit Sub
            End If
        End If
        Dim fname As String = path & "RA778 Complaints against bailiffs.pdf"
        RA778Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, fname)

    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub


    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


    End Sub

    
    Private Sub mainbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mainbtn.Click
        saved_bail_id = -1
        maintain.ShowDialog()
        Dim idx As Integer
        For idx = 1 To new_rows - orig_no_rows
            Dim idx2 As Integer = orig_no_rows + idx - 1
            Dim bail_id As Integer
            Try
                bail_id = maintain.DataGridView1.Rows(idx2).Cells(0).Value
            Catch ex As Exception
                Continue For
            End Try
            If bail_id > 0 Then
                Dim folder_name As String = maintain.DataGridView1.Rows(idx2).Cells(2).Value
                Dim bail_type As String = maintain.DataGridView1.Rows(idx2).Cells(3).Value
                If Microsoft.VisualBasic.Left(UCase(bail_type), 1) = "F" Then
                    bail_type = "FC"
                Else
                    bail_type = "Van"
                End If
                Try
                    maintain.Bailiff_ComplaintsTableAdapter.InsertQuery(bail_id, folder_name, bail_type)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End If
        Next
    End Sub
End Class

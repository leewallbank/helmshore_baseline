Public Class mainfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        testbtn.Enabled = False
        prodbtn.Enabled = False
        invbtn.Enabled = False
        Dim date_string As String
        start_year = Year(Now)
        If Microsoft.VisualBasic.DateAndTime.Day(Now) < 20 Then
            If Month(Now) = 1 Then
                start_year -= 1
                start_month = 1
            Else
                start_month = Month(Now) - 1
            End If
            date_string = Year(Now) & " " & Month(Now) & " " & 1
            end_date = CDate(date_string)
            end_date = DateAdd(DateInterval.Day, -1, end_date)
        Else
            start_month = Month(Now)
            end_date = DateAdd(DateInterval.Day, -1, Now)
        End If
        date_string = start_year & " " & start_month & " " & 1
        start_date = CDate(date_string)
        start_date = "28.09.2009"
        datelbl.Text = "Run for Month " & Format(end_date, "MMM yyyy")

    End Sub

    Private Sub datebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles datebtn.Click
        Try
            start_date = CDate(InputBox("Enter start date (dd.mm.yyyy)", "Change start date"))
        Catch ex As Exception
            MessageBox.Show("Invalid date")
            Exit Sub
        End Try
        testbtn.Enabled = False
        datelbl.Text = "Run for Month " & Format(start_date, "MMM yyyy")
        start_month = Month(end_date)
        start_year = Year(start_date)
    End Sub

    Private Sub workbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles workbtn.Click
        testbtn.Enabled = False
        prodbtn.Enabled = False
        invbtn.Enabled = False
        daysfrm.Text = "Working days for " & Format(end_date, "MMM yyyy")
        daysfrm.ShowDialog()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dispbtn.Click
        ProgressBar1.Visible = True
        commfrm.ShowDialog()
        ProgressBar1.Visible = False
        testbtn.Enabled = True
        prodbtn.Enabled = True

    End Sub

    Private Sub testbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles testbtn.Click

        Dim oWSH = CreateObject("WScript.Shell")
        Dim userid As String = oWSH.ExpandEnvironmentStrings("%USERNAME%")

        'update deficit commissions
        'first get next start month and year
        Dim next_month, next_year As Integer
        If start_month = 12 Then
            next_month = 1
            next_year = start_year + 1
        Else
            next_month = start_month + 1
            next_year = start_year
        End If
        Dim idx, bail_id As Integer
        Dim bail_deficit_cf As Decimal

        'get vat rate
        param1 = "onestep"
        param2 = "select fee_vatrate from ClientScheme where _rowid=45"
        Dim cs_dataset As DataSet = get_dataset(param1, param2)
        vat_rate = cs_dataset.Tables(0).Rows(0).Item(0)

        For idx = 0 To max_bail_no
            bail_id = bail_array(idx).bail_id
            bail_deficit_cf = bail_array(idx).deficit_cf
            Dim sage_acc As String = bail_array(idx).sage_acc
            'check if already exists on table
            daysfrm.Employed_bailiffsTableAdapter.FillBy(daysfrm.Employed_bailiffsDataSet.Employed_bailiffs, bail_id, next_month, next_year)
            Dim bail_days As Integer
            Try
                bail_days = daysfrm.Employed_bailiffsDataSet.Employed_bailiffs.Rows(0).Item(3)
                daysfrm.Employed_bailiffsTableAdapter.UpdateQuery1(bail_id, next_month, next_year, bail_deficit_cf)
            Catch ex2 As Exception
                daysfrm.Employed_bailiffsTableAdapter.InsertQuery(bail_id, next_month, next_year, 20, bail_deficit_cf)
            End Try

            'store invoice details
            Dim inv_no As String = Format(Now, "yyyyMMdd") & Format(bail_id, "0") & "EB/"
            Dim amt_payable As Decimal = bail_array(idx).amt_payable
            If amt_payable <= 0 Then
                Continue For
            End If
            BailiffInvoicesTableAdapter.InsertQuery(inv_no, bail_id, Now, Format(amt_payable, "#0.00"), 0, Now, userid, sage_acc)
            'get invoice number
            BailiffInvoicesTableAdapter.FillBy(TESTFeesSQLDataSet.BailiffInvoices, inv_no)
            Dim inv_seq As Integer = TESTFeesSQLDataSet.BailiffInvoices.Rows(0).Item(0)
            inv_no = inv_no & inv_seq
            BailiffInvoicesTableAdapter.UpdateQuery(inv_no, inv_seq)
            'save inv no in bailiff array
            bail_array(idx).inv_no = inv_no

            'now update fee payments table
            Dim idx2 As Integer
            For idx2 = 0 To tot_pif
                If case_array(idx2).bail_id = bail_id Then
                    Dim debtor As Integer = case_array(idx2).debtor
                    Dim swp_comm As Decimal = case_array(idx2).swp_comm
                    Dim date_allocated As Date = case_array(idx2).date_allocated
                    Dim date_visited As Date = case_array(idx2).date_visited
                    If swp_comm > 0 Then
                        insert_test_payment(userid, inv_no, inv_seq, bail_id, debtor, swp_comm, 5, date_allocated, date_visited)
                    End If
                    Dim levy_comm As Decimal = case_array(idx2).levy_comm
                    If levy_comm > 0 Then
                        insert_test_payment(userid, inv_no, inv_seq, bail_id, debtor, levy_comm, 1, date_allocated, date_visited)
                    End If
                    Dim visit1_comm As Decimal = case_array(idx2).visit1_comm
                    If visit1_comm > 0 Then
                        insert_test_payment(userid, inv_no, inv_seq, bail_id, debtor, visit1_comm, 2, date_allocated, date_visited)
                    End If
                    Dim visit2_comm As Decimal = case_array(idx2).visit2_comm
                    If visit2_comm > 0 Then
                        insert_test_payment(userid, inv_no, inv_seq, bail_id, debtor, visit2_comm, 3, date_allocated, date_visited)
                    End If
                    Dim van_fees As Decimal = case_array(idx2).el_van_fees
                    Dim van_comm As Decimal = van_fees * bail_array(idx).percent
                    'If van_comm > 0 Then
                    Dim disp_bail_pcent As Decimal = Format(bail_array(idx).percent * 100, "#0.00")
                    insert_test_payment2(userid, inv_no, inv_seq, bail_id, debtor, Format(van_comm, "#0.00"), 12, disp_bail_pcent, 1, date_allocated, date_visited)
                    'End If
                End If
            Next
        Next
        MessageBox.Show("Updates completed")
        invbtn.Enabled = True
    End Sub
    Sub insert_test_payment(ByVal userid As String, ByVal inv_no As String, ByVal inv_seq As Integer, ByVal bail_id As Integer, _
        ByVal debtor As Integer, ByVal comm As Decimal, ByVal payment_typeid As Integer, ByVal date_allocated As Date, ByVal date_visited As Date)
        'get amount already paid
        Dim already_paid As Integer
        param1 = "Fees"
        param2 = "select sum(PaymentAmount) from BailiffPayments where CaseId = " & debtor & _
        " and PaymentTypeID = " & payment_typeid
        Dim feepay_dataset As DataSet = get_dataset(param1, param2)
        Try
            already_paid = feepay_dataset.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            already_paid = 0
        End Try
        'insert payment into table
        If already_paid = 0 Then
            BailiffPaymentsTableAdapter.InsertQuery(debtor, bail_id, 0, payment_typeid, comm, date_visited, date_allocated, Now, inv_no, Now, Now, userid, inv_seq, " ", 0, vat_rate)
        End If
    End Sub

    Sub insert_test_payment2(ByVal userid As String, ByVal inv_no As String, ByVal inv_seq As Integer, ByVal bail_id As Integer, _
        ByVal debtor As Integer, ByVal comm As Decimal, ByVal payment_typeid As Integer, ByVal percent As Decimal, ByVal points As Decimal, ByVal date_allocated As Date, ByVal date_visited As Date)
        'get amount already paid
        Dim already_paid As Integer
        param1 = "Fees"
        param2 = "select sum(PaymentAmount) from BailiffPayments where CaseId = " & debtor & _
        " and PaymentTypeID = " & payment_typeid
        Dim feepay_dataset As DataSet = get_dataset(param1, param2)
        Try
            already_paid = feepay_dataset.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            already_paid = 0
        End Try
        'insert payment into table
        If already_paid = 0 Then
            BailiffPaymentsTableAdapter.InsertQuery1(debtor, bail_id, 0, payment_typeid, comm, date_visited, date_allocated, Now, inv_no, Now, Now, userid, inv_seq, " ", vat_rate, Format(percent, "#0.00"), points)
        End If
    End Sub

    Sub insert_prod_payment(ByVal userid As String, ByVal inv_no As String, ByVal inv_seq As Integer, ByVal bail_id As Integer, _
            ByVal debtor As Integer, ByVal comm As Decimal, ByVal payment_typeid As Integer, ByVal date_allocated As Date, ByVal date_visited As Date)
        'get amount already paid
        Dim already_paid As Integer
        param1 = "Fees"
        param2 = "select sum(PaymentAmount) from BailiffPayments where CaseId = " & debtor & _
        " and PaymentTypeID = " & payment_typeid
        Dim feepay_dataset As DataSet = get_dataset(param1, param2)
        Try
            already_paid = feepay_dataset.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            already_paid = 0
        End Try
        'insert payment into table
        If already_paid = 0 Then
            BailiffPaymentsTableAdapter1.InsertQuery(debtor, bail_id, 0, payment_typeid, comm, date_visited, date_allocated, Now, inv_no, Now, Now, userid, inv_seq, " ", 0, vat_rate)
        End If
    End Sub

    Sub insert_prod_payment2(ByVal userid As String, ByVal inv_no As String, ByVal inv_seq As Integer, ByVal bail_id As Integer, _
            ByVal debtor As Integer, ByVal comm As Decimal, ByVal payment_typeid As Integer, ByVal percent As Decimal, ByVal points As Decimal, ByVal date_allocated As Date, ByVal date_visited As Date)
        'get amount already paid
        Dim already_paid As Integer
        param1 = "Fees"
        param2 = "select sum(PaymentAmount) from BailiffPayments where CaseId = " & debtor & _
        " and PaymentTypeID = " & payment_typeid
        Dim feepay_dataset As DataSet = get_dataset(param1, param2)
        Try
            already_paid = feepay_dataset.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            already_paid = 0
        End Try
        'insert payment into table
        If already_paid = 0 Then
            BailiffPaymentsTableAdapter1.InsertQuery1(debtor, bail_id, 0, payment_typeid, comm, date_visited, date_allocated, Now, inv_no, Now, Now, userid, inv_seq, " ", vat_rate, Format(percent, "#0.00"), points)
        End If
    End Sub
    Private Sub prodbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles prodbtn.Click
        If MessageBox.Show("WARNING This can only be done once", "UPDATE employed bailiff commissions", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Cancel Then
            Exit Sub
        End If
        invbtn.Enabled = True
        Dim oWSH = CreateObject("WScript.Shell")
        Dim userid As String = oWSH.ExpandEnvironmentStrings("%USERNAME%")

        'update deficit commissions
        'first get next start month and year
        Dim next_month, next_year As Integer
        If start_month = 12 Then
            next_month = 1
            next_year = start_year + 1
        Else
            next_month = start_month + 1
            next_year = start_year
        End If
        Dim idx, bail_id As Integer
        Dim bail_deficit_cf As Decimal

        'get vat rate
        param1 = "onestep"
        param2 = "select fee_vatrate from ClientScheme where _rowid=45"
        Dim cs_dataset As DataSet = get_dataset(param1, param2)
        vat_rate = cs_dataset.Tables(0).Rows(0).Item(0)

        For idx = 0 To max_bail_no
            bail_id = bail_array(idx).bail_id
            bail_deficit_cf = bail_array(idx).deficit_cf
            Dim sage_acc As String = bail_array(idx).sage_acc
            'check if already exists on table
            daysfrm.Employed_bailiffsTableAdapter.FillBy(daysfrm.Employed_bailiffsDataSet.Employed_bailiffs, bail_id, next_month, next_year)
            Dim bail_days As Integer
            Try
                bail_days = daysfrm.Employed_bailiffsDataSet.Employed_bailiffs.Rows(0).Item(3)
                daysfrm.Employed_bailiffsTableAdapter.UpdateQuery1(bail_id, next_month, next_year, bail_deficit_cf)
            Catch ex2 As Exception
                daysfrm.Employed_bailiffsTableAdapter.InsertQuery(bail_id, next_month, next_year, 20, bail_deficit_cf)
            End Try

            'store invoice details
            Dim inv_no As String = Format(Now, "yyyyMMdd") & Format(bail_id, "0") & "EB/"
            Dim amt_payable As Decimal = bail_array(idx).amt_payable
            'If amt_payable <= 0 Then
            '    Continue For
            'End If
            BailiffInvoicesTableAdapter1.InsertQuery(inv_no, bail_id, Now, Format(amt_payable, "#0.00"), 0, Now, userid, sage_acc)
            'get invoice number
            BailiffInvoicesTableAdapter1.FillBy(FeesSQLDataSet.BailiffInvoices, inv_no)
            Dim inv_seq As Integer = FeesSQLDataSet.BailiffInvoices.Rows(0).Item(0)
            inv_no = inv_no & inv_seq
            'BailiffInvoicesTableAdapter1.UpdateQuery(inv_no, inv_seq)

            'save inv no in bailiff array
            bail_array(idx).inv_no = inv_no

            'now update fee payments table
            Dim idx2 As Integer
            For idx2 = 0 To tot_pif
                If case_array(idx2).bail_id = bail_id Then
                    Dim debtor As Integer = case_array(idx2).debtor
                    Dim swp_comm As Decimal = case_array(idx2).swp_comm
                    Dim date_allocated As Date = case_array(idx2).date_allocated
                    Dim date_visited As Date = case_array(idx2).date_visited
                    If swp_comm > 0 Then
                        insert_prod_payment(userid, inv_no, inv_seq, bail_id, debtor, swp_comm, 5, date_allocated, date_visited)
                    End If
                    Dim levy_comm As Decimal = case_array(idx2).levy_comm
                    If levy_comm > 0 Then
                        insert_prod_payment(userid, inv_no, inv_seq, bail_id, debtor, levy_comm, 1, date_allocated, date_visited)
                    End If
                    Dim visit1_comm As Decimal = case_array(idx2).visit1_comm
                    If visit1_comm > 0 Then
                        insert_prod_payment(userid, inv_no, inv_seq, bail_id, debtor, visit1_comm, 2, date_allocated, date_visited)
                    End If
                    Dim visit2_comm As Decimal = case_array(idx2).visit2_comm
                    If visit2_comm > 0 Then
                        insert_prod_payment(userid, inv_no, inv_seq, bail_id, debtor, visit2_comm, 3, date_allocated, date_visited)
                    End If
                    Dim van_fees As Decimal = case_array(idx2).el_van_fees
                    Dim van_comm As Decimal = van_fees * bail_array(idx).percent
                    'If van_comm > 0 Then
                    Dim disp_bail_pcent As Decimal = Format(bail_array(idx).percent * 100, "#0.00")
                    insert_prod_payment2(userid, inv_no, inv_seq, bail_id, debtor, Format(van_comm, "#0.00"), 12, disp_bail_pcent, 1, date_allocated, date_visited)
                    'End If
                End If
            Next
        Next
        MessageBox.Show("Updates completed")
    End Sub

    Private Sub invbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles invbtn.Click
        open_print_form()
    End Sub
End Class

Public Class commfrm

    Private Sub commfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'get all impact bailiffs
        param1 = "onestep"
        param2 = "select _rowid, name_sur, name_fore from Bailiff where agent_type = 'B' " & _
        " and _rowid = 2606 and internalExternal = 'E' and status = 'O' and left(typeSub,6) = 'Impact' order by name_sur"
        Dim bail_dataset As DataSet = get_dataset(param1, param2)
        Dim bail_name As String = ""
        Dim bail_id As Integer
        'DataGridView1.Rows.Clear()
        Dim bidx As Integer = 0
        Dim bailiff_rows As Integer = no_of_rows
        bail_no = 0
        max_bail_no = 0
        tot_pif = 0
        Dim max_rows As Integer = 1000
        ReDim doc_array(no_of_rows)
        ReDim bail_array(no_of_rows)
        ReDim case_array(max_rows)
        For Each row In bail_dataset.Tables(0).Rows
            doctext.Text = "                           Employed Bailiff Commission Run for Month " & Format(end_date, "MMM yyyy") & vbNewLine & vbNewLine
            mainfrm.ProgressBar1.Value = bidx / bailiff_rows * 100
            Dim pif As Integer = 0
            Dim tot_levy_comm As Decimal = 0
            Dim tot_van_fees As Decimal = 0
            Dim tot_unpaid_van As Decimal = 0
            Dim tot_unpaid_van_comm As Decimal = 0
            Dim tot_unpaid_swp As Decimal = 0
            Dim tot_unpaid_levy As Decimal = 0
            Dim tot_unpaid_visit1 As Decimal = 0
            Dim tot_unpaid_visit2 As Decimal = 0
            Dim first_visit_no As Integer = 0
            Dim second_visit_no As Integer = 0
            Dim tot_first_visit_comm As Decimal = 0
            Dim tot_second_visit_comm As Decimal = 0
            Dim swp_no As Integer = 0
            Dim tot_swp_comm As Decimal = 0
            Dim bail_deficit_cf As Decimal = 0
            Dim tot_already_paid As Decimal = 0
            bail_id = bail_dataset.Tables(0).Rows(bidx).Item(0)
            bail_array(bail_no).bail_id = bail_id

            bail_name = Trim(bail_dataset.Tables(0).Rows(bidx).Item(2)) & " " & Trim(bail_dataset.Tables(0).Rows(bidx).Item(1))
            bail_array(bail_no).bail_name = bail_name
            bail_array(bail_no).sage_acc = "vb" & LCase(Microsoft.VisualBasic.Left(bail_dataset.Tables(0).Rows(bidx).Item(1), 4) & _
                Microsoft.VisualBasic.Left(bail_dataset.Tables(0).Rows(bidx).Item(2), 1))
            bidx += 1
            'get number of days and deficit from last month
            param1 = "Employed"
            param2 = "select bail_days, bail_deficit_bf from Employed_bailiffs " & _
            " where bail_id = " & bail_id & " and bail_year = " & start_year & _
            " and bail_month = " & start_month
            Dim emp_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                MessageBox.Show("Last month has not been run yet")
                Me.Close()
                Exit Sub
            End If
            Dim bail_days As Integer = emp_dataset.Tables(0).Rows(0).Item(0)
            Dim bail_deficit_bf As Decimal = emp_dataset.Tables(0).Rows(0).Item(1)
            doctext.Text = doctext.Text & bail_name & " " & Format(bail_id, "#") & _
                        "   Number of working days = " & Format(bail_days, "#") & vbNewLine
            Dim net_target As Decimal = bail_days * 200
            Dim target As Decimal = net_target + bail_deficit_bf
            bail_array(bail_no).deficit_bf = bail_deficit_bf
            Dim target2 As Decimal = (bail_days * 351) + bail_deficit_bf
            doctext.Text = doctext.Text & "Target commission " & bail_days & " days @ �200 = " & _
            Format(net_target, "�#0.00")
            doctext.Text = doctext.Text & "    Plus deficit B/F of " & Format(bail_deficit_bf, "�#0.00") & _
            "  = " & Format(target, "�#0.00") & vbNewLine & vbNewLine

            'get all visits made 30 days before start of last month
            Dim earliest_visit As Date = DateAdd(DateInterval.Day, -30, start_date)
            param1 = "onestep"
            param2 = "select debtorID, date_visited from Visit " & _
            "where  bailiffID = " & bail_id & _
            " and date_visited > '" & Format(earliest_visit, "yyyy.MM.dd") & "'" & _
            " order by debtorID, date_visited desc"
            Dim visit_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim debtorid As Integer = 0
            Dim row2 As DataRow
            Dim vidx As Integer = 0
            Dim date_visited As Date
            Dim last_debtorid As Integer = 0
            For Each row2 In visit_dataset.Tables(0).Rows
                debtorid = visit_dataset.Tables(0).Rows(vidx).Item(0)
                date_visited = visit_dataset.Tables(0).Rows(vidx).Item(1)
                vidx += 1
                If debtorid = last_debtorid Then
                    Continue For
                End If
                'check debtor status is fully paid/successful
                param1 = "onestep"
                param2 = "select _rowid from Debtor " & _
                "where _rowid = " & debtorid & " and (status = 'S' or status = 'F')"
                Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 0 Then
                    Continue For
                End If
                last_debtorid = debtorid
                Dim levy_fees As Decimal = 0
                Dim levy_comm As Decimal = 0
                Dim other_fc_fees As Decimal = 0
                Dim van_fees As Decimal = 0
                Dim el_van_fees As Decimal = 0
                Dim already_paid As Decimal = 0
                Dim swp_comm As Decimal = 0
                Dim visit1_comm As Decimal = 0
                Dim visit2_comm As Decimal = 0
                Dim case_van_status As String = " "
                'ignore if there is a later visit date
                param1 = "onestep"
                param2 = "select max(date_visited) from Visit where debtorID = " & debtorid & _
                " and date_visited is not null "
                Dim visit3_dataset As DataSet = get_dataset(param1, param2)
                Dim last_date_visited As Date
                Try
                    last_date_visited = visit3_dataset.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    Continue For
                End Try
                If Format(last_date_visited, "yyyy.MM.dd") <> Format(date_visited, "yyyy.MM.dd") Then
                    Continue For
                End If

                'ignore if a payment date > end date exists 
                'include waiting except cheque with clear date > end date 
                param1 = "onestep"
                param2 = "select debtorID, amount_typeID, status_date from Payment where debtorID = " & debtorid & _
                " and (status = 'R' or status = 'W') " & _
                " and (amount_typeID = 1 or date > '" & Format(end_date, "yyyy.MM.dd") & "')"
                Dim payment2_dataset As DataSet = get_dataset(param1, param2)
                Dim idx As Integer = 0
                While idx < no_of_rows
                    Dim typeid As Integer = payment2_dataset.Tables(0).Rows(idx).Item(1)
                    If typeid = 1 Then
                        Dim st_date As Date = payment2_dataset.Tables(0).Rows(idx).Item(2)
                        If st_date > end_date Or debtorid = 5006200 Then
                            Continue For
                        End If
                    End If
                    idx += 1
                End While
                
                'check a payment date was last month 
                param1 = "onestep"
                param2 = "select status_date, amount, date from Payment " & _
                    " where debtorID = " & debtorid & " and " & _
                    " (status = 'W' or status = 'R') " & _
                    " order by date desc"
                Dim payment_dataset As DataSet = get_dataset(param1, param2)
                Dim payment_date, visit_date30 As Date
                Dim pidx As Integer
                Try
                    payment_date = payment_dataset.Tables(0).Rows(0).Item(2)
                Catch ex As Exception
                    Continue For
                End Try
                If payment_date < start_date Or payment_date > end_date Then
                    Continue For
                End If

                'get latest status/payment date (ignore if payment amount is zero)
                For pidx = 0 To no_of_rows - 1
                    If payment_dataset.Tables(0).Rows(pidx).Item(1) = 0 Then
                        Continue For
                    End If
                    payment_date = payment_dataset.Tables(0).Rows(pidx).Item(2)
                    visit_date30 = DateAdd("d", +30, date_visited)
                    Exit For
                Next

                If payment_date > visit_date30 Then
                    Continue For
                End If

                'get latest allocation date for debtor
                param1 = "onestep"
                param2 = "select max(date_allocated) from Visit where debtorID = " & debtorid & _
                " and bailiffID = " & bail_id

                Dim visita_dataset As DataSet = get_dataset(param1, param2)
                Dim date_allocated As Date
                Try
                    date_allocated = visita_dataset.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    Continue For
                End Try

                'get fees added
                'include all van fees
                param1 = "onestep"
                param2 = "select _rowid, fee_amount,type,fee_remit_col from Fee where debtorID = " & debtorid & _
                " and (bailiffID = " & bail_id & " or (bailiffID is not null and fee_remit_col = 4))  and fee_amount <> 0"
                Dim fee_dataset As DataSet = get_dataset(param1, param2)
                Dim row3 As DataRow
                Dim fidx As Integer = 0
                For Each row3 In fee_dataset.Tables(0).Rows
                    If fee_dataset.Tables(0).Rows(fidx).Item(3) <> 3 And _
                    fee_dataset.Tables(0).Rows(fidx).Item(3) <> 4 Then
                        fidx += 1
                        Continue For
                    End If
                    'check that fee was remitted after allocation date
                    Dim feeid As Integer = fee_dataset.Tables(0).Rows(fidx).Item(0)
                    'param1 = "Fees"
                    'param2 = "select sum(Amount) from FeePayments where feeId = " & feeid '& _
                    ''" and RemitedDate >='" & Format(date_allocated, "yyyy.MM.dd") & "'"
                    'Dim feepayments_datset As DataSet = get_dataset(param1, param2)
                    Dim fees As Decimal
                    Try
                        fees = fee_dataset.Tables(0).Rows(fidx).Item(1)
                    Catch
                        fidx += 1
                        Continue For
                    End Try
                    Dim fee_type As String = LCase(fee_dataset.Tables(0).Rows(fidx).Item(2))
                    If InStr(fee_type, "levy") > 0 Then
                        levy_fees = fees
                        'see if levy fees already paid
                        If get_paid(debtorid, 1) > 0 Then
                            fidx += 1
                            Continue For
                        End If
                        If levy_fees > 33 Then
                            tot_levy_comm += 10
                            levy_comm = 10
                        Else
                            levy_comm = (levy_fees * 0.3)
                            tot_levy_comm = tot_levy_comm + levy_comm
                        End If
                    ElseIf fee_dataset.Tables(0).Rows(fidx).Item(3) = 3 Then
                        other_fc_fees += fees
                        If InStr(fee_type, "visit") > 0 Then
                            If InStr(fee_type, "1") > 0 Then
                                If get_paid(debtorid, 2) = 0 Then
                                    tot_first_visit_comm += 3
                                    visit1_comm = 3
                                    tot_unpaid_visit1 += 3
                                    first_visit_no += 1
                                End If
                            ElseIf InStr(fee_type, "2") > 0 Then
                                If get_paid(debtorid, 3) = 0 Then
                                    second_visit_no += 1
                                    tot_second_visit_comm += 3.5
                                    visit2_comm = 3.5
                                    tot_unpaid_visit1 += 3.5
                                End If
                            End If
                        ElseIf InStr(fee_type, "walk") > 0 Then
                            If get_paid(debtorid, 5) = 0 Then
                                swp_no += 1
                                tot_swp_comm += 2
                                swp_comm = 2
                                tot_unpaid_swp += 2
                            End If
                        End If
                    ElseIf fee_dataset.Tables(0).Rows(fidx).Item(3) = 4 Then
                        Dim case_van_fees As Decimal = fees
                        van_fees += case_van_fees
                        tot_van_fees += case_van_fees
                        'see if van fees already paid
                        If get_paid(debtorid, 13) = 0 Then
                            If get_paid(debtorid, 12) > 0 Then
                                el_van_fees += case_van_fees
                                case_van_status = "P"
                            Else
                                tot_unpaid_van += case_van_fees
                                el_van_fees += case_van_fees
                                case_van_status = "U"
                            End If
                        Else
                            If get_paid(debtorid, 12) > 0 Then
                                el_van_fees += case_van_fees
                                case_van_status = "P"
                            Else
                                tot_unpaid_van += case_van_fees / 2
                                el_van_fees += case_van_fees / 2
                                case_van_status = "H"
                            End If
                        End If
                        'If get_paid(debtorid, 12) = 0 Then
                        '    If get_paid(debtorid, 13) > 0 Then
                        '        tot_unpaid_van += case_van_fees / 2
                        '        el_van_fees += case_van_fees / 2
                        '        case_van_status = "H"
                        '    Else
                        '        tot_unpaid_van += case_van_fees
                        '        el_van_fees += case_van_fees
                        '        case_van_status = "U"
                        '    End If
                        'Else
                        '    el_van_fees += case_van_fees
                        '    case_van_status = "P"
                        'End If
                    End If
                    fidx += 1

                Next
                'get amount already paid
                'param1 = "Fees"
                'param2 = "select sum(PaymentAmount) from BailiffPayments where CaseId = " & debtorid & _
                '" and (PaymentTypeID = 12 or PaymentTypeId = 13)"
                'Dim feepay2_dataset As DataSet = get_dataset(param1, param2)
                'Try
                '    already_paid += feepay2_dataset.Tables(0).Rows(0).Item(0)
                '    tot_already_paid += feepay2_dataset.Tables(0).Rows(0).Item(0)
                'Catch ex As Exception

                'End Try
                Dim debtor As String = debtorid

                'store values in table for display later
                case_array(tot_pif).bail_id = bail_id
                case_array(tot_pif).debtor = debtor
                case_array(tot_pif).van_fees = van_fees
                case_array(tot_pif).el_van_fees = el_van_fees
                case_array(tot_pif).levy_fees = levy_fees
                case_array(tot_pif).levy_comm = levy_comm
                case_array(tot_pif).visit1_comm = visit1_comm
                case_array(tot_pif).visit2_comm = visit2_comm
                case_array(tot_pif).swp_comm = swp_comm
                case_array(tot_pif).date_allocated = date_allocated
                case_array(tot_pif).date_visited = date_visited
                If van_fees = 0 Then
                    case_van_status = "Z"
                End If
                case_array(tot_pif).status = case_van_status
                pif += 1
                tot_pif += 1
            Next

            Dim avg_daily_comm As Decimal = tot_van_fees / bail_days
            Dim basic_comm As Decimal = 0
            Dim excess_comm As Decimal = 0
            Dim basic_rate As Decimal = 0.35
            If tot_van_fees < target Then
                bail_deficit_cf = target - tot_van_fees
            Else
                excess_comm = tot_van_fees - target
                If tot_van_fees >= target2 Then
                    basic_rate = 0.45
                End If
                basic_comm = excess_comm * basic_rate
            End If
            Dim kicker_rate As Decimal = 0
            If pif >= +60 And tot_van_fees >= (525 * bail_days) + bail_deficit_bf Then
                kicker_rate = 10
            ElseIf pif >= 50 And tot_van_fees >= (425 * bail_days) + bail_deficit_bf Then
                kicker_rate = 5
            ElseIf pif >= 40 And tot_van_fees >= (325 * bail_days) + bail_deficit_bf Then
                kicker_rate = 2.5
            End If

            Dim kicker_comm As Decimal = kicker_rate * (avg_daily_comm - 200) / 100
            Dim tot_fc_comm As Decimal = tot_first_visit_comm + tot_second_visit_comm + tot_swp_comm + tot_levy_comm
            If bail_deficit_cf = 0 Then
                tot_unpaid_van_comm = tot_unpaid_van * basic_rate
            End If
            If tot_unpaid_van_comm > basic_comm + kicker_comm Then
                tot_unpaid_van_comm = basic_comm + kicker_comm
            End If
            Dim amt_payable As Decimal = tot_fc_comm + tot_unpaid_van_comm
            If amt_payable < 0 Then
                amt_payable = 0
            End If
            doctext.Text = doctext.Text & "Total PIF's = " & pif & vbNewLine
            doctext.Text = doctext.Text & "Total Commissionable van fees = " & Format(tot_van_fees, "�#0.00")
            doctext.Text = doctext.Text & "     Total unpaid van fees = " & Format(tot_unpaid_van, "�#0.00") & vbNewLine
            'doctext.Text = doctext.Text & "Average daily commission earned = " & Format(avg_daily_comm, "�#0.00")
            doctext.Text = doctext.Text & "Deficit C/F = " & Format(bail_deficit_cf, "�#0.00") & vbNewLine & vbNewLine
            doctext.Text = doctext.Text & "Basic commission on " & _
            Format(excess_comm, "�#0.00") & " @ " & Format(basic_rate, "#0.00%") & " = " & Format(basic_comm, "�#0.00")
            doctext.Text = doctext.Text & "         Kicker commission @ " & Format(kicker_rate, "#0.0%") & _
            " = " & Format(kicker_comm, "�#0.00") & vbNewLine
            doctext.Text = doctext.Text & "Total van commission earned = " & _
            Format(basic_comm + kicker_comm, "�#0.00")

            doctext.Text = doctext.Text & "       Total unpaid van commission = " & Format(tot_unpaid_van_comm, "�#0.00") & vbNewLine & vbNewLine
            doctext.Text = doctext.Text & "FC FEES" & vbNewLine
            doctext.Text = doctext.Text & "     No of first visit fees = " & Format(first_visit_no, "#0")
            doctext.Text = doctext.Text & "     First visit commission earned = " & Format(tot_first_visit_comm, "�#0.00") & vbNewLine
            doctext.Text = doctext.Text & "     No of second visit fees = " & Format(second_visit_no, "#0")
            doctext.Text = doctext.Text & "     Second visit commission earned = " & Format(tot_second_visit_comm, "�#0.00") & vbNewLine
            doctext.Text = doctext.Text & "     No of SWP fees = " & Format(swp_no, "#0")
            doctext.Text = doctext.Text & "     SWP commission earned = " & Format(tot_swp_comm, "�#0.00") & vbNewLine
            doctext.Text = doctext.Text & "     Levy commission earned  = " & Format(tot_levy_comm, "�#0.00") & vbNewLine
            doctext.Text = doctext.Text & "     Total FC commission earned = " & Format(tot_fc_comm, "�#0.00") & vbNewLine & vbNewLine
            doctext.Text = doctext.Text & "Total Amount payable = " & Format(amt_payable, "�#0.00") & vbNewLine

            doc_array(bail_no) = doctext.Text
            doctext.Text = ""
            bail_array(bail_no).pif = pif
            bail_array(bail_no).amt_payable = amt_payable
            bail_array(bail_no).deficit_cf = bail_deficit_cf
            If tot_unpaid_van = 0 Then
                bail_array(bail_no).percent = 0
            Else
                bail_array(bail_no).percent = tot_unpaid_van_comm / tot_unpaid_van
            End If
            bail_array(bail_no).inv_no = 0
            max_bail_no = bail_no
            bail_no += 1
        Next
        doctext.Text = doc_array(0)
        bail_no = 0
    End Sub
    Private Function get_paid(ByVal debtorid As Integer, ByVal paymentid As Integer) As Decimal
        'get amount already paid
        Dim ret_paid As Decimal
        param1 = "Fees"
        param2 = "select sum(PaymentAmount) from BailiffPayments where CaseId = " & debtorid & _
        " and PaymentTypeID = " & paymentid
        Dim feepay_dataset As DataSet = get_dataset(param1, param2)
        Try
            ret_paid = feepay_dataset.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            ret_paid = 0
        End Try
        Return (ret_paid)
    End Function
    Private Sub printbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles printbtn.Click
        open_print_form()
    End Sub
    

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub nextbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nextbtn.Click
        If bail_no >= max_bail_no Then
            pagelbl.Text = "No more bailiffs"
            Exit Sub
        End If
        pagelbl.Text = ""
        bail_no += 1
        doctext.Text = doc_array(bail_no)
    End Sub

    Private Sub prevbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles prevbtn.Click
        If bail_no = 0 Then
            pagelbl.Text = "At first bailiff"
            Exit Sub
        End If
        pagelbl.Text = ""
        bail_no -= 1
        doctext.Text = doc_array(bail_no)
    End Sub

    Private Sub casesbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles casesbtn.Click
        casesfrm.ShowDialog()
    End Sub

    Private Sub printallbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        open_print_form()
    End Sub
End Class
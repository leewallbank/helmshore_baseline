Public Class mainform
    Dim debtorID As Integer
   

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Excel files | *.xls"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                load_vals(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        exitbtn.Enabled = False
        openbtn.Enabled = False
        Dim idx As Integer
        Dim notes As String = ""

        'write out headings
        outfile = "DebtorID,AllocationDate" & vbNewLine
        For idx = 0 To finalrow
            ProgressBar1.Value = (idx / finalrow) * 100
            Try
                debtorID = vals(idx, 3)
            Catch ex As Exception
                Continue For
            End Try
            If debtorID = 0 Then
                Continue For
            End If
            param2 = "select bail_allocated from Debtor where _rowid =  " & debtorID
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to find debtor ID = " & debtorID)
                Continue For
            End If
            Dim bail_allocated As Date
            Try
                bail_allocated = CDate(debtor_dataset.Tables(0).Rows(0).Item(0))
            Catch ex As Exception
                bail_allocated = Nothing
            End Try
            Dim created_date As Date = Nothing
            Dim note_found As Boolean = False
            If bail_allocated <> Nothing Then
                Dim end_date As Date = DateAdd(DateInterval.Day, 1, bail_allocated)
                param2 = "select text, _createdDate from Note where debtorID = " & debtorID & _
                " and type = 'Note' and _createdDate >= '" & Format(bail_allocated, "yyyy-MM-dd") & "'" & _
                " and _createdDate < '" & Format(end_date, "yyyy-MM-dd") & "'"
                Dim note_dataset As DataSet = get_dataset("onestep", param2)
                Dim idx2 As Integer
                For idx2 = 0 To no_of_rows - 1
                    Dim text As String = note_dataset.Tables(0).Rows(idx2).Item(0)
                    If Microsoft.VisualBasic.Left(text, 7) = "Bailiff" Then
                        note_found = True
                        If created_date = Nothing Then
                            created_date = note_dataset.Tables(0).Rows(idx2).Item(1)
                        Else
                            If created_date < note_dataset.Tables(0).Rows(idx2).Item(1) Then
                                created_date = note_dataset.Tables(0).Rows(idx2).Item(1)
                            End If
                        End If

                    End If
                Next
            End If
            If note_found Then
                outfile = outfile & debtorID & "," & Format(created_date, "dd/MM/yyyy HH:mm:ss") & vbNewLine
            Else
                outfile = outfile & debtorID & ", " & vbNewLine
            End If
        Next

        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_output.csv", outfile, False)
        MsgBox("report written")
        Me.Close()
    End Sub


    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        errors.Show()
    End Sub
End Class

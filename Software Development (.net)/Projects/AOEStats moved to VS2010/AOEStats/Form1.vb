Public Class Form1
    
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        Try
            Me.AOE_statsTableAdapter.DeleteQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        start_debtor = 0
        process_debtors()
    End Sub
    Private Sub process_debtors()
        'Dim log_file As String
        exitbtn.Enabled = False
        runbtn.Enabled = False
        restartbtn.Enabled = False

        'SaveFileDialog1.FileName = "R:/vb.net/AOEStats/aoe_log.txt"
        'My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, "AOE stats started " & Format(Now, "dd/MM/yyyy hh:mm:ss"), False)
        Dim start_date As Date = CDate("January 1 2009 00:00:00")
        param1 = "onestep"
        param2 = "select  _rowid, debt_amount, debt_costs, status, debt_fees from Debtor where _rowid > " & start_debtor & _
        " and (statusCode = 14 or statusCode = 5) order by _rowid"
        Dim debtor_dataset As DataSet = get_dataset(param1, param2)
        'If no_of_rows = 0 Then
        '    MsgBox("No AOE found")
        '    Exit Sub
        'Else
        '    MsgBox("Rows found = " & no_of_rows)
        'End If
        ProgressBar1.Value = 5
        Dim debtor_rows As Integer = no_of_rows - 1
        Dim idx, debtorID As Integer
        For idx = 0 To debtor_rows
            ProgressBar1.Value = (idx / (debtor_rows + 1)) * 100
            Application.DoEvents()
            debtorID = debtor_dataset.Tables(0).Rows(idx).Item(0)
            
            'get AOE allocation date
            param2 = "select _createdDate from Note where debtorID = " & debtorID & _
            " and (type = 'Note' or type = 'Allocated') and text like 'Admin (AOE%'"
            Dim note_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim created_date As Date = note_dataset.Tables(0).Rows(0).Item(0)
            If created_date < start_date Then
                Continue For
            End If

            Dim tot_debt_amt As Decimal = debtor_dataset.Tables(0).Rows(idx).Item(1) + _
            debtor_dataset.Tables(0).Rows(idx).Item(2) + debtor_dataset.Tables(0).Rows(idx).Item(4)

            'has this case got a visit?
            param2 = "select _rowid from Visit where debtorID = " & debtorID & _
            " and date_visited is not null"
            Dim visit_dataset As DataSet = get_dataset(param1, param2)
            Dim visit_found As Boolean = False
            If no_of_rows > 0 Then
                visit_found = True
            End If
            Dim year_idx As Integer = Year(created_date) - 2009

            'get end date
            Dim idx2 As Integer
            Dim aoe_end_date As Date = CDate("january 1, 2100")
            param2 = "select _createdDate from Note where debtorID = " & debtorID & _
            " and type = 'Removed agent' order by _createdDate"
            Dim note2_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows > 0 Then
                For idx2 = 0 To no_of_rows - 1
                    Dim removed_date As Date = note2_dataset.Tables(0).Rows(idx2).Item(0)
                    If removed_date > created_date Then
                        aoe_end_date = removed_date
                        Exit For
                    End If
                Next
            End If

            'see if another allocation
            Dim allocated_date As Date = CDate("January 1, 2100")
            param2 = "select _createdDate from Note where debtorID = " & debtorID & _
            " and type = 'Note'" & _
            " and (text like 'Phone Operator%' or text like 'Bailiff%') order by _createdDate"
            Dim note3_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows > 0 Then
                For idx2 = 0 To no_of_rows - 1
                    Dim dte = note3_dataset.Tables(0).Rows(idx2).Item(0)
                    If dte > created_date Then
                        allocated_date = dte
                        Exit For
                    End If
                Next
            End If
            If allocated_date < aoe_end_date Then
                aoe_end_date = allocated_date
            End If

            'get payments before allocation
            param2 = "select amount, status, split_debt, split_costs, split_fees, split_other, split_van from Payment" & _
                        " where debtorID = " & debtorID & " and date <= '" & Format(created_date, "yyyy-MM-dd") & "'" & _
                        " and status='R'"
            Dim payment_dataset As DataSet = get_dataset(param1, param2)

            Dim paid2client_before As Decimal = 0
            For idx2 = 0 To no_of_rows - 1
                paid2client_before = paid2client_before + payment_dataset.Tables(0).Rows(idx2).Item(2) + _
                payment_dataset.Tables(0).Rows(idx2).Item(3) + payment_dataset.Tables(0).Rows(idx2).Item(4) + _
                payment_dataset.Tables(0).Rows(idx2).Item(5) + payment_dataset.Tables(0).Rows(idx2).Item(6)
            Next
            Dim aoe_no_payments As Integer = 0
            'get payments since allocation but before end date
            param2 = "select amount, status, split_debt, split_costs, split_fees, split_van, date from Payment" & _
            " where debtorID = " & debtorID & _
            " and (status='R' or status='W')"

            Dim payment2_dataset As DataSet = get_dataset(param1, param2)
            Dim to_client As Decimal = 0
            Dim to_fees As Decimal = 0
            Dim waiting As Decimal = 0
            For idx2 = 0 To no_of_rows - 1
                Dim pay_date As Date = payment2_dataset.Tables(0).Rows(idx2).Item(6)
                If pay_date < created_date Or pay_date > aoe_end_date Then
                    Continue For
                End If
                If payment2_dataset.Tables(0).Rows(idx2).Item(1) = "R" Then
                    to_client = to_client + payment2_dataset.Tables(0).Rows(idx2).Item(2) + payment2_dataset.Tables(0).Rows(idx2).Item(3)
                    to_fees = to_fees + payment2_dataset.Tables(0).Rows(idx2).Item(4) + payment2_dataset.Tables(0).Rows(idx2).Item(5)
                Else
                    waiting += payment2_dataset.Tables(0).Rows(idx2).Item(0)
                End If
                aoe_no_payments += 1
            Next

            'see if anything paid after end date - if so not PIF by AOE
            Dim paid_after As Boolean = False
            If aoe_end_date <> CDate("2100,1,1") Then
                param2 = "select date from Payment" & _
                         " where debtorID = " & debtorID & _
                         " and (status = 'W' or status = 'R') "
                Dim payment3_dataset As DataSet = get_dataset(param1, param2)
                Dim no_of_payments As Integer = no_of_rows
                Dim idx4 As Integer
                For idx4 = 0 To no_of_payments - 1
                    Try
                        If payment3_dataset.Tables(0).Rows(idx4).Item(0) > Format(aoe_end_date, "yyyy-MM-dd") Then
                            paid_after = True
                            Exit For
                        End If
                    Catch ex As Exception
                    End Try
                Next
            End If
            'log_file = debtorID
            'Try
            '    My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, log_file, False)
            'Catch ex As Exception

            'End Try

            
            Dim debt_amt2 As Decimal = (tot_debt_amt - paid2client_before)
            Dim pif As String = "N"
            Dim status As String = debtor_dataset.Tables(0).Rows(idx).Item(3)
            If (status = "S" Or status = "F") And allocated_date = CDate("January 1, 2100") And paid_after = False And to_client > 0 Then
                pif = "Y"
            End If
            Dim visited As String = "N"
            If visit_found Then
                visited = "Y"
            End If
            Try
                Me.AOE_statsTableAdapter.InsertQuery(debtorID, Format(created_date, "yyyy-MM-dd"), to_client, to_fees, waiting, debt_amt2, visited, pif, Format(aoe_end_date, "yyyy-MM-dd"), aoe_no_payments)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
        Next
        'My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, "AOE stats finished " & Format(Now, "dd/MM/yyyy hh:mm:ss"), False)
        'MsgBox("Finished")
        Me.Close()
    End Sub



    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.AOE_statsTableAdapter.DeleteQuery()
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        start_debtor = 0
        process_debtors()
        Me.Close()
    End Sub

    Private Sub restartbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles restartbtn.Click
        
        start_debtor = InputBox("Enter start debtor ID", "Start debtor ID")
        start_debtor -= 1
        process_debtors()
    End Sub
End Class

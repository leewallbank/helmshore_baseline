<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class deptfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        'Me.DepartmentsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        'Me.DepartmentsDataSet = New Complaints.DepartmentsDataSet
        'Me.DepartmentsTableAdapter = New Complaints.DepartmentsDataSetTableAdapters.DepartmentsTableAdapter
        'Me.DeptcodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        'Me.DeptnameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        'CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        'CType(Me.DepartmentsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        'CType(Me.DepartmentsDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        'Me.DataGridView1.AllowUserToDeleteRows = False
        'Me.DataGridView1.AutoGenerateColumns = False
        'Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        'Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DeptcodeDataGridViewTextBoxColumn, Me.DeptnameDataGridViewTextBoxColumn})
        'Me.DataGridView1.DataSource = Me.DepartmentsBindingSource
        'Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        'Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        'Me.DataGridView1.Name = "DataGridView1"
        'Me.DataGridView1.Size = New System.Drawing.Size(448, 266)
        'Me.DataGridView1.TabIndex = 0
        ''
        ''DepartmentsBindingSource
        ''
        'Me.DepartmentsBindingSource.DataMember = "Departments"
        'Me.DepartmentsBindingSource.DataSource = Me.DepartmentsDataSet
        ''
        ''DepartmentsDataSet
        ''
        'Me.DepartmentsDataSet.DataSetName = "DepartmentsDataSet"
        'Me.DepartmentsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        ''
        ''DepartmentsTableAdapter
        ''
        'Me.DepartmentsTableAdapter.ClearBeforeFill = True
        '
        'DeptcodeDataGridViewTextBoxColumn
        '
        'Me.DeptcodeDataGridViewTextBoxColumn.DataPropertyName = "dept_code"
        'Me.DeptcodeDataGridViewTextBoxColumn.HeaderText = "Code"
        'Me.DeptcodeDataGridViewTextBoxColumn.Name = "DeptcodeDataGridViewTextBoxColumn"
        'Me.DeptcodeDataGridViewTextBoxColumn.ReadOnly = True
        'Me.DeptcodeDataGridViewTextBoxColumn.Width = 40
        ''
        ''DeptnameDataGridViewTextBoxColumn
        ''
        'Me.DeptnameDataGridViewTextBoxColumn.DataPropertyName = "dept_name"
        'Me.DeptnameDataGridViewTextBoxColumn.HeaderText = "Department name"
        'Me.DeptnameDataGridViewTextBoxColumn.Name = "DeptnameDataGridViewTextBoxColumn"
        'Me.DeptnameDataGridViewTextBoxColumn.Width = 300
        '
        'deptfrm
        '
        'Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        'Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        'Me.ClientSize = New System.Drawing.Size(448, 266)
        'Me.Controls.Add(Me.DataGridView1)
        'Me.Name = "deptfrm"
        'Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        'Me.Text = "Department form"
        'CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        'CType(Me.DepartmentsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        'CType(Me.DepartmentsDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        'Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    'Friend WithEvents DepartmentsDataSet As Complaints.DepartmentsDataSet
    'Friend WithEvents DepartmentsBindingSource As System.Windows.Forms.BindingSource
    'Friend WithEvents DepartmentsTableAdapter As Complaints.DepartmentsDataSetTableAdapters.DepartmentsTableAdapter
    'Friend WithEvents DeptcodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    'Friend WithEvents DeptnameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

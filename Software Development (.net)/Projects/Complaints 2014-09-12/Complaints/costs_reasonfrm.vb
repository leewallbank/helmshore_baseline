Public Class costs_reasonfrm

    Private Sub exit_no_savebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exit_no_savebtn.Click
        Me.Close()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        'save text
        If costs_reason_mode = "U" Then
            cancel_costs_reason_change = True
            cancel_costs_reason = Trim(costs_reasontbox.Text)
            If cancel_costs_reason.Length > 255 Then
                MsgBox("Maximum length is 255 characters - you have " & cancel_costs_reason.Length)
                Exit Sub
            End If
        End If
        
        Me.Close()
    End Sub

    Private Sub costs_reasonfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If costs_reason_mode = "U" Then
            exitbtn.Text = "Exit and Save"
            exit_no_savebtn.Text = "Exit without saving"
            exit_no_savebtn.Visible = True
            cancel_costs_reason_change = False
        Else
            exitbtn.Text = "Exit"
            exit_no_savebtn.Visible = False
        End If
        costs_reasontbox.Text = cancel_costs_reason
    End Sub
End Class
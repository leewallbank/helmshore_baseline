Public Class recvd_fromfrm

    Private Sub recvdfrom_frm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Me.Received_fromTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Received_from)
        param2 = "select recvd_from, recvd_text from Received_from order by recvd_from"
        Dim recvd_from_ds As DataSet = get_dataset("Complaints", param2)
        recvd_from_dg.Rows.Clear()
        Dim recvd_rows As Integer = no_of_rows - 1
        Dim recvd_idx As Integer
        For recvd_idx = 0 To recvd_rows
            recvd_from_dg.Rows.Add(recvd_from_ds.Tables(0).Rows(recvd_idx).Item(0), recvd_from_ds.Tables(0).Rows(recvd_idx).Item(1))
        Next
        recvd_from_orig_no_rows = recvd_from_dg.RowCount - 1
    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles recvd_from_dg.CellValueChanged
        If e.ColumnIndex = 1 And e.RowIndex >= 0 Then
            recvd_from = recvd_from_dg.Rows(e.RowIndex).Cells(0).Value
            If recvd_from = 0 Then Return
            recvd_from_dg.EndEdit()
            recvd_text = recvd_from_dg.Rows(e.RowIndex).Cells(1).Value

            Try
                'Me.Received_fromTableAdapter.UpdateQuery(recvd_text, recvd_from)
                log_text = "Complainant from amended - " & recvd_from & _
                                              " from " & orig_text & " to " & recvd_text
                upd_txt = "update Received_from set recvd_text = '" & recvd_text & "'" & _
                                " where recvd_from = " & recvd_from
                update_sql(upd_txt)
                log_type = "Master"
                add_log(log_type, log_text)
                'updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate complainant entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles recvd_from_dg.RowEnter

        recvd_from = recvd_from_dg.Rows(e.RowIndex).Cells(0).Value
        orig_text = recvd_from_dg.Rows(e.RowIndex).Cells(1).Value

    End Sub

    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles recvd_from_dg.RowsRemoved
        If e.RowIndex > 0 Then
            Try
                Try
                    Me.Received_fromTableAdapter.DeleteQuery(recvd_from)
                Catch
                    MessageBox.Show("can't delete as it's used in a complaint")
                End Try
                log_text = "Complainant deleted - " & recvd_from & " name " & orig_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If

    End Sub

    
    Private Sub Received_fromBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.Received_fromBindingSource.EndEdit()
        Me.Received_fromTableAdapter.Update(Me.PraiseAndComplaintsSQLDataSet.Received_from)

    End Sub
End Class
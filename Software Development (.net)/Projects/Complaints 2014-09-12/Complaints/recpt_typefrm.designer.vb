<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class recpt_typefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.recpt_type_dg = New System.Windows.Forms.DataGridView
        Me.recpt_code_dg = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.recpt_text_dg = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ReceipttypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.Receipt_typeTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Receipt_typeTableAdapter
        Me.ComplaintsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ComplaintsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ComplaintsTableAdapter
        CType(Me.recpt_type_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceipttypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'recpt_type_dg
        '
        Me.recpt_type_dg.AllowUserToDeleteRows = False
        Me.recpt_type_dg.AllowUserToOrderColumns = True
        Me.recpt_type_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.recpt_type_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.recpt_code_dg, Me.recpt_text_dg})
        Me.recpt_type_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.recpt_type_dg.Location = New System.Drawing.Point(0, 0)
        Me.recpt_type_dg.Name = "recpt_type_dg"
        Me.recpt_type_dg.Size = New System.Drawing.Size(455, 286)
        Me.recpt_type_dg.TabIndex = 0
        '
        'recpt_code_dg
        '
        Me.recpt_code_dg.HeaderText = "Code"
        Me.recpt_code_dg.Name = "recpt_code_dg"
        Me.recpt_code_dg.ReadOnly = True
        '
        'recpt_text_dg
        '
        Me.recpt_text_dg.HeaderText = "Text"
        Me.recpt_text_dg.Name = "recpt_text_dg"
        Me.recpt_text_dg.Width = 250
        '
        'ReceipttypeBindingSource
        '
        Me.ReceipttypeBindingSource.DataMember = "Receipt_type"
        Me.ReceipttypeBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSetBindingSource
        '
        'PraiseAndComplaintsSQLDataSetBindingSource
        '
        Me.PraiseAndComplaintsSQLDataSetBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        Me.PraiseAndComplaintsSQLDataSetBindingSource.Position = 0
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Receipt_typeTableAdapter
        '
        Me.Receipt_typeTableAdapter.ClearBeforeFill = True
        '
        'ComplaintsBindingSource
        '
        Me.ComplaintsBindingSource.DataMember = "Complaints"
        Me.ComplaintsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'ComplaintsTableAdapter
        '
        Me.ComplaintsTableAdapter.ClearBeforeFill = True
        '
        'recpt_typefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(455, 286)
        Me.Controls.Add(Me.recpt_type_dg)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "recpt_typefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Receipt type form"
        CType(Me.recpt_type_dg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceipttypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents recpt_type_dg As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents PraiseAndComplaintsSQLDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ReceipttypeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Receipt_typeTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Receipt_typeTableAdapter
    Friend WithEvents ComplaintsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ComplaintsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ComplaintsTableAdapter
    Friend WithEvents recpt_code_dg As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents recpt_text_dg As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

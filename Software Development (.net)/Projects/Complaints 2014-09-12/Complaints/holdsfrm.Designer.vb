<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class holdsfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.savebtn = New System.Windows.Forms.Button
        Me.nosavebtn = New System.Windows.Forms.Button
        Me.start_dtp = New System.Windows.Forms.DateTimePicker
        Me.end_dtp = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.start_tbox = New System.Windows.Forms.TextBox
        Me.end_tbox = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.stage_lbl = New System.Windows.Forms.Label
        Me.end_cbox = New System.Windows.Forms.CheckBox
        Me.end_gbox = New System.Windows.Forms.GroupBox
        Me.start_cbox = New System.Windows.Forms.CheckBox
        Me.start_gbox = New System.Windows.Forms.GroupBox
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.end_gbox.SuspendLayout()
        Me.start_gbox.SuspendLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'savebtn
        '
        Me.savebtn.Location = New System.Drawing.Point(498, 239)
        Me.savebtn.Name = "savebtn"
        Me.savebtn.Size = New System.Drawing.Size(93, 23)
        Me.savebtn.TabIndex = 3
        Me.savebtn.Text = "&Save and Exit"
        Me.savebtn.UseVisualStyleBackColor = True
        '
        'nosavebtn
        '
        Me.nosavebtn.Location = New System.Drawing.Point(12, 239)
        Me.nosavebtn.Name = "nosavebtn"
        Me.nosavebtn.Size = New System.Drawing.Size(138, 23)
        Me.nosavebtn.TabIndex = 4
        Me.nosavebtn.Text = "&Exit without saving"
        Me.nosavebtn.UseVisualStyleBackColor = True
        '
        'start_dtp
        '
        Me.start_dtp.Location = New System.Drawing.Point(40, 32)
        Me.start_dtp.Name = "start_dtp"
        Me.start_dtp.Size = New System.Drawing.Size(121, 20)
        Me.start_dtp.TabIndex = 0
        '
        'end_dtp
        '
        Me.end_dtp.Location = New System.Drawing.Point(33, 38)
        Me.end_dtp.Name = "end_dtp"
        Me.end_dtp.Size = New System.Drawing.Size(121, 20)
        Me.end_dtp.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(67, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(54, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Hold Start"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(66, 23)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Hold end"
        '
        'start_tbox
        '
        Me.start_tbox.Location = New System.Drawing.Point(40, 90)
        Me.start_tbox.Name = "start_tbox"
        Me.start_tbox.ReadOnly = True
        Me.start_tbox.Size = New System.Drawing.Size(121, 20)
        Me.start_tbox.TabIndex = 4
        '
        'end_tbox
        '
        Me.end_tbox.Location = New System.Drawing.Point(33, 88)
        Me.end_tbox.Name = "end_tbox"
        Me.end_tbox.ReadOnly = True
        Me.end_tbox.Size = New System.Drawing.Size(121, 20)
        Me.end_tbox.TabIndex = 5
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(61, 74)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(60, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "updated by"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(56, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(60, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "updated by"
        '
        'stage_lbl
        '
        Me.stage_lbl.AutoSize = True
        Me.stage_lbl.Location = New System.Drawing.Point(302, 21)
        Me.stage_lbl.Name = "stage_lbl"
        Me.stage_lbl.Size = New System.Drawing.Size(45, 13)
        Me.stage_lbl.TabIndex = 8
        Me.stage_lbl.Text = "Stage X"
        '
        'end_cbox
        '
        Me.end_cbox.AutoSize = True
        Me.end_cbox.Location = New System.Drawing.Point(333, 109)
        Me.end_cbox.Name = "end_cbox"
        Me.end_cbox.Size = New System.Drawing.Size(71, 17)
        Me.end_cbox.TabIndex = 9
        Me.end_cbox.Text = "End Date"
        Me.end_cbox.UseVisualStyleBackColor = True
        '
        'end_gbox
        '
        Me.end_gbox.Controls.Add(Me.Label2)
        Me.end_gbox.Controls.Add(Me.end_dtp)
        Me.end_gbox.Controls.Add(Me.Label4)
        Me.end_gbox.Controls.Add(Me.end_tbox)
        Me.end_gbox.Location = New System.Drawing.Point(410, 57)
        Me.end_gbox.Name = "end_gbox"
        Me.end_gbox.Size = New System.Drawing.Size(181, 128)
        Me.end_gbox.TabIndex = 10
        Me.end_gbox.TabStop = False
        Me.end_gbox.Text = "Hold End Date"
        '
        'start_cbox
        '
        Me.start_cbox.AutoSize = True
        Me.start_cbox.Location = New System.Drawing.Point(12, 109)
        Me.start_cbox.Name = "start_cbox"
        Me.start_cbox.Size = New System.Drawing.Size(74, 17)
        Me.start_cbox.TabIndex = 11
        Me.start_cbox.Text = "Start Date"
        Me.start_cbox.UseVisualStyleBackColor = True
        '
        'start_gbox
        '
        Me.start_gbox.Controls.Add(Me.Label1)
        Me.start_gbox.Controls.Add(Me.start_dtp)
        Me.start_gbox.Controls.Add(Me.Label3)
        Me.start_gbox.Controls.Add(Me.start_tbox)
        Me.start_gbox.Location = New System.Drawing.Point(92, 57)
        Me.start_gbox.Name = "start_gbox"
        Me.start_gbox.Size = New System.Drawing.Size(200, 128)
        Me.start_gbox.TabIndex = 12
        Me.start_gbox.TabStop = False
        Me.start_gbox.Text = "Hold Start Date"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'holdsfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(614, 283)
        Me.Controls.Add(Me.start_gbox)
        Me.Controls.Add(Me.start_cbox)
        Me.Controls.Add(Me.end_gbox)
        Me.Controls.Add(Me.end_cbox)
        Me.Controls.Add(Me.stage_lbl)
        Me.Controls.Add(Me.nosavebtn)
        Me.Controls.Add(Me.savebtn)
        Me.Name = "holdsfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Hold dates"
        Me.end_gbox.ResumeLayout(False)
        Me.end_gbox.PerformLayout()
        Me.start_gbox.ResumeLayout(False)
        Me.start_gbox.PerformLayout()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents savebtn As System.Windows.Forms.Button
    Friend WithEvents nosavebtn As System.Windows.Forms.Button
    Friend WithEvents start_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents end_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents start_tbox As System.Windows.Forms.TextBox
    Friend WithEvents end_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents stage_lbl As System.Windows.Forms.Label
    Friend WithEvents end_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents end_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents start_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents start_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
End Class

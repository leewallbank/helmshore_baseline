<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class corr_actionsfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.corr_dg = New System.Windows.Forms.DataGridView
        Me.CorrectiveActionsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.Corrective_actionsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Corrective_actionsTableAdapter
        Me.corr_code = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cor_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.corr_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.CorrectiveActionsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'corr_dg
        '
        Me.corr_dg.AllowUserToDeleteRows = False
        Me.corr_dg.AllowUserToOrderColumns = True
        Me.corr_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.corr_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.corr_code, Me.cor_name})
        Me.corr_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.corr_dg.Location = New System.Drawing.Point(0, 0)
        Me.corr_dg.Name = "corr_dg"
        Me.corr_dg.Size = New System.Drawing.Size(402, 266)
        Me.corr_dg.TabIndex = 0
        '
        'CorrectiveActionsBindingSource
        '
        Me.CorrectiveActionsBindingSource.DataMember = "Corrective actions"
        Me.CorrectiveActionsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSetBindingSource
        '
        'PraiseAndComplaintsSQLDataSetBindingSource
        '
        Me.PraiseAndComplaintsSQLDataSetBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        Me.PraiseAndComplaintsSQLDataSetBindingSource.Position = 0
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Corrective_actionsTableAdapter
        '
        Me.Corrective_actionsTableAdapter.ClearBeforeFill = True
        '
        'corr_code
        '
        Me.corr_code.HeaderText = "Code"
        Me.corr_code.Name = "corr_code"
        Me.corr_code.ReadOnly = True
        '
        'cor_name
        '
        Me.cor_name.HeaderText = "Name"
        Me.cor_name.Name = "cor_name"
        Me.cor_name.Width = 200
        '
        'corr_actionsfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(402, 266)
        Me.Controls.Add(Me.corr_dg)
        Me.Name = "corr_actionsfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Corrective Actions"
        CType(Me.corr_dg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.CorrectiveActionsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents corr_dg As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents CorrectiveActionsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Corrective_actionsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Corrective_actionsTableAdapter
    Friend WithEvents corr_code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cor_name As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

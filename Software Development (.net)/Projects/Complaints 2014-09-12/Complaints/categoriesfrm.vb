Public Class categoriesfrm

    Private Sub categoriesfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Complaint_categories' table. You can move, or remove it, as needed.
        Me.Complaint_categoriesTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_categories)
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Complaint_categories' table. You can move, or remove it, as needed.
       
        comp_cat_orig_no_rows = DataGridView1.RowCount
    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.ColumnIndex = 2 And e.RowIndex >= 0 Then
            cat_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            cat_number = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            cat_text = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            If DataGridView1.Rows(e.RowIndex).IsNewRow Then Return
            Try
                Me.Complaint_categoriesTableAdapter.UpdateQuery(cat_text, cat_code, cat_number)
                log_text = "Complaint category amended - " & cat_code & " " & cat_number & _
                           " from " & orig_text & " to " & cat_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate Category entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter
        If e.RowIndex > 0 Then
            cat_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            cat_number = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            orig_text = DataGridView1.Rows(e.RowIndex).Cells(2).Value
        End If
    End Sub
    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles DataGridView1.RowsRemoved
        If e.RowIndex > 0 Then
            Try
                Me.Complaint_categoriesTableAdapter.DeleteQuery(cat_code, cat_number)
                log_text = "Complaint category deleted - " & cat_code & " " & cat_number & " name " & orig_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show("Can't delete as complaints have this category assigned")
            End Try
        End If
    End Sub
    Private Sub DataGridView1_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.Validated

    End Sub

    Private Sub DataGridView1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles DataGridView1.Validating

    End Sub
End Class
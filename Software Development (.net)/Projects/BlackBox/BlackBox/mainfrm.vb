Public Class mainfrm
    '29.11.2010 confirmed address changed to broken arrangement - same heading
    Dim two_files As Boolean = False
    Dim three_files As Boolean = False
    Dim four_files As Boolean = False
    Dim five_files As Boolean = False
    Dim six_files As Boolean = False
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub createbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles createbtn.Click
        conn_open = False
        createbtn.Enabled = False
        exitbtn.Enabled = False
        Dim file As String = "Debtor" & vbTab & "ClientID" & vbTab & "created date" & vbTab & "Address line1" & _
        vbTab & "postcode" & vbTab & "easting" & vbTab & "northing" & vbTab & "scheme name" & vbTab & _
        "stage name" & vbTab & "last date" & vbTab & "offence date" & vbTab & "debt balance" & vbTab & _
        "case age" & vbTab & "bailiffID" & vbTab & "addconfirmed" & vbTab & "debt fees" & vbTab & "schemeID" & _
        vbTab & "debt paid" & vbTab & "linkID" & vbNewLine
        Dim filename As String = ""
        ProgressBar1.Value = 5
        param1 = "onestep"
        param2 = "select _rowid, clientschemeID, last_stageID, offence_court, address, add_postcode, " & _
        "add_os_easting, add_os_northing, last_date, _createdDate, debt_balance, bailiffID, arrange_broken, " & _
        " debt_fees, debtPaid, linkID, bail_current from Debtor where status_open_closed = 'O'" & _
        " and status = 'L' and status_hold = 'N' " & _
        " and status_nextdate is null order by clientschemeID"
        Dim debtor_dataset As DataSet
        Try
            debtor_dataset = get_dataset(param1, param2)
        Catch ex As Exception
            MsgBox("Unable to access onestep - try again later")
            End
        End Try

        Dim debtor_rows As Integer = no_of_rows

        Dim idx, csid, debtor, schemeid, clientid, easting, northing, case_age, bailiffid, linkid As Integer
        Dim addr_line1, postcode, stage_name, arrange_broken As String
        Dim offence_court, last_date, created_date As Date
        Dim debt_bal, debt_fees, debt_paid As Decimal
        Dim last_csid As Integer = 0
        Dim last_csid_valid As Boolean
        Dim sch_name As String = ""
        Dim row_count As Integer = 0
        For idx = 0 To debtor_rows - 1
            row_count += 1
            If row_count > 1000 Then
                row_count = 0
                ProgressBar2.Value = (idx / debtor_rows) * 100
            End If
            ProgressBar1.Value = (row_count / 10)
            Application.DoEvents()
            csid = debtor_dataset.Tables(0).Rows(idx).Item(1)
            If csid <> last_csid Then
                last_csid = csid
                last_csid_valid = False
                param2 = "select schemeID, clientID from ClientScheme where _rowid = " & csid & _
                " and branchID = 1 and clientID <> 1 and clientID <> 2 and clientID <> 24"
                Dim cs_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 0 Then
                    Continue For
                End If
                schemeid = cs_dataset.Tables(0).Rows(0).Item(0)
                clientid = cs_dataset.Tables(0).Rows(0).Item(1)
                param2 = "select name from Scheme where _rowid = " & schemeid
                Dim sch_dataset As DataSet = get_dataset(param1, param2)
                sch_name = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                Dim sch_name3 As String = Microsoft.VisualBasic.Left(sch_name, 3)
                If sch_name3 = "C T" Or sch_name3 = "CTa" Or sch_name3 = "Cta" Or _
                    sch_name3 = "NND" Or sch_name3 = "Roa" Or sch_name3 = "Com" Then
                    last_csid_valid = True
                Else
                    Continue For
                End If
            End If

            If last_csid_valid = False Then
                Continue For
            End If
            Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(2)
            param2 = "select name from Stage where _rowid = " & stageID
            Dim stage_dataset As DataSet = get_dataset(param1, param2)
            stage_name = Trim(stage_dataset.Tables(0).Rows(0).Item(0))
            If stage_name <> "AwaitingVanApproval" And stage_name <> "F C - Bailiff" And _
                stage_name <> "FurtherVanAttendance" And stage_name <> "Van Attendance" Then
                Continue For
            End If

            Try
                bailiffid = debtor_dataset.Tables(0).Rows(idx).Item(11)
            Catch ex As Exception
                bailiffid = 0
            End Try
            If debtor_dataset.Tables(0).Rows(idx).Item(16) = "Y" Then    'bail_current
                'ignore if bailiff does not have a pen
                param2 = "select hasPen from Bailiff where _rowid = " & bailiffid
                Dim bail_dataset As DataSet = get_dataset(param1, param2)
                If bail_dataset.Tables(0).Rows(0).Item(0) <> "Y" Then
                    Continue For
                End If
            Else
                bailiffid = 0
            End If
            debtor = debtor_dataset.Tables(0).Rows(idx).Item(0)

            created_date = debtor_dataset.Tables(0).Rows(idx).Item(9)
            Try
                offence_court = debtor_dataset.Tables(0).Rows(idx).Item(3)
            Catch ex As Exception
                offence_court = created_date
            End Try

            If offence_court > Now Then
                Dim date_amended As Boolean = False
                While date_amended = False
                    MsgBox("Debtor " & debtor & " has LO date in the future. Please correct on onestep")
                    Try
                        offence_court = InputBox("Enter correct date (dd.mm.yyyy)", debtor & " - LO date correction - " & Format(offence_court, "dd.MM.yyyy"))
                    Catch ex As Exception
                        MsgBox("enter a valid date")
                        Continue While
                    End Try
                    If offence_court > Now Then
                        MsgBox("Date must not be in the future")
                        Continue While
                    End If
                    Exit While
                End While
            End If

            Try
                postcode = debtor_dataset.Tables(0).Rows(idx).Item(5)
            Catch ex As Exception
                postcode = ""
            End Try

            Dim pc_length As Integer = InStr(postcode, Chr(10)) - 1
            If pc_length > 0 Then
                postcode = Microsoft.VisualBasic.Left(postcode, pc_length)
            End If
            pc_length = InStr(postcode, Chr(13)) - 1
            If pc_length > 0 Then
                postcode = Microsoft.VisualBasic.Left(postcode, pc_length)
            End If
            pc_length = InStr(postcode, vbTab) - 1
            If pc_length > 0 Then
                postcode = Microsoft.VisualBasic.Left(postcode, pc_length)
            End If
            Try
                easting = debtor_dataset.Tables(0).Rows(idx).Item(6)
            Catch ex As Exception
                easting = 0
            End Try
            Try
                northing = debtor_dataset.Tables(0).Rows(idx).Item(7)
            Catch ex As Exception
                northing = 0
            End Try

            last_date = debtor_dataset.Tables(0).Rows(idx).Item(8)

            debt_bal = debtor_dataset.Tables(0).Rows(idx).Item(10)
            case_age = DateDiff(DateInterval.Day, created_date, Now)

            arrange_broken = debtor_dataset.Tables(0).Rows(idx).Item(12)
            debt_fees = debtor_dataset.Tables(0).Rows(idx).Item(13)
            debt_paid = debtor_dataset.Tables(0).Rows(idx).Item(14)
            Try
                linkid = debtor_dataset.Tables(0).Rows(idx).Item(15)
            Catch ex As Exception
                linkid = 0
            End Try
            Try
                addr_line1 = debtor_dataset.Tables(0).Rows(idx).Item(4)
            Catch ex As Exception
                MsgBox("Warning case " & debtor & " has no address details")
                addr_line1 = ""
            End Try

            If Microsoft.VisualBasic.Left(addr_line1, 1) = Chr(10) Then
                addr_line1 = Microsoft.VisualBasic.Right(addr_line1, addr_line1.Length - 1)
            End If
            Dim addr_line1_length As Integer = InStr(addr_line1, Chr(10)) - 1
            If addr_line1_length > 0 Then
                addr_line1 = Microsoft.VisualBasic.Left(addr_line1, addr_line1_length)
            End If
            addr_line1_length = InStr(addr_line1, Chr(13)) - 1
            If addr_line1_length > 0 Then
                addr_line1 = Microsoft.VisualBasic.Left(addr_line1, addr_line1_length)
            End If
            addr_line1_length = InStr(addr_line1, ",") - 1
            If addr_line1_length > 0 Then
                addr_line1 = Microsoft.VisualBasic.Left(addr_line1, addr_line1_length)
            End If
            addr_line1_length = InStr(addr_line1, vbTab) - 1
            If addr_line1_length > 0 Then
                addr_line1 = Microsoft.VisualBasic.Left(addr_line1, addr_line1_length)
            End If
            If addr_line1.Length > 30 Then
                addr_line1 = Trim(Microsoft.VisualBasic.Left(addr_line1, 30))
            End If
            Try
                file = file & debtor & vbTab & clientid & vbTab & Format(offence_court, "yyyy-MM-dd") & vbTab & _
                addr_line1 & vbTab & postcode & vbTab & easting & vbTab & northing & vbTab & sch_name & _
                vbTab & stage_name & vbTab & Format(last_date, "yyyy-MM-dd") & vbTab & Format(created_date, "yyyy-MM-dd") & vbTab & _
                Format(debt_bal, "#.00") & vbTab & Format(case_age, "#") & vbTab & bailiffid & vbTab & arrange_broken & vbTab & _
                Format(debt_fees, "#.00") & vbTab & schemeid & vbTab & Format(debt_paid, "#.00") & vbTab & _
                linkid & vbNewLine

            Catch ex As Exception
                'save file1
                If two_files = False Then
                    MsgBox("file too big so save first file then combine later")
                    two_files = True
                    With SaveFileDialog1
                        .Title = "Save file"
                        .Filter = "TXT files |*.txt"
                        .DefaultExt = ".txt"
                        .OverwritePrompt = True
                        .FileName = "blackbox1.txt"
                    End With
                    If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
                        file = debtor & vbTab & clientid & vbTab & Format(offence_court, "yyyy-MM-dd") & vbTab & _
                     addr_line1 & vbTab & postcode & vbTab & easting & vbTab & northing & vbTab & sch_name & _
                     vbTab & stage_name & vbTab & Format(last_date, "yyyy-MM-dd") & vbTab & Format(created_date, "yyyy-MM-dd") & vbTab & _
                     Format(debt_bal, "#.00") & vbTab & Format(case_age, "#") & vbTab & bailiffid & vbTab & arrange_broken & vbTab & _
                     Format(debt_fees, "#.00") & vbTab & schemeid & vbTab & Format(debt_paid, "#.00") & vbTab & _
                     linkid & vbNewLine
                    End If
                Else
                    If three_files = False Then
                        three_files = True
                        MsgBox("save second part then combine later")
                        two_files = True
                        With SaveFileDialog1
                            .Title = "Save file"
                            .Filter = "TXT files |*.txt"
                            .DefaultExt = ".txt"
                            .OverwritePrompt = True
                            .FileName = "blackbox2.txt"
                        End With
                        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
                            file = debtor & vbTab & clientid & vbTab & Format(offence_court, "yyyy-MM-dd") & vbTab & _
                         addr_line1 & vbTab & postcode & vbTab & easting & vbTab & northing & vbTab & sch_name & _
                         vbTab & stage_name & vbTab & Format(last_date, "yyyy-MM-dd") & vbTab & Format(created_date, "yyyy-MM-dd") & vbTab & _
                         Format(debt_bal, "#.00") & vbTab & Format(case_age, "#") & vbTab & bailiffid & vbTab & arrange_broken & vbTab & _
                         Format(debt_fees, "#.00") & vbTab & schemeid & vbTab & Format(debt_paid, "#.00") & vbTab & _
                         linkid & vbNewLine
                        End If
                    Else
                        If four_files = False Then
                            four_files = True
                            MsgBox("save third part then combine later")
                            two_files = True
                            With SaveFileDialog1
                                .Title = "Save file"
                                .Filter = "TXT files |*.txt"
                                .DefaultExt = ".txt"
                                .OverwritePrompt = True
                                .FileName = "blackbox3.txt"
                            End With
                            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
                                file = debtor & vbTab & clientid & vbTab & Format(offence_court, "yyyy-MM-dd") & vbTab & _
                             addr_line1 & vbTab & postcode & vbTab & easting & vbTab & northing & vbTab & sch_name & _
                             vbTab & stage_name & vbTab & Format(last_date, "yyyy-MM-dd") & vbTab & Format(created_date, "yyyy-MM-dd") & vbTab & _
                             Format(debt_bal, "#.00") & vbTab & Format(case_age, "#") & vbTab & bailiffid & vbTab & arrange_broken & vbTab & _
                             Format(debt_fees, "#.00") & vbTab & schemeid & vbTab & Format(debt_paid, "#.00") & vbTab & _
                             linkid & vbNewLine
                            End If
                        Else
                            If five_files = False Then
                                five_files = True
                                MsgBox("save fourth part then combine later")
                                two_files = True
                                With SaveFileDialog1
                                    .Title = "Save file"
                                    .Filter = "TXT files |*.txt"
                                    .DefaultExt = ".txt"
                                    .OverwritePrompt = True
                                    .FileName = "blackbox4.txt"
                                End With
                                If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                                    My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
                                    file = debtor & vbTab & clientid & vbTab & Format(offence_court, "yyyy-MM-dd") & vbTab & _
                                 addr_line1 & vbTab & postcode & vbTab & easting & vbTab & northing & vbTab & sch_name & _
                                 vbTab & stage_name & vbTab & Format(last_date, "yyyy-MM-dd") & vbTab & Format(created_date, "yyyy-MM-dd") & vbTab & _
                                 Format(debt_bal, "#.00") & vbTab & Format(case_age, "#") & vbTab & bailiffid & vbTab & arrange_broken & vbTab & _
                                 Format(debt_fees, "#.00") & vbTab & schemeid & vbTab & Format(debt_paid, "#.00") & vbTab & _
                                 linkid & vbNewLine
                                End If
                            Else
                                If six_files = False Then
                                    six_files = True
                                    MsgBox("save fifth part then combine later")
                                    two_files = True
                                    With SaveFileDialog1
                                        .Title = "Save file"
                                        .Filter = "TXT files |*.txt"
                                        .DefaultExt = ".txt"
                                        .OverwritePrompt = True
                                        .FileName = "blackbox5.txt"
                                    End With
                                    If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                                        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
                                        file = debtor & vbTab & clientid & vbTab & Format(offence_court, "yyyy-MM-dd") & vbTab & _
                                     addr_line1 & vbTab & postcode & vbTab & easting & vbTab & northing & vbTab & sch_name & _
                                     vbTab & stage_name & vbTab & Format(last_date, "yyyy-MM-dd") & vbTab & Format(created_date, "yyyy-MM-dd") & vbTab & _
                                     Format(debt_bal, "#.00") & vbTab & Format(case_age, "#") & vbTab & bailiffid & vbTab & arrange_broken & vbTab & _
                                     Format(debt_fees, "#.00") & vbTab & schemeid & vbTab & Format(debt_paid, "#.00") & vbTab & _
                                     linkid & vbNewLine
                                    End If
                                Else
                                    MsgBox("Ask IT to allow up to 7 files")
                                End If
                            End If
                        End If
                    End If
                End If
            End Try
        Next
        Dim fname As String = "blackbox1.txt"
        If six_files Then
            fname = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) & "6.txt"
            My.Computer.FileSystem.WriteAllText(fname, file, False)
            MsgBox("Six Files created")
        ElseIf five_files Then
            fname = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) & "5.txt"
            My.Computer.FileSystem.WriteAllText(fname, file, False)
            MsgBox("Five Files created")
        ElseIf four_files Then
            fname = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) & "4.txt"
            My.Computer.FileSystem.WriteAllText(fname, file, False)
            MsgBox("Four Files created")
        ElseIf three_files Then
            fname = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) & "3.txt"
            My.Computer.FileSystem.WriteAllText(fname, file, False)
            MsgBox("Three Files created")
        Else
            If two_files Then
                fname = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) & "2.txt"
                My.Computer.FileSystem.WriteAllText(fname, file, False)
                MsgBox("Two Files created")
            Else
                With SaveFileDialog1
                    .Title = "Save file"
                    .Filter = "TXT files |*.txt"
                    .DefaultExt = ".txt"
                    .OverwritePrompt = True
                    .FileName = fname
                End With
                If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                    My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
                    MsgBox("One File created")
                End If
            End If
        End If
        Me.Close()
    End Sub

    Private Sub mainfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        conn.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub mainfrm_MaximizedBoundsChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.MaximizedBoundsChanged

    End Sub
End Class

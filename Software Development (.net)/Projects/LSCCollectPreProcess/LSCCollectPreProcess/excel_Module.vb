Module excel_Module
    Public filename, cf_outfile, wd_outfile, dr_outfile, ma_outfile, fn_outfile, rd_outfile, rk_outfile, sc_outfile, errorfile As String
    Public vals(1, 1) As String
    Public finalrow, finalcol As Integer
    Public Sub load_vals(ByVal filename As String)
        Dim row, col As Integer
        Dim xl = CreateObject("Excel.Application")
        xl.Workbooks.open(filename)
        xl.worksheets("Sheet1").activate()
        finalrow = xl.activesheet.usedrange.rows.count
        finalcol = xl.activesheet.usedrange.columns.count
        ReDim vals(finalrow, finalcol)
        Dim idx As Integer = 0
        For row = 1 To finalrow
            For col = 1 To finalcol
                idx += 1
                If xl.activesheet.cells(row, col).value = Nothing Then
                    'vals(row, col) = 0
                Else
                    vals(row, col) = xl.activesheet.cells(row, col).value.ToString
                End If
            Next
        Next
        xl.workbooks.close()
        xl = Nothing
    End Sub
End Module

Public Class mainform
    Dim outline As String = ""
    Dim amt As Decimal
    Dim lodate As Date
    Dim clref As String = ""
    Dim case_num As Integer
    Dim addr1 As String
    Dim addr2 As String
    Dim addr3 As String
    Dim cf_sep As String = "|"
    Dim dr_sep As String = "*"
    Dim ma_sep As String = "`"
    Dim fn_sep As String = "~"
    Dim rd_sep As String = "#"
    Dim rk_sep As String = "?"
    Dim sc_sep As String = "<"
    Dim wd_sep As String = ">"
    Dim ascii As New System.Text.ASCIIEncoding()

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Excel files | *.xls"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                load_vals(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        openbtn.Enabled = False

        Dim idx, idx2 As Integer
        Dim notes As String = ""
        Dim add_notes As String = ""
        Dim value As String = ""
        Dim debt_type As String = ""
        errorfile = Nothing
        cf_outfile = Nothing
        dr_outfile = Nothing
        ma_outfile = Nothing
        fn_outfile = Nothing
        rd_outfile = Nothing
        rk_outfile = Nothing
        sc_outfile = Nothing
        wd_outfile = Nothing
        Dim cf_cases As Integer = 0
        Dim dr_cases As Integer = 0
        Dim ma_cases As Integer = 0
        Dim fn_cases As Integer = 0
        Dim rd_cases As Integer = 0
        Dim rk_cases As Integer = 0
        Dim sc_cases As Integer = 0
        Dim wd_cases As Integer = 0
        Dim cf_heading As String = ""
        Dim dr_heading As String = ""
        Dim ma_heading As String = ""
        Dim fn_heading As String = ""
        Dim rd_heading As String = ""
        Dim rk_heading As String = ""
        Dim sc_heading As String = ""
        Dim wd_heading As String = ""
        For idx = 1 To finalrow
            ProgressBar1.Value = idx / finalrow * 100
            Try
                Dim lth As Integer = vals(idx, 1).Length
            Catch ex As Exception
                Exit For
            End Try
            debt_type = Trim(vals(idx, 10))
            For idx2 = 1 To finalcol
                value = Trim(vals(idx, idx2))
                'remove any separators from value
                Replace(value, cf_sep, "")
                Replace(value, dr_sep, "")
                Replace(value, ma_sep, "")
                Replace(value, fn_sep, "")
                Replace(value, rd_sep, "")
                Replace(value, rk_sep, "")
                Replace(value, sc_sep, "")
                Replace(value, wd_sep, "")
                'remove any quotes from value
                Dim idx3 As Integer
                Dim new_value As String = ""
                If value <> Nothing Then
                    For idx3 = 1 To value.Length
                        If Mid(value, idx3, 1) <> Chr(34) Then
                            new_value = new_value & Mid(value, idx3, 1)
                        End If
                    Next
                End If
                value = new_value
                'save headings
                If idx = 1 Then
                    If idx2 > 17 Then
                        Continue For
                    End If
                    If idx2 = 17 Then
                        cf_heading = cf_heading & "Notes" & vbNewLine
                        dr_heading = dr_heading & "Notes" & vbNewLine
                        ma_heading = ma_heading & "Notes" & vbNewLine
                        fn_heading = fn_heading & "Notes" & vbNewLine
                        rd_heading = rd_heading & "Notes" & vbNewLine
                        rk_heading = rk_heading & "Notes" & vbNewLine
                        sc_heading = sc_heading & "Notes" & vbNewLine
                        wd_heading = wd_heading & "Notes" & vbNewLine
                    Else
                        cf_heading = cf_heading & value & cf_sep
                        dr_heading = dr_heading & value & dr_sep
                        ma_heading = ma_heading & value & ma_sep
                        fn_heading = fn_heading & value & fn_sep
                        rd_heading = rd_heading & value & rd_sep
                        rk_heading = rk_heading & value & rk_sep
                        sc_heading = sc_heading & value & sc_sep
                        wd_heading = wd_heading & value & wd_sep
                    End If
                    Continue For
                End If
                If idx2 = 9 Then
                    If Microsoft.VisualBasic.Left(value, 4) = "Tel." Then
                        value = Microsoft.VisualBasic.Right(value, value.Length - 4)
                    End If
                ElseIf idx2 = 10 Then
                    'validate debt type
                    debt_type = value
                    If debt_type <> "CF" And debt_type <> "DR" _
                    And debt_type <> "MA" And debt_type <> "FN" And debt_type <> "WD" _
                    And debt_type <> "RD" And debt_type <> "RK" And debt_type <> "SC" Then
                        errorfile = errorfile & "Debt type invalid on record number " & idx & vbNewLine
                    End If
                ElseIf idx2 = 14 Then
                    Try
                        If value.Length > 0 Then
                            value = "Reason:" & value
                        End If
                    Catch ex As Exception

                    End Try
                ElseIf idx2 = 15 Or idx2 = 16 Or idx2 = 18 Then
                    Try
                        Dim dte As Date = CDate(value)
                        If Year(dte) < 1910 Then
                            value = ""
                        Else
                            value = Format(dte, "dd.MM.yyyy")
                        End If
                    Catch ex As Exception
                        value = ""
                    End Try
                ElseIf idx2 = 19 Then
                    'remove embedded spaces
                    Dim space_found As Boolean = False
                    new_value = ""
                    For idx3 = 1 To value.Length
                        If Mid(value, idx3, 1) = " " Then
                            If Not space_found Then
                                new_value = new_value & Mid(value, idx3, 1)
                                space_found = True
                            End If
                        Else
                            space_found = False
                            new_value = new_value & Mid(value, idx3, 1)
                        End If
                    Next
                    value = new_value
                End If
                If idx2 < 17 Then
                    outline = outline & value
                    'use appropriate seperator
                    Select Case debt_type
                        Case "CF"
                            outline = outline & cf_sep
                        Case "DR"
                            outline = outline & dr_sep
                        Case "MA"
                            outline = outline & ma_sep
                        Case "FN"
                            outline = outline & fn_sep
                        Case "RD"
                            outline = outline & rd_sep
                        Case "RK"
                            outline = outline & rk_sep
                        Case "SC"
                            outline = outline & sc_sep
                        Case "WD"
                            outline = outline & wd_sep
                    End Select

                ElseIf idx > 1 Then
                    Try
                        If value.Length > 0 Then
                            outline = outline & vals(1, idx2) & ":" & value & ";"
                        End If
                    Catch ex As Exception

                    End Try
                End If
            Next
            Select Case debt_type
                Case "CF"
                    If cf_outfile = Nothing Then
                        cf_outfile = cf_heading
                    End If
                    cf_outfile = cf_outfile & outline & vbNewLine
                    cf_cases += 1
                Case "DR"
                    If dr_outfile = Nothing Then
                        dr_outfile = dr_heading
                    End If
                    dr_outfile = dr_outfile & outline & vbNewLine
                    dr_cases += 1
                Case "MA"
                    If ma_outfile = Nothing Then
                        ma_outfile = ma_heading
                    End If
                    ma_outfile = ma_outfile & outline & vbNewLine
                    ma_cases += 1
                Case "FN"
                    If fn_outfile = Nothing Then
                        fn_outfile = fn_heading
                    End If
                    fn_outfile = fn_outfile & outline & vbNewLine
                    fn_cases += 1
                Case "RD"
                    If rd_outfile = Nothing Then
                        rd_outfile = rd_heading
                    End If
                    rd_outfile = rd_outfile & outline & vbNewLine
                    rd_cases += 1
                Case "RK"
                    If rk_outfile = Nothing Then
                        rk_outfile = rk_heading
                    End If
                    rk_outfile = rk_outfile & outline & vbNewLine
                    rk_cases += 1
                Case "SC"
                    If sc_outfile = Nothing Then
                        sc_outfile = sc_heading
                    End If
                    sc_outfile = sc_outfile & outline & vbNewLine
                    sc_cases += 1
                Case "WD"
                    If wd_outfile = Nothing Then
                        wd_outfile = wd_heading
                    End If
                    wd_outfile = wd_outfile & outline & vbNewLine
                    wd_cases += 1
            End Select
            outline = ""
        Next

        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText("error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            If cf_outfile <> Nothing Then
                My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocessCF.txt", cf_outfile, False, ascii)
            End If
            If dr_outfile <> Nothing Then
                My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocessDR.txt", dr_outfile, False, ascii)
            End If
            If ma_outfile <> Nothing Then
                My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocessMA.txt", ma_outfile, False, ascii)
            End If
            If fn_outfile <> Nothing Then
                My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocessFN.txt", fn_outfile, False, ascii)
            End If
            If rd_outfile <> Nothing Then
                My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocessRD.txt", rd_outfile, False, ascii)
            End If
            If rk_outfile <> Nothing Then
                My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocessRK.txt", rk_outfile, False, ascii)
            End If
            If sc_outfile <> Nothing Then
                My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocessSC.txt", sc_outfile, False, ascii)
            End If
            If wd_outfile <> Nothing Then
                My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocessWD.txt", wd_outfile, False, ascii)
            End If
            TextBox1.Text = "No errors found"
            Dim tot_cases As Integer = cf_cases + dr_cases + ma_cases + fn_cases + rd_cases _
            + rk_cases + sc_cases + wd_cases
            MsgBox("CF cases = " & cf_cases & vbNewLine & "DR cases = " & dr_cases & vbNewLine & _
                   "MA cases = " & ma_cases & vbNewLine & "FN cases = " & fn_cases & vbNewLine & _
                    "RD cases = " & rd_cases & vbNewLine & "RK cases = " & rk_cases & vbNewLine & _
                    "SC cases = " & sc_cases & vbNewLine & "WD cases =  " & wd_cases & vbNewLine & "Total cases = " & tot_cases)
            Me.Close()
        End If
        
        
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
End Class

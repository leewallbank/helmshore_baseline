Imports System.Collections
Public Class mainfrm
    Private RA210NPreport As RA210NP
    Private Const PARAMETER_FIELD_NAME1 As String = "parm_debtor"
    Private Const PARAMETER_FIELD_NAME2 As String = "parm_start_date"
    Private Const PARAMETER_FIELD_NAME3 As String = "parm_end_date"
    Dim myArrayList1 As ArrayList = New ArrayList()
    Dim myArrayList2 As ArrayList = New ArrayList()
    Dim myArrayList3 As ArrayList = New ArrayList()
    Dim folder_name, folder_name2, bail_type, xref_fname As String
    Dim start_date, end_date As Date


    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim days As Integer = 6 + Weekday(Now)
        start_Date_picker.Value = DateAdd(DateInterval.Day, -days, Now)
        end_date_picker.Value = DateAdd(DateInterval.Day, 6, start_Date_picker.Value)
    End Sub

    Private Sub datesbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub filebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles filebtn.Click
        exitbtn.Enabled = False
        Dim start_day, end_day As String
        start_date = start_Date_picker.Value
        end_date = end_date_picker.Value
        start_day = Format(start_date, "ddd")
        end_day = Format(end_date, "ddd")
        If MsgBox("Run from " & start_day & " " & Format(start_date, "dd.MM.yyyy") & " to " & end_day & " " & Format(end_date, "dd.MM.yyyy"), _
        MsgBoxStyle.OkCancel, "Create files") <> MsgBoxResult.Ok Then
            MsgBox("Files not created")
            exitbtn.Enabled = True
            Exit Sub
        End If
        ProgressBar1.Value = 5
        param1 = "onestep"
        param2 = "select _rowid, client_ref from Debtor " & _
        " where status_open_closed = 'O' and status = 'T' and clientschemeID = 214 " & _
        " and traceDate between '" & Format(start_date, "yyyy.MM.dd") & "' and '" & _
        Format(end_date, "yyyy.MM.dd") & "' order by client_ref"
        Dim debtor_dataset = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("There are no cases for this period")
            Exit Sub
        End If
        filebtn.Enabled = False
        Dim debtor_rows As Integer = no_of_rows
        Dim idx As Integer
        Dim fname As String = ""
        For idx = 0 To debtor_rows - 1
            ProgressBar1.Value = (idx / debtor_rows) * 100
            Application.DoEvents()
            Dim cl_ref As String
            cl_ref = Trim(debtor_dataset.tables(0).rows(idx).item(1).ToString)
            Dim debtor As Integer = debtor_dataset.tables(0).rows(idx).item(0)
            If idx = 0 Then
                With SaveFileDialog1
                    .Title = "Save files"
                    .Filter = "pdf|*.pdf"
                    .DefaultExt = ".pdf"
                    .OverwritePrompt = True
                    .InitialDirectory = "c:"
                    .FileName = debtor & "-" & cl_ref & ".pdf"
                    .Title = "Select folder for saving files (New folder BANQUER will be created)"
                End With
                If SaveFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
                    MsgBox("Files not created")
                    exitbtn.Enabled = True
                    Exit Sub
                Else
                    fname = SaveFileDialog1.FileName
                    Dim idx2 As Integer
                    For idx2 = fname.Length To 1 Step -1
                        If Mid(fname, idx2, 1) = "\" Then
                            fname = Microsoft.VisualBasic.Left(fname, idx2) & "BAILQUER\"
                            'check directory exists
                            Try
                                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(fname)
                                If System.IO.Directory.Exists(fname) = False Then
                                    di = System.IO.Directory.CreateDirectory(fname)
                                Else
                                    System.IO.Directory.Delete(fname, True)
                                    di = System.IO.Directory.CreateDirectory(fname)
                                End If
                                xref_fname = fname & "BAILQUER.txt"
                            Catch ex As Exception
                                MsgBox("Unable to create folder")
                                End
                            End Try
                            Exit For
                        End If
                    Next
                End If
            End If
            write_report(debtor, cl_ref, fname)
        Next
        MsgBox("Files created in " & fname)
        Me.Close()
    End Sub
    Sub write_report(ByVal debtor As Integer, ByVal cl_ref As String, ByVal fname As String)
        Dim RA210NPReport = New RA210NP
        myArrayList1.Add(debtor)
        myArrayList2.Add(Format(start_date, "yyyy-MM-dd"))
        myArrayList3.Add(Format(end_date, "yyyy-MM-dd"))
        SetCurrentValuesForParameterField1(RA210NPReport, myArrayList1)
        SetCurrentValuesForParameterField2(RA210NPReport, myArrayList2)
        SetCurrentValuesForParameterField3(RA210NPReport, myArrayList3)
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA210NPReport)

        Dim file As String = fname & cl_ref & "-" & debtor & ".pdf"
        RA210NPReport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, file)
        'write record to xref text file
        Dim xref_text As String = cl_ref & "|" & cl_ref & "-" & debtor & ".pdf" & "|TRACE|BAILQUER" & vbNewLine
        My.Computer.FileSystem.WriteAllText(xref_fname, xref_text, True)

    End Sub
    Public Sub SetDBLogonForReport(ByVal myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo, ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub
    Private Sub SetCurrentValuesForParameterField1(ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myArrayList As ArrayList)
        Dim currentParameterValues As CrystalDecisions.Shared.ParameterValues = New CrystalDecisions.Shared.ParameterValues()
        For Each submittedValue As Object In myArrayList
            Dim myParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue()
            myParameterDiscreteValue.Value = submittedValue.ToString()
            currentParameterValues.Add(myParameterDiscreteValue)
        Next
        Dim myParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        Dim myParameterFieldDefinition As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME1)
        myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)
    End Sub
    Private Sub SetCurrentValuesForParameterField2(ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myArrayList As ArrayList)
        Dim currentParameterValues As CrystalDecisions.Shared.ParameterValues = New CrystalDecisions.Shared.ParameterValues()
        For Each submittedValue As Object In myArrayList
            Dim myParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue()
            myParameterDiscreteValue.Value = submittedValue.ToString()
            currentParameterValues.Add(myParameterDiscreteValue)
        Next
        Dim myParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        Dim myParameterFieldDefinition As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME2)
        myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)
    End Sub
    Private Sub SetCurrentValuesForParameterField3(ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myArrayList As ArrayList)
        Dim currentParameterValues As CrystalDecisions.Shared.ParameterValues = New CrystalDecisions.Shared.ParameterValues()
        For Each submittedValue As Object In myArrayList
            Dim myParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue()
            myParameterDiscreteValue.Value = submittedValue.ToString()
            currentParameterValues.Add(myParameterDiscreteValue)
        Next
        Dim myParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        Dim myParameterFieldDefinition As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME3)
        myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)
    End Sub
End Class

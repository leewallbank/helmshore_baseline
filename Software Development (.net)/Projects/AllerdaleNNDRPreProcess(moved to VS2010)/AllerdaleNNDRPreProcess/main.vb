Public Class mainform



    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline, caption As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim propaddr As String = ""
        Dim curraddr As String = ""
        Dim bill_no As String = ""
        Dim name As String = ""
        Dim name2 As String = ""
        Dim tel1 As String = ""
        Dim tel2 As String = ""
        Dim lonum As String = ""
        Dim lostring As String = ""
        Dim idx, idx2, idx3 As Integer
        Dim lines As Integer = 0
        Dim debt_amt, war_amt As Decimal
        Dim lodate, fromdate, todate As Date
        Dim clref As String = ""
        Dim addrline As String = ""
        Dim amtstring As String = ""
        Dim comments As String = Nothing

        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "Client ref|Name|Name2|DebtAddress|tel1|Tel2|Current Address|LO Number|LO Date|Warrant Amt|From date|To date|Debt Amount|Comments" & vbNewLine
        outfile = outline

        'look for Account Number
        Dim total_owing_found As Boolean
        For idx = 2 To lines - 1
            caption = Mid(line(idx), 4, 14)
            If caption <> "Account number" Then
                Continue For
            Else
                clref = Trim(Mid(line(idx), 27, 12))
                For idx2 = idx + 1 To lines - 1
                    caption = Trim(Mid(line(idx2), 4, 14))
                    Select Case caption
                        Case Is = "Name:"
                            name = Trim(Mid(line(idx2), 27, 35))
                            name = Replace(name, "&", "and")
                            'If InStr(name, "&") Then
                            '    Dim start_idx As Integer = InStr(name, "&")
                            '    Dim end_idx As Integer
                            '    For end_idx = name.Length To 1 Step -1
                            '        If Mid(name, end_idx, 1) = " " Then
                            '            Exit For
                            '        End If
                            '    Next
                            '    name2 = Microsoft.VisualBasic.Right(name, name.Length - start_idx - 1)
                            '    name = Microsoft.VisualBasic.Left(name, start_idx - 1) & _
                            '        Microsoft.VisualBasic.Right(name, name.Length - end_idx)
                            'End If

                        Case Is = "Other(s):"
                            name2 = Trim(Mid(line(idx2), 27, 35))
                        Case Is = "Property Addre"
                            addrline = ""
                            For idx3 = idx2 To idx2 + 7
                                caption = Mid(line(idx3), 4, 14)
                                If caption = "Summons Number" Then
                                    Exit For
                                End If
                                addrline = remove_chars(Mid(line(idx3), 27, 35))
                                If Len(addrline) > 3 Then
                                    If propaddr <> "" Then
                                        propaddr = propaddr & ","
                                    End If
                                    propaddr = propaddr & addrline
                                End If
                            Next
                        Case Is = "Home Telephone"
                            tel1 = Mid(line(idx2), 27, 25)
                        Case Is = "Mobile Telepho"
                            tel2 = Mid(line(idx2), 27, 25)
                        Case Is = "Address:"
                            addrline = ""
                            For idx3 = idx2 To idx2 + 7
                                caption = Mid(line(idx3), 4, 14)
                                If caption = "Property Addre" Then
                                    Exit For
                                End If
                                addrline = remove_chars(Mid(line(idx3), 27, 35))
                                If Len(addrline) > 3 Then
                                    If curraddr <> "" Then
                                        curraddr = curraddr & ","
                                    End If
                                    curraddr = curraddr & addrline
                                End If
                            Next
                        Case Is = "Summons Number"
                            lonum = Mid(line(idx2), 27, 25)
                        Case Is = "Hearing Date:"
                            lostring = Mid(line(idx2), 27, 25)
                            If Not IsDate(lostring) Then
                                errorfile = errorfile & "Line  " & idx2 & " - LO Date not valid - " _
                                                             & lostring & vbNewLine
                            Else
                                lodate = lostring
                            End If
                        Case Is = "Amount Granted"
                            amtstring = Mid(line(idx2), 27, 10)
                            If Not IsNumeric(amtstring) Then
                                errorfile = errorfile & "Line  " & idx2 & " - Warrant amount not numeric - " _
                                         & amtstring & vbNewLine
                            Else
                                war_amt = amtstring
                            End If
                        Case Is = "Charge Period("
                            Dim temp_date As Date
                            lostring = Mid(line(idx2), 28, 10)
                            If Not IsDate(lostring) Then
                                errorfile = errorfile & "Line  " & idx2 & " - From Date not valid - " _
                                                               & lostring & vbNewLine
                            Else
                                comments = comments & "Charge period: " & lostring
                                temp_date = CDate(lostring)
                                If fromdate = Nothing Then
                                    fromdate = temp_date
                                Else
                                    If temp_date < fromdate Then
                                        fromdate = temp_date
                                    End If
                                End If
                            End If

                            lostring = Mid(line(idx2), 42, 10)
                            If Not IsDate(lostring) Then
                                errorfile = errorfile & "Line  " & idx3 & " - To Date not valid - " _
                                                              & lostring & vbNewLine
                            Else
                                comments = comments & " to " & lostring & "; "
                                temp_date = CDate(lostring)
                                If todate = Nothing Then
                                    todate = temp_date
                                Else
                                    If temp_date > todate Then
                                        todate = temp_date
                                    End If
                                End If
                            End If

                        Case Is = "Total Amount O"
                            total_owing_found = True
                            amtstring = Mid(line(idx2), 27, 10)
                            If Not IsNumeric(amtstring) Then
                                errorfile = errorfile & "Line  " & idx2 & " - Debt amount not numeric - " _
                                         & amtstring & vbNewLine
                            Else
                                debt_amt = amtstring
                            End If
                            idx = idx2
                            Exit For
                    End Select
                Next
            End If
            'validate case details
            If clref = Nothing Or Microsoft.VisualBasic.Left(clref, 3) = "   " Then
                errorfile = errorfile & "Line  " & idx2 & " - No client reference" & vbNewLine
            End If

            'save case in outline
            outfile = outfile & clref & "|" & name & "|" & name2 & "|" & propaddr & "|" _
            & tel1 & "|" & tel2 & "|" & curraddr & "|" & lonum & "|" & lodate & "|" & war_amt & _
            "|" & fromdate & "|" & todate & "|" & debt_amt & "|" & comments & vbNewLine
            name = ""
            name2 = ""
            propaddr = ""
            tel1 = ""
            tel2 = ""
            curraddr = ""
            lonum = ""
            lodate = Nothing
            debt_amt = Nothing
            war_amt = Nothing
            comments = Nothing
            fromdate = Nothing
            todate = Nothing
        Next
        
        viewbtn.Enabled = True
        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
    Private Function remove_chars(ByVal txt As String) As String
        Dim idx As Integer
        Dim return_txt As String = ""
        For idx = 1 To txt.Length
            If Mid(txt, idx, 1) = Chr(9) Or Mid(txt, idx, 1) = Chr(10) Or Mid(txt, idx, 1) = Chr(13) Then
                return_txt = return_txt & " "
            Else
                return_txt = return_txt & Mid(txt, idx, 1)
            End If
        Next
        Return (Trim(return_txt))
    End Function
End Class

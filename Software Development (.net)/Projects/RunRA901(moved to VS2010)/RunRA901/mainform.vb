Public Class mainform

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub artbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles artbtn.Click
        disable_buttons()
        Form1.ShowDialog()
        enable_buttons()
    End Sub

    Private Sub brtbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles brtbtn.Click
        disable_buttons()
        Form2.ShowDialog()
        enable_buttons()
    End Sub

    Private Sub disable_buttons()
        artbtn.Enabled = False
        brtbtn.Enabled = False
        exitbtn.Enabled = False
    End Sub
    Private Sub enable_buttons()
        artbtn.Enabled = True
        brtbtn.Enabled = True
        exitbtn.Enabled = True
    End Sub

End Class
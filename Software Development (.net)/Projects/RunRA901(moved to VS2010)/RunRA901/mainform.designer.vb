<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainform
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.artbtn = New System.Windows.Forms.Button
        Me.brtbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'artbtn
        '
        Me.artbtn.Location = New System.Drawing.Point(65, 59)
        Me.artbtn.Name = "artbtn"
        Me.artbtn.Size = New System.Drawing.Size(150, 23)
        Me.artbtn.TabIndex = 0
        Me.artbtn.Text = "RA901ART"
        Me.artbtn.UseVisualStyleBackColor = True
        '
        'brtbtn
        '
        Me.brtbtn.Location = New System.Drawing.Point(65, 115)
        Me.brtbtn.Name = "brtbtn"
        Me.brtbtn.Size = New System.Drawing.Size(150, 23)
        Me.brtbtn.TabIndex = 1
        Me.brtbtn.Text = "RA901BRT"
        Me.brtbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(203, 290)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 6
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'mainform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(290, 342)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.brtbtn)
        Me.Controls.Add(Me.artbtn)
        Me.MaximizeBox = False
        Me.Name = "mainform"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Run Crystal Reports"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents artbtn As System.Windows.Forms.Button
    Friend WithEvents brtbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.runbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.Monthly_statsDataSet = New DerbyMonthlyReport.Monthly_statsDataSet
        Me.Monthly_statsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Monthly_statsTableAdapter = New DerbyMonthlyReport.Monthly_statsDataSetTableAdapters.Monthly_statsTableAdapter
        Me.Label2 = New System.Windows.Forms.Label
        CType(Me.Monthly_statsDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Monthly_statsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(105, 115)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(104, 23)
        Me.runbtn.TabIndex = 0
        Me.runbtn.Text = "Update Table"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(188, 215)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 215)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 3
        '
        'Monthly_statsDataSet
        '
        Me.Monthly_statsDataSet.DataSetName = "Monthly_statsDataSet"
        Me.Monthly_statsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Monthly_statsBindingSource
        '
        Me.Monthly_statsBindingSource.DataMember = "Monthly_stats"
        Me.Monthly_statsBindingSource.DataSource = Me.Monthly_statsDataSet
        '
        'Monthly_statsTableAdapter
        '
        Me.Monthly_statsTableAdapter.ClearBeforeFill = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(112, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(39, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Label2"
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(348, 266)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Derby Monthly report"
        CType(Me.Monthly_statsDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Monthly_statsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Monthly_statsDataSet As DerbyMonthlyReport.Monthly_statsDataSet
    Friend WithEvents Monthly_statsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Monthly_statsTableAdapter As DerbyMonthlyReport.Monthly_statsDataSetTableAdapters.Monthly_statsTableAdapter
    Friend WithEvents Label2 As System.Windows.Forms.Label

End Class

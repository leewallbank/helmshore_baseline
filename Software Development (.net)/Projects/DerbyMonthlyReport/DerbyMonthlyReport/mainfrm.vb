Public Class mainfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        start_month = 4
        If Month(Now) < 4 Then
            start_year = Year(Now) - 1
        Else
            start_year = Year(Now)
        End If
        start_financial_year = CDate(CStr(start_year) & ",4,1")

        If MsgBox("Run for Derby CTAX?", MsgBoxStyle.YesNo, "CTAX or NNDR") = MsgBoxResult.Yes Then
            csid = 1067
            Label2.Text = "Derby CTAX"
        Else
            csid = 1068
            Label2.Text = "Derby NNDR"
        End If
        
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        'need to run for all months from april to last month
        'first do april to december
        Dim month_no As Integer
        For month_no = 4 To 12
            start_date = CDate(CStr(start_year) & "," & CStr(month_no) & ",1")
            If month_no = 12 Then
                end_date = CDate(CStr(start_year + 1) & ",1,1")
            Else
                end_date = CDate(CStr(start_year) & "," & CStr(month_no + 1) & ",1")
            End If
            ProgressBar1.Value = 0
            MsgBox("Updating " & start_date)
            update_month()
            If month_no + 1 = Month(Now) Then
                Exit For
            End If
        Next
        'now do Jan to March if required
        start_year = Year(Now)

        If Month(Now) > 1 And Month(Now) < 5 Then

            For month_no = 1 To 3
                start_date = CDate(CStr(start_year) & "," & CStr(month_no) & ",1")
                end_date = CDate(CStr(start_year) & "," & CStr(month_no + 1) & ",1")
                MsgBox("Updating " & start_date)
                ProgressBar1.Value = 0
                update_month()
                If month_no + 1 = Month(Now) Then
                    Exit For
                End If
            Next
        End If
        MsgBox("Updates complete")
    End Sub
    Sub update_month()
        Dim on_hold As Integer = 0
        Dim live As Integer = 0
        Dim update_on_hold As Boolean = True
        Dim on_hold_str As String = ""
        on_hold_str = InputBox("Enter Number of cases on hold", "Cases on Hold")
        If Microsoft.VisualBasic.Len(Trim(on_hold_str)) = 0 Then
            update_on_hold = False
        Else
            Try
                on_hold = on_hold_str
            Catch
                MsgBox("Number on hold must be numeric")
                Exit Sub
            End Try
        End If
        If update_on_hold = True Then
            Dim live_str As String = ""
            live_str = InputBox("Enter Number of LIVE cases", "LIVE Cases")
            Try
                live = live_str
            Catch
                MsgBox("Number of live cases must be numeric")
                Exit Sub
            End Try
        End If
        'get cases and value received in month

        param1 = "onestep"
        param2 = " select _rowid, debt_amount, arrange_started, arrange_broken," & _
        "_createdDate, status_open_closed, return_date, return_codeID from Debtor" & _
        " where clientschemeID = " & csid & " and _createdDate >= '" & Format(start_date, "yyyy.MM.dd") & _
        "' and _createdDate < '" & Format(end_date, "yyyy.MM.dd") & "'"
        Dim debtor_dataset As DataSet = get_dataset(param1, param2)
        Dim cases_recvd As Integer = no_of_rows
        Dim idx As Integer = 0
        Dim value_recvd As Decimal = 0
        Dim debtor As Integer
        Dim tot_paid As Decimal = 0
        Dim arr_no As Integer = 0
        Dim arr_unbroken As Integer = 0
        Dim first_visit_no As Decimal = 0
        Dim second_visit_no As Decimal = 0
        Dim third_visit_no As Decimal = 0
        Dim completed_no As Decimal = 0
        Dim return_no As Decimal = 0
        Dim nulla_bona As Integer = 0
        Dim no_contact As Integer = 0
        Dim gant As Integer = 0
        Dim client_request As Integer = 0
        Dim guidelines As Integer = 0


        Dim first_visit_pcent, second_visit_pcent, third_visit_pcent, completed_pcent As Decimal
        For idx = 0 To cases_recvd - 1
            ProgressBar1.Value = idx / cases_recvd * 100
            value_recvd += debtor_dataset.Tables(0).Rows(idx).Item(1)
            debtor = debtor_dataset.Tables(0).Rows(idx).Item(0)
            'get payments received
            param2 = "select split_debt, split_costs from Payment where debtorID = " & debtor & _
            " and status = 'R'"
            Dim payment_dataset As DataSet = get_dataset(param1, param2)
            Dim idx2 As Integer
            Dim pay_no As Integer = no_of_rows - 1
            For idx2 = 0 To pay_no
                tot_paid += payment_dataset.Tables(0).Rows(idx2).Item(0) + _
                            payment_dataset.Tables(0).Rows(idx2).Item(1)
            Next
            'number of arrangements made
            Dim arr_found As Boolean = True
            Try
                Dim arr_made As String = debtor_dataset.Tables(0).Rows(idx).Item(2)
            Catch
                arr_found = False
            End Try
            If arr_found Then
                arr_no += 1
                'number of unbroken arrangements
                If debtor_dataset.Tables(0).Rows(idx).Item(3) = "N" Then
                    arr_unbroken += 1
                End If
            End If
            'number with visits within certain days
            param2 = "select date_visited, bailiffID from Visit" & _
            " where debtorID = " & debtor & " order by date_visited"
            Dim visit_dataset As DataSet = get_dataset(param1, param2)
            Dim visit_no As Integer = no_of_rows - 1
            Dim bailiffid As Integer
            Dim first_visit_found As Boolean = False
            Dim second_visit_found As Boolean = False
            Dim third_visit_found As Boolean = False
            Dim created_date As Date = debtor_dataset.Tables(0).Rows(idx).Item(4)
            Dim date_str As String
            For idx2 = 0 To visit_no
                Dim date_visited As Date
                Try
                    date_visited = visit_dataset.Tables(0).Rows(idx2).Item(0)
                Catch ex As Exception
                    Continue For
                End Try
                bailiffid = visit_dataset.Tables(0).Rows(idx2).Item(1)
                param2 = "select name_sur from Bailiff " & _
                " where _rowid = " & bailiffid & " and agent_type = 'B'"
                Dim bailiff_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 0 Then
                    Continue For
                End If
                Dim bail_name As String = Microsoft.VisualBasic.Left(bailiff_dataset.Tables(0).Rows(0).Item(0), 7)
                If bail_name = "Account" Or bail_name = "Stacked" Or bail_name = "Overplu" Then
                    Continue For
                End If

                date_str = "#" & DatePart(DateInterval.Year, created_date) & " " & _
                DatePart(DateInterval.Month, created_date) & " " _
                & DatePart(DateInterval.Day, created_date) & " 0:0:0 AM#"
                created_date = CDate(date_str)
                date_str = "#" & DatePart(DateInterval.Year, date_visited) & " " & _
                DatePart(DateInterval.Month, date_visited) & " " _
                & DatePart(DateInterval.Day, date_visited) & " 0:0:0 AM#"
                date_visited = CDate(date_str)
                If first_visit_found = False Then
                    If DateDiff(DateInterval.Day, created_date, date_visited) < 15 Then
                        first_visit_no += 1
                        first_visit_found = True
                    End If
                ElseIf second_visit_found = False Then
                    If DateDiff(DateInterval.Day, created_date, date_visited) < 22 Then
                        second_visit_no += 1
                        second_visit_found = True
                    End If
                ElseIf third_visit_found = False Then
                    third_visit_no += 1
                    third_visit_found = True
                End If
            Next
            'number completed within 30 days
            If debtor_dataset.Tables(0).Rows(idx).Item(5) = "C" Then
                Dim return_date As Date
                Try
                    return_date = debtor_dataset.Tables(0).Rows(idx).Item(6)
                Catch
                    Continue For
                End Try
                date_str = "#" & DatePart(DateInterval.Year, return_date) & " " & _
                                DatePart(DateInterval.Month, return_date) & " " _
                                & DatePart(DateInterval.Day, return_date) & " 0:0:0 AM#"
                return_date = CDate(date_str)
                If DateDiff(DateInterval.Day, created_date, return_date) < 31 Then
                    return_no += 1
                End If
                Dim return_code As Integer
                Try
                    return_code = debtor_dataset.Tables(0).Rows(idx).Item(7)
                Catch
                    Continue For
                End Try
                'get return category
                param2 = "select fee_category from CodeReturns where _rowid = " & return_code
                Dim retn_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 0 Then
                    Continue For
                End If
                Select Case retn_dataset.Tables(0).Rows(0).Item(0)
                    Case 1
                        gant += 1
                    Case 2
                        client_request += 1
                    Case 3
                        nulla_bona += 1
                    Case 4
                        guidelines += 1
                    Case 6
                        no_contact += 1
                End Select
            End If
        Next
        Dim arr_pcent As Decimal
        Dim unbroken_pcent As Decimal
        If cases_recvd = 0 Then
            first_visit_pcent = 0
            second_visit_pcent = 0
            third_visit_pcent = 0
            arr_pcent = 0
            unbroken_pcent = 0
            completed_pcent = 0
        Else
            first_visit_pcent = first_visit_no / cases_recvd * 100
            second_visit_pcent = second_visit_no / cases_recvd * 100
            third_visit_pcent = third_visit_no / cases_recvd * 100
            arr_pcent = (arr_no / cases_recvd) * 100
            unbroken_pcent = (arr_unbroken / arr_no) * 100
            completed_pcent = return_no / cases_recvd * 100
        End If

        Dim prev_live As Integer = 0
        Dim prev_value As Decimal = 0
        Dim prev_paid As Decimal = 0
        'get previous year still live
        param2 = "select _rowid, debt_amount, status_open_closed, return_date from Debtor " & _
        " where clientschemeID = " & csid & _
        " and _createdDate < '" & Format(start_financial_year, "yyyy.MM.dd") & "'"
        Dim prev_dataset As DataSet = get_dataset(param1, param2)
        Dim prev_rows As Integer = no_of_rows - 1
        For idx = 0 To prev_rows
            ProgressBar1.Value = idx / prev_rows * 100
            debtor = prev_dataset.Tables(0).Rows(idx).Item(0)
            'get payments received
            param2 = "select split_debt, split_costs from Payment where debtorID = " & debtor & _
            " and status = 'R' and status_date < '" & Format(end_date, "yyyy.MM.dd") & "'"
            Dim payment2_dataset As DataSet = get_dataset(param1, param2)
            Dim idx2 As Integer
            Dim pay_no As Integer = no_of_rows - 1
            For idx2 = 0 To pay_no
                prev_paid += payment2_dataset.Tables(0).Rows(idx2).Item(0) + _
                            payment2_dataset.Tables(0).Rows(idx2).Item(1)
            Next
            If prev_dataset.Tables(0).Rows(idx).Item(2) = "O" Then
                prev_live += 1
                prev_value += prev_dataset.Tables(0).Rows(idx).Item(1)
                Continue For
            Else
                Dim return_date As Date
                Try
                    return_date = prev_dataset.Tables(0).Rows(idx).Item(3)
                Catch ex As Exception
                    Continue For
                End Try
                If return_date > end_date Then
                    prev_live += 1
                    prev_value += prev_dataset.Tables(0).Rows(idx).Item(1)
                End If
            End If
        Next
        'see if row already exists
        Monthly_statsTableAdapter.FillBy(Monthly_statsDataSet.Monthly_stats, csid, start_year, Month(start_date))
        If Monthly_statsDataSet.Tables(0).Rows.Count = 0 Then
            Try
                Monthly_statsTableAdapter.InsertQuery(csid, start_year, Month(start_date), _
                                cases_recvd, value_recvd, tot_paid, arr_pcent, unbroken_pcent, prev_live, _
                                prev_value, prev_paid, first_visit_pcent, second_visit_pcent, third_visit_pcent, _
                                completed_pcent, nulla_bona, no_contact, gant, client_request, guidelines, _
                                on_hold, live)

            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
        Else
            Try
                If update_on_hold Then
                    Monthly_statsTableAdapter.UpdateQuery(cases_recvd, value_recvd, tot_paid, arr_pcent, unbroken_pcent, prev_live, _
                               prev_value, prev_paid, first_visit_pcent, second_visit_pcent, third_visit_pcent, _
                               completed_pcent, nulla_bona, no_contact, gant, client_request, guidelines, _
                               on_hold, live, csid, start_year, Month(start_date))
                Else
                    Monthly_statsTableAdapter.UpdateQuery1(cases_recvd, value_recvd, tot_paid, arr_pcent, unbroken_pcent, prev_live, _
                                                   prev_value, prev_paid, first_visit_pcent, second_visit_pcent, third_visit_pcent, _
                                                   completed_pcent, nulla_bona, no_contact, gant, client_request, guidelines, _
                                                   csid, start_year, Month(start_date))
                End If
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
        End If
    End Sub
End Class

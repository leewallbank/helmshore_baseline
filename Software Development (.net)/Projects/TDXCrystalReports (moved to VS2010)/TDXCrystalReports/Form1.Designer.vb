<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rd229btn = New System.Windows.Forms.Button
        Me.RD253btn = New System.Windows.Forms.Button
        Me.allbtn = New System.Windows.Forms.Button
        Me.RD230btn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.run_dtp = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'rd229btn
        '
        Me.rd229btn.Location = New System.Drawing.Point(79, 158)
        Me.rd229btn.Name = "rd229btn"
        Me.rd229btn.Size = New System.Drawing.Size(92, 23)
        Me.rd229btn.TabIndex = 1
        Me.rd229btn.Text = "Run RD229"
        Me.rd229btn.UseVisualStyleBackColor = True
        '
        'RD253btn
        '
        Me.RD253btn.Location = New System.Drawing.Point(79, 261)
        Me.RD253btn.Name = "RD253btn"
        Me.RD253btn.Size = New System.Drawing.Size(92, 23)
        Me.RD253btn.TabIndex = 3
        Me.RD253btn.Text = "Run RD253"
        Me.RD253btn.UseVisualStyleBackColor = True
        '
        'allbtn
        '
        Me.allbtn.Location = New System.Drawing.Point(79, 93)
        Me.allbtn.Name = "allbtn"
        Me.allbtn.Size = New System.Drawing.Size(92, 23)
        Me.allbtn.TabIndex = 0
        Me.allbtn.Text = "All Reports"
        Me.allbtn.UseVisualStyleBackColor = True
        '
        'RD230btn
        '
        Me.RD230btn.Location = New System.Drawing.Point(79, 218)
        Me.RD230btn.Name = "RD230btn"
        Me.RD230btn.Size = New System.Drawing.Size(92, 23)
        Me.RD230btn.TabIndex = 2
        Me.RD230btn.Text = "Run RD230"
        Me.RD230btn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(205, 363)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 5
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'run_dtp
        '
        Me.run_dtp.Location = New System.Drawing.Point(63, 48)
        Me.run_dtp.Name = "run_dtp"
        Me.run_dtp.Size = New System.Drawing.Size(130, 20)
        Me.run_dtp.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(103, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Run date"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(13, 363)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 8
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(60, 142)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(143, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Open as xls and save as csv"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 202)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(238, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Open as xls, remove blank rows and save as csv"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(48, 314)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(177, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "RD253D now has separate program"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 417)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.run_dtp)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.RD230btn)
        Me.Controls.Add(Me.allbtn)
        Me.Controls.Add(Me.RD253btn)
        Me.Controls.Add(Me.rd229btn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TDX reports"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents rd229btn As System.Windows.Forms.Button
    Friend WithEvents RD253btn As System.Windows.Forms.Button
    Friend WithEvents allbtn As System.Windows.Forms.Button
    Friend WithEvents RD230btn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents run_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label

End Class

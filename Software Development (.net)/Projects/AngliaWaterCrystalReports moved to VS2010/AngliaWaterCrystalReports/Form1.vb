Imports System.IO
Public Class Form1
    Dim allReports As Boolean
    Dim filename As String = ""
    Dim filepath As String = ""
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub RD260btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RD260btn.Click
        disable_buttons()
        run_RD260P()
    End Sub
    Private Sub run_RD260P()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD260Preport = New RD260P
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD260Preport, myArrayList1)
        myArrayList1.Add(2560)  'vacated
        SetCurrentValuesForParameterField2(RD260Preport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD260Preport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD260Preport, myConnectionInfo)
        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = "RD260 Vacated Anglia Water Invoice.pdf"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                RD260Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
                RD260Preport.close()
                'now write out occupied scheme
                filepath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                filepath &= "\"
            Else
                MsgBox("Report not saved")
            End If
        End If
        filename = filepath & "RD260 Occupied Anglia Water Invoice.pdf"
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD260Preport, myArrayList1)
        myArrayList1.Add(2562)  'Occupied
        SetCurrentValuesForParameterField2(RD260Preport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD260Preport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD260Preport, myConnectionInfo)
        RD260Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD260Preport.close()

        Dim RD260NPreport = New RD260NP
        filename = filepath & "RD260NP Anglia Water Invoice.pdf"
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD260NPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD260NPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD260NPreport, myConnectionInfo)
        RD260NPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD260NPreport.close()
        If Not allReports Then
            MsgBox("Completed")
            Me.Close()
        End If
    End Sub
    Private Sub run_RD260C2P()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD260C2Preport = New RD260C2P
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD260C2Preport, myArrayList1)
        myArrayList1.Add(2560)  'vacated
        SetCurrentValuesForParameterField2(RD260C2Preport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD260C2Preport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD260C2Preport, myConnectionInfo)
        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = "RD260C2P Vacated Anglia Water Credit Note.pdf"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                filepath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                filepath &= "\"
            Else
                MsgBox("Report not saved")
            End If
        Else
            filename = filepath & "RD260C2P Vacated Anglia Water Credit Note.pdf"
        End If
        RD260C2Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD260C2Preport.close()
        'now write out occupied scheme
        filename = filepath & "RD260C2P Occupied Anglia Water Credit Note.pdf"
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD260C2Preport, myArrayList1)
        myArrayList1.Add(2562)  'Occupied
        SetCurrentValuesForParameterField2(RD260C2Preport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD260C2Preport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD260C2Preport, myConnectionInfo)
        RD260C2Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD260C2Preport.close()
        If Not allReports Then
            MsgBox("Completed")
            Me.Close()
        End If
    End Sub
    Private Sub disable_buttons()
        exitbtn.Enabled = False
        RD260btn.Enabled = False
        rd262btn.Enabled = False
        allbtn.Enabled = False
        rd260c2btn.Enabled = False
        rd244btn.Enabled = False
    End Sub

    Private Sub rd262btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd262btn.Click
        If MsgBox("Have Shelley or David Dicks run the process remitted payments since remit?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            MsgBox("Report not run")
            Exit Sub
        End If
        disable_buttons()
        run_RD262PS()

    End Sub
    Private Sub run_RD262PS()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD262PSreport = New RD262PS
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD262PSreport, myArrayList1)
        myArrayList1.Add(2560)  'vacated
        SetCurrentValuesForParameterField2(RD262PSreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD262PSreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        myConnectionInfo2.ServerName = "RossFeeTable"
        myConnectionInfo2.DatabaseName = "FeesSQL"
        myConnectionInfo2.UserID = "sa"
        myConnectionInfo2.Password = "sa"
        SetDBLogonForReport(myConnectionInfo, RD262PSreport, myConnectionInfo2)
        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = "RD262 Vacated Anglia Water Payment file.xls"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                filepath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                filepath &= "\"
            Else
                MsgBox("Report not saved")
            End If
        Else
            filename = filepath & "RD262 Vacated Anglia Water Payment file.xls"
        End If
        RD262PSreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
        RD262PSreport.close()
        'now write out occupied scheme
        filename = filepath & "RD262 Occupied Anglia Water Payment file.xls"
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD262PSreport, myArrayList1)
        myArrayList1.Add(2562)  'Occupied
        SetCurrentValuesForParameterField2(RD262PSreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD262PSreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        myConnectionInfo2.ServerName = "RossFeeTable"
        myConnectionInfo2.DatabaseName = "FeesSQL"
        myConnectionInfo2.UserID = "sa"
        myConnectionInfo2.Password = "sa"
        SetDBLogonForReport(myConnectionInfo, RD262PSreport, myConnectionInfo2)
        RD262PSreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
        RD262PSreport.close()
        'now write out RD262ADP for direct payments
        Dim RD262ADPreport = New RD262ADP
        filename = filepath & "RD262ADP Anglia Water Direct Payment file.xls"
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD262ADPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD262ADPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        myConnectionInfo2.ServerName = "RossFeeTable"
        myConnectionInfo2.DatabaseName = "FeesSQL"
        myConnectionInfo2.UserID = "sa"
        myConnectionInfo2.Password = "sa"
        SetDBLogonForReport(myConnectionInfo, RD262ADPreport, myConnectionInfo2)
        RD262ADPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
        RD262ADPreport.close()
        'now write out RD262PSN for payments on new schemes
        Dim RD262PSNreport = New RD262PSN
        filename = filepath & "RD262PSN Anglia Water Payment file.xls"
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD262PSNreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD262PSNreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        myConnectionInfo2.ServerName = "RossFeeTable"
        myConnectionInfo2.DatabaseName = "FeesSQL"
        myConnectionInfo2.UserID = "sa"
        myConnectionInfo2.Password = "sa"
        SetDBLogonForReport(myConnectionInfo, RD262PSNreport, myConnectionInfo2)
        RD262PSNreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
        RD262PSNreport.close()
        MsgBox("Completed")

        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        allReports = False
    End Sub

    Private Sub rd260c2btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd260c2btn.Click
        disable_buttons()
        run_RD260C2P()
    End Sub

    Private Sub allbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allbtn.Click
        If MsgBox("Have Shelley or David Dicks run the process remitted payments since remit?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            MsgBox("Report not run")
            Exit Sub
        End If
        allReports = True
        disable_buttons()
        run_RD260P()
        run_RD260C2P()
        run_RD262PS()
        run_rd244()
    End Sub

    Private Sub rd244btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd244btn.Click
        disable_buttons()
        run_RD244()
    End Sub
    Private Sub run_RD244()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD244P2report = New RD244P2
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD244P2report, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD244P2report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"

        SetDBLogonForReport(myConnectionInfo, RD244P2report, myConnectionInfo2)
        Dim crystalFilename As String = "H:\temp\RD244P2\" & "RD262 Vacated Anglia Water Payment file.xls"
        'create directories for returns report
        Dim dir_name As String = "H:\temp\RD244P2"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create folder")
            End
        End Try
        RD244P2report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, crystalFilename)
        RD244P2report.close()


        'now read in out write out as pipe separated
        Dim excel_file_contents(,) As String = InputFromExcel(crystalFilename)

        'first write out heading
        Dim returnFile As String
        returnFile = excel_file_contents(0, 0) & "|" & excel_file_contents(0, 1) & vbNewLine
        Dim maxRow As Integer = UBound(excel_file_contents)
        Dim rowIDx As Integer
        For rowIDx = 1 To maxRow
            Dim colIDX As Integer
            For colIDX = 0 To 27
                If colIDX = 3 Then
                    'date might be zero
                    Dim testDate As Date
                    Dim testDateStr As String = ""
                    Try
                        testDate = excel_file_contents(rowIDx, colIDX)
                        If Format(testDate, "yyyy") < 1900 Then
                            testDateStr = ""
                        Else
                            testDateStr = Format(testDate, "dd/MM/yyyy")
                        End If
                    Catch ex As Exception

                    End Try
                    returnFile &= testDateStr & "|"
                ElseIf colIDX = 26 Then
                    'set value to include decimals
                    Dim testAmt As Decimal = 0
                    Try
                        testAmt = excel_file_contents(rowIDx, colIDX)
                    Catch ex As Exception

                    End Try
                    returnFile &= Format(testAmt, "f") & "|"
                Else
                    returnFile &= excel_file_contents(rowIDx, colIDX) & "|"
                End If

            Next
            returnFile &= excel_file_contents(rowIDx, colIDX) & vbNewLine
        Next



        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "CSV files |*.csv"
                .DefaultExt = ".csv"
                .OverwritePrompt = True
                .FileName = "RD244 Anglia Water Returns.csv"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                filepath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                filepath &= "\"
            Else
                MsgBox("Report not saved")
            End If
        Else
            filename = filepath & "RD244 Anglia Water Returns.csv"
        End If
        My.Computer.FileSystem.WriteAllText(filename, returnFile, False, System.Text.Encoding.ASCII)
        MsgBox("Completed")

        Me.Close()
    End Sub
End Class

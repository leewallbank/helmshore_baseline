<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RD260btn = New System.Windows.Forms.Button
        Me.rd262btn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.start_dtp = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.end_dtp = New System.Windows.Forms.DateTimePicker
        Me.allbtn = New System.Windows.Forms.Button
        Me.rd260c2btn = New System.Windows.Forms.Button
        Me.rd244btn = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'RD260btn
        '
        Me.RD260btn.Location = New System.Drawing.Point(12, 176)
        Me.RD260btn.Name = "RD260btn"
        Me.RD260btn.Size = New System.Drawing.Size(126, 23)
        Me.RD260btn.TabIndex = 3
        Me.RD260btn.Text = "Run RD260/RD260N "
        Me.RD260btn.UseVisualStyleBackColor = True
        '
        'rd262btn
        '
        Me.rd262btn.Location = New System.Drawing.Point(77, 224)
        Me.rd262btn.Name = "rd262btn"
        Me.rd262btn.Size = New System.Drawing.Size(136, 23)
        Me.rd262btn.TabIndex = 5
        Me.rd262btn.Text = "Run RD262/RD262AD"
        Me.rd262btn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(247, 340)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 7
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'start_dtp
        '
        Me.start_dtp.Location = New System.Drawing.Point(76, 25)
        Me.start_dtp.Name = "start_dtp"
        Me.start_dtp.Size = New System.Drawing.Size(125, 20)
        Me.start_dtp.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(82, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Set Start and End dates"
        '
        'end_dtp
        '
        Me.end_dtp.Location = New System.Drawing.Point(76, 69)
        Me.end_dtp.Name = "end_dtp"
        Me.end_dtp.Size = New System.Drawing.Size(125, 20)
        Me.end_dtp.TabIndex = 1
        '
        'allbtn
        '
        Me.allbtn.Location = New System.Drawing.Point(76, 110)
        Me.allbtn.Name = "allbtn"
        Me.allbtn.Size = New System.Drawing.Size(125, 23)
        Me.allbtn.TabIndex = 2
        Me.allbtn.Text = "Run ALL reports"
        Me.allbtn.UseVisualStyleBackColor = True
        '
        'rd260c2btn
        '
        Me.rd260c2btn.Location = New System.Drawing.Point(169, 176)
        Me.rd260c2btn.Name = "rd260c2btn"
        Me.rd260c2btn.Size = New System.Drawing.Size(126, 23)
        Me.rd260c2btn.TabIndex = 4
        Me.rd260c2btn.Text = "Run RD260C2"
        Me.rd260c2btn.UseVisualStyleBackColor = True
        '
        'rd244btn
        '
        Me.rd244btn.Location = New System.Drawing.Point(77, 277)
        Me.rd244btn.Name = "rd244btn"
        Me.rd244btn.Size = New System.Drawing.Size(136, 23)
        Me.rd244btn.TabIndex = 6
        Me.rd244btn.Text = "Run RD244"
        Me.rd244btn.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(334, 375)
        Me.Controls.Add(Me.rd244btn)
        Me.Controls.Add(Me.rd260c2btn)
        Me.Controls.Add(Me.allbtn)
        Me.Controls.Add(Me.end_dtp)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.start_dtp)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.rd262btn)
        Me.Controls.Add(Me.RD260btn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Anglia Water Crystal reports"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents RD260btn As System.Windows.Forms.Button
    Friend WithEvents rd262btn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents start_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents end_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents allbtn As System.Windows.Forms.Button
    Friend WithEvents rd260c2btn As System.Windows.Forms.Button
    Friend WithEvents rd244btn As System.Windows.Forms.Button

End Class

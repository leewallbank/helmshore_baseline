Public Class mainform



    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim propaddr As String = ""
        Dim curraddr As String = ""
        Dim name As String = ""
        Dim idx, idx2 As Integer
        Dim lines As Integer = 0
        Dim amt As Decimal
        Dim lodate As Date
        Dim clref As String = ""
        Dim off_number As String = ""
        Dim title(5) As String
        Dim forename(5) As String
        Dim surname(5) As String
        Dim addr1(5) As String
        Dim addr2(5) As String
        Dim addr3(5) As String
        Dim addr4(5) As String
        Dim addr5(5) As String
        Dim pcode(5) As String
        Dim debt_addr(5) As String
        Dim debt_pcode As String = ""
        Dim name2 As String = ""
        Dim comments As String = ""
        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "ClientRef|Amount|OffenceNumber|LODate|title|forename|surname|curr_addr1" & _
        "|curr_addr2|curr_addr3|curr_addr4|curr_addr5|curr_postcode|name2|debt_addr1|" & _
        "debt_addr2|debt_addr3|debt_addr4|debt_addr5|debt_pcode|comments" & vbNewLine
        outfile = outline

        'process cases delimited by a pipe
        For idx = 0 To lines - 1
            Dim amtstring As String
            Dim lostring, field As String
            Dim line_length As Integer = line(idx).Length
            Dim start_field_idx As Integer = 1
            Dim end_field_idx As Integer = 0
            Dim field_no As Integer = 0
            For idx2 = 1 To line_length
                If Mid(line(idx), idx2, 1) = "|" Or idx2 = line_length Then
                    field_no += 1
                    end_field_idx = idx2
                    field = Mid(line(idx), start_field_idx, end_field_idx - start_field_idx)
                    If Microsoft.VisualBasic.Left(field, 1) = Chr(10) Then
                        field = Microsoft.VisualBasic.Right(field, field.Length - 1)
                    End If
                    Select Case field_no
                        Case 1
                            clref = Trim(field)
                        Case 2
                            amtstring = field
                            If Not IsNumeric(amtstring) Then
                                errorfile = errorfile & "Line  " & idx & " - amount not numeric - " _
                                         & amtstring & vbNewLine
                            Else
                                amt = amtstring
                            End If
                        Case 3
                            off_number = field
                        Case 4
                            lostring = field
                            If Not IsDate(lostring) Then
                                errorfile = errorfile & "Line  " & idx & " - LO Date not valid - " _
                                                             & lostring & vbNewLine
                            Else
                                lodate = lostring
                            End If
                        Case 35
                            title(0) = field
                        Case 37
                            forename(0) = field
                        Case 38
                            surname(0) = field
                        Case 39
                            addr1(0) = field
                        Case 40
                            addr2(0) = field
                        Case 41
                            addr3(0) = field
                        Case 42
                            addr4(0) = field
                        Case 43
                            addr5(0) = field
                        Case 44
                            pcode(0) = field
                        Case 45
                            title(1) = field
                        Case 47
                            forename(1) = field
                        Case 48
                            surname(1) = field
                        Case 49
                            addr1(1) = field
                        Case 50
                            addr2(1) = field
                        Case 51
                            addr3(1) = field
                        Case 52
                            addr4(1) = field
                        Case 53
                            addr5(1) = field
                        Case 54
                            pcode(1) = field
                        Case 55
                            title(2) = field
                        Case 57
                            forename(2) = field
                        Case 58
                            surname(2) = field
                        Case 59
                            addr1(2) = field
                        Case 60
                            addr2(2) = field
                        Case 61
                            addr3(2) = field
                        Case 62
                            addr4(2) = field
                        Case 63
                            addr5(2) = field
                        Case 64
                            pcode(2) = field
                        Case 65
                            title(3) = field
                        Case 67
                            forename(3) = field
                        Case 68
                            surname(3) = field
                        Case 69
                            addr1(3) = field
                        Case 70
                            addr2(3) = field
                        Case 71
                            addr3(3) = field
                        Case 72
                            addr4(3) = field
                        Case 73
                            addr5(3) = field
                        Case 74
                            pcode(3) = field
                        Case 75
                            title(4) = field
                        Case 77
                            forename(4) = field
                        Case 78
                            surname(4) = field
                        Case 79
                            addr1(4) = field
                        Case 80
                            addr2(4) = field
                        Case 81
                            addr3(4) = field
                        Case 82
                            addr4(4) = field
                        Case 83
                            addr5(4) = field
                        Case 84
                            pcode(4) = field
                        Case 85
                            debt_addr(0) = field
                        Case 86
                            debt_addr(1) = field
                        Case 87
                            debt_addr(2) = field
                        Case 88
                            debt_addr(3) = field
                        Case 89
                            debt_addr(4) = field
                        Case 90
                            debt_pcode = Trim(Mid(line(idx), start_field_idx, end_field_idx - start_field_idx + 1))
                            
                    End Select
                    start_field_idx = idx2 + 1
                End If
            Next
            'validate case details
            If clref = Nothing Or Microsoft.VisualBasic.Left(clref, 3) = "   " Then
                errorfile = errorfile & "Line  " & idx2 & " - No client reference" & vbNewLine
            End If

            name2 = Trim(Trim(title(1)) & " " & Trim(forename(1)) & " " & Trim(surname(1)))
            If name2.Length > 0 Then
                comments = "address2:" & Trim(Trim(addr1(1)) & " " & Trim(addr2(1)) & " " & _
                    Trim(addr3(1)) & " " & Trim(addr4(1)) & " " & Trim(addr5(1)) & " " & Trim(pcode(1))) & "; "
            End If
            'save case in outline
            'see if name 2 to 4 are present
            For idx2 = 2 To 4
                Dim name_str As String = Trim(Trim(title(idx2)) & " " & Trim(forename(idx2)) & " " & _
                Trim(surname(idx2)))
                If name_str.Length > 0 Then
                    comments = comments & "name" & idx2 + 1 & ":" & name_str & "; "
                    comments = comments & "address" & idx2 + 1 & ":" & Trim(Trim(addr1(idx2)) & " " & _
                        Trim(addr2(idx2)) & " " & Trim(addr3(idx2)) & " " & Trim(addr4(idx2)) & " " & _
                        Trim(addr5(idx2)) & " " & Trim(pcode(idx2))) & "; "
                End If
            Next
            outfile = outfile & clref & "|" & amt & "|" & off_number & "|" & lodate & "|" & _
                        title(0) & "|" & forename(0) & "|" & surname(0) & "|" & addr1(0) & "|" & _
                        addr2(0) & "|" & addr3(0) & "|" & addr4(0) & "|" & addr5(0) & "|" & pcode(0) & "|" & _
                        name2 & "|" & debt_addr(0) & "|" & debt_addr(1) & "|" & debt_addr(2) & "|" & debt_addr(3) & _
                        "|" & debt_addr(4) & "|" & Trim(debt_pcode)
            Dim comments2 As String = ""
            Dim comments3 As String = comments
            If comments.Length <= 250 Then
                outfile = outfile & "|" & comments
            Else
                While comments3.Length > 250
                    Dim len As Integer = Microsoft.VisualBasic.Len(comments3)
                    For idx2 = 250 To 1 Step -1
                        If Mid(comments3, idx2, 1) = ";" Then
                            Exit For
                        End If
                    Next
                    comments2 = comments2 & Microsoft.VisualBasic.Left(comments3, idx2)
                    For idx2 = idx2 To 250
                        comments2 = comments2 & " "
                    Next
                    comments3 = Microsoft.VisualBasic.Right(comments3, len - idx2)
                End While
                outfile = outfile & "|" & comments2 & comments3
            End If
            outfile = outfile & vbNewLine

            'clear fields
            name2 = ""
            For idx2 = 0 To 4
                title(idx2) = ""
                forename(idx2) = ""
                surname(idx2) = ""
                addr1(idx2) = ""
                addr2(idx2) = ""
                addr3(idx2) = ""
                addr4(idx2) = ""
                addr5(idx2) = ""
                pcode(idx2) = ""
                debt_addr(idx2) = ""
                comments = ""
            Next
            debt_pcode = ""
            lodate = Nothing
            clref = ""
            amt = Nothing
            off_number = ""
        Next

        viewbtn.Enabled = True
        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText("error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
End Class

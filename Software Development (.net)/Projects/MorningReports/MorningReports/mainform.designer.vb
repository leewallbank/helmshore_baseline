<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainform
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ra078btn = New System.Windows.Forms.Button
        Me.ra078pcbtn = New System.Windows.Forms.Button
        Me.ra533btn = New System.Windows.Forms.Button
        Me.ra533pcbtn = New System.Windows.Forms.Button
        Me.ra537btn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'ra078btn
        '
        Me.ra078btn.Location = New System.Drawing.Point(65, 33)
        Me.ra078btn.Name = "ra078btn"
        Me.ra078btn.Size = New System.Drawing.Size(150, 23)
        Me.ra078btn.TabIndex = 0
        Me.ra078btn.Text = "RA078 FC Allocation"
        Me.ra078btn.UseVisualStyleBackColor = True
        '
        'ra078pcbtn
        '
        Me.ra078pcbtn.Location = New System.Drawing.Point(65, 77)
        Me.ra078pcbtn.Name = "ra078pcbtn"
        Me.ra078pcbtn.Size = New System.Drawing.Size(150, 23)
        Me.ra078pcbtn.TabIndex = 1
        Me.ra078pcbtn.Text = "RA078PC FC Allocation"
        Me.ra078pcbtn.UseVisualStyleBackColor = True
        '
        'ra533btn
        '
        Me.ra533btn.Location = New System.Drawing.Point(65, 126)
        Me.ra533btn.Name = "ra533btn"
        Me.ra533btn.Size = New System.Drawing.Size(150, 23)
        Me.ra533btn.TabIndex = 2
        Me.ra533btn.Text = "RA533 Consolidated Van"
        Me.ra533btn.UseVisualStyleBackColor = True
        Me.ra533btn.Visible = False
        '
        'ra533pcbtn
        '
        Me.ra533pcbtn.Location = New System.Drawing.Point(65, 177)
        Me.ra533pcbtn.Name = "ra533pcbtn"
        Me.ra533pcbtn.Size = New System.Drawing.Size(150, 23)
        Me.ra533pcbtn.TabIndex = 3
        Me.ra533pcbtn.Text = "RA533PC Consolidated Van"
        Me.ra533pcbtn.UseVisualStyleBackColor = True
        Me.ra533pcbtn.Visible = False
        '
        'ra537btn
        '
        Me.ra537btn.Location = New System.Drawing.Point(65, 225)
        Me.ra537btn.Name = "ra537btn"
        Me.ra537btn.Size = New System.Drawing.Size(150, 23)
        Me.ra537btn.TabIndex = 4
        Me.ra537btn.Text = "RA537 14 Day Letter"
        Me.ra537btn.UseVisualStyleBackColor = True
        Me.ra537btn.Visible = False
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(203, 290)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 6
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'mainform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(290, 342)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.ra537btn)
        Me.Controls.Add(Me.ra533pcbtn)
        Me.Controls.Add(Me.ra533btn)
        Me.Controls.Add(Me.ra078pcbtn)
        Me.Controls.Add(Me.ra078btn)
        Me.MaximizeBox = False
        Me.Name = "mainform"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Run Crystal Reports"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ra078btn As System.Windows.Forms.Button
    Friend WithEvents ra078pcbtn As System.Windows.Forms.Button
    Friend WithEvents ra533btn As System.Windows.Forms.Button
    Friend WithEvents ra533pcbtn As System.Windows.Forms.Button
    Friend WithEvents ra537btn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
End Class

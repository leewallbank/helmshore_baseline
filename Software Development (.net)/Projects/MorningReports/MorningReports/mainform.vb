Public Class mainform

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub ra078btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ra078btn.Click
        disable_buttons()
        Form1.ShowDialog()
        enable_buttons()
    End Sub

    Private Sub ra078pcbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ra078pcbtn.Click
        disable_buttons()
        Form2.ShowDialog()
        enable_buttons()
    End Sub

    Private Sub ra533btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ra533btn.Click
        'disable_buttons()
        'Form3.ShowDialog()
        'enable_buttons()
    End Sub

    Private Sub ra537btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ra537btn.Click
        'disable_buttons()
        'Form5.ShowDialog()
        'enable_buttons()
    End Sub

    Private Sub ra533pcbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ra533pcbtn.Click
        'disable_buttons()
        'Form4.ShowDialog()
        'enable_buttons()
    End Sub
    Private Sub disable_buttons()
        ra078btn.Enabled = False
        ra078pcbtn.Enabled = False
        ra533btn.Enabled = False
        ra533pcbtn.Enabled = False
        ra537btn.Enabled = False
        exitbtn.Enabled = False
        'ra503btn.Enabled = False
    End Sub
    Private Sub enable_buttons()
        ra078btn.Enabled = True
        ra078pcbtn.Enabled = True
        'ra533btn.Enabled = True
        'ra533pcbtn.Enabled = True
        'ra537btn.Enabled = True
        exitbtn.Enabled = True
        'ra503btn.Enabled = True
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If MsgBox("This takes about half an hour to run - OK to Run?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Exit Sub
        End If
        disable_buttons()
        Form6.ShowDialog()
        enable_buttons()
    End Sub
End Class
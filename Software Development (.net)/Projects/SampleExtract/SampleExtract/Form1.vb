Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub createbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles createbtn.Click
        createbtn.Enabled = False
        exitbtn.Enabled = False
        ProgressBar1.Value = 5
        Dim file As String
        Dim idx As Integer
        Dim found_cases As Integer = 0
        'debtor table
        'file  = "Debtor|linkid|csid|cl_ref|cl_batch|offence_date|offence_court|" & _
        '"debt_amt|debt_costs|debt_fees|debt_vat|debt_balance|bailiffid|bail_allocated|" & _
        '"bail_current|bail_visited|bail_visits|bail_levied|bail_walker|last_stageid|" & _
        '"last_date|open/closed|status|next_date|next_days|arrange_started|arrange_amt|arrange_interval|" & _
        '"arrange_next|arrange_broken|return_date|return_codeid|return_details|return_fee|retern_remitid|" & _
        '"return_form|created_date|status_hold|hold_expires|period|year|arrange_giro|hold_client|" & _
        '"iscompany|onincsupport|isvulnerable|addcleaned|addconfirmed|status_code|" & _
        '"bail_seq|week|image_count|addscore|addband|isonjsa|court_name|makemodel|offence_from|" & _
        '"offence_typeid|prev_ref|offence_location|jsaproof|cs_retnid|arrange_broken_count|" & _
        '"offence_model|offence_colour|offence_court_code|debt_orig|arrange_init_amt|arrange_init due|" & _
        '"cl period|retn_lastagentid|retn_last_status|debt_paid|hold_date|trace_date|offence value|comm rate|" & vbNewLine
        ''get latest 1000 open debtors
        'param1 = "onestep"
        'param2 = "select * from Debtor where status_open_closed = 'O' " & _
        '" and _rowid > 5250000 order by _rowid desc "
        'Dim debtor_dataset As DataSet = get_dataset(param1, param2)
        'Dim debtor_rows As Integer = no_of_rows - 1
       
        'For idx = 0 To debtor_rows
        '    ProgressBar1.Value = (found_cases / 1000) * 100
        '    Dim csid As Integer = debtor_dataset.Tables(0).Rows(idx).Item(17)
        '    'check branch - just want 1
        '    param2 = "select branchID from ClientScheme where _rowid = " & csid
        '    Dim cs_dataset As DataSet = get_dataset(param1, param2)
        '    If cs_dataset.Tables(0).Rows(0).Item(0) <> 1 Then
        '        Continue For
        '    End If
        '    found_cases += 1
        '    Dim idx2 As Integer
        '    For idx2 = 0 To 116
        '        If (idx2 < 2 Or idx2 > 16) And (idx2 < 81 Or idx2 > 87) And idx2 <> 22 And idx2 <> 23 And idx2 <> 24 _
        '        And idx2 <> 62 And idx2 <> 78 And idx2 <> 89 And idx2 <> 90 And idx2 <> 95 And idx2 <> 98 _
        '        And idx2 <> 72 And idx2 <> 73 And idx2 <> 108 _
        '        And idx2 <> 51 And idx2 <> 55 And idx2 <> 57 And idx2 <> 64 And idx2 <> 69 Then
        '            Dim field
        '            Try
        '                field = Trim(debtor_dataset.Tables(0).Rows(idx).Item(idx2))
        '                field = remove_chars(field)
        '            Catch ex As Exception
        '                field = Nothing
        '            End Try
        '            file = file & field & "|"
        '        End If
        '    Next
        '    file = file & vbNewLine
        '    If found_cases = 1000 Then
        '        Exit For
        '    End If
        'Next
        'With SaveFileDialog1
        '    .Title = "Save file"
        '    .Filter = "TXT files |*.txt"
        '    .DefaultExt = ".txt"
        '    .OverwritePrompt = True
        '    .InitialDirectory = "c:\temp"
        '    .FileName = "debtor_sample.txt"
        'End With
        'If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
        '    My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
        'End If

        'payment table
        'file = "rowid|date|date_period|date_year|batchid|receipt_issued|debtorid|" & _
        '        "stageid|schemeid|bailiffid|amount|amt_typeid|amt_sourceid|" & _
        '        "amt_writeoff|amt_specialed|amt_fee|status|status_date|status_fee|" & _
        '        "status_remitid|split_debt|split_costs|split_fees|split_other|split_van|split_vat|" & _
        '        "split_preallocated|created_date|receipt_number|tracetext|prearrangement|date_week|" & _
        '        "accountedfor|commaccountedfor|payment_code|exception_flag|comm_amt|exception_no|comm-client_rate|" & _
        '        "amt_handling_fee|clientscheme_delivery" & vbNewLine
        ''get latest 1000 open debtors
        'param1 = "onestep"
        'param2 = "select * from Payment   " & _
        '"  where _rowid > 64000000 order by _rowid desc "
        'Dim pay_dataset As DataSet = get_dataset(param1, param2)
        'Dim pay_rows As Integer = no_of_rows - 1

        'found_cases = 0
        'For idx = 0 To pay_rows
        '    ProgressBar1.Value = (found_cases / 1000) * 100
        '    Dim csid As Integer = pay_dataset.Tables(0).Rows(idx).Item(8)
        '    'check branch - just want 1
        '    param2 = "select branchID from ClientScheme where _rowid = " & csid
        '    Dim cs_dataset As DataSet = get_dataset(param1, param2)
        '    If cs_dataset.Tables(0).Rows(0).Item(0) <> 1 Then
        '        Continue For
        '    End If
        '    found_cases += 1
        '    Dim idx2 As Integer
        '    For idx2 = 0 To 45
        '        If idx2 = 13 Or idx2 = 19 Or idx2 = 29 Or idx2 = 31 Or idx2 = 42 Then
        '            Continue For
        '        End If
        '        Dim field
        '        Try
        '            field = Trim(pay_dataset.Tables(0).Rows(idx).Item(idx2))
        '            field = remove_chars(field)
        '        Catch ex As Exception
        '            field = Nothing
        '        End Try
        '        file = file & field & "|"
        '    Next
        '    file = file & vbNewLine
        '    If found_cases = 1000 Then
        '        Exit For
        '    End If
        'Next
        'With SaveFileDialog1
        '    .Title = "Save file"
        '    .Filter = "TXT files |*.txt"
        '    .DefaultExt = ".txt"
        '    .OverwritePrompt = True
        '    .InitialDirectory = "c:\temp"
        '    .FileName = "payment_sample.txt"
        'End With
        'If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
        '    My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
        'End If

        'visit table
        'file = "rowid|debtorid|bailiffid|type|date_allocated|visited|date_visited|" & _
        '        "prop_colour|prop_lock_type|prop_grade|signed_levy|signed_wp|signed_arrange|" & _
        '        "amt_collected|created_date|date_period|date_year|receipt_no|mileage|" & _
        '        "accountedfor|check_vrm|bail_sequence|goods_removed|stage_name|date_week|clamped|" & _
        '        "exception_flag|visited_year|visited_period|visited_week|prop_type|clientscheme_delivery" & vbNewLine
        'param1 = "onestep"
        'param2 = "select * from Visit   " & _
        '"  where _rowid > 64000000 order by _rowid desc "
        'Dim visit_dataset As DataSet = get_dataset(param1, param2)
        'Dim visit_rows As Integer = no_of_rows - 1

        'found_cases = 0
        'For idx = 0 To visit_rows
        '    ProgressBar1.Value = (found_cases / 1000) * 100
       
        '    found_cases += 1
        '    Dim idx2 As Integer
        '    For idx2 = 0 To 37
        '        If idx2 = 14 Or idx2 = 16 Or (idx2 > 28 And idx2 < 33) Then
        '            Continue For
        '        End If
        '        Dim field
        '        Try
        '            field = Trim(visit_dataset.Tables(0).Rows(idx).Item(idx2))
        '            field = remove_chars(field)
        '        Catch ex As Exception
        '            field = Nothing
        '        End Try
        '        file = file & field & "|"
        '    Next
        '    file = file & vbNewLine
        '    If found_cases = 1000 Then
        '        Exit For
        '    End If
        'Next
        'With SaveFileDialog1
        '    .Title = "Save file"
        '    .Filter = "TXT files |*.txt"
        '    .DefaultExt = ".txt"
        '    .OverwritePrompt = True
        '    .InitialDirectory = "c:\temp"
        '    .FileName = "visit_sample.txt"
        'End With
        'If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
        '    My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
        'End If


        'fees table
        'file = "rowid|debtorid|bailiffid|type|date|date_end|" & _
        '        "fee_amt|fee_basis|fee_minimum|fee_maximum|fee_daily|" & _
        '        "fee_nom_accid|fee_vat_amt|fee_vat_rate|fee_vat_whopays|remit_priority|remit_col|" & _
        '        "comm_amt|comm_basis|comm_minimum|comm_maximum|comm_nom_accid|comm_paid|remited_fee|" & _
        '        "remited_vat|created_date|date_period|date_year|fee_daily_on_amt|date_week|" & _
        '        "fee_who_pays|paired_fees|exception_flag|include_levy_balance|clientschemeid|" & _
        '        "fee_original_amt|fee_original_vat|clientscheme_delivery" & vbNewLine
        'param1 = "onestep"
        'param2 = "select * from Fee   " & _
        '"  where _rowid > 64300000 order by _rowid desc "
        'Dim fee_dataset As DataSet = get_dataset(param1, param2)
        'Dim fee_rows As Integer = no_of_rows - 1

        'found_cases = 0
        'For idx = 0 To fee_rows
        '    ProgressBar1.Value = (found_cases / 1000) * 100
        '    Dim csid As Integer = fee_dataset.Tables(0).Rows(idx).Item(36)
        '    'check branch - just want 1
        '    param2 = "select branchID from ClientScheme where _rowid = " & csid
        '    Dim cs_dataset As DataSet = get_dataset(param1, param2)
        '    If cs_dataset.Tables(0).Rows(0).Item(0) <> 1 Then
        '        Continue For
        '    End If
        '    found_cases += 1
        '    Dim idx2 As Integer
        '    For idx2 = 0 To 39
        '        If idx2 = 25 Or idx2 = 27 Then
        '            Continue For
        '        End If
        '        Dim field
        '        Try
        '            field = Trim(fee_dataset.Tables(0).Rows(idx).Item(idx2))
        '            field = remove_chars(field)
        '        Catch ex As Exception
        '            field = Nothing
        '        End Try
        '        file = file & field & "|"
        '    Next
        '    file = file & vbNewLine
        '    If found_cases = 1000 Then
        '        Exit For
        '    End If
        'Next
        'With SaveFileDialog1
        '    .Title = "Save file"
        '    .Filter = "TXT files |*.txt"
        '    .DefaultExt = ".txt"
        '    .OverwritePrompt = True
        '    .InitialDirectory = "c:\temp"
        '    .FileName = "fee_sample.txt"
        'End With
        'If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
        '    My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
        'End If

        'notes table
        file = "rowid|debtorid|type|created_date|date_period|date_year|" & _
                "clientscheme_delivery" & vbNewLine
        param1 = "onestep"
        param2 = "select * from Note   " & _
        "  where _rowid > 64300000 order by _rowid desc "
        Dim note_dataset As DataSet = get_dataset(param1, param2)
        Dim note_rows As Integer = no_of_rows - 1

        found_cases = 0
        For idx = 0 To note_rows
            ProgressBar1.Value = (found_cases / 1000) * 100
            found_cases += 1
            Dim idx2 As Integer
            For idx2 = 0 To 11
                If (idx2 > 2 And idx2 < 7) Or idx2 = 8 Then
                    Continue For
                End If
                Dim field
                Try
                    field = Trim(note_dataset.Tables(0).Rows(idx).Item(idx2))
                    field = remove_chars(field)
                Catch ex As Exception
                    field = Nothing
                End Try
                file = file & field & "|"
            Next
            file = file & vbNewLine
            If found_cases = 1000 Then
                Exit For
            End If
        Next
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "TXT files |*.txt"
            .DefaultExt = ".txt"
            .OverwritePrompt = True
            .InitialDirectory = "c:\temp"
            .FileName = "note_sample.txt"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
        End If
        Me.Close()

    End Sub
    Function remove_chars(ByVal field As String) As String
        Dim idx As Integer
        Dim retn_str As String = ""
        Dim char1 As String
        For idx = 1 To field.Length
            char1 = Mid(field, idx, 1)
            If char1 <> Chr(10) And _
                            char1 <> Chr(13) Then
                retn_str = retn_str & char1
            End If
        Next
        Return (retn_str)
    End Function
End Class

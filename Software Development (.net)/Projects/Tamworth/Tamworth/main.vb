Public Class mainform
    Dim outline As String = ""
    Dim amt As Decimal
    Dim lodate As Date
    Dim clref As String = ""
    Dim case_num As Integer
    Dim addr1 As String
    Dim addr2 As String
    Dim addr3 As String
    Dim second_address As String

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Excel files | *.xls"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                load_vals(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here

        Dim idx As Integer
        Dim second_name As String
        Dim notes As String = ""
        Dim add_notes As String = ""
        Dim second_name_found As Boolean = False

        'write out headings
        For idx = 1 To finalcol
            outline = outline & vals(1, idx) & "|"
        Next
        outfile = outline & "Second name" & "|" & "Address Notes" & "|" & "Notes" & vbNewLine

        'check if duplicate record found
        For idx = 2 To finalrow
            second_name = ""
            If idx = 2 Then
                save_outline(idx)
            Else
                If clref = vals(idx, 1) And amt = vals(idx, 15) And case_num = vals(idx, 16) _
                And lodate = vals(idx, 2) Then
                    'second name found
                    add_notes = ""
                    second_name = vals(idx, 17) & " " & vals(idx, 18) & " " & vals(idx, 19) & " " & vals(idx, 20)
                    second_name = Trim(second_name)
                    If second_name_found Then
                        'add to notes as second name field already used
                        notes = notes & "Also " & second_name & " "
                    Else
                        outline = outline & second_name & "|"
                    End If

                    'check if address is different
                    second_address = Nothing
                    If vals(idx, 3) <> Nothing Then
                        If addr1 <> vals(idx, 3) Or addr2 <> vals(idx, 4) Then
                            second_address = vals(idx, 3) & " " & vals(idx, 4) & " " & _
                                             vals(idx, 5) & " " & vals(idx, 6) & " " & vals(idx, 7)
                            second_address = Trim(second_address)
                        End If
                    Else
                        If addr1 <> vals(idx, 8) Or addr2 <> vals(idx, 9) Then
                            second_address = vals(idx, 8) & " " & vals(idx, 9) & " " & _
                                             vals(idx, 10) & " " & vals(idx, 11) & " " & vals(idx, 12)
                            second_address = Trim(second_address)
                        End If
                    End If
                    If second_address <> Nothing Then
                        If second_name_found Then
                            'add to notes as second name field already used
                            notes = notes & " at " & second_address & " "
                        Else
                            add_notes = second_name & " at " & second_address
                            outline = outline & add_notes & "|"
                        End If
                    Else
                        If second_name_found = False Then
                            outline = outline & "|"
                        End If
                    End If
                    second_name_found = True
                Else
                    If outline <> "" Then
                        outfile = outfile & outline & notes & vbNewLine
                    End If
                    save_outline(idx)
                    second_name_found = False
                    notes = ""
                End If
            End If
        Next

        'save last outline to outfile
        If outline <> "" Then
            outfile = outfile & outline & notes & vbNewLine
        End If
        viewbtn.Enabled = True
        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText("error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
    End Sub

    Private Sub save_outline(ByVal idx As Integer)
        Dim idx2 As Integer
        outline = ""
        clref = vals(idx, 1)
        amt = vals(idx, 15)
        case_num = vals(idx, 16)
        lodate = vals(idx, 2)
        If vals(idx, 3) <> Nothing Then
            addr1 = vals(idx, 3)
            addr2 = vals(idx, 4)
        Else
            addr1 = vals(idx, 8)
            addr2 = vals(idx, 9)
        End If
        
        For idx2 = 1 To finalcol
            outline = outline & vals(idx, idx2) & "|"
        Next
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
End Class

Public Class mainfrm
    Dim filename, outfile As String
    Dim ascii As New System.Text.ASCIIEncoding()
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        conn_open = False
        openbtn.Enabled = False
        exitbtn.Enabled = False
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "txt files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                Me.Close()
                Exit Sub
            End Try
        Else
            MsgBox("File not opened")
            Me.Close()
            Exit Sub
        End If
        'do manipulation here
        Dim file As String = ""
        Dim field As String = ""
        Dim linetext As String = ""
        Dim line(0) As String

        Dim idx, idx2, debtorID As Integer
        Dim lines As Integer = 0
        Dim remit_ref, emp_NI, client_ref As String
        Dim csID, remitID, schID, clID As Integer
        Dim remit_date As Date
        outfile = ""
        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                If Mid(linetext, 2, 11) = ",,,,,,,,,,," Then
                    Continue For
                End If
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next
        'fields comma separated
        For idx = 0 To lines - 1
            ProgressBar1.Value = (idx / lines) * 100
            Application.DoEvents()
            Dim start_idx As Integer = 1
            Dim field_no As Integer = 1
            For field_no = 1 To 7
                For idx2 = start_idx To line(idx).Length

                    If Mid(line(idx), idx2, 1) = "," Then
                        field = Trim(Mid(line(idx), start_idx, idx2 - start_idx))
                        start_idx = idx2 + 1
                        Exit For
                    End If
                Next
                If idx2 >= line(idx).Length Then   'last column reached
                    field = Microsoft.VisualBasic.Right(line(idx), line(idx).Length - start_idx)
                End If
                Dim idx3 As Integer
                Dim new_field As String = ""
                For idx3 = 1 To field.Length
                    If Mid(field, idx3, 1) <> "," Then
                        new_field = new_field & Mid(field, idx3, 1)
                    End If
                Next
                field = Trim(new_field)
                
                If Microsoft.VisualBasic.Left(field, 1) = Chr(10) Then
                    field = Microsoft.VisualBasic.Right(field, field.Length - 1)
                End If
                field = Replace(field, "(", "")
                field = Replace(field, ")", "")
                field = Replace(field, "-", "")
                field = Replace(field, "&", "")
                field = Replace(field, "~", "")
                'remove any quotes
                new_field = ""
                If field <> Nothing Then
                    If field.Length > 35 Then
                        field = Microsoft.VisualBasic.Left(field, 35)
                    End If
                    For idx3 = 1 To field.Length
                        If Mid(field, idx3, 1) <> """" Then
                            new_field = new_field & Mid(field, idx3, 1)
                        End If
                    Next
                End If
                field = new_field
                Select Case field_no
                    Case 1
                        If field = Nothing Then
                            Exit For
                        End If
                        If field.Length < 2 Then
                            Exit For
                        End If
                        outfile = outfile & field & ","
                    Case 2
                        Try
                            remitID = field
                        Catch ex As Exception
                            Continue For
                        End Try

                        param2 = "select clientSchemeID, date from Remit where _rowid = " & remitID
                        Dim remit_dataset As DataSet = get_dataset("onestep", param2)
                        If no_of_rows = 0 Then
                            MsgBox("remit No = " & remitID & " not found")
                            Me.Close()
                            Exit Sub
                        End If
                        csID = remit_dataset.Tables(0).Rows(0).Item(0)
                        remit_date = remit_dataset.Tables(0).Rows(0).Item(1)
                        remit_ref = ""
                        ' param2 = "select partnerCode2, schemeID, clientID from clientScheme where _rowid = " & csID
                        param2 = "select bankAccCode, schemeID, clientID from clientScheme where _rowid = " & csID
                        Dim csid_dataset As DataSet = get_dataset("onestep", param2)
                        If no_of_rows = 0 Then
                            MsgBox("Unable to find client scheme ID = " & csID)
                            Me.Close()
                            Exit Sub
                        End If
                        schID = csid_dataset.Tables(0).Rows(0).Item(1)
                        clID = csid_dataset.Tables(0).Rows(0).Item(2)
                        Try
                            remit_ref = csid_dataset.Tables(0).Rows(0).Item(0)
                        Catch ex As Exception
                            remit_ref = ""
                        End Try
                        Try
                            remit_ref = remit_ref
                        Catch ex As Exception
                            remit_ref = ""
                        End Try
                        If remit_ref = "." Then
                            remit_ref = ""
                        End If
                        If LCase(remit_ref) = "scheme" Then
                            param2 = "select name from Scheme where _rowid = " & schID
                            Dim sch_dataset As DataSet = get_dataset("onestep", param2)
                            If no_of_rows = 0 Then
                                MsgBox("Unable to find scheme no = " & schID & " for csid = " & csID)
                                Me.Close()
                                Exit Sub
                            End If
                            remit_ref = Trim(sch_dataset.Tables(0).Rows(0).Item(0) & remitID)
                            remit_ref = Replace(remit_ref, " ", "")
                        End If
                        If csID = 1619 Or csID = 1620 Or csID = 1621 Or csID = 1622 Or csID = 3092 Then 'wrexham
                            param2 = "select name from Scheme where _rowid = " & schID
                            Dim sch_dataset As DataSet = get_dataset("onestep", param2)
                            If no_of_rows = 0 Then
                                MsgBox("Unable to find scheme no = " & schID & " for csid = " & csID)
                                Me.Close()
                                Exit Sub
                            End If
                            remit_ref = "Ross-" & remitID & Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                            remit_ref = Replace(remit_ref, " ", "")
                            remit_ref = Replace(remit_ref, "Wales", "")
                        End If

                        If csID = 2111 Or csID = 2129 Or csID = 2132 Or csID = 2133 Or csID = 2130 Or _
                           csID = 2131 Or (csID >= 2672 And csID <= 2687) Or (csID >= 2700 And csID <= 2710) Then  'CSA
                            param2 = "select debtorID from Payment where status_remitID = " & remitID
                            Dim pay_dataset As DataSet = get_dataset("onestep", param2)
                            If no_of_rows = 0 Then
                                MsgBox("Payment not found for remit ID = " & remitID)
                                Me.Close()
                                Exit Sub
                                'ElseIf no_of_rows > 1 Then
                                '    MsgBox("Multiple payments found for remit ID = " & remitID)
                                '    Me.Close()
                                '    Exit Sub
                            End If
                            debtorID = pay_dataset.Tables(0).Rows(0).Item(0)
                            param2 = "select empNI, client_ref from Debtor where _rowid = " & debtorID
                            Dim debt_dataset As DataSet = get_dataset("onestep", param2)
                            If no_of_rows = 0 Then
                                MsgBox("Unable to read debtor = " & debtorID)
                                Me.Close()
                                Exit Sub
                            End If
                            Try
                                client_ref = Trim(debt_dataset.Tables(0).Rows(0).Item(1))
                            Catch ex As Exception
                                MsgBox("client ref is blank for case number = " & debtorID)
                                Me.Close()
                                Exit Sub
                            End Try
                            If Microsoft.VisualBasic.Left(client_ref, 2) = "32" Then
                                Try
                                    emp_NI = debt_dataset.Tables(0).Rows(0).Item(0)
                                Catch ex As Exception
                                    emp_NI = ""
                                End Try
                                If emp_NI = "" Then
                                    MsgBox("NI Number is blank for case number = " & debtorID)
                                    Me.Close()
                                    Exit Sub
                                End If
                                remit_ref = Trim(emp_NI)
                            ElseIf Microsoft.VisualBasic.Left(client_ref, 1) = "1" _
                            Or Microsoft.VisualBasic.Left(client_ref, 1) = "7" Then
                                remit_ref = client_ref
                            ElseIf Microsoft.VisualBasic.Left(client_ref, 3) = "NST" Then
                                remit_ref = client_ref
                            Else
                                MsgBox("Don't know how to work out remit ref for debtor = " & debtorID)
                                Me.Close()
                                Exit Sub
                            End If
                        End If
                        If clID = 1003 Then 'cullimore duttan
                            param2 = "select debtorID from Payment where status_remitID = " & remitID
                            Dim pay_dataset As DataSet = get_dataset("onestep", param2)
                            If no_of_rows = 0 Then
                                MsgBox("Payment not found for remit ID = " & remitID)
                                Me.Close()
                                Exit Sub
                            End If
                            debtorID = pay_dataset.Tables(0).Rows(0).Item(0)
                            param2 = "select client_ref from Debtor where _rowid = " & debtorID
                            Dim debt_dataset As DataSet = get_dataset("onestep", param2)
                            If no_of_rows = 0 Then
                                MsgBox("Unable to read debtor = " & debtorID)
                                Me.Close()
                                Exit Sub
                            End If
                            remit_ref = debt_dataset.Tables(0).Rows(0).Item(0)
                        End If
                        If clID = 1523 Then  'BT
                            remit_ref = "RCLBTGroup" & Format(remit_date, "ddMMyyyy")
                        End If
                        If remit_ref = "" Then
                            remit_ref = field
                        End If
                        remit_ref = Replace(remit_ref, "ddmmyyyy", Format(remit_date, "ddMMyyyy"))
                        remit_ref = Replace(remit_ref, "ddmmyy", Format(remit_date, "ddMMyy"))
                        If remit_ref.Length > 18 Then
                            remit_ref = Microsoft.VisualBasic.Left(remit_ref, 18)
                        End If
                        outfile = outfile & remit_ref & ","
                    Case 3
                        If field.Length = 0 Then
                            MsgBox("No sortcode for  line no = " & idx + 1)
                            Me.Close()
                            Exit Sub
                        End If
                        outfile = outfile & field & ","
                    Case 4
                        If field.Length = 0 Then
                            MsgBox("No account number for  line no = " & idx + 1)
                            Me.Close()
                            Exit Sub
                        End If
                        outfile = outfile & field & ","
                    Case 5
                        outfile = outfile & field & ","
                    Case 6
                        outfile = outfile & field & ","
                    Case 7
                        outfile = outfile & field & vbNewLine
                End Select
            Next
        Next
        Dim filename_prefix As String = ""
        For idx = Len(filename) To 1 Step -1
            If Mid(filename, idx, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_postprocess.txt", outfile, False, ascii)
        MsgBox("Postprocess file written")
        Me.Close()
    End Sub
End Class

Public Class mainform
    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "csv/txt files | *.txt;*.csv"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If MsgBox("Collect trace?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            collect_trace = True
        Else
            collect_trace = False
        End If
        reformbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, addr1, addr2, addr3, addr4, addr5 As String
        Dim debtorID As Integer
        Dim linetext As String = ""
        Dim line(0) As String
       
        Dim idx, idx2 As Integer
        Dim lines As Integer = 0

        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                If Mid(linetext, 2, 11) = ",,,,,,,,,,," Then
                    Continue For
                End If
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outfile = vbTab & "DebtorID" & vbTab & vbTab & "M" & vbTab & vbTab & vbTab & vbTab & vbTab & _
        vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & _
        vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & _
        vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & "Address1" & vbTab & "Address2" & _
        vbTab & "Address3" & vbTab & "Address4" & vbTab & "Address5" & vbNewLine
        Dim field As String = ""
        'fields comma separated
        For idx = 1 To lines - 1
            'ignore first line as it is a heading
            Dim start_idx As Integer = 1
            Dim field_no As Integer = 1
            'ignore commas inside quotes
            Dim inside_quote As Boolean = False
            debtorID = 0
            addr1 = ""
            addr2 = ""
            addr3 = ""
            addr4 = ""
            addr5 = ""
            For field_no = 1 To 47
                For idx2 = start_idx To line(idx).Length
                    If Mid(line(idx), idx2, 1) = """" Then
                        If inside_quote Then
                            inside_quote = False
                        Else
                            inside_quote = True
                        End If
                    End If
                    If Mid(line(idx), idx2, 1) = "," And inside_quote = False Then
                        field = Trim(Mid(line(idx), start_idx, idx2 - start_idx))
                        start_idx = idx2 + 1
                        Exit For
                    End If
                Next
                Dim idx3 As Integer
                Dim new_field As String = ""
                For idx3 = 1 To field.Length
                    If Mid(field, idx3, 1) <> """" And _
                       Mid(field, idx3, 1) <> "," Then
                        new_field = new_field & Mid(field, idx3, 1)
                    End If
                Next
                field = new_field
                Select Case field_no
                    Case 2  'debtorid
                        debtorID = field
                    Case 4 'result
                        If field <> "M" Then
                            debtorID = 0
                            Exit For
                        End If
                    Case 31 'primary match col AE
                        If field <> "Y" Then
                            debtorID = 0
                            Exit For
                        End If
                    Case 37 'column AK
                        If field.Length > 0 Then
                            addr1 = field
                        End If
                    Case 38 'column AL
                        If field.Length > 0 Then
                            If addr1.Length > 0 Then
                                addr1 = addr1 & " " & field
                            Else
                                addr1 = field
                            End If
                        End If
                    Case 39 'column AM
                        If field.Length > 0 Then
                            If addr1.Length > 0 Then
                                addr1 = addr1 & " " & field
                            Else
                                addr1 = field
                            End If
                        End If
                    Case 40 'column AN
                        If field.Length > 0 Then
                            If addr1.Length > 0 Then
                                addr1 = addr1 & " " & field
                            Else
                                addr1 = field
                            End If
                        End If
                    Case 41 'column AO
                        addr2 = field
                    Case 42 'column AP
                        If field.Length > 0 Then
                            If addr2.Length > 0 Then
                                addr2 = addr2 & " " & field
                            Else
                                addr2 = field
                            End If
                        End If
                    Case 43 'column AQ
                        If field.Length > 0 Then
                            If addr2.Length > 0 Then
                                addr2 = addr2 & " " & field
                            Else
                                addr2 = field
                            End If
                        End If
                    Case 44 'column AR
                        If field.Length > 0 Then
                            addr3 = field
                        End If
                    Case 45 'column AS
                        If field.Length > 0 Then
                            addr4 = field
                        End If
                    Case 46 'column AT
                        If field.Length > 0 Then
                            addr5 = field
                        End If
                    Case 47 'column AU
                        If field.Length > 0 And collect_trace Then
                            addr5 = Trim(addr5 & " " & field)
                        End If
                End Select
            Next
            If debtorID > 0 Then
                If addr2.Length = 0 Then
                    addr2 = addr3
                    addr3 = addr4
                    addr4 = addr5
                    addr5 = ""
                    If addr2.Length = 0 Then
                        addr2 = addr3
                        addr3 = addr4
                        addr4 = ""
                    End If
                ElseIf addr3.Length = 0 Then
                    addr3 = addr4
                    addr4 = addr5
                    addr5 = ""
                End If

                outfile = outfile & vbTab & debtorID & vbTab & vbTab & "M" & vbTab & vbTab & vbTab & vbTab & vbTab & _
                        vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & _
                vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & _
                vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & addr1 & vbTab & addr2 & _
                vbTab & addr3 & vbTab & addr4 & vbTab & addr5 & vbNewLine
            End If
        Next
        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_successfultrace.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
        MsgBox("File saved")
        Me.Close()
    End Sub

    Private Sub checkbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

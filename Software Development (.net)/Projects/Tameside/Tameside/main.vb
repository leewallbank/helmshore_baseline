Public Class mainform
    Public wholename, addr1, addr2, addr3, postcode, comments, outline, clref As String
    Public debt_amt As Decimal
    Public lodate As Date
    Public rowidx As Integer
    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
        ProgressBar1.Value = 0
        TextBox1.Text = "Opening file"
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "XLS files|*.xls"
            .FileName = ""
            .CheckFileExists = True
        End With
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MessageBox.Show("No file Selected")
            Exit Sub
        End If
        load_vals(OpenFileDialog1.FileName)
        If finalrow > 0 Then
            reformbtn.Enabled = True
        End If
        TextBox1.Text = "File opened"
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        errorfile = Nothing
        outfile = Nothing
        ProgressBar1.Value = 0
        'write out headings
        outline = "WholeName|Address1|Address2|Address3|Postcode|Comments|LO Date|Client Ref|Debt Amount" & vbNewLine
        outfile = outline
        'read array to get name in column 1

        For rowidx = 2 To finalrow
            ProgressBar1.Value = rowidx / finalrow * 100
            If vals(rowidx, 1) = "Name" Then
                case_found()
            End If
        Next

        viewbtn.Enabled = True
        Dim namelength = Len(OpenFileDialog1.FileName)
        Dim filename_prefix As String = ""
        filename_prefix = Microsoft.VisualBasic.Left(OpenFileDialog1.FileName, namelength - 4)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
    End Sub

    Public Sub case_found()
        Dim lostring As String
        lodate = Nothing
        debt_amt = -1
        addr1 = Nothing
        addr2 = Nothing
        addr3 = Nothing
        wholename = Nothing
        postcode = Nothing
        comments = Nothing

        'validate case details
        rowidx += 1
        wholename = vals(rowidx, 1)

        addr1 = vals(rowidx, 2)
        addr2 = vals(rowidx, 3)
        addr3 = vals(rowidx, 4)
        postcode = vals(rowidx, 5)
        comments = Trim(vals(rowidx, 6))
        Dim last_comments As String = comments
        lostring = vals(rowidx, 7)
        clref = vals(rowidx, 8)
        Try
            lodate = CDate(lostring)
        Catch ex As Exception
            errorfile = errorfile & "Line  " & rowidx & " - Invalid LO date" & vbNewLine
            Exit Sub
        End Try

        If clref = Nothing Then
            errorfile = errorfile & "Line  " & rowidx & " - No client reference" & vbNewLine
            Exit Sub
        End If
        Name = ""

        'get debt amount from sum line
        
        Dim endrow As Integer = rowidx + 10
        If endrow > finalrow Then
            endrow = finalrow
        End If
        For rowidx = rowidx To endrow

            If vals(rowidx, 1) = "Name" Then
                rowidx -= 1
                Exit For
            End If
            If vals(rowidx, 6) <> Nothing Then
                If Trim(vals(rowidx, 6)) <> last_comments Then
                    comments = comments & " " & Trim(vals(rowidx, 6))
                    last_comments = Trim(vals(rowidx, 6))
                End If
            End If

            If vals(rowidx, 8) = "Sum:" Then
                Try
                    debt_amt = vals(rowidx, 9)
                Catch ex As Exception
                    debt_amt = -1
                End Try
                Exit For
            End If
        Next
        If debt_amt = -1 Then
            errorfile = errorfile & "Line  " & rowidx - 1 & " - INVALID DEBT AMOUNT" & vbNewLine
            Exit Sub
        ElseIf debt_amt = 0 Then
            errorfile = errorfile & "Line  " & rowidx - 1 & " - DEBT AMOUNT = 0 " & vbNewLine
        End If
        'save case in outline
        outline = wholename & "|" & addr1 & "|" & addr2 & "|" & addr3 _
         & "|" & postcode & "|" & comments & "|" & lodate & "|" & clref & "|" & debt_amt & _
          vbNewLine
        outfile = outfile & outline

    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub


    Private Sub checkbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

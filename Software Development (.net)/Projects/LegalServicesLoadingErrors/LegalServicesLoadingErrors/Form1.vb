Public Class Form1
    Dim new_file As String = ""
    Dim change_file As String
    Dim ln2 As Integer = Microsoft.VisualBasic.Len(filename)
    Dim debtor As Integer
    Dim first_time As Boolean = True
    Dim filetext As String
    Dim record As String = ""
    Dim filename, filename_id, filename_id_tag As String
    Dim first_error As Boolean = True
    Dim first_change As Boolean = True
    Dim on_onestep As Boolean
    Dim contrib_id As Double
    Dim no_changes As Integer = 0
    Dim changes As Integer = 0
    Dim applicant_id, summons_no, maat_id, first_name, surname, dob, ni_no As String
    Dim mthly_contrib_amt_str, upfront_contrib_amt_str, income_contrib_cap_str, income_uplift_applied As String
    Dim case_type, in_court_custody, debt_addr1, debt_addr2, debt_addr3, debt_addr4, debt_postcode As String
    Dim postal_addr1, postal_addr2, postal_addr3, postal_addr4, postal_postcode, landline, mobile, email As String
    Dim equity_amt, cap_amt, asset_no As Integer
    Dim valid_recs As Integer = 0
    Dim rej_recs As Integer = 0
    Dim no_of_errors As Integer = 0

    Dim asset_table(40, 3)
    Dim mthly_contrib_amt, upfront_contrib_amt, income_contrib_cap As Decimal
    Dim equity_amt_verified, cap_asset_type, cap_amt_verified, comments As String
    Dim allowable_cap_threshold, effective_date, pref_pay_method, pref_pay_date As String
    Dim bank_acc_name, bank_acc_no, sort_code, emp_status, rep_order, withdrawal_date, hardship_appl_recvd As String

    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click

        Try
            With OpenFileDialog1
                .Title = "Read XML file1"
                .Filter = "XML file|*.xml"
                .FileName = ""
                .CheckFileExists = True
            End With
            Dim xml_dataSet As New DataSet()
            Dim doc As New Xml.XmlDocument()

            Dim file As String = "Applicant ID" & "|" & "SummonsNum" & "|" & "MAAT ID" & _
                        "|" & "First name" & "|" & "Surname" & "|" & "Date of birth" & "|" & "NI No" & _
                        "|" & "Monthly Amount" & "|" & "Upfront Amt" & "|" & "Income contribution cap" & "|" & _
                        "Debt addr1" & "|" & "Debt addr2" & "|" & "Debt addr3" & "|" & "Debt addr4" & _
                        "|" & "Debt postcode" & "|" & "Curr addr1" & "|" & "Curr addr2" & _
                        "|" & "Curr addr3" & "|" & "Curr addr4" & "|" & "Curr postcode" & "|" & _
                        "Landline" & "|" & "Mobile" & "|" & "Email" & "|" & _
                         "Effective date" & "|" & "Comments" & vbNewLine

            Dim idx As Integer = 0
            If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                Try
                    filename = OpenFileDialog1.FileName
                    ln2 = Microsoft.VisualBasic.Len(filename)

                    new_file = Microsoft.VisualBasic.Left(filename, ln2 - 4) & "_test_error.txt"
                    My.Computer.FileSystem.WriteAllText(new_file, "Error messages " & Now, False)

                    new_file = Microsoft.VisualBasic.Left(filename, ln2 - 4) & "_test_changes.txt"
                    change_file = new_file
                    Dim change_message As String = "Debtor" & vbTab & "Maat ID" & vbTab & "Field" & _
                    vbTab & "Correct Value" & vbTab & "Incorrect value" & vbNewLine
                    My.Computer.FileSystem.WriteAllText(new_file, change_message, False)

                    reset_fields()
                    Dim rdr_name As String = ""
                    Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
                    reader.Read()
                    ProgressBar1.Value = 5
                    Dim record_count As Integer = 0
                    While (reader.ReadState <> Xml.ReadState.EndOfFile)
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        Try
                            rdr_name = reader.Name
                        Catch
                            Continue While
                        End Try

                        'Note ReadElementContentAsString moves focus to next element so don't need read
                        
                        ProgressBar1.Value = record_count
                        Select Case rdr_name
                            Case "FILENAME"
                                filename_id_tag = reader.Item(0)
                                filename_id = reader.ReadElementContentAsString
                            Case "CONTRIBUTIONS"
                                record_count += 1
                                If record_count > 100 Then
                                    record_count = 0
                                End If
                                If idx > 0 Then
                                    end_of_record()
                                    If validate_record() = False Then
                                        rej_recs += 1
                                    Else
                                        valid_recs += 1
                                        If on_onestep = False And mthly_contrib_amt > 0 Then
                                            file = file & record & vbNewLine
                                        End If
                                        'reset_fields()
                                    End If
                                    '020810 reset fields moved here to include rejected
                                    reset_fields()
                                End If
                                idx += 1
                                contrib_id = reader.Item(0)
                                reader.Read()
                            Case "APPLICANT_ID"
                                applicant_id = reader.ReadElementContentAsString
                            Case "MAAT_ID"
                                maat_id = reader.ReadElementContentAsString
                                asset_no = 0
                                If maat_id = 1584096 Then
                                    asset_no = 0
                                End If
                            Case "ARREST_SUMMONS_NUMBER"
                                summons_no = reader.ReadElementContentAsString
                            Case "FIRST_NAME"
                                first_name = reader.ReadElementContentAsString
                            Case "LAST_NAME"
                                surname = reader.ReadElementContentAsString
                            Case "DOB"
                                dob = reader.ReadElementContentAsString
                            Case "NI_NUMBER"
                                ni_no = reader.ReadElementContentAsString
                            Case "MONTHLY_CONTRIBUTION"
                                mthly_contrib_amt_str = reader.ReadElementContentAsString
                                mthly_contrib_amt = mthly_contrib_amt_str      ' pounds not pence/ 100
                            Case "UPFRONT_CONTRIBUTION"
                                upfront_contrib_amt_str = reader.ReadElementContentAsString
                                upfront_contrib_amt = upfront_contrib_amt_str     'pounds not pence/ 100
                            Case "INCOME_CONTRIBUTION_CAP"
                                income_contrib_cap_str = reader.ReadElementContentAsString
                                income_contrib_cap = income_contrib_cap_str      'pounds not pence/ 100
                            Case "INCOME_UPLIFT_APPLIED"
                                income_uplift_applied = reader.ReadElementContentAsString
                                comments = comments & "Inc uplift applied:" & income_uplift_applied & ";"
                            Case "DESCRIPTION"
                                case_type = reader.ReadElementContentAsString
                            Case "IN_COURT_CUSTODY"
                                in_court_custody = reader.ReadElementContentAsString
                            Case "HOME_ADDRESS"
                                reader.Read()
                                While reader.Name <> "HOME_ADDRESS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "LINE1"
                                            debt_addr1 = reader.ReadElementContentAsString
                                        Case "LINE2"
                                            debt_addr2 = reader.ReadElementContentAsString
                                        Case "LINE3"
                                            debt_addr3 = reader.ReadElementContentAsString
                                        Case "CITY"
                                            debt_addr4 = reader.ReadElementContentAsString
                                        Case "POSTCODE"
                                            debt_postcode = reader.ReadElementContentAsString
                                            'Case "COUNTRY"
                                            'home_addr = home_addr & " " & reader.ReadElementContentAsString
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "POSTAL_ADDRESS"
                                reader.Read()
                                While reader.Name <> "POSTAL_ADDRESS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "LINE1"
                                            postal_addr1 = reader.ReadElementContentAsString
                                        Case "LINE2"
                                            postal_addr2 = reader.ReadElementContentAsString
                                        Case "LINE3"
                                            postal_addr3 = reader.ReadElementContentAsString
                                        Case "CITY"
                                            postal_addr4 = reader.ReadElementContentAsString
                                        Case "POSTCODE"
                                            postal_postcode = reader.ReadElementContentAsString
                                            'Case "COUNTRY"
                                            'postal_addr = postal_addr & " " & reader.ReadElementContentAsString
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "CASE_TYPE"
                                Try
                                    case_type = reader.ReadElementContentAsString
                                Catch
                                End Try
                                If case_type <> "" Then
                                    comments = comments & "Case type:" & case_type & ";"
                                    Continue While
                                End If
                                reader.Read()
                                While reader.Name <> "CASE_TYPE"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "DESCRIPTION"
                                            case_type = Trim(reader.ReadElementContentAsString)
                                            comments = comments & "Case type:" & case_type & ";"
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "LANDLINE"
                                landline = reader.ReadElementContentAsString
                            Case "MOBILE"
                                mobile = reader.ReadElementContentAsString
                            Case "EMAIL"
                                email = reader.ReadElementContentAsString
                            Case "EQUITY_AMOUNT"
                                equity_amt = equity_amt + reader.ReadElementContentAsString
                            Case "EQUITY_AMOUNT_VERIFIED"
                                If equity_amt_verified <> "No" Then
                                    equity_amt_verified = reader.ReadElementContentAsString
                                    remove_chrs(equity_amt_verified)
                                Else
                                    reader.Read()
                                End If
                            Case "CAPITAL"
                                reader.Read()
                                While reader.Name <> "CAPITAL"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "CAPITAL_ASSET_TYPE"
                                            reader.Read()
                                            While reader.Name <> "CAPITAL_ASSET_TYPE"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "DESCRIPTION"
                                                        asset_no += 1
                                                        Dim temp As String = reader.ReadElementContentAsString
                                                        remove_chrs(temp)
                                                        Try
                                                            asset_table(asset_no, 1) = temp
                                                        Catch ex As Exception
                                                            MsgBox("asset table dimension requires increasing")
                                                            End
                                                        End Try
                                                    Case Else
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case "CAPITAL_AMOUNT"
                                            asset_table(asset_no, 2) = reader.ReadElementContentAsString
                                        Case "CAPITAL_AMOUNT_VERIFIED"
                                            cap_amt_verified = reader.ReadElementContentAsString
                                            remove_chrs(cap_amt_verified)
                                            asset_table(asset_no, 3) = cap_amt_verified
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "ALLOWABLE_CAPITAL_THRESHOLD"
                                allowable_cap_threshold = reader.ReadElementContentAsString
                                remove_chrs(allowable_cap_threshold)
                            Case "EFFECTIVE_DATE"
                                effective_date = reader.ReadElementContentAsString
                            Case "PREFERRED_PAYMENT_METHOD"
                                reader.Read()
                                While reader.Name <> "PREFERRED_PAYMENT_METHOD"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "DESCRIPTION"
                                            pref_pay_method = reader.ReadElementContentAsString
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case "PREFERRED_PAYMENT_DAY"
                                pref_pay_date = reader.ReadElementContentAsString
                            Case "ACCOUNT_NAME"
                                bank_acc_name = reader.ReadElementContentAsString
                            Case "ACCOUNT_NO"
                                bank_acc_no = reader.ReadElementContentAsString
                            Case "SORT_CODE"
                                sort_code = reader.ReadElementContentAsString
                            Case "EMPLOYMENT_STATUS"
                                Try
                                    emp_status = reader.ReadElementContentAsString
                                Catch
                                End Try
                                If emp_status <> "" Then
                                    comments = comments & "Emp status:" & emp_status & ";"
                                    Continue While
                                End If
                                While reader.Name <> "EMPLOYMENT_STATUS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "DESCRIPTION"
                                            emp_status = reader.ReadElementContentAsString
                                            comments = comments & "Emp status:" & emp_status & ";"
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                            Case Else
                                reader.Read()
                        End Select
                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Else
                MsgBox("XML file not processed")
                Exit Sub
            End If
            'write file including last applicant
            end_of_record()
            If validate_record() = False Then
                rej_recs += 1
            Else
                valid_recs += 1
                If on_onestep = False And mthly_contrib_amt > 0 Then
                    file = file & record & vbNewLine
                End If
            End If


            'Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
            'new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_test.txt"
            'My.Computer.FileSystem.WriteAllText(new_file, file, False)
            'write acknowledgement file in xml
            'Dim ack_file As String = "CONTRIBUTIONS_FILE_ACK_" & Format(Now, "yyyyMMddHHmm") & ".xml"
            'Dim writer As New Xml.XmlTextWriter(ack_file, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)
            'writer.Formatting = Xml.Formatting.Indented
            ''acknowledgement file includes all records not rejected
            'Dim tot_valid_recs As Integer = valid_recs + changes
            'Write_ack(writer, filename_id, filename_id_tag, tot_valid_recs, rej_recs, no_of_errors)
            'writer.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        ProgressBar1.Value = 100
        MsgBox("file saved as " & new_file)
        MsgBox("Cases with no changes = " & no_changes)
        Me.Close()
    End Sub

    Private Function validate_record() As Boolean
        'If mthly_contrib_amt = 0 Then
        '    Return (True)
        'End If

        Dim orig_no_of_errors As Integer = no_of_errors
        If maat_id = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = ""
            error_table(no_of_errors).error_text = "invalid maat id"
        End If

        If on_onestep Then
            Return (True)
        End If
        If first_name = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid first name"
        End If

        If surname = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid surname"
        End If

        If mthly_contrib_amt = 0 Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid monthly contribution"
        End If

        'If upfront_contrib_amt = 0 Then
        '    no_of_errors += 1
        '    error_table(no_of_errors).contrib_id = contrib_id
        '    error_table(no_of_errors).applicant_id = applicant_id
        '    error_table(no_of_errors).maat_id = maat_id
        '    error_table(no_of_errors).error_text = "invalid upfront contribution"
        'End If

        'If summons_no = "" Then
        'no_of_errors += 1
        'error_table(no_of_errors).contrib_id = contrib_id
        'error_table(no_of_errors).applicant_id = applicant_id
        'error_table(no_of_errors).maat_id = maat_id
        'error_table(no_of_errors).error_text = "invalid summons number"
        'End If

        If case_type = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid case type"
        End If

        If emp_status = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid employment status"
        End If

        If effective_date = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid effective date"
        End If

        If income_contrib_cap_str = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid income contribution cap"
        End If

        If income_uplift_applied = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid income uplift applied"
        End If

        If orig_no_of_errors = no_of_errors Then
            Return (True)
        Else
            If no_of_errors = UBound(error_table) Then
                ReDim Preserve error_table(no_of_errors + 10)
            End If
            Return (False)
        End If

    End Function

    Private Sub Write_ack(ByVal writer As Xml.XmlWriter, ByVal filename_id As String, _
    ByVal filename_id_tag As String, ByVal valid_recs As Integer, ByVal rej_recs As Integer, _
    ByVal no_of_errors As Integer)
        writer.WriteStartElement("CONTRIBUTIONS_FILE_ACK")
        writer.WriteStartElement("FILENAME")
        writer.WriteAttributeString("ID", filename_id_tag)
        writer.WriteString(filename_id)
        writer.WriteEndElement()
        writer.WriteElementString("DATE_LOADED", Format(Now, "dd-MM-yyyy"))
        writer.WriteElementString("NO_OF_RECORDS_LOADED", valid_recs)
        writer.WriteElementString("NO_OF_RECORDS_REJECTED", rej_recs)

        Dim idx As Integer
        Dim contrib_id As Double = 0
        Dim last_contrib_id As Double = 0
        Dim applicant_id As Double = 0
        Dim maat_id As Double = 0
        Dim error_text As String = ""
        For idx = 1 To no_of_errors
            If idx = 1 Then
                writer.WriteStartElement("REJECTED_RECORD_LIST")
            End If
            contrib_id = error_table(idx).contrib_id
            If contrib_id <> last_contrib_id Then
                last_contrib_id = contrib_id
                writer.WriteStartElement("CONTRIBUTIONS")
                writer.WriteAttributeString("ID", contrib_id)
                applicant_id = error_table(idx).applicant_id
                maat_id = error_table(idx).maat_id
                writer.WriteElementString("APPLICANT_ID", applicant_id)
                writer.WriteElementString("MAAT_ID", maat_id)
            End If

            error_text = error_table(idx).error_text
            writer.WriteElementString("ERROR_TEXT", error_text)

            If idx = no_of_errors Then
                writer.WriteEndElement()
                writer.WriteEndElement()
            ElseIf contrib_id <> error_table(idx + 1).contrib_id Then
                writer.WriteEndElement()
            End If
        Next
        writer.WriteEndElement()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub
    Private Sub end_of_record()
        Try
            If applicant_id = "" Then
                write_error("No applicant found")
                Exit Sub
            End If
            If applicant_id = "" Then
                write_error("No maat found")
                Exit Sub
            End If

            'If mthly_contrib_amt = 0 Then
            '    Exit Sub
            'End If

            If equity_amt > 0 Then
                comments = comments & "Equity amt:" & equity_amt & ";"
                comments = comments & "Equity amt verified:" & equity_amt_verified & ";"
            End If

            If allowable_cap_threshold <> "" Then
                comments = comments & "Allowable cap threshold:" & allowable_cap_threshold & ";"
            End If

            Dim idx3 As Integer
            Try
                For idx3 = 1 To asset_no
                    comments = comments & "Cap asset type:" & asset_table(idx3, 1) & ";"
                    comments = comments & "Cap amt:" & asset_table(idx3, 2) & ";"
                    comments = comments & "Cap amt verified:" & asset_table(idx3, 3) & ";"
                Next
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            asset_no = 0

            If landline = "" Then
                landline = mobile
                mobile = ""
            End If

            'check if maat-id already exists on onestep
            param1 = "onestep"
            param2 = "select  _createdDate, _rowid from Debtor" & _
            " where client_ref = '" & maat_id & _
            "' and clientschemeID = 1892 "
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                Exit Sub
            End If
            Dim created_date As Date = debtor_dataset.Tables(0).Rows(0).Item(0)
            If Format(created_date, "yyyy-MM-dd") <> Format(load_date, "yyyy-MM-dd") Then
                Exit Sub
            End If
            debtor = debtor_dataset.Tables(0).Rows(0).Item(1)
            'case exists on onestep so look for changes

            'compare with preprocess file
            Dim change_found As Boolean = False
            'read preprocess file
            Dim fileok As Boolean = True

            Try
                With OpenFileDialog1
                    .Title = "Open file"
                    .Filter = "Text files | *.txt"
                    .FileName = ""
                    .CheckFileExists = True
                End With
                If first_time Then
                    If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                        filename = OpenFileDialog1.FileName
                        filetext = My.Computer.FileSystem.ReadAllText(filename)
                    End If
                    first_time = False
                End If
               
               
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
            If Not fileok Then
                MsgBox("file not opened")
                Exit Sub
            End If
            Dim linetext As String = ""
            Dim line(0) As String
            Dim idx, start_idx, lines As Integer
            Try
                For idx = 1 To Len(filetext) - 1
                    If Mid(filetext, idx, 2) = vbNewLine Then
                        ReDim Preserve line(lines)
                        line(lines) = linetext
                        linetext = ""
                        lines += 1
                    Else
                        linetext = linetext + Mid(filetext, idx, 1)
                    End If
                Next
                Dim file_maat_id As Integer
                Dim field_no As Integer = 0
                Dim field As String = ""
                Dim file_appl_id As String = ""
                Dim file_summons As String = ""
                Dim file_nino As String = ""
                Dim file_debt_addr1 As String = ""
                Dim file_debt_addr2 As String = ""
                Dim file_debt_addr3 As String = ""
                Dim file_debt_addr4 As String = ""
                Dim file_curr_addr1 As String = ""
                Dim file_curr_addr2 As String = ""
                Dim file_curr_addr3 As String = ""
                Dim file_curr_addr4 As String = ""
                Dim file_first_name, file_surname As String
                Dim file_dob, file_eff_date As Date
                Dim file_mth_amt, file_upfront_amt, file_income_cap As Decimal
                Dim file_pcode As String = ""
                Dim file_curr_pcode As String = ""
                Dim file_landline As String = ""
                Dim file_mobile As String = ""
                Dim file_comments As String = ""
                For idx = 1 To lines - 1
                    start_idx = 2
                    field_no = 0
                    Dim idx2 As Integer
                    For idx2 = 1 To Len(line(idx)) - 1
                        If Mid(line(idx), idx2, 1) = "|" Then
                            field_no += 1
                            field = Mid(line(idx), start_idx, idx2 - start_idx)
                            start_idx = idx2 + 1
                            Select Case field_no
                                Case 1
                                    file_appl_id = field
                                Case 2
                                    file_summons = field
                                Case 3
                                    file_maat_id = field
                                    If maat_id <> file_maat_id Then
                                        Exit For
                                    End If
                                    If applicant_id <> file_appl_id Then
                                        write_change(debtor & "|" & maat_id & "|" & "Applicant ID" & "|" & _
                                             applicant_id & "|" & file_appl_id & vbNewLine)
                                    End If
                                    If summons_no <> file_summons Then
                                        write_change(debtor & "|" & maat_id & "|" & "Summons No" & "|" & _
                                             summons_no & "|" & file_summons & vbNewLine)
                                    End If
                                Case 4
                                    file_first_name = field
                                    If file_first_name <> first_name Then
                                        write_change(debtor & "|" & maat_id & "|" & "First Name" & "|" & _
                                             first_name & "|" & file_first_name & vbNewLine)
                                    End If
                                Case 5
                                    file_surname = field
                                    If file_surname <> surname Then
                                        write_change(debtor & "|" & maat_id & "|" & "Surname" & "|" & _
                                             surname & "|" & file_surname & vbNewLine)
                                    End If
                                Case 6
                                    file_dob = field
                                    If file_dob <> dob Then
                                        write_change(debtor & "|" & maat_id & "|" & "DOB" & "|" & _
                                             dob & "|" & file_dob & vbNewLine)
                                    End If
                                Case 7
                                    file_nino = field
                                    If file_nino <> ni_no Then
                                        write_change(debtor & "|" & maat_id & "|" & "NINO" & "|" & _
                                             ni_no & "|" & file_nino & vbNewLine)
                                    End If
                                Case 8
                                    file_mth_amt = field
                                    If file_mth_amt <> mthly_contrib_amt Then
                                        write_change(debtor & "|" & maat_id & "|" & "Mthly Amt" & "|" & _
                                             mthly_contrib_amt & "|" & file_mth_amt & vbNewLine)
                                    End If
                                Case 9
                                    file_upfront_amt = field
                                    If file_upfront_amt <> upfront_contrib_amt Then
                                        write_change(debtor & "|" & maat_id & "|" & "Upfront Amt" & "|" & _
                                             upfront_contrib_amt & "|" & file_upfront_amt & vbNewLine)
                                    End If
                                Case 10
                                    file_income_cap = field
                                    If file_income_cap <> income_contrib_cap Then
                                        write_change(debtor & "|" & maat_id & "|" & "Income cap Amt" & "|" & _
                                              income_contrib_cap & "|" & file_income_cap & vbNewLine)
                                    End If
                                Case 11
                                    file_debt_addr1 = field
                                    If file_debt_addr1 <> debt_addr1 Then
                                        write_change(debtor & "|" & maat_id & "|" & "debt_addr1" & "|" & _
                                              debt_addr1 & "|" & file_debt_addr1 & vbNewLine)
                                    End If
                                Case 12
                                    file_debt_addr2 = field
                                    If file_debt_addr2 <> debt_addr2 Then
                                        write_change(debtor & "|" & maat_id & "|" & "debt_addr2" & "|" & _
                                              debt_addr2 & "|" & file_debt_addr2 & vbNewLine)
                                    End If
                                Case 13
                                    file_debt_addr3 = field
                                    If file_debt_addr3 <> debt_addr3 Then
                                        write_change(debtor & "|" & maat_id & "|" & "debt_addr3" & "|" & _
                                              debt_addr3 & "|" & file_debt_addr3 & vbNewLine)
                                    End If
                                Case 14
                                    file_debt_addr4 = field
                                    If file_debt_addr4 <> debt_addr4 Then
                                        write_change(debtor & "|" & maat_id & "|" & "debt_addr4" & "|" & _
                                              debt_addr4 & "|" & file_debt_addr4 & vbNewLine)
                                    End If
                                Case 15
                                    file_pcode = field
                                    If file_pcode <> debt_postcode Then
                                        write_change(debtor & "|" & maat_id & "|" & "Debt Postcode" & "|" & _
                                             debt_postcode & "|" & file_pcode & vbNewLine)
                                    End If
                                Case 16
                                    file_curr_addr1 = field
                                    If file_curr_addr1 <> postal_addr1 Then
                                        write_change(debtor & "|" & maat_id & "|" & "Curr_addr1" & "|" & _
                                              postal_addr1 & "|" & file_curr_addr1 & vbNewLine)
                                    End If
                                Case 17
                                    file_curr_addr2 = field
                                    If file_curr_addr2 <> postal_addr2 Then
                                        write_change(debtor & "|" & maat_id & "|" & "Curr_addr2" & "|" & _
                                              postal_addr2 & "|" & file_curr_addr2 & vbNewLine)
                                    End If
                                Case 18
                                    file_curr_addr3 = field
                                    If file_curr_addr3 <> postal_addr3 Then
                                        write_change(debtor & "|" & maat_id & "|" & "Curr_addr3" & "|" & _
                                              postal_addr3 & "|" & file_curr_addr3 & vbNewLine)
                                    End If
                                Case 19
                                    file_curr_addr4 = field
                                    If file_curr_addr4 <> postal_addr4 Then
                                        write_change(debtor & "|" & maat_id & "|" & "Curr_addr4" & "|" & _
                                              postal_addr4 & "|" & file_curr_addr4 & vbNewLine)
                                    End If
                                Case 20
                                    file_curr_pcode = field
                                    If file_curr_pcode <> postal_postcode Then
                                        write_change(debtor & "|" & maat_id & "|" & "Curr Postcode" & "|" & _
                                             postal_postcode & "|" & file_curr_pcode & vbNewLine)
                                    End If
                                Case 21
                                    file_landline = field
                                    If file_landline <> landline Then
                                        write_change(debtor & "|" & maat_id & "|" & "Landline" & "|" & _
                                             landline & "|" & file_landline & vbNewLine)
                                    End If
                                Case 22
                                    file_mobile = field
                                    If file_mobile <> mobile Then
                                        write_change(debtor & "|" & maat_id & "|" & "Mobile" & "|" & _
                                             mobile & "|" & file_mobile & vbNewLine)
                                    End If
                                Case 23
                                    file_mobile = field
                                    If file_eff_date <> effective_date Then
                                        write_change(debtor & "|" & maat_id & "|" & "Effective date" & "|" & _
                                             effective_date & "|" & file_eff_date & vbNewLine)
                                    End If
                                Case 24
                                    file_mobile = field
                                    If file_comments <> comments Then
                                        write_change(debtor & "|" & maat_id & "|" & "Comments" & "|" & _
                                             comments & "|" & file_comments & vbNewLine)
                                    End If
                            End Select
                        End If

                    Next


                Next
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            If Not change_found Then
                no_changes += 1

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub write_error(ByVal error_message As String)
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_error.txt"
        error_message = error_message & vbNewLine
        My.Computer.FileSystem.WriteAllText(new_file, error_message, True)
    End Sub
    Private Sub write_change(ByVal change_message As String)
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        My.Computer.FileSystem.WriteAllText(change_file, change_message, True)
    End Sub
    Private Sub remove_chrs(ByRef in_field As String)
        Dim idx2 As Integer
        Dim out_field As String = ""
        For idx2 = 1 To Microsoft.VisualBasic.Len(in_field)
            If Mid(in_field, idx2, 1) <> Chr(9) And _
              Mid(in_field, idx2, 1) <> Chr(10) And _
            Mid(in_field, idx2, 1) <> Chr(13) Then
                out_field = out_field & Mid(in_field, idx2, 1)
            End If
        Next
        in_field = out_field
    End Sub

    Private Sub reset_fields()
        applicant_id = ""
        summons_no = ""
        maat_id = ""
        first_name = ""
        surname = ""
        dob = ""
        ni_no = ""
        mthly_contrib_amt_str = ""
        upfront_contrib_amt_str = ""
        income_contrib_cap_str = ""
        income_uplift_applied = ""
        case_type = ""
        in_court_custody = ""
        debt_addr1 = ""
        debt_addr2 = ""
        debt_addr3 = ""
        debt_addr4 = ""
        debt_postcode = ""
        postal_addr1 = ""
        postal_addr2 = ""
        postal_addr3 = ""
        postal_addr4 = ""
        postal_postcode = ""
        landline = ""
        mobile = ""
        email = ""
        equity_amt = 0
        equity_amt_verified = ""
        cap_asset_type = ""
        cap_amt = 0
        cap_amt_verified = ""
        allowable_cap_threshold = ""
        effective_date = ""
        pref_pay_method = ""
        pref_pay_date = ""
        bank_acc_name = ""
        bank_acc_no = ""
        sort_code = ""
        emp_status = ""
        rep_order = ""
        withdrawal_date = ""
        hardship_appl_recvd = ""
        comments = ""
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        load_date = CDate("2010-07-22")
    End Sub
End Class

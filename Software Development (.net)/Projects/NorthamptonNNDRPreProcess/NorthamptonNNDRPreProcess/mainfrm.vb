Public Class mainfrm
    Dim file, cl_ref, offence_no, full_name, name2, ta_name, curr_addr, debt_addr, lo_date_str, offence_from, offence_to, debt As String
    Dim file_cl_os As String
    Dim off_start_date, off_end_date, lo_date As Date
    Dim error_found As Boolean = False
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        exitbtn.Enabled = False
        openbtn.Enabled = False
        Dim filename As String = ""
        Dim filetext As String = ""
        Dim linetext As String = ""
        file = "Client Ref|Offence No|Name|TA name|Current Addr|Debt Addr|Offence Court|Offence From|" & _
        "Offence to|Debt amt|name2" & vbNewLine


        Dim fileok As Boolean = True
        Dim line(0) As String
        Try
            With OpenFileDialog1
                .Title = "Read NNDR file"
                .Filter = "txt file|*.txt"
                .FileName = ""
                .CheckFileExists = True
            End With
            If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                Try
                    filename = OpenFileDialog1.FileName
                    filetext = My.Computer.FileSystem.ReadAllText(filename)
                Catch ex As Exception
                    MsgBox("Unable to read file")
                    fileok = False
                End Try
            Else
                fileok = False
            End If

            If fileok = False Then
                MsgBox("Unable to read file")
                exitbtn.Enabled = True
                openbtn.Enabled = True
                Exit Sub
            End If

            ProgressBar1.Value = 5

            Dim lines As Integer

            'read file into array
            Dim idx As Integer
            For idx = 1 To Len(filetext)
                If Mid(filetext, idx, 2) = vbNewLine Then
                    ReDim Preserve line(lines)
                    line(lines) = linetext
                    linetext = ""
                    lines += 1
                Else
                    linetext = linetext + Mid(filetext, idx, 1)
                End If
            Next
            'save last line
            ReDim Preserve line(lines)
            line(lines) = linetext
            reset_fields()
            Dim test As String = line(lines - 1)
            'process each line
            For idx = 1 To lines
                Try
                    ProgressBar1.Value = (idx / (lines - 1)) * 100
                Catch ex As Exception
                    ProgressBar1.Value = 0
                End Try

                Application.DoEvents()
                'look for account number
                Dim start_idx As Integer = InStr(line(idx), "ACCOUNT NO:")
                If start_idx = 0 Then
                    Continue For
                End If
                start_idx += 11
                cl_ref = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - start_idx))
                If cl_ref.Length = 0 Then
                    Continue For
                End If
                Dim idx2 As Integer
                'now get all details for the account
                For idx2 = idx To lines
                    start_idx = InStr(line(idx2), "CASE NO:")
                    If start_idx > 0 Then
                        start_idx += 8
                        offence_no = Trim(Microsoft.VisualBasic.Right(line(idx2), line(idx2).Length - start_idx))
                        idx2 += 2
                        full_name = line(idx2)
                        full_name = Trim(remove_chars(full_name))
                        idx2 += 1
                        start_idx = InStr(line(idx2), "T/A")
                        If start_idx > 0 Then
                            start_idx += 3
                            ta_name = Trim(Microsoft.VisualBasic.Right(line(idx2), line(idx2).Length - start_idx))
                            idx2 += 1
                        End If
                        'if full name ends in & there is a second name
                        If Microsoft.VisualBasic.Right(full_name, 1) = "&" And ta_name = "" Then
                            full_name = Microsoft.VisualBasic.Right(full_name, full_name.Length - 1)
                            name2 = line(idx2)
                            name2 = Trim(remove_chars(name2))
                            idx2 += 1
                        End If
                        'current address down to RE:
                        While InStr(line(idx2), "RE: ") = 0
                            line(idx2) = Trim(remove_chars(line(idx2)))
                            If line(idx2).Length > 0 Then
                                curr_addr = curr_addr & line(idx2) & " "
                            End If
                            idx2 += 1
                        End While
                        'debt address down to LIABILITY
                        While InStr(line(idx2), "LIABILITY") = 0
                            line(idx2) = Replace(line(idx2), "RE: ", "")
                            line(idx2) = Trim(remove_chars(line(idx2)))
                            If line(idx2).Length > 0 Then
                                debt_addr = debt_addr & line(idx2) & " "
                            End If
                            idx2 += 1
                        End While
                    End If
                    start_idx = InStr(line(idx2), "GRANTED:")
                    If start_idx > 0 Then
                        start_idx += 8
                        lo_date_str = Trim(Microsoft.VisualBasic.Right(line(idx2), line(idx2).Length - start_idx))
                        Try
                            lo_date = lo_date_str
                        Catch ex As Exception
                            MsgBox("Invalid LO Date on line " & idx2)
                            error_found = True
                            Exit Sub
                        End Try
                        idx2 += 1
                    End If
                    start_idx = InStr(line(idx2), "PERIOD:")
                    If start_idx > 0 Then
                        start_idx += 7
                        Dim end_idx As Integer = InStr(line(idx2), "TO")
                        If end_idx = 0 Then
                            MsgBox("Invalid period on line " & idx2)
                            error_found = True
                            Exit Sub
                        End If
                        offence_from = Trim(Mid(line(idx2), start_idx, end_idx - start_idx))
                        Dim test_date As Date
                        Try
                            test_date = offence_from
                        Catch ex As Exception
                            MsgBox("Invalid Offence from Date on line " & idx2)
                            error_found = True
                            Exit Sub
                        End Try
                        If off_start_date = Nothing Then
                            off_start_date = test_date
                        Else
                            If test_date < off_start_date Then
                                off_start_date = test_date
                            End If
                        End If
                        offence_to = Trim(Microsoft.VisualBasic.Right(line(idx2), line(idx2).Length - end_idx - 2))
                        Try
                            test_date = offence_to
                        Catch ex As Exception
                            MsgBox("Invalid Offence to Date on line " & idx2)
                            error_found = True
                            Exit Sub
                        End Try
                        If off_end_date = Nothing Then
                            off_end_date = test_date
                        Else
                            If test_date < off_end_date Then
                                off_end_date = test_date
                            End If
                        End If
                        idx2 += 1
                    End If
                    'look for debt amount
                    start_idx = InStr(line(idx2), "CURRENT BALANCE:")
                    If start_idx > 0 Then
                        start_idx += 16
                        debt = Trim(Microsoft.VisualBasic.Right(line(idx2), line(idx2).Length - start_idx))
                        Dim debt_amt As Decimal
                        Try
                            debt_amt = debt
                        Catch ex As Exception
                            MsgBox("Invalid Debt amount on line " & idx2)
                            error_found = True
                            Exit Sub
                        End Try
                        Exit For
                    End If
                Next
                'save details in file
                file = file & cl_ref & "|" & offence_no & "|" & full_name & "|" & ta_name & "|" & _
                curr_addr & "|" & debt_addr & "|" & Format(lo_date, "dd/MM/yyyy") & "|" & _
                Format(off_start_date, "dd/MM/yyyy") & "|" & Format(off_end_date, "dd/MM/yyyy") & "|" & _
                debt & "|" & name2 & vbNewLine
                reset_fields()
                idx = idx2
            Next
            If error_found Then
                MsgBox("ERRORS FOUND")
            Else
                Dim filename_prefix As String = ""
                For idx = Len(filename) To 1 Step -1
                    If Mid(filename, idx, 1) = "." Then
                        filename_prefix = Microsoft.VisualBasic.Left(filename, idx - 1)
                        Exit For
                    End If
                Next
                My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", file, False)
                MsgBox("Preprocess File created")
            End If
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub reset_fields()
        cl_ref = ""
        offence_no = ""
        full_name = ""
        name2 = ""
        ta_name = ""
        curr_addr = ""
        debt_addr = ""
        lo_date_str = ""
        offence_from = ""
        offence_to = ""
        debt = ""
        lo_date = Nothing
        off_start_date = Nothing
        off_end_date = Nothing
    End Sub
    Private Function remove_chars(ByVal txt As String) As String
        Dim idx As Integer
        Dim retn_txt As String = ""
        For idx = 1 To txt.Length
            If Mid(txt, idx, 1) <> Chr(10) And Mid(txt, idx, 1) <> Chr(13) Then
                retn_txt = retn_txt & Mid(txt, idx, 1)
            End If
        Next
        Return (retn_txt)
    End Function
End Class

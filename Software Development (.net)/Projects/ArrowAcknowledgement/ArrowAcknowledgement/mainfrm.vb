Public Class mainfrm
    Dim first_debtor, last_debtor As Integer
    Dim clientid As Integer
    '13.12.2011 do not select scheme - ask for first case number
    '23.02.2011 add time to fle name
    Dim csid_array(50) As Integer
    Dim sch_id As Integer
    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'get arrow schemes

        'If MsgBox("IS this for RML?", MsgBoxStyle.YesNo, "Arrow global Acknowledgement") = MsgBoxResult.Yes Then
        '    clientid = 1301
        'ElseIf MsgBox("IS this for Guernsey?", MsgBoxStyle.YesNo, "Arrow global Acknowledgement") = MsgBoxResult.Yes Then
        '    clientid = 1160
        'Else
        '    clientid = 986
        'End If

        Try
            first_debtor = InputBox("Enter first case number", "FIRST CASE NUMBER")
        Catch ex As Exception
            MsgBox("Invalid number")
            Me.Close()
            Exit Sub
        End Try
        Try
            last_debtor = InputBox("Enter last case number", "LAST CASE NUMBER")
        Catch ex As Exception
            MsgBox("Invalid number")
            Me.Close()
            Exit Sub
        End Try
       
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        If accman_rbtn.Checked Then
            clientid = 1765
        ElseIf guernrbtn.Checked Then
            clientid = 1160
        ElseIf globrbtn.Checked Then
            clientid = 986
        ElseIf erudiorbtn.Checked Then
            clientid = 1874
        Else
            clientid = 1301
        End If

        exitbtn.Enabled = False
        runbtn.Enabled = False
        run_datepicker.Enabled = False
        Dim outfile As String = "ArrowKey|CustAcctNo|PortfolioDescription|Debtor1_Title|Debtor1_FirstName|" & _
        "Debtor1_surname|Debtor1_DateOfBirth|Debtor1_addressLine1|Debtor1_addressLine2|" & _
        "Debtor1_addressLine3|Debtor1_addressLine4|Debtor1_addressLine5|Debtor1_postcode|" & _
        "Debtor2_Title|Debtor2_FirstName|Debtor2_surname|Debtor2_DateOfBirth|Debtor2_addressLine1|" & _
        "Debtor2_addressline2|Debtor2_addressLine3|Debtor2_addressLine4|Debtor2_addressLine5|" & _
        "Debtor2_postcode|Debtor1_SelleraddressLine1|Debtor1_SelleraddressLine2|Debtor1_SelleraddressLine3|" & _
        "Debtor1_SelleraddressLine4|Debtor1_SelleraddressLine5|Debtor1_SellerPostcode|AccountBalance|" & _
        "HomePhoneNumber|WorkPhoneNumber|MobilePhoneNumber|DeterminationDate" & vbNewLine
        Dim start_date As New System.DateTime(Format(run_datepicker.Value, "yyyy"), Format(run_datepicker.Value, "MM"), Format(run_datepicker.Value, "dd"), 0, 0, 0)

        Dim end_date As Date = DateAdd(DateInterval.Day, 1, start_date)
        param1 = "onestep"
        param2 = "select schemeID, _rowid from ClientScheme where clientID = " & clientid
        Dim cs_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("No rows found for Arrow global")
            Exit Sub
        End If
        Dim csid_rows As Integer = no_of_rows - 1
        Dim csid_idx As Integer
        For csid_idx = 0 To csid_rows
            'get all cases loaded on selected date
            param1 = "onestep"
            param2 = "select _rowid, client_ref, offence_number, name_title, name_fore, name_sur, " & _
            "dateOfBirth, address, add_postcode, name2, offence_details, add_phone, add_fax, empPhone," & _
            "debt_amount, offence_date" & _
            " from Debtor where _createdDate >= '" & Format(start_date, "yyyy.MM.dd") & "'" & _
            " and _createdDate < '" & Format(end_date, "yyyy.MM.dd") & "'" & _
            " and _rowid >= " & first_debtor & _
              " and _rowid <= " & last_debtor & _
            " and clientschemeID = " & cs_dataset.Tables(0).Rows(csid_idx).Item(1)
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim idx, debtor_rows, debtor, char_start, char_end As Integer
            Dim cl_ref, offence_number, portfolio_desc, note_text, name_title, name_fore, name_sur, address As String
            Dim c_add_line1, c_add_line2, c_add_line3, c_add_line4, c_add_line5, c_pcode As String
            Dim o_add_line1, o_add_line2, o_add_line3, o_add_line4, o_add_line5, o_add_line6, o_pcode As String
            Dim dob, dob2, determination_date As Date
            Dim debt_amt As Decimal
            Dim add_phone, emp_phone, add_fax As String
            debtor_rows = no_of_rows - 1
            For idx = 0 To debtor_rows
                Application.DoEvents()
                Try
                    ProgressBar1.Value = (idx / debtor_rows) * 100
                Catch
                End Try

                cl_ref = Trim(debtor_dataset.Tables(0).Rows(idx).Item(1))
                Try
                    offence_number = Trim(debtor_dataset.Tables(0).Rows(idx).Item(2))
                Catch ex As Exception
                    offence_number = ""
                End Try

                debtor = debtor_dataset.Tables(0).Rows(idx).Item(0)
                Try
                    name_title = Trim(debtor_dataset.Tables(0).Rows(idx).Item(3))
                Catch ex As Exception
                    name_title = ""
                End Try
                Try
                    name_fore = Trim(debtor_dataset.Tables(0).Rows(idx).Item(4))
                Catch ex As Exception
                    name_fore = ""
                End Try
                Try
                    name_sur = Trim(debtor_dataset.Tables(0).Rows(idx).Item(5))
                Catch ex As Exception
                    name_sur = ""
                End Try


                Try
                    dob = debtor_dataset.Tables(0).Rows(idx).Item(6)
                Catch ex As Exception
                    dob = CDate("1800,1,1")
                End Try
                address = Trim(debtor_dataset.Tables(0).Rows(idx).Item(7))
                split_address_lines(address)
                c_add_line1 = add_line1
                c_add_line2 = add_line2
                c_add_line3 = add_line3
                c_add_line4 = add_line4
                c_add_line5 = add_line5
                Try
                    c_pcode = Trim(debtor_dataset.Tables(0).Rows(idx).Item(8)) 'DD needs to check for NULL
                Catch ex As Exception
                    c_pcode = ""
                End Try
                If c_add_line5 = c_pcode Then
                    c_add_line5 = ""
                ElseIf c_add_line4 = c_pcode Then
                    c_add_line4 = ""
                ElseIf c_add_line3 = c_pcode Then
                    c_add_line3 = ""
                End If
                Try
                    name2 = Trim(debtor_dataset.Tables(0).Rows(idx).Item(9))
                Catch ex As Exception
                    name2 = ""
                End Try

                split_name2()
                Try
                    offence_details = Trim(debtor_dataset.Tables(0).Rows(idx).Item(10))
                Catch ex As Exception
                    offence_details = ""
                End Try

                split_address_lines(offence_details)
                o_add_line1 = add_line1
                o_add_line2 = add_line2
                o_add_line3 = add_line3
                o_add_line4 = add_line4
                o_add_line5 = add_line5
                o_add_line6 = add_line6
                'last one is probably postcode
                o_pcode = ""
                If add_line6.Length > 0 Then
                    o_pcode = add_line6
                ElseIf add_line5.Length > 0 Then
                    o_pcode = add_line5
                    o_add_line5 = ""
                ElseIf add_line4.Length > 0 Then
                    o_pcode = add_line4
                    o_add_line4 = ""
                ElseIf add_line3.Length > 0 Then
                    o_pcode = add_line3
                    o_add_line3 = ""
                ElseIf add_line2.Length > 0 Then
                    o_pcode = add_line2
                    o_add_line2 = ""
                End If

                'get notes
                param2 = "select text from Note where debtorID =" & debtor & _
                " and (type = 'Client note' or type = 'Note') order by _rowid desc"
                Dim note_dataset As DataSet = get_dataset(param1, param2)
                portfolio_desc = ""
                Dim portfolio_found As Boolean = False
                Dim dob2_found As Boolean = False
                Dim debtor2_address_found = False
                Dim liquidity_found As Boolean = False
                dob2 = CDate("1800,1,1")
                If no_of_rows > 0 Then
                    Dim note_rows = no_of_rows - 1
                    Dim idx2 As Integer
                    For idx2 = 0 To note_rows
                        note_text = note_dataset.Tables(0).Rows(idx2).Item(0)
                        If portfolio_found = False Then
                            char_start = InStr(note_text, "PortfolioDescription:")
                            If char_start > 0 Then
                                Dim idx3 As Integer
                                For idx3 = char_start + 21 To note_text.Length
                                    If Mid(note_text, idx3, 1) = ";" Then
                                        char_end = idx3
                                        Exit For
                                    End If
                                Next
                                portfolio_desc = Mid(note_text, char_start + 21, char_end - char_start - 21)
                                portfolio_found = True
                            End If
                        End If
                        If debtor2_address_found = False Then
                            char_start = InStr(note_text, "Debtor2Address:")
                            If char_start > 0 Then
                                Dim idx3 As Integer
                                For idx3 = char_start + 15 To note_text.Length
                                    If Mid(note_text, idx3, 1) = ";" Then
                                        char_end = idx3
                                        Exit For
                                    End If
                                Next
                                debtor2_address = Mid(note_text, char_start + 15, char_end - char_start - 14)
                                debtor2_address_found = True
                            End If
                        End If
                        If dob2_found = False Then
                            char_start = InStr(note_text, "DobDebtor2:")
                            If char_start > 0 Then
                                Dim idx3 As Integer
                                For idx3 = char_start + 11 To note_text.Length
                                    If Mid(note_text, idx3, 1) = ";" Then
                                        char_end = idx3
                                        Exit For
                                    End If
                                Next
                                Try
                                    dob2 = Mid(note_text, char_start + 11, char_end - char_start - 11)
                                Catch ex As Exception
                                    MsgBox("invalid debtor2 DOB on case " & debtor)
                                    Me.Close()
                                End Try
                                dob2_found = True
                            End If
                        End If
                    Next
                End If
                If debtor2_address_found Then
                    split_debtor2_address()
                End If

                outfile = outfile & cl_ref & "|" & offence_number & "|" & portfolio_desc & "|" & _
                name_title & "|" & name_fore & "|" & name_sur & "|"
                If dob = CDate("1800,1,1") Then
                    outfile = outfile & "|"
                Else
                    outfile = outfile & Format(dob, "dd/MM/yyyy") & "|"
                End If
                outfile = outfile & c_add_line1 & "|" & c_add_line2 & "|" & _
                c_add_line3 & "|" & c_add_line4 & "|" & c_add_line5 & "|" & Trim(c_pcode) & _
                "|" & name2_title & "|" & name2_forename & "|" & name2_surname & "|"

                If dob2 = CDate("1800,1,1") Then
                    outfile = outfile & "|"
                Else
                    outfile = outfile & Format(dob2, "dd/MM/yyyy") & "|"
                End If
                If debtor2_address_found Then
                    outfile = outfile & d_add_line1 & "|" & d_add_line2 & "|" & d_add_line3 & "|" & _
                    d_add_line4 & "|" & d_add_line5 & "|" & d_pcode & "|"
                Else
                    outfile = outfile & "||||||"
                End If
                If o_add_line1.Length > 0 Then
                    outfile = outfile & o_add_line1 & "|" & o_add_line2 & "|" & o_add_line3 & "|" & _
                    o_add_line4 & "|" & o_add_line5 & "|" & o_pcode & "|"
                Else
                    outfile = outfile & "||||||"
                End If
                debt_amt = debtor_dataset.Tables(0).Rows(idx).Item(14)
                Try
                    add_phone = Trim(debtor_dataset.Tables(0).Rows(idx).Item(11))
                Catch ex As Exception
                    add_phone = ""
                End Try
                Try
                    emp_phone = Trim(debtor_dataset.Tables(0).Rows(idx).Item(13))
                Catch ex As Exception
                    emp_phone = ""
                End Try
                Try
                    add_fax = Trim(debtor_dataset.Tables(0).Rows(idx).Item(12))
                Catch ex As Exception
                    add_fax = ""
                End Try
                outfile = outfile & Format(debt_amt, "F") & "|" & add_phone & "|" & emp_phone & "|" & _
                add_fax & "|"

                Try
                    determination_date = debtor_dataset.Tables(0).Rows(idx).Item(15)
                Catch ex As Exception
                    determination_date = CDate("1800 1 1")
                End Try
                If determination_date = CDate("1800,1,1") Then
                    outfile = outfile & "|"
                Else
                    outfile = outfile & Format(determination_date, "dd/MM/yyyy") & "|"
                End If
                outfile = outfile & vbNewLine
            Next
        Next


        With SaveFileDialog1
            .Title = "Arrow Acknowledgement File"
            .FileName = "Rossendales_PlacementLoadCheckFileReport_" & Format(Now, "dd_MM_yyyy_HH_mm_ss")
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName & ".txt", outfile, False)
            MsgBox("file created")
        Else
            MsgBox("File not created")
        End If
        Me.Close()

    End Sub
    Private Sub split_address_lines(ByVal address As String)
        Dim idx As Integer
        Dim start_idx As Integer = 1
        Dim add_line As String
        add_line1 = ""
        add_line2 = ""
        add_line3 = ""
        add_line4 = ""
        add_line5 = ""
        add_line6 = ""
        For idx = 1 To address.Length
            If Mid(address, idx, 1) = Chr(10) Or idx = address.Length Then
                If idx = address.Length Then
                    idx += 1
                End If
                add_line = Trim(Mid(address, start_idx, idx - start_idx))
                If add_line.Length > 0 Then
                    If add_line1 = "" Then
                        add_line1 = add_line
                    ElseIf add_line2 = "" Then
                        add_line2 = add_line
                    ElseIf add_line3 = "" Then
                        add_line3 = add_line
                    ElseIf add_line4 = "" Then
                        add_line4 = add_line
                    ElseIf add_line5 = "" Then
                        add_line5 = add_line
                    Else
                        add_line6 = add_line
                    End If
                End If
                start_idx = idx + 1
            End If
        Next
        'remove any OD0A remaining
        Dim addr_idx As Integer
        Dim new_addr As String = ""
        If add_line1 <> "" Then
            For addr_idx = 1 To add_line1.Length
                If Mid(add_line1, addr_idx, 1) = Chr(10) Or Mid(add_line1, addr_idx, 1) = Chr(13) Then
                    new_addr &= " "
                Else
                    new_addr &= Mid(add_line1, addr_idx, 1)
                End If
            Next
            add_line1 = new_addr
        End If
        If add_line2 <> "" Then
            new_addr = ""
            For addr_idx = 1 To add_line2.Length
                If Mid(add_line2, addr_idx, 1) = Chr(10) Or Mid(add_line2, addr_idx, 1) = Chr(13) Then
                    new_addr &= " "
                Else
                    new_addr &= Mid(add_line2, addr_idx, 1)
                End If
            Next
            add_line2 = new_addr
        End If
        If add_line3 <> "" Then
            new_addr = ""
            For addr_idx = 1 To add_line3.Length
                If Mid(add_line3, addr_idx, 1) = Chr(10) Or Mid(add_line3, addr_idx, 1) = Chr(13) Then
                    new_addr &= " "
                Else
                    new_addr &= Mid(add_line3, addr_idx, 1)
                End If
            Next
            add_line3 = new_addr
        End If
        If add_line4 <> "" Then
            new_addr = ""
            For addr_idx = 1 To add_line4.Length
                If Mid(add_line4, addr_idx, 1) = Chr(10) Or Mid(add_line4, addr_idx, 1) = Chr(13) Then
                    new_addr &= " "
                Else
                    new_addr &= Mid(add_line4, addr_idx, 1)
                End If
            Next
            add_line4 = new_addr
        End If
        If add_line5 <> "" Then
            new_addr = ""
            For addr_idx = 1 To add_line5.Length
                If Mid(add_line5, addr_idx, 1) = Chr(10) Or Mid(add_line5, addr_idx, 1) = Chr(13) Then
                    new_addr &= " "
                Else
                    new_addr &= Mid(add_line5, addr_idx, 1)
                End If
            Next
            add_line5 = new_addr
        End If
        If add_line6 <> "" Then
            new_addr = ""
            For addr_idx = 1 To add_line2.Length
                If Mid(add_line6, addr_idx, 1) = Chr(10) Or Mid(add_line6, addr_idx, 1) = Chr(13) Then
                    new_addr &= " "
                Else
                    new_addr &= Mid(add_line6, addr_idx, 1)
                End If
            Next
            add_line6 = new_addr
        End If
    End Sub
    Private Sub split_name2()
        Dim idx As Integer
        Dim start_idx As Integer = name2.Length
        name2_title = ""
        name2_forename = ""
        name2_surname = ""
        For idx = name2.Length To 1 Step -1
            If Mid(name2, idx, 1) = " " Or idx = 1 Then
                If idx = 1 Then
                    idx = 0
                End If
                If name2_surname = "" Then
                    name2_surname = Mid(name2, idx + 1, start_idx - idx)
                ElseIf name2_forename = "" Then
                    name2_forename = Mid(name2, idx + 1, start_idx - idx)
                Else
                    If name2_title = "" Then
                        name2_title = Mid(name2, idx + 1, start_idx - idx)
                    Else
                        name2_forename = name2_title & " " & name2_forename
                        name2_title = Mid(name2, idx + 1, start_idx - idx)
                    End If
                End If
                start_idx = idx - 1
            End If
        Next
        'check title is a title!
        Dim title As String = LCase(name2_title)
        If title <> "mr" And title <> "mrs" And title <> "ms" And title <> "Dr" _
        And title <> "miss" Then
            name2_forename = name2_title & " " & name2_forename
            name2_title = ""
        End If
    End Sub
    Private Sub split_debtor2_address()
        Dim idx As Integer
        Dim start_idx As Integer = 1
        Dim add_line As String
        d_add_line1 = ""
        d_add_line2 = ""
        d_add_line3 = ""
        d_add_line4 = ""
        d_add_line5 = ""
        d_pcode = ""
        For idx = 1 To debtor2_address.Length
            If Mid(debtor2_address, idx, 1) = "," Or _
             Mid(debtor2_address, idx, 1) = ";" Or idx = debtor2_address.Length Then
                add_line = Mid(debtor2_address, start_idx, idx - start_idx)
                If d_add_line1 = "" Then
                    d_add_line1 = add_line
                ElseIf d_add_line2 = "" Then
                    d_add_line2 = add_line
                ElseIf d_add_line3 = "" Then
                    d_add_line3 = add_line
                ElseIf d_add_line4 = "" Then
                    d_add_line4 = add_line
                ElseIf d_add_line5 = "" Then
                    d_add_line5 = add_line
                ElseIf d_pcode = "" Then
                    d_pcode = add_line
                End If
                start_idx = idx + 1
            End If
        Next
    End Sub
    Private Sub enable_buttons()
        exitbtn.Enabled = True
        runbtn.Enabled = True
        run_datepicker.Enabled = True
    End Sub
End Class

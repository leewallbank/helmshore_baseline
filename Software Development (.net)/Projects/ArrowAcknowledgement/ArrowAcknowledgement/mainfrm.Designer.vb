<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.run_datepicker = New System.Windows.Forms.DateTimePicker
        Me.Label1 = New System.Windows.Forms.Label
        Me.runbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.accman_rbtn = New System.Windows.Forms.RadioButton
        Me.guernrbtn = New System.Windows.Forms.RadioButton
        Me.globrbtn = New System.Windows.Forms.RadioButton
        Me.rmlbtn = New System.Windows.Forms.RadioButton
        Me.cl_gbox = New System.Windows.Forms.GroupBox
        Me.erudiorbtn = New System.Windows.Forms.RadioButton
        Me.cl_gbox.SuspendLayout()
        Me.SuspendLayout()
        '
        'run_datepicker
        '
        Me.run_datepicker.Location = New System.Drawing.Point(63, 232)
        Me.run_datepicker.Name = "run_datepicker"
        Me.run_datepicker.Size = New System.Drawing.Size(136, 20)
        Me.run_datepicker.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(101, 216)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(42, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Run for"
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(93, 275)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(75, 23)
        Me.runbtn.TabIndex = 1
        Me.runbtn.Text = "Run report"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(194, 321)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 321)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 4
        '
        'accman_rbtn
        '
        Me.accman_rbtn.AutoSize = True
        Me.accman_rbtn.Checked = True
        Me.accman_rbtn.Location = New System.Drawing.Point(6, 27)
        Me.accman_rbtn.Name = "accman_rbtn"
        Me.accman_rbtn.Size = New System.Drawing.Size(135, 17)
        Me.accman_rbtn.TabIndex = 5
        Me.accman_rbtn.TabStop = True
        Me.accman_rbtn.Text = "Accounts Management"
        Me.accman_rbtn.UseVisualStyleBackColor = True
        '
        'guernrbtn
        '
        Me.guernrbtn.AutoSize = True
        Me.guernrbtn.Location = New System.Drawing.Point(6, 50)
        Me.guernrbtn.Name = "guernrbtn"
        Me.guernrbtn.Size = New System.Drawing.Size(70, 17)
        Me.guernrbtn.TabIndex = 6
        Me.guernrbtn.Text = "Guernsey"
        Me.guernrbtn.UseVisualStyleBackColor = True
        '
        'globrbtn
        '
        Me.globrbtn.AutoSize = True
        Me.globrbtn.Location = New System.Drawing.Point(6, 73)
        Me.globrbtn.Name = "globrbtn"
        Me.globrbtn.Size = New System.Drawing.Size(73, 17)
        Me.globrbtn.TabIndex = 7
        Me.globrbtn.Text = "Global Ltd"
        Me.globrbtn.UseVisualStyleBackColor = True
        '
        'rmlbtn
        '
        Me.rmlbtn.AutoSize = True
        Me.rmlbtn.Location = New System.Drawing.Point(6, 96)
        Me.rmlbtn.Name = "rmlbtn"
        Me.rmlbtn.Size = New System.Drawing.Size(48, 17)
        Me.rmlbtn.TabIndex = 8
        Me.rmlbtn.Text = "RML"
        Me.rmlbtn.UseVisualStyleBackColor = True
        '
        'cl_gbox
        '
        Me.cl_gbox.Controls.Add(Me.erudiorbtn)
        Me.cl_gbox.Controls.Add(Me.accman_rbtn)
        Me.cl_gbox.Controls.Add(Me.rmlbtn)
        Me.cl_gbox.Controls.Add(Me.guernrbtn)
        Me.cl_gbox.Controls.Add(Me.globrbtn)
        Me.cl_gbox.Location = New System.Drawing.Point(31, 31)
        Me.cl_gbox.Name = "cl_gbox"
        Me.cl_gbox.Size = New System.Drawing.Size(200, 155)
        Me.cl_gbox.TabIndex = 9
        Me.cl_gbox.TabStop = False
        Me.cl_gbox.Text = "Select Client"
        '
        'erudiorbtn
        '
        Me.erudiorbtn.AutoSize = True
        Me.erudiorbtn.Location = New System.Drawing.Point(6, 119)
        Me.erudiorbtn.Name = "erudiorbtn"
        Me.erudiorbtn.Size = New System.Drawing.Size(55, 17)
        Me.erudiorbtn.TabIndex = 9
        Me.erudiorbtn.Text = "Erudio"
        Me.erudiorbtn.UseVisualStyleBackColor = True
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 372)
        Me.Controls.Add(Me.cl_gbox)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.run_datepicker)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Arrow Acknowledgement"
        Me.cl_gbox.ResumeLayout(False)
        Me.cl_gbox.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents run_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents accman_rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents guernrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents globrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents rmlbtn As System.Windows.Forms.RadioButton
    Friend WithEvents cl_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents erudiorbtn As System.Windows.Forms.RadioButton

End Class

Imports System.IO
Public Class mainfrm

    Dim filename As String = ""
    Dim filepath As String = ""
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD134Preport = New RD134P
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD134Preport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD134Preport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD134Preport)
        ProgressBar1.Value = 10
        Application.DoEvents()
        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = "RD134P Aktiv Kapital Invoice.pdf"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                RD134Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
                RD134Preport.close()
                filepath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                filepath &= "\"
            Else
                MsgBox("Report not saved")
            End If
        End If
        runbtn.Enabled = False
        exitbtn.Enabled = False
        ProgressBar1.Value = 50
        Application.DoEvents()
        filename = filepath & "RD134DP Aktiv Kapital Direct Payment Invoice.pdf"
        Dim RD134DPreport = New RD134DP
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD134DPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD134DPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD134DPreport)
        RD134DPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD134Preport.close()
        MsgBox("Reports saved")
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

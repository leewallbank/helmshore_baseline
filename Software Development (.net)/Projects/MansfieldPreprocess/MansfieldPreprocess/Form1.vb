Public Class Form1

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click
        Dim new_file As String
        Try
            With OpenFileDialog1
                .Title = "Read XML file"
                .Filter = "XML file|*.xml"
                .FileName = ""
                .InitialDirectory = "c:\"
                .CheckFileExists = True
            End With
            Dim xml_dataSet As New DataSet()
            If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                xml_dataSet.ReadXml(OpenFileDialog1.FileName)
            Else
                MsgBox("XML file not processed")
                Exit Sub
            End If

            Dim row As DataRow
            Dim file As String = "Module" & vbTab & "AccountNum" & vbTab & "Amt Due" & vbTab & "Control" & _
            vbTab & "Hearing date" & vbTab & "Cur balance" & vbTab & "Costs bal" & vbTab & "Total costs" & _
            vbTab & "LO Amt" & vbTab & "Title" & vbTab & "initial" & vbTab & "forenames" & vbTab & _
            "surname" & vbTab & "Curr addr1" & vbTab & "Curr addr2" & vbTab & "Curr addr3" & vbTab & _
            "Curr addr4" & vbTab & "Curr addr5" & vbTab & "Curr postcode" & vbTab & "Debt Addr1" & vbTab & _
            "Debt addr2" & vbTab & "Debt addr3" & vbTab & "Debt addr4" & vbTab & "Debt addr5" & vbTab & "Debt postcode" & _
            vbTab & "Charge startdate" & vbTab & "Charge enddate" & vbTab & "Second name" & vbNewLine
            Dim idx As Integer = 0
            Dim filename As String = OpenFileDialog1.FileName
            Dim record As String = ""
            Dim second_name As String = ""
            For Each row In xml_dataSet.Tables(0).Rows
                If idx > 0 Then
                    'check if same refnum and from date but different name
                    If xml_dataSet.Tables(0).Rows(idx - 1).Item(1) = xml_dataSet.Tables(0).Rows(idx).Item(1) _
                    And xml_dataSet.Tables(0).Rows(idx - 1).Item(26) = xml_dataSet.Tables(0).Rows(idx).Item(26) _
                    And (xml_dataSet.Tables(0).Rows(idx - 1).Item(12) <> xml_dataSet.Tables(0).Rows(idx).Item(12) _
                    Or xml_dataSet.Tables(0).Rows(idx - 1).Item(11) <> xml_dataSet.Tables(0).Rows(idx).Item(11) _
                    Or xml_dataSet.Tables(0).Rows(idx - 1).Item(9) <> xml_dataSet.Tables(0).Rows(idx).Item(9)) Then

                        second_name = Trim(xml_dataSet.Tables(0).Rows(idx - 1).Item(9)) & " " & _
                        Trim(xml_dataSet.Tables(0).Rows(idx - 1).Item(11)) & " " & _
                        Trim(xml_dataSet.Tables(0).Rows(idx - 1).Item(12))
                        record = ""
                    Else
                        second_name = ""
                        file = file & record
                        record = ""
                    End If
                End If
                Dim col As DataColumn
                Dim idx2 As Integer = 0
                For Each col In xml_dataSet.Tables(0).Columns
                    record = record & xml_dataSet.Tables(0).Rows(idx).Item(idx2) & vbTab
                    idx2 += 1
                Next
                record = record & second_name & vbNewLine
                idx += 1
            Next
            'save last record

            Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_preprocess.txt"
            My.Computer.FileSystem.WriteAllText(new_file, file, False)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        MsgBox("file saved as " & new_file)
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub
End Class

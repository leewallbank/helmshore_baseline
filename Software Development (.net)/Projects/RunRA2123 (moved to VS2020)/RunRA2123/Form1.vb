Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       

    End Sub

    Private Sub schbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        
    End Sub

    Private Sub clbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clientbtn.Click
        clientfrm.ShowDialog()
        schemefrm.ShowDialog()
        schemefrm.sch_dg.EndEdit()
        clientbtn.Enabled = False
        disable_btns()

        'select report
        If startdtp.Visible Then
            If valuesrbtn.Checked Then
                run_RA2123CD()
            Else
                run_RA2123D()
            End If
        ElseIf valuesrbtn.Checked Then
            run_RA2123C()
        Else
            run_RA2123()
        End If
        Me.Close()
    End Sub
    Private Sub run_RA2123()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123report = New RA2123
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(clID)
        SetCurrentValuesForParameterField1(RA2123report, myArrayList1)

        Dim schRow As DataGridViewRow
        For Each schRow In schemefrm.sch_dg.Rows
            If schRow.Cells(0).Value Then
                Dim schID As Integer = schRow.Cells(2).Value
                myArrayList1.Add(schID)
                SetCurrentValuesForParameterField2(RA2123report, myArrayList1)
            End If
        Next

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123 Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123report.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub disable_btns()
        datebtn.Enabled = False
        clientbtn.Enabled = False
        branchbtn.Enabled = False
        exitbtn.Enabled = False
    End Sub

    Private Sub run_RA2123C()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123Creport = New RA2123C
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(clID)
        SetCurrentValuesForParameterField1(RA2123Creport, myArrayList1)

        Dim schRow As DataGridViewRow
        For Each schRow In schemefrm.sch_dg.Rows
            If schRow.Cells(0).Value Then
                Dim schID As Integer = schRow.Cells(2).Value
                myArrayList1.Add(schID)
                SetCurrentValuesForParameterField2(RA2123Creport, myArrayList1)
            End If
        Next

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123Creport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123C Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123Creport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123Creport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2123D()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123Dreport = New RA2123D
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(clID)
        SetCurrentValuesForParameterField1(RA2123Dreport, myArrayList1)
        Dim schRow As DataGridViewRow
        For Each schRow In schemefrm.sch_dg.Rows
            If schRow.Cells(0).Value Then
                Dim schID As Integer = schRow.Cells(2).Value
                myArrayList1.Add(schID)
                SetCurrentValuesForParameterField2(RA2123Dreport, myArrayList1)
            End If
        Next
        myArrayList1.Add(startdtp.Value)
        SetCurrentValuesForParameterField3(RA2123Dreport, myArrayList1)
        myArrayList1.Add(enddtp.Value)
        SetCurrentValuesForParameterField4(RA2123Dreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123Dreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123D Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123Dreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123Dreport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2123CD()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123CDreport = New RA2123CD
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(clID)
        SetCurrentValuesForParameterField1(RA2123CDreport, myArrayList1)
        Dim schRow As DataGridViewRow
        For Each schRow In schemefrm.sch_dg.Rows
            If schRow.Cells(0).Value Then
                Dim schID As Integer = schRow.Cells(2).Value
                myArrayList1.Add(schID)
                SetCurrentValuesForParameterField2(RA2123CDreport, myArrayList1)
            End If
        Next
        myArrayList1.Add(startdtp.Value)
        SetCurrentValuesForParameterField3(RA2123CDreport, myArrayList1)
        myArrayList1.Add(enddtp.Value)
        SetCurrentValuesForParameterField4(RA2123CDreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123CDreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123CD Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123CDreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123CDreport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2123X()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123Xreport = New RA2123X
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123Xreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123X Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123Xreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123Xreport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2123XB()
        Dim RA2123XBreport = New RA2123XB
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(branch_no)
        SetCurrentValuesForParameterField5(RA2123XBreport, myArrayList1)
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123XBreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123XB Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123XBreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123XBreport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2123CX()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123CXreport = New RA2123CX
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123CXreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123CX Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123CXreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123CXreport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub


    Private Sub datebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles datebtn.Click
        If startlbl.Visible Then
            startlbl.Visible = False
            startdtp.Visible = False
            endlbl.Visible = False
            enddtp.Visible = False
        Else
            startlbl.Visible = True
            startdtp.Visible = True
            endlbl.Visible = True
            enddtp.Visible = True
        End If
    End Sub

    Private Sub selectrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles selectrbtn.CheckedChanged
        clientlbl.Visible = selectrbtn.Checked
        clientbtn.Visible = selectrbtn.Checked
        cl_tbox.Visible = selectrbtn.Checked
    End Sub

    Private Sub allrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allrbtn.CheckedChanged
        If allrbtn.Checked Then
            If startdtp.Visible Then
                MsgBox("Dates not yet added for ALL clients option")
            Else

                If MsgBox("Run for all clients", MsgBoxStyle.YesNo, "Run for ALL clients") = MsgBoxResult.Yes Then
                    exitbtn.Enabled = False
                    datebtn.Enabled = False
                    If valuesrbtn.Checked Then
                        run_RA2123CX()
                    Else
                        run_RA2123X()
                    End If
                    Me.Close()
                End If

            End If
        End If
    End Sub

    Private Sub brrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles brrbtn.CheckedChanged
        branchbtn.Visible = brrbtn.Checked

    End Sub

    Private Sub branch_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub branchrbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles branchbtn.Click
        Try
            branch_no = InputBox("Enter branch no", "Enter branch no")
        Catch ex As Exception
            MsgBox("Invalid branch number")
            Exit Sub
        End Try
        disable_btns()
        run_RA2123XB()
        Me.Close()
    End Sub
End Class

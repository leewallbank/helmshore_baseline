Imports System.IO
Public Class Form1
    Dim allReports As Boolean
    Dim filename As String = ""
    Dim filepath As String = ""
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub RA890LSCbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RD890LSCbtn.Click
        disable_buttons()
        run_RA890LSC()
    End Sub
    Private Sub run_RA890CMEC()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA890CMECreport = New RA890CMEC
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA890CMECreport, myConnectionInfo)
        filename = "RA890 CMEC All Payments.xls"
        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = "RA890 CMEC All Payments.xls"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                
            Else
                MsgBox("Report not saved")
            End If
        Else
            filename = filepath & filename
        End If
        RA890CMECreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
        RA890CMECreport.close()
        If Not allReports Then
            MsgBox("Completed")
            Me.Close()
        End If
    End Sub
    Private Sub run_RA890LSC()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA890LSCreport = New RA890LSC
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA890LSCreport, myConnectionInfo)
        filename = "RA890 LSC All Payments.xls"
        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = "RA890 LSC All Payments.xls"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                filepath = Path.GetDirectoryName(SaveFileDialog1.FileName) & "\"
                
            Else
                MsgBox("Report not saved")
            End If
        Else
            filename = filepath & filename
        End If
        RA890LSCreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
        RA890LSCreport.close()
        If Not allReports Then
            MsgBox("Completed")
            Me.Close()
        End If
    End Sub
    
       
    Private Sub disable_buttons()
        exitbtn.Enabled = False
        RD890LSCbtn.Enabled = False
        allbtn.Enabled = False
        ra890cmecbtn.Enabled = False
        ra1624btn.Enabled = False
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        allReports = False
    End Sub

    Private Sub ra890cmecbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ra890cmecbtn.Click
        disable_buttons()
        run_RA890CMEC()
    End Sub

    Private Sub allbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allbtn.Click
        allReports = True
        disable_buttons()
        run_RA890LSC()
        run_RA890CMEC()
        run_RA1624()
    End Sub

    Private Sub ra1624btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ra1624btn.Click
        disable_buttons()
        run_RA1624()
    End Sub
    Private Sub run_RA1624()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA1624report = New RA1624
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA1624report, myConnectionInfo)
        filename = "RA1624 LSC Transactions fees added.xls"
        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = "RA1624 LSC Transactions fees added.xls"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
            Else
                MsgBox("Report not saved")
                Exit Sub
            End If
        Else
            filename = filepath & filename
        End If
        RA1624report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
        RA1624report.close()
        If Not allReports Then
            MsgBox("Completed")
        End If
        Me.Close()
    End Sub
End Class

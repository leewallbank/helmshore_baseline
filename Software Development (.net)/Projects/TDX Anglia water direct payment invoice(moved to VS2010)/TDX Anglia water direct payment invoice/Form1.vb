Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub


    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        runbtn.Enabled = False
        exitbtn.Enabled = False
        'get all clientschemes for TDX (Anglia water)
        param2 = "select _rowid from clientScheme where clientID = 1766"
        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        Dim sc_rows As Integer = no_of_rows - 1
        Dim cs_idx As Integer
        Dim payment_found As Boolean = False
        '" where (status ='W' or status = 'R') and clientschemeID = " & csID
        For cs_idx = 0 To sc_rows
            'get all direct payments for TDX remitted on remit date
            Dim csID As Integer = cs_ds.Tables(0).Rows(cs_idx).Item(0)
            param2 = "select _rowid, debtorID, amount_reference, amount_sourceID," & _
            " status_date, date, status from Payment" & _
            " where (status ='W' or status = 'R') and clientschemeID = " & csID
            Dim pay_ds As DataSet = get_dataset("onestep", param2)
            Dim pay_rows As Integer = no_of_rows - 1
            Dim pay_idx As Integer
            For pay_idx = 0 To pay_rows
                Try
                    ProgressBar1.Value = (pay_idx / pay_rows) * 100
                Catch ex As Exception

                End Try
                Application.DoEvents()
                Dim status As String = pay_ds.Tables(0).Rows(pay_idx).Item(6)
                If status = "R" Then
                    Dim status_date As Date = pay_ds.Tables(0).Rows(pay_idx).Item(4)
                    If Format(status_date, "yyyy-MM-dd") <> Format(remit_dtp.Value, "yyyy-MM-dd") Then
                        Continue For
                    End If
                    payment_found = True
                End If
                Dim sourceID As Integer = pay_ds.Tables(0).Rows(pay_idx).Item(3)
                param2 = "select  direct from PaySource where _rowid = " & sourceID
                Dim ps_ds As DataSet = get_dataset("onestep", param2)
                If ps_ds.Tables(0).Rows(0).Item(0) = "N" Then
                    Continue For
                End If

                Dim pay_auditID As Integer = pay_ds.Tables(0).Rows(pay_idx).Item(0)
                Me.TDX_payment_tran_datesTableAdapter1.FillBy(Me.FeesSQLDataSet.TDX_payment_tran_dates, pay_auditID)
                If Me.FeesSQLDataSet.TDX_payment_tran_dates.Rows.Count = 0 Then
                    Dim amt_ref As String = pay_ds.Tables(0).Rows(pay_idx).Item(2)
                    Dim debtorID As String = pay_ds.Tables(0).Rows(pay_idx).Item(1)
                    Dim tran_date As Date = Nothing
                    If amt_ref.Length = 8 Then
                        Try
                            tran_date = CDate(Microsoft.VisualBasic.Left(amt_ref, 4) & "," & Mid(amt_ref, 5, 2) & "," & Microsoft.VisualBasic.Right(amt_ref, 2))
                        Catch ex As Exception

                        End Try
                    End If
                    Dim paid_date As Date = pay_ds.Tables(0).Rows(pay_idx).Item(5)
                    If tran_date = Nothing Then
                        'look in notes for date
                        Dim paid_date_plus1 As Date = DateAdd(DateInterval.Day, 1, paid_date)
                        param2 = "select text from Note where debtorID = " & debtorID & _
                        " and _createdDate >= '" & Format(paid_date, "yyyy-MM-dd") & "'" & _
                        " and _createdDate < '" & Format(paid_date_plus1, "yyyy-MM-dd") & "'" & _
                        " and type = 'Note' order by _createdDate"
                        Dim note_ds As DataSet = get_dataset("onestep", param2)
                        Dim note_rows As Integer = no_of_rows - 1
                        Dim note_idx As Integer
                        For note_idx = 0 To note_rows
                            Dim note_txt As String = note_ds.Tables(0).Rows(note_idx).Item(0)
                            If Microsoft.VisualBasic.Left(note_txt, 2) = "PR" Then
                                amt_ref = Microsoft.VisualBasic.Right(note_txt, 8)
                                Try
                                    tran_date = CDate(Microsoft.VisualBasic.Left(amt_ref, 4) & "," & Mid(amt_ref, 5, 2) & "," & Microsoft.VisualBasic.Right(amt_ref, 2))
                                Catch ex As Exception

                                End Try
                            End If
                        Next
                    End If
                    Dim tran_date_valid As Boolean = True
                    If tran_date < CDate("jan 1, 2000") Then
                        tran_date_valid = False
                    End If
                    If tran_date <> Nothing And tran_date <> CDate("Jan 1, 1900") Then
                        If DateDiff(DateInterval.Day, tran_date, paid_date) > 100 Then
                            tran_date_valid = False
                        End If
                    End If

                    If tran_date_valid Then
                        Me.TDX_payment_tran_datesTableAdapter1.InsertQuery(pay_auditID, tran_date)
                    Else
                        While tran_date_valid = False
                            Try
                                tran_date = InputBox("Enter date for case " & debtorID & vbNewLine & _
                                   "Payment date " & Format(paid_date, "dd/MM/yyyy") & vbNewLine & _
                                   "format dd.mm.yyyy", "Enter transaction date")
                            Catch ex As Exception
                                MsgBox("Invalid date")
                                Continue While
                            End Try
                            tran_date_valid = True
                            If DateDiff(DateInterval.Day, tran_date, paid_date) > 100 Then
                                tran_date_valid = False
                            End If
                        End While
                        Me.TDX_payment_tran_datesTableAdapter1.InsertQuery(pay_auditID, tran_date)
                    End If
                End If
            Next

        Next
        If payment_found Then
            'write out crystal report
            Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
            Dim RD253D4report = New RD253D4
            Dim myArrayList1 As ArrayList = New ArrayList()
            myArrayList1.Add(remit_dtp.Value)
            SetCurrentValuesForParameterField1(RD253D4report, myArrayList1)

            Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
            'myConnectionInfo.ServerName = "DebtRecovery"
            'myConnectionInfo.DatabaseName = "DebtRecovery"
            'myConnectionInfo.UserID = "vbnet"
            'myConnectionInfo.Password = "tenbv"
            myConnectionInfo.ServerName = "DebtRecovery-Real"
            myConnectionInfo.DatabaseName = "DebtRecovery"
            myConnectionInfo.UserID = "thirdparty"
            myConnectionInfo.Password = "thirdparty"
            myConnectionInfo2.ServerName = "RossFeeTable"
            myConnectionInfo2.DatabaseName = "FeesSQL"
            myConnectionInfo2.UserID = "sa"
            myConnectionInfo2.Password = "sa"
            SetDBLogonForReport(myConnectionInfo, myConnectionInfo2, RD253D4report)


            Dim filename As String
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "PDF files |*.pdf"
                .DefaultExt = ".pdf"
                .OverwritePrompt = True
                .FileName = "RD253D4 TDX Invoice.pdf"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
            Else
                MsgBox("Report not saved")
                Me.Close()
                Exit Sub
            End If
            RD253D4report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
            RD253D4report.close()
            MsgBox("report saved")
        Else
            MsgBox("Nothing found for remit date")
        End If
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        remit_dtp.Value = DateAdd(DateInterval.Day, -Weekday(Now), Now)

    End Sub

   
End Class

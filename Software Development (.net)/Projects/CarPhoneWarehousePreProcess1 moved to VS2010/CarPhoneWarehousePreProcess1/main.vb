Public Class mainform
    
    Dim tot_arrow = 0
    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim idx, idx2 As Integer
        Dim lines As Integer = 0
        Dim amt, offence_val As Decimal
        Dim offence_court, reg_date, from_date, dob, determination_date As Date
        Dim clref As String = ""
        Dim off_number As String = ""
        Dim title As String = ""
        Dim forename As String = ""
        Dim affinity_brand As String = ""
        Dim product_type As String = ""
        Dim seller_name As String = ""
        Dim surname As String = ""
        Dim addr1 As String = ""
        Dim addr2 As String = ""
        Dim addr3 As String = ""
        Dim addr4 As String = ""
        Dim addr5 As String = ""
        Dim pcode As String = ""
        Dim d_addr1 As String = ""
        Dim d_addr2 As String = ""
        Dim d_addr3 As String = ""
        Dim d_addr4 As String = ""
        Dim d_addr5 As String = ""
        Dim d_pcode As String = ""
        Dim name2 As String = ""
        Dim comments As String = ""
        Dim debtor2_address_found As Boolean
        Dim debtor2_address As String = ""
        Dim addr_line As String
        Dim home_phone As String = ""
        Dim mobile_phone As String = ""
        Dim work_phone As String = ""
        ack_file = ""
        Dim twenty_years_ago As Date = CDate(Year(Now) - 20 & "," & Month(Now) & "," & Microsoft.VisualBasic.Day(Now))

        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "Client ref|Offence Number|Offence from|Reg date|Offence Court|title|forename|surname|DOB|" & _
        "curr_addr1|curr_addr2|curr_addr3|curr_addr4|curr_addr5|curr_postcode|name2|Debt addr1|" & _
        "Debt addr2|Debt add3|Debt addr4|Debt addr5|Debt pcode|DebtAmount|Offence value|" & _
        "HomePhone|WorkPhone|MobilePhone|AffinityBrand|ProductTypeDesc|SellerName|DeterminationDate|comments" & vbNewLine
        outfile = outline
        out_rml = outline
        out_guernsey = outline
        'ack_file = "ArrowKey|CustAccNo|PortfolioDescription|Debtor1_Title|Debtor1_FirstName|" & _
        '"Debtor_1surname|Debtor1_DateOfBirth|Debtor1_addressLine1|Debtor1_addressLine2|Debtor1_addressLine3|" & _
        '"Debtor1_addressLine4|Debtor1_addressLine5|Debtor1_postcode|Debtor2_Title|Debtor2_FirstName|" & _
        '"Debtor2_surname|Debtor2_DateOfBirth|Debtor2_addressLine1|Debtor2_addressLine2|Debtor2_addressLine3|" & _
        '"Debtor2_addressLine4|Debtor2_addressLine5|Debtor2_postcode|Debtor1_SelleraddressLine1|" & _
        '"Debtor1_SelleraddressLine2|Debtor1_SelleraddressLine3|Debtor1_SelleraddressLIne4|" & _
        '"Debtor1_SelleraddressLine5|Debtor1_SellerPostcode|AccountBalance|HomePhoneNumber|" & _
        '"WorkPhoneNumber|MobilePhoneNumber" & vbNewLine

        'process cases delimited by a pipe
        Dim buyer_name As String = ""
        For idx = 1 To lines - 1
            ProgressBar1.Value = (idx / lines) * 100
            Dim val_string As String
            Dim date_string, field As String
            Dim line_length As Integer = line(idx).Length
            Dim start_field_idx As Integer = 2
            Dim end_field_idx As Integer = 0
            Dim field_no As Integer = 0

            For idx2 = 1 To line_length
                If Mid(line(idx), idx2, 1) = "|" Or idx2 = line_length Then
                    field_no += 1
                    end_field_idx = idx2
                    If idx2 = line_length Then
                        field = Mid(line(idx), start_field_idx, end_field_idx - start_field_idx + 1)
                    Else
                        field = Mid(line(idx), start_field_idx, end_field_idx - start_field_idx)
                    End If
                    field = Trim(field)
                    Select Case field_no
                        Case 1
                            clref = field
                            ack_file = ack_file & field & "|"
                        Case 2
                            off_number = field
                            ack_file = ack_file & field & "|"
                        Case 3
                            'comments = comments & "AffinityBrand:" & field & ";"
                            affinity_brand = field
                        Case 4
                            comments = comments & "PortfolioDescription:" & field & ";"
                            ack_file = ack_file & field & "|"
                        Case 5
                            date_string = field
                            If Not IsDate(date_string) Then
                                'errorfile = errorfile & "Line  " & idx & " - LOffence From Date not valid - " _
                                '                             & date_string & vbNewLine
                                from_date = Nothing
                            Else
                                from_date = date_string
                                'ignore cases > 20 years old
                                If Format(from_date, "yyyy-MM-dd") < _
                                Format(twenty_years_ago, "yyyy-MM-dd") Then
                                    from_date = Nothing
                                End If
                            End If
                        Case 6
                            comments = comments & "DefaultedAmount:" & field & ";"
                        Case 7
                            date_string = field
                            If Not IsDate(date_string) Then
                                errorfile = errorfile & "Line  " & idx & " - Reg Date not valid - " _
                                                             & date_string & vbNewLine
                            Else
                                reg_date = date_string
                                'ignore reg date if > 20 years old
                                If Format(reg_date, "yyyy-MM-dd") < _
                                Format(twenty_years_ago, "yyyy-MM-dd") Then
                                    reg_date = Nothing
                                End If
                            End If
                        Case 8
                            date_string = field
                            If Not IsDate(date_string) Then
                                errorfile = errorfile & "Line  " & idx & " - Offence Court Date not valid - " _
                                                             & date_string & vbNewLine
                            Else
                                offence_court = date_string
                            End If
                        Case 9
                            title = field
                            ack_file = ack_file & field & "|"
                        Case 10
                            forename = field
                            ack_file = ack_file & field & "|"
                        Case 11
                            surname = field
                            ack_file = ack_file & field & "|"
                        Case 12
                            date_string = field
                            dob = Nothing
                            If field.Length > 0 Then
                                If Not IsDate(date_string) Then
                                    errorfile = errorfile & "Line  " & idx & " - DOB not valid - " _
                                                                 & date_string & vbNewLine
                                Else
                                    dob = date_string
                                End If
                            End If

                            ack_file = ack_file & field & "|"
                        Case 13
                            addr1 = field
                            ack_file = ack_file & field & "|"
                        Case 14
                            addr2 = field
                            ack_file = ack_file & field & "|"
                        Case 15
                            addr3 = field
                            ack_file = ack_file & field & "|"
                        Case 16
                            addr4 = field
                            ack_file = ack_file & field & "|"
                        Case 17
                            addr5 = field
                            ack_file = ack_file & field & "|"
                        Case 18
                            pcode = field
                            ack_file = ack_file & field & "|"
                        Case 19
                            name2 = Trim(field)
                        Case 20
                            name2 = Trim(name2 & " " & Trim(field))
                        Case 21
                            name2 = Trim(name2 & " " & Trim(field))
                            ack_file = ack_file & field & "|"
                        Case 22
                            If name2.Length > 0 And field.Length > 0 Then
                                date_string = field
                                If Not IsDate(date_string) Then
                                    errorfile = errorfile & "Line  " & idx & " - DOB2 not valid - " _
                                                                 & date_string & vbNewLine
                                Else
                                    comments = comments & "DobDebtor2:" & date_string & ";"
                                End If
                            End If
                            ack_file = ack_file & field & "|"
                        Case 23
                            If name2.Length > 0 Then
                                debtor2_address = Trim(field)
                                If debtor2_address.Length > 0 Then
                                    debtor2_address_found = True
                                    debtor2_address = remove_commas(debtor2_address) & ","
                                End If
                            End If
                        Case 24
                            If debtor2_address_found Then
                                addr_line = Trim(remove_commas(field))
                                If addr_line.Length > 0 Then
                                    debtor2_address = debtor2_address & addr_line & ","
                                End If
                            End If
                        Case 25
                            If debtor2_address_found Then
                                addr_line = Trim(remove_commas(field))
                                If addr_line.Length > 0 Then
                                    debtor2_address = debtor2_address & addr_line & ","
                                End If
                            End If
                        Case 26
                            If debtor2_address_found Then
                                addr_line = Trim(remove_commas(field))
                                If addr_line.Length > 0 Then
                                    debtor2_address = debtor2_address & addr_line & ","
                                End If
                            End If
                        Case 27
                            If debtor2_address_found Then
                                addr_line = Trim(remove_commas(field))
                                If addr_line.Length > 0 Then
                                    debtor2_address = debtor2_address & addr_line & ","
                                End If
                            End If
                        Case 28
                            If debtor2_address_found Then
                                addr_line = Trim(remove_commas(field))
                                If addr_line.Length > 0 Then
                                    debtor2_address = debtor2_address & addr_line
                                End If
                            End If
                            ack_file = ack_file & debtor2_address & "|"
                        Case 29
                            d_addr1 = field
                            ack_file = ack_file & field & "|"
                        Case 30
                            d_addr2 = field
                            ack_file = ack_file & field & "|"
                        Case 31
                            d_addr3 = field
                            ack_file = ack_file & field & "|"
                        Case 32
                            d_addr4 = field
                            ack_file = ack_file & field & "|"
                        Case 33
                            d_addr5 = field
                            ack_file = ack_file & field & "|"
                        Case 34
                            d_pcode = field
                            ack_file = ack_file & field & "|"
                        Case 35
                            val_string = field
                            If Not IsNumeric(val_string) Then
                                errorfile = errorfile & "Line  " & idx & " - amount not numeric - " _
                                         & val_string & vbNewLine
                            Else
                                amt = val_string
                            End If
                            ack_file = ack_file & field & "|"
                        Case 36
                            val_string = field
                            If Not IsNumeric(val_string) Then
                                errorfile = errorfile & "Line  " & idx & " - offence value not numeric - " _
                                         & val_string & vbNewLine
                            Else
                                offence_val = val_string
                            End If
                        Case 37
                            If field.Length > 0 Then
                                date_string = field
                                If Not IsDate(date_string) Then
                                    errorfile = errorfile & "Line  " & idx & " - Last payment date not valid - " _
                                                                 & date_string & vbNewLine
                                Else
                                    comments = comments & "LastPaymentDate:" & date_string & ";"
                                End If
                            End If
                        Case 38
                            If field.Length > 0 Then
                                val_string = field
                                If Not IsNumeric(val_string) Then
                                    errorfile = errorfile & "Line  " & idx & " - Last payment amount not numeric - " _
                                             & val_string & vbNewLine
                                Else
                                    comments = comments & "LastPaymentAmount:" & val_string & ";"
                                End If
                            End If
                        Case 39
                            If field.Length > 0 Then
                                If field.Length > 0 Then
                                    comments = comments & "LastPaymentMethod:" & field & ";"
                                End If
                            End If
                        Case 40
                            home_phone = field
                            ack_file = ack_file & field & "|"
                        Case 41
                            work_phone = field
                            ack_file = ack_file & field & "|"
                        Case 42
                            mobile_phone = field
                            ack_file = ack_file & field & vbNewLine
                        Case 43
                            If field.Length > 0 Then
                                comments = comments & "Liquidity:" & field & ";"
                            End If
                        Case 44
                            buyer_name = ""
                            If field.Length > 0 Then
                                buyer_name = field
                                comments = comments & "BuyerName:" & field & ";"
                            End If
                        Case 45
                            If field.Length > 0 Then
                                'comments = comments & "SellerName:" & field & ";"
                                seller_name = field
                            End If
                        Case 46
                            If field.Length > 0 Then
                                comments = comments & "OriginatorName:" & field & ";"
                            End If
                        Case 47
                            If field.Length > 0 Then
                                'comments = comments & "TypeDescription:" & field & ";"
                                product_type = field
                            End If
                        Case 48
                            If field.Length > 0 Then
                                comments = comments & "HomeOwnerIndicator:" & field & ";"
                            End If
                        Case 49
                            If field.Length > 0 Then
                                comments = comments & "LitigateIndicator:" & field & ";"
                            End If
                        Case 50
                            If field.Length > 0 Then
                                date_string = field
                                If Not IsDate(date_string) Then
                                    errorfile = errorfile & "Line  " & idx & " - Determination date not valid - " _
                                                                 & date_string & vbNewLine
                                Else
                                    determination_date = field
                                End If
                            End If
                    End Select
                    start_field_idx = idx2 + 1
                End If
            Next
            'validate case details
            If clref = Nothing Or Microsoft.VisualBasic.Left(clref, 3) = "   " Then
                errorfile = errorfile & "Line  " & idx2 & " - No client reference" & vbNewLine
            End If


            'save case in outline
            outline = clref & "|" & off_number & "|"
            If from_date = Nothing Then
                outline = outline & "|"
            Else
                outline = outline & from_date & "|"
            End If

            If reg_date = Nothing Then
                outline = outline & "|"
            Else
                outline = outline & reg_date & "|"
            End If

            outline = outline & offence_court & "|" & title & "|" & forename & "|" & surname & "|" & _
                        dob & "|" & addr1 & "|" & addr2 & "|" & addr3 & "|" & addr4 & "|" & addr5 & _
                        "|" & pcode & "|" & name2 & "|" & d_addr1 & "|" & d_addr2 & "|" & _
                        d_addr3 & "|" & d_addr4 & "|" & d_addr5 & "|" & d_pcode & "|" & amt & "|" & _
                        offence_val & "|" & home_phone & "|" & work_phone & "|" & mobile_phone & "|" & _
                        affinity_brand & "|" & product_type & "|" & seller_name & "|"
            If determination_date <> Nothing Then
                outline = outline & determination_date
            End If
            Dim comments2 As String = ""
            Dim comments3 As String = comments
            If comments.Length <= 250 Then
                outline = outline & "|" & comments
            Else
                While comments3.Length > 250
                    Dim len As Integer = Microsoft.VisualBasic.Len(comments3)
                    For idx2 = 250 To 1 Step -1
                        If Mid(comments3, idx2, 1) = ";" Then
                            Exit For
                        End If
                    Next
                    comments2 = comments2 & Microsoft.VisualBasic.Left(comments3, idx2)
                    Dim idx3 As Integer
                    For idx3 = idx2 To 250
                        comments2 = comments2 & " "
                    Next
                    comments3 = Microsoft.VisualBasic.Right(comments3, len - idx2)
                End While
                outline = outline & "|" & comments2 & comments3
            End If
            
            outfile = outfile & outline & vbNewLine
            tot_arrow += 1

            'clear fields
            forename = ""
            surname = ""
            addr1 = ""
            addr2 = ""
            addr3 = ""
            addr4 = ""
            addr5 = ""
            pcode = ""
            d_addr1 = ""
            d_addr2 = ""
            d_addr3 = ""
            d_addr4 = ""
            d_addr5 = ""
            d_pcode = ""
            offence_court = Nothing
            from_date = Nothing
            reg_date = Nothing
            dob = Nothing
            clref = ""
            amt = Nothing
            off_number = ""
            debtor2_address_found = False
            debtor2_address = ""
            home_phone = ""
            mobile_phone = ""
            work_phone = ""
            comments = ""
            affinity_brand = ""
            seller_name = ""
            product_type = ""
            determination_date = Nothing
        Next

        viewbtn.Enabled = True
        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_Carphone_warehouse_preprocess.txt", outfile, False)
        

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText("error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
            MsgBox("CarPhone Warehouse - " & tot_arrow & vbNewLine)
            Me.Close()
        End If
    End Sub
    Private Function remove_commas(ByVal addr_str As String) As String
        Dim idx As Integer
        Dim addr_str_out As String = ""
        For idx = 1 To addr_str.Length
            If Mid(addr_str, 1, idx) = "," Then
                addr_str_out = addr_str_out & " "
            Else
                addr_str_out = addr_str_out & Mid(addr_str, 1, idx)
            End If
        Next
        Return (addr_str_out)
    End Function
    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
End Class

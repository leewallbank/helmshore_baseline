<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Arrow_report_datesDataSet = New ArrowInventoryReport.Arrow_report_datesDataSet
        Me.Arrow_report_datesTableAdapter = New ArrowInventoryReport.Arrow_report_datesDataSetTableAdapters.Arrow_report_datesTableAdapter
        CType(Me.Arrow_report_datesDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Arrow_report_datesDataSet
        '
        Me.Arrow_report_datesDataSet.DataSetName = "Arrow_report_datesDataSet"
        Me.Arrow_report_datesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Arrow_report_datesTableAdapter
        '
        Me.Arrow_report_datesTableAdapter.ClearBeforeFill = True
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(332, 312)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Arrow Reports"
        CType(Me.Arrow_report_datesDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Arrow_report_datesDataSet As ArrowInventoryReport.Arrow_report_datesDataSet
    Friend WithEvents Arrow_report_datesTableAdapter As ArrowInventoryReport.Arrow_report_datesDataSetTableAdapters.Arrow_report_datesTableAdapter

End Class

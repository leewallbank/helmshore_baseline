Public Class mainfrm
    '020810 activitynextcontactdate and activitystatus removed from activity file
    '30112010 remove chr(13) from address lines and phone numbers
    '15.5.2013 add accounts management (clID 1765)
    Dim address, add_line1, add_line2, add_line3, add_line4, add_line5, add_pcode, saved_fname As String
    Dim csid, csid_idx As Integer
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim allreports As Boolean = False
    Dim csid_no_of_rows As Integer
    Dim csid_table(200) As Integer
    Dim log_file, log_name As String
    Dim file1_written As Boolean = False
    Dim file2_written As Boolean = False
    Dim file3_written As Boolean = False
    Dim file4_written As Boolean = False
    Dim file5_written As Boolean = False
    Dim file6_written As Boolean = False
    Dim file7_written As Boolean = False
    Dim file8_written As Boolean = False
    Dim file9_written As Boolean = False
    Dim file10_written As Boolean = False
    Dim file11_written As Boolean = False
    Dim file12_written As Boolean = False
    Dim file13_written As Boolean = False
    Dim file14_written As Boolean = False
    Dim file15_written As Boolean = False

    Dim inv_record As String
    Dim inv_file As String
    Dim fname As String
    Dim out_of_space As Boolean = False
    'Dim fname As String = "H:/temp/Rossendales_InventoryReport1_" & Format(Now, "ddMMyyyy") & ".txt"
    

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Function remove_chars(ByVal text As String) As String
        Dim idx As Integer
        Dim new_text As String = ""
        For idx = 1 To text.Length
            If Mid(text, idx, 1) = Chr(10) Or Mid(text, idx, 1) = Chr(9) Or _
            Mid(text, idx, 1) = Chr(13) Or Mid(text, idx, 1) = "|" Then
                new_text = new_text & " "
            Else
                new_text = new_text & Mid(text, idx, 1)
            End If
        Next
        Return new_text
    End Function




    Private Sub invbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Sub write_inventory_report()
        '1.12.14 ignore cases closed before 1.1.2014

        inv_file = "ServiceAccountReference|ArrowKey|DebtType|" & _
        "Title|FirstName|Surname|DateOfBirth|AddressLine1|AddressLine2|AddressLine3|" & _
        "AdddressLine4|AddressLine5|PostCode|HomePhoneNumber|WorkPhoneNumber|MobilePhoneNumber|" & _
        "FaxNumber|EmailAdddress|MaritalStatus|EmploymentStatus|DateOfDebtPlacement|" & _
        "OpeningBalance|AmountPaid|CurrentBalance|CommissionRate|CommissionAmount|DirectPayments|" & _
        "LastPaymentDate|LastPaymentAmount|LastContactDate|DateClosed|AccountStatus|ClosedStatus|" & _
        "WriteOffReason|WriteOffDate|WriteOffAmount|ArrangementInstalmentAmount|" & _
        "ArrangementFirstInstalmentDueDate|ArrangementInstalmentFrequency|" & _
        "ArrowGlobalLegalEntity|3PDMAllocationDate|3PDMName|3PDMContactDetails|CCARequestdate|" & _
        "CCARequestDetails|SARRequestDate|SARRequestDetails|RegulatoryComplaintsDate|" & _
        "RegulatoryComplaintDetails" & vbNewLine
        For csid_idx = 0 To csid_no_of_rows
            csid = csid_table(csid_idx)
            'get commission rate
            param1 = "onestep"
            param2 = "select fee_comm1 from ClientScheme where _rowid = " & csid
            Dim cs_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                log_file = "Clientscheme not found for csid = " & csid & vbNewLine
                My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                Exit Sub
            End If
            Dim comm_rate As Decimal = cs_dataset.Tables(0).Rows(0).Item(0)
            Dim return_start_date As Date = CDate("Jan 1, 2014")
            param2 = "select _rowid, client_ref, clientschemeID, name_title, name_fore, name_sur, dateOfBirth, address," & _
            " add_postcode, add_phone, add_fax, empPhone, addEmail, empName, _createdDate, debt_amount, " & _
            " debtPaid, debt_balance, status, status_open_closed, return_date, " & _
            " arrange_amount, arrange_started, arrange_interval, return_codeID" & _
            " from Debtor where clientschemeID = " & csid & _
            " and (status_open_closed = 'O' or (return_date is not null and return_date > '" & Format(return_start_date, "yyyy-MM-dd") & "'))"

            Dim last_payment_date, dob, created_date, return_date, last_contact, arr_started As Date

            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            Dim debtor, idx, arr_interval As Integer
            Dim cl_ref, sch_name, title, first_name, surname, home_phone, phone2, emp_phone, fax, email As String
            Dim emp_status, status, open_closed As String

            Dim last_payment_amt, opening_bal, amt_paid, cur_bal, dir_payments, arr_amt As Decimal
            Dim debtor_rows As Integer = no_of_rows
            For idx = 0 To debtor_rows - 1
                open_closed = debtor_dataset.Tables(0).Rows(idx).Item(19)

                Try
                    return_date = debtor_dataset.Tables(0).Rows(idx).Item(20)
                Catch ex As Exception
                    return_date = Nothing
                End Try
                'If open_closed = "C" And return_date <> Nothing Then
                '    If Format(return_date, "yyyy-MM-dd") < Format(return_start_date, "yyyy-MM-dd") Then
                '        Continue For
                '    End If
                'End If
                status = debtor_dataset.Tables(0).Rows(idx).Item(18)
                debtor = debtor_dataset.Tables(0).Rows(idx).Item(0)
                cl_ref = Trim(debtor_dataset.Tables(0).Rows(idx).Item(1))
                csid = debtor_dataset.Tables(0).Rows(idx).Item(2)
                param2 = "select schemeID from ClientScheme where _rowid = " & csid
                Dim csid_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    log_file = "error reading clientscheme for " & csid & vbNewLine
                    My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                    Exit Sub
                End If
                param2 = "select name from Scheme where _rowid = " & csid_dataset.Tables(0).Rows(0).Item(0)
                Dim sch_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    log_file = "error reading scheme for client scheme = " & csid * vbNewLine
                    My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                    Exit Sub
                End If
                sch_name = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                Try
                    title = Trim(debtor_dataset.Tables(0).Rows(idx).Item(3))
                Catch ex As Exception
                    title = Nothing
                End Try
                Try
                    first_name = Trim(debtor_dataset.Tables(0).Rows(idx).Item(4))
                Catch ex As Exception
                    first_name = Nothing
                End Try
                Try
                    surname = Trim(debtor_dataset.Tables(0).Rows(idx).Item(5))
                Catch ex As Exception
                    surname = ""
                End Try

                Try
                    dob = debtor_dataset.Tables(0).Rows(idx).Item(6)
                Catch ex As Exception
                    dob = Nothing
                End Try

                'get payments
                param2 = "select amount, date, amount_sourceID from Payment" & _
                " where debtorID = " & debtor & " and amount <> 0 and (status = 'W' or status = 'R')" & _
                " order by date desc"
                Dim payment_dataset As DataSet = get_dataset(param1, param2)
                dir_payments = 0
                If no_of_rows = 0 Then
                    last_payment_date = Nothing
                    last_payment_amt = Nothing
                Else
                    last_payment_date = payment_dataset.Tables(0).Rows(0).Item(1)
                    last_payment_amt = payment_dataset.Tables(0).Rows(0).Item(0)
                    'sum up direct payments
                    Dim idx2 As Integer
                    Dim pay_rows As Integer = no_of_rows
                    For idx2 = 0 To pay_rows - 1
                        Dim sourceid As Integer = payment_dataset.Tables(0).Rows(idx2).Item(2)
                        param2 = "select direct from PaySource where _rowid = " & sourceid
                        Dim paysource_dataset As DataSet = get_dataset(param1, param2)
                        If no_of_rows <> 1 Then
                            log_file = "Unable to read paysource = " & sourceid & vbNewLine
                            My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                            Exit Sub
                        End If
                        If paysource_dataset.Tables(0).Rows(0).Item(0) = "Y" Then
                            dir_payments += payment_dataset.Tables(0).Rows(idx2).Item(0)
                        End If
                    Next

                End If

                inv_record = inv_record & debtor & "|" & cl_ref & "|" & sch_name & "|" & _
                title & "|" & first_name & "|" & surname & "|"

                If dob = Nothing Then
                    inv_record = inv_record & ""
                Else
                    inv_record = inv_record & Format(dob, "dd/MM/yyyy")
                End If
                Try
                    address = Trim(debtor_dataset.Tables(0).Rows(idx).Item(7))
                Catch ex As Exception
                    address = ""
                End Try
                Try
                    add_pcode = Trim(debtor_dataset.Tables(0).Rows(idx).Item(8))
                Catch ex As Exception
                    add_pcode = ""
                End Try

                get_address_lines()
                Try
                    home_phone = Trim(debtor_dataset.Tables(0).Rows(idx).Item(9))
                    home_phone = remove_cr(home_phone)
                Catch ex As Exception
                    home_phone = ""
                End Try
                Try
                    phone2 = Trim(debtor_dataset.Tables(0).Rows(idx).Item(10))
                    phone2 = remove_cr(phone2)
                Catch ex As Exception
                    phone2 = ""
                End Try
                Try
                    emp_phone = Trim(debtor_dataset.Tables(0).Rows(idx).Item(11))
                    emp_phone = remove_cr(emp_phone)
                Catch ex As Exception
                    emp_phone = ""
                End Try
                fax = ""
                Try
                    email = Trim(debtor_dataset.Tables(0).Rows(idx).Item(12))
                    email = remove_cr(email)
                Catch ex As Exception
                    email = ""
                End Try
                emp_status = ""
                Try
                    Dim emp_name As String = Trim(debtor_dataset.Tables(0).Rows(idx).Item(13))
                    If emp_name.Length > 0 Then
                        emp_status = "Employed"
                    End If
                Catch ex As Exception

                End Try
                If emp_phone <> "" Then
                    emp_status = "Employed"
                End If

                created_date = debtor_dataset.Tables(0).Rows(idx).Item(14)
                opening_bal = debtor_dataset.Tables(0).Rows(idx).Item(15)
                amt_paid = debtor_dataset.Tables(0).Rows(idx).Item(16)
                cur_bal = opening_bal - amt_paid


                Dim comm_amt As Decimal = (amt_paid * comm_rate) / 100


                If open_closed = "O" And status <> "C" Then
                    return_date = Nothing
                End If
                'get last letter date
                param2 = "select max(_createdDate) from Note where type= 'Letter' and debtorID = " & debtor
                Dim note_dataset As DataSet = get_dataset(param1, param2)
                Try
                    last_contact = note_dataset.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    last_contact = Nothing
                End Try
                If last_contact = Nothing Then
                    'get last letter stage
                    param2 = "select _createdDate, text from Note where type = 'Stage' and debtorID = " & debtor & _
                    " order by _rowid desc"
                    Dim note2_dataset As DataSet = get_dataset(param1, param2)
                    If no_of_rows = 0 Then
                        last_contact = Nothing
                    Else
                        Dim idx2 As Integer
                        Dim note_rows As Integer = no_of_rows - 1
                        For idx2 = 0 To note_rows
                            If InStr(note2_dataset.Tables(0).Rows(idx2).Item(1), "Letter") > 0 Then
                                last_contact = note2_dataset.Tables(0).Rows(idx2).Item(0)
                                Exit For
                            End If
                        Next
                    End If
                End If
                Dim arr_frequency As String = ""
                If open_closed = "O" And status = "A" Then
                    arr_amt = debtor_dataset.Tables(0).Rows(idx).Item(21)
                    Try
                        arr_started = debtor_dataset.Tables(0).Rows(idx).Item(22)
                        arr_interval = debtor_dataset.Tables(0).Rows(idx).Item(23)
                    Catch ex As Exception
                        arr_started = Nothing
                        arr_interval = 0
                    End Try
                    Select Case arr_interval
                        Case 0
                            arr_frequency = ""
                        Case 7
                            arr_frequency = "Weekly"
                        Case 14
                            arr_frequency = "Fortnightly"
                        Case 28 To 31
                            arr_frequency = "Monthly"
                        Case 90
                            arr_frequency = "Quarterly"
                        Case Else
                            arr_frequency = "Every " & arr_interval & " Days"
                    End Select
                End If

                inv_record = inv_record & "|" & add_line1 & "|" & add_line2 & "|" & add_line3 & "|" & add_line4 & _
                "|" & add_line5 & "|" & add_pcode & "|" & home_phone & "|" & emp_phone & "|" & phone2 & "|" & fax & "|" & _
                email & "|" & "" & "|" & emp_status & "|" & Format(created_date, "dd/MM/yyyy") & "|" & _
                Format(opening_bal, "0.00") & "|" & Format(amt_paid, "F") & "|" & Format(cur_bal, "0.00") & "|" & _
                Format(comm_rate, "0.00") & "|" & Format(comm_amt, "0.00") & "|" & Format(dir_payments, "0.00") & "|"
                If last_payment_date = Nothing Then
                    inv_record = inv_record & ""
                Else
                    inv_record = inv_record & Format(last_payment_date, "dd/MM/yyyy")
                End If

                inv_record = inv_record & "|" & Format(last_payment_amt, "0.00") & "|"
                If last_contact = Nothing Then
                    inv_record = inv_record & ""
                Else
                    inv_record = inv_record & Format(last_contact, "dd/MM/yyyy")
                End If

                If return_date = Nothing Then
                    inv_record = inv_record & "|" & ""
                Else
                    inv_record = inv_record & "|" & Format(return_date, "dd/MM/yyyy")
                End If
                If open_closed = "O" Then
                    inv_record = inv_record & "|" & "Open"
                Else
                    inv_record = inv_record & "|" & "Closed"
                End If
                Dim status_text As String = ""
                Select Case status
                    Case "S"
                        If amt_paid > 0 Then
                            status_text = "PaidinFull"
                        Else
                            status_text = "Successful"
                        End If
                    Case "C"
                        status_text = "Cancelled"
                    Case "F"
                        status_text = "FullyPaid"
                    Case "A"
                        status_text = "Arrangement"
                    Case "T"
                        status_text = "Trace"
                    Case "L"
                        status_text = "Live"
                    Case "X"
                        status_text = "Expired"
                    Case Else
                        status_text = "Unknown"
                End Select
                inv_record = inv_record & "|" & status_text & "|"

                'write off amount is balance left
                Dim wo_date As Date = Nothing
                Dim wo_amt As Decimal
                Dim wo_reason As String = Nothing

                wo_date = Nothing
                wo_amt = debtor_dataset.Tables(0).Rows(idx).Item(17)
                If wo_amt > 0 And open_closed = "C" Then
                    wo_date = return_date
                    Dim retn_code As Integer
                    Try
                        retn_code = debtor_dataset.Tables(0).Rows(idx).Item(24)
                    Catch ex As Exception
                        retn_code = 0
                    End Try
                    If retn_code > 0 Then
                        'get return reason from codereturns table
                        param2 = "select reason_short from CodeReturns where _rowid = " & retn_code
                        Dim retn_dataset As DataSet = get_dataset(param1, param2)
                        If no_of_rows = 0 Then
                            log_file = "Can't find return reason for return code = " & retn_code & vbNewLine
                            My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                            Exit Sub
                        End If
                        wo_reason = retn_dataset.Tables(0).Rows(0).Item(0)
                    End If
                End If

                If Microsoft.VisualBasic.Left(wo_reason, 6) = "Settle" And wo_amt > 0 And open_closed = "C" Then
                    inv_record = inv_record & Trim(wo_reason) & "|"
                    If wo_date = Nothing Then
                        inv_record = inv_record & ""
                    Else
                        inv_record = inv_record & Format(wo_date, "dd/MM/yyyy")

                    End If
                    inv_record = inv_record & "|" & Format(wo_amt, "0.00") & "|"
                Else
                    inv_record = inv_record & "" & "|" & "" & "|" & "" & "|"
                End If

                If open_closed = "O" And status = "A" Then
                    inv_record = inv_record & Format(arr_amt, "0.00") & "|"
                    If arr_started = Nothing Then
                        inv_record = inv_record & "" & "|"
                    Else
                        inv_record = inv_record & Format(arr_started, "dd/MM/yyyy") & "|"
                    End If
                    inv_record &= arr_frequency & "|"
                Else
                    inv_record = inv_record & "" & "|" & "" & "|" & "" & "|"
                End If


                'get buyername from notes
                param2 = "select text from Note where (type = 'Note' or type = 'Client note') " & _
                " and debtorID = " & debtor & " order by _rowid desc"
                Dim note3_dataset As DataSet = get_dataset(param1, param2)
                Dim idx3 As Integer
                Dim note3_rows As Integer = no_of_rows - 1
                Dim text As String
                Dim buyer_name As String = ""
                For idx3 = 0 To note3_rows
                    text = note3_dataset.Tables(0).Rows(idx3).Item(0)
                    Dim start_idx As Integer = InStr(text, "BuyerName:")
                    If start_idx > 0 Then
                        Dim idx4 As Integer
                        For idx4 = start_idx + 10 To text.Length
                            If Mid(text, idx4, 1) = ";" Then
                                Exit For
                            End If
                        Next
                        buyer_name = Mid(text, start_idx + 10, idx4 - start_idx - 10)
                        Exit For
                    End If
                Next
                inv_record = inv_record & buyer_name & "|||||||||" & vbNewLine
                Try
                    inv_file = inv_file & inv_record
                Catch ex As Exception
                    save_file()
                    inv_file = "ServiceAccountReference|ArrowKey|DebtType|" & _
                            "Title|FirstName|Surname|DateOfBirth|AddressLine1|AddressLine2|AddressLine3|" & _
                            "AdddressLine4|AddressLine5|PostCode|HomePhoneNumber|WorkPhoneNumber|MobilePhoneNumber|" & _
                            "FaxNumber|EmailAdddress|MaritalStatus|EmploymentStatus|DateOfDebtPlacement|" & _
                            "OpeningBalance|AmountPaid|CurrentBalance|CommissionRate|CommissionAmount|DirectPayments|" & _
                            "LastPaymentDate|LastPaymentAmount|LastContactDate|DateClosed|AccountStatus|ClosedStatus|" & _
                            "WriteOffReason|WriteOffDate|WriteOffAmount|ArrangementInstalmentAmount|" & _
                            "ArrangementFirstInstalmentDueDate|ArrangementInstalmentFrequency|" & _
                            "ArrowGlobalLegalEntity|3PDMAllocationDate|3PDMName|3PDMContactDetails|CCARequestdate|" & _
                            "CCARequestDetails|SARRequestDate|SARRequestDetails|RegulatoryComplaintsDate|" & _
                            "RegulatoryComplaintDetails" & vbNewLine
                    inv_file = inv_file & inv_record
                End Try
                inv_record = ""

            Next
        Next
        If Not out_of_space Then save_file()


        Me.Close()
    End Sub
    Sub save_file()
        If file1_written = False Then
            file1_written = True
            Try
                fname = "R:/Arrow Reports/Rossendales_InventoryReport1_" & Format(Now, "ddMMyyyy") & ".txt"
                My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
            Catch
                log_file = "Can't save file1" & vbNewLine
                My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
            End Try
        Else
            If file2_written = False Then
                file2_written = True
                fname = "R:/Arrow Reports/Rossendales_InventoryReport2_" & Format(Now, "ddMMyyyy") & ".txt"
                Try
                    My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                Catch
                    log_file = "Can't save file2" & vbNewLine
                    My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                End Try
            Else
                If file3_written = False Then
                    file3_written = True
                    fname = "R:/Arrow Reports/Rossendales_InventoryReport3_" & Format(Now, "ddMMyyyy") & ".txt"
                    Try
                        My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                    Catch
                        log_file = "Can't save file3" & vbNewLine
                        My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                    End Try
                Else
                    If file4_written = False Then
                        file4_written = True
                        fname = "R:/Arrow Reports/Rossendales_InventoryReport4_" & Format(Now, "ddMMyyyy") & ".txt"
                        Try
                            My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                        Catch
                            log_file = "Can't save file4" & vbNewLine
                            My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                        End Try
                    Else
                        If file5_written = False Then
                            file5_written = True
                            fname = "R:/Arrow Reports/Rossendales_InventoryReport5_" & Format(Now, "ddMMyyyy") & ".txt"
                            Try
                                My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                            Catch
                                log_file = "Can't save file5" & vbNewLine
                                My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                            End Try
                        Else
                            If file6_written = False Then
                                file6_written = True
                                fname = "R:/Arrow Reports/Rossendales_InventoryReport6_" & Format(Now, "ddMMyyyy") & ".txt"
                                Try
                                    My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                                Catch
                                    log_file = "Can't save file6" & vbNewLine
                                    My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                End Try
                            Else
                                If file7_written = False Then
                                    file7_written = True
                                    fname = "R:/Arrow Reports/Rossendales_InventoryReport7_" & Format(Now, "ddMMyyyy") & ".txt"
                                    Try
                                        My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                                    Catch
                                        log_file = "Can't save file7" & vbNewLine
                                        My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                    End Try
                                Else
                                    If file8_written = False Then
                                        file8_written = True
                                        fname = "R:/Arrow Reports/Rossendales_InventoryReport8_" & Format(Now, "ddMMyyyy") & ".txt"
                                        Try
                                            My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                                        Catch
                                            log_file = "Can't save file8" & vbNewLine
                                            My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                        End Try
                                    Else
                                        If file9_written = False Then
                                            file9_written = True
                                            fname = "R:/Arrow Reports/Rossendales_InventoryReport9_" & Format(Now, "ddMMyyyy") & ".txt"
                                            Try
                                                My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                                            Catch
                                                log_file = "Can't save file9" & vbNewLine
                                                My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                            End Try
                                        Else
                                            If file10_written = False Then
                                                file10_written = True
                                                fname = "R:/Arrow Reports/Rossendales_InventoryReport10_" & Format(Now, "ddMMyyyy") & ".txt"
                                                Try
                                                    My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                                                Catch
                                                    log_file = "Can't save file10" & vbNewLine
                                                    My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                End Try
                                            Else
                                                If file11_written = False Then
                                                    file11_written = True
                                                    fname = "R:/Arrow Reports/Rossendales_InventoryReport11_" & Format(Now, "ddMMyyyy") & ".txt"
                                                    Try
                                                        My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                                                    Catch
                                                        log_file = "Can't save file11" & vbNewLine
                                                        My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                    End Try
                                                Else
                                                    If file12_written = False Then
                                                        file12_written = True
                                                        fname = "R:/Arrow Reports/Rossendales_InventoryReport12_" & Format(Now, "ddMMyyyy") & ".txt"
                                                        Try
                                                            My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                                                        Catch
                                                            log_file = "Can't save file12" & vbNewLine
                                                            My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                        End Try
                                                    Else
                                                        If file13_written = False Then
                                                            file13_written = True
                                                            fname = "R:/Arrow Reports/Rossendales_InventoryReport13_" & Format(Now, "ddMMyyyy") & ".txt"
                                                            Try
                                                                My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                                                            Catch
                                                                log_file = "Can't save file13" & vbNewLine
                                                                My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                            End Try
                                                        Else
                                                            If file14_written = False Then
                                                                file14_written = True
                                                                fname = "R:/Arrow Reports/Rossendales_InventoryReport14_" & Format(Now, "ddMMyyyy") & ".txt"
                                                                Try
                                                                    My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                                                                Catch
                                                                    log_file = "Can't save file14" & vbNewLine
                                                                    My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                                End Try
                                                            Else
                                                                If file15_written = False Then
                                                                    file15_written = True
                                                                    fname = "R:/Arrow Reports/Rossendales_InventoryReport15_" & Format(Now, "ddMMyyyy") & ".txt"
                                                                    Try
                                                                        My.Computer.FileSystem.WriteAllText(fname, inv_file, False, ascii)
                                                                    Catch
                                                                        log_file = "Can't save file15" & vbNewLine
                                                                        My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                                    End Try
                                                                Else
                                                                    out_of_space = True
                                                                    log_file = "More files required - contact IT" & vbNewLine
                                                                    My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
                                                                End If
                                                            End If
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub
    Sub get_address_lines()
        Dim idx, add_idx As Integer
        add_line1 = ""
        add_line2 = ""
        add_line3 = ""
        add_line4 = ""
        add_line5 = ""
        add_idx = 1
        For idx = 1 To address.Length
            If Mid(address, idx, 1) = Chr(9) Or Mid(address, idx, 1) = Chr(10) Then
                add_idx += 1
            ElseIf Mid(address, idx, 1) <> Chr(13) Then
                Select Case add_idx
                    Case 1
                        add_line1 = add_line1 & Mid(address, idx, 1)
                    Case 2
                        add_line2 = add_line2 & Mid(address, idx, 1)
                    Case 3
                        add_line3 = add_line3 & Mid(address, idx, 1)
                    Case 4
                        add_line4 = add_line4 & Mid(address, idx, 1)
                    Case 5
                        add_line5 = add_line5 & Mid(address, idx, 1)
                End Select
            End If
        Next
        If add_line2 = add_pcode Then
            add_line2 = ""
        End If
        If add_line3 = add_pcode Then
            add_line3 = ""
        End If
        If add_line4 = add_pcode Then
            add_line4 = ""
        End If
        If add_line5 = add_pcode Then
            add_line5 = ""
        End If
    End Sub
    Function remove_cr(ByVal chars As String) As String
        Dim new_chars As String = ""
        Dim idx As Integer
        For idx = 1 To chars.Length
            If Mid(chars, idx, 1) <> Chr(10) And Mid(chars, idx, 1) <> Chr(13) Then
                new_chars = new_chars & Mid(chars, idx, 1)
            End If
        Next
        Return new_chars
    End Function

    Private Sub mainfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        os_con.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'get a list of client scheme ids
        '09.05.2011 add guernsey
        '26.09.2011 and arrowRML
        '20.5.2013 add more output files
        '9.12.2014 add erudio cl=1874
        log_file = "Started Inventory report at " & Format(Now, "yyyy-MM-dd HH:mm:ss") & vbNewLine
        log_name = "R:/Arrow Reports/InventoryReport_log_" & Format(Now, "ddMMyyyy") & ".txt"
        'log_name = "H:/temp/InventoryReport_log_" & Format(Now, "ddMMyyyy") & ".txt"
        My.Computer.FileSystem.WriteAllText(log_name, log_file, False)
        param1 = "onestep"
        param2 = "select _rowid from ClientScheme where clientID = 986 or clientID = 1160 or clientID = 1301 or clientID = 1765 or clientID = 1874"
        Dim csid_dataset As DataSet = get_dataset(param1, param2)
        csid_no_of_rows = no_of_rows - 1
        Dim idx As Integer
        For idx = 0 To csid_no_of_rows
            csid_table(idx) = csid_dataset.Tables(0).Rows(idx).Item(0)
        Next
        write_inventory_report()
        If Not out_of_space Then
            log_file = "Finished Inventory report at " & Format(Now, "yyyy-MM-dd HH:mm:ss") & vbNewLine
            My.Computer.FileSystem.WriteAllText(log_name, log_file, True)
        End If
        
        Me.Close()
    End Sub

End Class

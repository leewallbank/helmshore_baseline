Public Class corr_actionsfrm

    Private Sub corr_actionsfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.Corrective_actionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Corrective_actions)
        param2 = "select cor_code, cor_name from [Corrective actions] order by cor_code"
        Dim corr_ds As DataSet = get_dataset("Complaints", param2)
        corr_dg.Rows.Clear()
        Dim corr_rows As Integer = no_of_rows - 1
        Dim corr_idx As Integer
        For corr_idx = 0 To corr_rows
            corr_dg.Rows.Add(corr_ds.Tables(0).Rows(corr_idx).Item(0), corr_ds.Tables(0).Rows(corr_idx).Item(1))
        Next
        'Me.Receipt_typeTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Receipt_type)
        corr_action_orig_no_rows = corr_dg.RowCount
        'corr_action_orig_no_rows = DataGridView1.RowCount
    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles corr_dg.CellValueChanged
        If e.ColumnIndex = 1 And e.RowIndex >= 0 Then
            corr_action_code = corr_dg.Rows(e.RowIndex).Cells(0).Value
            If corr_action_code = 0 Then Return
            corr_action_name = corr_dg.Rows(e.RowIndex).Cells(1).Value
            'If corr_dg.Rows(e.RowIndex).IsNewRow Then Return
            Try
                'Me.Corrective_actionsTableAdapter.UpdateQuery(corr_action_name, corr_action_code)
                upd_txt = "update [Corrective actions] set cor_name = '" & corr_action_name & "'" & _
                                " where cor_code = " & corr_action_code
                update_sql(upd_txt)
                log_text = "Corrective action amended - " & corr_action_code & _
                                                  " from " & orig_text & " to " & corr_action_name
                log_type = "Master"
                add_log(log_type, log_text)
                'updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate Corrective action entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles corr_dg.RowEnter

        corr_action_code = corr_dg.Rows(e.RowIndex).Cells(0).Value
        orig_text = corr_dg.Rows(e.RowIndex).Cells(1).Value

    End Sub

    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles corr_dg.RowsRemoved
        If e.RowIndex > 0 Then
            Try
                Try
                    Me.Corrective_actionsTableAdapter.DeleteQuery(corr_action_code)
                Catch
                    MessageBox.Show("Can't delete as it's used in a complaint")
                End Try
                log_text = "Corrective action deleted - " & corr_action_code & " name " & orig_text
                updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
End Class
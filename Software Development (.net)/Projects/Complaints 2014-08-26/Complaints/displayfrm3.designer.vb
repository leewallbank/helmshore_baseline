<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class displayfrm3
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim Comp_noLabel As System.Windows.Forms.Label
        Dim Comp_dateLabel As System.Windows.Forms.Label
        Dim Comp_case_noLabel As System.Windows.Forms.Label
        Dim Comp_cat_codeLabel As System.Windows.Forms.Label
        Dim Comp_cat_numberLabel As System.Windows.Forms.Label
        Dim Comp_textLabel As System.Windows.Forms.Label
        Dim Recvd_textLabel As System.Windows.Forms.Label
        Dim Recpt_textLabel As System.Windows.Forms.Label
        Dim Label1 As System.Windows.Forms.Label
        Dim Against_textLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Inv_textLabel As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Comp_old_comp_noLabel As System.Windows.Forms.Label
        Dim Label12 As System.Windows.Forms.Label
        Dim Label13 As System.Windows.Forms.Label
        Dim Label28 As System.Windows.Forms.Label
        Dim Label29 As System.Windows.Forms.Label
        Dim Label30 As System.Windows.Forms.Label
        Me.Comp_noTextBox = New System.Windows.Forms.TextBox
        Me.Comp_case_noTextBox = New System.Windows.Forms.TextBox
        Me.Comp_cat_codeTextBox = New System.Windows.Forms.TextBox
        Me.Comp_textTextBox = New System.Windows.Forms.TextBox
        Me.Recvd_textTextBox = New System.Windows.Forms.TextBox
        Me.Recpt_textTextBox = New System.Windows.Forms.TextBox
        Me.exitbtn = New System.Windows.Forms.Button
        Me.cltextbox = New System.Windows.Forms.TextBox
        Me.Against_textTextBox = New System.Windows.Forms.TextBox
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.agent_TextBox = New System.Windows.Forms.TextBox
        Me.Inv_textTextBox = New System.Windows.Forms.TextBox
        Me.Comp_responseTextBox = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.comprbtn = New System.Windows.Forms.RadioButton
        Me.notcomprbtn = New System.Windows.Forms.RadioButton
        Me.compbyTextBox = New System.Windows.Forms.TextBox
        Me.completed_datetextbox = New System.Windows.Forms.TextBox
        Me.openrbtn = New System.Windows.Forms.RadioButton
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.foundedrbtn = New System.Windows.Forms.RadioButton
        Me.unfoundedrbtn = New System.Windows.Forms.RadioButton
        Me.completedbylbl = New System.Windows.Forms.Label
        Me.completeddatelbl = New System.Windows.Forms.Label
        Me.dispbtn = New System.Windows.Forms.Button
        Me.Action_nameTextBox = New System.Windows.Forms.TextBox
        Me.updbtn = New System.Windows.Forms.Button
        Me.days_TextBox = New System.Windows.Forms.TextBox
        Me.days_Label = New System.Windows.Forms.Label
        Me.doc_textbox = New System.Windows.Forms.TextBox
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.ack_letter_sent = New System.Windows.Forms.TextBox
        Me.hold_letter_sent = New System.Windows.Forms.TextBox
        Me.doc_ListBox = New System.Windows.Forms.ListBox
        Me.cat_numbertextbox = New System.Windows.Forms.TextBox
        Me.Cor_nameTextBox = New System.Windows.Forms.TextBox
        Me.Hold_name_TextBox = New System.Windows.Forms.TextBox
        Me.stage_TextBox = New System.Windows.Forms.TextBox
        Me.entered_bytextbox = New System.Windows.Forms.TextBox
        Me.prioritytbox = New System.Windows.Forms.TextBox
        Me.recvd_datetextbox = New System.Windows.Forms.TextBox
        Me.old_comp_noTextBox = New System.Windows.Forms.TextBox
        Me.ComplaintsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.costs_cancel_tbox = New System.Windows.Forms.TextBox
        Me.compensation_tbox = New System.Windows.Forms.TextBox
        Me.stage2_gbox = New System.Windows.Forms.GroupBox
        Me.stage2_completed_by_textbox = New System.Windows.Forms.TextBox
        Me.stage2_completed_datepicker = New System.Windows.Forms.DateTimePicker
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.stage2_start_datepicker = New System.Windows.Forms.DateTimePicker
        Me.Label16 = New System.Windows.Forms.Label
        Me.gender_textbox = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.dob_textbox = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.eth_textbox = New System.Windows.Forms.TextBox
        Me.debt_typelbl = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.stage2_ack_letter_sent = New System.Windows.Forms.TextBox
        Me.stage2_hold_letter_sent = New System.Windows.Forms.TextBox
        Me.stage3_gbox = New System.Windows.Forms.GroupBox
        Me.stage3_completed_by_textbox = New System.Windows.Forms.TextBox
        Me.stage3_completed_datepicker = New System.Windows.Forms.DateTimePicker
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.stage3_start_datepicker = New System.Windows.Forms.DateTimePicker
        Me.Label24 = New System.Windows.Forms.Label
        Me.agent2_TextBox = New System.Windows.Forms.TextBox
        Me.Against2_textTextBox = New System.Windows.Forms.TextBox
        Me.cancel_reasonbtn = New System.Windows.Forms.Button
        Me.stage_tabs = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.stage1_esc_tbox = New System.Windows.Forms.TextBox
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.stage2_esc_tbox = New System.Windows.Forms.TextBox
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.TabPage4 = New System.Windows.Forms.TabPage
        Me.GroupBox9 = New System.Windows.Forms.GroupBox
        Me.legal_rbtn = New System.Windows.Forms.RadioButton
        Me.Label25 = New System.Windows.Forms.Label
        Me.solicitor_datepicker = New System.Windows.Forms.DateTimePicker
        Me.TabPage5 = New System.Windows.Forms.TabPage
        Me.pi_cbox = New System.Windows.Forms.CheckBox
        Me.pl_cbox = New System.Windows.Forms.CheckBox
        Me.GroupBox8 = New System.Windows.Forms.GroupBox
        Me.liability_rbtn = New System.Windows.Forms.RadioButton
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.ins_rbtn = New System.Windows.Forms.RadioButton
        Me.Label27 = New System.Windows.Forms.Label
        Me.act_not_tbox = New System.Windows.Forms.TextBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.ins_datepicker = New System.Windows.Forms.DateTimePicker
        Me.TabPage6 = New System.Windows.Forms.TabPage
        Me.monetary_risktbox = New System.Windows.Forms.TextBox
        Me.category_tbox = New System.Windows.Forms.TextBox
        Me.clref_lbl = New System.Windows.Forms.Label
        Me.feedback_tbox = New System.Windows.Forms.TextBox
        Me.type_code_tbox = New System.Windows.Forms.TextBox
        Me.ComplaintsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ComplaintsTableAdapter
        Me.Receipt_typeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Receipt_typeTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Receipt_typeTableAdapter
        Me.Received_fromBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Received_fromTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
        Me.Complaint_againstBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Complaint_againstTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
        Me.InvestigatorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InvestigatorsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
        Me.ActionsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ActionsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ActionsTableAdapter
        Me.Corrective_actionsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Corrective_actionsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Corrective_actionsTableAdapter
        Me.Hold_reasonBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Hold_reasonTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Hold_reasonTableAdapter
        Me.branch_tbox = New System.Windows.Forms.TextBox
        Me.Label31 = New System.Windows.Forms.Label
        Me.hold_lbl = New System.Windows.Forms.Label
        Me.cmpny_lbl = New System.Windows.Forms.Label
        Comp_noLabel = New System.Windows.Forms.Label
        Comp_dateLabel = New System.Windows.Forms.Label
        Comp_case_noLabel = New System.Windows.Forms.Label
        Comp_cat_codeLabel = New System.Windows.Forms.Label
        Comp_cat_numberLabel = New System.Windows.Forms.Label
        Comp_textLabel = New System.Windows.Forms.Label
        Recvd_textLabel = New System.Windows.Forms.Label
        Recpt_textLabel = New System.Windows.Forms.Label
        Label1 = New System.Windows.Forms.Label
        Against_textLabel = New System.Windows.Forms.Label
        Label2 = New System.Windows.Forms.Label
        Inv_textLabel = New System.Windows.Forms.Label
        Label3 = New System.Windows.Forms.Label
        Label4 = New System.Windows.Forms.Label
        Label5 = New System.Windows.Forms.Label
        Label8 = New System.Windows.Forms.Label
        Label9 = New System.Windows.Forms.Label
        Label10 = New System.Windows.Forms.Label
        Label11 = New System.Windows.Forms.Label
        Comp_old_comp_noLabel = New System.Windows.Forms.Label
        Label12 = New System.Windows.Forms.Label
        Label13 = New System.Windows.Forms.Label
        Label28 = New System.Windows.Forms.Label
        Label29 = New System.Windows.Forms.Label
        Label30 = New System.Windows.Forms.Label
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.stage2_gbox.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.stage3_gbox.SuspendLayout()
        Me.stage_tabs.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        CType(Me.Receipt_typeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Received_fromBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Complaint_againstBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Corrective_actionsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Hold_reasonBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Comp_noLabel
        '
        Comp_noLabel.AutoSize = True
        Comp_noLabel.Location = New System.Drawing.Point(29, 9)
        Comp_noLabel.Name = "Comp_noLabel"
        Comp_noLabel.Size = New System.Drawing.Size(51, 13)
        Comp_noLabel.TabIndex = 1
        Comp_noLabel.Text = "comp no:"
        '
        'Comp_dateLabel
        '
        Comp_dateLabel.AutoSize = True
        Comp_dateLabel.Location = New System.Drawing.Point(131, 9)
        Comp_dateLabel.Name = "Comp_dateLabel"
        Comp_dateLabel.Size = New System.Drawing.Size(77, 13)
        Comp_dateLabel.TabIndex = 3
        Comp_dateLabel.Text = "Date received:"
        '
        'Comp_case_noLabel
        '
        Comp_case_noLabel.AutoSize = True
        Comp_case_noLabel.Location = New System.Drawing.Point(218, 92)
        Comp_case_noLabel.Name = "Comp_case_noLabel"
        Comp_case_noLabel.Size = New System.Drawing.Size(49, 13)
        Comp_case_noLabel.TabIndex = 4
        Comp_case_noLabel.Text = "Case no:"
        '
        'Comp_cat_codeLabel
        '
        Comp_cat_codeLabel.AutoSize = True
        Comp_cat_codeLabel.Location = New System.Drawing.Point(28, 173)
        Comp_cat_codeLabel.Name = "Comp_cat_codeLabel"
        Comp_cat_codeLabel.Size = New System.Drawing.Size(52, 13)
        Comp_cat_codeLabel.TabIndex = 9
        Comp_cat_codeLabel.Text = "Category:"
        '
        'Comp_cat_numberLabel
        '
        Comp_cat_numberLabel.AutoSize = True
        Comp_cat_numberLabel.Location = New System.Drawing.Point(86, 173)
        Comp_cat_numberLabel.Name = "Comp_cat_numberLabel"
        Comp_cat_numberLabel.Size = New System.Drawing.Size(35, 13)
        Comp_cat_numberLabel.TabIndex = 11
        Comp_cat_numberLabel.Text = "Code:"
        '
        'Comp_textLabel
        '
        Comp_textLabel.AutoSize = True
        Comp_textLabel.Location = New System.Drawing.Point(86, 223)
        Comp_textLabel.Name = "Comp_textLabel"
        Comp_textLabel.Size = New System.Drawing.Size(102, 13)
        Comp_textLabel.TabIndex = 13
        Comp_textLabel.Text = "Details of complaint:"
        '
        'Recvd_textLabel
        '
        Recvd_textLabel.AutoSize = True
        Recvd_textLabel.Location = New System.Drawing.Point(481, 9)
        Recvd_textLabel.Name = "Recvd_textLabel"
        Recvd_textLabel.Size = New System.Drawing.Size(68, 13)
        Recvd_textLabel.TabIndex = 14
        Recvd_textLabel.Text = "Complainant:"
        '
        'Recpt_textLabel
        '
        Recpt_textLabel.AutoSize = True
        Recpt_textLabel.Location = New System.Drawing.Point(357, 9)
        Recpt_textLabel.Name = "Recpt_textLabel"
        Recpt_textLabel.Size = New System.Drawing.Size(80, 13)
        Recpt_textLabel.TabIndex = 16
        Recpt_textLabel.Text = "Form of receipt:"
        '
        'Label1
        '
        Label1.AutoSize = True
        Label1.Location = New System.Drawing.Point(409, 92)
        Label1.Name = "Label1"
        Label1.Size = New System.Drawing.Size(36, 13)
        Label1.TabIndex = 20
        Label1.Text = "Client:"
        '
        'Against_textLabel
        '
        Against_textLabel.AutoSize = True
        Against_textLabel.Location = New System.Drawing.Point(557, 92)
        Against_textLabel.Name = "Against_textLabel"
        Against_textLabel.Size = New System.Drawing.Size(122, 13)
        Against_textLabel.TabIndex = 20
        Against_textLabel.Text = "Complaint made against:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Location = New System.Drawing.Point(728, 92)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(38, 13)
        Label2.TabIndex = 23
        Label2.Text = "Agent:"
        '
        'Inv_textLabel
        '
        Inv_textLabel.AutoSize = True
        Inv_textLabel.Location = New System.Drawing.Point(836, 92)
        Inv_textLabel.Name = "Inv_textLabel"
        Inv_textLabel.Size = New System.Drawing.Size(95, 13)
        Inv_textLabel.TabIndex = 23
        Inv_textLabel.Text = "Complaints Officer:"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Location = New System.Drawing.Point(447, 223)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(102, 13)
        Label3.TabIndex = 26
        Label3.Text = "Complaint response:"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Location = New System.Drawing.Point(438, 551)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(94, 13)
        Label4.TabIndex = 42
        Label4.Text = "Action to Resolve:"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Location = New System.Drawing.Point(22, 418)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(93, 13)
        Label5.TabIndex = 47
        Label5.Text = "No of Documents:"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Location = New System.Drawing.Point(557, 551)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(179, 13)
        Label8.TabIndex = 55
        Label8.Text = "Recommendation/Corrective Action:"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Location = New System.Drawing.Point(632, 332)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(47, 13)
        Label9.TabIndex = 57
        Label9.Text = "Reason:"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Location = New System.Drawing.Point(326, 341)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(0, 13)
        Label10.TabIndex = 59
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Location = New System.Drawing.Point(233, 9)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(61, 13)
        Label11.TabIndex = 61
        Label11.Text = "Entered by:"
        '
        'Comp_old_comp_noLabel
        '
        Comp_old_comp_noLabel.AutoSize = True
        Comp_old_comp_noLabel.Location = New System.Drawing.Point(696, 242)
        Comp_old_comp_noLabel.Name = "Comp_old_comp_noLabel"
        Comp_old_comp_noLabel.Size = New System.Drawing.Size(70, 13)
        Comp_old_comp_noLabel.TabIndex = 63
        Comp_old_comp_noLabel.Text = "Old comp no:"
        '
        'Label12
        '
        Label12.AutoSize = True
        Label12.Location = New System.Drawing.Point(478, 590)
        Label12.Name = "Label12"
        Label12.Size = New System.Drawing.Size(85, 13)
        Label12.TabIndex = 66
        Label12.Text = "Costs cancelled:"
        '
        'Label13
        '
        Label13.AutoSize = True
        Label13.Location = New System.Drawing.Point(632, 595)
        Label13.Name = "Label13"
        Label13.Size = New System.Drawing.Size(77, 13)
        Label13.TabIndex = 68
        Label13.Text = "Compensation:"
        '
        'Label28
        '
        Label28.AutoSize = True
        Label28.Location = New System.Drawing.Point(858, 173)
        Label28.Name = "Label28"
        Label28.Size = New System.Drawing.Size(52, 13)
        Label28.TabIndex = 89
        Label28.Text = "Category:"
        '
        'Label29
        '
        Label29.AutoSize = True
        Label29.Location = New System.Drawing.Point(802, 551)
        Label29.Name = "Label29"
        Label29.Size = New System.Drawing.Size(70, 13)
        Label29.TabIndex = 92
        Label29.Text = "Feedback to:"
        '
        'Label30
        '
        Label30.AutoSize = True
        Label30.Location = New System.Drawing.Point(712, 173)
        Label30.Name = "Label30"
        Label30.Size = New System.Drawing.Size(78, 13)
        Label30.TabIndex = 94
        Label30.Text = "Further Details:"
        '
        'Comp_noTextBox
        '
        Me.Comp_noTextBox.Location = New System.Drawing.Point(12, 36)
        Me.Comp_noTextBox.Name = "Comp_noTextBox"
        Me.Comp_noTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Comp_noTextBox.TabIndex = 0
        '
        'Comp_case_noTextBox
        '
        Me.Comp_case_noTextBox.Location = New System.Drawing.Point(188, 108)
        Me.Comp_case_noTextBox.Name = "Comp_case_noTextBox"
        Me.Comp_case_noTextBox.ReadOnly = True
        Me.Comp_case_noTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Comp_case_noTextBox.TabIndex = 5
        Me.Comp_case_noTextBox.TabStop = False
        '
        'Comp_cat_codeTextBox
        '
        Me.Comp_cat_codeTextBox.Location = New System.Drawing.Point(31, 189)
        Me.Comp_cat_codeTextBox.Name = "Comp_cat_codeTextBox"
        Me.Comp_cat_codeTextBox.ReadOnly = True
        Me.Comp_cat_codeTextBox.Size = New System.Drawing.Size(33, 20)
        Me.Comp_cat_codeTextBox.TabIndex = 10
        Me.Comp_cat_codeTextBox.TabStop = False
        '
        'Comp_textTextBox
        '
        Me.Comp_textTextBox.Location = New System.Drawing.Point(31, 244)
        Me.Comp_textTextBox.Multiline = True
        Me.Comp_textTextBox.Name = "Comp_textTextBox"
        Me.Comp_textTextBox.ReadOnly = True
        Me.Comp_textTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Comp_textTextBox.Size = New System.Drawing.Size(257, 105)
        Me.Comp_textTextBox.TabIndex = 14
        Me.Comp_textTextBox.TabStop = False
        '
        'Recvd_textTextBox
        '
        Me.Recvd_textTextBox.Location = New System.Drawing.Point(463, 37)
        Me.Recvd_textTextBox.Name = "Recvd_textTextBox"
        Me.Recvd_textTextBox.ReadOnly = True
        Me.Recvd_textTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Recvd_textTextBox.TabIndex = 15
        Me.Recvd_textTextBox.TabStop = False
        '
        'Recpt_textTextBox
        '
        Me.Recpt_textTextBox.Location = New System.Drawing.Point(345, 36)
        Me.Recpt_textTextBox.Name = "Recpt_textTextBox"
        Me.Recpt_textTextBox.ReadOnly = True
        Me.Recpt_textTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Recpt_textTextBox.TabIndex = 17
        Me.Recpt_textTextBox.TabStop = False
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(850, 615)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 4
        Me.exitbtn.Text = "&Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'cltextbox
        '
        Me.cltextbox.Location = New System.Drawing.Point(308, 108)
        Me.cltextbox.Name = "cltextbox"
        Me.cltextbox.ReadOnly = True
        Me.cltextbox.Size = New System.Drawing.Size(224, 20)
        Me.cltextbox.TabIndex = 19
        Me.cltextbox.TabStop = False
        '
        'Against_textTextBox
        '
        Me.Against_textTextBox.Location = New System.Drawing.Point(560, 108)
        Me.Against_textTextBox.Name = "Against_textTextBox"
        Me.Against_textTextBox.ReadOnly = True
        Me.Against_textTextBox.Size = New System.Drawing.Size(114, 20)
        Me.Against_textTextBox.TabIndex = 21
        Me.Against_textTextBox.TabStop = False
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'agent_TextBox
        '
        Me.agent_TextBox.Location = New System.Drawing.Point(694, 108)
        Me.agent_TextBox.Name = "agent_TextBox"
        Me.agent_TextBox.ReadOnly = True
        Me.agent_TextBox.Size = New System.Drawing.Size(122, 20)
        Me.agent_TextBox.TabIndex = 22
        Me.agent_TextBox.TabStop = False
        '
        'Inv_textTextBox
        '
        Me.Inv_textTextBox.Location = New System.Drawing.Point(833, 108)
        Me.Inv_textTextBox.Name = "Inv_textTextBox"
        Me.Inv_textTextBox.ReadOnly = True
        Me.Inv_textTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Inv_textTextBox.TabIndex = 24
        Me.Inv_textTextBox.TabStop = False
        '
        'Comp_responseTextBox
        '
        Me.Comp_responseTextBox.Location = New System.Drawing.Point(360, 241)
        Me.Comp_responseTextBox.Multiline = True
        Me.Comp_responseTextBox.Name = "Comp_responseTextBox"
        Me.Comp_responseTextBox.ReadOnly = True
        Me.Comp_responseTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Comp_responseTextBox.Size = New System.Drawing.Size(258, 113)
        Me.Comp_responseTextBox.TabIndex = 27
        Me.Comp_responseTextBox.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.comprbtn)
        Me.GroupBox1.Controls.Add(Me.notcomprbtn)
        Me.GroupBox1.Location = New System.Drawing.Point(30, 551)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(116, 79)
        Me.GroupBox1.TabIndex = 28
        Me.GroupBox1.TabStop = False
        '
        'comprbtn
        '
        Me.comprbtn.AutoSize = True
        Me.comprbtn.Enabled = False
        Me.comprbtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.comprbtn.Location = New System.Drawing.Point(1, 42)
        Me.comprbtn.Name = "comprbtn"
        Me.comprbtn.Size = New System.Drawing.Size(84, 17)
        Me.comprbtn.TabIndex = 30
        Me.comprbtn.TabStop = True
        Me.comprbtn.Text = "Completed"
        Me.comprbtn.UseVisualStyleBackColor = True
        '
        'notcomprbtn
        '
        Me.notcomprbtn.AutoSize = True
        Me.notcomprbtn.Enabled = False
        Me.notcomprbtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.notcomprbtn.Location = New System.Drawing.Point(1, 19)
        Me.notcomprbtn.Name = "notcomprbtn"
        Me.notcomprbtn.Size = New System.Drawing.Size(107, 17)
        Me.notcomprbtn.TabIndex = 29
        Me.notcomprbtn.TabStop = True
        Me.notcomprbtn.Text = "Not completed"
        Me.notcomprbtn.UseVisualStyleBackColor = True
        '
        'compbyTextBox
        '
        Me.compbyTextBox.Location = New System.Drawing.Point(167, 610)
        Me.compbyTextBox.Name = "compbyTextBox"
        Me.compbyTextBox.ReadOnly = True
        Me.compbyTextBox.Size = New System.Drawing.Size(100, 20)
        Me.compbyTextBox.TabIndex = 30
        Me.compbyTextBox.TabStop = False
        '
        'completed_datetextbox
        '
        Me.completed_datetextbox.Location = New System.Drawing.Point(167, 559)
        Me.completed_datetextbox.Name = "completed_datetextbox"
        Me.completed_datetextbox.ReadOnly = True
        Me.completed_datetextbox.Size = New System.Drawing.Size(100, 20)
        Me.completed_datetextbox.TabIndex = 31
        Me.completed_datetextbox.TabStop = False
        '
        'openrbtn
        '
        Me.openrbtn.AutoSize = True
        Me.openrbtn.Checked = True
        Me.openrbtn.Enabled = False
        Me.openrbtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.openrbtn.Location = New System.Drawing.Point(6, 19)
        Me.openrbtn.Name = "openrbtn"
        Me.openrbtn.Size = New System.Drawing.Size(66, 17)
        Me.openrbtn.TabIndex = 34
        Me.openrbtn.TabStop = True
        Me.openrbtn.Text = "Not set"
        Me.openrbtn.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.foundedrbtn)
        Me.GroupBox2.Controls.Add(Me.openrbtn)
        Me.GroupBox2.Controls.Add(Me.unfoundedrbtn)
        Me.GroupBox2.Location = New System.Drawing.Point(290, 540)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(119, 98)
        Me.GroupBox2.TabIndex = 35
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Founded/unfounded"
        '
        'foundedrbtn
        '
        Me.foundedrbtn.AutoSize = True
        Me.foundedrbtn.Enabled = False
        Me.foundedrbtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.foundedrbtn.Location = New System.Drawing.Point(6, 71)
        Me.foundedrbtn.Name = "foundedrbtn"
        Me.foundedrbtn.Size = New System.Drawing.Size(74, 17)
        Me.foundedrbtn.TabIndex = 30
        Me.foundedrbtn.TabStop = True
        Me.foundedrbtn.Text = "Founded"
        Me.foundedrbtn.UseVisualStyleBackColor = True
        '
        'unfoundedrbtn
        '
        Me.unfoundedrbtn.AutoSize = True
        Me.unfoundedrbtn.Enabled = False
        Me.unfoundedrbtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.unfoundedrbtn.Location = New System.Drawing.Point(6, 47)
        Me.unfoundedrbtn.Name = "unfoundedrbtn"
        Me.unfoundedrbtn.Size = New System.Drawing.Size(87, 17)
        Me.unfoundedrbtn.TabIndex = 29
        Me.unfoundedrbtn.TabStop = True
        Me.unfoundedrbtn.Text = "Unfounded"
        Me.unfoundedrbtn.UseVisualStyleBackColor = True
        '
        'completedbylbl
        '
        Me.completedbylbl.AutoSize = True
        Me.completedbylbl.Location = New System.Drawing.Point(171, 589)
        Me.completedbylbl.Name = "completedbylbl"
        Me.completedbylbl.Size = New System.Drawing.Size(71, 13)
        Me.completedbylbl.TabIndex = 35
        Me.completedbylbl.Text = "Completed by"
        '
        'completeddatelbl
        '
        Me.completeddatelbl.AutoSize = True
        Me.completeddatelbl.Location = New System.Drawing.Point(171, 540)
        Me.completeddatelbl.Name = "completeddatelbl"
        Me.completeddatelbl.Size = New System.Drawing.Size(83, 13)
        Me.completeddatelbl.TabIndex = 36
        Me.completeddatelbl.Text = "Completed Date"
        '
        'dispbtn
        '
        Me.dispbtn.Location = New System.Drawing.Point(825, 277)
        Me.dispbtn.Name = "dispbtn"
        Me.dispbtn.Size = New System.Drawing.Size(100, 23)
        Me.dispbtn.TabIndex = 1
        Me.dispbtn.Text = "&Display Another"
        Me.dispbtn.UseVisualStyleBackColor = True
        '
        'Action_nameTextBox
        '
        Me.Action_nameTextBox.Location = New System.Drawing.Point(412, 567)
        Me.Action_nameTextBox.Name = "Action_nameTextBox"
        Me.Action_nameTextBox.ReadOnly = True
        Me.Action_nameTextBox.Size = New System.Drawing.Size(165, 20)
        Me.Action_nameTextBox.TabIndex = 43
        Me.Action_nameTextBox.TabStop = False
        '
        'updbtn
        '
        Me.updbtn.Location = New System.Drawing.Point(819, 322)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(112, 23)
        Me.updbtn.TabIndex = 2
        Me.updbtn.Text = "&Update Complaint"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'days_TextBox
        '
        Me.days_TextBox.Location = New System.Drawing.Point(831, 235)
        Me.days_TextBox.Name = "days_TextBox"
        Me.days_TextBox.ReadOnly = True
        Me.days_TextBox.Size = New System.Drawing.Size(100, 20)
        Me.days_TextBox.TabIndex = 44
        '
        'days_Label
        '
        Me.days_Label.AutoSize = True
        Me.days_Label.Location = New System.Drawing.Point(836, 219)
        Me.days_Label.Name = "days_Label"
        Me.days_Label.Size = New System.Drawing.Size(74, 13)
        Me.days_Label.TabIndex = 45
        Me.days_Label.Text = "Working Days"
        '
        'doc_textbox
        '
        Me.doc_textbox.Location = New System.Drawing.Point(121, 415)
        Me.doc_textbox.Name = "doc_textbox"
        Me.doc_textbox.ReadOnly = True
        Me.doc_textbox.Size = New System.Drawing.Size(46, 20)
        Me.doc_textbox.TabIndex = 0
        Me.doc_textbox.TabStop = False
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ack_letter_sent
        '
        Me.ack_letter_sent.Location = New System.Drawing.Point(25, 35)
        Me.ack_letter_sent.Name = "ack_letter_sent"
        Me.ack_letter_sent.ReadOnly = True
        Me.ack_letter_sent.Size = New System.Drawing.Size(74, 20)
        Me.ack_letter_sent.TabIndex = 49
        Me.ack_letter_sent.TabStop = False
        '
        'hold_letter_sent
        '
        Me.hold_letter_sent.Location = New System.Drawing.Point(25, 72)
        Me.hold_letter_sent.Name = "hold_letter_sent"
        Me.hold_letter_sent.ReadOnly = True
        Me.hold_letter_sent.Size = New System.Drawing.Size(74, 20)
        Me.hold_letter_sent.TabIndex = 51
        Me.hold_letter_sent.TabStop = False
        '
        'doc_ListBox
        '
        Me.doc_ListBox.FormattingEnabled = True
        Me.doc_ListBox.Location = New System.Drawing.Point(25, 451)
        Me.doc_ListBox.Name = "doc_ListBox"
        Me.doc_ListBox.Size = New System.Drawing.Size(201, 56)
        Me.doc_ListBox.TabIndex = 3
        '
        'cat_numbertextbox
        '
        Me.cat_numbertextbox.Location = New System.Drawing.Point(89, 189)
        Me.cat_numbertextbox.Name = "cat_numbertextbox"
        Me.cat_numbertextbox.ReadOnly = True
        Me.cat_numbertextbox.Size = New System.Drawing.Size(574, 20)
        Me.cat_numbertextbox.TabIndex = 54
        Me.cat_numbertextbox.TabStop = False
        '
        'Cor_nameTextBox
        '
        Me.Cor_nameTextBox.Location = New System.Drawing.Point(614, 567)
        Me.Cor_nameTextBox.Name = "Cor_nameTextBox"
        Me.Cor_nameTextBox.ReadOnly = True
        Me.Cor_nameTextBox.Size = New System.Drawing.Size(100, 20)
        Me.Cor_nameTextBox.TabIndex = 56
        Me.Cor_nameTextBox.TabStop = False
        '
        'Hold_name_TextBox
        '
        Me.Hold_name_TextBox.Location = New System.Drawing.Point(685, 325)
        Me.Hold_name_TextBox.Name = "Hold_name_TextBox"
        Me.Hold_name_TextBox.ReadOnly = True
        Me.Hold_name_TextBox.Size = New System.Drawing.Size(100, 20)
        Me.Hold_name_TextBox.TabIndex = 58
        Me.Hold_name_TextBox.TabStop = False
        '
        'stage_TextBox
        '
        Me.stage_TextBox.Location = New System.Drawing.Point(685, 290)
        Me.stage_TextBox.Name = "stage_TextBox"
        Me.stage_TextBox.ReadOnly = True
        Me.stage_TextBox.Size = New System.Drawing.Size(100, 20)
        Me.stage_TextBox.TabIndex = 53
        Me.stage_TextBox.TabStop = False
        '
        'entered_bytextbox
        '
        Me.entered_bytextbox.Location = New System.Drawing.Point(226, 36)
        Me.entered_bytextbox.Name = "entered_bytextbox"
        Me.entered_bytextbox.ReadOnly = True
        Me.entered_bytextbox.Size = New System.Drawing.Size(100, 20)
        Me.entered_bytextbox.TabIndex = 60
        Me.entered_bytextbox.TabStop = False
        '
        'prioritytbox
        '
        Me.prioritytbox.Location = New System.Drawing.Point(67, 371)
        Me.prioritytbox.Name = "prioritytbox"
        Me.prioritytbox.ReadOnly = True
        Me.prioritytbox.Size = New System.Drawing.Size(100, 20)
        Me.prioritytbox.TabIndex = 62
        Me.prioritytbox.TabStop = False
        '
        'recvd_datetextbox
        '
        Me.recvd_datetextbox.Location = New System.Drawing.Point(121, 37)
        Me.recvd_datetextbox.Name = "recvd_datetextbox"
        Me.recvd_datetextbox.ReadOnly = True
        Me.recvd_datetextbox.Size = New System.Drawing.Size(87, 20)
        Me.recvd_datetextbox.TabIndex = 63
        Me.recvd_datetextbox.TabStop = False
        '
        'old_comp_noTextBox
        '
        Me.old_comp_noTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.ComplaintsBindingSource, "comp_old_comp_no", True))
        Me.old_comp_noTextBox.Location = New System.Drawing.Point(685, 258)
        Me.old_comp_noTextBox.Name = "old_comp_noTextBox"
        Me.old_comp_noTextBox.ReadOnly = True
        Me.old_comp_noTextBox.Size = New System.Drawing.Size(100, 20)
        Me.old_comp_noTextBox.TabIndex = 64
        Me.old_comp_noTextBox.TabStop = False
        '
        'ComplaintsBindingSource
        '
        Me.ComplaintsBindingSource.DataMember = "Complaints"
        Me.ComplaintsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'costs_cancel_tbox
        '
        Me.costs_cancel_tbox.Location = New System.Drawing.Point(415, 611)
        Me.costs_cancel_tbox.Name = "costs_cancel_tbox"
        Me.costs_cancel_tbox.ReadOnly = True
        Me.costs_cancel_tbox.Size = New System.Drawing.Size(100, 20)
        Me.costs_cancel_tbox.TabIndex = 65
        Me.costs_cancel_tbox.TabStop = False
        '
        'compensation_tbox
        '
        Me.compensation_tbox.Location = New System.Drawing.Point(614, 611)
        Me.compensation_tbox.Name = "compensation_tbox"
        Me.compensation_tbox.ReadOnly = True
        Me.compensation_tbox.Size = New System.Drawing.Size(100, 20)
        Me.compensation_tbox.TabIndex = 67
        Me.compensation_tbox.TabStop = False
        '
        'stage2_gbox
        '
        Me.stage2_gbox.Controls.Add(Me.stage2_completed_by_textbox)
        Me.stage2_gbox.Controls.Add(Me.stage2_completed_datepicker)
        Me.stage2_gbox.Controls.Add(Me.Label14)
        Me.stage2_gbox.Controls.Add(Me.Label15)
        Me.stage2_gbox.Controls.Add(Me.stage2_start_datepicker)
        Me.stage2_gbox.Controls.Add(Me.Label16)
        Me.stage2_gbox.Enabled = False
        Me.stage2_gbox.Location = New System.Drawing.Point(161, 18)
        Me.stage2_gbox.Name = "stage2_gbox"
        Me.stage2_gbox.Size = New System.Drawing.Size(232, 88)
        Me.stage2_gbox.TabIndex = 69
        Me.stage2_gbox.TabStop = False
        Me.stage2_gbox.Text = "Stage 2"
        '
        'stage2_completed_by_textbox
        '
        Me.stage2_completed_by_textbox.Location = New System.Drawing.Point(98, 39)
        Me.stage2_completed_by_textbox.Name = "stage2_completed_by_textbox"
        Me.stage2_completed_by_textbox.ReadOnly = True
        Me.stage2_completed_by_textbox.Size = New System.Drawing.Size(100, 20)
        Me.stage2_completed_by_textbox.TabIndex = 73
        '
        'stage2_completed_datepicker
        '
        Me.stage2_completed_datepicker.Enabled = False
        Me.stage2_completed_datepicker.Location = New System.Drawing.Point(98, 66)
        Me.stage2_completed_datepicker.Name = "stage2_completed_datepicker"
        Me.stage2_completed_datepicker.Size = New System.Drawing.Size(128, 20)
        Me.stage2_completed_datepicker.TabIndex = 2
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(6, 16)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(56, 13)
        Me.Label14.TabIndex = 70
        Me.Label14.Text = "Start date:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(6, 71)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(86, 13)
        Me.Label15.TabIndex = 71
        Me.Label15.Text = "Completed Date:"
        '
        'stage2_start_datepicker
        '
        Me.stage2_start_datepicker.Enabled = False
        Me.stage2_start_datepicker.Location = New System.Drawing.Point(98, 11)
        Me.stage2_start_datepicker.Name = "stage2_start_datepicker"
        Me.stage2_start_datepicker.Size = New System.Drawing.Size(128, 20)
        Me.stage2_start_datepicker.TabIndex = 0
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(6, 42)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(74, 13)
        Me.Label16.TabIndex = 72
        Me.Label16.Text = "Completed by:"
        '
        'gender_textbox
        '
        Me.gender_textbox.Location = New System.Drawing.Point(602, 36)
        Me.gender_textbox.Name = "gender_textbox"
        Me.gender_textbox.ReadOnly = True
        Me.gender_textbox.Size = New System.Drawing.Size(61, 20)
        Me.gender_textbox.TabIndex = 70
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(618, 9)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(45, 13)
        Me.Label17.TabIndex = 71
        Me.Label17.Text = "Gender:"
        '
        'dob_textbox
        '
        Me.dob_textbox.Location = New System.Drawing.Point(685, 36)
        Me.dob_textbox.Name = "dob_textbox"
        Me.dob_textbox.ReadOnly = True
        Me.dob_textbox.Size = New System.Drawing.Size(100, 20)
        Me.dob_textbox.TabIndex = 73
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(691, 9)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(111, 13)
        Me.Label18.TabIndex = 74
        Me.Label18.Text = "Debtor's Date of Birth:"
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(65, 92)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(50, 13)
        Me.Label19.TabIndex = 75
        Me.Label19.Text = "Ethnicity:"
        '
        'eth_textbox
        '
        Me.eth_textbox.Location = New System.Drawing.Point(12, 108)
        Me.eth_textbox.Name = "eth_textbox"
        Me.eth_textbox.ReadOnly = True
        Me.eth_textbox.Size = New System.Drawing.Size(170, 20)
        Me.eth_textbox.TabIndex = 76
        '
        'debt_typelbl
        '
        Me.debt_typelbl.AutoSize = True
        Me.debt_typelbl.Location = New System.Drawing.Point(191, 131)
        Me.debt_typelbl.Name = "debt_typelbl"
        Me.debt_typelbl.Size = New System.Drawing.Size(10, 13)
        Me.debt_typelbl.TabIndex = 77
        Me.debt_typelbl.Text = " "
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.ack_letter_sent)
        Me.GroupBox3.Controls.Add(Me.hold_letter_sent)
        Me.GroupBox3.Location = New System.Drawing.Point(30, 6)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(117, 119)
        Me.GroupBox3.TabIndex = 78
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Stage 1 Letters"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(42, 56)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 13)
        Me.Label7.TabIndex = 80
        Me.Label7.Text = "Holding"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(22, 19)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(95, 13)
        Me.Label6.TabIndex = 79
        Me.Label6.Text = "Acknowledgement"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label20)
        Me.GroupBox4.Controls.Add(Me.Label21)
        Me.GroupBox4.Controls.Add(Me.stage2_ack_letter_sent)
        Me.GroupBox4.Controls.Add(Me.stage2_hold_letter_sent)
        Me.GroupBox4.Location = New System.Drawing.Point(28, 9)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(119, 119)
        Me.GroupBox4.TabIndex = 79
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Stage 2 Letters"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(42, 56)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(43, 13)
        Me.Label20.TabIndex = 80
        Me.Label20.Text = "Holding"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(22, 19)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(95, 13)
        Me.Label21.TabIndex = 79
        Me.Label21.Text = "Acknowledgement"
        '
        'stage2_ack_letter_sent
        '
        Me.stage2_ack_letter_sent.Location = New System.Drawing.Point(25, 35)
        Me.stage2_ack_letter_sent.Name = "stage2_ack_letter_sent"
        Me.stage2_ack_letter_sent.ReadOnly = True
        Me.stage2_ack_letter_sent.Size = New System.Drawing.Size(74, 20)
        Me.stage2_ack_letter_sent.TabIndex = 49
        Me.stage2_ack_letter_sent.TabStop = False
        '
        'stage2_hold_letter_sent
        '
        Me.stage2_hold_letter_sent.Location = New System.Drawing.Point(25, 72)
        Me.stage2_hold_letter_sent.Name = "stage2_hold_letter_sent"
        Me.stage2_hold_letter_sent.ReadOnly = True
        Me.stage2_hold_letter_sent.Size = New System.Drawing.Size(74, 20)
        Me.stage2_hold_letter_sent.TabIndex = 51
        Me.stage2_hold_letter_sent.TabStop = False
        '
        'stage3_gbox
        '
        Me.stage3_gbox.Controls.Add(Me.stage3_completed_by_textbox)
        Me.stage3_gbox.Controls.Add(Me.stage3_completed_datepicker)
        Me.stage3_gbox.Controls.Add(Me.Label22)
        Me.stage3_gbox.Controls.Add(Me.Label23)
        Me.stage3_gbox.Controls.Add(Me.stage3_start_datepicker)
        Me.stage3_gbox.Controls.Add(Me.Label24)
        Me.stage3_gbox.Location = New System.Drawing.Point(42, 18)
        Me.stage3_gbox.Name = "stage3_gbox"
        Me.stage3_gbox.Size = New System.Drawing.Size(232, 88)
        Me.stage3_gbox.TabIndex = 80
        Me.stage3_gbox.TabStop = False
        Me.stage3_gbox.Text = "Stage 3"
        '
        'stage3_completed_by_textbox
        '
        Me.stage3_completed_by_textbox.Location = New System.Drawing.Point(98, 39)
        Me.stage3_completed_by_textbox.Name = "stage3_completed_by_textbox"
        Me.stage3_completed_by_textbox.ReadOnly = True
        Me.stage3_completed_by_textbox.Size = New System.Drawing.Size(100, 20)
        Me.stage3_completed_by_textbox.TabIndex = 73
        '
        'stage3_completed_datepicker
        '
        Me.stage3_completed_datepicker.Enabled = False
        Me.stage3_completed_datepicker.Location = New System.Drawing.Point(98, 66)
        Me.stage3_completed_datepicker.Name = "stage3_completed_datepicker"
        Me.stage3_completed_datepicker.Size = New System.Drawing.Size(128, 20)
        Me.stage3_completed_datepicker.TabIndex = 2
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(2, 16)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(56, 13)
        Me.Label22.TabIndex = 70
        Me.Label22.Text = "Start date:"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(2, 70)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(86, 13)
        Me.Label23.TabIndex = 71
        Me.Label23.Text = "Completed Date:"
        '
        'stage3_start_datepicker
        '
        Me.stage3_start_datepicker.Enabled = False
        Me.stage3_start_datepicker.Location = New System.Drawing.Point(98, 11)
        Me.stage3_start_datepicker.Name = "stage3_start_datepicker"
        Me.stage3_start_datepicker.Size = New System.Drawing.Size(128, 20)
        Me.stage3_start_datepicker.TabIndex = 0
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(2, 42)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(74, 13)
        Me.Label24.TabIndex = 72
        Me.Label24.Text = "Completed by:"
        '
        'agent2_TextBox
        '
        Me.agent2_TextBox.Location = New System.Drawing.Point(694, 146)
        Me.agent2_TextBox.Name = "agent2_TextBox"
        Me.agent2_TextBox.ReadOnly = True
        Me.agent2_TextBox.Size = New System.Drawing.Size(122, 20)
        Me.agent2_TextBox.TabIndex = 82
        Me.agent2_TextBox.TabStop = False
        '
        'Against2_textTextBox
        '
        Me.Against2_textTextBox.Location = New System.Drawing.Point(560, 146)
        Me.Against2_textTextBox.Name = "Against2_textTextBox"
        Me.Against2_textTextBox.ReadOnly = True
        Me.Against2_textTextBox.Size = New System.Drawing.Size(114, 20)
        Me.Against2_textTextBox.TabIndex = 81
        Me.Against2_textTextBox.TabStop = False
        '
        'cancel_reasonbtn
        '
        Me.cancel_reasonbtn.Location = New System.Drawing.Point(521, 611)
        Me.cancel_reasonbtn.Name = "cancel_reasonbtn"
        Me.cancel_reasonbtn.Size = New System.Drawing.Size(75, 23)
        Me.cancel_reasonbtn.TabIndex = 86
        Me.cancel_reasonbtn.Text = "&Reason:"
        Me.cancel_reasonbtn.UseVisualStyleBackColor = True
        '
        'stage_tabs
        '
        Me.stage_tabs.Controls.Add(Me.TabPage1)
        Me.stage_tabs.Controls.Add(Me.TabPage2)
        Me.stage_tabs.Controls.Add(Me.TabPage3)
        Me.stage_tabs.Controls.Add(Me.TabPage4)
        Me.stage_tabs.Controls.Add(Me.TabPage5)
        Me.stage_tabs.Controls.Add(Me.TabPage6)
        Me.stage_tabs.Location = New System.Drawing.Point(232, 371)
        Me.stage_tabs.Name = "stage_tabs"
        Me.stage_tabs.SelectedIndex = 0
        Me.stage_tabs.Size = New System.Drawing.Size(718, 163)
        Me.stage_tabs.TabIndex = 87
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox5)
        Me.TabPage1.Controls.Add(Me.GroupBox3)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(710, 137)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Stage 1"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.stage1_esc_tbox)
        Me.GroupBox5.Location = New System.Drawing.Point(170, 25)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(366, 100)
        Me.GroupBox5.TabIndex = 89
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Escalation Reason Stage 1 to Stage 2"
        '
        'stage1_esc_tbox
        '
        Me.stage1_esc_tbox.Enabled = False
        Me.stage1_esc_tbox.Location = New System.Drawing.Point(13, 49)
        Me.stage1_esc_tbox.Name = "stage1_esc_tbox"
        Me.stage1_esc_tbox.Size = New System.Drawing.Size(270, 20)
        Me.stage1_esc_tbox.TabIndex = 88
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.GroupBox6)
        Me.TabPage2.Controls.Add(Me.GroupBox4)
        Me.TabPage2.Controls.Add(Me.stage2_gbox)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(710, 137)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Stage 2"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.stage2_esc_tbox)
        Me.GroupBox6.Location = New System.Drawing.Point(399, 18)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(303, 100)
        Me.GroupBox6.TabIndex = 90
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Escalation Reason Stage 2 to Stage 3"
        '
        'stage2_esc_tbox
        '
        Me.stage2_esc_tbox.Enabled = False
        Me.stage2_esc_tbox.Location = New System.Drawing.Point(13, 49)
        Me.stage2_esc_tbox.Name = "stage2_esc_tbox"
        Me.stage2_esc_tbox.Size = New System.Drawing.Size(270, 20)
        Me.stage2_esc_tbox.TabIndex = 88
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.stage3_gbox)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(710, 137)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Stage 3"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.GroupBox9)
        Me.TabPage4.Controls.Add(Me.Label25)
        Me.TabPage4.Controls.Add(Me.solicitor_datepicker)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(710, 137)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Legal"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.legal_rbtn)
        Me.GroupBox9.Location = New System.Drawing.Point(24, 13)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(110, 79)
        Me.GroupBox9.TabIndex = 91
        Me.GroupBox9.TabStop = False
        Me.GroupBox9.Text = "Legal"
        '
        'legal_rbtn
        '
        Me.legal_rbtn.AutoSize = True
        Me.legal_rbtn.Location = New System.Drawing.Point(32, 30)
        Me.legal_rbtn.Name = "legal_rbtn"
        Me.legal_rbtn.Size = New System.Drawing.Size(51, 17)
        Me.legal_rbtn.TabIndex = 94
        Me.legal_rbtn.TabStop = True
        Me.legal_rbtn.Text = "Legal"
        Me.legal_rbtn.UseVisualStyleBackColor = True
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(194, 36)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(100, 13)
        Me.Label25.TabIndex = 86
        Me.Label25.Text = "Referred to Solicitor"
        '
        'solicitor_datepicker
        '
        Me.solicitor_datepicker.Enabled = False
        Me.solicitor_datepicker.Location = New System.Drawing.Point(191, 52)
        Me.solicitor_datepicker.Name = "solicitor_datepicker"
        Me.solicitor_datepicker.Size = New System.Drawing.Size(122, 20)
        Me.solicitor_datepicker.TabIndex = 88
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.pi_cbox)
        Me.TabPage5.Controls.Add(Me.pl_cbox)
        Me.TabPage5.Controls.Add(Me.GroupBox8)
        Me.TabPage5.Controls.Add(Me.GroupBox7)
        Me.TabPage5.Controls.Add(Me.Label27)
        Me.TabPage5.Controls.Add(Me.act_not_tbox)
        Me.TabPage5.Controls.Add(Me.Label26)
        Me.TabPage5.Controls.Add(Me.ins_datepicker)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(710, 137)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Insurance"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'pi_cbox
        '
        Me.pi_cbox.AutoSize = True
        Me.pi_cbox.Location = New System.Drawing.Point(399, 77)
        Me.pi_cbox.Name = "pi_cbox"
        Me.pi_cbox.Size = New System.Drawing.Size(93, 17)
        Me.pi_cbox.TabIndex = 97
        Me.pi_cbox.Text = "Prof Indemnity"
        Me.pi_cbox.UseVisualStyleBackColor = True
        '
        'pl_cbox
        '
        Me.pl_cbox.AutoSize = True
        Me.pl_cbox.Location = New System.Drawing.Point(399, 100)
        Me.pl_cbox.Name = "pl_cbox"
        Me.pl_cbox.Size = New System.Drawing.Size(92, 17)
        Me.pl_cbox.TabIndex = 96
        Me.pl_cbox.Text = "Public Liability"
        Me.pl_cbox.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.liability_rbtn)
        Me.GroupBox8.Location = New System.Drawing.Point(525, 22)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(149, 55)
        Me.GroupBox8.TabIndex = 90
        Me.GroupBox8.TabStop = False
        Me.GroupBox8.Text = "Liability"
        '
        'liability_rbtn
        '
        Me.liability_rbtn.AutoSize = True
        Me.liability_rbtn.Location = New System.Drawing.Point(27, 19)
        Me.liability_rbtn.Name = "liability_rbtn"
        Me.liability_rbtn.Size = New System.Drawing.Size(59, 17)
        Me.liability_rbtn.TabIndex = 95
        Me.liability_rbtn.TabStop = True
        Me.liability_rbtn.Text = "Liability"
        Me.liability_rbtn.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.ins_rbtn)
        Me.GroupBox7.Location = New System.Drawing.Point(22, 13)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(110, 79)
        Me.GroupBox7.TabIndex = 90
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "Insurance"
        '
        'ins_rbtn
        '
        Me.ins_rbtn.AutoSize = True
        Me.ins_rbtn.Location = New System.Drawing.Point(32, 30)
        Me.ins_rbtn.Name = "ins_rbtn"
        Me.ins_rbtn.Size = New System.Drawing.Size(72, 17)
        Me.ins_rbtn.TabIndex = 94
        Me.ins_rbtn.TabStop = True
        Me.ins_rbtn.Text = "Insurance"
        Me.ins_rbtn.UseVisualStyleBackColor = True
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(402, 27)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(81, 13)
        Me.Label27.TabIndex = 93
        Me.Label27.Text = "Insurance Type"
        '
        'act_not_tbox
        '
        Me.act_not_tbox.Location = New System.Drawing.Point(397, 43)
        Me.act_not_tbox.Name = "act_not_tbox"
        Me.act_not_tbox.ReadOnly = True
        Me.act_not_tbox.Size = New System.Drawing.Size(100, 20)
        Me.act_not_tbox.TabIndex = 90
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(218, 27)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(95, 13)
        Me.Label26.TabIndex = 87
        Me.Label26.Text = "Referred to Insurer"
        '
        'ins_datepicker
        '
        Me.ins_datepicker.Enabled = False
        Me.ins_datepicker.Location = New System.Drawing.Point(214, 43)
        Me.ins_datepicker.Name = "ins_datepicker"
        Me.ins_datepicker.Size = New System.Drawing.Size(122, 20)
        Me.ins_datepicker.TabIndex = 89
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.monetary_risktbox)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Size = New System.Drawing.Size(710, 137)
        Me.TabPage6.TabIndex = 5
        Me.TabPage6.Text = "Monetary Risk"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'monetary_risktbox
        '
        Me.monetary_risktbox.Location = New System.Drawing.Point(72, 36)
        Me.monetary_risktbox.Name = "monetary_risktbox"
        Me.monetary_risktbox.ReadOnly = True
        Me.monetary_risktbox.Size = New System.Drawing.Size(100, 20)
        Me.monetary_risktbox.TabIndex = 0
        '
        'category_tbox
        '
        Me.category_tbox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.category_tbox.Location = New System.Drawing.Point(833, 189)
        Me.category_tbox.Name = "category_tbox"
        Me.category_tbox.ReadOnly = True
        Me.category_tbox.Size = New System.Drawing.Size(100, 20)
        Me.category_tbox.TabIndex = 88
        Me.category_tbox.TabStop = False
        '
        'clref_lbl
        '
        Me.clref_lbl.AutoSize = True
        Me.clref_lbl.Location = New System.Drawing.Point(316, 131)
        Me.clref_lbl.Name = "clref_lbl"
        Me.clref_lbl.Size = New System.Drawing.Size(53, 13)
        Me.clref_lbl.TabIndex = 90
        Me.clref_lbl.Text = "Client Ref"
        '
        'feedback_tbox
        '
        Me.feedback_tbox.Location = New System.Drawing.Point(745, 570)
        Me.feedback_tbox.Name = "feedback_tbox"
        Me.feedback_tbox.ReadOnly = True
        Me.feedback_tbox.Size = New System.Drawing.Size(205, 20)
        Me.feedback_tbox.TabIndex = 91
        Me.feedback_tbox.TabStop = False
        '
        'type_code_tbox
        '
        Me.type_code_tbox.Location = New System.Drawing.Point(681, 189)
        Me.type_code_tbox.Name = "type_code_tbox"
        Me.type_code_tbox.ReadOnly = True
        Me.type_code_tbox.Size = New System.Drawing.Size(135, 20)
        Me.type_code_tbox.TabIndex = 93
        '
        'ComplaintsTableAdapter
        '
        Me.ComplaintsTableAdapter.ClearBeforeFill = True
        '
        'Receipt_typeBindingSource
        '
        Me.Receipt_typeBindingSource.DataMember = "Receipt_type"
        Me.Receipt_typeBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Receipt_typeTableAdapter
        '
        Me.Receipt_typeTableAdapter.ClearBeforeFill = True
        '
        'Received_fromBindingSource
        '
        Me.Received_fromBindingSource.DataMember = "Received_from"
        Me.Received_fromBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Received_fromTableAdapter
        '
        Me.Received_fromTableAdapter.ClearBeforeFill = True
        '
        'Complaint_againstBindingSource
        '
        Me.Complaint_againstBindingSource.DataMember = "Complaint_against"
        Me.Complaint_againstBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Complaint_againstTableAdapter
        '
        Me.Complaint_againstTableAdapter.ClearBeforeFill = True
        '
        'InvestigatorsBindingSource
        '
        Me.InvestigatorsBindingSource.DataMember = "Investigators"
        Me.InvestigatorsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'InvestigatorsTableAdapter
        '
        Me.InvestigatorsTableAdapter.ClearBeforeFill = True
        '
        'ActionsBindingSource
        '
        Me.ActionsBindingSource.DataMember = "Actions"
        Me.ActionsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'ActionsTableAdapter
        '
        Me.ActionsTableAdapter.ClearBeforeFill = True
        '
        'Corrective_actionsBindingSource
        '
        Me.Corrective_actionsBindingSource.DataMember = "Corrective actions"
        Me.Corrective_actionsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Corrective_actionsTableAdapter
        '
        Me.Corrective_actionsTableAdapter.ClearBeforeFill = True
        '
        'Hold_reasonBindingSource
        '
        Me.Hold_reasonBindingSource.DataMember = "Hold_reason"
        Me.Hold_reasonBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Hold_reasonTableAdapter
        '
        Me.Hold_reasonTableAdapter.ClearBeforeFill = True
        '
        'branch_tbox
        '
        Me.branch_tbox.Location = New System.Drawing.Point(805, 37)
        Me.branch_tbox.Name = "branch_tbox"
        Me.branch_tbox.ReadOnly = True
        Me.branch_tbox.Size = New System.Drawing.Size(126, 20)
        Me.branch_tbox.TabIndex = 95
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(858, 9)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(44, 13)
        Me.Label31.TabIndex = 96
        Me.Label31.Text = "Branch:"
        '
        'hold_lbl
        '
        Me.hold_lbl.AutoSize = True
        Me.hold_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.hold_lbl.Location = New System.Drawing.Point(39, 60)
        Me.hold_lbl.Name = "hold_lbl"
        Me.hold_lbl.Size = New System.Drawing.Size(99, 16)
        Me.hold_lbl.TabIndex = 97
        Me.hold_lbl.Text = "HOLD LABEL"
        '
        'cmpny_lbl
        '
        Me.cmpny_lbl.AutoSize = True
        Me.cmpny_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmpny_lbl.Location = New System.Drawing.Point(195, 60)
        Me.cmpny_lbl.Name = "cmpny_lbl"
        Me.cmpny_lbl.Size = New System.Drawing.Size(82, 16)
        Me.cmpny_lbl.TabIndex = 98
        Me.cmpny_lbl.Text = "COMPANY"
        '
        'displayfrm3
        '
        Me.AcceptButton = Me.dispbtn
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(962, 658)
        Me.Controls.Add(Me.cmpny_lbl)
        Me.Controls.Add(Me.hold_lbl)
        Me.Controls.Add(Me.Label31)
        Me.Controls.Add(Me.branch_tbox)
        Me.Controls.Add(Label30)
        Me.Controls.Add(Me.type_code_tbox)
        Me.Controls.Add(Label29)
        Me.Controls.Add(Me.feedback_tbox)
        Me.Controls.Add(Me.clref_lbl)
        Me.Controls.Add(Label28)
        Me.Controls.Add(Me.category_tbox)
        Me.Controls.Add(Me.stage_tabs)
        Me.Controls.Add(Me.cancel_reasonbtn)
        Me.Controls.Add(Me.agent2_TextBox)
        Me.Controls.Add(Me.Against2_textTextBox)
        Me.Controls.Add(Me.debt_typelbl)
        Me.Controls.Add(Me.eth_textbox)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.dob_textbox)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.gender_textbox)
        Me.Controls.Add(Label13)
        Me.Controls.Add(Me.compensation_tbox)
        Me.Controls.Add(Label12)
        Me.Controls.Add(Me.costs_cancel_tbox)
        Me.Controls.Add(Comp_old_comp_noLabel)
        Me.Controls.Add(Me.old_comp_noTextBox)
        Me.Controls.Add(Me.recvd_datetextbox)
        Me.Controls.Add(Me.prioritytbox)
        Me.Controls.Add(Label11)
        Me.Controls.Add(Me.entered_bytextbox)
        Me.Controls.Add(Label10)
        Me.Controls.Add(Me.Hold_name_TextBox)
        Me.Controls.Add(Label9)
        Me.Controls.Add(Me.Cor_nameTextBox)
        Me.Controls.Add(Label8)
        Me.Controls.Add(Me.cat_numbertextbox)
        Me.Controls.Add(Me.stage_TextBox)
        Me.Controls.Add(Me.doc_ListBox)
        Me.Controls.Add(Me.doc_textbox)
        Me.Controls.Add(Label5)
        Me.Controls.Add(Me.days_Label)
        Me.Controls.Add(Me.days_TextBox)
        Me.Controls.Add(Me.updbtn)
        Me.Controls.Add(Me.Action_nameTextBox)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Me.dispbtn)
        Me.Controls.Add(Me.completedbylbl)
        Me.Controls.Add(Me.completeddatelbl)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.completed_datetextbox)
        Me.Controls.Add(Me.compbyTextBox)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Comp_responseTextBox)
        Me.Controls.Add(Label3)
        Me.Controls.Add(Inv_textLabel)
        Me.Controls.Add(Me.Inv_textTextBox)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.agent_TextBox)
        Me.Controls.Add(Against_textLabel)
        Me.Controls.Add(Me.Against_textTextBox)
        Me.Controls.Add(Label1)
        Me.Controls.Add(Me.cltextbox)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Recpt_textLabel)
        Me.Controls.Add(Me.Recpt_textTextBox)
        Me.Controls.Add(Recvd_textLabel)
        Me.Controls.Add(Me.Recvd_textTextBox)
        Me.Controls.Add(Comp_textLabel)
        Me.Controls.Add(Me.Comp_textTextBox)
        Me.Controls.Add(Comp_cat_numberLabel)
        Me.Controls.Add(Comp_cat_codeLabel)
        Me.Controls.Add(Me.Comp_cat_codeTextBox)
        Me.Controls.Add(Comp_case_noLabel)
        Me.Controls.Add(Me.Comp_case_noTextBox)
        Me.Controls.Add(Comp_dateLabel)
        Me.Controls.Add(Comp_noLabel)
        Me.Controls.Add(Me.Comp_noTextBox)
        Me.Name = "displayfrm3"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Display a complaint"
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.ComplaintsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.stage2_gbox.ResumeLayout(False)
        Me.stage2_gbox.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.stage3_gbox.ResumeLayout(False)
        Me.stage3_gbox.PerformLayout()
        Me.stage_tabs.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        CType(Me.Receipt_typeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Received_fromBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Complaint_againstBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Corrective_actionsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Hold_reasonBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Comp_noTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Comp_case_noTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Comp_cat_codeTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Comp_textTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Recvd_textTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Recpt_textTextBox As System.Windows.Forms.TextBox
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents cltextbox As System.Windows.Forms.TextBox
    Friend WithEvents Against_textTextBox As System.Windows.Forms.TextBox
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents agent_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Inv_textTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Comp_responseTextBox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents comprbtn As System.Windows.Forms.RadioButton
    Friend WithEvents notcomprbtn As System.Windows.Forms.RadioButton
    Friend WithEvents compbyTextBox As System.Windows.Forms.TextBox
    Friend WithEvents completed_datetextbox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents foundedrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents unfoundedrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents openrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents completedbylbl As System.Windows.Forms.Label
    Friend WithEvents completeddatelbl As System.Windows.Forms.Label
    Friend WithEvents dispbtn As System.Windows.Forms.Button
    Friend WithEvents Action_nameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents updbtn As System.Windows.Forms.Button
    Friend WithEvents days_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents days_Label As System.Windows.Forms.Label
    Friend WithEvents doc_textbox As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ack_letter_sent As System.Windows.Forms.TextBox
    Friend WithEvents hold_letter_sent As System.Windows.Forms.TextBox
    Friend WithEvents doc_ListBox As System.Windows.Forms.ListBox
    Friend WithEvents cat_numbertextbox As System.Windows.Forms.TextBox
    Friend WithEvents Cor_nameTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Hold_name_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents stage_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents entered_bytextbox As System.Windows.Forms.TextBox
    Friend WithEvents prioritytbox As System.Windows.Forms.TextBox
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents ComplaintsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ComplaintsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ComplaintsTableAdapter
    Friend WithEvents Receipt_typeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Receipt_typeTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Receipt_typeTableAdapter
    Friend WithEvents Received_fromBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Received_fromTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
    Friend WithEvents Complaint_againstBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaint_againstTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
    Friend WithEvents InvestigatorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InvestigatorsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
    Friend WithEvents ActionsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ActionsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ActionsTableAdapter
    Friend WithEvents Corrective_actionsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Corrective_actionsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Corrective_actionsTableAdapter
    Friend WithEvents Hold_reasonBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Hold_reasonTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Hold_reasonTableAdapter
    Friend WithEvents recvd_datetextbox As System.Windows.Forms.TextBox
    Friend WithEvents old_comp_noTextBox As System.Windows.Forms.TextBox
    Friend WithEvents compensation_tbox As System.Windows.Forms.TextBox
    Friend WithEvents costs_cancel_tbox As System.Windows.Forms.TextBox
    Friend WithEvents stage2_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents stage2_completed_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents stage2_start_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents gender_textbox As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents dob_textbox As System.Windows.Forms.TextBox
    Friend WithEvents eth_textbox As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents debt_typelbl As System.Windows.Forms.Label
    Friend WithEvents stage2_completed_by_textbox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents stage2_ack_letter_sent As System.Windows.Forms.TextBox
    Friend WithEvents stage2_hold_letter_sent As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents stage3_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents stage3_completed_by_textbox As System.Windows.Forms.TextBox
    Friend WithEvents stage3_completed_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents stage3_start_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents agent2_TextBox As System.Windows.Forms.TextBox
    Friend WithEvents Against2_textTextBox As System.Windows.Forms.TextBox
    Friend WithEvents cancel_reasonbtn As System.Windows.Forms.Button
    Friend WithEvents stage_tabs As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents stage1_esc_tbox As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents stage2_esc_tbox As System.Windows.Forms.TextBox
    Friend WithEvents category_tbox As System.Windows.Forms.TextBox
    Friend WithEvents ins_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents solicitor_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents monetary_risktbox As System.Windows.Forms.TextBox
    Friend WithEvents act_not_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents ins_rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents liability_rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents legal_rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents clref_lbl As System.Windows.Forms.Label
    Friend WithEvents feedback_tbox As System.Windows.Forms.TextBox
    Friend WithEvents type_code_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents branch_tbox As System.Windows.Forms.TextBox
    Friend WithEvents hold_lbl As System.Windows.Forms.Label
    Friend WithEvents pl_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents pi_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents cmpny_lbl As System.Windows.Forms.Label
End Class

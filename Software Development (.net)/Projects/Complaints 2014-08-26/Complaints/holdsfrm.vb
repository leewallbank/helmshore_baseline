Public Class holdsfrm
    Private Sub holdsfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        stage_lbl.Text = "Stage " & hold_stage_no
        start_dtp.Value = CDate("Jan 1, 1900")
        end_dtp.Value = CDate("Jan 1, 1900")
        'look for stage hold dates
        updatecmpfrm.Complaints_HoldsTableAdapter.FillBy(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds, comp_no, hold_stage_no)
        If updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows.Count = 0 Then
            end_cbox.Checked = False
            end_gbox.Visible = False
            start_cbox.Checked = False
            start_gbox.Visible = False
            hold_start_user = 0
            hold_end_user = 0
        Else
            start_cbox.Checked = True
            start_gbox.Visible = True
            'start date agent
            hold_start_user = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows(0).Item(4)
            If hold_start_user = 0 Then
                end_cbox.Checked = False
                end_gbox.Visible = False
                start_cbox.Checked = False
                start_gbox.Visible = False
                hold_end_user = 0
            Else
                logon.InvestigatorsTableAdapter.FillBy(logon.PraiseAndComplaintsSQLDataSet.Investigators, hold_start_user)
                start_tbox.Text = logon.PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
                start_dtp.Value = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows(0).Item(2)
                'end date agent
                hold_end_user = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows(0).Item(5)
                If hold_end_user = 0 Then
                    end_cbox.Checked = False
                    end_gbox.Visible = False
                Else
                    end_dtp.Value = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows(0).Item(3)
                    logon.InvestigatorsTableAdapter.FillBy(logon.PraiseAndComplaintsSQLDataSet.Investigators, hold_end_user)
                    end_tbox.Text = logon.PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
                    end_cbox.Checked = True
                    end_gbox.Visible = True
                End If
            End If
        End If
        orig_hold_start = start_cbox.Checked
        orig_hold_end = end_cbox.Checked
        orig_hold_start_date = start_dtp.Value
        orig_hold_end_date = end_dtp.Value
    End Sub

    Private Sub nosavebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nosavebtn.Click
        Me.Close()
    End Sub

    Private Sub end_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles end_cbox.CheckedChanged

    End Sub

    Private Sub start_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles start_cbox.CheckedChanged
        
        
    End Sub

    Private Sub start_cbox_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles start_cbox.MouseClick
        If start_cbox.Checked Then
            'check no other stage hold is in force
            updatecmpfrm.Complaints_HoldsTableAdapter.FillBy1(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds, comp_no, hold_stage_no)
            If updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows.Count > 0 Then
                MsgBox("Hold already in place at another stage")
                start_cbox.Checked = False
                Exit Sub
            End If
        End If

        If start_cbox.Checked = False And end_cbox.Checked Then
            start_cbox.Checked = True
        Else
            start_gbox.Visible = start_cbox.Checked
            If start_cbox.Checked Then
                start_dtp.Value = Now
                start_tbox.Text = log_user
                hold_start_user = log_code
            End If
        End If
    End Sub

    Private Sub start_cbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles start_cbox.Validating
       
    End Sub

    Private Sub end_cbox_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles end_cbox.MouseClick
        If end_cbox.Checked = False Then
            'check no other hold is in place
            updatecmpfrm.Complaints_HoldsTableAdapter.FillBy1(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds, comp_no, hold_stage_no)
            If updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows.Count > 0 Then
                MsgBox("Hold already in place at another stage")
                end_cbox.Checked = True
                Exit Sub
            End If
        End If

        If end_cbox.Checked And start_cbox.Checked = False Then
            end_cbox.Checked = False
        Else
            end_gbox.Visible = end_cbox.Checked
            If end_cbox.Checked Then
                end_dtp.Value = Now
                end_tbox.Text = log_user
                hold_end_user = log_code
            End If
        End If
    End Sub

    Private Sub start_dtp_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles start_dtp.Validated
        start_tbox.Text = log_user
        hold_start_user = log_code
    End Sub

    Private Sub start_dtp_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles start_dtp.Validating
        ErrorProvider1.SetError(start_dtp, "")
        If end_cbox.Checked Then
            If start_dtp.Value >= end_dtp.Value Then
                ErrorProvider1.SetError(start_dtp, "Start date must be before end date")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub end_dtp_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles end_dtp.Validated
        end_tbox.Text = log_user
        hold_end_user = log_code
    End Sub

    Private Sub end_dtp_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles end_dtp.Validating
        ErrorProvider1.SetError(end_dtp, "")
        If start_dtp.Value >= end_dtp.Value Then
            ErrorProvider1.SetError(end_dtp, "End date must be after start date")
            e.Cancel = True
        End If
    End Sub

    Private Sub savebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles savebtn.Click
        Dim hold_start_date, hold_end_date As Date
        log_type = "Hold"
        Dim change_found As Boolean = False
        If start_cbox.Checked <> orig_hold_start Then
            change_found = True
            If start_cbox.Checked Then
                hold_start_date = start_dtp.Value
                log_text = "Stage " & hold_stage_no & " Hold start date changed to " & Format(hold_start_date, "dd/MM/yyyy")
                add_log(log_type, log_text)
            Else
                hold_start_date = CDate("Jan 1, 1900")
                hold_start_user = 0
                log_text = "Stage " & hold_stage_no & " Hold start date removed"
                add_log(log_type, log_text)
            End If
        Else
            hold_start_date = start_dtp.Value
        End If

        If end_cbox.Checked <> orig_hold_end Then
            change_found = True
            If end_cbox.Checked Then
                hold_end_date = end_dtp.Value
                log_text = "Stage " & hold_stage_no & " Hold end date changed to " & Format(hold_end_date, "dd/MM/yyyy")
                add_log(log_type, log_text)
            Else
                hold_end_date = CDate("Jan 1, 1900")
                hold_end_user = 0
                log_text = "Stage " & hold_stage_no & " Hold end date removed"
                add_log(log_type, log_text)
            End If
        Else
            hold_end_date = end_dtp.Value
        End If
        If change_found Then
            'see if row already exists
            updatecmpfrm.Complaints_HoldsTableAdapter.FillBy(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds, comp_no, hold_stage_no)
            If updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows.Count = 0 Then
                updatecmpfrm.Complaints_HoldsTableAdapter.InsertQuery(comp_no, hold_stage_no, hold_start_date, hold_end_date, hold_start_user, hold_end_user)
            Else
                updatecmpfrm.Complaints_HoldsTableAdapter.UpdateQuery(hold_start_date, hold_end_date, hold_start_user, hold_end_user, comp_no, hold_stage_no)
            End If
            If start_cbox.Checked And Not end_cbox.Checked Then
                updatecmpfrm.hold_lbl.Text = "Stage " & hold_stage_no & " ON HOLD"
            Else
                updatecmpfrm.hold_lbl.Text = ""
            End If
            If orig_hold_end And Not end_cbox.Checked Then
                off_hold = True
            End If
        End If
        Me.Close()
    End Sub
End Class
Public Class updmastfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub catbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles catbtn.Click
        categoriesfrm.ShowDialog()
        Dim idx As Integer
        For idx = comp_cat_orig_no_rows To categoriesfrm.DataGridView1.RowCount - 1
            cat_code = categoriesfrm.DataGridView1.Rows(idx - 1).Cells(0).Value
            cat_number = categoriesfrm.DataGridView1.Rows(idx - 1).Cells(1).Value
            cat_text = categoriesfrm.DataGridView1.Rows(idx - 1).Cells(2).Value
            Try
                categoriesfrm.Complaint_categoriesTableAdapter.InsertQuery(cat_code, cat_number, cat_text)
                log_text = "Category added - " & cat_code & " " & cat_number & " " & cat_text
                LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    Private Sub recbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recbtn.Click
        recpt_typefrm.ShowDialog()
        recpt_typefrm.recpt_type_dg.EndEdit()
        Dim idx As Integer
        Dim test As Integer = recpt_typefrm.recpt_type_dg.RowCount
        For idx = recpt_orig_no_rows To recpt_typefrm.recpt_type_dg.RowCount
            Try
                If recpt_typefrm.recpt_type_dg.Rows(idx).Cells(1).Value.ToString.Length = 0 Then
                    Continue For
                End If
            Catch ex As Exception
                Continue For
            End Try
            recpt_text = recpt_typefrm.recpt_type_dg.Rows(idx).Cells(1).Value
            Try
                'recpt_typefrm.Receipt_typeTableAdapter.InsertQuery(recpt_text)
                upd_txt = "insert into Receipt_type (recpt_text) values ('" & recpt_text & "')"
                update_sql(upd_txt)
                log_text = "Receipt type added - " & recpt_text
                log_type = "Master"
                add_log(log_type, log_text)
                'LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    Private Sub recvdbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recvdbtn.Click
        recvd_fromfrm.ShowDialog()
        recvd_fromfrm.recvd_from_dg.EndEdit()
        Dim idx As Integer
        For idx = recvd_from_orig_no_rows To recvd_fromfrm.recvd_from_dg.RowCount
            Try
                If recvd_fromfrm.recvd_from_dg.Rows(idx).Cells(1).Value.ToString.Length = 0 Then
                    Continue For
                End If
            Catch ex As Exception
                Continue For
            End Try
            recvd_text = recvd_fromfrm.recvd_from_dg.Rows(idx).Cells(1).Value
            Try
                'recvd_fromfrm.Received_fromTableAdapter.InsertQuery(recvd_text)
                log_text = "Received from added - " & recvd_text
                upd_txt = "insert into Received_from (recvd_text) values ('" & recvd_text & "')"
                update_sql(upd_txt)
                log_type = "Master"
                add_log(log_type, log_text)
                'LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    Private Sub againstbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles againstbtn.Click
        'first get branch
        branchfrm.ShowDialog()
        branch_no = branchfrm.branch_cbox.SelectedIndex
        againstfrm.ShowDialog()
        againstfrm.branch_dg.EndEdit()
        Dim idx As Integer
        For idx = against_orig_no_rows To againstfrm.branch_dg.RowCount - 1
            If againstfrm.branch_dg.Rows(idx - 1).Cells(1).Value = Nothing Then
                Exit Sub
            End If
            against_text = againstfrm.branch_dg.Rows(idx - 1).Cells(1).Value
            Try
                'againstfrm.Complaint_againstTableAdapter.InsertQuery(against_text, branch_no)
                upd_txt = "insert into Complaint_against (against_text, against_branch_no) values ('" & against_text & "'," & branch_no & ")"
                update_sql(upd_txt)
                log_text = "Complaint against added - " & against_text & " for branch " & branch_no
                log_type = "Master"
                add_log(log_type, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    Private Sub deptbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'deptfrm.ShowDialog()
        'Dim idx As Integer
        'For idx = dept_orig_no_rows To deptfrm.DataGridView1.RowCount - 1
        '    If deptfrm.DataGridView1.Rows(idx - 1).Cells(1).Value = Nothing Then
        '        Exit Sub
        '    End If
        '    dept_text = deptfrm.DataGridView1.Rows(idx - 1).Cells(1).Value
        '    Try
        '        deptfrm.DepartmentsTableAdapter.InsertQuery(dept_text)
        '        log_text = "Department added - " & dept_text
        '        LogTableAdapter.InsertQuery(log_user, Now, "master", 0, log_text)
        '    Catch ex As Exception
        '        MessageBox.Show(ex.Message)
        '    End Try
        'Next
    End Sub

    Private Sub investbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles investbtn.Click
        investfrm.ShowDialog()
        investfrm.inv_dg.EndEdit()
        Dim idx As Integer

        For idx = inv_orig_no_rows To investfrm.inv_dg.RowCount
            If investfrm.inv_dg.Rows(idx).Cells(1).Value = Nothing Then
                Exit Sub
            End If
            inv_text = investfrm.inv_dg.Rows(idx).Cells(1).Value
            Dim admin As Boolean
            Try
                admin = investfrm.inv_dg.Rows(idx).Cells(3).Value
            Catch
                admin = False
            End Try
            Dim admin9 As Integer
            If admin Then
                admin9 = 1
            Else
                admin9 = 0
            End If
            Dim inv_cmpny_no As Integer = 0
            Dim inv_cmpny_name As String = "Rossendales"
            Try
                inv_cmpny_name = investfrm.inv_dg.Rows(idx).Cells(2).Value
            Catch ex As Exception

            End Try
            Select Case inv_cmpny_name
                Case "Rossendales"
                    inv_cmpny_no = 0
                Case "Swift"
                    inv_cmpny_no = 1
                Case "Marston"
                    inv_cmpny_no = 2
            End Select
            Try
                'investfrm.InvestigatorsTableAdapter.InsertQuery(inv_text, "password", admin)
                log_text = "Investigator added - " & inv_text
                'LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
                upd_txt = "insert into investigators (inv_text, inv_password, inv_admin, inv_deleted, inv_cmpny_no) values ('" & _
                inv_text & "','password'," & admin9 & "," & 0 & "," & inv_cmpny_no & ")"
                update_sql(upd_txt)
                log_type = "Master"
                add_log(log_type, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub



    Private Sub updmastfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Log' table. You can move, or remove it, as needed.
        Me.LogTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Log)
        'TODO: This line of code loads data into the 'LogDataSet.Log' table. You can move, or remove it, as needed.
        Me.LogTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Log)

    End Sub

    Private Sub actionsbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles actionsbtn.Click
        actionsfrm.ShowDialog()
        Dim idx As Integer
        For idx = action_orig_no_rows To actionsfrm.DataGridView1.RowCount - 1
            If actionsfrm.DataGridView1.Rows(idx - 1).Cells(1).Value = Nothing Then
                Exit Sub
            End If
            action_name = actionsfrm.DataGridView1.Rows(idx - 1).Cells(1).Value
            Try
                actionsfrm.ActionsTableAdapter.InsertQuery(action_name)
                log_text = "Action added - " & action_name
                LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    Private Sub resetbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles resetbtn.Click
        resetfrm.ShowDialog()
    End Sub

    Private Sub corractbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles corractbtn.Click
        corr_actionsfrm.ShowDialog()
        corr_actionsfrm.corr_dg.EndEdit()
        Dim idx As Integer
        For idx = corr_action_orig_no_rows To corr_actionsfrm.corr_dg.RowCount - 1
            Try
                If corr_actionsfrm.corr_dg.Rows(idx - 1).Cells(1).Value = Nothing Then
                    Exit Sub
                End If
            Catch ex As Exception
                Continue For
            End Try
         
            corr_action_name = corr_actionsfrm.corr_dg.Rows(idx - 1).Cells(1).Value
            Try
                'corr_actionsfrm.Corrective_actionsTableAdapter.InsertQuery(corr_action_name)
                upd_txt = "insert into [Corrective actions] (cor_name) values ('" & corr_action_name & "')"
                update_sql(upd_txt)
                log_text = "Corrective action added - " & corr_action_name
                'LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
                log_type = "Master"
                add_log(log_type, log_text)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    Private Sub cl_turnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cl_turnbtn.Click
        ProgressBar1.Value = 10
        client_turnfrm.ShowDialog()
        ProgressBar1.Visible = False
    End Sub

    Private Sub notifybtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles notifybtn.Click
        ProgressBar1.Value = 10
        NotifyClientfrm.ShowDialog()
        ProgressBar1.Visible = False
    End Sub

    Private Sub cl2_btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cl2_btn.Click
        ProgressBar1.Value = 10
        NotifyClientfrm2.ShowDialog()
        ProgressBar1.Visible = False
    End Sub
End Class
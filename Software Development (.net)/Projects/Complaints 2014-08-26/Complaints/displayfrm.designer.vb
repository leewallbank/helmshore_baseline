<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class displayfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.exitbtn = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.RadioButton3 = New System.Windows.Forms.RadioButton
        Me.completedrbtn = New System.Windows.Forms.RadioButton
        Me.notcomprbtn = New System.Windows.Forms.RadioButton
        Me.openrbtn = New System.Windows.Forms.RadioButton
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.allfoundedrbtn = New System.Windows.Forms.RadioButton
        Me.unfoundedrbtn = New System.Windows.Forms.RadioButton
        Me.Foundedrbtn = New System.Windows.Forms.RadioButton
        Me.findbtn = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.invrbtn = New System.Windows.Forms.RadioButton
        Me.allrbtn = New System.Windows.Forms.RadioButton
        Me.clrbtn = New System.Windows.Forms.RadioButton
        Me.caserbtn = New System.Windows.Forms.RadioButton
        Me.dispbtn = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.recvd_fromcombobox = New System.Windows.Forms.ComboBox
        Me.ReceivedfromBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.ComplaintagainstBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Complaint_againstTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
        Me.Label1 = New System.Windows.Forms.Label
        Me.cat_combobox = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Received_fromTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Label3 = New System.Windows.Forms.Label
        Me.agentcombobox = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.against_combobox = New System.Windows.Forms.ComboBox
        Me.ComplaintagainstBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.find2btn = New System.Windows.Forms.Button
        Me.stage_combobox = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.name_textbox = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.start_recvd_datepicker = New System.Windows.Forms.DateTimePicker
        Me.comp_datepicker = New System.Windows.Forms.DateTimePicker
        Me.recvd_cbox = New System.Windows.Forms.CheckBox
        Me.comp_cbox = New System.Windows.Forms.CheckBox
        Me.debt_typecombobox = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.receipt_typecombobox = New System.Windows.Forms.ComboBox
        Me.ReceipttypeBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Receipt_typeTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Receipt_typeTableAdapter
        Me.category_combobox = New System.Windows.Forms.ComboBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.branch_cbox = New System.Windows.Forms.ComboBox
        Me.ComplaintbranchesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label10 = New System.Windows.Forms.Label
        Me.hold_rb = New System.Windows.Forms.RadioButton
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.all_holds_rb = New System.Windows.Forms.RadioButton
        Me.not_hold_rb = New System.Windows.Forms.RadioButton
        Me.Complaint_branchesTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_branchesTableAdapter
        Me.cmpny_cbox = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.fromlbl = New System.Windows.Forms.Label
        Me.tolbl = New System.Windows.Forms.Label
        Me.end_recvd_datepicker = New System.Windows.Forms.DateTimePicker
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.ReceivedfromBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComplaintagainstBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComplaintagainstBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReceipttypeBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComplaintbranchesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(422, 595)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 22
        Me.exitbtn.Text = "&Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RadioButton3)
        Me.GroupBox2.Controls.Add(Me.completedrbtn)
        Me.GroupBox2.Controls.Add(Me.notcomprbtn)
        Me.GroupBox2.Location = New System.Drawing.Point(24, 404)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(148, 100)
        Me.GroupBox2.TabIndex = 14
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Completed/not completed"
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Location = New System.Drawing.Point(6, 19)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(67, 17)
        Me.RadioButton3.TabIndex = 15
        Me.RadioButton3.Text = "All cases"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'completedrbtn
        '
        Me.completedrbtn.AutoSize = True
        Me.completedrbtn.Location = New System.Drawing.Point(6, 68)
        Me.completedrbtn.Name = "completedrbtn"
        Me.completedrbtn.Size = New System.Drawing.Size(75, 17)
        Me.completedrbtn.TabIndex = 8
        Me.completedrbtn.Text = "Completed"
        Me.completedrbtn.UseVisualStyleBackColor = True
        '
        'notcomprbtn
        '
        Me.notcomprbtn.AutoSize = True
        Me.notcomprbtn.Checked = True
        Me.notcomprbtn.Location = New System.Drawing.Point(6, 43)
        Me.notcomprbtn.Name = "notcomprbtn"
        Me.notcomprbtn.Size = New System.Drawing.Size(94, 17)
        Me.notcomprbtn.TabIndex = 9
        Me.notcomprbtn.TabStop = True
        Me.notcomprbtn.Text = "Not completed"
        Me.notcomprbtn.UseVisualStyleBackColor = True
        '
        'openrbtn
        '
        Me.openrbtn.AutoSize = True
        Me.openrbtn.Location = New System.Drawing.Point(12, 43)
        Me.openrbtn.Name = "openrbtn"
        Me.openrbtn.Size = New System.Drawing.Size(51, 17)
        Me.openrbtn.TabIndex = 7
        Me.openrbtn.Text = "Open"
        Me.openrbtn.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.allfoundedrbtn)
        Me.GroupBox3.Controls.Add(Me.unfoundedrbtn)
        Me.GroupBox3.Controls.Add(Me.Foundedrbtn)
        Me.GroupBox3.Controls.Add(Me.openrbtn)
        Me.GroupBox3.Location = New System.Drawing.Point(182, 404)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(122, 114)
        Me.GroupBox3.TabIndex = 15
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Founded/unfounded"
        '
        'allfoundedrbtn
        '
        Me.allfoundedrbtn.AutoSize = True
        Me.allfoundedrbtn.Checked = True
        Me.allfoundedrbtn.Location = New System.Drawing.Point(12, 19)
        Me.allfoundedrbtn.Name = "allfoundedrbtn"
        Me.allfoundedrbtn.Size = New System.Drawing.Size(67, 17)
        Me.allfoundedrbtn.TabIndex = 15
        Me.allfoundedrbtn.TabStop = True
        Me.allfoundedrbtn.Text = "All cases"
        Me.allfoundedrbtn.UseVisualStyleBackColor = True
        '
        'unfoundedrbtn
        '
        Me.unfoundedrbtn.AutoSize = True
        Me.unfoundedrbtn.Location = New System.Drawing.Point(12, 67)
        Me.unfoundedrbtn.Name = "unfoundedrbtn"
        Me.unfoundedrbtn.Size = New System.Drawing.Size(78, 17)
        Me.unfoundedrbtn.TabIndex = 11
        Me.unfoundedrbtn.Text = "Unfounded"
        Me.unfoundedrbtn.UseVisualStyleBackColor = True
        '
        'Foundedrbtn
        '
        Me.Foundedrbtn.AutoSize = True
        Me.Foundedrbtn.Location = New System.Drawing.Point(12, 90)
        Me.Foundedrbtn.Name = "Foundedrbtn"
        Me.Foundedrbtn.Size = New System.Drawing.Size(67, 17)
        Me.Foundedrbtn.TabIndex = 12
        Me.Foundedrbtn.Text = "Founded"
        Me.Foundedrbtn.UseVisualStyleBackColor = True
        '
        'findbtn
        '
        Me.findbtn.Location = New System.Drawing.Point(18, 568)
        Me.findbtn.Name = "findbtn"
        Me.findbtn.Size = New System.Drawing.Size(94, 23)
        Me.findbtn.TabIndex = 19
        Me.findbtn.Text = "Find &Complaints"
        Me.findbtn.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.invrbtn)
        Me.GroupBox1.Controls.Add(Me.allrbtn)
        Me.GroupBox1.Controls.Add(Me.clrbtn)
        Me.GroupBox1.Controls.Add(Me.caserbtn)
        Me.GroupBox1.Location = New System.Drawing.Point(18, 85)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(163, 131)
        Me.GroupBox1.TabIndex = 4
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "OR Select criteria as required"
        '
        'invrbtn
        '
        Me.invrbtn.AutoSize = True
        Me.invrbtn.Location = New System.Drawing.Point(6, 97)
        Me.invrbtn.Name = "invrbtn"
        Me.invrbtn.Size = New System.Drawing.Size(80, 17)
        Me.invrbtn.TabIndex = 9
        Me.invrbtn.Text = "Investigator"
        Me.invrbtn.UseVisualStyleBackColor = True
        '
        'allrbtn
        '
        Me.allrbtn.AutoSize = True
        Me.allrbtn.Checked = True
        Me.allrbtn.Location = New System.Drawing.Point(6, 27)
        Me.allrbtn.Name = "allrbtn"
        Me.allrbtn.Size = New System.Drawing.Size(67, 17)
        Me.allrbtn.TabIndex = 14
        Me.allrbtn.TabStop = True
        Me.allrbtn.Text = "All cases"
        Me.allrbtn.UseVisualStyleBackColor = True
        '
        'clrbtn
        '
        Me.clrbtn.AutoSize = True
        Me.clrbtn.Location = New System.Drawing.Point(6, 74)
        Me.clrbtn.Name = "clrbtn"
        Me.clrbtn.Size = New System.Drawing.Size(51, 17)
        Me.clrbtn.TabIndex = 14
        Me.clrbtn.Text = "Client"
        Me.clrbtn.UseVisualStyleBackColor = True
        '
        'caserbtn
        '
        Me.caserbtn.AutoSize = True
        Me.caserbtn.Location = New System.Drawing.Point(6, 50)
        Me.caserbtn.Name = "caserbtn"
        Me.caserbtn.Size = New System.Drawing.Size(66, 17)
        Me.caserbtn.TabIndex = 13
        Me.caserbtn.Text = "Case No"
        Me.caserbtn.UseVisualStyleBackColor = True
        '
        'dispbtn
        '
        Me.dispbtn.Location = New System.Drawing.Point(18, 26)
        Me.dispbtn.Name = "dispbtn"
        Me.dispbtn.Size = New System.Drawing.Size(116, 23)
        Me.dispbtn.TabIndex = 0
        Me.dispbtn.Text = "&Display a complaint"
        Me.dispbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(242, 595)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 21
        Me.ProgressBar1.Visible = False
        '
        'recvd_fromcombobox
        '
        Me.recvd_fromcombobox.DataSource = Me.ReceivedfromBindingSource
        Me.recvd_fromcombobox.DisplayMember = "recvd_text"
        Me.recvd_fromcombobox.FormattingEnabled = True
        Me.recvd_fromcombobox.Location = New System.Drawing.Point(376, 344)
        Me.recvd_fromcombobox.Name = "recvd_fromcombobox"
        Me.recvd_fromcombobox.Size = New System.Drawing.Size(121, 21)
        Me.recvd_fromcombobox.TabIndex = 13
        '
        'ReceivedfromBindingSource
        '
        Me.ReceivedfromBindingSource.DataMember = "Received_from"
        Me.ReceivedfromBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ComplaintagainstBindingSource
        '
        Me.ComplaintagainstBindingSource.DataMember = "Complaint_against"
        Me.ComplaintagainstBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Complaint_againstTableAdapter
        '
        Me.Complaint_againstTableAdapter.ClearBeforeFill = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label1.Location = New System.Drawing.Point(402, 328)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Complainant:"
        '
        'cat_combobox
        '
        Me.cat_combobox.FormattingEnabled = True
        Me.cat_combobox.Items.AddRange(New Object() {"A", "B", "C"})
        Me.cat_combobox.Location = New System.Drawing.Point(375, 404)
        Me.cat_combobox.Name = "cat_combobox"
        Me.cat_combobox.Size = New System.Drawing.Size(121, 21)
        Me.cat_combobox.TabIndex = 16
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label2.Location = New System.Drawing.Point(390, 388)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 13)
        Me.Label2.TabIndex = 12
        Me.Label2.Text = "Category Code:"
        '
        'Received_fromTableAdapter
        '
        Me.Received_fromTableAdapter.ClearBeforeFill = True
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label3.Location = New System.Drawing.Point(390, 271)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 13
        Me.Label3.Text = "Staff/Bailiff:"
        '
        'agentcombobox
        '
        Me.agentcombobox.FormattingEnabled = True
        Me.agentcombobox.Location = New System.Drawing.Point(363, 287)
        Me.agentcombobox.Name = "agentcombobox"
        Me.agentcombobox.Size = New System.Drawing.Size(134, 21)
        Me.agentcombobox.TabIndex = 12
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label4.Location = New System.Drawing.Point(418, 451)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 13)
        Me.Label4.TabIndex = 14
        Me.Label4.Text = "Against:"
        '
        'against_combobox
        '
        Me.against_combobox.FormattingEnabled = True
        Me.against_combobox.Location = New System.Drawing.Point(372, 467)
        Me.against_combobox.Name = "against_combobox"
        Me.against_combobox.Size = New System.Drawing.Size(121, 21)
        Me.against_combobox.TabIndex = 17
        '
        'ComplaintagainstBindingSource1
        '
        Me.ComplaintagainstBindingSource1.DataMember = "Complaint_against"
        Me.ComplaintagainstBindingSource1.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'find2btn
        '
        Me.find2btn.Location = New System.Drawing.Point(18, 597)
        Me.find2btn.Name = "find2btn"
        Me.find2btn.Size = New System.Drawing.Size(176, 23)
        Me.find2btn.TabIndex = 20
        Me.find2btn.Text = "&Find complaints (excl hold letter)"
        Me.find2btn.UseVisualStyleBackColor = True
        '
        'stage_combobox
        '
        Me.stage_combobox.FormattingEnabled = True
        Me.stage_combobox.Items.AddRange(New Object() {"Stage 1", "Stage 2", "Stage 3", "Legal", "Insurance"})
        Me.stage_combobox.Location = New System.Drawing.Point(377, 131)
        Me.stage_combobox.Name = "stage_combobox"
        Me.stage_combobox.Size = New System.Drawing.Size(120, 21)
        Me.stage_combobox.TabIndex = 5
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label5.Location = New System.Drawing.Point(376, 115)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(121, 13)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "Stage/Legal/Insurance:"
        '
        'name_textbox
        '
        Me.name_textbox.Location = New System.Drawing.Point(376, 76)
        Me.name_textbox.Name = "name_textbox"
        Me.name_textbox.Size = New System.Drawing.Size(121, 20)
        Me.name_textbox.TabIndex = 3
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label6.Location = New System.Drawing.Point(382, 60)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(114, 13)
        Me.Label6.TabIndex = 18
        Me.Label6.Text = "Name or start of name:"
        '
        'start_recvd_datepicker
        '
        Me.start_recvd_datepicker.Location = New System.Drawing.Point(212, 182)
        Me.start_recvd_datepicker.Name = "start_recvd_datepicker"
        Me.start_recvd_datepicker.Size = New System.Drawing.Size(128, 20)
        Me.start_recvd_datepicker.TabIndex = 7
        Me.start_recvd_datepicker.Visible = False
        '
        'comp_datepicker
        '
        Me.comp_datepicker.Location = New System.Drawing.Point(369, 236)
        Me.comp_datepicker.Name = "comp_datepicker"
        Me.comp_datepicker.Size = New System.Drawing.Size(128, 20)
        Me.comp_datepicker.TabIndex = 9
        Me.comp_datepicker.Visible = False
        '
        'recvd_cbox
        '
        Me.recvd_cbox.AutoSize = True
        Me.recvd_cbox.Location = New System.Drawing.Point(219, 151)
        Me.recvd_cbox.Name = "recvd_cbox"
        Me.recvd_cbox.Size = New System.Drawing.Size(105, 17)
        Me.recvd_cbox.TabIndex = 6
        Me.recvd_cbox.Text = "Received Period"
        Me.recvd_cbox.UseVisualStyleBackColor = True
        '
        'comp_cbox
        '
        Me.comp_cbox.AutoSize = True
        Me.comp_cbox.Location = New System.Drawing.Point(254, 239)
        Me.comp_cbox.Name = "comp_cbox"
        Me.comp_cbox.Size = New System.Drawing.Size(109, 17)
        Me.comp_cbox.TabIndex = 8
        Me.comp_cbox.Text = "Completed Month"
        Me.comp_cbox.UseVisualStyleBackColor = True
        '
        'debt_typecombobox
        '
        Me.debt_typecombobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.debt_typecombobox.FormattingEnabled = True
        Me.debt_typecombobox.Items.AddRange(New Object() {"ALL", "C TAX", "NNDR", "RTD"})
        Me.debt_typecombobox.Location = New System.Drawing.Point(372, 529)
        Me.debt_typecombobox.Name = "debt_typecombobox"
        Me.debt_typecombobox.Size = New System.Drawing.Size(121, 21)
        Me.debt_typecombobox.TabIndex = 18
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label7.Location = New System.Drawing.Point(407, 513)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(60, 13)
        Me.Label7.TabIndex = 20
        Me.Label7.Text = "Debt Type:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label8.Location = New System.Drawing.Point(239, 271)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(74, 13)
        Me.Label8.TabIndex = 21
        Me.Label8.Text = "Receipt Type:"
        '
        'receipt_typecombobox
        '
        Me.receipt_typecombobox.DataSource = Me.ReceipttypeBindingSource
        Me.receipt_typecombobox.DisplayMember = "recpt_text"
        Me.receipt_typecombobox.FormattingEnabled = True
        Me.receipt_typecombobox.Location = New System.Drawing.Point(219, 287)
        Me.receipt_typecombobox.Name = "receipt_typecombobox"
        Me.receipt_typecombobox.Size = New System.Drawing.Size(121, 21)
        Me.receipt_typecombobox.TabIndex = 11
        '
        'ReceipttypeBindingSource
        '
        Me.ReceipttypeBindingSource.DataMember = "Receipt_type"
        Me.ReceipttypeBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'Receipt_typeTableAdapter
        '
        Me.Receipt_typeTableAdapter.ClearBeforeFill = True
        '
        'category_combobox
        '
        Me.category_combobox.FormattingEnabled = True
        Me.category_combobox.Items.AddRange(New Object() {"ANY", "Fast Track", "Normal", "Special Handling"})
        Me.category_combobox.Location = New System.Drawing.Point(376, 28)
        Me.category_combobox.Name = "category_combobox"
        Me.category_combobox.Size = New System.Drawing.Size(121, 21)
        Me.category_combobox.TabIndex = 2
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label9.Location = New System.Drawing.Point(415, 12)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(52, 13)
        Me.Label9.TabIndex = 23
        Me.Label9.Text = "Category:"
        '
        'branch_cbox
        '
        Me.branch_cbox.FormattingEnabled = True
        Me.branch_cbox.Location = New System.Drawing.Point(187, 75)
        Me.branch_cbox.Name = "branch_cbox"
        Me.branch_cbox.Size = New System.Drawing.Size(155, 21)
        Me.branch_cbox.TabIndex = 1
        '
        'ComplaintbranchesBindingSource
        '
        Me.ComplaintbranchesBindingSource.DataMember = "Complaint_branches"
        Me.ComplaintbranchesBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSetBindingSource
        '
        'PraiseAndComplaintsSQLDataSetBindingSource
        '
        Me.PraiseAndComplaintsSQLDataSetBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        Me.PraiseAndComplaintsSQLDataSetBindingSource.Position = 0
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label10.Location = New System.Drawing.Point(260, 59)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(44, 13)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "Branch:"
        '
        'hold_rb
        '
        Me.hold_rb.AutoSize = True
        Me.hold_rb.Location = New System.Drawing.Point(11, 42)
        Me.hold_rb.Name = "hold_rb"
        Me.hold_rb.Size = New System.Drawing.Size(64, 17)
        Me.hold_rb.TabIndex = 26
        Me.hold_rb.Text = "On Hold"
        Me.hold_rb.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.all_holds_rb)
        Me.GroupBox4.Controls.Add(Me.not_hold_rb)
        Me.GroupBox4.Controls.Add(Me.hold_rb)
        Me.GroupBox4.Location = New System.Drawing.Point(30, 287)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(104, 91)
        Me.GroupBox4.TabIndex = 10
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "On Hold"
        '
        'all_holds_rb
        '
        Me.all_holds_rb.AutoSize = True
        Me.all_holds_rb.Checked = True
        Me.all_holds_rb.Location = New System.Drawing.Point(11, 19)
        Me.all_holds_rb.Name = "all_holds_rb"
        Me.all_holds_rb.Size = New System.Drawing.Size(67, 17)
        Me.all_holds_rb.TabIndex = 28
        Me.all_holds_rb.TabStop = True
        Me.all_holds_rb.Text = "All cases"
        Me.all_holds_rb.UseVisualStyleBackColor = True
        '
        'not_hold_rb
        '
        Me.not_hold_rb.AutoSize = True
        Me.not_hold_rb.Location = New System.Drawing.Point(11, 65)
        Me.not_hold_rb.Name = "not_hold_rb"
        Me.not_hold_rb.Size = New System.Drawing.Size(82, 17)
        Me.not_hold_rb.TabIndex = 28
        Me.not_hold_rb.Text = "Not on Hold"
        Me.not_hold_rb.UseVisualStyleBackColor = True
        '
        'Complaint_branchesTableAdapter
        '
        Me.Complaint_branchesTableAdapter.ClearBeforeFill = True
        '
        'cmpny_cbox
        '
        Me.cmpny_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cmpny_cbox.Enabled = False
        Me.cmpny_cbox.FormattingEnabled = True
        Me.cmpny_cbox.Location = New System.Drawing.Point(221, 26)
        Me.cmpny_cbox.Name = "cmpny_cbox"
        Me.cmpny_cbox.Size = New System.Drawing.Size(121, 21)
        Me.cmpny_cbox.TabIndex = 26
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label11.Location = New System.Drawing.Point(251, 9)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(54, 13)
        Me.Label11.TabIndex = 27
        Me.Label11.Text = "Company:"
        '
        'fromlbl
        '
        Me.fromlbl.AutoSize = True
        Me.fromlbl.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.fromlbl.Location = New System.Drawing.Point(251, 166)
        Me.fromlbl.Name = "fromlbl"
        Me.fromlbl.Size = New System.Drawing.Size(33, 13)
        Me.fromlbl.TabIndex = 28
        Me.fromlbl.Text = "From:"
        Me.fromlbl.Visible = False
        '
        'tolbl
        '
        Me.tolbl.AutoSize = True
        Me.tolbl.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.tolbl.Location = New System.Drawing.Point(404, 166)
        Me.tolbl.Name = "tolbl"
        Me.tolbl.Size = New System.Drawing.Size(23, 13)
        Me.tolbl.TabIndex = 30
        Me.tolbl.Text = "To:"
        Me.tolbl.Visible = False
        '
        'end_recvd_datepicker
        '
        Me.end_recvd_datepicker.Location = New System.Drawing.Point(365, 182)
        Me.end_recvd_datepicker.Name = "end_recvd_datepicker"
        Me.end_recvd_datepicker.Size = New System.Drawing.Size(128, 20)
        Me.end_recvd_datepicker.TabIndex = 29
        Me.end_recvd_datepicker.Visible = False
        '
        'displayfrm
        '
        Me.AcceptButton = Me.dispbtn
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(535, 640)
        Me.Controls.Add(Me.tolbl)
        Me.Controls.Add(Me.end_recvd_datepicker)
        Me.Controls.Add(Me.fromlbl)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.cmpny_cbox)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.branch_cbox)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.category_combobox)
        Me.Controls.Add(Me.receipt_typecombobox)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.debt_typecombobox)
        Me.Controls.Add(Me.comp_cbox)
        Me.Controls.Add(Me.recvd_cbox)
        Me.Controls.Add(Me.comp_datepicker)
        Me.Controls.Add(Me.start_recvd_datepicker)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.name_textbox)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.stage_combobox)
        Me.Controls.Add(Me.find2btn)
        Me.Controls.Add(Me.against_combobox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.agentcombobox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cat_combobox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.recvd_fromcombobox)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.dispbtn)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.findbtn)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "displayfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Display Complaints"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.ReceivedfromBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComplaintagainstBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComplaintagainstBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReceipttypeBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComplaintbranchesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents completedrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents notcomprbtn As System.Windows.Forms.RadioButton
    Friend WithEvents openrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents unfoundedrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents Foundedrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents findbtn As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents clrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents caserbtn As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents allfoundedrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents allrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents dispbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents invrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents recvd_fromcombobox As System.Windows.Forms.ComboBox
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents ComplaintagainstBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaint_againstTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_againstTableAdapter
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cat_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents ReceivedfromBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Received_fromTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Received_fromTableAdapter
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents agentcombobox As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents against_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents ComplaintagainstBindingSource1 As System.Windows.Forms.BindingSource
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents find2btn As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents stage_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents name_textbox As System.Windows.Forms.TextBox
    Friend WithEvents comp_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents recvd_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents comp_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents start_recvd_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents debt_typecombobox As System.Windows.Forms.ComboBox
    Friend WithEvents receipt_typecombobox As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents ReceipttypeBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Receipt_typeTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Receipt_typeTableAdapter
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents category_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents branch_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents hold_rb As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents all_holds_rb As System.Windows.Forms.RadioButton
    Friend WithEvents not_hold_rb As System.Windows.Forms.RadioButton
    Friend WithEvents PraiseAndComplaintsSQLDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaint_branchesTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_branchesTableAdapter
    Friend WithEvents ComplaintbranchesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cmpny_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents tolbl As System.Windows.Forms.Label
    Friend WithEvents end_recvd_datepicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents fromlbl As System.Windows.Forms.Label
End Class

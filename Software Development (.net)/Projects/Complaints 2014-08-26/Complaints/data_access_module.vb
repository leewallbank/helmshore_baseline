Imports System.Configuration
Imports System.Data.SqlClient
Module data_access_module
    Public sqlCon As New SqlConnection
    Public table_array(,)
    Public no_of_rows, last_rowid As Integer
    Public os_con As New Odbc.OdbcConnection
    Public conn As New Odbc.OdbcConnection()
    Public conn2 As New OleDb.OleDbConnection
    Function get_dataset(ByVal database_name As String, Optional ByVal select_string As String = Nothing) As DataSet
        Dim dataset As New DataSet

        Try
            If database_name = "onestep" Then
                'If os_conn.State = ConnectionState.Closed Then
                '    'os_conn.ConnectionString = _
                '    '     "Integrated Security=True;Dsn=DebtRecovery-Real;db=DebtRecovery;uid=thirdparty;password=thirdparty;"
                '    os_conn.ConnectionString = _
                '                    "Integrated Security=True;Dsn=DebtRecovery;db=DebtRecovery;uid=vbnet;password=tenbv;"
                '    'os_conn.ConnectionString = _
                '    '                "Integrated Security=True;Dsn=Debtrecovery-local-rep;db=DebtRecovery;uid=crystal;password=latsyrc;"
                '    Try
                '        os_conn.Open()
                '    Catch ex As Exception
                '        Try
                '            os_conn.ConnectionString = _
                '                       "Integrated Security=True;Dsn=DebtRecovery-Real;db=DebtRecovery;uid=thirdparty;password=thirdparty;"
                '            os_conn.Open()
                '        Catch ex2 As Exception
                '            MsgBox("Unable to open onestep connection")
                '        End Try
                '    End Try

                'Dim adapter As New Odbc.OdbcDataAdapter(select_string, os_conn)
                'adapter.Fill(dataset)
                If os_con.State = ConnectionState.Closed Then
                    Connect_os_Db()
                End If
                Dim adapter As New Odbc.OdbcDataAdapter(select_string, os_con)
                adapter.Fill(dataset)
            Else
                If sqlCon.State = ConnectionState.Closed Then
                    Connect_sqlDb()
                End If
                Dim adapter As New SqlDataAdapter(select_string, sqlCon)
                adapter.Fill(dataset)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            'try to re-connect
            os_con.ConnectionString = _
                      "Integrated Security=True;Dsn=DebtRecovery;db=DebtRecovery;uid=vbnet;password=tenbv;"
            Try
                os_con.Open()
            Catch ex2 As Exception
                MsgBox("Unable to open onestep connection")
            End Try
            Dim adapter As New Odbc.OdbcDataAdapter(select_string, os_con)
            adapter.Fill(dataset)
        End Try
        Try
            no_of_rows = dataset.Tables(0).Rows.Count
        Catch
            no_of_rows = 0
        End Try
        Return dataset
        '    Try
        '        If database_name = "onestep" Then
        '            'conn.ConnectionString = _
        '            '  "Integrated Security=True;Dsn=DebtRecovery - DDSybase;na=192.168.19.26,14100;db=DebtRecovery;uid=thirdpartyB;password=thirdpartyB;"

        '            If conn.State <> ConnectionState.Open Then
        '                conn.ConnectionString = _
        '                                      "Integrated Security=True;Dsn=DebtRecovery;db=DebtRecovery;uid=vbnet;password=tenbv;"
        '                conn.Open()
        '            End If
        '            Dim adapter As New Odbc.OdbcDataAdapter(select_string, conn)

        '            adapter.Fill(dataset)
        '            conn.Close()
        '        Else
        '            conn2.ConnectionString = ""
        '            If database_name = "Fees" Then
        '                conn2.ConnectionString = _
        '                 "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=FeesSQL;Data Source=ROSSVR01\SQL2005"
        '            ElseIf database_name = "TestFees" Then
        '                conn2.ConnectionString = _
        '                "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=TestFeesSQL;Data Source=ROSSVR01\SQL2005"
        '            ElseIf database_name = "Complaints" Then
        '                conn2.ConnectionString = _
        '                   "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=PraiseAndComplaintsSQL;Data Source=ROSSVR01\SQL2005"
        '            ElseIf database_name = "Employed" Then
        '                conn2.ConnectionString = _
        '               "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=r:\Access tables\Employed_bailiffs.mdb;Persist Security Info=False"
        '            End If
        '            conn2.Open()
        '            Dim adapter As New OleDb.OleDbDataAdapter(select_string, conn2)
        '            adapter.Fill(dataset)
        '            conn2.Close()
        '        End If
        '    Catch ex As Exception
        '        MessageBox.Show(ex.Message)
        '    End Try
        '    Try
        '        no_of_rows = dataset.Tables(0).Rows.Count
        '    Catch
        '        no_of_rows = 0
        '    End Try
        '    Return dataset

    End Function
    Public Sub Connect_os_Db()

        Try
            If Not IsNothing(os_con) Then
                'This is only necessary following an exception...
                If os_con.State = ConnectionState.Open Then os_con.Close()
            End If
            os_con.ConnectionString = ""
            os_con.ConnectionString = ConfigurationManager.ConnectionStrings("DebtRecovery").ConnectionString
            os_con.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings("DebtRecovery").ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings("DebtRecovery").ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub
    Sub update_sql(ByVal upd_txt As String)
        Try
            If sqlCon.State = ConnectionState.Closed Then
                Connect_sqlDb()
            End If
            Dim sqlCMD As SqlCommand

            'Define attachment to database table specifics
            sqlCMD = New SqlCommand
            With sqlCMD
                .Connection = sqlCon
                .CommandText = upd_txt
                .ExecuteNonQuery()
            End With

            'Clean up the connection
            sqlCMD = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub Connect_sqlDb()
        Dim conn_str As String = ""
        Try
            If Not IsNothing(sqlCon) Then
                'This is only necessary following an exception...
                If sqlCon.State = ConnectionState.Open Then sqlCon.Close()
            End If

            If env_str = "Prod" Then
                conn_str = "Complaints"
            Else
                conn_str = "TestComplaints"
            End If
            sqlCon.ConnectionString = ConfigurationManager.ConnectionStrings(conn_str).ConnectionString
            sqlCon.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings(conn_str).ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings(conn_Str).ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub
    Function get_table(ByVal database_name As String, ByVal table_name As String, Optional ByVal columns As String = Nothing, _
                Optional ByVal where_clause As String = Nothing, Optional ByVal order_clause As String = Nothing, _
                Optional ByVal pigeon_hole As Boolean = False) As Integer
        Dim dataset As New DataSet
        Dim idx2 As Integer
        Dim error_found As Boolean = True
        Dim select_string As String = "SELECT * from " & table_name & " " & where_clause & order_clause
        If database_name = "onestep" Then
            If conn.State <> ConnectionState.Open Then
                conn.ConnectionString = _
                                      "Integrated Security=True;Dsn=DebtRecovery;db=DebtRecovery;uid=vbnet;password=tenbv;"
                conn.Open()
            End If
            If Left(columns, 8) <> "01_rowid" And Len(columns) > 0 Then
                MessageBox.Show("Where clause must start 01_rowid")
                Return 1
            End If
            Dim adapter As New Odbc.OdbcDataAdapter(select_string, conn)
            adapter.Fill(dataset)
            conn.Close()
        Else
            conn2.ConnectionString = ""
            If database_name = "Fees" Then
                conn2.ConnectionString = _
                "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=FeesSQL;Data Source=ROSSVR01\SQL2005"
            ElseIf database_name = "Complaints" Then
                conn2.ConnectionString = _
                   "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=PraiseAndComplaintsSQL;Data Source=ROSSVR01\SQL2005"
            End If
            conn2.Open()
            Dim adapter As New OleDb.OleDbDataAdapter(select_string, conn2)
            adapter.Fill(dataset)
            conn2.Close()
        End If

        Try
            no_of_rows = dataset.Tables(0).Rows.Count
            If no_of_rows = 0 Then
                Return 100
            End If

            If pigeon_hole Then
                last_rowid = 0
            Else
                last_rowid = no_of_rows
            End If

            Dim lastcol As Integer = dataset.Tables(0).Columns.Count

            Dim table As DataTable
            Dim row As DataRow
            Dim col As DataColumn
            Dim idx As Integer = 0

            ReDim table_array(9999, lastcol)
            For Each table In dataset.Tables
                error_found = False
                For Each row In table.Rows
                    idx2 = 0
                    If Not pigeon_hole Then
                        idx += 1
                    Else
                        idx = row.Item(0).ToString
                        If idx > last_rowid Then
                            last_rowid = idx
                        End If
                    End If
                    For Each col In table.Columns
                        Dim col_no As Integer
                        col_no = InStr(1, columns, col.ColumnName)
                        If col_no > 0 Then
                            idx2 = Mid(columns, col_no - 2, 2)
                        Else
                            idx2 += 1
                        End If
                        If col_no > 0 Or columns = Nothing Then
                            table_array(idx, idx2) = row.Item(col).ToString
                        End If
                    Next
                Next
            Next
            ReDim Preserve table_array(last_rowid, lastcol)
            Return 0
        Catch ex As Exception
            If error_found Then
                MessageBox.Show(ex.Message)
                Return 1
            Else
                If idx2 = 0 Then
                    Return 100
                End If
            End If
        End Try
    End Function

End Module

Public Class logon

    Dim changed_row As Integer
    Private Sub logon_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'get environment
        env_str = "Test"
        comp_start_no = 161
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            env_str = "Test"
            comp_start_no = 1
        End Try

        If env_str <> "Prod" Then
            comp_start_no = 1
            MsgBox("WARNING - TEST DATABASE")
        End If

        param2 = "select cmpny_name from Complaint_Companies" & _
        " order by cmpny_code"
        Dim cmpny_ds As DataSet = get_dataset("Complaints", param2)
        cmpny_cbox.Items.Clear()
        Dim cmpny_row As DataRow
        For Each cmpny_row In cmpny_ds.Tables(0).Select
            cmpny_cbox.Items.Add(cmpny_row(0))
        Next
        Application.DoEvents()
        Me.InvestigatorsTableAdapter.FillBy1(Me.PraiseAndComplaintsSQLDataSet.Investigators)
        
        Dim row As DataRow
        Dim idx As Integer = 0
        Inv_textComboBox.Items.Clear()
        For Each row In PraiseAndComplaintsSQLDataSet.Investigators.Rows
            inv_text = PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(1)
            'ignore deleted investigators
            Dim inv_deleted As Boolean
            Try
                inv_deleted = PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(4)
            Catch ex As Exception
                inv_deleted = False
            End Try
            If inv_deleted Then
                idx += 1
                Continue For
            End If
            Inv_textComboBox.Items.Add(inv_text)
            idx += 1
        Next
        Inv_textComboBox.SelectedIndex = -1
        Inv_textComboBox.SelectedText = ""
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub savebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles savebtn.Click
        'log_code = PraiseAndComplaintsSQLDataSet.Investigators.Rows(Inv_textComboBox.SelectedIndex).Item(0)
        'If PraiseAndComplaintsSQLDataSet.Investigators.Rows(Inv_textComboBox.SelectedIndex).Item(3) = True Then
        '    admin_user = True
        'Else
        '    admin_user = False
        'End If

        log_user = My.User.Name
        If inv_passwordtextbox.Text <> user_password Then
            MsgBox("The password is not valid")
            inv_count += 1
            If inv_count > 5 Then
                MsgBox("Contact administrator to reset password")
                Dim oWSH = CreateObject("WScript.Shell")
                log_user = oWSH.ExpandEnvironmentStrings("%USERNAME%")
                log_user = My.User.Name
                Try
                    log_text = "5 invalid attempts at logon - " & Inv_textComboBox.Text & " by computer for " & log_user
                    updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "logon", 0, log_text)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
                Me.Close()
            End If
            inv_passwordtextbox.Focus()
            inv_passwordtextbox.SelectAll()
            Exit Sub
        End If
        log_user = Inv_textComboBox.Text
        If inv_passwordtextbox.Text = "password" Then
            password_reset = False
            newpassfrm.ShowDialog()
            If password_reset = False Then
                MsgBox("Password was not set")
                Me.Close()
            Else
                'update password on table
                Try
                    Try
                        InvestigatorsTableAdapter.updatepassword(inv_password, log_code)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                        Exit Sub
                    End Try
                    log_text = "New password set for - " & Inv_textComboBox.Text
                    updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "logon", 0, log_text)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End If
        End If
        super_user = False
        'Jeff and Ros initially set as supeuseres 16 and 30
        If log_code = 16 Or log_code = 30 Then
            super_user = True
        End If
        mainfrm.ShowDialog()
        Me.Close()

    End Sub

    Private Sub Inv_textComboBox_Enter(ByVal sender As Object, ByVal e As System.EventArgs)
        Inv_textComboBox.SelectedItem = -1
        inv_passwordtextbox.Text = ""
    End Sub

    Private Sub Inv_textComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Inv_textComboBox.Validated
        Dim idx As Integer = 0
        For Each row In PraiseAndComplaintsSQLDataSet.Investigators.Rows
            If PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(1) = Inv_textComboBox.Text Then
                log_code = PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(0)
                admin_user = PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(3)
                user_password = PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(2)
                changed_row = idx
                Try
                    cmpny_no = PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(5)
                Catch ex As Exception
                    cmpny_no = 0
                End Try

                cmpny_cbox.SelectedIndex = cmpny_no
                cmpny_name = cmpny_cbox.SelectedItem
                Exit For
            End If
            idx += 1
        Next
        inv_passwordtextbox.Text = ""
    End Sub

    Private Sub Inv_textComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Inv_textComboBox.SelectedIndexChanged

    End Sub

    Private Sub Inv_textComboBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Inv_textComboBox.Validating
        ErrorProvider1.SetError(Inv_textComboBox, "")
        If Len(Inv_textComboBox.Text) = 0 Then
            ErrorProvider1.SetError(Inv_textComboBox, "You must select your name")
            e.Cancel = True
        End If
    End Sub


    Private Sub inv_passwordtextbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles inv_passwordtextbox.TextChanged

    End Sub

    Private Sub cmpny_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmpny_cbox.SelectedIndexChanged

    End Sub
End Class
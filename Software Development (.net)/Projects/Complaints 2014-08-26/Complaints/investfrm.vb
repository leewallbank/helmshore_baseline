Public Class investfrm

    Private Sub investfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        param2 = "select inv_code, inv_text, inv_admin, inv_deleted, inv_cmpny_no from investigators " & _
        " order by inv_text"
        Dim inv_ds As DataSet = get_dataset("Complaints", param2)
        inv_dg.Rows.Clear()
        Dim inv_rows As Integer = no_of_rows - 1
        Dim inv_idx As Integer
        For inv_idx = 0 To inv_rows
            Dim admin As Boolean
            Try
                admin = inv_ds.Tables(0).Rows(inv_idx).Item(2)
            Catch ex As Exception
                admin = False
            End Try
            Dim deleted As Boolean
            Try
                deleted = inv_ds.Tables(0).Rows(inv_idx).Item(3)
            Catch ex As Exception
                deleted = False
            End Try
            Dim inv_cmpny_no As Integer
            Try
                inv_cmpny_no = inv_ds.Tables(0).Rows(inv_idx).Item(4)
            Catch ex As Exception
                inv_cmpny_no = 0
            End Try
            Dim inv_cmpny_name As String = "Rossendales"
            Select Case inv_cmpny_no
                Case 0
                    inv_cmpny_name = "Rossendales"
                Case 1
                    inv_cmpny_name = "Swift"
                Case 2
                    inv_cmpny_name = "Marston"
            End Select
            inv_dg.Rows.Add(inv_ds.Tables(0).Rows(inv_idx).Item(0), inv_ds.Tables(0).Rows(inv_idx).Item(1), inv_cmpny_name, admin, deleted)
        Next
        inv_orig_no_rows = inv_dg.RowCount - 1
    End Sub

    Private Sub inv_dg_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles inv_dg.CellValueChanged
        If e.RowIndex >= 0 Then
            Dim Inv_code As Integer = inv_dg.Rows(e.RowIndex).Cells(0).Value
            Dim Inv_text As String = inv_dg.Rows(e.RowIndex).Cells(1).Value
            

            If inv_dg.Rows(e.RowIndex).IsNewRow Or Inv_code = 0 Then Return
            If e.ColumnIndex = 1 Then
                'Me.InvestigatorsTableAdapter.UpdateQuery(Inv_text, admin, Inv_code)
                log_text = "Investigator update amended - " & Inv_code & _
                                                                 " from " & orig_text & " to " & Inv_text
                upd_txt = "update Investigators set inv_text = '" & Inv_text & "'" & _
                               " where inv_code = " & Inv_code
                update_sql(upd_txt)
                log_type = "Master"
                add_log(log_type, log_text)
                'updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
            ElseIf e.ColumnIndex = 2 Then
                'company changed
                Dim inv_dg_cmpny_name As String = inv_dg.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
                Dim inv_dg_cmpny_no As Integer = 0
                log_text = "Investigator amended - " & Inv_code & _
                         " company changed to " & inv_dg_cmpny_name
                Select Case inv_dg_cmpny_name
                    Case "Rossendales"
                        inv_dg_cmpny_no = 0
                    Case "Swift"
                        inv_dg_cmpny_no = 1
                    Case "Marston"
                        inv_dg_cmpny_no = 2
                End Select
                upd_txt = "update Investigators set inv_cmpny_no = " & inv_dg_cmpny_no & _
                               " where inv_code = " & Inv_code
                update_sql(upd_txt)
                log_type = "Master"
                add_log(log_type, log_text)

            ElseIf e.ColumnIndex = 3 Then
                Dim admin As Boolean
                Try
                    admin = inv_dg.Rows(e.RowIndex).Cells(2).Value
                Catch
                    admin = False
                End Try
                Dim admin9 As Integer
                If admin Then
                    admin9 = 1
                Else
                    admin9 = 0
                End If
                log_text = "Investigator amended - " & Inv_code & _
                                        " admin changed to " & admin
                upd_txt = "update Investigators set inv_admin = " & admin9 & _
                                               " where inv_code = " & Inv_code
                update_sql(upd_txt)
                log_type = "Master"
                add_log(log_type, log_text)
            ElseIf e.ColumnIndex = 4 Then
                Dim deleted As Boolean
                Try
                    deleted = inv_dg.Rows(e.RowIndex).Cells(3).Value
                Catch
                    deleted = False
                End Try
                Dim deleted9 As Integer
                If deleted Then
                    deleted9 = 1
                Else
                    deleted9 = 0
                End If
                log_text = "Investigator amended - " & Inv_code & _
                                                    " deleted changed to " & deleted
                upd_txt = "update Investigators set inv_deleted = " & deleted9 & _
                                               " where inv_code = " & Inv_code
                update_sql(upd_txt)
                log_type = "Master"
                add_log(log_type, log_text)
            End If
            Try

            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate Investigator entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub inv_dg_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles inv_dg.RowEnter

        Inv_code = inv_dg.Rows(e.RowIndex).Cells(0).Value
        orig_text = inv_dg.Rows(e.RowIndex).Cells(1).Value

    End Sub

    Private Sub inv_dg_RowLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles inv_dg.RowLeave
        inv_dg.EndEdit()
    End Sub

    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles inv_dg.RowsRemoved
        'If e.RowIndex > 0 Then
        '    Try
        '        Try
        '            Me.InvestigatorsTableAdapter.DeleteQuery(inv_code)
        '        Catch ex As Exception
        '            MessageBox.Show("Can't delete as complaints are allocated")
        '            Exit Sub
        '        End Try
        '        log_text = "Investigator deleted - " & inv_code & " name " & orig_text
        '        updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)

        '    Catch ex As Exception
        '        MessageBox.Show(ex.Message)
        '    End Try
        'End If
    End Sub

End Class
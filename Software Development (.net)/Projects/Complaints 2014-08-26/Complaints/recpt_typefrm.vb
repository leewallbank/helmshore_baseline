Public Class recpt_typefrm

    Private Sub recpt_typefrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.ComplaintsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaints)
        param2 = "select recpt_code, recpt_text from Receipt_type order by recpt_code"
        Dim recpt_type_ds As DataSet = get_dataset("Complaints", param2)
        recpt_type_dg.Rows.Clear()
        Dim recpt_rows As Integer = no_of_rows - 1
        Dim recpt_idx As Integer
        For recpt_idx = 0 To recpt_rows
            recpt_type_dg.Rows.Add(recpt_type_ds.Tables(0).Rows(recpt_idx).Item(0), recpt_type_ds.Tables(0).Rows(recpt_idx).Item(1))
        Next
        'Me.Receipt_typeTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Receipt_type)
        recpt_orig_no_rows = recpt_type_dg.RowCount - 1
    End Sub
    Private Sub recpt_type_dg_cellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles recpt_type_dg.CellValueChanged
        If e.ColumnIndex = 1 And e.RowIndex >= 0 Then
            Dim recpt_code As Integer = recpt_type_dg.Rows(e.RowIndex).Cells(0).Value
            If recpt_code = 0 Then Return
            recpt_type_dg.EndEdit()
            Dim recpt_text As String = recpt_type_dg.Rows(e.RowIndex).Cells(1).Value

            Try
                'Me.Receipt_typeTableAdapter.UpdateQuery(recpt_text, recpt_code)

                'updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
                upd_txt = "update Receipt_type set recpt_text = '" & recpt_text & "'" & _
                " where recpt_code = " & recpt_code
                update_sql(upd_txt)
                log_text = "Receipt type amended - " & recpt_code & _
                                                           " from " & orig_text & " to " & recpt_text
                log_type = "Master"
                add_log(log_type, log_text)
            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate receipt type entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles recpt_type_dg.RowEnter
        'If e.RowIndex > 0 Then
        recpt_code = recpt_type_dg.Rows(e.RowIndex).Cells(0).Value
        orig_text = recpt_type_dg.Rows(e.RowIndex).Cells(1).Value
        'End If
    End Sub

    Private Sub recpt_type_dg_RowLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles recpt_type_dg.RowLeave
        recpt_type_dg.EndEdit()
    End Sub
    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles recpt_type_dg.RowsRemoved
        If e.RowIndex > 0 Then
            Try
                Try
                    'Me.Receipt_typeTableAdapter.DeleteQuery(recpt_code)
                Catch
                    MessageBox.Show("Can't delete as there are complaints with this receipt type")
                    Exit Sub
                End Try
                'log_text = "Receipt type deleted - " & recpt_code & " name " & orig_text
                'updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
                'End If
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If

    End Sub

End Class
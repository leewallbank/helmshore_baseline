Public Class updatecmpfrm
    Dim initial_load As Boolean = True
    Dim comp_cmpny_no As Integer
    Private Sub updatecmpfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        off_hold = False

        Dim ent_code As Integer
        Dim completed_date As Date
        'get data for complaint from test or live
        If env_str = "Prod" And comp_no <= 160 Then
            MsgBox("First complaint in Production is 161")
            Me.Close()
            Exit Sub
        End If
        Dim prev_completed As Boolean

        If env_str <> "Prod" Then
            param2 = "select comp_recpt_code,comp_entered_by, comp_date, comp_text, " & _
            " comp_response, comp_recvd_code, comp_case_no," & _
            "comp_client_no, comp_cat_code, comp_cat_number,comp_against_code," & _
            "comp_against_code2,comp_allocated_to,comp_action, comp_founded," & _
            "comp_stage_no, comp_cor_code,comp_priority,comp_against_agent,comp_against_agent2," & _
            "comp_old_comp_no,comp_costs_cancel_value, comp_compensation,comp_response_no," & _
            "comp_stage2_start_date,comp_stage2_completed_date,comp_stage2_completed_by, " & _
            "comp_gender, comp_ethnicity, comp_ack_letter_date, comp_holding_letter_date,  " & _
            "comp_stage2_ack_letter_date, comp_stage2_holding_letter_date," & _
            "comp_stage3_start_date ,comp_stage3_completed_by, comp_stage3_completed_date," & _
            "Comp_referred_to_solicitor,Comp_referred_to_insurer, Comp_monetary_risk, " & _
            "Comp_legal_insurance_flag, comp_cancel_costs_reason, Comp_category, " & _
            "comp_branch_no, Comp_insurance_type_no, " & _
            " Comp_liability_flag, Comp_part_founded_agent_no, comp_feedback_no, " & _
            " Comp_type_code, comp_completed_by, comp_completion_date, comp_hold_code, " & _
            " comp_prof_indemnity_no, comp_public_liability_no, comp_cmpny_no " & _
            " FROM Complaints WHERE comp_no = " & comp_no
            Dim comp_ds As DataSet = get_dataset("Complaints", param2)
            If no_of_rows = 0 Then
                MsgBox("There is no complaint number " & comp_no)
                Me.Close()
                Exit Sub
            End If

            Try
                comp_cmpny_no = comp_ds.Tables(0).Rows(0).Item(53)
            Catch ex As Exception
                comp_cmpny_no = 0
            End Try
            If comp_cmpny_no <> cmpny_no And Not super_user Then
                MsgBox("Complaint number " & comp_no & " does not exist for " & cmpny_name)
                Me.Close()
                Exit Sub
            End If

            recpt_code = comp_ds.Tables(0).Rows(0).Item(0)
            ent_code = comp_ds.Tables(0).Rows(0).Item(1)
            param2 = "select inv_text from Investigators where inv_code = " & ent_code
            Dim inv_ds As DataSet = get_dataset("Complaints", param2)
            Try
                entered_byTextBox.Text = inv_ds.Tables(0).Rows(0).Item(0)
            Catch ex As Exception
                entered_byTextBox.Text = "Unknown"
            End Try

            Comp_dateTextBox.Text = CDate(comp_ds.Tables(0).Rows(0).Item(2))
            orig_details = comp_ds.Tables(0).Rows(0).Item(3)
            Try
                orig_resp_details = comp_ds.Tables(0).Rows(0).Item(4)
            Catch ex As Exception
                orig_resp_details = ""
            End Try
            recvd_from = comp_ds.Tables(0).Rows(0).Item(5)
            case_no = comp_ds.Tables(0).Rows(0).Item(6)
            cl_no = comp_ds.Tables(0).Rows(0).Item(7)
            cat_code = comp_ds.Tables(0).Rows(0).Item(8)
            cat_number = comp_ds.Tables(0).Rows(0).Item(9)
            against_code = comp_ds.Tables(0).Rows(0).Item(10)
            Try
                against2_code = comp_ds.Tables(0).Rows(0).Item(11)
            Catch ex As Exception
                against2_code = 0
            End Try
            alloc_to_code = comp_ds.Tables(0).Rows(0).Item(12)
            action_code = comp_ds.Tables(0).Rows(0).Item(13)
            orig_founded = comp_ds.Tables(0).Rows(0).Item(14)
            stage_no = comp_ds.Tables(0).Rows(0).Item(15)
            cor_code = comp_ds.Tables(0).Rows(0).Item(16)
            orig_priority = comp_ds.Tables(0).Rows(0).Item(17)
            agent_no = comp_ds.Tables(0).Rows(0).Item(18)
            Try
                agent2_no = comp_ds.Tables(0).Rows(0).Item(19)
            Catch ex As Exception
                agent2_no = 0
            End Try
            Try
                old_comp_no = comp_ds.Tables(0).Rows(0).Item(20)
            Catch ex As Exception
                old_comp_no = 0
            End Try
            Try
                costs_cancel = comp_ds.Tables(0).Rows(0).Item(21)
            Catch ex As Exception
                costs_cancel = 0
            End Try
            Try
                compensation = comp_ds.Tables(0).Rows(0).Item(22)
            Catch ex As Exception
                compensation = 0
            End Try
            Try
                resp_forgotten = comp_ds.Tables(0).Rows(0).Item(23)
            Catch ex As Exception
                resp_forgotten = 0
            End Try
            Try
                stage2_start_date = comp_ds.Tables(0).Rows(0).Item(24)
            Catch ex As Exception
                stage2_start_date = Nothing
            End Try
            Try
                stage2_completed_date = comp_ds.Tables(0).Rows(0).Item(25)
            Catch ex As Exception
                stage2_completed_date = Nothing
            End Try
            Try
                stage2_completed_by = comp_ds.Tables(0).Rows(0).Item(26)
            Catch ex As Exception
                stage2_completed_by = 0
            End Try
            Try
                gender = comp_ds.Tables(0).Rows(0).Item(27)
            Catch ex As Exception
                gender = "Unknown"
            End Try
            Try
                ethnicity = comp_ds.Tables(0).Rows(0).Item(28)
            Catch ex As Exception
                ethnicity = 0
            End Try
            Try
                ack_date = comp_ds.Tables(0).Rows(0).Item(29)
            Catch ex As Exception
                ack_date = CDate("1800,1,1")
            End Try
            Try
                holding_date = comp_ds.Tables(0).Rows(0).Item(30)
            Catch ex As Exception
                holding_date = CDate("1800,1,1")
            End Try
            Try
                stage2_ack_date = comp_ds.Tables(0).Rows(0).Item(31)
            Catch ex As Exception
                stage2_ack_date = CDate("1800,1,1")
            End Try
            Try
                stage2_holding_date = comp_ds.Tables(0).Rows(0).Item(32)
            Catch ex As Exception
                stage2_holding_date = CDate("1800,1,1")
            End Try
            Try
                stage3_start_date = comp_ds.Tables(0).Rows(0).Item(33)
            Catch ex As Exception
                stage3_start_date = CDate("1800,1,1")
            End Try
            Try
                stage3_completed_by = comp_ds.Tables(0).Rows(0).Item(34)
            Catch ex As Exception
                stage3_completed_by = 0
            End Try
            Try
                stage3_completed_date = comp_ds.Tables(0).Rows(0).Item(35)
            Catch ex As Exception
                stage3_completed_date = CDate("1800,1,1")
            End Try
            Try
                referred_to_solicitor = comp_ds.Tables(0).Rows(0).Item(36)
            Catch ex As Exception
                referred_to_solicitor = CDate("1800,1,1")
            End Try
            Try
                referred_to_insurer = comp_ds.Tables(0).Rows(0).Item(37)
            Catch ex As Exception
                referred_to_insurer = CDate("1800,1,1")
            End Try
            Try
                monetary_risk = comp_ds.Tables(0).Rows(0).Item(38)
            Catch ex As Exception
                monetary_risk = -999
            End Try

            Try
                legal_flag = comp_ds.Tables(0).Rows(0).Item(39)
            Catch ex As Exception
                legal_flag = "N"
            End Try
            Try
                cancel_costs_reason = comp_ds.Tables(0).Rows(0).Item(40)
            Catch ex As Exception
                cancel_costs_reason = ""
            End Try
            Try
                category = comp_ds.Tables(0).Rows(0).Item(41)
            Catch ex As Exception
                category = 0
            End Try
            Try
                branch_no = comp_ds.Tables(0).Rows(0).Item(42)
            Catch ex As Exception
                branch_no = 0
            End Try
            Try
                insurance_type_no = comp_ds.Tables(0).Rows(0).Item(43)
            Catch ex As Exception
                insurance_type_no = -1
            End Try
            liability_flag = ""
            If insurance_type_no >= 0 Then
                Try
                    liability_flag = comp_ds.Tables(0).Rows(0).Item(44)
                Catch ex As Exception
                    MsgBox("Check liability on insurance is correct")
                End Try
            End If
            If orig_founded = "P" Then
                Try
                    orig_pf_agent_no = comp_ds.Tables(0).Rows(0).Item(45)
                Catch ex As Exception
                    orig_pf_agent_no = 0
                End Try
            End If
            feedback_no = 0
            Try
                feedback_no = comp_ds.Tables(0).Rows(0).Item(46)
            Catch ex As Exception

            End Try
            Try
                type_code = comp_ds.Tables(0).Rows(0).Item(47)
            Catch ex As Exception
                type_code = 0
            End Try
            completed = False
            Try
                If comp_ds.Tables(0).Rows(0).Item(48) > 0 Then
                    completed = True
                    compby_code = comp_ds.Tables(0).Rows(0).Item(48)
                    completed_date = CDate(comp_ds.Tables(0).Rows(0).Item(49))
                End If
            Catch ex As Exception
                compby_code = 0
            End Try

            'check if complaint was previously completed
            prev_completed = True
            Dim testDate As Date
            Try
                testDate = CDate(comp_ds.Tables(0).Rows(0).Item(49))
            Catch ex As Exception
                prev_completed = False
            End Try
            Try
                hold_code = comp_ds.Tables(0).Rows(0).Item(50)
            Catch ex As Exception

            End Try
            Try
                prof_indemnity_no = comp_ds.Tables(0).Rows(0).Item(51)
            Catch ex As Exception
                prof_indemnity_no = 0
            End Try
            Try
                public_liability_no = comp_ds.Tables(0).Rows(0).Item(52)
            Catch ex As Exception
                public_liability_no = 0
            End Try
        Else
            Me.Complaints_type_codeTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaints_type_code)
            initial_load = True
            Me.Stage_EscalationTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Stage_Escalation)
            Me.Complaints_resp_clientsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaints_resp_clients)
            Me.ComplaintsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaints, comp_no)
            Try

                recpt_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(2)
            Catch
                MsgBox("There is no complaint number " & comp_no)
                Me.Close()
                Exit Sub
            End Try
            Try
                comp_cmpny_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(57)
            Catch ex As Exception
                comp_cmpny_no = 0
            End Try
            If comp_cmpny_no <> cmpny_no And Not super_user Then
                MsgBox("Complaint number " & comp_no & " does not exist for " & cmpny_name)
                Me.Close()
                Exit Sub
            End If
            Comp_dateTextBox.Text = CDate(PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(1))
            recvd_from = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(3)
            case_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(4)
            cl_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(5)
            against_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(6)
            agent_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(7)
            cat_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(8)
            cat_number = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(9)
            add_compfrm.EthnicityTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Ethnicity)
            ent_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(11)
            Me.InvestigatorsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Investigators, ent_code)
            entered_byTextBox.Text = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
            alloc_to_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(12)
            orig_details = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(13)
            completed = False
            Try
                If PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(15) > 0 Then
                    completed = True
                    compby_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(15)
                    completed_date = CDate(PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(14))
                End If
            Catch ex As Exception
                compby_code = 0
            End Try

            'see if complaint was previously completed
            prev_completed = True
            Dim testDate As Date
            Try
                testDate = CDate(PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(14))
            Catch ex As Exception
                prev_completed = False
            End Try
            orig_founded = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(18)
            Try
                orig_resp_details = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(19)
            Catch ex As Exception
                orig_resp_details = ""
            End Try
            action_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(20)
            stage_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(21)
            cor_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(22)
            Try
                hold_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(23)
            Catch ex As Exception

            End Try
            orig_priority = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(24)
            Try
                old_comp_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(25)
            Catch ex As Exception
                old_comp_no = 0
            End Try
            Try
                costs_cancel = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(26)
            Catch ex As Exception
                costs_cancel = 0
            End Try
            Try
                compensation = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(27)
            Catch ex As Exception
                compensation = 0
            End Try
            Try
                resp_forgotten = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(28)
            Catch ex As Exception
                resp_forgotten = 0
            End Try
            Try
                stage2_start_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(29)
            Catch ex As Exception
                stage2_start_date = Nothing
            End Try
            Try
                stage2_completed_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(30)
            Catch ex As Exception
                stage2_completed_date = Nothing
            End Try
            Try
                stage2_completed_by = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(31)
            Catch ex As Exception
                stage2_completed_by = 0
            End Try
            Try
                gender = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(32)
            Catch ex As Exception
                gender = "Unknown"
            End Try
            Try
                ethnicity = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(33)
            Catch ex As Exception
                ethnicity = 0
            End Try
            Try
                ack_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(34)
            Catch ex As Exception
                ack_date = CDate("1800,1,1")
            End Try
            Try
                holding_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(35)
            Catch ex As Exception
                holding_date = CDate("1800,1,1")
            End Try
            Try
                stage2_ack_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(36)
            Catch ex As Exception
                stage2_ack_date = CDate("1800,1,1")
            End Try
            Try
                stage2_holding_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(37)
            Catch ex As Exception
                stage2_holding_date = CDate("1800,1,1")
            End Try
            Try
                stage3_start_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(38)
            Catch ex As Exception
                stage3_start_date = CDate("1800,1,1")
            End Try
            Try
                stage3_completed_by = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(39)
            Catch ex As Exception
                stage3_completed_by = 0
            End Try
            Try
                stage3_completed_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(40)
            Catch ex As Exception
                stage3_completed_date = CDate("1800,1,1")
            End Try
            Try
                against2_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(41)
            Catch ex As Exception
                against2_code = 0
            End Try
            Try
                agent2_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(42)
            Catch ex As Exception
                agent2_no = 0
            End Try
            Try
                referred_to_solicitor = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(43)
            Catch ex As Exception
                referred_to_solicitor = CDate("1800,1,1")
            End Try
            Try
                referred_to_insurer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(44)
            Catch ex As Exception
                referred_to_insurer = CDate("1800,1,1")
            End Try
            Try
                monetary_risk = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(45)
            Catch ex As Exception
                monetary_risk = -999
            End Try

            Try
                legal_flag = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(46)
            Catch ex As Exception
                legal_flag = "N"
            End Try
            Try
                cancel_costs_reason = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(47)
            Catch ex As Exception
                cancel_costs_reason = ""
            End Try
            Try
                category = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(48)
            Catch ex As Exception
                category = 0
            End Try
            Try
                branch_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(49)
            Catch ex As Exception
                branch_no = 0
            End Try
            Try
                insurance_type_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(50)
            Catch ex As Exception
                insurance_type_no = -1
            End Try
            liability_flag = ""
            If insurance_type_no >= 0 Then
                Try
                    liability_flag = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(51)
                Catch ex As Exception
                    MsgBox("Check liability on insurance is correct")
                End Try
            End If
            If orig_founded = "P" Then
                Try
                    orig_pf_agent_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(52)
                Catch ex As Exception
                    orig_pf_agent_no = 0
                End Try
            End If
            feedback_no = 0
            Try
                feedback_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(53)
            Catch ex As Exception

            End Try
            Try
                type_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(54)
            Catch ex As Exception
                type_code = 0
            End Try

            Try
                prof_indemnity_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(55)
            Catch ex As Exception
                prof_indemnity_no = 0
            End Try
            Try
                public_liability_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(56)
            Catch ex As Exception
                public_liability_no = 0
            End Try

            Me.Hold_reasonTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Hold_reason)
            Me.Corrective_actionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Corrective_actions)
            Me.ActionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Actions)
            Me.ResponseTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.response)
            Me.Complaint_againstTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_against)
            Me.Complaint_categoriesTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_categories)

        End If

        Try
            Dim idx As Integer
            ErrorProvider1.SetError(agentComboBox, "")
            ErrorProvider1.SetError(Cat_textComboBox, "")
            ErrorProvider1.SetError(updbtn, "")
            ErrorProvider1.SetError(actions_ComboBox, "")
            ErrorProvider1.SetError(GroupBox2, "")
            ErrorProvider1.SetError(stage2_gbox, "")

            If env_str <> "Prod" Then
                If comp_no > 160 Then
                    MsgBox("Only update up to comp no 160 in TEST")
                    Me.Close()
                    Exit Sub
                End If
            End If

            'save original values
            orig_entered_date = Comp_dateTextBox.Text
            orig_recpt_code = recpt_code
            orig_recvd_from = recvd_from
            orig_case_no = case_no
            orig_cl_no = cl_no
            orig_agent_no = agent_no
            orig_agent2_no = agent2_no
            orig_alloc_to_code = alloc_to_code
            orig_action_code = action_code
            orig_cor_code = cor_code
            orig_old_comp_no = old_comp_no
            orig_costs_cancel = costs_cancel
            orig_compensation = compensation
            orig_resp_forgotten = resp_forgotten
            orig_stage_no = stage_no
            orig_stage2_start_date = stage2_start_date
            orig_stage2_completed_date = stage2_completed_date
            orig_stage2_completed_by = stage2_completed_by
            orig_stage3_start_date = stage3_start_date
            orig_stage3_completed_date = stage3_completed_date
            orig_stage3_completed_by = stage3_completed_by
            orig_legal_flag = legal_flag
            orig_insurance_type_no = insurance_type_no
            orig_liability = False
            If liability_flag = "Y" Then
                orig_liability = True
            End If
            orig_referred_to_solicitor = referred_to_solicitor
            orig_referred_to_insurer = referred_to_insurer
            orig_monetary_risk = monetary_risk
            orig_category = category
            orig_branch_no = branch_no
            orig_feedback_no = feedback_no
            orig_type_code = type_code
            orig_prof_indemnity_no = prof_indemnity_no
            orig_public_liability_no = public_liability_no

            'company
            param2 = "select cmpny_name from Complaint_companies" & _
            " where cmpny_code = " & comp_cmpny_no
            Dim cmpny_ds As DataSet = get_dataset("Complaints", param2)
            cmpny_lbl.Text = cmpny_ds.Tables(0).Rows(0).Item(0)

            'gender
            Select Case gender
                Case "U"
                    gender = "Unknown"
                Case "N"
                    gender = "N/A"
            End Select
            gender_combobox.Text = gender
            orig_gender = gender

            'ethnicity
            eth_combobox.Items.Clear()
            param2 = "select eth_code, eth_name from Ethnicity order by eth_code"
            Dim eth_ds As DataSet = get_dataset("Complaints", param2)
            Dim eth_rows As Integer = no_of_rows - 1
            Dim eth_idx As Integer
            For eth_idx = 0 To eth_rows
                eth_combobox.Items.Add(eth_ds.Tables(0).Rows(eth_idx).Item(1))
                If ethnicity = eth_idx Then
                    eth_combobox.Text = eth_ds.Tables(0).Rows(eth_idx).Item(1)
                End If
            Next
            'For idx = 0 To Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows.Count - 1
            '    Dim eth_code As Integer = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(0)
            '    Dim eth_desc As String = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(1)
            '    eth_combobox.Items.Add(eth_desc)
            '    If ethnicity = eth_code Then
            '        eth_combobox.Text = eth_desc
            '    End If
            'Next
            ethnicity_desc = eth_combobox.Text
            orig_ethnicity_desc = eth_combobox.Text

            doc_change_made = False

            Comp_noTextBox.Text = comp_no
            Comp_textTextBox.Text = orig_details
            Comp_responseTextBox.Text = orig_resp_details

            'receipt type
            'Me.Receipt_typeTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Receipt_type, recpt_code)
            param2 = "select recpt_code, recpt_text from Receipt_type order by recpt_code "
            Dim recpt_ds As DataSet = get_dataset("Complaints", param2)
            'recpt_text = PraiseAndComplaintsSQLDataSet.Receipt_type.Rows(0).Item(1)
            'Me.Receipt_typeTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Receipt_type)
            recpt_combobox.Items.Clear()
            'idx = 0
            'For Each row In PraiseAndComplaintsSQLDataSet.Receipt_type.Rows
            '    recpt_combobox.Items.Add(PraiseAndComplaintsSQLDataSet.Receipt_type.Rows(idx).Item(1))
            '    idx += 1
            'Next
            Dim recpt_rows As Integer = no_of_rows - 1
            For idx = 0 To recpt_rows
                recpt_combobox.Items.Add(recpt_ds.Tables(0).Rows(idx).Item(1))
                If recpt_code = recpt_ds.Tables(0).Rows(idx).Item(0) Then
                    recpt_text = recpt_ds.Tables(0).Rows(idx).Item(1)
                End If
            Next
            recpt_combobox.Text = recpt_text
            orig_recpt_text = recpt_text

            'received from
            param2 = "select recvd_from, recvd_text from Received_from order by recvd_from "
            Dim recvd_ds As DataSet = get_dataset("Complaints", param2)
            'Me.Received_fromTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Received_from, recvd_from)
            'recvd_text = PraiseAndComplaintsSQLDataSet.Received_from.Rows(0).Item(1)

            'Me.Received_fromTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Received_from)
            recvd_textComboBox.Items.Clear()
            Dim recvd_rows As Integer = no_of_rows - 1
            For idx = 0 To recvd_rows
                recvd_textComboBox.Items.Add(recvd_ds.Tables(0).Rows(idx).Item(1))
                If recvd_from = recvd_ds.Tables(0).Rows(idx).Item(0) Then
                    recvd_text = recvd_ds.Tables(0).Rows(idx).Item(1)
                End If
            Next
            'For Each row In PraiseAndComplaintsSQLDataSet.Received_from.Rows
            '    recvd_textComboBox.Items.Add(PraiseAndComplaintsSQLDataSet.Received_from.Rows(idx).Item(1))
            '    idx += 1
            'Next
            orig_recvd_text = recvd_text
            recvd_textComboBox.Text = recvd_text

            'case no and client no
            comp_case_noTextBox.Text = case_no
            'if non-zero case no get dob from onestep
            Dim dob_str As String = "N/A"
            clref_lbl.Text = ""
            debt_typelbl.Text = ""
            If case_no > 0 And comp_cmpny_no = 0 Then
                param1 = "onestep"
                param2 = "select dateOfBirth, clientschemeID, client_ref from Debtor where _rowid = " & case_no
                Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 1 Then
                    Dim dob As Date
                    Try
                        dob = debtor_dataset.Tables(0).Rows(0).Item(0)
                    Catch ex As Exception
                        dob_str = "unknown"
                    End Try
                    If dob_str <> "unknown" Then
                        dob_str = Format(dob, "dd/MM/yyyy")
                    End If
                    'get cl ref
                    clref_lbl.Text = "Client Ref:" & debtor_dataset.Tables(0).Rows(0).Item(2)
                    'get debt type
                    Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(1)
                    param2 = "select schemeID from ClientScheme where _rowid = " & csid
                    Dim csid_dataset As DataSet = get_dataset(param1, param2)
                    Dim sch_id As Integer = csid_dataset.Tables(0).Rows(0).Item(0)
                    param2 = "select name from Scheme where _rowid = " & sch_id
                    Dim sch_dataset As DataSet = get_dataset(param1, param2)
                    debt_typelbl.Text = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                End If
            End If
            dob_textbox.Text = dob_str

            'category code
            orig_cat_code = cat_code
            orig_cat_number = cat_number
            '5.3.14
            'Me.Complaint_categoriesTableAdapter.Fill2(cat_table, cat_code)
            param2 = "select cat_code, cat_number, cat_text from Complaint_categories" & _
            " where cat_code = '" & cat_code & "'" & _
            " order by cat_number"
            Dim cat_ds As DataSet = get_dataset("Complaints", param2)

            Dim cat_text As String

            Select Case cat_code
                Case "A" : cat_codeComboBox.SelectedIndex = 0
                Case "B" : cat_codeComboBox.SelectedIndex = 1
                Case "C" : cat_codeComboBox.SelectedIndex = 2
            End Select

            idx = 0
            Cat_textComboBox.Items.Clear()
            'For Each row In cat_table.Rows
            For Each row In cat_ds.Tables(0).Rows
                'cat_text = cat_table.Rows(idx).Item(1) & " " & cat_table.Rows(idx).Item(2)
                cat_text = cat_ds.Tables(0).Rows(idx).Item(1) & " " & cat_ds.Tables(0).Rows(idx).Item(2)
                Cat_textComboBox.Items.Add(cat_text)
                If cat_number = 99 Then
                    Cat_textComboBox.Text = "99 Contentious"
                    'ElseIf cat_number = cat_table.Rows(idx).Item(1) Then
                ElseIf cat_number = cat_ds.Tables(0).Rows(idx).Item(1) Then
                    Cat_textComboBox.SelectedIndex = idx
                    Cat_textComboBox.Text = cat_text
                End If
                idx += 1
            Next

            'branch
            branch_cbox.Items.Clear()
            param2 = "select branch_code, branch_name from complaint_branches " & _
            " where branch_cmpny_no = " & comp_cmpny_no & _
            " order by branch_code"
            Dim branch_ds As DataSet = get_dataset("Complaints", param2)
            'add_compfrm.Complaint_branchesTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_branches)

            'For Each row In Me.PraiseAndComplaintsSQLDataSet.Complaint_branches.Rows
            comp_against_codeComboBox.Items.Clear()
            comp_against2_codecombobox.Items.Clear()
            Dim branchRow, againstRow As DataRow
            For Each branchRow In branch_ds.Tables(0).Rows
                Dim branch_code As Integer = branchRow(0)
                branch_cbox.Items.Add(branchRow(1))
                If branch_code = branch_no Then
                    branch_cbox.Text = branchRow(1)
                End If

                'get complaint against for this branch code
                param2 = "select against_code, against_text from Complaint_against" & _
                " where against_branch_no = " & branch_code
                Dim against_ds As DataSet = get_dataset("Complaints", param2)
                For Each againstRow In against_ds.Tables(0).Rows
                    comp_against_codeComboBox.Items.Add(againstRow(1))
                    comp_against2_codecombobox.Items.Add(againstRow(1))
                    If againstRow(0) = against_code Then
                        comp_against_codeComboBox.Text = againstRow(1)
                    End If
                    If againstRow(0) = against2_code Then
                        comp_against2_codecombobox.Text = againstRow(1)
                    End If
                Next
            Next

            'against code
            'orig_against_code = against_code
            'param2 = "select against_text from complaint_against where against_code = " & against_code
            'Dim against_ds As DataSet = get_dataset("Complaints", param2)
            'against_text = against_ds.Tables(0).Rows(0).Item(0)
            ''Me.Complaint_againstTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaint_against, against_code)
            ''Try
            '' against_text = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(0).Item(1)
            ''Catch ex As Exception
            ''   against_text = ""
            ''End Try

            'orig_against_text = against_text
            'param2 = "select against_text from Complaint_against where against_branch_no = " & branch_no
            'Dim against_branch_ds As DataSet = get_dataset("Complaints", param2)
            ''Me.Complaint_againstTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Complaint_against, branch_no)
            '
            'idx = 0
            ''For Each row In PraiseAndComplaintsSQLDataSet.Complaint_against.Rows
            'For Each row In against_branch_ds.Tables(0).Rows
            '    'comp_against_codeComboBox.Items.Add(PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(idx).Item(1))
            '    comp_against_codeComboBox.Items.Add(against_branch_ds.Tables(0).Rows(idx).Item(0))
            '    If against_text = against_branch_ds.Tables(0).Rows(idx).Item(0) Then
            '        comp_against_codeComboBox.SelectedIndex = idx
            '        comp_against_codeComboBox.Text = against_text
            '    End If
            '    idx += 1
            'Next

            'against2 code
            'orig_against2_code = against2_code
            'If against2_code > 0 Then
            '    param2 = "select against_text from Complaint_against where against_code = " & against2_code
            '    Dim against2_ds As DataSet = get_dataset("Complaints", param2)
            '    against2_text = against2_ds.Tables(0).Rows(0).Item(0)
            '    'Me.Complaint_againstTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaint_against, against2_code)
            '    'Try
            '    ' against2_text = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(0).Item(1)
            '    'Catch ex As Exception
            '    'against2_text = ""
            '    'End Try
            'Else
            '    against2_text = ""
            'End If

            'param2 = "select against_text from complaint_against where against_branch_no = " & branch_no
            'Dim against2_branch_ds As DataSet = get_dataset("Complaints", param2)
            ''Me.Complaint_againstTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Complaint_against, branch_no)
            'comp_against2_codecombobox.Items.Clear()
            'idx = 0
            ''For Each row In PraiseAndComplaintsSQLDataSet.Complaint_against.Rows
            'For Each row In against2_branch_ds.Tables(0).Rows
            '    comp_against2_codecombobox.Items.Add(against2_branch_ds.Tables(0).Rows(idx).Item(0))
            '    'comp_against2_codecombobox.Items.Add(PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(idx).Item(1))
            '    If against2_text = against2_branch_ds.Tables(0).Rows(idx).Item(0) Then
            '        comp_against2_codecombobox.SelectedIndex = idx
            '        comp_against2_codecombobox.Text = against2_text
            '    End If
            '    idx += 1
            'Next
            orig_against_code = against_code
            orig_against2_text = against2_text
            orig_against2_code = against2_code

            agentComboBox.Items.Clear()
            populate_agentcombo(comp_cmpny_no)

            agent2_combobox.Items.Clear()
            populate_agent2combo(comp_cmpny_no)

            'investigating officer
            param2 = "select inv_text from investigators where inv_code = " & alloc_to_code
            Dim inv_ds As DataSet = get_dataset("Complaints", param2)
            inv_text = inv_ds.Tables(0).Rows(0).Item(0)
            'Me.InvestigatorsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Investigators, alloc_to_code)
            'inv_text = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
            orig_inv_text = inv_text

            'Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
            param2 = "select inv_text from investigators where inv_deleted = 0 order by inv_text"
            Dim inv2_ds As DataSet = get_dataset("Complaints", param2)
            invComboBox.Items.Clear()
            idx = 0
            'For Each row In PraiseAndComplaintsSQLDataSet.Investigators.Rows
            For Each row In inv2_ds.Tables(0).Rows
                'invComboBox.Items.Add(PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(1))
                invComboBox.Items.Add(inv2_ds.Tables(0).Rows(idx).Item(0))
                idx += 1
            Next
            invComboBox.Text = inv_text

            'action
            If action_code <> 0 Then
                param2 = "select action_name from actions where action_code = " & action_code
                Dim action_ds As DataSet = get_dataset("Complaints", param2)
                'Me.ActionsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Actions, action_code)
                Try
                    'action_name = PraiseAndComplaintsSQLDataSet.Actions.Rows(0).Item(1)
                    action_name = action_ds.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    action_name = "N/A"
                End Try
                orig_action_text = action_name
            End If
            param2 = "select action_name from actions order by action_name"
            Dim action2_ds As DataSet = get_dataset("Complaints", param2)
            'Me.ActionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Actions)
            actions_ComboBox.Items.Clear()
            idx = 0
            'For Each row In PraiseAndComplaintsSQLDataSet.Actions.Rows
            For Each row In action2_ds.Tables(0).Rows
                'actions_ComboBox.Items.Add(PraiseAndComplaintsSQLDataSet.Actions.Rows(idx).Item(1))
                actions_ComboBox.Items.Add(action2_ds.Tables(0).Rows(idx).Item(0))
                idx += 1
            Next
            If action_code <> 0 Then
                actions_ComboBox.Text = action_name
            Else
                actions_ComboBox.Text = "N/A"
                actions_ComboBox.SelectedItem = -1
            End If


            orig_completed = completed
            compbycombobox.Items.Clear()
            'Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
            param2 = "select inv_text from investigators where inv_deleted = 0 order by inv_text"
            Dim inv3_ds As DataSet = get_dataset("Complaints", param2)

            Dim row2 As DataRow
            idx = 0
            'For Each row2 In PraiseAndComplaintsSQLDataSet.Investigators.Rows
            For Each row2 In inv3_ds.Tables(0).Rows
                'compbycombobox.Items.Add(PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(1))
                compbycombobox.Items.Add(inv3_ds.Tables(0).Rows(idx).Item(0))
                idx += 1
            Next
            If completed Then
                compdatetimepicker.Value = completed_date
                orig_comp_date = completed_date
                compdatetimepicker.Visible = True
                comprbtn.Checked = True
                'compby_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(15)
                'Me.InvestigatorsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Investigators, compby_code)
                param2 = "select inv_text from investigators where inv_code = " & compby_code
                Dim inv4_ds As DataSet = get_dataset("Complaints", param2)
                'compbycombobox.Text = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
                compbycombobox.Text = inv4_ds.Tables(0).Rows(0).Item(0)
                orig_compby = compbycombobox.Text
                compbycombobox.Visible = True
            Else
                notcomprbtn.Checked = True
                compdatetimepicker.Visible = False
                compbycombobox.Visible = False
                compbycombobox.SelectedIndex = -1
            End If

            'founded/unfounded
            new_founded = ""
            part_founded_cbox.Visible = False
            Select Case orig_founded
                Case "Y"
                    foundedrbtn.Checked = True
                Case "N"
                    unfoundedrbtn.Checked = True
                Case "U"
                    openrbtn.Checked = True
                Case "P"
                    part_founded_rbtn.Checked = True
                    part_founded_cbox.Visible = True
                    populate_pf_bailiff_table()
                    part_founded_cbox.Items.Clear()
                    For idx = 1 To pf_agent_rows
                        part_founded_cbox.Items.Add(pf_bailiff_table(idx, 2))
                        If orig_pf_agent_no = pf_bailiff_table(idx, 1) Then
                            part_founded_cbox.Text = pf_bailiff_table(idx, 2)
                        End If
                    Next
                    part_founded_cbox.Items.Add("Various")
                    If orig_pf_agent_no = 9999 Then
                        part_founded_cbox.Text = "Various"
                    End If
            End Select

            cl_ComboBox.Items.Clear()
            If case_no = 0 Then
                populate_client_table(comp_cmpny_no)
                For idx = 1 To cl_rows
                    cl_ComboBox.Items.Add(client_table(idx, 2))
                    If cl_no = client_table(idx, 1) Then
                        cl_name = client_table(idx, 2)
                    End If
                Next
            Else
                If comp_cmpny_no = 0 Then
                    param2 = "select name from Client" & _
                    " where _rowid = " & cl_no
                    Dim cl_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Error reading client " & cl_no)
                        Exit Sub
                    End If
                    cl_name = Trim(cl_dataset.Tables(0).Rows(0).Item(0))
                    cl_ComboBox.Items.Add(cl_name)
                    cl_ComboBox.SelectedItem = cl_name
                Else
                    param2 = "select clnt_name from Complaint_Clients where clnt_code = " & cl_no
                    Dim cl_dataset As DataSet = get_dataset("Complaints", param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Error reading client " & cl_no)
                    Else
                        cl_name = Trim(cl_dataset.Tables(0).Rows(0).Item(0))
                        cl_ComboBox.Items.Add(cl_name)
                        cl_ComboBox.SelectedItem = cl_name
                    End If

                End If
            End If

            If cl_no = 0 Then
                cl_ComboBox.SelectedItem = -1
            Else
                cl_ComboBox.SelectedItem = cl_name
            End If

            'no of documents
            doc_textbox.Text = populate_document_combobox_upd()
            orig_no_docs = doc_textbox.Text
            If doc_textbox.Text = 0 Then
                doc_ListBox.Visible = False
                deldocbtn.Enabled = False
            Else
                doc_ListBox.Visible = True
                deldocbtn.Enabled = True
            End If

            'check if stage 1 acknowledgement letter has been sent
            If ack_date <> CDate("1800,1,1") Then
                ack_datepicker.Visible = True
                ack_cbox.Checked = True
                ack_datepicker.Value = ack_date
            Else
                ack_datepicker.Visible = False
                ack_cbox.Checked = False
                If case_no > 0 Then
                    If comp_cmpny_no = 0 Then
                        param1 = "onestep"
                        param2 = "select text, _createdDate from Note" & _
                        " where debtorID = " & case_no & " and type = 'Letter'" & _
                        " order by _createdDate"
                        Dim note_dataset As DataSet = get_dataset(param1, param2)
                        For idx = 0 To no_of_rows - 1
                            Dim created_date As Date = note_dataset.Tables(0).Rows(idx).Item(1)
                            Dim note_text As String = note_dataset.Tables(0).Rows(idx).Item(0)
                            If InStr(note_text, "Complaint_Ack") > 0 And _
                            created_date >= CDate(Comp_dateTextBox.Text) And _
                            InStr(note_text, "Stage") = 0 Then
                                ack_datepicker.Text = FormatDateTime(note_dataset.Tables(0).Rows(idx).Item(1), DateFormat.ShortDate)
                                ack_datepicker.Visible = True
                                ack_cbox.Checked = True
                                ack_date = ack_datepicker.Value
                            End If
                        Next
                    End If
                   
                End If
            End If
            orig_ack_date = ack_date

            'check if stage 1 holding letter has been sent
            If holding_date <> CDate("1800,1,1") Then
                holding_datepicker.Visible = True
                holding_cbox.Checked = True
                holding_datepicker.Value = holding_date
            Else
                holding_datepicker.Visible = False
                holding_cbox.Checked = False
                If case_no > 0 Then
                    If check_held_letter(case_no, Comp_dateTextBox.Text) = "Y" Then
                        If stage_no < 2 Then
                            holding_datepicker.Text = FormatDateTime(parm_hold_date, DateFormat.ShortDate)
                            holding_datepicker.Visible = True
                            holding_cbox.Checked = True
                            holding_date = holding_datepicker.Value
                        ElseIf stage2_start_date <> CDate("1800,1,1") Then
                            If parm_hold_date < stage2_start_date Then
                                holding_datepicker.Text = FormatDateTime(parm_hold_date, DateFormat.ShortDate)
                                holding_datepicker.Visible = True
                                holding_cbox.Checked = True
                                holding_date = holding_datepicker.Value
                            End If
                        End If

                    End If
                End If
            End If

            orig_holding_date = holding_date

            'check if stage 2 acknowledgement letter has been sent
            If stage2_ack_date <> CDate("1800,1,1") Then
                stage2_ack_datepicker.Visible = True
                stage2_ack_cbox.Checked = True
                stage2_ack_datepicker.Value = stage2_ack_date
            Else
                stage2_ack_datepicker.Visible = False
                stage2_ack_cbox.Checked = False
                If case_no > 0 Then
                    If comp_cmpny_no = 0 Then
                        param1 = "onestep"
                        param2 = "select text, _createdDate from Note" & _
                        " where debtorID = " & case_no & " and type = 'Letter'" & _
                        " order by _createdDate"
                        Dim compare_date As Date
                        If stage2_start_date <> Nothing Then
                            compare_date = stage2_start_date
                        Else
                            compare_date = CDate(Comp_dateTextBox.Text)
                        End If
                        Dim note_dataset As DataSet = get_dataset(param1, param2)
                        For idx = 0 To no_of_rows - 1
                            Dim created_date As Date = note_dataset.Tables(0).Rows(idx).Item(1)
                            Dim note_text As String = note_dataset.Tables(0).Rows(idx).Item(0)
                            If InStr(note_text, "Complaint_Ack") > 0 And _
                            created_date >= compare_date And _
                            InStr(note_text, "Stage 2") > 0 Then
                                stage2_ack_datepicker.Text = FormatDateTime(note_dataset.Tables(0).Rows(idx).Item(1), DateFormat.ShortDate)
                                stage2_ack_datepicker.Visible = True
                                stage2_ack_cbox.Checked = True
                                stage2_ack_date = stage2_ack_datepicker.Value
                            End If
                        Next
                    End If
                   
                End If
            End If
            orig_stage2_ack_date = stage2_ack_date

            'check if stage 2 holding letter has been sent
            If stage2_holding_date <> CDate("1800,1,1") Then
                stage2_holding_datepicker.Visible = True
                stage2_holding_cbox.Checked = True
                stage2_holding_datepicker.Value = stage2_holding_date
            Else
                stage2_holding_datepicker.Visible = False
                stage2_holding_cbox.Checked = False
                If case_no > 0 And stage2_start_date <> Nothing Then
                    If check_held_letter(case_no, stage2_start_date) = "Y" Then
                        stage2_holding_datepicker.Text = FormatDateTime(parm_hold_date, DateFormat.ShortDate)
                        stage2_holding_datepicker.Visible = True
                        stage2_holding_cbox.Checked = True
                    End If
                End If
            End If

            orig_stage2_holding_date = stage2_holding_date

            'stage
            stage_ComboBox.Text = "Stage " & stage_no

            'corrective action
            If cor_code > 0 Then
                'Me.Corrective_actionsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Corrective_actions, cor_code)
                'cor_name = PraiseAndComplaintsSQLDataSet.Corrective_actions.Rows(0).Item(1)
                param2 = "select cor_name from [Corrective actions] where cor_code = " & cor_code
                Dim cor_ds As DataSet = get_dataset("Complaints", param2)
                cor_name = cor_ds.Tables(0).Rows(0).Item(0)
                orig_cor_text = cor_name
            End If
            'Me.Corrective_actionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Corrective_actions)
            param2 = "select cor_name from [Corrective actions] order by cor_code"
            Dim cor2_ds As DataSet = get_dataset("Complaints", param2)
            cor_nameComboBox.Items.Clear()
            idx = 0
            'For Each row In PraiseAndComplaintsSQLDataSet.Corrective_actions.Rows
            For Each row In cor2_ds.Tables(0).Rows
                'cor_nameComboBox.Items.Add(PraiseAndComplaintsSQLDataSet.Corrective_actions.Rows(idx).Item(1))
                cor_nameComboBox.Items.Add(cor2_ds.Tables(0).Rows(idx).Item(0))
                idx += 1
            Next
            If cor_code = 0 Then
                cor_nameComboBox.Text = "N/A"
                cor_nameComboBox.SelectedItem = -1
            Else
                cor_nameComboBox.Text = cor_name
            End If

            'hold reason
            If holding_cbox.Checked = False Then
                hold_name_ComboBox.Visible = False
                reason_label.Visible = False
                hold_code = 1
                orig_hold_name = "N/A"
            Else
                reason_label.Visible = True
                hold_name_ComboBox.Visible = True
                '5.3.14
                'Me.Hold_reasonTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Hold_reason)
                param2 = "select hold_code, hold_name from Hold_reason order by hold_code"
                Dim hold_ds As DataSet = get_dataset("Complaints", param2)

                hold_name_ComboBox.Items.Clear()
                hold_name_ComboBox.Text = ""
                idx = 0
                'For Each row In PraiseAndComplaintsSQLDataSet.Hold_reason.Rows
                For Each row In hold_ds.Tables(0).Rows
                    'hold_name_ComboBox.Items.Add(PraiseAndComplaintsSQLDataSet.Hold_reason.Rows(idx).Item(1))
                    hold_name_ComboBox.Items.Add(hold_ds.Tables(0).Rows(idx).Item(1))
                    If hold_ds.Tables(0).Rows(idx).Item(0) = hold_code Then
                        hold_name_ComboBox.Text = hold_ds.Tables(0).Rows(idx).Item(1)
                    End If
                    idx += 1
                Next
                'Me.Hold_reasonTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Hold_reason, hold_code)
                'Try
                '    hold_name_ComboBox.Text = Me.PraiseAndComplaintsSQLDataSet.Hold_reason.Rows(0).Item(1)
                'Catch ex As Exception
                '    hold_name_ComboBox.Text = ""
                'End Try

                orig_hold_name = hold_name_ComboBox.Text

            End If
            hold_name = orig_hold_name
            orig_hold_code = hold_code

            'priority
            Comp_priorityCheckBox.Checked = orig_priority

            'category
            category_cbox.SelectedIndex = category

            'if complaint is completed or was previously completed can only change response,stage and priority
            'can now also change case number
            'prev completed setting now moved above
            'Dim prev_completed As Boolean = True

            'Dim comp_date As Date
            'Try
            '    comp_date = CDate(PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(14))
            'Catch
            '    prev_completed = False
            'End Try

            'stage > 1 can now update details box
            If prev_completed = True Then
                branch_cbox.Enabled = False
                recpt_combobox.Enabled = False
                recvd_textComboBox.Enabled = False
                'comp_case_noTextBox.Enabled = False
                cl_ComboBox.Enabled = False
                'comp_against_codeComboBox.Enabled = False
                'agentComboBox.Enabled = False
                'agent2_combobox.Enabled = False
                'comp_against2_codecombobox.Enabled = False
                'cat_codeComboBox.Enabled = False
                'Cat_textComboBox.Enabled = False
                If stage_no < 2 Then
                    Comp_textTextBox.Enabled = False
                Else
                    Comp_textTextBox.Enabled = True
                End If
            Else
                branch_cbox.Enabled = True
                recpt_combobox.Enabled = True
                recvd_textComboBox.Enabled = True
                comp_case_noTextBox.Enabled = True
                cl_ComboBox.Enabled = True
                comp_against_codeComboBox.Enabled = True
                agentComboBox.Enabled = True
                agent2_combobox.Enabled = True
                comp_against2_codecombobox.Enabled = True
                cat_codeComboBox.Enabled = True
                Cat_textComboBox.Enabled = True
                Comp_textTextBox.Enabled = True
            End If
            If prev_completed = True And completed = False Then
                prevcomplbl.Visible = True
            Else
                prevcomplbl.Visible = False
            End If

            'old comp no
            old_comp_notextbox.Text = orig_old_comp_no

            'costs cancelled
            If costs_cancel = 0 Then
                costs_cancel_tbox.Text = " "
                costs_reasonbtn.Visible = False
            Else
                costs_cancel_tbox.Text = Format(costs_cancel, "�#0.00")
                costs_reasonbtn.Visible = True
            End If

            'compensation
            If compensation = 0 Then
                compensation_tbox.Text = " "
            Else
                compensation_tbox.Text = Format(compensation, "�#0.00")
            End If

            'response forgotten
            If orig_resp_forgotten = 0 Then
                respcbox.Checked = False
            Else
                respcbox.Checked = True
            End If

            'complaint escalation
            '6.3.14
            param2 = "select compesc_reason from Complaint_Escalation where compesc_comp_no = " & comp_no & _
            " and compesc_stage_no = 1"
            Dim compesc_ds As DataSet = get_dataset("Complaints", param2)
            If no_of_rows > 0 Then
                stage1_esc_code = compesc_ds.Tables(0).Rows(0).Item(0)
            Else
                stage1_esc_code = 0
            End If
            'Me.Complaint_EscalationTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaint_Escalation, comp_no, 1)
            'If Me.PraiseAndComplaintsSQLDataSet.Complaint_Escalation.Rows.Count > 0 Then
            '    stage1_esc_code = Me.PraiseAndComplaintsSQLDataSet.Complaint_Escalation.Rows(0).Item(2)
            'Else
            '    stage1_esc_code = 0
            'End If
            orig_stage1_esc_code = stage1_esc_code
            param2 = "select compesc_reason from Complaint_Escalation where compesc_comp_no = " & comp_no & _
                        " and compesc_stage_no = 2"
            Dim compesc2_ds As DataSet = get_dataset("Complaints", param2)
            If no_of_rows > 0 Then
                stage2_esc_code = compesc2_ds.Tables(0).Rows(0).Item(0)
            Else
                stage2_esc_code = 0
            End If
            'Me.Complaint_EscalationTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaint_Escalation, comp_no, 2)
            'If Me.PraiseAndComplaintsSQLDataSet.Complaint_Escalation.Rows.Count > 0 Then
            '    stage2_esc_code = Me.PraiseAndComplaintsSQLDataSet.Complaint_Escalation.Rows(0).Item(2)
            'Else
            '    stage2_esc_code = 0
            'End If
            orig_stage2_esc_code = stage2_esc_code

            'fill stage 1 and stage2  escalation combobx with values from table
            stage1_esc_combobox.Items.Clear()
            stage2_esc_combobox.Items.Clear()
            param2 = "select esc_code, esc_name from Stage_Escalation order by esc_code"
            Dim esc_ds As DataSet = get_dataset("Complaints", param2)

            'For idx = 0 To Me.PraiseAndComplaintsSQLDataSet.Stage_Escalation.Rows.Count - 1
            For idx = 0 To esc_ds.Tables(0).Rows.Count - 1
                'stage1_esc_combobox.Items.Add(Me.PraiseAndComplaintsSQLDataSet.Stage_Escalation.Rows(idx).Item(1))
                stage1_esc_combobox.Items.Add(esc_ds.Tables(0).Rows(idx).Item(1))
                'stage2_esc_combobox.Items.Add(Me.PraiseAndComplaintsSQLDataSet.Stage_Escalation.Rows(idx).Item(1))
                stage2_esc_combobox.Items.Add(esc_ds.Tables(0).Rows(idx).Item(1))
                'If stage1_esc_code = Me.PraiseAndComplaintsSQLDataSet.Stage_Escalation.Rows(idx).Item(0) Then
                If stage1_esc_code = esc_ds.Tables(0).Rows(idx).Item(0) Then
                    'stage1_esc_combobox.Text = Me.PraiseAndComplaintsSQLDataSet.Stage_Escalation.Rows(idx).Item(1)
                    stage1_esc_combobox.Text = esc_ds.Tables(0).Rows(idx).Item(1)
                End If
                'If stage2_esc_code = Me.PraiseAndComplaintsSQLDataSet.Stage_Escalation.Rows(idx).Item(0) Then
                If stage2_esc_code = esc_ds.Tables(0).Rows(idx).Item(0) Then
                    'stage2_esc_combobox.Text = Me.PraiseAndComplaintsSQLDataSet.Stage_Escalation.Rows(idx).Item(1)
                    stage2_esc_combobox.Text = esc_ds.Tables(0).Rows(idx).Item(1)
                End If
            Next

            'stage 2 group box
            Dim idx2 As Integer
            If stage_no > 1 Then
                stage2_start_datepicker.Visible = True
                stage2_start_datepicker.Text = stage2_start_date
                stage2_completed_datepicker.Visible = True
                stage2_completed_by_combobox.Visible = True
                Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
                Dim row As DataRow
                stage2_completed_by_combobox.Items.Clear()
                For Each row In Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows
                    stage2_completed_by_combobox.Items.Add(PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx2).Item(1))
                    If Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx2).Item(0) = stage2_completed_by Then
                        stage2_completed_by_combobox.Text = Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx2).Item(1)
                    End If
                    idx2 += 1
                Next
                If stage2_completed_by > 0 Then
                    stage2_completed_datepicker.Text = stage2_completed_date
                Else
                    stage2_completed_datepicker.Visible = False
                    stage2_completed_by_combobox.SelectedItem = -1
                End If
            Else
                stage2_start_datepicker.Visible = False
                stage2_completed_datepicker.Visible = False
                stage2_completed_by_combobox.Visible = False
                stage2_start_date = Nothing
                stage2_completed_date = Nothing
                stage2_completed_by = 0
            End If

            'stage 3 group box

            If stage_no > 2 Then
                idx2 = 0
                stage3_start_datepicker.Visible = True
                stage3_start_datepicker.Text = stage3_start_date
                stage3_completed_datepicker.Visible = True
                stage3_completed_by_combobox.Visible = True
                Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
                Dim row As DataRow

                stage3_completed_by_combobox.Items.Clear()
                For Each row In Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows
                    stage3_completed_by_combobox.Items.Add(PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx2).Item(1))
                    If Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx2).Item(0) = stage3_completed_by Then
                        stage3_completed_by_combobox.Text = Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx2).Item(1)
                    End If
                    idx2 += 1
                Next
                If stage3_completed_by > 0 Then
                    stage3_completed_datepicker.Text = stage3_completed_date
                Else
                    stage3_completed_datepicker.Visible = False
                    stage3_completed_by_combobox.SelectedItem = -1
                End If
            Else
                stage3_start_datepicker.Visible = False
                stage3_completed_datepicker.Visible = False
                stage3_completed_by_combobox.Visible = False
                stage3_start_date = Nothing
                stage3_completed_date = Nothing
                stage3_completed_by = 0
            End If

            'legal group box
            If legal_flag = "N" Then
                legalcbox.Checked = False
                legalgbox.Visible = False
                upd_solicitor_datepicker.Value = Now
            Else
                legalcbox.Checked = True
                legalgbox.Visible = True
                If referred_to_solicitor = CDate("1800,1,1") Then
                    solicitor_cbox.Checked = False
                    upd_solicitor_datepicker.Visible = False
                    upd_solicitor_datepicker.Value = Now
                Else
                    solicitor_cbox.Checked = True
                    solicitor_cbox.Visible = True
                    upd_solicitor_datepicker.Visible = True
                    upd_solicitor_datepicker.Value = referred_to_solicitor
                End If
            End If

            'insurance
            If insurance_type_no >= 0 Then
                upd_inscbox.Checked = True
                upd_insurercbox.Visible = True
                If referred_to_insurer = CDate("1800,1,1") Then
                    'upd_insurercbox.Checked = False
                    upd_ins_datepicker.Visible = False
                    upd_ins_datepicker.Value = Now
                Else
                    upd_insurercbox.Checked = True
                    upd_ins_datepicker.Visible = True
                    upd_ins_datepicker.Value = referred_to_insurer
                End If
                ins_panel.Visible = True

                upd_liability_cbox.Visible = True
                upd_liability_cbox.Checked = False
                If liability_flag = "Y" Then
                    upd_liability_cbox.Checked = True
                End If
                If prof_indemnity_no = 1 Then
                    pi_cbox.Checked = True
                Else
                    pi_cbox.Checked = False
                End If
                If public_liability_no = 1 Then
                    pl_cbox.Checked = True
                Else
                    pl_cbox.Checked = False
                End If
            Else
                upd_inscbox.Checked = False
                upd_insurercbox.Checked = False
                upd_insurercbox.Visible = False
                upd_ins_datepicker.Visible = False
                upd_ins_datepicker.Value = Now
                upd_liability_cbox.Visible = False
                ins_panel.Visible = False
                pi_cbox.Visible = False
                pl_cbox.Visible = False
                pi_cbox.Checked = False
                pl_cbox.Checked = False
                upd_unknownrbtn.Checked = True
            End If

            Select Case orig_insurance_type_no
                Case 0
                    upd_unknownrbtn.Checked = True
                Case 1
                    upd_act_not_rbtn.Checked = True
                Case 2
                    upd_bord_rbtn.Checked = True
            End Select
            insurance_type_no = orig_insurance_type_no
            If monetary_risk = -999 Then
                monetary_risk_tbox.Text = ""
            Else
                monetary_risk_tbox.Text = Format(monetary_risk, "�#0.00")
            End If

            'feedback
            feedback_cbox.Items.Clear()
            idx2 = 0
            param2 = "select feedback_code, feedback_name from complaints_feedback " & _
            " order by feedback_code"
            Dim feed_ds As DataSet = get_dataset("complaints", param2)
            For Each row In feed_ds.Tables(0).Rows
                feedback_cbox.Items.Add(row(1))
                If feedback_no = row(0) Then
                    feedback_cbox.Text = row(1)
                End If
            Next
            'Me.Complaints_feedbackTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaints_feedback)
            'For Each row In Me.PraiseAndComplaintsSQLDataSet.Complaints_feedback.Rows
            '    feedback_cbox.Items.Add(Me.PraiseAndComplaintsSQLDataSet.Complaints_feedback.Rows(idx2).Item(1))
            '    If feedback_no = idx2 Then
            '        feedback_cbox.Text = Me.PraiseAndComplaintsSQLDataSet.Complaints_feedback.Rows(idx2).Item(1)
            '    End If
            '    idx2 += 1
            'Next

            'type code
            type_code = orig_type_code
            upd_type_code_cbox.Items.Clear()
            If (cat_code = "A" And cat_number = 9) Or _
            (cat_code = "B" And cat_number = 11) Or _
            (cat_code = "C" And cat_number = 8) Then
                param2 = "select type_code, type_name from Complaints_type_code order by type_code"
                Dim type_code_ds As DataSet = get_dataset("Complaints", param2)
                'For idx2 = 0 To Me.PraiseAndComplaintsSQLDataSet.Complaints_type_code.Rows.Count - 1
                For idx2 = 0 To type_code_ds.Tables(0).Rows.Count - 1
                    'upd_type_code_cbox.Items.Add(Me.PraiseAndComplaintsSQLDataSet.Complaints_type_code.Rows(idx2).Item(1))
                    upd_type_code_cbox.Items.Add(type_code_ds.Tables(0).Rows(idx2).Item(1))
                Next
                upd_type_code_cbox.SelectedIndex = type_code
            End If

            'set hold label
            hold_lbl.Text = ""
            param2 = "select hold_stage_no from Complaints_Holds where hold_comp_no = " & comp_no
            Dim holds_ds As DataSet = get_dataset("Complaints", param2)
            If no_of_rows > 0 Then
                Dim hold_stage_no As Integer = holds_ds.Tables(0).Rows(0).Item(0)
                hold_lbl.Text = "Stage " & hold_stage_no & " ON HOLD"
            End If
            'Me.Complaints_HoldsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Complaints_Holds, comp_no)
            'If Me.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows.Count > 0 Then
            '    'get stage hold still open
            '    Dim hold_stage_no As Integer = Me.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows(0).Item(1)
            '    hold_lbl.Text = "Stage " & hold_stage_no & " ON HOLD"
            'End If
            initial_load = False
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub populate_agentcombo(ByVal disp_cmpny_no As Integer)
        Try
            Dim idx As Integer
            dept_code = 0
            If disp_cmpny_no <> 0 Then
                param1 = "Complaints"
                param2 = "select agnt_code, agnt_name from Complaint_Agents" & _
                " order by agnt_code"
                Dim agnt_ds As DataSet = get_dataset(param1, param2)
                agent_rows = agnt_ds.Tables(0).Rows.Count - 1
                ReDim bailiff_table(no_of_rows, no_of_rows)
                For idx = 0 To agent_rows
                    agentComboBox.Items.Add(agnt_ds.Tables(0).Rows(idx).Item(1))
                    bailiff_table(idx, 1) = agnt_ds.Tables(0).Rows(idx).Item(0)
                    bailiff_table(idx, 2) = agnt_ds.Tables(0).Rows(idx).Item(1)
                    If bailiff_table(idx, 1) = agent_no Then
                        agentComboBox.Text = agnt_ds.Tables(0).Rows(idx).Item(1)
                    End If
                Next
            Else
                Dim where_clause As String
                If Microsoft.VisualBasic.Left(comp_against_codeComboBox.Text, 2) = "FC" Then
                    where_clause = " where agent_type = 'B' and internalExternal = 'I' and branchID <> 10"
                ElseIf comp_against_codeComboBox.Text = "Van Bailiff" Or _
                comp_against_codeComboBox.Text = "VB" Then
                    where_clause = " where agent_type = 'B' and internalExternal = 'E' and branchID <> 10"
                ElseIf comp_against_codeComboBox.Text = "High Court" Then
                    where_clause = " where agent_type = 'B' and (internalExternal = 'E' or internalExternal = 'I') and branchID <> 10"
                ElseIf comp_against_codeComboBox.Text = "Marston" Then
                    where_clause = " where agent_type ='B' "
                ElseIf comp_against_codeComboBox.Text = "Contact Centre" Then
                    where_clause = " where (agent_type ='P' or agent_type = 'A') "
                Else
                    where_clause = " where (agent_type ='P' or agent_type = 'A')  and branchID <> 10"
                End If
                If comp_against_codeComboBox.Text = "Enforcement Officer" Then
                    where_clause = " where agent_type = 'B' and branchID <> 10"
                End If
                populate_bailiff_table(where_clause)

                For idx = 1 To agent_rows
                    agentComboBox.Items.Add(bailiff_table(idx, 2))
                    If agent_no = bailiff_table(idx, 1) Then
                        agentComboBox.Text = bailiff_table(idx, 2)
                    End If
                Next
                agentComboBox.Items.Add("Various")
                If agent_no = 9999 Then
                    agentComboBox.Text = "Various"
                End If
                If comp_against_codeComboBox.Text.Length = 0 Then
                    If agent_no <> 0 Then
                        comp_against_codeComboBox.Items.Add("Unknown")
                        comp_against_codeComboBox.Text = "Unknown"
                        'get agent name 
                        param2 = "select name_sur, name_fore from Bailiff where _rowid = " & agent_no
                        Dim agent_ds As DataSet = get_dataset("onestep", param2)
                        If no_of_rows > 0 Then
                            Dim agent_name As String = agent_ds.Tables(0).Rows(0).Item(0)
                            Try
                                agent_name &= ", " & agent_ds.Tables(0).Rows(0).Item(1)
                            Catch ex As Exception

                            End Try
                            agentComboBox.Items.Add(agent_name)
                            agentComboBox.Text = agent_name
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub
    Private Sub populate_agent2combo(ByVal disp_cmpny_no As Integer)

        Try
            Dim idx As Integer
            If disp_cmpny_no <> 0 Then
                param1 = "Complaints"
                param2 = "select agnt_code, agnt_name from Complaint_Agents" & _
                " order by agnt_code"
                Dim agnt_ds As DataSet = get_dataset(param1, param2)
                agent2_rows = agnt_ds.Tables(0).Rows.Count - 1
                ReDim bailiff2_table(no_of_rows, no_of_rows)
                For idx = 0 To agent2_rows
                    agent2_combobox.Items.Add(agnt_ds.Tables(0).Rows(idx).Item(1))
                    bailiff2_table(idx, 1) = agnt_ds.Tables(0).Rows(idx).Item(0)
                    bailiff2_table(idx, 2) = agnt_ds.Tables(0).Rows(idx).Item(1)
                    If bailiff2_table(idx, 1) = agent2_no Then
                        agent2_combobox.Text = agnt_ds.Tables(0).Rows(idx).Item(1)
                    End If
                Next
            Else
                If agent2_no = 0 Then
                    Exit Sub
                End If
                Dim where_clause As String
                If Microsoft.VisualBasic.Left(comp_against2_codecombobox.Text, 2) = "FC" Then
                    where_clause = " where agent_type = 'B' and internalExternal = 'I' and branchID <> 10"
                ElseIf comp_against2_codecombobox.Text = "Van Bailiff" Or _
                comp_against2_codecombobox.Text = "VB" Then
                    where_clause = " where agent_type = 'B' and internalExternal = 'E' and branchID <> 10"
                ElseIf comp_against2_codecombobox.Text = "High Court" Then
                    where_clause = " where agent_type = 'B' and (internalExternal = 'I' or internalExternal = 'E') and branchID <> 10"
                ElseIf comp_against2_codecombobox.Text = "Marston" Then
                    where_clause = " where agent_type = 'B' "
                ElseIf comp_against2_codecombobox.Text = "Contact Centre" Then
                    where_clause = " where (agent_type ='P' or agent_type = 'A') "
                Else
                    where_clause = " where (agent_type ='P' or agent_type = 'A') and branchID <> 10"
                End If
                If comp_against2_codecombobox.Text = "Enforcement Officer" Then
                    where_clause = " where agent_type = 'B' and branchID <> 10"
                End If
                populate_bailiff2_table(where_clause)

                For idx = 1 To agent2_rows
                    agent2_combobox.Items.Add(bailiff2_table(idx, 2))
                    If agent2_no = bailiff2_table(idx, 1) Then
                        agent2_combobox.Text = bailiff2_table(idx, 2)
                    End If
                Next
                agent2_combobox.Items.Add("Various")
                If agent2_no = 9999 Then
                    agent2_combobox.Text = "Various"
                End If
                If comp_against2_codecombobox.Text.Length = 0 Then
                    If agent2_no <> 0 Then
                        comp_against2_codecombobox.Items.Add("Unknown")
                        comp_against2_codecombobox.Text = "Unknown"
                        'get agent2 name 
                        param2 = "select name_sur, name_fore from Bailiff where _rowid = " & agent2_no
                        Dim agent2_ds As DataSet = get_dataset("onestep", param2)
                        If no_of_rows > 0 Then
                            Dim agent2_name As String = agent2_ds.Tables(0).Rows(0).Item(0)
                            Try
                                agent2_name &= ", " & agent2_ds.Tables(0).Rows(0).Item(1)
                            Catch ex As Exception

                            End Try
                            agent2_combobox.Items.Add(agent2_name)
                            agent2_combobox.Text = agent2_name
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub
    Private Sub populate_departments()
        'Me.DepartmentsTableAdapter.Fill(Me.DepartmentsDataSet.Departments)
        'agentComboBox.Items.Clear()
        'Dim idx As Integer = 0
        'Dim row As DataRow
        'dept_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(10)
        'For Each row In DepartmentsDataSet.Tables(0).Rows
        '    agentComboBox.Items.Add(DepartmentsDataSet.Tables(0).Rows(idx).Item(1))
        '    If DepartmentsDataSet.Tables(0).Rows(idx).Item(0) = dept_code _
        '    And orig_against_code > 0 Then
        '        agentComboBox.Text = DepartmentsDataSet.Tables(0).Rows(idx).Item(1)
        '    End If
        '    idx += 1
        'Next
        'dept_rows = idx
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Comp_case_noTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Cat_textComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cat_textComboBox.SelectedIndexChanged
        If Len(Cat_textComboBox.SelectedItem) = 0 Then
            cat_number = 0
        Else
            'orig_cat_code = Microsoft.VisualBasic.Left(cat_codeComboBox.SelectedItem, 1)
            'cat_number is number before the space
            If Mid(Cat_textComboBox.SelectedItem, 2, 1) = " " Then
                cat_number = Trim(Microsoft.VisualBasic.Left(Cat_textComboBox.SelectedItem, 2))
            Else
                cat_number = Trim(Microsoft.VisualBasic.Left(Cat_textComboBox.SelectedItem, 3))
            End If
        End If
        'If orig_cat_number <> cat_number And _
        '(cat_number = 99 Or orig_cat_number = 99) Then
        '    MsgBox("Stage will be reset")
        '    If cat_number = 99 Then
        '        stage_Label.Visible = False
        '        stage_ComboBox.Visible = False
        '        stage_no = 0
        '    Else
        '        stage_Label.Visible = True
        '        stage_ComboBox.Visible = True
        '        stage_ComboBox.Text = "Stage 1"
        '        stage_no = 1
        '    End If
        'End If
        upd_type_code_cbox.Items.Clear()
        type_code = 0
        If (cat_code = "A" And cat_number = 9) Or _
        (cat_code = "B" And cat_number = 11) Or _
        (cat_code = "C" And cat_number = 8) Then
            Dim idx As Integer
            param2 = "select type_code, type_name from Complaints_type_code order by type_code"
            Dim type_code_ds As DataSet = get_dataset("Complaints", param2)
            'For idx = 0 To Me.PraiseAndComplaintsSQLDataSet.Complaints_type_code.Rows.Count - 1
            For idx = 0 To type_code_ds.Tables(0).Rows.Count - 1
                'upd_type_code_cbox.Items.Add(Me.PraiseAndComplaintsSQLDataSet.Complaints_type_code.Rows(idx).Item(1))
                upd_type_code_cbox.Items.Add(type_code_ds.Tables(0).Rows(idx).Item(1))
            Next
        End If
    End Sub


    Private Sub Recvd_textComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub agentComboBox_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentComboBox.Enter

    End Sub

    Private Sub agentComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agentComboBox.SelectedIndexChanged

    End Sub

    Private Sub cl_ComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cl_ComboBox.SelectedIndexChanged

    End Sub

    Private Sub GroupBox1_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox1.Enter

    End Sub

    Private Sub GroupBox1_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupBox1.Validated

    End Sub

    Private Sub compdatetimepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles compdatetimepicker.Validating

        ErrorProvider1.SetError(compdatetimepicker, "")
        If compdatetimepicker.Value > Now Then
            ErrorProvider1.SetError(compdatetimepicker, "Completion Date can't be in the future")
            e.Cancel = True
        ElseIf compdatetimepicker.Value < Comp_dateTextBox.Text Then
            ErrorProvider1.SetError(compdatetimepicker, "Completion Date must be after date received")
            e.Cancel = True
        ElseIf stage_no > 1 And stage2_completed_by > 0 And compdatetimepicker.Value > stage2_completed_datepicker.Value Then
            ErrorProvider1.SetError(compdatetimepicker, "Completion date must not be after stage 2 completion date")
            e.Cancel = True
        End If
    End Sub

    Private Sub compdatetimepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles compdatetimepicker.ValueChanged

    End Sub

    Private Sub GroupBox3_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub


    Private Sub GroupBox2_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles GroupBox2.Validated
        If foundedrbtn.Checked = True Then
            new_founded = "Y"
        ElseIf unfoundedrbtn.Checked = True Then
            new_founded = "N"
        ElseIf foundedrbtn.Checked = True Then
            new_founded = "U"
        Else
            new_founded = "P"
        End If
    End Sub

    Private Sub compbytextbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub updbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updbtn.Click
        Try
            Dim change_made As Boolean = False
            Dim stage2_change_made As Boolean = False
            Dim stage2_completed_change_made As Boolean = False
            Dim stage3_change_made As Boolean = False
            Dim stage3_completed_change_made As Boolean = False
            Dim stage1_esc_change As Boolean = False
            Dim stage2_esc_change As Boolean = False

            ErrorProvider1.SetError(Comp_dateTextBox, "")
            ErrorProvider1.SetError(agentComboBox, "")
            ErrorProvider1.SetError(Cat_textComboBox, "")
            ErrorProvider1.SetError(comp_against_codeComboBox, "")
            ErrorProvider1.SetError(comp_against2_codecombobox, "")
            ErrorProvider1.SetError(updbtn, "")
            ErrorProvider1.SetError(actions_ComboBox, "")
            ErrorProvider1.SetError(GroupBox1, "")
            ErrorProvider1.SetError(GroupBox2, "")
            ErrorProvider1.SetError(compbycombobox, "")
            ErrorProvider1.SetError(Comp_textTextBox, "")
            ErrorProvider1.SetError(Comp_responseTextBox, "")
            ErrorProvider1.SetError(stage2_gbox, "")
            ErrorProvider1.SetError(stage3_gbox, "")


            If off_hold And Comp_responseTextBox.Text = orig_resp_details Then
                ErrorProvider1.SetError(Comp_responseTextBox, "Update response when hold removed")
                Comp_responseTextBox.Focus()
                Exit Sub
            End If

            If comp_against_codeComboBox.SelectedIndex < 0 Then
                ErrorProvider1.SetError(comp_against_codeComboBox, "Select complaint against")
                comp_against_codeComboBox.Focus()
                Exit Sub
            End If


            If Len(agentComboBox.Text) = 0 And _
            (comp_against_codeComboBox.SelectedIndex > 1 _
            And comp_against_codeComboBox.SelectedIndex < 5) Then
                ErrorProvider1.SetError(agentComboBox, "Select Agent or Various")
                agentComboBox.Focus()
                Exit Sub
            End If

            If Len(Cat_textComboBox.Text) = 0 Then
                ErrorProvider1.SetError(Cat_textComboBox, "Select category code")
                agentComboBox.Focus()
                Exit Sub
            End If

            If (cat_code = "A" And _
               cat_number = 8) Or _
              (cat_code = "B" And _
              (cat_number = 1 Or cat_number = 7)) Or _
              (cat_code = "C" And _
              (cat_number = 6 Or cat_number = 7 Or cat_number = 9)) Then
                MsgBox("The Complaint category chosen is no longer allowed")
                Exit Sub
            End If

            'If orig_completed And comprbtn.Checked = False And orig_checked And chkdrbtn.Checked = True Then
            '    ErrorProvider1.SetError(updbtn, "Need to change to not checked if you are changing to not complete")
            '    Exit Sub
            'End If

            If orig_founded = "U" And (foundedrbtn.Checked Or unfoundedrbtn.Checked) _
            And comprbtn.Checked = False Then
                ErrorProvider1.SetError(GroupBox2, "Can only set Founded/unfounded when complaint completed")
                Exit Sub
            End If

            If openrbtn.Checked And comprbtn.Checked = True Then
                ErrorProvider1.SetError(GroupBox2, "Need to set Founded/unfounded when complaint is completed")
                Exit Sub
            End If

            If comprbtn.Checked = True And compbycombobox.SelectedIndex = -1 Then
                ErrorProvider1.SetError(compbycombobox, "Need to set who completed the complaint")
                Exit Sub
            End If

            If compbycombobox.Text = "Not yet known" Then
                ErrorProvider1.SetError(compbycombobox, "Please select who completed the complaint")
                Exit Sub
            End If

            If openrbtn.Checked = False And comprbtn.Checked = False Then
                MsgBox("Founded/unfounded will be changed to not set")
                new_founded = "U"
                openrbtn.Checked = True
                Exit Sub
            End If

            If (orig_founded = "N" Or orig_founded = "Y") And openrbtn.Checked _
                    And comprbtn.Checked = True Then
                ErrorProvider1.SetError(GroupBox2, "Can only reset Founded/unfounded when complaint not completed")
                Exit Sub
            End If

            If orig_founded <> "Y" And new_founded = "Y" And actions_ComboBox.Text = "N/A" Then
                ErrorProvider1.SetError(actions_ComboBox, "Action needs to be entered")
                Exit Sub
            End If

            If Microsoft.VisualBasic.Len(Comp_textTextBox.Text) > 500 Then
                ErrorProvider1.SetError(Comp_textTextBox, "Maximum length of text is 500 characters")
                Exit Sub
            End If

            If Microsoft.VisualBasic.Len(Comp_responseTextBox.Text) > 500 Then
                ErrorProvider1.SetError(Comp_responseTextBox, "Maximum length of response is 500 characters")
                Exit Sub
            End If

            If stage_no > 2 And stage2_start_datepicker.Enabled = False Then
                ErrorProvider1.SetError(stage2_gbox, "Enter stage 2 before a higher stage")
                Exit Sub
            End If

            If stage_no > 2 And stage2_completed_by = 0 Then
                ErrorProvider1.SetError(stage2_gbox, "Enter stage 2 completed by before moving on to next stage")
                Exit Sub
            End If

            If stage_no > 1 And stage2_completed_by > 0 And CDate(stage2_start_date) > CDate(stage2_completed_date) Then
                ErrorProvider1.SetError(stage2_gbox, "Stage 2 completed date must be on or after start date")
                Exit Sub
            End If

            If stage_no > 2 And stage3_completed_by > 0 And Format(CDate(stage3_start_date), "yyyy-MM-dd") > _
            Format(CDate(stage3_completed_date), "yyyy-MM-dd") Then
                ErrorProvider1.SetError(stage3_gbox, "Stage 3 completed date must be on or after start date")
                Exit Sub
            End If

            If stage_no = 3 And stage3_start_date = CDate("01 01 1800") Then
                ErrorProvider1.SetError(stage3_gbox, "stage 3 start date must be entered")
                Exit Sub
            End If
            'write log for changes made
            log_type = "update complaint"
            If orig_entered_date <> Comp_dateTextBox.Text Then
                log_text = "Received date changed from " & orig_entered_date & " to " & Comp_dateTextBox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If recpt_code <> orig_recpt_code Then
                log_text = "Form of Receipt changed from " & orig_recpt_text & " to " & recpt_combobox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If recvd_from <> orig_recvd_from Then
                log_text = "Received from changed from " & orig_recvd_text & " to " & recvd_textComboBox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If case_no <> orig_case_no Then
                log_text = "Case number changed from " & orig_case_no & _
                                          " to " & case_no
                add_log(log_type, log_text)
                change_made = True
            End If

            If cl_no <> orig_cl_no Then
                log_text = "Client number changed from " & orig_cl_no & _
                                          " to " & cl_no
                add_log(log_type, log_text)
                change_made = True
            End If

            If branch_no <> orig_branch_no Then
                log_text = "Branch changed from " & orig_branch_no & " to " & branch_no
                add_log(log_type, log_text)
                change_made = True
            End If

            If against_code <> orig_against_code Then
                log_text = "Complaint Against changed from " & orig_against_text & " to " & comp_against_codeComboBox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If agent_no <> orig_agent_no Then
                log_text = "Agent changed from " & orig_agent_no & _
                                          " to " & agent_no
                add_log(log_type, log_text)
                change_made = True
            End If

            'If dept_code <> PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(10) Then
            '    log_text = "Department changed from " & PraiseAndComplaintsSQLDataSet.Complaints.Rows(0).Item(10) & _
            '                              " to " & dept_code
            '    updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "update complaint", comp_no, log_text)
            '    change_made = True
            'End If
            If against2_code <> orig_against2_code Then
                log_text = "Complaint Against2 changed from " & orig_against2_text & " to " & comp_against2_codecombobox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If agent2_no <> orig_agent2_no Then
                log_text = "Agent2 changed from " & orig_agent2_no & _
                                          " to " & agent2_no
                add_log(log_type, log_text)
                change_made = True
            End If

            If cat_code <> orig_cat_code Or _
                cat_number <> orig_cat_number Then
                log_text = "Category changed from " & orig_cat_code & _
                                 orig_cat_number & " to " & cat_code & cat_number
                add_log(log_type, log_text)
                change_made = True
            End If

            If alloc_to_code <> orig_alloc_to_code Then
                log_text = "Investigator changed from " & orig_inv_text & " to " & invComboBox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If action_code <> orig_action_code Then
                log_text = "Action changed from " & orig_action_text & " to " & actions_ComboBox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If Comp_textTextBox.Text <> orig_details Then
                log_text = "Complaint details changed "
                add_log(log_type, log_text)
                If env_str <> "Prod" Then
                    upd_txt = "insert into detail_history (hist_comp_no,hist_date, hist_user, hist_text) values (" & _
                                                comp_no & ",'" & _
                                                Format(Now, "yyyy-MMM-dd") & "'," & _
                                                log_code & ",'" & _
                                                orig_details & "')"
                    update_sql(upd_txt)
                Else
                    Detail_historyTableAdapter.InsertQuery(comp_no, Now, log_code, orig_details)
                End If

                change_made = True
            End If

            If Comp_responseTextBox.Text <> orig_resp_details Then
                log_text = "Complaint response changed "
                add_log(log_type, log_text)
                If env_str <> "Prod" Then
                    upd_txt = "insert into response (resp_comp_no,resp_date, resp_user, resp_text) values (" & _
                                                comp_no & ",'" & _
                                                Format(Now, "yyyy-MMM-dd") & "'," & _
                                                log_code & ",'" & _
                                                orig_resp_details & "')"
                    update_sql(upd_txt)
                Else

                    ResponseTableAdapter.InsertQuery(comp_no, Now, log_code, orig_resp_details)
                End If

                change_made = True
            End If

            If stage_no <> orig_stage_no Then
                log_text = "Stage changed from " & orig_stage_no & " to " & stage_no
                add_log(log_type, log_text)
                change_made = True
            End If

            If cor_code <> orig_cor_code Then
                log_text = "Corrective action changed from " & orig_cor_text & " to " & cor_nameComboBox.Text
                add_log(log_type, log_text)
                change_made = True
            End If

            If hold_name <> orig_hold_name Then
                log_text = "Hold reason changed from " & orig_hold_name & " to " & hold_name
                add_log(log_type, log_text)
                change_made = True
            End If

            If Comp_priorityCheckBox.Checked <> orig_priority Then
                log_text = "Priority changed from " & orig_priority & _
                                          " to " & Comp_priorityCheckBox.Checked
                add_log(log_type, log_text)
                change_made = True
            End If

            If category <> orig_category Then
                log_text = "Category changed from " & orig_category & _
                                                          " to " & category
                add_log(log_type, log_text)
                change_made = True
            End If
            If old_comp_no <> orig_old_comp_no Then
                log_text = "Old complaint number changed from " & orig_old_comp_no & _
                                                      " to " & old_comp_no
                add_log(log_type, log_text)
                change_made = True
            End If

            If costs_cancel <> orig_costs_cancel Then
                log_text = "Cancel costs changed from " & Format(orig_costs_cancel, "�#0.00") & _
                                                      " to " & Format(costs_cancel, "�#0.00")
                add_log(log_type, log_text)
                change_made = True
            End If

            If compensation <> orig_compensation Then
                log_text = "Compensation changed from " & Format(orig_compensation, "�#0.00") & _
                                                      " to " & Format(compensation, "�#0.00")
                add_log(log_type, log_text)
                change_made = True
            End If

            If resp_forgotten <> orig_resp_forgotten Then
                If orig_resp_forgotten = 0 Then
                    log_text = "Response forgotten set"
                Else
                    log_text = "Response forgotten reset"
                End If
                add_log(log_type, log_text)
                change_made = True
            End If

            If stage_no > 1 And stage2_start_date <> orig_stage2_start_date Then
                log_text = "Stage2 start date changed from " & Format(orig_stage2_start_date, "dd/MM/yyyy") & _
                                                      " to " & Format(stage2_start_date, "dd/MM/yyyy")
                add_log(log_type, log_text)
                stage2_change_made = True
            End If

            If stage_no > 1 And stage2_completed_by > 0 And stage2_completed_date <> orig_stage2_completed_date Then
                log_text = "Stage2 completed date changed from " & Format(orig_stage2_completed_date, "dd/MM/yyyy") & _
                                                      " to " & Format(stage2_completed_date, "dd/MM/yyyy")
                add_log(log_type, log_text)
                stage2_completed_change_made = True
            End If

            If gender <> orig_gender Then
                log_text = "Gender changed from " & orig_gender & _
                                                      " to " & gender
                add_log(log_type, log_text)
                change_made = True
            End If

            If ethnicity_desc <> orig_ethnicity_desc Then
                log_text = "Ethnicity changed from " & orig_ethnicity_desc & _
                                                                      " to " & ethnicity_desc
                add_log(log_type, log_text)
                change_made = True
                param2 = "select eth_code from ethnicity where eth_name = '" & ethnicity_desc & "'"
                Dim eth_ds As DataSet = get_dataset("Complaints", param2)
                ethnicity = eth_ds.Tables(0).Rows(0).Item(0)
                'get new ethnicity_code
                'Dim idx As Integer
                'For idx = 0 To Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows.Count - 1
                '    Dim eth_code As Integer = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(0)
                '    Dim eth_desc As String = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(1)
                '    If ethnicity_desc = eth_desc Then
                '        ethnicity = eth_code
                '        Exit For
                '    End If
                'Next
            End If
            If ack_cbox.Checked Then
                ack_date = ack_datepicker.Value
            End If
            If ack_date <> orig_ack_date Then
                log_text = "Acknowledgement Date changed from "
                If orig_ack_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(orig_ack_date, "dd/MM/yyyy")
                End If
                log_text = log_text & " to "
                If ack_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(ack_date, "dd/MM/yyyy")
                End If
                add_log(log_type, log_text)
                change_made = True
            End If
            If holding_cbox.Checked Then
                holding_date = holding_datepicker.Value
            End If
            If holding_date <> orig_holding_date Then
                log_text = "Holding Date changed from "
                If orig_holding_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(orig_holding_date, "dd/MM/yyyy")
                End If
                log_text = log_text & " to "
                If holding_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(holding_date, "dd/MM/yyyy")
                End If
                add_log(log_type, log_text)
                change_made = True
            End If
            If stage2_ack_cbox.Checked Then
                stage2_ack_date = stage2_ack_datepicker.Value
            End If
            If stage2_ack_date <> orig_stage2_ack_date Then
                log_text = "Stage 2 Acknowledgement Date changed from "
                If orig_stage2_ack_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(orig_stage2_ack_date, "dd/MM/yyyy")
                End If
                log_text = log_text & " to "
                If stage2_ack_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(stage2_ack_date, "dd/MM/yyyy")
                End If
                add_log(log_type, log_text)
                change_made = True
            End If
            If stage2_holding_cbox.Checked Then
                stage2_holding_date = stage2_holding_datepicker.Value
            End If
            If stage2_holding_date <> orig_stage2_holding_date Then
                log_text = "Holding Date changed from "
                If orig_stage2_holding_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(orig_stage2_holding_date, "dd/MM/yyyy")
                End If
                log_text = log_text & " to "
                If stage2_holding_date = CDate("1800,1,1") Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(stage2_holding_date, "dd/MM/yyyy")
                End If
                add_log(log_type, log_text)
                change_made = True
            End If

            If stage_no > 2 And stage3_start_date <> orig_stage3_start_date Then
                log_text = "Stage3 start date changed from " & Format(orig_stage3_start_date, "dd/MM/yyyy") & _
                                                      " to " & Format(stage3_start_date, "dd/MM/yyyy")
                add_log(log_type, log_text)
                stage3_change_made = True
            End If

            If stage_no > 2 And stage3_completed_by > 0 And stage3_completed_date <> orig_stage3_completed_date Then
                log_text = "Stage3 completed date changed from " & Format(orig_stage3_completed_date, "dd/MM/yyyy") & _
                                                      " to " & Format(stage3_completed_date, "dd/MM/yyyy")
                add_log(log_type, log_text)
                stage3_completed_change_made = True
            End If

            'changing from completed_by > 1 to not yet known (1) treat as reset back to zero
            If orig_stage2_completed_by > 1 And stage2_completed_by = 1 Then
                stage2_completed_by = 0
            End If

            If stage_no > 1 And stage2_completed_by <> orig_stage2_completed_by Then
                log_text = "Stage2 completed by changed from " & orig_stage2_completed_by & _
                                                      " to " & stage2_completed_by
                add_log(log_type, log_text)
                stage2_completed_change_made = True
            End If

            If stage_no > 2 And stage3_completed_by <> orig_stage3_completed_by Then
                log_text = "Stage3 completed by changed from " & orig_stage3_completed_by & _
                                                      " to " & stage3_completed_by
                add_log(log_type, log_text)
                stage3_completed_change_made = True
            End If

            If feedback_no <> orig_feedback_no Then
                log_text = "feedback changed from " & orig_feedback_no & " to " & feedback_no
                add_log(log_type, log_text)
                change_made = True
            End If
            If type_code <> orig_type_code Then
                log_text = "Type Code changed from " & orig_type_code & " to " & type_code
                add_log(log_type, log_text)
                change_made = True
            End If

            'update complaints database
            If change_made Then
                If env_str <> "Prod" Then
                    Dim priority As Integer = 0
                    If Comp_priorityCheckBox.Checked Then priority = 1
                    upd_txt = "update Complaints set comp_date= '" & _
                Format(CDate(Comp_dateTextBox.Text), "dd/MMM/yyyy") & _
                    "',comp_recpt_code = " & recpt_code & _
                    ", comp_recvd_code = " & recvd_from & _
                    ", comp_case_no = " & case_no & _
                    ", comp_client_no = " & cl_no & _
                    ",comp_against_code = " & against_code & _
                    ", comp_against_agent = " & agent_no & _
                    ", comp_cat_code = '" & cat_code & _
                    "', comp_cat_number = " & cat_number & _
                    ", comp_dept_code = " & dept_code & _
                    ", comp_allocated_to = " & alloc_to_code & _
                    ", comp_text = '" & Comp_textTextBox.Text & _
                    "', comp_response = '" & Comp_responseTextBox.Text & _
                    "', comp_action = " & action_code & _
                    ", comp_stage_no = " & stage_no & _
                    ", comp_cor_code = " & cor_code & _
                    ", comp_hold_code = " & hold_code & _
                    ", comp_priority = " & priority & _
                    ",comp_old_comp_no=" & old_comp_no & _
                    ",comp_costs_cancel_value=" & costs_cancel & _
                    ",comp_compensation=" & compensation & _
                    ", comp_response_no=" & resp_forgotten & _
                    ",comp_gender='" & Microsoft.VisualBasic.Left(gender, 1) & _
                    "', comp_ethnicity=" & ethnicity & _
                    ", comp_ack_letter_date= '" & Format(ack_date, "dd/MMM/yyyy") & _
                    "',comp_holding_letter_date='" & Format(holding_date, "dd/MMM/yyyy") & _
                    "',comp_stage2_ack_letter_date='" & Format(stage2_ack_date, "dd/MMM/yyyy") & _
                    "',comp_stage2_holding_letter_date='" & Format(stage2_holding_date, "dd/MMM/yyyy") & _
                    "',comp_against_code2=" & against2_code & _
                    ",comp_against_agent2=" & agent2_no & _
                    ", comp_category=" & category & _
                    ", comp_branch_no=" & branch_no & _
                    ",comp_feedback_no=" & feedback_no & _
                    ", comp_type_code=" & type_code & _
                    " WHERE     comp_no = " & comp_no
                    update_sql(upd_txt)
                Else
                    Try
                        ComplaintsTableAdapter.UpdateQuery1(Comp_dateTextBox.Text, recpt_code, _
                        recvd_from, case_no, cl_no, _
                        against_code, agent_no, cat_code, _
                        cat_number, dept_code, alloc_to_code, _
                        Comp_textTextBox.Text, Comp_responseTextBox.Text, _
                        action_code, stage_no, cor_code, hold_code, Comp_priorityCheckBox.Checked, _
                        old_comp_no, costs_cancel, compensation, resp_forgotten, gender, ethnicity, _
                        ack_date, holding_date, stage2_ack_date, stage2_holding_date, against2_code, _
                        agent2_no, category, branch_no, feedback_no, type_code, comp_no)
                    Catch ex As Exception
                        MessageBox.Show(ex.Message)
                        Me.Close()
                        Exit Sub
                    End Try
                End If

            End If
            'check if completed has changed
            If orig_completed <> completed Then
                If completed = False Then
                    compby_code = 0
                End If
                upd_txt = "update Complaints set comp_completed_by = " & compby_code & _
                                                       ",comp_completion_date = '" & Format(compdatetimepicker.Value, "yyyy-MM-dd") & _
                                                       "' where comp_no = " & comp_no
                update_sql(upd_txt)
                'ComplaintsTableAdapter.UpdateQuery2(compdatetimepicker.Text, compby_code, comp_no)
                If completed Then
                    log_text = "Complaint changed to completed by " & compbycombobox.Text
                    'check if one of the clients to notify
                    If cl_no > 0 Then
                        param2 = "select comp_resp_cl_no from Complaints_resp_clients " & _
                        " where comp_resp_cl_no = " & cl_no
                        Dim resp_ds As DataSet = get_dataset("Complaints", param2)
                        If no_of_rows > 0 Then
                            MsgBox("Don't forget to send details to the client")
                        End If
                        'Me.Complaints_resp_clientsTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaints_resp_clients, cl_no)
                        'If Me.PraiseAndComplaintsSQLDataSet.Complaints_resp_clients.Rows.Count = 1 Then
                        '    MsgBox("Don't forget to send details to the client")
                        'End If
                    End If
                Else
                    log_text = "Complaint changed to not completed "
                End If
                add_log(log_type, log_text)
                change_made = True
            End If

            'check if completed date or completed by has changed
            Dim completed_changed As Boolean = False
            If completed And orig_completed Then
                If orig_comp_date <> compdatetimepicker.Text Then
                    log_text = "Completed date changed from " & orig_comp_date & _
                                                          " to " & CDate(compdatetimepicker.Text)
                    add_log(log_type, log_text)
                    completed_changed = True
                End If
                If orig_compby <> compbycombobox.Text Then
                    log_text = "Completed by changed from " & orig_compby & _
                                                                          " to " & compbycombobox.Text
                    add_log(log_type, log_text)
                    completed_changed = True
                End If
                If completed_changed Then
                    upd_txt = "update Complaints set comp_completed_by = " & compby_code & _
                                        ",comp_completion_date = '" & Format(compdatetimepicker.Value, "yyyy-MM-dd") & _
                                       "' where comp_no = " & comp_no
                    update_sql(upd_txt)
                    'ComplaintsTableAdapter.UpdateQuery2(compdatetimepicker.Text, compby_code, comp_no)
                End If
            End If

            'check if founded has changed
            If orig_founded <> new_founded And Not (new_founded = Nothing) _
            Or orig_pf_agent_no <> selected_pf_agent_no Then
                change_made = True
                If new_founded = "Y" Then
                    log_text = "Complaint set to founded"
                    selected_pf_agent_no = 0
                ElseIf new_founded = "N" Then
                    log_text = "Complaint set to not founded"
                    selected_pf_agent_no = 0
                ElseIf new_founded = "U" Then
                    log_text = "Complaint set to founded not set"
                    selected_pf_agent_no = 0
                Else
                    If orig_pf_agent_no <> selected_pf_agent_no Then
                        log_text = "Complaint part founded agent changed to " & selected_pf_agent_no
                        add_log(log_type, log_text)
                    End If
                    log_text = "Complaint set to part founded"
                    If new_founded = "" Then
                        new_founded = orig_founded
                    End If
                End If
                If env_str <> "Prod" Then
                    Dim priority As Integer = 0
                    upd_txt = "update Complaints set comp_founded = '" & _
                     new_founded & _
                    "',comp_part_founded_agent_no = " & selected_pf_agent_no & _
                     " WHERE     comp_no = " & comp_no
                    update_sql(upd_txt)
                Else
                    ComplaintsTableAdapter.UpdateQuery3(new_founded, selected_pf_agent_no, comp_no)
                    add_log(log_type, log_text)
                End If
            End If

            'see if checked by has changed
            'Dim checked_by As Integer
            'If checked <> orig_checked Then
            '    If checked Then
            '        log_text = "Complaint changed to checked"
            '        checked_by = log_code
            '    Else
            '        log_text = "Complaint changed to not checked"
            '        checked_by = 0
            '    End If
            '    'save on test database for test logid
            '    If log_code = test_logid Then
            '        ComplaintsTableAdapter.UpdateQuery4(checkeddatetimepicker.Value, checked_by, comp_no)
            '    Else
            '        ComplaintsTableAdapter.UpdateQuery4(checkeddatetimepicker.Value, checked_by, comp_no)
            '    End If
            '    add_log(log_type, log_text)
            '    change_made = True
            'End If

            'see if stage2 has changed
            If stage2_change_made Then
                upd_txt = "update Complaints set comp_stage2_start_date = '" & Format(stage2_start_date, "yyy-MM-dd") & _
                                                       "' where comp_no = " & comp_no
                update_sql(upd_txt)
                'ComplaintsTableAdapter.UpdateQuery4(stage2_start_date, comp_no)
            End If
            If stage3_change_made Then
                upd_txt = "update Complaints set comp_stage3_start_date = '" & Format(stage3_start_date, "yyy-MM-dd") & _
                                                                       "' where comp_no = " & comp_no
                update_sql(upd_txt)
                'ComplaintsTableAdapter.UpdateQuery6(stage3_start_date, comp_no)
            End If
            If stage2_completed_change_made Or (stage_no = 1 And orig_stage_no > 1) Then
                If stage_no = 1 Then
                    stage2_completed_by = 0
                End If
                upd_txt = "update Complaints set comp_stage2_completed_date = '" & _
                             Format(stage2_completed_date, "yyy-MM-dd") & "'," & _
                            " comp_stage2_completed_by = " & stage2_completed_by & _
                            " where comp_no = " & comp_no
                update_sql(upd_txt)
                'ComplaintsTableAdapter.UpdateQuery5(stage2_completed_by, stage2_completed_date, comp_no)
            End If

            If stage3_completed_change_made Or (stage_no = 2 And orig_stage_no > 2) Then
                If stage_no = 2 Then
                    stage3_completed_by = 0
                End If
                upd_txt = "update Complaints set comp_stage3_completed_date = '" & _
                                             Format(stage3_completed_date, "yyy-MM-dd") & "'," & _
                                            " comp_stage3_completed_by = " & stage3_completed_by & _
                                            " where comp_no = " & comp_no
                update_sql(upd_txt)
                'ComplaintsTableAdapter.UpdateQuery7(stage3_completed_by, stage3_completed_date, comp_no)
            End If

            'see if legal has changed
            Dim legal_change As Boolean = False
            If legal_flag <> orig_legal_flag Then
                legal_change = True
                If legal_flag = "N" Then
                    referred_to_solicitor = CDate("1800,1,1")
                    log_text = "Legal changed from Yes to No"
                Else
                    log_text = "Legal changed from No to Yes"
                End If
                add_log(log_type, log_text)
            End If
            If legal_flag = "Y" Then
                If orig_referred_to_solicitor <> referred_to_solicitor Then
                    legal_change = True
                    log_text = "Referred to solicitor date changed to "
                    If referred_to_solicitor = CDate("1800,1,1") Then
                        log_text = log_text & "blank"
                    Else
                        log_text = log_text & Format(referred_to_solicitor, "dd.MM.yyyy")
                    End If
                    add_log(log_type, log_text)
                End If

            End If
            If legal_change = True Then
                upd_txt = "update Complaints set comp_referred_to_solicitor = '" & Format(referred_to_solicitor, "dd/MMM/yyyy") & "'" & _
                                                   ", comp_legal_insurance_flag= '" & legal_flag & _
                                                   "' where comp_no = " & comp_no
                update_sql(upd_txt)
                'ComplaintsTableAdapter.UpdateQuery9(legal_flag, referred_to_solicitor, comp_no)
            End If

            'insurance change
            Dim ins_change As Boolean = False
            If (orig_insurance_type_no < 0 And upd_inscbox.Checked) _
            Or (orig_insurance_type_no >= 0 And upd_inscbox.Checked = False) Then
                ins_change = True
                If orig_insurance_type_no >= 0 Then
                    referred_to_insurer = CDate("1800,1,1")
                    log_text = "Insurance changed from Yes to No"
                Else
                    log_text = "Insurance changed from No to Yes"
                End If
                add_log(log_type, log_text)
                If upd_inscbox.Checked = False Then
                    insurance_type_no = -1
                    prof_indemnity_no = 0
                    public_liability_no = 0
                End If
            End If
            If upd_inscbox.Checked Then
                If Format(orig_referred_to_insurer, "yyyy-MM-dd") <> Format(referred_to_insurer, "yyyy-MM-dd") Then
                    ins_change = True
                    log_text = "Referred to insurer date changed to "
                    If referred_to_insurer = CDate("1800,1,1") Then
                        log_text = log_text & "blank"
                    Else
                        log_text = log_text & Format(referred_to_insurer, "dd.MM.yyyy")
                    End If

                    add_log(log_type, log_text)
                End If
                If upd_liability_cbox.Checked <> orig_liability Then
                    ins_change = True
                    If upd_liability_cbox.Checked Then
                        log_text = "Liability changed to Yes"
                    Else
                        log_text = "Liability changed to No"
                    End If
                    add_log(log_type, log_text)
                End If
                If insurance_type_no <> orig_insurance_type_no Then
                    ins_change = True
                    Select Case insurance_type_no
                        Case 0
                            log_text = "Insurance type changed to unknown"
                        Case 1
                            log_text = "Insurance type changed to Actual Notification"
                        Case 2
                            log_text = "Insurance type changed to Bordereau"

                    End Select
                    add_log(log_type, log_text)
                End If
                If prof_indemnity_no <> orig_prof_indemnity_no Then
                    ins_change = True
                    If prof_indemnity_no = 1 Then
                        log_text = "Prof Indemnity changed to Yes"
                    Else
                        log_text = "Prof Indemnity changed to No"
                    End If
                    add_log(log_type, log_text)
                End If
                If public_liability_no <> orig_public_liability_no Then
                    ins_change = True
                    If public_liability_no = 1 Then
                        log_text = "Public Liability changed to Yes"
                    Else
                        log_text = "Public Liability changed to No"
                    End If
                    add_log(log_type, log_text)
                End If
                liability_flag = "N"
                If upd_liability_cbox.Checked = True Then
                    liability_flag = "Y"
                End If
            End If
            If ins_change Then
                upd_txt = "update Complaints set comp_referred_to_insurer = '" & Format(referred_to_insurer, "dd/MMM/yyyy") & "'" & _
                                    ", comp_insurance_type_no=" & insurance_type_no & _
                                    ", comp_liability_flag= '" & liability_flag & _
                                    "', comp_prof_indemnity_no = " & prof_indemnity_no & _
                                    ", comp_public_liability_no = " & public_liability_no & _
                                    " where comp_no = " & comp_no
                update_sql(upd_txt)
                'ComplaintsTableAdapter.UpdateQuery11(referred_to_insurer, insurance_type_no, liability_flag, comp_no)
            End If
            'If ins2_change Then
            '    upd_txt = "update Complaints set comp_insurance_type2_no = " & insurance_type2_no & _
            '                                           " where comp_no = " & comp_no
            '    update_sql(upd_txt)
            'End If
            If (legal_flag = "Y" Or upd_inscbox.Checked) And (orig_monetary_risk <> monetary_risk) Then
                ins_change = True
                log_text = "Monetary Risk changed from "
                If orig_monetary_risk = -999 Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(orig_monetary_risk, "�0.00")
                End If
                log_text = log_text & " to "
                If monetary_risk = -999 Then
                    log_text = log_text & "blank"
                Else
                    log_text = log_text & Format(monetary_risk, "�#0.00")
                End If
                add_log(log_type, log_text)
                'update monetary risk
                upd_txt = "update Complaints set comp_monetary_risk = " & Format(monetary_risk, "f") & _
                                           " where comp_no = " & comp_no
                update_sql(upd_txt)
                'ComplaintsTableAdapter.UpdateQuery12(Format(monetary_risk, "f"), comp_no)
            End If

            If cancel_costs_reason_change = True Then
                If cancel_costs_reason = "" Then
                    log_text = "Cancel Costs reason removed"
                Else
                    log_text = "Cancel Costs reason changed"
                End If

                add_log(log_type, log_text)
                upd_txt = "update Complaints set comp_cancel_costs_reason = '" & cancel_costs_reason & "'" & _
                                    " where comp_no = " & comp_no
                update_sql(upd_txt)
                'ComplaintsTableAdapter.UpdateQuery10(cancel_costs_reason, comp_no)
            End If

            If orig_stage1_esc_code <> stage1_esc_code Then
                stage1_esc_change = True
                log_text = "Stage 1 Escalation changed from " & orig_stage1_esc_code & " to " & stage1_esc_code
                add_log(log_type, log_text)
                '6.3.14
                upd_txt = "delete from Complaint_Escalation where  compesc_comp_no = " & comp_no & _
                " and compesc_stage_no = 1"
                update_sql(upd_txt)
                upd_txt = "insert into Complaint_Escalation (compesc_comp_no, compesc_stage_no, compesc_reason,compesc_date,compesc_created_by)" & _
                " values (" & comp_no & ",1," & stage1_esc_code & ",'" & Format(Now, "yyyy-MM-dd") & "'," & log_code & ")"
                update_sql(upd_txt)
                'Me.Complaint_EscalationTableAdapter.DeleteQuery(comp_no, 1)
                'Me.Complaint_EscalationTableAdapter.InsertQuery(comp_no, 1, stage1_esc_code, Format(Now, "dd/MM/yyyy"), log_code)
            End If

            If orig_stage2_esc_code <> stage2_esc_code Then
                stage2_esc_change = True
                log_text = "Stage 2 Escalation changed from " & orig_stage2_esc_code & " to " & stage2_esc_code
                add_log(log_type, log_text)
                upd_txt = "delete from Complaint_Escalation where  compesc_comp_no = " & comp_no & _
                                " and compesc_stage_no = 2"
                update_sql(upd_txt)
                upd_txt = "insert into Complaint_Escalation (compesc_comp_no, compesc_stage_no, compesc_reason,compesc_date,compesc_created_by)" & _
                " values (" & comp_no & ",2," & stage2_esc_code & ",'" & Format(Now, "yyyy-MM-dd") & "'," & log_code & ")"
                update_sql(upd_txt)
                'Me.Complaint_EscalationTableAdapter.DeleteQuery(comp_no, 2)
                'Me.Complaint_EscalationTableAdapter.InsertQuery(comp_no, 2, stage2_esc_code, Format(Now, "dd/MM/yyyy"), log_code)
            End If

            If change_made Or doc_change_made Or completed_changed Or stage2_change_made Or _
            stage2_completed_change_made Or stage3_change_made Or stage3_completed_change_made Or _
            legal_change Or ins_change Or cancel_costs_reason_change Or stage1_esc_change Or stage2_esc_change Then
                MsgBox("Complaint has been updated")
            Else
                MsgBox("No changes have been made")
            End If

            Me.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub agentComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentComboBox.Validated
        Dim idx As Integer
        agent_no = 0
        If agentComboBox.SelectedItem = "Various" Then
            agent_no = 9999
        Else
            For idx = 0 To agent_rows
                If bailiff_table(idx, 2) = agentComboBox.SelectedItem Then
                    agent_no = bailiff_table(idx, 1)
                    Exit For
                End If
            Next
        End If
        dept_code = 0
    End Sub

    Private Sub Cat_textComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Cat_textComboBox.Validated

    End Sub

    Private Sub Cat_textComboBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Cat_textComboBox.Validating
        ErrorProvider1.SetError(Cat_textComboBox, "")
        If Cat_textComboBox.SelectedItem = "99 Contentious" Then
            ErrorProvider1.SetError(Cat_textComboBox, "Contentious is no longer valid")
            e.Cancel = True
        End If
    End Sub

    Private Sub invComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles invComboBox.Validated
        'Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
        param2 = "select inv_code from investigators where inv_text = '" & invComboBox.Text & "'"
        Dim inv_ds As DataSet = get_dataset("Complaints", param2)

        Try
            'alloc_to_code = PraiseAndComplaintsSQLDataSet.Investigators.Rows(invComboBox.SelectedIndex).Item(0)
            alloc_to_code = inv_ds.Tables(0).Rows(0).Item(0)
        Catch
            MsgBox("Invalid allocated to name")
        End Try
    End Sub

    Private Sub cat_codeComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cat_codeComboBox.SelectedIndexChanged
        '5.3.14
        
        'Dim cat_table As New PraiseAndComplaintsSQLDataSet.Complaint_categoriesDataTable
        cat_code = Microsoft.VisualBasic.Left(cat_codeComboBox.SelectedItem, 1)

        'If cat_code <> orig_cat_code Then
        '    orig_cat_code = ""
        'End If
        param2 = "select cat_code, cat_number, cat_text from Complaint_categories" & _
                            " where cat_code = '" & cat_code & "'" & _
                            " order by cat_number"
        Dim cat_ds As DataSet = get_dataset("Complaints", param2)
        'Me.Complaint_categoriesTableAdapter.Fill2(cat_table, cat_code)
        Dim row As DataRow
        Dim cat_text As String
        Dim idx As Integer = 0
        Cat_textComboBox.Items.Clear()
        upd_type_code_cbox.Items.Clear()
        type_code = 0
        'For Each row In cat_table.Rows
        For Each row In cat_ds.Tables(0).Rows
            'cat_text = cat_table.Rows(idx).Item(1) & " " & cat_table.Rows(idx).Item(2)
            cat_text = cat_ds.Tables(0).Rows(idx).Item(1) & " " & cat_ds.Tables(0).Rows(idx).Item(2)
            Cat_textComboBox.Items.Add(cat_text)
            'If cat_number = cat_table.Rows(idx).Item(1) And orig_cat_code <> "" Then
            If cat_number = cat_ds.Tables(0).Rows(idx).Item(1) And orig_cat_code <> "" Then
                If cat_number < 99 Then
                    Cat_textComboBox.SelectedIndex = cat_number - 1
                Else
                    Select Case cat_number
                        Case 99
                            Cat_textComboBox.Text = "Contentious"
                        Case 100
                            Cat_textComboBox.Text = "Request Report"
                        Case 101
                            Cat_textComboBox.Text = "SAR"
                    End Select
                End If
            End If
            idx += 1
        Next
    End Sub

    Private Sub cat_codeComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cat_codeComboBox.Validated


    End Sub

    Private Sub recpt_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recpt_combobox.SelectedIndexChanged

    End Sub

    Private Sub recpt_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles recpt_combobox.Validated

        recpt_text = recpt_combobox.Text.ToString
        param2 = "select recpt_code from Receipt_type where recpt_text = '" & recpt_text & "'"
        Dim recpt_ds As DataSet = get_dataset("Complaints", param2)
        recpt_code = recpt_ds.Tables(0).Rows(0).Item(0)
    End Sub

    Private Sub recvd_textComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles recvd_textComboBox.Validated
        recvd_text = recvd_textComboBox.Text.ToString
        param2 = "select recvd_from from Received_from where recvd_text = '" & recvd_text & "'"
        Dim recvd_ds As DataSet = get_dataset("Complaints", param2)
        recvd_from = recvd_ds.Tables(0).Rows(0).Item(0)
    End Sub

    Private Sub comp_case_noTextBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles comp_case_noTextBox.Validated
        case_no = comp_case_noTextBox.Text
        clref_lbl.Text = ""
        Dim idx As Integer
        If case_no > 0 Then
            If comp_cmpny_no > 0 Then
                param2 = "select clnt_name from Complaint_clients" & _
                " order by clnt_name"
                Dim clnt_ds As DataSet = get_dataset("Complaints", param2)
                cl_ComboBox.Items.Clear()
                For Each row In clnt_ds.Tables(0).Rows
                    cl_ComboBox.Items.Add(row(0))
                Next
            Else
                Dim dob_str As String = "N/A"
                param1 = "onestep"
                param2 = "select dateOfBirth, clientschemeID, client_ref from Debtor where _rowid = " & case_no
                Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 1 Then
                    Dim dob As Date
                    Try
                        dob = debtor_dataset.Tables(0).Rows(0).Item(0)
                    Catch ex As Exception
                        dob_str = "unknown"
                    End Try
                    If dob_str <> "unknown" Then
                        dob_str = Format(dob, "dd/MM/yyyy")
                    End If
                    dob_textbox.Text = dob_str
                    'get client ref
                    clref_lbl.Text = "Client Ref:" & debtor_dataset.Tables(0).Rows(0).Item(2)
                    'get debt type
                    Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(1)
                    param2 = "select schemeID from ClientScheme where _rowid = " & csid
                    Dim csid_dataset As DataSet = get_dataset(param1, param2)
                    Dim sch_id As Integer = csid_dataset.Tables(0).Rows(0).Item(0)
                    param2 = "select name from Scheme where _rowid = " & sch_id
                    Dim sch_dataset As DataSet = get_dataset(param1, param2)
                    debt_typelbl.Text = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                End If
            End If
        End If

        'reset ack letter and holding letter dates
        If ack_date <> CDate("1800,1,1") Then
            ack_datepicker.Visible = True
            ack_cbox.Checked = True
            ack_datepicker.Value = ack_date
        Else
            ack_datepicker.Visible = False
            ack_cbox.Checked = False
            If case_no > 0 Then
                If comp_cmpny_no = 0 Then
                    param1 = "onestep"
                    param2 = " select text, _createdDate from Note" & _
                    " where debtorID = " & case_no & " and type = 'Letter'" & _
                    " and _createdDate >= '" & Format(CDate(Comp_dateTextBox.Text), "yyyy-MM-dd") & "'" & _
                    " order by _rowid"
                    Dim note_dataset As DataSet = get_dataset(param1, param2)
                    For idx = 0 To no_of_rows - 1
                        Dim note_text As String = note_dataset.Tables(0).Rows(idx).Item(0)
                        If InStr(note_text, "Complaint_Ack") > 0 And _
                            InStr(note_text, "Stage 2") = 0 Then
                            ack_datepicker.Text = FormatDateTime(note_dataset.Tables(0).Rows(idx).Item(1), DateFormat.ShortDate)
                            ack_datepicker.Visible = True
                            ack_cbox.Checked = True
                            ack_date = ack_datepicker.Value
                        End If
                    Next
                End If
               
            End If
        End If
        If holding_date <> CDate("1800,1,1") Then
            holding_datepicker.Visible = True
            holding_cbox.Checked = True
            holding_datepicker.Value = holding_date
        Else
            holding_datepicker.Visible = False
            holding_cbox.Checked = False
            If check_held_letter(case_no, Comp_dateTextBox.Text) = "Y" Then
                holding_datepicker.Text = FormatDateTime(parm_hold_date, DateFormat.ShortDate)
                holding_datepicker.Visible = True
                holding_cbox.Checked = True
                holding_date = holding_datepicker.Value
            End If
        End If

        If stage2_ack_date <> CDate("1800,1,1") Then
            stage2_ack_datepicker.Visible = True
            stage2_ack_cbox.Checked = True
            stage2_ack_datepicker.Value = stage2_ack_date
        Else
            stage2_ack_datepicker.Visible = False
            stage2_ack_cbox.Checked = False
            If case_no > 0 Then
                If comp_cmpny_no = 0 Then
                    param1 = "onestep"
                    param2 = " select text, _createdDate from Note" & _
                    " where debtorID = " & case_no & " and type = 'Letter'"
                    If stage2_start_date <> Nothing Then
                        param2 = param2 & " and _createdDate >= '" & Format(stage2_start_date, "yyyy-MM-dd") & "'"
                    Else
                        param2 = param2 & " and _createdDate >= '" & Format(CDate(Comp_dateTextBox.Text), "yyyy-MM-dd") & "'"
                    End If
                    param2 = param2 & " order by _rowid"
                    Dim note_dataset As DataSet = get_dataset(param1, param2)
                    For idx = 0 To no_of_rows - 1
                        Dim note_text As String = note_dataset.Tables(0).Rows(idx).Item(0)
                        If InStr(note_text, "Complaint_Ack") > 0 And _
                            InStr(note_text, "Stage 2") > 0 Then
                            stage2_ack_datepicker.Text = FormatDateTime(note_dataset.Tables(0).Rows(idx).Item(1), DateFormat.ShortDate)
                            stage2_ack_datepicker.Visible = True
                            stage2_ack_cbox.Checked = True
                            stage2_ack_date = stage2_ack_datepicker.Value
                        End If
                    Next
                End If
                
            End If
        End If
    End Sub

    Private Sub comp_case_noTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles comp_case_noTextBox.Validating
        Try
            ErrorProvider1.SetError(comp_case_noTextBox, "")
            If Len(Trim(comp_case_noTextBox.Text)) = 0 Then
                comp_case_noTextBox.Text = 0
            End If
            Try
                If comp_case_noTextBox.Text = 0 Or comp_cmpny_no > 0 Then
                    populate_client_table(comp_cmpny_no)
                    Dim idx2 As Integer
                    For idx2 = 1 To cl_rows
                        cl_ComboBox.Items.Add(client_table(idx2, 2))
                    Next
                    Exit Sub
                End If
            Catch ex As Exception
                ErrorProvider1.SetError(comp_case_noTextBox, "Case number must be numeric")
                e.Cancel = True
                Exit Sub
            End Try

            'check case nunber is on onestep
            If comp_cmpny_no = 0 Then
                param1 = "onestep"
                param2 = "select clientschemeID from Debtor" & _
                " where _rowid = " & comp_case_noTextBox.Text
                Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    ErrorProvider1.SetError(comp_case_noTextBox, "Case number is not on Onestep")
                    e.Cancel = True
                    Exit Sub
                End If

                Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(0)

                'get client ID
                param2 = "select clientID from ClientScheme" & _
                " where _rowid = " & csid
                Dim csid_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    ErrorProvider1.SetError(comp_case_noTextBox, "Client scheme is not on Onestep")
                    Exit Sub
                End If
                cl_no = csid_dataset.Tables(0).Rows(0).Item(0)
                'get client name
                param1 = "onestep"
                param2 = "select name from Client" & _
                " where _rowid = " & cl_no
                Dim cl_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    MsgBox("Unable to get client name for " & cl_no)
                    Exit Sub
                End If
                cl_name = Trim(cl_dataset.Tables(0).Rows(0).Item(0))

                cl_ComboBox.Items.Clear()
                cl_ComboBox.Items.Add(cl_name)
                cl_ComboBox.SelectedItem = cl_name
                comp_against_codeComboBox.Focus()
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub comp_case_noTextBox_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comp_case_noTextBox.TextChanged

    End Sub

    Private Sub cl_ComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cl_ComboBox.Validated
        Dim idx As Integer
        For idx = 1 To cl_rows
            If client_table(idx, 2) = cl_ComboBox.SelectedItem Then
                cl_no = client_table(idx, 1)
                Exit For
            End If
        Next
    End Sub

    Private Sub comp_against_codeComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comp_against_codeComboBox.SelectedIndexChanged

    End Sub

    Private Sub comp_against_codeComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles comp_against_codeComboBox.Validated
        ErrorProvider1.SetError(comp_against_codeComboBox, "")
        Try
            Dim idx As Integer
            If comp_cmpny_no <> 0 Then
                param1 = "Complaints"
                param2 = "select agnt_code, agnt_name from Complaint_Agents" & _
                " order by agnt_code"
                Dim agnt_ds As DataSet = get_dataset(param1, param2)
                agent_rows = agnt_ds.Tables(0).Rows.Count - 1
                ReDim bailiff_table(no_of_rows, no_of_rows)
                agentComboBox.Items.Clear()
                For idx = 0 To agent_rows
                    agentComboBox.Items.Add(agnt_ds.Tables(0).Rows(idx).Item(1))
                    bailiff_table(idx, 1) = agnt_ds.Tables(0).Rows(idx).Item(0)
                    bailiff_table(idx, 2) = agnt_ds.Tables(0).Rows(idx).Item(1)
                Next
            Else
                param2 = "select against_code from complaint_against where against_text = '" & comp_against_codeComboBox.Text & "'"
                Dim against_ds As DataSet = get_dataset("Complaints", param2)
                against_code = against_ds.Tables(0).Rows(0).Item(0)
                'against_code = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(comp_against_codeComboBox.SelectedIndex).Item(0)
                If against_code <> orig_against_code Then
                    orig_against_code = -1
                Else
                    Exit Sub
                End If
                agentComboBox.Items.Clear()
                agentComboBox.Text = ""
                Dim where_clause As String
                If Microsoft.VisualBasic.Left(comp_against_codeComboBox.Text, 2) = "FC" Then
                    where_clause = " where agent_type = 'B' and internalExternal = 'I' and branchID <> 10"
                ElseIf comp_against_codeComboBox.Text = "Van Bailiff" Or _
                comp_against_codeComboBox.Text = "VB" Then
                    where_clause = " where agent_type = 'B' and internalExternal = 'E' and branchID <> 10"
                ElseIf comp_against_codeComboBox.Text = "High Court" Then
                    where_clause = " where agent_type = 'B' and (internalExternal = 'I' or internalExternal = 'E') and branchID <> 10"
                ElseIf comp_against_codeComboBox.Text = "Marston" Then
                    where_clause = " where agent_type = 'B'"
                ElseIf comp_against_codeComboBox.Text = "Contact Centre" Then
                    where_clause = " where (agent_type ='P' or agent_type = 'A') "
                Else
                    where_clause = " where (agent_type ='P' or agent_type = 'A')  and branchID <> 10"
                End If
                If comp_against_codeComboBox.Text = "Enforcement Officer" Then
                    where_clause = " where agent_type = 'B' and branchID <> 10"
                End If
                populate_bailiff_table(where_clause)

                For idx = 1 To agent_rows
                    agentComboBox.Items.Add(bailiff_table(idx, 2))
                Next
                agentComboBox.Items.Add("Various")
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub Comp_dateTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub notcomprbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles notcomprbtn.CheckedChanged
        If comprbtn.Checked = True Then
            completed = True
            compdatetimepicker.Visible = True
            compbycombobox.Visible = True
            If orig_completed = False Then
                compdatetimepicker.Value = Now
                compbycombobox.SelectedIndex = -1
                compbycombobox.Text = "Not yet known"
            End If
            Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
            Dim row As DataRow
            Dim idx As Integer
            compbycombobox.Items.Clear()
            For Each row In Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows
                compbycombobox.Items.Add(PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(1))
                idx += 1
            Next
        Else
            completed = False
            compdatetimepicker.Visible = False
            compbycombobox.Visible = False
        End If
    End Sub

    Private Sub response_TextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub chkdrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If completed And Not orig_completed Then
            compbycombobox.SelectedIndex = -1
        End If
    End Sub

    Private Sub GroupBox1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles GroupBox1.Validating


    End Sub

    Private Sub actions_ComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles actions_ComboBox.SelectedIndexChanged

    End Sub

    Private Sub actions_ComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles actions_ComboBox.Validated
        param2 = "select action_code from actions where action_name = '" & actions_ComboBox.Text & "'"
        Dim action_ds As DataSet = get_dataset("Complaints", param2)
        action_code = action_ds.Tables(0).Rows(0).Item(0)
        'Me.ActionsTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Actions)
        'action_code = PraiseAndComplaintsSQLDataSet.Actions.Rows(actions_ComboBox.SelectedIndex).Item(0)
    End Sub

    Private Sub addbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addbtn.Click
        Try
            OpenFileDialog1.FileName = ""
            OpenFileDialog1.Filter = "Text,word or tif|*.txt; *.doc;*.tif"
            If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                Dim fname As String = OpenFileDialog1.FileName
                Dim dirname As String = comp_no
                save_document(fname, dirname)
                doc_textbox.Text = populate_document_combobox_upd()
                Dim f2name As String = ""
                Dim idx As Integer
                Dim flen As Integer = Microsoft.VisualBasic.Len(fname)
                For idx = flen To 1 Step -1
                    If Mid(fname, idx, 1) = "\" Then
                        f2name = Microsoft.VisualBasic.Right(fname, flen - idx)
                        Exit For
                    End If
                Next
                log_text = "Document added " & f2name
                log_type = "Document"
                add_log(log_type, log_text)
                doc_change_made = True

                If doc_textbox.Text = 0 Then
                    doc_ListBox.Visible = False
                Else
                    doc_ListBox.Visible = True
                End If
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub Comp_responseTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Comp_responseTextBox.TextChanged

    End Sub

    Private Sub stage_ComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage_ComboBox.SelectedIndexChanged

    End Sub

    Private Sub stage_ComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage_ComboBox.Validated
        stage_no = Microsoft.VisualBasic.Right(stage_ComboBox.Text, 1)
        If stage_no = 2 Then
            stage2_start_datepicker.Visible = True
            If orig_stage_no = 1 Then
                stage2_start_date = Now
                stage2_start_datepicker.Text = Now
                stage2_completed_by = 0
                stage2_completed_date = Now
                stage2_completed_datepicker.Text = Now
            Else
                If orig_stage_no > 2 Then
                    stage2_completed_datepicker.Visible = True
                    stage2_completed_by_combobox.Visible = True
                End If
            End If
            Comp_textTextBox.Enabled = True
        ElseIf stage_no = 3 Then
            stage3_start_datepicker.Visible = True
            If orig_stage_no = 2 Then
                stage3_start_date = Now
                stage3_start_datepicker.Text = Now
                stage3_completed_by = 0
                stage3_completed_date = Now
                stage3_completed_datepicker.Text = Now
            End If
            Comp_textTextBox.Enabled = True
        End If

    End Sub

    Private Sub cor_nameComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cor_nameComboBox.SelectedIndexChanged

    End Sub

    Private Sub cor_nameComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cor_nameComboBox.Validated
        'cor_code = PraiseAndComplaintsSQLDataSet.Corrective_actions.Rows(cor_nameComboBox.SelectedIndex).Item(0)
        Dim cor_name As String = cor_nameComboBox.Text
        param2 = "select cor_code from [Corrective actions] where cor_name = '" & cor_name & " '"
        Dim cor_ds As DataSet = get_dataset("Complaints", param2)
        cor_code = cor_ds.Tables(0).Rows(0).Item(0)
    End Sub

    Private Sub hold_name_ComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles hold_name_ComboBox.Validated
        Try
            'hold_code = PraiseAndComplaintsSQLDataSet.Actions.Rows(hold_name_ComboBox.SelectedIndex).Item(0)
            hold_name = hold_name_ComboBox.SelectedItem
            param2 = "select hold_code from Hold_reason where hold_name = '" & hold_name & "'"
            Dim hold_ds As DataSet = get_dataset("Complaints", param2)
            hold_code = hold_ds.Tables(0).Rows(0).Item(0)
        Catch
            hold_code = 1
        End Try
    End Sub

    Private Sub doc_ListBox_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles doc_ListBox.DoubleClick
        file_path = "r:\complaints\" & comp_no & "\" & doc_ListBox.Text
        If env_str <> "Prod" Then
            file_path = "r:\complaints\Test\" & comp_no & "\" & doc_ListBox.Text
        End If
        If Microsoft.VisualBasic.Right(doc_ListBox.Text, 3) = "tif" Then
            tiffrm.ShowDialog()
        Else
            Dim strWordpadFilename As String = "C:\program files\windows nt\accessories\wordpad " & """" & Trim(file_path) & """"
            ' start up wordpad & display the current file being imported
            Dim RetVal As Integer = Shell(strWordpadFilename, 1)    ' Run wordpad.
        End If

    End Sub

    Private Sub deldocbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles deldocbtn.Click
        deldocfrm.ShowDialog()
        'no of documents
        doc_textbox.Text = populate_document_combobox_upd()

        If doc_textbox.Text = 0 Then
            doc_ListBox.Visible = False
        Else
            doc_ListBox.Visible = True
        End If
    End Sub


    Private Sub compbycombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles compbycombobox.Validated
        'Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
        param2 = "select inv_code from investigators where inv_text = '" & compbycombobox.Text & "'"
        Dim inv_ds As DataSet = get_dataset("Complaints", param2)
        Try
            'compby_code = PraiseAndComplaintsSQLDataSet.Investigators.Rows(compbycombobox.SelectedIndex).Item(0)
            compby_code = inv_ds.Tables(0).Rows(0).Item(0)
        Catch
            MsgBox("Completed by person not found")
        End Try

    End Sub

    Private Sub old_comp_notextbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles old_comp_notextbox.Validating
        ErrorProvider1.SetError(old_comp_notextbox, "")
        If Microsoft.VisualBasic.Len(Trim(old_comp_notextbox.Text)) = 0 Then
            old_comp_no = 0
        Else
            Try
                old_comp_no = old_comp_notextbox.Text
            Catch ex As Exception
                ErrorProvider1.SetError(old_comp_notextbox, "Old complaint no must be numeric")
                e.Cancel = True
                Exit Sub
            End Try
            If old_comp_no >= 9999 Then
                ErrorProvider1.SetError(old_comp_notextbox, "Old complaint no must be less than 9999")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub costs_canc_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles costs_cancel_tbox.TextChanged

    End Sub

    Private Sub costs_cancel_tbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles costs_cancel_tbox.Validated
        If costs_cancel > 0 Then
            costs_reasonbtn.Visible = True
        Else
            costs_reasonbtn.Visible = False
            cancel_costs_reason = ""
            cancel_costs_reason_change = True
        End If
    End Sub

    Private Sub costs_canc_tbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles costs_cancel_tbox.Validating
        ErrorProvider1.SetError(costs_cancel_tbox, "")
        If Microsoft.VisualBasic.Len(Trim(costs_cancel_tbox.Text)) = 0 Then
            costs_cancel = 0
        Else
            Try
                costs_cancel = costs_cancel_tbox.Text
            Catch ex As Exception
                ErrorProvider1.SetError(costs_cancel_tbox, "Cancelled costs must be numeric")
                e.Cancel = True
            End Try
        End If

    End Sub

    Private Sub compensation_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles compensation_tbox.TextChanged

    End Sub

    Private Sub compensation_tbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles compensation_tbox.Validating
        ErrorProvider1.SetError(compensation_tbox, "")
        If Microsoft.VisualBasic.Len(Trim(compensation_tbox.Text)) = 0 Then
            compensation = 0
        Else
            Try
                compensation = compensation_tbox.Text
            Catch ex As Exception
                ErrorProvider1.SetError(compensation_tbox, "Compensation must be numeric")
                e.Cancel = True
            End Try
        End If
    End Sub

    Private Sub respcbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles respcbox.CheckedChanged

    End Sub

    Private Sub respcbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles respcbox.Validated
        If respcbox.Checked = True Then
            resp_forgotten = 1
        Else
            resp_forgotten = 0
        End If
    End Sub

    Private Sub stage2_completed_by_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage2_completed_by_combobox.SelectedIndexChanged

    End Sub

    Private Sub stage2_completed_by_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage2_completed_by_combobox.Validated
        If stage2_completed_by_combobox.SelectedIndex = -1 Then
            Exit Sub
        End If
        stage2_completed_by = PraiseAndComplaintsSQLDataSet.Investigators.Rows(stage2_completed_by_combobox.SelectedIndex).Item(0)
        If stage2_completed_by > 0 Then
            stage2_completed_datepicker.Visible = True
            stage2_completed_date = Now
        End If
    End Sub

    Private Sub stage2_completed_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage2_completed_datepicker.Validated
        stage2_completed_date = stage2_completed_datepicker.Text

    End Sub

    Private Sub stage2_completed_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage2_completed_datepicker.Validating
        If stage2_completed_datepicker.Text > Now Then
            ErrorProvider1.SetError(stage2_gbox, "Completed date must not be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub stage2_completed_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage2_completed_datepicker.ValueChanged

    End Sub

    Private Sub stage2_start_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage2_start_datepicker.Validated
        stage2_start_date = stage2_start_datepicker.Text
    End Sub

    Private Sub stage2_start_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage2_start_datepicker.Validating
        ErrorProvider1.SetError(stage2_gbox, "")
        If stage2_start_datepicker.Text > Now Then
            ErrorProvider1.SetError(stage2_gbox, "Start date must not be in the future")
            e.Cancel = True
        ElseIf CDate(stage2_start_datepicker.Text) < CDate(Comp_dateTextBox.Text) Then
            ErrorProvider1.SetError(stage2_gbox, "Start date must not be before received date")
            e.Cancel = True
        End If
    End Sub

    Private Sub stage2_start_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage2_start_datepicker.ValueChanged

    End Sub

    Private Sub GroupBox2_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles GroupBox2.Enter

    End Sub

    Private Sub compbycombobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles compbycombobox.SelectedIndexChanged

    End Sub

    Private Sub stage2_completed_by_combobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage2_completed_by_combobox.Validating
        If stage2_completed_by_combobox.Text = "Not yet known" And orig_stage2_completed_by = 0 Then
            ErrorProvider1.SetError(stage2_completed_by_combobox, "Please select who completed stage 2")
            e.Cancel = True
            Exit Sub
        End If
    End Sub

    Private Sub gender_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gender_combobox.SelectedIndexChanged

    End Sub

    Private Sub gender_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles gender_combobox.Validated
        gender = gender_combobox.Text
    End Sub

    Private Sub eth_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles eth_combobox.SelectedIndexChanged

    End Sub

    Private Sub eth_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles eth_combobox.Validated
        ethnicity_desc = eth_combobox.Text
    End Sub


    Private Sub ack_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ack_cbox.CheckedChanged

    End Sub

    Private Sub ack_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles ack_cbox.Validated
        If ack_cbox.Checked Then
            ack_datepicker.Visible = True
            ack_datepicker.Value = Now
        Else
            ack_datepicker.Visible = False
            ack_date = CDate("1800,1,1")
        End If
    End Sub

    Private Sub ack_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles ack_datepicker.Validated
        ack_date = ack_datepicker.Value
    End Sub

    Private Sub ack_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ack_datepicker.Validating
        ErrorProvider1.SetError(ack_gbox, "")
        If ack_datepicker.Text > Now Then
            ErrorProvider1.SetError(ack_gbox, "Start date must not be in the future")
            e.Cancel = True
        ElseIf CDate(ack_datepicker.Text) < CDate(Comp_dateTextBox.Text) Then
            ErrorProvider1.SetError(ack_gbox, "Start date must not be before received date")
            e.Cancel = True
        End If
    End Sub

    Private Sub holding_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles holding_datepicker.Validating
        ErrorProvider1.SetError(holding_gbox, "")
        If holding_datepicker.Text > Now Then
            ErrorProvider1.SetError(holding_gbox, "Start date must not be in the future")
            e.Cancel = True
        ElseIf CDate(holding_datepicker.Text) < CDate(Comp_dateTextBox.Text) Then
            ErrorProvider1.SetError(holding_gbox, "Start date must not be before received date")
            e.Cancel = True
        End If
    End Sub

    Private Sub holding_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles holding_cbox.Validated
        If holding_cbox.Checked Then
            holding_datepicker.Visible = True
            holding_datepicker.Value = Now
        Else
            holding_datepicker.Visible = False
            holding_date = CDate("1800,1,1")
        End If
    End Sub

    Private Sub holding_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles holding_datepicker.ValueChanged

    End Sub

    Private Sub ack_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ack_datepicker.ValueChanged

    End Sub

    Private Sub stage2_ack_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage2_ack_cbox.CheckedChanged

    End Sub

    Private Sub stage2_ack_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage2_ack_cbox.Validated
        If stage2_ack_cbox.Checked Then
            stage2_ack_datepicker.Visible = True
            stage2_ack_datepicker.Value = Now
        Else
            stage2_ack_datepicker.Visible = False
            stage2_ack_date = CDate("1800,1,1")
        End If
    End Sub

    Private Sub holding_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles holding_cbox.CheckedChanged

    End Sub

    Private Sub stage2_holding_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage2_holding_cbox.CheckedChanged

    End Sub

    Private Sub stage2_holding_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage2_holding_cbox.Validated
        If stage2_holding_cbox.Checked Then
            stage2_holding_datepicker.Visible = True
            stage2_holding_datepicker.Value = Now
        Else
            stage2_holding_datepicker.Visible = False
            stage2_holding_date = CDate("1800,1,1")
        End If
    End Sub

    Private Sub stage3_start_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage3_start_datepicker.Validated
        stage3_start_date = stage3_start_datepicker.Text
    End Sub

    Private Sub stage3_start_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage3_start_datepicker.Validating
        ErrorProvider1.SetError(stage3_gbox, "")
        If stage3_start_datepicker.Text > Now Then
            ErrorProvider1.SetError(stage3_gbox, "Start date must not be in the future")
            e.Cancel = True
        ElseIf CDate(stage3_start_datepicker.Text) < CDate(stage2_completed_datepicker.Text) Then
            ErrorProvider1.SetError(stage3_gbox, "Start date must not be before stage 2 completed date")
            e.Cancel = True
        End If
    End Sub

    Private Sub stage3_completed_by_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage3_completed_by_combobox.Validated
        If stage3_completed_by_combobox.SelectedIndex = -1 Then
            Exit Sub
        End If
        stage3_completed_by = PraiseAndComplaintsSQLDataSet.Investigators.Rows(stage3_completed_by_combobox.SelectedIndex).Item(0)
        If stage3_completed_by > 0 Then
            stage3_completed_datepicker.Visible = True
            stage3_completed_date = Now
        End If
    End Sub

    Private Sub stage3_completed_by_combobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage3_completed_by_combobox.Validating
        If stage3_completed_by_combobox.Text = "Not yet known" And orig_stage3_completed_by = 0 Then
            ErrorProvider1.SetError(stage3_completed_by_combobox, "Please select who completed stage 3")
            e.Cancel = True
            Exit Sub
        End If
    End Sub

    Private Sub stage3_completed_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage3_completed_datepicker.Validated
        stage3_completed_date = stage3_completed_datepicker.Text
    End Sub

    Private Sub stage3_completed_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles stage3_completed_datepicker.Validating
        If stage3_completed_datepicker.Text > Now Then
            ErrorProvider1.SetError(stage3_gbox, "Completed date must not be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub stage3_start_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage3_start_datepicker.ValueChanged

    End Sub

    Private Sub comp_against2_codecombobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comp_against2_codecombobox.SelectedIndexChanged

    End Sub

    Private Sub comp_against2_codecombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles comp_against2_codecombobox.Validated
        Try
            If comp_against2_codecombobox.SelectedIndex = -1 Then
                Exit Sub
            End If
            Dim idx As Integer
            agent2_combobox.Items.Clear()
            agent2_combobox.Text = ""
            If comp_cmpny_no > 0 Then
                param1 = "Complaints"
                param2 = "select agnt_code, agnt_name from Complaint_Agents" & _
                " order by agnt_code"
                Dim agnt_ds As DataSet = get_dataset(param1, param2)
                agent2_rows = agnt_ds.Tables(0).Rows.Count - 1
                ReDim bailiff2_table(no_of_rows, no_of_rows)
                agent2_combobox.Items.Clear()
                For idx = 0 To agent2_rows
                    agent2_combobox.Items.Add(agnt_ds.Tables(0).Rows(idx).Item(1))
                    bailiff2_table(idx, 1) = agnt_ds.Tables(0).Rows(idx).Item(0)
                    bailiff2_table(idx, 2) = agnt_ds.Tables(0).Rows(idx).Item(1)
                Next
            Else
                
                Dim where_clause As String
                If Microsoft.VisualBasic.Left(comp_against2_codecombobox.Text, 2) = "FC" Then
                    where_clause = " where agent_type = 'B' and internalExternal = 'I' and branchId <> 10"
                ElseIf comp_against2_codecombobox.Text = "Van Bailiff" Or _
                comp_against2_codecombobox.Text = "VB" Then
                    where_clause = " where agent_type = 'B' and internalExternal = 'E' and branchId <> 10"
                ElseIf comp_against2_codecombobox.Text = "High Court" Then
                    where_clause = " where agent_type = 'B' and (internalExternal = 'I' or internalExternal = 'E') and branchId <> 10"
                ElseIf comp_against2_codecombobox.Text = "Marston" Then
                    where_clause = " where agent_type = 'B' "
                ElseIf comp_against2_codecombobox.Text = "Contact Centre" Then
                    where_clause = " where (agent_type ='P' or agent_type = 'A') "
                Else
                    where_clause = " where (agent_type ='P' or agent_type = 'A')  and branchId <> 10"
                End If
                If comp_against2_codecombobox.Text = "Enforcement Officer" Then
                    where_clause = " where agent_type = 'B' and branchId <> 10"
                End If
                populate_bailiff2_table(where_clause)

                For idx = 1 To agent2_rows
                    agent2_combobox.Items.Add(bailiff2_table(idx, 2))
                Next
                agent2_combobox.Items.Add("Various")
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub agent2_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agent2_combobox.SelectedIndexChanged

    End Sub

    Private Sub agent2_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles agent2_combobox.Validated
        Dim idx As Integer
        agent2_no = 0
        If agent2_combobox.SelectedItem = "Various" Then
            agent2_no = 9999
        Else
            For idx = 0 To agent2_rows
                If bailiff2_table(idx, 2) = agent2_combobox.SelectedItem Then
                    agent2_no = bailiff2_table(idx, 1)
                    Exit For
                End If
            Next
        End If
        orig_against2_code = against2_code
    End Sub

    Private Sub Comp_dateTextBox_TextChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Comp_dateTextBox.TextChanged

    End Sub

    Private Sub Comp_dateTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Comp_dateTextBox.Validating
        ErrorProvider1.SetError(Comp_dateTextBox, "")
        If Not IsDate(Comp_dateTextBox.Text) Then
            ErrorProvider1.SetError(Comp_dateTextBox, "Invalid date")
            e.Cancel = True
        End If
    End Sub

    Private Sub legalcbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles legalcbox.CheckedChanged
        If legalcbox.Checked = True Then
            legal_flag = "Y"
            legalgbox.Visible = True
            If referred_to_solicitor = CDate("1800,1,1") Then
                solicitor_cbox.Checked = False
                upd_solicitor_datepicker.Visible = False
            Else
                solicitor_cbox.Checked = True
                upd_solicitor_datepicker.Visible = True
            End If
        Else
            legal_flag = "N"
            legalgbox.Visible = False
        End If
    End Sub


    Private Sub legalcbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles legalcbox.Validated


    End Sub

    Private Sub solicitor_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles solicitor_cbox.CheckedChanged
        upd_solicitor_datepicker.Visible = solicitor_cbox.Checked
        If solicitor_cbox.Checked = False Then
            upd_solicitor_datepicker.Value = CDate("1800,1,1")
            referred_to_solicitor = upd_solicitor_datepicker.Value
        ElseIf initial_load = False Then
            upd_solicitor_datepicker.Value = Now
            referred_to_solicitor = upd_solicitor_datepicker.Value
        End If

    End Sub

    Private Sub solicitor_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles solicitor_cbox.Validated

    End Sub


    Private Sub inscbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles upd_insurercbox.Validated

    End Sub

    Private Sub solicitor_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles upd_solicitor_datepicker.Validated
        referred_to_solicitor = upd_solicitor_datepicker.Value
    End Sub

    Private Sub solicitor_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles upd_solicitor_datepicker.Validating
        ErrorProvider1.SetError(upd_solicitor_datepicker, "")
        If Format(CDate(upd_solicitor_datepicker.Text), "yyyy-MM-dd") < Format(CDate(Comp_dateTextBox.Text), "yyyy-MM-dd") Then
            ErrorProvider1.SetError(upd_solicitor_datepicker, "Date can't be before entered date")
            e.Cancel = True
        ElseIf CDate(upd_solicitor_datepicker.Text) > Now Then
            ErrorProvider1.SetError(upd_solicitor_datepicker, "Date can't be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub solicitor_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_solicitor_datepicker.ValueChanged

    End Sub

    Private Sub ins_datepicker_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles upd_ins_datepicker.Validated
        referred_to_insurer = upd_ins_datepicker.Value
    End Sub

    Private Sub ins_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles upd_ins_datepicker.Validating
        ErrorProvider1.SetError(upd_ins_datepicker, "")
        If Format(CDate(upd_ins_datepicker.Text), "yyyy-MM-dd") < Format(CDate(Comp_dateTextBox.Text), "yyyy-MM-dd") Then
            ErrorProvider1.SetError(upd_ins_datepicker, "Date can't be before entered date")
            e.Cancel = True
        ElseIf upd_ins_datepicker.Text > Now Then
            ErrorProvider1.SetError(upd_ins_datepicker, "Date can't be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub ins_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_ins_datepicker.ValueChanged

    End Sub

    Private Sub monetary_risktbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles monetary_risktbox.TextChanged

    End Sub

    Private Sub monetary_risktbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles monetary_risktbox.Validating
        ErrorProvider1.SetError(monetary_risktbox, "")
        If Trim(monetary_risktbox.Text).Length = 0 Then
            monetary_risk = -999
        Else
            If Not IsNumeric(monetary_risktbox.Text) Then
                ErrorProvider1.SetError(monetary_risktbox, "Value must be numeric")
                e.Cancel = True
            ElseIf monetary_risktbox.Text < 0 Then
                ErrorProvider1.SetError(monetary_risktbox, "Value must be positive")
                e.Cancel = True
            Else
                monetary_risk = monetary_risktbox.Text
            End If
        End If
    End Sub

    Private Sub costs_reasonbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles costs_reasonbtn.Click
        costs_reason_mode = "U"
        costs_reasonfrm.ShowDialog()
    End Sub

    Private Sub stage1_esc_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage1_esc_combobox.SelectedIndexChanged

    End Sub

    Private Sub stage1_esc_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage1_esc_combobox.Validated
        stage1_esc_code = stage1_esc_combobox.SelectedIndex + 3
    End Sub

    Private Sub stage2_esc_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles stage2_esc_combobox.SelectedIndexChanged

    End Sub

    Private Sub stage2_esc_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles stage2_esc_combobox.Validated
        stage2_esc_code = stage2_esc_combobox.SelectedIndex + 3
    End Sub

    Private Sub category_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles category_cbox.SelectedIndexChanged

    End Sub

    Private Sub category_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles category_cbox.Validated
        category = category_cbox.SelectedIndex
    End Sub

    Private Sub branch_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles branch_cbox.SelectedIndexChanged

    End Sub

    Private Sub branch_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles branch_cbox.Validated
        Dim idx As Integer
        Dim sel_branch_name As String = branch_cbox.SelectedItem
        param2 = "select branch_code from Complaint_branches" & _
        " where branch_name = '" & sel_branch_name & "'" & _
        " and branch_cmpny_no = " & comp_cmpny_no
        Dim br_ds As DataSet = get_dataset("Complaints", param2)
        Dim sel_branch_no As Integer = br_ds.Tables(0).Rows(0).Item(0)
        param2 = "select against_code, against_text from complaint_against where against_branch_no = " & branch_no
        Dim against_ds As DataSet = get_dataset("Complaints", param2)
        'Me.Complaint_againstTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Complaint_against, branch_no)
        comp_against_codeComboBox.Items.Clear()
        comp_against2_codecombobox.Items.Clear()
        comp_against_codeComboBox.SelectedIndex = -1
        comp_against2_codecombobox.SelectedIndex = -1
        agentComboBox.Items.Clear()
        agent2_combobox.Items.Clear()
        agent_no = 0
        agent2_no = 0
        against_code = 0
        against2_code = 0
        idx = 0
        'For Each row In PraiseAndComplaintsSQLDataSet.Complaint_against.Rows
        For Each row In against_ds.Tables(0).Rows
            comp_against_codeComboBox.Items.Add(against_ds.Tables(0).Rows(idx).Item(1))
            comp_against2_codecombobox.Items.Add(against_ds.Tables(0).Rows(idx).Item(1))
            idx += 1
        Next
    End Sub

    Private Sub part_founded_rbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles part_founded_rbtn.CheckedChanged
        part_founded_cbox.Visible = part_founded_rbtn.Checked
        If Not part_founded_cbox.Visible Then
            Exit Sub
        End If
        Dim idx As Integer
        populate_pf_bailiff_table()
        part_founded_cbox.Items.Clear()
        For idx = 1 To pf_agent_rows
            part_founded_cbox.Items.Add(pf_bailiff_table(idx, 2))
        Next
        part_founded_cbox.Items.Add("Various")
    End Sub

    Private Sub part_founded_rbtn_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles part_founded_rbtn.Validated

    End Sub

    Private Sub foundedrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles foundedrbtn.CheckedChanged

    End Sub

    Private Sub part_founded_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles part_founded_cbox.SelectedIndexChanged

    End Sub

    Private Sub part_founded_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles part_founded_cbox.Validated
        Dim idx As Integer

        selected_pf_agent_no = 0
        If part_founded_cbox.SelectedItem = "Various" Then
            selected_pf_agent_no = 9999
        Else
            For idx = 1 To agent_rows
                If pf_bailiff_table(idx, 2) = part_founded_cbox.SelectedItem Then
                    selected_pf_agent_no = pf_bailiff_table(idx, 1)
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub act_not_rbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_act_not_rbtn.CheckedChanged
        If upd_act_not_rbtn.Checked Then
            insurance_type_no = 1
        End If
    End Sub

    Private Sub bord_rbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_bord_rbtn.CheckedChanged
        If upd_bord_rbtn.Checked Then
            insurance_type_no = 2
        End If
    End Sub

    Private Sub unknownrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_unknownrbtn.CheckedChanged
        If upd_unknownrbtn.Checked Then
            insurance_type_no = 0
        End If
    End Sub

    Private Sub monetary_risk_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles monetary_risk_tbox.TextChanged

    End Sub

    Private Sub monetary_risk_tbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles monetary_risk_tbox.Validated

    End Sub

    Private Sub monetary_risk_tbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles monetary_risk_tbox.Validating
        ErrorProvider1.SetError(monetary_risktbox, "")
        If Microsoft.VisualBasic.Len(Trim(monetary_risk_tbox.Text)) = 0 Then
            monetary_risk = -999
        Else
            If Not IsNumeric(monetary_risk_tbox.Text) Then
                ErrorProvider1.SetError(monetary_risk_tbox, "Please enter valid amount")
                e.Cancel = True
            Else
                monetary_risk = monetary_risk_tbox.Text
                If monetary_risk < 0 Then
                    ErrorProvider1.SetError(monetary_risk_tbox, "Please enter positive amount")
                    e.Cancel = True
                End If
            End If
        End If
    End Sub

    Private Sub inscbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_inscbox.CheckedChanged
        upd_insurercbox.Visible = upd_inscbox.Checked
        upd_insurercbox.Checked = False
        upd_liability_cbox.Visible = upd_inscbox.Checked
        upd_ins_datepicker.Visible = False
        ins_panel.Visible = upd_inscbox.Checked
        pi_cbox.Visible = upd_inscbox.Checked
        pl_cbox.Visible = upd_inscbox.Checked
        pi_cbox.Checked = False
        pl_cbox.Checked = False
        upd_unknownrbtn.Checked = True
        upd_liability_cbox.Checked = False
        upd_ins_datepicker.Value = CDate("1800,1,1")
        If upd_inscbox.Checked = False Then
            referred_to_insurer = upd_ins_datepicker.Value
            insurance_type_no = -1
            pi_cbox.Checked = False
            pl_cbox.Checked = False
        ElseIf initial_load = False Then
            'upd_ins_datepicker.Value = Now
            referred_to_insurer = upd_ins_datepicker.Value
            insurance_type_no = 0

        End If
    End Sub

    Private Sub liability_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_liability_cbox.CheckedChanged

    End Sub

    Private Sub upd_insurercbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_insurercbox.CheckedChanged
        upd_ins_datepicker.Visible = upd_insurercbox.Checked
        If upd_insurercbox.Checked Then
            upd_ins_datepicker.Value = Now
        Else
            upd_ins_datepicker.Value = CDate("1800,1,1")
        End If
        referred_to_insurer = upd_ins_datepicker.Value
    End Sub

    Private Sub feedback_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles feedback_cbox.SelectedIndexChanged
        feedback_no = feedback_cbox.SelectedIndex
    End Sub

    Private Sub upd_type_code_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_type_code_cbox.SelectedIndexChanged
        type_code = upd_type_code_cbox.SelectedIndex
    End Sub

    Private Sub comprbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comprbtn.CheckedChanged
        If comprbtn.Checked = True Then
            completed = True
            compdatetimepicker.Visible = True
            compbycombobox.Visible = True
            If orig_completed = False Then
                compdatetimepicker.Value = Now
                compbycombobox.SelectedIndex = -1
                compbycombobox.Text = "Not yet known"
            End If
            param2 = "select inv_text from investigators where inv_deleted = 0 order by inv_text"
            Dim inv_ds As DataSet = get_dataset("Complaints", param2)
            'Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
            Dim row As DataRow
            Dim idx As Integer
            compbycombobox.Items.Clear()
            'For Each row In Me.PraiseAndComplaintsSQLDataSet.Investigators.Rows
            For Each row In inv_ds.Tables(0).Rows
                'compbycombobox.Items.Add(PraiseAndComplaintsSQLDataSet.Investigators.Rows(idx).Item(1))
                compbycombobox.Items.Add(inv_ds.Tables(0).Rows(idx).Item(0))
                idx += 1
            Next
        Else
            completed = False
            compdatetimepicker.Visible = False
            compbycombobox.Visible = False
        End If
    End Sub


    Private Sub s1_holdbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles s1_holdbtn.Click
        hold_stage_no = 1
        holdsfrm.ShowDialog()
    End Sub

    Private Sub s2_holdbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles s2_holdbtn.Click
        hold_stage_no = 2
        holdsfrm.ShowDialog()
    End Sub

    Private Sub s3_hold_btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles s3_hold_btn.Click
        hold_stage_no = 3
        holdsfrm.ShowDialog()
    End Sub

    Private Sub entered_byTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles entered_byTextBox.TextChanged

    End Sub

    Private Sub pi_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pi_cbox.CheckedChanged
        If pi_cbox.Checked Then
            prof_indemnity_no = 1
        Else
            prof_indemnity_no = 0
        End If
    End Sub

    Private Sub pl_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pl_cbox.CheckedChanged
        If pl_cbox.Checked Then
            public_liability_no = 1
        Else
            public_liability_no = 0
        End If
    End Sub

    Private Sub hold_name_ComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hold_name_ComboBox.SelectedIndexChanged

    End Sub

    Private Sub comp_against2_codecombobox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles comp_against2_codecombobox.Validating
        If comp_against2_codecombobox.SelectedIndex = -1 Then
            comp_against2_codecombobox.Text = ""
            agent2_combobox.Items.Clear()
            against2_code = 0
            agent2_no = 0
            Exit Sub
        End If
        param2 = "select against_code from complaint_against where against_text = '" & comp_against2_codecombobox.Text & "'"
        Dim against_ds As DataSet = get_dataset("Complaints", param2)
        If no_of_rows = 0 Then
            ErrorProvider1.SetError(comp_against2_codecombobox, "Invalid selection")
            e.Cancel = True
        Else
            against2_code = against_ds.Tables(0).Rows(0).Item(0)
        End If

    End Sub

    Private Sub doc_ListBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles doc_ListBox.SelectedIndexChanged

    End Sub
End Class
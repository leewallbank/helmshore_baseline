<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class updmastfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.catbtn = New System.Windows.Forms.Button
        Me.recbtn = New System.Windows.Forms.Button
        Me.recvdbtn = New System.Windows.Forms.Button
        Me.againstbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.investbtn = New System.Windows.Forms.Button
        Me.actionsbtn = New System.Windows.Forms.Button
        Me.resetbtn = New System.Windows.Forms.Button
        Me.corractbtn = New System.Windows.Forms.Button
        Me.cl_turnbtn = New System.Windows.Forms.Button
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.LogBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LogTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.LogTableAdapter
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.notifybtn = New System.Windows.Forms.Button
        Me.cl2_btn = New System.Windows.Forms.Button
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LogBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'catbtn
        '
        Me.catbtn.Location = New System.Drawing.Point(67, 35)
        Me.catbtn.Name = "catbtn"
        Me.catbtn.Size = New System.Drawing.Size(123, 23)
        Me.catbtn.TabIndex = 0
        Me.catbtn.Text = "&Complaint Categories"
        Me.catbtn.UseVisualStyleBackColor = True
        '
        'recbtn
        '
        Me.recbtn.Location = New System.Drawing.Point(267, 35)
        Me.recbtn.Name = "recbtn"
        Me.recbtn.Size = New System.Drawing.Size(123, 23)
        Me.recbtn.TabIndex = 1
        Me.recbtn.Text = "Receipt &Types"
        Me.recbtn.UseVisualStyleBackColor = True
        '
        'recvdbtn
        '
        Me.recvdbtn.Location = New System.Drawing.Point(465, 35)
        Me.recvdbtn.Name = "recvdbtn"
        Me.recvdbtn.Size = New System.Drawing.Size(123, 23)
        Me.recvdbtn.TabIndex = 2
        Me.recvdbtn.Text = "Co&mplainants"
        Me.recvdbtn.UseVisualStyleBackColor = True
        '
        'againstbtn
        '
        Me.againstbtn.Location = New System.Drawing.Point(67, 96)
        Me.againstbtn.Name = "againstbtn"
        Me.againstbtn.Size = New System.Drawing.Size(123, 23)
        Me.againstbtn.TabIndex = 3
        Me.againstbtn.Text = "Complaint &Against"
        Me.againstbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(513, 395)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 11
        Me.exitbtn.Text = "&Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'investbtn
        '
        Me.investbtn.Location = New System.Drawing.Point(267, 96)
        Me.investbtn.Name = "investbtn"
        Me.investbtn.Size = New System.Drawing.Size(123, 23)
        Me.investbtn.TabIndex = 4
        Me.investbtn.Text = "Complaints &Officers"
        Me.investbtn.UseVisualStyleBackColor = True
        '
        'actionsbtn
        '
        Me.actionsbtn.Location = New System.Drawing.Point(465, 96)
        Me.actionsbtn.Name = "actionsbtn"
        Me.actionsbtn.Size = New System.Drawing.Size(123, 23)
        Me.actionsbtn.TabIndex = 5
        Me.actionsbtn.Text = "Actions to &Resolve"
        Me.actionsbtn.UseVisualStyleBackColor = True
        '
        'resetbtn
        '
        Me.resetbtn.Location = New System.Drawing.Point(465, 169)
        Me.resetbtn.Name = "resetbtn"
        Me.resetbtn.Size = New System.Drawing.Size(123, 23)
        Me.resetbtn.TabIndex = 8
        Me.resetbtn.Text = "Reset &Password"
        Me.resetbtn.UseVisualStyleBackColor = True
        '
        'corractbtn
        '
        Me.corractbtn.Location = New System.Drawing.Point(67, 169)
        Me.corractbtn.Name = "corractbtn"
        Me.corractbtn.Size = New System.Drawing.Size(123, 23)
        Me.corractbtn.TabIndex = 6
        Me.corractbtn.Text = "Correcti&ve Actions"
        Me.corractbtn.UseVisualStyleBackColor = True
        '
        'cl_turnbtn
        '
        Me.cl_turnbtn.Location = New System.Drawing.Point(267, 169)
        Me.cl_turnbtn.Name = "cl_turnbtn"
        Me.cl_turnbtn.Size = New System.Drawing.Size(123, 23)
        Me.cl_turnbtn.TabIndex = 7
        Me.cl_turnbtn.Text = "Client T&urnaround"
        Me.cl_turnbtn.UseVisualStyleBackColor = True
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LogBindingSource
        '
        Me.LogBindingSource.DataMember = "Log"
        Me.LogBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'LogTableAdapter
        '
        Me.LogTableAdapter.ClearBeforeFill = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.BackColor = System.Drawing.Color.CadetBlue
        Me.ProgressBar1.Location = New System.Drawing.Point(34, 395)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 11
        Me.ProgressBar1.Visible = False
        '
        'notifybtn
        '
        Me.notifybtn.Location = New System.Drawing.Point(126, 260)
        Me.notifybtn.Name = "notifybtn"
        Me.notifybtn.Size = New System.Drawing.Size(180, 23)
        Me.notifybtn.TabIndex = 9
        Me.notifybtn.Text = "Clients to Notify &When complete"
        Me.notifybtn.UseVisualStyleBackColor = True
        '
        'cl2_btn
        '
        Me.cl2_btn.Location = New System.Drawing.Point(356, 260)
        Me.cl2_btn.Name = "cl2_btn"
        Me.cl2_btn.Size = New System.Drawing.Size(176, 23)
        Me.cl2_btn.TabIndex = 10
        Me.cl2_btn.Text = "Clients to &Notify new complaints"
        Me.cl2_btn.UseVisualStyleBackColor = True
        '
        'updmastfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(721, 458)
        Me.Controls.Add(Me.cl2_btn)
        Me.Controls.Add(Me.notifybtn)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.cl_turnbtn)
        Me.Controls.Add(Me.corractbtn)
        Me.Controls.Add(Me.resetbtn)
        Me.Controls.Add(Me.actionsbtn)
        Me.Controls.Add(Me.investbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.againstbtn)
        Me.Controls.Add(Me.recvdbtn)
        Me.Controls.Add(Me.recbtn)
        Me.Controls.Add(Me.catbtn)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "updmastfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Update Master Tables"
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LogBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents catbtn As System.Windows.Forms.Button
    Friend WithEvents recbtn As System.Windows.Forms.Button
    Friend WithEvents recvdbtn As System.Windows.Forms.Button
    Friend WithEvents againstbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents investbtn As System.Windows.Forms.Button
    Friend WithEvents actionsbtn As System.Windows.Forms.Button
    Friend WithEvents resetbtn As System.Windows.Forms.Button
    Friend WithEvents corractbtn As System.Windows.Forms.Button
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents LogBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LogTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.LogTableAdapter
    Friend WithEvents cl_turnbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents notifybtn As System.Windows.Forms.Button
    Friend WithEvents cl2_btn As System.Windows.Forms.Button

End Class

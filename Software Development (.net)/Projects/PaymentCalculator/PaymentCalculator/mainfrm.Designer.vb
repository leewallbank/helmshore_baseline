<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.exitbtn = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.mth3_month_tbox = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.mth3_fort_tbox = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.mth3_week_tbox = New System.Windows.Forms.TextBox
        Me.balbtn = New System.Windows.Forms.Button
        Me.debtor_lbl = New System.Windows.Forms.Label
        Me.debtorbtn = New System.Windows.Forms.Button
        Me.name_lbl = New System.Windows.Forms.Label
        Me.bal_lbl = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.mth6_week_tbox = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.mth6_fort_tbox = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.mth6_month_tbox = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.mth9_week_tbox = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.mth9_fort_tbox = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.mth9_month_tbox = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.mth12_week_tbox = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.mth12_fort_tbox = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.mth12_month_tbox = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.var_week_tbox = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.var_fort_tbox = New System.Windows.Forms.TextBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.var_month_tbox = New System.Windows.Forms.TextBox
        Me.var_lbl = New System.Windows.Forms.Label
        Me.var_ratebtn = New System.Windows.Forms.Button
        Me.Label20 = New System.Windows.Forms.Label
        Me.discount_tbox = New System.Windows.Forms.TextBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.offer_tbox = New System.Windows.Forms.TextBox
        Me.settlement_pcentbtn = New System.Windows.Forms.Button
        Me.client_name_lbl = New System.Windows.Forms.Label
        Me.displaybtn = New System.Windows.Forms.Button
        Me.sch_name_lbl = New System.Windows.Forms.Label
        Me.maintainbtn = New System.Windows.Forms.Button
        Me.discountbtn = New System.Windows.Forms.Button
        Me.settlement_notes_tbox = New System.Windows.Forms.TextBox
        Me.settle_pcent_lbl = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(823, 404)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 4
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(73, 101)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "3 Months"
        '
        'mth3_month_tbox
        '
        Me.mth3_month_tbox.Location = New System.Drawing.Point(59, 120)
        Me.mth3_month_tbox.Name = "mth3_month_tbox"
        Me.mth3_month_tbox.ReadOnly = True
        Me.mth3_month_tbox.Size = New System.Drawing.Size(83, 20)
        Me.mth3_month_tbox.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(148, 123)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Monthly"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(148, 167)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(67, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Per Fortnight"
        '
        'mth3_fort_tbox
        '
        Me.mth3_fort_tbox.Location = New System.Drawing.Point(59, 164)
        Me.mth3_fort_tbox.Name = "mth3_fort_tbox"
        Me.mth3_fort_tbox.ReadOnly = True
        Me.mth3_fort_tbox.Size = New System.Drawing.Size(83, 20)
        Me.mth3_fort_tbox.TabIndex = 8
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(148, 216)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(43, 13)
        Me.Label6.TabIndex = 11
        Me.Label6.Text = "Weekly"
        '
        'mth3_week_tbox
        '
        Me.mth3_week_tbox.Location = New System.Drawing.Point(59, 213)
        Me.mth3_week_tbox.Name = "mth3_week_tbox"
        Me.mth3_week_tbox.ReadOnly = True
        Me.mth3_week_tbox.Size = New System.Drawing.Size(83, 20)
        Me.mth3_week_tbox.TabIndex = 10
        '
        'balbtn
        '
        Me.balbtn.Location = New System.Drawing.Point(149, 34)
        Me.balbtn.Name = "balbtn"
        Me.balbtn.Size = New System.Drawing.Size(84, 23)
        Me.balbtn.TabIndex = 0
        Me.balbtn.Text = "Enter Balance"
        Me.balbtn.UseVisualStyleBackColor = True
        '
        'debtor_lbl
        '
        Me.debtor_lbl.AutoSize = True
        Me.debtor_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.debtor_lbl.Location = New System.Drawing.Point(353, 61)
        Me.debtor_lbl.Name = "debtor_lbl"
        Me.debtor_lbl.Size = New System.Drawing.Size(43, 13)
        Me.debtor_lbl.TabIndex = 14
        Me.debtor_lbl.Text = "debtor"
        '
        'debtorbtn
        '
        Me.debtorbtn.Location = New System.Drawing.Point(325, 35)
        Me.debtorbtn.Name = "debtorbtn"
        Me.debtorbtn.Size = New System.Drawing.Size(114, 23)
        Me.debtorbtn.TabIndex = 1
        Me.debtorbtn.Text = "Enter DebtorID"
        Me.debtorbtn.UseVisualStyleBackColor = True
        '
        'name_lbl
        '
        Me.name_lbl.AutoSize = True
        Me.name_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.name_lbl.Location = New System.Drawing.Point(456, 61)
        Me.name_lbl.Name = "name_lbl"
        Me.name_lbl.Size = New System.Drawing.Size(39, 13)
        Me.name_lbl.TabIndex = 15
        Me.name_lbl.Text = "Name"
        '
        'bal_lbl
        '
        Me.bal_lbl.AutoSize = True
        Me.bal_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bal_lbl.Location = New System.Drawing.Point(171, 61)
        Me.bal_lbl.Name = "bal_lbl"
        Me.bal_lbl.Size = New System.Drawing.Size(53, 13)
        Me.bal_lbl.TabIndex = 16
        Me.bal_lbl.Text = "Balance"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(369, 216)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Weekly"
        '
        'mth6_week_tbox
        '
        Me.mth6_week_tbox.Location = New System.Drawing.Point(280, 213)
        Me.mth6_week_tbox.Name = "mth6_week_tbox"
        Me.mth6_week_tbox.ReadOnly = True
        Me.mth6_week_tbox.Size = New System.Drawing.Size(83, 20)
        Me.mth6_week_tbox.TabIndex = 22
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(369, 167)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(67, 13)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Per Fortnight"
        '
        'mth6_fort_tbox
        '
        Me.mth6_fort_tbox.Location = New System.Drawing.Point(280, 164)
        Me.mth6_fort_tbox.Name = "mth6_fort_tbox"
        Me.mth6_fort_tbox.ReadOnly = True
        Me.mth6_fort_tbox.Size = New System.Drawing.Size(83, 20)
        Me.mth6_fort_tbox.TabIndex = 20
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(369, 123)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(44, 13)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Monthly"
        '
        'mth6_month_tbox
        '
        Me.mth6_month_tbox.Location = New System.Drawing.Point(280, 120)
        Me.mth6_month_tbox.Name = "mth6_month_tbox"
        Me.mth6_month_tbox.ReadOnly = True
        Me.mth6_month_tbox.Size = New System.Drawing.Size(83, 20)
        Me.mth6_month_tbox.TabIndex = 18
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(294, 101)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "6 Months"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(588, 216)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(43, 13)
        Me.Label9.TabIndex = 30
        Me.Label9.Text = "Weekly"
        '
        'mth9_week_tbox
        '
        Me.mth9_week_tbox.Location = New System.Drawing.Point(499, 213)
        Me.mth9_week_tbox.Name = "mth9_week_tbox"
        Me.mth9_week_tbox.ReadOnly = True
        Me.mth9_week_tbox.Size = New System.Drawing.Size(83, 20)
        Me.mth9_week_tbox.TabIndex = 29
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(588, 167)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(67, 13)
        Me.Label10.TabIndex = 28
        Me.Label10.Text = "Per Fortnight"
        '
        'mth9_fort_tbox
        '
        Me.mth9_fort_tbox.Location = New System.Drawing.Point(499, 164)
        Me.mth9_fort_tbox.Name = "mth9_fort_tbox"
        Me.mth9_fort_tbox.ReadOnly = True
        Me.mth9_fort_tbox.Size = New System.Drawing.Size(83, 20)
        Me.mth9_fort_tbox.TabIndex = 27
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(588, 123)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(44, 13)
        Me.Label11.TabIndex = 26
        Me.Label11.Text = "Monthly"
        '
        'mth9_month_tbox
        '
        Me.mth9_month_tbox.Location = New System.Drawing.Point(499, 120)
        Me.mth9_month_tbox.Name = "mth9_month_tbox"
        Me.mth9_month_tbox.ReadOnly = True
        Me.mth9_month_tbox.Size = New System.Drawing.Size(83, 20)
        Me.mth9_month_tbox.TabIndex = 25
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(513, 101)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(59, 13)
        Me.Label12.TabIndex = 24
        Me.Label12.Text = "9 Months"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(148, 394)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(43, 13)
        Me.Label13.TabIndex = 37
        Me.Label13.Text = "Weekly"
        '
        'mth12_week_tbox
        '
        Me.mth12_week_tbox.Location = New System.Drawing.Point(59, 391)
        Me.mth12_week_tbox.Name = "mth12_week_tbox"
        Me.mth12_week_tbox.ReadOnly = True
        Me.mth12_week_tbox.Size = New System.Drawing.Size(83, 20)
        Me.mth12_week_tbox.TabIndex = 36
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(148, 345)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(67, 13)
        Me.Label14.TabIndex = 35
        Me.Label14.Text = "Per Fortnight"
        '
        'mth12_fort_tbox
        '
        Me.mth12_fort_tbox.Location = New System.Drawing.Point(59, 342)
        Me.mth12_fort_tbox.Name = "mth12_fort_tbox"
        Me.mth12_fort_tbox.ReadOnly = True
        Me.mth12_fort_tbox.Size = New System.Drawing.Size(83, 20)
        Me.mth12_fort_tbox.TabIndex = 34
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(148, 301)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(44, 13)
        Me.Label15.TabIndex = 33
        Me.Label15.Text = "Monthly"
        '
        'mth12_month_tbox
        '
        Me.mth12_month_tbox.Location = New System.Drawing.Point(59, 298)
        Me.mth12_month_tbox.Name = "mth12_month_tbox"
        Me.mth12_month_tbox.ReadOnly = True
        Me.mth12_month_tbox.Size = New System.Drawing.Size(83, 20)
        Me.mth12_month_tbox.TabIndex = 32
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(73, 279)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(66, 13)
        Me.Label16.TabIndex = 31
        Me.Label16.Text = "12 Months"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(369, 394)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(43, 13)
        Me.Label17.TabIndex = 44
        Me.Label17.Text = "Weekly"
        '
        'var_week_tbox
        '
        Me.var_week_tbox.Location = New System.Drawing.Point(280, 391)
        Me.var_week_tbox.Name = "var_week_tbox"
        Me.var_week_tbox.ReadOnly = True
        Me.var_week_tbox.Size = New System.Drawing.Size(83, 20)
        Me.var_week_tbox.TabIndex = 43
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(369, 345)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(67, 13)
        Me.Label18.TabIndex = 42
        Me.Label18.Text = "Per Fortnight"
        '
        'var_fort_tbox
        '
        Me.var_fort_tbox.Location = New System.Drawing.Point(280, 342)
        Me.var_fort_tbox.Name = "var_fort_tbox"
        Me.var_fort_tbox.ReadOnly = True
        Me.var_fort_tbox.Size = New System.Drawing.Size(83, 20)
        Me.var_fort_tbox.TabIndex = 41
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(369, 301)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(44, 13)
        Me.Label19.TabIndex = 40
        Me.Label19.Text = "Monthly"
        '
        'var_month_tbox
        '
        Me.var_month_tbox.Location = New System.Drawing.Point(280, 298)
        Me.var_month_tbox.Name = "var_month_tbox"
        Me.var_month_tbox.ReadOnly = True
        Me.var_month_tbox.Size = New System.Drawing.Size(83, 20)
        Me.var_month_tbox.TabIndex = 39
        '
        'var_lbl
        '
        Me.var_lbl.AutoSize = True
        Me.var_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.var_lbl.Location = New System.Drawing.Point(294, 279)
        Me.var_lbl.Name = "var_lbl"
        Me.var_lbl.Size = New System.Drawing.Size(66, 13)
        Me.var_lbl.TabIndex = 38
        Me.var_lbl.Text = "24 Months"
        '
        'var_ratebtn
        '
        Me.var_ratebtn.Location = New System.Drawing.Point(266, 253)
        Me.var_ratebtn.Name = "var_ratebtn"
        Me.var_ratebtn.Size = New System.Drawing.Size(124, 23)
        Me.var_ratebtn.TabIndex = 2
        Me.var_ratebtn.Text = "Variable rate"
        Me.var_ratebtn.UseVisualStyleBackColor = True
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(529, 279)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(67, 13)
        Me.Label20.TabIndex = 45
        Me.Label20.Text = "Settlement"
        '
        'discount_tbox
        '
        Me.discount_tbox.Location = New System.Drawing.Point(558, 369)
        Me.discount_tbox.Name = "discount_tbox"
        Me.discount_tbox.ReadOnly = True
        Me.discount_tbox.Size = New System.Drawing.Size(100, 20)
        Me.discount_tbox.TabIndex = 47
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(481, 303)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(62, 13)
        Me.Label21.TabIndex = 48
        Me.Label21.Text = "Percentage"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(555, 305)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(100, 13)
        Me.Label23.TabIndex = 50
        Me.Label23.Text = "OFFER TO PAY:"
        '
        'offer_tbox
        '
        Me.offer_tbox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.offer_tbox.Location = New System.Drawing.Point(558, 323)
        Me.offer_tbox.Name = "offer_tbox"
        Me.offer_tbox.ReadOnly = True
        Me.offer_tbox.Size = New System.Drawing.Size(100, 20)
        Me.offer_tbox.TabIndex = 52
        '
        'settlement_pcentbtn
        '
        Me.settlement_pcentbtn.Location = New System.Drawing.Point(474, 319)
        Me.settlement_pcentbtn.Name = "settlement_pcentbtn"
        Me.settlement_pcentbtn.Size = New System.Drawing.Size(75, 23)
        Me.settlement_pcentbtn.TabIndex = 53
        Me.settlement_pcentbtn.Text = "80%"
        Me.settlement_pcentbtn.UseVisualStyleBackColor = True
        '
        'client_name_lbl
        '
        Me.client_name_lbl.AutoSize = True
        Me.client_name_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.client_name_lbl.Location = New System.Drawing.Point(456, 18)
        Me.client_name_lbl.Name = "client_name_lbl"
        Me.client_name_lbl.Size = New System.Drawing.Size(75, 13)
        Me.client_name_lbl.TabIndex = 54
        Me.client_name_lbl.Text = "Client Name"
        '
        'displaybtn
        '
        Me.displaybtn.Location = New System.Drawing.Point(712, 75)
        Me.displaybtn.Name = "displaybtn"
        Me.displaybtn.Size = New System.Drawing.Size(145, 23)
        Me.displaybtn.TabIndex = 2
        Me.displaybtn.Text = "Display Percentages"
        Me.displaybtn.UseVisualStyleBackColor = True
        '
        'sch_name_lbl
        '
        Me.sch_name_lbl.AutoSize = True
        Me.sch_name_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.sch_name_lbl.Location = New System.Drawing.Point(456, 40)
        Me.sch_name_lbl.Name = "sch_name_lbl"
        Me.sch_name_lbl.Size = New System.Drawing.Size(88, 13)
        Me.sch_name_lbl.TabIndex = 56
        Me.sch_name_lbl.Text = "Scheme Name"
        '
        'maintainbtn
        '
        Me.maintainbtn.Location = New System.Drawing.Point(712, 113)
        Me.maintainbtn.Name = "maintainbtn"
        Me.maintainbtn.Size = New System.Drawing.Size(145, 23)
        Me.maintainbtn.TabIndex = 3
        Me.maintainbtn.Text = "Maintain Percentages"
        Me.maintainbtn.UseVisualStyleBackColor = True
        '
        'discountbtn
        '
        Me.discountbtn.Location = New System.Drawing.Point(477, 369)
        Me.discountbtn.Name = "discountbtn"
        Me.discountbtn.Size = New System.Drawing.Size(75, 23)
        Me.discountbtn.TabIndex = 57
        Me.discountbtn.Text = "Discount"
        Me.discountbtn.UseVisualStyleBackColor = True
        '
        'settlement_notes_tbox
        '
        Me.settlement_notes_tbox.Location = New System.Drawing.Point(672, 279)
        Me.settlement_notes_tbox.Multiline = True
        Me.settlement_notes_tbox.Name = "settlement_notes_tbox"
        Me.settlement_notes_tbox.ReadOnly = True
        Me.settlement_notes_tbox.Size = New System.Drawing.Size(226, 110)
        Me.settlement_notes_tbox.TabIndex = 58
        '
        'settle_pcent_lbl
        '
        Me.settle_pcent_lbl.AutoSize = True
        Me.settle_pcent_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.settle_pcent_lbl.Location = New System.Drawing.Point(669, 253)
        Me.settle_pcent_lbl.Name = "settle_pcent_lbl"
        Me.settle_pcent_lbl.Size = New System.Drawing.Size(52, 13)
        Me.settle_pcent_lbl.TabIndex = 59
        Me.settle_pcent_lbl.Text = "Label22"
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(921, 456)
        Me.Controls.Add(Me.settle_pcent_lbl)
        Me.Controls.Add(Me.settlement_notes_tbox)
        Me.Controls.Add(Me.discountbtn)
        Me.Controls.Add(Me.maintainbtn)
        Me.Controls.Add(Me.sch_name_lbl)
        Me.Controls.Add(Me.displaybtn)
        Me.Controls.Add(Me.client_name_lbl)
        Me.Controls.Add(Me.settlement_pcentbtn)
        Me.Controls.Add(Me.offer_tbox)
        Me.Controls.Add(Me.Label23)
        Me.Controls.Add(Me.Label21)
        Me.Controls.Add(Me.discount_tbox)
        Me.Controls.Add(Me.Label20)
        Me.Controls.Add(Me.var_ratebtn)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.var_week_tbox)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.var_fort_tbox)
        Me.Controls.Add(Me.Label19)
        Me.Controls.Add(Me.var_month_tbox)
        Me.Controls.Add(Me.var_lbl)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.mth12_week_tbox)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.mth12_fort_tbox)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.mth12_month_tbox)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.mth9_week_tbox)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.mth9_fort_tbox)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.mth9_month_tbox)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.mth6_week_tbox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.mth6_fort_tbox)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.mth6_month_tbox)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.bal_lbl)
        Me.Controls.Add(Me.name_lbl)
        Me.Controls.Add(Me.debtorbtn)
        Me.Controls.Add(Me.debtor_lbl)
        Me.Controls.Add(Me.balbtn)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.mth3_week_tbox)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.mth3_fort_tbox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.mth3_month_tbox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Payment Calculator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents mth3_month_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents mth3_fort_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents mth3_week_tbox As System.Windows.Forms.TextBox
    Friend WithEvents balbtn As System.Windows.Forms.Button
    Friend WithEvents debtor_lbl As System.Windows.Forms.Label
    Friend WithEvents debtorbtn As System.Windows.Forms.Button
    Friend WithEvents name_lbl As System.Windows.Forms.Label
    Friend WithEvents bal_lbl As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents mth6_week_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents mth6_fort_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents mth6_month_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents mth9_week_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents mth9_fort_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents mth9_month_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents mth12_week_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents mth12_fort_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents mth12_month_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents var_week_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents var_fort_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents var_month_tbox As System.Windows.Forms.TextBox
    Friend WithEvents var_lbl As System.Windows.Forms.Label
    Friend WithEvents var_ratebtn As System.Windows.Forms.Button
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents discount_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents offer_tbox As System.Windows.Forms.TextBox
    Friend WithEvents settlement_pcentbtn As System.Windows.Forms.Button
    Friend WithEvents client_name_lbl As System.Windows.Forms.Label
    Friend WithEvents displaybtn As System.Windows.Forms.Button
    Friend WithEvents sch_name_lbl As System.Windows.Forms.Label
    Friend WithEvents maintainbtn As System.Windows.Forms.Button
    Friend WithEvents discountbtn As System.Windows.Forms.Button
    Friend WithEvents settlement_notes_tbox As System.Windows.Forms.TextBox
    Friend WithEvents settle_pcent_lbl As System.Windows.Forms.Label

End Class

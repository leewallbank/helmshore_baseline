Public Class mainfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bal_lbl.Text = ""
        debtor_lbl.Text = ""
        name_lbl.Text = ""
        client_name_lbl.Text = ""
        sch_name_lbl.Text = ""
        var_rate = 24
        settlement_pcent = 80
        settlement_pcentbtn.Text = "80%"
        settlement_notes_tbox.Text = ""
        settle_pcent_lbl.Text = ""
        Dim oWSH = CreateObject("WScript.Shell")
        log_user = oWSH.ExpandEnvironmentStrings("%USERNAME%")
        'log_user = My.User.Name
        If log_user = "JBlundell" Or log_user = "AAhmed" Or log_user = "AFielden" Then
            maintainbtn.Enabled = True
        Else
            maintainbtn.Enabled = False
        End If
    End Sub


    Private Sub balbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles balbtn.Click
        bal = 0
        bal_lbl.Text = ""
        name_lbl.Text = ""
        debtor_lbl.Text = ""
        client_name_lbl.Text = ""
        sch_name_lbl.Text = ""
        settlement_pcent = 80
        settlement_pcentbtn.Text = "80%"
        settlement_notes_tbox.Text = ""
        settle_pcent_lbl.Text = ""
        discount_entered = 0
        Try
            bal = InputBox("Enter balance", "Enter balance")
        Catch ex As Exception
            MsgBox("Balance has to be valid amount or blank")
            Exit Sub
        End Try
        If bal = 0 Then
            Exit Sub
        End If
        If bal < 0 Then
            MsgBox("Balance can't be less than zero")
            Exit Sub
        End If
        calc_values()
    End Sub
    Private Sub calc_values()
        Dim mth_bal As Decimal = bal / 3
        mth3_month_tbox.Text = Format(mth_bal, "c")
        Dim fort_bal As Decimal = mth_bal / 2
        mth3_fort_tbox.Text = Format(fort_bal, "c")
        Dim week_bal As Decimal = fort_bal / 2
        mth3_week_tbox.Text = Format(week_bal, "c")
        bal_lbl.Text = Format(bal, "c")

        mth_bal = bal / 6
        mth6_month_tbox.Text = Format(mth_bal, "c")
        fort_bal = mth_bal / 2
        mth6_fort_tbox.Text = Format(fort_bal, "c")
        week_bal = fort_bal / 2
        mth6_week_tbox.Text = Format(week_bal, "c")

        mth_bal = bal / 9
        mth9_month_tbox.Text = Format(mth_bal, "c")
        fort_bal = mth_bal / 2
        mth9_fort_tbox.Text = Format(fort_bal, "c")
        week_bal = fort_bal / 2
        mth9_week_tbox.Text = Format(week_bal, "c")

        mth_bal = bal / 12
        mth12_month_tbox.Text = Format(mth_bal, "c")
        fort_bal = mth_bal / 2
        mth12_fort_tbox.Text = Format(fort_bal, "c")
        week_bal = fort_bal / 2
        mth12_week_tbox.Text = Format(week_bal, "c")

        mth_bal = bal / var_rate
        var_month_tbox.Text = Format(mth_bal, "c")
        fort_bal = mth_bal / 2
        var_fort_tbox.Text = Format(fort_bal, "c")
        week_bal = fort_bal / 2
        var_week_tbox.Text = Format(week_bal, "c")
        Dim offer As Decimal
        If discount_entered > 0 Then
            discount_tbox.Text = "�" & discount_entered
            offer = bal - discount_entered
            settlement_pcentbtn.Text = "N/A"
        Else
            settlement_pcentbtn.Text = settlement_pcent & "%"
            settlement = bal * settlement_pcent / 100
            discount_tbox.Text = Format(bal - settlement, "c")
            offer = settlement
        End If
        offer_tbox.Text = Format(offer, "c")
    End Sub

    Private Sub debtorbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles debtorbtn.Click
        bal_lbl.Text = ""
        debtor_lbl.Text = ""
        name_lbl.Text = ""
        settlement_pcent = 80
        settlement_pcentbtn.Text = "80%"
        settlement_notes_tbox.Text = ""
        settle_pcent_lbl.Text = ""
        Dim debtorID As Integer
        Dim in_str As String
        Try
            in_str = Trim(InputBox("Enter Debtor ID", "Enter Debtor ID"))
            If in_str.Length = 0 Then
                Exit Sub
            End If
            debtorID = in_str
        Catch ex As Exception
            MsgBox("Debtor ID has to be a valid number or blank")
        End Try
        param2 = "select name_fore, name_sur, debt_balance, clientschemeID from Debtor where _rowid = " & _
        debtorID
        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("debtor " & debtorID & " is not on onestep")
            Exit Sub
        End If
        debtor_lbl.Text = debtorID
        Dim cs_id As Integer = debtor_dataset.Tables(0).Rows(0).Item(3)
        param2 = "select clientID, schemeID from clientScheme where _rowid = " & cs_id
        Dim cs_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to access clientscheme for rowid = " & cs_id)
            Exit Sub
        End If
        Dim cl_id As Integer = cs_dataset.Tables(0).Rows(0).Item(0)
        param2 = "select name from Client where _rowid = " & cl_id
        Dim client_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to access client table for client ID = " & cl_id)
            Exit Sub
        End If
        Dim client_name As String = client_dataset.Tables(0).Rows(0).Item(0)
        client_name_lbl.Text = client_name
        Dim sch_id As Integer = cs_dataset.Tables(0).Rows(0).Item(1)
        param2 = "select name from Scheme where _rowid = " & sch_id
        Dim sch_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to access Scheme table for Scheme ID = " & sch_id)
            Exit Sub
        End If
        Dim sch_name As String = sch_dataset.Tables(0).Rows(0).Item(0)
        sch_name_lbl.Text = sch_name
        Dim debtor_name As String = ""
        Try
            debtor_name = Trim(debtor_dataset.Tables(0).Rows(0).Item(0))
        Catch ex As Exception

        End Try
        debtor_name = Trim(debtor_name & " " & debtor_dataset.Tables(0).Rows(0).Item(1))
        name_lbl.Text = debtor_name
        bal = debtor_dataset.Tables(0).Rows(0).Item(2)
        bal_lbl.Text = Format(bal, "c")
        'see if there is a settlement percent for this client and scheme
        discount_entered = 0
        Settlement_clients.Settlement_percentagesTableAdapter.FillBy(Settlement_clients.DataSet1.Settlement_percentages, cl_id)
        Dim idx As Integer
        For idx = 0 To Settlement_clients.DataSet1.Settlement_percentages.Rows.Count - 1
            If Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(1) = 0 Then
                settlement_pcent = Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(2)
                settlement_notes_tbox.Text = Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(3)
                If settlement_pcent > 0 Then
                    settle_pcent_lbl.Text = "Settlement without referral up to " & settlement_pcent & "%"
                End If

                Try
                    discount_entered = Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(4)
                Catch ex As Exception

                End Try
                Exit For
            Else
                If Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(1) = sch_id Then
                    settlement_pcent = Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(2)
                    settlement_notes_tbox.Text = Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(3)
                    If settlement_pcent > 0 Then
                        settle_pcent_lbl.Text = "Settlement without referral up to " & settlement_pcent & "%"
                    End If
                    Try
                        discount_entered = Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(4)
                    Catch ex As Exception

                    End Try
                    Exit For
                End If
            End If
        Next
        If settlement_pcent = 0 Then
            settlement_pcent = 100
        End If
        calc_values()
    End Sub

    Private Sub var_ratebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles var_ratebtn.Click
        Try
            var_rate = InputBox("Enter Variable rate", "Enter Variable rate")
        Catch ex As Exception
            MsgBox("Variable rate has to be valid number")
            Exit Sub
        End Try
        If var_rate < 0 Then
            MsgBox("Variable rate can't be less than zero")
            Exit Sub
        End If
        var_lbl.Text = var_rate & " months"

        calc_values()
    End Sub

    Private Sub pcentbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles settlement_pcentbtn.Click
        Try
            settlement_pcent = InputBox("Enter Settlement Percent", "Enter Settlement Percent")
        Catch ex As Exception
            MsgBox("Settlement Percent has to be valid number")
            Exit Sub
        End Try
        If settlement_pcent < 0 Then
            MsgBox("Settlement Percent can't be less than zero")
            Exit Sub
        Else
            If settlement_pcent > 100 Then
                MsgBox("Settlement Percent can't be more than 100%")
                Exit Sub
            End If
        End If
        settlement_pcentbtn.Text = settlement_pcent & "%"
        discount_entered = 0
        calc_values()
    End Sub

    Private Sub maintainbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles displaybtn.Click
        Settlement_clients.ShowDialog()

    End Sub

    Private Sub maintainbtn_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles maintainbtn.Click
        selected_client_no = 0
        selected_client_name = ""
        selected_scheme_no = 0
        selected_scheme_name = ""

        first_letters = InputBox("Enter first part of client name", "Select client")
        first_letters = Trim(first_letters)
        Try
            first_letters = UCase(Microsoft.VisualBasic.Left(first_letters, 1)) & _
                   Microsoft.VisualBasic.Right(first_letters, first_letters.Length - 1)
        Catch ex As Exception
            MsgBox("NO Letters entered")
            Exit Sub
        End Try
       
        If first_letters.Length = 0 Then
            MsgBox("NO Letters entered")
            Exit Sub
        End If
        clientfrm.ShowDialog()
        'see if selected client is already set up in settlement percents
        If selected_client_no = 0 Then
            MsgBox("NO client selected")
            Exit Sub
        End If
        Settlement_clients.Settlement_percentagesTableAdapter.FillBy(Settlement_clients.DataSet1.Settlement_percentages, selected_client_no)
        schfrm.ShowDialog()
    End Sub

    Private Sub discountbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles discountbtn.Click
        Try
            discount_entered = InputBox("Enter discount(�)", "Enter discount amount")
        Catch ex As Exception
            MsgBox("Discount has to be a valid amount")
            Exit Sub
        End Try
        If discount_entered < 0 Then
            MsgBox("Discount can't be less than zero")
            Exit Sub
        End If
        If discount_entered > bal Then
            MsgBox("Discount can't be more than outstanding balance")
            Exit Sub
        End If
        calc_values()
    End Sub
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class schfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.all_schemes_rbtn = New System.Windows.Forms.RadioButton
        Me.specific_scheme_rbtn = New System.Windows.Forms.RadioButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.scheme_cbox = New System.Windows.Forms.ComboBox
        Me.exitbtn = New System.Windows.Forms.Button
        Me.settle_pcent_tbox = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.settle_notes_tbox = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.settle_discount_tbox = New System.Windows.Forms.TextBox
        Me.scheme_lbl = New System.Windows.Forms.Label
        Me.quitbtn = New System.Windows.Forms.Button
        Me.client_name_lbl = New System.Windows.Forms.Label
        Me.client_no_lbl = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'all_schemes_rbtn
        '
        Me.all_schemes_rbtn.AutoSize = True
        Me.all_schemes_rbtn.Checked = True
        Me.all_schemes_rbtn.Location = New System.Drawing.Point(6, 19)
        Me.all_schemes_rbtn.Name = "all_schemes_rbtn"
        Me.all_schemes_rbtn.Size = New System.Drawing.Size(81, 17)
        Me.all_schemes_rbtn.TabIndex = 0
        Me.all_schemes_rbtn.TabStop = True
        Me.all_schemes_rbtn.Text = "All schemes"
        Me.all_schemes_rbtn.UseVisualStyleBackColor = True
        '
        'specific_scheme_rbtn
        '
        Me.specific_scheme_rbtn.AutoSize = True
        Me.specific_scheme_rbtn.Location = New System.Drawing.Point(6, 56)
        Me.specific_scheme_rbtn.Name = "specific_scheme_rbtn"
        Me.specific_scheme_rbtn.Size = New System.Drawing.Size(105, 17)
        Me.specific_scheme_rbtn.TabIndex = 1
        Me.specific_scheme_rbtn.Text = "Specific Scheme"
        Me.specific_scheme_rbtn.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.scheme_cbox)
        Me.GroupBox1.Controls.Add(Me.all_schemes_rbtn)
        Me.GroupBox1.Controls.Add(Me.specific_scheme_rbtn)
        Me.GroupBox1.Location = New System.Drawing.Point(44, 49)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 236)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Scheme Selection"
        '
        'scheme_cbox
        '
        Me.scheme_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.scheme_cbox.FormattingEnabled = True
        Me.scheme_cbox.Location = New System.Drawing.Point(23, 94)
        Me.scheme_cbox.Name = "scheme_cbox"
        Me.scheme_cbox.Size = New System.Drawing.Size(171, 21)
        Me.scheme_cbox.TabIndex = 12
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(666, 351)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(102, 23)
        Me.exitbtn.TabIndex = 5
        Me.exitbtn.Text = "SAVE and Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'settle_pcent_tbox
        '
        Me.settle_pcent_tbox.Location = New System.Drawing.Point(308, 68)
        Me.settle_pcent_tbox.Name = "settle_pcent_tbox"
        Me.settle_pcent_tbox.Size = New System.Drawing.Size(100, 20)
        Me.settle_pcent_tbox.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(308, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Settlement percent"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(308, 162)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Settlement notes"
        '
        'settle_notes_tbox
        '
        Me.settle_notes_tbox.Location = New System.Drawing.Point(308, 178)
        Me.settle_notes_tbox.Multiline = True
        Me.settle_notes_tbox.Name = "settle_notes_tbox"
        Me.settle_notes_tbox.Size = New System.Drawing.Size(421, 20)
        Me.settle_notes_tbox.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(308, 109)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(100, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Settlement discount"
        '
        'settle_discount_tbox
        '
        Me.settle_discount_tbox.Location = New System.Drawing.Point(308, 125)
        Me.settle_discount_tbox.Name = "settle_discount_tbox"
        Me.settle_discount_tbox.Size = New System.Drawing.Size(100, 20)
        Me.settle_discount_tbox.TabIndex = 8
        '
        'scheme_lbl
        '
        Me.scheme_lbl.AutoSize = True
        Me.scheme_lbl.Location = New System.Drawing.Point(308, 22)
        Me.scheme_lbl.Name = "scheme_lbl"
        Me.scheme_lbl.Size = New System.Drawing.Size(106, 13)
        Me.scheme_lbl.TabIndex = 10
        Me.scheme_lbl.Text = "FOR ALL SCHEMES"
        '
        'quitbtn
        '
        Me.quitbtn.Location = New System.Drawing.Point(44, 351)
        Me.quitbtn.Name = "quitbtn"
        Me.quitbtn.Size = New System.Drawing.Size(144, 23)
        Me.quitbtn.TabIndex = 4
        Me.quitbtn.Text = "Quit without saving"
        Me.quitbtn.UseVisualStyleBackColor = True
        '
        'client_name_lbl
        '
        Me.client_name_lbl.AutoSize = True
        Me.client_name_lbl.Location = New System.Drawing.Point(92, 22)
        Me.client_name_lbl.Name = "client_name_lbl"
        Me.client_name_lbl.Size = New System.Drawing.Size(39, 13)
        Me.client_name_lbl.TabIndex = 2
        Me.client_name_lbl.Text = "Label5"
        '
        'client_no_lbl
        '
        Me.client_no_lbl.AutoSize = True
        Me.client_no_lbl.Location = New System.Drawing.Point(32, 22)
        Me.client_no_lbl.Name = "client_no_lbl"
        Me.client_no_lbl.Size = New System.Drawing.Size(39, 13)
        Me.client_no_lbl.TabIndex = 11
        Me.client_no_lbl.Text = "Label5"
        '
        'schfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(811, 408)
        Me.Controls.Add(Me.client_no_lbl)
        Me.Controls.Add(Me.client_name_lbl)
        Me.Controls.Add(Me.quitbtn)
        Me.Controls.Add(Me.scheme_lbl)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.settle_discount_tbox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.settle_notes_tbox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.settle_pcent_tbox)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "schfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Schemes"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents all_schemes_rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents specific_scheme_rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents settle_pcent_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents settle_notes_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents settle_discount_tbox As System.Windows.Forms.TextBox
    Friend WithEvents scheme_lbl As System.Windows.Forms.Label
    Friend WithEvents quitbtn As System.Windows.Forms.Button
    Friend WithEvents client_name_lbl As System.Windows.Forms.Label
    Friend WithEvents client_no_lbl As System.Windows.Forms.Label
    Friend WithEvents scheme_cbox As System.Windows.Forms.ComboBox
End Class

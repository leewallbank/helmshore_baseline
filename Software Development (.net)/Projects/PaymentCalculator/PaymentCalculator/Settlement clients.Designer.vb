<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Settlement_clients
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.settlement_dg = New System.Windows.Forms.DataGridView
        Me.DataSet1 = New PaymentCalculator.DataSet1
        Me.Settlement_percentagesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Settlement_percentagesTableAdapter = New PaymentCalculator.DataSet1TableAdapters.Settlement_percentagesTableAdapter
        Me.cl_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.sch_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cl_percent = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.settle_notes = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.settle_discount = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.settlement_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Settlement_percentagesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'settlement_dg
        '
        Me.settlement_dg.AllowUserToDeleteRows = False
        Me.settlement_dg.AllowUserToOrderColumns = True
        Me.settlement_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.settlement_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cl_name, Me.sch_name, Me.cl_percent, Me.settle_notes, Me.settle_discount})
        Me.settlement_dg.Location = New System.Drawing.Point(0, 0)
        Me.settlement_dg.Name = "settlement_dg"
        Me.settlement_dg.ReadOnly = True
        Me.settlement_dg.Size = New System.Drawing.Size(928, 345)
        Me.settlement_dg.TabIndex = 0
        '
        'DataSet1
        '
        Me.DataSet1.DataSetName = "DataSet1"
        Me.DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Settlement_percentagesBindingSource
        '
        Me.Settlement_percentagesBindingSource.DataMember = "Settlement_percentages"
        Me.Settlement_percentagesBindingSource.DataSource = Me.DataSet1
        '
        'Settlement_percentagesTableAdapter
        '
        Me.Settlement_percentagesTableAdapter.ClearBeforeFill = True
        '
        'cl_name
        '
        Me.cl_name.HeaderText = "Client Name"
        Me.cl_name.Name = "cl_name"
        Me.cl_name.ReadOnly = True
        Me.cl_name.Width = 250
        '
        'sch_name
        '
        Me.sch_name.HeaderText = "Scheme Name"
        Me.sch_name.Name = "sch_name"
        Me.sch_name.ReadOnly = True
        Me.sch_name.Width = 200
        '
        'cl_percent
        '
        Me.cl_percent.HeaderText = "Settlement %"
        Me.cl_percent.Name = "cl_percent"
        Me.cl_percent.ReadOnly = True
        '
        'settle_notes
        '
        Me.settle_notes.HeaderText = "Notes"
        Me.settle_notes.Name = "settle_notes"
        Me.settle_notes.ReadOnly = True
        Me.settle_notes.Width = 200
        '
        'settle_discount
        '
        Me.settle_discount.HeaderText = "Discount"
        Me.settle_discount.Name = "settle_discount"
        Me.settle_discount.ReadOnly = True
        '
        'Settlement_clients
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(940, 386)
        Me.Controls.Add(Me.settlement_dg)
        Me.Name = "Settlement_clients"
        Me.Text = "Settlement Clients"
        CType(Me.settlement_dg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Settlement_percentagesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents settlement_dg As System.Windows.Forms.DataGridView
    Friend WithEvents DataSet1 As PaymentCalculator.DataSet1
    Friend WithEvents Settlement_percentagesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Settlement_percentagesTableAdapter As PaymentCalculator.DataSet1TableAdapters.Settlement_percentagesTableAdapter
    Friend WithEvents cl_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sch_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cl_percent As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents settle_notes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents settle_discount As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

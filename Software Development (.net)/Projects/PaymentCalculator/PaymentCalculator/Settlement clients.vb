Public Class Settlement_clients

    Private Sub Settlement_clients_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Settlement_percentagesTableAdapter.Fill(Me.DataSet1.Settlement_percentages)
        settlement_dg.Rows.Clear()

        Dim idx As Integer
        For idx = 0 To Me.DataSet1.Settlement_percentages.Rows.Count - 1
            Dim cl_no As Integer = Me.DataSet1.Settlement_percentages.Rows(idx).Item(0)
            param2 = "select name from Client where _rowid = " & cl_no
            Dim cl_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to read client table for cl-no = " & cl_no)
                Exit Sub
            End If
            Dim cl_name As String = cl_dataset.Tables(0).Rows(0).Item(0)
            Dim sch_no As Integer = Me.DataSet1.Settlement_percentages.Rows(idx).Item(1)
            Dim sch_name As String
            If sch_no = 0 Then
                sch_name = "ALL Schemes"
            Else
                param2 = "select name from Scheme where _rowid = " & sch_no
                Dim sch_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    MsgBox("Unable to read scheme table for sch-no = " & sch_no)
                    Exit Sub
                End If
                sch_name = sch_dataset.Tables(0).Rows(0).Item(0)
            End If
            Dim settlement_pcent = Me.DataSet1.Settlement_percentages.Rows(idx).Item(2)
            Dim settle_notes As String = Me.DataSet1.Settlement_percentages.Rows(idx).Item(3)
            Dim settle_discount As String = ""
            Try
                settle_discount = Me.DataSet1.Settlement_percentages.Rows(idx).Item(4)
            Catch ex As Exception
                
            End Try
            If settlement_pcent = 0 Then
                settlement_pcent = ""
            End If
            If settle_discount = "0" Then
                settle_discount = ""
            End If
            settlement_dg.Rows.Add(cl_name, sch_name, settlement_pcent, settle_notes, settle_discount)
        Next
    End Sub
End Class
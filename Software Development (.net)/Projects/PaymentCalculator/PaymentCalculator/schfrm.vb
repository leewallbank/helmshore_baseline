Public Class schfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        If settle_pcent_tbox.Text.Length = 0 Then
            settle_pcent_tbox.Text = "0"
        End If
        If settle_discount_tbox.Text.Length = 0 Then
            settle_discount_tbox.Text = "0"
        End If
        If all_schemes_rbtn.Checked Then
            'first delete any existing entries for client
            Settlement_clients.Settlement_percentagesTableAdapter.DeleteQuery(selected_client_no)
            Try
                Settlement_clients.Settlement_percentagesTableAdapter.InsertQuery(selected_client_no, 0, settle_pcent_tbox.Text, settle_notes_tbox.Text, settle_discount_tbox.Text)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
        Else
            'first delete any existing entry for client and scheme
            Settlement_clients.Settlement_percentagesTableAdapter.DeleteQuery1(selected_client_no, selected_scheme_no)
            'also delete any entry for client and all schemes
            Settlement_clients.Settlement_percentagesTableAdapter.DeleteQuery1(selected_client_no, 0)
            Try
                Settlement_clients.Settlement_percentagesTableAdapter.InsertQuery(selected_client_no, selected_scheme_no, settle_pcent_tbox.Text, settle_notes_tbox.Text, settle_discount_tbox.Text)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
        End If
        MsgBox("client updated")
        Me.Close()
    End Sub

    Private Sub quitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles quitbtn.Click
        Me.Close()
    End Sub

    Private Sub schfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        scheme_cbox.Items.Clear()
        client_name_lbl.Text = selected_client_name
        client_no_lbl.Text = selected_client_no
        all_schemes_rbtn.Checked = True
        scheme_lbl.Text = "FOR ALL SCHEMES"
        settle_pcent_tbox.Text = ""
        settle_notes_tbox.Text = ""
        settle_discount_tbox.Text = ""
        Dim idx As Integer
        For idx = 0 To Settlement_clients.DataSet1.Settlement_percentages.Rows.Count - 1
            If Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(1) = 0 Then
                all_schemes_rbtn.Checked = True
                settle_pcent_tbox.Text = Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(2)
                settle_notes_tbox.Text = Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(3)
                Try
                    settle_discount_tbox.Text = Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(4)
                Catch ex As Exception
                    settle_discount_tbox.Text = ""
                End Try
                Exit For
            Else
                specific_scheme_rbtn.Checked = True

                selected_scheme_no = Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(1)
                settle_pcent_tbox.Text = Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(2)
                settle_notes_tbox.Text = Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(3)
                Try
                    settle_discount_tbox.Text = Settlement_clients.DataSet1.Settlement_percentages.Rows(idx).Item(4)
                Catch ex As Exception
                    settle_discount_tbox.Text = ""
                End Try
                param2 = "select name from Scheme where _rowid = " & selected_scheme_no
                Dim sch_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    MsgBox("Unable to read scheme table for scheme-no = " & selected_scheme_no)
                    Exit Sub
                End If
                selected_scheme_name = sch_dataset.Tables(0).Rows(0).Item(0)
                scheme_lbl.Text = selected_scheme_name
                populate_sch_cbox()
                Exit For
            End If
        Next

    End Sub
    Private Sub populate_sch_cbox()
        scheme_cbox.Items.Clear()
        param2 = "select schemeID from clientScheme where clientID = " & selected_client_no & _
        " and branchID = 2"
        Dim cs_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("No schemes found for this client")
            Exit Sub
        End If
        Dim idx As Integer
        Dim csid_rows As Integer = no_of_rows
        For idx = 0 To csid_rows - 1
            Dim sch_id As Integer = cs_dataset.Tables(0).Rows(idx).Item(0)
            param2 = "select name from Scheme where _rowid = " & sch_id
            Dim sch_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to read scheme for sch-no = " & sch_id)
                Exit For
            End If
            Dim scheme_name As String = sch_dataset.Tables(0).Rows(0).Item(0)
            scheme_cbox.Items.Add(scheme_name)
            If scheme_name = selected_scheme_name Then
                scheme_cbox.Text = selected_scheme_name
            End If
            scheme_array(idx) = sch_id
        Next
        scheme_array_max = idx
    End Sub
    Private Sub specific_scheme_rbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles specific_scheme_rbtn.CheckedChanged
        If specific_scheme_rbtn.Checked Then
            populate_sch_cbox()
        End If
    End Sub

    Private Sub scheme_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles scheme_cbox.Validated
        selected_scheme_name = scheme_cbox.Text
        scheme_lbl.Text = selected_scheme_name
        Try
            selected_scheme_no = scheme_array(scheme_cbox.SelectedIndex)
        Catch ex As Exception
            selected_scheme_no = 0
        End Try

    End Sub

    Private Sub all_schemes_rbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles all_schemes_rbtn.CheckedChanged
        If all_schemes_rbtn.Checked Then
            scheme_cbox.Items.Clear()
        End If
    End Sub

    Private Sub scheme_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles scheme_cbox.SelectedIndexChanged

    End Sub
End Class
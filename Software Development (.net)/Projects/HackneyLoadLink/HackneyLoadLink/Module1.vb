Module Module1
    Public Const PARAMETER_FIELD_NAME1 As String = "RunDate"
   
    Public Sub SetCurrentValuesForParameterField1(ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myArrayList As ArrayList)
        Dim currentParameterValues As CrystalDecisions.Shared.ParameterValues = New CrystalDecisions.Shared.ParameterValues()
        For Each submittedValue As Object In myArrayList
            Dim myParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue()
            myParameterDiscreteValue.Value = submittedValue.ToString()
            currentParameterValues.Add(myParameterDiscreteValue)
        Next
        Dim myParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        Dim myParameterFieldDefinition As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME1)
        myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)
    End Sub
    Public Sub SetDBLogonForReport(ByVal myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo, ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub
    
    Public Sub CloseComObject(ByVal o As Object)
        Try
            While (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0)
            End While
        Catch
        Finally
            o = Nothing
        End Try

    End Sub
End Module

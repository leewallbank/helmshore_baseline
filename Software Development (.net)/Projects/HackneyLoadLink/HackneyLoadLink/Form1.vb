Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        runbtn.Enabled = False
        exitbtn.Enabled = False

        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RM045report = New RM045
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(run_dtp.Value)
        SetCurrentValuesForParameterField1(RM045report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RM045report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RM045 Hackney Load Link.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim filename As String = SaveFileDialog1.FileName
            RM045report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
            RM045report.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
        Me.Close()
    End Sub
End Class

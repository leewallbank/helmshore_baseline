Public Class bail_clfrm

    Private Sub bail_clfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'update all existing rows to priority true
        'insert all rows in grid as priority false
        'delete all rows with priority true
        'this is to prevent all clients being lost if the databse connection fails
        Try
            Me.Dialler_bailiff_clientsTableAdapter.UpdateQuery(True)
        Catch ex As Exception
            MsgBox(ex.Message & "  CONTACT IT - error 1")
            Exit Sub
        End Try
        Dim idx As Integer = 0
        Dim row As DataGridViewRow
        For Each row In client_dg.Rows
            Try
                Dim cl_no As Integer = client_dg.Rows(idx).Cells(0).Value
                If cl_no > 0 Then
                    Me.Dialler_bailiff_clientsTableAdapter.InsertQuery(cl_no, False)
                End If
            Catch ex As Exception
                If InStr(LCase(ex.Message), "duplicate") = 0 Then
                    MsgBox(ex.Message & "  CONTACT IT error 2")
                    Exit Sub
                End If
            End Try
            idx += 1
        Next
       
        Try
            Me.Dialler_bailiff_clientsTableAdapter.DeleteQuery(True)
        Catch ex As Exception
            MsgBox(ex.Message & "  CONTACT IT error 3")
        End Try
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Dialler_bailiff_clientsTableAdapter.Fill(Me.FeesSQLDataSet.Dialler_bailiff_clients)
        client_dg.Rows.Clear()
        Dim idx As Integer = 0
        Dim row As DataRow
        For Each row In Me.FeesSQLDataSet.Dialler_bailiff_clients.Rows
            Dim cl_no As Integer = Me.FeesSQLDataSet.Dialler_bailiff_clients.Rows(idx).Item(0)
            param2 = "select name from Client where _rowid = " & cl_no
            Dim cl_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to read client name for cl_no = " & cl_no)
                Me.Close()
                Exit Sub
            End If
            Dim cl_name As String = Trim(cl_dataset.Tables(0).Rows(0).Item(0))
            client_dg.Rows.Add(cl_no, cl_name)
            idx += 1
        Next
    End Sub

    Private Sub client_dg_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles client_dg.CellEnter
        If e.ColumnIndex = 0 Then
            orig_cl_no = client_dg.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
        End If
    End Sub

    Private Sub client_dg_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles client_dg.CellValidating
        If e.RowIndex < 0 Or client_dg.Rows(e.RowIndex).IsNewRow Then
            Exit Sub
        End If
        If e.ColumnIndex = 0 Then
            ErrorProvider1.SetError(client_dg, "")
            Dim cl_no As Integer
            Try
                cl_no = client_dg.Rows(e.RowIndex).Cells(0).EditedFormattedValue
            Catch ex As Exception
                ErrorProvider1.SetError(client_dg, "Invalid client number")
                e.Cancel = True
            End Try
            If cl_no = orig_cl_no Then
                Exit Sub
            End If
            param2 = "select name from Client where _rowid = " & cl_no
            Dim cl_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                ErrorProvider1.SetError(client_dg, "Client number does not exist on onestep")
                e.Cancel = True
                Exit Sub
            End If
            client_dg.Rows(e.RowIndex).Cells(1).Value = Trim(cl_dataset.Tables(0).Rows(0).Item(0))
        End If
    End Sub

    Private Sub client_dg_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles client_dg.CellContentClick

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        first_letters = InputBox("Enter first part of client name", "Select client")
        first_letters = Trim(first_letters)
        first_letters = UCase(Microsoft.VisualBasic.Left(first_letters, 1)) & _
        Microsoft.VisualBasic.Right(first_letters, first_letters.Length - 1)
        If first_letters.Length = 0 Then
            MsgBox("NO Letters entered")
            Exit Sub
        End If
        clientfrm.ShowDialog()
    End Sub
End Class

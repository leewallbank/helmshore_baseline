<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class bail_clfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.client_dg = New System.Windows.Forms.DataGridView
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.FeesSQLDataSet = New MaintainDiallerClients.FeesSQLDataSet
        Me.Dialler_bailiff_clientsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dialler_bailiff_clientsTableAdapter = New MaintainDiallerClients.FeesSQLDataSetTableAdapters.Dialler_bailiff_clientsTableAdapter
        Me.Button1 = New System.Windows.Forms.Button
        Me.client_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cientl_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.client_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dialler_bailiff_clientsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'client_dg
        '
        Me.client_dg.AllowUserToOrderColumns = True
        Me.client_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.client_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.client_no, Me.cientl_name})
        Me.client_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.client_dg.Location = New System.Drawing.Point(0, 0)
        Me.client_dg.Name = "client_dg"
        Me.client_dg.Size = New System.Drawing.Size(431, 460)
        Me.client_dg.TabIndex = 0
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dialler_bailiff_clientsBindingSource
        '
        Me.Dialler_bailiff_clientsBindingSource.DataMember = "Dialler_bailiff_clients"
        Me.Dialler_bailiff_clientsBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'Dialler_bailiff_clientsTableAdapter
        '
        Me.Dialler_bailiff_clientsTableAdapter.ClearBeforeFill = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(288, 0)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Client List"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'client_no
        '
        Me.client_no.HeaderText = "Client No"
        Me.client_no.Name = "client_no"
        Me.client_no.Width = 50
        '
        'cientl_name
        '
        Me.cientl_name.HeaderText = "Client name"
        Me.cientl_name.Name = "cientl_name"
        Me.cientl_name.ReadOnly = True
        Me.cientl_name.Width = 300
        '
        'bail_clfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(431, 460)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.client_dg)
        Me.Name = "bail_clfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Maintain Dialler Clients"
        CType(Me.client_dg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dialler_bailiff_clientsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents client_dg As System.Windows.Forms.DataGridView
    Friend WithEvents FeesSQLDataSet As MaintainDiallerClients.FeesSQLDataSet
    Friend WithEvents Dialler_bailiff_clientsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dialler_bailiff_clientsTableAdapter As MaintainDiallerClients.FeesSQLDataSetTableAdapters.Dialler_bailiff_clientsTableAdapter
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents client_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cientl_name As System.Windows.Forms.DataGridViewTextBoxColumn

End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class clientfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.cl_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cl_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cl_ref = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cl_no, Me.cl_name, Me.cl_ref})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(562, 549)
        Me.DataGridView1.TabIndex = 0
        '
        'cl_no
        '
        Me.cl_no.HeaderText = "Client No."
        Me.cl_no.Name = "cl_no"
        Me.cl_no.ReadOnly = True
        Me.cl_no.Width = 50
        '
        'cl_name
        '
        Me.cl_name.HeaderText = "Client name"
        Me.cl_name.Name = "cl_name"
        Me.cl_name.ReadOnly = True
        Me.cl_name.Width = 250
        '
        'cl_ref
        '
        Me.cl_ref.HeaderText = "Client Reference"
        Me.cl_ref.Name = "cl_ref"
        Me.cl_ref.ReadOnly = True
        Me.cl_ref.Width = 150
        '
        'clientfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(562, 549)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "clientfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "clientfrm"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents cl_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cl_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cl_ref As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

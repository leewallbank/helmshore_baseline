Public Class textfrm
    Dim orig_text_line As String
    Dim case_no As Integer
    Private Sub textfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        orig_text_line = text_line
        TextBox1.Text = text_line
        
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        If rep_tot_cases > 0 Then
            If case_no > rep_tot_cases Then
                case_no = rep_tot_cases
            End If
            ProgressBar1.Value = case_no / rep_tot_cases * 100
        End If
        Me.Close()
    End Sub

    Private Sub resetbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles resetbtn.Click
        TextBox1.Text = orig_text_line
        TextBox1.Focus()
    End Sub


    Private Sub TextBox1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles TextBox1.Validating
        
        Dim idx As Integer
        Dim pipe_count As Integer = 0
        text_line = TextBox1.Text
        For idx = 1 To Microsoft.VisualBasic.Len(text_line)
            If Mid(text_line, idx, 1) = "|" Then
                pipe_count += 1
            End If
        Next
        If pipe_count = 0 Then
            ErrorProvider1.SetError(TextBox1, "Enter a pipe between name and address")
            e.Cancel = True
        ElseIf pipe_count > 1 Then
            ErrorProvider1.SetError(TextBox1, "Enter ONLY ONE pipe between name and address")
            e.Cancel = True
        Else
            ErrorProvider1.SetError(TextBox1, "")
        End If
        case_no += 1
        
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub splitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles splitbtn.Click
        maintainfrm.ShowDialog()
        check_names()
        TextBox1.Text = text_line
        TextBox1.Focus()
    End Sub
End Class
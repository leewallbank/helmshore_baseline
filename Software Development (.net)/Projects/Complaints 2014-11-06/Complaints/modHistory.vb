﻿Module modHistory

    '16.04.2013 JEB add splash screen and button short cuts
    '13.05.2013 JEB add inv_deleted to investigators table
    '26.08.2014 JEB original version saved on 21.08.2014 - changes made for Group Complaints 
    '29.08.2014 JEB actionsfrm changed to remove table adapter
    '29.08.2014 JEB add_compfrm receipt_type_codecombobox started but binding source not yet removed

End Module

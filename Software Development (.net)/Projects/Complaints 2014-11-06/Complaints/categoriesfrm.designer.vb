<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class categoriesfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.CatcodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CatnumberDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CattextDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ComplaintcategoriesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.Complaint_categoriesTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_categoriesTableAdapter
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ComplaintcategoriesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CatcodeDataGridViewTextBoxColumn, Me.CatnumberDataGridViewTextBoxColumn, Me.CattextDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.ComplaintcategoriesBindingSource
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(816, 566)
        Me.DataGridView1.TabIndex = 0
        '
        'CatcodeDataGridViewTextBoxColumn
        '
        Me.CatcodeDataGridViewTextBoxColumn.DataPropertyName = "cat_code"
        Me.CatcodeDataGridViewTextBoxColumn.HeaderText = "Category"
        Me.CatcodeDataGridViewTextBoxColumn.MinimumWidth = 52
        Me.CatcodeDataGridViewTextBoxColumn.Name = "CatcodeDataGridViewTextBoxColumn"
        Me.CatcodeDataGridViewTextBoxColumn.Width = 52
        '
        'CatnumberDataGridViewTextBoxColumn
        '
        Me.CatnumberDataGridViewTextBoxColumn.DataPropertyName = "cat_number"
        Me.CatnumberDataGridViewTextBoxColumn.HeaderText = "Number"
        Me.CatnumberDataGridViewTextBoxColumn.MinimumWidth = 50
        Me.CatnumberDataGridViewTextBoxColumn.Name = "CatnumberDataGridViewTextBoxColumn"
        Me.CatnumberDataGridViewTextBoxColumn.Width = 50
        '
        'CattextDataGridViewTextBoxColumn
        '
        Me.CattextDataGridViewTextBoxColumn.DataPropertyName = "cat_text"
        Me.CattextDataGridViewTextBoxColumn.HeaderText = "Text"
        Me.CattextDataGridViewTextBoxColumn.Name = "CattextDataGridViewTextBoxColumn"
        Me.CattextDataGridViewTextBoxColumn.Width = 600
        '
        'ComplaintcategoriesBindingSource
        '
        Me.ComplaintcategoriesBindingSource.DataMember = "Complaint_categories"
        Me.ComplaintcategoriesBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Complaint_categoriesTableAdapter
        '
        Me.Complaint_categoriesTableAdapter.ClearBeforeFill = True
        '
        'categoriesfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(816, 566)
        Me.Controls.Add(Me.DataGridView1)
        Me.MinimizeBox = False
        Me.Name = "categoriesfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Complaint Categories"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ComplaintcategoriesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents CatcodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CatnumberDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CattextDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ComplaintcategoriesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Complaint_categoriesTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.Complaint_categoriesTableAdapter
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class actionsfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.actions_dg = New System.Windows.Forms.DataGridView
        Me.action_code = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.action_text = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ActionsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.ActionsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ActionsTableAdapter
        CType(Me.actions_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ActionsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'actions_dg
        '
        Me.actions_dg.AllowUserToOrderColumns = True
        Me.actions_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.actions_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.action_code, Me.action_text})
        Me.actions_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.actions_dg.Location = New System.Drawing.Point(0, 0)
        Me.actions_dg.Name = "actions_dg"
        Me.actions_dg.Size = New System.Drawing.Size(450, 368)
        Me.actions_dg.TabIndex = 0
        '
        'action_code
        '
        Me.action_code.HeaderText = "Code"
        Me.action_code.Name = "action_code"
        Me.action_code.ReadOnly = True
        '
        'action_text
        '
        Me.action_text.HeaderText = "Text"
        Me.action_text.Name = "action_text"
        Me.action_text.Width = 300
        '
        'ActionsBindingSource
        '
        Me.ActionsBindingSource.DataMember = "Actions"
        Me.ActionsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ActionsTableAdapter
        '
        Me.ActionsTableAdapter.ClearBeforeFill = True
        '
        'actionsfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(450, 368)
        Me.Controls.Add(Me.actions_dg)
        Me.Name = "actionsfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Actions"
        CType(Me.actions_dg, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ActionsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents actions_dg As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents ActionsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ActionsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.ActionsTableAdapter
    Friend WithEvents action_code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents action_text As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

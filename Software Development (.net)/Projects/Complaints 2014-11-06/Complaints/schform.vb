Public Class schform

    Private Sub schform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        param1 = "onestep"
        param2 = "select schemeID, _rowid from ClientScheme where clientID = " & cl_no
        Dim sch_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("There are no schemes for this client")
            Me.Close()
            Exit Sub
        End If
        Dim sch_rows As Integer = no_of_rows - 1
        Dim idx As Integer
        DataGridView1.Rows.Clear()
        For idx = 0 To sch_rows
            Dim csid_no As Integer = sch_dataset.Tables(0).Rows(idx).Item(1)
            param2 = "select name from Scheme where _rowid = " & sch_dataset.Tables(0).Rows(idx).Item(0)
            Dim sch2_dataset As DataSet = get_dataset(param1, param2)
            DataGridView1.Rows.Add(csid_no, sch2_dataset.Tables(0).Rows(0).Item(0))
        Next
    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        search_csid_no = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        Me.Close()
    End Sub
End Class
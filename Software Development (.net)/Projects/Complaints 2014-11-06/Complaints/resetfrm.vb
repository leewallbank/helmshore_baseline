Public Class resetfrm

    Private Sub resetfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'PraiseAndComplaintsSQLDataSet.Investigators' table. You can move, or remove it, as needed.
        Me.InvestigatorsTableAdapter.FillBy1(Me.PraiseAndComplaintsSQLDataSet.Investigators)
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub resetbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles resetbtn.Click
        'update password on table
        inv_password = "password"
        Dim upd_log_code As Integer = PraiseAndComplaintsSQLDataSet.Investigators.Rows(Inv_textComboBox.SelectedIndex).Item(0)
        Dim upd_log_text As String = PraiseAndComplaintsSQLDataSet.Investigators.Rows(Inv_textComboBox.SelectedIndex).Item(1)
        Try
            Me.InvestigatorsTableAdapter.Updatepassword(inv_password, upd_log_code)
            log_text = "Password reset for - " & upd_log_text
            updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "reset", 0, log_text)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Me.Close()
            Exit Sub
        End Try
        MsgBox("Password has been reset for " & upd_log_text)
        Me.Close()
    End Sub
End Class
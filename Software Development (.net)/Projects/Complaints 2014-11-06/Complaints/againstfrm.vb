Public Class againstfrm

    Private Sub againstfrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

    End Sub

    Private Sub againstfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.Complaint_againstTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Complaint_against, branch_no)
        param2 = "select against_code, against_text from Complaint_against " & _
        " where against_branch_no = " & branch_no & _
        "order by against_code"
        Dim branch_ds As DataSet = get_dataset("Complaints", param2)
        Dim branch_rows As Integer = no_of_rows - 1
        Dim branch_idx As Integer
        branch_dg.Rows.Clear()
        For branch_idx = 0 To branch_rows
            branch_dg.Rows.Add(branch_ds.Tables(0).Rows(branch_idx).Item(0), branch_ds.Tables(0).Rows(branch_idx).Item(1))
        Next
        against_orig_no_rows = branch_dg.RowCount
    End Sub
    Private Sub branch_dg_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles branch_dg.CellValueChanged
        If e.ColumnIndex = 1 And e.RowIndex >= 0 Then
            branch_dg.EndEdit()
            against_code = branch_dg.Rows(e.RowIndex).Cells(0).Value
            against_text = branch_dg.Rows(e.RowIndex).Cells(1).Value
            If branch_dg.Rows(e.RowIndex).IsNewRow Then Return
            Try
                'Me.Complaint_againstTableAdapter.UpdateQuery(against_text, against_code)
                upd_txt = "update Complaint_against set against_text = '" & against_text & _
                   "'  where against_code = " & against_code & _
                   " and against_branch_no = " & branch_no
                update_sql(upd_txt)
                If orig_text <> against_text Then
                    log_text = "Complaint against amended on branch = " & against_code & _
                                                      " from " & orig_text & " to " & against_text
                    log_type = "Master"
                    add_log(log_type, log_text)
                End If
                'updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)

            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate against type entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles branch_dg.RowEnter
        against_code = branch_dg.Rows(e.RowIndex).Cells(0).Value
        orig_text = branch_dg.Rows(e.RowIndex).Cells(1).Value
    End Sub
    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles branch_dg.RowsRemoved
        'If e.RowIndex > 3 Then
        '    Try
        '        Try
        '            Me.Complaint_againstTableAdapter.DeleteQuery(against_code)
        '        Catch
        '            MessageBox.Show("Can't delete as it's used in a complaint")
        '        End Try
        '        log_text = "Complaint against deleted - " & against_code & " name " & orig_text
        '        updmastfrm.LogTableAdapter.InsertQuery(log_code, Now, "master", 0, log_text)
        '    Catch ex As Exception
        '        MessageBox.Show(ex.Message)
        '    End Try
        'End If
    End Sub
End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class logfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.Log_date = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.log_user = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.log_type = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.log_text = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PraiseAndComplaintsSQLDataSet = New Complaints.PraiseAndComplaintsSQLDataSet
        Me.LogBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.LogTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.LogTableAdapter
        Me.compnotextbox = New System.Windows.Forms.TextBox
        Me.InvestigatorsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.InvestigatorsTableAdapter = New Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.LogBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Log_date, Me.log_user, Me.log_type, Me.log_text})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(867, 545)
        Me.DataGridView1.TabIndex = 0
        '
        'Log_date
        '
        Me.Log_date.HeaderText = "Log date"
        Me.Log_date.Name = "Log_date"
        Me.Log_date.ReadOnly = True
        Me.Log_date.Width = 120
        '
        'log_user
        '
        Me.log_user.HeaderText = "Log User"
        Me.log_user.Name = "log_user"
        Me.log_user.ReadOnly = True
        '
        'log_type
        '
        Me.log_type.HeaderText = "Log type"
        Me.log_type.Name = "log_type"
        Me.log_type.ReadOnly = True
        '
        'log_text
        '
        Me.log_text.HeaderText = "Log Text"
        Me.log_text.Name = "log_text"
        Me.log_text.ReadOnly = True
        Me.log_text.Width = 500
        '
        'PraiseAndComplaintsSQLDataSet
        '
        Me.PraiseAndComplaintsSQLDataSet.DataSetName = "PraiseAndComplaintsSQLDataSet"
        Me.PraiseAndComplaintsSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LogBindingSource
        '
        Me.LogBindingSource.DataMember = "Log"
        Me.LogBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'LogTableAdapter
        '
        Me.LogTableAdapter.ClearBeforeFill = True
        '
        'compnotextbox
        '
        Me.compnotextbox.Location = New System.Drawing.Point(0, 0)
        Me.compnotextbox.Name = "compnotextbox"
        Me.compnotextbox.ReadOnly = True
        Me.compnotextbox.Size = New System.Drawing.Size(132, 20)
        Me.compnotextbox.TabIndex = 1
        Me.compnotextbox.TabStop = False
        '
        'InvestigatorsBindingSource
        '
        Me.InvestigatorsBindingSource.DataMember = "Investigators"
        Me.InvestigatorsBindingSource.DataSource = Me.PraiseAndComplaintsSQLDataSet
        '
        'InvestigatorsTableAdapter
        '
        Me.InvestigatorsTableAdapter.ClearBeforeFill = True
        '
        'logfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(867, 545)
        Me.Controls.Add(Me.compnotextbox)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "logfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Complaints Audit"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PraiseAndComplaintsSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.LogBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.InvestigatorsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents PraiseAndComplaintsSQLDataSet As Complaints.PraiseAndComplaintsSQLDataSet
    Friend WithEvents LogBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LogTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.LogTableAdapter
    Friend WithEvents compnotextbox As System.Windows.Forms.TextBox
    Friend WithEvents InvestigatorsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents InvestigatorsTableAdapter As Complaints.PraiseAndComplaintsSQLDataSetTableAdapters.InvestigatorsTableAdapter
    Friend WithEvents Log_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents log_user As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents log_type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents log_text As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

Public Class add_compfrm

    Private Sub add_compfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.Complaint_branchesTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Complaint_branches)
        cmpny_tbox.Text = cmpny_name
        Try

            Comp_dateDateTimePicker.Value = Now
            'Me.EthnicityTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Ethnicity)
            eth_combobox.Text = "N/A"
            ethnicity_desc = "N/A"
            gender_combobox.Text = "N/A"
            last_against_code = 0
            'Me.Complaint_againstTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Complaint_against, branch_no)

            'Me.Receipt_typeTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Receipt_type)
            param2 = "select recpt_code, recpt_text from Receipt_type " & _
            " order by recpt_text"
            Dim recpt_ds As DataSet = get_dataset("complaints", param2)
            Comp_recpt_codeComboBox.Items.Clear()
            For Each row In recpt_ds.Tables(0).Rows
                Comp_recpt_codeComboBox.Items.Add(row(1))
            Next
            Comp_recpt_codeComboBox.SelectedIndex = -1

            'delete any temporary documents
            delete_temp_directory()

            'still required for complaints officer (invcombobox) + any others?
            'Me.InvestigatorsTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Investigators)
            InvComboBox.SelectedIndex = -1
            InvComboBox.Items.Clear()
            param2 = "select inv_text from Investigators " & _
            " where inv_deleted = 0 " & _
            " order by inv_text"
            Dim inv_ds As DataSet = get_dataset("complaints", param2)
            For Each row In inv_ds.Tables(0).Rows
                Dim invText As String = row(0)
                InvComboBox.Items.Add(row(0))
            Next

            'Me.Received_fromTableAdapter.Fill(Me.PraiseAndComplaintsSQLDataSet.Received_from)
            param2 = "select recvd_from, recvd_text from Received_from " & _
            " order by recvd_from"
            Dim recvd_ds As DataSet = get_dataset("complaints", param2)
            Comp_recvd_codeComboBox.Items.Clear()
            For Each row In recvd_ds.Tables(0).Rows
                Comp_recvd_codeComboBox.Items.Add(row(1))
            Next
            Comp_recvd_codeComboBox.SelectedIndex = -1

            populate_client_table(cmpny_no)
            Dim idx As Integer
            cl_combobox.Items.Clear()
            For idx = 1 To cl_rows
                cl_combobox.Items.Add(client_table(idx, 2))
            Next

            case_no = 0
            case_noTextBox.Text = 0
            debt_typelbl.Text = ""
            cl_ref_lbl.Text = ""
            cl_combobox.SelectedIndex = -1
            cl_combobox.Text = ""
            agentComboBox.SelectedIndex = -1
            agentComboBox.Text = ""
            agent_no = 0
            agent2_combobox.SelectedIndex = -1
            agent2_combobox.Text = ""
            agent2_no = 0
            catcombobox.SelectedIndex = -1
            codeComboBox.SelectedIndex = -1
            compTextBox.Text = ""
            alloc_to_code = 1

            against_code = 1
            doc_textbox.Text = 0
            prioritycbox.Checked = False
            old_comp_no_tbox.Text = " "
            old_comp_no = 0
            ack_datepicker.Visible = False
            ack_cbox.Checked = False
            legalcbox.Checked = False
            inscbox.Checked = False
            pi_cbox.Checked = False
            pl_cbox.Checked = False
            monetary_risktbox.Text = ""
            solicitor_datepicker.Checked = False
            ins_datepicker.Checked = False
            liability_cbox.Checked = False
            liability_cbox.Visible = False
            ins_Panel.Visible = False
            pi_cbox.Visible = False
            pl_cbox.Visible = False
            solicitor_datepicker.Visible = False
            ins_datepicker.Visible = False
            monetary_risktbox.Visible = False

            'category
            category = 2
            category_cbox.SelectedIndex = 2

            'ethnicity
            eth_combobox.Items.Clear()
            param2 = "select eth_code, eth_name from Ethnicity order by eth_code"
            Dim eth_ds As DataSet = get_dataset("Complaints", param2)
            Dim eth_rows As Integer = no_of_rows - 1
            Dim eth_idx As Integer
            For eth_idx = 0 To eth_rows
                eth_combobox.Items.Add(eth_ds.Tables(0).Rows(eth_idx).Item(1))
            Next
            eth_combobox.SelectedIndex = -1

            'branch
            branch_cbox.Items.Clear()
            'comp against code/2 combobox
            Comp_against_codeComboBox.Items.Clear()
            comp_against2_codecombobox.Items.Clear()
            Comp_against_codeComboBox.SelectedIndex = -1
            comp_against2_codecombobox.SelectedIndex = -1
            against_code = 0
            against2_code = 0
            param2 = "select branch_code, branch_name from Complaint_branches " & _
            " where branch_cmpny_no = " & cmpny_no & _
            " order by branch_code"
            Dim branch_ds As DataSet = get_dataset("Complaints", param2)
            Dim branch_rows As Integer = no_of_rows - 1
            Dim branch_idx As Integer
            For branch_idx = 0 To branch_rows
                branch_cbox.Items.Add(branch_ds.Tables(0).Rows(branch_idx).Item(1))
                'do not need to populate against until a branch is selected
                'Dim branchCode As Integer = branch_ds.Tables(0).Rows(branch_idx).Item(0)
                'param2 = "select against_code, against_text from Complaint_against " & _
                '            " where against_branch_no = " & branchCode & _
                '            " order by against_code"
                'Dim against_ds As DataSet = get_dataset("Complaints", param2)
                'Dim against_rows As Integer = no_of_rows - 1
                'Dim against_idx As Integer
                'For against_idx = 0 To against_rows
                '    Comp_against_codeComboBox.Items.Add(against_ds.Tables(0).Rows(against_idx).Item(1))
                '    comp_against2_codecombobox.Items.Add(against_ds.Tables(0).Rows(against_idx).Item(1))
                'Next
            Next

            'For Each row In Me.PraiseAndComplaintsSQLDataSet.Complaint_against.Rows
            ' comp_against2_codecombobox.Items.Add(Me.PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(idx).Item(1))
            'idx += 1
            'Next

            type_code_cbox.Items.Add(" ")
            type_code = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub case_noTextBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles case_noTextBox.Validated
        'give warning if case already exists in a complaint
        If case_no = 0 Or cmpny_no > 0 Then
            Exit Sub
        End If
        'Me.ComplaintsTableAdapter.Fill2(Me.PraiseAndComplaintsSQLDataSet.Complaints, case_no)
        param2 = "select count(*) from Complaints where comp_no > " & comp_start_no & _
        " and comp_case_no = " & case_no
        Dim comp_ds As DataSet = get_dataset("complaints", param2)

        'If Me.PraiseAndComplaintsSQLDataSet.Complaints.Rows.Count > 0 Then
        If comp_ds.Tables(0).Rows(0).Item(0) > 0 Then
            MsgBox("Warning - this case number already has a complaint on the system")
        Else
            param2 = "select linkID from Debtor where _rowid = " & case_no
            Dim debt_ds As DataSet = get_dataset("onestep", param2)
            Dim link As Integer
            Try
                link = debt_ds.Tables(0).Rows(0).Item(0)
            Catch ex As Exception
                link = 0
            End Try
            If link > 0 Then
                param2 = "select _rowid from Debtor where linkID = " & link
                Dim link_ds As DataSet = get_dataset("onestep", param2)
                Dim idx As Integer
                For idx = 0 To no_of_rows - 1
                    Dim link_case_no As Integer = link_ds.Tables(0).Rows(idx).Item(0)
                    'Me.ComplaintsTableAdapter.Fill2(Me.PraiseAndComplaintsSQLDataSet.Complaints, link_case_no)
                    param2 = "select count(*) from Complaints where comp_no > " & comp_start_no & _
                           " and comp_case_no = " & link_case_no
                    Dim comp2_ds As DataSet = get_dataset("complaints", param2)
                    'If Me.PraiseAndComplaintsSQLDataSet.Complaints.Rows.Count > 0 Then
                    If comp2_ds.Tables(0).Rows(0).Item(0) > 0 Then
                        MsgBox("Warning - this case number already has a complaint on the system")
                        Exit For
                    End If
                Next
            End If
        End If

    End Sub


    Private Sub case_noTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles case_noTextBox.Validating
        Try
            cl_ref_lbl.Text = ""
            ErrorProvider1.SetError(case_noTextBox, "")
            case_no = 0
            If Len(Trim(case_noTextBox.Text)) = 0 Then
                populate_client_table(cmpny_no)
                Dim idx2 As Integer
                For idx2 = 1 To cl_rows
                    cl_combobox.Items.Add(client_table(idx2, 2))
                Next
                Exit Sub
            End If
            If case_noTextBox.Text = 0 Then
                populate_client_table(cmpny_no)
                Dim idx2 As Integer
                For idx2 = 1 To cl_rows
                    cl_combobox.Items.Add(client_table(idx2, 2))
                Next
                Exit Sub
            End If

            If Not IsNumeric(case_noTextBox.Text) Then
                ErrorProvider1.SetError(case_noTextBox, "Case number must be numeric")
                e.Cancel = True
                Exit Sub
            End If
            case_no = case_noTextBox.Text

            'check case number is on onestep if rossendales company
            If cmpny_no = 0 Then
                param1 = "onestep"
                param2 = " select _rowid, clientschemeID, client_ref from Debtor" & _
                " where _rowid = " & case_noTextBox.Text
                Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    ErrorProvider1.SetError(case_noTextBox, "Case number is not on Onestep")
                    Exit Sub
                End If
                Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(1)

                'get client number
                param2 = "select _rowid, clientID, schemeID from ClientScheme" & _
                " where _rowid = " & csid

                Dim csid_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    ErrorProvider1.SetError(case_noTextBox, "Client scheme is not on Onestep")
                    Exit Sub
                End If
                cl_no = csid_dataset.Tables(0).Rows(0).Item(1)
                cl_ref_lbl.Text = debtor_dataset.Tables(0).Rows(0).Item(2)
                'get client name
                Dim idx As Integer
                For idx = 1 To cl_rows
                    If client_table(idx, 1) = cl_no Then
                        cl_name = client_table(idx, 2)
                        Exit For
                    End If
                Next
                cl_combobox.Items.Clear()
                cl_combobox.Items.Add(cl_name)
                cl_combobox.SelectedItem = cl_name
                Comp_against_codeComboBox.Focus()

                'get debt type
                Dim sch_id As Integer = csid_dataset.Tables(0).Rows(0).Item(2)
                param2 = "select name from Scheme where _rowid = " & sch_id
                Dim sch_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    MsgBox("Unable to find scheme no " & sch_id)
                End If
                debt_typelbl.Text = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub


    Private Sub savebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles savebtn.Click
        Try
            ErrorProvider1.SetError(Comp_recpt_codeComboBox, "")
            If Len(Comp_recpt_codeComboBox.Text) = 0 Then
                ErrorProvider1.SetError(Comp_recpt_codeComboBox, "Receipt type must be entered")
                Comp_recpt_codeComboBox.Focus()
                Exit Sub
            End If
            ErrorProvider1.SetError(Comp_recvd_codeComboBox, "")
            If Len(Comp_recvd_codeComboBox.Text) = 0 Then
                ErrorProvider1.SetError(Comp_recvd_codeComboBox, "Received from must be entered")
                Comp_recvd_codeComboBox.Focus()
                Exit Sub
            End If

            ErrorProvider1.SetError(branch_cbox, "")
            If branch_cbox.SelectedIndex = -1 Then
                ErrorProvider1.SetError(branch_cbox, "Branch name must be entered")
                branch_cbox.Focus()
                Exit Sub
            End If

            ErrorProvider1.SetError(Comp_against_codeComboBox, "")
            If Len(Comp_against_codeComboBox.Text) = 0 Then
                ErrorProvider1.SetError(Comp_against_codeComboBox, "Complaint made against must be entered")
                Comp_against_codeComboBox.Focus()
                Exit Sub
            End If

            ErrorProvider1.SetError(agentComboBox, "")
            If Len(agentComboBox.Text) = 0 And _
                (Comp_against_codeComboBox.SelectedIndex > 1 And _
                 Comp_against_codeComboBox.SelectedIndex < 5) Then
                ErrorProvider1.SetError(agentComboBox, "Complaint against name must be entered")
                agentComboBox.Focus()
                Exit Sub
            End If

            ErrorProvider1.SetError(catcombobox, "")
            If Len(catcombobox.Text) = 0 Then
                ErrorProvider1.SetError(catcombobox, "Category must be entered")
                catcombobox.Focus()
                Exit Sub
            End If

            ErrorProvider1.SetError(catcombobox, "")
            If Len(codeComboBox.Text) = 0 Then
                ErrorProvider1.SetError(codeComboBox, "Category code must be entered")
                codeComboBox.Focus()
                Exit Sub
            End If

            ErrorProvider1.SetError(compTextBox, "")
            If Len(compTextBox.Text) > 500 Then
                ErrorProvider1.SetError(compTextBox, "Maximum of 500 characters")
                compTextBox.Focus()
                Exit Sub
            End If

            Dim code_no As Integer
            If codeComboBox.Text = "99 Contentious" Then
                stage_no = 0
                code_no = 99
            Else
                stage_no = 1
                If Mid(codeComboBox.Text, 2, 1) = " " Then
                    code_no = Microsoft.VisualBasic.Left(codeComboBox.Text, 2)
                Else
                    code_no = Microsoft.VisualBasic.Left(codeComboBox.Text, 3)
                End If
            End If

            If (Microsoft.VisualBasic.Left(catcombobox.Text, 1) = "A" And _
                        code_no = 8) Or _
                (Microsoft.VisualBasic.Left(catcombobox.Text, 1) = "B" And _
                        (code_no = 1 Or code_no = 7)) Or _
                (Microsoft.VisualBasic.Left(catcombobox.Text, 1) = "C" And _
                        (code_no = 6 Or code_no = 7 Or code_no = 9)) Then
                MsgBox("The Complaint category chosen is not allowed for new complaints")
                Exit Sub
            End If

            'get ethnicity code
            If eth_combobox.SelectedIndex = -1 Then
                ethnicity = 0
            Else
                ethnicity = eth_combobox.SelectedIndex
            End If

            'Dim idx As Integer
            'For idx = 0 To Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows.Count - 1
            '    Dim eth_code As Integer = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(0)
            '    Dim eth_desc As String = Me.PraiseAndComplaintsSQLDataSet.Ethnicity.Rows(idx).Item(1)
            '    If ethnicity_desc = eth_desc Then
            '        ethnicity = eth_code
            '        Exit For
            '    End If
            'Next
            'insert complaint
            'save on test database for test logid
            If env_str <> "Prod" Then
                Dim priority As Integer = 0
                If prioritycbox.Checked Then priority = 1
                upd_txt = "insert into Complaints (comp_date,comp_recpt_code,comp_recvd_code,comp_case_no," & _
                           "comp_client_no,comp_against_code,comp_against_agent,comp_cat_code,comp_cat_number,comp_dept_code," & _
                         "comp_entered_by,comp_allocated_to,comp_text,comp_response,comp_completed_by," & _
                         "comp_founded,comp_checked_by,comp_action, comp_stage_no, comp_cor_code, comp_hold_code," & _
                         "comp_priority,comp_old_comp_no,comp_gender,comp_ethnicity,comp_against_code2," & _
                         "comp_against_agent2,comp_category,comp_branch_no, comp_type_code, comp_cmpny_no) values ('" _
                         & Comp_dateDateTimePicker.Text & "'," _
                      & recpt_code & "," _
                      & recvd_from & "," _
                      & case_no & "," _
                      & cl_no & "," _
                      & against_code & "," _
                         & agent_no & ",'" _
                         & Microsoft.VisualBasic.Left(catcombobox.Text, 1) & "'," _
                         & code_no & "," _
                         & dept_code & "," _
                         & log_code & "," _
                         & alloc_to_code & ",'" _
                         & compTextBox.Text & "','" _
                         & " " & "'," _
                         & 0 & ",'" _
                      & "U" & "'," _
                         & 0 & "," _
                         & 3 & "," _
                         & stage_no & "," _
                         & 1 & "," _
                         & 1 & "," _
                         & priority & "," _
                         & old_comp_no & ",'" _
                         & Microsoft.VisualBasic.Left(gender_combobox.Text, 1) & "'," _
                         & ethnicity & "," _
                         & against2_code & "," _
                         & agent2_no & "," _
                         & category & "," _
                         & branch_no & "," _
                         & type_code & "," _
                         & cmpny_no & ")"
                update_sql(upd_txt)
                param2 = "select max(comp_no) from Complaints where comp_entered_by = " & log_code
                Dim comp_ds As DataSet = get_dataset("complaints", param2)
                Try
                    comp_no = comp_ds.Tables(0).Rows(0).Item(0)
                Catch ex As Exception

                End Try
                'update ack date if entered
                If ack_cbox.Checked = True Then
                    upd_txt = "update Complaints set comp_ack_letter_date = '" & Format(ack_datepicker.Value, "dd/MMM/yyyy") & _
                    "' where comp_no = " & comp_no
                    update_sql(upd_txt)
                    'ComplaintsTableAdapter.UpdateQuery8(ack_datepicker.Value, comp_no)
                End If
                'update legal details 
                If legalcbox.Checked = True Then
                    Dim solicitor_date As Date
                    If solicitor_datepicker.Checked = True Then
                        solicitor_date = solicitor_datepicker.Value
                    Else
                        solicitor_date = CDate("1800,1,1")
                    End If
                    upd_txt = "update Complaints set comp_legal_insurance_flag = 'Y'," & _
                    " comp_referred_to_solicitor = '" & Format(solicitor_date, "dd/MMM/yyyy") & "'" & _
                    " where comp_no = " & comp_no
                    update_sql(upd_txt)
                    'ComplaintsTableAdapter.UpdateQuery9("Y", solicitor_date, comp_no)
                End If
                If inscbox.Checked = True Then
                    Dim ins_date As Date
                    If ins_datepicker.Checked = True Then
                        ins_date = ins_datepicker.Value
                    Else
                        ins_date = CDate("1800,1,1")
                    End If
                    Dim liability_flag As String = "N"
                    If liability_cbox.Checked = True Then
                        liability_flag = "Y"
                    End If
                    insurance_type_no = 0
                    If act_not_rbtn.Checked Then
                        insurance_type_no = 1
                    ElseIf border_rbtn.Checked Then
                        insurance_type_no = 2
                    End If
                    If pi_cbox.Checked Then
                        prof_indemnity_no = 1
                    Else
                        prof_indemnity_no = 0
                    End If
                    If pl_cbox.Checked Then
                        public_liability_no = 1
                    Else
                        public_liability_no = 0
                    End If
                    upd_txt = "update Complaints set comp_referred_to_insurer = '" & Format(ins_date, "dd/MMM/yyyy") & "'" & _
                    ", comp_insurance_type_no=" & insurance_type_no & _
                    ", comp_liability_flag= '" & liability_flag & _
                    "', comp_prof_indemnity_no = " & prof_indemnity_no & _
                    ", comp_public_liability_no = " & public_liability_no & _
                    " where comp_no = " & comp_no
                    update_sql(upd_txt)
                    'ComplaintsTableAdapter.UpdateQuery11(ins_date, insurance_type_no, liability_flag, comp_no)
                End If
                If legalcbox.Checked Or inscbox.Checked And monetary_risk <> -999 Then
                    upd_txt = "update Complaints set comp_monetary_risk = " & Format(monetary_risk, "f") & _
                              " where comp_no = " & comp_no
                    update_sql(upd_txt)
                    'ComplaintsTableAdapter.UpdateQuery12(Format(monetary_risk, "f"), comp_no)
                End If
            Else
                Try
                    ComplaintsTableAdapter.InsertQuery(Comp_dateDateTimePicker.Text, recpt_code, _
                    recvd_from, case_no, cl_no, _
                    against_code, agent_no, _
                    Microsoft.VisualBasic.Left(catcombobox.Text, 1), code_no, _
                    dept_code, log_code, alloc_to_code, compTextBox.Text, " ", 0, "U", 0, 3, _
                    stage_no, 1, 1, prioritycbox.Checked, old_comp_no, gender_combobox.Text, _
                    ethnicity, against2_code, agent2_no, category, branch_no, type_code, cmpny_no)
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    Me.Close()
                    Exit Sub
                End Try
                comp_no = Me.ComplaintsTableAdapter.ScalarQuery3(log_code)
                'update ack date if entered
                If ack_cbox.Checked = True Then
                    ComplaintsTableAdapter.UpdateQuery8(ack_datepicker.Value, comp_no)
                End If
                'update legal details 
                If legalcbox.Checked = True Then
                    Dim solicitor_date As Date
                    If solicitor_datepicker.Checked = True Then
                        solicitor_date = Format(solicitor_datepicker.Value, "yyyy-MM-dd")
                    Else
                        solicitor_date = CDate("1800,1,1")
                    End If
                    ComplaintsTableAdapter.UpdateQuery9("Y", solicitor_date, comp_no)
                End If
                If inscbox.Checked = True Then
                    Dim ins_date As Date
                    If ins_datepicker.Checked = True Then
                        ins_date = Format(ins_datepicker.Value, "yyyy-MM-dd")
                    Else
                        ins_date = CDate("1800,1,1")
                    End If
                    Dim liability_flag As String = "N"
                    If liability_cbox.Checked = True Then
                        liability_flag = "Y"
                    End If
                    insurance_type_no = 0
                    If act_not_rbtn.Checked Then
                        insurance_type_no = 1
                    ElseIf border_rbtn.Checked Then
                        insurance_type_no = 2
                    End If
                    If pi_cbox.Checked Then
                        prof_indemnity_no = 1
                    Else
                        prof_indemnity_no = 0
                    End If
                    If pl_cbox.Checked Then
                        public_liability_no = 1
                    Else
                        public_liability_no = 0
                    End If
                    upd_txt = "update Complaints set comp_referred_to_insurer = '" & Format(ins_date, "dd/MMM/yyyy") & "'" & _
                                        ", comp_insurance_type_no=" & insurance_type_no & _
                                        ", comp_liability_flag= '" & liability_flag & _
                                       "', comp_prof_indemnity_no = " & prof_indemnity_no & _
                                       ", comp_public_liability_no = " & public_liability_no & _
                                        " where comp_no = " & comp_no
                    update_sql(upd_txt)
                    'ComplaintsTableAdapter.UpdateQuery11(ins_date, insurance_type_no, liability_flag, comp_no)
                End If
                If legalcbox.Checked Or inscbox.Checked And monetary_risk <> -999 Then
                    ComplaintsTableAdapter.UpdateQuery12(Format(monetary_risk, "f"), comp_no)
                End If
            End If
            'display complaint no
            'check if one of the clients to notify
            If cl_no > 0 Then
                'notifyclientfrm2.Complaints_resp2_clientsTableAdapter.FillBy(notifyclientfrm2.PraiseAndComplaintsSQLDataSet.Complaints_resp2_clients, cl_no)
                param2 = "select count(*) from Complaints_resp2_clients " & _
                " where comp_resp2_cl_no = " & cl_no
                Dim resp_ds As DataSet = get_dataset("complaints", param2)
                'If notifyclientfrm2.PraiseAndComplaintsSQLDataSet.Complaints_resp2_clients.Rows.Count = 1 Then
                If resp_ds.Tables(0).Rows(0).Item(0) > 0 Then
                    MsgBox("Don't forget to send details to the client")
                End If
            End If
            Try
                MsgBox("Complaint number is " & comp_no)

            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try

            'rename directory for any documents saved
            Dim dirname As String = "r:\complaints\T" & log_code
            rename_directory(dirname)
            Me.Close()
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub Comp_dateDateTimePicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Comp_dateDateTimePicker.Validating
        ErrorProvider1.SetError(Comp_dateDateTimePicker, "")
        If Comp_dateDateTimePicker.Text < DateAdd("d", -200, Now) Then
            ErrorProvider1.SetError(Comp_dateDateTimePicker, "Date can only go back 200 days")
            e.Cancel = True
        ElseIf Comp_dateDateTimePicker.Text > Now Then
            ErrorProvider1.SetError(Comp_dateDateTimePicker, "Date can't be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub Comp_recpt_codeComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Comp_recpt_codeComboBox.Validated
        'get recpt code number
        ErrorProvider1.SetError(Comp_recpt_codeComboBox, "")
        ' recpt_code = PraiseAndComplaintsSQLDataSet.Receipt_type.Rows(Comp_recpt_codeComboBox.SelectedIndex).Item(0)
        param2 = "select recpt_code from Receipt_type " & _
        " where recpt_text = '" & Comp_recpt_codeComboBox.Text & "'"
        Dim recpt_ds As DataSet = get_dataset("complaints", param2)
        Try
            recpt_code = recpt_ds.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            MsgBox("Invalid receipt type selected")
            recpt_code = 0
            Comp_recpt_codeComboBox.SelectedIndex = -1
        End Try
    End Sub


    Private Sub dontsavebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dontsavebtn.Click
        Me.Close()
    End Sub


    Private Sub catcombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles catcombobox.Validated

    End Sub


    Private Sub Comp_against_codeComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Comp_against_codeComboBox.Validated
        Try
            'against_code = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(Comp_against_codeComboBox.SelectedIndex).Item(0)
            agentComboBox.Items.Clear()
            If Comp_against_codeComboBox.SelectedIndex = -1 Then
                Exit Sub
            End If
            param2 = "select against_code from Complaint_against " & _
                        " where against_branch_no = " & branch_no & _
                         " and against_text = '" & Comp_against_codeComboBox.Text & "'"
            Dim against_ds As DataSet = get_dataset("Complaints", param2)
            against_code = against_ds.Tables(0).Rows(0).Item(0)
            If against_code <> last_against_code Then
                last_against_code = against_code
            Else
                Exit Sub
            End If


            'against_code = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(Comp_against_codeComboBox.SelectedIndex).Item(0)
            Dim idx As Integer
            If cmpny_no = 0 Then
                Dim where_clause As String = ""
                If InStr(Comp_against_codeComboBox.Text, "FC") + _
                    InStr(Comp_against_codeComboBox.Text, "F C") > 0 Then
                    where_clause = " where agent_type = 'B' and internalExternal = 'I' and branchID <> 10 "
                ElseIf InStr(Comp_against_codeComboBox.Text, "Van") + _
                    InStr(Comp_against_codeComboBox.Text, "VB") > 0 Then
                    where_clause = " where agent_type = 'B' and internalExternal = 'E' and branchID <> 10 "
                ElseIf Comp_against_codeComboBox.Text = "High Court" Then
                    where_clause = " where agent_type = 'B' and (internalExternal = 'I' or internalExternal = 'E') and branchID <> 10 "
                ElseIf Comp_against_codeComboBox.Text = "Marston" Then
                    where_clause = " where agent_type = 'B' "
                ElseIf Comp_against_codeComboBox.Text = "Contact Centre" Then
                    where_clause = " where (agent_type = 'P' or agent_type = 'A') "
                ElseIf Comp_against_codeComboBox.Text = "Rossendales" Then
                    where_clause = " where branchID <> 10"
                Else
                    where_clause = " where branchID <> 10"
                End If
                populate_bailiff_table(where_clause)


                For idx = 1 To agent_rows
                    agentComboBox.Items.Add(bailiff_table(idx, 2))
                Next
                agentComboBox.Items.Add("Various")
            Else
                param1 = "Complaints"
                param2 = "select agnt_code, agnt_name from Complaint_Agents" & _
                " order by agnt_code"
                Dim agnt_ds As DataSet = get_dataset(param1, param2)
                agent_rows = agnt_ds.Tables(0).Rows.Count - 1
                ReDim bailiff_table(no_of_rows, no_of_rows)
                For idx = 0 To agent_rows
                    agentComboBox.Items.Add(agnt_ds.Tables(0).Rows(idx).Item(1))
                    bailiff_table(idx, 1) = agnt_ds.Tables(0).Rows(idx).Item(0)
                    bailiff_table(idx, 2) = agnt_ds.Tables(0).Rows(idx).Item(1)
                Next
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    'Private Sub populate_departments()
    '    Me.DepartmentsTableAdapter.Fill(Me.DepartmentsDataSet.Departments)
    '    agentComboBox.Items.Clear()
    '    Dim idx As Integer = 0
    '    Dim row As DataRow
    '    For Each row In DepartmentsDataSet.Tables(0).Rows
    '        agentComboBox.Items.Add(DepartmentsDataSet.Tables(0).Rows(idx).Item(1))
    '        idx += 1
    '    Next
    '    dept_rows = idx
    'End Sub
    Private Sub Comp_recvd_codeComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Comp_recvd_codeComboBox.Validated
        'get recvd from number
        'recvd_from = PraiseAndComplaintsSQLDataSet.Received_from.Rows(Comp_recvd_codeComboBox.SelectedIndex).Item(0)
        param2 = "select recvd_from from Received_from " & _
        " where recvd_text = '" & Comp_recvd_codeComboBox.Text & "'"
        Dim recvd_ds As DataSet = get_dataset("complaints", param2)
        Try
            recvd_from = recvd_ds.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            MsgBox("Invalid complainant")
            Comp_recvd_codeComboBox.SelectedIndex = -1
        End Try
    End Sub


    Private Sub cl_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles cl_combobox.Validated
        Dim idx As Integer
        For idx = 1 To cl_rows
            If client_table(idx, 2) = cl_combobox.SelectedItem Then
                cl_no = client_table(idx, 1)
                Exit For
            End If
        Next
    End Sub


    Private Sub agentComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles agentComboBox.Validated
        Dim idx As Integer
        agent_no = 0
        ErrorProvider1.SetError(agentComboBox, "")
        If agentComboBox.SelectedItem = "Various" Then
            agent_no = 9999
        Else
            For idx = 0 To agent_rows
                If bailiff_table(idx, 2) = agentComboBox.SelectedItem Then
                    agent_no = bailiff_table(idx, 1)
                    Exit For
                End If
            Next
        End If
        dept_code = 0
        Exit Sub

        agent_no = 0
    End Sub

    Private Sub Comp_recpt_codeComboBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Comp_recpt_codeComboBox.Validating
        ErrorProvider1.SetError(Comp_recpt_codeComboBox, "")
        If Len(Comp_recpt_codeComboBox.Text) = 0 Then
            ErrorProvider1.SetError(Comp_recpt_codeComboBox, "You must select a form of receipt")
            e.Cancel = True
        End If
    End Sub

    Private Sub Comp_recvd_codeComboBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Comp_recvd_codeComboBox.Validating
        ErrorProvider1.SetError(Comp_recvd_codeComboBox, "")
        If Len(Comp_recvd_codeComboBox.Text) = 0 Then
            ErrorProvider1.SetError(Comp_recvd_codeComboBox, "You must select received from")
            e.Cancel = True
        End If
    End Sub

    Private Sub addbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addbtn.Click
        OpenFileDialog1.FileName = ""
        OpenFileDialog1.Filter = "Text,word or tif|*.txt; *.doc;*.tif"
        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim fname As String = OpenFileDialog1.FileName
            Dim dirname As String = "T" & log_code
            save_document(fname, dirname)
        End If
        doc_textbox.Text = doc_textbox.Text + 1

    End Sub

    Private Sub compTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles compTextBox.TextChanged

    End Sub

    Private Sub Comp_against_codeComboBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles Comp_against_codeComboBox.Validating
        ErrorProvider1.SetError(Comp_against_codeComboBox, "")
        'If Len(Comp_against_codeComboBox.Text) = 0 And branch_cbox.SelectedIndex >= 0 Then
        '    ErrorProvider1.SetError(Comp_against_codeComboBox, "You must select who the complaint is against")
        '    e.Cancel = True
        'End If
    End Sub

    Private Sub codeComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles codeComboBox.Validated
        ErrorProvider1.SetError(codeComboBox, "")
    End Sub

    Private Sub InvComboBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles InvComboBox.Validated
        'Try
        '    alloc_to_code = PraiseAndComplaintsSQLDataSet.Investigators.Rows(InvComboBox.SelectedIndex).Item(0)
        'Catch
        '    alloc_to_code = 1
        'End Try
        param2 = "select inv_code from Investigators " & _
        " where inv_text = '" & InvComboBox.Text & "'"
        Dim inv_ds As DataSet = get_dataset("complaints", param2)
        Try
            alloc_to_code = inv_ds.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            alloc_to_code = 1
        End Try
    End Sub

    Private Sub old_comp_no_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles old_comp_no_tbox.Validating
        ErrorProvider1.SetError(old_comp_no_tbox, "")
        If Microsoft.VisualBasic.Len(Trim(old_comp_no_tbox.Text)) = 0 Then
            old_comp_no = 0
            Exit Sub
        Else
            Try
                old_comp_no = Trim(old_comp_no_tbox.Text)
            Catch ex As Exception
                ErrorProvider1.SetError(old_comp_no_tbox, "Old complaint number must be blank or numeric")
                e.Cancel = True
            End Try
        End If
        If old_comp_no > 9999 Then
            ErrorProvider1.SetError(old_comp_no_tbox, "Old complaint number too high (up to 9999)")
            e.Cancel = True
        End If
    End Sub

    Private Sub eth_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles eth_combobox.SelectedIndexChanged

    End Sub

    Private Sub eth_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles eth_combobox.Validated
        ethnicity_desc = eth_combobox.Text
    End Sub

    Private Sub case_noTextBox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles case_noTextBox.TextChanged


    End Sub

    Private Sub ack_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ack_cbox.CheckedChanged
        ack_datepicker.Visible = ack_cbox.Checked
    End Sub

    Private Sub ack_cbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ack_cbox.Validating

    End Sub

    Private Sub ack_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ack_datepicker.Validating
        ErrorProvider1.SetError(ack_datepicker, "")
        If Format(ack_datepicker.Value, "yyyy.MM.dd") < Format(Comp_dateDateTimePicker.Value, "yyyy.MM.dd") Then
            ErrorProvider1.SetError(ack_datepicker, "Acknowledgement date can't be before complaint entry date")
            e.Cancel = True
        End If
        If Format(ack_datepicker.Value, "yyyy.MM.dd") > Format(Now, "yyyy.MM.dd") Then
            ErrorProvider1.SetError(ack_datepicker, "Acknowledgement date can't be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub Comp_against_codeComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Comp_against_codeComboBox.SelectedIndexChanged

    End Sub

    Private Sub agentComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agentComboBox.SelectedIndexChanged

    End Sub

    Private Sub comp_against2_codecombobox_TextUpdate(ByVal sender As Object, ByVal e As System.EventArgs) Handles comp_against2_codecombobox.TextUpdate

    End Sub

    Private Sub comp_against2_codecombobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles comp_against2_codecombobox.Validated
        If comp_against2_codecombobox.SelectedIndex < 0 Then
            Exit Sub
        End If
        Try
            Dim against2_name As String = comp_against2_codecombobox.Text
            param2 = "select against_code from Complaint_against " & _
             " where against_branch_no = " & branch_no & " and against_text = '" & against2_name & "'"
            Dim against2_ds As DataSet = get_dataset("Complaints", param2)
            against2_code = against2_ds.Tables(0).Rows(0).Item(0)
            'against2_code = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(comp_against2_codecombobox.SelectedIndex).Item(0)
            If against2_code <> last_against2_code Then
                last_against2_code = against2_code
            Else
                Exit Sub
            End If
            Dim idx As Integer
            agent2_combobox.Items.Clear()
            'against2_code = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(comp_against2_codecombobox.SelectedIndex).Item(0)
            If cmpny_no = 0 Then
                Dim where_clause As String
                If InStr(against2_name, "FC") + InStr(against2_name, "F C") > 0 Then
                    where_clause = " where agent_type = 'B' and internalExternal = 'I' and branchID <> 10 "
                ElseIf InStr(against2_name, "VB") + InStr(against2_name, "Van") > 0 Then
                    where_clause = " where agent_type = 'B' and internalExternal = 'E' and branchID <> 10 "
                ElseIf comp_against2_codecombobox.Text = "High Court" Then
                    where_clause = " where agent_type = 'B' and (internalExternal = 'I' or internalExternal = 'E') and branchID <> 10 "
                ElseIf comp_against2_codecombobox.Text = "Marston" Then
                    where_clause = " where agent_type = 'B' "
                ElseIf comp_against2_codecombobox.Text = "Contact Centre" Then
                    where_clause = " where (agent_type = 'P' or agent_type = 'A') "
                Else
                    where_clause = " where (agent_type = 'P' or agent_type = 'A') and branchID <> 10 "
                End If

                populate_bailiff_table(where_clause)


                For idx = 1 To agent_rows
                    agent2_combobox.Items.Add(bailiff_table(idx, 2))
                Next
                agent2_combobox.Items.Add("Various")
            Else
                param1 = "Complaints"
                param2 = "select agnt_code, agnt_name from Complaint_Agents" & _
                " order by agnt_code"
                Dim agnt_ds As DataSet = get_dataset(param1, param2)
                agent2_rows = agnt_ds.Tables(0).Rows.Count - 1
                ReDim bailiff2_table(no_of_rows, no_of_rows)
                For idx = 0 To agent2_rows
                    agent2_combobox.Items.Add(agnt_ds.Tables(0).Rows(idx).Item(1))
                    bailiff2_table(idx, 1) = agnt_ds.Tables(0).Rows(idx).Item(0)
                    bailiff2_table(idx, 2) = agnt_ds.Tables(0).Rows(idx).Item(1)
                Next
            End If
            
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub agent2_combobox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles agent2_combobox.Validated
        Dim idx As Integer
        agent2_no = 0
        ErrorProvider1.SetError(agent2_combobox, "")
        If agent2_combobox.SelectedItem = "Various" Then
            agent2_no = 9999
        Else
            For idx = 1 To agent_rows
                If bailiff_table(idx, 2) = agent2_combobox.SelectedItem Then
                    agent2_no = bailiff_table(idx, 1)
                    Exit For
                End If
            Next
        End If
        dept_code = 0
        Exit Sub

        agent2_no = 0
    End Sub

    Private Sub agent2_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agent2_combobox.SelectedIndexChanged

    End Sub

    Private Sub codeComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles codeComboBox.SelectedIndexChanged
        Dim selected_code As Integer = Microsoft.VisualBasic.Left(codeComboBox.Text, 2)
        type_code_cbox.Items.Clear()
        If (catcombobox.Text = "A" And selected_code = 9) Or _
        (catcombobox.Text = "B" And selected_code = 11) Or _
        (catcombobox.Text = "C" And selected_code = 8) Then
            'updatecmpfrm.Complaints_type_codeTableAdapter.Fill(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_type_code)
            param2 = "select type_name from Complaints_type_code " & _
            " order by type_name"
            Dim type_ds As DataSet = get_dataset("complaints", param2)
            'Dim idx As Integer
            'For idx = 0 To updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_type_code.Rows.Count - 1
            For Each row In type_ds.Tables(0).Rows
                'type_code_cbox.Items.Add(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_type_code.Rows(idx).Item(1))
                type_code_cbox.Items.Add(row(0))
            Next
        End If
    End Sub

    Private Sub codeComboBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles codeComboBox.Validating
        ErrorProvider1.SetError(codeComboBox, "")
        If codeComboBox.SelectedItem = "99 Contentious" Then
            ErrorProvider1.SetError(codeComboBox, "Contentious is no longer valid")
            e.Cancel = True
        End If
    End Sub


    Private Sub legalcbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles legalcbox.CheckedChanged
        If legalcbox.Checked = True Then
            If inscbox.Checked = False Then
                monetary_risk = -999
            End If
            solicitor_datepicker.Visible = True
            monetary_risktbox.Visible = True
        Else
            solicitor_datepicker.Visible = False
            If inscbox.Checked = False Then
                monetary_risktbox.Visible = False
            End If
        End If
    End Sub

    Private Sub legalcbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles legalcbox.Validated

    End Sub

    Private Sub legal_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles solicitor_datepicker.Validating
        ErrorProvider1.SetError(solicitor_datepicker, "")
        If solicitor_datepicker.Checked = True Then
            If Format(CDate(solicitor_datepicker.Text), "yyyy-MM-dd") < Format(Comp_dateDateTimePicker.Value, "yyyy-MM-dd") Then
                ErrorProvider1.SetError(solicitor_datepicker, "Date can't be before entered date")
                e.Cancel = True
            ElseIf solicitor_datepicker.Text > Now Then
                ErrorProvider1.SetError(solicitor_datepicker, "Date can't be in the future")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub legal_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles solicitor_datepicker.ValueChanged

    End Sub

    Private Sub ins_datepicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles ins_datepicker.Validating
        ErrorProvider1.SetError(ins_datepicker, "")
        If ins_datepicker.Checked = True Then
            If Format(CDate(ins_datepicker.Text), "yyyy-MM-dd") < Format(Comp_dateDateTimePicker.Value, "yyyy-MM-dd") Then
                ErrorProvider1.SetError(ins_datepicker, "Date can't be before entered date")
                e.Cancel = True
            ElseIf ins_datepicker.Text > Now Then
                ErrorProvider1.SetError(ins_datepicker, "Date can't be in the future")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub monetary_risktbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles monetary_risktbox.TextChanged

    End Sub

    Private Sub monetary_risktbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles monetary_risktbox.Validating
        ErrorProvider1.SetError(monetary_risktbox, "")
        If Microsoft.VisualBasic.Len(Trim(monetary_risktbox.Text)) = 0 Then
            monetary_risk = -999
        Else
            If Not IsNumeric(monetary_risktbox.Text) Then
                ErrorProvider1.SetError(monetary_risktbox, "Please enter valid amount")
                e.Cancel = True
            Else
                monetary_risk = monetary_risktbox.Text
                If monetary_risk < 0 Then
                    ErrorProvider1.SetError(monetary_risktbox, "Please enter positive amount")
                    e.Cancel = True
                End If
            End If
        End If
    End Sub

    Private Sub ins_datepicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ins_datepicker.ValueChanged

    End Sub

    Private Sub Comp_dateDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Comp_dateDateTimePicker.ValueChanged

    End Sub

    Private Sub category_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles category_cbox.SelectedIndexChanged

    End Sub

    Private Sub category_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles category_cbox.Validated
        category = category_cbox.SelectedIndex
    End Sub

    Private Sub category_cbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles category_cbox.Validating
        ErrorProvider1.SetError(category_cbox, "")
        If category_cbox.SelectedIndex = 0 Then
            ErrorProvider1.SetError(category_cbox, "Category can't be blank")
            e.Cancel = True
        End If
    End Sub

    Private Sub comp_against2_codecombobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comp_against2_codecombobox.SelectedIndexChanged

    End Sub

    Private Sub branch_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles branch_cbox.SelectedIndexChanged

    End Sub

    Private Sub branch_cbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles branch_cbox.Validated
        Dim branch_name As String = branch_cbox.Text
        param2 = "select branch_code from Complaint_branches " & _
        " where branch_name = '" & branch_name & "'" & _
        " and branch_cmpny_no = " & cmpny_no
        Dim branch_ds As DataSet = get_dataset("Complaints", param2)
        branch_no = branch_ds.Tables(0).Rows(0).Item(0)
        'Me.Complaint_againstTableAdapter.FillBy2(Me.PraiseAndComplaintsSQLDataSet.Complaint_against, branch_no)
        'Dim idx As Integer = 0
        'comp against code/2 combobox
        Comp_against_codeComboBox.Items.Clear()
        comp_against2_codecombobox.Items.Clear()
        param2 = "select against_code, against_text from Complaint_against " & _
        " where against_branch_no = " & branch_no & " order by against_code"
        Dim against_ds As DataSet = get_dataset("Complaints", param2)
        Dim against_rows As Integer = no_of_rows - 1
        Dim against_idx As Integer
        For against_idx = 0 To against_rows
            Comp_against_codeComboBox.Items.Add(against_ds.Tables(0).Rows(against_idx).Item(1))
            comp_against2_codecombobox.Items.Add(against_ds.Tables(0).Rows(against_idx).Item(1))
        Next
        'For Each row In Me.PraiseAndComplaintsSQLDataSet.Complaint_against.Rows
        '    comp_against2_codecombobox.Items.Add(Me.PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(idx).Item(1))
        '    idx += 1
        'Next
    End Sub

    Private Sub legalgbox_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub inscbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles inscbox.CheckedChanged
        If inscbox.Checked = True Then
            If legalcbox.Checked = False Then
                monetary_risk = -999
            End If
            monetary_risktbox.Visible = True
            liability_cbox.Visible = True
            ins_datepicker.Visible = True
            ins_Panel.Visible = True
            pi_cbox.Visible = True
            pl_cbox.Visible = True
        Else
            liability_cbox.Visible = False
            ins_datepicker.Visible = False
            ins_Panel.Visible = False
            pi_cbox.Visible = False
            pl_cbox.Visible = True
            If legalcbox.Checked = False Then
                monetary_risktbox.Visible = False
            End If
        End If
    End Sub

    Private Sub inscbox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles inscbox.Validated
        
    End Sub

    Private Sub catcombobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles catcombobox.SelectedIndexChanged
        Try
            ErrorProvider1.SetError(catcombobox, "")
            'Dim cat_table As New PraiseAndComplaintsSQLDataSet.Complaint_categoriesDataTable
            Dim cat_code As String = Microsoft.VisualBasic.Left(catcombobox.SelectedItem, 1)
            'Me.Complaint_categoriesTableAdapter.Fill2(cat_table, cat_code)
            param2 = "select cat_number, cat_text from Complaint_categories " & _
            " where cat_code = '" & cat_code & "'" & _
            " order by cat_number"
            Dim cat_ds As DataSet = get_dataset("complaints", param2)
            Dim row As DataRow
            Dim cat_text As String
            Dim idx As Integer = 0
            codeComboBox.Items.Clear()
            type_code_cbox.Items.Clear()
            'For Each row In cat_table.Rows
            For Each row In cat_ds.Tables(0).Rows
                'cat_text = cat_table.Rows(idx).Item(1) & " " & cat_table.Rows(idx).Item(2)
                cat_text = row(0) & " " & row(1)
                codeComboBox.Items.Add(cat_text)
                idx += 1
            Next
            codeComboBox.SelectedIndex = -1
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub type_code_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles type_code_cbox.SelectedIndexChanged
        'type_code = type_code_cbox.SelectedIndex
        param2 = "select type_code from Complaints_type_code " & _
        " where type_name = '" & type_code_cbox.Text & "'"
        Dim type_ds As DataSet = get_dataset("complaints", param2)
        Try
            type_code = type_ds.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            type_code = 0
        End Try
    End Sub

    Private Sub unk_rbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles unk_rbtn.CheckedChanged

    End Sub

    Private Sub cl_combobox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cl_combobox.SelectedIndexChanged

    End Sub

    Private Sub Label18_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub InvComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InvComboBox.SelectedIndexChanged

    End Sub

    Private Sub Comp_recpt_codeComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Comp_recpt_codeComboBox.SelectedIndexChanged

    End Sub

    Private Sub Comp_recvd_codeComboBox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Comp_recvd_codeComboBox.SelectedIndexChanged

    End Sub

    Private Sub branch_cbox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles branch_cbox.Validating
        ErrorProvider1.SetError(branch_cbox, "")
    End Sub
End Class



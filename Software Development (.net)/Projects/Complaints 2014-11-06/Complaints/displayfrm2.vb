Imports System.IO
Public Class displayfrm2
    Dim gtot_cases As Integer
    Private Sub display_using_build()
        Try
            file = "Investigator,Complaint No,Case No,Client Ref,Recvd date,Branch,Category,Stage 1 completed,Stage 2 recvd," & _
                        "Stage 2 completed,Stage 3 recvd, Stage 3 completed,Days taken,Days left," & _
                        "client,Scheme, Complainant,against,agent,Complainant2,Against2,Category Code,hold letter," & _
                        "Founded,Start Hold date, days on Hold,Date Notified to Insurers,Insurance Type,Prof Indemnity,Public Liability,Complaint Details,Response,Other Information" & vbNewLine
            comp_dg.Rows.Clear()
            Dim select_param As String = " SELECT comp_no, comp_date, comp_recpt_code, comp_recvd_code, " & _
                        " comp_case_no, comp_client_no, comp_against_code, comp_against_agent, " & _
                        " comp_cat_code, comp_cat_number, comp_dept_code, comp_entered_by, comp_allocated_to," & _
                        "comp_text, comp_completion_date, comp_completed_by, comp_checked_date, comp_checked_by," & _
                        " comp_founded, comp_response, comp_action, comp_stage_no, comp_cor_code, comp_hold_code," & _
                        " comp_priority, comp_old_comp_no, comp_costs_cancel_value, comp_compensation," & _
                        " comp_response_no, comp_stage2_start_date, comp_stage2_completed_by," & _
                        " comp_stage2_completed_date, comp_gender, comp_ethnicity, comp_ack_letter_date," & _
                        " comp_holding_letter_date, comp_stage2_ack_letter_date, comp_stage2_holding_letter_date," & _
                        " comp_stage3_start_date, comp_stage3_completed_by, comp_stage3_completed_date," & _
                        " comp_against_code2, comp_against_agent2, Comp_referred_to_solicitor," & _
                        " Comp_referred_to_insurer, Comp_monetary_risk, Comp_legal_insurance_flag ," & _
                        " comp_cancel_costs_reason, Comp_category, Comp_branch_no, Comp_insurance_type_no," & _
                        " Comp_liability_flag, Comp_part_founded_agent_no, Comp_feedback_no," & _
                        " Comp_type_code, Comp_prof_indemnity_no, comp_public_liability_no, " & _
                        " comp_cmpny_no FROM Complaints " & _
                        " where  comp_no >= " & comp_start_no
            Select Case search_no
                Case 2
                    select_param &= " and comp_case_no = " & case_no
                    held_search = "A"
                Case 3
                    select_param &= " and comp_client_no = " & cl_no
                    held_search = "A"
                    'Case 4
                    '    select_param  &= " and (comp_against_agent = " & agent1 & _
                    '    " or comp_against_agent2= " & agent2 & ")"
                    '    held_search = "A"
                Case 7
                    select_param &= " and comp_allocated_to = " & inv_code
                    held_search = "A"
            End Select

            'company no
            If cmpny_no = 0 Then
                select_param &= " and (comp_cmpny_no is null " & _
                           " or comp_cmpny_no = 0)"
            ElseIf cmpny_no <> 3 Then  '3 is ALL companies
                select_param &= " and comp_cmpny_no = " & cmpny_no
            End If

            If cat_text <> "" Then
                select_param &= " and comp_cat_code = '" & cat_text & "' "
            End If

            If displayfrm.category_combobox.SelectedIndex > 0 Then
                select_param &= " and comp_category = " & displayfrm.category_combobox.SelectedIndex
            End If

            'branch
            If search_branch_no >= 0 Then
                param2 = "select branch_code from complaint_branches " & _
                " where branch_name = '" & search_branch_name & "'"
                If cmpny_no <> 3 Then
                    param2 &= " and branch_cmpny_no = " & cmpny_no
                ElseIf cmpny_no <> 3 Then  '3 is ALL companies
                    select_param &= " and comp_cmpny_no = " & cmpny_no
                End If
                Dim branch_ds As DataSet = get_dataset("complaints", param2)
                Try
                    select_param &= " and comp_branch_no = " & branch_ds.Tables(0).Rows(0).Item(0)
                Catch ex As Exception

                End Try

            End If
            If founded_search <> "A" Then
                select_param &= " and comp_founded = '" & founded_search & "'"
            End If

            'check stage
            Dim search_stage_no As Integer = 0
            If search_stage_text <> "" Then
                If search_stage_text = "Legal" Then
                    select_param &= " and Comp_legal_insurance_flag = 'Y'"
                ElseIf search_stage_text = "Insurance" Then
                    select_param &= " and Comp_insurance_type_no >= 0"
                Else
                    search_stage_no = Microsoft.VisualBasic.Right(search_stage_text, 1)
                    select_param &= " and Comp_stage_no = " & search_stage_no
                End If
            End If

            If displayfrm.recvd_cbox.Checked Then
                'to ensure all complaints picked up
                'add a day to end date and use <
                Dim end_date As Date
                end_date = DateAdd("d", 1, displayfrm.end_recvd_datepicker.Value)
                Select Case search_stage_no
                    Case 0

                        select_param &= " and ((comp_date>='" & Format(displayfrm.start_recvd_datepicker.Value, "yyyy-MM-dd") & "'" & _
                                       " and comp_date < '" & Format(end_date, "yyyy-MM-dd") & "')"
                        select_param &= " or (comp_stage2_start_date is not null and (comp_stage2_start_date>='" & Format(displayfrm.start_recvd_datepicker.Value, "yyyy-MM-dd") & "'" & _
                                                               " and comp_stage2_start_date < '" & Format(end_date, "yyyy-MM-dd") & "'))"
                        select_param &= " or (comp_stage3_start_date is not null and (comp_stage3_start_date>='" & Format(displayfrm.start_recvd_datepicker.Value, "yyyy-MM-dd") & "'" & _
                                                               " and comp_stage3_start_date < '" & Format(end_date, "yyyy-MM-dd") & "')))"
                    Case 1

                        select_param &= " and comp_date>='" & Format(displayfrm.start_recvd_datepicker.Value, "yyyy-MM-dd") & "'" & _
                                       " and comp_date < '" & Format(end_date, "yyyy-MM-dd") & "'"
                    Case 2

                        select_param &= " and comp_stage2_start_date>='" & Format(displayfrm.start_recvd_datepicker.Value, "yyyy-MM-dd") & "'" & _
                                       " and comp_stage2_start_date < '" & Format(end_date, "yyyy-MM-dd") & "'"
                    Case 3

                        select_param &= " and comp_stage3_start_date>='" & Format(displayfrm.start_recvd_datepicker.Value, "yyyy-MM-dd") & "'" & _
                                       " and comp_stage3_start_date < '" & Format(end_date, "yyyy-MM-dd") & "'"
                End Select
            End If
            Select Case search_stage_no
                Case 0
                    If completed_search = "N" Then
                        select_param &= " and (comp_completed_by = 0" & _
                        "  or (comp_stage_no > 1 and (comp_stage2_completed_by is null" & _
                           " or comp_stage2_completed_by =0))" & _
                        " or (comp_stage_no > 2 and (comp_stage3_completed_by is null" & _
                           " or comp_stage3_completed_by = 0)))"
                    ElseIf completed_search = "Y" Then
                        select_param &= " and (comp_completed_by > 0" & _
                        " or (comp_stage_no > 1 and comp_stage2_completed_by > 0)" & _
                        " or (comp_stage_no > 2 and comp_stage3_completed_by > 0))"
                    End If
                Case 1
                    If completed_search = "N" Then
                        select_param &= " and comp_completed_by = 0 "
                    ElseIf completed_search = "Y" Then
                        select_param &= " and comp_completed_by > 0 "
                    End If
                Case 2
                    If completed_search = "N" Then
                        select_param &= " and (comp_stage2_completed_by is null" & _
                        " or comp_stage2_completed_by = 0)"
                    ElseIf completed_search = "Y" Then
                        select_param &= " and comp_stage2_completed_by > 0"
                    End If
                Case 3
                    If completed_search = "N" Then
                        select_param &= " and (comp_stage3_completed_by is null" & _
                       " or comp_stage3_completed_by = 0)"
                    ElseIf completed_search = "Y" Then
                        select_param &= " and comp_stage3_completed_by > 0"
                    End If

            End Select

            'received from
            If search_recvd_text <> "" Then
                param2 = " select recvd_from from Received_from " & _
                " where recvd_text = '" & search_recvd_text & "'"
                Dim recvd_ds As DataSet = get_dataset("complaints", param2)
                If no_of_rows = 1 Then
                    select_param &= " and comp_recvd_code = " & recvd_ds.Tables(0).Rows(0).Item(0)
                End If
            End If

            'complaint against
            If search_dept <> "" Then
                param2 = " select against_code from Complaint_against " & _
                " where against_text = '" & search_dept & "'"
                Dim against_ds As DataSet = get_dataset("complaints", param2)
                Dim againstIDX As Integer
                For againstIDX = 0 To no_of_rows - 1
                    If againstIDX = 0 Then
                        select_param &= " and ((comp_against_code = " & _
                                           against_ds.Tables(0).Rows(againstIDX).Item(0) & _
                                          " or (comp_against_code2 is not null and comp_against_code2 = " & _
                                          against_ds.Tables(0).Rows(againstIDX).Item(0) & "))"
                    Else
                        select_param &= " or (comp_against_code = " & _
                                           against_ds.Tables(0).Rows(againstIDX).Item(0) & _
                                          " or (comp_against_code2 is not null and comp_against_code2 = " & _
                                          against_ds.Tables(0).Rows(againstIDX).Item(0) & "))"
                    End If
                    If againstIDX = no_of_rows - 1 Then
                        select_param &= ")"
                    End If
                Next
            End If

            'completed month
            If displayfrm.comp_cbox.Checked Then
                Dim month_start_date As Date = displayfrm.comp_datepicker.Value
                month_start_date = CDate(Format(month_start_date, "yyyy-MM-") & "01 00:00:00")
                Dim month_end_date As Date = DateAdd("m", 1, month_start_date)

                Select Case search_stage_text
                    Case ""
                        select_param &= " and ((comp_completed_by > 0 and comp_completion_date >='" & _
                        Format(month_start_date, "yyyy-MM-dd") & "'" & _
                        " and comp_completion_date < '" & Format(month_end_date, "yyyy-MM-dd") & "')" & _
                        " or (comp_stage2_completed_by is not null and comp_stage2_completed_by > 0 " & _
                        " and comp_stage2_completed_date >= '" & Format(month_start_date, "yyyy-MM-dd") & "'" & _
                        " and comp_stage2_completed_date < '" & Format(month_end_date, "yyyy-MM-dd") & "')" & _
                        " or (comp_stage3_completed_by is not null and comp_stage3_completed_by > 0 " & _
                        " and comp_stage3_completed_date >= '" & Format(month_start_date, "yyyy-MM-dd") & "'" & _
                        " and comp_stage3_completed_date < '" & Format(month_end_date, "yyyy-MM-dd") & "'))"
                    Case "Stage 1"
                        select_param &= " and comp_completed_by > 0 and comp_completion_date >='" & _
                        Format(month_start_date, "yyyy-MM-dd") & "'" & _
                        " and comp_completion_date < '" & Format(month_end_date, "yyyy-MM-dd") & "'"
                    Case "Stage 2"
                        select_param &= " and comp_stage2_completed_by Is Not null And comp_stage2_completed_by > 0 " & _
                        " and comp_stage2_completed_date >= '" & Format(month_start_date, "yyyy-MM-dd") & "'" & _
                        " and comp_stage2_completed_date < '" & Format(month_end_date, "yyyy-MM-dd") & "'"
                    Case "Stage 3"
                        select_param &= " and comp_stage3_completed_by is not null and comp_stage3_completed_by > 0 " & _
                        " and comp_stage3_completed_date >= '" & Format(month_start_date, "yyyy-MM-dd") & "'" & _
                        " and comp_stage3_completed_date < '" & Format(month_end_date, "yyyy-MM-dd") & "'"
                End Select
            End If

            'receipt type
            If search_receipt_type <> "" Then
                param2 = " select recpt_code from receipt_type" & _
                " where recpt_text = '" & search_receipt_type & "'"
                Dim recpt_ds As DataSet = get_dataset("complaints", param2)
                If no_of_rows = 1 Then
                    select_param &= " and comp_recpt_code = " & recpt_ds.Tables(0).Rows(0).Item(0)
                End If
            End If

            'agent selected
            Dim idx As Integer
            If displayfrm.agentcombobox.SelectedItem = "Various" Then
                agent1 = 9999
            ElseIf displayfrm.agentcombobox.SelectedItem <> "" Then
                For idx = 1 To agent_rows
                    If search_bailiff_table(idx, 2) = displayfrm.agentcombobox.SelectedItem Then
                        If agent1 = -1 Then
                            agent1 = search_bailiff_table(idx, 1)
                        Else
                            agent2 = search_bailiff_table(idx, 1)
                            Exit For
                        End If
                    End If
                Next
            End If
            If agent1 + agent2 > 0 Then
                select_param &= " and ((comp_against_agent = " & agent1 & _
                                " or comp_against_agent = " & agent2 & ") " & _
                                " or (comp_against_agent2 is not null " & _
                                " and (comp_against_agent2 = " & agent1 & _
                                " or comp_against_agent2 = " & agent2 & ")))"
            End If

            Dim comp_ds As DataSet = get_dataset("Complaints", select_param)

            Dim no_of_complaints As Integer = no_of_rows
            Dim CompIDX As Integer = 0
            For Each row In comp_ds.Tables(0).Rows
                Try
                    displayfrm.ProgressBar1.Value = (CompIDX / no_of_complaints) * 100
                Catch ex As Exception

                End Try
                CompIDX += 1
                Application.DoEvents()
                Dim comp_case_no As Integer
                Try
                    comp_case_no = row(4)
                Catch ex As Exception
                    comp_case_no = 0
                End Try
                'if scheme entered need to check if case number is on complaint
                If search_csid_no > 0 And comp_case_no > 0 Then
                    param2 = "select _rowid from Debtor where _rowid = " & comp_case_no & _
                    " and clientschemeID = " & search_csid_no
                    Dim debtor2_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        Continue For
                    End If
                End If

                Dim name As String = LCase(Trim(displayfrm.name_textbox.Text))
                name = LCase(Trim(name))
                If name.Length > 0 Then
                    If comp_case_no > 0 Then
                        param2 = "select name_sur from Debtor " & _
                        " where _rowid = " & comp_case_no
                        Dim name_dataset As DataSet = get_dataset("onestep", param2)
                        If no_of_rows = 0 Then
                            Continue For
                        End If
                        Dim name_found As String = Trim(LCase(name_dataset.Tables(0).Rows(0).Item(0)))
                        If name_found.Length > name.Length Then
                            If Microsoft.VisualBasic.Left(name_found, name.Length) <> name Then
                                Continue For
                            End If
                        Else
                            If Microsoft.VisualBasic.Left(name, name_found.Length) <> name_found Then
                                Continue For
                            End If
                        End If
                    End If
                End If
                Dim comp_no As Integer = row(0)
                If displayfrm.all_holds_rb.Checked = False Then
                    param2 = " select hold_end_date from Complaints_holds " & _
                    " where hold_comp_no = " & comp_no
                    Dim hold_ds As DataSet = get_dataset("complaints", param2)
                    Dim holdIDX As Integer
                    Dim onHold As Boolean = False
                    For holdIDX = 0 To no_of_rows - 1
                        Dim holdEndDate As Date = hold_ds.Tables(0).Rows(0).Item(0)
                        If holdEndDate = CDate("Jan 1, 1900") Then
                            onHold = True
                        Else
                            If holdEndDate > Now Then
                                onHold = True
                            End If
                        End If
                        If onHold Then
                            Exit For
                        End If
                    Next
                    If onHold Then
                        If displayfrm.not_hold_rb.Checked Then
                            Continue For
                        End If
                    Else
                        If displayfrm.hold_rb.Checked Then
                            Continue For
                        End If
                    End If
                End If
                Dim client_ref As String = ""
                Dim scheme_name As String = ""
                Dim work_type As Integer = 0
                param2 = "select clientschemeID, client_ref from Debtor where _rowid = " & comp_case_no
                Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 1 Then
                    Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(0)
                    client_ref = debtor_dataset.Tables(0).Rows(0).Item(1)
                    param2 = "select schemeID from ClientScheme where _rowid =  " & csid
                    Dim cs_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 1 Then
                        Dim sch_id As Integer = cs_dataset.Tables(0).Rows(0).Item(0)
                        param2 = "select work_type, name from Scheme where _rowid = " & sch_id
                        Dim sch_dataset As DataSet = get_dataset("onestep", param2)
                        If no_of_rows = 1 Then
                            work_type = sch_dataset.Tables(0).Rows(0).Item(0)
                            scheme_name = sch_dataset.Tables(0).Rows(0).Item(1)
                            If displayfrm.debt_typecombobox.SelectedIndex > 0 Then
                                If comp_case_no = 0 Then
                                    idx += 1
                                    Continue For
                                End If
                                Select Case displayfrm.debt_typecombobox.SelectedIndex
                                    Case 1 'CTAX
                                        If work_type <> 2 Then
                                            idx += 1
                                            Continue For
                                        End If
                                    Case 2 'NNDR
                                        If work_type <> 3 Then
                                            idx += 1
                                            Continue For
                                        End If
                                    Case 3 'RTD
                                        If work_type <> 16 And work_type <> 20 Then
                                            idx += 1
                                            Continue For
                                        End If
                                End Select
                            End If
                        End If
                    End If
                End If
                Dim hold As String = ""
                Dim recvd_date As Date = row(1)
                If get_letter Then
                    hold = check_held_letter(comp_case_no, recvd_date)
                Else
                    hold = "?"
                End If

                Dim disp_comp_officer As String = ""
                param2 = "select inv_text from Investigators " & _
                " where inv_code = " & row(12)
                Dim inv_ds As DataSet = get_dataset("Complaints", param2)
                Try
                    disp_comp_officer = inv_ds.Tables(0).Rows(0).Item(0)
                Catch ex As Exception

                End Try

                Dim disp_comp_no As Integer = row(0)


                Dim branch_name As String = ""
                param2 = "select branch_name from Complaint_branches " & _
                " where branch_code = " & row(49)
                Dim branch_ds As DataSet = get_dataset("Complaints", param2)
                Try
                    branch_name = branch_ds.Tables(0).Rows(0).Item(0)
                Catch ex As Exception

                End Try
                Dim cat_str As String = "?"
                Dim category_no As Integer
                Try
                    category_no = row(48)
                Catch ex As Exception
                    category_no = 0
                End Try
                Select Case category_no
                    Case 0
                        cat_str = "?"
                    Case 1
                        cat_str = "F"
                    Case 2
                        cat_str = "N"
                    Case 3
                        cat_str = "S"
                End Select

                Try
                    legal_flag = row(46)
                Catch ex As Exception
                    legal_flag = "N"
                End Try

                Dim days_taken As Integer
                Dim stage1_comp_date_string As String = ""
                Dim comp_completed_by As Integer = row(15)
                If comp_completed_by > 0 Then
                    stage1_comp_date_string = Format(row(14), "yyyy-MM-dd")
                    days_taken = DateDiff("d", row(1), row(14)) - _
                    (DateDiff("w", row(1), _
                         row(14)) * 2)
                    If Weekday(row(1)) > _
                        Weekday(row(14)) Then
                        days_taken = days_taken - 2
                    End If
                    If days_taken = 0 Then days_taken = 1
                Else
                    days_taken = DateDiff("d", row(1), Now) - _
                    (DateDiff("w", row(1), Now) * 2)
                    If Weekday(row(1)) > Weekday(Now) Then
                        days_taken = days_taken - 2
                    End If
                End If

                Dim cl_turn_days As Integer = 10
                Dim client_name As String = ""
                Dim cl_no As Integer = row(5)
                Dim comp_cat_number As Integer = row(9)
                Dim comp_cmpny_no As Integer
                Try
                    comp_cmpny_no = row(57)
                Catch ex As Exception
                    comp_cmpny_no = 0
                End Try

                If comp_cat_number = 101 Then
                    cl_turn_days = 40
                ElseIf cl_no > 0 Then
                    If comp_cmpny_no = 0 Then
                        param2 = "select name from Client" & _
                        " where _rowid = " & cl_no
                        Dim cl_dataset As DataSet = get_dataset("onestep", param2)
                        If no_of_rows = 1 Then
                            client_name = cl_dataset.Tables(0).Rows(0).Item(0)
                        Else
                            client_name = ""
                        End If
                    End If

                    param2 = "select cl_turn_days from Client_turnaround " & _
                    " where cl_no = " & cl_no
                    Dim turn_ds As DataSet = get_dataset("complaints", param2)
                    Try
                        cl_turn_days = turn_ds.Tables(0).Rows(0).Item(0)
                    Catch ex As Exception

                    End Try
                Else
                    'check if branch is LSC (branch_code=3)
                    If row(49) = 3 Then
                        cl_turn_days = 3
                    End If
                End If
                Dim days_left As Integer
                If comp_completed_by > 0 Or legal_flag = "Y" Then
                    days_left = Nothing
                Else
                    days_left = cl_turn_days - days_taken
                End If
                Dim stage_no As Integer = row(21)
                Dim stage2_start_date_string As String = ""
                Dim stage2_comp_date_string As String = ""
                Dim stage3_start_date_string As String = ""
                Dim stage3_comp_date_string As String = ""
                Try
                    stage2_start_date = row(29)
                Catch ex As Exception
                    stage2_start_date = Nothing
                End Try
                Try
                    stage3_start_date = row(38)
                Catch ex As Exception
                    stage3_start_date = Nothing
                End Try
                Try
                    stage2_completed_date = row(31)
                Catch ex As Exception
                    stage2_completed_date = Nothing
                End Try
                Try
                    stage3_completed_date = row(40)
                Catch ex As Exception
                    stage3_completed_date = Nothing
                End Try
                Try
                    stage3_completed_by = row(39)
                Catch ex As Exception
                    stage3_completed_by = Nothing
                End Try
                If stage_no = 2 Then
                    stage2_start_date_string = Format(stage2_start_date, "yyyy-MM-dd")
                    If stage2_completed_date <> Nothing Then
                        stage2_comp_date_string = Format(stage2_completed_date, "yyyy-MM-dd")
                    End If
                End If
                If stage_no = 3 Then
                    stage2_start_date_string = Format(stage2_start_date, "yyyy-MM-dd")
                    If stage2_completed_date <> Nothing Then
                        stage2_comp_date_string = Format(stage2_completed_date, "yyyy-MM-dd")
                    End If
                    stage3_start_date_string = Format(stage3_start_date, "yyyy-MM-dd")
                    If stage3_completed_date <> Nothing Then
                        stage3_comp_date_string = Format(stage3_completed_date, "yyyy-MM-dd")
                        If stage3_completed_by = 0 Then
                            stage3_comp_date_string = ""
                        End If
                    End If
                End If

                Dim recvd_from As Integer = row(3)
                param2 = "select recvd_text from Received_from " & _
                " where recvd_from = " & recvd_from
                Dim recvd_ds As DataSet = get_dataset("complaints", param2)
                Dim type As String = ""
                Try
                    type = recvd_ds.Tables(0).Rows(0).Item(0)
                Catch ex As Exception

                End Try

                Dim against As String = ""
                Try
                    against_code = row(6)
                Catch ex As Exception
                    against_code = 0
                End Try

                Dim against2 As String = ""
                Try
                    against2_code = row(41)
                Catch ex As Exception
                    against2_code = 0
                End Try

                param2 = "select against_text from Complaint_against " & _
                " where against_code = " & against_code
                Dim against_ds As DataSet = get_dataset("complaints", param2)
                Try
                    against = against_ds.Tables(0).Rows(0).Item(0)
                Catch ex As Exception

                End Try
                Dim agent_no As Integer
                Try
                    agent_no = row(7)
                Catch ex As Exception
                    agent_no = 0
                End Try

                Dim agent As String = ""
                If agent_no = 9999 Then
                    agent = "Various"
                Else
                    If agent_no > 0 Then
                        If comp_cmpny_no = 0 Then
                            param2 = "select name_sur, name_fore, status from Bailiff" & _
                            " where _rowid = " & agent_no
                            Dim bail_dataset As DataSet = get_dataset("onestep", param2)
                            If no_of_rows = 1 Then
                                Dim forename As String
                                Try
                                    forename = Trim(bail_dataset.Tables(0).Rows(0).Item(1))
                                Catch ex As Exception
                                    forename = ""
                                End Try
                                agent = Trim(bail_dataset.Tables(0).Rows(0).Item(0)) & _
                                                                     ", " & forename
                                If bail_dataset.Tables(0).Rows(0).Item(2) = "C" Then
                                    agent = agent & " (C)"
                                End If
                                agent = Replace(agent, ",", " ")
                            Else
                                agent = ""
                            End If
                        End If
                    End If
                End If

                Dim category_code As String = row(8) & " " & row(9)


                'founded/unfounded
                Dim founded As String = ""
                Try
                    founded = row(18)
                Catch ex As Exception
                    founded = ""
                End Try
                If founded = "U" Then founded = " "


                gtot_cases += 1
                comp_dg.Rows.Add(disp_comp_officer, disp_comp_no, comp_case_no, branch_name, _
                cat_str, legal_flag, Format(recvd_date, "yyyy-MM-dd"), _
                stage1_comp_date_string, stage2_start_date_string, stage2_comp_date_string, _
                stage3_start_date_string, stage3_comp_date_string, days_taken, days_left, _
                client_name, type, against, agent, category_code, hold, founded)

                'get cl_ref if required
                If client_ref = "" And comp_cmpny_no = 0 Then
                    param2 = "select client_ref from Debtor where _rowid = " & comp_case_no
                    Dim debt_dataset As DataSet = get_dataset("onestep", param2)

                    If no_of_rows = 0 Then
                        client_ref = ""
                    Else
                        client_ref = debt_dataset.Tables(0).Rows(0).Item(0)
                    End If
                End If
                Dim agent_no2_str As String = ""
                Dim agent_no2 As Integer
                Try
                    agent_no2 = row(42)
                Catch ex As Exception

                End Try
                If agent_no2 = 9999 Then
                    agent_no2_str = "Various"
                Else
                    If agent_no2 > 0 Then
                        If comp_cmpny_no = 0 Then
                            param1 = "onestep"
                            param2 = "select name_sur, name_fore, status from Bailiff" & _
                            " where _rowid = " & agent_no2
                            Dim bail_dataset As DataSet = get_dataset(param1, param2)
                            If no_of_rows = 1 Then
                                Dim forename As String
                                Try
                                    forename = Trim(bail_dataset.Tables(0).Rows(0).Item(1))
                                Catch ex As Exception
                                    forename = ""
                                End Try
                                agent_no2_str = Trim(bail_dataset.Tables(0).Rows(0).Item(0)) & _
                                                                     ", " & forename
                                If bail_dataset.Tables(0).Rows(0).Item(2) = "C" Then
                                    agent_no2_str = agent_no2_str & " (C)"
                                End If
                                agent_no2_str = Replace(agent_no2_str, ",", " ")
                            Else
                                agent_no2_str = ""
                            End If
                        End If

                    End If
                End If
                'see if case is on hold
                Dim hold_start_date_string = ""
                Dim hold_days As Integer = 0
                If displayfrm.not_hold_rb.Checked = False Then
                    param2 = " select hold_start_date from Complaints_holds " & _
                                       " where hold_comp_no = " & comp_no & _
                             " and hold_start_user > 0 and hold_end_user=0"

                    Dim hold_ds As DataSet = get_dataset("complaints", param2)
                    If no_of_rows > 0 Then
                        Dim hold_start_date As Date = hold_ds.Tables(0).Rows(0).Item(0)
                        hold_start_date_string = Format(hold_start_date, "dd/MM/yyyy")
                        hold_days = DateDiff("d", hold_start_date, Now)
                    End If
                End If
                Dim prof_indemnity_desc As String = ""
                Dim public_liability_desc As String = ""

                Try
                    prof_indemnity_no = row(55)
                Catch ex As Exception
                    prof_indemnity_no = 0
                End Try
                Try
                    public_liability_no = row(56)
                Catch ex As Exception
                    public_liability_no = 0
                End Try
                If prof_indemnity_no > 0 Then
                    prof_indemnity_desc = "Prof. Indemnity"
                End If
                If public_liability_no > 0 Then
                    public_liability_desc = "Public Liability"
                End If
                'see if referred to insurer
                Dim referred_to_insurer As Date = Nothing
                Dim insurance_type As Integer = -1
                Dim insurance_type_desc As String = ""
                Try
                    insurance_type = row(50)
                Catch ex As Exception

                End Try


                Dim referred_to_insurer_date_string As String = ""
                If insurance_type >= 0 Then
                    Try
                        referred_to_insurer = row(44)
                    Catch ex As Exception

                    End Try
                    If referred_to_insurer <> Nothing Then
                        If Format(referred_to_insurer, "yyyy-MM-dd") = Format(CDate("Jan 1 1800"), "yyyy-MM-dd") Then
                            referred_to_insurer = Nothing
                        Else
                            referred_to_insurer_date_string = Format(referred_to_insurer, "dd/MM/yyyy")
                        End If
                        Select Case insurance_type
                            Case 0
                                insurance_type_desc = "Unknown"
                            Case 1
                                insurance_type_desc = "Actual Notification"
                            Case 2
                                insurance_type_desc = "Bordereau"
                        End Select
                    End If
                End If
                Dim comp_type_code As Integer
                Try
                    comp_type_code = row(54)
                Catch ex As Exception
                    comp_type_code = 0
                End Try
                Dim comp_text As String
                Try
                    comp_text = row(13)
                    'take out carriage returns and commas
                    Dim idx3 As Integer
                    Dim test_text As String = comp_text
                    comp_text = ""
                    For idx3 = 1 To test_text.Length
                        If Mid(test_text, idx3, 1) = Chr(10) Or Mid(test_text, idx3, 1) = Chr(13) Or _
                        Mid(test_text, idx3, 1) = "," Then
                            comp_text = comp_text & " "
                        Else
                            comp_text = comp_text & Mid(test_text, idx3, 1)
                        End If
                    Next
                Catch ex As Exception
                    comp_text = ""
                End Try
                Dim response As String = ""
                Try
                    response = row(19)
                Catch ex As Exception

                End Try
                'take out carriage returns AND COMMAS
                Dim idx4 As Integer
                Dim resp_text As String = response
                response = ""
                For idx4 = 1 To resp_text.Length
                    If Mid(resp_text, idx4, 1) = Chr(10) Or Mid(resp_text, idx4, 1) = Chr(13) Or _
                    Mid(resp_text, idx4, 1) = "," Then
                        response = response & " "
                    Else
                        response = response & Mid(resp_text, idx4, 1)
                    End If
                Next
                Dim other_desc As String = ""
                If comp_type_code > 0 Then
                    param2 = "select type_name from complaints_type_code " & _
                    " where type_code = " & comp_type_code
                    Dim type_ds As DataSet = get_dataset("complaints", param2)
                    If no_of_rows = 1 Then
                        other_desc = type_ds.Tables(0).Rows(0).Item(0)
                    End If
                End If
                Try
                    If file2 = "" Then
                        file &= disp_comp_officer & "," & disp_comp_no & "," & comp_case_no & "," & _
                                                           client_ref & "," & recvd_date & "," & branch_name & "," & cat_str & "," & _
                                                           stage1_comp_date_string & "," & stage2_start_date_string & "," & stage2_comp_date_string & "," & _
                                                           stage3_start_date_string & "," & stage3_comp_date_string & "," & days_taken & "," _
                                                           & days_left & "," & client_name & "," & scheme_name & "," & type & "," & _
                                                            against & "," & agent & "," & against2 & "," & agent_no2_str & "," & category_code & "," & _
                                                            hold & "," & founded & "," & hold_start_date_string & "," & hold_days & "," & _
                                                            referred_to_insurer_date_string & "," & insurance_type_desc & "," & prof_indemnity_desc & "," & _
                                                            public_liability_desc & "," & comp_text & "," & response & "," & other_desc & vbNewLine
                    Else
                        file2 &= disp_comp_officer & "," & disp_comp_no & "," & comp_case_no & "," & _
                                                           client_ref & "," & recvd_date & "," & branch_name & "," & cat_str & "," & _
                                                           stage1_comp_date_string & "," & stage2_start_date_string & "," & stage2_comp_date_string & "," & _
                                                           stage3_start_date_string & "," & stage3_comp_date_string & "," & days_taken & "," _
                                                           & days_left & "," & client_name & "," & scheme_name & "," & type & "," & _
                                                            against & "," & agent & "," & against2 & "," & agent_no2_str & "," & category_code & "," & _
                                                            hold & "," & founded & "," & hold_start_date_string & "," & hold_days & "," & _
                                                            referred_to_insurer_date_string & "," & insurance_type_desc & "," & prof_indemnity_desc & "," & _
                                                           public_liability_desc & "," & comp_text & "," & response & "," & other_desc & vbNewLine
                    End If

                Catch ex As Exception
                    file2 = "Investigator,Complaint No,Case No,Client Ref,Recvd date,Branch,Category,Stage 1 completed,Stage 2 recvd," & _
                                "Stage 2 completed,Stage 3 recvd, Stage 3 completed,Days taken,Days left," & _
                                "client,Scheme, Complainant,against,agent,Complainant2,Against2,Category Code,hold letter," & _
                                "Founded,Start Hold date, days on Hold,Date Notified to Insurers,Complaint Details,Response,Other Information" & vbNewLine
                    file2 &= disp_comp_officer & "," & disp_comp_no & "," & comp_case_no & "," & _
                                   client_ref & "," & recvd_date & "," & branch_name & "," & cat_str & "," & _
                                   stage1_comp_date_string & "," & stage2_start_date_string & "," & stage2_comp_date_string & "," & _
                                   stage3_start_date_string & "," & stage3_comp_date_string & "," & days_taken & "," _
                                   & days_left & "," & client_name & "," & scheme_name & "," & type & "," & _
                                    against & "," & agent & "," & against2 & "," & agent_no2_str & "," & category_code & "," & _
                                    hold & "," & founded & "," & hold_start_date_string & "," & hold_days & "," & _
                                    referred_to_insurer_date_string & "," & insurance_type_desc & "," & prof_indemnity_desc & "," & _
                                   public_liability_desc & "," & comp_text & "," & response & "," & other_desc & vbNewLine
                End Try

            Next

        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try

    End Sub
    Private Sub displayfrm2_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            gtot_cases = 0
            Dim name As String = LCase(Trim(displayfrm.name_textbox.Text))
            name = LCase(Trim(name))
            If admin_user = False Then
                updbtn.Enabled = False
            End If
            'try new selection by building sql
            display_using_build()
            If gtot_cases = 0 Then
                MsgBox("No complaints found")
                Me.Close()
                Exit Sub
            Else
                If gtot_cases > 0 Then
                    Exit Sub
                End If
            End If
            'FOLLOWING CODE NO LONGER USED
            'End If
            Dim no_of_complaints As Integer = ComplaintsTableAdapter.ScalarQuery()
            Select Case search_no
                Case 1
                    ComplaintsTableAdapter.Fill(PraiseAndComplaintsSQLDataSet.Complaints, comp_start_no)
                Case 2
                    ComplaintsTableAdapter.Fill2(PraiseAndComplaintsSQLDataSet.Complaints, case_no)
                    held_search = "A"
                Case 3
                    ComplaintsTableAdapter.Fill3(PraiseAndComplaintsSQLDataSet.Complaints, cl_no)
                    held_search = "A"
                Case 4
                    ComplaintsTableAdapter.FillBy1(PraiseAndComplaintsSQLDataSet.Complaints, agent1, agent2)
                    held_search = "A"
                Case 5
                    'Me.ComplaintsTableAdapter.Fill5(Me.ComplaintsDataSet.Complaints, dept_code)
                Case 6
                    'ComplaintsTableAdapter.Fill6(PraiseAndComplaintsSQLDataSet.Complaints)
                    'held_search = "A"
                Case 7
                    ComplaintsTableAdapter.FillBy2(PraiseAndComplaintsSQLDataSet.Complaints, inv_code)
                    held_search = "A"
            End Select
            'remove any rows not required for completed, founded or letter
            Dim row As DataRow
            Dim idx As Integer = 0
            Dim idx2 As Integer = 0
            gtot_cases = 0
            comp_dg.Rows.Clear()
            Dim super_user_comp_no = 0
            If super_user Then
                If displayfrm.cmpny_cbox.SelectedItem = "ALL" Then
                    super_user_comp_no = 99
                Else
                    param2 = "select cmpny_code from Complaint_companies" & _
                    " where cmpny_name = '" & displayfrm.cmpny_cbox.SelectedItem & "'"
                    Dim cmpny_ds As DataSet = get_dataset("Complaints", param2)
                    super_user_comp_no = cmpny_ds.Tables(0).Rows(0).Item(0)
                End If
            End If
            file = "Investigator,Complaint No,Case No,Client Ref,Recvd date,Branch,Category,Stage 1 completed,Stage 2 recvd," & _
            "Stage 2 completed,Stage 3 recvd, Stage 3 completed,Days taken,Days left," & _
            "client,Complainant,against,agent,Complainant2,Against2,Category Code,hold letter," & _
            "Founded,Start Hold date, days on Hold,Date Notified to Insurers,Insurance Type,Prof Indemnity,Public Liability,Complaint Details,Response,Other Information" & vbNewLine
            Dim tot_rows As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows.Count
            Dim days_taken, client_name, type, agent, against, against2, agent_no2_str, category_code, hold As String
            Dim days_left As Decimal
            For Each row In PraiseAndComplaintsSQLDataSet.Complaints.Rows
                displayfrm.ProgressBar1.Value = (idx / tot_rows) * 100
                Application.DoEvents()
                Dim disp_Comp_no As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(0)
                Dim recvd_date As Date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1)
                Dim comp_case_no As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(4)
                Dim founded As String = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(18)
                Dim comp_completed_by As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(15)
                alloc_to_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(12)
                InvestigatorsTableAdapter.FillBy(PraiseAndComplaintsSQLDataSet.Investigators, alloc_to_code)
                Dim disp_comp_officer As String = PraiseAndComplaintsSQLDataSet.Investigators.Rows(0).Item(1)
                InvestigatorsTableAdapter.Fill(PraiseAndComplaintsSQLDataSet.Investigators)
                stage_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(21)
                Dim branch_code As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(49)
                Dim comp_cmpny_no As Integer
                Try
                    comp_cmpny_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(57)
                Catch ex As Exception
                    comp_cmpny_no = 0
                End Try
                If super_user Then
                    If super_user_comp_no <> 99 Then
                        If super_user_comp_no <> comp_cmpny_no Then
                            idx += 1
                            Continue For
                        End If
                    End If
                Else
                    If comp_cmpny_no <> cmpny_no Then
                        idx += 1
                        Continue For
                    End If
                End If

                Dim stage2_start_date As Date = Nothing
                Dim stage2_completed_by As Integer = 0
                Dim stage2_completed_date As Date = Nothing
                Dim stage3_start_date As Date = Nothing
                Dim stage3_completed_by As Integer = 0
                Dim stage3_completed_date As Date = Nothing
                Select Case stage_no
                    Case 2
                        Try
                            stage2_start_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(29)
                        Catch ex As Exception
                            stage2_start_date = Nothing
                        End Try
                        Try
                            stage2_completed_by = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(31)
                        Catch ex As Exception
                            stage2_completed_by = 0
                        End Try
                        Try
                            stage2_completed_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(30)
                        Catch ex As Exception
                            stage2_completed_date = Nothing
                        End Try
                    Case 3
                        Try
                            stage2_start_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(29)
                        Catch ex As Exception
                            stage2_start_date = Nothing
                        End Try
                        Try
                            stage2_completed_by = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(31)
                        Catch ex As Exception
                            stage2_completed_by = 0
                        End Try
                        Try
                            stage2_completed_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(30)
                        Catch ex As Exception
                            stage2_completed_date = Nothing
                        End Try
                        Try
                            stage3_start_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(38)
                        Catch ex As Exception
                            stage3_start_date = Nothing
                        End Try
                        Try
                            stage3_completed_by = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(39)
                        Catch ex As Exception
                            stage3_completed_by = 0
                        End Try
                        Try
                            stage3_completed_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(40)
                        Catch ex As Exception
                            stage3_completed_date = Nothing
                        End Try

                End Select
                Try
                    legal_flag = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(46)
                Catch ex As Exception
                    legal_flag = "N"
                End Try

                Try
                    branch_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(49)
                Catch ex As Exception
                    branch_no = 0
                End Try
                If search_branch_no >= 0 Then
                    If search_branch_no <> branch_no Then
                        idx += 1
                        Continue For
                    End If
                End If
                'get branch name
                add_compfrm.Complaint_branchesTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Complaint_branches, branch_no)
                Dim branch_name As String = Me.PraiseAndComplaintsSQLDataSet.Complaint_branches.Rows(0).Item(1)
                'ignore any stage 0 complaints
                If stage_no = 0 Then
                    idx += 1
                    Continue For
                End If
                'founded/unfounded
                If founded_search <> "A" Then
                    If founded <> founded_search Then
                        idx += 1
                        Continue For
                    End If
                End If
                If founded = "U" Then founded = " "

                'category text
                If cat_text <> "" Then
                    If cat_text <> PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(8) Then
                        idx += 1
                        Continue For
                    End If
                End If

                'received from
                If search_recvd_from >= 0 Then
                    If search_recvd_from + 1 <> PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(3) Then
                        idx += 1
                        Continue For
                    End If
                End If

                'received against
                against = ""
                against_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(6)
                Complaint_againstTableAdapter.FillBy(PraiseAndComplaintsSQLDataSet.Complaint_against, against_code)
                against = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(0).Item(1)


                'received against2
                against2 = ""
                Dim against_code2 As Integer
                Try
                    against_code2 = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(41)
                Catch ex As Exception
                    against_code2 = 0
                End Try
                If against_code2 > 0 Then
                    Complaint_againstTableAdapter.FillBy(PraiseAndComplaintsSQLDataSet.Complaint_against, against_code2)
                    against2 = PraiseAndComplaintsSQLDataSet.Complaint_against.Rows(0).Item(1)
                End If

                If search_dept.Length > 0 Then
                    If search_dept <> against And search_dept <> against2 Then
                        idx += 1
                        Continue For
                    End If
                End If
                'receipt type
                If search_receipt_type.Length > 0 Then
                    Dim receipt_type As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(2)
                    displayfrm3.Receipt_typeTableAdapter.FillBy(displayfrm3.PraiseAndComplaintsSQLDataSet.Receipt_type, receipt_type)
                    Dim receipt_type_text As String = displayfrm3.PraiseAndComplaintsSQLDataSet.Receipt_type.Rows(0).Item(1)
                    If search_receipt_type <> receipt_type_text Then
                        idx += 1
                        Continue For
                    End If
                End If
                'get agent
                agent = ""
                Dim agent_no2 As Integer
                Try
                    agent_no2 = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(42)
                Catch ex As Exception
                    agent_no2 = 0
                End Try
                'agent_no1 and agent_no2 are numbers from complaint
                'agent1 and agent 2 are search agents
                agent_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(7)
                If agent1 > 0 And agent1 <> agent_no And agent1 <> agent_no2 Then
                    If agent2 > 0 Then
                        If agent2 <> agent_no And agent2 <> agent_no2 Then
                            idx += 1
                            Continue For
                        End If
                    Else
                        idx += 1
                        Continue For
                    End If
                End If
                'see if referred to insurer
                Dim referred_to_insurer As Date = Nothing
                Dim insurance_type As Integer = -1
                Dim insurance_type_desc As String = ""
                Try
                    insurance_type = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(50)
                Catch ex As Exception

                End Try


                Dim referred_to_insurer_date_string As String = ""
                If insurance_type >= 0 Then
                    Try
                        referred_to_insurer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(44)
                    Catch ex As Exception

                    End Try
                    If referred_to_insurer <> Nothing Then
                        If Format(referred_to_insurer, "yyyy-MM-dd") = Format(CDate("Jan 1 1800"), "yyyy-MM-dd") Then
                            referred_to_insurer = Nothing
                        Else
                            referred_to_insurer_date_string = Format(referred_to_insurer, "dd/MM/yyyy")
                            Select Case insurance_type
                                Case 0
                                    insurance_type_desc = "Unknown"
                                Case 1
                                    insurance_type_desc = "Actual Notification"
                                Case 2
                                    insurance_type_desc = "Bordereau"
                            End Select
                        End If
                    End If
                End If

                'check stage
                If search_stage_text <> "" Then
                    If search_stage_text = "Legal" Then
                        If legal_flag = "N" Then
                            idx += 1
                            Continue For
                        End If
                    ElseIf search_stage_text = "Insurance" Then
                        If insurance_type_desc = "" Then
                            idx += 1
                            Continue For
                        End If
                    Else
                        If stage_no = 1 And (search_stage_text = "Stage 2" Or search_stage_text = "Stage 3") _
                        Or (stage_no = 2 And search_stage_text = "Stage 3") Then
                            idx += 1
                            Continue For
                        End If
                    End If
                End If
                'completed/not completed stage
                If completed_search <> "A" Then
                    'need to check stage completed if a stage is selected
                    If search_stage_text = "" Then
                        If completed_search = "Y" Then
                            Select Case stage_no
                                Case 1
                                    If comp_completed_by = 0 Then
                                        idx += 1
                                        Continue For
                                    End If
                                Case 2
                                    If comp_completed_by = 0 Or stage2_completed_by = 0 Then
                                        idx += 1
                                        Continue For
                                    End If
                                Case 3
                                    If comp_completed_by = 0 Or stage2_completed_by = 0 Or stage3_completed_by = 0 Then
                                        idx += 1
                                        Continue For
                                    End If
                            End Select
                        End If
                        If completed_search = "N" Then
                            Select Case stage_no
                                Case 1
                                    If comp_completed_by > 0 Then
                                        idx += 1
                                        Continue For
                                    End If
                                Case 2
                                    If comp_completed_by > 0 And stage2_completed_by > 0 Then
                                        idx += 1
                                        Continue For
                                    End If
                                Case 3
                                    If comp_completed_by > 0 And stage2_completed_by > 0 And stage3_completed_by > 0 Then
                                        idx += 1
                                        Continue For
                                    End If
                            End Select
                        End If
                    Else
                        Select Case search_stage_text
                            Case "Stage 1"
                                If (completed_search = "Y" And comp_completed_by = 0) Or _
                                  (completed_search = "N" And comp_completed_by > 0) Then
                                    idx += 1
                                    Continue For
                                End If
                            Case "Stage 2"
                                If (completed_search = "Y" And stage2_completed_by = 0) Or _
                                  (completed_search = "N" And stage2_completed_by > 0) Then
                                    idx += 1
                                    Continue For
                                End If
                            Case "Stage 3"
                                If (completed_search = "Y" And stage3_completed_by = 0) Or _
                                  (completed_search = "N" And stage3_completed_by > 0) Then
                                    idx += 1
                                    Continue For
                                End If
                        End Select

                    End If
                End If
                'if stage 2 need to check all/open/closed
                'now stage 2 same as other stages
                'If stage_no = 2 Then
                '    If search_stage_text <> "Stage 2 all" Then
                '        Dim stage2_closed As Boolean = False
                '        stage2_closed = True
                '        Try
                '            Dim stage2_completed_date As Date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(31)
                '        Catch ex As Exception
                '            stage2_closed = False
                '        End Try
                '        Select Case search_stage_text
                '            Case "Stage 2 open"
                '                If stage2_closed = True Then
                '                    idx += 1
                '                    Continue For
                '                End If
                '            Case "Stage 2 closed"
                '                If stage2_closed = False Then
                '                    idx += 1
                '                    Continue For
                '                End If
                '        End Select
                '    End If
                'End If
                If get_letter Then
                    hold = check_held_letter(comp_case_no, recvd_date)
                Else
                    hold = "?"
                End If
                'if name entered check name matches
                If name.Length > 0 Then
                    If comp_case_no > 0 Then
                        param2 = "select name_sur from Debtor " & _
                        " where _rowid = " & comp_case_no
                        Dim name_dataset As DataSet = get_dataset(param1, param2)
                        If no_of_rows = 0 Then
                            idx += 1
                            Continue For
                        End If
                        Dim name_found As String = Trim(LCase(name_dataset.Tables(0).Rows(0).Item(0)))
                        If name_found.Length > name.Length Then
                            If Microsoft.VisualBasic.Left(name_found, name.Length) <> name Then
                                idx += 1
                                Continue For
                            End If
                        Else
                            If Microsoft.VisualBasic.Left(name, name_found.Length) <> name_found Then
                                idx += 1
                                Continue For
                            End If
                        End If
                    End If
                End If
                'if scheme entered need to check if case number is on complaint
                If search_csid_no > 0 And comp_case_no > 0 Then
                    param2 = "select _rowid from Debtor where _rowid = " & comp_case_no & _
                    " and clientschemeID = " & search_csid_no
                    Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                    If no_of_rows = 0 Then
                        idx += 1
                        Continue For
                    End If
                End If
                'if debt type entered check it
                If displayfrm.debt_typecombobox.SelectedIndex > 0 Then
                    If comp_case_no = 0 Then
                        idx += 1
                        Continue For
                    End If
                    param2 = "select clientschemeID from Debtor where _rowid = " & comp_case_no
                    Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                    If no_of_rows = 1 Then
                        Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(0)
                        param2 = "select schemeID from ClientScheme where _rowid =  " & csid
                        Dim cs_dataset As DataSet = get_dataset(param1, param2)
                        If no_of_rows = 1 Then
                            Dim sch_id As Integer = cs_dataset.Tables(0).Rows(0).Item(0)
                            param2 = "select work_type from Scheme where _rowid = " & sch_id
                            Dim sch_dataset As DataSet = get_dataset(param1, param2)
                            If no_of_rows = 1 Then
                                Dim work_type As Integer = sch_dataset.Tables(0).Rows(0).Item(0)
                                Select Case displayfrm.debt_typecombobox.SelectedIndex
                                    Case 1 'CTAX
                                        If work_type <> 2 Then
                                            idx += 1
                                            Continue For
                                        End If
                                    Case 2 'NNDR
                                        If work_type <> 3 Then
                                            idx += 1
                                            Continue For
                                        End If
                                    Case 3 'RTD
                                        If work_type <> 16 And work_type <> 20 Then
                                            idx += 1
                                            Continue For
                                        End If
                                End Select
                            End If

                        End If
                    End If
                End If
                'if recvd date entered just select complaints received in that month
                'need to select all stages received in that month
                If displayfrm.recvd_cbox.Checked Then
                    Dim compare_date As Date
                    Dim include_comp As Boolean = False
                    Select Case search_stage_text
                        Case ""
                            compare_date = recvd_date
                            If Month(compare_date) = Month(displayfrm.start_recvd_datepicker.Value) _
                                                And Year(compare_date) = Year(displayfrm.start_recvd_datepicker.Value) Then
                                include_comp = True
                                Exit Select
                            End If
                            Try
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(29)
                            Catch ex As Exception
                                Exit Select
                            End Try
                            If Month(compare_date) = Month(displayfrm.start_recvd_datepicker.Value) _
                                                And Year(compare_date) = Year(displayfrm.start_recvd_datepicker.Value) Then
                                include_comp = True
                                Exit Select
                            End If
                            Try
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(38)
                            Catch ex As Exception
                                Exit Select
                            End Try
                            If Month(compare_date) = Month(displayfrm.start_recvd_datepicker.Value) _
                                                And Year(compare_date) = Year(displayfrm.start_recvd_datepicker.Value) Then
                                include_comp = True
                            End If
                        Case "Stage 1"
                            compare_date = recvd_date
                            If Month(compare_date) = Month(displayfrm.start_recvd_datepicker.Value) _
                                                And Year(compare_date) = Year(displayfrm.start_recvd_datepicker.Value) Then
                                include_comp = True
                            End If
                        Case "Stage 2"
                            compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(29)
                            If Month(compare_date) = Month(displayfrm.start_recvd_datepicker.Value) _
                                                And Year(compare_date) = Year(displayfrm.start_recvd_datepicker.Value) Then
                                include_comp = True
                            End If
                        Case "Stage 3"
                            compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(38)
                            If Month(compare_date) = Month(displayfrm.start_recvd_datepicker.Value) _
                            And Year(compare_date) = Year(displayfrm.start_recvd_datepicker.Value) Then
                                include_comp = True
                            End If
                    End Select
                    If include_comp = False Then
                        idx += 1
                        Continue For
                    End If
                End If
                'if comp date entered just select complaints resolved in that month
                If displayfrm.comp_cbox.Checked Then
                    Dim compare_date As Date = Nothing
                    Dim include_comp As Boolean = False
                    Select Case search_stage_text
                        Case ""
                            If comp_completed_by > 0 Then
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(14)
                                If Month(compare_date) = Month(displayfrm.comp_datepicker.Value) _
                                                  And Year(compare_date) = Year(displayfrm.comp_datepicker.Value) Then
                                    include_comp = True
                                    Exit Select
                                End If
                            End If
                            If stage2_completed_by > 0 Then
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(30)
                                If Month(compare_date) = Month(displayfrm.comp_datepicker.Value) _
                                          And Year(compare_date) = Year(displayfrm.comp_datepicker.Value) Then
                                    include_comp = True
                                    Exit Select
                                End If
                            End If
                            If stage3_completed_by > 0 Then
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(40)
                                If Month(compare_date) = Month(displayfrm.comp_datepicker.Value) _
                                                                          And Year(compare_date) = Year(displayfrm.comp_datepicker.Value) Then
                                    include_comp = True
                                End If
                            End If
                        Case "Stage 1"
                            If comp_completed_by > 0 Then
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(14)
                                If Month(compare_date) = Month(displayfrm.comp_datepicker.Value) _
                                                  And Year(compare_date) = Year(displayfrm.comp_datepicker.Value) Then
                                    include_comp = True
                                End If
                            End If
                        Case "Stage 2"
                            If stage2_completed_by > 0 Then
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(30)
                                If Month(compare_date) = Month(displayfrm.comp_datepicker.Value) _
                                          And Year(compare_date) = Year(displayfrm.comp_datepicker.Value) Then
                                    include_comp = True
                                End If
                            End If
                        Case "Stage 3"
                            If stage3_completed_by > 0 Then
                                compare_date = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(40)
                                If Month(compare_date) = Month(displayfrm.comp_datepicker.Value) _
                                                                          And Year(compare_date) = Year(displayfrm.comp_datepicker.Value) Then
                                    include_comp = True
                                End If
                            End If
                    End Select
                    If include_comp = False Then
                        idx += 1
                        Continue For
                    End If
                End If
                'If held_search <> "A" Then
                '    If hold <> held_search Then
                '        idx += 1
                '        Continue For
                '    End If
                'End If
                'If search_no = 6 Then
                '    'only include complaints 6+ days old with no hold letter
                '    'or 17+ days old with held letter
                '    If hold = "N" And DateDiff("d", recvd_date, Now) < 6 Then
                '        idx += 1
                '        Continue For
                '    End If
                '    If hold = "Y" And DateDiff("d", recvd_date, Now) < 17 Then
                '        idx += 1
                '        Continue For
                '    End If
                'End I
                'category
                Dim category_no As Integer
                Try
                    category_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(48)
                Catch ex As Exception
                    category_no = 0
                End Try
                If displayfrm.category_combobox.SelectedIndex > 0 Then
                    If displayfrm.category_combobox.SelectedIndex <> category_no Then
                        idx += 1
                        Continue For
                    End If
                End If
                'see if holds radio buttons pressed
                updatecmpfrm.Complaints_HoldsTableAdapter.FillBy2(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds, disp_Comp_no)
                If displayfrm.all_holds_rb.Checked = False Then
                    If updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows.Count > 0 Then
                        If displayfrm.not_hold_rb.Checked Then
                            idx += 1
                            Continue For
                        End If
                    Else
                        If displayfrm.hold_rb.Checked Then
                            idx += 1
                            Continue For
                        End If
                    End If
                End If
                Dim cat_str As String = "?"
                Select Case category_no
                    Case 0
                        cat_str = "?"
                    Case 1
                        cat_str = "F"
                    Case 2
                        cat_str = "N"
                    Case 3
                        cat_str = "S"
                End Select

                Dim stage1_comp_date_string As String = ""
                If comp_completed_by > 0 Then
                    stage1_comp_date_string = Format(PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(14), "yyyy-MM-dd")
                    days_taken = DateDiff("d", PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1), _
                         PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(14)) - _
                    (DateDiff("w", PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1), _
                         PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(14)) * 2)
                    If Weekday(PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1)) > _
                        Weekday(PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(14)) Then
                        days_taken = days_taken - 2
                    End If
                    If days_taken = 0 Then days_taken = 1
                Else
                    days_taken = DateDiff("d", PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1), Now) - _
                    (DateDiff("w", PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1), Now) * 2)
                    If Weekday(PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(1)) > Weekday(Now) Then
                        days_taken = days_taken - 2
                    End If
                End If
                'get client name
                client_name = ""
                Dim cl_turn_days As Integer = 10
                If PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(9) = 101 Then
                    cl_turn_days = 40
                ElseIf PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(5) > 0 Then
                    Dim cl_no As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(5)
                    If comp_cmpny_no = 0 Then
                        param1 = "onestep"
                        param2 = "select name from Client" & _
                        " where _rowid = " & cl_no
                        Dim cl_dataset As DataSet = get_dataset(param1, param2)
                        If no_of_rows = 1 Then
                            client_name = cl_dataset.Tables(0).Rows(0).Item(0)
                        Else
                            client_name = ""
                        End If
                    End If

                    If client_turnfrm.Client_turnaroundTableAdapter.FillBy(Me.PraiseAndComplaintsSQLDataSet.Client_turnaround, cl_no) > 0 Then
                        cl_turn_days = Me.PraiseAndComplaintsSQLDataSet.Client_turnaround.Rows(0).Item(1)
                    Else
                        'check if branch is LSC (branch_code=3)
                        If branch_code = 3 Then
                            cl_turn_days = 3
                        End If
                    End If
                End If
                'days left
                If comp_completed_by > 0 Or legal_flag = "Y" Then
                    days_left = Nothing
                Else
                    days_left = cl_turn_days - days_taken
                End If
                'get type
                type = ""
                'received from
                Dim recvd_from2 As Integer = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(3)
                Received_fromTableAdapter.FillBy(PraiseAndComplaintsSQLDataSet.Received_from, recvd_from2)
                type = PraiseAndComplaintsSQLDataSet.Received_from.Rows(0).Item(1)

                'get category code
                category_code = ""
                category_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(8) & " " _
                           & PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(9)


                If agent_no = 9999 Then
                    agent = "Various"
                Else
                    If agent_no > 0 Then
                        If comp_cmpny_no = 0 Then
                            param1 = "onestep"
                            param2 = "select name_sur, name_fore, status from Bailiff" & _
                            " where _rowid = " & agent_no
                            Dim bail_dataset As DataSet = get_dataset(param1, param2)
                            If no_of_rows = 1 Then
                                Dim forename As String
                                Try
                                    forename = Trim(bail_dataset.Tables(0).Rows(0).Item(1))
                                Catch ex As Exception
                                    forename = ""
                                End Try
                                agent = Trim(bail_dataset.Tables(0).Rows(0).Item(0)) & _
                                                                     ", " & forename
                                If bail_dataset.Tables(0).Rows(0).Item(2) = "C" Then
                                    agent = agent & " (C)"
                                End If
                                agent = Replace(agent, ",", " ")
                            Else
                                agent = ""
                            End If
                        End If

                    Else
                        dept_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(10)
                        'If dept_code > 0 Then
                        '    updatecmpfrm.DepartmentsTableAdapter.FillBy(updatecmpfrm.DepartmentsDataSet.Departments, dept_code)
                        '    agent = updatecmpfrm.DepartmentsDataSet.Tables(0).Rows(0).Item(1)
                        'End If
                    End If
                End If

                agent_no2_str = ""
                If agent_no2 = 9999 Then
                    agent_no2_str = "Various"
                Else
                    If agent_no2 > 0 Then
                        If comp_cmpny_no = 0 Then
                            param1 = "onestep"
                            param2 = "select name_sur, name_fore, status from Bailiff" & _
                            " where _rowid = " & agent_no2
                            Dim bail_dataset As DataSet = get_dataset(param1, param2)
                            If no_of_rows = 1 Then
                                Dim forename As String
                                Try
                                    forename = Trim(bail_dataset.Tables(0).Rows(0).Item(1))
                                Catch ex As Exception
                                    forename = ""
                                End Try
                                agent_no2_str = Trim(bail_dataset.Tables(0).Rows(0).Item(0)) & _
                                                                     ", " & forename
                                If bail_dataset.Tables(0).Rows(0).Item(2) = "C" Then
                                    agent_no2_str = agent_no2_str & " (C)"
                                End If
                                agent_no2_str = Replace(agent_no2_str, ",", " ")
                            Else
                                agent_no2_str = ""
                            End If
                        End If

                    End If
                End If


                Dim comp_type_code As Integer
                Try
                    comp_type_code = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(54)
                Catch ex As Exception
                    comp_type_code = 0
                End Try
                Dim comp_text As String
                Try
                    comp_text = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(13)
                    'take out carriage returns and commas
                    Dim idx3 As Integer
                    Dim test_text As String = comp_text
                    comp_text = ""
                    For idx3 = 1 To test_text.Length
                        If Mid(test_text, idx3, 1) = Chr(10) Or Mid(test_text, idx3, 1) = Chr(13) Or _
                        Mid(test_text, idx3, 1) = "," Then
                            comp_text = comp_text & " "
                        Else
                            comp_text = comp_text & Mid(test_text, idx3, 1)
                        End If
                    Next
                Catch ex As Exception
                    comp_text = ""
                End Try
                Dim response As String = ""
                Try
                    response = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(19)
                Catch ex As Exception

                End Try
                'take out carriage returns AND COMMAS
                Dim idx4 As Integer
                Dim resp_text As String = response
                response = ""
                For idx4 = 1 To resp_text.Length
                    If Mid(resp_text, idx4, 1) = Chr(10) Or Mid(resp_text, idx4, 1) = Chr(13) Or _
                    Mid(resp_text, idx4, 1) = "," Then
                        response = response & " "
                    Else
                        response = response & Mid(resp_text, idx4, 1)
                    End If
                Next
                Dim other_desc As String = ""
                If comp_type_code > 0 Then
                    updatecmpfrm.Complaints_type_codeTableAdapter.FillBy(updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_type_code, comp_type_code)
                    other_desc = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_type_code.Rows(0).Item(1)
                End If

                idx += 1
                Dim stage2_start_date_string As String = ""
                Dim stage2_comp_date_string As String = ""
                Dim stage3_start_date_string As String = ""
                Dim stage3_comp_date_string As String = ""
                If stage_no = 2 Then
                    stage2_start_date_string = Format(stage2_start_date, "yyyy-MM-dd")
                    If stage2_completed_date <> Nothing Then
                        stage2_comp_date_string = Format(stage2_completed_date, "yyyy-MM-dd")
                    End If
                End If
                If stage_no = 3 Then
                    stage2_start_date_string = Format(stage2_start_date, "yyyy-MM-dd")
                    If stage2_completed_date <> Nothing Then
                        stage2_comp_date_string = Format(stage2_completed_date, "yyyy-MM-dd")
                    End If
                    stage3_start_date_string = Format(stage3_start_date, "yyyy-MM-dd")
                    If stage3_completed_date <> Nothing Then
                        stage3_comp_date_string = Format(stage3_completed_date, "yyyy-MM-dd")
                    End If
                End If
                'get cl_ref
                Dim client_ref As String = ""
                If comp_cmpny_no = 0 Then
                    param2 = "select client_ref from Debtor where _rowid = " & comp_case_no
                    Dim debt_dataset As DataSet = get_dataset("onestep", param2)

                    If no_of_rows = 0 Then
                        client_ref = ""
                    Else
                        client_ref = debt_dataset.Tables(0).Rows(0).Item(0)
                    End If
                End If

                gtot_cases += 1
                comp_dg.Rows.Add(disp_comp_officer, disp_Comp_no, comp_case_no, branch_name, _
                cat_str, legal_flag, Format(recvd_date, "yyyy-MM-dd"), _
                stage1_comp_date_string, stage2_start_date_string, stage2_comp_date_string, _
                stage3_start_date_string, stage3_comp_date_string, days_taken, days_left, _
                client_name, type, against, agent, category_code, hold, founded)

                'see if case is on hold
                Dim hold_start_date_string = ""
                Dim hold_days As Integer = 0
                If updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows.Count > 0 Then
                    Dim hold_start_date As Date = updatecmpfrm.PraiseAndComplaintsSQLDataSet.Complaints_Holds.Rows(0).Item(2)
                    hold_start_date_string = Format(hold_start_date, "dd/MM/yyyy")
                    hold_days = DateDiff("d", hold_start_date, Now)
                End If
                Dim prof_indemnity_desc As String = ""
                Dim public_liability_desc As String = ""

                Try
                    prof_indemnity_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(55)
                Catch ex As Exception
                    prof_indemnity_no = 0
                End Try
                Try
                    public_liability_no = PraiseAndComplaintsSQLDataSet.Complaints.Rows(idx).Item(56)
                Catch ex As Exception
                    public_liability_no = 0
                End Try
                If prof_indemnity_no > 0 Then
                    prof_indemnity_desc = "Prof. Indemnity"
                End If
                If public_liability_no > 0 Then
                    public_liability_desc = "Public Liability"
                End If
                Try
                    If file2 = "" Then
                        file &= disp_comp_officer & "," & disp_Comp_no & "," & comp_case_no & "," & _
                                                           client_ref & "," & recvd_date & "," & branch_name & "," & cat_str & "," & _
                                                           stage1_comp_date_string & "," & stage2_start_date_string & "," & stage2_comp_date_string & "," & _
                                                           stage3_start_date_string & "," & stage3_comp_date_string & "," & days_taken & "," _
                                                           & days_left & "," & client_name & "," & type & "," & _
                                                            against & "," & agent & "," & against2 & "," & agent_no2_str & "," & category_code & "," & _
                                                            hold & "," & founded & "," & hold_start_date_string & "," & hold_days & "," & _
                                                            referred_to_insurer_date_string & "," & insurance_type_desc & "," & prof_indemnity_desc & "," & _
                                                            public_liability_desc & "," & comp_text & "," & response & "," & other_desc & vbNewLine
                    Else
                        file2 &= disp_comp_officer & "," & disp_Comp_no & "," & comp_case_no & "," & _
                                                           client_ref & "," & recvd_date & "," & branch_name & "," & cat_str & "," & _
                                                           stage1_comp_date_string & "," & stage2_start_date_string & "," & stage2_comp_date_string & "," & _
                                                           stage3_start_date_string & "," & stage3_comp_date_string & "," & days_taken & "," _
                                                           & days_left & "," & client_name & "," & type & "," & _
                                                            against & "," & agent & "," & against2 & "," & agent_no2_str & "," & category_code & "," & _
                                                            hold & "," & founded & "," & hold_start_date_string & "," & hold_days & "," & _
                                                            referred_to_insurer_date_string & "," & insurance_type_desc & "," & prof_indemnity_desc & "," & _
                                                           public_liability_desc & "," & comp_text & "," & response & "," & other_desc & vbNewLine
                    End If

                Catch ex As Exception
                    file2 = "Investigator,Complaint No,Case No,Client Ref,Recvd date,Branch,Category,Stage 1 completed,Stage 2 recvd," & _
                                "Stage 2 completed,Stage 3 recvd, Stage 3 completed,Days taken,Days left," & _
                                "client,Complainant,against,agent,Complainant2,Against2,Category Code,hold letter," & _
                                "Founded,Start Hold date, days on Hold,Date Notified to Insurers,Complaint Details,Response,Other Information" & vbNewLine
                    file2 &= disp_comp_officer & "," & disp_Comp_no & "," & comp_case_no & "," & _
                                   client_ref & "," & recvd_date & "," & branch_name & "," & cat_str & "," & _
                                   stage1_comp_date_string & "," & stage2_start_date_string & "," & stage2_comp_date_string & "," & _
                                   stage3_start_date_string & "," & stage3_comp_date_string & "," & days_taken & "," _
                                   & days_left & "," & client_name & "," & type & "," & _
                                    against & "," & agent & "," & against2 & "," & agent_no2_str & "," & category_code & "," & _
                                    hold & "," & founded & "," & hold_start_date_string & "," & hold_days & "," & _
                                    referred_to_insurer_date_string & "," & insurance_type_desc & "," & prof_indemnity_desc & "," & _
                                   public_liability_desc & "," & comp_text & "," & response & "," & other_desc & vbNewLine
                End Try

                If search_no = 6 Then
                    Try
                        mainfrm.ProgressBar1.Value = (idx / no_of_complaints) * 100
                    Catch ex As Exception

                    End Try
                Else
                    Try
                        displayfrm.ProgressBar1.Value = (idx / no_of_complaints) * 100
                    Catch ex As Exception

                    End Try
                End If
            Next
            Me.Text = "Display Complaints List - " & gtot_cases & " cases"
            comp_dg.Sort(comp_dg.Columns("days_left"), System.ComponentModel.ListSortDirection.Ascending)
            If comp_dg.RowCount = 0 Then
                If search_no = 4 Then
                    MsgBox("There are no complaints for " & search_agent_name)
                Else
                    MsgBox("There are no complaints for selected criteria")
                End If

                Me.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            exception_string = ex.ToString
            Call error_log()
        End Try
    End Sub

    Private Sub DataGridView1_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles comp_dg.CellEnter
        selected_comp_no = comp_dg.Rows(e.RowIndex).Cells(1).Value
    End Sub

    Private Sub showbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dispbtn.Click
        comp_no = selected_comp_no
        displayfrm3.ShowDialog()
    End Sub


    Private Sub updbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updbtn.Click
        comp_no = selected_comp_no
        updatecmpfrm.ShowDialog()
    End Sub

    Private Sub ComplaintsBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.ComplaintsBindingSource.EndEdit()
        Me.ComplaintsTableAdapter.Update(Me.PraiseAndComplaintsSQLDataSet.Complaints)

    End Sub

    Private Sub excelbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles excelbtn.Click
        With SaveFileDialog1
            .Title = "Save to xls"
            .Filter = "excel|*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "complaints.xls"
        End With
        If Len(file) = 0 Then
            MessageBox.Show("There are no cases selected")
        Else
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False)
                If file2 <> "" Then
                    Dim InputFilePath As String = Path.GetDirectoryName(SaveFileDialog1.FileName)
                    InputFilePath &= "\"
                    My.Computer.FileSystem.WriteAllText(InputFilePath & "complaints2.xls", file2, False)
                End If
            End If
        End If
    End Sub
End Class
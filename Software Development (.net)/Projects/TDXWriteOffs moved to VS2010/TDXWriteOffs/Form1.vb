Public Class Form1

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim listID As Integer
        Try
            listID = list_tbox.Text
        Catch ex As Exception
            MsgBox("Invalid List ID")
            Exit Sub
        End Try
        Dim debtorID As Integer
        Try
            debtorID = case_tbox.Text
        Catch ex As Exception
            MsgBox("Example Case number is invalid")
            Exit Sub
        End Try
        'check list is on onestep
        param2 = "select objectRowId from ListDetail where listID = " & listID
        Dim list_ds As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("List " & listID & " does not exist on onestep")
            Me.Close()
            Exit Sub
        End If

        Dim listIDX As Integer
        Dim listRows As Integer = no_of_rows - 1
        Dim caseFound As Boolean = False

        'check all cases have status C, S or X and that example case exists in the list
        For listIDX = 0 To listRows
            Try
                ProgressBar.Value = (listIDX / listRows) * 100
            Catch ex As Exception

            End Try

            Application.DoEvents()
            Dim listDebtorID As Integer = list_ds.Tables(0).Rows(listIDX).Item(0)
            If listDebtorID = debtorID Then
                caseFound = True
            End If
            param2 = "select status from Debtor where _rowid = " & listDebtorID
            Dim list2_ds As DataSet = get_dataset("onestep", param2)
            If list2_ds.Tables(0).Rows(0).Item(0) <> "C" And _
                list2_ds.Tables(0).Rows(0).Item(0) <> "S" And _
                list2_ds.Tables(0).Rows(0).Item(0) <> "X" Then
                MsgBox("Case No " & listDebtorID & " status is not C, S or X")
                Exit Sub
            End If
        Next

        If Not caseFound Then
            MsgBox("Example case No is NOT in the list!")
            Exit Sub
        End If
        ProgressBar.Value = 50
        'check list has not already been run
        param2 = "select tdx_listId from TDXWriteOffs where tdx_listID = " & listID
        Dim tdx_ds As DataSet = get_dataset("Fees", param2)
        If no_of_rows > 0 Then
            If MsgBox("This list has already been run - do again?", MsgBoxStyle.YesNo, "List already run") = MsgBoxResult.No Then
                MsgBox("Report not produced")
                Me.Close()
                Exit Sub
            End If
        Else
            'save details in fees table
            Dim user As String = My.User.Name
            For listIDX = 0 To listRows
                Dim listDebtorID As Integer = list_ds.Tables(0).Rows(listIDX).Item(0)
                upd_txt = "insert into TDXWriteOffs (tdx_listID,tdx_debtorID,tdx_date,tdx_user) " & _
                        " values(" & listID & "," & listDebtorID & ",'" & Format(Now, "dd MMM yyyy") & "','" & user & "')"
                update_sql(upd_txt)
            Next
        End If
        crystal_lbl.Visible = True
        Application.DoEvents()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD229Lreport = New RD229L
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(listID)
        SetCurrentValuesForParameterField1(RD229Lreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD229Lreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RD229L TDX Write off file.xls"
        End With
        Dim filename As String
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = SaveFileDialog1.FileName
            Application.DoEvents()
            RD229Lreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, filename)
            RD229Lreport.close()
            MsgBox("Completed")

        End If
        Me.Close()
    End Sub

End Class

Module data_access_module
    Public table_array(,)
    Public no_of_rows, last_rowid, ret_code As Integer
    Public conn As New Odbc.OdbcConnection()
    Public conn2 As New OleDb.OleDbConnection
    Public param1, param2, param3, param4, param5 As String
   
    Function get_table(ByVal database_name As String, ByVal table_name As String, Optional ByVal columns As String = Nothing, _
                Optional ByVal where_clause As String = Nothing, Optional ByVal order_clause As String = Nothing, _
                Optional ByVal pigeon_hole As Boolean = False) As Integer
        Dim dataset As New DataSet
        Dim idx2 As Integer
        Dim error_found As Boolean = True
        Dim select_string As String = "SELECT * from " & table_name & " " & where_clause & " " & order_clause
        If database_name = "onestep" Then
            conn.ConnectionString = ""
            If Left(columns, 8) <> "01_rowid" And Len(columns) > 0 Then
                MessageBox.Show("Where clause must start 01_rowid")
                Return 1
            End If
            conn.ConnectionString = _
               "Integrated Security=True;Dsn=DebtRecovery - DDSybase;na=192.168.19.26,14100;db=DebtRecovery;uid=thirdpartyB;password=thirdpartyB;"
            conn.Open()
            Dim adapter As New Odbc.OdbcDataAdapter(select_string, conn)
            adapter.Fill(dataset)
            conn.Close()
        Else
            conn2.ConnectionString = ""
            If database_name = "Fees" Then
                conn2.ConnectionString = _
                "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=FeesSQL;Data Source=ROSSVR01\SQL2005"
            ElseIf database_name = "Complaints" Then
                conn2.ConnectionString = _
                   "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=PraiseAndComplaintsSQL;Data Source=ROSSVR01\SQL2005"
            ElseIf database_name = "Employed" Then
                conn2.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=r:\Access tables\Employed_bailiffs.mdb;Persist Security Info=False"
            End If
            conn2.Open()
            Dim adapter As New OleDb.OleDbDataAdapter(select_string, conn2)
            adapter.Fill(dataset)
            conn2.Close()
        End If
        Try
            no_of_rows = dataset.Tables(0).Rows.Count
            If no_of_rows = 0 Then
                Return 100
            End If

            If pigeon_hole Then
                last_rowid = 0
            Else
                last_rowid = no_of_rows
            End If

            Dim lastcol As Integer = dataset.Tables(0).Columns.Count

            Dim table As DataTable
            Dim row As DataRow
            Dim col As DataColumn
            Dim idx As Integer = 0

            ReDim table_array(no_of_rows, lastcol)
            For Each table In dataset.Tables
                error_found = False
                For Each row In table.Rows
                    idx2 = 0
                    If Not pigeon_hole Then
                        idx += 1
                    Else
                        idx = row.Item(0).ToString
                        If idx > last_rowid Then
                            last_rowid = idx
                        End If
                    End If
                    For Each col In table.Columns
                        Dim col_no As Integer
                        col_no = InStr(1, columns, col.ColumnName)
                        If col_no > 0 Then
                            Try
                                idx2 = Mid(columns, col_no - 2, 2)
                            Catch
                                Continue For
                            End Try
                        Else
                            idx2 += 1
                        End If
                        If col_no > 0 Or columns = Nothing Then
                            table_array(idx, idx2) = row.Item(col).ToString
                        End If
                    Next
                Next
            Next
            'ReDim Preserve table_array(last_rowid, lastcol)
            Return 0
        Catch ex As Exception
            If error_found Then
                MessageBox.Show(ex.Message)
                Return 1
            Else
                If idx2 = 0 Then
                    Return 100
                Else
                    MessageBox.Show(ex.Message)
                    Return 2
                End If
            End If
        End Try
    End Function

    Function get_dataset(ByVal database_name As String, Optional ByVal select_string As String = Nothing) As DataSet
        Dim dataset As New DataSet
        Try
            If database_name = "onestep" Then
                conn.ConnectionString = ""
                conn.ConnectionString = _
                   "Integrated Security=True;Dsn=DebtRecovery - DDSybase;na=192.168.19.26,14100;db=DebtRecovery;uid=thirdpartyB;password=thirdpartyB;"
                conn.Open()
                Dim adapter As New Odbc.OdbcDataAdapter(select_string, conn)

                adapter.Fill(dataset)
                conn.Close()
            Else
                conn2.ConnectionString = ""
                If database_name = "Fees" Then
                    conn2.ConnectionString = _
                     "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=FeesSQL;Data Source=ROSSVR01\SQL2005"
                ElseIf database_name = "TestFees" Then
                    conn2.ConnectionString = _
                    "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=TestFeesSQL;Data Source=ROSSVR01\SQL2005"
                ElseIf database_name = "Complaints" Then
                    conn2.ConnectionString = _
                       "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Password=sa;Initial Catalog=PraiseAndComplaintsSQL;Data Source=ROSSVR01\SQL2005"
                ElseIf database_name = "Employed" Then
                    conn2.ConnectionString = _
                   "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=r:\Access tables\Employed_bailiffs.mdb;Persist Security Info=False"
                End If
                conn2.Open()
                Dim adapter As New OleDb.OleDbDataAdapter(select_string, conn2)
                adapter.Fill(dataset)
                conn2.Close()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Try
            no_of_rows = dataset.Tables(0).Rows.Count
        Catch
            no_of_rows = 0
        End Try
        Return dataset

    End Function
   
End Module

Public Class mainfrm
    Dim start_date As Date
    Dim start_year As Integer
    Dim start_month As Integer
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub storebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles storebtn.Click
        'get balances for all LSC cases
        param1 = "onestep"
        param2 = "select _rowid from ClientScheme where branchID = 3"
        Dim cs_dataset As DataSet = get_dataset(param1, param2)
        Dim cs_rows As Integer = no_of_rows - 1
        Dim idx As Integer
        Dim csid As Integer
        ProgressBar1.Value = 5
        For idx = 0 To cs_rows
            csid = cs_dataset.Tables(0).Rows(idx).Item(0)
            param2 = "select _rowid, debt_amount, debt_costs, debt_balance, arrangeBrokenCount, " & _
            " last_stageID, status, arrange_interval, arrange_amount, arrange_initial_amount, " & _
            " arrange_initial_due, _createdDate from Debtor where clientschemeID = " & csid & ""
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            Dim debtor_rows As Integer = no_of_rows - 1
            Dim idx2 As Integer
            For idx2 = 0 To debtor_rows
                ProgressBar1.Value = (idx2 / (debtor_rows + 1)) * 100
                Dim debtorid As Integer = debtor_dataset.Tables(0).Rows(idx2).Item(0)
                Dim debt_amt As Decimal = debtor_dataset.Tables(0).Rows(idx2).Item(1)
                Dim debt_costs As Decimal = debtor_dataset.Tables(0).Rows(idx2).Item(2)
                Dim arr_broken_count As Integer = debtor_dataset.Tables(0).Rows(idx2).Item(4)
                If debt_costs = 0.01 Then
                    debt_costs = 0
                End If
                Dim debt_balance As Decimal = debtor_dataset.Tables(0).Rows(idx2).Item(3)
                'If debt_costs > 0 Then
                '    If debt_balance <= debt_costs Then
                '        If arr_broken_count = 0 Then  'arrangebrokencount
                '            debt_costs = 0
                '        Else
                '            Dim stageid As Integer = debtor_dataset.Tables(0).Rows(idx2).Item(5)
                '            param2 = "select name from Stage where _rowid = " & stageid
                '            Dim stage_dataset As DataSet = get_dataset(param1, param2)
                '            If InStr(stage_dataset.Tables(0).Rows(0).Item(0), "Awaiting") > 0 Then
                '                debt_costs = 0
                '            End If
                '        End If
                '    End If
                'End If
                'get remitted and os fees
                param2 = "select fee_amount, remited_fee, fee_remit_col from Fee where debtorID = " & debtorid & _
                " and feeWhoPays = 'D'"
                Dim fee_dataset As DataSet = get_dataset(param1, param2)
                Dim fee_rows As Integer = no_of_rows - 1
                Dim idx3 As Integer
                Dim remitted_bal As Decimal = 0
                Dim remitted_costs As Decimal = 0
                Dim remitted_fees As Decimal = 0
                Dim os_fees As Decimal = 0
                Dim waiting As Decimal = 0
                For idx3 = 0 To fee_rows
                    Dim remit_coll As Integer = fee_dataset.Tables(0).Rows(idx3).Item(2)
                    Dim fee_amt As Decimal = fee_dataset.Tables(0).Rows(idx3).Item(0)
                    Dim remited_fee As Decimal = fee_dataset.Tables(0).Rows(idx3).Item(1)
                    If remit_coll = 1 Then
                        remitted_bal += remited_fee
                    ElseIf remit_coll = 2 Then
                        remitted_costs += remited_fee
                    Else
                        remitted_fees += remited_fee
                        os_fees = os_fees + fee_amt - remited_fee
                    End If
                Next
                'get any waiting payments
                param2 = "select sum(amount) from Payment where debtorID = " & debtorid & _
                " and status = 'W'"
                Dim pay_dataset As DataSet = get_dataset(param1, param2)
                Try
                    waiting += pay_dataset.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                End Try

                'get amount due to be paid next month for arrangement cases
                Dim amt_due As Decimal = 0
                If debtor_dataset.Tables(0).Rows(idx2).Item(6) = "A" Then
                    Dim arr_interval As Integer
                    Dim arr_amt As Decimal
                    Dim arr_date As Date
                    Try
                        arr_interval = debtor_dataset.Tables(0).Rows(idx2).Item(7)
                        arr_amt = debtor_dataset.Tables(0).Rows(idx2).Item(8)
                    Catch ex As Exception
                        arr_interval = 28
                        Try
                            arr_amt = debtor_dataset.Tables(0).Rows(idx2).Item(9)
                            arr_date = debtor_dataset.Tables(0).Rows(idx2).Item(10)
                        Catch ex2 As Exception
                            arr_amt = 0
                        End Try
                        If Month(arr_date) <> start_month Then
                            arr_amt = 0
                        End If
                    End Try
                    If debt_balance < debt_amt Then
                        If arr_interval > 25 Then
                            amt_due = arr_amt
                        ElseIf arr_interval > 8 Then
                            amt_due = arr_amt * 2
                        Else
                            amt_due = arr_amt * 4
                        End If
                    Else
                        If debt_costs > 0 And arr_broken_count > 0 Then
                            If arr_interval > 25 Then
                                amt_due = arr_amt
                            ElseIf arr_interval > 8 Then
                                amt_due = arr_amt * 2
                            Else
                                amt_due = arr_amt * 4
                            End If
                        End If
                    End If
                    If amt_due > debt_amt + debt_costs Then
                        amt_due = debt_amt + debt_costs
                    End If
                End If
                Try
                    'see if row already exists
                    'Me.LSC_opening_balancesTableAdapter.FillBy2(FeesSQLDataSet.LSC_opening_balances, debtorid)
                    'If FeesSQLDataSet.LSC_opening_balances.Rows(0).Item(11) = 0 Then
                    '    Me.LSC_opening_balancesTableAdapter.UpdateQuery1(amt_due, debtorid)
                    'End If

                    Me.LSC_opening_balancesTableAdapter.FillBy1(FeesSQLDataSet.LSC_opening_balances, start_year, start_month, debtorid)
                    If FeesSQLDataSet.LSC_opening_balances.Rows.Count = 0 Then
                        Me.LSC_opening_balancesTableAdapter.InsertQuery(start_year, start_month, debtorid, debt_amt, debt_costs, remitted_bal, remitted_costs, remitted_fees, os_fees, waiting, amt_due)
                    Else
                        Try
                            Me.LSC_opening_balancesTableAdapter.DeleteQuery(start_year, start_month, debtorid)
                            Me.LSC_opening_balancesTableAdapter.InsertQuery(start_year, start_month, debtorid, debt_amt, debt_costs, remitted_bal, remitted_costs, remitted_fees, os_fees, waiting, amt_due)
                        Catch ex2 As Exception
                            MsgBox("Unable to store opening balance for debtor = " & debtorid)
                            MsgBox(ex2.Message)
                        End Try
                    End If

                Catch ex As Exception

                End Try
                'get transfer amount
                Dim created_date As Date = debtor_dataset.Tables(0).Rows(idx2).Item(11)
                'get final date
                param2 = "select _createdDate from Note where debtorID = " & debtorid & _
                " and type = 'Letter' and text = 'sent letter (Final_Contribution_Notification)'" & _
                " order by _rowid"
                Dim note_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 0 Then
                    Continue For
                End If
                Dim final_date As Date = note_dataset.Tables(0).Rows(0).Item(0)
                Dim final_date_2 As Date = DateAdd(DateInterval.Day, 1, final_date)
                'get figures changed record from final date

                param2 = "select text from Note where debtorID = " & debtorid & _
                " and type = 'Figures changed'" & _
                " and _createdDate >= '" & Format(final_date, "yyyy-MM-dd") & "'" & _
                " and _createdDate <= '" & Format(final_date_2, "yyyy-MM-dd") & "'" & _
                " order by _rowid"
                Dim note_dataset2 As DataSet = get_dataset(param1, param2)
                Dim contrib_debt As Decimal = 0
                Dim contrib_costs As Decimal = 0
                Dim contrib_debt_found As Boolean = False
                Dim contrib_costs_found As Boolean = False
                Dim idx4 As Integer
                For idx4 = 0 To no_of_rows - 1
                    Dim note_text As String = note_dataset2.Tables(0).Rows(idx4).Item(0)
                    If Microsoft.VisualBasic.Left(note_text, 4) = "Debt" _
                      And contrib_debt_found = False Then
                        Dim idx5 As Integer
                        Dim amt_str As String = ""
                        Dim start_idx As Integer = InStr(note_text, "was") + 3
                        For idx5 = start_idx To note_text.Length
                            If Mid(note_text, idx5, 1) = "." Then
                                amt_str = amt_str & Mid(note_text, idx5, 3)
                                contrib_debt_found = True
                                Exit For
                            Else
                                If Mid(note_text, idx5, 1) <> "," Then
                                    amt_str = amt_str & Mid(note_text, idx5, 1)
                                End If
                            End If
                        Next
                        If contrib_debt_found Then
                            contrib_debt = amt_str
                        End If
                    End If
                    If InStr(note_text, "Costs amount was") > 0 _
                    And contrib_costs_found = False Then
                        Dim idx5 As Integer
                        Dim amt_str As String = ""
                        For idx5 = 18 To note_text.Length
                            If Mid(note_text, idx5, 1) = "." Then
                                amt_str = amt_str & Mid(note_text, idx5, 3)
                                Exit For
                            Else
                                If Mid(note_text, idx5, 1) <> "," Then
                                    amt_str = amt_str & Mid(note_text, idx5, 1)
                                    contrib_costs_found = True
                                End If
                            End If
                        Next
                        If contrib_costs_found Then
                            contrib_costs = amt_str
                        End If
                    End If
                    If contrib_debt_found And contrib_costs_found Then
                        Exit For
                    End If
                Next
                If Not contrib_debt_found And Not contrib_costs_found Then
                    Dim final_date_1 As Date = DateAdd(DateInterval.Day, -6, final_date)
                    'look further back for figures changed
                    param2 = "select text from Note where debtorID = " & debtorid & _
                                    " and type = 'Figures changed'" & _
                                    " and _createdDate >= '" & Format(final_date_1, "yyyy-MM-dd") & "'" & _
                                    " and _createdDate <= '" & Format(final_date_2, "yyyy-MM-dd") & "'" & _
                                    " order by _rowid desc"
                    Dim note_dataset3 As DataSet = get_dataset(param1, param2)
                    If no_of_rows = 0 Then
                        MsgBox("Contribution due not found for cases " & debtorid)
                        Continue For
                    End If
                    For idx4 = 0 To no_of_rows - 1
                        Dim note_text As String = note_dataset3.Tables(0).Rows(idx4).Item(0)
                        If Microsoft.VisualBasic.Left(note_text, 4) = "Debt" _
                          And contrib_debt_found = False Then
                            Dim idx5 As Integer
                            Dim amt_str As String = ""
                            Dim start_idx As Integer = InStr(note_text, "was") + 3
                            For idx5 = start_idx To note_text.Length
                                If Mid(note_text, idx5, 1) = "." Then
                                    amt_str = amt_str & Mid(note_text, idx5, 3)
                                    contrib_debt_found = True
                                    Exit For
                                Else
                                    If Mid(note_text, idx5, 1) <> "," Then
                                        amt_str = amt_str & Mid(note_text, idx5, 1)
                                    End If
                                End If
                            Next
                            If contrib_debt_found Then
                                contrib_debt = amt_str
                            End If
                        End If
                        If InStr(note_text, "Costs amount was") > 0 _
                        And contrib_costs_found = False Then
                            Dim idx5 As Integer
                            Dim amt_str As String = ""
                            For idx5 = 18 To note_text.Length
                                If Mid(note_text, idx5, 1) = "." Then
                                    amt_str = amt_str & Mid(note_text, idx5, 3)
                                    Exit For
                                Else
                                    If Mid(note_text, idx5, 1) <> "," Then
                                        amt_str = amt_str & Mid(note_text, idx5, 1)
                                        contrib_costs_found = True
                                    End If
                                End If
                            Next
                            If contrib_costs_found Then
                                contrib_costs = amt_str
                            End If
                        End If
                        If contrib_debt_found And contrib_costs_found Then
                            Exit For
                        End If
                    Next
                End If
                Dim instalments As Decimal = DateDiff(DateInterval.Day, created_date, final_date) / 28
                If instalments > 5 Then
                    instalments = 5
                End If
                instalments = Int(instalments)
                Dim contrib_due As Decimal = contrib_debt * instalments / 5
                'add costs if arrangement broken 
                If instalments = 5 And arr_broken_count > 0 Then
                    contrib_due += contrib_costs
                End If
                Dim transfer_amt As Decimal = contrib_due
                If transfer_amt > debt_amt + debt_costs Then
                    transfer_amt = debt_amt + debt_costs
                End If
                'store transfer amt in table
                Try
                    Me.LSC_opening_balancesTableAdapter.UpdateQuery2(transfer_amt, start_year, start_month, debtorid)
                Catch ex As Exception
                    MsgBox("Transfer amt not stored for " & debtorid)
                End Try
            Next
        Next
        MsgBox("Opening balances stored")
        Me.Close()
    End Sub


    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Microsoft.VisualBasic.DateAndTime.Day(Now) < 10 Then
            start_month = Month(Now)
            start_year = Year(Now)
        Else
            start_month = Month(Now) + 1
            start_year = Year(Now)
        End If
        
        If start_month = 13 Then
            start_month = 1
            start_year += 1
        End If

        start_date = CDate(start_year & "," & start_month & ",1")
        Label1.Text = "For month " & Format(start_date, "MMM yyyy")
    End Sub

End Class

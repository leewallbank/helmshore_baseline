Module excel_Module
    Public filename, outfile, errorfile As String
    Public vals(1, 1) As String
    Public finalrow, finalcol As Integer
    Public Sub load_vals(ByVal filename As String, ByVal sheet_name As String)
        Dim row, col As Integer
        Dim xl = CreateObject("Excel.Application")
        xl.Workbooks.open(filename)
        xl.worksheets(sheet_name).activate()
        finalrow = xl.activesheet.usedrange.rows.count
        finalcol = xl.activesheet.usedrange.columns.count
        ReDim vals(finalrow, finalcol)
        Dim idx As Integer = 0
        For row = 1 To finalrow
            For col = 1 To finalcol
                idx += 1
                Try
                    vals(row, col) = xl.activesheet.cells(row, col).value.ToString
                Catch ex As Exception
                    vals(row, col) = Nothing
                End Try
            Next
        Next
        xl.workbooks.close()
        xl = Nothing
    End Sub
End Module

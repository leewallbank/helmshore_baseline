Public Class mainfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        exitbtn.Enabled = False
        openbtn.Enabled = False
        Dim fileok As Boolean = True
        Dim sheet_name As String = InputBox("What is sheet name?", "Enter sheet name")
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Excel files | *.xlsx"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                load_vals(filename, sheet_name)
            Catch ex As Exception
                MsgBox("Unable to read file")
                Me.Close()
                Exit Sub
            End Try
        Else
            fileok = False
        End If
        If fileok = False Then
            MsgBox("File NOT Opened")
            Me.Close()
            Exit Sub
        End If
        Dim idx, idx2, debtorID, in_number As Integer
        Dim new_txt As String = ""
        Dim in_str As String = ""
        Dim text_end As String = ";from experian feed on " & Format(Now, "dd/MM/yyyy") & vbNewLine
        For idx = 2 To finalrow
            ProgressBar1.Value = (idx / finalrow) * 100
            Application.DoEvents()
            Try
                debtorID = vals(idx, 1)
            Catch ex As Exception
                MsgBox("Invalid debtorID on line " & idx)
                Me.Close()
                Exit Sub
            End Try

            For idx2 = 10 To finalcol
                in_str = vals(idx, idx2)
                Try
                    in_number = in_str
                Catch ex As Exception
                    in_str = ""
                End Try
                If in_str <> "" Then
                    new_txt = ""
                    Select Case idx2
                        Case 10
                            new_txt = "Credit_cards_active:"
                        Case 11
                            new_txt = "Credit_cards_default:"
                        Case 12
                            new_txt = "Mortgage_current_balance:"
                        Case 13
                            new_txt = "Mortgage_total:"
                        Case 14
                            new_txt = "Unsecured_loans_active:"
                        Case 15
                            new_txt = "Unsecured_loans_default:"
                        Case 16
                            new_txt = "Mail_order_accts_active:"
                        Case 17
                            new_txt = "Mail_order_accts_default:"
                        Case 18
                            new_txt = "Current_accts_active:"
                        Case 19
                            new_txt = "Current_accts_default:"
                        Case 20
                            new_txt = "Current_accts_accum_balance:"
                        Case 21
                            new_txt = "Home_credit_accts_active:"
                        Case 22
                            new_txt = "Home_credit_accts_default:"
                        Case 23
                            new_txt = "Other_accts_active:"
                        Case 24
                            new_txt = "Other_accts_default:"
                        Case 25
                            new_txt = "Credit_score:"
                        Case 26
                            new_txt = "Property_valuation:"
                        Case 27
                            new_txt = "Secured_equity:"
                    End Select
                    If new_txt <> "" Then
                        filetext = filetext & debtorID & "|" & new_txt & in_number & text_end
                    End If
                End If
            Next
        Next

        'write out note file
        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_note.txt", filetext, False)
        MsgBox("Note txt file saved")
        Me.Close()
    End Sub
End Class

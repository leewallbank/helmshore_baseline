Public Class Form1
    Dim filename As String
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        selected_client_no = 0
        selected_client_scheme_no = 0
        cl_lbl.Text = ""
        sch_lbl.Text = ""
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub clientbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clientbtn.Click

        client_str = InputBox("enter first part of client name", "Select client")
        If client_str.Length = 0 Then
            MsgBox("No client name entered")
        Else
            clientfrm.ShowDialog()
            cl_lbl.Text = selected_client_name
        End If
    End Sub

    Private Sub schbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles schbtn.Click
        If selected_client_no <> 0 Then
            schfrm.ShowDialog()
            sch_lbl.Text = selected_sch_name
        End If
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        clientbtn.Enabled = False
        schbtn.Enabled = False
        runbtn.Enabled = False
        exitbtn.Enabled = False
        If selected_client_scheme_no <> 0 Then
            ProgressBar1.Value = 10
            Application.DoEvents()
            Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
            Dim RA1718Areport = New ra1718A
            Dim myArrayList1 As ArrayList = New ArrayList()
            myArrayList1.Add(selected_client_scheme_no)
            SetCurrentValuesForParameterField1(RA1718Areport, myArrayList1)
            myArrayList1.Add(My.User.Name)
            SetCurrentValuesForParameterField2(RA1718Areport, myArrayList1)
            myConnectionInfo.ServerName = "DebtRecovery"
            myConnectionInfo.DatabaseName = "DebtRecovery"
            myConnectionInfo.UserID = "vbnet"
            myConnectionInfo.Password = "tenbv"
            SetDBLogonForReport(myConnectionInfo, RA1718Areport)
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "PDF files |*.pdf"
                .DefaultExt = ".pdf"
                .OverwritePrompt = True
                .FileName = "RA1718A Performance report.pdf"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                ProgressBar1.Value = 25
                Application.DoEvents()
                filename = SaveFileDialog1.FileName
                RA1718Areport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
                RA1718Areport.close()

                Dim RA1718Breport = New RA1718B
                myArrayList1.Add(selected_client_scheme_no)
                SetCurrentValuesForParameterField1(RA1718Breport, myArrayList1)
                myArrayList1.Add(My.User.Name)
                SetCurrentValuesForParameterField2(RA1718Breport, myArrayList1)
                SetDBLogonForReport(myConnectionInfo, RA1718Breport)
                filename = Replace(filename, "RA1718A", "RA1718B")
                RA1718Breport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
                RA1718Breport.close()
                ProgressBar1.Value = 50
                Application.DoEvents()
                Dim RA1718Creport = New RA1718C
                myArrayList1.Add(selected_client_scheme_no)
                SetCurrentValuesForParameterField1(RA1718Creport, myArrayList1)
                myArrayList1.Add(My.User.Name)
                SetCurrentValuesForParameterField2(RA1718Creport, myArrayList1)
                SetDBLogonForReport(myConnectionInfo, RA1718Breport)
                filename = Replace(filename, "RA1718B", "RA1718C")
                RA1718Creport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
                RA1718Creport.close()
                ProgressBar1.Value = 75
                Application.DoEvents()
                Dim RA1718Dreport = New RA1718D
                myArrayList1.Add(selected_client_scheme_no)
                SetCurrentValuesForParameterField1(RA1718Dreport, myArrayList1)
                myArrayList1.Add(My.User.Name)
                SetCurrentValuesForParameterField2(RA1718Dreport, myArrayList1)
                SetDBLogonForReport(myConnectionInfo, RA1718Breport)
                filename = Replace(filename, "RA1718C", "RA1718D")
                RA1718Dreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
                RA1718Dreport.close()
                MsgBox("Completed")
                Me.Close()
            End If
        End If
    End Sub
End Class

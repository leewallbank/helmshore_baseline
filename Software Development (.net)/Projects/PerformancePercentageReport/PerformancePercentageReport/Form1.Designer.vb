<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.clientbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.schbtn = New System.Windows.Forms.Button
        Me.cl_lbl = New System.Windows.Forms.Label
        Me.sch_lbl = New System.Windows.Forms.Label
        Me.runbtn = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.SuspendLayout()
        '
        'clientbtn
        '
        Me.clientbtn.Location = New System.Drawing.Point(86, 43)
        Me.clientbtn.Name = "clientbtn"
        Me.clientbtn.Size = New System.Drawing.Size(90, 23)
        Me.clientbtn.TabIndex = 0
        Me.clientbtn.Text = "Select client"
        Me.clientbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(205, 250)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 3
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'schbtn
        '
        Me.schbtn.Location = New System.Drawing.Point(86, 105)
        Me.schbtn.Name = "schbtn"
        Me.schbtn.Size = New System.Drawing.Size(90, 23)
        Me.schbtn.TabIndex = 1
        Me.schbtn.Text = "Select scheme"
        Me.schbtn.UseVisualStyleBackColor = True
        '
        'cl_lbl
        '
        Me.cl_lbl.AutoSize = True
        Me.cl_lbl.Location = New System.Drawing.Point(99, 27)
        Me.cl_lbl.Name = "cl_lbl"
        Me.cl_lbl.Size = New System.Drawing.Size(39, 13)
        Me.cl_lbl.TabIndex = 3
        Me.cl_lbl.Text = "Label1"
        '
        'sch_lbl
        '
        Me.sch_lbl.AutoSize = True
        Me.sch_lbl.Location = New System.Drawing.Point(99, 89)
        Me.sch_lbl.Name = "sch_lbl"
        Me.sch_lbl.Size = New System.Drawing.Size(39, 13)
        Me.sch_lbl.TabIndex = 4
        Me.sch_lbl.Text = "Label1"
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(86, 165)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(75, 23)
        Me.runbtn.TabIndex = 2
        Me.runbtn.Text = "Run report"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 250)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 5
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 315)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.runbtn)
        Me.Controls.Add(Me.sch_lbl)
        Me.Controls.Add(Me.cl_lbl)
        Me.Controls.Add(Me.schbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.clientbtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Performance percentage report"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents clientbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents schbtn As System.Windows.Forms.Button
    Friend WithEvents cl_lbl As System.Windows.Forms.Label
    Friend WithEvents sch_lbl As System.Windows.Forms.Label
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar

End Class

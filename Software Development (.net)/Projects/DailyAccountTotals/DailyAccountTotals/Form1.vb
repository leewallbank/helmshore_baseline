Public Class Form1
    Dim outline As String
    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        Dim idx As Integer
        Dim day_rows As Integer = Me.FeesSQLDataSet.Calendar.Rows.Count
        'change start day here
        For idx = 21 To day_rows - 1
            ProgressBar1.Value = (idx / day_rows) * 100
            Dim day As Integer = Me.FeesSQLDataSet.Calendar.Rows(idx).Item(0)
            Dim month As Integer = Me.FeesSQLDataSet.Calendar.Rows(idx).Item(1)
            Dim start_date As Date = CDate("2011," & month & "," & day)
            Dim end_date As Date = DateAdd(DateInterval.Day, 1, start_date)
            'get clients schemes
            Dim csid As Integer
            param1 = "onestep"
            param2 = "select _rowid from ClientScheme where branchID = 1" & _
            " order by _rowid"
            Dim cs_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                MsgBox("No clientschemes found")
                Exit Sub
            End If
            Dim csid_rows As Integer = no_of_rows
            Dim idx2 As Integer
            For idx2 = 0 To csid_rows - 1
                csid = cs_dataset.Tables(0).Rows(idx2).Item(0)
                param2 = "select clientID, schemeID from ClientScheme where _rowid = " & csid
                Dim csid_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    MsgBox("Clientscheme not found for " & csid)
                    Exit Sub
                End If
                Dim cl_name, sch_name As String
                Dim cl_id As Integer = csid_dataset.Tables(0).Rows(0).Item(0)
                If cl_id = 1 Or cl_id = 2 Or cl_id = 24 Then
                    Continue For
                End If
                param2 = "select name from Client where _rowid = " & cl_id
                Dim cl_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    MsgBox("Client not found for " & cl_id)
                    Exit Sub
                End If
                cl_name = Trim(cl_dataset.Tables(0).Rows(0).Item(0))
                Dim sch_id As Integer = csid_dataset.Tables(0).Rows(0).Item(1)
                param2 = "select name from Scheme where _rowid = " & sch_id
                Dim sch_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    MsgBox("Scheme not found for " & sch_id)
                    Exit Sub
                End If
                sch_name = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                'cases/value loaded
                param2 = "select _createdDate, debt_original from Debtor where clientschemeID = " & csid
                Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                Dim case_rows As Integer = no_of_rows
                Dim idx3 As Integer
                Dim value_loaded As Decimal = 0
                Dim cases_loaded As Integer = 0
                For idx3 = 0 To case_rows - 1
                    Dim created_date As Date = debtor_dataset.Tables(0).Rows(idx3).Item(0)
                    If created_date >= start_date And created_date < end_date Then
                        cases_loaded += 1
                        value_loaded += debtor_dataset.Tables(0).Rows(idx3).Item(1)
                    End If
                Next
                'funds remitted
                param2 = "select split_debt, split_costs, split_fees, split_van from Remit where clientschemeID = " & csid & _
                " and date >= '" & Format(start_date, "yyyy-MM-dd") & "'" & _
                " and date < '" & Format(end_date, "yyyy-MM-dd") & "'"
                Dim remit_dataset As DataSet = get_dataset(param1, param2)
                Dim remit_rows As Integer = no_of_rows
                Dim cl_remit As Decimal = 0
                Dim fee_remit As Decimal = 0
                For idx3 = 0 To remit_rows - 1
                    cl_remit += remit_dataset.Tables(0).Rows(idx3).Item(0) + remit_dataset.Tables(0).Rows(idx3).Item(1)
                    fee_remit += remit_dataset.Tables(0).Rows(idx3).Item(2) + remit_dataset.Tables(0).Rows(idx3).Item(3)
                Next
                'get payments
                param2 = "select amount, status, amount_sourceID from Payment where clientschemeID = " & csid & _
                " and _createdDate >= '" & Format(start_date, "yyyy-MM-dd") & "'" & _
                " and _createdDate < '" & Format(end_date, "yyyy-MM-dd") & "'"
                Dim pay_dataset As DataSet = get_dataset(param1, param2)
                Dim pay_rows As Integer = no_of_rows
                Dim gross_receipts As Decimal = 0
                Dim bounced As Decimal = 0
                Dim direct As Decimal = 0
                For idx3 = 0 To pay_rows - 1
                    Dim status As String = pay_dataset.Tables(0).Rows(idx3).Item(1)
                    If status = "B" Then
                        bounced += pay_dataset.Tables(0).Rows(idx3).Item(0)
                    ElseIf status = "W" Or status = "R" Then
                        param2 = "select direct from PaySource where _rowid = " & pay_dataset.Tables(0).Rows(idx3).Item(2)
                        Dim paysource_dataset As DataSet = get_dataset(param1, param2)
                        If no_of_rows <> 1 Then
                            MsgBox("Unable to read paysource for " & pay_dataset.Tables(0).Rows(idx3).Item(1))
                            Exit Sub
                        End If
                        If paysource_dataset.Tables(0).Rows(0).Item(0) = "Y" Then
                            direct += pay_dataset.Tables(0).Rows(idx3).Item(0)
                        Else
                            gross_receipts += pay_dataset.Tables(0).Rows(idx3).Item(0)
                        End If
                    End If
                Next
                If cases_loaded + cl_remit + fee_remit + bounced + gross_receipts + direct = 0 Then
                    Continue For
                End If
                outline = csid & "|" & cl_name & "|" & sch_name & "|" & Format(start_date, "dd/MM/yyyy") & "|" & cases_loaded & "|" & Format(value_loaded, "n") & _
                                            "|" & Format(cl_remit, "n") & "|" & Format(fee_remit, "n") & "|" & Format(gross_receipts, "n") & _
                                            "|" & Format(bounced, "n") & "|" & Format(gross_receipts + bounced, "n") & "|" & _
                                            Format(direct, "n") & "|" & Format(gross_receipts + bounced + direct, "n") & vbNewLine
                My.Computer.FileSystem.WriteAllText("H:/temp/dat.txt", outline, True)
            Next
        Next
        MsgBox("Completed")
        Me.Close()
    End Sub

   

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FeesSQLDataSet.Calendar' table. You can move, or remove it, as needed.
        Me.CalendarTableAdapter.Fill(Me.FeesSQLDataSet.Calendar)
        
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub
End Class

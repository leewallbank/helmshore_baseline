<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class commfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.doctext = New System.Windows.Forms.TextBox
        Me.printbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.nextbtn = New System.Windows.Forms.Button
        Me.prevbtn = New System.Windows.Forms.Button
        Me.pagelbl = New System.Windows.Forms.Label
        Me.casesbtn = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'doctext
        '
        Me.doctext.Location = New System.Drawing.Point(0, 0)
        Me.doctext.Multiline = True
        Me.doctext.Name = "doctext"
        Me.doctext.ReadOnly = True
        Me.doctext.Size = New System.Drawing.Size(495, 511)
        Me.doctext.TabIndex = 4
        Me.doctext.TabStop = False
        '
        'printbtn
        '
        Me.printbtn.Location = New System.Drawing.Point(515, 229)
        Me.printbtn.Name = "printbtn"
        Me.printbtn.Size = New System.Drawing.Size(92, 32)
        Me.printbtn.TabIndex = 3
        Me.printbtn.Text = "Print "
        Me.printbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(518, 441)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 5
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'nextbtn
        '
        Me.nextbtn.Location = New System.Drawing.Point(518, 23)
        Me.nextbtn.Name = "nextbtn"
        Me.nextbtn.Size = New System.Drawing.Size(89, 23)
        Me.nextbtn.TabIndex = 0
        Me.nextbtn.Text = "Next Bailiff"
        Me.nextbtn.UseVisualStyleBackColor = True
        '
        'prevbtn
        '
        Me.prevbtn.Location = New System.Drawing.Point(518, 71)
        Me.prevbtn.Name = "prevbtn"
        Me.prevbtn.Size = New System.Drawing.Size(92, 23)
        Me.prevbtn.TabIndex = 1
        Me.prevbtn.Text = "Previous Bailiff"
        Me.prevbtn.UseVisualStyleBackColor = True
        '
        'pagelbl
        '
        Me.pagelbl.AutoSize = True
        Me.pagelbl.Location = New System.Drawing.Point(524, 185)
        Me.pagelbl.Name = "pagelbl"
        Me.pagelbl.Size = New System.Drawing.Size(25, 13)
        Me.pagelbl.TabIndex = 0
        Me.pagelbl.Text = "      "
        '
        'casesbtn
        '
        Me.casesbtn.Location = New System.Drawing.Point(518, 118)
        Me.casesbtn.Name = "casesbtn"
        Me.casesbtn.Size = New System.Drawing.Size(89, 25)
        Me.casesbtn.TabIndex = 2
        Me.casesbtn.Text = "Display cases"
        Me.casesbtn.UseVisualStyleBackColor = True
        '
        'commfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(637, 510)
        Me.Controls.Add(Me.casesbtn)
        Me.Controls.Add(Me.pagelbl)
        Me.Controls.Add(Me.prevbtn)
        Me.Controls.Add(Me.nextbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.printbtn)
        Me.Controls.Add(Me.doctext)
        Me.Name = "commfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Employed bailiff RTD Commissions"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents doctext As System.Windows.Forms.TextBox
    Friend WithEvents printbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents nextbtn As System.Windows.Forms.Button
    Friend WithEvents prevbtn As System.Windows.Forms.Button
    Friend WithEvents pagelbl As System.Windows.Forms.Label
    Friend WithEvents casesbtn As System.Windows.Forms.Button
End Class

Public Class mainfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        Dim start_date As Date = CDate("2010-01-01")
        Dim record As String = "Debtor|Levy|FC Fees|FC Fees remitted|Van fees|Van fees remitted|" & vbNewLine
        param1 = "onestep"
        param2 = "select _rowid, clientschemeID, last_stageID, status from Debtor" & _
        " where _createdDate >= '" & _
        Format(start_date, "yyyy-MM-dd") & "'"
        Dim debtor_dataset As DataSet = get_dataset(param1, param2)
        Dim debtor_rows As Integer = no_of_rows - 1
        Dim idx As Integer
        Dim hit_team_found As Boolean
        For idx = 0 To debtor_rows
            hit_team_found = False
            Dim debtor As Integer = debtor_dataset.Tables(0).Rows(idx).Item(0)
            'check stage is pre-van
            Dim stageID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(2)
            param2 = "select name from Stage where _rowid = " & stageID
            Dim stage_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows <> 1 Then
                MsgBox("Invalid stage on debtor = " & debtor)
                Exit Sub
            End If
            Dim stage As String = stage_dataset.Tables(0).Rows(0).Item(0)
            If Microsoft.VisualBasic.Left(stage, 3) <> "Van" And _
            Microsoft.VisualBasic.Left(stage, 3) <> "Fur" And _
            Microsoft.VisualBasic.Left(stage, 3) <> "(Re" Then
                Continue For
            End If
            'check branch is 1
            Dim csID As Integer = debtor_dataset.Tables(0).Rows(idx).Item(1)
            param2 = "select branchID from ClientScheme where _rowid = " & csID
            Dim cs_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                MsgBox("invalid clientscheme on debtor = " & debtor)
                Exit Sub
            End If
            If cs_dataset.Tables(0).Rows(0).Item(0) <> 1 Then
                Continue For
            End If

            'see if there was a van bailiff allocation
            Dim pre_van As Boolean = True
            param2 = "select bailiffID from Visit where debtorID = " & debtor & _
            " order by _rowid desc"
            Dim visit_dataset As DataSet = get_dataset(param1, param2)
            Dim visit_rows As Integer = no_of_rows - 1
            Dim idx2 As Integer
            For idx2 = 0 To visit_rows
                Dim bailID As Integer
                Try
                    bailID = visit_dataset.Tables(0).Rows(idx2).Item(0)
                Catch ex As Exception
                    Continue For
                End Try
                param2 = "select _rowid from Bailiff where _rowid = " & bailID & _
                " and internalExternal = 'E'"
                Dim bail_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 1 Then
                    pre_van = False
                    Exit For
                End If
            Next
            If pre_van Then
                Continue For
            End If
            'check if payment taken in an arrangement
            Dim status As String = debtor_dataset.Tables(0).Rows(idx).Item(3)
            param2 = "select _createdBy, type from Note where debtorID = " & debtor & _
                " and (type = 'Arrangement' or type = 'Broken') order by _rowid desc"
            Dim note_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim type As String = note_dataset.Tables(0).Rows(0).Item(1)
            Dim arr_user As String = ""
            If type = "Arrangement" Then
                arr_user = note_dataset.Tables(0).Rows(0).Item(0)
            End If
            If arr_user <> "" Then
                'now get agent record
                param2 = "select typeSub from Bailiff where login_name = '" & arr_user & "'"
                Dim bail_dataset As DataSet = get_dataset(param1, param2)
                Dim bail_rows As Integer = no_of_rows - 1
                For idx2 = 0 To bail_rows
                    Try
                        If Trim(bail_dataset.Tables(0).Rows(idx2).Item(0)) = "Hit Team" Then
                            hit_team_found = True
                            Exit For
                        End If
                    Catch ex As Exception
                    End Try
                Next
            End If
            If Not hit_team_found Then
                'see if last payment taken by hit team member
                param2 = "select _createdBy from Payment where debtorID = " & debtor & _
                " and amount > 0 order by _rowid desc"
                Dim pay_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 0 Then
                    Continue For
                End If
                'now get agent record
                Dim pay_user As String = ""
                Try
                    pay_user = pay_dataset.Tables(0).Rows(0).Item(0)
                Catch ex As Exception

                End Try
                If pay_user <> "" Then
                    param2 = "select typeSub from Bailiff where login_name = '" & pay_user & "'"
                    Dim bail_dataset As DataSet = get_dataset(param1, param2)
                    Dim bail_rows As Integer = no_of_rows - 1
                    For idx2 = 0 To bail_rows
                        Try
                            If Trim(bail_dataset.Tables(0).Rows(idx2).Item(0)) = "Hit Team" Then
                                hit_team_found = True
                                Exit For
                            End If
                        Catch ex As Exception

                        End Try
                    Next
                End If
            End If
            If hit_team_found Then
                'get fee details
                param2 = "select type, fee_amount, remited_fee, fee_remit_col from Fee" & _
                " where debtorID = " & debtor & " and fee_amount > 0 and " & _
                " (fee_remit_col = 3 or fee_remit_col = 4)"
                Dim fee_dataset As DataSet = get_dataset(param1, param2)
                Dim fee_rows As Integer = no_of_rows - 1
                Dim levy_found As String = "N"
                Dim fc_fees As Decimal = 0
                Dim fc_fees_remit As Decimal = 0
                Dim van_fees As Decimal = 0
                Dim van_fees_remit As Decimal = 0
                Dim van_fees_found As Boolean = False
                For idx2 = 0 To fee_rows
                    Dim fee_amt As Decimal = fee_dataset.Tables(0).Rows(idx2).Item(1)
                    Dim remited_fee_amt As Decimal = fee_dataset.Tables(0).Rows(idx2).Item(2)
                    Dim fee_type As String = fee_dataset.Tables(0).Rows(idx2).Item(0)
                    If Microsoft.VisualBasic.Left(LCase(fee_type), 4) = "levy" Then
                        levy_found = "Y"
                    End If
                    If fee_dataset.Tables(0).Rows(idx2).Item(3) = 3 Then
                        fc_fees += fee_amt
                        fc_fees_remit += remited_fee_amt
                    Else
                        van_fees_found = True
                        van_fees += fee_amt
                        van_fees_remit += remited_fee_amt
                    End If
                Next
                If van_fees_found Then
                    Continue For
                End If
                record = record & debtor & "|" & levy_found & "|" & Format(fc_fees, "Fixed") & "|" & _
                Format(fc_fees_remit, "Fixed") & "|" & Format(van_fees, "Fixed") & "|" & _
                Format(van_fees_remit, "Fixed") & "|" & vbNewLine
            End If
        Next
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "TXT files |*.txt"
            .DefaultExt = ".txt"
            .OverwritePrompt = True
            .FileName = "Hit_team_postvan.txt"
        End With

        Dim filepath As String = SaveFileDialog1.FileName
        
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, record, False)
        End If
        MsgBox("File saved in - " & SaveFileDialog1.FileName)
        Me.Close()
    End Sub
End Class

﻿Public Class Form1
    Public ascii As New System.Text.ASCIIEncoding()
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        Dim arr_no As Integer = 0
        Dim fees As Decimal = 0
        Dim pot_fees As Decimal = 0
        Dim client As Decimal = 0
        Dim pot_client As Decimal = 0
        Dim waiting As Decimal = 0
        Dim alloc_no As Integer = 0
        outfile = "Number of Cases Loaded,Number of cases dialed,Number of Contacts,Success,Number of arrangements," &
            "actual fees,actual Client money, waiting payments,Number allocated to Bailiff" & vbNewLine

        'read in spreadsheet of cases
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Excel files | *.xls;*.xlsx"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then

            filename = OpenFileDialog1.FileName
            If load_vals(filename) = 1 Then   'file not opened
                Me.Close()
                Exit Sub
            End If
           
        Else
            fileok = False
        End If
        If fileok = True Then

        Else
            MsgBox("File not opened")
            Exit Sub
        End If
        'do manipulation here
        runbtn.Enabled = False
        exitbtn.Enabled = False
        Dim idx As Integer
        For idx = 1 To finalrow
            Try
                ProgressBar1.Value = (idx / finalrow) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            Dim debtorID As Decimal
            Try
                debtorID = vals(idx, 1)
            Catch ex As Exception
                Continue For
            End Try
            'see if an arrangement set up since campaign start
            param2 = "select count(*) from Note where debtorID = " & debtorID &
                " and type = 'Arrangement' and _createdDate >= '" & Format(campaign_dtp.Value, "yyyy-MM-dd") & "'"
            Dim note_ds As DataSet = get_dataset("onestep", param2)
            If note_ds.Tables(0).Rows(0).Item(0) > 0 Then
                arr_no += 1
            End If

            'see if payments remitted since campaign
            param2 = "select split_debt, split_costs, split_fees, split_van, split_other from Payment " &
                " where debtorID = " & debtorID & " and status = 'R'" &
                " and date >= '" & Format(campaign_dtp.Value, "yyyy-MM-dd") & "'"
            Dim paid_ds As DataSet = get_dataset("onestep", param2)
            Dim paid_rows As Integer = no_of_rows - 1
            For idx2 = 0 To paid_rows
                client = client + paid_ds.Tables(0).Rows(idx2).Item(0) + paid_ds.Tables(0).Rows(idx2).Item(1)
                fees = fees + paid_ds.Tables(0).Rows(idx2).Item(2) + paid_ds.Tables(0).Rows(idx2).Item(3) + paid_ds.Tables(0).Rows(idx2).Item(4)
            Next

            'get any waiting payments
            param2 = "select amount from Payment " &
                " where debtorID = " & debtorID & " and status = 'W'" &
                " and date >= '" & Format(campaign_dtp.Value, "yyyy-MM-dd") & "'"
            Dim paid2_ds As DataSet = get_dataset("onestep", param2)
            paid_rows = no_of_rows - 1
            For idx2 = 0 To paid_rows
                waiting += paid2_ds.Tables(0).Rows(idx2).Item(0)
            Next


            'add in any unremitted debts and fees as potential - not now required
            'param2 = "select fee_amount, remited_fee, fee_remit_col from Fee where debtorID = " & debtorID &
            '    " and feeWhoPays = 'D'"
            'Dim fee_ds As DataSet = get_dataset("onestep", param2)
            'Dim fee_rows As Integer = no_of_rows - 1
            'For fee_idx = 0 To fee_rows
            '    If fee_ds.Tables(0).Rows(fee_idx).Item(2) < 3 Then
            '        pot_client = pot_client + fee_ds.Tables(0).Rows(fee_idx).Item(0) - fee_ds.Tables(0).Rows(fee_idx).Item(1)
            '    Else

            '        pot_fees = pot_fees + fee_ds.Tables(0).Rows(fee_idx).Item(0) - fee_ds.Tables(0).Rows(fee_idx).Item(1)
            '    End If
            'Next

            'get number allocated to a bailiff
            param2 = "select text from Note where debtorID = " & debtorID &
                 " and type = 'Note' and _createdDate >= '" & Format(campaign_dtp.Value, "yyyy-MM-dd") & "'"
            Dim note2_ds As DataSet = get_dataset("onestep", param2)
            Dim note_rows As Integer = no_of_rows - 1
            For note_idx = 0 To note_rows
                Dim note_text As String = LCase(note2_ds.Tables(0).Rows(note_idx).Item(0))
                If Microsoft.VisualBasic.Left(note_text, 7) <> "bailiff" Then
                    Continue For
                End If
                If InStr(note_text, "account") > 0 Then
                    Continue For
                End If
                If InStr(note_text, "stacked") > 0 Then
                    Continue For
                End If
                If InStr(note_text, "overplu") > 0 Then
                    Continue For
                End If
                If InStr(note_text, "return") > 0 Then
                    Continue For
                End If
                Dim start_idx As Integer = InStr(note_text, "id:")
                If start_idx = 0 Then
                    Continue For
                End If
                Dim idx2 As Integer
                For idx2 = start_idx To note_text.Length
                    If Mid(note_text, idx2, 1) = ")" Then
                        Exit For
                    End If
                Next
                Dim bailiffID As Integer = Mid(note_text, start_idx + 3, idx2 - start_idx - 3)
                param2 = "select name_sur from Bailiff where _rowid = " & bailiffID &
                    " and agent_type = 'B'"
                Dim bail_ds As DataSet = get_dataset("onestep", param2)
                If no_of_rows > 0 Then
                    Dim bail_name As String = bail_ds.Tables(0).Rows(0).Item(0)
                    alloc_no += 1
                    Exit For
                End If
            Next
        Next

        'write out results file
        outfile = outfile & ",,,," & arr_no & "," & fees & "," & client & "," &
             waiting & "," & alloc_no & vbNewLine
        For idx = filename.Length To 1 Step -1
            If Mid(filename, idx, 1) = "\" Then
                Exit For
            End If
        Next
        filename = Microsoft.VisualBasic.Left(filename, idx)
        filename = filename & "results.csv"
        My.Computer.FileSystem.WriteAllText(filename, outfile, False, ascii)
        MsgBox("Results saved")
        Me.Close()
    End Sub
End Class

Module excel_Module
    Public filename, outfile As String
    Public vals(1, 1) As String
    Public finalrow, finalcol As Integer
    Public Function load_vals(ByVal filename As String) As Integer
        Dim row, col As Integer
        Dim xl = CreateObject("Excel.Application")
        xl.Workbooks.open(filename)
        Try
            xl.worksheets("Sheet1").activate()
        Catch ex As Exception
            xl.workbooks.close()
            MsgBox("Unable to read file - make sure sheet is called Sheet1")
            Return (1)
        End Try


        finalrow = xl.activesheet.usedrange.rows.count
        finalcol = xl.activesheet.usedrange.columns.count
        ReDim vals(finalrow, finalcol)
        Dim idx As Integer = 0
        For row = 1 To finalrow
            For col = 1 To finalcol
                idx += 1
                If xl.activesheet.cells(row, col).value = Nothing Then
                    'vals(row, col) = 0
                Else
                    vals(row, col) = xl.activesheet.cells(row, col).value.ToString
                End If
            Next
        Next
        xl.workbooks.close()
        xl = Nothing
        Return (0)
    End Function
End Module

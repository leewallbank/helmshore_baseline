﻿Public Class Form1
    'table for keeping record of telephone numbers reported on
    Dim telArray(10000, 3) As String
    Dim telMax As Integer = 0
    Dim outfile As String = ""
    Dim rowCount As Integer = 0
    Dim ascii As New System.Text.ASCIIEncoding()
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("StudentLoans")

        'get address changes for last week
        Dim startdate As Date = DateAdd(DateInterval.Day, -Weekday(Now) - 6, Now)
        startdate = CDate(Format(startdate, "MMM dd, yyyy" & " 00:00:00"))
        Dim endDate As Date = DateAdd(DateInterval.Day, 7, startdate)
        Dim debt_dt As New DataTable
        '1876
        LoadDataTable2("DebtRecovery", "select D._rowid, D.client_ref,name_title, name_fore, name_sur, N.text,N._createdDate,D.address,D.add_postcode," & _
                       "D.status,D.arrange_amount,arrange_interval,debt_balance " &
                       " from Debtor D, note N, clientScheme CS" & _
                       " where N.debtorID = D._rowID" & _
                       " and D.clientSchemeID = CS._rowID" & _
                       " and CS.clientID = 1943" & _
                       " and N.type = 'Address'" & _
                       " and N.text like 'changed from:%'" & _
                       " and N._createdDate >= '" & Format(startdate, "yyyy-MM-dd") & "'" & _
                       " and N._createdDate < ' " & Format(endDate, "yyyy-MM-dd") & "'", debt_dt, False)
        For Each debtrow In debt_dt.Rows
            Dim debtorID As Integer = debtrow(0)
            Dim clientRef As String = Microsoft.VisualBasic.Left(debtrow(1), 13)
            Dim fullName As String = ""
            Try
                fullName = debtrow(2) & " "
            Catch ex As Exception

            End Try
            Try
                fullName &= debtrow(3) & " "
            Catch ex As Exception

            End Try
            Try
                fullName &= debtrow(4)
            Catch ex As Exception

            End Try
            outfile &= clientRef & "|" & fullName & "|"
            Dim address As String = debtrow(5)
            Dim addressArray() As String = address.Split(",")
            Dim postCode As String = ""
            Dim addLine(10) As String
            For addressidx = 0 To 10
                addLine(addressidx) = ""
            Next
            For addressIDX = 0 To UBound(addressArray)
                Dim addrLine As String = Trim(Replace(addressArray(addressIDX), "changed from:", ""))
                addLine(addressIDX) = addrLine
            Next
            'set postcode to last address line
            For addressidx = 5 To 0 Step -1
                If addLine(addressidx) <> "" Then
                    postCode = addLine(addressidx)
                    addLine(addressidx) = ""
                    Exit For
                End If
            Next
            'set last address line if more than 4
            For addressidx = 5 To 10
                If addLine(addressidx) <> "" Then
                    addLine(4) &= " " & addLine(addressidx)
                End If
            Next
            Dim notedate As Date = debtrow(6)
            outfile &= addLine(0) & "|" & addLine(1) & "|" & addLine(2) & "|" & addLine(3) & "|" & addLine(4) & "|" & postCode & "|" & Format(notedate, "dd/MM/yyyy") & "|"
            outfile &= fullName & "|"
            Dim newPostCode As String = ""
            Try
                newPostCode = debtrow(8)
            Catch ex As Exception

            End Try
            Dim newAddress As String = debtrow(7)
            'separate out into address lines
            Dim newAddLines(10) As String
            For addressidx = 0 To 10
                newAddLines(addressidx) = ""
            Next
            Dim addLineIDX As Integer = 0
            Dim addressline As String = ""
            For addressidx = 1 To newAddress.Length
                If Mid(newAddress, addressidx, 1) = Chr(10) Or Mid(newAddress, addressidx, 1) = Chr(13) Then
                    newAddLines(addLineIDX) = addressline
                    addLineIDX += 1
                    addressline = ""
                Else
                    addressline &= Mid(newAddress, addressidx, 1)
                End If
            Next
            'save last line
          
            newAddLines(addLineIDX) &= addressline
            'if any address line is same as postcode - blank it out
            For addressidx = 0 To 4
                If newAddLines(addressidx) = newPostCode Then
                    newAddLines(addressidx) = ""
                End If
            Next
            'if addlines 5-10 are not blank - add to 4
            For addressidx = 5 To 10
                If newAddLines(addressidx) <> "" And newAddLines(addressidx) <> newPostCode Then
                    newAddLines(4) &= " " & newAddLines(addressidx)
                End If
            Next
            outfile &= newAddLines(0) & "|" & newAddLines(1) & "|" & newAddLines(2) & "|" & newAddLines(3) & "|" & newAddLines(4) & "|"
            outfile &= newPostCode & "|"
            Dim frequency As String = ""
            If debtrow(9) = "A" Then
                frequency = ""
                outfile &= debtrow(10) & "|"
                Try
                    frequency = debtrow(11)
                Catch ex As Exception
                    frequency = "Whole Amount"
                End Try
                If frequency = "" Then
                    Select Case debtrow(11)
                        Case 7 : frequency = "Weekly"
                        Case 14 : frequency = "Fortnightly"
                        Case 30 : frequency = "Monthly"
                        Case 31 : frequency = "Monthly"
                        Case Else : frequency = "Periodic"
                    End Select
                End If

                outfile &= frequency & "|"
            Else
                outfile &= "|" & "|"
            End If

            'dob and homeowner status
            outfile &= "|" & "|"

            'get any tel number changes for this debtor
            Dim tel_dt As New DataTable

            LoadDataTable2("StudentLoans", "select add_phone,add_fax,empphone,empfax" & _
                           " from rpt.UUBusinessRetailDebtor " & _
                           " where DebtorID = " & debtorID & _
                           " and Builddate >= '" & Format(startdate, "yyyy-MM-dd") & "'" & _
                           " and Builddate < '" & Format(endDate, "yyyy-MM-dd") & "'", tel_dt, False)
            'Just do one phone number here - any others picked up later on separate line
            Dim phoneReported As Boolean = False
            For Each telRow In tel_dt.Rows
                Dim telNo As String = ""
                Try
                    telNo = Trim(telRow(0))
                Catch ex As Exception

                End Try
                'see if 
                If telNo.Length > 4 Then
                    Dim phoneType As String
                    If Microsoft.VisualBasic.Left(telNo, 2) = "07" Then
                        phoneType = "M"
                    Else
                        phoneType = "H"
                    End If
                    'see if already reported on
                    Dim telFound As Boolean = False
                    For telIDX = 1 To telMax
                        If telArray(telIDX, 1) = debtorID And
                            telArray(telIDX, 2) = telNo Then
                            telFound = True
                            Exit For
                        End If
                    Next
                    If Not telFound Then
                        'see if phone type already reported on
                        Dim phoneTypeFound As Boolean = False
                        Dim phoneTypeOther As Boolean = False
                        For telIDx = 1 To telMax
                            If telArray(telIDx, 1) = debtorID Then
                                If telArray(telIDx, 3) = phoneType Then
                                    phoneTypeFound = True
                                End If
                                If telArray(telIDx, 3) = "O" Then
                                    phoneTypeOther = True
                                End If
                            End If
                        Next
                        If Not phoneTypeFound Then
                            telMax += 1
                            telArray(telMax, 1) = debtorID
                            telArray(telMax, 2) = telNo
                            telArray(telMax, 3) = phoneType
                            phoneReported = True
                            outfile &= telNo & "|" & phoneType & "|"
                        ElseIf Not phoneTypeOther Then
                            telMax += 1
                            telArray(telMax, 1) = debtorID
                            telArray(telMax, 2) = telNo
                            telArray(telMax, 3) = "O"
                            phoneReported = True
                            outfile &= telNo & "|" & phoneType & "|"
                        End If
                    End If

                End If
            Next
            If Not phoneReported Then
                outfile &= "|" & "|"
            End If

            'balance and emp status
            outfile &= debtrow(12) & "|" & "|"

            outfile &= vbNewLine
            rowCount += 1
        Next

        'now get all phone number changes
        Dim tel2_dt As New DataTable

        LoadDataTable2("StudentLoans", "select DebtorID, add_phone,add_fax,empphone,empfax" & _
                       " from rpt.UUBusinessRetailDebtor " & _
                       " where Builddate >= '" & Format(startdate, "yyyy-MM-dd") & "'" & _
                       " and Builddate < '" & Format(endDate, "yyyy-MM-dd") & "'", tel2_dt, False)
        For Each tel2Row In tel2_dt.Rows
            Dim debtorID As Integer = tel2Row(0)

            Dim Phone1 As String = ""
            Try
                Phone1 = tel2Row(1)
            Catch ex As Exception

            End Try

            Dim Phone2 As String = ""
            Try
                Phone2 = tel2Row(2)
            Catch ex As Exception

            End Try
            Dim Phone3 As String = ""
            Try
                Phone3 = tel2Row(3)
            Catch ex As Exception

            End Try
            Dim Phone4 As String = ""
            Try
                Phone4 = tel2Row(4)
            Catch ex As Exception

            End Try

            Dim phoneType As String
            If Phone1.Length > 4 Then
                'check if already reported on
                If Not check_tel(debtorID, Phone1) Then
                    'get phone type to report if available
                    If Microsoft.VisualBasic.Left(Phone1, 2) = "07" Then
                        phoneType = "M"
                    Else
                        phoneType = "H"
                    End If
                    If check_tel_type(debtorID, phoneType) = False Then
                        'OK can use 
                        telMax += 1
                        telArray(telMax, 1) = debtorID
                        telArray(telMax, 2) = Phone1
                        telArray(telMax, 3) = phoneType
                        write_out_tel_change(debtorID, Phone1, phoneType)
                    Else
                        'see if other is availble
                        If check_tel_type(debtorID, "O") = False Then
                            'OK can use 
                            telMax += 1
                            telArray(telMax, 1) = debtorID
                            telArray(telMax, 2) = Phone1
                            telArray(telMax, 3) = "O"
                            write_out_tel_change(debtorID, Phone1, "O")
                        End If
                    End If
                End If
            End If
            If Phone2.Length > 4 Then
                'check if already reported on
                If Not check_tel(debtorID, Phone2) Then
                    'get phone type to report if available
                    If Microsoft.VisualBasic.Left(Phone2, 2) = "07" Then
                        phoneType = "M"
                    Else
                        phoneType = "H"
                    End If
                    If check_tel_type(debtorID, phoneType) = False Then
                        'OK can use 
                        telMax += 1
                        telArray(telMax, 1) = debtorID
                        telArray(telMax, 2) = Phone2
                        telArray(telMax, 3) = phoneType
                        write_out_tel_change(debtorID, Phone2, phoneType)
                    Else
                        'see if other is availble
                        If check_tel_type(debtorID, "O") = False Then
                            'OK can use 
                            telMax += 1
                            telArray(telMax, 1) = debtorID
                            telArray(telMax, 2) = Phone2
                            telArray(telMax, 3) = "O"
                            write_out_tel_change(debtorID, Phone2, "O")
                        End If
                    End If
                End If
            End If
            If Phone3.Length > 4 Then
                'check if already reported on
                If Not check_tel(debtorID, Phone3) Then
                    'get phone type to report if available
                    If Microsoft.VisualBasic.Left(Phone3, 2) = "07" Then
                        phoneType = "M"
                    Else
                        phoneType = "W"
                    End If
                    If check_tel_type(debtorID, phoneType) = False Then
                        'OK can use 
                        telMax += 1
                        telArray(telMax, 1) = debtorID
                        telArray(telMax, 2) = Phone3
                        telArray(telMax, 3) = phoneType
                        write_out_tel_change(debtorID, Phone3, phoneType)
                    Else
                        'see if other is availble
                        If check_tel_type(debtorID, "O") = False Then
                            'OK can use 
                            telMax += 1
                            telArray(telMax, 1) = debtorID
                            telArray(telMax, 2) = Phone3
                            telArray(telMax, 3) = "O"
                            write_out_tel_change(debtorID, Phone3, "O")
                        End If
                    End If
                End If
            End If
            If Phone4.Length > 4 Then
                'check if already reported on
                If Not check_tel(debtorID, Phone4) Then
                    'get phone type to report if available
                    If Microsoft.VisualBasic.Left(Phone2, 2) = "07" Then
                        phoneType = "M"
                    Else
                        phoneType = "W"
                    End If
                    If check_tel_type(debtorID, phoneType) = False Then
                        'OK can use 
                        telMax += 1
                        telArray(telMax, 1) = debtorID
                        telArray(telMax, 2) = Phone4
                        telArray(telMax, 3) = phoneType
                        write_out_tel_change(debtorID, Phone4, phoneType)
                    Else
                        'see if other is availble
                        If check_tel_type(debtorID, "O") = False Then
                            'OK can use 
                            telMax += 1
                            telArray(telMax, 1) = debtorID
                            telArray(telMax, 2) = Phone4
                            telArray(telMax, 3) = "O"
                            write_out_tel_change(debtorID, Phone4, "O")
                        End If
                    End If
                End If
            End If
        Next
        outfile = Format(Now, "dd/MM/yyyy") & "|" & Format(rowCount, "#") & vbNewLine & outfile
        'im filename As String = "C:\AATemp\Rossendales_aa_Update_" & Format(Now, "yyyyMMddHHmmss") & ".csv"
        Dim filename As String = "\\ross-helm-fp001\Rossendales Shared\UnitedUtilitiesBR Reports\Rossendales_aa_Update_" & Format(Now, "yyyyMMddHHmmss") & ".csv"
        My.Computer.FileSystem.WriteAllText(filename, outfile, False, ascii)
        Me.Close()
    End Sub
    Private Function check_tel(ByVal debtorID As Integer, ByVal telno As String) As Boolean
        'see if already reported on
        Dim telFound As Boolean = False
        For telIDX = 1 To telMax
            If telArray(telIDX, 1) = debtorID And
                telArray(telIDX, 2) = telNo Then
                telFound = True
                Exit For
            End If
        Next

        Return (telFound)
    End Function
    Private Function check_tel_type(ByVal debtorID As Integer, ByVal phonetype As String) As String
        'see if phone type available to report on
        Dim phoneTypeFound As Boolean = False
        For telIDX = 1 To telMax
            If telArray(telIDX, 1) = debtorID And
               telArray(telIDX, 3) = phonetype Then
                phoneTypeFound = True
            End If
        Next
        Return (phoneTypeFound)
    End Function
    Private Sub write_out_tel_change(ByVal debtorID As Integer, ByVal telNo As String, ByVal phonetype As String)
        Dim debt_dt As New DataTable
        LoadDataTable2("DebtRecovery", "select _rowid, client_ref,name_title, name_fore, name_sur,debt_balance " &
                       " from Debtor" & _
                       " where _rowID = " & debtorID, debt_dt, False)
        For Each debtRow In debt_dt.Rows
            Dim clientRef As String = Microsoft.VisualBasic.Left(debtRow(1), 13)
            Dim fullName As String = ""
            Try
                fullName = debtRow(2) & " "
            Catch ex As Exception

            End Try
            Try
                fullName &= debtRow(3) & " "
            Catch ex As Exception

            End Try
            Try
                fullName &= debtRow(4)
            Catch ex As Exception

            End Try
            outfile &= clientRef & "|" & fullName & "|"
            'all columns blank until phone 
            outfile &= "||||||||||||||||||"
            
            outfile &= telNo & "|" & phonetype & "|"
            'balance and emp status
            outfile &= Format(debtRow(5), "##.##") & "|" & "|" & vbNewLine
            rowCount += 1
        Next
    End Sub
End Class

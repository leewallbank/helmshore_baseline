
Imports System.Xml.Schema

Public Class Form1
    Dim file_date As Date
    Dim xml_valid As Boolean = True
    Dim new_file As String = ""
    'Dim outcome_file As String
    Dim input_file, input_file2 As String
    Dim write_audit As Boolean = False
    Dim audit_file, no_changes_file As String
    Dim record As String = ""
    Dim filename, filename_id, header_id As String
    Dim date_error_file As String = ""
    Dim first_error As Boolean = True
    Dim first_change As Boolean = True
    Dim on_onestep, equity_check As Boolean
    Dim contrib_id As Double
    Dim no_changes As Integer = 0
    Dim changes As Integer = 0
    Dim spaces As Integer = 250
    Dim test_date As Date
    Dim applicant_id, summons_no, maat_id, first_name, surname, dob, invalid_dob, ni_no, outcome, outcome2, appeal_type, rep_status As String
    Dim mthly_contrib_amt_str, upfront_contrib_amt_str, income_contrib_cap_str, income_uplift_applied As String
    Dim case_type, in_court_custody, debt_addr1, debt_addr2, debt_addr3, debt_addr4, debt_postcode As String
    Dim postal_addr1, postal_addr2, postal_addr3, postal_addr4, postal_postcode, landline, mobile, email As String
    Dim equity_amt, appl_equity_amt, partner_equity_amt, cap_amt, asset_no, inc_ev_no As Integer
    Dim ci_code, ci_desc, case_type_code, rep_status_desc As String
    Dim has_partner, partner_first_name, partner_last_name, partner_nino, partner_emp_status, partner_emp_code As String
    Dim contrary_interest, disability_declaration, disability_description, nfa, si, hardship_result As String
    Dim assessment_reason, assessment_code, bedroom_count, undeclared_property, no_capital_declared, imprisoned, sentence_date_str As String
    Dim declared_value, declared_mortgage, verified_mortgage, verified_market_value As Decimal
    Dim all_evidence_date, uplift_applied_date, uplift_removed_date, sentence_date As Date
    Dim effective_date, rep_status_date, committal_date, hardship_review_date, assessment_date, partner_dob As Date
    Dim valid_recs As Integer = 0
    Dim rej_recs As Integer = 0
    Dim no_of_errors As Integer = 0
    Dim percent_owned_partner, percent_owned_appl As Integer
    Dim asset_table(100, 5)
    Dim inc_evidence_table(30, 4) As String
    Dim rep_order_withdrawal_date As Date
    Dim future_effective_date As Boolean
    Dim earlier_contrib_found As Boolean
    Dim mthly_contrib_amt, upfront_contrib_amt, income_contrib_cap As Decimal
    Dim comments2 As String
    Dim equity_amt_verified, cap_asset_type, cap_amt_verified, comments, residential_code, residential_desc As String
    Dim allowable_cap_threshold, effective_date_str, pref_pay_method, pref_pay_code, pref_pay_day, prop_type_code, prop_type_desc As String
    Dim bank_acc_name, bank_acc_no, bank_sort_code, emp_status, emp_code, rep_order_withdrawal_date_str, hardship_appl_recvd As String

    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click
        conn_open = False
        readbtn.Enabled = False
        exitbtn.Enabled = False
        Try
            With OpenFileDialog1
                .Title = "Read XML file"
                .Filter = "XML file|*.xml"
                .FileName = ""
                .CheckFileExists = True
            End With
            Dim xml_dataSet As New DataSet()
            Dim doc As New Xml.XmlDocument()

            Dim contrib_file As String = "Applicant ID" & "|" & "SummonsNum" & "|" & "MAAT ID" & _
                        "|" & "First name" & "|" & "Surname" & "|" & "Date of birth" & "|" & "NI No" & _
                        "|" & "Monthly Amount" & "|" & "Upfront Amt" & "|" & "Income contribution cap" & "|" & _
                        "Debt addr1" & "|" & "Debt addr2" & "|" & "Debt addr3" & "|" & "Debt addr4" & _
                        "|" & "Debt postcode" & "|" & "Curr addr1" & "|" & "Curr addr2" & _
                        "|" & "Curr addr3" & "|" & "Curr addr4" & "|" & "Curr postcode" & "|" & _
                        "Landline" & "|" & "Mobile" & "|" & "Email" & "|" & _
                         "Effective date" & "|" & "Comments" & vbNewLine
            Dim appeal_file As String = contrib_file
            Dim equity_file As String = contrib_file
            no_changes_file = "Maat ID" & vbNewLine
            date_error_file = "DebtorID|Date Field|Invalid date" & vbNewLine
            input_file = contrib_file
            input_file2 = contrib_file

            'MsgBox("Using test client")

            Dim idx As Integer = 0

            If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                validate_xml_file()
                If xml_valid = False Then
                    MsgBox("XML file has failed validation - see error file")
                    If MsgBox("Do you want to continue processing the file?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                        Me.Close()
                        Exit Sub
                    End If
                End If
                Try
                    filename = OpenFileDialog1.FileName
                    Dim ln2 As Integer = Microsoft.VisualBasic.Len(filename)

                    reset_fields()
                    Dim rdr_name As String = ""
                    Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
                    reader.Read()
                    ProgressBar1.Value = 5
                    Dim record_count As Integer = 0
                    While (reader.ReadState <> Xml.ReadState.EndOfFile)
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        Try
                            rdr_name = reader.Name
                        Catch
                            Continue While
                        End Try

                        'Note ReadElementContentAsString moves focus to next element so don't need read
                        ProgressBar1.Value = record_count
                        Application.DoEvents()
                        Select Case rdr_name
                            Case "header"
                                header_id = reader.Item(0)
                              
                                reader.Read()
                            Case "filename"
                                filename_id = reader.ReadElementContentAsString
                                Dim date_str As String = Mid(filename_id, 15, 8)
                                file_date = CDate(Microsoft.VisualBasic.Left(date_str, 4) & "," &
                                    Mid(date_str, 5, 2) & "," & Mid(date_str, 7, 2))

                            Case "CONTRIBUTIONS"
                                    record_count += 1
                                    If record_count > 100 Then
                                        record_count = 0
                                    End If
                                    If idx > 0 Then
                                        end_of_record()
                                        Try
                                            input_file = input_file & record & vbNewLine
                                        Catch ex As Exception
                                            input_file2 = input_file
                                            input_file = record & vbNewLine
                                        End Try

                                        If validate_record() = False Then
                                            rej_recs += 1
                                            audit_file = audit_file & maat_id & "|rejected" & vbNewLine
                                        Else
                                            valid_recs += 1
                                            If on_onestep = False Then
                                                If outcome = "APPEAL" Or _
                                                UCase(Microsoft.VisualBasic.Left(case_type, 6)) = "APPEAL" Then
                                                    audit_file = audit_file & maat_id & "|appeal" & vbNewLine
                                                    appeal_file = appeal_file & record & vbNewLine
                                                ElseIf equity_check = True Then
                                                    audit_file = audit_file & maat_id & "|equity" & vbNewLine
                                                    equity_file = equity_file & record & vbNewLine
                                                Else
                                                    audit_file = audit_file & maat_id & "|contrib" & vbNewLine
                                                    contrib_file = contrib_file & record & vbNewLine
                                                End If
                                            End If
                                        End If
                                        reset_fields()
                                    End If
                                    idx += 1
                                    contrib_id = reader.Item(0)
                                    reader.Read()
                            Case "maat_id"
                                    maat_id = reader.ReadElementContentAsString
                                    asset_no = 0
                                    inc_ev_no = 0
                                    'get last contrib id
                               
                            Case "applicant"
                                    applicant_id = reader.Item(0)
                                    reader.Read()
                            Case "firstName"
                                    first_name = reader.ReadElementContentAsString
                            Case "lastName"
                                    surname = reader.ReadElementContentAsString
                            Case "dob"
                                dob = reader.ReadElementContentAsString
                                Try
                                    Dim dob_date As Date = dob
                                    dob = Format(dob_date, "dd-MM-yyyy")
                                Catch ex As Exception
                                    invalid_dob = dob
                                    dob = Nothing
                                End Try
                            Case "ni_number"
                                    ni_no = reader.ReadElementContentAsString
                            Case "landline"
                                    landline = reader.ReadElementContentAsString
                            Case "mobile"
                                    mobile = reader.ReadElementContentAsString
                            Case "email"
                                    email = reader.ReadElementContentAsString
                            Case "noFixedAbode"
                                    nfa = reader.ReadElementContentAsString
                                    comments = comments & "No Fixed Abode:" & nfa & ";"
                                    space_comments()
                            Case "specialInvestigation"
                                    si = reader.ReadElementContentAsString
                                    comments = comments & "Special Investigation:" & si & ";"
                                    space_comments()
                            Case "homeAddress"
                                    reader.Read()
                                    While reader.Name <> "homeAddress"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "line1"
                                                debt_addr1 = reader.ReadElementContentAsString
                                            Case "line2"
                                                debt_addr2 = reader.ReadElementContentAsString
                                            Case "line3"
                                                debt_addr3 = reader.ReadElementContentAsString
                                            Case "city"
                                                debt_addr4 = reader.ReadElementContentAsString
                                            Case "postcode"
                                                debt_postcode = reader.ReadElementContentAsString
                                            Case "country"
                                                Dim country As String = reader.ReadElementContentAsString
                                                If country <> "GB" Then
                                                    debt_addr4 = debt_addr4 & " " & Trim(country)
                                                End If
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "postalAddress"
                                    reader.Read()
                                    While reader.Name <> "postalAddress"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "line1"
                                                postal_addr1 = reader.ReadElementContentAsString
                                            Case "line2"
                                                postal_addr2 = reader.ReadElementContentAsString
                                            Case "line3"
                                                postal_addr3 = reader.ReadElementContentAsString
                                            Case "city"
                                                postal_addr4 = reader.ReadElementContentAsString
                                            Case "postcode"
                                                postal_postcode = reader.ReadElementContentAsString
                                            Case "country"
                                                Dim country As String = reader.ReadElementContentAsString
                                                If country <> "GB" Then
                                                    postal_addr4 = postal_addr4 & " " & Trim(country)
                                                End If
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "employmentStatus"
                                    reader.Read()
                                    While reader.Name <> "employmentStatus"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "description"
                                                emp_status = reader.ReadElementContentAsString
                                                comments = comments & "Emp status:" & emp_status & ";"
                                                space_comments()
                                            Case "code"
                                                emp_code = reader.ReadElementContentAsString
                                                comments = comments & "Emp Code:" & emp_code & ";"
                                                space_comments()
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "partner"
                                    reader.Read()
                                    While reader.Name <> "partner"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "hasPartner"
                                                has_partner = reader.ReadElementContentAsString
                                                comments = comments & "Has Partner:" & has_partner & ";"
                                                space_comments()
                                            Case "contraryInterest"
                                                contrary_interest = reader.ReadElementContentAsString
                                                comments = comments & "Contrary Interest:" & contrary_interest & ";"
                                                space_comments()
                                            Case "ciDetails"
                                                reader.Read()
                                                While reader.Name <> "ciDetails"
                                                    rdr_name = reader.Name
                                                    If reader.NodeType > 1 Then
                                                        reader.Read()
                                                        Continue While
                                                    End If
                                                    Select Case rdr_name
                                                        Case "code"
                                                            ci_code = reader.ReadElementContentAsString
                                                            comments = comments & "Contrary Interest Code:" & ci_code & ";"
                                                            space_comments()
                                                        Case "description"
                                                            ci_desc = reader.ReadElementContentAsString
                                                            comments = comments & "Contrary Interest Description:" & ci_desc & ";"
                                                            space_comments()
                                                        Case Else
                                                            reader.Read()
                                                    End Select
                                                End While
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "partnerDetails"
                                    reader.Read()
                                    While reader.Name <> "partnerDetails"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "firstName"
                                                partner_first_name = reader.ReadElementContentAsString
                                                comments = comments & "Partner First Name:" & partner_first_name & ";"
                                                space_comments()
                                            Case "lastName"
                                                partner_last_name = reader.ReadElementContentAsString
                                                comments = comments & "Partner Last Name:" & partner_last_name & ";"
                                                space_comments()
                                            Case "dob"
                                                partner_dob = reader.ReadElementContentAsString
                                                comments = comments & "Partner DOB:" & Format(partner_dob, "dd/MM/yyyy") & ";"
                                                space_comments()
                                            Case "niNumber"
                                                partner_nino = reader.ReadElementContentAsString
                                                comments = comments & "Partner NINO:" & partner_nino & ";"
                                                space_comments()
                                            Case "employmentStatus"
                                                reader.Read()
                                                While reader.Name <> "employmentStatus"
                                                    rdr_name = reader.Name
                                                    If reader.NodeType > 1 Then
                                                        reader.Read()
                                                        Continue While
                                                    End If
                                                    Select Case rdr_name
                                                        Case "description"
                                                            partner_emp_status = reader.ReadElementContentAsString
                                                        comments = comments & "Emp Status Partner:" & partner_emp_status & ";"
                                                            space_comments()
                                                        Case "code"
                                                            partner_emp_code = reader.ReadElementContentAsString
                                                        comments = comments & "Emp Code Partner:" & partner_emp_code & ";"
                                                            space_comments()
                                                        Case Else
                                                            reader.Read()
                                                    End Select
                                                End While
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "disabilitySummary"
                                    reader.Read()
                                    While reader.Name <> "disabilitySummary"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "declaration"
                                                disability_declaration = reader.ReadElementContentAsString
                                                comments = comments & "Disability Declaration:" & disability_declaration & ";"
                                                space_comments()
                                            Case "disability"
                                                reader.Read()
                                                While reader.Name <> "disability"
                                                    rdr_name = reader.Name
                                                    If reader.NodeType > 1 Then
                                                        reader.Read()
                                                        Continue While
                                                    End If
                                                    Select Case rdr_name
                                                        Case "description"
                                                            disability_description = reader.ReadElementContentAsString
                                                            comments = comments & "Disability Description:" & disability_description & ";"
                                                            space_comments()
                                                        Case Else
                                                            reader.Read()
                                                    End Select
                                                End While
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "caseType"
                                    reader.Read()
                                    While reader.Name <> "caseType"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "description"
                                                case_type = Trim(reader.ReadElementContentAsString)
                                                comments = comments & "Case type:" & case_type & ";"
                                                space_comments()
                                            Case "code"
                                                case_type_code = reader.ReadElementContentAsString
                                                comments = comments & "Case Type Code:" & case_type_code & ";"
                                                space_comments()
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "repStatus"
                                    reader.Read()
                                    While reader.Name <> "repStatus"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "status"
                                                rep_status = reader.ReadElementContentAsString
                                                comments = comments & "Rep Status:" & rep_status & ";"
                                                space_comments()
                                            Case "description"
                                                rep_status_desc = reader.ReadElementContentAsString
                                                comments = comments & "Rep Status Description:" & rep_status_desc & ";"
                                                space_comments()
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "repStatusDate"
                                    rep_status_date = reader.ReadElementContentAsString
                                    comments = comments & "Rep Status Date:" & Format(rep_status_date, "dd/MM/yyyy") & ";"
                                    space_comments()
                            Case "arrestSummonsNumber"
                                    summons_no = reader.ReadElementContentAsString
                            Case "inCourtCustody"
                                    in_court_custody = reader.ReadElementContentAsString
                                    If in_court_custody.Length > 0 Then
                                        If LCase(Microsoft.VisualBasic.Left(in_court_custody, 1)) = "n" Then
                                            in_court_custody = "No"
                                        End If
                                        If LCase(Microsoft.VisualBasic.Left(in_court_custody, 1)) = "y" Then
                                            in_court_custody = "Yes"
                                        End If
                                        comments = comments & "In Court Custody:" & in_court_custody & ";"
                                        space_comments()
                                    End If
                            Case "imprisoned"
                                    imprisoned = reader.ReadElementContentAsString
                                    If imprisoned.Length > 0 Then
                                        If LCase(Microsoft.VisualBasic.Left(imprisoned, 1)) = "n" Then
                                            imprisoned = "No"
                                        End If
                                        If LCase(Microsoft.VisualBasic.Left(imprisoned, 1)) = "y" Then
                                            imprisoned = "Yes"
                                        End If
                                        comments = comments & "InPrison:" & imprisoned & ";"
                                        space_comments()
                                    End If
                            Case "sentenceDate"
                                    sentence_date_str = reader.ReadElementContentAsString
                                    Try
                                        sentence_date = CDate(sentence_date_str)
                                        comments = comments & "SentenceOrderDate:" & Format(sentence_date, "dd/MM/yyyy") & ";"
                                        space_comments()
                                    Catch ex As Exception
                                        sentence_date = Nothing
                                    End Try
                            Case "committalDate"
                                    committal_date = reader.ReadElementContentAsString
                                    comments = comments & "Committal Date:" & Format(committal_date, "dd/MM/yyyy") & ";"
                                    space_comments()
                            Case "ccHardship"
                                    reader.Read()
                                    While reader.Name <> "ccHardship"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "reviewDate"
                                                hardship_review_date = reader.ReadElementContentAsString
                                                comments = comments & "Hardship Review Date:" & Format(hardship_review_date, "dd/MM/yyyy") & ";"
                                                space_comments()
                                            Case "reviewResult"
                                                hardship_result = reader.ReadElementContentAsString
                                                comments = comments & "Hardship Review Result:" & hardship_result & ";"
                                                space_comments()
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "effectiveDate"
                                    effective_date_str = reader.ReadElementContentAsString
                                    Try
                                        effective_date = effective_date_str
                                    Catch ex As Exception
                                        effective_date = Nothing
                                    End Try
                                    'if effective date in the future - add to notes 
                                    If effective_date <> Nothing Then
                                        If effective_date > Now Then
                                            future_effective_date = True
                                            comments = comments & "Effective Date:" & Format(effective_date, "dd/MM/yyyy") & ";"
                                            space_comments()
                                        End If
                                    End If
                            Case "monthlyContribution"
                                    mthly_contrib_amt_str = reader.ReadElementContentAsString
                                    mthly_contrib_amt = mthly_contrib_amt_str      ' pounds not pence/ 100
                            Case "upfrontContribution"
                                    upfront_contrib_amt_str = reader.ReadElementContentAsString
                                    upfront_contrib_amt = upfront_contrib_amt_str     'pounds not pence/ 100
                            Case "incomeContributionCap"
                                    income_contrib_cap_str = reader.ReadElementContentAsString
                                    income_contrib_cap = income_contrib_cap_str      'pounds not pence/ 100
                            Case "assessmentReason"
                                    reader.Read()
                                    While reader.Name <> "assessmentReason"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "description"
                                                assessment_reason = reader.ReadElementContentAsString
                                                comments = comments & "Assessment Reason:" & assessment_reason & ";"
                                                space_comments()
                                            Case "code"
                                                assessment_code = reader.ReadElementContentAsString
                                                comments = comments & "Assessment Code:" & assessment_code & ";"
                                                space_comments()
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "assessmentDate"
                                    assessment_date = reader.ReadElementContentAsString
                                    comments = comments & "Assessment Date:" & Format(assessment_date, "dd/MM/yyyy") & ";"
                                    space_comments()
                            Case "bedRoomCount"
                                    bedroom_count = reader.ReadElementContentAsString
                                    comments = comments & "Bedroom Count:" & bedroom_count & ";"
                                    space_comments()
                            Case "undeclaredProperty"
                                    undeclared_property = reader.ReadElementContentAsString
                                    comments = comments & "Undeclared Property:" & undeclared_property & ";"
                                    space_comments()
                            Case "residentialStatus"
                                    reader.Read()
                                    While reader.Name <> "residentialStatus"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "code"
                                                residential_code = reader.ReadElementContentAsString
                                                comments = comments & "Residential Code:" & Trim(residential_code) & ";"
                                                space_comments()
                                            Case "description"
                                                residential_desc = reader.ReadElementContentAsString
                                                comments = comments & "Residential Description:" & Trim(residential_desc) & ";"
                                                space_comments()
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "propertyType"
                                    reader.Read()
                                    While reader.Name <> "propertyType"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "code"
                                                prop_type_code = reader.ReadElementContentAsString
                                                comments = comments & "Property Type Code:" & Trim(prop_type_code) & ";"
                                                space_comments()
                                            Case "description"
                                                prop_type_desc = reader.ReadElementContentAsString
                                                comments = comments & "Property Type Description:" & Trim(prop_type_desc) & ";"
                                                space_comments()
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "percentageApplicantOwned"
                                    percent_owned_appl = reader.ReadElementContentAsString
                                    comments = comments & "Percent Owned Applicant:" & percent_owned_appl & ";"
                                    space_comments()
                            Case "percentagePartnerOwned"
                                    percent_owned_partner = reader.ReadElementContentAsString
                                    comments = comments & "Percent Owned Partner:" & percent_owned_partner & ";"
                                    space_comments()
                            Case "applicantEquityAmount"
                                    appl_equity_amt = reader.ReadElementContentAsString
                                    comments = comments & "Applicant Equity Amount:" & appl_equity_amt & ";"
                                    space_comments()
                            Case "partnerEquityAmount"
                                    partner_equity_amt = reader.ReadElementContentAsString
                                    comments = comments & "Partner Equity Amount:" & partner_equity_amt & ";"
                                    space_comments()
                            Case "declaredMortgage"
                                    declared_mortgage = reader.ReadElementContentAsString
                                    comments = comments & "Declared Mortgage:" & declared_mortgage & ";"
                                    space_comments()
                            Case "declaredValue"
                                    declared_value = reader.ReadElementContentAsString
                                    comments = comments & "Declared Value:" & declared_value & ";"
                                    space_comments()
                            Case "verifiedMortgage"
                                    verified_mortgage = reader.ReadElementContentAsString
                                    comments = comments & "Verified Mortgage:" & verified_mortgage & ";"
                                    space_comments()
                            Case "verifiedValue"
                                    verified_market_value = reader.ReadElementContentAsString
                                    comments = comments & "Verified Market Value:" & verified_market_value & ";"
                                    space_comments()
                                    'comments = comments & "Equity amt:" & equity_amt & ";"
                                    'space_comments()
                            Case "equityVerified"
                                    equity_amt_verified = LCase(reader.ReadElementContentAsString)
                                    If Microsoft.VisualBasic.Left(equity_amt_verified, 1) = "n" Then
                                        equity_amt_verified = "No"
                                    End If
                                    If Microsoft.VisualBasic.Left(equity_amt_verified, 1) = "y" Then
                                        equity_amt_verified = "Yes"
                                    End If
                                    comments = comments & "Equity Amt Verified:" & equity_amt_verified & ";"
                                    space_comments()
                            Case "capitalSummary"
                                    reader.Read()
                                    While reader.Name <> "capitalSummary"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "noCapitalDeclared"
                                                no_capital_declared = reader.ReadElementContentAsString
                                                comments = comments & "No Capital Declared:" & no_capital_declared & ";"
                                                space_comments()
                                            Case "additionalProperties"
                                                comments = comments & "Additional Properties:" & reader.ReadElementContentAsString & ";"
                                                space_comments()
                                            Case "undeclaredProperties"
                                                comments = comments & "Undeclared Properties:" & reader.ReadElementContentAsString & ";"
                                                space_comments()
                                            Case "totalCapitalAssets"
                                                comments = comments & "Total Capital Assets:" & reader.ReadElementContentAsString & ";"
                                                space_comments()
                                            Case "allEvidenceDate"
                                                all_evidence_date = reader.ReadElementContentAsString
                                                comments = comments & "All Evidence Date:" & Format(all_evidence_date, "dd/MM/yyyy") & ";"
                                                space_comments()
                                            Case "capAllowanceWithheld"
                                                comments = comments & "Cap Allowance Withheld:" & reader.ReadElementContentAsString & ";"
                                                space_comments()
                                            Case "capAllowanceRestore"
                                                comments = comments & "Cap Allowance Restore:" & reader.ReadElementContentAsString & ";"
                                                space_comments()
                                            Case "assetList"
                                                reader.Read()
                                                While reader.Name <> "assetList"
                                                    rdr_name = reader.Name
                                                    If reader.NodeType > 1 Then
                                                        reader.Read()
                                                        Continue While
                                                    End If
                                                    Select Case rdr_name
                                                        Case "description"
                                                            asset_no += 1
                                                            Dim temp As String = reader.ReadElementContentAsString
                                                            remove_chrs(temp)
                                                            Try
                                                                asset_table(asset_no, 1) = temp
                                                            Catch ex As Exception
                                                                MsgBox("asset table dimension requires increasing")
                                                                End
                                                            End Try
                                                            asset_table(asset_no, 2) = ""
                                                            asset_table(asset_no, 3) = ""
                                                            asset_table(asset_no, 4) = ""
                                                            asset_table(asset_no, 5) = ""
                                                        Case "amount"
                                                            asset_table(asset_no, 2) = reader.ReadElementContentAsString
                                                        Case "verified"
                                                            cap_amt_verified = reader.ReadElementContentAsString
                                                            remove_chrs(cap_amt_verified)
                                                            asset_table(asset_no, 3) = cap_amt_verified
                                                        Case "dateVerified"
                                                            test_date = reader.ReadElementContentAsString
                                                            asset_table(asset_no, 4) = Format(test_date, "dd/MM/yyyy")
                                                        Case "evidenceReceivedDate"
                                                            test_date = reader.ReadElementContentAsString
                                                            asset_table(asset_no, 5) = Format(test_date, "dd/MM/yyyy")
                                                        Case Else
                                                            reader.Read()
                                                    End Select
                                                End While
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "repOrderWithdrawalDate"
                                    rep_order_withdrawal_date_str = reader.ReadElementContentAsString
                                    Try
                                        rep_order_withdrawal_date = rep_order_withdrawal_date_str
                                    Catch ex As Exception
                                        rep_order_withdrawal_date_str = ""
                                    End Try
                                    If IsDate(rep_order_withdrawal_date) Then
                                        comments = comments & "Rep-Order-Withdrawn-Date:" & _
                                        Format(rep_order_withdrawal_date, "dd/MM/yyyy") & ";"
                                        space_comments()
                                    End If
                            Case "incomeEvidenceList"
                                    reader.Read()
                                    Dim new_comment As String = ""
                                    While reader.Name <> "incomeEvidenceList"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            If reader.Name = "incomeEvidence" And reader.NodeType = Xml.XmlNodeType.EndElement Then
                                                If new_comment.Length <= 250 Then
                                                    comments = comments & new_comment
                                                    space_comments()
                                                Else
                                                    get_comment_break(new_comment)
                                                End If
                                                new_comment = ""
                                            End If
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "evidence"
                                                inc_ev_no += 1
                                                inc_evidence_table(inc_ev_no, 1) = reader.ReadElementContentAsString
                                                inc_evidence_table(inc_ev_no, 2) = ""
                                                inc_evidence_table(inc_ev_no, 3) = ""
                                                inc_evidence_table(inc_ev_no, 4) = ""
                                                new_comment = new_comment & "Inc Evidence:" & inc_evidence_table(inc_ev_no, 1) & ";"
                                            Case "mandatory"
                                                inc_evidence_table(inc_ev_no, 2) = reader.ReadElementContentAsString
                                                new_comment = new_comment & "Inc Evidence Mandatory:" & inc_evidence_table(inc_ev_no, 2) & ";"
                                            Case "otherText"
                                                Try
                                                    inc_evidence_table(inc_ev_no, 3) = reader.ReadElementContentAsString
                                                    remove_chrs(inc_evidence_table(inc_ev_no, 3))
                                                    new_comment = new_comment & "Inc Evidence Other Text:" & inc_evidence_table(inc_ev_no, 3) & ";"
                                                Catch ex As Exception
                                                    MsgBox(ex.Message)
                                                End Try
                                            Case "dateReceived"
                                                test_date = reader.ReadElementContentAsString
                                                inc_evidence_table(inc_ev_no, 4) = Format(test_date, "dd/MM/yyyy")
                                                new_comment = new_comment & "Inc Evidence Date Received:" & Format(test_date, "dd/MM/yyyy") & ";"
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "incomeUpliftApplied"
                                    income_uplift_applied = reader.ReadElementContentAsString
                                    comments = comments & "Inc uplift applied:" & income_uplift_applied & ";"
                                    space_comments()
                            Case "upliftAppliedDate"
                                    uplift_applied_date = reader.ReadElementContentAsString
                                    comments = comments & "uplift applied date:" & Format(uplift_applied_date, "dd/MM/yyyy") & ";"
                                    space_comments()
                            Case "upliftRemovedDate"
                                    uplift_removed_date = reader.ReadElementContentAsString
                                    comments = comments & "uplift removed date:" & Format(uplift_removed_date, "dd/MM/yyyy") & ";"
                                    space_comments()
                            Case "ccOutcomes"
                                    reader.Read()
                                    While reader.Name <> "ccOutcomes" And reader.Name <> "CONTRIBUTIONS"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            If reader.Name = "ccOutcome" And reader.NodeType = Xml.XmlNodeType.EndElement Then
                                                space_comments()
                                            End If
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "code"
                                                If outcome = "" Then
                                                    outcome = reader.ReadElementContentAsString
                                                    comments = comments & "OUTCOME:" & outcome & ";"
                                                Else
                                                    outcome2 = reader.ReadElementContentAsString
                                                    comments = comments & "OUTCOME:" & outcome2 & ";"
                                                End If
                                            Case "date"
                                                Try
                                                    test_date = reader.ReadElementContentAsString
                                                Catch ex As Exception
                                                    reader.Read()
                                                    Continue While
                                                End Try
                                                comments = comments & "Outcome Date:" & Format(test_date, "dd/MM/yyyy") & ";"
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                                    'Case "FINAL_DEFENCE_COST"
                                    '    final_defence_cost = reader.ReadElementContentAsString

                                    'Case "ALLOWABLE_CAPITAL_THRESHOLD"
                                    '    allowable_cap_threshold = reader.ReadElementContentAsString
                                    '    remove_chrs(allowable_cap_threshold)

                            Case "preferredPaymentMethod"
                                    reader.Read()
                                    While reader.Name <> "preferredPaymentMethod"
                                        rdr_name = reader.Name
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        Select Case rdr_name
                                            Case "description"
                                                pref_pay_method = reader.ReadElementContentAsString
                                                comments = comments & "Preferred Payment Method:" & pref_pay_method & ";"
                                                space_comments()
                                            Case "code"
                                                pref_pay_code = reader.ReadElementContentAsString
                                                comments = comments & "Preferred Payment Code:" & pref_pay_code & ";"
                                                space_comments()
                                            Case Else
                                                reader.Read()
                                        End Select
                                    End While
                            Case "preferredPaymentDay"
                                    pref_pay_day = reader.ReadElementContentAsString
                                    comments = comments & "Preferred Payment Day:" & pref_pay_day & ";"
                                    space_comments()
                            Case "accountName"
                                    bank_acc_name = reader.ReadElementContentAsString
                                    If bank_acc_name.Length > 0 Then
                                        comments = comments & "Bank Acc Name:" & bank_acc_name & ";"
                                        space_comments()
                                    End If
                            Case "accountNo"
                                    bank_acc_no = reader.ReadElementContentAsString
                                    If bank_acc_no.Length > 0 Then
                                        comments = comments & "Bank Acc No:" & bank_acc_no & ";"
                                        space_comments()
                                    End If
                            Case "sortCode"
                                    bank_sort_code = reader.ReadElementContentAsString
                                    If bank_sort_code.Length > 0 Then
                                        comments = comments & "Bank Sort Code:" & bank_sort_code & ";"
                                        space_comments()
                                    End If

                            Case Else
                                    If maat_id.Length > 1 And _
                                    rdr_name <> "application" And _
                                    rdr_name <> "assessment" And _
                                    rdr_name <> "bankDetails" And _
                                    rdr_name <> "equity" Then
                                        MsgBox("what is this? " & rdr_name)
                                    End If

                                    reader.Read()
                        End Select
                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Else
                MsgBox("XML file not processed")
                Exit Sub
            End If
            'write file including last applicant
            end_of_record()
            input_file = input_file & record & vbNewLine
            If validate_record() = False Then
                rej_recs += 1
                audit_file = audit_file & maat_id & "|rejected" & vbNewLine
            Else
                valid_recs += 1
            End If
            Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
            
           
           
            'acknowledgement file includes all records not rejected
            Dim tot_valid_recs As Integer = valid_recs
           
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        ProgressBar1.Value = 100
        MsgBox("Cases with no changes = " & no_changes & vbNewLine & "Changes = " & changes)
        Me.Close()
    End Sub
    Private Sub space_comments()
        'Try
        '    'Dim len As Integer = comments.Length
        '    'comments = comments & Space(spaces - comments.Length)
        '    'spaces += 250
        'Catch ex As Exception
        '    MsgBox("Out of comment space")
        'End Try
        Dim spaces As Integer
        Try

            spaces = (Int((comments.Length / 250)) + 1) * 250
            comments = comments & Space(spaces - comments.Length)
        Catch ex As Exception
            MsgBox("error in spacing comments")
        End Try

    End Sub
    Private Function validate_record() As Boolean
        equity_check = False
        Dim orig_no_of_errors As Integer = no_of_errors
        If maat_id = "" Then ' D.J.Dicks 09.12.2011 Why is this in "Or maat_id <> 2968467"
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = Nothing
            error_table(no_of_errors).error_text = "invalid maat id"
        End If

        If sentence_date <> Nothing Then
            If sentence_date < CDate("2001,1,1") Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid sentence order date"
            End If
        End If

        If earlier_contrib_found = True Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "MAAT ID contrib ID is less than previous"
        End If

        If outcome <> "" Then
            If outcome <> "CONVICTED" And outcome <> "PART CONVICTED" And outcome <> "APPEAL" And outcome <> "ABANDONED" _
                    And outcome <> "AQUITTED" And outcome <> "DISMISSED" And outcome <> "DISMISSED" _
                    And outcome <> "PART SUCCESS" And outcome <> "UNSUCCESSFUL" And outcome <> "SUCCESSFUL" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid crown court outcome"
            End If
        End If

        If appeal_type <> "" Then
            If appeal_type <> "ASE" And appeal_type <> "ACV" And appeal_type <> "ACS" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid appeal type"
            End If
        End If

        If rep_status <> "" Then
            If rep_status <> "CURR" And rep_status <> "ERR" And rep_status <> "SUSP" _
            And rep_status <> "FI" And rep_status <> "NOT SENT FOR TRIAL" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid rep type"
            End If
        End If

        If on_onestep Then
            Return (True)
        End If


        If first_name = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid first name"
        End If

        If surname = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid surname"
        End If

        If mthly_contrib_amt_str = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid monthly contribution"
        ElseIf mthly_contrib_amt = 0 Then
            If outcome = "CONVICTED" Or outcome = "PART CONVICTED" Then
                equity_check = True
            ElseIf outcome <> "APPEAL" And UCase(Microsoft.VisualBasic.Left(case_type, 6)) <> "APPEAL" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid monthly contribution"
            End If
        End If

        If case_type = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid case type"
        End If

        If in_court_custody <> "Yes" And in_court_custody <> "No" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid in court custody"
        End If

        If emp_status = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid employment status"
        End If

        If effective_date = Nothing And future_effective_date = False Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid effective date"
        End If

        If income_contrib_cap_str = "" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).applicant_id = applicant_id
            error_table(no_of_errors).maat_id = maat_id
            error_table(no_of_errors).error_text = "invalid income contribution cap"
        End If

        'If income_uplift_applied <> "Yes" And income_uplift_applied <> "No" Then
        '    no_of_errors += 1
        '    error_table(no_of_errors).contrib_id = contrib_id
        '    error_table(no_of_errors).applicant_id = applicant_id
        '    error_table(no_of_errors).maat_id = maat_id
        '    error_table(no_of_errors).error_text = "invalid income uplift applied"
        'End If

        If equity_amt_verified <> "" Then
            If equity_amt_verified <> "Yes" And equity_amt_verified <> "No" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid equity amount verified"
            End If
        End If

        If cap_amt_verified <> "" Then
            If LCase(cap_amt_verified) <> "yes" And cap_amt_verified <> "no" Then
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = "invalid capital amount verified"
            End If
        End If

        

        If orig_no_of_errors = no_of_errors Then
            audit_file = audit_file & maat_id & "|no errors" & vbNewLine
            Return (True)
        Else
            equity_check = False
            audit_file = audit_file & maat_id & "|errors" & vbNewLine
            If no_of_errors = UBound(error_table) Then
                ReDim Preserve error_table(no_of_errors + 10)
            End If
            Return (False)
        End If

    End Function

    Private Sub Write_ack(ByVal writer As Xml.XmlWriter, ByVal filename_id As String, _
    ByVal filename_id_tag As String, ByVal valid_recs As Integer, ByVal rej_recs As Integer, _
    ByVal no_of_errors As Integer)
        writer.WriteStartElement("CONTRIBUTIONS_FILE_ACK")
        writer.WriteStartElement("FILENAME")
        writer.WriteAttributeString("ID", filename_id_tag)
        writer.WriteString(filename_id)
        writer.WriteEndElement()
        writer.WriteElementString("DATE_LOADED", Format(Now, "dd-MM-yyyy"))
        writer.WriteElementString("NO_OF_RECORDS_LOADED", valid_recs)
        writer.WriteElementString("NO_OF_RECORDS_REJECTED", rej_recs)

        Dim idx As Integer
        Dim contrib_id As Double = 0
        Dim last_contrib_id As Double = 0
        Dim applicant_id As Double = 0
        Dim maat_id As Double = 0
        Dim error_text As String = ""
        For idx = 1 To no_of_errors
            If idx = 1 Then
                writer.WriteStartElement("REJECTED_RECORD_LIST")
            End If
            contrib_id = error_table(idx).contrib_id
            If contrib_id <> last_contrib_id Then
                last_contrib_id = contrib_id
                writer.WriteStartElement("CONTRIBUTIONS")
                writer.WriteAttributeString("ID", contrib_id)
                applicant_id = error_table(idx).applicant_id
                maat_id = error_table(idx).maat_id
                writer.WriteElementString("APPLICANT_ID", applicant_id)
                writer.WriteElementString("MAAT_ID", maat_id)
            End If

            error_text = error_table(idx).error_text
            writer.WriteElementString("ERROR_TEXT", error_text)

            If idx = no_of_errors Then
                writer.WriteEndElement()
                writer.WriteEndElement()
            ElseIf contrib_id <> error_table(idx + 1).contrib_id Then
                writer.WriteEndElement()
            End If
        Next
        writer.WriteEndElement()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub end_of_record()

        Try
            If applicant_id = "" Then
                write_error("No applicant found")
                Exit Sub
            End If
            If maat_id = "" Then
                write_error("No maat found")
                Exit Sub
            End If

            If allowable_cap_threshold <> "" Then
                comments = comments & "Allowable cap threshold:" & allowable_cap_threshold & ";"
                space_comments()
            End If

            'If outcome <> "" Then
            '    comments = comments & "Outcome:" & outcome & ";"
            '    space_comments()
            'End If

            Dim idx3 As Integer
            Try
                For idx3 = 1 To asset_no
                    comments = comments & "Cap asset type:" & asset_table(idx3, 1) & ";"
                    comments = comments & "Cap amt:" & asset_table(idx3, 2) & ";"
                    comments = comments & "Cap amt verified:" & asset_table(idx3, 3) & ";"
                    If asset_table(idx3, 4) <> "" Then
                        comments = comments & "Cap amt Date Verified:" & asset_table(idx3, 4) & ";"
                    End If
                    If asset_table(idx3, 5) <> "" Then
                        comments = comments & "Cap amt Evidence Received Date:" & asset_table(idx3, 5) & ";"
                    End If
                    space_comments()
                Next
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try



            If landline = "" Then
                landline = mobile
                mobile = ""
            End If
            'use final costs if appeal type and mthly amt=0
            'If outcome = "APPEAL" Or UCase(Microsoft.VisualBasic.Left(case_type, 6)) = "APPEAL" _
            'And mthly_contrib_amt = 0 And upfront_contrib_amt = 0 Then
            '    upfront_contrib_amt = final_defence_cost
            '    comments = comments & "Final Defence Costs:" & Format(final_defence_cost, "F") & ";"
            '    space_comments()
            'End If
            record = applicant_id & "|" & summons_no & "|" & maat_id & "|" & first_name & "|" & _
                                        surname & "|" & dob & "|" & ni_no & "|" & mthly_contrib_amt & "|" & upfront_contrib_amt & "|" & _
                                        income_contrib_cap & "|" & debt_addr1 & "|" & debt_addr2 & "|" & debt_addr3 & "|" & _
                                        debt_addr4 & "|" & debt_postcode & "|" & postal_addr1 & "|" & postal_addr2 & "|" & _
                                        postal_addr3 & "|" & postal_addr4 & "|" & postal_postcode & "|" & landline & "|" & mobile & "|" & email & _
                                        "|"
            If future_effective_date Then
                record = record & ""
            Else
                record = record & Format(effective_date, "dd-MM-yyyy")
            End If
            'check if maat-id already exists on onestep


            param1 = "onestep"
            param2 = "select  clientschemeID, _rowid from Debtor" & _
            " where  client_ref = '" & maat_id & "'"
            Dim debtor1_dataset As DataSet = get_dataset(param1, param2)
            Dim debtor As Integer = 0
            If no_of_rows > 0 Then
                Dim idx As Integer
                Dim csid_no As Integer = no_of_rows - 1
                For idx = 0 To csid_no
                    Dim csid As Integer = debtor1_dataset.Tables(0).Rows(idx).Item(0)
                    param2 = "select clientID from ClientScheme where _rowid = " & csid
                    Dim csid_datatset As DataSet = get_dataset(param1, param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read clientscheme for csid = " & csid)
                        Exit Sub
                    End If

                    'use <> 24 for test client; 909 for production client
                    If csid_datatset.Tables(0).Rows(0).Item(0) <> 909 Then
                        Continue For
                    Else
                        debtor = debtor1_dataset.Tables(0).Rows(idx).Item(1)
                        Exit For
                    End If
                Next
            End If

            If no_of_rows = 0 Or debtor = 0 Then
                on_onestep = False
                audit_file = audit_file & maat_id & "|not on onestep" & vbNewLine
                Dim comments2 As String = ""
                Dim comments3 As String = comments
                'If comments.Length <= 250 Then
                record = record & "|" & comments
                'Else
                '    While comments3.Length > 250
                '        Dim len As Integer = Microsoft.VisualBasic.Len(comments3)
                '        Dim idx As Integer
                '        For idx = 250 To 1 Step -1
                '            If Mid(comments3, idx, 1) = ";" Then
                '                Exit For
                '            End If
                '        Next
                '        comments2 = comments2 & Microsoft.VisualBasic.Left(comments3, idx)
                '        Dim idx2 As Integer
                '        For idx2 = idx To 250
                '            comments2 = comments2 & " "
                '        Next
                '        comments3 = Microsoft.VisualBasic.Right(comments3, len - idx)
                '    End While
                'record = record & "|" & comments2 & comments3
                'end If
                Exit Sub
            End If

            'case exists on onestep so look for changes
            audit_file = audit_file & maat_id & "|on onestep" & vbNewLine
            Dim change_found As Boolean = False
            on_onestep = True
            param2 = "select _rowid, offence_number, offence_court, debt_amount, debt_costs, " & _
           " offenceValue, empNI, add_phone, add_fax, addEmail, prevReference, add_postcode, " & _
           " address, clientschemeID from Debtor" & _
           " where _rowid = " & debtor
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to read debtorID = " & debtor)
                Exit Sub
            End If
            Dim idx4 As Integer
            'get notes into dataset
            param2 = "select text, _createdDate from Note where debtorID = " & debtor & _
                            " and type = 'Client note' order by _rowid desc"
            Dim note_dataset As DataSet = get_dataset(param1, param2)
            Dim note_text As String = ""
            Dim no_of_notes As String = no_of_rows
            Dim note_date As Date

            'see if Rep Status has changed
            If rep_status <> "" Then
                Dim note_rep_status As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    note_date = note_dataset.Tables(0).Rows(idx3).Item(1)
                    Dim start_idx As Integer = InStr(note_text, "rep status:")
                    If start_idx > 0 Then
                        start_idx += 11
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_rep_status = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_rep_status = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_rep_status <> "" Then
                        Exit For
                    End If
                Next
                If note_rep_status = "" Then
                    write_note(debtor & "|" & "Rep Status:" & rep_status)
                    change_found = True
                Else
                    If LCase(rep_status) <> note_rep_status Then
                        If note_date < file_date Then
                            write_note(debtor & "|" & "Rep Status:" & rep_status)
                            change_found = True
                        End If
                    End If
                End If
            End If

            'see if Rep Status Description has changed
            If rep_status_desc <> "" Then
                Dim note_rep_status_desc As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    note_date = note_dataset.Tables(0).Rows(idx3).Item(1)
                    Dim start_idx As Integer = InStr(note_text, "rep status description:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_rep_status_desc = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_rep_status_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_rep_status_desc <> "" Then
                        Exit For
                    End If
                Next
                If note_rep_status_desc = "" Then
                    write_note(debtor & "|" & "Rep Status Description:" & rep_status_desc)
                    change_found = True
                Else
                    If LCase(rep_status_desc) <> note_rep_status_desc Then
                        If note_date < file_date Then
                            write_note(debtor & "|" & "Rep Status Description:" & rep_status_desc)
                            change_found = True
                        End If
                    End If
                End If
            End If

            'see if Rep Status Date has changed
            If rep_status_date <> Nothing Then
                Dim note_rep_status_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_date = note_dataset.Tables(0).Rows(idx3).Item(1)
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "rep status date:")
                    If start_idx > 0 Then
                        start_idx += 16
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_rep_status_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_rep_status_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_rep_status_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_rep_status_date_str)
                    If rep_status_date <> test_date Then
                        If note_date < file_date Then
                            write_note(debtor & "|" & "Rep Status Date:" & Format(rep_status_date, "dd/MM/yyyy"))
                            change_found = True
                        End If
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Rep Status Date:" & Format(rep_status_date, "dd/MM/yyyy"))
                End Try
            End If

            'ANY CHANGES FOUND?
            If Not change_found Then
                audit_file = audit_file & maat_id & "|no changes found" & vbNewLine
                no_changes_file = no_changes_file & maat_id & vbNewLine
                no_changes += 1
            Else
                audit_file = audit_file & maat_id & "|changes found" & vbNewLine
                changes += 1
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Private Sub write_error(ByVal error_message As String)
        Static Dim error_no As Integer
        error_no += 1
        Dim ln As Integer = Microsoft.VisualBasic.Len(OpenFileDialog1.FileName)
        new_file = Microsoft.VisualBasic.Left(OpenFileDialog1.FileName, ln - 4) & "_error.txt"
        error_message = error_message & vbNewLine
        If error_no = 1 Then
            My.Computer.FileSystem.WriteAllText(new_file, error_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(new_file, error_message, True)
        End If

    End Sub
    Private Sub write_eff_date(ByVal change_message As String)
    End Sub
    Private Sub write_change(ByVal change_message As String)
        
    End Sub
    Private Sub write_outcome(ByVal change_message As String)
        'Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        'new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_outcome.txt"
        'My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
    End Sub
    Private Sub write_note(ByVal change_message As String)
        change_message = change_message & ";from feed on " & Format(file_date, "dd.MM.yyyy") & vbNewLine
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = "H:\temp\rep_note.txt"
        
        My.Computer.FileSystem.WriteAllText(new_file, change_message, True)

    End Sub
    Private Sub remove_chrs(ByRef in_field As String)
        Dim idx2 As Integer
        Dim out_field As String = ""
        For idx2 = 1 To Microsoft.VisualBasic.Len(in_field)
            If Mid(in_field, idx2, 1) <> Chr(9) And _
              Mid(in_field, idx2, 1) <> Chr(10) And _
            Mid(in_field, idx2, 1) <> Chr(13) Then
                out_field = out_field & Mid(in_field, idx2, 1)
            End If
        Next
        in_field = out_field
    End Sub

    Private Sub reset_fields()
        applicant_id = ""
        summons_no = ""
        maat_id = ""
        first_name = ""
        surname = ""
        dob = ""
        invalid_dob = ""
        ni_no = ""
        mthly_contrib_amt_str = ""
        mthly_contrib_amt = Nothing
        upfront_contrib_amt_str = ""
        upfront_contrib_amt = Nothing
        'final_defence_cost = Nothing
        income_contrib_cap_str = ""
        income_uplift_applied = ""
        case_type = ""
        in_court_custody = ""
        imprisoned = ""
        sentence_date = Nothing
        debt_addr1 = ""
        debt_addr2 = ""
        debt_addr3 = ""
        debt_addr4 = ""
        debt_postcode = ""
        postal_addr1 = ""
        postal_addr2 = ""
        postal_addr3 = ""
        postal_addr4 = ""
        postal_postcode = ""
        landline = ""
        mobile = ""
        email = ""
        equity_amt = -9999
        partner_equity_amt = -9999
        appl_equity_amt = -9999
        equity_amt_verified = ""
        cap_asset_type = ""
        cap_amt = 0
        cap_amt_verified = ""
        allowable_cap_threshold = ""
        effective_date_str = ""
        effective_date = Nothing
        future_effective_date = False
        pref_pay_method = ""
        pref_pay_code = ""
        pref_pay_day = ""
        bank_acc_name = ""
        bank_acc_no = ""
        bank_sort_code = ""
        emp_status = ""
        emp_code = ""
        rep_order_withdrawal_date_str = ""
        rep_order_withdrawal_date = Nothing
        hardship_appl_recvd = ""
        comments = ""
        outcome = ""
        outcome2 = ""
        rep_status = ""
        appeal_type = ""
        residential_code = ""
        residential_desc = ""
        prop_type_code = ""
        prop_type_desc = ""
        percent_owned_appl = -999
        percent_owned_partner = -999
        has_partner = ""
        partner_first_name = ""
        partner_last_name = ""
        partner_dob = Nothing
        partner_nino = ""
        partner_emp_status = ""
        partner_emp_code = ""
        contrary_interest = ""
        disability_declaration = ""
        disability_description = ""
        declared_value = Nothing
        rep_status_date = Nothing
        committal_date = Nothing
        hardship_review_date = Nothing
        hardship_result = ""
        si = ""
        nfa = ""
        assessment_reason = ""
        assessment_code = ""
        assessment_date = Nothing
        bedroom_count = ""
        undeclared_property = ""
        declared_mortgage = -99999
        verified_mortgage = -99999
        verified_market_value = -99999
        no_capital_declared = ""
        all_evidence_date = Nothing
        uplift_applied_date = Nothing
        uplift_removed_date = Nothing
        ci_code = ""
        ci_desc = ""
        case_type_code = ""
        rep_status_desc = ""
    End Sub
    Private Sub get_comment_break(ByVal new_comment As String)
        Dim idx, idx2 As Integer
        'break comments into 250 chunks broken at ;
        Dim no_of_250s As Integer = Int(new_comment.Length / 250) + 1
        For idx = 1 To no_of_250s
            For idx2 = 250 To 1 Step -1
                If Mid(new_comment, idx2, 1) = ";" Then
                    Exit For
                End If
            Next
            comments = comments & Microsoft.VisualBasic.Left(new_comment, idx2)
            new_comment = Microsoft.VisualBasic.Right(new_comment, new_comment.Length - idx2)
            If new_comment.Length <= 250 Then
                comments = comments & new_comment & Space(250 - new_comment.Length)
                Exit For
            End If
        Next
    End Sub

    Private Sub validate_xml_file()
        'open file as text first to remove any � signs
        Dim txt_doc As String
        txt_doc = My.Computer.FileSystem.ReadAllText(OpenFileDialog1.FileName)
        txt_doc = Replace(txt_doc, "�", "")
        My.Computer.FileSystem.WriteAllText(OpenFileDialog1.FileName, txt_doc, False)
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(OpenFileDialog1.FileName)
        myDocument.Schemas.Add("", "R:\vb.net\LSC XSD\contribution_file.xsd")
        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
    End Sub

    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_error("Error: " & e.Message)
            Case XmlSeverityType.Warning
                write_error("Warning " & e.Message)
        End Select
    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        log_user = My.User.Name
        Dim slash_idx As Integer = InStr(log_user, "\")
        If slash_idx > 0 Then
            log_user = Microsoft.VisualBasic.Right(log_user, log_user.Length - slash_idx)
        End If
    End Sub

    Private Sub LSC_preprocess_last_Header_IDBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("United Utilities Commercial Returns")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Rossendales")> 
<Assembly: AssemblyProduct("United Utilities Commercial Returns")> 
<Assembly: AssemblyCopyright("Copyright © Rossendales 2014")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("cf1661fc-d22e-4ea6-845f-bac5a8046b56")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.5.0.0")> 
<Assembly: AssemblyFileVersion("1.5.0.0")> 

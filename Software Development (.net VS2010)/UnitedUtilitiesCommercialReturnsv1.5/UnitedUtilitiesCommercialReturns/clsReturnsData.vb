﻿Imports CommonLibrary

Public Class clsReturnsData
    Private Debtor As New DataTable, MasterList As New DataTable, TempTable As New DataTable, AddressSource As New DataTable, Address As New DataTable, MergedSource As New DataTable, ContactSource As New DataTable
    Private DebtorDV As DataView, AddressDV As DataView
    'Private ClientSchemeIDList As String = "3397, 3398, 3399, 3400, 4435" ' Commented out request ref 90094
    'Private ClientSchemeIDList As String = "3397, 3398, 3399, 3400, 4435, 4911, 4912, 4913" ' ConnIDs added TS request ref 90094. Commented out TS 06/Feb/2017. Request ref 103274
    Private ClientIDList As String = ("1931, 1943") ' Added TS 06/Feb/2017. Request ref 103274

    Public ReadOnly Property Debtors() As DataView
        Get
            Debtors = DebtorDV
        End Get
    End Property

    Public ReadOnly Property Addresses() As DataView
        Get
            Addresses = AddressDV
        End Get
    End Property

    Public Sub New()
        ' item MaxLength added to ensure we don't supply values that are too large
        With Address.Columns

            .Add("DebtorID", Type.GetType("System.String"))
            .Item("DebtorID").ExtendedProperties.Add("MaxLength", 10)

            .Add("client_ref", Type.GetType("System.String"))
            .Item("client_ref").ExtendedProperties.Add("MaxLength", 10)

            .Add("defaultCourtCode", Type.GetType("System.String"))
            .Item("defaultCourtCode").ExtendedProperties.Add("MaxLength", 6)

            .Add("name_title", Type.GetType("System.String"))
            .Item("name_title").ExtendedProperties.Add("MaxLength", 50)

            .Add("name_fore", Type.GetType("System.String"))
            .Item("name_fore").ExtendedProperties.Add("MaxLength", 50)

            .Add("name_sur", Type.GetType("System.String"))
            .Item("name_sur").ExtendedProperties.Add("MaxLength", 50)

            .Add("HouseNumber", Type.GetType("System.String"))
            .Item("HouseNumber").ExtendedProperties.Add("MaxLength", 20)

            .Add("Street", Type.GetType("System.String"))
            .Item("Street").ExtendedProperties.Add("MaxLength", 250)

            .Add("District", Type.GetType("System.String"))
            .Item("District").ExtendedProperties.Add("MaxLength", 60)

            .Add("City", Type.GetType("System.String"))
            .Item("City").ExtendedProperties.Add("MaxLength", 60)

            .Add("PostCode", Type.GetType("System.String"))
            .Item("PostCode").ExtendedProperties.Add("MaxLength", 10)

            .Add("POBox", Type.GetType("System.String"))
            .Item("POBox").ExtendedProperties.Add("MaxLength", 20)

            .Add("Phone", Type.GetType("System.String"))
            .Item("Phone").ExtendedProperties.Add("MaxLength", 13)

            ' the following added TS 02/Sep/2015. Request ref 59358
            .Add("Address", Type.GetType("System.String"))
            .Item("Address").ExtendedProperties.Add("MaxLength", 500)

        End With

        TempTable.Columns.Add("client_ref", Type.GetType("System.String"))
    End Sub

    Public Sub GetDebtors(ByVal DateFrom As Date, ByVal DateTo As Date)
        Dim Sql As String

        Try
            ' Get updated cases
            Sql = "SELECT d._rowID AS debtorID " & _
                  "     , d.client_ref " & _
                  "     , cs.defaultCourtCode " & _
                  "     , s.desc_long AS clientReturnNumber " & _
                  "     , IFNULL(d.name_title, '') AS name_title " & _
                  "     , IFNULL(d.name_fore, '') AS name_fore " & _
                  "     , IFNULL(d.name_sur, '') AS name_sur " & _
                  "     , n.text AS return_details " & _
                  "     , CAST(NULL AS DECIMAL(12,2)) AS arrange_amount " & _
                  "     , CAST(NULL AS SIGNED) AS arrange_interval " & _
                  "     , CAST(NULL AS DATETIME) AS arrange_next " & _
                  "     , CAST(NULL AS DECIMAL(12,2)) AS debtPaid " & _
                  "     , CAST(NULL AS DATETIME) AS GoneAwayDate " & _
                  "     , d.highCourtName AS AssociateName " & _
                  "     , d.judgmentDate AS AssociateDOB " & _
                  "     , d.highCourtActionNumber AS AssociateNI " & _
                  "     , d.empNI " & _
                  "FROM Debtor AS d " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "INNER JOIN CodeStatus AS s ON d.statusCode = s._rowid " & _
                  "INNER JOIN note AS n ON d._rowID = n.DebtorID " & _
            "INNER JOIN ( SELECT n_s.DebtorID " & _
                  "                  , MAX(n_s._rowID) AS LastNoteID " & _
                  "             FROM debtor AS d_s " & _
                  "             INNER JOIN note AS n_s ON d_s._rowID = n_s.DebtorID " & _
                  "             INNER JOIN clientscheme AS cs_s ON d_s.clientschemeID = cs_s._RowID " & _
                  "             WHERE n_s.type = 'Status changed' " & _
                  "               AND cs_s.ClientID IN (" & ClientIDList & ") " & _
                  "               AND n_s._createdDate >'" & DateFrom.ToString("yyyy-MM-dd") & "' " & _
                  "               AND n_s._createdDate <'" & DateTo.AddDays(+1).ToString("yyyy-MM-dd") & "' " & _
                  "             GROUP BY n_s.DebtorID " & _
                  "           ) AS n_max ON n.DebtorID = n_max.DebtorID " & _
                  "                     AND n._rowID   = n_max.LastNoteID " & _
                  " WHERE cs.ClientID IN (" & ClientIDList & ")" & _
                  "  AND s._rowID IN (35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,157,158,159,160)"
            ' "               AND DATE(n_s._createdDate) >'" & DateFrom.AddDays(-1).ToString("yyyy-MM-dd") & "' " & _
            ' "               AND DATE(n_s._createdDate) <'" & DateTo.AddDays(+1).ToString("yyyy-MM-dd") & "' " & _
            LoadDataTable("DebtRecovery", Sql, Debtor, False)

            ' Get closed cases next.
            ' Modified - NOT EXISTS added TS 16/Jul/2015. Request ref 55017
            Sql = "SELECT d._rowID AS debtorID " & _
                  "     , d.client_ref " & _
                  "     , cs.defaultCourtCode " & _
                  "     , CASE WHEN d.status = 'S' THEN 7 ELSE csr.clientReturnNumber END AS clientReturnNumber " & _
                  "     , IFNULL(d.name_title, '') AS name_title " & _
                  "     , IFNULL(d.name_fore, '') AS name_fore " & _
                  "     , IFNULL(d.name_sur, '') AS name_sur " & _
                  "     , d.return_details " & _
                  "     , NULL AS arrange_amount " & _
                  "     , NULL AS arrange_interval " & _
                  "     , NULL AS arrange_next " & _
                  "     , d.debtPaid " & _
                  "     , STR_TO_DATE(REPLACE(SUBSTRING_INDEX(n.text, ' ', -1), '.', '/'), '%d/%m/%Y' ) AS GoneAwayDate " & _
                  "     , NULL AS AssociateName " & _
                  "     , NULL AS AssociateDOB " & _
                  "     , NULL AS AssociateNI " & _
                  "     , NULL AS empNI " & _
                  "FROM Debtor AS d " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "INNER JOIN remit AS r ON d.return_remitid = r._rowid " & _
                  "LEFT JOIN ClientSchemeReturn AS csr ON d.ClientSchemeID = csr.ClientSchemeid " & _
                  "                                   AND d.return_codeid  = csr.returnid " & _
                  "LEFT JOIN ( SELECT n_s.DebtorID " & _
                  "                 , n_s.text " & _
                  "            FROM note AS n_s " & _
                  "            INNER JOIN ( SELECT n_m_s.DebtorID " & _
                  "                              , MAX(n_m_s._rowID) AS _rowID " & _
                  "                         FROM debtor AS d_m_s " & _
                  "                         INNER JOIN clientscheme AS cs_m_s ON d_m_s.ClientSchemeID = cs_m_s._rowID " & _
                  "                         INNER JOIN note AS n_m_s ON d_m_s._rowID = n_m_s._rowID " & _
                  "                         WHERE cs_m_s.ClientID IN (" & ClientIDList & ") " & _
                  "                           AND d_m_s.status_open_closed = 'C' " & _
                  "                           AND n_m_s.type = 'Cancelled' " & _
                  "                           AND n_m_s.text LIKE '%Re: Gone Away - %' " & _
                  "                         GROUP BY n_m_s.DebtorID " & _
                  "                       ) AS n_m ON n_s.DebtorID = n_m.DebtorID " & _
                  "                               AND n_s._rowID   = n_m._rowID" & _
                  "          ) AS n ON n.DebtorID = d._rowID " & _
                  "WHERE cs.ClientID IN (" & ClientIDList & ") " & _
                  "  AND d.status_open_closed = 'C' " & _
                  "  AND r.date BETWEEN '" & DateFrom.ToString("yyyy-MM-dd") & "' " & _
                  "                 AND '" & DateTo.AddDays(+1).ToString("yyyy-MM-dd") & "'" & _
                  "  AND NOT EXISTS ( SELECT 1 " & _
                  "                   FROM note AS n_s " & _
                  "                   WHERE n_s.DebtorID = d._rowID " & _
                  "                     AND n_s.text = 'account closed as per client request - not to be reported on the clients closure file as per David Rutter' " & _
                  "                 )"
            '"  AND DATE(r.date) BETWEEN '" & DateFrom.ToString("yyyy-MM-dd") & "' " & _
            '"                       AND '" & DateTo.ToString("yyyy-MM-dd") & "'" & _

            LoadDataTable("DebtRecovery", Sql, Debtor, True)

            ' Add ID's of each address / contact detail change so these are looped through 
            If Not IsNothing(Addresses) Then
                For Each Change As DataRow In Addresses.Table.Rows
                    Debtor.Rows.Add({Change(0), Change(1), Change(2), 8, Change(3), Change(4), Change(5)}) ' 8 is the return code for addresses
                Next Change
            End If

            ' Add arrangement changes status
            Sql = "SELECT d._rowID AS debtorID " & _
                  "     , d.client_ref " & _
                  "     , cs.defaultCourtCode " & _
                  "     , 65 AS clientReturnNumber " & _
                  "     , IFNULL(d.name_title, '') AS name_title " & _
                  "     , IFNULL(d.name_fore, '') AS name_fore " & _
                  "     , IFNULL(d.name_sur, '') AS name_sur " & _
                  "     , NULL AS return_details " & _
                  "     , d.arrange_amount " & _
                  "     , d.arrange_interval " & _
                  "     , d.arrange_next " & _
                  "     , NULL AS debtPaid " & _
                  "     , NULL AS GoneAwayDate " & _
                  "     , NULL AS AssociateName " & _
                  "     , NULL AS AssociateDOB " & _
                  "     , NULL AS AssociateNI " & _
                  "     , NULL AS empNI " & _
                  "FROM Debtor AS d " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "INNER JOIN ( SELECT DISTINCT d_s._rowID " & _
                  "               FROM debtor AS d_s " & _
                  "               INNER JOIN note AS n_s ON d_s._rowID = n_s.DebtorID " & _
                  "               INNER JOIN clientscheme AS cs_s ON d_s.ClientSchemeID = cs_s._RowID " & _
                  "               WHERE n_s.type = 'Arrangement' " & _
                  "                 AND cs_s.ClientID IN (" & ClientIDList & ") " & _
                  "                 AND n_s._createdDate BETWEEN '" & DateFrom.ToString("yyyy-MM-dd") & "' " & _
                  "                                                AND '" & DateTo.AddDays(-1).ToString("yyyy-MM-dd") & "' " & _
                  "               GROUP BY n_s.DebtorID " & _
                  "             ) AS d_a ON d._rowID = d_a._rowID " & _
                  "WHERE cs.ClientID IN (" & ClientIDList & ") " & _
                  "  AND d.status = 'A'" & _
                  "  AND d.arrange_amount > 0"

            LoadDataTable("DebtRecovery", Sql, Debtor, True)

            DebtorDV = New DataView(Debtor)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub GetAddresses(ByVal DateFrom As Date, ByVal DateTo As Date)
        Dim OutputDataRow As DataRow
        Dim AddressLine() As String, AddressWord() As String, Sql As String

        Try
            ' Load address changes
            Sql = "SELECT d._rowID AS debtorID " & _
                  "     , d.client_ref " & _
                  "     , cs.defaultCourtCode " & _
                  "     , IFNULL(d.name_title, '') AS name_title " & _
                  "     , IFNULL(d.name_fore, '') AS name_fore " & _
                  "     , IFNULL(d.name_sur, '') AS name_sur " & _
                  "     , IFNULL(d.address,'')  AS address " & _
                  "     , '' AS add_phone " & _
                  "     , '' AS AssociateName " & _
                  "     , '' AS dateOfBirth " & _
                  "     , '' AS empNI " & _
                  "FROM debtor AS d " & _
                  "INNER JOIN note AS n ON d._rowID = n.DebtorID " & _
                  "INNER JOIN clientscheme AS cs ON d.clientschemeID = cs._RowID " & _
                  "INNER JOIN scheme AS s ON cs.schemeID = s._RowID " & _
                  "INNER JOIN ( SELECT n_s.DebtorID " & _
                  "                  , MAX(n_s._rowID) AS LastNoteID " & _
                  "             FROM debtor AS d_s " & _
                  "             INNER JOIN note AS n_s ON d_s._rowID = n_s.DebtorID " & _
                  "             INNER JOIN clientscheme AS cs_s ON d_s.clientschemeID = cs_s._RowID " & _
                  "             WHERE n_s.type = 'Address' " & _
                  "               AND n_s.text LIKE 'changed from:%'" & _
                  "               AND cs_s.ClientID IN (" & ClientIDList & ") " & _
                  "               AND DATE(n_s._createdDate) BETWEEN '" & DateFrom.ToString("yyyy-MM-dd") & "' " & _
                  "                                              AND '" & DateTo.ToString("yyyy-MM-dd") & "' " & _
                  "             GROUP BY n_s.DebtorID " & _
                  "           ) AS n_max ON n.DebtorID = n_max.DebtorID " & _
                  "                     AND n._rowID   = n_max.LastNoteID " & _
                  "LEFT JOIN note AS nt ON n.debtorID           = nt.debtorID " & _
                  "                    AND 'Off trace'          = nt.type " & _
                  "                    AND DATE(n._createdDate) = DATE(nt._createdDate)" & _
                  "WHERE n.type = 'Address' " & _
                  "  AND n.text LIKE 'changed from:%'" & _
                  "  AND cs.ClientID IN (" & ClientIDList & ")"

            LoadDataTable("DebtRecovery", Sql, AddressSource, False)

            ' Load contact detail changes
            Sql = "SELECT sd.DebtorID " & _
                  "     , sd.client_ref " & _
                  "     , cs.defaultCourtCode " & _
                  "     , ISNULL(sd.name_title, '') AS name_title " & _
                  "     , ISNULL(sd.name_fore, '') AS name_fore " & _
                  "     , ISNULL(sd.name_sur, '') AS name_sur " & _
                  "     , '' AS address " & _
                  "     , ISNULL(d.add_phone,'') AS add_phone " & _
                  "     , '' AS AssociateName " & _
                  "     , ISNULL(CONVERT(VARCHAR,d.dateOfBirth, 103),'') AS dateOfBirth " & _
                  "     , ISNULL(d.empNI,'') AS empNI " & _
                  "FROM rpt.UUDebtor AS d " & _
                  "INNER JOIN stg.Debtor AS sd ON d.DebtorID = sd.DebtorID " & _
                  "INNER JOIN stg.ClientScheme AS cs ON cs.ClientSchemeID = sd.ClientSchemeID " & _
                  "WHERE CAST(FLOOR(CAST(d.BuildDate AS FLOAT)) AS DATETIME) BETWEEN '" & DateFrom.ToString("yyyy-MMM-dd") & "' " & _
                  "                                                              AND '" & DateTo.ToString("yyyy-MMM-dd") & "' " & _
                  "  AND cs.ClientID IN (" & ClientIDList & ")"

            LoadDataTable("StudentLoans", Sql, ContactSource, False)

            MergeUpdates()

            Address.Clear()

            For Each SourceDataRow As DataRow In MergedSource.Rows
                OutputDataRow = Address.Rows.Add

                ' The following section performs the mapping from DebtRecovery fields to the output file

                OutputDataRow("DebtorID") = SourceDataRow("DebtorID")
                OutputDataRow("client_ref") = SourceDataRow("client_ref")
                OutputDataRow("defaultCourtCode") = SourceDataRow("defaultCourtCode")
                OutputDataRow("name_title") = SourceDataRow("name_title")
                OutputDataRow("name_fore") = SourceDataRow("name_fore")
                OutputDataRow("name_sur") = SourceDataRow("name_sur")

                ' Create an array of address lines. Note DebtRecovery sometimes just uses a line feed for new line with no carriage return
                If SourceDataRow("Address") <> "" Then
                    AddressLine = Replace(LTrim(SourceDataRow("Address")), vbCr, "").ToString.TrimEnd(vbLf).Split(vbLf)
                Else
                    AddressLine = Nothing
                End If

                ' DebtRecovery stored the address in one field. The following logic is to break it down into multiple
                If Not IsNothing(AddressLine) Then

                    OutputDataRow("Address") = Replace(LTrim(SourceDataRow("Address")), vbCr, "").ToString.TrimEnd(vbLf).Replace(";", "").Replace(vbLf, ",") ' added TS 02/Sep/2015. Request ref 59358

                    AddressWord = AddressLine(0).ToString.Split(" ") ' Split first line into words to get house number and flat details

                    ' If the second word is a number assume a flat or appartment or if addressline contains the word flat assume a flat
                    If (UBound(AddressWord) > 1 AndAlso System.Text.RegularExpressions.Regex.Match(AddressWord(1), "\d").Success) Then

                        ' If the flat and its number are the only line contents, assume the next line is the road and rebuild the array
                        If UBound(AddressWord) < 2 Then
                            OutputDataRow("Street") = AddressLine(1)
                            Dim objArrayList As New ArrayList(AddressLine)
                            objArrayList.RemoveAt(1)
                            AddressLine = objArrayList.ToArray(GetType(String))
                        Else
                            For WordLoop As Integer = 2 To UBound(AddressWord) ' 
                                OutputDataRow("Street") &= " " & AddressWord(WordLoop) ' Append the rest to Street
                            Next WordLoop
                        End If

                        If Not IsDBNull(OutputDataRow("Street")) Then OutputDataRow("Street") = LTrim(OutputDataRow("Street"))

                        If System.Text.RegularExpressions.Regex.Match(OutputDataRow("Street"), "\d").Success Then
                            ' If the first word is a number assume a normal house number
                            OutputDataRow("HouseNumber") = Left(OutputDataRow("Street"), OutputDataRow("Street").IndexOf(" "))
                            OutputDataRow("Street") = Right(OutputDataRow("Street"), OutputDataRow("Street").Length - OutputDataRow("Street").IndexOf(" ") - 1)
                        End If

                    ElseIf System.Text.RegularExpressions.Regex.Match(AddressWord(0), "\d").Success Then
                        ' If the first word is a number assume a normal house number
                        If AddressLine(0).IndexOf(" ") > 0 Then ' Added TS 16/Jul/2013 Portal task ref 16226
                            ' Original logic
                            OutputDataRow("HouseNumber") = Left(AddressLine(0), AddressLine(0).IndexOf(" "))
                            OutputDataRow("Street") = Right(AddressLine(0), AddressLine(0).Length - AddressLine(0).IndexOf(" ") - 1)
                        Else
                            OutputDataRow("HouseNumber") = AddressLine(0)
                        End If
                    Else

                        ' Nothing captured above
                        OutputDataRow("Street") = AddressLine(0)
                    End If

                    ' put the lines in the address into the different fields. The logic used differs based on the number of lines in the address
                    Select Case UBound(AddressLine)
                        Case 0
                            ' no need to do anything as the single line will go into street and postcode
                        Case 1
                            ' no need to do anything as the first line will go into street and the last into postcode
                        Case 2
                            OutputDataRow("District") = AddressLine(1) ' MOD TS 30/Oct/2012 was county
                        Case 3
                            OutputDataRow("District") = AddressLine(1)
                            OutputDataRow("City") = AddressLine(2)
                        Case 4
                            OutputDataRow("Street") &= ", " & AddressLine(1)
                            OutputDataRow("District") = AddressLine(2)
                            OutputDataRow("City") = AddressLine(3)
                        Case Else
                            Dim AdditionalStreetLines As String = ""
                            For LineCount As Integer = 1 To UBound(AddressLine) - 3
                                If AdditionalStreetLines <> "" Then AdditionalStreetLines += " "
                                AdditionalStreetLines += AddressLine(LineCount) ' Append the rest to District
                            Next LineCount

                            OutputDataRow("Street") &= ", " & AdditionalStreetLines
                            OutputDataRow("District") = AddressLine(UBound(AddressLine) - 2)
                            OutputDataRow("City") = AddressLine(UBound(AddressLine) - 1)
                    End Select

                    OutputDataRow("PostCode") = AddressLine(UBound(AddressLine)).Trim

                    For Each Line As String In AddressLine
                        If Line.ToUpper.Contains("PO BOX") Then OutputDataRow("POBox") = Line.Substring(InStr(Line.ToUpper, "PO BOX") - 1).ToUpper
                    Next Line

                End If
                OutputDataRow("Phone") = SourceDataRow("add_phone").ToString.Replace(" ", "")

            Next SourceDataRow

            AddressDV = New DataView(Address)
            AddressDV.AllowNew = False

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub

    Private Sub MergeUpdates()
        Try
            Dim AddressDV As DataView = New DataView(AddressSource)
            Dim ContactDV As DataView = New DataView(ContactSource)

            MergedSource = AddressSource.Clone() ' Clone the record structure of one of the source files for the merge output
            TempTable.Clear()

            ' Append both sets of headers to temp table
            For Each dr As DataRow In AddressSource.Rows
                TempTable.Rows.Add(dr("client_ref"))
            Next dr

            For Each dr As DataRow In ContactSource.Rows
                TempTable.Rows.Add(dr("client_ref"))
            Next dr

            ' Get distinct line headers
            MasterList = TempTable.DefaultView.ToTable(True)

            ' Build the merged table
            For Each dr As DataRow In MasterList.Rows
                AddressDV.RowFilter = ("client_ref = '" & dr("client_ref") & "'")
                ContactDV.RowFilter = ("client_ref = '" & dr("client_ref") & "'")

                If AddressDV.Count = 0 And ContactDV.Count > 0 Then
                    MergedSource.ImportRow(ContactDV.Item(0).Row)
                ElseIf ContactDV.Count = 0 And AddressDV.Count > 0 Then
                    MergedSource.ImportRow(AddressDV.Item(0).Row)
                Else ' Assume data in both as the client ref must have been in at least one of the source tables
                    MergedSource.Rows.Add({AddressDV.Item(0).Item(0), _
                                          AddressDV.Item(0).Item(1), _
                                          AddressDV.Item(0).Item(2), _
                                          AddressDV.Item(0).Item(3), _
                                          AddressDV.Item(0).Item(4), _
                                          AddressDV.Item(0).Item(5), _
                                          AddressDV.Item(0).Item(6), _
                                          ContactDV.Item(0).Item(7), _
                                          ContactDV.Item(0).Item(8), _
                                          ContactDV.Item(0).Item(9), _
                                          ContactDV.Item(0).Item(10)})
                End If

            Next dr

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
End Class

﻿
Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click

        Try
            invNumber = invtbox.Text
        Catch ex As Exception
            MsgBox("Invoice number is not numeric")
            Exit Sub
        End Try

        remitfrm.ShowDialog()
        Me.Close()
    End Sub
End Class

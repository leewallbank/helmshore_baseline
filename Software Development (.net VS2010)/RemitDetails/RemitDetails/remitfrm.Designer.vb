﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class remitfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.remitdg = New System.Windows.Forms.DataGridView()
        Me.description = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.debtorID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.collected_nett = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.collected_vat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        CType(Me.remitdg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'remitdg
        '
        Me.remitdg.AllowUserToAddRows = False
        Me.remitdg.AllowUserToDeleteRows = False
        Me.remitdg.AllowUserToOrderColumns = True
        Me.remitdg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.remitdg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.description, Me.debtorID, Me.collected_nett, Me.collected_vat})
        Me.remitdg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.remitdg.Location = New System.Drawing.Point(0, 0)
        Me.remitdg.Name = "remitdg"
        Me.remitdg.ReadOnly = True
        Me.remitdg.Size = New System.Drawing.Size(811, 432)
        Me.remitdg.TabIndex = 0
        '
        'description
        '
        Me.description.HeaderText = "Description"
        Me.description.Name = "description"
        Me.description.ReadOnly = True
        Me.description.Width = 450
        '
        'debtorID
        '
        Me.debtorID.HeaderText = "DebtorID"
        Me.debtorID.Name = "debtorID"
        Me.debtorID.ReadOnly = True
        '
        'collected_nett
        '
        Me.collected_nett.HeaderText = "Nett"
        Me.collected_nett.Name = "collected_nett"
        Me.collected_nett.ReadOnly = True
        '
        'collected_vat
        '
        Me.collected_vat.HeaderText = "Vat"
        Me.collected_vat.Name = "collected_vat"
        Me.collected_vat.ReadOnly = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(199, 0)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(103, 23)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "Write to Excel"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'remitfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(811, 432)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.remitdg)
        Me.Name = "remitfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "remitfrm"
        CType(Me.remitdg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents remitdg As System.Windows.Forms.DataGridView
    Friend WithEvents description As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents debtorID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents collected_nett As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents collected_vat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
End Class

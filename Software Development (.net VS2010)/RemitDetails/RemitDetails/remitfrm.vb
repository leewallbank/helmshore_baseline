﻿Imports CommonLibrary
Imports System.IO
Public Class remitfrm
    Dim remit_dt As New DataTable
    Dim excelFile As String = "Description,DebtorID,Nett,Vat" & vbNewLine
    Private Sub remitfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        remitdg.Rows.Clear()
        LoadDataTable("DebtRecovery", "select RD.description, RD.value_line, RD.vat_amount " & _
                      " from remit R, remitdetail RD " & _
                      " where R.invoiceID = " & invNumber & _
                      " and R._rowID = RD.remitID", remit_dt, False)
        For Each remitRow In remit_dt.Rows
            Dim description As String = remitRow(0)
            Dim valueLine As Decimal = remitRow(1)
            Dim vatAmount As Decimal = remitRow(2)
            Dim debtorID As Integer = 0
            Dim debtor_str As String = ""
            Dim startIDX As Integer = InStr(description, "caseID:")
            If startIDX > 0 Then
                debtor_str = Mid(description, startIDX + 7, 8)
                If IsNumeric(debtor_str) Then
                    debtorID = debtor_str
                End If
            End If
            remitdg.Rows.Add(description, debtorID, valueLine, vatAmount)
            description = Replace(description, ",", " ")
            excelFile &= description & "," & debtorID & "," & valueLine & "," & vatAmount & vbNewLine
        Next

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click


        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "CSV files |*.csv"
            .DefaultExt = ".csv"
            .OverwritePrompt = True
            .FileName = "RemitDetails"
        End With
        If SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, excelFile, False)
            MsgBox("File saved")
        End If
        Me.Close()
    End Sub
End Class
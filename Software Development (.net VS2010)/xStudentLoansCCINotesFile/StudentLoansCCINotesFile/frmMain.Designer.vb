﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblModifiedDate = New System.Windows.Forms.Label()
        Me.dtpModifiedDate = New System.Windows.Forms.DateTimePicker()
        Me.cmdCreateFile = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblModifiedDate
        '
        Me.lblModifiedDate.AutoSize = True
        Me.lblModifiedDate.Location = New System.Drawing.Point(43, 61)
        Me.lblModifiedDate.Name = "lblModifiedDate"
        Me.lblModifiedDate.Size = New System.Drawing.Size(78, 13)
        Me.lblModifiedDate.TabIndex = 6
        Me.lblModifiedDate.Text = "Modified since:"
        '
        'dtpModifiedDate
        '
        Me.dtpModifiedDate.Location = New System.Drawing.Point(124, 57)
        Me.dtpModifiedDate.Name = "dtpModifiedDate"
        Me.dtpModifiedDate.Size = New System.Drawing.Size(126, 20)
        Me.dtpModifiedDate.TabIndex = 1
        '
        'cmdCreateFile
        '
        Me.cmdCreateFile.Location = New System.Drawing.Point(96, 133)
        Me.cmdCreateFile.Name = "cmdCreateFile"
        Me.cmdCreateFile.Size = New System.Drawing.Size(101, 30)
        Me.cmdCreateFile.TabIndex = 2
        Me.cmdCreateFile.Text = "Create File"
        Me.cmdCreateFile.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 218)
        Me.Controls.Add(Me.cmdCreateFile)
        Me.Controls.Add(Me.lblModifiedDate)
        Me.Controls.Add(Me.dtpModifiedDate)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.Text = "CCI Notes file"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblModifiedDate As System.Windows.Forms.Label
    Friend WithEvents dtpModifiedDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmdCreateFile As System.Windows.Forms.Button

End Class

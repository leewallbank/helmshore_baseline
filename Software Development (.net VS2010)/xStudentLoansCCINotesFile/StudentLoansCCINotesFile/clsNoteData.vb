﻿Imports CommonLibrary

Public Class clsNoteData

    Private Note As New DataTable
    Private NoteDV As DataView

    Public ReadOnly Property NoteDataView() As DataView
        Get
            NoteDataView = NoteDV
        End Get
    End Property

    Public Sub GetUpdates(ByVal ModDate As Date)
        Dim Sql As String

        Try
            ' Load note changes
            Sql = "SELECT cs.defaultCourtCode " & _
                  "     , n._rowID AS NoteID " & _
                  "     , d._rowID AS DebtorID " & _
                  "     , n._createdDate " & _
                  "     , n.text " & _
                  "FROM Note AS n " & _
                  "INNER JOIN Debtor AS d ON n.debtorID = d._rowid " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "WHERE n._createdDate >= '" & ModDate.ToString("yyyy-MM-dd") & "'" & _
                  "  AND d.clientschemeID IN (3162, 3163, 3164, 3167, 3168, 3174, 3175, 3176, 3177, 3178) " & _
                  "  AND (    n.type <> 'Client note' " & _
                  "      ) " & _
                  "ORDER BY cs.defaultCourtCode " & _
                  "       , d._rowID "
            '                  "        OR n.text NOT LIKE '%XXX%' -- need to confirm this" & _

            LoadDataTable("DebtRecovery", Sql, Note, False)

            NoteDV = New DataView(Note)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub LogOutput(ByVal DebtorID As String, ByVal NoteID As String)
        Dim Sql As String

        Try
            Sql = "EXEC dbo.LogCCIOutput " & DebtorID & ", 'N', '" & NoteID & "'"

            ExecStoredProc("StudentLoans", Sql)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Function OutputExists(ByVal DebtorID As String, ByVal NoteID As String) As Boolean
        OutputExists = GetProcResults("EXEC dbo.CCIOutputExists " & DebtorID & ", 'N', '" & NoteID & "'")
    End Function
End Class

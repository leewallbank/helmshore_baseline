﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.run_dtp = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.runbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.SuspendLayout
        '
        'run_dtp
        '
        Me.run_dtp.Location = New System.Drawing.Point(66, 71)
        Me.run_dtp.Name = "run_dtp"
        Me.run_dtp.Size = New System.Drawing.Size(143, 20)
        Me.run_dtp.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(112, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Load date"
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(92, 140)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(75, 23)
        Me.runbtn.TabIndex = 2
        Me.runbtn.Text = "Run Reports"
        Me.runbtn.UseVisualStyleBackColor = true
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(197, 226)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 3
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = true
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.run_dtp)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TFL Acknowledgement Files"
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents run_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog

End Class

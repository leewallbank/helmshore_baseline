﻿Imports CommonLibrary
Public Class Form1
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim outfile As String = "Our Ref,Clien Ref,date Received,Original O/S" & vbNewLine
    Dim outxmlfilename As String
    Dim totCases As Integer
    Dim TotDebt As Decimal
    Dim schemeName As String
    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(sender As System.Object, e As System.EventArgs) Handles runbtn.Click
        runbtn.Enabled = False
        exitbtn.Enabled = False
        outxmlfilename = "H:TFL.xml"

        Dim writer As New Xml.XmlTextWriter(outxmlfilename, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)
        writer.Formatting = Xml.Formatting.Indented
        writer.WriteStartElement("ns0:DebtCase")
        writer.WriteAttributeString("xmlns:ns0", "http://www.capita.co.uk/TfL/DebtCase")
        writer.WriteStartElement("ns1:Header ")
        writer.WriteAttributeString("xmlns:ns1", "http://www.capita.co.uk/TfL/Header")
        writer.WriteStartElement("partner")
        writer.WriteStartElement("partnerto")
        writer.WriteElementString("partnerid", "ACC")
        writer.WriteElementString("partnerrole", "Business Operations")
        writer.WriteEndElement()  'partnerto
        writer.WriteStartElement("partnerfrom")
        writer.WriteElementString("partnerid", "Marston")
        writer.WriteElementString("partnerrole", "Debt Recovery Agency")
        writer.WriteEndElement()  'partnerfrom
        writer.WriteEndElement()  'partner
        Dim firstCase As Boolean = True
        writer.WriteStartElement("batchdetails")
        

        'get all TFL cases for selected date
        Dim debt_dt As New DataTable

        LoadDataTable("DebtRecovery", "select D._rowID, D.client_ref, D.debt_original, S.name, client_batch from debtor D, clientscheme CS, scheme S " & _
                      " where D.clientschemeID = CS._rowID " & _
                      " and CS.clientID = 1793" & _
                      " and CS.schemeID = S._rowID " & _
                      " and date(D._createdDate) = '" & Format(run_dtp.Value, "yyyy-MM-dd") & "'" & _
                      " order by D.client_ref", debt_dt, False)
        For Each debtrow In debt_dt.Rows
            Dim debtorID As Integer = debtrow(0)
            Dim clientRef As String = debtrow(1)
            Dim OriginalDebt As Decimal = debtrow(2)
            schemeName = debtrow(3)
            Dim clientbatch As Integer = debtrow(4)
            If firstCase Then
                firstCase = False
                writer.WriteElementString("batchno", clientbatch)
                writer.WriteElementString("batchdate", Format(run_dtp.Value, "yyyy-MM-ddTHH:mm:ss"))
                writer.WriteElementString("messagetype", "ACK")
                writer.WriteEndElement()  'batchdetails
                writer.WriteEndElement()  'ns1:Header
            End If
            writer.WriteStartElement("CaseDetails")
            writer.WriteStartElement("CaseItem")
            writer.WriteElementString("autopayaccountnumber", clientRef)
            writer.WriteElementString("messagecode", "2000")
            writer.WriteEndElement()   'caseItem
            writer.WriteEndElement()  'CaseDetails
            totCases += 1
            TotDebt += OriginalDebt
            outfile &= debtorID & "," & clientRef & "," & Format(run_dtp.Value, "dd/MM/yyyy") & "," & OriginalDebt & vbNewLine
        Next

        'trailer
        writer.WriteStartElement("ns2:Trailer")
        writer.WriteAttributeString("xmlns:ns2", "http://www.capita.co.uk/tfl/Trailer")
        writer.WriteElementString("batchitems", totCases)
        writer.WriteEndElement()  'trailer
        writer.WriteEndElement()  'ns0:Debtcase

        outfile &= "Total number of cases in list:, " & totCases & ",Total amount due for list:," & TotDebt & vbNewLine

        writer.Close()
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "CSV files |*.csv"
            .DefaultExt = ".csv"
            .OverwritePrompt = True
            .FileName = "Transport For London " & schemeName & ".csv"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False, ascii)
            SaveFileDialog1.FileName = Replace(SaveFileDialog1.FileName, ".csv", ".xml")
            My.Computer.FileSystem.CopyFile(outxmlfilename, SaveFileDialog1.FileName, True)
            MsgBox("reports saved")
        Else
            MsgBox("reports not saved")
        End If
        Me.Close()

    End Sub
End Class

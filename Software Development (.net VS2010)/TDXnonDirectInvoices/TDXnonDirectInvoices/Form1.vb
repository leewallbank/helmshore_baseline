Imports System.Configuration
Public Class Form1
    Dim start_date As Date
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub


    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        
            'write out crystal report
           
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        remit_dtp.Value = DateAdd(DateInterval.Day, -Weekday(Now), Now)
        report_cbox.SelectedIndex = 0
    End Sub
    Sub update_sql(ByVal upd_txt As String)
        Try
            If conn.State = ConnectionState.Closed Then
                Connect_sqlDb()
            End If
            Dim ODBCCMD As Odbc.OdbcCommand

            'Define attachment to database table specifics
            ODBCCMD = New Odbc.OdbcCommand
            With ODBCCMD
                .Connection = conn
                .CommandText = upd_txt
                .ExecuteNonQuery()
            End With

            'Clean up the connection
            ODBCCMD = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub Connect_sqlDb()

        Try
            If Not IsNothing(conn) Then
                'This is only necessary following an exception...
                If conn.State = ConnectionState.Open Then conn.Close()
            End If
            conn.ConnectionString = ""
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("FeesSQL").ConnectionString
            conn.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings("FeesSQL").ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings("FeesSQL").ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub

    Private Sub runbtn_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click

        Try
            start_date = InputBox("Enter period start date for display on invoice (DD/MM/YYYY)", "Enter period start date")
        Catch ex As Exception
            MsgBox("Invalid period start date")
            Exit Sub
        End Try
        runbtn.Enabled = False
        exitbtn.Enabled = False
        Select Case report_cbox.SelectedIndex
            Case 0
                run_RD253_Anglia_Water()
            Case 1
                run_bt_report()
            Case 2
                run_sandwell_report()
        End Select



    End Sub
    Private Sub run_RD253_Anglia_Water()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD253Preport = New RD253P
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(remit_dtp.Value)
        SetCurrentValuesForParameterField1(RD253Preport, myArrayList1)
        myArrayList1.Add(start_date)
        SetCurrentValuesForParameterField2(RD253Preport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"

        SetDBLogonForReport(myConnectionInfo, RD253Preport)


        Dim filename As String
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RD253P TDX Anglia Water Invoice.pdf"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = SaveFileDialog1.FileName
        Else
            MsgBox("Report not saved")
            Me.Close()
            Exit Sub
        End If
        RD253Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD253Preport.close()
        If MsgBox("report saved - do you want to run another?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Me.Close()
        End If
        runbtn.Enabled = True
        exitbtn.Enabled = True
    End Sub
    Private Sub run_bt_report()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD253P2report = New RD253P2
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(remit_dtp.Value)
        SetCurrentValuesForParameterField1(RD253P2report, myArrayList1)
        myArrayList1.Add(start_date)
        SetCurrentValuesForParameterField2(RD253P2report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"

        SetDBLogonForReport(myConnectionInfo, RD253P2report)


        Dim filename As String
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RD253P2 BT non_direct Invoice.pdf"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = SaveFileDialog1.FileName
        Else
            MsgBox("Report not saved")
            Me.Close()
            Exit Sub
        End If
        RD253P2report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD253P2report.close()
        If MsgBox("report saved - do you want to run another?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Me.Close()
        End If
        runbtn.Enabled = True
        exitbtn.Enabled = True
    End Sub
    Private Sub run_sandwell_report()

        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD253SPreport = New RD253SP
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(remit_dtp.Value)
        SetCurrentValuesForParameterField1(RD253SPreport, myArrayList1)
        myArrayList1.Add(start_date)
        SetCurrentValuesForParameterField2(RD253SPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"

        SetDBLogonForReport(myConnectionInfo, RD253SPreport)


        Dim filename As String
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RD253SP Sandwell non_direct Invoice.pdf"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = SaveFileDialog1.FileName
        Else
            MsgBox("Report not saved")
            Me.Close()
            Exit Sub
        End If
        RD253SPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD253SPreport.close()
        If MsgBox("report saved - do you want to run another?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Me.Close()
        End If
        runbtn.Enabled = True
        exitbtn.Enabled = True
    End Sub
End Class

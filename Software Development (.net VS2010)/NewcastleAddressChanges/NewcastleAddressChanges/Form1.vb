﻿Public Class Form1
    Dim outfile As String = "Scheme|Client Ref|Debtor name|Liability Address|New Contact Address|Client Balance" & vbNewLine
    Dim rowCount As Integer = 0
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim env_str As String = ""
    Dim prod_run As Boolean = False
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        ConnectDb2("DebtRecovery")

        If env_str = "Prod" Then
            prod_run = True
        Else
            prod_run = False
        End If

        'get address changes for last month 
        Dim endDate As Date = CDate(Format(Now, "MMM 01, yyyy") & " 00:00:00")
        Dim startdate As Date = DateAdd(DateInterval.Month, -1, endDate)


        Dim debt_dt As New DataTable

        LoadDataTable2("DebtRecovery", "select D._rowid, D.client_ref, name_title, name_fore, name_sur," & _
                       "D.address,D.add_postcode, S.name, D.offence_Details" & _
                        " from Debtor D, note N, clientScheme CS, scheme S" & _
                       " where N.debtorID = D._rowID" & _
                        " and D.clientSchemeID = CS._rowID" & _
                        " and CS.schemeID = S._rowID " & _
                       " and CS.clientID = 1769" & _
                       " and D.status_open_closed = 'O'" & _
                       " and N.type = 'Address'" & _
                       " and N.text like 'changed from:%'" & _
                       " and N._createdDate >= '" & Format(startdate, "yyyy-MM-dd") & "'" & _
                       " and N._createdDate < ' " & Format(endDate, "yyyy-MM-dd") & "'", debt_dt, False)
        For Each debtrow In debt_dt.Rows
            Dim debtorID As Integer = debtrow(0)
            Dim clientRef As String = debtrow(1)
            Dim fullName As String = ""
            Try
                fullName = debtrow(2) & " "
            Catch ex As Exception

            End Try
            Try
                fullName &= debtrow(3) & " "
            Catch ex As Exception

            End Try
            Try
                fullName &= debtrow(4)
            Catch ex As Exception

            End Try
            'fullName = Replace(fullName, ",", " ")

            Dim Liabilityaddress As String = debtrow(8)
            Dim addressArray() As String = Liabilityaddress.Split(",")
            Dim postCode As String = ""
            Dim addLine(10) As String
            For addressidx = 0 To 10
                addLine(addressidx) = ""
            Next
            For addressIDX = 0 To UBound(addressArray)
                Dim addrLine As String = Trim(Replace(addressArray(addressIDX), "changed from:", ""))
                addLine(addressIDX) = addrLine
            Next
            'set postcode to last address line
            For addressidx = 10 To 0 Step -1
                If addLine(addressidx) <> "" Then
                    postCode = addLine(addressidx)
                    addLine(addressidx) = ""
                    Exit For
                End If
            Next
            'set last address line if more than 4
            For addressidx = 5 To 10
                If addLine(addressidx) <> "" Then
                    addLine(4) &= " " & addLine(addressidx)
                End If
            Next


            Dim newPostCode As String = ""
            Try
                newPostCode = debtrow(6)
            Catch ex As Exception

            End Try
            Dim newAddress As String = debtrow(5)
            'separate out into address lines
            Dim newAddLines(7) As String
            For addressidx = 0 To 7
                newAddLines(addressidx) = ""
            Next
            Dim addLineIDX As Integer = 0
            Dim addressline As String = ""
            For addressidx = 1 To newAddress.Length
                If Mid(newAddress, addressidx, 1) = Chr(10) Or Mid(newAddress, addressidx, 1) = Chr(13) Then
                    newAddLines(addLineIDX) = addressline
                    addLineIDX += 1
                    addressline = ""
                Else
                    addressline &= Mid(newAddress, addressidx, 1)
                End If
            Next
            'save last line
            newAddLines(addLineIDX) = addressline
            'if any address line is same as postcode - blank it out
            For addressidx = 0 To 7
                If newAddLines(addressidx) = newPostCode Then
                    newAddLines(addressidx) = ""
                End If
            Next

            Dim scheme As String = debtrow(7)
            Dim clientBal As Decimal = get_client_bal(debtorID)

            Liabilityaddress = Replace(Liabilityaddress, Chr(10), " ")
            Liabilityaddress = Replace(Liabilityaddress, Chr(13), " ")

            newAddress = Replace(newAddress, Chr(10), " ")
            newAddress = Replace(newAddress, Chr(13), " ")

            outfile &= scheme & "|" & clientRef & "|" & fullName & "|" & Liabilityaddress & "|" & newAddress & "|" & clientBal & vbNewLine

            rowCount += 1
        Next

        Dim filename As String = "\\ross-helm-fp001\Rossendales Shared\Newcastle Reports\Rossendales_Update_" & Format(Now, "yyyyMMdd") & ".csv"
        If Not prod_run Then
            filename = "C:\AAtemp\Rossendales_Update_" & Format(Now, "yyyyMMdd") & ".txt"
        End If

        My.Computer.FileSystem.WriteAllText(filename, outfile, False, ascii)
        Me.Close()
    End Sub
   
    Private Function get_client_bal(ByVal debtorID As Integer) As Decimal
        Dim client_bal As Decimal
        client_bal = GetSQLResults2("DebtRecovery", "SELECT sum(fee_amount - remited_fee) from fee " & _
                                                    " WHERE DebtorID = " & debtorID & _
                                                    " and fee_remit_col = 1")
        Return client_bal
    End Function

End Class

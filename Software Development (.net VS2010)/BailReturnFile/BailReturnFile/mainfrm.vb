Public Class mainfrm
    Dim filename, outfile As String
    Dim ascii As New System.Text.ASCIIEncoding()
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
       
        selected_scheme_name = ""
        selected_cl_name = ""
    End Sub

    Private Sub retnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Function remove_chars(ByVal txt As String) As String
        Dim new_txt As String = ""
        Dim idx As Integer
        For idx = 1 To txt.Length
            If Mid(txt, idx, 1) <> Chr(10) And Mid(txt, idx, 1) <> Chr(13) Then
                new_txt = new_txt & Mid(txt, idx, 1)
            Else
                new_txt = new_txt & " "
            End If
        Next
        Return (Trim(new_txt))
    End Function
    
    Private Sub disable_buttons()
        exitbtn.Enabled = False
        filebtn.Enabled = False
    End Sub
    Private Sub enable_buttons()
        exitbtn.Enabled = True
        filebtn.Enabled = True
    End Sub

    Private Sub filebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles filebtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                enable_buttons()
                fileok = False
                Exit Sub
            End Try
        Else
            fileok = False
            MsgBox("File not opened")
            enable_buttons()
            Exit Sub
        End If
        'do manipulation here
        Dim file As String
        Dim linetext As String = ""
        Dim line(0) As String

        Dim idx As Integer
        Dim lines As Integer = 0

        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)
        For idx = 1 To Len(file) - 1
            ProgressBar1.Value = (idx / Len(file)) * 100
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next
        disable_buttons()
        Dim debtor As Integer
        Dim grp As String
        For idx = 0 To lines - 1
            If line(idx).Length < 10 Then
                Continue For
            End If
            grp = ""
            If InStr(line(idx), "NullaBona") > 0 Then
                grp = "BAILNULL"
            Else
                Dim debtor_start As Integer = InStr(line(idx), "-")
                '13.1.2014 cater for 8 digit debtorID
                Try
                    debtor = Mid(line(idx), debtor_start + 1, 8)
                Catch ex As Exception
                    MsgBox("debtor not found on line = " & idx)
                    Exit Sub
                End Try

                'get return code from onestep
                param2 = "select return_codeID from Debtor where _rowid = " & debtor
                Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    MsgBox("Debtor number = " & debtor & " not found on onestep")
                    Exit Sub
                End If
                Dim retn_code As Integer
                Try
                    retn_code = debtor_dataset.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    Continue For
                End Try
                'get fee category from returncodes
                param2 = "select fee_category from CodeReturns where _rowid = " & retn_code
                Dim retn_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    MsgBox("Return code not found for debtor = " & debtor)
                    Exit Sub
                End If
                Dim fee_cat As Integer = retn_dataset.Tables(0).Rows(0).Item(0)
                If fee_cat = 1 Then
                    grp = "BAILQUER"
                Else
                    grp = "BAILCLO"
                End If
            End If
            Dim pdf_start As Integer = InStr(line(idx), "O:")
            Dim txt As String = Microsoft.VisualBasic.Left(line(idx), pdf_start - 1)
            txt = remove_chars(txt)
            outfile = outfile & txt & grp & vbNewLine
        Next
        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_BAIL.txt", outfile, False, ascii)
        MsgBox("New report created")

        Me.Close()

    End Sub
End Class

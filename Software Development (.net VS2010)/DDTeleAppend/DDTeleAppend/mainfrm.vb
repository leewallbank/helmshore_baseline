Imports CommonLibrary
Public Class mainfrm

    Dim outfile1 As String = ""
    Dim param1 As String = "onestep"
    Dim param2 As String

    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click
        exitbtn.Enabled = False
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "XLS files|*.xls"
            .FileName = ""
            .CheckFileExists = True
        End With
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MessageBox.Show("No file Selected")
            exitbtn.Enabled = True
            Exit Sub
        End If
        exitbtn.Enabled = False
        readbtn.Enabled = False
        ProgressBar1.Value = 5
        Application.DoEvents()
        Dim excel_vals(,) As String = InputFromExcel(OpenFileDialog1.FileName)
        ProgressBar1.Value = 10
        Application.DoEvents()
        Dim rowMax As Integer = UBound(excel_vals)
        Try
            Dim rowidx As Integer
            For rowidx = 0 To rowMax
                Try
                    ProgressBar1.Value = rowidx / rowMax * 100
                Catch ex As Exception

                End Try
                Application.DoEvents()
                If rowidx = 0 Then
                    'write out headers
                    outfile1 = "Our Ref,Phone1,Phone2,Phone3" & vbNewLine
                Else
                    If excel_vals(rowidx, 1) = " " Then
                        Exit For
                    End If
                    Dim ph1_no As String = Trim(excel_vals(rowidx, 12))
                    Dim ph2_no As String = Trim(excel_vals(rowidx, 15))
                    Dim ph3_no As String = Trim(excel_vals(rowidx, 22))
                    Dim ph1_found As Boolean = False
                    Dim ph2_found As Boolean = False
                    Dim ph3_found As Boolean = False
                    If ph1_no.Length > 4 And ph1_no <> "XD" Then
                        ph1_found = True
                    End If
                    If ph2_no.Length > 4 And ph2_no <> "XD" Then
                        ph2_found = True
                    End If
                    If ph3_no.Length > 4 And ph3_no <> "XD" Then
                        ph3_found = True
                    End If
                    If ph1_found = False Then
                        ph1_no = ph2_no
                        ph2_no = ph3_no
                        ph3_no = ""
                        If ph2_found = False Then
                            ph1_no = ph2_no
                            ph2_no = ""
                        End If
                    Else
                        If ph2_found = False Then
                            ph2_no = ph3_no
                            ph3_no = ""
                        End If
                    End If
                    If ph1_no.Length > 4 And ph1_no <> "XD" Then
                        If ph3_no.Length > 0 Then
                            ph3_no = "additional telephone number provided by thirdparty:" & ph3_no
                        End If
                        outfile1 = outfile1 & excel_vals(rowidx, 1) & "," & ph1_no & "," & ph2_no & "," & ph3_no & vbNewLine
                        'get any open linked case
                        param2 = "select linkID from Debtor where _rowid = " & excel_vals(rowidx, 1)
                        Dim debtor_dataset As DataSet = get_dataset(param1, param2)

                        If no_of_rows = 0 Then
                            MsgBox("Unable to read debtor No = " & excel_vals(rowidx, 1))
                        End If
                        Dim linkid As Integer
                        Try
                            linkid = debtor_dataset.Tables(0).Rows(0).Item(0)
                        Catch ex As Exception
                            linkid = 0
                        End Try
                        If linkid > 0 Then
                            param2 = "select _rowid from Debtor where _rowid <> " & excel_vals(rowidx, 1) & _
                                     " and status_open_closed = 'O' and linkID = " & linkid
                            Dim link_dataset As DataSet = get_dataset(param1, param2)
                            Dim idx2 As Integer
                            For idx2 = 0 To no_of_rows - 1
                                outfile1 = outfile1 & link_dataset.Tables(0).Rows(idx2).Item(0) & "," & ph1_no & "," & ph2_no & "," & ph3_no & vbNewLine
                            Next
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Exit Sub
        End Try
        Dim idx As Integer
        Dim filename_prefix As String = ""
        For idx = Len(OpenFileDialog1.FileName) To 1 Step -1
            If Mid(OpenFileDialog1.FileName, idx, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(OpenFileDialog1.FileName, idx - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.csv", outfile1, False)
        MessageBox.Show("File created successfully")
        Me.Close()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub


    Private Sub filebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

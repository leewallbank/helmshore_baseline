﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain
    Private AnglianWaterData As New clsTDXAnglianWaterData
    Private InputFilePath As String, FileName As String, FileExt As String

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try

            Dim FileDialog As New OpenFileDialog
            Dim LineNumber As Integer
            Dim OutputFile As String
            Dim AuditLog As String, ErrorLog As String = "", InputLineArray() As String, DebtorID As String

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)
            If File.Exists(InputFilePath & FileName & "_PreProcessed.txt") Then File.Delete(InputFilePath & FileName & "_PreProcessed.txt")

            lblReadingFile.Visible = True
            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents)

            WriteFile(InputFilePath & FileName & "_PreProcessed.txt", "") ' overwrite the file if it exists

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                If LineNumber = 1 Then Continue For ' skip header line

                OutputFile = ""
                InputLineArray = InputLine.Split(",")

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                DebtorID = AnglianWaterData.GetDebtorID(InputLineArray(0))

                If DebtorID <> "" Then

                    OutputFile &= DebtorID & "|"

                    If InputLineArray(1) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(1), "Move in date", ";"))
                    If InputLineArray(2) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(2), "Move out date", ";"))
                    If InputLineArray(3) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(3), "Latest bill start date", ";"))
                    If InputLineArray(4) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(4), "Vacated", ";"))
                    If InputLineArray(5) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(5), "Last invoice date", ";"))
                    If InputLineArray(6) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(6), "Last bill amount", ";"))
                    If InputLineArray(7) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(7), "Bill period end date", ";"))
                    If InputLineArray(8) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(8), "Measured / unmeasured", ";"))
                    If InputLineArray(9) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(9), "Bill paid by landlord", ";"))

                    OutputFile &= vbCrLf

                Else
                    ErrorLog &= "Cannot find DebtorID for client ref " & InputLineArray(0) & " at line number " & LineNumber.ToString & vbCrLf
                End If

                AppendToFile(InputFilePath & FileName & "_PreProcessed.txt", OutputFile)

            Next InputLine

            AuditLog = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
            AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

            AuditLog &= "Number of cases: " & UBound(FileContents).ToString & vbCrLf

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)

            MsgBox("Pre-processing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.runbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.FeesSQLDataSet = New BankAccountChanges.FeesSQLDataSet()
        Me.BankAccountsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BankAccountsTableAdapter = New BankAccountChanges.FeesSQLDataSetTableAdapters.BankAccountsTableAdapter()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BankAccountsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(94, 221)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(121, 23)
        Me.runbtn.TabIndex = 2
        Me.runbtn.Text = "Run report and email"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(271, 287)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 4
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 274)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(91, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(103, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Email to be sent to :-"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(131, 184)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(25, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "and"
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BankAccountsBindingSource
        '
        Me.BankAccountsBindingSource.DataMember = "BankAccounts"
        Me.BankAccountsBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'BankAccountsTableAdapter
        '
        Me.BankAccountsTableAdapter.ClearBeforeFill = True
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.CheckOnClick = True
        Me.CheckedListBox1.FormattingEnabled = True
        Me.CheckedListBox1.Items.AddRange(New Object() {"RDexter", "NBrandon", "KHoyle", "SMcDermott"})
        Me.CheckedListBox1.Location = New System.Drawing.Point(95, 64)
        Me.CheckedListBox1.MultiColumn = True
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.Size = New System.Drawing.Size(120, 79)
        Me.CheckedListBox1.TabIndex = 1
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(358, 363)
        Me.Controls.Add(Me.CheckedListBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bank Account change report"
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BankAccountsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents FeesSQLDataSet As BankAccountChanges.FeesSQLDataSet
    Friend WithEvents BankAccountsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BankAccountsTableAdapter As BankAccountChanges.FeesSQLDataSetTableAdapters.BankAccountsTableAdapter
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents CheckedListBox1 As System.Windows.Forms.CheckedListBox

End Class

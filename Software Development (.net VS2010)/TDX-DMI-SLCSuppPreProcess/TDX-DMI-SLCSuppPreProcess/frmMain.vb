﻿Imports CommonLibrary
Imports System.IO
Public Class frmMain
    Private TDXDMISLCData As New clsTDXDMISLCData
    Private InputFilePath As String, FileName As String, FileExt As String
    Private PersonRelationship As New Dictionary(Of String, String)

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        PersonRelationship.Add("F", "Father")
        PersonRelationship.Add("M", "Mother")
        PersonRelationship.Add("U", "Uncle")
        PersonRelationship.Add("A", "Aunt")
        PersonRelationship.Add("G", "Grandparent")
        PersonRelationship.Add("B", "Brother")
        PersonRelationship.Add("S", "Sister")
        PersonRelationship.Add("O", "Other")

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try

            Dim FileDialog As New OpenFileDialog
            Dim LineNumber As Integer
            Dim OutputLine As String
            Dim AuditLog As String, ErrorLog As String = "", InputLineArray() As String, DebtorID As String
            Dim AccountID = New List(Of String) ' added TS 08/Sep/2015. Request ref 57978

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            lblReadingFile.Visible = True
            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents)

            For Each OldFile As String In Directory.GetFiles(InputFilePath, FileName & "_*.txt")
                File.Delete(OldFile)
            Next OldFile

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                If LineNumber = 1 Or LineNumber = UBound(FileContents) + 1 Then Continue For ' skip header and trailer line

                InputLine.Replace("|", "")
                OutputLine = ""
                InputLineArray = InputLine.Split(",")

                If UBound(InputLineArray) <> 8 Then
                    ErrorLog &= "Unexpected line length of " & (UBound(InputLineArray) + 1).ToString & " items found at line number " & LineNumber.ToString & ". Line not loaded." & vbCrLf
                    Continue For
                End If

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                AccountID.Add(InputLineArray(0))

                DebtorID = TDXDMISLCData.GetDebtorID(InputLineArray(1))

                If DebtorID <> "" Then
                    OutputLine &= DebtorID & "|"

                    OutputLine &= String.Join("", ToNote(InputLineArray(4), "Brand", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(5), "1st Loan Pack", ";"))

                    If PersonRelationship.ContainsKey(InputLineArray(6)) Then
                        OutputLine &= ToNote(PersonRelationship(InputLineArray(6)), "Contact 1 Relationship", ";")(0)
                    ElseIf InputLineArray(6) <> "" Then
                        OutputLine &= ToNote("Other", "Contact 1 Relationship", ";")(0)
                    End If

                    If PersonRelationship.ContainsKey(InputLineArray(7)) Then
                        OutputLine &= ToNote(PersonRelationship(InputLineArray(7)), "Contact 2 Relationship", ";")(0)
                    ElseIf InputLineArray(7) <> "" Then
                        OutputLine &= ToNote("Other", "Contact 2 Relationship", ";")(0)
                    End If

                    OutputLine &= String.Join("", ToNote(InputLineArray(8), "Repayment Due Date", ";"))

                    OutputLine &= vbCrLf

                Else
                    ErrorLog &= "Cannot find open DebtorID for client ref " & InputLineArray(1).Replace("""", "") & " at line number " & LineNumber.ToString & vbCrLf
                End If

                AppendToFile(InputFilePath & FileName & "_PreProcessed.txt", OutputLine)

            Next InputLine

            InputLineArray = FileContents(0).Split(",") ' Check header record case count
            If AccountID.ToArray.Distinct.ToArray.Count <> InputLineArray(3).Replace("""", "") Then ErrorLog &= "Expecting " & InputLineArray(3) & " cases, found " & (AccountID.ToArray.Distinct.ToArray.Count + 1).ToString

            AuditLog = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
            AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

            AuditLog &= "Number of new supplementary details: " & (UBound(FileContents) - 1).ToString & vbCrLf

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
                MessageBox.Show("Errors found.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            MessageBox.Show("Pre-processing complete.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("notepad.exe", """" & InputFilePath & FileName & "_PreProcessed.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
            If File.Exists(InputFilePath & FileName & "_Balance_discrepancies.xls") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Balance_discrepancies.xls")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class

﻿Imports CommonLibrary

Public Class clsRemunerationData
    Private Visit As New DataTable
    Private PIF As New DataTable
    Private CGA As New DataTable
    Private RecoveredComplianceFee As New DataTable
    Private AllocationBatchDetails As New DataTable
    Private MHRVisit As New DataTable ' Added TS 14/Jun/2016. Request ref 82594
    Private CollectPayment As New DataTable ' Added TS 14/Jun/2016. Request ref 75025
    Private CGAPIF As New DataTable ' Added TS 07/Jul/2017. Request ref 112264
    Private Sage As New DataTable

    Private PIFRemitCountDT As New DataTable
    ' Private PIFBailiffCountDT As New DataTable
    Private PIFRemitCountDV As New DataView
    ' Private PIFBailiffCountDV As New DataView

    Public ReadOnly Property VisitCases() As DataTable
        Get
            VisitCases = Visit
        End Get
    End Property

    Public ReadOnly Property PIFCases() As DataTable
        Get
            PIFCases = PIF
        End Get
    End Property

    Public ReadOnly Property PIFRemitCount() As DataView
        Get
            PIFRemitCount = PIFRemitCountDV
        End Get
    End Property

    'Public ReadOnly Property PIFBailiffCount() As DataView
    '    Get
    '        PIFBailiffCount = PIFBailiffCountDV
    '    End Get
    'End Property

    Public ReadOnly Property CGACases() As DataTable
        Get
            CGACases = CGA
        End Get
    End Property

    Public ReadOnly Property RecoveredComplianceFeeCases() As DataTable
        Get
            RecoveredComplianceFeeCases = RecoveredComplianceFee
        End Get
    End Property

    Public ReadOnly Property MHRVisitCases() As DataTable ' Added TS 14/Jun/2016. Request ref 82594
        Get
            MHRVisitCases = MHRVisit
        End Get
    End Property

    Public ReadOnly Property CollectPaymentCases() As DataTable ' Added TS 14/Jun/2016. Request ref 75025
        Get
            CollectPaymentCases = CollectPayment
        End Get
    End Property

    Public ReadOnly Property CGAPIFCases() As DataTable ' Added TS 07/Jul/2017. Request ref 112264
        Get
            CGAPIFCases = CGAPIF
        End Get
    End Property

    Public ReadOnly Property AllocationBatch() As DataTable
        Get
            AllocationBatch = AllocationBatchDetails
        End Get
    End Property

    Public ReadOnly Property SageExtract() As DataTable
        Get
            SageExtract = Sage
        End Get
    End Property

    Public Function VatRate(ByVal EffectiveDate As DateTime) As Decimal
        Dim Sql As String

        VatRate = Nothing

        Try
            Sql = "EXEC dbo.GetVATRate '" & EffectiveDate.ToString("dd/MMM/yyyy") & "'"

            VatRate = GetSQLResults("BailiffRemuneration", Sql)

            VatRate = VatRate / 100

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Sub GetPIFRemitCount()
        Dim Sql As String

        Try
            Sql = "EXEC dbo.GetPIFRemitCOunt"

            LoadDataTable("BailiffRemuneration", Sql, PIFRemitCountDT, False)

            PIFRemitCountDV = New DataView(PIFRemitCountDT)
            PIFRemitCountDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    'Public Sub GetPIFBailiffCount()
    '    Dim Sql As String

    '    Try
    '        Sql = "EXEC dbo.GetPIFBailiffCOunt"

    '        LoadDataTable("BailiffRemuneration", Sql, PIFBailiffCountDT, False)

    '        PIFBailiffCountDV = New DataView(PIFBailiffCountDT)
    '        PIFBailiffCountDV.AllowNew = False

    '    Catch ex As Exception
    '        HandleException(ex)
    '    End Try
    'End Sub

    Public Sub GetSageExtract(ByVal StartPeriod As Date, ByVal EndPeriod As Date, InvoiceType As String, BranchID As Integer)
        Dim Sql As String

        Try

            Sql = "EXEC dbo.GetSageExtract '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "', '" & InvoiceType & "'," & BranchID.ToString

            LoadDataTable("BailiffRemuneration", Sql, Sage, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub ImportBailiff()
        Try
            ExecStoredProc("BailiffRemuneration", "EXEC stg.ImportBailiff", 0)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub ImportVisit(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Dim Sql As String

        Try
            ExecStoredProc("BailiffRemuneration", "EXEC stg.ImportVisit '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'", 0)

            Sql = "EXEC dbo.GetVisit"

            LoadDataTable("BailiffRemuneration", Sql, Visit, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub ImportPIF(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Dim Sql As String

        Try
            ExecStoredProc("BailiffRemuneration", "EXEC stg.ImportPIF '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'", 0)

            Sql = "EXEC dbo.GetPIF"

            LoadDataTable("BailiffRemuneration", Sql, PIF, False)

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Sub

    Public Sub ImportCGA(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Dim Sql As String

        Try
            ExecStoredProc("BailiffRemuneration", "EXEC stg.ImportCGA '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'", 0)

            Sql = "EXEC dbo.GetCGA"

            LoadDataTable("BailiffRemuneration", Sql, CGA, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub ImportRecoveredComplianceFee(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Dim Sql As String

        Try
            ExecStoredProc("BailiffRemuneration", "EXEC stg.ImportRecoveredComplianceFee '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'", 0)

            Sql = "EXEC dbo.GetRecoveredComplianceFee"

            LoadDataTable("BailiffRemuneration", Sql, RecoveredComplianceFee, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub ImportMHRVisit(ByVal StartPeriod As Date, ByVal EndPeriod As Date) ' Added TS 14/Jun/2016. Request ref 82594
        Dim Sql As String

        Try
            ExecStoredProc("BailiffRemuneration", "EXEC stg.ImportMHRVisit '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'", 0)

            Sql = "EXEC dbo.GetMHRVisit"

            LoadDataTable("BailiffRemuneration", Sql, MHRVisit, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub ImportCollectPayment(ByVal StartPeriod As Date, ByVal EndPeriod As Date) ' Added TS 14/Jun/2016. Request ref 75025
        Dim Sql As String

        Try
            ExecStoredProc("BailiffRemuneration", "EXEC stg.ImportCollectPayment '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'", 0)

            Sql = "EXEC dbo.GetCollectPayment"

            LoadDataTable("BailiffRemuneration", Sql, CollectPayment, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub ImportCGAPIF(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Dim Sql As String

        Try
            ExecStoredProc("BailiffRemuneration", "EXEC stg.ImportCGAPIF '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'", 0)

            Sql = "EXEC dbo.GetCGAPIF"

            LoadDataTable("BailiffRemuneration", Sql, CGAPIF, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub ImportSaleFee(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Dim Sql As String

        Try

            Sql = "EXEC dbo.GetPIF"

            LoadDataTable("BailiffRemuneration", Sql, PIF, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
    'Public Sub ImportArrangement(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
    '    Dim Sql As String

    '    Try
    '        'ExecStoredProc("BailiffRemuneration", "EXEC stg.ImportArrangement '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'", 0)

    '        Sql = "EXEC dbo.GetArrangement"

    '        LoadDataTable("BailiffRemuneration", Sql, Arrangement, False)

    '    Catch ex As Exception
    '        HandleException(ex)
    '    End Try
    'End Sub

    Public Sub UpdateInvoiceTotals(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Try
            ExecStoredProc("BailiffRemuneration", "EXEC dbo.UpdateInvoiceTotals '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'", 0)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetAllocationBatchDetailsPIF(ByVal CaseID As String, ByVal LinkID As String, ByVal AllocationDate As Date, ByVal BailiffID As String, ByVal FirstAllocationVisitDate As Date, EnforcementFeeDate As Date, PaymentPeriodEndDate As Date)
        Dim Sql As String
        ' EnforcementFeeDate can be NULL in the base table but this Sub is only called where enforcement fees exist so no need to handle NULL
        Try

            Sql = "SELECT SUM(IFNULL(t.ClientDebtBalance,0)) AS TotalClientDebtBalance " & _
                  "     , COUNT(CASE WHEN t.status_open_closed = 'O' THEN 1 ELSE NULL END) AS OpenCases" & _
                  "     , COUNT(CASE WHEN t.status_open_closed = 'C' THEN 1 ELSE NULL END) AS ClosedCases " & _
                  "     , MAX(t.LastPaymentDate) AS LastBatchPaymentDate " & _
                  "     , MIN(t.LastPaymentDate) AS EarliestLastBatchPaymentDate " & _
                  "FROM ( SELECT d._rowid" & _
                  "            , IFNULL(SUM(p.split_debt), 0) AS ClientDebtBalance" & _
                  "            , MAX(p.date) AS LastPaymentDate" & _
                  "            , CASE WHEN DATE(d.return_date) > '" & PaymentPeriodEndDate.ToString("yyyy-MM-dd") & "' THEN 'O' ELSE d.status_open_closed END AS status_open_closed " & _
                  "       FROM debtor AS d" & _
                  "       LEFT JOIN payment AS p ON d._rowID = p.DebtorID" & _
                  "                             AND p.status = 'R'" & _
                  "                             AND p.status_date >= '" & FirstAllocationVisitDate.ToString("yyyy-MM-dd") & "'"

            If LinkID = "" Then
                Sql &= "       WHERE d._rowID = " & CaseID
            Else
                Sql &= "       WHERE d.LinkID = " & LinkID
            End If

            Sql &= "         AND EXISTS ( SELECT 1" & _
                   "                      FROM visit AS v_s" & _
                   "                      INNER JOIN bailiff AS b_s ON v_s.BailiffID = b_s._rowID" & _
                   "                      INNER JOIN debtor AS d_s ON v_s.DebtorID = d_s._rowID" & _
                   "                      INNER JOIN fee AS f_s ON f_s.DebtorID = d_s._rowID" & _
                   "                      WHERE v_s.Debtorid = d._rowID" & _
                   "                        AND v_s.date_allocated = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _
                   "                        AND DATE(f_s.date) = '" & EnforcementFeeDate.ToString("yyyy-MM-dd") & "'" & _
                   "                        AND b_s._rowID = " & BailiffID & " " & _
                   "                        AND b_s.agent_type = 'B' " & _
                   "                        AND d_s.ClientSchemeID >= 3856 " & _
                   "                        AND f_s.type = 'Enforcement' " & _
                   "                    )" & _
                   "         AND d.ClientSchemeID >= 3856 " & _
                   "       GROUP BY d._rowid" & _
                   "              , d.status_open_closed" & _
                   "     ) t"

            'The following code commented out TS 08/Oct/2014. Request ref 32306
            'Sql = "SELECT SUM(IFNULL(t.ClientDebtBalance,0)) AS TotalClientDebtBalance " & _
            '      "     , COUNT(CASE WHEN t.status_open_closed = 'O' THEN 1 ELSE NULL END) AS OpenCases" & _
            '      "     , COUNT(CASE WHEN t.status_open_closed = 'C' THEN 1 ELSE NULL END) AS ClosedCases " & _
            '      "FROM ( SELECT d._rowid" & _
            '      "            , IFNULL((SELECT SUM(p.split_debt) FROM payment AS p WHERE p.DebtorID = d._rowid AND p.status = 'R' AND p.status_date >= '" & FirstAllocationVisitDate.ToString("yyyy-MM-dd") & "'), 0) AS ClientDebtBalance" & _
            '      "            , d.status_open_closed" & _
            '      "            , d.debt_amount" & _
            '      "       FROM debtor AS d"

            'If LinkID = "" Then
            '    Sql &= "       WHERE d._rowID = " & CaseID
            'Else
            '    Sql &= "       WHERE d.LinkID = " & LinkID
            'End If

            'Sql &= "         AND EXISTS ( SELECT 1" & _
            '       "                      FROM visit AS v_s" & _
            '       "                      INNER JOIN bailiff AS b_s ON v_s.BailiffID = b_s._rowID" & _
            '       "                      INNER JOIN debtor AS d_s ON v_s.DebtorID = d_s._rowID" & _
            '       "                      INNER JOIN fee AS f_s ON f_s.DebtorID = d_s._rowID" & _
            '       "                      WHERE v_s.Debtorid = d._rowID" & _
            '       "                        AND v_s.date_allocated = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _
            '       "                        AND DATE(f_s.date) = '" & EnforcementFeeDate.ToString("yyyy-MM-dd") & "'" & _
            '       "                        AND b_s._rowID = " & BailiffID & " " & _
            '       "                        AND b_s.agent_type = 'B' " & _
            '       "                        AND d_s.ClientSchemeID >= 3856 " & _
            '       "                        AND f_s.type = 'Enforcement' " & _
            '       "                    )" & _
            '       "         AND d.ClientSchemeID >= 3856 " & _
            '       "     ) t"

            'Sql = "SELECT MIN(CASE WHEN t.EnforcementFees IS NOT NULL THEN _rowID ELSE NULL END) AS EnforcementCase" & _
            '      "     , IFNULL(SUM(t.EnforcementFees), 0) AS EnforcementFees" & _
            '      "     , SUM(debt_amount) AS TotalDebt" & _
            '      "     , COUNT(CASE WHEN t.status_open_closed = 'O' THEN 1 ELSE NULL END) AS OpenCases" & _
            '      "     , COUNT(CASE WHEN t.status_open_closed = 'C' THEN 1 ELSE NULL END) AS ClosedCases " & _
            '      "FROM ( SELECT d._rowid" & _
            '      "            , (SELECT SUM(f.fee_amount) FROM fee AS f WHERE f.DebtorID = d._rowid AND f.fee_remit_col = 4) AS EnforcementFees" & _
            '      "            , d.status_open_closed" & _
            '      "            , d.debt_amount" & _
            '      "       FROM debtor AS d" & _
            '      "       WHERE LinkID = " & LinkID & _
            '      "         AND EXISTS ( SELECT 1" & _
            '      "                      FROM visit AS v_s" & _
            '      "                      INNER JOIN bailiff AS b_s ON v_s.BailiffID = b_s._rowID" & _
            '      "                      WHERE v_s.Debtorid = d._rowID" & _
            '      "                        AND v_s.date_allocated = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _
            '      "                        AND b_s._rowID = " & BailiffID & " " & _
            '      "                        AND b_s.hasPen = 'Y'" & _
            '      "                    )" & _
            '      "     ) t"

            LoadDataTable("DebtRecovery", Sql, AllocationBatchDetails, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetAllocationBatchDetailsCOM(ByVal CaseID As String, ByVal LinkID As String, ByVal AllocationDate As Date, ByVal BailiffID As String, PaymentPeriodEndDate As Date)
        Dim Sql As String
        ' EnforcementFeeDate can be NULL in the base table but this Sub is only called where enforcement fees exist so no need to handle NULL
        Try

            Sql = "SELECT COUNT(CASE WHEN t.status_open_closed = 'O' THEN 1 ELSE NULL END) AS OpenCases" & _
                  "     , COUNT(CASE WHEN t.status_open_closed = 'C' THEN 1 ELSE NULL END) AS ClosedCases " & _
                  "FROM ( SELECT d._rowid" & _
                  "            , CASE WHEN DATE(d.return_date) > '" & PaymentPeriodEndDate.ToString("yyyy-MM-dd") & "' THEN 'O' ELSE d.status_open_closed END AS status_open_closed " & _
                  "       FROM debtor AS d"

            If LinkID = "" Then
                Sql &= "       WHERE d._rowID = " & CaseID
            Else
                Sql &= "       WHERE d.LinkID = " & LinkID
            End If

            Sql &= "         AND EXISTS ( SELECT 1" & _
                   "                      FROM visit AS v_s" & _
                   "                      INNER JOIN bailiff AS b_s ON v_s.BailiffID = b_s._rowID" & _
                   "                      INNER JOIN debtor AS d_s ON v_s.DebtorID = d_s._rowID" & _
                   "                      WHERE v_s.Debtorid = d._rowID" & _
                   "                        AND v_s.date_allocated = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _
                   "                        AND b_s._rowID = " & BailiffID & " " & _
                   "                        AND b_s.agent_type = 'B' " & _
                   "                        AND d_s.ClientSchemeID >= 3856 " & _
                   "                    )" & _
                   "         AND d.ClientSchemeID >= 3856 " & _
                   "       GROUP BY d._rowid" & _
                   "              , d.status_open_closed" & _
                   "     ) t"

            LoadDataTable("DebtRecovery", Sql, AllocationBatchDetails, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function GetAllocationBatchPayingCase(ByVal LinkID As String, ByVal AllocationDate As Date, ByVal BailiffID As String, InvoiceType As String) As String
        Dim Sql As String
        Dim PayingCase As String

        GetAllocationBatchPayingCase = ""

        Try

            Sql = "SELECT p.CaseID " & _
                  "FROM dbo.BailiffPayment AS p " & _
                  "INNER JOIN dbo.BailiffInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                  "WHERE p.LinkID = " & LinkID & _
                  "  AND p.AllocationDate = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _
                  "  AND p.BailiffID = " & BailiffID & " " & _
                  "  AND i.InvoiceType = '" & InvoiceType & "'"

            PayingCase = GetSQLResults("BailiffRemuneration", Sql)

            If IsDBNull(PayingCase) Then PayingCase = ""

            GetAllocationBatchPayingCase = PayingCase

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Function GetAllocationBatchPayingCaseFV(ByVal LinkID As String, ByVal VisitDate As Date, ByVal BailiffID As String) As String ' created TS 10/Dec/2014 request ref 36738
        ' created TS 10/Dec/2014. Request ref 36738
        Dim Sql As String
        Dim PayingCase As String

        GetAllocationBatchPayingCaseFV = ""

        Try
            Sql = "SELECT p.CaseID " & _
                      "FROM dbo.BailiffPayment AS p " & _
                      "INNER JOIN dbo.BailiffInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                      "WHERE p.LinkID = " & LinkID & _
                      "  AND p.VisitDate = '" & VisitDate.ToString("yyyy-MM-dd") & "'" & _
                      "  AND p.BailiffID = " & BailiffID & " " & _
                      "  AND i.InvoiceType = 'FV'"

            PayingCase = GetSQLResults("BailiffRemuneration", Sql)

            If IsDBNull(PayingCase) Then PayingCase = ""

            GetAllocationBatchPayingCaseFV = PayingCase

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Function GetAllocationBatchPayingCasePIF(ByVal LinkID As String, ByVal EnforcementFeeDate As Date, ByVal BailiffID As String) As String ' created TS 14/Aug/2014 request ref 28078
        Dim Sql As String
        Dim PayingCase As String

        GetAllocationBatchPayingCasePIF = ""

        Try

            Sql = "SELECT p.CaseID " & _
                  "FROM dbo.BailiffPayment AS p " & _
                  "INNER JOIN dbo.BailiffInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                  "WHERE p.LinkID = " & LinkID & _
                  "  AND p.EnforcementFeeDate = '" & EnforcementFeeDate.ToString("yyyy-MM-dd") & "'" & _
                  "  AND p.BailiffID = " & BailiffID & " " & _
                  "  AND i.InvoiceType = 'PIF'"

            PayingCase = GetSQLResults("BailiffRemuneration", Sql)

            If IsDBNull(PayingCase) Then PayingCase = ""

            GetAllocationBatchPayingCasePIF = PayingCase

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Function GetAllocationBatchPayingCaseCGA(ByVal CaseID As String, ByVal LinkID As String, ByVal VisitDate As Date, ByVal BailiffID As String) As String ' created TS 13/Aug/2014 request ref 25433
        ' AllocationDate removed TS 06/Oct/2014. Request ref 32153
        Dim Sql As String
        Dim PayingCase As String

        GetAllocationBatchPayingCaseCGA = ""

        Try
            If LinkID = "" Then

                Sql = "SELECT p.CaseID " & _
                      "FROM dbo.BailiffPayment AS p " & _
                      "INNER JOIN dbo.BailiffInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                      "WHERE p.CaseID = " & CaseID & _
                      "  AND p.VisitDate = '" & VisitDate.ToString("yyyy-MM-dd") & "'" & _
                      "  AND p.BailiffID = " & BailiffID & " " & _
                      "  AND i.InvoiceType = 'CGA'"
                '"  AND p.AllocationDate = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _
            Else

                Sql = "SELECT p.CaseID " & _
                      "FROM dbo.BailiffPayment AS p " & _
                      "INNER JOIN dbo.BailiffInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                      "WHERE p.LinkID = " & LinkID & _
                      "  AND p.VisitDate = '" & VisitDate.ToString("yyyy-MM-dd") & "'" & _
                      "  AND p.BailiffID = " & BailiffID & " " & _
                      "  AND i.InvoiceType = 'CGA'"
                '"  AND p.AllocationDate = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _

            End If

            PayingCase = GetSQLResults("BailiffRemuneration", Sql)

            If IsDBNull(PayingCase) Then PayingCase = ""

            GetAllocationBatchPayingCaseCGA = PayingCase

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Function GetAllocationBatchTotalPaymentCOM(ByVal CaseID As String, ByVal LinkID As String, ByVal AllocationDate As Date, ByVal BailiffID As String) As Decimal ' created TS 27/Oct/2014 request ref 33864
        Dim Sql As String

        GetAllocationBatchTotalPaymentCOM = 0

        Try
            If LinkID = "" Then ' This is only called when LinkID is not null but I have left this in just in case this changes.

                Sql = "SELECT ISNULL(SUM(p.PaymentAmount),0) " & _
                      "FROM ( SELECT p_s.CaseID " & _
                      "            , p_s.PaymentAmount " & _
                      "       FROM dbo.BailiffPayment AS p_s " & _
                      "       INNER JOIN dbo.BailiffInvoice AS i_s ON p_s.InvoiceNumber = i_s.InvoiceNumber " & _
                      "       WHERE p_s.CaseID = " & CaseID & _
                      "         AND p_s.AllocationDate = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _
                      "         AND p_s.BailiffID = " & BailiffID & " " & _
                      "         AND i_s.InvoiceType = 'COM'" & _
                      "     ) AS p"
            Else

                Sql = "SELECT ISNULL(SUM(p.PaymentAmount),0) " & _
                      "FROM ( SELECT p_s.CaseID " & _
                      "            , p_s.PaymentAmount " & _
                      "       FROM dbo.BailiffPayment AS p_s " & _
                      "       INNER JOIN dbo.BailiffInvoice AS i_s ON p_s.InvoiceNumber = i_s.InvoiceNumber " & _
                      "       WHERE p_s.LinkID = " & LinkID & _
                      "         AND p_s.AllocationDate = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _
                      "         AND p_s.BailiffID = " & BailiffID & " " & _
                      "         AND i_s.InvoiceType = 'COM'" & _
                      "     ) AS p"

                '    Sql = "SELECT ISNULL(SUM(p.PaymentAmount),0) " & _
                '         "FROM ( SELECT p_s.CaseID " & _
                '         "            , p_s.PaymentAmount " & _
                '         "       FROM BailiffRemuneration.dbo.BailiffPayment AS p_s " & _
                '         "       INNER JOIN BailiffRemuneration.dbo.BailiffInvoice AS i_s ON p_s.InvoiceNumber = i_s.InvoiceNumber " & _
                '         "       WHERE p_s.CaseID = " & CaseID & _
                '         "         AND p_s.AllocationDate = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _
                '         "         AND p_s.BailiffID = " & BailiffID & " " & _
                '         "         AND i_s.InvoiceType = 'COM'" & _
                '         "       UNION ALL " & _
                '         "       SELECT tp_s.CaseID " & _
                '         "            , tp_s.PaymentAmount " & _
                '         "       FROM TestBailiffRemuneration.dbo.BailiffPayment AS tp_s " & _
                '         "       INNER JOIN TestBailiffRemuneration.dbo.BailiffInvoice AS ti_s ON tp_s.InvoiceNumber = ti_s.InvoiceNumber " & _
                '         "       WHERE tp_s.CaseID = " & CaseID & _
                '         "         AND tp_s.AllocationDate = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _
                '         "         AND tp_s.BailiffID = " & BailiffID & " " & _
                '         "         AND ti_s.InvoiceType = 'COM'" & _
                '         "     ) AS p"
                'Else

                '    Sql = "SELECT ISNULL(SUM(p.PaymentAmount),0) " & _
                '          "FROM ( SELECT p_s.CaseID " & _
                '          "            , p_s.PaymentAmount " & _
                '          "       FROM BailiffRemuneration.dbo.BailiffPayment AS p_s " & _
                '          "       INNER JOIN BailiffRemuneration.dbo.BailiffInvoice AS i_s ON p_s.InvoiceNumber = i_s.InvoiceNumber " & _
                '          "       WHERE p_s.LinkID = " & LinkID & _
                '          "         AND p_s.AllocationDate = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _
                '          "         AND p_s.BailiffID = " & BailiffID & " " & _
                '          "         AND i_s.InvoiceType = 'COM'" & _
                '          "       UNION ALL " & _
                '          "       SELECT tp_s.CaseID " & _
                '          "            , tp_s.PaymentAmount " & _
                '          "       FROM TestBailiffRemuneration.dbo.BailiffPayment AS tp_s " & _
                '          "       INNER JOIN TestBailiffRemuneration.dbo.BailiffInvoice AS ti_s ON tp_s.InvoiceNumber = ti_s.InvoiceNumber " & _
                '          "       WHERE tp_s.LinkID = " & LinkID & _
                '          "         AND tp_s.AllocationDate = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _
                '          "         AND tp_s.BailiffID = " & BailiffID & " " & _
                '          "         AND ti_s.InvoiceType = 'COM'" & _
                '          "     ) AS p"
            End If

            GetAllocationBatchTotalPaymentCOM = GetSQLResults("BailiffRemuneration", Sql)

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Function GetCaseCGACount(ByVal CaseID As String) As Integer
        Dim Sql As String

        GetCaseCGACount = 0

        Try

            Sql = "SELECT COUNT(*) " & _
                  "FROM Visit AS v " & _
                  "WHERE v.DebtorID = " & CaseID & _
                  "  AND v.signed_wp = 'Y'"

            GetCaseCGACount = GetSQLResults("DebtRecovery", Sql)

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Function GetPayingCase(ByVal CaseID As String, InvoiceType As String) As String
        Dim Sql As String
        Dim PayingCase As String

        GetPayingCase = ""
        ' UNION added TS 04/Nov/2014. Request ref 34569
        Try

            Sql = "SELECT p.CaseID " & _
                  "FROM BailiffRemuneration.dbo.BailiffPayment AS p " & _
                  "INNER JOIN BailiffRemuneration.dbo.BailiffInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                  "WHERE p.CaseID = " & CaseID & _
                  "  AND i.InvoiceType = '" & InvoiceType & "'" & _
                  "UNION " & _
                  "SELECT tp.CaseID " & _
                  "FROM TestBailiffRemuneration.dbo.BailiffPayment AS tp " & _
                  "INNER JOIN TestBailiffRemuneration.dbo.BailiffInvoice AS ti ON tp.InvoiceNumber = ti.InvoiceNumber " & _
                  "WHERE tp.CaseID = " & CaseID & _
                  "  AND ti.InvoiceType = '" & InvoiceType & "'"

            PayingCase = GetSQLResults("BailiffRemuneration", Sql)

            If IsDBNull(PayingCase) Then PayingCase = ""

            GetPayingCase = PayingCase

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Function GetPayingCaseCOM(ByVal CaseID As String, InternalExternal As String) As String
        Dim Sql As String
        Dim PayingCase As String
        ' created TS 25/Nov/2014. Request ref 35859
        ' InternalExternal added TS 11/Dec/2014/ Request ref 
        GetPayingCaseCOM = ""

        Try

            Sql = "SELECT p.CaseID " & _
                  "FROM dbo.BailiffPayment AS p " & _
                  "INNER JOIN dbo.BailiffInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                  "INNER JOIN dbo.Bailiff AS b ON i.BailiffID = b.BailiffID " & _
                  "WHERE p.CaseID = " & CaseID & _
                  "  AND i.InvoiceType = 'COM'" & _
                  "  AND b.internalExternal = '" & InternalExternal & "'"

            PayingCase = GetSQLResults("BailiffRemuneration", Sql)

            If IsDBNull(PayingCase) Then PayingCase = ""

            GetPayingCaseCOM = PayingCase

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Function GetPayingCaseFV(ByVal CaseID As String, StartPeriod As Date, ByVal BailiffID As Integer) As String
        Dim Sql As String
        Dim PayingCase As String

        GetPayingCaseFV = ""

        Try

            'Sql = "SELECT p.CaseID " & _
            '"FROM dbo.BailiffPayment AS p " & _
            '"INNER JOIN dbo.BailiffInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
            '"WHERE p.CaseID = " & CaseID & _
            '"  AND p.AllocationDate = '" & AllocationDate.ToString("yyyy-MM-dd") & "' " & _
            '"  AND i.InvoiceType = 'FV'"

            ' Amended TS 04/Nov/2014. Request ref 34525
            Sql = "SELECT p.CaseID " & _
                  "FROM dbo.BailiffPayment AS p " & _
                  "INNER JOIN dbo.BailiffInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                  "WHERE p.CaseID = " & CaseID & _
                  "  AND i.StartPeriod = '" & StartPeriod.ToString("yyyy-MM-dd") & "' " & _
                  "  AND i.InvoiceType = 'FV'" & _
                  "  AND i.BailiffID = " & BailiffID.ToString ' Added TS 26/Sep/2016. Request ref 91792

            PayingCase = GetSQLResults("BailiffRemuneration", Sql)

            If IsDBNull(PayingCase) Then PayingCase = ""

            GetPayingCaseFV = PayingCase

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Function GetAllocationBatchPayingCaseCGAPIF(ByVal CaseID As String, ByVal LinkID As String, ByVal VisitDate As Date, ByVal BailiffID As String) As String
        ' AllocationDate removed TS 06/Oct/2014. Request ref 32153
        Dim Sql As String
        Dim PayingCase As String

        GetAllocationBatchPayingCaseCGAPIF = ""

        Try
            If LinkID = "" Then

                Sql = "SELECT p.CaseID " & _
                      "FROM dbo.BailiffPayment AS p " & _
                      "INNER JOIN dbo.BailiffInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                      "WHERE p.CaseID = " & CaseID & _
                      "  AND p.VisitDate = '" & VisitDate.ToString("yyyy-MM-dd") & "'" & _
                      "  AND p.BailiffID = " & BailiffID & " " & _
                      "  AND i.InvoiceType = 'CGAPIF'"
            Else

                Sql = "SELECT p.CaseID " & _
                      "FROM dbo.BailiffPayment AS p " & _
                      "INNER JOIN dbo.BailiffInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                      "WHERE p.LinkID = " & LinkID & _
                      "  AND p.VisitDate = '" & VisitDate.ToString("yyyy-MM-dd") & "'" & _
                      "  AND p.BailiffID = " & BailiffID & " " & _
                      "  AND i.InvoiceType = 'CGAPIF'"


            End If

            PayingCase = GetSQLResults("BailiffRemuneration", Sql)

            If IsDBNull(PayingCase) Then PayingCase = ""

            GetAllocationBatchPayingCaseCGAPIF = PayingCase

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Function GetAllocationBatchPayingCaseSaleFee(ByVal LinkID As String, ByVal EnforcementFeeDate As Date, ByVal BailiffID As String) As String ' created TS 09/Aug/2017. Request ref 114817
        Dim Sql As String
        Dim PayingCase As String

        GetAllocationBatchPayingCaseSaleFee = ""

        Try

            Sql = "SELECT p.CaseID " & _
                  "FROM dbo.BailiffPayment AS p " & _
                  "INNER JOIN dbo.BailiffInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                  "WHERE p.LinkID = " & LinkID & _
                  "  AND p.EnforcementFeeDate = '" & EnforcementFeeDate.ToString("yyyy-MM-dd") & "'" & _
                  "  AND p.BailiffID = " & BailiffID & " " & _
                  "  AND i.InvoiceType = 'SF'"

            PayingCase = GetSQLResults("BailiffRemuneration", Sql)

            If IsDBNull(PayingCase) Then PayingCase = ""

            GetAllocationBatchPayingCaseSaleFee = PayingCase

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Sub AddBailiffPayment(ByVal CaseID As String, _
                                 ByVal LinkID As String, _
                                 ByVal BailiffID As String, _
                                 ByVal ClientID As String, _
                                 ByVal WorkTypeID As String, _
                                 ByVal VisitID As String, _
                                 ByVal VisitDate As Date, _
                                 ByVal AllocationDate As Date, _
                                 ByVal EnforcementFeeDate As Nullable(Of Date), _
                                 ByVal DebriefDate As Date, _
                                 ByVal InvoiceNumber As String, _
                                 ByVal InvoiceDate As Date, _
                                 ByVal BasicPayment As Decimal, _
                                 ByVal LargeBalance As Decimal, _
                                 ByVal Quality As Decimal, _
                                 ByVal Productivity As Decimal, _
                                 ByVal WorkingAwayUplift As Decimal, _
                                 ByVal Vat As Decimal, _
                                 ByVal StartPeriod As Date, _
                                 ByVal EndPeriod As Date, _
                                 ByVal BailiffSageAccount As String, _
                                 ByVal InvoiceType As String)

        ' EnforcementFeeDate added TS 14/Aug/2014 request ref 28078
        ' WorkingAwayUplift added TS 19/Mar/2015 request ref 22167

        If LinkID = "" Then LinkID = "NULL"

        Dim Sql As String = "EXEC dbo.AddBailiffPayment " & CaseID & _
                                                        ", " & LinkID & _
                                                        ", " & BailiffID & _
                                                        ", " & ClientID & _
                                                        ", " & WorkTypeID & _
                                                        ", " & VisitID & _
                                                        ", '" & VisitDate.ToString("dd/MMM/yyyy") & "'" & _
                                                        ", '" & AllocationDate.ToString("dd/MMM/yyyy") & "'"

        If Not IsNothing(EnforcementFeeDate) Then
            Sql &= ",'" & CDate(EnforcementFeeDate).ToString("dd/MMM/yyyy") & "'"
        Else
            Sql &= ",NULL"
        End If


        Sql &= ", '" & DebriefDate.ToString("dd/MMM/yyyy") & "'" & _
               ", '" & InvoiceNumber & "'" & _
               ", '" & InvoiceDate.ToString("dd/MMM/yyyy") & "'" & _
               ", " & BasicPayment.ToString("0.00") & _
               ", " & LargeBalance.ToString("0.00") & _
               ", " & Quality.ToString("0.00") & _
               ", " & Productivity.ToString("0.00") & _
               ", " & WorkingAwayUplift.ToString("0.00") & _
               ", " & Vat.ToString("0.00") & _
               ", '" & StartPeriod.ToString("dd/MMM/yyyy") & "'" & _
               ", '" & EndPeriod.ToString("dd/MMM/yyyy") & "'" & _
               ", '" & BailiffSageAccount & "'" & _
               ", '" & InvoiceType & "'"

        ExecStoredProc("BailiffRemuneration", Sql, 0)
    End Sub

    Public Sub UpdateBailiffPerformanceTriggers(ByVal BailiffID As Integer, _
                                                ByVal Turnaround As String, _
                                                ByVal Complaints As String, _
                                                ByVal FieldAdministration As String, _
                                                ByVal BWV As String, _
                                                ByVal Visited As String, _
                                                ByVal Productivity As Integer)

        Dim Sql As String = "EXEC dbo.UpdateBailiffPerformanceTriggers " & BailiffID & _
                                                                    ", '" & Turnaround & "'" & _
                                                                    ", '" & Complaints & "'" & _
                                                                    ", '" & FieldAdministration & "'" & _
                                                                    ", '" & BWV & "'" & _
                                                                    ", '" & Visited & "'" & _
                                                                    ", " & Productivity

        ExecStoredProc("BailiffRemuneration", Sql, 0)

    End Sub

    Public Function GetSaleFeeCount(ByVal CaseID As String) As Integer
        Dim Sql As String

        GetSaleFeeCount = 0

        Try

            Sql = "SELECT COUNT(*) " & _
                  "FROM fee AS f " & _
                  "WHERE f.DebtorID = " & CaseID & _
                  "  AND f.type = 'Sale or disposal' " & _
                  "  AND f.fee_amount > 0"

            GetSaleFeeCount = GetSQLResults("DebtRecovery", Sql)

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function
End Class


﻿Imports CommonLibrary
Imports System.Xml.Schema

Public Class Form1
    Dim start_date As Date
    Dim end_date As Date
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim prod_run As Boolean = False
    Dim lastDataChangeDate As Date
    Dim totalRecords As Integer
    Dim error_no As Integer = 0
    Dim xml_valid As Boolean = True
    Dim auditFile As String = ""
    Dim clientID As Integer = 1417
    Dim env_str As String = ""
    Dim nulldate As Date = CDate("Jan 1, 1900")
      
    Dim filename As String = "BailiffSheriffChanges_CTAX_" & Format(Now, "ddMMyyyy") & ".xml"
    Dim outfile As String = "\\ross-helm-fp001\Rossendales Shared\Torbay XML\" & filename



    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try

        If Weekday(Now) = 6 Then   'Friday
            end_date = Now
        ElseIf Weekday(Now) = 7 Then
            end_date = DateAdd(DateInterval.Day, -1, Now)
        Else
            end_date = DateAdd(DateInterval.Day, -Weekday(Now) - 1, Now)
        End If

        end_date = CDate(Format(end_date, "MMM d, yyyy" & " 00:00:00"))
        start_date = DateAdd(DateInterval.Day, -7, end_date)

        If env_str = "Prod" Then
            prod_run = True
        Else
            MsgBox("Test run only")
            'clientID = 1168
            ' end_date = Now
        End If


        Dim fileDate As Date = Now
        auditFile = "\\ross-helm-fp001\Rossendales Shared\Torbay XML\CTAX_Changes_audit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        If Not prod_run Then
            outfile = "C:\AAtemp\" & filename
            auditFile = "C:\AAtemp\Torbay_Changes_CTAX_audit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        End If
        Dim writer As New Xml.XmlTextWriter(outfile, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)

        write_audit("report started at " & Now, False)

        writer.Formatting = Xml.Formatting.Indented
        writer.WriteStartElement("BailiffSheriffChanges")

        writer.WriteStartElement("Header")
        writer.WriteElementString("Module", "C")
        writer.WriteElementString("ExtractDate", Format(Now, "yyyy-MM-dd"))
        writer.WriteElementString("LAName", "Torbay Council")
        writer.WriteEndElement()  'Header
        writer.WriteStartElement("ChangedRecords")
        get_changes("2", writer)  'CTAX

        writer.WriteEndElement()  'ChangedRecords
        writer.WriteStartElement("Trailer")
        writer.WriteElementString("NumberOfRecords", totalRecords)
        writer.WriteEndElement()  'Trailer
       
        writer.WriteEndElement()  'bailiffsheriffchanges
        writer.Close()

        'validate using xsd
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(outfile)
        myDocument.Schemas.Add("", "\\ross-helm-fp001\Rossendales Shared\vb.net\Torbay XSD\BailiffSheriffChanges.xsd")
        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
        write_audit(error_no & " - errors in report finished at " & Now, False)
        'write out file
        If error_no = 0 Then
            If prod_run Then
                'prod file name here

            End If
        End If
        'My.Computer.FileSystem.WriteAllText(filename, outfile, False, ascii)


        'now do NNDR
        filename = "BailiffSheriffChanges_NNDR_" & Format(Now, "ddMMyyyy") & ".xml"
        outfile = "\\ross-helm-fp001\Rossendales Shared\Torbay XML\" & filename
        auditFile = "\\ross-helm-fp001\Rossendales Shared\Torbay XML\NNDR_Changes_audit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        If Not prod_run Then
            outfile = "C:\AAtemp\" & filename
            auditFile = "C:\AAtemp\Torbay_Changes_NNDR_audit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        End If
        Dim writer2 As New Xml.XmlTextWriter(outfile, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)

        write_audit("report started at " & Now, False)

        writer2.Formatting = Xml.Formatting.Indented
        writer2.WriteStartElement("BailiffSheriffChanges")

       
        writer2.WriteStartElement("Header")
        writer2.WriteElementString("Module", "N")
        writer2.WriteElementString("ExtractDate", Format(Now, "yyyy-MM-dd"))
        writer2.WriteElementString("LAName", "Torbay Council")
        writer2.WriteEndElement()  'Header
        writer2.WriteStartElement("ChangedRecords")
        totalRecords = 0

        get_changes("3", writer2)  'NNDR

        writer2.WriteEndElement()  'ChangedRecords
        writer2.WriteStartElement("Trailer")
        writer2.WriteElementString("NumberOfRecords", totalRecords)
        writer2.WriteEndElement()  'Trailer
        writer2.WriteEndElement()  'bailiffsheriffchanges
        writer2.Close()

        'validate using xsd
        Dim myDocument2 As New Xml.XmlDocument
        myDocument2.Load(outfile)
        myDocument2.Schemas.Add("", "\\ross-helm-fp001\Rossendales Shared\vb.net\Torbay XSD\BailiffSheriffChanges.xsd")
        Dim eventHandler2 As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler2)
        write_audit(error_no & " - errors in report finished at " & Now, False)
        'write out file
        If error_no = 0 Then
            If prod_run Then
                'prod file name here

            End If
        End If
        Me.Close()
    End Sub
    Private Sub get_changes(ByVal workType As String, ByVal writer As Xml.XmlTextWriter)

        'get address changes for date interval
        Dim lastDebtorID As Integer = 0
        Dim addressChangeFound As Boolean
        Dim debt_dt As New DataTable
        LoadDataTable("DebtRecovery", "select D._rowid, D.client_ref,D.address,D.add_postcode, offencefrom, N.text, name_title," & _
                       " name_fore, name_sur, debt_original from Debtor D, note N, clientScheme CS, scheme S" & _
                       " where N.debtorID = D._rowID" & _
                       " and D.clientSchemeID = CS._rowID" & _
                       " and CS.schemeID = S._rowID" & _
                       " and S.work_type = " & workType & _
                       " and CS.clientID =" & clientID & _
                       " and N.type = 'Address'" & _
                       " and N._createdDate >= '" & Format(start_date, "yyyy-MM-dd") & "'" & _
                       " and N._createdDate < ' " & Format(end_date, "yyyy-MM-dd") & "'" & _
                       " order by D._rowID, N.text", debt_dt, False)
        For Each debtrow In debt_dt.Rows
            Dim debtorID As Integer = debtrow(0)
            If debtorID <> lastDebtorID Then
                lastDebtorID = debtorID
                addressChangeFound = False
            End If
            Dim clientRef As String = debtrow(1)
            Dim address As String = debtrow(2)
            Dim postcode As String = ""
            Try
                postcode = debtrow(3)
            Catch ex As Exception

            End Try
            Dim offenceFrom As Date
            Try
                offenceFrom = debtrow(4)
            Catch ex As Exception

            End Try
            Dim notetext As String = debtrow(5)
            Dim reason As String = ""
            If Microsoft.VisualBasic.Left(notetext, 13) = "changed from:" Then
                reason = "CON"
                addressChangeFound = True
            ElseIf Microsoft.VisualBasic.Left(notetext, 13) = "cleaned from:" And addressChangeFound = False Then
                reason = "COR"
            Else
                Continue For
            End If

            'remove postcode from address
            address = Replace(address, postcode, "")
            'split address into address lines

            Dim addressLine1 As String = ""
            Dim addressLine2 As String = ""
            Dim addressLine3 As String = ""
            Dim addressLine4 As String = ""
            Dim addressNo As Integer = 1
            Dim addressLine As String = ""
            For idx = 1 To address.Length
                If Mid(address, idx, 1) = "," Or Mid(address, idx, 1) = Chr(10) Or Mid(address, idx, 1) = Chr(13) Then
                    Select Case addressNo
                        Case 1
                            addressLine1 = addressLine
                            addressLine = ""
                        Case 2
                            addressLine2 = addressLine
                            addressLine = ""
                        Case 3
                            addressLine3 = addressLine
                            addressLine = ""
                        Case 4
                            addressLine4 = addressLine
                            addressLine = ""
                        Case Else
                            addressLine4 &= " " & addressLine
                    End Select
                    addressNo += 1
                Else
                    addressLine &= Mid(address, idx, 1)
                End If
            Next
            totalRecords += 1
            writer.WriteStartElement("ChangedRecord")

            writer.WriteElementString("AccountReference", clientRef)
            writer.WriteElementString("BailiffSheriffRef", debtorID)
            writer.WriteElementString("RecoveryControl", Format(offenceFrom, "yyyy/MM"))
            writer.WriteStartElement("Associates")
            writer.WriteStartElement("Associate")
            writer.WriteStartElement("PersonName")
            Dim nameTitle As String = " "
            Try
                nameTitle = debtrow(6)

            Catch ex As Exception

            End Try
            Dim nameFore As String = ""
            Try
                nameFore = debtrow(7)

            Catch ex As Exception

            End Try
            Dim namesur As String = ""
            Try
                namesur = debtrow(8)

            Catch ex As Exception

            End Try
            If nameTitle <> "" Then
                writer.WriteElementString("PersonTitle", nameTitle)
            End If
            If nameFore <> "" Then
                writer.WriteElementString("PersonForename", nameFore)
            End If
            writer.WriteElementString("PersonSurname", namesur)
            writer.WriteEndElement()  'PersonName
            writer.WriteStartElement("PostalAddress")
            If addressLine1 <> "" Then
                writer.WriteElementString("PostalAddressLine", addressLine1)
            End If
            If addressLine2 <> "" Then
                writer.WriteElementString("PostalAddressLine", addressLine2)
            End If
            If addressLine3 <> "" Then
                writer.WriteElementString("PostalAddressLine", addressLine3)
            End If
            If addressLine4 <> "" Then
                writer.WriteElementString("PostalAddressLine", addressLine4)
            End If
            If postcode <> "" Then
                writer.WriteElementString("PostalPostCode", postcode)
            End If
            writer.WriteEndElement()  'PostalAddress
            writer.WriteEndElement()  'Associate
            writer.WriteEndElement()  'Associates


            writer.WriteStartElement("RevisedBalance")
            Dim clientBal As Decimal = get_client_bal(debtorID)
            Dim paidToDate As Decimal = paid_to_date(debtorID)
            writer.WriteElementString("RecoveryBalance", clientBal)
            writer.WriteElementString("PaidToDate", paidToDate)
            writer.WriteElementString("Reason", reason)
            Dim origAmt As Decimal = debtrow(9)
            writer.WriteElementString("OriginalLOAmount", origAmt)
            writer.WriteElementString("OriginalCostsAmount", "0")
            writer.WriteEndElement()  'revisedBalance
            writer.WriteEndElement()  'ChangedRecord

        Next

        'get any debt adjustments in the period
        Dim debtadj_dt As New DataTable
        LoadDataTable("DebtRecovery", "select D._rowid, D.client_ref, D.debt_original, N.text, D.offenceFrom, name_title," & _
                       " name_fore, name_sur,N._createdDate from Debtor D, note N, clientScheme CS, scheme S" & _
                       " where N.debtorID = D._rowID" & _
                       " and D.clientSchemeID = CS._rowID" & _
                       " and CS.schemeID = S._rowID " & _
                       " and S.work_type = " & workType & _
                       " and CS.clientID =" & clientID & _
                       " and N.type = 'Debt Changed'" & _
                       " and N._createdDate >= '" & Format(start_date, "yyyy-MM-dd") & "'" & _
                       " and N._createdDate < ' " & Format(end_date, "yyyy-MM-dd") & "'", debtadj_dt, False)
        For Each debtrow In debtadj_dt.Rows
            Dim debtorID As Integer = debtrow(0)
            Dim clientRef As String = debtrow(1)
            Dim debtOriginal As Decimal = debtrow(2)
            Dim noteText As String = debtrow(3)
            Dim offenceFrom As Date = nulldate
            Try
                offenceFrom = debtrow(4)
            Catch ex As Exception

            End Try

            Dim transactionAmt As Decimal = 0
            If Microsoft.VisualBasic.Left(noteText, 15) = "Debt Reduced by" Then
                Dim wasidx As Integer = InStr(noteText, "was")
                If wasidx > 0 Then
                    Try
                        transactionAmt = Mid(noteText, 16, wasidx - 16) * -1
                    Catch ex As Exception

                    End Try
                End If
            Else
                If Microsoft.VisualBasic.Left(noteText, 17) = "Debt Increased by" Then
                    Dim wasidx As Integer = InStr(noteText, "was")
                    If wasidx > 0 Then
                        Try
                            transactionAmt = Mid(noteText, 18, wasidx - 18)
                        Catch ex As Exception

                        End Try
                    End If
                End If
            End If
            writer.WriteStartElement("ChangedRecord")

            writer.WriteElementString("AccountReference", clientRef)
            writer.WriteElementString("BailiffSheriffRef", debtorID)
            writer.WriteElementString("RecoveryControl", Format(offenceFrom, "yyyy/MM"))
            writer.WriteStartElement("Associates")
            writer.WriteStartElement("Associate")
            writer.WriteStartElement("PersonName")
            Dim nameTitle As String = ""
            Try
                nameTitle = debtrow(5)

            Catch ex As Exception

            End Try
            Dim nameFore As String = ""
            Try
                nameFore = debtrow(6)

            Catch ex As Exception

            End Try
            Dim namesur As String = ""
            Try
                namesur = debtrow(7)

            Catch ex As Exception

            End Try
            If nameTitle <> "" Then
                writer.WriteElementString("PersonTitle", nameTitle)
            End If
            If nameFore <> "" Then
                writer.WriteElementString("PersonForename", nameFore)
            End If
            writer.WriteElementString("PersonSurname", namesur)
            writer.WriteEndElement()  'PersonName
            writer.WriteEndElement()  'Associate
            writer.WriteEndElement()  'Associates

            writer.WriteStartElement("RevisedBalance")
            Dim clientBal As Decimal = get_client_bal(debtorID)
            Dim paidToDate As Decimal = paid_to_date(debtorID)
            writer.WriteElementString("RecoveryBalance", clientBal)
            writer.WriteElementString("PaidToDate", paidToDate)
            writer.WriteElementString("Reason", "ADJ")
            writer.WriteStartElement("Transaction")
            writer.WriteElementString("TransactionType", "DEBT")
            writer.WriteElementString("TransactionSubType", "")
            writer.WriteElementString("TransactionAmount", transactionAmt)
            Dim transactiondate As Date = debtrow(8)
            writer.WriteElementString("TransactionDate", Format(transactiondate, "dd-MM-yyyy"))
            writer.WriteEndElement()  'transaction
            writer.WriteElementString("OriginalLOAmount", debtOriginal)
            writer.WriteElementString("OriginalCostsAmount", "0")


            writer.WriteEndElement()  'RevisedBalance
            writer.WriteEndElement()  'ChangedRecord

        Next


        ''now get phone and name changes
        Dim phone_dt As New DataTable
        LoadDataTable("StudentLoans", "select BuildDate, add_phone, add_fax, empphone,empfax, name_title,name_fore, name_sur, debtorID" & _
                       " from rpt.TorbayDebtor" & _
                       " where BuildDate >= '" & start_date.ToString("yyyy-MMM-dd") & "'" & _
                       " and BuildDate < '" & end_date.ToString("yyyy-MMM-dd") & "'", phone_dt, False)
        For Each phoneRow In phone_dt.Rows
            Dim nameChange As Boolean = False
            Dim addPhone As String = ""
            Try
                addPhone = phoneRow(1)
            Catch ex As Exception

            End Try
            Dim addFax As String = ""
            Try
                addFax = phoneRow(2)
            Catch ex As Exception

            End Try
            Dim empPhone As String = ""
            Try
                empPhone = phoneRow(3)
            Catch ex As Exception

            End Try
            Dim empFax As String = ""
            Try
                empFax = phoneRow(4)
            Catch ex As Exception

            End Try
            Dim nameTitle As String = ""
            Try
                nameTitle = phoneRow(5)
                nameChange = True
            Catch ex As Exception

            End Try
            Dim nameFore As String = ""
            Try
                nameFore = phoneRow(6)
                nameChange = True
            Catch ex As Exception

            End Try
            Dim namesur As String = ""
            Try
                namesur = phoneRow(7)
                nameChange = True
            Catch ex As Exception

            End Try



            Dim debtorID As Integer = phoneRow(8)
            Dim array As Object = GetSQLResultsArray("DebtRecovery", "select client_ref, offenceFrom, name_title, name_fore, " & _
                                                     " name_sur, debt_original from debtor D, clientscheme CS, scheme S " & _
                                                    " where D._rowID = " & debtorID & _
                                                    " and D.clientschemeID=CS._rowID" & _
                                                    " and CS.schemeID = S._rowID" & _
                                                    " and S.work_type = " & workType)
            Dim clientRef As String = ""
            Try
                clientRef = array(0)
            Catch ex As Exception

            End Try
            If clientRef = "" Then
                Continue For
            End If
            Dim offenceFrom As Date
            Try
                offenceFrom = array(1)
            Catch ex As Exception

            End Try

            If nameChange Then
                totalRecords += 1
                writer.WriteStartElement("ChangedRecord")

                writer.WriteElementString("AccountReference", clientRef)
                writer.WriteElementString("BailiffSheriffRef", debtorID)
                writer.WriteElementString("RecoveryControl", Format(offenceFrom, "yyyy/MM"))
                writer.WriteStartElement("Associates")
                writer.WriteStartElement("Associate")
                writer.WriteStartElement("PersonName")
                If nameTitle <> "" Then
                    writer.WriteElementString("PersonTitle", nameTitle)
                End If
                If nameFore <> "" Then
                    writer.WriteElementString("PersonForename", nameFore)
                End If
                writer.WriteElementString("PersonSurname", namesur)
                writer.WriteEndElement()  'PersonName
                writer.WriteEndElement()  'associate
                writer.WriteEndElement()  'associates
                writer.WriteStartElement("RevisedBalance")
                Dim clientBal As Decimal = get_client_bal(debtorID)
                Dim paidToDate As Decimal = paid_to_date(debtorID)
                writer.WriteElementString("RecoveryBalance", clientBal)
                writer.WriteElementString("PaidToDate", paidToDate)
                writer.WriteElementString("Reason", "CON")
                Dim origAmt As Decimal = array(5)
                writer.WriteElementString("OriginalLOAmount", origAmt)
                writer.WriteElementString("OriginalCostsAmount", "0")
                writer.WriteEndElement()  'revisedBalance
                writer.WriteEndElement()  'ChangedRecord
            Else
                'for phone changes - use name from onestep
                If nameTitle = "" Then
                    Try
                        nameTitle = array(2)
                    Catch ex As Exception

                    End Try

                End If
                If nameFore = "" Then
                    Try
                        nameFore = array(3)
                    Catch ex As Exception

                    End Try

                End If
                If namesur = "" Then
                    Try
                        namesur = array(4)
                    Catch ex As Exception
                        namesur = "Unknown "
                    End Try

                End If
            End If




            If addPhone.Length > 5 Then
                
                totalRecords += 1
                writer.WriteStartElement("ChangedRecord")

                writer.WriteElementString("AccountReference", clientRef)
                writer.WriteElementString("BailiffSheriffRef", debtorID)
                writer.WriteElementString("RecoveryControl", Format(offenceFrom, "yyyy/MM"))
                writer.WriteStartElement("Associates")
                writer.WriteStartElement("Associate")
                writer.WriteStartElement("PersonName")
                If nameTitle <> "" Then
                    writer.WriteElementString("PersonTitle", nameTitle)
                End If
                If nameFore <> "" Then
                    writer.WriteElementString("PersonForename", nameFore)
                End If
                writer.WriteElementString("PersonSurname", namesur)
                writer.WriteEndElement()  'PersonName
                writer.WriteStartElement("AssociateContactDetails")
                If Microsoft.VisualBasic.Left(addPhone, 2) = "07" Then
                    writer.WriteElementString("ACDescription", "Mobile Telephone Number")
                    writer.WriteElementString("ACType", "Mobile")
                Else
                    writer.WriteElementString("ACDescription", "Telephone Number")
                    writer.WriteElementString("ACType", "Telephone")
                End If
                'remove spaces
                addPhone = Replace(addPhone, " ", "")
                writer.WriteElementString("ACNumber", addPhone)
                writer.WriteEndElement()  'AssociateContactDetails
                writer.WriteEndElement()  'associate
                writer.WriteEndElement()  'associates
                writer.WriteStartElement("RevisedBalance")
                Dim clientBal As Decimal = get_client_bal(debtorID)
                Dim paidToDate As Decimal = paid_to_date(debtorID)
                writer.WriteElementString("RecoveryBalance", clientBal)
                writer.WriteElementString("PaidToDate", paidToDate)
                writer.WriteElementString("Reason", "CON")
                Dim origAmt As Decimal = array(5)
                writer.WriteElementString("OriginalLOAmount", origAmt)
                writer.WriteElementString("OriginalCostsAmount", "0")
                writer.WriteEndElement()  'revisedBalance
                writer.WriteEndElement()  'ChangedRecord
            End If
            If addFax.Length > 5 Then
                totalRecords += 1
                writer.WriteStartElement("ChangedRecord")

                writer.WriteElementString("AccountReference", clientRef)
                writer.WriteElementString("BailiffSheriffRef", debtorID)
                writer.WriteElementString("RecoveryControl", Format(offenceFrom, "yyyy/MM"))
                writer.WriteStartElement("Associates")
                writer.WriteStartElement("Associate")
                writer.WriteStartElement("PersonName")
                If nameTitle <> "" Then
                    writer.WriteElementString("PersonTitle", nameTitle)
                End If
                If nameFore <> "" Then
                    writer.WriteElementString("PersonForename", nameFore)
                End If
                writer.WriteElementString("PersonSurname", namesur)
                writer.WriteEndElement()  'PersonName
                writer.WriteStartElement("AssociateContactDetails")
                If Microsoft.VisualBasic.Left(addPhone, 2) = "07" Then
                    writer.WriteElementString("ACDescription", "Mobile Telephone Number")
                    writer.WriteElementString("ACType", "Mobile")
                Else
                    writer.WriteElementString("ACDescription", "Telephone Number")
                    writer.WriteElementString("ACType", "Telephone")
                End If
                'remove spaces
                addFax = Replace(addFax, " ", "")
                writer.WriteElementString("ACNumber", addFax)
                writer.WriteEndElement()  'AssociateContactDetails
                writer.WriteEndElement()  'associate
                writer.WriteEndElement()  'associates
                writer.WriteStartElement("RevisedBalance")
                Dim clientBal As Decimal = get_client_bal(debtorID)
                Dim paidToDate As Decimal = paid_to_date(debtorID)
                writer.WriteElementString("RecoveryBalance", clientBal)
                writer.WriteElementString("PaidToDate", paidToDate)
                writer.WriteElementString("Reason", "CON")
                Dim origAmt As Decimal = array(5)
                writer.WriteElementString("OriginalLOAmount", origAmt)
                writer.WriteElementString("OriginalCostsAmount", "0")
                writer.WriteEndElement()  'revisedBalance
                writer.WriteEndElement()  'ChangedRecord
            End If
            If empPhone.Length > 5 Then
                totalRecords += 1
                writer.WriteStartElement("ChangedRecord")

                writer.WriteElementString("AccountReference", clientRef)
                writer.WriteElementString("BailiffSheriffRef", debtorID)
                writer.WriteElementString("RecoveryControl", Format(offenceFrom, "yyyy/MM"))
                writer.WriteStartElement("Associates")
                writer.WriteStartElement("Associate")
                writer.WriteStartElement("PersonName")
                If nameTitle <> "" Then
                    writer.WriteElementString("PersonTitle", nameTitle)
                End If
                If nameFore <> "" Then
                    writer.WriteElementString("PersonForename", nameFore)
                End If
                writer.WriteElementString("PersonSurname", namesur)
                writer.WriteEndElement()  'PersonName
                writer.WriteStartElement("AssociateContactDetails")
                If Microsoft.VisualBasic.Left(addPhone, 2) = "07" Then
                    writer.WriteElementString("ACDescription", "Work Mobile Telephone Number")
                    writer.WriteElementString("ACType", "Mobile")
                Else
                    writer.WriteElementString("ACDescription", "Work Telephone Number")
                    writer.WriteElementString("ACType", "Telephone")
                End If
                'remove spaces
                empPhone = Replace(empPhone, " ", "")
                writer.WriteElementString("ACNumber", empPhone)
                writer.WriteEndElement()  'AssociateContactDetails
                writer.WriteEndElement()  'associate
                writer.WriteEndElement()  'associates
                writer.WriteStartElement("RevisedBalance")
                Dim clientBal As Decimal = get_client_bal(debtorID)
                Dim paidToDate As Decimal = paid_to_date(debtorID)
                writer.WriteElementString("RecoveryBalance", clientBal)
                writer.WriteElementString("PaidToDate", paidToDate)
                writer.WriteElementString("Reason", "CON")
                Dim origAmt As Decimal = array(5)
                writer.WriteElementString("OriginalLOAmount", origAmt)
                writer.WriteElementString("OriginalCostsAmount", "0")
                writer.WriteEndElement()  'revisedBalance
                writer.WriteEndElement()  'ChangedRecord
            End If
            If empFax.Length > 5 Then
                totalRecords += 1
                writer.WriteStartElement("ChangedRecord")

                writer.WriteElementString("AccountReference", clientRef)
                writer.WriteElementString("BailiffSheriffRef", debtorID)
                writer.WriteElementString("RecoveryControl", Format(offenceFrom, "yyyy/MM"))
                writer.WriteStartElement("Associates")
                writer.WriteStartElement("Associate")
                writer.WriteStartElement("PersonName")
                If nameTitle <> "" Then
                    writer.WriteElementString("PersonTitle", nameTitle)
                End If
                If nameFore <> "" Then
                    writer.WriteElementString("PersonForename", nameFore)
                End If
                writer.WriteElementString("PersonSurname", namesur)
                writer.WriteEndElement()  'PersonName
                writer.WriteStartElement("AssociateContactDetails")
                If Microsoft.VisualBasic.Left(addPhone, 2) = "07" Then
                    writer.WriteElementString("ACDescription", "Work Mobile Telephone Number")
                    writer.WriteElementString("ACType", "Mobile")
                Else
                    writer.WriteElementString("ACDescription", "Work Telephone Number")
                    writer.WriteElementString("ACType", "Telephone")
                End If
                'remove spaces
                addFax = Replace(addFax, " ", "")
                writer.WriteElementString("ACNumber", addFax)
                writer.WriteEndElement()  'AssociateContactDetails
                writer.WriteEndElement()  'associate
                writer.WriteEndElement()  'associates
                writer.WriteStartElement("RevisedBalance")
                Dim clientBal As Decimal = get_client_bal(debtorID)
                Dim paidToDate As Decimal = paid_to_date(debtorID)
                writer.WriteElementString("RecoveryBalance", clientBal)
                writer.WriteElementString("PaidToDate", paidToDate)
                writer.WriteElementString("Reason", "CON")
                Dim origAmt As Decimal = array(5)
                writer.WriteElementString("OriginalLOAmount", origAmt)
                writer.WriteElementString("OriginalCostsAmount", "0")
                writer.WriteEndElement()  'revisedBalance
                writer.WriteEndElement()  'ChangedRecord
            End If
        Next

    End Sub
    Private Function get_client_bal(ByVal debtorID As Integer) As Decimal
        Dim client_bal As Decimal
        client_bal = GetSQLResults("DebtRecovery", "SELECT sum(fee_amount - remited_fee) from fee " & _
                                                    " WHERE DebtorID = " & debtorID & _
                                                    " and fee_remit_col = 1")
        Return client_bal
    End Function
    Private Function paid_to_date(ByVal debtorID As Integer) As Decimal
        Dim paidTodate As Decimal
        paidTodate = GetSQLResults("DebtRecovery", "SELECT sum(remited_fee) from fee " & _
                                                    " WHERE DebtorID = " & debtorID & _
                                                    " and fee_remit_col = 1")
        Return paidTodate
    End Function
    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_audit("Error: " & e.Message, True)
            Case XmlSeverityType.Warning
                write_audit("Warning " & e.Message, True)
        End Select
    End Sub
    Private Sub write_audit(ByVal audit_message As String, ByVal errorMessage As Boolean)
        If errorMessage Then
            error_no += 1
        End If
        audit_message = audit_message & vbNewLine
        My.Computer.FileSystem.WriteAllText(auditFile, audit_message, True)
    End Sub
End Class

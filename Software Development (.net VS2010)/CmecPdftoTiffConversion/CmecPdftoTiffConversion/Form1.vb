﻿Imports System.Drawing.Printing
Imports System.IO
Public Class Form1
    'Dim objAdobeApp As Object
    'Dim itfAVDocument As Object
    ' Dim itfPDDocument As Object
    'Dim nPages As Long
    
    Dim start_remit_no As Integer
    Dim objUDC As UDC.IUDC
    Dim itfPrinter As UDC.IUDCPrinter
    Dim defaultprinter As String
    Dim itfProfile As UDC.IProfile
    Dim default_debtor As Integer = 0
    'Dim AppDataPath As String
    Dim ProfilePath As String
    Dim adCmdText As ADODB.CommandTypeEnum

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub convertbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles convertbtn.Click
        Dim file_name As String

        'create directories for returns
        Dim dir_name As String = "O:\DebtRecovery\Archives\Remittances\TIF\"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create tif folder")
            Exit Sub
            End
        End Try
        dir_name = "O:\DebtRecovery\Archives\Remittances\TIF do not use\"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create tif do not use folder")
            Exit Sub
            End
        End Try
        start_remit_no = start_remit_tbox.Text
        'get last remit no
        convertbtn.Enabled = False
        exitbtn.Enabled = False
        start_remit_tbox.Enabled = False

        param2 = "select max(_rowid) from Remit"
        Dim max_remit_dataset As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("Unable to read last remit")
            Me.Close()
            Exit Sub
        End If

        Dim end_remit_no As Integer = max_remit_dataset.Tables(0).Rows(0).Item(0)
        If onerbtn.Checked Then
            end_remit_no = start_remit_no
        End If
        Dim remit_no As Integer
        Dim parm_yr As String = ""
        Dim parm_mth As String = ""
        Dim parm_day As String = ""

        set_up_printer()

        'set default printer to udc
        set_default_printer("Universal Document Converter")

        Dim no_of_files As Integer = 0
        Dim last_file_name As String = ""
        Dim new_filename As String
        Dim pbar As Integer = 0
        For remit_no = start_remit_no To end_remit_no
            'If no_of_files = 5 Then
            '    Exit For
            'End If
            pbar += 1
            Try
                ProgressBar1.Value = pbar
            Catch ex As Exception
                pbar = 0
            End Try
            Application.DoEvents()
            'get clientschemeid of case
            param2 = "select clientSchemeID, _rowid from Debtor where return_remitID = " & remit_no
            Dim debtor_dataset = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(0)
            'check work-type (12 for CMEC case)
            param2 = "select schemeID from clientScheme where _rowid = " & csid
            Dim csid_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows <> 1 Then
                MsgBox("Unable to read schemeID for csid = " & csid)
                Me.Close()
                Exit Sub
            End If
            Dim schID As Integer = csid_dataset.Tables(0).Rows(0).Item(0)
            param2 = "select work_type from Scheme where _rowid = " & schID
            Dim sch_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows <> 1 Then
                Continue For
            End If
            If sch_dataset.Tables(0).Rows(0).Item(0) <> 12 Then
                Continue For
            End If
            Dim debtor As Integer = debtor_dataset.Tables(0).Rows(0).Item(1)
            'get return pdf
            'get remit date
            param2 = "select date from Remit where _rowid = " & remit_no
            Dim remit_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows <> 1 Then
                MsgBox("Unable to read remit for remit_no = " & remit_no)
                Me.Close()
                Exit Sub
            End If
          
            Dim remit_date As Date = remit_dataset.Tables(0).Rows(0).Item(0)
            parm_yr = Format(remit_date, "yyyy") & "\"
            parm_mth = Format(remit_date, "MM") & "_" & Format(remit_date, "MMM") & "\"
            parm_day = Format(remit_date, "dd") & "_" & Format(remit_date, "ddd")
            file_name = "O:\DebtRecovery\Archives\" & parm_yr & parm_mth & parm_day & "\Remittances\" & remit_no & "\Returns"
            'file_name = "H:temp"
            Try

                For Each foundFile As String In My.Computer.FileSystem.GetFiles _
             (file_name, FileIO.SearchOption.SearchTopLevelOnly, "*.pdf")
                    'convert pdf to tiff
                    'get debtor from file name
                    Dim fileIDX As Integer
                    For fileIDX = foundFile.Length To 1 Step -1
                        If Mid(foundFile, fileIDX, 1) = "-" Then
                            Exit For
                        End If
                    Next

                    Try
                        debtor = Mid(foundFile, fileIDX + 1, foundFile.Length - fileIDX - 4)
                    Catch ex As Exception
                        'MsgBox("Unable to get debtorID for file " & foundFile)
                        default_debtor += 1
                        debtor = default_debtor
                    End Try
                    no_of_files += 1
                    new_filename = "O:\DebtRecovery\Archives\Remittances\TIF do not use\" & debtor & ".pdf"
                    My.Computer.FileSystem.CopyFile(foundFile, new_filename, True)

                    Try
                        new_PrintPDFToTIFF(new_filename)
                    Catch ex As Exception
                        MsgBox(ex.ToString)
                    End Try



                    last_file_name = "O:\DebtRecovery\Archives\Remittances\TIF do not use\" & debtor & "-doc-Page001.tif"
                Next
            Catch ex As Exception
                'MsgBox(ex.Message)

            End Try
        Next

        'update table with latest remit_no
        If normalrbtn.Checked Then
            Connect_sqlDb()
            Dim upd_txt As String = "update CMECLastRemitPdf2Tiff set cmec_last_remit_no = " & end_remit_no

            update_sql(upd_txt)
        End If
        'wait until last tiff file has been created
        'when last tiff file is there can delete all pdf files and reset default printer
        If no_of_files = 0 Then
            MsgBox("No files found")
        Else
            new_filename = "O:\DebtRecovery\Archives\Remittances\TIF do not use\"
            Dim last_tif_found As Boolean = False
            MsgBox("Waiting for tiff background tasks to catch up")
            Try
                While last_tif_found = False
                    Try
                        My.Computer.FileSystem.ReadAllText(last_file_name)
                        last_tif_found = True
                    Catch ex As Exception
                        System.Threading.Thread.Sleep(10000)
                    End Try
                End While
            Catch ex As Exception

            End Try
            'delete all pdf files
            'System.Threading.Thread.Sleep(10000)
            'new_filename = "O:\DebtRecovery\Archives\Remittances\TIF\"
            'Try
            '    For Each foundFile As String In My.Computer.FileSystem.GetFiles _
            '    (new_filename, FileIO.SearchOption.SearchTopLevelOnly, "*.pdf")
            '        My.Computer.FileSystem.DeleteFile(foundFile)
            '    Next
            'Catch ex As Exception
            '    MsgBox(ex.Message)
            'End Try
        End If

        'reset default printer
        set_default_printer(defaultprinter)

        'now read all tif files and create separate files for each page
        MsgBox("Continue when documents have finished", MsgBoxStyle.OkOnly)
        If no_of_files > 0 Then
            file_name = "O:\DebtRecovery\Archives\Remittances\TIF do not use\"
            Try
                For Each foundFile As String In My.Computer.FileSystem.GetFiles _
             (file_name, FileIO.SearchOption.SearchTopLevelOnly, "*.tif")
                    pbar += 1
                    Try
                        ProgressBar1.Value = pbar
                    Catch ex As Exception
                        pbar = 0
                    End Try
                    Application.DoEvents()

                    new_filename = "O:\DebtRecovery\Archives\Remittances\TIF\" & Path.GetFileName(foundFile)
                    My.Computer.FileSystem.CopyFile(foundFile, new_filename, True)
                    'Dim tif_image As Image
                    'tif_image = Image.FromFile(foundFile)
                    'Dim page_no As Integer = tif_image.GetFrameCount(Imaging.FrameDimension.Page)
                    'For idx = 0 To page_no - 1
                    '    'Dim tif_page As Image
                    '    tif_image.SelectActiveFrame(Imaging.FrameDimension.Page, idx)
                    '    Dim tif_page_file_name As String = Replace(foundFile, ".tif", "-doc-Page00" & idx + 1 & ".tif")
                    '    tif_page_file_name = Replace(tif_page_file_name, "TIF do not use", "TIF")
                    '    tif_image.Save(tif_page_file_name, Imaging.ImageFormat.Tiff)


                    'Next
                Next
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If

        MsgBox("Completed")
        Me.Close()
    End Sub
   

    Private Sub set_up_printer()
        'get default printer
        Dim printDoc As New PrintDocument
        If (printDoc.PrinterSettings.IsDefaultPrinter()) Then
            defaultprinter = printDoc.PrinterSettings.PrinterName
        End If

        ' Use Universal Document Converter API to change settings of converted document
        objUDC = New UDC.APIWrapper

        itfPrinter = objUDC.Printers("Universal Document Converter")
        itfProfile = itfPrinter.Profile

        'ProfilePath = "X:\Universal Document converter\UDC Profiles\PDF to TIFF Remittances.xml"
        'ProfilePath = "R:\vb.net shortcuts\W7PDF to TIFF Remittances.xml"
        ProfilePath = "R:\vb.net shortcuts\UDC SettingsJun2015.xml"

        'ProfilePath = "R:\vb.net shortcuts\PDF to TIFF Remittances.xml"

        itfProfile.Load(ProfilePath)

        itfProfile.OutputLocation.FolderPath = "O:DebtRecovery\Archives\Remittances\TIF do not use"


    End Sub
    Private Function set_default_printer(ByVal strPrinterName As String)
        Dim strOldPrinter As String = ""
        Dim WshNetwork As Object = Nothing
        Dim pd As New PrintDocument

        Try
            strOldPrinter = pd.PrinterSettings.PrinterName
            WshNetwork = Microsoft.VisualBasic.CreateObject("WScript.Network")
            WshNetwork.SetDefaultPrinter(strPrinterName)
            pd.PrinterSettings.PrinterName = strPrinterName
            If pd.PrinterSettings.IsValid Then
                Return True
            Else
                WshNetwork.SetDefaultPrinter(strOldPrinter)
                Return False
            End If
        Catch exptd As Exception
            WshNetwork.SetDefaultPrinter(strOldPrinter)
            Return False
        Finally
            WshNetwork = Nothing
            pd = Nothing
        End Try

    End Function

    Private Sub new_PrintPDFToTIFF(ByVal strFilePath As String)

        Try
            Dim MyProcess As New Process
            MyProcess.StartInfo.CreateNoWindow = False

            MyProcess.StartInfo.Verb = "print"

            MyProcess.StartInfo.FileName = strFilePath
            Try
                MyProcess.Start()
            Catch ex2 As Exception
                MsgBox(ex2.ToString)

            End Try

            MyProcess.WaitForExit(1000)
            MyProcess.CloseMainWindow()
            MyProcess.Close()
        Catch ex As Exception

        End Try
        


    End Sub





    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            param2 = "select cmec_last_remit_no from CMECLastRemitPdf2Tiff"
            Dim cmec_dataset As DataSet = get_dataset("Fees", param2)
            If no_of_rows = 0 Then
                start_remit_no = 0
            Else
                start_remit_no = cmec_dataset.Tables(0).Rows(0).Item(0)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        start_remit_tbox.Text = start_remit_no
    End Sub

    Private Sub start_remit_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles start_remit_tbox.TextChanged

    End Sub
End Class

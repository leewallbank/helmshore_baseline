﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblDebtAmount = New System.Windows.Forms.Label()
        Me.txtDebtAmount = New System.Windows.Forms.TextBox()
        Me.lblDebtorID = New System.Windows.Forms.Label()
        Me.txtDebtorID = New System.Windows.Forms.TextBox()
        Me.lblFeeAmount = New System.Windows.Forms.Label()
        Me.txtFeeAmount = New System.Windows.Forms.TextBox()
        Me.cmdLoadOpenCase = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblDebtAmount
        '
        Me.lblDebtAmount.AutoSize = True
        Me.lblDebtAmount.Location = New System.Drawing.Point(36, 99)
        Me.lblDebtAmount.Name = "lblDebtAmount"
        Me.lblDebtAmount.Size = New System.Drawing.Size(69, 13)
        Me.lblDebtAmount.TabIndex = 7
        Me.lblDebtAmount.Text = "Debt Amount"
        '
        'txtDebtAmount
        '
        Me.txtDebtAmount.Location = New System.Drawing.Point(119, 96)
        Me.txtDebtAmount.Name = "txtDebtAmount"
        Me.txtDebtAmount.ReadOnly = True
        Me.txtDebtAmount.Size = New System.Drawing.Size(76, 20)
        Me.txtDebtAmount.TabIndex = 6
        Me.txtDebtAmount.TabStop = False
        '
        'lblDebtorID
        '
        Me.lblDebtorID.AutoSize = True
        Me.lblDebtorID.Location = New System.Drawing.Point(36, 57)
        Me.lblDebtorID.Name = "lblDebtorID"
        Me.lblDebtorID.Size = New System.Drawing.Size(53, 13)
        Me.lblDebtorID.TabIndex = 5
        Me.lblDebtorID.Text = "Debtor ID"
        '
        'txtDebtorID
        '
        Me.txtDebtorID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtDebtorID.Location = New System.Drawing.Point(119, 57)
        Me.txtDebtorID.Name = "txtDebtorID"
        Me.txtDebtorID.Size = New System.Drawing.Size(76, 20)
        Me.txtDebtorID.TabIndex = 2
        '
        'lblFeeAmount
        '
        Me.lblFeeAmount.AutoSize = True
        Me.lblFeeAmount.Location = New System.Drawing.Point(36, 139)
        Me.lblFeeAmount.Name = "lblFeeAmount"
        Me.lblFeeAmount.Size = New System.Drawing.Size(64, 13)
        Me.lblFeeAmount.TabIndex = 9
        Me.lblFeeAmount.Text = "Fee Amount"
        '
        'txtFeeAmount
        '
        Me.txtFeeAmount.Location = New System.Drawing.Point(119, 136)
        Me.txtFeeAmount.Name = "txtFeeAmount"
        Me.txtFeeAmount.ReadOnly = True
        Me.txtFeeAmount.Size = New System.Drawing.Size(76, 20)
        Me.txtFeeAmount.TabIndex = 8
        Me.txtFeeAmount.TabStop = False
        '
        'cmdLoadOpenCase
        '
        Me.cmdLoadOpenCase.Location = New System.Drawing.Point(39, 12)
        Me.cmdLoadOpenCase.Name = "cmdLoadOpenCase"
        Me.cmdLoadOpenCase.Size = New System.Drawing.Size(156, 26)
        Me.cmdLoadOpenCase.TabIndex = 1
        Me.cmdLoadOpenCase.Text = "Load Open Case"
        Me.cmdLoadOpenCase.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(235, 189)
        Me.Controls.Add(Me.cmdLoadOpenCase)
        Me.Controls.Add(Me.lblFeeAmount)
        Me.Controls.Add(Me.txtFeeAmount)
        Me.Controls.Add(Me.lblDebtAmount)
        Me.Controls.Add(Me.txtDebtAmount)
        Me.Controls.Add(Me.lblDebtorID)
        Me.Controls.Add(Me.txtDebtorID)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.Text = "High Court Fee Calculator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblDebtAmount As System.Windows.Forms.Label
    Friend WithEvents txtDebtAmount As System.Windows.Forms.TextBox
    Friend WithEvents lblDebtorID As System.Windows.Forms.Label
    Friend WithEvents txtDebtorID As System.Windows.Forms.TextBox
    Friend WithEvents lblFeeAmount As System.Windows.Forms.Label
    Friend WithEvents txtFeeAmount As System.Windows.Forms.TextBox
    Friend WithEvents cmdLoadOpenCase As System.Windows.Forms.Button

End Class

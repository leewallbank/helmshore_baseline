﻿Public Class frmMain
    Dim FeeCalculator As New clsFeeCalculator

    Private Sub txtDebtorID_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtDebtorID.KeyPress
        If Asc(e.KeyChar) = 13 Then RefreshData()
    End Sub

    Private Sub RefreshData()
        Dim DebtAmount As Decimal, FeeAmount As Decimal

        If Not IsNumeric(txtDebtorID.Text) Then Return

        txtDebtAmount.Text = FeeCalculator.GetDebtAmount(txtDebtorID.Text).ToString()

        If IsNumeric(txtDebtAmount.Text) Then

            DebtAmount = CDec(txtDebtAmount.Text)

            If DebtAmount >= 100 Then
                FeeAmount = 5 + ((DebtAmount - 100) * 0.025)
            Else
                FeeAmount = DebtAmount * 0.05
            End If

            txtFeeAmount.Text = Math.Round(FeeAmount, 2).ToString
        Else
            txtFeeAmount.Text = ""
        End If
    End Sub

    Private Sub cmdLoadOpenCase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLoadOpenCase.Click
        Dim Processes() As System.Diagnostics.Process = System.Diagnostics.Process.GetProcessesByName("DebtRecovery")
        Dim WindowTitle As String

        If Processes.Count > 0 Then
            WindowTitle = Processes(0).MainWindowTitle
            If WindowTitle.Substring(0, 1) = "(" And IsNumeric(WindowTitle.Substring(1, 7)) And WindowTitle.Substring(8, 1) = ")" Then txtDebtorID.Text = WindowTitle.Substring(1, 7)
        Else
            txtDebtorID.Text = ""
        End If

        RefreshData()
    End Sub

End Class

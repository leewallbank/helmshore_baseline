﻿Imports System.Configuration
Imports System.Data
Imports System.Data.Odbc

Module modData
    Public DBCon As New OdbcConnection

    Public Sub ConnectDb()
        Try
            If Not IsNothing(DBCon) Then
                'This is only necessary following an exception...
                If DBCon.State = ConnectionState.Open Then DBCon.Close()
            End If

            DBCon.ConnectionString = ConfigurationManager.ConnectionStrings("DebtRecoveryRep").ConnectionString
            DBCon.Open()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub DisconnectDb()
        DBCon.Close()
        DBCon.Dispose()
    End Sub

    Public Function GetValue(ByVal Sql As String) As Object
        Dim command As New OdbcCommand(Sql, DBCon)
        Dim Obj As Object = Nothing

        ConnectDb()

        Dim reader As OdbcDataReader = command.ExecuteReader()

        While reader.Read()
            Obj = reader(0)
        End While

        reader.Close()
        DisconnectDb()

        Return Obj
    End Function

End Module


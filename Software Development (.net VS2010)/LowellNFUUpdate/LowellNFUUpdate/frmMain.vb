﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration

Public Class frmMain
    Private InputFilePath As String, FileName As String, FileExt As String
    Dim debtorArray(9999, 4) As Integer
    Dim phoneMax As Integer = 0

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            ConnectDb2("DebtRecovery")
            Dim prod_run As Boolean = False
            Dim env_str As String = ""
            Try
                env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
            Catch ex As Exception
                prod_run = False
            End Try
            If env_str = "Prod" Then prod_run = True

            Dim filedialog As New OpenFileDialog

            If Not filedialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(filedialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(filedialog.FileName)
            FileExt = Path.GetExtension(filedialog.FileName)
            Dim phonefile1 As String = "DebtorID,PhoneNumber1,PhoneNumber2,Emp Phone1,Emp Phone2" & vbNewLine
            Dim closedphonefile As String = ""
            Dim AddressFile As String = ""
            Dim NameFile As String = ""

            Dim ClosedAddressFile As String = ""
            Dim ClosedNameFile As String = ""

            Dim addressException As String = ""

            Dim errorNo As Integer = 0
            Dim phoneNo As Integer = 0
            Dim nameNo As Integer = 0
            Dim AddressNo As Integer = 0

            Dim ClosedNameNo As Integer = 0
            Dim ClosedAddressNo As Integer = 0
          
            Dim closedphoneNo As Integer = 0
           
            Dim AuditLog As String, ErrorLog As String = ""
            lblReadingFile.Visible = True
            'read excel file - ZERO based
            Dim FileContents() As String = System.IO.File.ReadAllLines(filedialog.FileName)


            ProgressBar.Maximum = UBound(FileContents)
            Dim inputLineArray() As String
            Dim LineNumber As Integer = 0
            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                Application.DoEvents()
                LineNumber += 1
                inputLineArray = InputLine.Split(",")
                Dim clientRef As String = inputLineArray(0)
                If clientRef.Length < 5 Then
                    Continue For
                End If
                Dim title As String = Trim(inputLineArray(5))
                Dim foreName As String = inputLineArray(6)
                Dim surName As String = inputLineArray(7)
                Dim name As String = title & " " & foreName & " " & surname
                Dim postcode As String = Trim(inputLineArray(12))
                Dim inpLine As String = inputLineArray(13)
                inpLine = Replace(inpLine, "|", "")
                Dim address As String = ""
                If inpLine <> "" Then
                    address = inpLine & "|"
                End If
                inpLine = inputLineArray(14)
                inpLine = Replace(inpLine, "|", "")
                If inpLine <> "" Then
                    address &= inpLine & "|"
                End If
                Try
                    inpLine = inputLineArray(15)
                    inpLine = Replace(inpLine, "|", "")
                    If inpLine <> "" Then
                        address &= inpLine & "|"
                    End If
                Catch ex As Exception

                End Try
                Try
                    inpLine = inputLineArray(16)
                    inpLine = Replace(inpLine, "|", "")
                    If inpLine <> "" Then
                        address &= inpLine & "|"
                    End If
                Catch ex As Exception

                End Try
                If address <> "" Then
                    address = Trim(address)
                    address = Replace(address, ",", " ")
                    If address.Length < 10 Then
                        address = ""
                    End If
                End If
                Dim phone1 As String = Trim(inputLineArray(8))
                Dim phone2 As String = Trim(inputLineArray(9))
                Dim phone3 As String = Trim(inputLineArray(10))
                If phone1 <> "" Then
                    phone1 = Replace(phone1, " ", "")
                    If Microsoft.VisualBasic.Left(phone1, 1) <> "0" Then
                        phone1 = "0" & phone1
                    End If
                End If
                If phone2 <> "" Then
                    phone2 = Replace(phone2, " ", "")
                    If Microsoft.VisualBasic.Left(phone2, 1) <> "0" Then
                        phone2 = "0" & phone2
                    End If
                End If
                If phone3 <> "" Then
                    phone3 = Replace(phone3, " ", "")
                    If Microsoft.VisualBasic.Left(phone3, 1) <> "0" Then
                        phone3 = "0" & phone3
                    End If
                End If
                Dim dualScheme As Boolean = False
                'get debtorID from onestep
                Dim dt As New DataTable
                LoadDataTable2("DebtRecovery", "SELECT D._rowid, claimantname, status_open_closed,add_phone, add_fax,empphone, empfax, CS._rowID, name_title,   " & _
                                                           "name_fore, name_sur, address, add_postcode " & _
                                                        "FROM debtor D, clientscheme CS  " & _
                                                        "WHERE client_ref = '" & clientRef & "'" & _
                                                        " and D.clientschemeID = CS._rowID" & _
                                                        " and CS.clientID =1815 ", dt, False)
                For Each debtRow In dt.Rows
                    Dim debtorID As Integer = debtRow(0)
                    Dim os_title As String = ""
                    Try
                        os_title = debtRow(8)
                    Catch ex As Exception

                    End Try
                    Dim os_forename As String = ""
                    Try
                        os_forename = debtRow(9)
                    Catch ex As Exception

                    End Try
                    Dim os_surname As String = ""
                    Try
                        os_surname = debtRow(10)
                    Catch ex As Exception

                    End Try
                    Dim OSAddress As String = ""
                    Try
                        OSAddress = debtRow(11)
                    Catch ex As Exception

                    End Try
                    'get first line of onestep address
                    Dim OSFirstAddressLine As String = ""
                    For idx = 1 To OSAddress.Length
                        If Mid(OSAddress, idx, 1) = Chr(10) Or Mid(OSAddress, idx, 1) = Chr(13) Then
                            Exit For
                        Else
                            OSFirstAddressLine &= Mid(OSAddress, idx, 1)
                        End If
                    Next
                    Dim OSPostCode As String = ""
                    Try
                        OSPostCode = debtRow(12)
                    Catch ex As Exception

                    End Try
                    'write to exception if address first line and postcode same as on onestep
                    If address <> "" Then
                        If OSFirstAddressLine = inputLineArray(13) And OSPostCode = postcode Then
                            addressException &= debtorID & " same as on onestep " & address & vbNewLine
                            address = ""
                        End If
                    End If
                    'make sure correct number of pipes are in address
                    If address <> "" Then
                        Dim NoOFMissingPipes As Integer = 4 - address.Length + Replace(address, "|", "").Length
                        While NoOFMissingPipes > 0
                            address &= "|"
                            NoOFMissingPipes -= 1
                        End While
                    End If
                    If LCase(title) = LCase(os_title) And LCase(foreName) = LCase(os_forename) And LCase(surName) = LCase(os_surname) Then
                        name = ""
                    End If
                    name = Trim(name)
                    Dim addPhone As String = ""
                    Dim addFax As String = ""
                    Dim empPhone As String = ""
                    Dim empFax As String = ""
                    Try
                        addPhone = Trim(debtRow(3))
                    Catch ex As Exception

                    End Try
                    Try
                        addFax = Trim(debtRow(4))
                    Catch ex As Exception

                    End Try
                    Try
                        empPhone = Trim(debtRow(5))
                    Catch ex As Exception

                    End Try
                    Try
                        empFax = Trim(debtRow(6))
                    Catch ex As Exception

                    End Try
                    Dim CSID As Integer = debtRow(7)
                    If CSID = 4437 Or (CSID >= 4723 And CSID <= 4726) Then
                        dualScheme = True
                    Else
                        dualScheme = False
                    End If
                    Dim claimantName As String
                    Try
                        claimantName = debtRow(1)
                    Catch ex As Exception
                        claimantName = ""
                    End Try
                    Dim noteTag As String
                    Dim person1 As Boolean = True
                    If claimantName = "Secondary" Then
                        noteTag = "Name 2 TDX unique identifier:"
                        person1 = False
                    Else
                        noteTag = "TDX unique identifier:"
                    End If
                    Dim openClosed As String = debtRow(2)
                    'check phone number not already on onestep
                    Dim testPhoneNo As String = ""
                    If phone1 = addPhone Or phone1 = addFax Or phone1 = empPhone Or phone1 = empFax Then
                        phone1 = ""
                    End If
                    If phone2 = addPhone Or phone2 = addFax Or phone2 = empPhone Or phone2 = empFax Then
                        phone2 = ""
                    End If
                    If phone3 = addPhone Or phone3 = addFax Or phone3 = empPhone Or phone3 = empFax Then
                        phone3 = ""
                    End If

                    If name <> "" Then
                        If openClosed = "O" Then
                            NameFile &= debtorID & "|" & name & vbNewLine
                            nameNo += 1
                        Else
                            ClosedNameFile &= debtorID & "|" & name & vbNewLine
                            ClosedNameNo += 1
                        End If
                    End If
                    If address <> "" Then
                        If openClosed = "O" Then
                            AddressFile &= debtorID & "|" & postcode & "|" & address & vbNewLine
                            AddressNo += 1
                        Else
                            ClosedAddressFile &= debtorID & "|" & postcode & "|" & address & vbNewLine
                            ClosedAddressNo += 1
                        End If
                    End If
                    Dim reportNo As Integer = 0
                    If phone1 <> "" Then
                        If openClosed = "O" Then
                            reportNo = checkDebtor(debtorID, addPhone, addFax, empPhone, empFax)
                            Select Case reportNo
                                Case 1
                                    phonefile1 &= debtorID & "," & phone1 & ",,," & vbNewLine
                                Case 2
                                    phonefile1 &= debtorID & ",," & phone1 & ",," & vbNewLine
                                Case 3
                                    phonefile1 &= debtorID & ",,," & phone1 & "," & vbNewLine
                                Case 4
                                    phonefile1 &= debtorID & ",,,," & phone1 & vbNewLine
                            End Select
                            phoneNo += 1
                        Else
                            closedphonefile &= debtorID & "|" & phone1 & vbNewLine
                            closedphoneNo += 1
                        End If
                    End If
                    If phone2 <> "" Then
                        If openClosed = "O" Then
                            reportNo = checkDebtor(debtorID, addPhone, addFax, empPhone, empFax)
                            Select Case reportNo
                                Case 1
                                    phonefile1 &= debtorID & "," & phone2 & ",,," & vbNewLine
                                Case 2
                                    phonefile1 &= debtorID & ",," & phone2 & ",," & vbNewLine
                                Case 3
                                    phonefile1 &= debtorID & ",,," & phone2 & "," & vbNewLine
                                Case 4
                                    phonefile1 &= debtorID & ",,,," & phone2 & vbNewLine
                            End Select
                            phoneNo += 1
                        Else
                            closedphonefile &= debtorID & "|" & phone2 & vbNewLine
                            closedphoneNo += 1
                        End If
                    End If
                    If phone3 <> "" Then
                        If openClosed = "O" Then
                            reportNo = checkDebtor(debtorID, addPhone, addFax, empPhone, empFax)
                            Select Case reportNo
                                Case 1
                                    phonefile1 &= debtorID & "," & phone3 & ",,," & vbNewLine
                                Case 2
                                    phonefile1 &= debtorID & ",," & phone3 & ",," & vbNewLine
                                Case 3
                                    phonefile1 &= debtorID & ",,," & phone3 & "," & vbNewLine
                                Case 4
                                    phonefile1 &= debtorID & ",,,," & phone3 & vbNewLine
                            End Select
                            phoneNo += 1
                        Else
                            closedphonefile &= debtorID & "|" & phone3 & vbNewLine
                            closedphoneNo += 1
                        End If
                    End If
                Next
            Next

            lblReadingFile.Visible = False
            ' WriteFile(InputFilePath & FileName & "_PreProcessed.txt", OutputFile)

            AuditLog = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
            AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

            AuditLog &= "P1 Open   Address changes: " & AddressNo & vbNewLine
            AuditLog &= "P1 Open   Name    changes: " & nameNo & vbNewLine
            AuditLog &= "P1 Closed Address changes: " & ClosedAddressNo & vbNewLine
            AuditLog &= "P1 Closed Name    changes: " & ClosedNameNo & vbNewLine

            AuditLog &= "Open Phone   changes: " & phoneNo & vbNewLine
            AuditLog &= "Closed Phone   changes: " & closedphoneNo & vbNewLine
            AuditLog &= "Errors: " & errorNo & vbNewLine
            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If nameNo > 0 Then WriteFile(InputFilePath & FileName & "_NameChanges.txt", NameFile)
            If AddressNo > 0 Then WriteFile(InputFilePath & FileName & "_AddressChanges.txt", AddressFile)

            If ClosedNameNo > 0 Then WriteFile(InputFilePath & FileName & "_ClosedNameChanges.txt", ClosedNameFile)
            If ClosedAddressNo > 0 Then WriteFile(InputFilePath & FileName & "_ClosedAddressChanges.txt", ClosedAddressFile)
         
            If addressException <> "" Then
                WriteFile(InputFilePath & FileName & "_AddressException.txt", addressException)
            End If
            If phoneNo > 0 Then WriteFile(InputFilePath & FileName & "_PhoneChanges.txt", phonefile1)
            If closedphoneNo > 0 Then WriteFile(InputFilePath & FileName & "_ClosedPhoneChanges.txt", closedphonefile)
            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)

            MsgBox("Pre-processing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            ' btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
    Private Function checkDebtor(ByVal DebtorID As Integer, ByVal addPhone As String, ByVal addFax As String, ByVal empPhone As String, ByVal empfax As String) As Integer
        Dim reportID As Integer = 1
        'see if case has already been processed
        For phoneIDX = 1 To phoneMax
            If debtorArray(phoneIDX, 1) = DebtorID Then
                reportID = debtorArray(phoneIDX, 2)
                For repIDX = reportID To 5
                    Select Case repIDX
                        Case 1
                            If addFax = "" Then
                                reportID = 2
                                debtorArray(phoneIDX, 2) = reportID
                            End If
                            Exit For
                        Case 2
                            If empPhone = "" Then
                                reportID = 3
                                debtorArray(phoneIDX, 2) = reportID
                                Exit For
                            End If
                        Case 3
                            If empfax = "" Then
                                reportID = 4
                                debtorArray(phoneIDX, 2) = reportID
                                Exit For
                            End If
                        Case 4
                            reportID = 5
                            Exit For

                    End Select
                Next
                Exit For
            End If
        Next
        If reportID = 1 Then
            phoneMax += 1
            debtorArray(phoneMax, 1) = DebtorID
            If addPhone = "" Then
                reportID = 1
            ElseIf addFax = "" Then
                reportID = 2
            ElseIf empPhone = "" Then
                reportID = 3
            ElseIf empfax = "" Then
                reportID = 4
            Else
                reportID = 5
            End If
            debtorArray(phoneMax, 2) = reportID

        End If

        Return (reportID)
    End Function
    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

﻿Imports System.IO

Public Class Form1
    Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
    Dim filename As String = ""
    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim startDate As Date = DateAdd(DateInterval.Month, -1, Now)

        start_dtp.Value = CDate(Format(startDate, "yyyy-MM-") & "01 00:00:00")
        Dim endDate As Date = CDate(Format(Now, "yyyy-MM-") & "01 00:00:00")
        endDate = DateAdd(DateInterval.Day, -1, endDate)
        end_dtp.Value = endDate
    End Sub

    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(sender As System.Object, e As System.EventArgs) Handles runbtn.Click
        runbtn.Enabled = False
        exitbtn.Enabled = False
        Dim RD422Preport = New RD422P
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(PONumbertbox.Text)
        SetCurrentValuesForParameterField1(RD422Preport, myArrayList1)
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField2(RD422Preport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD422Preport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD422Preport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RD422P STW Invoice.pdf"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = SaveFileDialog1.FileName
        Else
            MsgBox("Report not saved")
            Me.Close()
            Exit Sub
        End If

        RD422Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD422Preport.Close()

        Dim InputFilePath As String = Path.GetDirectoryName(filename)
        InputFilePath &= "\"
        filename = Path.GetFileName(filename)
        'now do RD422FP
        Dim RD422FPreport = New RD422FP
        myArrayList1.Add(PONumbertbox.Text)
        SetCurrentValuesForParameterField1(RD422FPreport, myArrayList1)
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField2(RD422FPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD422FPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD422FPreport)
        filename = Replace(filename, "Invoice", "Fee Invoice")
        RD422FPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, InputFilePath & filename)
        RD422FPreport.Close()
        MsgBox("Reports saved")
        Me.Close()
    End Sub
End Class

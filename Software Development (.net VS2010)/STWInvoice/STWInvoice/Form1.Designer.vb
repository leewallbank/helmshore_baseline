﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.runbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PONumbertbox = New System.Windows.Forms.TextBox()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.end_dtp = New System.Windows.Forms.DateTimePicker()
        Me.start_dtp = New System.Windows.Forms.DateTimePicker()
        Me.SuspendLayout()
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(84, 254)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(75, 23)
        Me.runbtn.TabIndex = 0
        Me.runbtn.Text = "Run report"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(197, 292)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(97, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "PO Number"
        '
        'PONumbertbox
        '
        Me.PONumbertbox.Location = New System.Drawing.Point(75, 68)
        Me.PONumbertbox.Name = "PONumbertbox"
        Me.PONumbertbox.Size = New System.Drawing.Size(100, 20)
        Me.PONumbertbox.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(97, 182)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(50, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "End date"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(97, 121)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(55, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Start Date"
        '
        'end_dtp
        '
        Me.end_dtp.Location = New System.Drawing.Point(72, 198)
        Me.end_dtp.Name = "end_dtp"
        Me.end_dtp.Size = New System.Drawing.Size(128, 20)
        Me.end_dtp.TabIndex = 6
        '
        'start_dtp
        '
        Me.start_dtp.Location = New System.Drawing.Point(72, 137)
        Me.start_dtp.Name = "start_dtp"
        Me.start_dtp.Size = New System.Drawing.Size(128, 20)
        Me.start_dtp.TabIndex = 7
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 357)
        Me.Controls.Add(Me.start_dtp)
        Me.Controls.Add(Me.end_dtp)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PONumbertbox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "STWInvoice"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PONumbertbox As System.Windows.Forms.TextBox
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents end_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents start_dtp As System.Windows.Forms.DateTimePicker

End Class

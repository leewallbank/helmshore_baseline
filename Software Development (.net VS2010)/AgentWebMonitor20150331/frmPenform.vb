Option Strict Off
Option Explicit On
Option Compare Binary
Imports VB = Microsoft.VisualBasic
Imports commonlibrary

Friend Class frmPenform
    Inherits System.Windows.Forms.Form

    Private Sub GetAgentWeb()
        Dim StartOfWeek As DateTime = DateTime.Today.AddDays(-Weekday(DateTime.Today, FirstDayOfWeek.System)) ' First Sunday of week
        Dim lngAgentWebCount As Integer, lngTotalReceivedToday As Integer, lngTotalReceivedHour As Integer, lngTotalOutstanding As Integer, lngTotalProcessed As Integer, lngTotalProcessedHour As Integer, lngTotalProcessedToday As Integer, lngTotalReceivedYesterday As Integer, lngAttention As Integer
        Dim AgentWeb As New DataTable

        Try
            lblCheckProcessServer.Visible = False

            LoadDataTable("opsWallboard", "SELECT p._createdDate, p.status, p.statusDate " & _
                               "FROM PenForm AS p " & _
                               "WHERE p._createdDate >= " & "'" & StartOfWeek.ToString("yyyy-MM-dd") & "'", AgentWeb, False)

            lngAgentWebCount = AgentWeb.Rows.Count

            For Each PenForm As DataRow In AgentWeb.Rows

                ' Received yesterday
                If Weekday(PenForm("_createddate")) = Weekday(System.DateTime.FromOADate(Now.ToOADate - 1)) Then lngTotalReceivedYesterday = lngTotalReceivedYesterday + 1

                ' Received today and current hour
                If Weekday(PenForm("_createddate")) = Weekday(Now) Then
                    If Hour(PenForm("_createddate")) = Hour(Now) Then lngTotalReceivedHour = lngTotalReceivedHour + 1
                    lngTotalReceivedToday = lngTotalReceivedToday + 1
                End If

                If Trim(PenForm("status")) <> "U" And Trim(PenForm("status")) <> "D" And Trim(PenForm("status")) <> "DE" And Trim(PenForm("status")) <> "DU" Then
                    lngTotalOutstanding = lngTotalOutstanding + 1
                Else
                    lngTotalProcessed = lngTotalProcessed + 1
                End If

                ' Processed in current day & hour
                If Not IsDBNull(PenForm("statusDate")) AndAlso Weekday(PenForm("statusDate")) = Weekday(Now) Then
                    If Hour(PenForm("statusDate")) = Hour(Now) Then
                        lngTotalProcessedHour = lngTotalProcessedHour + 1
                    End If
                    lngTotalProcessedToday = lngTotalProcessedToday + 1
                End If

                ' cases requiring attention
                If InStr(1, PenForm("status"), "?") > 0 Then lngAttention = lngAttention + 1
            Next PenForm

            lblReceivedWeek.Text = lngAgentWebCount.ToString
            lblReceivedToday.Text = lngTotalReceivedToday.ToString
            lblReceivedHour.Text = lngTotalReceivedHour.ToString

            lblOutstandingWeek.Text = lngTotalOutstanding.ToString
            lblOutstandingToday.Text = lngTotalOutstanding.ToString
            lblOutstandingHour.Text = lngTotalOutstanding.ToString

            lblAttentionWeek.Text = lngAttention.ToString
            lblAttentionToday.Text = lngAttention.ToString
            lblAttentionHour.Text = lngAttention.ToString

            lblProcessedWeek.Text = lngTotalProcessed.ToString
            lblProcessedToday.Text = lngTotalProcessedToday.ToString
            lblProcessedHour.Text = lngTotalProcessedHour.ToString

            Dim AgentWebPenForm() As Object = GetSQLResultsArray("OpsWallboard", "SELECT COUNT(*), MIN(p._createdDate), CAST(MIN(CASE WHEN p.status = 'N' THEN p._createdDate END) AS DATETIME) " & _
                                                                                 "FROM penform AS p " & _
                                                                                 "WHERE p.status NOT IN ('U','D','DE','DU')")

            lblOutstandingAll.Text = AgentWebPenForm(0).ToString

            lblOldestCase.Text = "Oldest Outstanding Case " & AgentWebPenForm(1)

            ' now put a warning if the oldest penform with a status of N is over 1 hour old
            If DateDiff(DateInterval.Hour, AgentWebPenForm(2), Now) > 1 Then
                lblCheckProcessServer.Visible = True
                lblCheckProcessServer.Text = "Check Process Server - Penform " & AgentWebPenForm(2)
            Else
                lblCheckProcessServer.Visible = False
            End If

            txtNumberSent.Text = GetSQLResults("OpsWallboard", "SELECT COUNT(*) FROM Message WHERE status = 'S' AND senderDate >= '" & Now.ToString("yyyy-MM-dd") & "'")

            txtNumberWaiting.Text = GetSQLResults("OpsWallboard", "SELECT COUNT(*) FROM Message WHERE status = 'W'")

            txtNumberFailed.Text = GetSQLResults("OpsWallboard", "SELECT COUNT(*) FROM Message WHERE status = 'F'")

            Exit Sub

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Sub

    'UPGRADE_WARNING: Event Check1.CheckStateChanged may fire when form is initialized. Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="88B12AE1-6DE0-48A0-86F1-60C0686C026A"'
    Private Sub chkHideOldest_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkHideOldest.CheckStateChanged

        lblOldestCase.Visible = chkHideOldest.CheckState

    End Sub

    Private Sub Timer1_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles Timer1.Tick

        Timer1.Enabled = False
        frmAgentStats.Show()
        Me.Hide()
    End Sub

    Private Sub frmPenform_VisibleChanged(sender As Object, e As System.EventArgs) Handles Me.VisibleChanged
        Call GetAgentWeb()
        lblLastUpdated.Text = "Last Updated " & Today & " " & TimeOfDay
        Timer1.Enabled = True
    End Sub
End Class
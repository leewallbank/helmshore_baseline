﻿Imports System.IO
Imports CommonLibrary
Public Class Form1

    Private InputFilePath As String, FileName As String, FileExt As String
    Dim outfile As String = ""
    Dim debitSortCode As String
    Dim debitAcctNo As String
    Dim payment As Decimal
    Dim remitID As Integer
    Dim payDate As Date
    Dim benAcctNo As String
    Dim benSortCode As String
    Dim benAcctName As String
    Dim remitRef As String
    Dim reportSuffix As String
    Dim totalAmount As Decimal
    Dim seqNo As Integer = 1
    Private SortCodeData As New clsSortCode
    Private bankAccountdata As New clsBankAccount
    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click
        'open file
        conn_open = False

        Dim FileDialog As New OpenFileDialog
        Dim InputLineArray() As String
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)
        If bacs1rbtn.Checked Then
            reportSuffix = "_bacs_single.txt"
        ElseIf bacsmultrbtn.Checked Then
            reportSuffix = "_bacs_mult.txt"
        ElseIf chapsrbtn.Checked Then
            reportSuffix = "_chaps.txt"
        Else
            reportSuffix = "_faster_payment_"
        End If
        Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
        Dim rowCount As Integer
        Dim lineCount As Integer
        For Each InputLine As String In FileContents
            Try
                rowCount += 1
                ProgressBar1.Value = rowCount
            Catch ex As Exception
                rowCount = 0
            End Try
            lineCount += 1
            Application.DoEvents()
            InputLineArray = InputLine.Split(",")
            debitSortCode = InputLineArray(0)
            debitAcctNo = InputLineArray(1)
            payment = InputLineArray(2)
            remitID = InputLineArray(3)
            payDate = InputLineArray(4)
            Try
                benAcctNo = InputLineArray(5)
            Catch ex As Exception
                MsgBox("There is no beneficiary account number on line = " & lineCount)
                Me.Close()
                Exit Sub
            End Try

            Try
                benSortCode = InputLineArray(6)
            Catch ex As Exception
                MsgBox("There is no beneficiary sort code on line = " & lineCount)
                Me.Close()
                Exit Sub
            End Try

            benAcctName = InputLineArray(7)
            If benAcctName.Length > 18 Then
                benAcctName = Microsoft.VisualBasic.Left(benAcctName, 18)
            End If
            'remove special charcters from benacctname
            Dim newBenAcctName As String = ""
            For idx = 1 To benAcctName.Length
                Dim test As Char = Mid(benAcctName, idx, 1)
                If Char.IsDigit(test) Or Char.IsLetter(test) Then
                    newBenAcctName &= test
                Else
                    newBenAcctName &= " "
                End If
            Next
            benAcctName = newBenAcctName
            remitRef = ""
            param2 = "select clientSchemeID, date, invoiceID from Remit where _rowid = " & remitID
            Dim remit_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("remit No = " & remitID & " not found")
                Me.Close()
                Exit Sub
            End If
            Dim csID As Integer = remit_dataset.Tables(0).Rows(0).Item(0)
            Dim remitDate As Date = remit_dataset.Tables(0).Rows(0).Item(1)
            Dim invoiceID As Integer = remit_dataset.Tables(0).Rows(0).Item(2)
            remitRef = ""

            param2 = "select bankAccCode, schemeID, clientID, remitBacsSortCode, remitBAcsAccountNumber from clientScheme where _rowid = " & csID
            Dim csid_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to find client scheme ID = " & csID)
                Me.Close()
                Exit Sub
            End If
            Dim schID As Integer = csid_dataset.Tables(0).Rows(0).Item(1)
            Dim clID As Integer = csid_dataset.Tables(0).Rows(0).Item(2)
            Dim SortCode As Integer
            Try
                SortCode = csid_dataset.Tables(0).Rows(0).Item(3)
            Catch ex As Exception
                SortCode = 0
                MsgBox("No sortcode set up for Conn ID = " & csID)
            End Try
            Dim BankAccount As Long
            Try
                BankAccount = csid_dataset.Tables(0).Rows(0).Item(4)
            Catch ex As Exception
                BankAccount = 0
                MsgBox("No bank account set up for Conn ID = " & csID)
            End Try

            Try
                remitRef = csid_dataset.Tables(0).Rows(0).Item(0)
            Catch ex As Exception
                remitRef = ""
            End Try

            Try

            Catch ex As Exception

            End Try
            If remitRef = "." Then
                remitRef = ""
            End If
            If LCase(remitRef) = "scheme" Then
                param2 = "select name from Scheme where _rowid = " & schID
                Dim sch_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    MsgBox("Unable to find scheme no = " & schID & " for csid = " & csID)
                    Me.Close()
                    Exit Sub
                End If
                remitRef = Trim(sch_dataset.Tables(0).Rows(0).Item(0) & remitID)
                remitRef = Replace(remitRef, " ", "")
            End If
            If csID = 1619 Or csID = 1620 Or csID = 1621 Or csID = 1622 Or csID = 3092 Or csID = 4067 Or csID = 4068 Or csID = 4069 Then 'wrexham
                param2 = "select name from Scheme where _rowid = " & schID
                Dim sch_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    MsgBox("Unable to find scheme no = " & schID & " for csid = " & csID)
                    Me.Close()
                    Exit Sub
                End If
                remitRef = "Ross-" & remitID & Trim(sch_dataset.Tables(0).Rows(0).Item(0))
                remitRef = Replace(remitRef, " ", "")
                remitRef = Replace(remitRef, "Wales", "")
            End If
            Dim debtorID As Integer
            '17.4.14 use schemeID rather than CSID to determine csa/cmec
            'T79352 add 4710
            If schID = 811 Or schID = 822 Or schID = 916 Or schID = 917 Or schID = 1169 Or schID = 1170 Or schID = 1172 Or schID = 1240 Or schID = 4710 Then
                'If csID = 2111 Or csID = 2129 Or csID = 2132 Or csID = 2133 Or csID = 2130 Or _
                '  csID = 2131 Or (csID >= 2672 And csID <= 2687) Or (csID >= 2700 And csID <= 2710) Then  'CSA
                param2 = "select debtorID from Payment where status_remitID = " & remitID
                Dim pay_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    MsgBox("Payment not found for remit ID = " & remitID)
                    Me.Close()
                    Exit Sub
                    'ElseIf no_of_rows > 1 Then
                    '    MsgBox("Multiple payments found for remit ID = " & remitID)
                    '    Me.Close()
                    '    Exit Sub
                End If
                debtorID = pay_dataset.Tables(0).Rows(0).Item(0)
                param2 = "select empNI, client_ref from Debtor where _rowid = " & debtorID
                Dim debt_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    MsgBox("Unable to read debtor = " & debtorID)
                    Me.Close()
                    Exit Sub
                End If
                Dim clientRef As String
                Try
                    clientRef = Trim(debt_dataset.Tables(0).Rows(0).Item(1))
                Catch ex As Exception
                    MsgBox("client ref is blank for case number = " & debtorID)
                    Me.Close()
                    Exit Sub
                End Try
                Dim empNI As String = ""
                'Now always use NI Number if present otherwise use client ref
                Try
                    empNI = debt_dataset.Tables(0).Rows(0).Item(0)
                    remitRef = Trim(empNI)
                Catch ex As Exception
                    remitRef = clientRef
                End Try

                'If Microsoft.VisualBasic.Left(clientRef, 2) = "32" Then
                '    Try
                '        empNI = debt_dataset.Tables(0).Rows(0).Item(0)
                '    Catch ex As Exception
                '        empNI = ""
                '    End Try
                '    If empNI = "" Then
                '        MsgBox("NI Number is blank for case number = " & debtorID)
                '        Me.Close()
                '        Exit Sub
                '    End If
                '    remitRef = Trim(empNI)
                'ElseIf Microsoft.VisualBasic.Left(clientRef, 1) = "1" _
                'Or Microsoft.VisualBasic.Left(clientRef, 1) = "7" Then
                '    remitRef = clientRef
                'ElseIf Microsoft.VisualBasic.Left(clientRef, 3) = "NST" Then
                '    remitRef = clientRef
                'Else
                '    MsgBox("Don't know how to work out remit ref for debtor = " & debtorID)
                '    Me.Close()
                '    Exit Sub
                'End If
            End If
            If clID = 1003 Then 'cullimore duttan
                param2 = "select debtorID from Payment where status_remitID = " & remitID
                Dim pay_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    MsgBox("Payment not found for remit ID = " & remitID)
                    Me.Close()
                    Exit Sub
                End If
                debtorID = pay_dataset.Tables(0).Rows(0).Item(0)
                param2 = "select client_ref from Debtor where _rowid = " & debtorID
                Dim debt_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    MsgBox("Unable to read debtor = " & debtorID)
                    Me.Close()
                    Exit Sub
                End If
                remitRef = debt_dataset.Tables(0).Rows(0).Item(0)
            End If
            If clID = 1523 Then  'BT
                remitRef = "RCLBTGroup" & Format(remitDate, "ddMMyyyy")
            End If
                If remitRef = "" Then
                    remitRef = remitID
            End If
            'prefix Uttlesford with ST
            If clID = 1681 Then
                remitRef = "ST" & remitRef
            End If
            'replace DDMMYY for HMRC ETC with next working day
            If clID = 1736 Then
                Dim nextWorkingDate As Date = Now
                Dim newDateFound As Boolean = False
                While Not newDateFound
                    nextWorkingDate = DateAdd(DateInterval.Day, 1, nextWorkingDate)
                    If nextWorkingDate.DayOfWeek = 6 Or nextWorkingDate.DayOfWeek = 0 Then  'Saturday/Sunday
                        nextWorkingDate = DateAdd(DateInterval.Day, 1, nextWorkingDate)
                    Else
                        'check if a bank holiday
                        nextWorkingDate = CDate(Format(nextWorkingDate, "yyyy, MMM dd") & " 00:00:00")
                        Dim testArray As Object = Nothing
                        Try
                            testArray = GetSQLResultsArray("FeesSQL", "SELECT count(*) " & _
                                                          "FROM BankHolidayDates " & _
                                                          "WHERE BankHolidayDate= '" & nextWorkingDate.ToString("dd/MMM/yyyy") & "'")
                        Catch ex As Exception

                        End Try
                        Try
                            If testArray(0) > 0 Then
                                nextWorkingDate = DateAdd(DateInterval.Day, 1, nextWorkingDate)
                            Else
                                newDateFound = True
                            End If
                        Catch ex As Exception
                            newDateFound = True
                        End Try

                    End If
                End While
                remitRef = Replace(remitRef, "DDMMYY", Format(nextWorkingDate, "ddMMyy"))
            End If
            'for sheffield use invoiceid
            If clID = 939 Then
                remitRef = invoiceID
            End If
            remitRef = Replace(remitRef, "ddmmyyyy", Format(remitDate, "ddMMyyyy"))
            remitRef = Replace(remitRef, "ddmmyy", Format(remitDate, "ddMMyy"))
            If remitRef.Length > 18 Then
                remitRef = Microsoft.VisualBasic.Left(remitRef, 18)
            End If
            'check sortcode is the same as on onestep
            If SortCode <> 0 Then
                If benSortCode <> SortCode Then
                    MsgBox("Sort Code has changed on Onestep for from " & benSortCode & " to " & SortCode & vbNewLine &
                           "ClientSchemeID = " & csID)
                Else
                    'check sortcode is the same as on bank accounts table
                    Dim bankAccountSortCode As String = SortCodeData.GetSortCode(csID)
                    Dim testSortCode As Integer
                    Try
                        testSortCode = bankAccountSortCode
                    Catch ex As Exception
                        testSortCode = 0
                    End Try
                    If benSortCode <> testSortCode Then
                        MsgBox("Sort Code has changed on Onestep from " & testSortCode & " to " & benSortCode & vbNewLine & _
                               "ClientSchemeID = " & csID)
                    End If
                End If
            End If
            If BankAccount <> 0 Then
                'check bank account is the same as on onestep
                If benAcctNo <> BankAccount Then
                    MsgBox("bank Account Number has changed on Onestep from " & benAcctNo & " to " & BankAccount & vbNewLine &
                           "ClientSchemeID = " & csID)
                Else
                    'check bank account number is the same as on bank accounts table
                    Dim testAccountNumber As String = bankAccountdata.GetBankAccount(csID)
                    Dim testLong As Long
                    Try
                        testLong = testAccountNumber
                    Catch ex As Exception
                        testLong = 0
                    End Try
                    If benAcctNo <> testLong Then
                        MsgBox("bank Account Number has changed on Onestep from " & testLong & " to " & benAcctNo & vbNewLine &
                        "ClientSchemeID = " & csID)
                    End If
                End If
            End If
           
            If bacs1rbtn.Checked Then
                process_bacs_single()
            ElseIf bacsmultrbtn.Checked Then
                process_bacs_multiple()
            ElseIf chapsrbtn.Checked Then
                process_chaps()
            Else
                process_fp()
            End If
        Next

        'write file
        If fasterrbtn.Checked Then
            WriteFile(InputFilePath & FileName & reportSuffix & seqNo & ".txt", outfile)
        ElseIf bacsmultrbtn.Checked Then
            'add bacs header+
            'add 2 working days to todays date
            'add first day
            Dim bacsDate As Date = Now
            Dim newDateFound As Boolean = False
            While Not newDateFound
                bacsDate = DateAdd(DateInterval.Day, 1, bacsDate)
                If bacsDate.DayOfWeek = 6 Or bacsDate.DayOfWeek = 0 Then  'Saturday/Sunday
                    bacsDate = DateAdd(DateInterval.Day, 1, bacsDate)
                Else
                    'check if a bank holiday
                    bacsDate = CDate(Format(bacsDate, "yyyy, MMM dd") & " 00:00:00")
                    Dim testArray As Object = Nothing
                    Try
                        testArray = GetSQLResultsArray("FeesSQL", "SELECT count(*) " & _
                                                      "FROM BankHolidayDates " & _
                                                      "WHERE BankHolidayDate= '" & bacsDate.ToString("dd/MMM/yyyy") & "'")
                    Catch ex As Exception

                    End Try
                    Try
                        If testArray(0) > 0 Then
                            bacsDate = DateAdd(DateInterval.Day, 1, bacsDate)
                        Else
                            newDateFound = True
                        End If
                    Catch ex As Exception
                        newDateFound = True
                    End Try
                    
                End If
            End While
            'add second day
            newDateFound = False
            While Not newDateFound
                bacsDate = DateAdd(DateInterval.Day, 1, bacsDate)
                If bacsDate.DayOfWeek = 6 Or bacsDate.DayOfWeek = 0 Then  'Saturday/Sunday
                    bacsDate = DateAdd(DateInterval.Day, 1, bacsDate)
                Else
                    'check if a bank holiday
                    bacsDate = CDate(Format(bacsDate, "yyyy, MMM dd") & " 00:00:00")
                    Dim testArray As Object = Nothing
                    Try
                        testArray = GetSQLResultsArray("FeesSQL", "SELECT count(*) " & _
                                                      "FROM BankHolidayDates " & _
                                                      "WHERE BankHolidayDate= '" & bacsDate.ToString("dd/MMM/yyyy") & "'")
                    Catch ex As Exception

                    End Try
                    Try
                        If testArray(0) > 0 Then
                            bacsDate = DateAdd(DateInterval.Day, 1, bacsDate)
                        Else
                            newDateFound = True
                        End If
                    Catch ex As Exception
                        newDateFound = True
                    End Try
                   
                End If
            End While
          
            outfile = "BACS_Payment,," & debitSortCode & "," & debitAcctNo & ",GBP,GBP,," &
              totalAmount & ",,," & Format(bacsDate, "dd-MMM-yyyy") & ",,,,,,,,,,,,,,,,,,,,,,," _
             & "Remittance" & ",,,,,,,,,,,,,,,,,,,,,,,," & vbNewLine & outfile
            WriteFile(InputFilePath & FileName & reportSuffix, outfile)
        Else
            WriteFile(InputFilePath & FileName & reportSuffix, outfile)
        End If

        MsgBox("File written")
        Me.Close()

    End Sub
    Private Sub process_bacs_single()
       
        outfile &= "Single_BACS_GBP_Payment" & ",," & debitSortCode & "," & debitAcctNo & "," _
            & "GBP,GBP,," & payment & ",,," & Format(payDate, "dd-MMM-yyyy") & "," _
            & "," & benAcctNo & "," & benSortCode & ",,," & benAcctName & ",,,,,,,,,,,,,,,,," & _
            remitRef & ",,,,,,,,,,,,,,,,,,,,,,,," & vbNewLine
    End Sub
    Private Sub process_bacs_multiple()
        outfile &= "Ben" & ",,,,,,,,,,,," & benAcctNo & "," & benSortCode & "," _
            & payment & ",," & benAcctName & ",,,,,,,,,,,,,,,,,," & _
            remitRef & ",,,,,,,,,,,,,,,,,,,,,,," & vbNewLine
        totalAmount += payment
    End Sub
    Private Sub process_chaps()
        outfile &= "Chaps_GBP_Payment" & ",," & debitSortCode & "," & debitAcctNo & "," _
            & "GBP,GBP,," & payment & ",,," & Format(payDate, "dd-MMM-yyyy") & ",," _
            & benAcctNo & "," & benSortCode & ",,," & benAcctName & ",,,,,,,,,,,,,,,,," & _
            remitRef & ",,,,,,,,,,,,,,,,,,,,,,,," & vbNewLine
    End Sub
    Private Sub process_fp()
       
        If totalAmount + payment > 100000 Then
            If payment > 100000 Then
                If totalAmount > 0 Then
                    WriteFile(InputFilePath & FileName & reportSuffix & seqNo & ".txt", outfile)
                    seqNo += 1
                End If
                totalAmount = payment
                While totalAmount > 100000
                    outfile = "Faster_Payment" & ",," & debitSortCode & "," & debitAcctNo & "," _
            & "GBP,GBP,," & 100000 & ",,," & Format(payDate, "dd-MMM-yyyy") & ",," _
            & benAcctNo & "," & benSortCode & ",,," & benAcctName & ",,,,,,,,,,,,,,,,," & _
            remitRef & ",,,,,,,,,,,,,,,,,,,,,,,," & vbNewLine
                    WriteFile(InputFilePath & FileName & reportSuffix & seqNo & ".txt", outfile)
                    seqNo += 1
                    totalAmount -= 100000
                End While
                payment = totalAmount
            Else
                WriteFile(InputFilePath & FileName & reportSuffix & seqNo & ".txt", outfile)
                seqNo += 1
            End If

            totalAmount = payment
            outfile = ""
        Else
            totalAmount += payment
        End If

        outfile &= "Faster_Payment,," & debitSortCode & "," & debitAcctNo & "," _
            & "GBP,GBP,," & payment & ",,," & Format(payDate, "dd-MMM-yyyy") & ",," _
            & benAcctNo & "," & benSortCode & ",,," & benAcctName & ",,,,,,,,,,,,,,,,," & _
             remitRef & ",,,,,,,,,,,,,,,,,,,,,,,," & vbNewLine
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class


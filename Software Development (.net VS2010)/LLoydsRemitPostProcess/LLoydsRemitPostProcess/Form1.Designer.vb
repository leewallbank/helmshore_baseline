﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.fasterrbtn = New System.Windows.Forms.RadioButton()
        Me.chapsrbtn = New System.Windows.Forms.RadioButton()
        Me.bacsmultrbtn = New System.Windows.Forms.RadioButton()
        Me.bacs1rbtn = New System.Windows.Forms.RadioButton()
        Me.readbtn = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.fasterrbtn)
        Me.GroupBox1.Controls.Add(Me.chapsrbtn)
        Me.GroupBox1.Controls.Add(Me.bacsmultrbtn)
        Me.GroupBox1.Controls.Add(Me.bacs1rbtn)
        Me.GroupBox1.Location = New System.Drawing.Point(26, 44)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 176)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Select Post process"
        '
        'fasterrbtn
        '
        Me.fasterrbtn.AutoSize = True
        Me.fasterrbtn.Location = New System.Drawing.Point(19, 135)
        Me.fasterrbtn.Name = "fasterrbtn"
        Me.fasterrbtn.Size = New System.Drawing.Size(103, 17)
        Me.fasterrbtn.TabIndex = 3
        Me.fasterrbtn.TabStop = True
        Me.fasterrbtn.Text = "Faster Payments"
        Me.fasterrbtn.UseVisualStyleBackColor = True
        '
        'chapsrbtn
        '
        Me.chapsrbtn.AutoSize = True
        Me.chapsrbtn.Location = New System.Drawing.Point(19, 94)
        Me.chapsrbtn.Name = "chapsrbtn"
        Me.chapsrbtn.Size = New System.Drawing.Size(61, 17)
        Me.chapsrbtn.TabIndex = 2
        Me.chapsrbtn.TabStop = True
        Me.chapsrbtn.Text = "CHAPS"
        Me.chapsrbtn.UseVisualStyleBackColor = True
        '
        'bacsmultrbtn
        '
        Me.bacsmultrbtn.AutoSize = True
        Me.bacsmultrbtn.Location = New System.Drawing.Point(19, 59)
        Me.bacsmultrbtn.Name = "bacsmultrbtn"
        Me.bacsmultrbtn.Size = New System.Drawing.Size(92, 17)
        Me.bacsmultrbtn.TabIndex = 1
        Me.bacsmultrbtn.TabStop = True
        Me.bacsmultrbtn.Text = "BACS Multiple"
        Me.bacsmultrbtn.UseVisualStyleBackColor = True
        '
        'bacs1rbtn
        '
        Me.bacs1rbtn.AutoSize = True
        Me.bacs1rbtn.Checked = True
        Me.bacs1rbtn.Location = New System.Drawing.Point(19, 19)
        Me.bacs1rbtn.Name = "bacs1rbtn"
        Me.bacs1rbtn.Size = New System.Drawing.Size(85, 17)
        Me.bacs1rbtn.TabIndex = 0
        Me.bacs1rbtn.TabStop = True
        Me.bacs1rbtn.Text = "BACS Single"
        Me.bacs1rbtn.UseVisualStyleBackColor = True
        '
        'readbtn
        '
        Me.readbtn.Location = New System.Drawing.Point(73, 238)
        Me.readbtn.Name = "readbtn"
        Me.readbtn.Size = New System.Drawing.Size(75, 23)
        Me.readbtn.TabIndex = 1
        Me.readbtn.Text = "Read File"
        Me.readbtn.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 276)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(268, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 311)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.readbtn)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "LLoyds Remit Post Process"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents fasterrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents chapsrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents bacsmultrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents bacs1rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents readbtn As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar

End Class

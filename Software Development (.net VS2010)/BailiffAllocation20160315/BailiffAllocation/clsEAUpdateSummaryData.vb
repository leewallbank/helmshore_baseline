﻿Imports CommonLibrary

Public Class clsEAUpdateSummaryData
    Private LastLoadDT As New DataTable
    Private SummaryDT As New DataTable
    Private DetailDT As New DataTable
    Private PeriodTypeDT As New DataTable
    Private CompanyDT As New DataTable
    Private EAUpdateAttributesDT As New DataTable

    Private LastLoadDV As DataView
    Private SummaryDV As DataView
    Private DetailDV As DataView
    Private PeriodTypeDV As New DataView
    Private CompanyDV As DataView

    Public Sub New()
        Try

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPeriodTypes 'U'", PeriodTypeDT)
            PeriodTypeDV = New DataView(PeriodTypeDT)
            PeriodTypeDV.AllowDelete = False
            PeriodTypeDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetCompanies 'U'", CompanyDT)
            CompanyDV = New DataView(CompanyDT)
            CompanyDV.AllowDelete = False
            CompanyDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#Region " Public Properties"

    Public ReadOnly Property LastLoadDataView() As DataView
        Get
            LastLoadDataView = LastLoadDV
        End Get
    End Property

    Public ReadOnly Property SummaryDataView() As DataView
        Get
            SummaryDataView = SummaryDV
        End Get
    End Property

    Public ReadOnly Property DetailDataView() As DataView
        Get
            DetailDataView = DetailDV
        End Get
    End Property

    Public ReadOnly Property PeriodTypeDataView() As DataView
        Get
            PeriodTypeDataView = PeriodTypeDV
        End Get
    End Property

    Public ReadOnly Property CompanyDataView() As DataView
        Get
            CompanyDataView = CompanyDV
        End Get
    End Property

    Public ReadOnly Property EAUpdateAttributes() As DataTable
        Get
            EAUpdateAttributes = EAUpdateAttributesDT
        End Get
    End Property

#End Region

#Region "Public Methods"

    Public Sub GetSummary(ByVal ParamList As String, ByVal Abs As Boolean)
        Try
            Dim SummaryParamList As String = ""

            SummaryDV = Nothing ' Needed to clear column list otherwise new columns eg clientname are added at the end

            If Abs Then SummaryParamList = ParamList & ",'A'" Else SummaryParamList = ParamList & ",'P'"

            SummaryDT.Clear()
            SummaryDT.Reset()

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateSummary " & SummaryParamList, SummaryDT)
            SummaryDV = New DataView(SummaryDT)
            SummaryDV.AllowDelete = False
            SummaryDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetLastLoad()
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetLastLoad", LastLoadDT)
            LastLoadDV = New DataView(LastLoadDT)
            LastLoadDV.AllowDelete = False
            LastLoadDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub RefreshDatabase()
        Try
            ExecStoredProc("BailiffAllocation", "EXEC dbo.BuildBailiffAllocation 'I'", 1800)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetEAUpdateAttributes(CompanyID As String, DebtorID As String)

        EAUpdateAttributesDT.Clear()
        EAUpdateAttributesDT.Reset()

        LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateAttributes " & CompanyID & ", " & DebtorID, EAUpdateAttributesDT)

    End Sub




#End Region

End Class

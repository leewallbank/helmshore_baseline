Public Class mainform



    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline, caption As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim name As String = ""
        Dim name2 As String = ""
        Dim telNo1 As String = ""
        Dim telNo2 As String = ""
        Dim emailNo1 As String = ""
        Dim emailNo2 As String = ""
        Dim lonum As String = ""
        Dim lostring As String = ""
        Dim idx As Integer
        Dim lines As Integer = 0
        Dim debt_amt, costs As Decimal
        Dim fromdate, todate As Date
        Dim warrantDate As String = ""
        Dim clref As String = ""
        Dim addrline As String = ""
        Dim amtstring As String = ""
        Dim comments As String = Nothing
        Dim cases As Integer = 0
        Dim totAmount As Decimal = 0
        Dim GTotAmount As Decimal
        Dim curBalance As Decimal
        Dim debtAddr1, debtAddr2, debtAddr3, debtAddr4, debtAddr5, debtAddr6 As String
        Dim currAddr1, currAddr2, curraddr3, currAddr4, currAddr5, currAddr6 As String
        Dim courtName As String = ""
        Dim courtAddress As String = ""
        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "Client Reference|1st. Liable Name|Other Names|FwdAddress1|FwdAddress2|FwdAddress3|FwdAddress4|FwdAddress5|" & _
            "FwdAddress6|DebtAddress1|DebtAddress2|DebtAddress3|DebtAddress4|DebtAddress5|DebtAddress6|Debt|CourtCosts|LiabilityOrderAmount|" & _
            "CurrentBalance|FromDate|ToDate|WarrantDate|CourtName|CourtAddress|Tel No1|TelNo2|Email|Comments" & vbNewLine
        outfile = outline

        'look for Account Number
        For idx = 1 To lines - 1
            caption = Mid(line(idx), 2, 10)
            If caption = "Hearing Da" Then
                warrantDate = Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 32)
                idx += 1
                courtName = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 32))
                idx += 1
                courtAddress = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 32))
            End If
            If caption = "Account No" Then
                clref = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 13))
                cases += 1
                idx = idx + 1
                caption = Mid(line(idx), 2, 10)
                name = ""
                name2 = ""
                currAddr1 = ""
                currAddr2 = ""
                curraddr3 = ""
                currAddr4 = ""
                currAddr5 = ""
                currAddr6 = ""
                debtAddr1 = ""
                debtAddr2 = ""
                debtAddr3 = ""
                debtAddr4 = ""
                debtAddr5 = ""
                debtAddr6 = ""
                debt_amt = 0
                fromdate = Nothing
                todate = Nothing
                telNo1 = ""
                telNo2 = ""
                emailNo1 = ""
                emailNo2 = ""
                comments = ""
                While caption <> "Account No" And caption <> "Hearing Da" And idx < lines - 1
                    Select Case caption
                        Case "Name/Corre"
                            'skip 2 lines
                            idx += 2
                            name = Trim(Mid(line(idx), 2, 39))
                            fromdate = Mid(line(idx), 86, 10)
                            debt_amt = Microsoft.VisualBasic.Right(line(idx), 15)
                            'next line has addresses line 1, todate and costs
                            idx += 1
                            currAddr1 = Trim(Mid(line(idx), 2, 39))
                            debtAddr1 = Trim(Mid(line(idx), 43, 40))
                            todate = Mid(line(idx), 86, 10)
                            costs = Microsoft.VisualBasic.Right(line(idx), 15)
                            'next line has addresses line 2
                            idx += 1
                            currAddr2 = Trim(Mid(line(idx), 2, 39))
                            debtAddr2 = Trim(Mid(line(idx), 43, 40))
                            'next line has addresses line 3 and total
                            idx += 1
                            curraddr3 = Trim(Mid(line(idx), 2, 39))
                            debtAddr3 = Trim(Mid(line(idx), 43, 40))
                            totAmount = Microsoft.VisualBasic.Right(line(idx), 15)

                            'next line has addresses line 4
                            idx += 1
                            currAddr4 = Trim(Mid(line(idx), 2, 39))
                            debtAddr4 = Trim(Mid(line(idx), 43, 40))
                            'next line has addresses line 5 and balance
                            idx += 1
                            currAddr5 = Trim(Mid(line(idx), 2, 39))
                            debtAddr5 = Trim(Mid(line(idx), 43, 40))
                            curBalance = Microsoft.VisualBasic.Right(line(idx), 15)
                            GTotAmount += curBalance
                            'next line has addresses line 6
                            idx += 1
                            currAddr6 = Trim(Mid(line(idx), 2, 39))
                            debtAddr6 = Trim(Mid(line(idx), 43, 40))
                        Case "Other Name"
                            name2 = Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 14)
                            'check to see if next line is a continuation of name
                            Dim nextLine As String = line(idx + 1)
                            nextLine = Replace(nextLine, Chr(10), "")
                            nextLine = Replace(nextLine, Chr(13), "")
                            Try
                                If nextLine.Length > 3 Then
                                    If InStr(nextLine, "Hearing Date & Time") = 0 Then
                                        name2 = Trim(name2 & " " & Trim(nextLine))
                                    End If
                                End If
                            Catch ex As Exception

                            End Try
                        Case "Day Teleph"
                            If telNo1 = "" Then
                                telNo1 = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 21))
                            Else
                                telNo2 = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 21))
                            End If
                        Case "E-Mail Add"
                            Dim email As String
                            email = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 21))
                            'check next line for rest of address
                            Dim nextLine As String = line(idx + 1)
                            nextLine = Replace(nextLine, Chr(10), "")
                            nextLine = Replace(nextLine, Chr(13), "")
                            Try
                                If nextLine.Length > 3 Then
                                    If InStr(nextLine, "Hearing Date & Time") + InStr(nextLine, "E-mail") = 0 Then
                                        email &= Trim(Microsoft.VisualBasic.Right(nextLine, nextLine.Length - 21))
                                    End If
                                End If
                            Catch ex As Exception

                            End Try
                            If emailNo1 = "" Then
                                emailNo1 = email
                            Else
                                comments &= "Email:" & email & ";"
                            End If
                        Case Else
                            If InStr(line(idx), "hone") > 0 Then
                                If telNo1 = "" Then
                                    telNo1 = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 22))
                                Else
                                    telNo2 = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 22))
                                End If
                            Else
                                If InStr(line(idx), "E-mail") > 0 Then
                                    Dim email As String
                                    email = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 21))
                                    'check next line for rest of address
                                    Dim nextLine As String = line(idx + 1)
                                    nextLine = Replace(nextLine, Chr(10), "")
                                    nextLine = Replace(nextLine, Chr(13), "")
                                    Try
                                        If nextLine.Length > 3 Then
                                            If InStr(nextLine, "Hearing Date & Time") + InStr(nextLine, "E-mail") = 0 Then
                                                email &= Trim(Microsoft.VisualBasic.Right(nextLine, nextLine.Length - 21))
                                            End If
                                        End If
                                    Catch ex As Exception

                                    End Try
                                    If emailNo1 = "" Then
                                        emailNo1 = email
                                    Else
                                        comments &= "Email:" & email & ";"
                                    End If
                                End If
                            End If
                    End Select
                    idx += 1
                    caption = Mid(line(idx), 2, 10)
                End While

                'validate case details
                If clref = Nothing Or Microsoft.VisualBasic.Left(clref, 3) = "" Then
                    If idx > 6 Then
                        errorfile = errorfile & "Line  " & idx & " - No client reference" & vbNewLine
                    End If
                Else
                    'save case in outline
                    outfile = outfile & clref & "|" & name & "|" & name2 & "|" & currAddr1 & "|" & currAddr2 & "|" & curraddr3 & "|" & _
                        currAddr4 & "|" & currAddr5 & "|" & currAddr6 & "|" & debtAddr1 & "|" & debtAddr2 & "|" & debtAddr3 & "|" & debtAddr4 & "|" & _
                        debtAddr5 & "|" & debtAddr6 & "|" & debt_amt & "|" & costs & "|" & totAmount & "|" & curBalance & "|" & _
                        Format(fromdate, "dd-MM-yyyy") & "|" & Format(todate, "dd-MM-yyyy") & "|" & warrantDate & "|" & courtName & "|" & courtAddress & "|" & telNo1 & "|" & telNo2 & "|" & _
                        emailNo1 & "|" & comments & vbNewLine
                    idx -= 1
                End If
            End If
        Next
        
        viewbtn.Enabled = True
        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
            MsgBox("Cases: " & cases & vbNewLine & "Amount: " & GTotAmount)
        End If
        Me.Close()
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
End Class

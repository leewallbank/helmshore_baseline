﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.rball2ea = New System.Windows.Forms.RadioButton()
        Me.rb12EA = New System.Windows.Forms.RadioButton()
        Me.rball2u = New System.Windows.Forms.RadioButton()
        Me.rb12u = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(41, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(183, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Email all documents in selected folder"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(41, 88)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(181, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "EA ID should be in brackets eg (925)"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(26, 127)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(234, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "The file name must also contain the EA surname"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(95, 363)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(127, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Select first document"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(29, 409)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(276, 23)
        Me.ProgressBar1.TabIndex = 4
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rball2ea)
        Me.GroupBox1.Controls.Add(Me.rb12EA)
        Me.GroupBox1.Controls.Add(Me.rball2u)
        Me.GroupBox1.Controls.Add(Me.rb12u)
        Me.GroupBox1.Location = New System.Drawing.Point(44, 163)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 152)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Choose Option"
        '
        'rball2ea
        '
        Me.rball2ea.AutoSize = True
        Me.rball2ea.Location = New System.Drawing.Point(7, 129)
        Me.rball2ea.Name = "rball2ea"
        Me.rball2ea.Size = New System.Drawing.Size(138, 17)
        Me.rball2ea.TabIndex = 8
        Me.rball2ea.Text = "Send ALL emails to EAs"
        Me.rball2ea.UseVisualStyleBackColor = True
        '
        'rb12EA
        '
        Me.rb12EA.AutoSize = True
        Me.rb12EA.Location = New System.Drawing.Point(6, 89)
        Me.rb12EA.Name = "rb12EA"
        Me.rb12EA.Size = New System.Drawing.Size(127, 17)
        Me.rb12EA.TabIndex = 7
        Me.rb12EA.Text = "Send one email to EA"
        Me.rb12EA.UseVisualStyleBackColor = True
        '
        'rball2u
        '
        Me.rball2u.AutoSize = True
        Me.rball2u.Location = New System.Drawing.Point(6, 53)
        Me.rball2u.Name = "rball2u"
        Me.rball2u.Size = New System.Drawing.Size(136, 17)
        Me.rball2u.TabIndex = 6
        Me.rball2u.Text = "Send ALL emails to you"
        Me.rball2u.UseVisualStyleBackColor = True
        '
        'rb12u
        '
        Me.rb12u.AutoSize = True
        Me.rb12u.Checked = True
        Me.rb12u.Location = New System.Drawing.Point(7, 20)
        Me.rb12u.Name = "rb12u"
        Me.rb12u.Size = New System.Drawing.Size(132, 17)
        Me.rb12u.TabIndex = 0
        Me.rb12u.TabStop = True
        Me.rb12u.Text = "Send One email to you"
        Me.rb12u.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(354, 458)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Email EA Invoices"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rball2ea As System.Windows.Forms.RadioButton
    Friend WithEvents rb12EA As System.Windows.Forms.RadioButton
    Friend WithEvents rball2u As System.Windows.Forms.RadioButton
    Friend WithEvents rb12u As System.Windows.Forms.RadioButton

End Class

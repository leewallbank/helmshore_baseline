﻿Imports System.IO
Imports CommonLibrary

Public Class Form1
    Dim exePath As String = "C:\Program Files\7-Zip\7z.exe"
    Dim args As String = ""
    Private InputFilePath As String, FileName As String, FileExt As String
    Dim audit_file As String
    Dim subject As String = ""
    Dim fromaddress As String
    Dim toaddress As String = ""
    Dim body, attachment As String
    Dim att1 As String = ""
    'Dim att2 As String = " "
    'Dim att3 As String = " "
    'Dim att4 As String = " "
    'Dim att5 As String = " "
    'Dim att6 As String = " "
    'Dim att7 As String = " "
    'Dim att8 As String = " "
    'Dim att9 As String = " "
    Dim attachmentIDX As Integer
    Dim selectedEA As Integer
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Dim prod_run As Boolean = False
        Dim env_str As String = ""
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
            If env_str = "Prod" Then
                prod_run = True
            End If
        Catch ex As Exception
            prod_run = False
        End Try
        Dim password As String
        If prod_run Then
            password = InputBox("Enter password", "Enter password")
            If password <> "EARDRUM" Then
                MsgBox("Invalid password")
                Me.Close()
                Exit Sub
            End If
        End If
        Dim filedialog As New OpenFileDialog
        If Not filedialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        InputFilePath = Path.GetDirectoryName(filedialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(filedialog.FileName)
        FileExt = Path.GetExtension(filedialog.FileName)
        'delete/create sub folder for documents
        Dim EADocsDirectoryName As String = InputFilePath & "EADOCS"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(EADocsDirectoryName)
            If System.IO.Directory.Exists(EADocsDirectoryName) = False Then
                di = System.IO.Directory.CreateDirectory(EADocsDirectoryName)
            Else
                System.IO.Directory.Delete(EADocsDirectoryName, True)
                di = System.IO.Directory.CreateDirectory(EADocsDirectoryName)
            End If
        Catch ex As Exception
            MsgBox("Unable to create EADocs subfolder")
            End
        End Try
        selectedEA = 0
        Dim selectedEAemail As String = ""
        If rb12EA.Checked Or rb12u.Checked Then
            Try
                selectedEA = InputBox("Enter EA number", "Enter EA number")
            Catch ex As Exception
                MsgBox("Invalid EA number entered")
                Exit Sub
            End Try
            'check selected ea number is valid
            Dim EARow As Object
            EARow = GetSQLResultsArray("DebtRecovery", "SELECT name_fore, name_sur, add_email from bailiff " & _
                                                    "WHERE _rowid= " & selectedEA)
            Dim EAName As String = ""
            Try
                EAName = EARow(0) & " "
            Catch ex As Exception

            End Try
            selectedEAemail = EARow(2)
            If rb12u.Checked Then
                selectedEAemail = My.User.Name & "@rossendales.com"
                selectedEAemail = Replace(selectedEAemail, "MGL\", "")
            End If
            EAName &= EARow(1)
            If MsgBox("Send email for " & EAName & " to email:" & selectedEAemail, MsgBoxStyle.YesNo, "Check EA Name") = MsgBoxResult.No Then
                MsgBox("No email sent")
                Exit Sub
            End If
        Else
            If rball2u.Checked Then
                If MsgBox("Last Chance to STOP emails. Do you want to send ALL emails to yourself?", MsgBoxStyle.YesNo, "Send All emails to You") = MsgBoxResult.No Then
                    MsgBox("NO EMAILS SENT")
                    Exit Sub
                End If
            Else
                If rball2ea.Checked Then
                    If MsgBox("Last Chance to STOP emails. Do you want to send ALL emails to the EAs?", MsgBoxStyle.YesNo, "Send All emails to EAs") = MsgBoxResult.No Then
                        MsgBox("NO EMAILS SENT")
                        Exit Sub
                    End If
                End If
            End If
        End If


        Dim EAdt As New DataTable
        LoadDataTable("DebtRecovery", "SELECT _rowid, name_sur, add_email from bailiff " & _
                                                "WHERE (typeSub is null or typeSub <> 'Employed')" & _
                                                " and agent_type='B'" & _
                                                " and status = 'O'", EAdt, False)
        ProgressBar1.Maximum = EAdt.Rows.Count

        Dim rowCount As Integer
        For Each EARow In EAdt.Rows
            rowCount += 1
            ProgressBar1.Value = rowCount
            Application.DoEvents()
          
            Dim EAID As Integer = EARow(0)
            If selectedEA > 0 And EAID <> selectedEA Then
                Continue For
            End If
            
            Dim EASurname As String = UCase(EARow(1))
            Dim emailAddress As String = ""
            Try
                emailAddress = EARow(2)
            Catch ex As Exception

            End Try
            If InStr(emailAddress, "@") = 0 Then
                emailAddress = ""
            End If
            If EAID = selectedEA Then
                emailAddress = selectedEAemail
            End If
            attachmentIDX = 0
            att1 = ""
            'att2 = " "
            'att3 = " "
            'att4 = " "
            'att5 = " "
            'att6 = " "
            'att7 = " "
            'att8 = " "
            'att9 = " "
            fromaddress = "crystal@rossendales.com"
            If env_str <> "Prod" Then
                toaddress = "JBlundell@rossendales.com"
            Else
                toaddress = emailAddress
            End If
            subject = "Remuneration Statements"
            body = "Good Morning"
            If Format(Now, "HH") >= 12 Then
                body = "Good afternoon"
            End If
            body &= vbNewLine
            body &= "Please find attached remuneration statements for last month"
            body &= vbNewLine & vbNewLine & "Regards" & vbNewLine
            body &= "Accounts Team" & vbNewLine & _
                    "Rossendales" & vbNewLine
            'see if any documents for EAID
            Dim EAIDSearch As String = "*(" & EAID & ")*.*"
            Dim document_name As String
            For Each foundFile As String In My.Computer.FileSystem.GetFiles(
              InputFilePath,
               Microsoft.VisualBasic.FileIO.SearchOption.SearchTopLevelOnly, EAIDSearch)
                document_name = UCase(foundFile)
                ' CreateZipFile(document_name, InputFilePath)
                If InStr(document_name, EASurname) = 0 Then
                    audit_file &= document_name & "," & "ignored as EA Surname " & EASurname & " not in document name" & vbNewLine
                    Continue For
                End If

                If emailAddress = "" Then
                    audit_file &= document_name & "," & "ignored as EA has no valid email address" & vbNewLine
                    Continue For
                End If
                attachmentIDX += 1

                'move document to subfolder
                FileName = Path.GetFileName(document_name)
                Dim newdoc_name As String = InputFilePath & "EADocs\" & EAID & "\" & FileName
                My.Computer.FileSystem.CopyFile(document_name, newdoc_name, True)


                'Select Case attachmentIDX
                '    Case 1
                '        att1 = document_name
                '    Case 2
                '        att2 = document_name
                '    Case 3
                '        att3 = document_name
                '    Case 4
                '        att4 = document_name
                '    Case 5
                '        att5 = document_name
                '    Case 6
                '        att6 = document_name
                '    Case 7
                '        att7 = document_name
                '    Case 8
                '        att8 = document_name
                '    Case 9
                '        att9 = document_name
                '    Case Else
                '        MsgBox("More than 10 attachments - only 9 sent to " & emailAddress)
                '        Exit For
                'End Select
            Next

            If attachmentIDX > 0 Then
                'save document in zip file
                Dim EApassword As String = UCase(Microsoft.VisualBasic.Left(EASurname, 3))
                Dim passwordNumber As Integer = (EAID * EAID) + 52392
                EApassword &= Microsoft.VisualBasic.Left(passwordNumber, 5)
                args = "a " & InputFilePath & "EADocs\" & EAID & "\" & EAID & ".7z " & InputFilePath & "EADocs\" & EAID & "\*" & " -p" & EApassword
                'args = "a " & InputFilePath & "EADocs\" & EAID & "\" & EAID & ".7z " & InputFilePath & "EADocs\" & EAID & "\*"
                Try
                    Process.Start(exePath, args)
                Catch ex As Exception
                    MsgBox(ex.Message.ToString)
                End Try

                att1 = InputFilePath & "EADocs\" & EAID & "\" & EAID & ".7z"
                'MsgBox("ATT1" & att1)
                'wait 
                System.Threading.Thread.Sleep(5000)
                send_email()
            End If

        Next

        My.Computer.FileSystem.WriteAllText(InputFilePath & "Audit.csv", audit_file, False)
        MsgBox("Completed")
        Me.Close()
    End Sub
    Sub send_email()
        'If email(toaddress, fromaddress, subject, body, att1, att2, att3, att4, att5, att6, att7, att8, att9) = 0 Then
        ' MsgBox("To address" & toaddress)
        Try
            If email(toaddress, fromaddress, subject, body, att1) = 0 Then
                audit_file &= "1 document sent to " & toaddress & "," & att1 & vbNewLine
            Else
                audit_file &= error_message & vbNewLine
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
    End Sub
End Class

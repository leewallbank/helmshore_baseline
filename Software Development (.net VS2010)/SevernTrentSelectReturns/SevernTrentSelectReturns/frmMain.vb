﻿Imports System.Configuration
Imports System.IO
Imports CommonLibrary

Public Class frmMain
    Private Returns As New ReturnsData

    Private SourceRow As Integer, SourceCol As Integer

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        If Now.DayOfWeek = DayOfWeek.Monday Then
            dtpDateFrom.Value = DateAdd(DateInterval.Day, -2, Now)
            dtpDateTo.Value = Now
        Else
            dtpDateFrom.Value = Now
            dtpDateTo.Value = dtpDateFrom.Value
        End If

        cmsDGV.Items.Add("Cut")
        cmsDGV.Items.Add("Copy")
        cmsDGV.Items.Add("Paste")

        btnCreateReturns.Enabled = False
    End Sub

    Private Sub dgvMain_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMain.CellValueChanged
        Try
            If Not IsDBNull(dgvMain(e.ColumnIndex, e.RowIndex).Value) Then dgvMain(e.ColumnIndex, e.RowIndex).Value = LTrim(dgvMain(e.ColumnIndex, e.RowIndex).Value)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvMain.MouseDown
        Try
            Dim p As Point = dgvMain.PointToClient(Control.MousePosition)
            Dim hit As DataGridView.HitTestInfo = dgvMain.HitTest(p.X, p.Y)

            SourceRow = hit.RowIndex
            SourceCol = hit.ColumnIndex

            If hit.Type = DataGridViewHitTestType.Cell And e.Button = MouseButtons.Left Then
                dgvMain.BeginEdit(False)
                If Not dgvMain(hit.ColumnIndex, hit.RowIndex).IsInEditMode Then
                    dgvMain.DoDragDrop(dgvMain(hit.ColumnIndex, hit.RowIndex).Value, DragDropEffects.Copy Or DragDropEffects.Move)
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvMain.MouseUp
        Try
            If e.Button = MouseButtons.Right Then cmsDGV.Show(sender, e.Location)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvMain.DragEnter
        Try
            If (e.Data.GetDataPresent(DataFormats.Text)) Then
                e.Effect = DragDropEffects.Copy
            Else
                e.Effect = DragDropEffects.None
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvMain.DragDrop
        Try
            Dim p As System.Drawing.Point = dgvMain.PointToClient(Control.MousePosition)
            Dim hit As DataGridView.HitTestInfo = dgvMain.HitTest(p.X, p.Y)

            If hit.Type = DataGridViewHitTestType.Cell AndAlso SourceRow = hit.RowIndex AndAlso Not dgvMain(hit.ColumnIndex, hit.RowIndex).ReadOnly Then ' Check rows as we do not want data moving from one case to another
                dgvMain(SourceCol, SourceRow).Value = ""
                ' if drop position is in the left hand half of the cell, insert data before the existing contents, else insert after
                If p.X < dgvMain.GetCellDisplayRectangle(hit.ColumnIndex, hit.RowIndex, False).Left + (dgvMain.Columns(hit.ColumnIndex).Width / 2) Then
                    dgvMain(hit.ColumnIndex, hit.RowIndex).Value = e.Data.GetData(DataFormats.Text).ToString & dgvMain(hit.ColumnIndex, hit.RowIndex).Value
                Else
                    dgvMain(hit.ColumnIndex, hit.RowIndex).Value &= e.Data.GetData(DataFormats.Text).ToString
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub frmMain_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If dgvMain.SelectedCells.Count = 0 Then Return

        Try
            Dim ClickCell As DataGridViewCell = dgvMain.SelectedCells(0)
            If e.Control Then
                Select Case e.KeyCode
                    Case Keys.C
                        Clipboard.SetText(ClickCell.Value, TextDataFormat.Text)
                    Case Keys.V
                        If Not ClickCell.ReadOnly Then ClickCell.Value = Clipboard.GetText(TextDataFormat.Text)
                    Case Keys.X
                        If Not ClickCell.ReadOnly Then
                            Clipboard.SetText(ClickCell.Value)
                            ClickCell.Value = ""
                        End If
                End Select
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dtpRemitDate_CloseUp(ByVal sender As Object, ByVal e As System.EventArgs)
        RefreshData()
    End Sub

    Private Sub cmdRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        lblNoData.Visible = False
        RefreshData()
    End Sub

    Private Sub cmsDGV_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsDGV.ItemClicked
        Try
            Dim ClickCell As DataGridViewCell = dgvMain.SelectedCells(0)
            Select Case e.ClickedItem.Text
                Case "Cut"
                    If Not ClickCell.ReadOnly Then
                        Clipboard.SetText(ClickCell.Value)
                        ClickCell.Value = ""
                    End If
                Case "Copy"
                    Clipboard.SetText(ClickCell.Value, TextDataFormat.Text)
                Case "Paste"
                    If Not ClickCell.ReadOnly Then ClickCell.Value = Clipboard.GetText(TextDataFormat.Text)
            End Select

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        Try
            If DataValid() Then MsgBox("Data is valid.", vbOKOnly + vbInformation, Me.Text)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub RefreshData()
        Try
            Me.Cursor = Cursors.WaitCursor

            Returns.GetAddresses(dtpDateFrom.Value, dtpDateTo.Value)
            Returns.GetDebtors(dtpDateFrom.Value, dtpDateTo.Value)

            With dgvMain
                .DataSource = Returns.Addresses

                For Each DataColumn As DataGridViewTextBoxColumn In .Columns
                    DataColumn.SortMode = DataGridViewColumnSortMode.NotSortable
                Next DataColumn

                .Columns("DebtorID").ReadOnly = True

            End With

            DataValid()

            Me.Cursor = Cursors.Default
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Function DataValid() As Boolean
        Dim ColLoopCount As Integer, RowLoopCount As Integer, MaxSize As Integer
        Dim ClipboardText As String = Nothing

        DataValid = True

        Try
            dgvMain.EndEdit()

            For Each DataColumn As DataGridViewTextBoxColumn In dgvMain.Columns
                ColLoopCount = DataColumn.Index

                MaxSize = Returns.Addresses.Table.Columns(ColLoopCount).ExtendedProperties("MaxLength")
                For RowLoopCount = 0 To dgvMain.RowCount - 1
                    With dgvMain(ColLoopCount, RowLoopCount)
                        If .Value.ToString.Length > MaxSize Then
                            .Style.BackColor = Color.Red
                            DataValid = False
                        Else
                            .Style.BackColor = Color.White
                        End If
                    End With
                Next RowLoopCount
            Next DataColumn

            For Each ReturnCase As DataRowView In Returns.Debtors
                If IsDBNull(ReturnCase.Item("clientReturnNumber")) Then
                    If Not IsNothing(ClipboardText) Then ClipboardText &= ","
                    ClipboardText &= ReturnCase.Item("DebtorID")
                    DataValid = False
                End If
            Next ReturnCase

            If Not IsNothing(ClipboardText) Then
                Clipboard.SetText(ClipboardText, TextDataFormat.Text)
                MsgBox("Cases have missing return codes." & vbCrLf & "IDs copied to clipboard.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, Me.Text)
            End If

            If Not DataValid Then MsgBox("Invalid data.", vbOKOnly + vbCritical, Me.Text)

            btnCreateReturns.Enabled = DataValid

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        Return DataValid
    End Function

    Private Sub btnCreateReturns_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateReturns.Click
        Dim ExceptionLog As String = "", FullName As String, Separator As String = ";"
        Try

            If Returns.Debtors.Count = 0 Then
                MsgBox("No cases found")
                Return
            End If

            Dim FolderBrowserDialog As New FolderBrowserDialog

            If FolderBrowserDialog.ShowDialog() <> Windows.Forms.DialogResult.OK Then
                Return
            End If

            ' Now start creating the file
            Dim FileName As String = "Waterplus_ROReturns_" & DateTime.Now.ToString("ddMMyy") & ".csv"
            Dim ReturnsFile As String = String.Join(Separator, {"Agency Response Code" _
                                                               , "Account Number" _
                                                               , "Agency Response Text" _
                                                               , "Customer Name" _
                                                               , "Invoice Address 1 (Street Number)" _
                                                               , "Invoice Address 2 (Street)" _
                                                               , "Invoice Address 3 (District Name)" _
                                                               , "Invoice Address 4 (City)" _
                                                               , "Invoice Address 5 (Postcode)" _
                                                               , "Invoice Address 6 (PO Box)" _
                                                               , "Phone number" _
                                                               , "DCA Instruction Marker" _
                                                               , "Leaver Final Settlement" _
                                                               , "Gone Away Date" _
                                                               , "Forwarding Address" _
                                                               }) & vbCrLf

            For Each Debtor As DataRowView In Returns.Debtors
                Dim OutputLineArray(14) As String
                OutputLineArray(0) = Debtor("clientReturnNumber").ToString '.PadLeft(2, "0")
                OutputLineArray(1) = Debtor("client_ref")

                FullName = Debtor("name_title")
                If Debtor("name_title") <> "" Then FullName &= " "
                FullName &= Debtor("name_fore")
                If Debtor("name_fore") <> "" Then FullName &= " "
                FullName &= Debtor("name_sur")

                OutputLineArray(3) = FullName

                OutputLineArray(11) = Strings.Left(Debtor("defaultCourtCode"), 3)

                Select Case Debtor.Item("clientReturnNumber")
                    Case 1
                        If Not IsDBNull(Debtor.Item("GoneAwayDate")) Then OutputLineArray(13) = CDate(Debtor.Item("GoneAwayDate")).ToString("yyyyMMdd") & vbCrLf

                    Case 8

                        Returns.Addresses.RowFilter = "DebtorID = " & Debtor("DebtorID")

                        If Returns.Addresses.Count > 0 Then

                            If Not IsDBNull(Returns.Addresses.Item(0).Item("Address")) Then OutputLineArray(2) = Returns.Addresses.Item(0).Item("Address") ' Added TS 02/Sep/2015. Request ref 59358

                            ' the following commented out TS 02/Sep/2015. Request ref 59358
                            'If Not IsDBNull(Returns.Addresses.Item(0).Item("HouseNumber")) Then OutputLineArray(4) = Returns.Addresses.Item(0).Item("HouseNumber")
                            'If Not IsDBNull(Returns.Addresses.Item(0).Item("Street")) Then OutputLineArray(5) = Returns.Addresses.Item(0).Item("Street")
                            'If Not IsDBNull(Returns.Addresses.Item(0).Item("District")) Then OutputLineArray(6) = Returns.Addresses.Item(0).Item("District")
                            'If Not IsDBNull(Returns.Addresses.Item(0).Item("City")) Then OutputLineArray(7) = Returns.Addresses.Item(0).Item("City")
                            'If Not IsDBNull(Returns.Addresses.Item(0).Item("PostCode")) Then OutputLineArray(8) = Returns.Addresses.Item(0).Item("PostCode")
                            'If Not IsDBNull(Returns.Addresses.Item(0).Item("POBox")) Then OutputLineArray(9) = Returns.Addresses.Item(0).Item("POBox")
                            If Not IsDBNull(Returns.Addresses.Item(0).Item("Phone")) Then OutputLineArray(10) = Returns.Addresses.Item(0).Item("Phone")

                        End If

                    Case 11
                        OutputLineArray(12) &= Debtor("debtPaid").ToString

                    Case 80 To 89, 35 To 42
                        Dim ReturnDetails As String = ""
                        If Not IsDBNull(Debtor("return_details")) Then ReturnDetails = Debtor("return_details")
                        ' Replace in this order so that a CrLf does not become two spaces
                        ReturnDetails = ReturnDetails.Replace(vbCrLf, " ")
                        ReturnDetails = ReturnDetails.Replace(vbCr, " ")
                        ReturnDetails = ReturnDetails.Replace(vbLf, " ")
                        ReturnDetails = ReturnDetails.Replace(vbTab, " ")
                        ReturnDetails = ReturnDetails.Replace(";", "") ' added TS 02/Sep/2015. Request ref 59358

                        OutputLineArray(2) = ReturnDetails

                End Select

                ReturnsFile &= String.Join(Separator, OutputLineArray) & vbCrLf

            Next Debtor

            Returns.Addresses.RowFilter = ""

            If ExceptionLog <> "" Then

                WriteFile(FolderBrowserDialog.SelectedPath & "\" & "Exceptions.txt", ExceptionLog)
                System.Diagnostics.Process.Start(FolderBrowserDialog.SelectedPath & "\Exceptions.txt")

            End If

            WriteFile(FolderBrowserDialog.SelectedPath & "\" & FileName, ReturnsFile)

            MsgBox("Returns file created.", vbOKOnly, Me.Text)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub

End Class

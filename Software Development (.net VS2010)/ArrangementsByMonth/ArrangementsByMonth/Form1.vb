﻿Imports System.Configuration
Imports System.Data
Imports System.Data.Odbc
Public Class Form1
    Dim file As String = "Debtor,arrdate,debt,paid" & vbNewLine
    Public ascii As New System.Text.ASCIIEncoding()


    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        ConnectDb2("DebtRecovery")
        Dim cs_dt As New DataTable
        Dim rowcount As Integer = 0
        My.Computer.FileSystem.WriteAllText("H:/temp/arrangements.csv", file, False, ascii)
        'get all cases in branch 2,8,12 that has had arrangement
        Dim start_date As Date = CDate("Jan 1, 2012 00:00:00")

        LoadDataTable2("DebtRecovery", "SELECT _rowid " & _
                                                "FROM clientScheme " & _
                                                "WHERE branchID = 2 or branchID = 8 or branchID = 12", cs_dt, False)
        For Each CSrow In cs_dt.Rows
            Dim csID As Integer = CSrow.item(0)
            Dim debt_dt As New DataTable
            LoadDataTable2("DebtRecovery", "SELECT _rowid, debt_amount, debt_costs, debtPaid " & _
                                               "FROM debtor " & _
                                               "WHERE clientSchemeID = " & csID & _
                                               " AND arrange_started is not null", debt_dt, False)
            For Each Debtrow In debt_dt.Rows
                'If rowcount > 100 Then
                '    Exit For
                'End If
                Dim note_dt As New DataTable

                Dim debtorid As Integer = Debtrow.item(0)
                'get first arrangement
                LoadDataTable2("DebtRecovery", "SELECT _createdDate " & _
                                               "FROM note " & _
                                               "WHERE debtorID = " & debtorid & _
                                               " AND type = 'Arrangement'" & _
                                               " order by _createdDate", note_dt, False)
                For Each noterow In note_dt.Rows
                    Dim Arrdate As Date = noterow.item(0)
                    Arrdate = CDate(Format(Arrdate, "MMM dd, yyyy") & " 00:00:00")
                    If Arrdate < start_date Then
                        Exit For
                    End If
                    Dim debt As Decimal = Debtrow.item(1) + Debtrow.item(2)
                    Dim debtPaid As Decimal = Debtrow.item(3)
                    Dim paid As Decimal = 0
                    rowcount += 1
                    If debtPaid > 0 Then
                        'get paid before arrangement date
                        Dim paid_dt As New DataTable
                        LoadDataTable2("DebtRecovery", "select split_debt, split_costs from payment" & _
                                       " where debtorID = " & debtorid & _
                                       " and status = 'R'" & _
                                       " and _createdDate < '" & Arrdate.ToString("yyyy-MM-dd") & "'", paid_dt, False)
                        For Each paidrow In paid_dt.Rows
                            paid += paidrow.item(0) + paidrow.item(1)
                        Next
                        debt -= paid
                        file = debtorid & "," & Arrdate & "," & debt & "," & paid & vbNewLine
                        My.Computer.FileSystem.WriteAllText("H:/temp/arrangements.csv", file, True, ascii)
                    Else
                        file = debtorid & "," & Arrdate & "," & debt & "," & paid & vbNewLine
                        My.Computer.FileSystem.WriteAllText("H:/temp/arrangements.csv", file, True, ascii)
                    End If
                    Exit For
                Next
            Next

        Next
        MsgBox("Completed")
        Me.Close()
    End Sub
End Class

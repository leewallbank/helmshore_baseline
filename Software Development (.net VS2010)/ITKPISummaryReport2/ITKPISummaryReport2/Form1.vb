﻿Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        process_month()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        process_month()
    End Sub
    Private Sub process_month()
        Dim end_date As Date = CDate(MonthName(Month(Now)) & " 1," & Year(Now))

        Dim run_date As Date = DateAdd(DateInterval.Month, -1, end_date)
        Dim last_csid As Integer = 0
        Dim last_created_date As Date = CDate("jan 1, 1900")
        'first delete any existing data for month
        Me.IT_cases_loadedTableAdapter.DeleteQuery(run_date)

        param2 = "select _rowid, clientschemeID, _createdBy, _createdDate  from Debtor " &
            " where _createdDate >= '" & Format(run_date, "yyyy-MM-dd") & "'" &
            " and _createddate < '" & Format(end_date, "yyyy-MM-dd") & "'" &
            " order by clientschemeID, _createdDate"
        Dim debt_ds As DataSet = get_dataset("onestep", param2)
        Dim debt_rows As Integer = no_of_rows - 1
        Dim enforce_cases As Integer = 0
        Dim enforce_batches As Integer = 0
        Dim collect_cases As Integer = 0
        Dim collect_batches As Integer = 0
        Dim manual_cases As Integer = 0
        Dim sub_tot_enforce_cases As Integer = 0
        Dim sub_tot_collect_cases As Integer = 0
        Dim sub_total_manual_cases As Integer = 0
        For debt_idx = 0 To debt_rows
            'Try
            '    ProgressBar1.Value = (debt_idx / debt_rows) * 100
            'Catch ex As Exception

            'End Try
            'Application.DoEvents()
            Dim cs_id As Integer = debt_ds.Tables(0).Rows(debt_idx).Item(1)
            param2 = "select clientID, branchID from clientScheme where _rowid = " & cs_id
            Dim cs_ds As DataSet = get_dataset("onestep", param2)
            Dim client_id As Integer = cs_ds.Tables(0).Rows(0).Item(0)
            If client_id = 1 Or client_id = 2 Or client_id = 24 Then
                Continue For
            End If
            Dim created_date As Date = debt_ds.Tables(0).Rows(debt_idx).Item(3)
            Dim branch_ID As Integer = cs_ds.Tables(0).Rows(0).Item(1)
            Dim it_batch As Boolean = False
            Dim created_by As String
            Try
                created_by = UCase(debt_ds.Tables(0).Rows(debt_idx).Item(2))
            Catch ex As Exception
                Continue For
            End Try

            If created_by = "DDICKS" Or created_by = "STURNER" Or created_by = "AMANNING" Or
                created_by = "SCOWPE" Or created_by = "MGREEN" Or created_by = "JBLUNDELL" Or
                created_by = "SATKINSON" Or created_by = "SHELLEYATKINSON" Or created_by = "TSTEER" Or
                created_by = "SVR-PROC" Then
                it_batch = True
            Else
                If created_by = "CDOBSON" And Format(created_date, "yyyy-MM-dd") >= Format(CDate("30 Jul, 2012"), "yyyy-MM-dd") Then
                    it_batch = True
                End If
            End If
            If cs_id = last_csid And Format(created_date, "yyyy-MM-dd") = Format(last_created_date, "yyyy-MM-dd") Then
                If it_batch Then
                    If branch_ID = 2 Then
                        sub_tot_collect_cases += 1
                    Else
                        sub_tot_enforce_cases += 1
                    End If
                Else
                    sub_total_manual_cases += 1
                End If
                Continue For
            End If
            last_csid = cs_id
            last_created_date = created_date
            If sub_tot_enforce_cases > 0 Then
                enforce_batches += 1
                enforce_cases += sub_tot_enforce_cases
                sub_tot_enforce_cases = 0
            End If
            If sub_tot_collect_cases > 0 Then
                collect_batches += 1
                collect_cases += sub_tot_collect_cases
                sub_tot_collect_cases = 0
            End If
            If sub_total_manual_cases > 0 Then
                manual_cases += sub_total_manual_cases
                sub_total_manual_cases = 0
            End If
            If it_batch Then
                If branch_ID = 2 Then
                    sub_tot_collect_cases += 1
                Else
                    sub_tot_enforce_cases += 1
                End If
            Else
                sub_total_manual_cases += 1
            End If
        Next
        If sub_tot_enforce_cases > 0 Then
            enforce_batches += 1
            enforce_cases += sub_tot_enforce_cases
        End If
        If sub_tot_collect_cases > 0 Then
            collect_batches += 1
            collect_cases += sub_tot_collect_cases
        End If
        If sub_total_manual_cases > 0 Then
            manual_cases += sub_total_manual_cases
        End If
        Dim batch_cum_tot, manual_cum_tot As Long
        If Month(run_date) = 2 Then
            batch_cum_tot = enforce_cases + collect_cases
            manual_cum_tot = manual_cases
        Else
            Dim it_date As Date = DateAdd(DateInterval.Month, -1, run_date)
            Me.IT_cases_loadedTableAdapter.FillBy(Me.FeesSQLDataSet.IT_cases_loaded, Format(it_date, "yyyy-MM-dd"))
            batch_cum_tot = enforce_cases + collect_cases + Me.FeesSQLDataSet.IT_cases_loaded.Rows(0).Item(6) 
            manual_cum_tot = manual_cases + Me.FeesSQLDataSet.IT_cases_loaded.Rows(0).Item(7)
        End If
        Try
            Me.IT_cases_loadedTableAdapter.InsertQuery(Format(run_date, "yyyy-MM-dd"), enforce_batches, enforce_cases, collect_batches, collect_cases, manual_cases, batch_cum_tot, manual_cum_tot)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        'MsgBox("Month saved")
        Me.Close()
    End Sub
End Class

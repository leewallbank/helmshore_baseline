﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.runbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.FeesSQLDataSet = New ITKPISummaryReport2.FeesSQLDataSet()
        Me.IT_cases_loadedTableAdapter = New ITKPISummaryReport2.FeesSQLDataSetTableAdapters.IT_cases_loadedTableAdapter()
        Me.TableAdapterManager = New ITKPISummaryReport2.FeesSQLDataSetTableAdapters.TableAdapterManager()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(91, 73)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(75, 23)
        Me.runbtn.TabIndex = 0
        Me.runbtn.Text = "Run report"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(213, 203)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'IT_cases_loadedTableAdapter
        '
        Me.IT_cases_loadedTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.IT_cases_loadedTableAdapter = Me.IT_cases_loadedTableAdapter
        Me.TableAdapterManager.UpdateOrder = ITKPISummaryReport2.FeesSQLDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 203)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(323, 266)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "IT KPI Summary report 2"
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents FeesSQLDataSet As ITKPISummaryReport2.FeesSQLDataSet
    Friend WithEvents IT_cases_loadedTableAdapter As ITKPISummaryReport2.FeesSQLDataSetTableAdapters.IT_cases_loadedTableAdapter
    Friend WithEvents TableAdapterManager As ITKPISummaryReport2.FeesSQLDataSetTableAdapters.TableAdapterManager
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar

End Class

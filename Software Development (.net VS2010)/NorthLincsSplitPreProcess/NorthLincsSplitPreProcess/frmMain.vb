﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration

Public Class frmMain
    Private InputFilePath As String, FileName As String, FileExt As String


    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try

            Dim FileDialog As New OpenFileDialog
            Dim LineNumber As Integer = 0
            Dim OutputFile As String = ""
            Dim OutputFile_wrp As String = ""
            Dim AuditLog As String, ErrorLog As String = "", InputLineArray() As String
            Dim traceFile As String = ""
            Dim TotalBalance As Decimal = 0
            Dim TotalBalance_wrp As Decimal = 0
            Dim Cases As Integer = 0
            Dim traceCases As Integer = 0
            Dim cases_wrp As Integer = 0
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            lblReadingFile.Visible = True

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            lblReadingFile.Visible = False
            ProgressBar.Maximum = UBound(FileContents)

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                Application.DoEvents()
                LineNumber += 1
                If LineNumber = 1 Then
                    OutputFile &= InputLine & vbNewLine
                    OutputFile_wrp &= InputLine & vbNewLine
                    Continue For
                End If

                If LineNumber > UBound(FileContents) Then
                    Continue For
                End If

                InputLineArray = InputLine.Split("|")
                Dim CaseBalance As Decimal
                Try
                    CaseBalance = CDec(InputLineArray(18))
                Catch ex As Exception
                    Continue For
                End Try

                'remove ZZZZ and any asterisks from file
                InputLine = Replace(InputLine, "ZZZZ", "")
                InputLine = Replace(InputLine, "*", "")
                'balance check no longer required.
                'If CaseBalance <= 180 Then
                '    OutputFile_wrp &= InputLine & vbNewLine
                '    TotalBalance_wrp += CaseBalance
                '    cases_wrp += 1
                'Else
                OutputFile &= InputLine & vbNewLine
                TotalBalance += CaseBalance
                Cases += 1
                ' End If
                'look for last known address
                Dim address As String = ""
                Try
                    address = LCase(Trim(InputLineArray(9)))
                Catch ex As Exception

                End Try
                If address = "last known address" Then
                    traceFile &= InputLineArray(0) & vbNewLine
                    traceCases += 1
                End If
            Next InputLine

            InputLineArray = FileContents(UBound(FileContents)).Split("|") ' Check case count and balance total in last row
            If Cases + cases_wrp <> InputLineArray(1) Then
                ErrorLog &= "Expecting " & InputLineArray(1) & " cases, found " & Cases + cases_wrp
            ElseIf TotalBalance + TotalBalance_wrp <> InputLineArray(2) Then
                ErrorLog &= "Expecting " & InputLineArray(2) & " balances, found " & TotalBalance + TotalBalance_wrp
            End If

            'If Cases > 0 Then
            '    WriteFile(InputFilePath & FileName & "_PreProcessed.txt", OutputFile)
            'End If

            'If cases_wrp > 0 Then
            '    WriteFile(InputFilePath & FileName & "_PreProcessed_wrp.txt", OutputFile_wrp)
            'End If

            AuditLog = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
            AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

            AuditLog &= "Number of new cases: " & Cases & vbCrLf
            AuditLog &= "Total balance of new cases: " & TotalBalance & vbCrLf
            AuditLog &= "Number of Trace CAses: " & traceCases

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If traceCases > 0 Then
                WriteFile(InputFilePath & FileName & "_Traces.txt", traceFile)
            End If
            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)

            MsgBox("Pre-processing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True
            btnViewOutputFileWRP.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
        
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewOutputFileWRP.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed_wrp.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

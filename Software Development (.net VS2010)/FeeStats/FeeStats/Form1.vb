﻿Public Class Form1
    Dim tot_fee, tot_fee_remit As Decimal
    Dim pif_fee As Decimal
    Dim cl_id, sch_id As Integer
    Dim cl_name, sch_name, file As String
    Dim mth_remit(30) As Decimal
    Dim mth_date(30) As Date
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        Dim start_month As String = Format(start_month_dtp.Value, "MMM yyyy")
        If MsgBox("Run stats for " & start_month, MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            Me.Close()
            Exit Sub
        End If
        Dim start_date, end_date As Date
        start_date = CDate("01 " & start_month)
        end_date = DateAdd("m", 1, start_date)

        Dim new_file As String = "H:\temp\fees.txt"
        Dim cl_ds As DataSet
        Dim sch_ds As DataSet

        file = "Total Fees|Total fees remitted from PIF from one payment|Client|Scheme"
        Dim parm_date As Date = start_date
        Dim mth_idx As Integer = 1
        While parm_date < Now
            mth_date(mth_idx) = parm_date
            mth_idx += 1
            file = file & "|" & Format(parm_date, "MMM-yy")
            parm_date = DateAdd("m", 1, parm_date)
        End While
        file = file & vbNewLine
        My.Computer.FileSystem.WriteAllText(new_file, file, False)

        'get all fees added in month - not collect
        param2 = "select debtorID, fee_amount, clientSchemeID, _rowid from Fee where _createdDate >= '" &
            Format(start_date, "yyyy-MM-dd") & "' and " &
            " _createdDate < '" & Format(end_date, "yyyy-MM-dd") & "' and fee_remit_col > 2" &
            " order by clientSchemeID"
        Dim fee_ds As DataSet = get_dataset("onestep", param2)
        Dim fee_rows As Integer = no_of_rows
        Dim last_csid As Integer = 0
        For idx = 0 To fee_rows - 1
            Try
                ProgressBar1.Value = (idx / fee_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            Dim csid As Integer = fee_ds.Tables(0).Rows(idx).Item(2)
            If last_csid = 0 Then
                last_csid = csid
            Else
                If csid <> last_csid Then
                    If tot_fee > 0 Then
                        param2 = "select name from Client where _rowid = " & cl_id
                        cl_ds = get_dataset("onestep", param2)
                        cl_name = cl_ds.Tables(0).Rows(0).Item(0)
                        param2 = "select name from Scheme where _rowid = " & sch_id
                        sch_ds = get_dataset("onestep", param2)
                        sch_name = sch_ds.Tables(0).Rows(0).Item(0)

                        file = tot_fee & "|" & pif_fee & "|" & cl_name & "|" & sch_name
                        For idx3 = 1 To mth_idx
                            file = file & "|" & mth_remit(idx3)
                        Next
                        file = file & vbNewLine
                        My.Computer.FileSystem.WriteAllText(new_file, file, True)
                    End If
                    tot_fee = 0
                    pif_fee = 0
                    ReDim mth_remit(35)
                End If
            End If
            last_csid = csid
            param2 = "select branchID, clientID, schemeID from clientScheme where _rowid = " & csid
            Dim csid_ds As DataSet = get_dataset("onestep", param2)
            If csid_ds.Tables(0).Rows(0).Item(0) = 2 Then
                Continue For
            End If
            cl_id = csid_ds.Tables(0).Rows(0).Item(1)
            sch_id = csid_ds.Tables(0).Rows(0).Item(2)
            Dim fee_amt As Decimal = fee_ds.Tables(0).Rows(idx).Item(1)
            tot_fee += fee_amt
            Dim debtor As Integer = fee_ds.Tables(0).Rows(idx).Item(0)
            param2 = "select status from Debtor where _rowid = " & debtor
            Dim debt_ds As DataSet = get_dataset("onestep", param2)
            If debt_ds.Tables(0).Rows(0).Item(0) = "S" Or _
                debt_ds.Tables(0).Rows(0).Item(0) = "F" Then
                param2 = "select count(*) from Payment where debtorID = " & debtor
                Dim pay_ds As DataSet = get_dataset("onestep", param2)
                If pay_ds.Tables(0).Rows(0).Item(0) = 1 Then
                    pif_fee += fee_amt
                End If
            End If
            'get remitted fee dates for start_month
            Dim fee_id As Integer = fee_ds.Tables(0).Rows(idx).Item(3)
            param2 = "select Amount, RemitedDate from FeePayments where Feeid = " & fee_id
            Dim fee_ds2 As DataSet = get_dataset("Fees", param2)
            For idx2 = 0 To no_of_rows - 1
                Dim remit_date As Date = fee_ds2.Tables(0).Rows(idx2).Item(1)
                For idx3 = 1 To mth_idx
                    If Format(remit_date, "MM") = Format(mth_date(idx3), "MM") And
                   Format(remit_date, "yyyy") = Format(mth_date(idx3), "yyyy") Then
                        mth_remit(idx3) += fee_ds2.Tables(0).Rows(idx2).Item(0)
                    End If
                Next
               
            Next
        Next
        'write out last one
        param2 = "select name from Client where _rowid = " & cl_id
        cl_ds = get_dataset("onestep", param2)
        cl_name = cl_ds.Tables(0).Rows(0).Item(0)
        param2 = "select name from Scheme where _rowid = " & sch_id
        sch_ds = get_dataset("onestep", param2)
        sch_name = sch_ds.Tables(0).Rows(0).Item(0)
        file = tot_fee & "|" & pif_fee & "|" & cl_name & "|" & sch_name
        For idx3 = 1 To mth_idx
            file = file & "|" & mth_remit(idx3)
        Next
        file = file & vbNewLine
        My.Computer.FileSystem.WriteAllText(new_file, file, True)
        MsgBox("Completed")
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        start_month_dtp.Value = CDate("01 June 2011")
    End Sub
End Class

Public Class Form1
    Dim start_date As Date
    Dim end_date As Date
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        end_date = DateAdd(DateInterval.Day, -Weekday(Now) + 1, Now)
        start_date = DateAdd(DateInterval.Day, -6, end_date)


        startDTP.Value = start_date
        EndDTP.Value = end_date
    End Sub

    Private Sub schbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        
    End Sub

    Private Sub clbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
       
        runbtn.Enabled = False
        run_RD652()


        Me.Close()
    End Sub
    Private Sub disable_btns()
        runbtn.Enabled = False
        exitbtn.Enabled = False
    End Sub

    Private Sub run_RD652()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD652report = New RD652
        Dim myArrayList1 As ArrayList = New ArrayList()

        myArrayList1.Add(startDTP.Value)
        SetCurrentValuesForParameterField1(RD652report, myArrayList1)
        end_date = DateAdd(DateInterval.Day, 1, EndDTP.Value)
        myArrayList1.Add(end_date)
        SetCurrentValuesForParameterField2(RD652report, myArrayList1)

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD652report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RD652 Lowell Payments.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RD652report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RD652report.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
   

End Class

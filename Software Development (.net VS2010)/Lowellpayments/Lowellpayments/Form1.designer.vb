<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.runbtn = New System.Windows.Forms.Button()
        Me.msglbl = New System.Windows.Forms.Label()
        Me.EndDTP = New System.Windows.Forms.DateTimePicker()
        Me.startDTP = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(361, 205)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 3
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(141, 181)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(106, 23)
        Me.runbtn.TabIndex = 1
        Me.runbtn.Text = "Run Report"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'msglbl
        '
        Me.msglbl.AutoSize = True
        Me.msglbl.Location = New System.Drawing.Point(36, 259)
        Me.msglbl.Name = "msglbl"
        Me.msglbl.Size = New System.Drawing.Size(139, 13)
        Me.msglbl.TabIndex = 4
        Me.msglbl.Text = "Running report - please wait"
        Me.msglbl.Visible = False
        '
        'EndDTP
        '
        Me.EndDTP.Location = New System.Drawing.Point(147, 91)
        Me.EndDTP.Name = "EndDTP"
        Me.EndDTP.Size = New System.Drawing.Size(133, 20)
        Me.EndDTP.TabIndex = 5
        '
        'startDTP
        '
        Me.startDTP.Location = New System.Drawing.Point(147, 34)
        Me.startDTP.Name = "startDTP"
        Me.startDTP.Size = New System.Drawing.Size(127, 20)
        Me.startDTP.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(186, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Start Date"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(186, 75)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 13)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "End Date"
        '
        'Form1
        '
        Me.AcceptButton = Me.runbtn
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(448, 303)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.startDTP)
        Me.Controls.Add(Me.EndDTP)
        Me.Controls.Add(Me.msglbl)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Run Lowell Payments"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents msglbl As System.Windows.Forms.Label
    Friend WithEvents EndDTP As System.Windows.Forms.DateTimePicker
    Friend WithEvents startDTP As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label

End Class

﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String
    Dim addName(10) As String
    Dim addAddr(10) As String

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim OutputFile As String = ""
            Dim totValue As Decimal = 0
            Dim totCases As Integer = 0
            Dim lastClientRef As String = ""
            Dim lastbalance As Decimal = 0
            Dim lastLODate As Date = Nothing
            Dim lastAddress As String = ""
            Dim InputLineArray() As String
            Dim inputLine As String
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents)
            Dim rowMax As Integer = UBound(FileContents)
            For rowIDX = 0 To rowMax
                ProgressBar.Value = rowIDX
                Application.DoEvents()
                inputLine = FileContents(rowIDX)
                InputLineArray = Split(inputLine, ",", Chr(34))
                For colIDX = 0 To 13
                    Dim colValue As String = ""
                    Try
                        colValue = InputLineArray(colIDX)
                    Catch ex As Exception
                        colValue = ""
                    End Try

                    'remove quotes
                    'Dim newColvalue As String = ""
                    'For QIDX = 1 To colValue.Length
                    '    If Mid(colValue, QIDX, 1) <> Chr(34) Then
                    '        newColvalue &= Mid(colValue, QIDX, 1)
                    '    End If
                    'Next
                    'colValue = Trim(newColvalue)

                    'write first 2 columns out to file
                    If colIDX < 2 Then
                        OutputFile &= colValue & ","
                    End If
                    If colIDX = 2 Then
                        'remove any &
                        Dim idx As Integer = InStr(colValue, "&")
                        If idx = 0 Then
                            OutputFile &= colValue & ","
                        Else
                            colValue = Trim(Microsoft.VisualBasic.Left(colValue, idx - 1)) & """"
                            OutputFile &= colValue & ","
                        End If
                    End If
                    'write rest ofcolumns out to file
                    If colIDX > 2 Then
                        OutputFile &= colValue
                        If colIDX <> 13 Then
                            OutputFile &= ","
                        End If
                    End If
                Next
                OutputFile &= vbNewLine
                totCases += 1

            Next
            btnViewInputFile.Enabled = True
            If totCases > 0 Then
                WriteFile(InputFilePath & FileName & "_preprocess.csv", OutputFile)
                btnViewOutputFile.Enabled = True
            End If


            MsgBox("Rows = " & totCases & vbNewLine)
            Me.Close()
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_preprocess.csv") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_preprocess.csv" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & "_nonWRP_4200.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_nonWRP_4200.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

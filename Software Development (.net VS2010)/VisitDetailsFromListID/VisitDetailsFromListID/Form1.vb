﻿
Public Class Form1

    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(sender As System.Object, e As System.EventArgs) Handles runbtn.Click
        'check ListID is numeric
        Dim listID As Integer
        Try
            listID = ListIDtbox.Text
        Catch ex As Exception
            MsgBox("List ID is not numeric")
            Exit Sub
        End Try

        'check debtorID is numeric
        Dim debtorID As Integer
        Try
            debtorID = debtorIDtbox.Text
        Catch ex As Exception
            MsgBox("DebtorID is not numeric")
            Exit Sub
        End Try
        ConnectDb2("DebtRecovery")
        'see if listID exists on onestep
        Dim listCount As Integer = GetSQLResults2("DebtRecovery", "SELECT count(*)  from listdetail " & _
                                                        "WHERE listID = " & listID)
        If listCount = 0 Then
            MsgBox("ListID " & listID & " does not exist on onestep")
            Exit Sub
        End If

        'check case number exists in the list
        listCount = GetSQLResults2("DebtRecovery", "SELECT count(*)  from listdetail " & _
                                                        "WHERE listID = " & listID & _
                                                        " and objectRowID = " & debtorID)

        If listCount = 0 Then
            MsgBox("DebtorID " & debtorID & " does not appear in list " & listID)
            Exit Sub
        End If

        runbtn.Enabled = False
        exitbtn.Enabled = False
        'run crystal report
        Dim RD618report = New RD618
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(listID)
        SetCurrentValuesForParameterField1(RD618report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD618report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RD618 Personal Calls Visits report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RD618report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RD618report.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If


        Me.Close()

    End Sub
End Class

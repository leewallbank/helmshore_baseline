﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization
Imports System.Net.Mail

Public Class frmMain
    Const Separator As String = "|"
    Private ConnCode As New Dictionary(Of String, Integer)
    Private CustomerType As New Dictionary(Of String, String)
    Private PersonType As New Dictionary(Of String, String)
    Private Nationality As New Dictionary(Of String, String)
    Private IDType As New Dictionary(Of String, String)
    Private PersonAddressType As New Dictionary(Of String, String)
    Private ConnID As String() = {"4372", "4375", "4376"}
    Private InputFilePath As String, FileName As String, FileExt As String
    Private OutputFiles As New ArrayList()
    Private NewCases As New ArrayList()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        ConnCode.Add("PR1R", 0)
        ConnCode.Add("PRTC1", 1)
        ConnCode.Add("PRTC2", 2)

        CustomerType.Add("COR", "Corporate")
        CustomerType.Add("IND", "Individual")
        CustomerType.Add("JSH", "Joint Account Holder")
        CustomerType.Add("RES", "Residential")
        CustomerType.Add("SME", "Small Business")

        PersonType.Add("AUT", "Authorised User")
        PersonType.Add("GUA", "Guarantor")
        PersonType.Add("JAH", "Joint Account Holder")
        PersonType.Add("PAH", "Primary Account Holder")
        PersonType.Add("SAH", "Secondary Account Holder")

        Nationality.Add("GB", "UK")
        Nationality.Add("BE", "Belgium")
        Nationality.Add("FI", "Finland")
        Nationality.Add("FR", "France")
        Nationality.Add("DE", "Germany")
        Nationality.Add("GR", "Greece")

        IDType.Add("NIF", "Spanish National ID Card")
        IDType.Add("NIE", "Resident Card")
        IDType.Add("CIF", "Company Registration")
        IDType.Add("PAS", "Passport")
        IDType.Add("NIN", "National Insurance Number")
        IDType.Add("SSN", "Social Security Number")
        IDType.Add("OTH", "Other")

        PersonAddressType.Add("BAD", "Business Address")
        PersonAddressType.Add("CAD", "Current Address")
        PersonAddressType.Add("EAD", "Employers Address")
        PersonAddressType.Add("INT", "International Address")
        PersonAddressType.Add("PAD", "Previous Address")
        PersonAddressType.Add("PAF", "Post Office Address File formatted address")
        PersonAddressType.Add("SAD", "Service Address")
        PersonAddressType.Add("UKN", "Unknown")
    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String, InputLineArray() As String, ConnKey As String, DebtNotes As String
        Dim NewDebtFile(UBound(ConnID)) As String
        Dim NewDebtSumm(UBound(ConnID), 1) As Decimal, TotalNewDebt As Decimal
        Dim LineNumber As Integer

        Try
            FileDialog.Filter = "TDX Sandwell assignment files|*.csv|All files (*.*)|*.*"
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            For Each OldFile As String In Directory.GetFiles(InputFilePath, "*" & FileName & "_*.txt")
                File.Delete(OldFile)
            Next OldFile

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents) + (UBound(NewDebtFile) + 1) + 1 ' +1 for audit log

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1

                If LineNumber = 1 Then ' validate header
                    If InputLine.Split(",")(4) <> UBound(FileContents) - 1 Then ErrorLog &= "Count total in the header (" & InputLine.Split(",")(4) & ") does not match file contents (" & (UBound(FileContents) - 1).ToString & ")." & vbCrLf
                    Continue For
                End If

                If LineNumber = UBound(FileContents) + 1 Then Continue For ' skip footer line

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                InputLineArray = InputLine.Split(",")

                ' Identify the client scheme
                ConnKey = InputLineArray(12)
                If ConnCode.ContainsKey(ConnKey) Then

                    If Not InputLineArray(27).StartsWith("0") And Not String.IsNullOrEmpty(InputLineArray(27)) Then InputLineArray(27) = "0" & InputLineArray(27)
                    If Not InputLineArray(29).StartsWith("0") And Not String.IsNullOrEmpty(InputLineArray(29)) Then InputLineArray(29) = "0" & InputLineArray(29)
                    If Not InputLineArray(31).StartsWith("0") And Not String.IsNullOrEmpty(InputLineArray(31)) Then InputLineArray(31) = "0" & InputLineArray(31)
                    If Not InputLineArray(45).StartsWith("0") And Not String.IsNullOrEmpty(InputLineArray(45)) Then InputLineArray(45) = "0" & InputLineArray(45)
                    If Not InputLineArray(47).StartsWith("0") And Not String.IsNullOrEmpty(InputLineArray(47)) Then InputLineArray(47) = "0" & InputLineArray(47)
                    If Not InputLineArray(49).StartsWith("0") And Not String.IsNullOrEmpty(InputLineArray(49)) Then InputLineArray(49) = "0" & InputLineArray(49)

                    If InputLineArray(56) = "UKN" Then InputLineArray(56) = "" ' Blank UKN / Unknown country code
                    If InputLineArray(64) = "UKN" Then InputLineArray(64) = "" ' Blank UKN / Unknown country code
                    If InputLineArray(72) = "UKN" Then InputLineArray(72) = "" ' Blank UKN / Unknown country code

                    DebtNotes = ToNote(InputLineArray(0), "AccountID", ";")(0) & _
                                ToNote(InputLineArray(5), "Outstanding balance as per Clients system", ";")(0) & _
                                ToNote(InputLineArray(7), "Client Brand", ";")(0) & _
                                ToNote(InputLineArray(8), "Debt country of origin", ";")(0) & _
                                ToNote(InputLineArray(9), "Assignment ID", ";")(0) & _
                                ToNote(InputLineArray(10), "Assignment Type Code", ";")(0) & _
                                ToNote(InputLineArray(11), "Total Amount Assigned", ";")(0) & _
                                ToNote(InputLineArray(12), "Segment Name", ";")(0)

                    If CustomerType.ContainsKey(InputLineArray(13)) Then
                        DebtNotes &= ToNote(CustomerType(InputLineArray(13)), "Small Business or Residential Customer", ";")(0)
                    Else
                        DebtNotes &= ToNote("??", "Small Business or Residential Customer", ";")(0)
                        ErrorLog &= "Unexpected PrimaryCustomerTypeCode of " & InputLineArray(13) & " found at line number " & LineNumber.ToString & vbCrLf
                    End If

                    DebtNotes &= ToNote(InputLineArray(14), "Sandwell unique identifier", ";")(0) & _
                                 ToNote(InputLineArray(15), "Person Type Code", ";")(0)

                    If PersonType.ContainsKey(InputLineArray(15)) Then
                        DebtNotes &= ToNote(PersonType(InputLineArray(15)), "Primary Person", ";")(0)
                    Else
                        DebtNotes &= ToNote("??", "Primary Person", ";")(0)
                        ErrorLog &= "Unexpected PersonTypeCode of " & InputLineArray(15) & " found at line number " & LineNumber.ToString & vbCrLf
                    End If

                    If Nationality.ContainsKey(InputLineArray(20)) Then
                        DebtNotes &= ToNote(Nationality(InputLineArray(20)), "Main Debtor Nationality", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(20), "Main Debtor Nationality", ";")(0)
                    End If

                    DebtNotes &= ToNote(InputLineArray(21), "PrimaryIDTypeCode", ";")(0)

                    If IDType.ContainsKey(InputLineArray(21)) Then
                        DebtNotes &= ToNote(IDType(InputLineArray(21)), "PrimaryIDTypeDescription", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(21), "PrimaryIDTypeDescription", ";")(0)
                    End If

                    DebtNotes &= ToNote(InputLineArray(22), "Primary ID Detail1", ";")(0) & _
                                 ToNote(InputLineArray(24), "Debtor 1 special needs", ";")(0) & _
                                 ToNote(InputLineArray(25), "Debtor 1 Bank Account Number", ";")(0) & _
                                 ToNote(InputLineArray(32), "Name 2 TDX unique identifier", ";")(0) & _
                                 ToNote(InputLineArray(33), "SecondaryPersonTypeCode", ";")(0)

                    If PersonType.ContainsKey(InputLineArray(33)) Then
                        DebtNotes &= ToNote(PersonType(InputLineArray(33)), "Name 2 type description", ";")(0)
                    ElseIf Not String.IsNullOrEmpty(InputLineArray(33)) Then
                        DebtNotes &= ToNote("??", "Name 2 type description", ";")(0)
                        ErrorLog &= "Unexpected SecondaryPersonTypeCode of " & InputLineArray(33) & " found at line number " & LineNumber.ToString & vbCrLf
                    End If

                    DebtNotes &= ToNote(InputLineArray(37), "Name 2 DOB", ";")(0)

                    If Nationality.ContainsKey(InputLineArray(38)) Then
                        DebtNotes &= ToNote(Nationality(InputLineArray(38)), "Debtor 2 Nationality", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(38), "Debtor 2 Nationality", ";")(0)
                    End If

                    DebtNotes &= ToNote(InputLineArray(39), "Debtor 2 ID type", ";")(0)

                    If IDType.ContainsKey(InputLineArray(39)) Then
                        DebtNotes &= ToNote(IDType(InputLineArray(39)), "SecondaryIDTypeDescription", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(39), "SecondaryIDTypeDescription", ";")(0)
                    End If

                    DebtNotes &= ToNote(InputLineArray(40), "Debtor 2 ID Detail 1", ";")(0) & _
                                 ToNote(InputLineArray(41), "Debtor 2 Email", ";")(0) & _
                                 ToNote(InputLineArray(42), "Debtor 2 special needs", ";")(0) & _
                                 ToNote(InputLineArray(43), "Debtor 2 Bank Account Number", ";")(0) & _
                                 ToNote(InputLineArray(44), "SecondaryPhoneTypeCode1", ";")(0) & _
                                 ToNote(InputLineArray(45), "Debtor 2 Home Tel Number", ";")(0) & _
                                 ToNote(InputLineArray(46), "SecondaryPhoneTypeCode2", ";")(0) & _
                                 ToNote(InputLineArray(47), "Debtor 2 Work Tel Number", ";")(0) & _
                                 ToNote(InputLineArray(48), "SecondaryPhoneTypeCode3", ";")(0) & _
                                 ToNote(InputLineArray(49), "SecondaryTelephoneNo3", ";")(0) & _
                                 ToNote(InputLineArray(50), "PersonAddressTypeCode1", ";")(0)

                    If PersonAddressType.ContainsKey(InputLineArray(50)) Then
                        DebtNotes &= ToNote(PersonAddressType(InputLineArray(50)), "PersonAddressType1", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(50), "PersonAddressType1", ";")(0)
                    End If

                    DebtNotes &= ToNote(InputLineArray(58), "PersonAddressTypeCode2", ";")(0)

                    If PersonAddressType.ContainsKey(InputLineArray(58)) Then
                        DebtNotes &= ToNote(PersonAddressType(InputLineArray(58)), "PersonAddressType2", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(58), "PersonAddressType2", ";")(0)
                    End If

                    DebtNotes &= ToNote(InputLineArray(66), "PersonAddressTypeCode3", ";")(0)

                    If PersonAddressType.ContainsKey(InputLineArray(66)) Then
                        DebtNotes &= ToNote(PersonAddressType(InputLineArray(66)), "PersonAddressType3", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(66), "PersonAddressType3", ";")(0)
                    End If

                    DebtNotes &= ToNote(ConcatFields({InputLineArray(67), InputLineArray(68), InputLineArray(69), InputLineArray(70), InputLineArray(71), InputLineArray(72), InputLineArray(73)}, ",") _
                                        , "Previous Address", ";")(0)

                    DebtNotes &= ToNote(InputLineArray(74), "Last Payment Amount", ";")(0) & _
                                 ToNote(InputLineArray(75), "Last Payment Date", ";")(0) & _
                                 ToNote(InputLineArray(76), "Total number of payments made by the debtor to date", ";")(0) & _
                                 ToNote(InputLineArray(77), "Value of payments made to date by debtor", ";")(0) & _
                                 ToNote(InputLineArray(78), "Method in which the debtor usually pays", ";")(0) & _
                                 ToNote(InputLineArray(79), "Default date", ";")(0) & _
                                 ToNote(InputLineArray(80), "Default amount", ";")(0) & _
                                 ToNote(InputLineArray(81), "Termination reason", ";")(0) & _
                                 ToNote(InputLineArray(82), "Amount owing to date not including fees", ";")(0) & _
                                 ToNote(InputLineArray(83), "Fees charged to date", ";")(0)

                    DebtNotes &= ToNote(InputLineArray(84), "Interest to date", ";")(0) & _
                                 ToNote(InputLineArray(85), "PrimaryProductNumber", ";")(0) & _
                                 ToNote(InputLineArray(86), "PrimaryProductDescription", ";")(0) & _
                                 ToNote(InputLineArray(87), "PrimaryProductValue", ";")(0) & _
                                 ToNote(InputLineArray(88), "PrimaryProductOpenDate", ";")(0) & _
                                 ToNote(InputLineArray(89), "PrimaryProductCloseDate", ";")(0) & _
                                 ToNote(InputLineArray(90), "Reading1", ";")(0) & _
                                 ToNote(InputLineArray(91), "Reading2", ";")(0) & _
                                 ToNote(InputLineArray(92), "Reading3", ";")(0) & _
                                 ToNote(InputLineArray(93), "AssociatedProduct1ProductNumber", ";")(0)

                    DebtNotes &= ToNote(InputLineArray(94), "AssociatedProduct1ProductDescription", ";")(0) & _
                                 ToNote(InputLineArray(95), "AssociatedProduct1ProductValue", ";")(0) & _
                                 ToNote(InputLineArray(96), "AssociatedProduct1ProductOpenDate", ";")(0) & _
                                 ToNote(InputLineArray(97), "AssociatedProduct1ProductCloseDate", ";")(0) & _
                                 ToNote(InputLineArray(98), "AssociatedProduct1Reading1", ";")(0) & _
                                 ToNote(InputLineArray(99), "AssociatedProduct1Reading2", ";")(0) & _
                                 ToNote(InputLineArray(100), "AssociatedProduct1Reading3", ";")(0) & _
                                 ToNote(InputLineArray(101), "AssociatedProduct2ProductNumber", ";")(0) & _
                                 ToNote(InputLineArray(102), "AssociatedProduct2ProductDescription", ";")(0) & _
                                 ToNote(InputLineArray(103), "AssociatedProduct2ProductValue", ";")(0)

                    DebtNotes &= ToNote(InputLineArray(104), "AssociatedProduct2OpenDate", ";")(0) & _
                                 ToNote(InputLineArray(105), "AssociatedProduct2CloseDate", ";")(0) & _
                                 ToNote(InputLineArray(106), "AssociatedProduct2Reading1", ";")(0) & _
                                 ToNote(InputLineArray(107), "AssociatedProduct2Reading2", ";")(0) & _
                                 ToNote(InputLineArray(108), "AssociatedProduct2Reading3", ";")(0) & _
                                 ToNote(InputLineArray(109), "AssociatedProduct3ProductNumber", ";")(0) & _
                                 ToNote(InputLineArray(110), "AssociatedProduct3ProductDescription", ";")(0) & _
                                 ToNote(InputLineArray(111), "AssociatedProduct3ProductValue", ";")(0) & _
                                 ToNote(InputLineArray(112), "AssociatedProduct3OpenDate", ";")(0) & _
                                 ToNote(InputLineArray(113), "AssociatedProduct3CloseDate", ";")(0)

                    DebtNotes &= ToNote(InputLineArray(114), "AssociatedProduct3Reading1", ";")(0) & _
                                 ToNote(InputLineArray(115), "AssociatedProduct3Reading2", ";")(0) & _
                                 ToNote(InputLineArray(116), "AssociatedProduct3Reading3", ";")(0) & _
                                 ToNote(InputLineArray(117), "Propensity score", ";")(0) & _
                                 ToNote(InputLineArray(118), "Litigation status", ";")(0) & _
                                 ToNote(InputLineArray(119), "Contract available", ";")(0) & _
                                 ToNote(InputLineArray(121), "Fee%", ";")(0) & _
                                 ToNote(InputLineArray(122), "Flat fee amount", ";")(0) & _
                                 ToNote(InputLineArray(123), "Vulnerable", ";")(0)

                    DebtNotes &= String.Join("", ToNote(InputLineArray(124), "Client Comments", ";")) & _
                                 ToNote(InputLineArray(125), "IGT Flag", ";")(0) & _
                                 ToNote(InputLineArray(126), "Disconnection flag", ";")(0) & _
                                 ToNote(InputLineArray(127), "Post litigation flag", ";")(0) & vbCrLf

                    InputLineArray(24) = "Sandwell MBC"

                    NewDebtFile(ConnCode(ConnKey)) = Join(InputLineArray, Separator) & _
                                    Separator

                    NewDebtFile(ConnCode(ConnKey)) &= DebtNotes

                    NewDebtSumm(ConnCode(ConnKey), 0) += 1
                    NewDebtSumm(ConnCode(ConnKey), 1) += InputLineArray(11)
                    TotalNewDebt += InputLineArray(11)
                    AppendToFile(InputFilePath & ConnID(ConnCode(ConnKey)) & "_" & FileName & "_PreProcessed.txt", NewDebtFile(ConnCode(ConnKey)))
                Else
                    ErrorLog &= "Unable to identify client scheme at line number " & LineNumber.ToString & vbCrLf
                End If

            Next InputLine

            If FileContents(0).Split(",")(3) <> TotalNewDebt Then ErrorLog &= "Debt total in the header (" & FileContents(0).Split(",")(3) & ") does not match file contents (" & TotalNewDebt.ToString & ")." & vbCrLf

            ProgressBar.Value += 1

            AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            For NewDebtFileCount As Integer = 0 To UBound(NewDebtFile)
                ProgressBar.Value += 1
                If NewDebtFile(NewDebtFileCount) <> "" Then
                    OutputFiles.Add(InputFilePath & ConnID(NewDebtFileCount) & "_" & FileName & "_PreProcessed.txt")
                    AuditLog &= "Clientscheme: " & ConnID(NewDebtFileCount) & vbCrLf
                    AuditLog &= "Number of new cases: " & NewDebtSumm(NewDebtFileCount, 0).ToString & vbCrLf
                    AuditLog &= "Total balance of new cases: " & NewDebtSumm(NewDebtFileCount, 1).ToString & vbCrLf & vbCrLf
                End If
            Next NewDebtFileCount

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)
            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", LogHeader & ExceptionLog)

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub
End Class


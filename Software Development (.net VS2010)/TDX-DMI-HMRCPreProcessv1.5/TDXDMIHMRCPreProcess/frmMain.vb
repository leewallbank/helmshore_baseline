﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization
Imports System.Net.Mail

Public Class frmMain
    Const Separator As String = "|"
    Private ConnCode As New Dictionary(Of String, Integer)
    Private CustomerType As New Dictionary(Of String, String)
    Private PersonType As New Dictionary(Of String, String)
    Private Nationality As New Dictionary(Of String, String)
    Private IDType As New Dictionary(Of String, String)
    Private PersonAddressType As New Dictionary(Of String, String)
    Private ConnID As String() = {"4620", "4621", "4622", "4658", "4705", "4706", "4777"}
    Private InputFilePath As String, FileName As String, FileExt As String
    Private OutputFiles As New ArrayList()
    Private NewCases As New ArrayList()

    Private PreProcessData As New clsPreProcessData

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        ConnCode.Add("Corporation Tax|PL1", 0)
        ConnCode.Add("Value Added Tax|PL1", 1)
        ConnCode.Add("Class 2 National Insurance Contributions|PL1", 2)
        ConnCode.Add("Pay As You Earn|PL1", 3)
        ConnCode.Add("Self-Assessment Penalties|PL1", 4)
        ConnCode.Add("Self-Assessment|PL1", 5)
        ConnCode.Add("Self-Assessment Mixed|PL1", 5)
        ConnCode.Add("Pay As You Earn|PL2", 6) ' added TS 4/Apr/2016. Request ref 73633

        CustomerType.Add("COR", "Corporate")
        CustomerType.Add("IND", "Individual")
        CustomerType.Add("JSH", "Joint Account Holder")
        CustomerType.Add("RES", "Residential")
        CustomerType.Add("SME", "Small Business")

        PersonType.Add("AUT", "Authorised User")
        PersonType.Add("GUA", "Guarantor")
        PersonType.Add("JAH", "Joint Account Holder")
        PersonType.Add("PAH", "Primary Account Holder")
        PersonType.Add("SAH", "Secondary Account Holder")

        Nationality.Add("GB", "United Kingdom")
        Nationality.Add("BE", "Belgium")
        Nationality.Add("FI", "Finland")
        Nationality.Add("FR", "France")
        Nationality.Add("DE", "Germany")
        Nationality.Add("GR", "Greece")

        IDType.Add("NIF", "Spanish National ID Card")
        IDType.Add("NIE", "Resident Card")
        IDType.Add("CIF", "Company Registration")
        IDType.Add("PAS", "Passport")
        IDType.Add("NIN", "National Insurance Number")
        IDType.Add("SSN", "Social Security Number")
        IDType.Add("OTH", "Other")

        PersonAddressType.Add("BAD", "Business Address")
        PersonAddressType.Add("CAD", "Current Address")
        PersonAddressType.Add("EAD", "Employers Address")
        PersonAddressType.Add("INT", "International Address")
        PersonAddressType.Add("PAD", "Previous Address")
        PersonAddressType.Add("PAF", "Post Office Address File formatted address")
        PersonAddressType.Add("SAD", "Service Address")
        PersonAddressType.Add("UKN", "Unknown")
    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String, InputLineArray() As String, ConnKey As String, DebtNotes As String
        Dim NewDebtFile(UBound(ConnID)) As String
        Dim NewDebtSumm(UBound(ConnID), 1) As Decimal, TotalNewDebt As Decimal
        Dim LineNumber As Integer

        Try
            FileDialog.Filter = "TDX DMI HMRC assignment files|*.csv|All files (*.*)|*.*"
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            ' Validate filename. Added TS 05/Apr/2016. Request ref 78276
            Dim FileDateStamp As Date
            If DateTime.TryParseExact(Strings.Right(FileName, 8), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, FileDateStamp) Then
                If DateTime.Today.Subtract(FileDateStamp).Days > 1 Then
                    If MessageBox.Show("File is more than one day old. Proceed?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                        Return
                    End If
                End If
            Else
                MessageBox.Show("Cannot parse filename", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If
            ' End of 78276 section.

            For Each OldFile As String In Directory.GetFiles(InputFilePath, "*" & FileName & "_*.txt")
                File.Delete(OldFile)
            Next OldFile

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents) + (UBound(NewDebtFile) + 1) + 1 ' +1 for audit log

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1

                If LineNumber = 1 Then ' validate header
                    If InputLine.Split(",")(4) <> UBound(FileContents) - 1 Then ErrorLog &= "Count total in the header (" & InputLine.Split(",")(4) & ") does not match file contents (" & (UBound(FileContents) - 1).ToString & ")." & vbCrLf
                    Continue For
                End If

                If LineNumber = UBound(FileContents) + 1 Then Continue For ' skip footer line

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                'InputLineArray = InputLine.Split(",") Commented out TS 13/Oct/2015. Request ref 62514
                InputLineArray = Split(InputLine, ",", """") ' added TS 13/Oct/2015. Request ref 62514

                If UBound(InputLineArray) <> 118 Then
                    ErrorLog &= "Unexpected line length of " & (UBound(InputLineArray) + 1).ToString & " items found at line number " & LineNumber.ToString & ". Line not loaded." & vbCrLf
                    Continue For
                End If

                ' remove double quotes and commas from addresses. TS 13/Oct/2015. Request ref 62514
                For LoopCount = 51 To 73
                    If {58, 66}.Contains(LoopCount) Then Continue For
                    ' the following could be done in one line but has been kept separate for readability
                    InputLineArray(LoopCount) = InputLineArray(LoopCount).Replace("""", "")
                    InputLineArray(LoopCount) = InputLineArray(LoopCount).Replace(",", " ")
                    InputLineArray(LoopCount) = InputLineArray(LoopCount).Replace("  ", " ")
                    InputLineArray(LoopCount) = InputLineArray(LoopCount).Trim
                Next LoopCount
                ' end of request ref 62514 section

                ' Identify the client scheme' Two fields concatentated TS 21/Mar/2016. Request ref 73633
                ConnKey = InputLineArray(7) & "|" & InputLineArray(10)
                If ConnCode.ContainsKey(ConnKey) Then

                    DebtNotes = ToNote(InputLineArray(0), "AccountID", ";")(0) & _
                                ToNote(InputLineArray(5), "Outstanding balance as per Clients system", ";")(0) & _
                                ToNote(InputLineArray(7), "Client Brand", ";")(0) & _
                                ToNote(InputLineArray(8), "Debt country of origin", ";")(0) & _
                                ToNote(InputLineArray(9), "Assignment ID", ";")(0) & _
                                ToNote(InputLineArray(10), "Assignment Type Code", ";")(0) & _
                                ToNote(InputLineArray(11), "Total Amount Assigned", ";")(0) & _
                                ToNote(InputLineArray(12), "Segment Name", ";")(0)

                    If CustomerType.ContainsKey(InputLineArray(13)) Then
                        DebtNotes &= ToNote(CustomerType(InputLineArray(13)), "Small Business or Residential Customer", ";")(0)
                    Else
                        DebtNotes &= ToNote("??", "Small Business or Residential Customer", ";")(0)
                        ErrorLog &= "Unexpected PrimaryCustomerTypeCode of " & InputLineArray(13) & " found at line number " & LineNumber.ToString & vbCrLf
                    End If

                    DebtNotes &= ToNote(InputLineArray(14), "TDX unique identifier", ";")(0) & _
                                 ToNote(InputLineArray(15), "Person Type Code", ";")(0)

                    If PersonType.ContainsKey(InputLineArray(15)) Then
                        DebtNotes &= ToNote(PersonType(InputLineArray(15)), "Primary Person", ";")(0)
                    Else
                        DebtNotes &= ToNote("??", "Primary Person", ";")(0)
                        ErrorLog &= "Unexpected PersonTypeCode of " & InputLineArray(15) & " found at line number " & LineNumber.ToString & vbCrLf
                    End If

                    If Nationality.ContainsKey(InputLineArray(20)) Then
                        DebtNotes &= ToNote(Nationality(InputLineArray(20)), "Main Debtor Nationality", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(20), "Main Debtor Nationality", ";")(0)
                    End If

                    DebtNotes &= ToNote(InputLineArray(21), "PrimaryIDTypeCode", ";")(0)

                    If IDType.ContainsKey(InputLineArray(21)) Then
                        DebtNotes &= ToNote(IDType(InputLineArray(21)), "PrimaryIDTypeDescription", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(21), "PrimaryIDTypeDescription", ";")(0)
                    End If

                    DebtNotes &= ToNote(InputLineArray(22), "Primary ID Detail 1", ";")(0) & _
                                 ToNote(InputLineArray(24), "Debtor 1 special needs", ";")(0) & _
                                 ToNote(InputLineArray(25), "Debtor 1 Bank Account Number", ";")(0) & _
                                 ToNote(InputLineArray(32), "Name 2 TDX unique identifier", ";")(0)

                    If PersonType.ContainsKey(InputLineArray(33)) Then
                        DebtNotes &= ToNote(PersonType(InputLineArray(33)), "Name 2 type description", ";")(0)
                    ElseIf Not String.IsNullOrEmpty(InputLineArray(33)) Then
                        DebtNotes &= ToNote("??", "Name 2 type description", ";")(0)
                        ErrorLog &= "Unexpected SecondaryPersonTypeCode of " & InputLineArray(33) & " found at line number " & LineNumber.ToString & vbCrLf
                    End If

                    DebtNotes &= ToNote(InputLineArray(37), "Name 2 DOB", ";")(0)

                    If Nationality.ContainsKey(InputLineArray(38)) Then
                        DebtNotes &= ToNote(Nationality(InputLineArray(38)), "Debtor 2 Nationality", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(38), "Debtor 2 Nationality", ";")(0)
                    End If

                    DebtNotes &= ToNote(InputLineArray(39), "Debtor 2 ID type", ";")(0)

                    If IDType.ContainsKey(InputLineArray(39)) Then
                        DebtNotes &= ToNote(IDType(InputLineArray(39)), "SecondaryIDTypeDescription", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(39), "SecondaryIDTypeDescription", ";")(0)
                    End If

                    DebtNotes &= ToNote(InputLineArray(40), "Debtor 2 ID Detail 1", ";")(0) & _
                                 ToNote(InputLineArray(41), "Debtor 2 Email", ";")(0) & _
                                 ToNote(InputLineArray(42), "Debtor 2 special needs", ";")(0) & _
                                 ToNote(InputLineArray(43), "Debtor 2 Bank Account Number", ";")(0) & _
                                 ToNote(InputLineArray(44), "SecondaryPhoneTypeCode1", ";")(0) & _
                                 ToNote(InputLineArray(45), "Debtor 2 Home Tel Number", ";")(0) & _
                                 ToNote(InputLineArray(46), "SecondaryPhoneTypeCode2", ";")(0) & _
                                 ToNote(InputLineArray(47), "Debtor 2 Work Tel Number", ";")(0) & _
                                 ToNote(InputLineArray(48), "SecondaryPhoneTypeCode3", ";")(0) & _
                                 ToNote(InputLineArray(49), "SecondaryTelephoneNo3", ";")(0) & _
                                 ToNote(InputLineArray(50), "PersonAddressTypeCode1", ";")(0)

                    If PersonAddressType.ContainsKey(InputLineArray(50)) Then
                        DebtNotes &= ToNote(PersonAddressType(InputLineArray(50)), "PersonAddressType1", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(50), "PersonAddressType1", ";")(0)
                    End If

                    DebtNotes &= ToNote(InputLineArray(58), "PersonAddressTypeCode2", ";")(0)

                    If PersonAddressType.ContainsKey(InputLineArray(58)) Then
                        DebtNotes &= ToNote(PersonAddressType(InputLineArray(58)), "PersonAddressType2", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(58), "PersonAddressType2", ";")(0)
                    End If

                    ' This section added TS 24/Nov/2015. Request ref 66469

                    DebtNotes &= ToNote(ConcatFields({InputLineArray(51), InputLineArray(52), InputLineArray(53), InputLineArray(54), InputLineArray(55), InputLineArray(56), InputLineArray(57)}, ",") _
                                       , "Person Address 1", ";")(0)

                    DebtNotes &= ToNote(ConcatFields({InputLineArray(59), InputLineArray(60), InputLineArray(61), InputLineArray(62), InputLineArray(63), InputLineArray(64), InputLineArray(65)}, ",") _
                                       , "Person Address 2", ";")(0)

                    If InputLineArray(58) = "CAD" Then
                        ' Move all adress details to the first section
                        InputLineArray(50) = InputLineArray(58)
                        InputLineArray(51) = InputLineArray(59)
                        InputLineArray(52) = InputLineArray(60)
                        InputLineArray(53) = InputLineArray(61)
                        InputLineArray(54) = InputLineArray(62)
                        InputLineArray(55) = InputLineArray(63)
                        InputLineArray(56) = InputLineArray(64)
                        InputLineArray(57) = InputLineArray(65)
                    End If
                    ' End of 66469 section

                    DebtNotes &= ToNote(InputLineArray(66), "PersonAddressTypeCode3", ";")(0)

                    If PersonAddressType.ContainsKey(InputLineArray(66)) Then
                        DebtNotes &= ToNote(PersonAddressType(InputLineArray(66)), "PersonAddressType3", ";")(0)
                    Else
                        DebtNotes &= ToNote(InputLineArray(66), "PersonAddressType3", ";")(0)
                    End If

                    DebtNotes &= ToNote(ConcatFields({InputLineArray(67), InputLineArray(68), InputLineArray(69), InputLineArray(70), InputLineArray(71), InputLineArray(72), InputLineArray(73)}, ",") _
                                        , "Person Address 3", ";")(0) ' Changed from previous address. TS 01/Dec/2015. Request ref 66469

                    ' This section added TS 24/Nov/2015. Request ref 66469
                    If InputLineArray(66) = "CAD" Then
                        ' Move all adress details to the first section
                        InputLineArray(50) = InputLineArray(66)
                        InputLineArray(51) = InputLineArray(67)
                        InputLineArray(52) = InputLineArray(68)
                        InputLineArray(53) = InputLineArray(69)
                        InputLineArray(54) = InputLineArray(70)
                        InputLineArray(55) = InputLineArray(71)
                        InputLineArray(56) = InputLineArray(72)
                        InputLineArray(57) = InputLineArray(73)
                    End If
                    ' End of 66469 section

                    DebtNotes &= ToNote(InputLineArray(74), "Last Payment Amount", ";")(0) & _
                                 ToNote(InputLineArray(75), "Last Payment Date", ";")(0) & _
                                 ToNote(InputLineArray(76), "Total number of payments made by the debtor to date", ";")(0) & _
                                 ToNote(InputLineArray(77), "Value of payments made to date by debtor", ";")(0) & _
                                 ToNote(InputLineArray(78), "Method in which the debtor usually pays", ";")(0) & _
                                 ToNote(InputLineArray(79), "Default date", ";")(0) & _
                                 ToNote(InputLineArray(80), "Default amount", ";")(0) & _
                                 ToNote(InputLineArray(81), "Termination reason", ";")(0) & _
                                 ToNote(InputLineArray(82), "Amount owing to date not including fees", ";")(0) & _
                                 ToNote(InputLineArray(83), "Fees charged to date", ";")(0)

                    DebtNotes &= ToNote(InputLineArray(84), "Interest to date", ";")(0) & _
                                 ToNote(InputLineArray(85), "PrimaryProductNumber", ";")(0) & _
                                 ToNote(InputLineArray(86), "PrimaryProductDescription", ";")(0) & _
                                 ToNote(InputLineArray(87), "PrimaryProductValue", ";")(0) & _
                                 ToNote(InputLineArray(88), "PrimaryProductOpenDate", ";")(0) & _
                                 ToNote(InputLineArray(89), "PrimaryProductCloseDate", ";")(0) & _
                                 ToNote(InputLineArray(90), "Product1AccountNumber", ";")(0) & _
                                 ToNote(InputLineArray(91), "Product1Description", ";")(0) & _
                                 ToNote(InputLineArray(92), "Product1Value", ";")(0) & _
                                 ToNote(InputLineArray(93), "Product1OpenDate", ";")(0)

                    DebtNotes &= ToNote(InputLineArray(94), "Product1CloseDate", ";")(0) & _
                                 ToNote(InputLineArray(95), "Product2AccountNumber", ";")(0) & _
                                 ToNote(InputLineArray(96), "Product2Description", ";")(0) & _
                                 ToNote(InputLineArray(97), "Product2AccountValue", ";")(0) & _
                                 ToNote(InputLineArray(98), "Product2OpenDate", ";")(0) & _
                                 ToNote(InputLineArray(99), "Product2CloseDate", ";")(0) & _
                                 ToNote(InputLineArray(100), "Product3AccountNumber", ";")(0) & _
                                 ToNote(InputLineArray(101), "Product3Description", ";")(0) & _
                                 ToNote(InputLineArray(102), "Product3Value", ";")(0) & _
                                 ToNote(InputLineArray(103), "Product3OpenDate", ";")(0)

                    DebtNotes &= ToNote(InputLineArray(104), "Product3CloseDate", ";")(0) & _
                                 ToNote(InputLineArray(105), "PropensityScore", ";")(0) & _
                                 ToNote(InputLineArray(106), "LitigationStatus", ";")(0) & _
                                 ToNote(InputLineArray(107), "ContractAvailable", ";")(0) & _
                                 ToNote(InputLineArray(108), "Region", ";")(0) & _
                                 ToNote(InputLineArray(109), "NumberOfDocuments", ";")(0) & _
                                 ToNote(InputLineArray(110), "OriginalPrincipleBalance", ";")(0) & _
                                 ToNote(InputLineArray(111), "ClientDebtorType", ";")(0) & _
                                 ToNote(InputLineArray(112), "AccountStatus", ";")(0) & _
                                 ToNote(InputLineArray(113), "ServiceRestrictionDate", ";")(0)

                    DebtNotes &= ToNote(InputLineArray(114), "NumberOfServices", ";")(0) & _
                                 ToNote(InputLineArray(115), "SAPAccountNumber", ";")(0) & _
                                 ToNote(InputLineArray(116), "TypeOfPortfolio", ";")(0) & _
                                 ToNote(InputLineArray(117), "SpareField1", ";")(0) & _
                                 ToNote(InputLineArray(118), "SpareField2", ";")(0) '& _
                    'ToNote(InputLineArray(119), "SpareField3", ";")(0) commented out TS 13/Oct/2015. Request ref 62514

                    NewDebtFile(ConnCode(ConnKey)) = Join(InputLineArray, Separator) & Separator

                    NewDebtFile(ConnCode(ConnKey)) &= DebtNotes & vbCrLf

                    NewDebtSumm(ConnCode(ConnKey), 0) += 1
                    NewDebtSumm(ConnCode(ConnKey), 1) += InputLineArray(11)
                    TotalNewDebt += InputLineArray(11)
                    AppendToFile(InputFilePath & ConnID(ConnCode(ConnKey)) & "_" & FileName & "_PreProcessed.txt", NewDebtFile(ConnCode(ConnKey)))
                Else
                    ErrorLog &= "Unable to identify client scheme at line number " & LineNumber.ToString & vbCrLf
                End If

            Next InputLine

            If FileContents(0).Split(",")(3) <> TotalNewDebt Then ErrorLog &= "Debt total in the header (" & FileContents(0).Split(",")(3) & ") does not match file contents (" & TotalNewDebt.ToString & ")." & vbCrLf

            ProgressBar.Value += 1

            AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            For NewDebtFileCount As Integer = 0 To UBound(NewDebtFile)
                ProgressBar.Value += 1
                If NewDebtFile(NewDebtFileCount) <> "" Then
                    OutputFiles.Add(InputFilePath & ConnID(NewDebtFileCount) & "_" & FileName & "_PreProcessed.txt")
                    AuditLog &= "Clientscheme: " & ConnID(NewDebtFileCount) & " - " & PreProcessData.GetConnIDName(ConnID(NewDebtFileCount)) & vbCrLf
                    AuditLog &= "Number of new cases: " & NewDebtSumm(NewDebtFileCount, 0).ToString & vbCrLf
                    AuditLog &= "Total balance of new cases: " & NewDebtSumm(NewDebtFileCount, 1).ToString & vbCrLf & vbCrLf
                End If
            Next NewDebtFileCount

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)
            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
                MessageBox.Show("Errors found.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", LogHeader & ExceptionLog)

            MessageBox.Show("Preprocessing complete.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub
End Class


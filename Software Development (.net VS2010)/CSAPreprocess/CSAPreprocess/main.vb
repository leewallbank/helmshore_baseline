
Imports Microsoft.Office.Interop
Imports System.IO
'Imports Microsoft.Office.Core


Public Class mainform
    Dim docWord As New Microsoft.Office.Interop.Word.Document
    Dim file_path, filename As String
    Dim format_no As Integer
    Dim comments, scheme1, scheme2, headings As String
    Dim retn_code As Integer
    Dim schemes(50) As String
    Dim scheme_idx As Integer = 0
    Dim first_format1 As Boolean = True
    Dim first_format5 As Boolean = True
    Dim second_address As Boolean
    Dim second_address_str As String
    Dim first_doc As Boolean = True
    Dim doc_name As String
    Dim oldFormatFound As Boolean = False

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        'T63145 add new format
        format_no = 2
        If format4rbtn.Checked Then
            format_no = 4
        ElseIf format3rbtn.Checked Then
            format_no = 3
        ElseIf format2017rbtn.Checked Then
            format_no = 5
        ElseIf TDXCMGLegacyrbtn.Checked Then
            format_no = 2
        ElseIf TDXCMGPrimerbtn.Checked Then
            format_no = 5
        End If
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        '.Filter = "Word files | *.doc,*.docx"
        With OpenFileDialog1
            .Title = "Open file"

            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                'filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
            Exit Sub
        End If
        exitbtn.Enabled = False
        Dim outline As String = ""
        Dim idx As Integer
        'get all doc files in same directory as first document
        filename = OpenFileDialog1.FileName
        For idx = filename.Length To 1 Step -1
            If Mid(filename, idx, 1) = "\" Then
                Exit For
            End If
        Next
        file_path = Path.GetDirectoryName(OpenFileDialog1.FileName)
        file_path &= "\"
        file_path = Microsoft.VisualBasic.Left(filename, idx)
        filename = Path.GetFileNameWithoutExtension(OpenFileDialog1.FileName)
        Dim area As String = ""
        Dim scheme3 As String = ""
        'write out headings
        If format_no = 1 Then
            headings = "Court_Case_ref_no|CSA_Ref|Court_Order_Date|Parents_Name|Parent_with_care_name|" & _
                    "child_name1|child_name2|Additional_Info|Section|section_phone|contact_name|date|" & _
                    "debtor_name|debt_addr1|debt_addr2|debt_addr3|debt_addr4|debt_pcode|" & _
                    "2nd_addr1|2nd_addr2|2nd_addr3|2nd_addr4|2nd_pcode|Sum|LO_Location|LO_Date|From_Date|To_Date|" & _
                    "Signed_By|Signed_Date|NRP_Description|TAx_credit_yes|Tax_credit_no|credit_ref_yes|" & _
                    "credit_ref_no|lr_check|benefit_yes|benefit_no|elect_reg_yes|elect_reg_no|" & _
                    "pwc_yes|pwc_no|?|?|nrp_contact_no|relationship|oth_person|other_address|" & _
                    "violent_yes|violent_no|assets|v1_make|v1_reg|v1_kept|v2_make|v2_reg|v2_kept|" & _
                    "Other_assets|equity_yes|equity_no|arrears_only|reg_maint|per_month|paid|" & _
                    "LO_Amount|deductions|To_Collect|explanation|deo_status|debt_reducing_by|" & _
                    "first_lo_on_case|lo_number_yes|lo_number|prev_lo_number|outcome|occupation|" & _
                    "working_pattern|first_referral_yes|first_referral_no|prev_outcome|prev_bailiff_ref|" & _
                    "total_arrears|Comments" & vbNewLine
        ElseIf format_no = 2 Then
            headings = "CSA_Area|Debt_type|Court_name|CSA_Ref|Parents_Name|Parents_dob|Parent_nino|Parent_with_care_name|" & _
                    "child_name1|child_name2|child_name3|child_name4|Section|section_phone|contact_name|date|" & _
                    "Sum|LO_date|From_date|To_Date|debtor_name|debt_addr1|debt_addr2|debt_addr3|debt_addr4|debt_pcode|" & _
                    "Last_contact_date|2nd_addr1|2nd_addr2|2nd_addr3|2nd_addr4|2nd_pcode|2nd_addr_comments|Employed|" & _
                    "Self_employed|claiming_benefit|emp_address|nrp_violent_yes|nrp_violent_no|lang_barrier_yes|" & _
                    "lang_barrier_no|lang_barrier_info|disability_yes|disability_no|more_info|reg_maint_assessed|paying_reg_MA_yes|" & _
                    "paying_reg_MA_no|paying_monthly|LO_amount|costs_yes|costs_no|costs_amount|deductions|bailiff_to_collect|" & _
                    "other_agreements|order_status|reducing_each_month|owed_to_pwc_yes|owed_to_pwc_no|owned_to_pwc_amount|" & _
                    "owned_to_sos_yes|owed_to_sos_no|owed_to_sos_amount|both|cred_ref_info|date_checked|other_info|" & _
                    "tax_credit_yes|tax_credit_no|credit_ref_yes|credit_ref_no|lr_check|benefit_yes|benefit_no|" & _
                    "elect_reg_yes|elect_reg_no|arrears_collected_yes|arrears_coll_no|Parent_contact_no1|Parent_contact_no2|" & _
                    "parent_contact_no3|other_residing|" & _
                    "os_loans_yes|os_loans_no|value_of_first_loan|paying_monthly|repaid_date|info_from_pwc|info_from_nrp|" & _
                    "info_from_cra|available_credit|v1_make|v1_reg|v1_kept|v2_make|v2_reg|v2_kept|parent_desc|dv_check_yes|dv_check_no|" & _
                    "photo_yes|photo_no|Comments" & vbNewLine
        ElseIf format_no = 3 Then
            scheme1 = ""
            scheme2 = "legal"
            headings = "Court name|SR No|TRN No|PP Name|DOB|NINO|RP Name|Child1|child2|child3|child4|" & _
                    "Sum|LO Number|LO Date|From Date|To Date|Debtor Name|Debt addr1|Debt addr2|Debt addr3|Debt addr4|" & _
                    "Debtor PCode|Last Contact Date|2nd addr1|2nd addr2|2nd addr3|2nd addr4|2nd addr pcode|" & _
                    "Relevance of 2nd address|Employed|Self Employed|Claiming benefit|Emp Address|" & _
                    "PPViolent Yes|PP Violent No|Lang barrier Yes|Lang barrier no|Lang barrier info|" & _
                    "PP difficulty Yes|PP difficulty No|difficulty info|" & _
                    "Reg Maint|Paying reg yes|paying reg no|Pay monthly|LO Amt|Costs yes|costs No|Costs" & _
                    "|deductions|To Collect|Other agreements|current deduction|Debt reducing|debt owed RP yes|" & _
                    "debt owed RP no|debt owed RP|SS Yes|SS NO|SS|Both|credit info|date checked|other info|" & _
                    "tax credit yes|tax cred no|cred ref yes|cred ref no|LR Check|benefit yes|benefit no|" & _
                    "ER yes|ER no|arrears yes|arrears no|PP contact1|PP contact2|PP contact3|other persons|" & _
                    "OS loans yes|OS loans No|value|monthly amt|repaid date|info from RP|infor from PP|info from CRA|" & _
                    "avail credit|V1make|V1Reg|V1Kept|V2Make|V2model|V2kept|" & _
                    "PP desc|DVS check yes|DVS check no|photo yes|photo no|Comments" & vbNewLine
            'T63145 add new format
        ElseIf format_no = 5 Then
            headings = "LOBA|Court name|TRN No|PP Name|DOB|NINO|RP Name|Child1|child2|child3|child4|" & _
                    "Sum|LO No|LO Date|From Date|To Date|Debtor Name|Debt addr1|Debt addr2|Debt addr3|Debt addr4|" & _
                    "Debtor PCode|Last Contact Date|2nd addr1|2nd addr2|2nd addr3|2nd addr4|2nd addr pcode|" & _
                    "Relevance of 2nd address|Employed|Self Employed|Claiming benefit|Emp Address|" & _
                    "PPViolent Yes|PP Violent No|Lang barrier Yes|Lang barrier no|Lang barrier info|" & _
                    "PP difficulty Yes|PP difficulty No|difficulty info|" & _
                    "Reg Maint|Paying reg yes|paying reg no|Pay monthly|LO Amt|Costs yes|costs No|Costs" & _
                    "|deductions|To Collect|Other agreements|current deduction|Debt reducing|debt owed RP yes|" & _
                    "debt owed RP no|debt owed RP|SS Yes|SS NO|SS|Both|credit info|date checked|other info|" & _
                    "tax credit yes|tax cred no|cred ref yes|cred ref no|LR Check|benefit yes|benefit no|" & _
                    "ER yes|ER no|ER Not Known|arrears yes|arrears no|PP contact1|PP contact2|PP contact3|other persons|" & _
                    "OS loans yes|OS loans No|value|monthly amt|repaid date|info from RP|infor from PP|info from CRA|" & _
                    "avail credit|V1make|V1Reg|V1Kept|V2Make|V2model|V2kept|Other assets|" & _
                    "PP desc|DVS check yes|DVS check no|photo yes|photo no|Comments" & vbNewLine
        Else
            scheme1 = ""
            scheme2 = "legal"
            headings = "company|area|LOBA|Court name|TRN No|PP Name|DOB|NINO|RP Name|Child1|child2|child3|child4|" & _
                    "section|phone|name|date|Sum|LO Date|From Date|To Date|Debtor Name|Debt addr1|Debt addr2|Debt addr3|Debt addr4|" & _
                    "Debtor PCode|Last Contact Date|2nd addr1|2nd addr2|2nd addr3|2nd addr4|2nd addr pcode|" & _
                    "Relevance of 2nd address|Employed|Self Employed|Claiming benefit|Emp Address|" & _
                    "PPViolent Yes|PP Violent No|Lang barrier Yes|Lang barrier no|Lang barrier info|" & _
                    "PP difficulty Yes|PP difficulty No|difficulty info|" & _
                    "Reg Maint|Paying reg yes|paying reg no|Pay monthly|LO Amt|Costs yes|costs No|Costs" & _
                    "|deductions|To Collect|Other agreements|current deduction|Debt reducing|debt owed RP yes|" & _
                    "debt owed RP no|debt owed RP|SS Yes|SS NO|SS|Both|credit info|date checked|other info|" & _
                    "tax credit yes|tax cred no|cred ref yes|cred ref no|LR Check|benefit yes|benefit no|" & _
                    "ER yes|ER no|arrears yes|arrears no|PP contact1|PP contact2|PP contact3|other persons|" & _
                    "OS loans yes|OS loans No|value|monthly amt|repaid date|info from RP|infor from PP|info from CRA|" & _
                    "avail credit|V1make|V1Reg|V1Kept|V2Make|V2model|V2kept|" & _
                    "PP desc|DVS check yes|DVS check no|photo yes|photo no|Comments" & vbNewLine
        End If

        Dim objWord As New Word.Application
        Dim Doc As New Word.Document
        Dim file_count As Integer
        'T69221 add .doc*
        For Each foundFile As String In My.Computer.FileSystem.GetFiles _
               (file_path, FileIO.SearchOption.SearchTopLevelOnly, "*.doc*")
            Try
                file_count += 5
                ProgressBar1.Value = file_count
            Catch ex As Exception
                file_count = 0
            End Try
            Try
                Doc = objWord.Documents.Open(foundFile)
            Catch ex As Exception
                Continue For
            End Try

            doc_name = Doc.Name.ToString
            Dim formfields_cnt As Integer = Doc.FormFields.Count
            Dim field_type, result As String
            For idx = 1 To formfields_cnt
                If format_no < 3 And idx = 1 Then
                    Continue For
                End If
                If format_no = 1 And (idx < 15 Or (idx > 42 And idx < 49) Or (idx > 50 And idx < 57)) Then
                    Continue For
                End If
                'get field type and result
                field_type = Doc.Fields(idx).Type.ToString
                Try
                    result = Doc.FormFields(idx).Result
                Catch ex As Exception
                    result = MsgBox(ex.Message)
                End Try
                If result.Length = 1 Then
                    If Asc(result) = 34 Then
                        result = ""
                    End If
                End If
                result = remove_chars(result)
                'save name and address to check against onestep later
                'Dim check_name, check_address As String
                'not now required
                'If idx = 6 Then
                '    check_name = result
                'End If
                'If idx = 23 Then
                '    check_address = result
                'End If
                'If idx >= 24 And idx <= 27 And result.Length > 0 Then
                '    check_address = check_address & " " & result
                'End If
                'If idx = 27 Then
                '    'check name against agents on onestep
                '    'check_name_on_onestep(check_name, check_address)
                'End If
                outline = outline & result & "|"
                Select Case format_no
                    Case 1
                        retn_code = process_format_1_comments(idx, result, doc_name)
                    Case 2
                        retn_code = process_format_2_comments(idx, result, doc_name)
                    Case 3
                        retn_code = process_format_3_comments(idx, result, doc_name)
                    Case 4
                        retn_code = process_format_4_comments(idx, result, doc_name)
                    Case 5
                        retn_code = process_format_5_comments(idx, result, doc_name)
                        If oldFormatFound Then
                            Exit For
                        End If
                End Select
                If retn_code <> 0 Then
                    Doc.Close()
                    Exit Sub
                End If
            Next
            outline = outline & comments
            If retn_code = 0 Then
                'see if headings are required
                If format_no = 1 Then
                    If first_format1 Then
                        first_format1 = False
                        outfile = headings & outline & vbNewLine
                        My.Computer.FileSystem.WriteAllText(file_path & "CSA_preprocess.txt", outfile, False)
                    Else
                        outfile = outline & vbNewLine
                        My.Computer.FileSystem.WriteAllText(file_path & "CSA_preprocess.txt", outfile, True)
                    End If
                ElseIf format_no = 2 Then
                    If format2_headings_required() Then
                        outfile = headings & outline & vbNewLine
                        My.Computer.FileSystem.WriteAllText(file_path & scheme1 & "_" & scheme2 & "_5234_CSA_preprocess.txt", outfile, False)
                    Else
                        outfile = outline & vbNewLine
                        My.Computer.FileSystem.WriteAllText(file_path & scheme1 & "_" & scheme2 & "_5234_CSA_preprocess.txt", outfile, True)
                    End If
                ElseIf format_no = 3 Then
                    If format3_headings_required() Then
                        outfile = headings & outline & vbNewLine
                        My.Computer.FileSystem.WriteAllText(file_path & scheme2 & "_CSA_preprocess.txt", outfile, False)
                    Else
                        outfile = outline & vbNewLine
                        My.Computer.FileSystem.WriteAllText(file_path & scheme2 & "_CSA_preprocess.txt", outfile, True)
                    End If
                ElseIf format_no = 5 Then
                    If first_format5 Then
                        first_format5 = False
                        outfile = headings & outline & vbNewLine
                        My.Computer.FileSystem.WriteAllText(file_path & "legal_CSA_5233_preprocess.txt", outfile, False)
                    Else
                        outfile = outline & vbNewLine
                        My.Computer.FileSystem.WriteAllText(file_path & "legal_CSA_5233_preprocess.txt", outfile, True)
                    End If
                Else
                        area = Doc.FormFields(2).Result
                        scheme3 = Doc.FormFields(3).Result
                        'If first_doc Then
                        'first_doc = False
                        ' outfile = headings & outline & vbNewLine

                        '  My.Computer.FileSystem.WriteAllText(file_path & area & "_" & scheme3 & "_CSA_preprocess.txt", outfile, False)
                        ' Else
                        outfile = outline & vbNewLine
                        My.Computer.FileSystem.WriteAllText(file_path & area & "_" & scheme3 & "_CSA_preprocess.txt", outfile, True)
                        ' End If

                    End If
            End If
            'End If
            outline = ""
            outfile = ""
            comments = ""
            Try
                Doc.Close()
            Catch ex As Exception

            End Try


        Next
        If retn_code = 0 And oldFormatFound = False Then
            MsgBox("CSA_preprocess file created")
        End If
        Me.Close()
    End Sub
    Private Function format2_headings_required() As Boolean
        If scheme_idx = 0 Then
            scheme_idx += 1
            schemes(scheme_idx) = scheme1 & scheme2
            Return (True)
        End If
        'see if scheme already written to!
        Dim idx As Integer
        For idx = 1 To scheme_idx
            If schemes(idx) = scheme1 & scheme2 Then
                Return (False)
            End If
        Next
        scheme_idx += 1
        schemes(scheme_idx) = scheme1 & scheme2
        Return (True)
    End Function
    Private Function format3_headings_required() As Boolean
        If scheme_idx = 0 Then
            scheme_idx += 1
            schemes(scheme_idx) = scheme1 & scheme2
            Return (True)
        End If
        'see if scheme already written to!
        Dim idx As Integer
        For idx = 1 To scheme_idx
            If schemes(idx) = scheme1 & scheme2 Then
                Return (False)
            End If
        Next
        scheme_idx += 1
        schemes(scheme_idx) = scheme1 & scheme2
        Return (True)
    End Function
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub
    Private Function process_format_1_comments(ByVal idx As Integer, ByVal result As String, ByVal doc_nane As String) As Integer


        Dim spaces As Integer

        Select Case idx
            Case 20
                If result.Length > 0 Then
                    comments = comments & "Child1 name:" & result & ";"
                    comments = comments & Space(spaces - comments.Length)
                    spaces += 250
                End If
            Case 21
                If result.Length > 0 Then
                    comments = comments & "Child2 name:" & result & ";"
                    comments = comments & Space(spaces - comments.Length)
                    spaces += 250
                End If
            Case 22
                If result.Length > 0 Then
                    comments = comments & "ChildAdditionalInfo:" & result & ";"
                    comments = comments & Space(spaces - comments.Length)
                    spaces += 250
                End If
            Case 57
                If result.Length > 0 Then
                    comments = comments & "NRPDescription:" & result & ";"
                    comments = comments & Space(spaces - comments.Length)
                    spaces += 250
                End If
            Case 58
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "TaxCredits:Yes;"
                        comments = comments & Space(spaces - comments.Length)
                        spaces += 250
                    End If
                End If
            Case 59
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "TaxCredits:No;"
                        comments = comments & Space(spaces - comments.Length)
                        spaces += 250
                    End If
                End If
            Case 60
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "CreditRef:Yes;"
                        comments = comments & Space(spaces - comments.Length)
                        spaces += 250
                    End If
                End If
            Case 61
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "CreditRef:No;"
                        comments = comments & Space(spaces - comments.Length)
                        spaces += 250
                    End If
                End If
            Case 62
                If result.Length > 0 Then
                    comments = comments & "LRCheck:" & result & ";"
                    comments = comments & Space(spaces - comments.Length)
                    spaces += 250
                End If
            Case 63
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "BenefitInPayment:Yes;"
                        comments = comments & Space(spaces - comments.Length)
                        spaces += 250
                    End If
                End If
            Case 64
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "BenefitInPayment:No;"
                        comments = comments & Space(spaces - comments.Length)
                        spaces += 250
                    End If
                End If
            Case 65
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "ElectoralRegister:Yes;"
                        comments = comments & Space(spaces - comments.Length)
                        spaces += 250
                    End If
                End If
            Case 66
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "ElectoralRegister:No;"
                        comments = comments & Space(spaces - comments.Length)
                        spaces += 250
                    End If
                End If
            Case 67
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "PWC:Yes;"
                        comments = comments & Space(spaces - comments.Length)
                        spaces += 250
                    End If
                End If
            Case 68
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "PWC:No;"
                        comments = comments & Space(spaces - comments.Length)
                        spaces += 250
                    End If
                End If
            Case 72
                If result.Length > 0 Then
                    comments = comments & "OtherPersons:" & result & ";"
                    comments = comments & Space(spaces - comments.Length)
                    spaces += 250
                End If
        End Select
        Return (0)
    End Function

    Private Function process_format_2_comments(ByVal idx As Integer, ByVal result As String, ByVal doc_name As String) As Integer

        Select Case idx
            Case 2
                second_address = False
                scheme1 = result
            Case 3
                If result <> "Legal" And result <> "LOBA" Then
                    MsgBox("Remove document with wrong format before continuing - " & doc_name)
                    Return (1)
                    Exit Function
                End If
                scheme2 = result
            Case 9
                If result.Length > 0 Then
                    comments = comments & "Parent_with_cares_name:" & result & ";"
                    space_comments(result)
                End If
            Case 10
                If result.Length > 0 Then
                    comments = comments & "Child1 name:" & result & ";"
                    space_comments(result)
                End If
            Case 11
                If result.Length > 0 Then
                    comments = comments & "Child2 name:" & result & ";"
                   space_comments(result)
                End If
            Case 12
                If result.Length > 0 Then
                    comments = comments & "Child3 name:" & result & ";"
                    space_comments(result)
                End If
            Case 13
                If result.Length > 0 Then
                    comments = comments & "Child4 name:" & result & ";"
                    space_comments(result)
                End If
            Case 28
                If result.Length > 0 Then
                    comments = comments & "LastContactDate:" & result & ";"
                    space_comments(result)
                End If
            Case 29
                If result.Length > 0 Then
                    second_address = True
                    comments = comments & "2ndAddress:" & result & " "
                End If
            Case 30
                If result.Length > 0 Then
                    If second_address Then
                        comments = comments & result & " "
                    Else
                        second_address = True
                        comments = comments & "2ndAddress:" & result & " "
                    End If
                End If
            Case 31
                If result.Length > 0 Then
                    If second_address Then
                        comments = comments & result & " "
                    Else
                        second_address = True
                        comments = comments & "2ndAddress:" & result & " "
                    End If
                End If
            Case 32
                If result.Length > 0 Then
                    If second_address Then
                        comments = comments & result & " "
                    Else
                        second_address = True
                        comments = comments & "2ndAddress:" & result & " "
                    End If
                End If
            Case 33
                If result.Length > 0 Then
                    If second_address Then
                        comments = comments & result & ";"
                    Else
                        comments = comments & "2ndAddress:" & result & ";"
                    End If
                   space_comments(result)
                ElseIf second_address Then
                    comments = comments & ";"
                    space_comments(result)
                End If
            Case 35
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "NonResidentParent:Employed;"
                        space_comments(result)
                    End If
                End If
            Case 36
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "NonResidentParent:Self-employed;"
                       space_comments(result)
                    End If
                End If
            Case 37
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "NonResidentParent:Claiming Benefit;"
                        space_comments(result)
                    End If
                End If
            Case 38
                If result.Length > 0 Then
                    comments = comments & "Emp_address:" & result & ";"
                    space_comments(result)
                End If
            Case 43
                If result.Length > 0 Then
                    comments = comments & "LangBarrierInfo:" & result & ";"
                    space_comments(result)
                End If
            Case 44
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "Disability:Yes;"
                            space_comments(result)
                        End If
                    End If
            Case 45
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "Disability:No;"
                            space_comments(result)
                        End If
                    End If
            Case 46
                    If result.Length > 0 Then
                        comments = comments & "CommunicationBarrierInfo:" & result & ";"
                        space_comments(result)
                    End If
            Case 47
                If result.Length > 0 Then
                    comments = comments & "RegularMaintenanceAssessedMonthly:" & result & ";"
                    space_comments(result)
                End If
            Case 48
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "NRPPayingRegularMaintenance:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 49
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "NRPPayingRegularMaintenance:No;"
                        space_comments(result)
                    End If
                End If
            Case 50
                If result.Length > 0 Then
                    comments = comments & "NRPPayingMonthly:" & result & ";"
                   space_comments(result)
                End If
            Case 57
                If result.Length > 0 Then
                    comments = comments & "OtherAgreements:" & result & ";"
                    space_comments(result)
                End If
            Case 60
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "DebtOwedtoParentwithCare:Yes;"
                            space_comments(result)
                        End If
                    End If
            Case 61
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "DebtOwedtoParentwithCare:No;"
                            space_comments(result)
                        End If
                    End If
            Case 62
                    If result.Length > 0 Then
                        comments = comments & "AmtDebtOwedtoParentwithCareis:" & result & ";"
                       space_comments(result)
                    End If
            Case 63
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "DebtOwedtoSecretaryofState:Yes;"
                           space_comments(result)
                        End If
                    End If
            Case 64
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "DebtOwedtoSecretaryofState:No;"
                           space_comments(result)
                        End If
                    End If
            Case 65
                    If result.Length > 0 Then
                        comments = comments & "AmtDebtOwedtoSecretaryosStateis:" & result & ";"
                        space_comments(result)
                    End If
            Case 67
                    If result.Length > 0 Then
                        comments = comments & "CreditRefInfo:" & result & ";"
                        space_comments(result)
                    End If
            Case 68
                    If result.Length > 0 Then
                        comments = comments & "CreditDetailsCheckedDate:" & result & ";"
                        space_comments(result)
                    End If
            Case 69
                    If result.Length > 0 Then
                        comments = comments & "OtherRelevantInfo:" & result & ";"
                       space_comments(result)
                    End If
            Case 70
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "TaxCredits:Yes;"
                            space_comments(result)
                        End If
                    End If
            Case 71
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "TaxCredits:No;"
                           space_comments(result)
                        End If
                    End If
            Case 72
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "CreditRef:Yes;"
                            space_comments(result)
                        End If
                    End If
            Case 73
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "CreditRef:No;"
                           space_comments(result)
                        End If
                    End If
            Case 74
                    If result.Length > 0 Then
                        comments = comments & "LRCheck:" & result & ";"
                        space_comments(result)
                    End If
            Case 75
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "BenefitinPayment:Yes;"
                            space_comments(result)
                        End If
                    End If
            Case 76
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "BenefitinPayment:No;"
                            space_comments(result)
                        End If
                    End If
            Case 77
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "ElectoralRegister:Yes;"
                            space_comments(result)
                        End If
                    End If
            Case 78
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "ElectoralRegister:No;"
                            space_comments(result)
                        End If
                    End If
            Case 79
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "ArrearstobeCollected:Yes;"
                           space_comments(result)
                        End If
                    End If
            Case 80
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "ArrearstobeCollected:No;"
                            space_comments(result)
                        End If
                    End If
            Case 84
                    If result.Length > 0 Then
                        comments = comments & "OtherResidents:" & result & ";"
                        space_comments(result)
                    End If
            Case 85
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "OutstandingLoans:Yes;"
                           space_comments(result)
                        End If
                    End If
            Case 86
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "OutstandingLoans:No;"
                           space_comments(result)
                        End If
                    End If
            Case 87
                    If result.Length > 0 Then
                        comments = comments & "ValueofFirstLoan:" & result & ";"
                       space_comments(result)
                    End If
            Case 88
                    If result.Length > 0 Then
                        comments = comments & "MonthlyAmtofFirstLoan:" & result & ";"
                        space_comments(result)
                    End If
            Case 89
                    If result.Length > 0 Then
                        comments = comments & "DateFirstLoanRepaid:" & result & ";"
                       space_comments(result)
                    End If
            Case 90
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "LoanInfoObtainedFromPWC:Yes;"
                            space_comments(result)
                        End If
                    End If
            Case 91
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "LoanInfoObtainedFromNRP:Yes;"
                            space_comments(result)
                        End If
                    End If
            Case 92
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "LoanInfoObtainedFromCRA:Yes;"
                           space_comments(result)
                        End If
                    End If
            Case 93
                    If result.Length > 0 Then
                        comments = comments & "AvailableCredit:" & result & ";"
                        space_comments(result)
                    End If
            Case 94
                    If result.Length > 0 Then
                        comments = comments & "Vehicle1Make:" & result & ";"
                         space_comments(result)
                    End If
            Case 95
                    If result.Length > 0 Then
                        comments = comments & "Vehicle1Reg:" & result & ";"
                       space_comments(result)
                    End If
            Case 96
                    If result.Length > 0 Then
                        comments = comments & "Vehicle1Kept:" & result & ";"
                        space_comments(result)
                    End If
            Case 97
                    If result.Length > 0 Then
                        comments = comments & "Vehicle2Make:" & result & ";"
                       space_comments(result)
                    End If
            Case 98
                    If result.Length > 0 Then
                        comments = comments & "Vehicle2Reg:" & result & ";"
                        space_comments(result)
                    End If
            Case 99
                    If result.Length > 0 Then
                        comments = comments & "Vehicle2Kept:" & result & ";"
                        space_comments(result)
                    End If
            Case 100
                    If result.Length > 0 Then
                        comments = comments & "NRPDescription:" & result & ";"
                        space_comments(result)
                    End If
            Case 101
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "DriverValidationCheck:Yes;"
                            space_comments(result)
                        End If
                    End If
            Case 102
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "DriverValidationCheck:No;"
                            space_comments(result)
                        End If
                    End If
            Case 103
                    If result.Length > 0 Then
                        If result = 1 Then
                            comments = comments & "PhotoObtained:Yes;"
                             space_comments(result)
                        End If
                    End If
            Case 104
                    If result.Length > 0 Then
                        If result = 1 Then
                        comments = comments & "PhotoObtained:No;"
                        space_comments(result)
                        End If
                    End If
        End Select
        Return (0)
    End Function
    Private Function process_format_3_comments(ByVal idx As Integer, ByVal result As String, ByVal doc_name As String) As Integer

        Select Case idx

            Case 7
                If result.Length > 0 Then
                    comments = comments & "Receiving Parents Name:" & result & ";"
                    space_comments(result)
                End If
            Case 8
                If result.Length > 0 Then
                    comments = comments & "Child 1 Name:" & result & ";"
                    space_comments(result)
                End If
            Case 9
                If result.Length > 0 Then
                    comments = comments & "Child 2 Name:" & result & ";"
                    space_comments(result)
                End If
            Case 10
                If result.Length > 0 Then
                    comments = comments & "Child 3 Name:" & result & ";"
                    space_comments(result)
                End If
            Case 11
                If result.Length > 0 Then
                    comments = comments & "Child 4 Name:" & result & ";"
                    space_comments(result)
                End If
            Case 17
                If result.Length > 0 Then
                    comments = comments & "Name of Debtor:" & result & ";"
                    space_comments(result)
                End If
            Case 23
                If result.Length > 0 Then
                    comments = comments & "Last successful contact with paying parent at address:" & result & ";"
                    space_comments(result)
                End If
            Case 24
                second_address_str = ""
                If result.Length > 0 Then
                    second_address_str = result
                End If
            Case 25
                If result.Length > 0 Then
                    If second_address_str <> "" Then
                        second_address_str &= ", "
                    End If
                    second_address_str &= result
                End If
            Case 26
                If result.Length > 0 Then
                    If second_address_str <> "" Then
                        second_address_str &= ", "
                    End If
                    second_address_str &= result
                End If
            Case 27
                If result.Length > 0 Then
                    If second_address_str <> "" Then
                        second_address_str &= ", "
                    End If
                    second_address_str &= result
                End If
            Case 28
                If result.Length > 0 Then
                    If second_address_str <> "" Then
                        second_address_str &= ", "
                    End If
                    second_address_str &= result
                End If
                If second_address_str <> "" Then
                    comments = comments & "2nd Address:" & second_address_str & ";"
                    space_comments(result)
                End If
            Case 29
                If result.Length > 0 Then
                    comments = comments & "Relevance of 2nd Address:" & result & ";"
                    space_comments(result)
                End If
            Case 30
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Employment status:Employed;"
                        space_comments(result)
                    End If
                End If
            Case 31
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Employment status:Self Employed;"
                        space_comments(result)
                    End If
                End If
            Case 32
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Employment status:Claiming Benefit;"
                        space_comments(result)
                    End If
                End If
            Case 34
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Parent is potentially violent:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 35
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Parent is potentially violent:No;"
                        space_comments(result)
                    End If
                End If
            Case 36
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Language barriers when communicationg with parent:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 37
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Language barriers when communicationg with parent:No;"
                        space_comments(result)
                    End If
                End If
            Case 38
                If result.Length > 0 Then
                    comments = comments & "Language barriers information:" & result & ";"
                    space_comments(result)
                End If
            Case 39
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Paying Parent has a disability that could mean they have difficulty communicating:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 40
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Paying Parent has a disability that could mean they have difficulty communicating:No;"
                        space_comments(result)
                    End If
                End If
            Case 41
                If result.Length > 0 Then
                    comments = comments & "Communication difficulties information:" & result & ";"
                    space_comments(result)
                End If
            Case 42
                If result.Length > 0 Then
                    comments = comments & "Regular maintenece is currently assessed at:" & result & ";"
                    space_comments(result)
                End If
            Case 43
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Parent paying regular maintenance:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 44
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Parent paying regular maintenance:No;"
                        space_comments(result)
                    End If
                End If
            Case 45
                If result.Length > 0 Then
                    comments = comments & "Paying regular amounts per month of:" & result & ";"
                    space_comments(result)
                End If
            Case 46
                If result.Length > 0 Then
                    comments = comments & "Liability order amount:" & result & ";"
                    space_comments(result)
                End If
            Case 47
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Costs awarded:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 48
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Costs awarded:No;"
                        space_comments(result)
                    End If
                End If
            Case 49
                If result.Length > 0 Then
                    comments = comments & "Costs amount:" & result & ";"
                    space_comments(result)
                End If
            Case 50
                If result.Length > 0 Then
                    comments = comments & "Payments/Deductions made:" & result & ";"
                    space_comments(result)
                End If
            Case 51
                If result.Length > 0 Then
                    comments = comments & "Bailiff to Collect:" & result & ";"
                    space_comments(result)
                End If
            Case 52
                If result.Length > 0 Then
                    comments = comments & "other arrangements in place:" & result & ";"
                    space_comments(result)
                End If
            Case 53
                If result.Length > 0 Then
                    comments = comments & "Current deductions from earnings order status:" & result & ";"
                    space_comments(result)
                End If
            Case 54
                If result.Length > 0 Then
                    comments = comments & "Amount deducted from earnings order:" & result & ";"
                    space_comments(result)
                End If
            Case 55
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debt owed to receiving parent:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 56
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debt owed to receiving parent:No;"
                        space_comments(result)
                    End If
                End If
            Case 57
                If result.Length > 0 Then
                    comments = comments & "Amount owed to receiving parent:" & result & ";"
                    space_comments(result)
                End If
            Case 58
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debt owed to Secretary of State:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 59
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debt owed to Secretary of State:No;"
                        space_comments(result)
                    End If
                End If
            Case 60
                If result.Length > 0 Then
                    comments = comments & "Amount owed to Secretary of State:" & result & ";"
                    space_comments(result)
                End If
            Case 61
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Amount owed to both receiving parent and Secretary of State:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 62
                If result.Length > 0 Then
                    comments = comments & "Any other relevant credit reference information:" & result & ";"
                    space_comments(result)
                End If
            Case 63
                If result.Length > 0 Then
                    comments = comments & "Date credit details checked:" & result & ";"
                    space_comments(result)
                End If
            Case 64
                If result.Length > 0 Then
                    comments = comments & "Any other relevant information you wish the bailiff to know:" & result & ";"
                    space_comments(result)
                End If
            Case 65
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from New tax Credit in payment:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 66
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from New tax Credit in payment:No;"
                        space_comments(result)
                    End If
                End If
            Case 67
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Credit Ref:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 68
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Credit Ref:No;"
                        space_comments(result)
                    End If
                End If
            Case 69
                If result.Length > 0 Then
                    comments = comments & "Debtor Identification & Address Verification from Land Registry check:" & result & ";"
                    space_comments(result)
                End If
            Case 70
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Benefit in payment:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 71
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Benefit in payment:No;"
                        space_comments(result)
                    End If
                End If
            Case 72
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Electoral register:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 73
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Electoral register:No;"
                        space_comments(result)
                    End If
                End If
            Case 74
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Receiving parent wants arrears collected:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 75
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Receiving parent wants arrears collected:No;"
                        space_comments(result)
                    End If
                End If
            Case 76
                If result.Length > 0 Then
                    comments = comments & "Paying parent contact number1:" & result & ";"
                    space_comments(result)
                End If
            Case 77
                If result.Length > 0 Then
                    comments = comments & "Paying parent contact number2:" & result & ";"
                    space_comments(result)
                End If
            Case 78
                If result.Length > 0 Then
                    comments = comments & "Paying parent contact number3:" & result & ";"
                    space_comments(result)
                End If
            Case 79
                If result.Length > 0 Then
                    comments = comments & "Details of other persons known to reside at same address and relationship with paying parent:" & result & ";"
                    space_comments(result)
                End If
            Case 80
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Paying parent has any known outstanding loans or hire purchase commitments:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 81
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Paying parent has any known outstanding loans or hire purchase commitments:No;"
                        space_comments(result)
                    End If
                End If
            Case 82
                If result.Length > 0 Then
                    comments = comments & "Value of first loan owed by paying parent:" & result & ";"
                    space_comments(result)
                End If
            Case 83
                If result.Length > 0 Then
                    comments = comments & "Amount of first loan owed by paying parent per month:" & result & ";"
                    space_comments(result)
                End If
            Case 84
                If result.Length > 0 Then
                    comments = comments & "Date first loan owed by paying parent will be paid:" & result & ";"
                    space_comments(result)
                End If
            Case 85
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Loan information received from:receiving parent;"
                        space_comments(result)
                    End If
                End If
            Case 86
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Loan information received from:paying parent;"
                        space_comments(result)
                    End If
                End If
            Case 87
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Loan information received from:credit reference agency;"
                        space_comments(result)
                    End If
                End If
            Case 88
                If result.Length > 0 Then
                    comments = comments & "Credit ref checks shows paying parent has available credit of:" & result & ";"
                    space_comments(result)
                End If
            Case 89
                If result.Length > 0 Then
                    comments = comments & "Vehicle1Make:" & result & ";"
                    space_comments(result)
                End If
            Case 90
                If result.Length > 0 Then
                    comments = comments & "Vehicle1Reg:" & result & ";"
                    space_comments(result)
                End If
            Case 91
                If result.Length > 0 Then
                    comments = comments & "Vehicle1Kept:" & result & ";"
                    space_comments(result)
                End If
            Case 92
                If result.Length > 0 Then
                    comments = comments & "Vehicle2Make:" & result & ";"
                    space_comments(result)
                End If
            Case 93
                If result.Length > 0 Then
                    comments = comments & "Vehicle2Reg:" & result & ";"
                    space_comments(result)
                End If
            Case 94
                If result.Length > 0 Then
                    comments = comments & "Vehicle2Kept:" & result & ";"
                    space_comments(result)
                End If
            Case 95
                If result.Length > 0 Then
                    comments = comments & "Paying parent description:" & result & ";"
                    space_comments(result)
                End If
            Case 96
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "DriverValidationCheck:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 97
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "DriverValidationCheck:No;"
                        space_comments(result)
                    End If
                End If
            Case 98
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "PhotoObtained:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 99
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "PhotoObtained:No;"
                        space_comments(result)
                    End If
                End If
        End Select
        Return (0)
    End Function
    Private Function process_format_4_comments(ByVal idx As Integer, ByVal result As String, ByVal doc_name As String) As Integer
        Select Case idx

            Case 9
                If result.Length > 0 Then
                    comments = comments & "Receiving Parents Name:" & result & ";"
                    space_comments(result)
                End If
            Case 10
                If result.Length > 0 Then
                    comments = comments & "Child 1 Name:" & result & ";"
                    space_comments(result)
                End If
            Case 11
                If result.Length > 0 Then
                    comments = comments & "Child 2 Name:" & result & ";"
                    space_comments(result)
                End If
            Case 12
                If result.Length > 0 Then
                    comments = comments & "Child 3 Name:" & result & ";"
                    space_comments(result)
                End If
            Case 13
                If result.Length > 0 Then
                    comments = comments & "Child 4 Name:" & result & ";"
                    space_comments(result)
                End If
            Case 22
                If result.Length > 0 Then
                    comments = comments & "Name of Debtor:" & result & ";"
                    space_comments(result)
                End If
            Case 28
                If result.Length > 0 Then
                    comments = comments & "Last successful contact with paying parent at address:" & result & ";"
                    space_comments(result)
                End If
            Case 29
                second_address_str = ""
                If result.Length > 0 Then
                    second_address_str = result
                End If
            Case 30
                If result.Length > 0 Then
                    If second_address_str <> "" Then
                        second_address_str &= ", "
                    End If
                    second_address_str &= result
                End If
            Case 31
                If result.Length > 0 Then
                    If second_address_str <> "" Then
                        second_address_str &= ", "
                    End If
                    second_address_str &= result
                End If
            Case 32
                If result.Length > 0 Then
                    If second_address_str <> "" Then
                        second_address_str &= ", "
                    End If
                    second_address_str &= result
                End If
            Case 33
                If result.Length > 0 Then
                    If second_address_str <> "" Then
                        second_address_str &= ", "
                    End If
                    second_address_str &= result
                End If
                If second_address_str <> "" Then
                    comments = comments & "2nd Address:" & second_address_str & ";"
                    space_comments(result)
                End If
            Case 34
                If result.Length > 0 Then
                    comments = comments & "Relevance of 2nd Address:" & result & ";"
                    space_comments(result)
                End If
            Case 35
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Employment status:Employed;"
                        space_comments(result)
                    End If
                End If
            Case 36
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Employment status:Self Employed;"
                        space_comments(result)
                    End If
                End If
            Case 37
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Employment status:Claiming Benefit;"
                        space_comments(result)
                    End If
                End If
            Case 39
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Parent is potentially violent:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 40
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Parent is potentially violent:No;"
                        space_comments(result)
                    End If
                End If
            Case 41
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Language barriers when communicationg with parent:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 42
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Language barriers when communicationg with parent:No;"
                        space_comments(result)
                    End If
                End If
            Case 43
                If result.Length > 0 Then
                    comments = comments & "Language barriers information:" & result & ";"
                    space_comments(result)
                End If
            Case 44
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Paying Parent has a disability that could mean they have difficulty communicating:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 45
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Paying Parent has a disability that could mean they have difficulty communicating:No;"
                        space_comments(result)
                    End If
                End If
            Case 46
                If result.Length > 0 Then
                    comments = comments & "Communication difficulties information:" & result & ";"
                    space_comments(result)
                End If
            Case 47
                If result.Length > 0 Then
                    comments = comments & "Regular maintenece is currently assessed at:" & result & ";"
                    space_comments(result)
                End If
            Case 48
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Parent paying regular maintenance:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 49
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Parent paying regular maintenance:No;"
                        space_comments(result)
                    End If
                End If
            Case 50
                If result.Length > 0 Then
                    comments = comments & "Paying regular amounts per month of:" & result & ";"
                    space_comments(result)
                End If
            Case 51
                If result.Length > 0 Then
                    comments = comments & "Liability order amount:" & result & ";"
                    space_comments(result)
                End If
            Case 52
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Costs awarded:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 53
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Costs awarded:No;"
                        space_comments(result)
                    End If
                End If
            Case 54
                If result.Length > 0 Then
                    comments = comments & "Costs amount:" & result & ";"
                    space_comments(result)
                End If
            Case 55
                If result.Length > 0 Then
                    comments = comments & "Payments/Deductions made:" & result & ";"
                    space_comments(result)
                End If
            Case 56
                If result.Length > 0 Then
                    comments = comments & "Bailiff to Collect:" & result & ";"
                    space_comments(result)
                End If
            Case 57
                If result.Length > 0 Then
                    comments = comments & "other arrangements in place:" & result & ";"
                    space_comments(result)
                End If
            Case 58
                If result.Length > 0 Then
                    comments = comments & "Current deductions from earnings order status:" & result & ";"
                    space_comments(result)
                End If
            Case 59
                If result.Length > 0 Then
                    comments = comments & "Amount deducted from earnings order:" & result & ";"
                    space_comments(result)
                End If
            Case 60
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debt owed to receiving parent:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 61
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debt owed to receiving parent:No;"
                        space_comments(result)
                    End If
                End If
            Case 62
                If result.Length > 0 Then
                    comments = comments & "Amount owed to receiving parent:" & result & ";"
                    space_comments(result)
                End If
            Case 63
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debt owed to Secretary of State:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 64
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debt owed to Secretary of State:No;"
                        space_comments(result)
                    End If
                End If
            Case 65
                If result.Length > 0 Then
                    comments = comments & "Amount owed to Secretary of State:" & result & ";"
                    space_comments(result)
                End If
            Case 66
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Amount owed to both receiving parent and Secretary of State:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 67
                If result.Length > 0 Then
                    comments = comments & "Any other relevant credit reference information:" & result & ";"
                    space_comments(result)
                End If
            Case 68
                If result.Length > 0 Then
                    comments = comments & "Date credit details checked:" & result & ";"
                    space_comments(result)
                End If
            Case 69
                If result.Length > 0 Then
                    comments = comments & "Any other relevant information you wish the bailiff to know:" & result & ";"
                    space_comments(result)
                End If
            Case 70
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from New tax Credit in payment:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 71
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from New tax Credit in payment:No;"
                        space_comments(result)
                    End If
                End If
            Case 72
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Credit Ref:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 73
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Credit Ref:No;"
                        space_comments(result)
                    End If
                End If
            Case 74
                If result.Length > 0 Then
                    comments = comments & "Debtor Identification & Address Verification from Land Registry check:" & result & ";"
                    space_comments(result)
                End If
            Case 75
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Benefit in payment:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 76
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Benefit in payment:No;"
                        space_comments(result)
                    End If
                End If
            Case 77
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Electoral register:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 78
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Electoral register:No;"
                        space_comments(result)
                    End If
                End If
            Case 79
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Receiving parent wants arrears collected:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 80
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Receiving parent wants arrears collected:No;"
                        space_comments(result)
                    End If
                End If
            Case 81
                If result.Length > 0 Then
                    comments = comments & "Paying parent contact number1:" & result & ";"
                    space_comments(result)
                End If
            Case 82
                If result.Length > 0 Then
                    comments = comments & "Paying parent contact number2:" & result & ";"
                    space_comments(result)
                End If
            Case 83
                If result.Length > 0 Then
                    comments = comments & "Paying parent contact number3:" & result & ";"
                    space_comments(result)
                End If
            Case 84
                If result.Length > 0 Then
                    comments = comments & "Details of other persons known to reside at same address and relationship with paying parent:" & result & ";"
                    space_comments(result)
                End If
            Case 85
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Paying parent has any known outstanding loans or hire purchase commitments:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 86
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Paying parent has any known outstanding loans or hire purchase commitments:No;"
                        space_comments(result)
                    End If
                End If
            Case 87
                If result.Length > 0 Then
                    comments = comments & "Value of first loan owed by paying parent:" & result & ";"
                    space_comments(result)
                End If
            Case 88
                If result.Length > 0 Then
                    comments = comments & "Amount of first loan owed by paying parent per month:" & result & ";"
                    space_comments(result)
                End If
            Case 89
                If result.Length > 0 Then
                    comments = comments & "Date first loan owed by paying parent will be paid:" & result & ";"
                    space_comments(result)
                End If
            Case 90
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Loan information received from:receiving parent;"
                        space_comments(result)
                    End If
                End If
            Case 91
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Loan information received from:paying parent;"
                        space_comments(result)
                    End If
                End If
            Case 92
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Loan information received from:credit reference agency;"
                        space_comments(result)
                    End If
                End If
            Case 93
                If result.Length > 0 Then
                    comments = comments & "Credit ref checks shows paying parent has available credit of:" & result & ";"
                    space_comments(result)
                End If
            Case 94
                If result.Length > 0 Then
                    comments = comments & "Vehicle1Make:" & result & ";"
                    space_comments(result)
                End If
            Case 95
                If result.Length > 0 Then
                    comments = comments & "Vehicle1Reg:" & result & ";"
                    space_comments(result)
                End If
            Case 96
                If result.Length > 0 Then
                    comments = comments & "Vehicle1Kept:" & result & ";"
                    space_comments(result)
                End If
            Case 97
                If result.Length > 0 Then
                    comments = comments & "Vehicle2Make:" & result & ";"
                    space_comments(result)
                End If
            Case 98
                If result.Length > 0 Then
                    comments = comments & "Vehicle2Reg:" & result & ";"
                    space_comments(result)
                End If
            Case 99
                If result.Length > 0 Then
                    comments = comments & "Vehicle2Kept:" & result & ";"
                    space_comments(result)
                End If
            Case 100
                If result.Length > 0 Then
                    comments = comments & "Paying parent description:" & result & ";"
                    space_comments(result)
                End If
            Case 101
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "DriverValidationCheck:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 102
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "DriverValidationCheck:No;"
                        space_comments(result)
                    End If
                End If
            Case 103
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "PhotoObtained:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 104
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "PhotoObtained:No;"
                        space_comments(result)
                    End If
                End If
        End Select
        Return (0)
    End Function
    Private Function process_format_5_comments(ByVal idx As Integer, ByVal result As String, ByVal doc_name As String) As Integer
        Select Case idx

            Case 7
                If result.Length > 0 Then
                    comments = comments & "Receiving Parents Name:" & result & ";"
                    space_comments(result)
                End If
            Case 8
                If result.Length > 0 Then
                    comments = comments & "Child 1 Name:" & result & ";"
                    space_comments(result)
                End If
            Case 9
                If result.Length > 0 Then
                    comments = comments & "Child 2 Name:" & result & ";"
                    space_comments(result)
                End If
            Case 10
                If result.Length > 0 Then
                    comments = comments & "Child 3 Name:" & result & ";"
                    space_comments(result)
                End If
            Case 11
                If result.Length > 0 Then
                    comments = comments & "Child 4 Name:" & result & ";"
                    space_comments(result)
                End If
            Case 17
                If result.Length > 0 Then
                    comments = comments & "Name of Debtor:" & result & ";"
                    space_comments(result)
                End If
            Case 23
                If result.Length > 0 Then
                    comments = comments & "Last successful contact with paying parent at address:" & result & ";"
                    space_comments(result)
                End If
            Case 24
                second_address_str = ""
                If result.Length > 0 Then
                    second_address_str = result
                End If
            Case 25
                If result.Length > 0 Then
                    If second_address_str <> "" Then
                        second_address_str &= ", "
                    End If
                    second_address_str &= result
                End If
            Case 26
                If result.Length > 0 Then
                    If second_address_str <> "" Then
                        second_address_str &= ", "
                    End If
                    second_address_str &= result
                End If
            Case 27
                If result.Length > 0 Then
                    If second_address_str <> "" Then
                        second_address_str &= ", "
                    End If
                    second_address_str &= result
                End If
            Case 28
                If result.Length > 0 Then
                    If second_address_str <> "" Then
                        second_address_str &= ", "
                    End If
                    second_address_str &= result
                End If
                If second_address_str <> "" Then
                    comments = comments & "2nd Address:" & second_address_str & ";"
                    space_comments(result)
                End If
            Case 29
                If result.Length > 0 Then
                    comments = comments & "Relevance of 2nd Address:" & result & ";"
                    space_comments(result)
                End If
            Case 30
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Employment status:Employed;"
                        space_comments(result)
                    End If
                End If
            Case 31
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Employment status:Self Employed;"
                        space_comments(result)
                    End If
                End If
            Case 32
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Employment status:Claiming Benefit;"
                        space_comments(result)
                    End If
                End If
            Case 33
                If result.Length > 0 Then
                    comments = comments & "Employers Address:" & result & ";"
                    space_comments(result)
                End If
            Case 34
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Parent is potentially violent:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 35
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Parent is potentially violent:No;"
                        space_comments(result)
                    End If
                End If
            Case 36
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Language barriers when communicationg with parent:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 37
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Language barriers when communicationg with parent:No;"
                        space_comments(result)
                    End If
                End If
            Case 38
                If result.Length > 0 Then
                    comments = comments & "Language barriers information:" & result & ";"
                    space_comments(result)
                End If
            Case 39
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Paying Parent has a disability that could mean they have difficulty communicating:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 40
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Paying Parent has a disability that could mean they have difficulty communicating:No;"
                        space_comments(result)
                    End If
                End If
            Case 41
                If result.Length > 0 Then
                    comments = comments & "Communication difficulties information:" & result & ";"
                    space_comments(result)
                End If
            Case 42
                If result.Length > 0 Then
                    comments = comments & "Regular maintenece is currently assessed at:" & result & ";"
                    space_comments(result)
                End If
            Case 43
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Parent paying regular maintenance:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 44
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Parent paying regular maintenance:No;"
                        space_comments(result)
                    End If
                End If
            Case 45
                If result.Length > 0 Then
                    comments = comments & "Paying regular amounts per month of:" & result & ";"
                    space_comments(result)
                End If
            Case 46
                If result.Length > 0 Then
                    comments = comments & "Liability order amount:" & result & ";"
                    space_comments(result)
                End If
            Case 47
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Costs awarded:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 48
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Costs awarded:No;"
                        space_comments(result)
                    End If
                End If
            Case 49
                If result.Length > 0 Then
                    comments = comments & "Costs amount:" & result & ";"
                    space_comments(result)
                End If
            Case 50
                If result.Length > 0 Then
                    comments = comments & "Payments/Deductions made:" & result & ";"
                    space_comments(result)
                End If
            Case 51
                If result.Length > 0 Then
                    comments = comments & "Bailiff to Collect:" & result & ";"
                    space_comments(result)
                End If
            Case 52
                If result.Length > 0 Then
                    comments = comments & "other arrangements in place:" & result & ";"
                    space_comments(result)
                End If
            Case 53
                If result.Length > 0 Then
                    comments = comments & "Current deductions from earnings order status:" & result & ";"
                    space_comments(result)
                End If
            Case 54
                If result.Length > 0 Then
                    comments = comments & "Amount deducted from earnings order:" & result & ";"
                    space_comments(result)
                End If
            Case 55
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debt owed to receiving parent:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 56
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debt owed to receiving parent:No;"
                        space_comments(result)
                    End If
                End If
            Case 57
                If result.Length > 0 Then
                    comments = comments & "Amount owed to receiving parent:" & result & ";"
                    space_comments(result)
                End If
            Case 58
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debt owed to Secretary of State:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 59
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debt owed to Secretary of State:No;"
                        space_comments(result)
                    End If
                End If
            Case 60
                If result.Length > 0 Then
                    comments = comments & "Amount owed to Secretary of State:" & result & ";"
                    space_comments(result)
                End If
            Case 61
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Amount owed to both receiving parent and Secretary of State:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 62
                If result.Length > 0 Then
                    comments = comments & "Any other relevant credit reference information:" & result & ";"
                    space_comments(result)
                End If
            Case 63
                If result.Length > 0 Then
                    comments = comments & "Date credit details checked:" & result & ";"
                    space_comments(result)
                End If
            Case 64
                If result.Length > 0 Then
                    comments = comments & "Any other relevant information you wish the bailiff to know:" & result & ";"
                    space_comments(result)
                End If
            Case 65
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from New tax Credit in payment:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 66
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from New tax Credit in payment:No;"
                        space_comments(result)
                    End If
                End If
            Case 67
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Credit Ref:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 68
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Credit Ref:No;"
                        space_comments(result)
                    End If
                End If
            Case 69
                If result.Length > 0 Then
                    comments = comments & "Debtor Identification & Address Verification from Land Registry check:" & result & ";"
                    space_comments(result)
                End If
            Case 70
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Benefit in payment:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 71
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Benefit in payment:No;"
                        space_comments(result)
                    End If
                End If
            Case 72
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Electoral register:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 73
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Electoral register:No;"
                        space_comments(result)
                    End If
                End If
            Case 74
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Debtor Identification & Address Verification from Electoral register:Not Known;"
                        space_comments(result)
                    End If
                End If
            Case 75
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Receiving parent wants arrears collected:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 76
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Receiving parent wants arrears collected:No;"
                        space_comments(result)
                    End If
                End If
                If result.Length = 0 Then
                    MsgBox("Old format found " & doc_name)
                    oldFormatFound = True
                    Exit Function
                End If
            Case 77
                If result.Length > 0 Then
                    comments = comments & "Paying parent contact number1:" & result & ";"
                    space_comments(result)
                End If
            Case 78
                If result.Length > 0 Then
                    comments = comments & "Paying parent contact number2:" & result & ";"
                    space_comments(result)
                End If
            Case 79
                If result.Length > 0 Then
                    comments = comments & "Paying parent contact number3:" & result & ";"
                    space_comments(result)
                End If
            Case 80
                If result.Length > 0 Then
                    comments = comments & "Details of other persons known to reside at same address and relationship with paying parent:" & result & ";"
                    space_comments(result)
                End If
            Case 81
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Paying parent has any known outstanding loans or hire purchase commitments:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 82
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Paying parent has any known outstanding loans or hire purchase commitments:No;"
                        space_comments(result)
                    End If
                End If
            Case 83
                If result.Length > 0 Then
                    comments = comments & "Value of first loan owed by paying parent:" & result & ";"
                    space_comments(result)
                End If
            Case 84
                If result.Length > 0 Then
                    comments = comments & "Amount of first loan owed by paying parent per month:" & result & ";"
                    space_comments(result)
                End If
            Case 85
                If result.Length > 0 Then
                    comments = comments & "Date first loan owed by paying parent will be paid:" & result & ";"
                    space_comments(result)
                End If
            Case 86
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Loan information received from:receiving parent;"
                        space_comments(result)
                    End If
                End If
            Case 87
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Loan information received from:paying parent;"
                        space_comments(result)
                    End If
                End If
            Case 88
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "Loan information received from:credit reference agency;"
                        space_comments(result)
                    End If
                End If
            Case 89
                If result.Length > 0 Then
                    comments = comments & "Credit ref checks shows paying parent has available credit of:" & result & ";"
                    space_comments(result)
                End If
            Case 90
                If result.Length > 0 Then
                    comments = comments & "Vehicle1Make:" & result & ";"
                    space_comments(result)
                End If
            Case 91
                If result.Length > 0 Then
                    comments = comments & "Vehicle1Reg:" & result & ";"
                    space_comments(result)
                End If
            Case 92
                If result.Length > 0 Then
                    comments = comments & "Vehicle1Kept:" & result & ";"
                    space_comments(result)
                End If
            Case 93
                If result.Length > 0 Then
                    comments = comments & "Vehicle2Make:" & result & ";"
                    space_comments(result)
                End If
            Case 94
                If result.Length > 0 Then
                    comments = comments & "Vehicle2Reg:" & result & ";"
                    space_comments(result)
                End If
            Case 95
                If result.Length > 0 Then
                    comments = comments & "Vehicle2Kept:" & result & ";"
                    space_comments(result)
                End If
            Case 96
                If result.Length > 0 Then
                    comments = comments & "Other Assets:" & result & ";"
                    space_comments(result)
                End If
            Case 97
                If result.Length > 0 Then
                    comments = comments & "NRPDescription:" & result & ";"
                    space_comments(result)
                End If
            Case 98
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "DriverValidationCheck:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 99
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "DriverValidationCheck:No;"
                        space_comments(result)
                    End If
                End If
            Case 100
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "PhotoObtained:Yes;"
                        space_comments(result)
                    End If
                End If
            Case 101
                If result.Length > 0 Then
                    If result = 1 Then
                        comments = comments & "PhotoObtained:No;"
                        space_comments(result)
                    End If
                End If
        End Select
        Return (0)
    End Function
    Private Sub space_comments(ByVal result As String)
        Dim spaces As Integer
        spaces = (Int((comments.Length / 250)) + 1) * 250
        comments = comments & Space(spaces - comments.Length)
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'format_no = 1  'obsolete old format
        'If MsgBox("New Format?", MsgBoxStyle.YesNo, "Select format to be used") = MsgBoxResult.Yes Then
        '    format_no = 3
        'Else
        '    format_no = 2
        'End If
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Function remove_chars(ByVal txt As String) As String
        Dim new_txt As String = ""
        Dim idx As Integer
        For idx = 1 To txt.Length
            If Mid(txt, idx, 1) <> Chr(10) And Mid(txt, idx, 1) <> Chr(13) _
            And Mid(txt, idx, 1) <> "[" Then
                new_txt = new_txt & Mid(txt, idx, 1)
            Else
                new_txt = new_txt & " "
            End If
        Next
        'take out mobile and home
        If InStr(new_txt, "ome") > 0 Or InStr(new_txt, "obile") > 0 Then
            new_txt = LCase(new_txt)
            new_txt = Replace(new_txt, "home:", "")
            new_txt = Replace(new_txt, "mobile:", "")
        End If
        Return (Trim(new_txt))
    End Function
    'Private Sub check_name_on_onestep(ByVal name As String, ByVal address As String)
    '    'break name down into parts
    '    name = Trim(name)
    '    Dim name1 As String = ""
    '    Dim name2 As String = ""
    '    Dim name3 As String = ""
    '    Dim name_build As String = ""
    '    Dim idx As Integer
    '    For idx = 1 To name.Length
    '        If Mid(name, idx, 1) = " " Then
    '            If name1 = "" Then
    '                name1 = name_build
    '                name_build = ""
    '            Else
    '                If name2 = "" Then
    '                    name2 = name_build
    '                    name_build = ""
    '                Else
    '                    name3 = name_build
    '                End If
    '            End If
    '        Else
    '            name_build = name_build & Mid(name, idx, 1)
    '        End If
    '    Next
    '    name3 = name_build
    '    If name3 = "" Then
    '        name3 = name2
    '        name2 = name1
    '    End If
    '    name3 = name_build

    'look for surname in agent on onestep
    'take apostrophes out of name
    'removed - now in separate process
    'name3 = Replace(name3, "'", "")
    'param2 = "select _rowid from Bailiff where name_sur = '" & name3 & "'" & _
    '" and status = 'O'"
    'Dim bail_dataset As DataSet = get_dataset("onestep", param2)
    'Dim bail_rows As Integer = no_of_rows - 1
    'For idx = 0 To bail_rows
    '    Dim bail_rowid As Integer = bail_dataset.Tables(0).Rows(idx).Item(0)
    '    'look for forename in agent on onestep
    '    name2 = Replace(name2, "'", "")
    '    param2 = "select _rowid from Bailiff where name_fore = '" & name2 & "'" & _
    '    " and _rowid = " & bail_rowid
    '    Dim bail2_dataset As DataSet = get_dataset("onestep", param2)
    '    Dim bail2_rows As Integer = no_of_rows - 1
    '    Dim idx2 As Integer
    '    Dim body As String = name & " on today's file from CSA may be the same as agent no " & bail_rowid & " on onestep" & vbNewLine
    '    body = body & "Address is: " & address
    '    For idx2 = 0 To bail2_rows
    '        If email("fdicks@rossendales.com", "vshang@rossendales.com", "Potential CSA case", body) <> 0 Then
    '            MsgBox("Contact IT - error message EM123")
    '            Exit Sub
    '        End If
    '    Next
    'Next

    'End Sub

    Private Sub rbtn2017_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles format2017rbtn.CheckedChanged

    End Sub
End Class

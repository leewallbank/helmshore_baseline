<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainform
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.openbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.format2rbtn = New System.Windows.Forms.RadioButton()
        Me.format3rbtn = New System.Windows.Forms.RadioButton()
        Me.format4rbtn = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.format2017rbtn = New System.Windows.Forms.RadioButton()
        Me.TDXCMGLegacyrbtn = New System.Windows.Forms.RadioButton()
        Me.TDXCMGPrimerbtn = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'openbtn
        '
        Me.openbtn.Location = New System.Drawing.Point(94, 229)
        Me.openbtn.Name = "openbtn"
        Me.openbtn.Size = New System.Drawing.Size(127, 23)
        Me.openbtn.TabIndex = 0
        Me.openbtn.Text = "Open First Document"
        Me.openbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.exitbtn.Location = New System.Drawing.Point(262, 299)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 3
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(26, 279)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 4
        '
        'format2rbtn
        '
        Me.format2rbtn.AutoSize = True
        Me.format2rbtn.Location = New System.Drawing.Point(6, 19)
        Me.format2rbtn.Name = "format2rbtn"
        Me.format2rbtn.Size = New System.Drawing.Size(73, 17)
        Me.format2rbtn.TabIndex = 5
        Me.format2rbtn.Text = "Old format"
        Me.format2rbtn.UseVisualStyleBackColor = True
        '
        'format3rbtn
        '
        Me.format3rbtn.AutoSize = True
        Me.format3rbtn.Location = New System.Drawing.Point(6, 42)
        Me.format3rbtn.Name = "format3rbtn"
        Me.format3rbtn.Size = New System.Drawing.Size(80, 17)
        Me.format3rbtn.TabIndex = 6
        Me.format3rbtn.Text = "2012 cases"
        Me.format3rbtn.UseVisualStyleBackColor = True
        '
        'format4rbtn
        '
        Me.format4rbtn.AutoSize = True
        Me.format4rbtn.Checked = True
        Me.format4rbtn.Location = New System.Drawing.Point(6, 65)
        Me.format4rbtn.Name = "format4rbtn"
        Me.format4rbtn.Size = New System.Drawing.Size(86, 17)
        Me.format4rbtn.TabIndex = 7
        Me.format4rbtn.TabStop = True
        Me.format4rbtn.Text = "NEW Format"
        Me.format4rbtn.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TDXCMGPrimerbtn)
        Me.GroupBox1.Controls.Add(Me.TDXCMGLegacyrbtn)
        Me.GroupBox1.Controls.Add(Me.format2017rbtn)
        Me.GroupBox1.Controls.Add(Me.format4rbtn)
        Me.GroupBox1.Controls.Add(Me.format2rbtn)
        Me.GroupBox1.Controls.Add(Me.format3rbtn)
        Me.GroupBox1.Location = New System.Drawing.Point(61, 35)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 171)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Choose Format"
        '
        'format2017rbtn
        '
        Me.format2017rbtn.AutoSize = True
        Me.format2017rbtn.Location = New System.Drawing.Point(6, 88)
        Me.format2017rbtn.Name = "format2017rbtn"
        Me.format2017rbtn.Size = New System.Drawing.Size(84, 17)
        Me.format2017rbtn.TabIndex = 9
        Me.format2017rbtn.Text = "2017 Format"
        Me.format2017rbtn.UseVisualStyleBackColor = True
        '
        'TDXCMGLegacyrbtn
        '
        Me.TDXCMGLegacyrbtn.AutoSize = True
        Me.TDXCMGLegacyrbtn.Location = New System.Drawing.Point(6, 126)
        Me.TDXCMGLegacyrbtn.Name = "TDXCMGLegacyrbtn"
        Me.TDXCMGLegacyrbtn.Size = New System.Drawing.Size(112, 17)
        Me.TDXCMGLegacyrbtn.TabIndex = 10
        Me.TDXCMGLegacyrbtn.Text = "TDX CMG Legacy"
        Me.TDXCMGLegacyrbtn.UseVisualStyleBackColor = True
        '
        'TDXCMGPrimerbtn
        '
        Me.TDXCMGPrimerbtn.AutoSize = True
        Me.TDXCMGPrimerbtn.Location = New System.Drawing.Point(6, 154)
        Me.TDXCMGPrimerbtn.Name = "TDXCMGPrimerbtn"
        Me.TDXCMGPrimerbtn.Size = New System.Drawing.Size(103, 17)
        Me.TDXCMGPrimerbtn.TabIndex = 11
        Me.TDXCMGPrimerbtn.Text = "TDX CMG Prime"
        Me.TDXCMGPrimerbtn.UseVisualStyleBackColor = True
        '
        'mainform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.CancelButton = Me.exitbtn
        Me.ClientSize = New System.Drawing.Size(363, 362)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.openbtn)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "mainform"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CSA Preprocess"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents openbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents format2rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents format3rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents format4rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents format2017rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents TDXCMGPrimerbtn As System.Windows.Forms.RadioButton
    Friend WithEvents TDXCMGLegacyrbtn As System.Windows.Forms.RadioButton

End Class

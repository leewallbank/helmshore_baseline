﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String
    Dim addName(10) As String
    Dim addAddr(10) As String

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim OutputFile As String = ""
            Dim totValue As Decimal = 0
            Dim totCases As Integer = 0
            Dim lastClientRef As String = ""
            Dim lastbalance As Decimal = 0
            Dim lastLODate As Date = Nothing
            Dim lastAddress As String = ""

            Dim InputLineArray() As String
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents)
            Dim rowMax As Integer = UBound(FileContents)
            Dim matchFound As Boolean = False
            Dim matchesFound As Integer = 0
            For rowIDX = 0 To rowMax
                InputLineArray = FileContents(rowIDX).Split("|")
                Dim clientRef As String = InputLineArray(44)
                Dim balance As Decimal = InputLineArray(38)
                Dim LODate As Date = InputLineArray(36)
                Dim address As String = InputLineArray(2) & " " & _
                    InputLineArray(3) & " " & _
                    InputLineArray(4) & " " & _
                    InputLineArray(5) & " " & _
                    InputLineArray(6) & " " & _
                    InputLineArray(7)
                If clientRef <> lastClientRef Or _
                    balance <> lastbalance Or _
                    LODate <> lastLODate Then
                    If matchFound Then
                        'remove carriage return from outputfile
                        Dim idx As Integer
                        Dim test As Integer = OutputFile.Length
                        For idx = OutputFile.Length To 1 Step -1
                            If Mid(OutputFile, idx, 1) <> Chr(10) And
                                Mid(OutputFile, idx, 1) <> Chr(13) Then
                                Exit For
                            End If
                        Next
                        OutputFile = Microsoft.VisualBasic.Left(OutputFile, idx)
                        test = OutputFile.Length
                        For idx = 1 To matchesFound
                            OutputFile &= addName(idx) & ";"
                        Next
                        OutputFile &= "|"
                        For idx = 1 To matchesFound
                            If addAddr(idx) <> "" Then
                                OutputFile &= ":" & addName(idx) & " address:" & addAddr(idx) & ";"
                            End If
                        Next
                        OutputFile &= vbNewLine
                    End If
                    If rowIDX <> 0 And Not matchFound Then
                        OutputFile &= FileContents(rowIDX - 1) & vbNewLine
                        totCases += 1
                        totValue += lastbalance
                    End If
                    If rowIDX = rowMax Then
                        OutputFile &= FileContents(rowIDX) & vbNewLine
                        totCases += 1
                        totValue += balance
                    End If
                    matchFound = False
                    matchesFound = 0
                Else
                    matchesFound += 1
                    If matchesFound = 1 Then
                        OutputFile &= FileContents(rowIDX - 1)
                        'remove carriage return from outputfile
                        OutputFile = Microsoft.VisualBasic.Left(OutputFile, OutputFile.Length - 2) & "|"
                    End If
                    'add 2nd name and check if fwd address is different
                    addName(matchesFound) = InputLineArray(0)
                    addAddr(matchesFound) = ""
                    'OutputFile &= InputLineArray(0) & "|"
                    If address <> lastAddress Then
                        addAddr(matchesFound) = address
                        'OutputFile &= InputLineArray(1) & " address:" & address
                    End If
                    'add name and check if fwd address is different
                    

                    'OutputFile &= ";Name" & matchesFound & ":" & InputLineArray(1)
                    'If address <> lastAddress Then
                    '    OutputFile &= ";address:" & address
                    'End If
                    totCases += 1
                    totValue += lastbalance
                    OutputFile &= vbNewLine
                    matchFound = True
                    '24.11.2014 add last addresses if last row is a match
                    If rowIDX = rowMax Then
                        'OutputFile = Microsoft.VisualBasic.Left(OutputFile, OutputFile.Length - 2) & "|"
                        'remove carriage return from outputfile
                        Dim idx As Integer
                        Dim test As Integer = OutputFile.Length
                        For idx = OutputFile.Length To 1 Step -1
                            If Mid(OutputFile, idx, 1) <> Chr(10) And
                                Mid(OutputFile, idx, 1) <> Chr(13) Then
                                Exit For
                            End If
                        Next
                        OutputFile = Microsoft.VisualBasic.Left(OutputFile, idx)
                        test = OutputFile.Length
                        For idx = 1 To matchesFound
                            OutputFile &= addName(idx) & ";"
                        Next
                        OutputFile &= "|"
                        For idx = 1 To matchesFound
                            If addAddr(idx) <> "" Then
                                OutputFile &= ":" & addName(idx) & " address:" & addAddr(idx) & ";"
                            End If
                        Next
                        OutputFile &= vbNewLine
                    End If
                End If
                lastbalance = balance
                lastClientRef = clientRef
                lastLODate = LODate
                lastAddress = address
            Next
            btnViewInputFile.Enabled = True
            If totCases > 0 Then
                WriteFile(InputFilePath & FileName & "_preprocess.txt", OutputFile)
                btnViewOutputFile.Enabled = True
            End If


            MsgBox("Cases = " & totCases & " Value " & totValue & vbNewLine)
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_preprocess.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_preprocess.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & "_nonWRP_4200.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_nonWRP_4200.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

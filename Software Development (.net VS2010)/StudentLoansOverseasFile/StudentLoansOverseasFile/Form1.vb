﻿Public Class Form1
    Dim load_date, load_date_plus1 As Date
    Dim file, file_heading, summary_file As String
    Dim filepath As String = ""
    Dim scheme_name As String
    Dim total_cases As Integer
    Dim total_balance, csid_balance As Decimal
    Dim ascii As New System.Text.ASCIIEncoding()
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        load_date = DateAdd(DateInterval.Day, -1, Now)
        load_dtp.Value = load_date
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        exitbtn.Enabled = False
        runbtn.Enabled = False
        load_dtp.Enabled = False
        load_date = load_dtp.Value
        load_date_plus1 = DateAdd(DateInterval.Day, 1, load_date)
        file_heading = "RossRef|1st LOANPACK|PAYINBOOK REFERENCE|CUSTOMER FULL NAME|DOB|CUSTOMER GENDER|Sub Building Name|Building Name|Building Number|Street|Locality|town|County|Postcode|CUSTOMER HOME PHONE|CUSTOMER MOBILE PHONE|CUSTOMER WORK PHONE|CUSTOMER EMAIL|TOTAL ARREARS|TOTAL CHARGES|Total overdue|Total balance|MONTHS DOWN|INTEREST RATE|MONTHLY PYMNT DUE AMT|REPAYMENT DUE DAY|LAST PAID DATE|MATURITY DATE|DEFERMENT START DATE|DEFERMENT END DATE|CONTACT1 NAME|CONTACT1 RELATIONSHIP|CONTACT1 SUBBUILDING NAME|CONTACT1 BUILDING NAME|CONTACT1 BUILDING NUMBER|CONTACT1 STREET|CONTACT1 LOCALITY|CONTACT1 TOWN|CONTACT1 COUNTY/COUNTRY|CONTACT1 POSTCODE|CONTACT1 HOME PHONE|CONTACT1 MOBILE PHONE|CONTACT1 WORK PHONE|CONTACT1 EMAIL|CONTACT2 NAME|CONTACT2 RELATIONSHIP|CONTACT2 SUBBUILDING NAME|CONTACT2 BUILDING NAME|CONTACT2 BUILDING NUMBER|CONTACT2 STREET|CONTACT2 LOCALITY|CONTACT2 TOWN|CONTACT2 COUNTY/COUNTRY|CONTACT2 POSTCODE|CONTACT2 HOME PHONE|CONTACT2 MOBILE PHONE|CONTACT2 WORK PHONE|CONTACT2 EMAIL" & vbNewLine
        'get all cases for OS schemes loaded on load_date

        'get client name for summary file
        param2 = "select name from Client where _rowid = " & 1572
        Dim cl_ds As DataSet = get_dataset("onestep", param2)
        Dim client_name As String = cl_ds.Tables(0).Rows(0).Item(0)

        summary_file = "Client name, Scheme name, CSID, No of cases, Total balance" & vbNewLine

        'now get all csids
        param2 = "select _rowid, schemeID from clientScheme where clientID =  1572"
        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        Dim cs_rows As Integer = no_of_rows - 1
        For idx = 0 To cs_rows
            Try
                ProgressBar1.Value = (idx / cs_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            'get scheme name
            param2 = "select name from Scheme where _rowid = " & cs_ds.Tables(0).Rows(idx).Item(1)
            Dim sch_ds As DataSet = get_dataset("onestep", param2)
            If InStr(sch_ds.Tables(0).Rows(0).Item(0), "OS") = 0 Then
                Continue For
            End If
            file = ""
            scheme_name = sch_ds.Tables(0).Rows(0).Item(0)
            'OS scheme found
            Dim cs_id As Integer = cs_ds.Tables(0).Rows(idx).Item(0)
            'now get all cases for this CSID on load date
            param2 = "select _rowid, client_ref, prevReference, name_title, name_fore, name_sur, dateOfBirth," &
                " add_phone, add_fax, empPhone, addEmail, offenceValue, offence_number, debt_balance" &
                " from Debtor where clientSchemeID = " & cs_id &
                " and status_open_closed = 'O'" &
            " and _createdDate >= '" & Format(load_date, "yyyy-MM-dd") & "'" &
            " and _createdDate < '" & Format(load_date_plus1, "yyyy-MM-dd") & "'"

            Dim debtor_ds As DataSet = get_dataset("onestep", param2)
            Dim debtor_rows As Integer = no_of_rows - 1
            For idx2 = 0 To debtor_rows
                If idx2 = 0 Then
                    file = file_heading
                End If
                Dim debtorID As Integer = debtor_ds.Tables(0).Rows(idx2).Item(0)
                file = file & debtorID & "|"
                Dim cl_ref As String = debtor_ds.Tables(0).Rows(idx2).Item(1)
                file = file & cl_ref & "|"

                'interest rate
                Dim payinbook_reference As String = ""
                'only required for RGO schemes
                If InStr(scheme_name, "RGO") = 0 Then
                    file = file & "|"
                Else
                    Try
                        payinbook_reference = debtor_ds.Tables(0).Rows(idx2).Item(12)
                    Catch ex As Exception

                    End Try
                    file = file & payinbook_reference & "|"
                End If
                

                'Dim prev_ref As String = debtor_ds.Tables(0).Rows(idx2).Item(2)
                'file = file & prev_ref & "|"


                Dim name As String = ""
                Try
                    name = debtor_ds.Tables(0).Rows(idx2).Item(3)  'title
                    name = name & " "
                Catch ex As Exception

                End Try
                Try
                    name = name & debtor_ds.Tables(0).Rows(idx2).Item(4)  'forename
                    name = name & " "
                Catch ex As Exception

                End Try
                Try
                    name = name & debtor_ds.Tables(0).Rows(idx2).Item(5)  'name
                Catch ex As Exception

                End Try
                file = file & name & "|"
                Dim dob As Date
                Try
                    dob = debtor_ds.Tables(0).Rows(idx2).Item(6)  'dob
                Catch ex As Exception
                    dob = Nothing
                End Try
                If dob = Nothing Then
                    file = file & "|"
                Else
                    file = file & Format(dob, "ddMMyyyy") & "|"
                End If

                'get gender from notes
                file = file & get_notes(debtorID, "Gender:") & "|"

                'sub building name
                file = file & get_notes(debtorID, "Sub Building Name:") & "|"


                'building name
                file = file & get_notes(debtorID, "Building Name:") & "|"

                'get building number from notes
                file = file & get_notes(debtorID, "Building Number:") & "|"

                'get street from notes
                file = file & get_notes(debtorID, "Street:") & "|"

                'locality
                file = file & get_notes(debtorID, "Locality:") & "|"

                'town
                file = file & get_notes(debtorID, "Town:") & "|"

                'county
                file = file & get_notes(debtorID, "County:") & "|"

                'postcode
                file = file & get_notes(debtorID, "Postcode:") & "|"

                Dim add_phone As String = ""
                Try
                    add_phone = debtor_ds.Tables(0).Rows(idx2).Item(7)
                Catch ex As Exception

                End Try
                file = file & add_phone & "|"

                Dim add_fax As String = ""
                Try
                    add_fax = debtor_ds.Tables(0).Rows(idx2).Item(8)
                Catch ex As Exception

                End Try
                file = file & add_fax & "|"

                Dim emp_phone As String = ""
                Try
                    emp_phone = debtor_ds.Tables(0).Rows(idx2).Item(9)
                Catch ex As Exception

                End Try
                file = file & emp_phone & "|"

                Dim add_email As String = ""
                Try
                    add_email = debtor_ds.Tables(0).Rows(idx2).Item(10)
                Catch ex As Exception

                End Try
                file = file & add_email & "|"

                'total arrears
                file = file & get_notes(debtorID, "Total Arrears:") & "|"

                'total charges
                file = file & get_notes(debtorID, "Total Charges:") & "|"

                'total overdue
                file = file & debtor_ds.Tables(0).Rows(idx2).Item(13) & "|"

                'total balance
                file = file & debtor_ds.Tables(0).Rows(idx2).Item(13) & "|"

                'months down
                file = file & get_notes(debtorID, "Months In Arrears:") & "|"

                'interest rate
                Dim interest_rate As String = ""
                Try
                    interest_rate = debtor_ds.Tables(0).Rows(idx2).Item(11)
                Catch ex As Exception

                End Try
                file = file & interest_rate & "|"

                'mthly payment due amount
                file = file & get_notes(debtorID, "Scheduled Monthly Payment Amount:") & "|"

                'repayment due day
                file = file & get_notes(debtorID, "Next Due Date For Scheduled Repayment:") & "|"

                'last paid date
                Dim test_date As Date
                Try
                    test_date = get_notes(debtorID, "Date Customer Last Made A Payment:")
                    file = file & Format(test_date, "ddMMyyyy") & "|"
                Catch ex As Exception
                    file = file & "|"
                End Try


                'maturity date
                Try
                    test_date = get_notes(debtorID, "Loan Maturity Date:")
                    file = file & Format(test_date, "ddMMyyyy") & "|"
                Catch ex As Exception
                    file = file & "|"
                End Try

                'deferment start date
                Try
                    test_date = get_notes(debtorID, "Current Deferment Start Date:")
                    file = file & Format(test_date, "ddMMyyyy") & "|"
                Catch ex As Exception
                    file = file & "|"
                End Try

                'deferment end date
                Try
                    test_date = get_notes(debtorID, "Current Deferment End Date:")
                    file = file & Format(test_date, "ddMMyyyy") & "|"
                Catch ex As Exception
                    file = file & "|"
                End Try

                'contact1 name
                file = file & get_notes(debtorID, "1st Contact Name:") & "|"

                'contact1 relationship
                file = file & get_notes(debtorID, "1st Contact Relationship to Customer:") & "|"

                'contact1 sub building name
                file = file & get_notes(debtorID, "1st Contact Address - Sub Building Name:") & "|"

                'contact1 building name
                file = file & get_notes(debtorID, "1st Contact Address - Building Name:") & "|"

                'contact1 building number
                file = file & get_notes(debtorID, "1st Contact Address - Building Number:") & "|"

                'contact1 street
                file = file & get_notes(debtorID, "1st Contact Address - Street:") & "|"

                'contact1 locality
                file = file & get_notes(debtorID, "1st Contact Address - Locality:") & "|"

                'contact1 town
                file = file & get_notes(debtorID, "1st Contact Address - Town:") & "|"

                'contact1 county
                file = file & get_notes(debtorID, "1st Contact Address - County:") & "|"

                'contact1 postcode
                file = file & get_notes(debtorID, "1st Contact Address - Postcode:") & "|"

                'contact1 home
                file = file & get_notes(debtorID, "1st Contact Home Phone:") & "|"

                'contact1 mobile
                file = file & get_notes(debtorID, "1st Contact Mobile Phone:") & "|"

                'contact1 work
                file = file & get_notes(debtorID, "1st Contact Work Phone:") & "|"

                'contact1 email
                file = file & get_notes(debtorID, "1st Contact Email:") & "|"

                'contact2 name
                file = file & get_notes(debtorID, "2nd Contact Name:") & "|"

                'contact2 relationship
                file = file & get_notes(debtorID, "2nd Contact Relationship to Customer:") & "|"

                'contact2 sub building name
                file = file & get_notes(debtorID, "2nd Contact Address - Sub Building Name:") & "|"

                'contact2 building name
                file = file & get_notes(debtorID, "2nd Contact Address - Building Name:") & "|"

                'contact2 building number
                file = file & get_notes(debtorID, "2nd Contact Address - Building Number:") & "|"

                'contact2 street
                file = file & get_notes(debtorID, "2nd Contact Address - Street:") & "|"

                'contact2 locality
                file = file & get_notes(debtorID, "2nd Contact Address - Locality:") & "|"

                'contact2 town
                file = file & get_notes(debtorID, "2nd Contact Address - Town:") & "|"

                'contact2 county
                file = file & get_notes(debtorID, "2nd Contact Address - County:") & "|"

                'contact2 postcode
                file = file & get_notes(debtorID, "2nd Contact Address - Postcode:") & "|"

                'contact2 home
                file = file & get_notes(debtorID, "2nd Contact Home Phone:") & "|"

                'contact2 mobile
                file = file & get_notes(debtorID, "2nd Contact Mobile Phone:") & "|"

                'contact2 work
                file = file & get_notes(debtorID, "2nd Contact Work Phone:") & "|"

                'contact2 email
                file = file & get_notes(debtorID, "2nd Contact Email:")

                file = file & vbNewLine
                csid_balance += debtor_ds.Tables(0).Rows(idx2).Item(13)
            Next
            'write out file
            If Len(file) = 0 Then
                Continue For
            End If
            summary_file = summary_file & client_name & "," & scheme_name & "," & cs_id & "," & debtor_rows + 1 & "," & csid_balance & vbNewLine
            total_cases = total_cases + debtor_rows + 1
            total_balance += csid_balance
            csid_balance = 0
            If filepath = "" Then
                With SaveFileDialog1
                    .Title = "Save file"
                    .Filter = "TXT files |*.txt"
                    .DefaultExt = ".txt"
                    .OverwritePrompt = True
                    .FileName = scheme_name & "_" & cs_id & ".txt"
                End With
                If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                    My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False, ascii)
                End If
                filepath = SaveFileDialog1.FileName
                Dim idx3 As Integer
                For idx3 = filepath.Length To 1 Step -1
                    If Mid(filepath, idx3, 1) = "\" Then
                        Exit For
                    End If
                Next
                filepath = Microsoft.VisualBasic.Left(filepath, idx3)
            Else
                SaveFileDialog1.FileName = filepath & scheme_name & "_" & cs_id & ".txt"
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file, False, ascii)
            End If
        Next
        If total_cases = 0 Then
            MsgBox("No Open cases loaded on " & Format(load_date, "dd/MM/yyyy"))
        Else
            summary_file = summary_file & "Total,,," & total_cases & "," & total_balance & vbNewLine
            SaveFileDialog1.FileName = filepath & "Summary - Rossendales-" & Format(Now, "yyyyMMdd") & ".csv"
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, summary_file, False, ascii)
            MsgBox("reports saved")
        End If
        
        Me.Close()
    End Sub
    Private Function get_notes(ByVal debtor As String, ByVal tag As String) As String
        Dim note_text As String = ""
        param2 = "select text from Note where instr(text,'" & tag & "')>0" &
            " and debtoriD = " & debtor &
            " order by _createdDate desc"
        Dim note_ds As DataSet = get_dataset("onestep", param2)
        For idx = no_of_rows - 1 To 0 Step -1
            note_text = note_ds.Tables(0).Rows(idx).Item(0)
            Dim start_idx As Integer = InStr(note_text, tag)
            If start_idx > 1 Then
                note_text = ""
                Continue For
            End If
            start_idx = InStr(note_text, ":")
            Dim end_idx As Integer = InStr(note_text, ";")
            If end_idx > start_idx Then
                note_text = Mid(note_text, start_idx + 1, end_idx - start_idx - 1)
            End If
            Exit For
        Next
       

        Return (Trim(note_text))
    End Function
End Class

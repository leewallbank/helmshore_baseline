﻿Imports System.Configuration
Imports System.IO
Imports CommonLibrary

Public Class frmMain
    Private Returns As New ReturnsData

    Private SourceRow As Integer, SourceCol As Integer

    Private Const DCAID As String = "7"
    Private Const LineTerminator As String = vbLf
    Private Const Separator As String = "|" ' the same separator is for input and output files

    Private Sub cmdCreateReturns_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCreateReturns.Click

        Try
            Me.Cursor = Cursors.WaitCursor

            Dim RequestedDebtorIDs As List(Of String) = New List(Of String), OutputDebtorIDs As List(Of String) = New List(Of String)
            Dim FilePath As String

            For Each DebtorIDLine As String In TextBox1.Lines
                DebtorIDLine = DebtorIDLine.Replace("ROSS", "") ' HMRC reefer to our case reference with this prefix.
                If IsNumeric(DebtorIDLine) Then RequestedDebtorIDs.Add(DebtorIDLine)
            Next DebtorIDLine

            If RequestedDebtorIDs.Count > 0 Then Returns.GetDebtors(String.Join(",", RequestedDebtorIDs.ToArray))

            If Returns.Debtors.Count = 0 Then ' this needs to be a separate check to the one above as the list may contain DebtorIDs that are not valid
                MessageBox.Show("No cases found.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Return
            End If

            Dim FolderBrowserDialog As New FolderBrowserDialog

            If FolderBrowserDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                FilePath = FolderBrowserDialog.SelectedPath & "\"
            Else
                Return
            End If

            ' Now start creating the file
            Dim UpdateFileName As String = FilePath & "ReturnFiles\" & "DCA.DEBT_UPDATE.D." & DCAID.PadLeft(5, "0") & "." & DateTime.Today.ToString("yyyyMMdd") & ".dat"

            If File.Exists(UpdateFileName) Then File.Delete(UpdateFileName)

            Dim UpdateLine(26) As String

            For Each Debtor As DataRowView In Returns.Debtors

                OutputDebtorIDs.Add(Debtor.Item("DebtorID"))

                ReDim UpdateLine(26)

                UpdateLine(0) = DCAID
                UpdateLine(1) = "D"
                UpdateLine(2) = "ROSS" & Debtor.Item("DebtorID")
                UpdateLine(3) = Debtor.Item("client_ref")
                UpdateLine(4) = Debtor.Item("TrancheID")
                UpdateLine(19) = CDate(Debtor.Item("QueryDate")).ToString("yyyyMMdd")
                UpdateLine(20) = Debtor.Item("QueryCode")

                Select Case Debtor.Item("QueryCode")
                    Case 1 To 28
                        UpdateLine(26) = Debtor.Item("DebtBalance")

                    Case 101 To 108

                        Dim ReturnDetails As String = ""
                        If Not IsDBNull(Debtor("return_details")) Then ReturnDetails = Debtor("return_details")
                        ' Replace in this order so that a CrLf does not become two spaces
                        ReturnDetails = ReturnDetails.Replace(vbCrLf, " ")
                        ReturnDetails = ReturnDetails.Replace(vbCr, " ")
                        ReturnDetails = ReturnDetails.Replace(vbLf, " ")
                        ReturnDetails = ReturnDetails.Replace(vbTab, " ")

                        UpdateLine(22) = Microsoft.VisualBasic.Left(ReturnDetails, 200)

                    Case 201 To 228
                        UpdateLine(17) = CDate(Debtor.Item("ReturnDate")).ToString("yyyyMMdd")
                        UpdateLine(18) = Debtor.Item("QueryCode")
                        UpdateLine(20) = "11"

                End Select

                AppendToFile(UpdateFileName, String.Join(Separator, UpdateLine) & LineTerminator)

            Next Debtor

            Me.Cursor = Cursors.Default

            Dim ClipboardText As String = Nothing

            ' Check that every requested DebtorID is in the output file
            If RequestedDebtorIDs.Count <> OutputDebtorIDs.Count Then

                For Each RequestedDebtorID As String In RequestedDebtorIDs
                    If Not OutputDebtorIDs.Contains(RequestedDebtorID) Then
                        If Not ClipboardText Is Nothing Then ClipboardText &= ","
                        ClipboardText &= RequestedDebtorID
                    End If
                Next RequestedDebtorID

                If Not ClipboardText Is Nothing Then
                    Clipboard.SetText(ClipboardText, TextDataFormat.Text)
                    MessageBox.Show("The following cases have not been picked up" & vbCrLf & ClipboardText.Replace(",", vbCrLf) & vbCrLf & "IDs copied to clipboard.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

            End If

            MessageBox.Show("Returns file created.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

            Process.Start("explorer.exe", "/select," & UpdateFileName)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

End Class

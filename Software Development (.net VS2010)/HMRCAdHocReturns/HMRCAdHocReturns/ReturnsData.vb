﻿Imports CommonLibrary

Public Class ReturnsData
    Private Debtor As New DataTable
    Private DebtorDV As DataView
    Private ConnID As String = "4402,4403,4404,4405,4518,4519,4521,4522,4600,4602,4604,4606" ' removed connID 4520. TS 19/May/2015. Request ref 50300

    Public ReadOnly Property Debtors() As DataView
        Get
            Debtors = DebtorDV
        End Get
    End Property

    Public Sub GetDebtors(DebtorIDs As String)
        Dim Sql As String

        Try
            ' Get closed and recalled cases 
            Sql = "SELECT d._rowID AS debtorID " & _
                  "     , d.client_ref " & _
                  "     , d.client_batch AS TrancheID " & _
                  "     , CASE WHEN d.status = 'S' THEN 6 ELSE IFNULL(csr.clientReturnNumber,0) END AS QueryCode " & _
                  "     , d.return_details " & _
                  "     , r.date AS QueryDate " & _
                  "     , d.debt_balance AS DebtBalance " & _
                  "     , d.return_date AS ReturnDate " & _
                  "FROM Debtor AS d " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "INNER JOIN remit AS r ON d.return_remitid = r._rowid " & _
                  "LEFT JOIN ClientSchemeReturn AS csr ON d.ClientSchemeID = csr.ClientSchemeid " & _
                  "                                   AND d.return_codeid  = csr.returnid " & _
                  "LEFT JOIN ( SELECT n_s.DebtorID " & _
                  "                 , n_s.text " & _
                  "            FROM note AS n_s " & _
                  "            INNER JOIN ( SELECT n_m_s.DebtorID " & _
                  "                              , MAX(n_m_s._rowID) AS _rowID " & _
                  "                         FROM debtor AS d_m_s " & _
                  "                         INNER JOIN clientscheme AS cs_m_s ON d_m_s.ClientSchemeID = cs_m_s._rowID " & _
                  "                         INNER JOIN note AS n_m_s ON d_m_s._rowID = n_m_s._rowID " & _
                  "                         WHERE cs_m_s._rowID IN (" & ConnID & ") " & _
                  "                           AND d_m_s.status_open_closed = 'C' " & _
                  "                           AND n_m_s.type = 'Cancelled' " & _
                  "                           AND n_m_s.text LIKE '%Re: Gone Away - %' " & _
                  "                         GROUP BY n_m_s.DebtorID " & _
                  "                       ) AS n_m ON n_s.DebtorID = n_m.DebtorID " & _
                  "                               AND n_s._rowID   = n_m._rowID" & _
                  "          ) AS n ON n.DebtorID = d._rowID " & _
                  "WHERE cs._rowID IN (" & ConnID & ")" & _
                  "  AND d.status_open_closed = 'C' " & _
                  "  AND d._rowID IN ( " & DebtorIDs & ") "

            LoadDataTable("DebtRecovery", Sql, Debtor, False)

            DebtorDV = New DataView(Debtor)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

End Class

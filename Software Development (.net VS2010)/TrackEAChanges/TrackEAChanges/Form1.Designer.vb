﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.changebtn = New System.Windows.Forms.Button()
        Me.runbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.enddtp = New System.Windows.Forms.DateTimePicker()
        Me.startdtp = New System.Windows.Forms.DateTimePicker()
        Me.startlbl = New System.Windows.Forms.Label()
        Me.endlbl = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.SuspendLayout()
        '
        'changebtn
        '
        Me.changebtn.Location = New System.Drawing.Point(139, 49)
        Me.changebtn.Name = "changebtn"
        Me.changebtn.Size = New System.Drawing.Size(116, 23)
        Me.changebtn.TabIndex = 0
        Me.changebtn.Text = "Check for Changes"
        Me.changebtn.UseVisualStyleBackColor = True
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(155, 114)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(75, 23)
        Me.runbtn.TabIndex = 1
        Me.runbtn.Text = "Run report"
        Me.runbtn.UseVisualStyleBackColor = True
        Me.runbtn.Visible = False
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(287, 264)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'enddtp
        '
        Me.enddtp.Location = New System.Drawing.Point(217, 178)
        Me.enddtp.Name = "enddtp"
        Me.enddtp.Size = New System.Drawing.Size(117, 20)
        Me.enddtp.TabIndex = 3
        Me.enddtp.Visible = False
        '
        'startdtp
        '
        Me.startdtp.Location = New System.Drawing.Point(32, 178)
        Me.startdtp.Name = "startdtp"
        Me.startdtp.Size = New System.Drawing.Size(123, 20)
        Me.startdtp.TabIndex = 4
        Me.startdtp.Visible = False
        '
        'startlbl
        '
        Me.startlbl.AutoSize = True
        Me.startlbl.Location = New System.Drawing.Point(66, 162)
        Me.startlbl.Name = "startlbl"
        Me.startlbl.Size = New System.Drawing.Size(55, 13)
        Me.startlbl.TabIndex = 5
        Me.startlbl.Text = "Start Date"
        Me.startlbl.Visible = False
        '
        'endlbl
        '
        Me.endlbl.AutoSize = True
        Me.endlbl.Location = New System.Drawing.Point(245, 162)
        Me.endlbl.Name = "endlbl"
        Me.endlbl.Size = New System.Drawing.Size(52, 13)
        Me.endlbl.TabIndex = 6
        Me.endlbl.Text = "End Date"
        Me.endlbl.Visible = False
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 264)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(243, 23)
        Me.ProgressBar1.TabIndex = 7
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(384, 330)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.endlbl)
        Me.Controls.Add(Me.startlbl)
        Me.Controls.Add(Me.startdtp)
        Me.Controls.Add(Me.enddtp)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Controls.Add(Me.changebtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Track EA Changes"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents changebtn As System.Windows.Forms.Button
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents enddtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents startdtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents startlbl As System.Windows.Forms.Label
    Friend WithEvents endlbl As System.Windows.Forms.Label
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog

End Class

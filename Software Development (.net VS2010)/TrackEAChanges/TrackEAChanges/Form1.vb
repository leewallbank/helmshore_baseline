﻿Imports CommonLibrary
Public Class Form1

    Dim upd_txt As String
    Dim outfile As String = "BailiffID,Forename,Surname,AgentType,IntExt,LoginName,DateChangeFound,ChangeType,Original,New" & vbNewLine
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub changebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles changebtn.Click
        'ConnectDb2("DebtRecoveryLocal")
        'ConnectDb2Fees("FeesSQL")
        'delete all rows
        'upd_txt = "delete from EAChanges where EA_bailID =3"
        'update_sql(upd_txt)
        'upd_txt = "update EAChanges set EA_login = ' ' where EA_bailID =3"
        'update_sql(upd_txt)
        'upd_txt = "delete from EAChangesFound where EAF_bailID >0"
        'update_sql(upd_txt)
        'get last date from EAChangesFound
        Dim lastChangeDate As Date
        Dim dateArray As Object()
        'ONE row ONLY
        dateArray = GetSQLResultsArray("FeesSQL", "SELECT max(EAF_date) FROM EAChangesFound")
        Try
            lastChangeDate = dateArray(0)
        Catch ex As Exception
            lastChangeDate = Now
        End Try

        startdtp.Value = lastChangeDate
        'get all EAs
        changebtn.Enabled = False
        exitbtn.Enabled = False
        Dim bail_dt As New DataTable
        LoadDataTable("DebtRecoveryLocal", "SELECT _rowid, name_sur, name_fore,address,vatReg, status,internalExternal, " & _
                       "login_name FROM bailiff " & _
                       " order by _rowID", bail_dt, False)
        ProgressBar1.Maximum = bail_dt.Rows.Count
        For Each bailRow In bail_dt.Rows
            ProgressBar1.Value += 1
            Application.DoEvents()
            Dim bailID As Integer = bailRow(0)
            Dim nameSur As String = ""
            Try
                nameSur = bailRow(1)
            Catch ex As Exception

            End Try

            nameSur = Replace(nameSur, "'", " ")
            Dim nameFore As String = ""
            Try
                nameFore = bailRow(2)
            Catch ex As Exception

            End Try
            Dim address As String = ""
            Try
                address = bailRow(3)
            Catch ex As Exception

            End Try
            address = Replace(address, "'", " ")
            Dim vatReg As String = ""
            Try
                vatReg = bailRow(4)
            Catch ex As Exception

            End Try

            Dim status As String = bailRow(5)
            Dim intExt As String = bailRow(6)
            Dim login As String = ""
            Try
                login = bailRow(7)
            Catch ex As Exception

            End Try
            'see if bailID exists on table

            Dim bailArray As Object()
            'ONE row ONLY
            bailArray = GetSQLResultsArray("FeesSQL", "SELECT EA_bailID, EA_nameSur, EA_nameFore, EA_address," & _
                                           "EA_VATReg, EA_status, EA_intExt, EA_login FROM EAChanges " & _
                                                    "WHERE EA_bailID = " & bailID)
            Dim bailFound As Boolean = False
            Try
                Dim bail = bailArray(0)
                bailFound = True
            Catch ex As Exception
                upd_txt = "insert into EAChanges (EA_bailID, EA_nameSur, " & _
             "EA_nameFore, EA_address, EA_VATReg, EA_status, EA_intExt, EA_login)" & _
             "values (" & bailID & ",'" & nameSur & "','" & nameFore & "','" & address & "','" &
             vatReg & "','" & status & "','" & intExt & "','" & login & "')"
                Try
                    update_sql(upd_txt)
                Catch ex2 As Exception
                    MsgBox(ex2.Message)
                End Try
            End Try
            If bailFound Then
                'see if anything has changed
                Dim test As Integer
                Try
                    test = bailArray(1).length
                Catch ex As Exception
                    bailArray(1) = ""
                End Try
                Try
                    test = bailArray(2).length
                Catch ex As Exception
                    bailArray(2) = ""
                End Try
                Try
                    test = bailArray(3).length
                Catch ex As Exception
                    bailArray(3) = ""
                End Try
                Try
                    test = bailArray(7).length
                Catch ex As Exception
                    bailArray(7) = ""
                End Try
                If bailArray(1) <> nameSur Then
                    'change found
                    upd_txt = "insert into EAChangesFound (EAF_bailID, EAF_date, EAF_type, EAF_original,EAF_new)" & _
                         " values (" & bailArray(0) & ",'" & Format(Now, "yyyy-MMM-dd HH:mm:ss") & "','Surname'" & ",'" & _
                         bailArray(1) & "','" & nameSur & "')"
                    update_sql(upd_txt)
                    upd_txt = "update EAChanges set EA_nameSur = '" & nameSur & "'" & _
                        " where EA_bailID = " & bailArray(0)
                    update_sql(upd_txt)
                End If
                If bailArray(2) <> nameFore Then
                    upd_txt = "insert into EAChangesFound (EAF_bailID, EAF_date, EAF_type, EAF_original,EAF_new)" & _
                        " values (" & bailArray(0) & ",'" & Format(Now, "yyyy-MMM-dd HH:mm:ss") & "','Forename'" & ",'" & _
                        bailArray(2) & "','" & nameFore & "')"
                    update_sql(upd_txt)
                    upd_txt = "update EAChanges set EA_nameFore = '" & nameFore & "'" & _
                        " where EA_bailID = " & bailArray(0)
                    update_sql(upd_txt)
                End If
                If bailArray(3) <> address Then
                    upd_txt = "insert into EAChangesFound (EAF_bailID, EAF_date, EAF_type, EAF_original,EAF_new)" & _
                        " values (" & bailArray(0) & ",'" & Format(Now, "yyyy-MMM-dd HH:mm:ss") & "','Address'" & ",'" & _
                        bailArray(3) & "','" & address & "')"
                    update_sql(upd_txt)
                    upd_txt = "update EAChanges set EA_address = '" & address & "'" & _
                        " where EA_bailID = " & bailArray(0)
                    update_sql(upd_txt)
                End If
                If bailArray(4) <> vatReg Then
                    upd_txt = "insert into EAChangesFound (EAF_bailID, EAF_date, EAF_type, EAF_original,EAF_new)" & _
                        " values (" & bailArray(0) & ",'" & Format(Now, "yyyy-MMM-dd HH:mm:ss") & "','VAT Reg'" & ",'" & _
                        bailArray(4) & "','" & vatReg & "')"
                    Try
                        update_sql(upd_txt)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                    upd_txt = "update EAChanges set EA_VATReg = '" & vatReg & "'" & _
                        " where EA_bailID = " & bailArray(0)
                    update_sql(upd_txt)
                End If
                If bailArray(5) <> status Then
                    upd_txt = "insert into EAChangesFound (EAF_bailID, EAF_date, EAF_type, EAF_original,EAF_new)" & _
                        " values (" & bailArray(0) & ",'" & Format(Now, "yyyy-MMM-dd HH:mm:ss") & "','Status'" & ",'" & _
                        bailArray(5) & "','" & status & "')"
                    update_sql(upd_txt)
                    upd_txt = "update EAChanges set EA_status = '" & status & "'" & _
                       " where EA_bailID = " & bailArray(0)
                    update_sql(upd_txt)
                End If
                If bailArray(6) <> intExt Then
                    upd_txt = "insert into EAChangesFound (EAF_bailID, EAF_date, EAF_type, EAF_original,EAF_new)" & _
                        " values (" & bailArray(0) & ",'" & Format(Now, "yyyy-MMM-dd HH:mm:ss") & "','IntExt'" & ",'" & _
                        bailArray(6) & "','" & intExt & "')"
                    upd_txt = "update EAChanges set EA_intExt = '" & intExt & "'" & _
                       " where EA_bailID = " & bailArray(0)
                    update_sql(upd_txt)
                    update_sql(upd_txt)
                End If
                If bailArray(7) <> login Then
                    upd_txt = "insert into EAChangesFound (EAF_bailID, EAF_date, EAF_type, EAF_original,EAF_new)" & _
                        " values (" & bailArray(0) & ",'" & Format(Now, "yyyy-MMM-dd HH:mm:ss") & "','Login'" & ",'" & _
                        bailArray(7) & "','" & login & "')"
                    update_sql(upd_txt)
                    upd_txt = "update EAChanges set EA_login = '" & login & "'" & _
                       " where EA_bailID = " & bailArray(0)
                    update_sql(upd_txt)
                End If
            End If
        Next
        runbtn.Visible = True
        startdtp.Visible = True
        startlbl.Visible = True
        enddtp.Visible = True
        endlbl.Visible = True
        changebtn.Enabled = True
        exitbtn.Enabled = True

    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        'get changes in selected period
        Dim EAF_dt As New DataTable
        LoadDataTable("FeesSQL", "SELECT EAF_bailID, EAF_date, EAF_type,EAF_original,EAF_new " & _
                       " FROM EAChangesFound " & _
                       " order by EAF_bailID, EAF_date", EAF_dt, False)
        For Each EAFrow In EAF_dt.Rows
            If Format(EAFrow(1), "yyyy-MM-dd") < Format(startdtp.Value, "yyyy-MM-dd") Then
                Continue For
            End If
            If Format(EAFrow(1), "yyyy-MM-dd") > Format(enddtp.Value, "yyyy-MM-dd") Then
                Continue For
            End If
            Dim bailID As Integer = EAFrow(0)
            'get details from onestep
            Dim bail2array As Object
            bail2array = GetSQLResultsArray("DebtRecoveryLocal", " select name_sur, name_fore, agent_type, internalExternal, login_name" & _
                                           " from bailiff where _rowid = " & bailID)
            'write output
            Dim forename As String = ""
            Try
                forename = bail2array(1)
            Catch ex As Exception

            End Try
            outfile &= bailID & "," & forename & "," & bail2array(0) & "," & bail2array(2) & "," & bail2array(3) & _
                "," & bail2array(4) & "," & Format(EAFrow(1), "dd/MM/yyyy") & "," & EAFrow(2) & "," & EAFrow(3) & "," & EAFrow(4) & vbNewLine
        Next

        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "CSV files |*.csv"
            .DefaultExt = ".csv"
            .OverwritePrompt = True
            .FileName = "EA Changes.csv"
        End With
        If Not SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            MsgBox("No file name selected for save ")
        Else
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
            MsgBox("File saved")
            Me.Close()
        End If

    End Sub
End Class

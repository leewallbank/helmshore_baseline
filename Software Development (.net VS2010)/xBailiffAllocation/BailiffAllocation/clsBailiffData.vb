﻿Public Class clsBailiffData
    Private BailiffDS As New DataSet
    Private BailiffDV As DataView

    Public Sub New()
        BailiffDS.Tables.Add("Bailiff")
    End Sub

    Protected Overrides Sub Finalize()
        If Not IsNothing(BailiffDV) Then BailiffDV.Dispose()

        If Not IsNothing(BailiffDS) Then BailiffDS.Dispose() : BailiffDS = Nothing
        MyBase.Finalize()
    End Sub

    Public ReadOnly Property BailiffDataView() As DataView
        Get
            BailiffDataView = BailiffDV
        End Get
    End Property

    Public Sub GetBailiffs(ByVal ParamList As String())

        ClearDataTable(BailiffDS, "Bailiff") ' was raw

        For Each Param As String In ParamList
            LoadDataTable("EXEC dbo.GetBailiffs " & Param, BailiffDS, "Bailiff", True) ' was raw
        Next Param

        BailiffDV = New DataView(BailiffDS.Tables("Bailiff").DefaultView.ToTable(True)) ' The true option removes duplicates. Shouldn't be any but caters for future change
        BailiffDV.AllowDelete = False
        BailiffDV.AllowNew = False
    End Sub
End Class

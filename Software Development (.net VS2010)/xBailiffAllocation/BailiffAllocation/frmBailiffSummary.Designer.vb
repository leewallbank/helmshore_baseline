﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBailiffSummary
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.cboDisplaySet = New System.Windows.Forms.ComboBox()
        Me.radPerc = New System.Windows.Forms.RadioButton()
        Me.cmdBailiffTypeClear = New System.Windows.Forms.Button()
        Me.cmdRefreshDB = New System.Windows.Forms.Button()
        Me.dgvBailiffType = New System.Windows.Forms.DataGridView()
        Me.BailiffType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdBailiffTypeAll = New System.Windows.Forms.Button()
        Me.radAbs = New System.Windows.Forms.RadioButton()
        Me.pnlAbsPerc = New System.Windows.Forms.Panel()
        Me.lblLine1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdClearAll = New System.Windows.Forms.Button()
        Me.cboPeriodType = New System.Windows.Forms.ComboBox()
        Me.dgvPostcodeArea = New System.Windows.Forms.DataGridView()
        Me.PostcodeArea = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmsList = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.lblPeriodType = New System.Windows.Forms.Label()
        Me.cmdPostcodeAreaAll = New System.Windows.Forms.Button()
        Me.cmdPostcodeAreaClear = New System.Windows.Forms.Button()
        Me.cmsSummary = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmdBailiffNameClear = New System.Windows.Forms.Button()
        Me.cmdBailiffNameAll = New System.Windows.Forms.Button()
        Me.dgvBailiffName = New System.Windows.Forms.DataGridView()
        Me.BailiffName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colClientNameTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvSummary = New System.Windows.Forms.DataGridView()
        Me.cmdStatusNameClear = New System.Windows.Forms.Button()
        Me.dgvStatusName = New System.Windows.Forms.DataGridView()
        Me.StatusName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdStatusNameAll = New System.Windows.Forms.Button()
        Me.cmsForm = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.lblSummary = New System.Windows.Forms.Label()
        Me.cmdVanFeesAppliedClear = New System.Windows.Forms.Button()
        Me.dgvVanFeesApplied = New System.Windows.Forms.DataGridView()
        Me.cmdVanFeesAppliedAll = New System.Windows.Forms.Button()
        Me.cmdLeviedClear = New System.Windows.Forms.Button()
        Me.dgvLevied = New System.Windows.Forms.DataGridView()
        Me.cmdLeviedAll = New System.Windows.Forms.Button()
        Me.Levied = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VanFeesApplied = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvBailiffType, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAbsPerc.SuspendLayout()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBailiffName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvStatusName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvVanFeesApplied, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLevied, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cboDisplaySet
        '
        Me.cboDisplaySet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisplaySet.FormattingEnabled = True
        Me.cboDisplaySet.Items.AddRange(New Object() {"Months", "Periods"})
        Me.cboDisplaySet.Location = New System.Drawing.Point(592, 280)
        Me.cboDisplaySet.Name = "cboDisplaySet"
        Me.cboDisplaySet.Size = New System.Drawing.Size(59, 21)
        Me.cboDisplaySet.TabIndex = 86
        '
        'radPerc
        '
        Me.radPerc.AutoSize = True
        Me.radPerc.Location = New System.Drawing.Point(55, 4)
        Me.radPerc.Name = "radPerc"
        Me.radPerc.Size = New System.Drawing.Size(33, 17)
        Me.radPerc.TabIndex = 4
        Me.radPerc.TabStop = True
        Me.radPerc.Text = "%"
        Me.radPerc.UseVisualStyleBackColor = True
        '
        'cmdBailiffTypeClear
        '
        Me.cmdBailiffTypeClear.Location = New System.Drawing.Point(446, 73)
        Me.cmdBailiffTypeClear.Name = "cmdBailiffTypeClear"
        Me.cmdBailiffTypeClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdBailiffTypeClear.TabIndex = 75
        Me.cmdBailiffTypeClear.Text = "Clear"
        Me.cmdBailiffTypeClear.UseVisualStyleBackColor = True
        '
        'cmdRefreshDB
        '
        Me.cmdRefreshDB.Location = New System.Drawing.Point(7, 282)
        Me.cmdRefreshDB.Name = "cmdRefreshDB"
        Me.cmdRefreshDB.Size = New System.Drawing.Size(75, 20)
        Me.cmdRefreshDB.TabIndex = 76
        Me.cmdRefreshDB.Text = "Refresh DB"
        Me.cmdRefreshDB.UseVisualStyleBackColor = True
        '
        'dgvBailiffType
        '
        Me.dgvBailiffType.AllowUserToAddRows = False
        Me.dgvBailiffType.AllowUserToDeleteRows = False
        Me.dgvBailiffType.AllowUserToResizeColumns = False
        Me.dgvBailiffType.AllowUserToResizeRows = False
        Me.dgvBailiffType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBailiffType.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BailiffType, Me.DataGridViewTextBoxColumn5})
        Me.dgvBailiffType.Location = New System.Drawing.Point(369, 8)
        Me.dgvBailiffType.Name = "dgvBailiffType"
        Me.dgvBailiffType.ReadOnly = True
        Me.dgvBailiffType.RowHeadersVisible = False
        Me.dgvBailiffType.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvBailiffType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBailiffType.Size = New System.Drawing.Size(142, 65)
        Me.dgvBailiffType.TabIndex = 73
        '
        'BailiffType
        '
        Me.BailiffType.DataPropertyName = "BailiffType"
        Me.BailiffType.HeaderText = "Type"
        Me.BailiffType.Name = "BailiffType"
        Me.BailiffType.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 40
        '
        'cmdBailiffTypeAll
        '
        Me.cmdBailiffTypeAll.Location = New System.Drawing.Point(384, 73)
        Me.cmdBailiffTypeAll.Name = "cmdBailiffTypeAll"
        Me.cmdBailiffTypeAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdBailiffTypeAll.TabIndex = 74
        Me.cmdBailiffTypeAll.Text = "All"
        Me.cmdBailiffTypeAll.UseVisualStyleBackColor = True
        '
        'radAbs
        '
        Me.radAbs.AutoSize = True
        Me.radAbs.Location = New System.Drawing.Point(6, 4)
        Me.radAbs.Name = "radAbs"
        Me.radAbs.Size = New System.Drawing.Size(43, 17)
        Me.radAbs.TabIndex = 3
        Me.radAbs.TabStop = True
        Me.radAbs.Text = "Abs"
        Me.radAbs.UseVisualStyleBackColor = True
        '
        'pnlAbsPerc
        '
        Me.pnlAbsPerc.Controls.Add(Me.radPerc)
        Me.pnlAbsPerc.Controls.Add(Me.radAbs)
        Me.pnlAbsPerc.Location = New System.Drawing.Point(839, 280)
        Me.pnlAbsPerc.Name = "pnlAbsPerc"
        Me.pnlAbsPerc.Size = New System.Drawing.Size(99, 25)
        Me.pnlAbsPerc.TabIndex = 71
        '
        'lblLine1
        '
        Me.lblLine1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLine1.Location = New System.Drawing.Point(7, 273)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(1007, 1)
        Me.lblLine1.TabIndex = 70
        '
        'cmdClearAll
        '
        Me.cmdClearAll.Location = New System.Drawing.Point(946, 281)
        Me.cmdClearAll.Name = "cmdClearAll"
        Me.cmdClearAll.Size = New System.Drawing.Size(68, 20)
        Me.cmdClearAll.TabIndex = 69
        Me.cmdClearAll.Text = "Clear All"
        Me.cmdClearAll.UseVisualStyleBackColor = True
        '
        'cboPeriodType
        '
        Me.cboPeriodType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriodType.FormattingEnabled = True
        Me.cboPeriodType.Location = New System.Drawing.Point(717, 280)
        Me.cboPeriodType.Name = "cboPeriodType"
        Me.cboPeriodType.Size = New System.Drawing.Size(92, 21)
        Me.cboPeriodType.TabIndex = 84
        '
        'dgvPostcodeArea
        '
        Me.dgvPostcodeArea.AllowUserToAddRows = False
        Me.dgvPostcodeArea.AllowUserToDeleteRows = False
        Me.dgvPostcodeArea.AllowUserToResizeColumns = False
        Me.dgvPostcodeArea.AllowUserToResizeRows = False
        Me.dgvPostcodeArea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPostcodeArea.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PostcodeArea, Me.DataGridViewTextBoxColumn9})
        Me.dgvPostcodeArea.Location = New System.Drawing.Point(758, 8)
        Me.dgvPostcodeArea.Name = "dgvPostcodeArea"
        Me.dgvPostcodeArea.ReadOnly = True
        Me.dgvPostcodeArea.RowHeadersVisible = False
        Me.dgvPostcodeArea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPostcodeArea.Size = New System.Drawing.Size(115, 242)
        Me.dgvPostcodeArea.TabIndex = 81
        '
        'PostcodeArea
        '
        Me.PostcodeArea.DataPropertyName = "PostcodeArea"
        Me.PostcodeArea.HeaderText = "Area"
        Me.PostcodeArea.Name = "PostcodeArea"
        Me.PostcodeArea.ReadOnly = True
        Me.PostcodeArea.Width = 55
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 40
        '
        'cmsList
        '
        Me.cmsList.Name = "ContextMenuStrip1"
        Me.cmsList.Size = New System.Drawing.Size(61, 4)
        '
        'lblPeriodType
        '
        Me.lblPeriodType.Location = New System.Drawing.Point(592, 285)
        Me.lblPeriodType.Name = "lblPeriodType"
        Me.lblPeriodType.Size = New System.Drawing.Size(119, 16)
        Me.lblPeriodType.TabIndex = 85
        Me.lblPeriodType.Text = "based on:"
        Me.lblPeriodType.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmdPostcodeAreaAll
        '
        Me.cmdPostcodeAreaAll.Location = New System.Drawing.Point(759, 250)
        Me.cmdPostcodeAreaAll.Name = "cmdPostcodeAreaAll"
        Me.cmdPostcodeAreaAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaAll.TabIndex = 82
        Me.cmdPostcodeAreaAll.Text = "All"
        Me.cmdPostcodeAreaAll.UseVisualStyleBackColor = True
        '
        'cmdPostcodeAreaClear
        '
        Me.cmdPostcodeAreaClear.Location = New System.Drawing.Point(821, 250)
        Me.cmdPostcodeAreaClear.Name = "cmdPostcodeAreaClear"
        Me.cmdPostcodeAreaClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaClear.TabIndex = 83
        Me.cmdPostcodeAreaClear.Text = "Clear"
        Me.cmdPostcodeAreaClear.UseVisualStyleBackColor = True
        '
        'cmsSummary
        '
        Me.cmsSummary.Name = "ContextMenuStrip1"
        Me.cmsSummary.Size = New System.Drawing.Size(61, 4)
        '
        'cmdBailiffNameClear
        '
        Me.cmdBailiffNameClear.Location = New System.Drawing.Point(242, 250)
        Me.cmdBailiffNameClear.Name = "cmdBailiffNameClear"
        Me.cmdBailiffNameClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdBailiffNameClear.TabIndex = 57
        Me.cmdBailiffNameClear.Text = "Clear"
        Me.cmdBailiffNameClear.UseVisualStyleBackColor = True
        '
        'cmdBailiffNameAll
        '
        Me.cmdBailiffNameAll.Location = New System.Drawing.Point(180, 250)
        Me.cmdBailiffNameAll.Name = "cmdBailiffNameAll"
        Me.cmdBailiffNameAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdBailiffNameAll.TabIndex = 56
        Me.cmdBailiffNameAll.Text = "All"
        Me.cmdBailiffNameAll.UseVisualStyleBackColor = True
        '
        'dgvBailiffName
        '
        Me.dgvBailiffName.AllowUserToAddRows = False
        Me.dgvBailiffName.AllowUserToDeleteRows = False
        Me.dgvBailiffName.AllowUserToResizeColumns = False
        Me.dgvBailiffName.AllowUserToResizeRows = False
        Me.dgvBailiffName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBailiffName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BailiffName, Me.colClientNameTotal})
        Me.dgvBailiffName.Location = New System.Drawing.Point(156, 8)
        Me.dgvBailiffName.Name = "dgvBailiffName"
        Me.dgvBailiffName.ReadOnly = True
        Me.dgvBailiffName.RowHeadersVisible = False
        Me.dgvBailiffName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBailiffName.Size = New System.Drawing.Size(160, 242)
        Me.dgvBailiffName.TabIndex = 46
        '
        'BailiffName
        '
        Me.BailiffName.DataPropertyName = "BailiffName"
        Me.BailiffName.HeaderText = "Name"
        Me.BailiffName.Name = "BailiffName"
        Me.BailiffName.ReadOnly = True
        '
        'colClientNameTotal
        '
        Me.colClientNameTotal.DataPropertyName = "Total"
        Me.colClientNameTotal.HeaderText = "Total"
        Me.colClientNameTotal.Name = "colClientNameTotal"
        Me.colClientNameTotal.ReadOnly = True
        Me.colClientNameTotal.Width = 40
        '
        'dgvSummary
        '
        Me.dgvSummary.AllowUserToAddRows = False
        Me.dgvSummary.AllowUserToDeleteRows = False
        Me.dgvSummary.AllowUserToOrderColumns = True
        Me.dgvSummary.AllowUserToResizeRows = False
        Me.dgvSummary.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSummary.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSummary.Location = New System.Drawing.Point(7, 311)
        Me.dgvSummary.Name = "dgvSummary"
        Me.dgvSummary.ReadOnly = True
        Me.dgvSummary.RowHeadersVisible = False
        Me.dgvSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvSummary.Size = New System.Drawing.Size(1007, 239)
        Me.dgvSummary.TabIndex = 44
        '
        'cmdStatusNameClear
        '
        Me.cmdStatusNameClear.Location = New System.Drawing.Point(641, 73)
        Me.cmdStatusNameClear.Name = "cmdStatusNameClear"
        Me.cmdStatusNameClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdStatusNameClear.TabIndex = 89
        Me.cmdStatusNameClear.Text = "Clear"
        Me.cmdStatusNameClear.UseVisualStyleBackColor = True
        '
        'dgvStatusName
        '
        Me.dgvStatusName.AllowUserToAddRows = False
        Me.dgvStatusName.AllowUserToDeleteRows = False
        Me.dgvStatusName.AllowUserToResizeColumns = False
        Me.dgvStatusName.AllowUserToResizeRows = False
        Me.dgvStatusName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStatusName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StatusName, Me.DataGridViewTextBoxColumn2})
        Me.dgvStatusName.Location = New System.Drawing.Point(564, 8)
        Me.dgvStatusName.Name = "dgvStatusName"
        Me.dgvStatusName.ReadOnly = True
        Me.dgvStatusName.RowHeadersVisible = False
        Me.dgvStatusName.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvStatusName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvStatusName.Size = New System.Drawing.Size(142, 65)
        Me.dgvStatusName.TabIndex = 87
        '
        'StatusName
        '
        Me.StatusName.DataPropertyName = "StatusName"
        Me.StatusName.HeaderText = "Status"
        Me.StatusName.Name = "StatusName"
        Me.StatusName.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 40
        '
        'cmdStatusNameAll
        '
        Me.cmdStatusNameAll.Location = New System.Drawing.Point(579, 73)
        Me.cmdStatusNameAll.Name = "cmdStatusNameAll"
        Me.cmdStatusNameAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdStatusNameAll.TabIndex = 88
        Me.cmdStatusNameAll.Text = "All"
        Me.cmdStatusNameAll.UseVisualStyleBackColor = True
        '
        'cmsForm
        '
        Me.cmsForm.Name = "ContextMenuStrip1"
        Me.cmsForm.Size = New System.Drawing.Size(61, 4)
        '
        'lblSummary
        '
        Me.lblSummary.AutoSize = True
        Me.lblSummary.Location = New System.Drawing.Point(88, 286)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(39, 13)
        Me.lblSummary.TabIndex = 90
        Me.lblSummary.Text = "Label1"
        '
        'cmdVanFeesAppliedClear
        '
        Me.cmdVanFeesAppliedClear.Location = New System.Drawing.Point(641, 250)
        Me.cmdVanFeesAppliedClear.Name = "cmdVanFeesAppliedClear"
        Me.cmdVanFeesAppliedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdVanFeesAppliedClear.TabIndex = 96
        Me.cmdVanFeesAppliedClear.Text = "Clear"
        Me.cmdVanFeesAppliedClear.UseVisualStyleBackColor = True
        '
        'dgvVanFeesApplied
        '
        Me.dgvVanFeesApplied.AllowUserToAddRows = False
        Me.dgvVanFeesApplied.AllowUserToDeleteRows = False
        Me.dgvVanFeesApplied.AllowUserToResizeColumns = False
        Me.dgvVanFeesApplied.AllowUserToResizeRows = False
        Me.dgvVanFeesApplied.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVanFeesApplied.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.VanFeesApplied, Me.DataGridViewTextBoxColumn3})
        Me.dgvVanFeesApplied.Location = New System.Drawing.Point(564, 185)
        Me.dgvVanFeesApplied.Name = "dgvVanFeesApplied"
        Me.dgvVanFeesApplied.ReadOnly = True
        Me.dgvVanFeesApplied.RowHeadersVisible = False
        Me.dgvVanFeesApplied.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvVanFeesApplied.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVanFeesApplied.Size = New System.Drawing.Size(142, 65)
        Me.dgvVanFeesApplied.TabIndex = 94
        '
        'cmdVanFeesAppliedAll
        '
        Me.cmdVanFeesAppliedAll.Location = New System.Drawing.Point(579, 250)
        Me.cmdVanFeesAppliedAll.Name = "cmdVanFeesAppliedAll"
        Me.cmdVanFeesAppliedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdVanFeesAppliedAll.TabIndex = 95
        Me.cmdVanFeesAppliedAll.Text = "All"
        Me.cmdVanFeesAppliedAll.UseVisualStyleBackColor = True
        '
        'cmdLeviedClear
        '
        Me.cmdLeviedClear.Location = New System.Drawing.Point(446, 250)
        Me.cmdLeviedClear.Name = "cmdLeviedClear"
        Me.cmdLeviedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdLeviedClear.TabIndex = 93
        Me.cmdLeviedClear.Text = "Clear"
        Me.cmdLeviedClear.UseVisualStyleBackColor = True
        '
        'dgvLevied
        '
        Me.dgvLevied.AllowUserToAddRows = False
        Me.dgvLevied.AllowUserToDeleteRows = False
        Me.dgvLevied.AllowUserToResizeColumns = False
        Me.dgvLevied.AllowUserToResizeRows = False
        Me.dgvLevied.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLevied.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Levied, Me.DataGridViewTextBoxColumn6})
        Me.dgvLevied.Location = New System.Drawing.Point(369, 185)
        Me.dgvLevied.Name = "dgvLevied"
        Me.dgvLevied.ReadOnly = True
        Me.dgvLevied.RowHeadersVisible = False
        Me.dgvLevied.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvLevied.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLevied.Size = New System.Drawing.Size(142, 65)
        Me.dgvLevied.TabIndex = 91
        '
        'cmdLeviedAll
        '
        Me.cmdLeviedAll.Location = New System.Drawing.Point(384, 250)
        Me.cmdLeviedAll.Name = "cmdLeviedAll"
        Me.cmdLeviedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdLeviedAll.TabIndex = 92
        Me.cmdLeviedAll.Text = "All"
        Me.cmdLeviedAll.UseVisualStyleBackColor = True
        '
        'Levied
        '
        Me.Levied.DataPropertyName = "Levied"
        Me.Levied.HeaderText = "Levied"
        Me.Levied.Name = "Levied"
        Me.Levied.ReadOnly = True
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 40
        '
        'VanFeesApplied
        '
        Me.VanFeesApplied.DataPropertyName = "VanFeesApplied"
        Me.VanFeesApplied.HeaderText = "Van Fees"
        Me.VanFeesApplied.Name = "VanFeesApplied"
        Me.VanFeesApplied.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 40
        '
        'frmBailiffSummary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1020, 559)
        Me.Controls.Add(Me.cmdVanFeesAppliedClear)
        Me.Controls.Add(Me.dgvVanFeesApplied)
        Me.Controls.Add(Me.cmdVanFeesAppliedAll)
        Me.Controls.Add(Me.cmdLeviedClear)
        Me.Controls.Add(Me.dgvLevied)
        Me.Controls.Add(Me.cmdLeviedAll)
        Me.Controls.Add(Me.lblSummary)
        Me.Controls.Add(Me.cmdStatusNameClear)
        Me.Controls.Add(Me.dgvStatusName)
        Me.Controls.Add(Me.cmdStatusNameAll)
        Me.Controls.Add(Me.cboDisplaySet)
        Me.Controls.Add(Me.cmdBailiffTypeClear)
        Me.Controls.Add(Me.cmdRefreshDB)
        Me.Controls.Add(Me.dgvBailiffType)
        Me.Controls.Add(Me.cmdBailiffTypeAll)
        Me.Controls.Add(Me.pnlAbsPerc)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdClearAll)
        Me.Controls.Add(Me.cboPeriodType)
        Me.Controls.Add(Me.dgvPostcodeArea)
        Me.Controls.Add(Me.lblPeriodType)
        Me.Controls.Add(Me.cmdPostcodeAreaAll)
        Me.Controls.Add(Me.cmdPostcodeAreaClear)
        Me.Controls.Add(Me.cmdBailiffNameClear)
        Me.Controls.Add(Me.cmdBailiffNameAll)
        Me.Controls.Add(Me.dgvBailiffName)
        Me.Controls.Add(Me.dgvSummary)
        Me.Name = "frmBailiffSummary"
        Me.Text = "Bailiff Summary"
        CType(Me.dgvBailiffType, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAbsPerc.ResumeLayout(False)
        Me.pnlAbsPerc.PerformLayout()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBailiffName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvStatusName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvVanFeesApplied, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLevied, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboDisplaySet As System.Windows.Forms.ComboBox
    Friend WithEvents radPerc As System.Windows.Forms.RadioButton
    Friend WithEvents cmdBailiffTypeClear As System.Windows.Forms.Button
    Friend WithEvents cmdRefreshDB As System.Windows.Forms.Button
    Friend WithEvents dgvBailiffType As System.Windows.Forms.DataGridView
    Friend WithEvents cmdBailiffTypeAll As System.Windows.Forms.Button
    Friend WithEvents radAbs As System.Windows.Forms.RadioButton
    Friend WithEvents pnlAbsPerc As System.Windows.Forms.Panel
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cmdClearAll As System.Windows.Forms.Button
    Friend WithEvents cboPeriodType As System.Windows.Forms.ComboBox
    Friend WithEvents dgvPostcodeArea As System.Windows.Forms.DataGridView
    Friend WithEvents PostcodeArea As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmsList As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents lblPeriodType As System.Windows.Forms.Label
    Friend WithEvents cmdPostcodeAreaAll As System.Windows.Forms.Button
    Friend WithEvents cmdPostcodeAreaClear As System.Windows.Forms.Button
    Friend WithEvents cmsSummary As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmdBailiffNameClear As System.Windows.Forms.Button
    Friend WithEvents cmdBailiffNameAll As System.Windows.Forms.Button
    Friend WithEvents dgvBailiffName As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSummary As System.Windows.Forms.DataGridView
    Friend WithEvents BailiffType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BailiffName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClientNameTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdStatusNameClear As System.Windows.Forms.Button
    Friend WithEvents dgvStatusName As System.Windows.Forms.DataGridView
    Friend WithEvents StatusName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdStatusNameAll As System.Windows.Forms.Button
    Friend WithEvents cmsForm As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents cmdVanFeesAppliedClear As System.Windows.Forms.Button
    Friend WithEvents dgvVanFeesApplied As System.Windows.Forms.DataGridView
    Friend WithEvents cmdVanFeesAppliedAll As System.Windows.Forms.Button
    Friend WithEvents cmdLeviedClear As System.Windows.Forms.Button
    Friend WithEvents dgvLevied As System.Windows.Forms.DataGridView
    Friend WithEvents cmdLeviedAll As System.Windows.Forms.Button
    Friend WithEvents VanFeesApplied As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Levied As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

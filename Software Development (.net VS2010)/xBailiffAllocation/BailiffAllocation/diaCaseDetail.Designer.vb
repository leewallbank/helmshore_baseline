﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class diaCaseDetail
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.dgvDetail = New System.Windows.Forms.DataGridView()
        Me.cmdCopyDebtorID = New System.Windows.Forms.Button()
        Me.lblSummary = New System.Windows.Forms.Label()
        Me.cmsDetail = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.chkInclLinked = New System.Windows.Forms.CheckBox()
        CType(Me.dgvDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvDetail
        '
        Me.dgvDetail.AllowUserToAddRows = False
        Me.dgvDetail.AllowUserToDeleteRows = False
        Me.dgvDetail.AllowUserToOrderColumns = True
        Me.dgvDetail.AllowUserToResizeRows = False
        Me.dgvDetail.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDetail.Location = New System.Drawing.Point(12, 8)
        Me.dgvDetail.Name = "dgvDetail"
        Me.dgvDetail.ReadOnly = True
        Me.dgvDetail.RowHeadersVisible = False
        Me.dgvDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvDetail.Size = New System.Drawing.Size(703, 254)
        Me.dgvDetail.TabIndex = 0
        '
        'cmdCopyDebtorID
        '
        Me.cmdCopyDebtorID.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCopyDebtorID.Location = New System.Drawing.Point(622, 276)
        Me.cmdCopyDebtorID.Name = "cmdCopyDebtorID"
        Me.cmdCopyDebtorID.Size = New System.Drawing.Size(91, 24)
        Me.cmdCopyDebtorID.TabIndex = 1
        Me.cmdCopyDebtorID.Text = "Copy case IDs"
        Me.cmdCopyDebtorID.UseVisualStyleBackColor = True
        '
        'lblSummary
        '
        Me.lblSummary.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblSummary.AutoSize = True
        Me.lblSummary.Location = New System.Drawing.Point(11, 282)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(39, 13)
        Me.lblSummary.TabIndex = 2
        Me.lblSummary.Text = "Label1"
        '
        'cmsDetail
        '
        Me.cmsDetail.Name = "ContextMenuStrip1"
        Me.cmsDetail.Size = New System.Drawing.Size(61, 4)
        '
        'chkInclLinked
        '
        Me.chkInclLinked.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.chkInclLinked.AutoSize = True
        Me.chkInclLinked.Location = New System.Drawing.Point(333, 281)
        Me.chkInclLinked.Name = "chkInclLinked"
        Me.chkInclLinked.Size = New System.Drawing.Size(92, 17)
        Me.chkInclLinked.TabIndex = 3
        Me.chkInclLinked.Text = "Include linked"
        Me.chkInclLinked.UseVisualStyleBackColor = True
        '
        'diaCaseDetail
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(725, 312)
        Me.Controls.Add(Me.cmdCopyDebtorID)
        Me.Controls.Add(Me.lblSummary)
        Me.Controls.Add(Me.chkInclLinked)
        Me.Controls.Add(Me.dgvDetail)
        Me.Name = "diaCaseDetail"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Case Detail"
        CType(Me.dgvDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvDetail As System.Windows.Forms.DataGridView
    Friend WithEvents cmdCopyDebtorID As System.Windows.Forms.Button
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents cmsDetail As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents chkInclLinked As System.Windows.Forms.CheckBox
End Class

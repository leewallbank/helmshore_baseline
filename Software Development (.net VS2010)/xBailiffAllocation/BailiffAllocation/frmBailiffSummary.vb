﻿Public Class frmBailiffSummary

    Private SummaryData As New clsBailiffSummaryData
    ' These cannot be instantiated here as the datagridviews have not been instantiated at this point but they need to be declared here to achieve the right scope...

    Private BailiffNameGridState As clsGridState
    Private BailiffTypeGridState As clsGridState
    Private StatusNameGridState As clsGridState
    Private LeviedGridState As clsGridState
    Private VanFeesAppliedGridState As clsGridState
    Private PostcodeAreaGridState As clsGridState

    Private ColSort As String

    Private ParamList As String = "null,null,null,null,null,null,3" ' i.e. top level

#Region "New and open"

    Public Sub New()

        InitializeComponent()

        ' Now instantiate these

        BailiffNameGridState = New clsGridState(dgvBailiffName)
        BailiffTypeGridState = New clsGridState(dgvBailiffType)
        StatusNameGridState = New clsGridState(dgvStatusName)
        LeviedGridState = New clsGridState(dgvLevied)
        VanFeesAppliedGridState = New clsGridState(dgvVanFeesApplied)
        PostcodeAreaGridState = New clsGridState(dgvPostcodeArea)

        AddControlHandlers()

        cmsSummary.Items.Add("View Cases")
        cmsSummary.Items.Add("Copy")
        cmsSummary.Items.Add("Select All")

        cmsList.Items.Add("Copy")
        cmsList.Items.Add("Select All")

        cmsForm.Items.Add("Case View")

        SummaryData.GetSummary(ParamList, True, "M")
        dgvSummary.DataSource = SummaryData.GridDataView
        FormatGridColumns(dgvSummary)

        SummaryData.GetList(ParamList)
        SetListGrids()

        FormatListColumns()

        cboPeriodType.DataSource = SummaryData.PeriodTypeDataView
        cboPeriodType.ValueMember = "PeriodTypeID"
        cboPeriodType.DisplayMember = "PeriodTypeDesc"


    End Sub

    Private Sub frmSummary_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        RemoveSelectionHandlers()
    End Sub

    Private Sub frmSummary_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        SetSelections() 'Arrays will be nothing so clear all selections - by default the first row in a datagridview is always selected
        dgvSummary.ClearSelection()

        radAbs.Checked = True
        cboPeriodType.SelectedValue = 3
        cboDisplaySet.Text = "Months"

        AddHandler radAbs.CheckedChanged, AddressOf radAbs_CheckedChanged

        AddSelectionHandlers()

        ' SetcmdRefreshTip()
        cmdRefreshDB.Visible = False
        cboDisplaySet.Visible = False
        lblPeriodType.Text = "Months based on:"
    End Sub

    Private Sub frmBailiffSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        If e.Button = MouseButtons.Right Then
            cmsForm.Show(sender, e.Location)
        End If
    End Sub

    Private Sub frmBailiff_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        RemoveSelectionHandlers()
    End Sub

#End Region

#Region "Refresh"

    Private Sub RefreshGrid()

        ParamList = GetParam(dgvBailiffName, "BailiffName") & ","
        ParamList += GetParam(dgvBailiffType, "BailiffType") & ","
        ParamList += GetParam(dgvStatusName, "StatusName") & ","
        ParamList += GetParam(dgvLevied, "Levied") & ","
        ParamList += GetParam(dgvVanFeesApplied, "VanFeesApplied") & ","
        ParamList += GetParam(dgvPostcodeArea, "PostcodeArea")

        ParamList += "," & cboPeriodType.SelectedValue.ToString

        PreRefresh(Me)

        RemoveSelectionHandlers()

        GetSelections()

        SummaryData.GetSummary(ParamList, radAbs.Checked, cboDisplaySet.Text.Substring(0, 1))
        dgvSummary.DataSource = SummaryData.GridDataView

        FormatGridColumns(dgvSummary)

        SummaryData.GetList(ParamList)
        SetListGrids()
        FormatListColumns()

        SetSelections()

        AddSelectionHandlers()

        PostRefresh(Me)

        dgvSummary.ClearSelection() ' The first cell always get selected
    End Sub

    Private Function GetParam(ByRef DataGrid As DataGridView, ByVal ColumnName As String) As String
        ' Used when selection criteria change
        Dim Param As String = Nothing

        For Each dr As DataGridViewRow In DataGrid.SelectedRows
            If Not IsNothing(Param) Then Param += vbTab
            Param += dr.Cells(ColumnName).Value
        Next dr

        If IsNothing(Param) Then
            Param = "null"
        Else
            Param = "'" & Param & "'"
        End If

        Return Param
    End Function

    Private Function GetParam(ByVal ParamColumn As String, ByRef Cell As DataGridViewCell, ByRef ListDataGridView As DataGridView)
        ' Used when detail for a particular cell in the summary grid is retrieved
        Dim Param As String = ""

        If IsNothing(Cell.DataGridView.Columns(ParamColumn)) Then
            ' The column may not be present as only one criteria is applicable
            If ListDataGridView.SelectedRows.Count = 1 Then
                Param = "'" & ListDataGridView.SelectedRows(0).Cells(0).Value & "'"
            Else
                Param = "null"
            End If
        Else
            Param = "'" & Cell.DataGridView.Item(ParamColumn, Cell.RowIndex).Value & "'"
        End If

        Return Param
    End Function
#End Region

#Region "Grid selection events"

    Private Sub cboPeriodType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboPeriodType.SelectedValueChanged
        RefreshGrid()
    End Sub

    Private Sub cboDisplaySet_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboDisplaySet.SelectedValueChanged
        RefreshGrid()
    End Sub

    Private Sub dgvSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        RefreshGrid()
    End Sub

    Private Sub dgvMouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
        ' This is needed to make mousewheel scroll list items
        sender.Select()
    End Sub

    Private Sub dgvMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

        If hti.Type = DataGridViewHitTestType.ColumnHeader Then
            GetSelections()
            RemoveSelectionHandlers()
        End If
    End Sub

    Private Sub dgvMouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

        If hti.Type = DataGridViewHitTestType.ColumnHeader Then
            SetSelections()
            AddSelectionHandlers()
        End If

        If e.Button = MouseButtons.Right Then
            cmsList.Show(sender, e.Location)
        End If

    End Sub
    Private Sub dgvSummary_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseDown
        Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

        If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
            dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected = True
        End If

        If hti.Type = DataGridViewHitTestType.ColumnHeader Then
            ColSort = SummaryData.GridDataView.Sort
        End If
    End Sub

    Private Sub dgvSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseUp
        If e.Button = MouseButtons.Right Then cmsSummary.Show(dgvSummary, e.Location)

        Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

        If hti.Type = DataGridViewHitTestType.ColumnHeader Then
            If ColSort <> "" Then SummaryData.GridDataView.Sort += "," & ColSort
        End If
    End Sub

    Private Sub dgvSummary_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSummary.SelectionChanged
        Dim TotalCases As Integer = 0

        If dgvSummary.SelectedCells.Count = 0 Then
            lblSummary.Text = ""
        Else
            For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then TotalCases += Cell.Value
            Next Cell
            lblSummary.Text = TotalCases.ToString & " case" & If(TotalCases > 1, "s", "")
        End If

    End Sub

    Private Sub ShowDetail()

        Dim DetailParamList As String
        Dim Detail As New diaCaseDetail(Me)

        PreRefresh(Me)

        For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
            If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then

                DetailParamList = GetParam("BailiffName", Cell, dgvBailiffName) & ","
                DetailParamList += GetParam("BailiffType", Cell, dgvBailiffType) & ","
                DetailParamList += GetParam("StatusName", Cell, dgvStatusName) & ","
                DetailParamList += GetParam("Levied", Cell, dgvLevied) & ","
                DetailParamList += GetParam("VanFeesApplied", Cell, dgvVanFeesApplied) & ","
                DetailParamList += GetParam("PostcodeArea", Cell, dgvPostcodeArea)

                DetailParamList += "," & cboPeriodType.SelectedValue.ToString

                Detail.AddCases(DetailParamList, dgvSummary.Columns(Cell.ColumnIndex).Name)

            End If
        Next Cell

        PostRefresh(Me)

        Detail.ShowDialog()
        Detail = Nothing

    End Sub
#End Region

#Region "'All' and 'Clear' button click events"

    Private Sub cmdBailiffNameAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBailiffNameAll.Click
        dgvBailiffName.SelectAll()
        BailiffNameGridState.GetState()
    End Sub

    Private Sub cmdBailiffNameClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBailiffNameClear.Click
        dgvBailiffName.ClearSelection()
        BailiffNameGridState.GetState()
    End Sub

    Private Sub cmdBailiffTypeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBailiffTypeAll.Click
        dgvBailiffType.SelectAll()
        BailiffTypeGridState.GetState()
    End Sub

    Private Sub cmdBailiffTypeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBailiffTypeClear.Click
        dgvBailiffType.ClearSelection()
        BailiffTypeGridState.GetState()
    End Sub

    Private Sub cmdStatusNameAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStatusNameAll.Click
        dgvStatusName.SelectAll()
        StatusNameGridState.GetState()
    End Sub

    Private Sub cmdStatusNameClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStatusNameClear.Click
        dgvStatusName.ClearSelection()
        StatusNameGridState.GetState()
    End Sub

    Private Sub cmdStatusLeviedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLeviedAll.Click
        dgvLevied.SelectAll()
        LeviedGridState.GetState()
    End Sub

    Private Sub cmdLeviedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLeviedClear.Click
        dgvLevied.ClearSelection()
        LeviedGridState.GetState()
    End Sub

    Private Sub cmdVanFeesAppliedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVanFeesAppliedAll.Click
        dgvVanFeesApplied.SelectAll()
        VanFeesAppliedGridState.GetState()
    End Sub

    Private Sub cmdVanFeesAppliedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVanFeesAppliedClear.Click
        dgvVanFeesApplied.ClearSelection()
        VanFeesAppliedGridState.GetState()
    End Sub

    Private Sub cmdPostcodeAreaAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaAll.Click
        dgvPostcodeArea.SelectAll()
        PostcodeAreaGridState.GetState()
    End Sub

    Private Sub cmdPostcodeAreaClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaClear.Click
        dgvPostcodeArea.ClearSelection()
        PostcodeAreaGridState.GetState()
    End Sub
#End Region

#Region "Handlers"

    Private Sub RemoveSelectionHandlers()
        ' Need to find a way to do these at once but these SelectionChanged event is specific to the datagridview class and not control - the collection on the form

        RemoveHandler dgvBailiffName.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvBailiffType.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvStatusName.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvLevied.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvVanFeesApplied.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
        RemoveHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged
    End Sub

    Private Sub AddSelectionHandlers()

        AddHandler dgvBailiffName.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvBailiffType.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvStatusName.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvLevied.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvVanFeesApplied.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
        AddHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged
    End Sub

    Private Sub AddControlHandlers()
        ' All these grids and buttons call the same events so no point declaring them all separately
        For Each Control As Control In Me.Controls
            If TypeOf Control Is DataGridView Then
                AddHandler Control.MouseDown, AddressOf dgvMouseDown
                AddHandler Control.MouseUp, AddressOf dgvMouseUp
                AddHandler Control.MouseEnter, AddressOf dgvMouseEnter
            End If
        Next Control
    End Sub

#End Region

#Region "DataGridView methods"

    Private Sub SetListGrids()
        dgvBailiffName.DataSource = SummaryData.BailiffNameDataView
        BailiffNameGridState.SetSort()

        dgvBailiffType.DataSource = SummaryData.BailiffTypeDataView
        BailiffTypeGridState.SetSort()

        dgvStatusName.DataSource = SummaryData.StatusNameDataView
        StatusNameGridState.SetSort()

        dgvLevied.DataSource = SummaryData.LeviedDataView
        LeviedGridState.SetSort()

        dgvVanFeesApplied.DataSource = SummaryData.VanFeesAppliedDataView
        VanFeesAppliedGridState.SetSort()

        dgvPostcodeArea.DataSource = SummaryData.PostcodeAreaDataView
        PostcodeAreaGridState.SetSort()
    End Sub

    Private Sub FormatListColumns()
        ' This is slightly different to settings in modCommon as we want some of the fields wider than they need to be for presentation
        For Each Control As Control In Me.Controls
            If TypeOf Control Is DataGridView Then
                If Control.Name <> "dgvSummary" Then ' This is not a list grid
                    For Each Column In CType(Control, DataGridView).Columns
                        Select Case Column.Name
                            Case "Total"
                                Column.Width = 40
                            Case "BailiffName"
                                Column.Width = 100
                                Column.HeaderText = "Bailiff"
                            Case "BailiffType"
                                Column.Width = 100
                                Column.HeaderText = "Type"
                            Case "StatusName"
                                Column.Width = 100
                                Column.HeaderText = "Status"
                            Case "Levied"
                                Column.Width = 100
                                Column.HeaderText = "Levied"
                            Case "VanFeesApplied"
                                Column.Width = 100
                                Column.HeaderText = "Van Fees"
                            Case "PostcodeArea"
                                Column.Width = 55
                                Column.HeaderText = "Area"
                            Case Else
                                Column.Width = 40
                        End Select
                    Next Column
                End If
            End If
        Next Control
    End Sub

    Private Sub GetSelections()

        BailiffNameGridState.GetState()
        BailiffTypeGridState.GetState()
        StatusNameGridState.GetState()
        LeviedGridState.GetState()
        VanFeesAppliedGridState.GetState()
        PostcodeAreaGridState.GetState()

    End Sub

    Private Sub SetSelections()

        BailiffNameGridState.SetState()
        BailiffTypeGridState.SetState()
        StatusNameGridState.SetState()
        LeviedGridState.SetState()
        VanFeesAppliedGridState.SetState()
        PostcodeAreaGridState.SetState()

    End Sub

#End Region

#Region "Additional functionality"

    Private Sub cmdClearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClearAll.Click

        RemoveSelectionHandlers()

        cmdBailiffNameClear_Click(sender, New System.EventArgs)
        cmdBailiffTypeClear_Click(sender, New System.EventArgs)
        cmdStatusNameClear_Click(sender, New System.EventArgs)
        cmdLeviedClear_Click(sender, New System.EventArgs)
        cmdVanFeesAppliedClear_Click(sender, New System.EventArgs)
        cmdPostcodeAreaClear_Click(sender, New System.EventArgs)

        AddSelectionHandlers()

        RefreshGrid()
    End Sub

    Private Sub radAbs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles radAbs.CheckedChanged
        RefreshGrid()
        If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"
    End Sub

    Private Sub radPerc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPerc.CheckedChanged
        RefreshGrid()
        If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"
    End Sub

  

    'Private Sub cmdRefreshDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.Click

    '    If MsgBox("This could take several minutes, are you sure?", vbYesNo + vbDefaultButton2 + vbQuestion) = vbYes Then
    '        PreRefresh(Me)
    '        SummaryData.RefreshDatabase()
    '        RefreshGrid()
    '        PostRefresh(Me)
    '        MsgBox("Refresh complete.", vbOKOnly + vbInformation)
    '        SetcmdRefreshTip()
    '    End If

    'End Sub

    'Private Sub cmdRefreshDB_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.MouseLeave
    '    ' on XP machines there is a bug that prevents the tooltip showing a second time. This addresses the problem.
    '    ToolTip1.Active = False
    '    ToolTip1.Active = True
    'End Sub

    'Private Sub SetcmdRefreshTip()
    '    SummaryData.GetLastLoad()
    '    ToolTip1.SetToolTip(cmdRefreshDB, "Last refreshed at " & SummaryData.LastLoadDataView.Item(0).Item(0).ToString & " by " & SummaryData.LastLoadDataView.Item(0).Item(1).ToString)
    'End Sub

    Private Sub cmsSummary_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsSummary.ItemClicked
        Select Case e.ClickedItem.Text
            Case "View Cases"
                ShowDetail()
            Case "Copy"
                dgvSummary.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                Clipboard.SetDataObject(dgvSummary.GetClipboardContent())
            Case "Select All"
                dgvSummary.SelectAll()
        End Select
    End Sub

    Private Sub cmsList_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsList.ItemClicked
        ' Slightly more complicated than the summary grid as this event fires for all other datagridviews.
        Select Case e.ClickedItem.Text
            Case "Copy"
                sender.SourceControl.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                Clipboard.SetDataObject(sender.SourceControl.GetClipboardContent())
            Case "Select All"
                sender.SourceControl.SelectAll()
        End Select
    End Sub

    Private Sub cmsForm_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsForm.ItemClicked
        Select Case e.ClickedItem.Text
            Case "Case View"
                If FormOpen("frmCaseSummary") Then
                    frmCaseSummary.Activate()
                Else
                    frmCaseSummary.ShowDialog()
                End If
        End Select
    End Sub
#End Region

End Class
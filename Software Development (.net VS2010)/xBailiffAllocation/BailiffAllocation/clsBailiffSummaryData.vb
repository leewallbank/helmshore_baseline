﻿Public Class clsBailiffSummaryData

    Private SummaryDS As New DataSet

    Private LastLoadDV As DataView

    Private GridDV As DataView
    Private DetailDV As DataView
    Private PeriodTypeDV As DataView

    Private BailiffNameDV As DataView
    Private BailiffTypeDV As DataView
    Private StatusNameDV As DataView
    Private LeviedDV As DataView
    Private VanFeesAppliedDV As DataView
    Private PostcodeAreaDV As DataView

    Public Sub New()
        SummaryDS.Tables.Add("LastLoad")

        SummaryDS.Tables.Add("Grid")
        SummaryDS.Tables.Add("Detail")
        SummaryDS.Tables.Add("Consortium")
        SummaryDS.Tables.Add("PeriodType")

        SummaryDS.Tables.Add("BailiffName")
        SummaryDS.Tables.Add("BailiffType")
        SummaryDS.Tables.Add("StatusName")
        SummaryDS.Tables.Add("Levied")
        SummaryDS.Tables.Add("VanFeesApplied")
        SummaryDS.Tables.Add("PostcodeArea")

        LoadDataTable("EXEC dbo.GetPeriodTypes 'B'", SummaryDS, "PeriodType")
        PeriodTypeDV = New DataView(SummaryDS.Tables("PeriodType"))
        PeriodTypeDV.AllowDelete = False
        PeriodTypeDV.AllowNew = False
    End Sub

    Protected Overrides Sub Finalize()
        If Not IsNothing(LastLoadDV) Then LastLoadDV.Dispose()

        If Not IsNothing(GridDV) Then GridDV.Dispose()
        If Not IsNothing(DetailDV) Then DetailDV.Dispose()
        If Not IsNothing(PeriodTypeDV) Then PeriodTypeDV.Dispose()
        If Not IsNothing(BailiffNameDV) Then BailiffNameDV.Dispose()
        If Not IsNothing(BailiffTypeDV) Then BailiffTypeDV.Dispose()
        If Not IsNothing(StatusNameDV) Then StatusNameDV.Dispose()
        If Not IsNothing(LeviedDV) Then LeviedDV.Dispose()
        If Not IsNothing(VanFeesAppliedDV) Then VanFeesAppliedDV.Dispose()
        If Not IsNothing(PostcodeAreaDV) Then PostcodeAreaDV.Dispose()

        If Not IsNothing(SummaryDS) Then SummaryDS.Dispose() : SummaryDS = Nothing
        MyBase.Finalize()
    End Sub

#Region " Public Properties"

    Public ReadOnly Property SummaryDataSet() As DataSet
        Get
            SummaryDataSet = SummaryDS
        End Get
    End Property

    Public ReadOnly Property LastLoadDataView() As DataView
        Get
            LastLoadDataView = LastLoadDV
        End Get
    End Property

    Public ReadOnly Property GridDataView() As DataView
        Get
            GridDataView = GridDV
        End Get
    End Property

    Public ReadOnly Property DetailDataView() As DataView
        Get
            DetailDataView = DetailDV
        End Get
    End Property

    Public ReadOnly Property PeriodTypeDataView() As DataView
        Get
            PeriodTypeDataView = PeriodTypeDV
        End Get
    End Property

    Public ReadOnly Property BailiffNameDataView() As DataView
        Get
            BailiffNameDataView = BailiffNameDV
        End Get
    End Property

    Public ReadOnly Property BailiffTypeDataView() As DataView
        Get
            BailiffTypeDataView = BailiffTypeDV
        End Get
    End Property

    Public ReadOnly Property StatusNameDataView() As DataView
        Get
            StatusNameDataView = StatusNameDV
        End Get
    End Property

    Public ReadOnly Property LeviedDataView() As DataView
        Get
            LeviedDataView = LeviedDV
        End Get
    End Property

    Public ReadOnly Property VanFeesAppliedDataView() As DataView
        Get
            VanFeesAppliedDataView = VanFeesAppliedDV
        End Get
    End Property

    Public ReadOnly Property PostcodeAreaDataView() As DataView
        Get
            PostcodeAreaDataView = PostcodeAreaDV
        End Get
    End Property

#End Region

#Region "Public Methods"

    Public Sub GetSummary(ByVal ParamList As String, ByVal Abs As Boolean, ByVal DisplaySet As String)
        GridDV = Nothing ' Needed to clear column list otherwise new columns eg clientname are added at the end
        'If TopClients Then ParamList += ",1" Else ParamList += ",0"
        If Abs Then ParamList += ",'A'" Else ParamList += ",'P'"

        ParamList += "," & DisplaySet

        LoadDataTable("EXEC dbo.GetBailiffSummary " & ParamList, SummaryDS, "Grid")
        GridDV = New DataView(SummaryDS.Tables("Grid"))
        GridDV.AllowDelete = False
        GridDV.AllowNew = False
    End Sub

   

    'Public Sub GetCasesByBailiff(ByVal BailiffID)
    '    LoadDataTable("EXEC dbo.GetCasesByBailiff " & BailiffID.ToString, SummaryDS, "Detail", True)
    '    DetailDV = New DataView(SummaryDS.Tables("Detail"))
    '    DetailDV.AllowDelete = False
    '    DetailDV.AllowNew = False
    'End Sub

    Public Sub GetList(ByVal ParamList As String)

        LoadDataTable("EXEC dbo.GetBailiffList " & ParamList & ",'BailiffName'", SummaryDS, "BailiffName")
        BailiffNameDV = New DataView(SummaryDS.Tables("BailiffName"))
        BailiffNameDV.AllowDelete = False
        BailiffNameDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetBailiffList " & ParamList & ",'BailiffType'", SummaryDS, "BailiffType")
        BailiffTypeDV = New DataView(SummaryDS.Tables("BailiffType"))
        BailiffTypeDV.AllowDelete = False
        BailiffTypeDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetBailiffList " & ParamList & ",'StatusName'", SummaryDS, "StatusName")
        StatusNameDV = New DataView(SummaryDS.Tables("StatusName"))
        StatusNameDV.AllowDelete = False
        StatusNameDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetBailiffList " & ParamList & ",'Levied'", SummaryDS, "Levied")
        LeviedDV = New DataView(SummaryDS.Tables("Levied"))
        LeviedDV.AllowDelete = False
        LeviedDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetBailiffList " & ParamList & ",'VanFeesApplied'", SummaryDS, "VanFeesApplied")
        VanFeesAppliedDV = New DataView(SummaryDS.Tables("VanFeesApplied"))
        VanFeesAppliedDV.AllowDelete = False
        VanFeesAppliedDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetBailiffList " & ParamList & ",'PostcodeArea'", SummaryDS, "PostCodeArea")
        PostcodeAreaDV = New DataView(SummaryDS.Tables("PostCodeArea"))
        PostcodeAreaDV.AllowDelete = False
        PostcodeAreaDV.AllowNew = False
    End Sub
#End Region
End Class

﻿Module modCommon

    Public DataColumnsList() As String = {"Total", "Period1", "Period2", "Period3", "Period4", "Period5", "Period6", "Month1", "Month2", "Month3", "Month4", "Month5", "Month6", "Month7", "Month8", "Month9", "Month10", "Month11", "Month12", "YearPlus"} 'Used for validation within events

    Sub Main()
        Application.EnableVisualStyles()

        Select Case GetStartScreen()
            Case "C"
                frmCaseSummary.ShowDialog()
            Case "B"
                frmBailiffSummary.ShowDialog()
        End Select
    End Sub

    Public Sub PreRefresh(ByRef Control As Control)
        Control.Cursor = Cursors.WaitCursor
        Control.SuspendLayout()
    End Sub

    Public Sub PostRefresh(ByRef Control As Control)
        Control.ResumeLayout()
        Control.Cursor = Cursors.Default
    End Sub

    Public Sub FormatGridColumns(ByRef DataGridView As DataGridView, Optional ByVal StageList As String() = Nothing)
        If IsNothing(StageList) Then StageList = {}

        Dim PeriodNames As String() = GetPeriodNames(String.Join(vbTab, StageList))

        For Each Column As DataGridViewColumn In DataGridView.Columns
            Select Case Column.Name
                Case "StageName"
                    Column.Width = 140
                    Column.HeaderText = "Stage"
                Case "ClientName"
                    Column.Width = 170
                    Column.HeaderText = "Client"
                Case "WorkType"
                    Column.Width = 80
                    Column.HeaderText = "Work type"
                Case "SchemeName"
                    Column.Width = 100
                    Column.HeaderText = "Scheme"
                Case "Allocated"
                    Column.Width = 55
                Case "Levied"
                    Column.Width = 55
                Case "VanFeesApplied"
                    Column.Width = 65
                    Column.HeaderText = "Van Fees"
                Case "Payment"
                    Column.Width = 55
                Case "Visited"
                    Column.Width = 55
                Case "AddConfirmed"
                    Column.Width = 60
                    Column.HeaderText = "Confirmed"
                Case "ArrangementBroken"
                    Column.Width = 60
                    Column.HeaderText = "Broken"
                Case "Covered"
                    Column.Width = 90
                Case "BailiffName"
                    Column.Width = 120
                    Column.HeaderText = "Bailiff"
                Case "PostcodeArea"
                    If DataGridView.Name = "dgvSummary" Then
                        Column.Width = 55
                        Column.HeaderText = "Area"
                    Else
                        Column.Visible = False
                    End If
                Case "Total"
                    Column.Width = 55
                Case "Period1"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(0)
                Case "Period2"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(1)
                Case "Period3"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(2)
                Case "Period4"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(3)
                Case "Period5"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(4)
                Case "Period6"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(5)
                Case "Month1"
                    Column.Width = 55
                Case "Month2"
                    Column.Width = 55
                Case "Month3"
                    Column.Width = 55
                Case "Month4"
                    Column.Width = 55
                Case "Month5"
                    Column.Width = 55
                Case "Month6"
                    Column.Width = 55
                Case "Month7"
                    Column.Width = 55
                Case "Month8"
                    Column.Width = 55
                Case "Month9"
                    Column.Width = 55
                Case "Month10"
                    Column.Width = 55
                Case "Month11"
                    Column.Width = 55
                Case "Month12"
                    Column.Width = 55
                Case "YearPlus"
                    Column.Width = 55
                Case "DebtorID"
                    Column.Width = 70
                    Column.HeaderText = "Case ID"
                Case "LinkID"
                    Column.Width = 70
                    Column.HeaderText = "Link ID"
                Case "Warning"
                    Column.Width = 55
                Case "add_postcode"
                    Column.Width = 70
                    Column.HeaderText = "Postcode"
                Case "CreatedDate"
                    Column.Width = 105
                    Column.HeaderText = "Created date"
                Case "debt_balance"
                    Column.Width = 60
                    Column.HeaderText = "Balance"
                Case "FeesOutstanding"
                    Column.Width = 60
                    Column.HeaderText = "Os fees"
                Case "LastAction"
                    Column.Width = 105
                    Column.HeaderText = "Last action"
                Case "BailiffID"
                    Column.Visible = False
                Case "name_fore"
                    Column.Width = 70
                    Column.HeaderText = "Forename"
                Case "name_sur"
                    Column.Width = 70
                    Column.HeaderText = "Surname"
                Case "BailiffType"
                    Column.Width = 60
                    Column.HeaderText = "Type"
                Case "CasesAllocated"
                    Column.Width = 60
                    Column.HeaderText = "Allocated"
                Case "MaxCases"
                    Column.Width = 50
                    Column.HeaderText = "Max"
                Case "LastAllocation"
                    Column.HeaderText = "Last Allocation"
                Case "BailiffAllocatedDate"
                    Column.Width = 70
                    Column.HeaderText = "Allocated"
                Case "LODate"
                    Column.Width = 70
                    Column.HeaderText = "LO date"
                Case "ResidencyScore"
                    Column.Width = 80
                    Column.HeaderText = "Res score"
                Case "add_os_easting"
                    Column.Visible = False
                Case "add_os_northing"
                    Column.Visible = False
                Case "LinkedCase"
                    Column.Visible = False
            End Select

        Next Column
    End Sub

    Public Function FormOpen(ByVal Formname As String) As Boolean
        FormOpen = False

        For Each frm As Form In Application.OpenForms
            If Formname = frm.Name Then FormOpen = True
        Next

    End Function
End Module

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCaseSummary
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvSummary = New System.Windows.Forms.DataGridView()
        Me.dgvStageName = New System.Windows.Forms.DataGridView()
        Me.StageName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colStageNameTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvClientName = New System.Windows.Forms.DataGridView()
        Me.ClientName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colClientNameTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvPayment = New System.Windows.Forms.DataGridView()
        Me.Payment = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvLevied = New System.Windows.Forms.DataGridView()
        Me.Levied = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvVisited = New System.Windows.Forms.DataGridView()
        Me.Visited = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvAllocated = New System.Windows.Forms.DataGridView()
        Me.dgvSchemeName = New System.Windows.Forms.DataGridView()
        Me.SchemeName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdStageAll = New System.Windows.Forms.Button()
        Me.cmdStageClear = New System.Windows.Forms.Button()
        Me.cmdAllocatedClear = New System.Windows.Forms.Button()
        Me.cmdAllocatedAll = New System.Windows.Forms.Button()
        Me.cmdClientClear = New System.Windows.Forms.Button()
        Me.cmdClientAll = New System.Windows.Forms.Button()
        Me.cmdSchemeClear = New System.Windows.Forms.Button()
        Me.cmdSchemeAll = New System.Windows.Forms.Button()
        Me.cmdLeviedClear = New System.Windows.Forms.Button()
        Me.cmdLeviedAll = New System.Windows.Forms.Button()
        Me.cmdVisitedClear = New System.Windows.Forms.Button()
        Me.cmdVisitedAll = New System.Windows.Forms.Button()
        Me.cmdPaymentClear = New System.Windows.Forms.Button()
        Me.cmdPaymentAll = New System.Windows.Forms.Button()
        Me.cmdAddConfirmedClear = New System.Windows.Forms.Button()
        Me.cmdAddConfirmedAll = New System.Windows.Forms.Button()
        Me.dgvAddConfirmed = New System.Windows.Forms.DataGridView()
        Me.AddConfirmed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdClearAll = New System.Windows.Forms.Button()
        Me.lblLine1 = New System.Windows.Forms.Label()
        Me.pnlAbsPerc = New System.Windows.Forms.Panel()
        Me.radPerc = New System.Windows.Forms.RadioButton()
        Me.radAbs = New System.Windows.Forms.RadioButton()
        Me.chkTopClients = New System.Windows.Forms.CheckBox()
        Me.dgvWorkType = New System.Windows.Forms.DataGridView()
        Me.cmdWorkTypeClear = New System.Windows.Forms.Button()
        Me.cmdWorkTypeAll = New System.Windows.Forms.Button()
        Me.cmdRefreshDB = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmsSummary = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmdCoveredClear = New System.Windows.Forms.Button()
        Me.cmdCoveredAll = New System.Windows.Forms.Button()
        Me.dgvCovered = New System.Windows.Forms.DataGridView()
        Me.Covered = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bxtnCoveredAll = New System.Windows.Forms.Button()
        Me.dgvPostcodeArea = New System.Windows.Forms.DataGridView()
        Me.PostcodeArea = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdPostcodeAreaClear = New System.Windows.Forms.Button()
        Me.cmdPostcodeAreaAll = New System.Windows.Forms.Button()
        Me.cmsList = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cboPeriodType = New System.Windows.Forms.ComboBox()
        Me.lblPeriodType = New System.Windows.Forms.Label()
        Me.cboDisplaySet = New System.Windows.Forms.ComboBox()
        Me.cmsForm = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.lblSummary = New System.Windows.Forms.Label()
        Me.cmdArrangementBrokenClear = New System.Windows.Forms.Button()
        Me.cmdArrangementBrokenAll = New System.Windows.Forms.Button()
        Me.dgvArrangementBroken = New System.Windows.Forms.DataGridView()
        Me.ArrangementBroken = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvVanFeesApplied = New System.Windows.Forms.DataGridView()
        Me.VanFeesApplied = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdVanFeesAppliedClear = New System.Windows.Forms.Button()
        Me.cmdVanFeesAppliedAll = New System.Windows.Forms.Button()
        Me.WorkType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Allocated = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvStageName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLevied, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvVisited, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAllocated, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSchemeName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAddConfirmed, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAbsPerc.SuspendLayout()
        CType(Me.dgvWorkType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCovered, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvArrangementBroken, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvVanFeesApplied, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvSummary
        '
        Me.dgvSummary.AllowUserToAddRows = False
        Me.dgvSummary.AllowUserToDeleteRows = False
        Me.dgvSummary.AllowUserToOrderColumns = True
        Me.dgvSummary.AllowUserToResizeRows = False
        Me.dgvSummary.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSummary.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvSummary.Location = New System.Drawing.Point(7, 367)
        Me.dgvSummary.Name = "dgvSummary"
        Me.dgvSummary.ReadOnly = True
        Me.dgvSummary.RowHeadersVisible = False
        Me.dgvSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvSummary.Size = New System.Drawing.Size(1007, 205)
        Me.dgvSummary.TabIndex = 0
        '
        'dgvStageName
        '
        Me.dgvStageName.AllowUserToAddRows = False
        Me.dgvStageName.AllowUserToDeleteRows = False
        Me.dgvStageName.AllowUserToResizeColumns = False
        Me.dgvStageName.AllowUserToResizeRows = False
        Me.dgvStageName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStageName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StageName, Me.colStageNameTotal})
        Me.dgvStageName.Location = New System.Drawing.Point(7, 12)
        Me.dgvStageName.Name = "dgvStageName"
        Me.dgvStageName.ReadOnly = True
        Me.dgvStageName.RowHeadersVisible = False
        Me.dgvStageName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvStageName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvStageName.Size = New System.Drawing.Size(160, 292)
        Me.dgvStageName.TabIndex = 1
        '
        'StageName
        '
        Me.StageName.DataPropertyName = "StageName"
        Me.StageName.HeaderText = "Stage"
        Me.StageName.Name = "StageName"
        Me.StageName.ReadOnly = True
        Me.StageName.Width = 90
        '
        'colStageNameTotal
        '
        Me.colStageNameTotal.DataPropertyName = "Total"
        Me.colStageNameTotal.HeaderText = "Total"
        Me.colStageNameTotal.Name = "colStageNameTotal"
        Me.colStageNameTotal.ReadOnly = True
        Me.colStageNameTotal.Width = 40
        '
        'dgvClientName
        '
        Me.dgvClientName.AllowUserToAddRows = False
        Me.dgvClientName.AllowUserToDeleteRows = False
        Me.dgvClientName.AllowUserToResizeColumns = False
        Me.dgvClientName.AllowUserToResizeRows = False
        Me.dgvClientName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClientName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClientName, Me.colClientNameTotal})
        Me.dgvClientName.Location = New System.Drawing.Point(174, 12)
        Me.dgvClientName.Name = "dgvClientName"
        Me.dgvClientName.ReadOnly = True
        Me.dgvClientName.RowHeadersVisible = False
        Me.dgvClientName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClientName.Size = New System.Drawing.Size(160, 292)
        Me.dgvClientName.TabIndex = 2
        '
        'ClientName
        '
        Me.ClientName.DataPropertyName = "ClientName"
        Me.ClientName.HeaderText = "Client"
        Me.ClientName.Name = "ClientName"
        Me.ClientName.ReadOnly = True
        '
        'colClientNameTotal
        '
        Me.colClientNameTotal.DataPropertyName = "Total"
        Me.colClientNameTotal.HeaderText = "Total"
        Me.colClientNameTotal.Name = "colClientNameTotal"
        Me.colClientNameTotal.ReadOnly = True
        Me.colClientNameTotal.Width = 40
        '
        'dgvPayment
        '
        Me.dgvPayment.AllowUserToAddRows = False
        Me.dgvPayment.AllowUserToDeleteRows = False
        Me.dgvPayment.AllowUserToResizeColumns = False
        Me.dgvPayment.AllowUserToResizeRows = False
        Me.dgvPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPayment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Payment, Me.DataGridViewTextBoxColumn2})
        Me.dgvPayment.Location = New System.Drawing.Point(640, 12)
        Me.dgvPayment.Name = "dgvPayment"
        Me.dgvPayment.ReadOnly = True
        Me.dgvPayment.RowHeadersVisible = False
        Me.dgvPayment.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvPayment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPayment.Size = New System.Drawing.Size(112, 65)
        Me.dgvPayment.TabIndex = 3
        '
        'Payment
        '
        Me.Payment.DataPropertyName = "Payment"
        Me.Payment.HeaderText = "Payment"
        Me.Payment.Name = "Payment"
        Me.Payment.ReadOnly = True
        Me.Payment.Width = 60
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 40
        '
        'dgvLevied
        '
        Me.dgvLevied.AllowUserToAddRows = False
        Me.dgvLevied.AllowUserToDeleteRows = False
        Me.dgvLevied.AllowUserToResizeColumns = False
        Me.dgvLevied.AllowUserToResizeRows = False
        Me.dgvLevied.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLevied.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Levied, Me.DataGridViewTextBoxColumn4})
        Me.dgvLevied.Location = New System.Drawing.Point(881, 148)
        Me.dgvLevied.Name = "dgvLevied"
        Me.dgvLevied.ReadOnly = True
        Me.dgvLevied.RowHeadersVisible = False
        Me.dgvLevied.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvLevied.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLevied.Size = New System.Drawing.Size(132, 65)
        Me.dgvLevied.TabIndex = 4
        '
        'Levied
        '
        Me.Levied.DataPropertyName = "Levied"
        Me.Levied.HeaderText = "Levied"
        Me.Levied.Name = "Levied"
        Me.Levied.ReadOnly = True
        Me.Levied.Width = 60
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 40
        '
        'dgvVisited
        '
        Me.dgvVisited.AllowUserToAddRows = False
        Me.dgvVisited.AllowUserToDeleteRows = False
        Me.dgvVisited.AllowUserToResizeColumns = False
        Me.dgvVisited.AllowUserToResizeRows = False
        Me.dgvVisited.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVisited.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Visited, Me.DataGridViewTextBoxColumn6})
        Me.dgvVisited.Location = New System.Drawing.Point(640, 148)
        Me.dgvVisited.Name = "dgvVisited"
        Me.dgvVisited.ReadOnly = True
        Me.dgvVisited.RowHeadersVisible = False
        Me.dgvVisited.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvVisited.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVisited.Size = New System.Drawing.Size(112, 65)
        Me.dgvVisited.TabIndex = 6
        '
        'Visited
        '
        Me.Visited.DataPropertyName = "Visited"
        Me.Visited.HeaderText = "Visited"
        Me.Visited.Name = "Visited"
        Me.Visited.ReadOnly = True
        Me.Visited.Width = 60
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 40
        '
        'dgvAllocated
        '
        Me.dgvAllocated.AllowUserToAddRows = False
        Me.dgvAllocated.AllowUserToDeleteRows = False
        Me.dgvAllocated.AllowUserToResizeColumns = False
        Me.dgvAllocated.AllowUserToResizeRows = False
        Me.dgvAllocated.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAllocated.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Allocated, Me.DataGridViewTextBoxColumn8})
        Me.dgvAllocated.Location = New System.Drawing.Point(341, 239)
        Me.dgvAllocated.Name = "dgvAllocated"
        Me.dgvAllocated.ReadOnly = True
        Me.dgvAllocated.RowHeadersVisible = False
        Me.dgvAllocated.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvAllocated.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAllocated.Size = New System.Drawing.Size(126, 65)
        Me.dgvAllocated.TabIndex = 5
        '
        'dgvSchemeName
        '
        Me.dgvSchemeName.AllowUserToAddRows = False
        Me.dgvSchemeName.AllowUserToDeleteRows = False
        Me.dgvSchemeName.AllowUserToResizeColumns = False
        Me.dgvSchemeName.AllowUserToResizeRows = False
        Me.dgvSchemeName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSchemeName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SchemeName, Me.Total})
        Me.dgvSchemeName.Location = New System.Drawing.Point(474, 12)
        Me.dgvSchemeName.Name = "dgvSchemeName"
        Me.dgvSchemeName.ReadOnly = True
        Me.dgvSchemeName.RowHeadersVisible = False
        Me.dgvSchemeName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSchemeName.Size = New System.Drawing.Size(160, 201)
        Me.dgvSchemeName.TabIndex = 7
        '
        'SchemeName
        '
        Me.SchemeName.DataPropertyName = "SchemeName"
        Me.SchemeName.HeaderText = "Scheme"
        Me.SchemeName.Name = "SchemeName"
        Me.SchemeName.ReadOnly = True
        '
        'Total
        '
        Me.Total.DataPropertyName = "Total"
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        Me.Total.Width = 40
        '
        'cmdStageAll
        '
        Me.cmdStageAll.Location = New System.Drawing.Point(30, 304)
        Me.cmdStageAll.Name = "cmdStageAll"
        Me.cmdStageAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageAll.TabIndex = 8
        Me.cmdStageAll.Text = "All"
        Me.cmdStageAll.UseVisualStyleBackColor = True
        '
        'cmdStageClear
        '
        Me.cmdStageClear.Location = New System.Drawing.Point(92, 304)
        Me.cmdStageClear.Name = "cmdStageClear"
        Me.cmdStageClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageClear.TabIndex = 9
        Me.cmdStageClear.Text = "Clear"
        Me.cmdStageClear.UseVisualStyleBackColor = True
        '
        'cmdAllocatedClear
        '
        Me.cmdAllocatedClear.Location = New System.Drawing.Point(409, 304)
        Me.cmdAllocatedClear.Name = "cmdAllocatedClear"
        Me.cmdAllocatedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdAllocatedClear.TabIndex = 11
        Me.cmdAllocatedClear.Text = "Clear"
        Me.cmdAllocatedClear.UseVisualStyleBackColor = True
        '
        'cmdAllocatedAll
        '
        Me.cmdAllocatedAll.Location = New System.Drawing.Point(347, 304)
        Me.cmdAllocatedAll.Name = "cmdAllocatedAll"
        Me.cmdAllocatedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdAllocatedAll.TabIndex = 10
        Me.cmdAllocatedAll.Text = "All"
        Me.cmdAllocatedAll.UseVisualStyleBackColor = True
        '
        'cmdClientClear
        '
        Me.cmdClientClear.Location = New System.Drawing.Point(259, 304)
        Me.cmdClientClear.Name = "cmdClientClear"
        Me.cmdClientClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientClear.TabIndex = 13
        Me.cmdClientClear.Text = "Clear"
        Me.cmdClientClear.UseVisualStyleBackColor = True
        '
        'cmdClientAll
        '
        Me.cmdClientAll.Location = New System.Drawing.Point(197, 304)
        Me.cmdClientAll.Name = "cmdClientAll"
        Me.cmdClientAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientAll.TabIndex = 12
        Me.cmdClientAll.Text = "All"
        Me.cmdClientAll.UseVisualStyleBackColor = True
        '
        'cmdSchemeClear
        '
        Me.cmdSchemeClear.Location = New System.Drawing.Point(560, 213)
        Me.cmdSchemeClear.Name = "cmdSchemeClear"
        Me.cmdSchemeClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdSchemeClear.TabIndex = 15
        Me.cmdSchemeClear.Text = "Clear"
        Me.cmdSchemeClear.UseVisualStyleBackColor = True
        '
        'cmdSchemeAll
        '
        Me.cmdSchemeAll.Location = New System.Drawing.Point(498, 213)
        Me.cmdSchemeAll.Name = "cmdSchemeAll"
        Me.cmdSchemeAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdSchemeAll.TabIndex = 14
        Me.cmdSchemeAll.Text = "All"
        Me.cmdSchemeAll.UseVisualStyleBackColor = True
        '
        'cmdLeviedClear
        '
        Me.cmdLeviedClear.Location = New System.Drawing.Point(953, 213)
        Me.cmdLeviedClear.Name = "cmdLeviedClear"
        Me.cmdLeviedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdLeviedClear.TabIndex = 17
        Me.cmdLeviedClear.Text = "Clear"
        Me.cmdLeviedClear.UseVisualStyleBackColor = True
        '
        'cmdLeviedAll
        '
        Me.cmdLeviedAll.Location = New System.Drawing.Point(891, 213)
        Me.cmdLeviedAll.Name = "cmdLeviedAll"
        Me.cmdLeviedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdLeviedAll.TabIndex = 16
        Me.cmdLeviedAll.Text = "All"
        Me.cmdLeviedAll.UseVisualStyleBackColor = True
        '
        'cmdVisitedClear
        '
        Me.cmdVisitedClear.Location = New System.Drawing.Point(702, 213)
        Me.cmdVisitedClear.Name = "cmdVisitedClear"
        Me.cmdVisitedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdVisitedClear.TabIndex = 19
        Me.cmdVisitedClear.Text = "Clear"
        Me.cmdVisitedClear.UseVisualStyleBackColor = True
        '
        'cmdVisitedAll
        '
        Me.cmdVisitedAll.Location = New System.Drawing.Point(640, 213)
        Me.cmdVisitedAll.Name = "cmdVisitedAll"
        Me.cmdVisitedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdVisitedAll.TabIndex = 18
        Me.cmdVisitedAll.Text = "All"
        Me.cmdVisitedAll.UseVisualStyleBackColor = True
        '
        'cmdPaymentClear
        '
        Me.cmdPaymentClear.Location = New System.Drawing.Point(702, 77)
        Me.cmdPaymentClear.Name = "cmdPaymentClear"
        Me.cmdPaymentClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPaymentClear.TabIndex = 21
        Me.cmdPaymentClear.Text = "Clear"
        Me.cmdPaymentClear.UseVisualStyleBackColor = True
        '
        'cmdPaymentAll
        '
        Me.cmdPaymentAll.Location = New System.Drawing.Point(640, 77)
        Me.cmdPaymentAll.Name = "cmdPaymentAll"
        Me.cmdPaymentAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPaymentAll.TabIndex = 20
        Me.cmdPaymentAll.Text = "All"
        Me.cmdPaymentAll.UseVisualStyleBackColor = True
        '
        'cmdAddConfirmedClear
        '
        Me.cmdAddConfirmedClear.Location = New System.Drawing.Point(702, 304)
        Me.cmdAddConfirmedClear.Name = "cmdAddConfirmedClear"
        Me.cmdAddConfirmedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdAddConfirmedClear.TabIndex = 24
        Me.cmdAddConfirmedClear.Text = "Clear"
        Me.cmdAddConfirmedClear.UseVisualStyleBackColor = True
        '
        'cmdAddConfirmedAll
        '
        Me.cmdAddConfirmedAll.Location = New System.Drawing.Point(640, 304)
        Me.cmdAddConfirmedAll.Name = "cmdAddConfirmedAll"
        Me.cmdAddConfirmedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdAddConfirmedAll.TabIndex = 23
        Me.cmdAddConfirmedAll.Text = "All"
        Me.cmdAddConfirmedAll.UseVisualStyleBackColor = True
        '
        'dgvAddConfirmed
        '
        Me.dgvAddConfirmed.AllowUserToAddRows = False
        Me.dgvAddConfirmed.AllowUserToDeleteRows = False
        Me.dgvAddConfirmed.AllowUserToResizeColumns = False
        Me.dgvAddConfirmed.AllowUserToResizeRows = False
        Me.dgvAddConfirmed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAddConfirmed.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AddConfirmed, Me.DataGridViewTextBoxColumn3})
        Me.dgvAddConfirmed.Location = New System.Drawing.Point(640, 239)
        Me.dgvAddConfirmed.Name = "dgvAddConfirmed"
        Me.dgvAddConfirmed.ReadOnly = True
        Me.dgvAddConfirmed.RowHeadersVisible = False
        Me.dgvAddConfirmed.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvAddConfirmed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAddConfirmed.Size = New System.Drawing.Size(113, 65)
        Me.dgvAddConfirmed.TabIndex = 22
        '
        'AddConfirmed
        '
        Me.AddConfirmed.DataPropertyName = "AddConfirmed"
        Me.AddConfirmed.HeaderText = "Confirmed"
        Me.AddConfirmed.Name = "AddConfirmed"
        Me.AddConfirmed.ReadOnly = True
        Me.AddConfirmed.Width = 60
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 40
        '
        'cmdClearAll
        '
        Me.cmdClearAll.Location = New System.Drawing.Point(946, 337)
        Me.cmdClearAll.Name = "cmdClearAll"
        Me.cmdClearAll.Size = New System.Drawing.Size(68, 20)
        Me.cmdClearAll.TabIndex = 25
        Me.cmdClearAll.Text = "Clear All"
        Me.cmdClearAll.UseVisualStyleBackColor = True
        '
        'lblLine1
        '
        Me.lblLine1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLine1.Location = New System.Drawing.Point(7, 329)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(1007, 1)
        Me.lblLine1.TabIndex = 26
        '
        'pnlAbsPerc
        '
        Me.pnlAbsPerc.Controls.Add(Me.radPerc)
        Me.pnlAbsPerc.Controls.Add(Me.radAbs)
        Me.pnlAbsPerc.Location = New System.Drawing.Point(839, 336)
        Me.pnlAbsPerc.Name = "pnlAbsPerc"
        Me.pnlAbsPerc.Size = New System.Drawing.Size(99, 25)
        Me.pnlAbsPerc.TabIndex = 29
        '
        'radPerc
        '
        Me.radPerc.AutoSize = True
        Me.radPerc.Location = New System.Drawing.Point(55, 4)
        Me.radPerc.Name = "radPerc"
        Me.radPerc.Size = New System.Drawing.Size(33, 17)
        Me.radPerc.TabIndex = 4
        Me.radPerc.TabStop = True
        Me.radPerc.Text = "%"
        Me.radPerc.UseVisualStyleBackColor = True
        '
        'radAbs
        '
        Me.radAbs.AutoSize = True
        Me.radAbs.Location = New System.Drawing.Point(6, 4)
        Me.radAbs.Name = "radAbs"
        Me.radAbs.Size = New System.Drawing.Size(43, 17)
        Me.radAbs.TabIndex = 3
        Me.radAbs.TabStop = True
        Me.radAbs.Text = "Abs"
        Me.radAbs.UseVisualStyleBackColor = True
        '
        'chkTopClients
        '
        Me.chkTopClients.AutoSize = True
        Me.chkTopClients.Location = New System.Drawing.Point(202, 341)
        Me.chkTopClients.Name = "chkTopClients"
        Me.chkTopClients.Size = New System.Drawing.Size(79, 17)
        Me.chkTopClients.TabIndex = 30
        Me.chkTopClients.Text = "Top Clients"
        Me.chkTopClients.UseVisualStyleBackColor = True
        '
        'dgvWorkType
        '
        Me.dgvWorkType.AllowUserToAddRows = False
        Me.dgvWorkType.AllowUserToDeleteRows = False
        Me.dgvWorkType.AllowUserToResizeColumns = False
        Me.dgvWorkType.AllowUserToResizeRows = False
        Me.dgvWorkType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvWorkType.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WorkType, Me.DataGridViewTextBoxColumn5})
        Me.dgvWorkType.Location = New System.Drawing.Point(341, 12)
        Me.dgvWorkType.Name = "dgvWorkType"
        Me.dgvWorkType.ReadOnly = True
        Me.dgvWorkType.RowHeadersVisible = False
        Me.dgvWorkType.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvWorkType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvWorkType.Size = New System.Drawing.Size(126, 154)
        Me.dgvWorkType.TabIndex = 31
        '
        'cmdWorkTypeClear
        '
        Me.cmdWorkTypeClear.Location = New System.Drawing.Point(409, 166)
        Me.cmdWorkTypeClear.Name = "cmdWorkTypeClear"
        Me.cmdWorkTypeClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdWorkTypeClear.TabIndex = 33
        Me.cmdWorkTypeClear.Text = "Clear"
        Me.cmdWorkTypeClear.UseVisualStyleBackColor = True
        '
        'cmdWorkTypeAll
        '
        Me.cmdWorkTypeAll.Location = New System.Drawing.Point(347, 166)
        Me.cmdWorkTypeAll.Name = "cmdWorkTypeAll"
        Me.cmdWorkTypeAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdWorkTypeAll.TabIndex = 32
        Me.cmdWorkTypeAll.Text = "All"
        Me.cmdWorkTypeAll.UseVisualStyleBackColor = True
        '
        'cmdRefreshDB
        '
        Me.cmdRefreshDB.Location = New System.Drawing.Point(7, 338)
        Me.cmdRefreshDB.Name = "cmdRefreshDB"
        Me.cmdRefreshDB.Size = New System.Drawing.Size(75, 20)
        Me.cmdRefreshDB.TabIndex = 34
        Me.cmdRefreshDB.Text = "Refresh DB"
        Me.cmdRefreshDB.UseVisualStyleBackColor = True
        '
        'cmsSummary
        '
        Me.cmsSummary.Name = "ContextMenuStrip1"
        Me.cmsSummary.Size = New System.Drawing.Size(61, 4)
        '
        'cmdCoveredClear
        '
        Me.cmdCoveredClear.Location = New System.Drawing.Point(953, 122)
        Me.cmdCoveredClear.Name = "cmdCoveredClear"
        Me.cmdCoveredClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdCoveredClear.TabIndex = 37
        Me.cmdCoveredClear.Text = "Clear"
        Me.cmdCoveredClear.UseVisualStyleBackColor = True
        '
        'cmdCoveredAll
        '
        Me.cmdCoveredAll.Location = New System.Drawing.Point(891, 122)
        Me.cmdCoveredAll.Name = "cmdCoveredAll"
        Me.cmdCoveredAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdCoveredAll.TabIndex = 36
        Me.cmdCoveredAll.Text = "All"
        Me.cmdCoveredAll.UseVisualStyleBackColor = True
        '
        'dgvCovered
        '
        Me.dgvCovered.AllowUserToAddRows = False
        Me.dgvCovered.AllowUserToDeleteRows = False
        Me.dgvCovered.AllowUserToResizeColumns = False
        Me.dgvCovered.AllowUserToResizeRows = False
        Me.dgvCovered.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCovered.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Covered, Me.DataGridViewTextBoxColumn7})
        Me.dgvCovered.Location = New System.Drawing.Point(881, 12)
        Me.dgvCovered.Name = "dgvCovered"
        Me.dgvCovered.ReadOnly = True
        Me.dgvCovered.RowHeadersVisible = False
        Me.dgvCovered.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvCovered.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCovered.Size = New System.Drawing.Size(132, 110)
        Me.dgvCovered.TabIndex = 35
        '
        'Covered
        '
        Me.Covered.DataPropertyName = "Covered"
        Me.Covered.HeaderText = "Covered"
        Me.Covered.Name = "Covered"
        Me.Covered.ReadOnly = True
        Me.Covered.Width = 80
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 40
        '
        'bxtnCoveredAll
        '
        Me.bxtnCoveredAll.Location = New System.Drawing.Point(360, 160)
        Me.bxtnCoveredAll.Name = "bxtnCoveredAll"
        Me.bxtnCoveredAll.Size = New System.Drawing.Size(50, 20)
        Me.bxtnCoveredAll.TabIndex = 36
        Me.bxtnCoveredAll.Text = "All"
        Me.bxtnCoveredAll.UseVisualStyleBackColor = True
        '
        'dgvPostcodeArea
        '
        Me.dgvPostcodeArea.AllowUserToAddRows = False
        Me.dgvPostcodeArea.AllowUserToDeleteRows = False
        Me.dgvPostcodeArea.AllowUserToResizeColumns = False
        Me.dgvPostcodeArea.AllowUserToResizeRows = False
        Me.dgvPostcodeArea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPostcodeArea.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PostcodeArea, Me.DataGridViewTextBoxColumn9})
        Me.dgvPostcodeArea.Location = New System.Drawing.Point(759, 12)
        Me.dgvPostcodeArea.Name = "dgvPostcodeArea"
        Me.dgvPostcodeArea.ReadOnly = True
        Me.dgvPostcodeArea.RowHeadersVisible = False
        Me.dgvPostcodeArea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPostcodeArea.Size = New System.Drawing.Size(115, 292)
        Me.dgvPostcodeArea.TabIndex = 38
        '
        'PostcodeArea
        '
        Me.PostcodeArea.DataPropertyName = "PostcodeArea"
        Me.PostcodeArea.HeaderText = "Area"
        Me.PostcodeArea.Name = "PostcodeArea"
        Me.PostcodeArea.ReadOnly = True
        Me.PostcodeArea.Width = 55
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 40
        '
        'cmdPostcodeAreaClear
        '
        Me.cmdPostcodeAreaClear.Location = New System.Drawing.Point(823, 304)
        Me.cmdPostcodeAreaClear.Name = "cmdPostcodeAreaClear"
        Me.cmdPostcodeAreaClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaClear.TabIndex = 40
        Me.cmdPostcodeAreaClear.Text = "Clear"
        Me.cmdPostcodeAreaClear.UseVisualStyleBackColor = True
        '
        'cmdPostcodeAreaAll
        '
        Me.cmdPostcodeAreaAll.Location = New System.Drawing.Point(761, 304)
        Me.cmdPostcodeAreaAll.Name = "cmdPostcodeAreaAll"
        Me.cmdPostcodeAreaAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaAll.TabIndex = 39
        Me.cmdPostcodeAreaAll.Text = "All"
        Me.cmdPostcodeAreaAll.UseVisualStyleBackColor = True
        '
        'cmsList
        '
        Me.cmsList.Name = "ContextMenuStrip1"
        Me.cmsList.Size = New System.Drawing.Size(61, 4)
        '
        'cboPeriodType
        '
        Me.cboPeriodType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriodType.FormattingEnabled = True
        Me.cboPeriodType.Location = New System.Drawing.Point(717, 336)
        Me.cboPeriodType.Name = "cboPeriodType"
        Me.cboPeriodType.Size = New System.Drawing.Size(92, 21)
        Me.cboPeriodType.TabIndex = 41
        '
        'lblPeriodType
        '
        Me.lblPeriodType.Location = New System.Drawing.Point(592, 341)
        Me.lblPeriodType.Name = "lblPeriodType"
        Me.lblPeriodType.Size = New System.Drawing.Size(119, 16)
        Me.lblPeriodType.TabIndex = 42
        Me.lblPeriodType.Text = "based on:"
        Me.lblPeriodType.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboDisplaySet
        '
        Me.cboDisplaySet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisplaySet.FormattingEnabled = True
        Me.cboDisplaySet.Items.AddRange(New Object() {"Months", "Periods"})
        Me.cboDisplaySet.Location = New System.Drawing.Point(592, 336)
        Me.cboDisplaySet.Name = "cboDisplaySet"
        Me.cboDisplaySet.Size = New System.Drawing.Size(59, 21)
        Me.cboDisplaySet.TabIndex = 43
        '
        'cmsForm
        '
        Me.cmsForm.Name = "ContextMenuStrip1"
        Me.cmsForm.Size = New System.Drawing.Size(61, 4)
        '
        'lblSummary
        '
        Me.lblSummary.AutoSize = True
        Me.lblSummary.Location = New System.Drawing.Point(88, 342)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(39, 13)
        Me.lblSummary.TabIndex = 91
        Me.lblSummary.Text = "Label1"
        '
        'cmdArrangementBrokenClear
        '
        Me.cmdArrangementBrokenClear.Location = New System.Drawing.Point(953, 304)
        Me.cmdArrangementBrokenClear.Name = "cmdArrangementBrokenClear"
        Me.cmdArrangementBrokenClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdArrangementBrokenClear.TabIndex = 94
        Me.cmdArrangementBrokenClear.Text = "Clear"
        Me.cmdArrangementBrokenClear.UseVisualStyleBackColor = True
        '
        'cmdArrangementBrokenAll
        '
        Me.cmdArrangementBrokenAll.Location = New System.Drawing.Point(891, 304)
        Me.cmdArrangementBrokenAll.Name = "cmdArrangementBrokenAll"
        Me.cmdArrangementBrokenAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdArrangementBrokenAll.TabIndex = 93
        Me.cmdArrangementBrokenAll.Text = "All"
        Me.cmdArrangementBrokenAll.UseVisualStyleBackColor = True
        '
        'dgvArrangementBroken
        '
        Me.dgvArrangementBroken.AllowUserToAddRows = False
        Me.dgvArrangementBroken.AllowUserToDeleteRows = False
        Me.dgvArrangementBroken.AllowUserToResizeColumns = False
        Me.dgvArrangementBroken.AllowUserToResizeRows = False
        Me.dgvArrangementBroken.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvArrangementBroken.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ArrangementBroken, Me.DataGridViewTextBoxColumn10})
        Me.dgvArrangementBroken.Location = New System.Drawing.Point(881, 239)
        Me.dgvArrangementBroken.Name = "dgvArrangementBroken"
        Me.dgvArrangementBroken.ReadOnly = True
        Me.dgvArrangementBroken.RowHeadersVisible = False
        Me.dgvArrangementBroken.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvArrangementBroken.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvArrangementBroken.Size = New System.Drawing.Size(132, 65)
        Me.dgvArrangementBroken.TabIndex = 92
        '
        'ArrangementBroken
        '
        Me.ArrangementBroken.DataPropertyName = "ArrangementBroken"
        Me.ArrangementBroken.HeaderText = "Broken"
        Me.ArrangementBroken.Name = "ArrangementBroken"
        Me.ArrangementBroken.ReadOnly = True
        Me.ArrangementBroken.Width = 60
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 40
        '
        'dgvVanFeesApplied
        '
        Me.dgvVanFeesApplied.AllowUserToAddRows = False
        Me.dgvVanFeesApplied.AllowUserToDeleteRows = False
        Me.dgvVanFeesApplied.AllowUserToResizeColumns = False
        Me.dgvVanFeesApplied.AllowUserToResizeRows = False
        Me.dgvVanFeesApplied.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVanFeesApplied.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.VanFeesApplied, Me.DataGridViewTextBoxColumn11})
        Me.dgvVanFeesApplied.Location = New System.Drawing.Point(474, 239)
        Me.dgvVanFeesApplied.Name = "dgvVanFeesApplied"
        Me.dgvVanFeesApplied.ReadOnly = True
        Me.dgvVanFeesApplied.RowHeadersVisible = False
        Me.dgvVanFeesApplied.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvVanFeesApplied.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVanFeesApplied.Size = New System.Drawing.Size(160, 65)
        Me.dgvVanFeesApplied.TabIndex = 95
        '
        'VanFeesApplied
        '
        Me.VanFeesApplied.DataPropertyName = "VanFeesApplied"
        Me.VanFeesApplied.HeaderText = "Van Fees"
        Me.VanFeesApplied.Name = "VanFeesApplied"
        Me.VanFeesApplied.ReadOnly = True
        Me.VanFeesApplied.Width = 120
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 40
        '
        'cmdVanFeesAppliedClear
        '
        Me.cmdVanFeesAppliedClear.Location = New System.Drawing.Point(560, 304)
        Me.cmdVanFeesAppliedClear.Name = "cmdVanFeesAppliedClear"
        Me.cmdVanFeesAppliedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdVanFeesAppliedClear.TabIndex = 97
        Me.cmdVanFeesAppliedClear.Text = "Clear"
        Me.cmdVanFeesAppliedClear.UseVisualStyleBackColor = True
        '
        'cmdVanFeesAppliedAll
        '
        Me.cmdVanFeesAppliedAll.Location = New System.Drawing.Point(498, 304)
        Me.cmdVanFeesAppliedAll.Name = "cmdVanFeesAppliedAll"
        Me.cmdVanFeesAppliedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdVanFeesAppliedAll.TabIndex = 96
        Me.cmdVanFeesAppliedAll.Text = "All"
        Me.cmdVanFeesAppliedAll.UseVisualStyleBackColor = True
        '
        'WorkType
        '
        Me.WorkType.DataPropertyName = "WorkType"
        Me.WorkType.HeaderText = "Work Type"
        Me.WorkType.Name = "WorkType"
        Me.WorkType.ReadOnly = True
        Me.WorkType.Width = 84
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 40
        '
        'Allocated
        '
        Me.Allocated.DataPropertyName = "Allocated"
        Me.Allocated.HeaderText = "Allocated"
        Me.Allocated.Name = "Allocated"
        Me.Allocated.ReadOnly = True
        Me.Allocated.Width = 84
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 40
        '
        'frmCaseSummary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.ClientSize = New System.Drawing.Size(1020, 579)
        Me.Controls.Add(Me.dgvVanFeesApplied)
        Me.Controls.Add(Me.cmdVanFeesAppliedClear)
        Me.Controls.Add(Me.cmdVanFeesAppliedAll)
        Me.Controls.Add(Me.cmdArrangementBrokenClear)
        Me.Controls.Add(Me.cmdArrangementBrokenAll)
        Me.Controls.Add(Me.dgvArrangementBroken)
        Me.Controls.Add(Me.cmdCoveredClear)
        Me.Controls.Add(Me.cmdCoveredAll)
        Me.Controls.Add(Me.dgvCovered)
        Me.Controls.Add(Me.lblSummary)
        Me.Controls.Add(Me.cboDisplaySet)
        Me.Controls.Add(Me.lblPeriodType)
        Me.Controls.Add(Me.cboPeriodType)
        Me.Controls.Add(Me.dgvPostcodeArea)
        Me.Controls.Add(Me.cmdWorkTypeClear)
        Me.Controls.Add(Me.cmdPostcodeAreaClear)
        Me.Controls.Add(Me.dgvWorkType)
        Me.Controls.Add(Me.cmdPostcodeAreaAll)
        Me.Controls.Add(Me.cmdWorkTypeAll)
        Me.Controls.Add(Me.cmdPaymentClear)
        Me.Controls.Add(Me.cmdRefreshDB)
        Me.Controls.Add(Me.cmdPaymentAll)
        Me.Controls.Add(Me.chkTopClients)
        Me.Controls.Add(Me.pnlAbsPerc)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdClearAll)
        Me.Controls.Add(Me.cmdStageClear)
        Me.Controls.Add(Me.cmdStageAll)
        Me.Controls.Add(Me.cmdVisitedClear)
        Me.Controls.Add(Me.cmdLeviedClear)
        Me.Controls.Add(Me.dgvAddConfirmed)
        Me.Controls.Add(Me.cmdSchemeClear)
        Me.Controls.Add(Me.cmdLeviedAll)
        Me.Controls.Add(Me.cmdVisitedAll)
        Me.Controls.Add(Me.cmdAddConfirmedClear)
        Me.Controls.Add(Me.cmdAddConfirmedAll)
        Me.Controls.Add(Me.dgvSchemeName)
        Me.Controls.Add(Me.dgvPayment)
        Me.Controls.Add(Me.dgvStageName)
        Me.Controls.Add(Me.cmdClientClear)
        Me.Controls.Add(Me.cmdSchemeAll)
        Me.Controls.Add(Me.cmdClientAll)
        Me.Controls.Add(Me.dgvVisited)
        Me.Controls.Add(Me.dgvSummary)
        Me.Controls.Add(Me.dgvAllocated)
        Me.Controls.Add(Me.cmdAllocatedClear)
        Me.Controls.Add(Me.dgvClientName)
        Me.Controls.Add(Me.dgvLevied)
        Me.Controls.Add(Me.cmdAllocatedAll)
        Me.Name = "frmCaseSummary"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Allocation Summary"
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvStageName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPayment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLevied, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvVisited, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAllocated, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSchemeName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAddConfirmed, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAbsPerc.ResumeLayout(False)
        Me.pnlAbsPerc.PerformLayout()
        CType(Me.dgvWorkType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCovered, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvArrangementBroken, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvVanFeesApplied, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvSummary As System.Windows.Forms.DataGridView
    Friend WithEvents dgvStageName As System.Windows.Forms.DataGridView
    Friend WithEvents dgvClientName As System.Windows.Forms.DataGridView
    Friend WithEvents dgvPayment As System.Windows.Forms.DataGridView
    Friend WithEvents dgvLevied As System.Windows.Forms.DataGridView
    Friend WithEvents dgvVisited As System.Windows.Forms.DataGridView
    Friend WithEvents dgvAllocated As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSchemeName As System.Windows.Forms.DataGridView
    Friend WithEvents ClientName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClientNameTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Payment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Levied As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Visited As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StageName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colStageNameTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdStageAll As System.Windows.Forms.Button
    Friend WithEvents cmdStageClear As System.Windows.Forms.Button
    Friend WithEvents cmdAllocatedClear As System.Windows.Forms.Button
    Friend WithEvents cmdAllocatedAll As System.Windows.Forms.Button
    Friend WithEvents cmdClientClear As System.Windows.Forms.Button
    Friend WithEvents cmdClientAll As System.Windows.Forms.Button
    Friend WithEvents cmdSchemeClear As System.Windows.Forms.Button
    Friend WithEvents cmdSchemeAll As System.Windows.Forms.Button
    Friend WithEvents cmdLeviedClear As System.Windows.Forms.Button
    Friend WithEvents cmdLeviedAll As System.Windows.Forms.Button
    Friend WithEvents cmdVisitedClear As System.Windows.Forms.Button
    Friend WithEvents cmdVisitedAll As System.Windows.Forms.Button
    Friend WithEvents cmdPaymentClear As System.Windows.Forms.Button
    Friend WithEvents cmdPaymentAll As System.Windows.Forms.Button
    Friend WithEvents cmdAddConfirmedClear As System.Windows.Forms.Button
    Friend WithEvents cmdAddConfirmedAll As System.Windows.Forms.Button
    Friend WithEvents dgvAddConfirmed As System.Windows.Forms.DataGridView
    Friend WithEvents AddConfirmed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdClearAll As System.Windows.Forms.Button
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents pnlAbsPerc As System.Windows.Forms.Panel
    Friend WithEvents radPerc As System.Windows.Forms.RadioButton
    Friend WithEvents radAbs As System.Windows.Forms.RadioButton
    Friend WithEvents chkTopClients As System.Windows.Forms.CheckBox
    Friend WithEvents dgvWorkType As System.Windows.Forms.DataGridView
    Friend WithEvents cmdWorkTypeClear As System.Windows.Forms.Button
    Friend WithEvents cmdWorkTypeAll As System.Windows.Forms.Button
    Friend WithEvents cmdRefreshDB As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cmsSummary As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmdCoveredClear As System.Windows.Forms.Button
    Friend WithEvents cmdCoveredAll As System.Windows.Forms.Button
    Friend WithEvents dgvCovered As System.Windows.Forms.DataGridView
    Friend WithEvents bxtnCoveredAll As System.Windows.Forms.Button
    Friend WithEvents SchemeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Covered As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvPostcodeArea As System.Windows.Forms.DataGridView
    Friend WithEvents cmdPostcodeAreaClear As System.Windows.Forms.Button
    Friend WithEvents cmdPostcodeAreaAll As System.Windows.Forms.Button
    Friend WithEvents PostcodeArea As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmsList As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cboPeriodType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriodType As System.Windows.Forms.Label
    Friend WithEvents cboDisplaySet As System.Windows.Forms.ComboBox
    Friend WithEvents cmsForm As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents cmdArrangementBrokenClear As System.Windows.Forms.Button
    Friend WithEvents cmdArrangementBrokenAll As System.Windows.Forms.Button
    Friend WithEvents dgvArrangementBroken As System.Windows.Forms.DataGridView
    Friend WithEvents ArrangementBroken As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvVanFeesApplied As System.Windows.Forms.DataGridView
    Friend WithEvents cmdVanFeesAppliedClear As System.Windows.Forms.Button
    Friend WithEvents cmdVanFeesAppliedAll As System.Windows.Forms.Button
    Friend WithEvents VanFeesApplied As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WorkType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Allocated As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn

End Class

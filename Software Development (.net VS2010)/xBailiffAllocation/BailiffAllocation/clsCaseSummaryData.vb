﻿Public Class clsCaseSummaryData

    Private SummaryDS As New DataSet

    Private LastLoadDV As DataView

    Private SummaryDV As DataView
    Private WarningSummaryDV As DataView
    Private DetailDV As DataView
    Private ConsortiumDV As DataView
    Private PeriodTypeDV As DataView

    Private StageNameDV As DataView
    Private ClientNameDV As DataView
    Private WorkTypeDV As DataView
    Private SchemeNameDV As DataView
    Private AllocatedDV As DataView
    Private LeviedDV As DataView
    Private VanFeesAppliedDV As DataView
    Private PaymentDV As DataView
    Private VisitedDV As DataView
    Private AddConfirmedDV As DataView
    Private CoveredDV As DataView
    Private PostcodeAreaDV As DataView
    Private ArrangementBrokenDV As DataView

    Private MapCaseDV As DataView

    Public Sub New()

        SummaryDS.Tables.Add("LastLoad")

        SummaryDS.Tables.Add("Summary")
        SummaryDS.Tables.Add("WarningSummary")
        SummaryDS.Tables.Add("Detail")
        SummaryDS.Tables.Add("Consortium")
        SummaryDS.Tables.Add("PeriodType")

        SummaryDS.Tables.Add("StageName")
        SummaryDS.Tables.Add("ClientName")
        SummaryDS.Tables.Add("WorkType")
        SummaryDS.Tables.Add("SchemeName")
        SummaryDS.Tables.Add("Allocated")
        SummaryDS.Tables.Add("Levied")
        SummaryDS.Tables.Add("VanFeesApplied")
        SummaryDS.Tables.Add("Payment")
        SummaryDS.Tables.Add("Visited")
        SummaryDS.Tables.Add("AddConfirmed")
        SummaryDS.Tables.Add("Covered")
        SummaryDS.Tables.Add("PostcodeArea")
        SummaryDS.Tables.Add("ArrangementBroken")

        SummaryDS.Tables.Add("MapCase")
        SummaryDS.Tables("MapCase").Columns.Add("DebtorID")
        SummaryDS.Tables("MapCase").Columns.Add("DataSet")
        SummaryDS.Tables("MapCase").Columns.Add("Easting")
        SummaryDS.Tables("MapCase").Columns.Add("Northing")
        SummaryDS.Tables("MapCase").Columns.Add("Postcode")
        SummaryDS.Tables("MapCase").Columns.Add("BailiffName")

        LoadDataTable("EXEC dbo.GetConsortiums", SummaryDS, "Consortium")
        ConsortiumDV = New DataView(SummaryDS.Tables("Consortium"))
        ConsortiumDV.AllowDelete = False
        ConsortiumDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetPeriodTypes 'C'", SummaryDS, "PeriodType")
        PeriodTypeDV = New DataView(SummaryDS.Tables("PeriodType"))
        PeriodTypeDV.AllowDelete = False
        PeriodTypeDV.AllowNew = False
    End Sub

    Protected Overrides Sub Finalize()
        If Not IsNothing(LastLoadDV) Then LastLoadDV.Dispose()

        If Not IsNothing(SummaryDV) Then SummaryDV.Dispose()
        If Not IsNothing(WarningSummaryDV) Then WarningSummaryDV.Dispose()
        If Not IsNothing(DetailDV) Then DetailDV.Dispose()
        If Not IsNothing(ConsortiumDV) Then ConsortiumDV.Dispose()
        If Not IsNothing(PeriodTypeDV) Then PeriodTypeDV.Dispose()
        If Not IsNothing(StageNameDV) Then StageNameDV.Dispose()
        If Not IsNothing(ClientNameDV) Then ClientNameDV.Dispose()
        If Not IsNothing(WorkTypeDV) Then WorkTypeDV.Dispose()
        If Not IsNothing(SchemeNameDV) Then SchemeNameDV.Dispose()
        If Not IsNothing(AllocatedDV) Then AllocatedDV.Dispose()
        If Not IsNothing(LeviedDV) Then LeviedDV.Dispose()
        If Not IsNothing(LeviedDV) Then VanFeesAppliedDV.Dispose()
        If Not IsNothing(PaymentDV) Then PaymentDV.Dispose()
        If Not IsNothing(VisitedDV) Then VisitedDV.Dispose()
        If Not IsNothing(AddConfirmedDV) Then AddConfirmedDV.Dispose()
        If Not IsNothing(CoveredDV) Then CoveredDV.Dispose()
        If Not IsNothing(PostcodeAreaDV) Then PostcodeAreaDV.Dispose()
        If Not IsNothing(ArrangementBrokenDV) Then ArrangementBrokenDV.Dispose()
        If Not IsNothing(MapCaseDV) Then MapCaseDV.Dispose()

        If Not IsNothing(SummaryDS) Then SummaryDS.Dispose() : SummaryDS = Nothing
        MyBase.Finalize()

    End Sub

#Region " Public Properties"

    Public ReadOnly Property SummaryDataSet() As DataSet
        Get
            SummaryDataSet = SummaryDS
        End Get
    End Property

    Public ReadOnly Property LastLoadDataView() As DataView
        Get
            LastLoadDataView = LastLoadDV
        End Get
    End Property

    Public ReadOnly Property SummaryDataView() As DataView
        Get
            SummaryDataView = SummaryDV
        End Get
    End Property

    Public ReadOnly Property WarningSummaryDataView() As DataView
        Get
            WarningSummaryDataView = WarningSummaryDV
        End Get
    End Property
    Public ReadOnly Property DetailDataView() As DataView
        Get
            DetailDataView = DetailDV
        End Get
    End Property

    Public ReadOnly Property ConsortiumDataView() As DataView
        Get
            ConsortiumDataView = ConsortiumDV
        End Get
    End Property

    Public ReadOnly Property PeriodTypeDataView() As DataView
        Get
            PeriodTypeDataView = PeriodTypeDV
        End Get
    End Property

    Public ReadOnly Property StageNameDataView() As DataView
        Get
            StageNameDataView = StageNameDV
        End Get
    End Property

    Public ReadOnly Property ClientNameDataView() As DataView
        Get
            ClientNameDataView = ClientNameDV
        End Get
    End Property

    Public ReadOnly Property WorkTypeDataView() As DataView
        Get
            WorkTypeDataView = WorkTypeDV
        End Get
    End Property

    Public ReadOnly Property SchemeNameDataView() As DataView
        Get
            SchemeNameDataView = SchemeNameDV
        End Get
    End Property

    Public ReadOnly Property AllocatedDataView() As DataView
        Get
            AllocatedDataView = AllocatedDV
        End Get
    End Property

    Public ReadOnly Property LeviedDataView() As DataView
        Get
            LeviedDataView = LeviedDV
        End Get
    End Property

    Public ReadOnly Property VanFeesAppliedDataView() As DataView
        Get
            VanFeesAppliedDataView = VanFeesAppliedDV
        End Get
    End Property

    Public ReadOnly Property PaymentDataView() As DataView
        Get
            PaymentDataView = PaymentDV
        End Get
    End Property

    Public ReadOnly Property VisitedDataView() As DataView
        Get
            VisitedDataView = VisitedDV
        End Get
    End Property

    Public ReadOnly Property AddConfirmedDataView() As DataView
        Get
            AddConfirmedDataView = AddConfirmedDV
        End Get
    End Property

    Public ReadOnly Property CoveredDataView() As DataView
        Get
            CoveredDataView = CoveredDV
        End Get
    End Property

    Public ReadOnly Property PostcodeAreaDataView() As DataView
        Get
            PostcodeAreaDataView = PostcodeAreaDV
        End Get
    End Property

    Public ReadOnly Property ArrangementBrokenDataView() As DataView
        Get
            ArrangementBrokenDataView = ArrangementBrokenDV
        End Get
    End Property

    Public ReadOnly Property MapCaseDataView() As DataView
        Get
            MapCaseDataView = MapCaseDV
        End Get
    End Property

#End Region

#Region "Public Methods"

    Public Sub GetSummary(ByVal ParamList As String, ByVal Abs As Boolean, ByVal DisplaySet As String)
        Dim SummaryParamList As String = "", WarningSummaryParamList As String

        SummaryDV = Nothing ' Needed to clear column list otherwise new columns eg clientname are added at the end
        WarningSummaryDV = Nothing

        If Abs Then SummaryParamList = ParamList & ",'A'" Else ParamList = ParamList & ",'P'"

        SummaryParamList &= "," & DisplaySet
        WarningSummaryParamList = ParamList & "," & DisplaySet

        LoadDataTable("EXEC dbo.GetSummary " & SummaryParamList, SummaryDS, "Summary")
        SummaryDV = New DataView(SummaryDS.Tables("Summary"))
        SummaryDV.AllowDelete = False
        SummaryDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetWarningSummary " & WarningSummaryParamList, SummaryDS, "WarningSummary")
        WarningSummaryDV = New DataView(SummaryDS.Tables("WarningSummary"))
        WarningSummaryDV.AllowDelete = False
        WarningSummaryDV.AllowNew = False
    End Sub

    Public Sub GetCases(ByVal ParamList As String, ByVal Period As String)
        ParamList += "," & Period.ToString

        LoadDataTable("EXEC dbo.GetCases " & ParamList, SummaryDS, "Detail", True)
        DetailDV = New DataView(SummaryDS.Tables("Detail").DefaultView.ToTable(True))
        DetailDV.AllowDelete = False
        DetailDV.AllowNew = False
    End Sub

    Public Sub GetBailiffCases(ByVal ParamList As String, ByVal Period As String)
        ParamList += "," & Period.ToString

        LoadDataTable("EXEC dbo.GetBailiffCases " & ParamList, SummaryDS, "Detail", True)
        DetailDV = New DataView(SummaryDS.Tables("Detail").DefaultView.ToTable(True))
        DetailDV.AllowDelete = False
        DetailDV.AllowNew = False
    End Sub

    Public Sub GetCasesByBailiff(ByVal BailiffID As Integer)
        LoadDataTable("EXEC dbo.GetCasesByBailiff " & BailiffID.ToString, SummaryDS, "Detail", True)
        DetailDV = New DataView(SummaryDS.Tables("Detail"))
        DetailDV.AllowDelete = False
        DetailDV.AllowNew = False
    End Sub

    Public Sub GetCasesForMap(ByRef Source As DataGridView)
        Dim DataSetName As String
        SummaryDS.Tables("MapCase").Clear()

        For Each Row As DataRow In SummaryDS.Tables("Detail").Rows
            DataSetName = ""

            If Not IsNothing(Source.Columns("StageName")) Then DataSetName &= Row.Item("StageName")

            If Not IsNothing(Source.Columns("ClientName")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                DataSetName &= Row.Item("ClientName")
            End If

            If Not IsNothing(Source.Columns("WorkType")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                DataSetName &= Row.Item("WorkType")
            End If

            If Not IsNothing(Source.Columns("Allocated")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                If Row.Item("Allocated") = "Y" Then
                    DataSetName &= "allocated"
                Else
                    DataSetName &= "unallocated"
                End If
            End If

            If Not IsNothing(Source.Columns("SchemeName")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                DataSetName &= Row.Item("SchemeName")
            End If

            If Not IsNothing(Source.Columns("Payment")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                If Row.Item("Payment") = "Y" Then
                    DataSetName &= "payment made"
                Else
                    DataSetName &= "no payment"
                End If
            End If

            If Not IsNothing(Source.Columns("Visited")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                If Row.Item("Visited") = "Y" Then
                    DataSetName &= "visited"
                Else
                    DataSetName &= "no visits"
                End If
            End If

            If Not IsNothing(Source.Columns("Covered")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                DataSetName &= Row.Item("Covered")
            End If

            If Not IsNothing(Source.Columns("Levied")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                If Row.Item("Levied") = "Y" Then
                    DataSetName &= "levied"
                Else
                    DataSetName &= "not levied"
                End If
            End If

            If Not IsNothing(Source.Columns("VanFeesApplied")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                If Row.Item("VanFeesApplied") = "Y" Then
                    DataSetName &= "Van fees applied"
                Else
                    DataSetName &= "Van fees not applied"
                End If
            End If

            If Not IsNothing(Source.Columns("Broken")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                If Row.Item("Broken") = "Y" Then
                    DataSetName &= "broken"
                Else
                    DataSetName &= "not broken"
                End If
            End If

            If DataSetName = "" Then
                DataSetName = "All cases"
            Else
                DataSetName = StrConv(DataSetName, VbStrConv.ProperCase)
            End If


            Dim NewRow As DataRow = SummaryDS.Tables("MapCase").Rows.Add
            NewRow.Item("DebtorID") = Row.Item("DebtorID")
            NewRow.Item("Dataset") = DataSetName
            NewRow.Item("Easting") = Row.Item("add_os_easting")
            NewRow.Item("Northing") = Row.Item("add_os_northing")
            NewRow.Item("Postcode") = Row.Item("add_postcode")
            NewRow.Item("BailiffName") = Row.Item("BailiffName")
        Next Row

        MapCaseDV = New DataView(SummaryDS.Tables("MapCase"))
        MapCaseDV.AllowDelete = False
        MapCaseDV.AllowNew = False
    End Sub

    Public Sub GetList(ByVal ParamList As String)

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'StageName'", SummaryDS, "StageName")
        StageNameDV = New DataView(SummaryDS.Tables("StageName"))
        StageNameDV.AllowDelete = False
        StageNameDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'ClientName'", SummaryDS, "ClientName")
        ClientNameDV = New DataView(SummaryDS.Tables("ClientName"))
        ClientNameDV.AllowDelete = False
        ClientNameDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'WorkType'", SummaryDS, "WorkType")
        WorkTypeDV = New DataView(SummaryDS.Tables("WorkType"))
        WorkTypeDV.AllowDelete = False
        WorkTypeDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'SchemeName'", SummaryDS, "SchemeName")
        SchemeNameDV = New DataView(SummaryDS.Tables("SchemeName"))
        SchemeNameDV.AllowDelete = False
        SchemeNameDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'Allocated'", SummaryDS, "Allocated")
        AllocatedDV = New DataView(SummaryDS.Tables("Allocated"))
        AllocatedDV.AllowDelete = False
        AllocatedDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'Levied'", SummaryDS, "Levied")
        LeviedDV = New DataView(SummaryDS.Tables("Levied"))
        LeviedDV.AllowDelete = False
        LeviedDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'VanFeesApplied'", SummaryDS, "VanFeesApplied")
        VanFeesAppliedDV = New DataView(SummaryDS.Tables("VanFeesApplied"))
        VanFeesAppliedDV.AllowDelete = False
        VanFeesAppliedDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'Payment'", SummaryDS, "Payment")
        PaymentDV = New DataView(SummaryDS.Tables("Payment"))
        PaymentDV.AllowDelete = False
        PaymentDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'Visited'", SummaryDS, "Visited")
        VisitedDV = New DataView(SummaryDS.Tables("Visited"))
        VisitedDV.AllowDelete = False
        VisitedDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'AddConfirmed'", SummaryDS, "AddConfirmed")
        AddConfirmedDV = New DataView(SummaryDS.Tables("AddConfirmed"))
        AddConfirmedDV.AllowDelete = False
        AddConfirmedDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'Covered'", SummaryDS, "Covered")
        CoveredDV = New DataView(SummaryDS.Tables("Covered"))
        CoveredDV.AllowDelete = False
        CoveredDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'PostcodeArea'", SummaryDS, "PostCodeArea")
        PostcodeAreaDV = New DataView(SummaryDS.Tables("PostCodeArea"))
        PostcodeAreaDV.AllowDelete = False
        PostcodeAreaDV.AllowNew = False

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'ArrangementBroken'", SummaryDS, "ArrangementBroken")
        ArrangementBrokenDV = New DataView(SummaryDS.Tables("ArrangementBroken"))
        ArrangementBrokenDV.AllowDelete = False
        ArrangementBrokenDV.AllowNew = False
    End Sub

    Public Sub GetClientList(ByVal ParamList As String)

        LoadDataTable("EXEC dbo.GetList " & ParamList & ",'ClientName'", SummaryDS, "ClientName")
        ClientNameDV = New DataView(SummaryDS.Tables("ClientName"))
        ClientNameDV.AllowDelete = False
        ClientNameDV.AllowNew = False

    End Sub

    Public Sub GetLastLoad()
        LoadDataTable("EXEC dbo.GetLastLoad", SummaryDS, "LastLoad")
        LastLoadDV = New DataView(SummaryDS.Tables("LastLoad"))
        LastLoadDV.AllowDelete = False
        LastLoadDV.AllowNew = False
    End Sub

    Public Sub RefreshDatabase()

        ExecStoredProc("EXEC dbo.BuildBailiffAllocation 'I'", 600)

    End Sub

#End Region

End Class


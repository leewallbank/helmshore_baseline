﻿Public Class frmCaseSummary

    Private SummaryData As New clsCaseSummaryData
    'Private Map As frmMap
    ' These cannot be instantiated here as the datagridviews have not been instantiated at this point but they need to be declared here to achieve the right scope...

    Private StageGridState As clsGridState
    Private ClientGridState As clsGridState
    Private WorkTypeGridState As clsGridState
    Private SchemeGridState As clsGridState
    Private AllocatedGridState As clsGridState
    Private PaymentGridState As clsGridState
    Private LeviedGridState As clsGridState
    Private VanFeesAppliedGridState As clsGridState
    Private VisitedGridState As clsGridState
    Private AddConfirmedGridState As clsGridState
    Private CoveredGridState As clsGridState
    Private PostcodeAreaGridState As clsGridState
    Private ArrangementBrokenGridState As clsGridState

    Private ColSort As String

    Private ParamList As String = "null,null,null,null,null,null,null,null,null,null,null,null,null,0,1" ' i.e. top level
    Public RefreshClientScroll As Boolean = True ' needed as chkTopClients will change the number of rows in dgvClientName

#Region "New and open"

    Public Sub New()

        InitializeComponent()

        ' Now instantiate these
        ' Map = New frmMap(dgvSummary)
        StageGridState = New clsGridState(dgvStageName)
        ClientGridState = New clsGridState(dgvClientName)
        WorkTypeGridState = New clsGridState(dgvWorkType)
        SchemeGridState = New clsGridState(dgvSchemeName)
        AllocatedGridState = New clsGridState(dgvAllocated)
        PaymentGridState = New clsGridState(dgvPayment)
        LeviedGridState = New clsGridState(dgvLevied)
        VanFeesAppliedGridState = New clsGridState(dgvVanFeesApplied)
        VisitedGridState = New clsGridState(dgvVisited)
        AddConfirmedGridState = New clsGridState(dgvAddConfirmed)
        CoveredGridState = New clsGridState(dgvCovered)
        PostcodeAreaGridState = New clsGridState(dgvPostcodeArea)
        ArrangementBrokenGridState = New clsGridState(dgvArrangementBroken)

        AddControlHandlers()

        cmsSummary.Items.Add("View Cases")
        cmsSummary.Items.Add("Map Cases")
        cmsSummary.Items.Add("Copy")
        cmsSummary.Items.Add("Select All")

        cmsList.Items.Add("Consortiums...")
        cmsList.Items.Add("Copy")
        cmsList.Items.Add("Select All")

        cmsForm.Items.Add("Bailiff View")

        Dim Consortium As ToolStripMenuItem = Convert.ChangeType(cmsList.Items(0), GetType(ToolStripMenuItem))

        For Each Row As DataRow In SummaryData.ConsortiumDataView.ToTable(True, "Consortium").Rows
            Dim SubMenuItem As ToolStripMenuItem = New ToolStripMenuItem(Row.Item(0).ToString)

            AddHandler SubMenuItem.Click, AddressOf ConsortiumContextSubMenu_Click
            Consortium.DropDownItems.Add(SubMenuItem)
        Next

        SummaryData.GetSummary(ParamList, True, "P")
        dgvSummary.DataSource = SummaryData.SummaryDataView

        FormatGridColumns(dgvSummary, GetStages)

        SummaryData.GetList(ParamList)
        SetListGrids()
        FormatListColumns()

        cboPeriodType.DataSource = SummaryData.PeriodTypeDataView
        cboPeriodType.ValueMember = "PeriodTypeID"
        cboPeriodType.DisplayMember = "PeriodTypeDesc"

    End Sub

    Private Sub frmSummary_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        RemoveSelectionHandlers()
    End Sub

    Private Sub frmCaseSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        If e.Button = MouseButtons.Right Then
            cmsForm.Show(sender, e.Location)
        End If
    End Sub

    Private Sub frmSummary_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        HighlightWarnings()
        SetSelections() 'Arrays will be nothing so clear all selections - by default the first row in a datagridview is always selected
        dgvSummary.ClearSelection()

        radAbs.Checked = True
        chkTopClients.Checked = False
        cboPeriodType.SelectedValue = 1
        cboDisplaySet.Text = "Periods"

        AddHandler radAbs.CheckedChanged, AddressOf radAbs_CheckedChanged
        AddHandler chkTopClients.CheckedChanged, AddressOf chkTopClients_CheckedChanged

        AddSelectionHandlers()

        SetcmdRefreshTip()

    End Sub
#End Region

#Region "Refresh"

    Private Sub RefreshGrid()

        ParamList = GetParam(dgvStageName, "StageName") & ","
        ParamList += GetParam(dgvClientName, "ClientName") & ","
        ParamList += GetParam(dgvWorkType, "WorkType") & ","
        ParamList += GetParam(dgvSchemeName, "SchemeName") & ","
        ParamList += GetParam(dgvAllocated, "Allocated") & ","
        ParamList += GetParam(dgvLevied, "Levied") & ","
        ParamList += GetParam(dgvVanFeesApplied, "VanFeesApplied") & ","
        ParamList += GetParam(dgvPayment, "Payment") & ","
        ParamList += GetParam(dgvVisited, "Visited") & ","
        ParamList += GetParam(dgvAddConfirmed, "AddConfirmed") & ","
        ParamList += GetParam(dgvCovered, "Covered") & ","
        ParamList += GetParam(dgvPostcodeArea, "PostcodeArea") & ","
        ParamList += GetParam(dgvArrangementBroken, "ArrangementBroken") & ","

        ParamList += chkTopClients.Checked.ToString
        ParamList += "," & cboPeriodType.SelectedValue.ToString

        PreRefresh(Me)

        RemoveSelectionHandlers()

        GetSelections()

        SummaryData.GetSummary(ParamList, radAbs.Checked, cboDisplaySet.Text.Substring(0, 1))
        dgvSummary.DataSource = SummaryData.SummaryDataView

        FormatGridColumns(dgvSummary, GetStages)
        HighlightWarnings()

        SummaryData.GetList(ParamList)
        SetListGrids()
        FormatListColumns()

        SetSelections()

        AddSelectionHandlers()

        PostRefresh(Me)

        dgvSummary.ClearSelection() ' The first cell always get selected
    End Sub

    Private Function GetParam(ByRef DataGrid As DataGridView, ByVal ColumnName As String) As String
        ' Used when selection criteria change
        Dim Param As String = Nothing

        For Each dr As DataGridViewRow In DataGrid.SelectedRows
            If Not IsNothing(Param) Then Param += vbTab
            Param += dr.Cells(ColumnName).Value
        Next dr

        If IsNothing(Param) Then
            Param = "null"
        Else
            Param = "'" & Param & "'"
        End If

        Return Param
    End Function

    Private Function GetParam(ByVal ParamColumn As String, ByRef Cell As DataGridViewCell, ByRef ListDataGridView As DataGridView)
        ' Used when detail for a particular cell in the summary grid is retrieved
        Dim Param As String = ""

        If IsNothing(Cell.DataGridView.Columns(ParamColumn)) Then
            ' The column may not be present as only one criteria is applicable
            If ListDataGridView.SelectedRows.Count = 1 Then
                Param = "'" & ListDataGridView.SelectedRows(0).Cells(0).Value & "'"
            Else
                Param = "null"
            End If
        Else
            Param = "'" & Cell.DataGridView.Item(ParamColumn, Cell.RowIndex).Value & "'"
        End If

        Return Param
    End Function
#End Region

#Region "Grid selection events"

    Private Sub cboPeriodType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboPeriodType.SelectedValueChanged
        RefreshGrid()
    End Sub

    Private Sub cboDisplaySet_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboDisplaySet.SelectedValueChanged
        RefreshGrid()
    End Sub

    Private Sub dgvSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        RefreshGrid()
    End Sub

    Private Sub dgvMouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
        ' This is needed to make mousewheel scroll list items
        sender.Select()
    End Sub

    Private Sub dgvMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

        If hti.Type = DataGridViewHitTestType.ColumnHeader Then
            GetSelections()
            RemoveSelectionHandlers()
        End If
    End Sub

    Private Sub dgvMouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

        If hti.Type = DataGridViewHitTestType.ColumnHeader Then
            SetSelections()
            AddSelectionHandlers()
        End If

        If e.Button = MouseButtons.Right Then
            If sender.name <> "dgvClientName" And sender.name <> "dgvSchemeName" Then cmsList.Items(0).Visible = False Else cmsList.Items(0).Visible = True
            cmsList.Show(sender, e.Location)
        End If

    End Sub

    Private Sub dgvSummary_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvSummary.DataBindingComplete
        ' This needs to be handled differently. I'm not sure why but doing this later causes events to fire. Recommended practice on the following url
        ' http://msdn.microsoft.com/en-us/library/system.windows.forms.datagridviewcolumn.visible.aspx

        If dgvSummary.Columns.Contains("RowID") Then dgvSummary.Columns("RowID").Visible = False
    End Sub
    Private Sub dgvSummary_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseDown
        Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

        If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
            dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected = True
        End If

        If hti.Type = DataGridViewHitTestType.ColumnHeader Then
            ColSort = SummaryData.SummaryDataView.Sort
        End If
    End Sub

    Private Sub dgvSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseUp
        If e.Button = MouseButtons.Right Then cmsSummary.Show(dgvSummary, e.Location)

        Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

        If hti.Type = DataGridViewHitTestType.ColumnHeader Then
            If ColSort <> "" Then SummaryData.SummaryDataView.Sort += "," & ColSort
        End If
    End Sub

    Private Sub dgvSummary_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSummary.SelectionChanged
        Dim TotalCases As Integer = 0

        If dgvSummary.SelectedCells.Count = 0 Or radAbs.Checked = False Then
            lblSummary.Text = ""
        Else
            For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then TotalCases += Cell.Value
            Next Cell
            lblSummary.Text = TotalCases.ToString & " case" & If(TotalCases > 1, "s", "")
        End If

    End Sub

    Private Sub ShowDetail()

        Dim DetailParamList As String
        Dim Detail As New diaCaseDetail(Me)

        PreRefresh(Me)

        For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
            If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then
                DetailParamList = GetParam("StageName", Cell, dgvStageName) & ","
                DetailParamList += GetParam("ClientName", Cell, dgvClientName) & ","
                DetailParamList += GetParam("WorkType", Cell, dgvWorkType) & ","
                DetailParamList += GetParam("SchemeName", Cell, dgvSchemeName) & ","
                DetailParamList += GetParam("Allocated", Cell, dgvAllocated) & ","
                DetailParamList += GetParam("Levied", Cell, dgvLevied) & ","
                DetailParamList += GetParam("VanFeesApplied", Cell, dgvVanFeesApplied) & ","
                DetailParamList += GetParam("Payment", Cell, dgvPayment) & ","
                DetailParamList += GetParam("Visited", Cell, dgvVisited) & ","
                DetailParamList += GetParam("AddConfirmed", Cell, dgvAddConfirmed) & ","
                DetailParamList += GetParam("Covered", Cell, dgvCovered) & ","
                DetailParamList += GetParam("PostcodeArea", Cell, dgvPostcodeArea) & ","
                DetailParamList += GetParam("ArrangementBroken", Cell, dgvArrangementBroken) & ","

                DetailParamList += chkTopClients.Checked.ToString

                DetailParamList += "," & cboPeriodType.SelectedValue.ToString

                Detail.AddCases(DetailParamList, dgvSummary.Columns(Cell.ColumnIndex).Name)

            End If
        Next Cell

        PostRefresh(Me)

        Detail.Show()
        Detail = Nothing

    End Sub
    Private Sub ShowMap()
        Dim DetailParamList As String

        PreRefresh(Me)

        Dim Map As New frmMap(dgvSummary)

        For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
            If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then
                DetailParamList = GetParam("StageName", Cell, dgvStageName) & ","
                DetailParamList += GetParam("ClientName", Cell, dgvClientName) & ","
                DetailParamList += GetParam("WorkType", Cell, dgvWorkType) & ","
                DetailParamList += GetParam("SchemeName", Cell, dgvSchemeName) & ","
                DetailParamList += GetParam("Allocated", Cell, dgvAllocated) & ","
                DetailParamList += GetParam("Levied", Cell, dgvLevied) & ","
                DetailParamList += GetParam("VanFeesApplied", Cell, dgvVanFeesApplied) & ","
                DetailParamList += GetParam("Payment", Cell, dgvPayment) & ","
                DetailParamList += GetParam("Visited", Cell, dgvVisited) & ","
                DetailParamList += GetParam("AddConfirmed", Cell, dgvAddConfirmed) & ","
                DetailParamList += GetParam("Covered", Cell, dgvCovered) & ","
                DetailParamList += GetParam("PostcodeArea", Cell, dgvPostcodeArea) & ","
                DetailParamList += GetParam("ArrangementBroken", Cell, dgvArrangementBroken) & ","

                DetailParamList += chkTopClients.Checked.ToString

                DetailParamList += "," & cboPeriodType.SelectedValue.ToString

                Map.AddCases(DetailParamList, dgvSummary.Columns(Cell.ColumnIndex).Name)

            End If
        Next Cell

        Map.GetCasesForMap()

        PostRefresh(Me)

        Map.Show()
        Map = Nothing
    End Sub
#End Region

#Region "'All' and 'Clear' button click events"

    Private Sub cmdStageAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageAll.Click
        dgvStageName.SelectAll()
        StageGridState.GetState()
    End Sub

    Private Sub cmdStageClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageClear.Click
        dgvStageName.ClearSelection()
        StageGridState.GetState()
    End Sub

    Private Sub cmdClientAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientAll.Click
        dgvClientName.SelectAll()
        ClientGridState.GetState()
    End Sub

    Private Sub cmdClientClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientClear.Click
        dgvClientName.ClearSelection()
        ClientGridState.GetState()
    End Sub

    Private Sub cmdWorkTypeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWorkTypeAll.Click
        dgvWorkType.SelectAll()
        WorkTypeGridState.GetState()
    End Sub

    Private Sub cmdWorkTypeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWorkTypeClear.Click
        dgvWorkType.ClearSelection()
        WorkTypeGridState.GetState()
    End Sub

    Private Sub cmdSchemeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeAll.Click
        dgvSchemeName.SelectAll()
        SchemeGridState.GetState()
    End Sub

    Private Sub cmdSchemeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeClear.Click
        dgvSchemeName.ClearSelection()
        SchemeGridState.GetState()
    End Sub

    Private Sub cmdAllocatedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAllocatedAll.Click
        dgvAllocated.SelectAll()
        AllocatedGridState.GetState()
    End Sub

    Private Sub cmdAllocatedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAllocatedClear.Click
        dgvAllocated.ClearSelection()
        AllocatedGridState.GetState()
    End Sub

    Private Sub cmdLeviedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLeviedAll.Click
        dgvLevied.SelectAll()
        LeviedGridState.GetState()
    End Sub

    Private Sub cmdLeviedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLeviedClear.Click
        dgvLevied.ClearSelection()
        LeviedGridState.GetState()
    End Sub

    Private Sub cmdVanFeesAppliedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVanFeesAppliedAll.Click
        dgvVanFeesApplied.SelectAll()
        VanFeesAppliedGridState.GetState()
    End Sub

    Private Sub cmdVanFeesAppliedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVanFeesAppliedClear.Click
        dgvVanFeesApplied.ClearSelection()
        VanFeesAppliedGridState.GetState()
    End Sub

    Private Sub cmdPaymentAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPaymentAll.Click
        dgvPayment.SelectAll()
        PaymentGridState.GetState()
    End Sub

    Private Sub cmdPaymentClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPaymentClear.Click
        dgvPayment.ClearSelection()
        PaymentGridState.GetState()
    End Sub

    Private Sub cmdVisitedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVisitedAll.Click
        dgvVisited.SelectAll()
        VisitedGridState.GetState()
    End Sub

    Private Sub cmdVisitedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVisitedClear.Click
        dgvVisited.ClearSelection()
        VisitedGridState.GetState()
    End Sub

    Private Sub cmdAddConfirmedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfirmedAll.Click
        dgvAddConfirmed.SelectAll()
        AddConfirmedGridState.GetState()
    End Sub

    Private Sub cmdAddConfirmedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfirmedClear.Click
        dgvAddConfirmed.ClearSelection()
        AddConfirmedGridState.GetState()
    End Sub

    Private Sub cmdCoveredAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCoveredAll.Click
        dgvCovered.SelectAll()
        CoveredGridState.GetState()
    End Sub

    Private Sub cmdCoveredClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCoveredClear.Click
        dgvCovered.ClearSelection()
        CoveredGridState.GetState()
    End Sub

    Private Sub cmdPostcodeAreaAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaAll.Click
        dgvPostcodeArea.SelectAll()
        PostcodeAreaGridState.GetState()
    End Sub

    Private Sub cmdPostcodeAreaClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaClear.Click
        dgvPostcodeArea.ClearSelection()
        PostcodeAreaGridState.GetState()
    End Sub

    Private Sub cmdArrangementBrokenAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdArrangementBrokenAll.Click
        dgvArrangementBroken.SelectAll()
        ArrangementBrokenGridState.GetState()
    End Sub

    Private Sub cmdArrangementBrokenClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdArrangementBrokenClear.Click
        dgvArrangementBroken.ClearSelection()
        ArrangementBrokenGridState.GetState()
    End Sub
#End Region

#Region "Handlers"

    Private Sub RemoveSelectionHandlers()
        ' Need to find a way to do these at once but these SelectionChanged event is specific to the datagridview class and not control - the collection on the form
        RemoveHandler dgvStageName.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvWorkType.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvAllocated.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvLevied.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvVanFeesApplied.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvPayment.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvVisited.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvAddConfirmed.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvCovered.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
        RemoveHandler dgvArrangementBroken.SelectionChanged, AddressOf dgvSelectionChanged

        RemoveHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
        RemoveHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged
    End Sub

    Private Sub AddSelectionHandlers()
        AddHandler dgvStageName.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvWorkType.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvAllocated.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvLevied.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvVanFeesApplied.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvPayment.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvVisited.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvAddConfirmed.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvCovered.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
        AddHandler dgvArrangementBroken.SelectionChanged, AddressOf dgvSelectionChanged

        AddHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
        AddHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged
    End Sub

    Private Sub AddControlHandlers()
        ' All these grids and buttons call the same events so no point declaring them all separately
        For Each Control As Control In Me.Controls
            If TypeOf Control Is DataGridView Then
                AddHandler Control.MouseDown, AddressOf dgvMouseDown
                AddHandler Control.MouseUp, AddressOf dgvMouseUp
                AddHandler Control.MouseEnter, AddressOf dgvMouseEnter
            End If
        Next Control
    End Sub

#End Region

#Region "DataGridView methods"

    Private Sub SetListGrids()
        dgvStageName.DataSource = SummaryData.StageNameDataView
        StageGridState.SetSort()

        dgvClientName.DataSource = SummaryData.ClientNameDataView
        ClientGridState.SetSort()

        dgvWorkType.DataSource = SummaryData.WorkTypeDataView
        WorkTypeGridState.SetSort()

        dgvSchemeName.DataSource = SummaryData.SchemeNameDataView

        SchemeGridState.SetSort()

        dgvAllocated.DataSource = SummaryData.AllocatedDataView
        AllocatedGridState.SetSort()

        dgvLevied.DataSource = SummaryData.LeviedDataView
        LeviedGridState.SetSort()

        dgvVanFeesApplied.DataSource = SummaryData.VanFeesAppliedDataView
        VanFeesAppliedGridState.SetSort()

        dgvPayment.DataSource = SummaryData.PaymentDataView
        PaymentGridState.SetSort()

        dgvVisited.DataSource = SummaryData.VisitedDataView
        VisitedGridState.SetSort()

        dgvAddConfirmed.DataSource = SummaryData.AddConfirmedDataView
        AddConfirmedGridState.SetSort()

        dgvCovered.DataSource = SummaryData.CoveredDataView
        CoveredGridState.SetSort()

        dgvPostcodeArea.DataSource = SummaryData.PostcodeAreaDataView
        PostcodeAreaGridState.SetSort()

        dgvArrangementBroken.DataSource = SummaryData.ArrangementBrokenDataView
        ArrangementBrokenGridState.SetSort()
    End Sub

    Private Sub FormatListColumns()
        ' This is slightly different to settings in modCommon as we want some of the fields wider than they need to be for presentation
        For Each Control As Control In Me.Controls
            If TypeOf Control Is DataGridView Then
                If Control.Name <> "dgvSummary" Then ' This is not a list grid
                    For Each Column In CType(Control, DataGridView).Columns
                        Select Case Column.Name
                            Case "Total"
                                Column.Width = 40
                            Case "StageName"
                                Column.Width = 100
                                Column.HeaderText = "Stage"
                            Case "ClientName"
                                Column.Width = 100
                                Column.HeaderText = "Client"
                            Case "WorkType"
                                Column.Width = 84
                                Column.HeaderText = "Work Type"
                            Case "SchemeName"
                                Column.Width = 100
                                Column.HeaderText = "Scheme"
                            Case "Allocated"
                                Column.Width = 84
                            Case "Levied"
                                Column.Width = 90
                            Case "VanFeesApplied"
                                Column.Width = 120
                                Column.HeaderText = "Van Fees"
                            Case "Payment"
                                Column.Width = 70
                            Case "Visited"
                                Column.Width = 70
                            Case "AddConfirmed"
                                Column.Width = 70
                                Column.HeaderText = "Confirmed"
                            Case "ArrangementBroken"
                                Column.Width = 90
                                Column.HeaderText = "Broken"
                            Case "Covered"
                                Column.Width = 90
                            Case "PostcodeArea"
                                Column.Width = 55
                                Column.HeaderText = "Area"
                            Case Else
                                Column.Width = 40
                        End Select
                    Next Column
                End If
            End If
        Next Control
    End Sub

    Private Sub HighlightWarnings()
        Dim RowIndex As Integer

        For Each DataRow As DataRow In SummaryData.WarningSummaryDataView.ToTable.Rows
            ' Find the row with warnings on the datagridview
            For Each GridRow As DataGridViewRow In dgvSummary.Rows
                If GridRow.Cells("RowID").Value = DataRow.Item("RowID") Then RowIndex = GridRow.Index
            Next GridRow

            ' Loop through periods and highlight if any warnings exist
            For Each Item As DataColumn In DataRow.Table.Columns
                If DataColumnsList.Contains(Item.ColumnName) AndAlso DataRow.Item(Item.ColumnName) > 0 Then
                    dgvSummary.Rows(RowIndex).Cells(Item.ColumnName).Style.ForeColor = Color.Red
                End If
            Next Item
        Next DataRow
    End Sub

    Private Sub GetSelections()

        StageGridState.GetState()
        ClientGridState.GetState()
        WorkTypeGridState.GetState()
        SchemeGridState.GetState()
        AllocatedGridState.GetState()
        LeviedGridState.GetState()
        VanFeesAppliedGridState.GetState()
        PaymentGridState.GetState()
        VisitedGridState.GetState()
        AddConfirmedGridState.GetState()
        CoveredGridState.GetState()
        PostcodeAreaGridState.GetState()
        ArrangementBrokenGridState.GetState()

    End Sub

    Private Sub SetSelections()

        StageGridState.SetState()
        ClientGridState.SetState()
        WorkTypeGridState.SetState()
        SchemeGridState.SetState()
        AllocatedGridState.SetState()
        LeviedGridState.SetState()
        VanFeesAppliedGridState.SetState()
        PaymentGridState.SetState()
        VisitedGridState.SetState()
        AddConfirmedGridState.SetState()
        CoveredGridState.SetState()
        PostcodeAreaGridState.SetState()
        ArrangementBrokenGridState.SetState()

    End Sub

#End Region

#Region "Additional functionality"

    Private Sub cmdClearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClearAll.Click

        RemoveSelectionHandlers()

        cmdStageClear_Click(sender, New System.EventArgs)
        cmdClientClear_Click(sender, New System.EventArgs)
        cmdWorkTypeClear_Click(sender, New System.EventArgs)
        cmdSchemeClear_Click(sender, New System.EventArgs)
        cmdAllocatedClear_Click(sender, New System.EventArgs)
        cmdLeviedClear_Click(sender, New System.EventArgs)
        cmdVanFeesAppliedClear_Click(sender, New System.EventArgs)
        cmdPaymentClear_Click(sender, New System.EventArgs)
        cmdVisitedClear_Click(sender, New System.EventArgs)
        cmdAddConfirmedClear_Click(sender, New System.EventArgs)
        cmdCoveredClear_Click(sender, New System.EventArgs)
        cmdPostcodeAreaClear_Click(sender, New System.EventArgs)
        cmdArrangementBrokenClear_Click(sender, New System.EventArgs)

        AddSelectionHandlers()

        RefreshGrid()
    End Sub

    Private Sub radAbs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles radAbs.CheckedChanged
        RefreshGrid()
        If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"
    End Sub

    Private Sub radPerc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPerc.CheckedChanged
        RefreshGrid()
        If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"
    End Sub

    Private Sub chkTopClients_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles chkTopClients.CheckedChanged
        RefreshClientScroll = False

        If chkTopClients.Checked Then
            ' Not ideal but we need to refresh the client list before the main refresh to exclude any clients selected that would not be in the top list
            RemoveSelectionHandlers()

            GetSelections()
            SummaryData.GetClientList(ParamList)
            SetSelections()

            AddSelectionHandlers()
        End If

        RefreshGrid()

        RefreshClientScroll = True
    End Sub

    Private Sub cmdRefreshDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.Click

        If MsgBox("This could take several minutes, are you sure?", vbYesNo + vbDefaultButton2 + vbQuestion) = vbYes Then
            PreRefresh(Me)
            SummaryData.RefreshDatabase()
            RefreshGrid()
            PostRefresh(Me)
            MsgBox("Refresh complete.", vbOKOnly + vbInformation)
            SetcmdRefreshTip()
        End If

    End Sub

    Private Sub cmdRefreshDB_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.MouseLeave
        ' on XP machines there is a bug that prevents the tooltip showing a second time. This addresses the problem.
        ToolTip1.Active = False
        ToolTip1.Active = True
    End Sub

    Private Sub SetcmdRefreshTip()
        SummaryData.GetLastLoad()
        ToolTip1.SetToolTip(cmdRefreshDB, "Last refreshed at " & SummaryData.LastLoadDataView.Item(0).Item(0).ToString & " by " & SummaryData.LastLoadDataView.Item(0).Item(1).ToString)
    End Sub

    Private Sub cmsSummary_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsSummary.ItemClicked
        Select Case e.ClickedItem.Text
            Case "View Cases"
                ShowDetail()
            Case "Map Cases"
                ShowMap()
            Case "Copy"
                dgvSummary.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                Clipboard.SetDataObject(dgvSummary.GetClipboardContent())
            Case "Select All"
                dgvSummary.SelectAll()
        End Select
    End Sub

    Private Sub cmsList_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsList.ItemClicked
        ' Slightly more complicated than the summary grid as this event fires for all other datagridviews.
        Select Case e.ClickedItem.Text
            Case "Copy"
                sender.SourceControl.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                Clipboard.SetDataObject(sender.SourceControl.GetClipboardContent())
            Case "Select All"
                sender.SourceControl.SelectAll()
        End Select
    End Sub

    Private Sub cmsForm_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsForm.ItemClicked
        Select Case e.ClickedItem.Text
            Case "Bailiff View"
                If FormOpen("frmBailiffSummary") Then
                    frmBailiffSummary.Activate()
                Else
                    frmBailiffSummary.ShowDialog()
                End If
        End Select
    End Sub

    Private Sub ConsortiumContextSubMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ScrollbarSet As Boolean = False

        RemoveSelectionHandlers()

        SummaryData.ConsortiumDataView.RowFilter = "Consortium = '" & CType(sender, ToolStripItem).Text & "'"

        For Each ConsortiumDataRow As DataRow In SummaryData.ConsortiumDataView.ToTable.Rows
            For Each GridDataRow As DataGridViewRow In dgvClientName.Rows
                If GridDataRow.Cells("ClientName").Value = ConsortiumDataRow.Item("ClientName") Then
                    GridDataRow.Selected = True
                    If Not ScrollbarSet Then
                        dgvClientName.FirstDisplayedScrollingRowIndex = GridDataRow.Index
                        ScrollbarSet = True
                    End If
                End If
            Next GridDataRow

            ScrollbarSet = False

            For Each GridDataRow As DataGridViewRow In dgvSchemeName.Rows
                If GridDataRow.Cells("SchemeName").Value = ConsortiumDataRow.Item("SchemeName") Then
                    GridDataRow.Selected = True
                    If Not ScrollbarSet Then
                        dgvSchemeName.FirstDisplayedScrollingRowIndex = GridDataRow.Index
                        ScrollbarSet = True
                    End If
                End If
            Next GridDataRow
        Next ConsortiumDataRow

        AddSelectionHandlers()
        RefreshGrid()
    End Sub

    Private Function GetStages() As String()
        Dim SelectedSorts As New List(Of String)
        GetStages = Nothing

        For Each Item As DataGridViewRow In dgvStageName.SelectedRows
            SelectedSorts.Add(Item.Cells("StageName").Value.ToString)
        Next Item

        If Not IsNothing(SelectedSorts) Then GetStages = SelectedSorts.ToArray

    End Function
#End Region
 
End Class

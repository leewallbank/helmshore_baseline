﻿Public Class diaBailiff
    Private BailiffData As New clsBailiffData

    Public Sub AddCases(ByVal ParamList() As String)
        PreRefresh(Me)

        BailiffData.GetBailiffs(ParamList)
        dgvBailiff.DataSource = BailiffData.BailiffDataView

        FormatGridColumns(dgvBailiff)

        PostRefresh(Me)
    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        cmsBailiff.Items.Add("View Cases")

    End Sub

    Private Sub dgvBailiff_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvBailiff.MouseDown
        Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

        If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvBailiff.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
            dgvBailiff.Rows(hti.RowIndex).Selected = True
        End If

    End Sub

    Private Sub dgvBailiff_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvBailiff.MouseUp
        If e.Button = MouseButtons.Right Then cmsBailiff.Show(dgvBailiff, e.Location)
    End Sub

    Private Sub cmsBailiff_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsBailiff.ItemClicked
        Select Case e.ClickedItem.Text
            Case "View Cases"
                Dim Detail As New diaCaseDetail(Me)
                Detail.Text += " for " & dgvBailiff.SelectedRows(0).Cells("name_fore").Value & " " & dgvBailiff.SelectedRows(0).Cells("name_sur").Value
                Detail.AddCasesByBailiff(dgvBailiff.SelectedRows(0).Cells("BailiffID").Value)
                Detail.Show()
                Detail = Nothing
        End Select
    End Sub

End Class
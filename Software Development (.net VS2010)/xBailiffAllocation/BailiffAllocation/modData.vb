﻿Imports System.Configuration
Imports System.Data.SqlClient


Module modData
    Public DBCon As New SqlConnection

    Public Sub ConnectDb()
        Try
            If Not IsNothing(DBCon) Then
                'This is only necessary following an exception...
                If DBCon.State = ConnectionState.Open Then DBCon.Close()
            End If

            DBCon.ConnectionString = ConfigurationManager.ConnectionStrings("BailiffAllocation").ConnectionString
            DBCon.Open()

        Catch ex As System.Data.OleDb.OleDbException
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings("BailiffAllocation").ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings("BailiffAllocation").ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub

    Public Sub DisconnectDb()
        DBCon.Close()
        DBCon.Dispose()
    End Sub

    Public Sub ClearDataTable(ByRef DataSet As DataSet, ByVal DataTable As String)
        DataSet.Tables(DataTable).Clear()
        DataSet.Tables(DataTable).Reset()
    End Sub

    Public Sub LoadDataTable(ByVal Sql As String, ByRef DataSet As DataSet, ByVal DataTable As String, Optional ByVal Preserve As Boolean = False)
        Try
            Dim da As New SqlDataAdapter(Sql, DBCon)

            ConnectDb()

            If Preserve = False Then ClearDataTable(DataSet, DataTable)

            da.Fill(DataSet, DataTable)

            da.Dispose()
            da = Nothing

            DisconnectDb()

        Catch ex As Exception
            MsgBox("An error has occurred - the error message is:" & Chr(13) & Chr(13) & ex.Message)
        End Try

    End Sub

    Public Sub ExecStoredProc(ByVal SQL As String, Optional ByVal TimeoutSeconds As Short = 60)

        Try
            ConnectDb()

            Dim Comm As New SqlCommand(SQL, DBCon)
            Comm.CommandTimeout = TimeoutSeconds
            Comm.ExecuteNonQuery()

            DisconnectDb()

            Comm.Dispose()
            Comm = Nothing

        Catch ex As Exception
            MsgBox("An error has occurred - the error message is:" & Chr(13) & Chr(13) & ex.Message)
        End Try

    End Sub

    Public Function GetStartScreen() As String
        GetStartScreen = ExecProc("EXEC GetStartScreen")
    End Function

    Public Function GetPeriodNames(ByVal StageList As String) As String()
        GetPeriodNames = ExecProc("EXEC GetPeriodNames '" & StageList & "'").Split(",")
    End Function

    Private Function ExecProc(ByVal SQL As String) As String
        ExecProc = Nothing

        Try
            ConnectDb()
            Dim reader As SqlDataReader
            Dim Comm As New SqlCommand(SQL, DBCon)

            reader = Comm.ExecuteReader

            If reader.HasRows Then
                reader.Read()
                ExecProc = reader(0)
            End If

            DisconnectDb()

            Comm.Dispose()
            Comm = Nothing

        Catch ex As Exception
            MsgBox("An error has occurred - the error message is:" & Chr(13) & Chr(13) & ex.Message)
        End Try
    End Function
End Module

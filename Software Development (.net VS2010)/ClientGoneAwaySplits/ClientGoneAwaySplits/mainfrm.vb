Public Class mainfrm
    Dim first_letter As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim days As Integer = Weekday(Now)
        date_picker.Value = DateAdd(DateInterval.Day, -days, Now)
        selected_cl_name = ""
    End Sub

    Private Sub retnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles retnbtn.Click
        If selected_cl_name = "" Then
            MsgBox("Select client before doing the splits")
            Exit Sub
        End If
        disable_buttons()
        ProgressBar1.Value = 5
        retn_month = Format(date_picker.Value, "MM_MMM")
        retn_day = Format(date_picker.Value, "dd_ddd")
        Dim files_found As Boolean = False
        'get all csids for branch=1
        param2 = "select _rowid from clientScheme where branchID = 1 and clientID = " & selected_cl_id
        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        Dim cs_rows As Integer = no_of_rows - 1
        Dim idx As Integer
        files_found = False
        For idx = 0 To cs_rows
            selected_csid = cs_ds.Tables(0).Rows(idx).Item(0)
            'get remittance number
            param2 = "select _rowid from Remit where clientschemeID = " & selected_csid & _
            " and date = '" & Format(date_picker.Value, "yyyy-MM-dd") & "'"
            Dim remit_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim remit_no As Integer = remit_dataset.Tables(0).Rows(0).Item(0)
            file_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                                       retn_day & "\Remittances\" & Format(remit_no, "#") & "\Returns\"
            Dim file_name, client_ref As String
            'create directories for returns
            'just need goneaway for option 2
            Dim dir_name As String
            If op2rbtn.Checked Then
                dir_name = file_path & "Gone Away"
                Try
                    Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                    If System.IO.Directory.Exists(dir_name) = False Then
                        di = System.IO.Directory.CreateDirectory(dir_name)
                    Else
                        System.IO.Directory.Delete(dir_name, True)
                        di = System.IO.Directory.CreateDirectory(dir_name)
                    End If
                Catch ex As Exception
                    MsgBox("Unable to create folder")
                    End
                End Try
            End If

            dir_name = file_path & "PIF"
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
            'for option 1 use Return for option 2 use Other
            If op1rbtn.Checked Then
                dir_name = file_path & "Return"
            Else
                dir_name = file_path & "Other"
            End If

            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
            Dim debtor, retn_codeid As Integer

            For Each foundFile As String In My.Computer.FileSystem.GetFiles _
            (file_path, FileIO.SearchOption.SearchTopLevelOnly, "*.pdf")
                'get debtor number
                files_found = True
                Try
                    ProgressBar1.Value += 5
                Catch ex As Exception
                    ProgressBar1.Value = 5
                End Try

                Try
                    debtor = Mid(foundFile, foundFile.Length - 11, 8)
                Catch ex As Exception
                    Continue For
                End Try
                If debtor < 0 Then
                    debtor = debtor * -1
                End If
                'get return code for this debtor
                param2 = "select return_codeID, client_ref, status_open_closed, status from Debtor " & _
                " where _rowid = " & debtor
                Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 0 Then
                    MsgBox("Unable to find case number" & debtor)
                    Exit Sub
                End If
                Dim retn_group As String = "Return"
                Dim status_open_closed As String = debtor_dataset.Tables(0).Rows(0).Item(2)
                If status_open_closed = "O" Then
                    Continue For
                End If
                retn_codeid = 0
                Dim status As String = debtor_dataset.Tables(0).Rows(0).Item(3)
                If status = "S" Then
                    retn_group = "PIF"
                Else
                    Try
                        retn_codeid = debtor_dataset.Tables(0).Rows(0).Item(0)
                    Catch ex As Exception
                        retn_codeid = 0
                    End Try
                End If

                'get retn_group for non-zero retn_codeid - now only required for option 2

                If retn_codeid > 0 And op2rbtn.Checked Then
                    retn_group = "Other"
                    param2 = "select fee_category from CodeReturns" & _
                    " where _rowid = " & retn_codeid
                    Dim cr_dataset As DataSet = get_dataset(param1, param2)
                    If no_of_rows = 1 Then
                        If cr_dataset.Tables(0).Rows(0).Item(0) = 1 Then
                            retn_group = "Gone Away"
                        End If
                    End If

                End If
                client_ref = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
                file_name = file_path & retn_group & "\" & client_ref & "-" & debtor & ".pdf"
                Try
                    My.Computer.FileSystem.CopyFile(foundFile, file_name)
                Catch ex As Exception

                End Try

            Next
        Next
      
        If files_found Then
            MsgBox("All reports copied to directories")
        Else
            MsgBox("No reports were found")
        End If
        Me.Close()
    End Sub

    Private Sub clntbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clntbtn.Click
        clntfrm.ShowDialog()
        cl_lbl.Text = selected_cl_name
    End Sub
    Private Sub disable_buttons()
        exitbtn.Enabled = False
        clntbtn.Enabled = False
        retnbtn.Enabled = False
    End Sub
    Private Sub enable_buttons()
        exitbtn.Enabled = True
        clntbtn.Enabled = True
        retnbtn.Enabled = True
    End Sub
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.retnbtn = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.date_picker = New System.Windows.Forms.DateTimePicker()
        Me.sch_lbl = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.clntbtn = New System.Windows.Forms.Button()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.cl_lbl = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.op1rbtn = New System.Windows.Forms.RadioButton()
        Me.op2rbtn = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(216, 371)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 4
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'retnbtn
        '
        Me.retnbtn.Location = New System.Drawing.Point(77, 303)
        Me.retnbtn.Name = "retnbtn"
        Me.retnbtn.Size = New System.Drawing.Size(125, 23)
        Me.retnbtn.TabIndex = 3
        Me.retnbtn.Text = "Split Returns"
        Me.retnbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(10, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = " "
        '
        'date_picker
        '
        Me.date_picker.Location = New System.Drawing.Point(77, 237)
        Me.date_picker.Name = "date_picker"
        Me.date_picker.Size = New System.Drawing.Size(125, 20)
        Me.date_picker.TabIndex = 2
        Me.date_picker.Value = New Date(2010, 5, 17, 0, 0, 0, 0)
        '
        'sch_lbl
        '
        Me.sch_lbl.AutoSize = True
        Me.sch_lbl.Location = New System.Drawing.Point(74, 88)
        Me.sch_lbl.Name = "sch_lbl"
        Me.sch_lbl.Size = New System.Drawing.Size(109, 13)
        Me.sch_lbl.TabIndex = 8
        Me.sch_lbl.Text = "All Branch 1 schemes"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(97, 221)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Select remit date"
        '
        'clntbtn
        '
        Me.clntbtn.Location = New System.Drawing.Point(66, 36)
        Me.clntbtn.Name = "clntbtn"
        Me.clntbtn.Size = New System.Drawing.Size(125, 23)
        Me.clntbtn.TabIndex = 0
        Me.clntbtn.Text = "Select Client"
        Me.clntbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 371)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 10
        '
        'cl_lbl
        '
        Me.cl_lbl.AutoSize = True
        Me.cl_lbl.Location = New System.Drawing.Point(63, 20)
        Me.cl_lbl.Name = "cl_lbl"
        Me.cl_lbl.Size = New System.Drawing.Size(95, 13)
        Me.cl_lbl.TabIndex = 11
        Me.cl_lbl.Text = " No selected client"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.op2rbtn)
        Me.GroupBox1.Controls.Add(Me.op1rbtn)
        Me.GroupBox1.Location = New System.Drawing.Point(53, 126)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 78)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Choose Option for folders"
        '
        'op1rbtn
        '
        Me.op1rbtn.AutoSize = True
        Me.op1rbtn.Checked = True
        Me.op1rbtn.Location = New System.Drawing.Point(6, 19)
        Me.op1rbtn.Name = "op1rbtn"
        Me.op1rbtn.Size = New System.Drawing.Size(79, 17)
        Me.op1rbtn.TabIndex = 0
        Me.op1rbtn.TabStop = True
        Me.op1rbtn.Text = "PIF  Return"
        Me.op1rbtn.UseVisualStyleBackColor = True
        '
        'op2rbtn
        '
        Me.op2rbtn.AutoSize = True
        Me.op2rbtn.Location = New System.Drawing.Point(6, 55)
        Me.op2rbtn.Name = "op2rbtn"
        Me.op2rbtn.Size = New System.Drawing.Size(131, 17)
        Me.op2rbtn.TabIndex = 1
        Me.op2rbtn.Text = "PIF  Other  GoneAway"
        Me.op2rbtn.UseVisualStyleBackColor = True
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(314, 429)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cl_lbl)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.clntbtn)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.sch_lbl)
        Me.Controls.Add(Me.date_picker)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.retnbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Client Split Gone aways"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents retnbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents date_picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents sch_lbl As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clntbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents cl_lbl As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents op2rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents op1rbtn As System.Windows.Forms.RadioButton

End Class

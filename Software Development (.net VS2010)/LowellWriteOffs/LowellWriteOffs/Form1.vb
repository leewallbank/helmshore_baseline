Imports System.IO

Public Class Form1
    Dim outfile As String
    Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
    Public ascii As New System.Text.ASCIIEncoding()
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim listID As Integer
        Try
            listID = list_tbox.Text
        Catch ex As Exception
            MsgBox("Invalid List ID")
            Exit Sub
        End Try
        Dim debtorID As Integer
        Try
            debtorID = case_tbox.Text
        Catch ex As Exception
            MsgBox("Example Case number is invalid")
            Exit Sub
        End Try
        'check list is on onestep
        param2 = "select objectRowId from ListDetail where listID = " & listID
        Dim list_ds As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("List " & listID & " does not exist on onestep")
            Me.Close()
            Exit Sub
        End If

        Dim listIDX As Integer
        Dim listRows As Integer = no_of_rows - 1
        Dim caseFound As Boolean = False
        Dim user As String = My.User.Name
        'check all cases have status C, S or X and that example case exists in the list
        For listIDX = 0 To listRows
            Try
                ProgressBar.Value = (listIDX / listRows) * 100
            Catch ex As Exception

            End Try

            Application.DoEvents()
            Dim listDebtorID As Integer = list_ds.Tables(0).Rows(listIDX).Item(0)
            If listDebtorID = debtorID Then
                caseFound = True
            End If
            param2 = "select status from Debtor where _rowid = " & listDebtorID
            Dim list2_ds As DataSet = get_dataset("onestep", param2)
            If list2_ds.Tables(0).Rows(0).Item(0) <> "C" And _
                list2_ds.Tables(0).Rows(0).Item(0) <> "S" And _
                list2_ds.Tables(0).Rows(0).Item(0) <> "F" And _
                list2_ds.Tables(0).Rows(0).Item(0) <> "X" Then
                MsgBox("Case No " & listDebtorID & " status is not C, S, F or X")
            End If
        Next

        If Not caseFound Then
            MsgBox("Example case No is NOT in the list!")
            Exit Sub
        End If
        ProgressBar.Value = 50
        'run RD229L crystal report
        Dim RD229LNreport = New RD229LN
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(listID)
        SetCurrentValuesForParameterField1(RD229LNreport, myArrayList1)

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD229LNreport)
        Dim FileName As String = "LowellWriteoffs_" & Format(Now, "yyyyMMdd") & ".csv"
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "CSV files |*.csv"
            .DefaultExt = ".csv"
            .OverwritePrompt = True
            .FileName = filename
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Application.DoEvents()
            'filename = Path.GetFileName(SaveFileDialog1.FileName)
            'FileName &= "LowellWriteoffs_" & Format(Now, "yyyyMMdd") & ".csv"
            RD229LNreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.CharacterSeparatedValues, SaveFileDialog1.FileName)
            MsgBox("Completed")
        Else
            MsgBox("Report not saved")
        End If
        Me.Close()
    End Sub

End Class

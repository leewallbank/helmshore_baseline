﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Interop.Word

Public Class Form1
    Private InputFilePath As String, FileName As String, FileExt As String


    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub convertbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles convertbtn.Click
        Dim file_name As String = ""
        Dim new_filename As String = ""
        Dim last_file_name As String = ""
        convertbtn.Enabled = False
        exitbtn.Enabled = False
        Dim debtor As Integer
        Dim FileDialog As New OpenFileDialog
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"

        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)
        Dim objWord As New Word.Application
        Dim Doc As New Word.Document
        Doc = objWord.Documents.Open(FileDialog.FileName)
        Dim contents As String = Doc.Content().Text


        Doc.Close()

        Dim start_idx As Integer
        Dim end_of_file As Boolean = False
        Dim letter_page_no As Integer
        Dim tif_filename As String
        'now get case number and rename files
        Dim page_no As Integer = 0
        While Not end_of_file
            start_idx = InStr(contents, "Rossref:")
            If start_idx = 0 Then
                end_of_file = True
                Exit While
            End If
            page_no += 1
            Try
                debtor = Mid(contents, start_idx + 8, 8)
            Catch ex As Exception
                MsgBox("Unable to read debtor at page no = " & page_no)
                Me.Close()
                Exit Sub
            End Try
            'get letter page no
            Try
                letter_page_no = Mid(contents, start_idx + 21, 2)
            Catch ex As Exception
                MsgBox("Unable to get letter page number on page " & page_no)
                Me.Close()
                Exit Sub
            End Try

            contents = Microsoft.VisualBasic.Right(contents, contents.Length - start_idx - 15)
            tif_filename = InputFilePath & FileName & "-" & Format(page_no, "000") & ".tif"
            'rename tif file to new name
            Dim tif_new_file_name As String = InputFilePath & debtor & "-doc-page" & Format(letter_page_no, "000") & ".tif"
            Try
                Rename(tif_filename, tif_new_file_name)
            Catch ex As Exception
                MsgBox("Unable to rename file " & tif_filename & vbNewLine & _
                       ex.Message)
                Me.Close()
                Exit Sub
            End Try
        End While

        MsgBox("Completed")
        Me.Close()
    End Sub




    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

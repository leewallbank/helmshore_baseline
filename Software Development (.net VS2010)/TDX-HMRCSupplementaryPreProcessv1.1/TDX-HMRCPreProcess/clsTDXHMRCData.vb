﻿Imports CommonLibrary

Public Class clsTDXHMRCData
    Public Function GetDebtorID(ByVal ClientRef As String) As String

        GetDebtorID = GetSQLResults("DebtRecovery", "SELECT d._rowID " & _
                                                    "FROM debtor AS d " & _
                                                    "INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID " & _
                                                    "WHERE d.client_ref = '" & ClientRef & "' " & _
                                                    "  AND d.status_open_closed = 'O' " & _
                                                    "  AND cs.clientID = 1736")

    End Function

    Public Function GetCaseBalance(ByVal DebtorID As String) As Decimal

        GetCaseBalance = GetSQLResults("DebtRecovery", "SELECT d.debt_amount " & _
                                                       "FROM debtor AS d " & _
                                                       "WHERE d._rowID = " & DebtorID)
    End Function
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.casetbox = New System.Windows.Forms.TextBox()
        Me.checkbtn = New System.Windows.Forms.Button()
        Me.nametbox = New System.Windows.Forms.TextBox()
        Me.namelbl = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.schemetbox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.clienttbox = New System.Windows.Forms.TextBox()
        Me.textdg = New System.Windows.Forms.DataGridView()
        Me.question_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.question = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.answer_text = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.qtc = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.midDTP = New System.Windows.Forms.DateTimePicker()
        Me.midlbl = New System.Windows.Forms.Label()
        Me.q31cbox = New System.Windows.Forms.ComboBox()
        Me.q31tbox = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.nosavebtn = New System.Windows.Forms.Button()
        Me.savebtn = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.insptimedtp = New System.Windows.Forms.DateTimePicker()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.inspdatedtp = New System.Windows.Forms.DateTimePicker()
        Me.yesnodg = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewCheckBoxColumn1 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.q3cbox = New System.Windows.Forms.ComboBox()
        Me.q3tbox = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.q2cbox = New System.Windows.Forms.ComboBox()
        Me.q2tbox = New System.Windows.Forms.TextBox()
        Me.tbox1 = New System.Windows.Forms.TextBox()
        Me.relationdg = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.answer_relationship = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.reptbtn = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.addrtbox = New System.Windows.Forms.TextBox()
        CType(Me.textdg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.qtc.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.yesnodg, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.relationdg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(93, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(99, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Enter Case Number"
        '
        'casetbox
        '
        Me.casetbox.Location = New System.Drawing.Point(92, 36)
        Me.casetbox.Name = "casetbox"
        Me.casetbox.Size = New System.Drawing.Size(100, 20)
        Me.casetbox.TabIndex = 1
        '
        'checkbtn
        '
        Me.checkbtn.Location = New System.Drawing.Point(217, 33)
        Me.checkbtn.Name = "checkbtn"
        Me.checkbtn.Size = New System.Drawing.Size(106, 23)
        Me.checkbtn.TabIndex = 2
        Me.checkbtn.Text = "Check onestep"
        Me.checkbtn.UseVisualStyleBackColor = True
        '
        'nametbox
        '
        Me.nametbox.Location = New System.Drawing.Point(348, 36)
        Me.nametbox.Name = "nametbox"
        Me.nametbox.ReadOnly = True
        Me.nametbox.Size = New System.Drawing.Size(176, 20)
        Me.nametbox.TabIndex = 3
        '
        'namelbl
        '
        Me.namelbl.AutoSize = True
        Me.namelbl.Location = New System.Drawing.Point(386, 20)
        Me.namelbl.Name = "namelbl"
        Me.namelbl.Size = New System.Drawing.Size(70, 13)
        Me.namelbl.TabIndex = 4
        Me.namelbl.Text = "Debtor Name"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(767, 20)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Scheme Name"
        '
        'schemetbox
        '
        Me.schemetbox.Location = New System.Drawing.Point(727, 36)
        Me.schemetbox.Name = "schemetbox"
        Me.schemetbox.ReadOnly = True
        Me.schemetbox.Size = New System.Drawing.Size(176, 20)
        Me.schemetbox.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(573, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(64, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Client Name"
        '
        'clienttbox
        '
        Me.clienttbox.Location = New System.Drawing.Point(535, 36)
        Me.clienttbox.Name = "clienttbox"
        Me.clienttbox.ReadOnly = True
        Me.clienttbox.Size = New System.Drawing.Size(176, 20)
        Me.clienttbox.TabIndex = 10
        '
        'textdg
        '
        Me.textdg.AllowUserToAddRows = False
        Me.textdg.AllowUserToDeleteRows = False
        Me.textdg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.textdg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.question_no, Me.question, Me.answer_text})
        Me.textdg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.textdg.Location = New System.Drawing.Point(3, 3)
        Me.textdg.Name = "textdg"
        Me.textdg.Size = New System.Drawing.Size(919, 496)
        Me.textdg.TabIndex = 13
        '
        'question_no
        '
        Me.question_no.HeaderText = "Question No"
        Me.question_no.Name = "question_no"
        Me.question_no.ReadOnly = True
        Me.question_no.Width = 60
        '
        'question
        '
        Me.question.HeaderText = "Question"
        Me.question.Name = "question"
        Me.question.ReadOnly = True
        Me.question.Width = 250
        '
        'answer_text
        '
        Me.answer_text.HeaderText = "Answer"
        Me.answer_text.Name = "answer_text"
        Me.answer_text.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.answer_text.Width = 500
        '
        'qtc
        '
        Me.qtc.Controls.Add(Me.TabPage1)
        Me.qtc.Controls.Add(Me.TabPage2)
        Me.qtc.Location = New System.Drawing.Point(96, 88)
        Me.qtc.Name = "qtc"
        Me.qtc.SelectedIndex = 0
        Me.qtc.Size = New System.Drawing.Size(933, 572)
        Me.qtc.TabIndex = 14
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.textdg)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(925, 502)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Text Answers"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.midDTP)
        Me.TabPage2.Controls.Add(Me.midlbl)
        Me.TabPage2.Controls.Add(Me.q31cbox)
        Me.TabPage2.Controls.Add(Me.q31tbox)
        Me.TabPage2.Controls.Add(Me.TextBox3)
        Me.TabPage2.Controls.Add(Me.nosavebtn)
        Me.TabPage2.Controls.Add(Me.savebtn)
        Me.TabPage2.Controls.Add(Me.Label8)
        Me.TabPage2.Controls.Add(Me.insptimedtp)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.inspdatedtp)
        Me.TabPage2.Controls.Add(Me.yesnodg)
        Me.TabPage2.Controls.Add(Me.Label6)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.Label2)
        Me.TabPage2.Controls.Add(Me.q3cbox)
        Me.TabPage2.Controls.Add(Me.q3tbox)
        Me.TabPage2.Controls.Add(Me.TextBox2)
        Me.TabPage2.Controls.Add(Me.q2cbox)
        Me.TabPage2.Controls.Add(Me.q2tbox)
        Me.TabPage2.Controls.Add(Me.tbox1)
        Me.TabPage2.Controls.Add(Me.relationdg)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(925, 546)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Multiple Choice answers"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'midDTP
        '
        Me.midDTP.Location = New System.Drawing.Point(744, 30)
        Me.midDTP.Name = "midDTP"
        Me.midDTP.Size = New System.Drawing.Size(134, 20)
        Me.midDTP.TabIndex = 35
        '
        'midlbl
        '
        Me.midlbl.AutoSize = True
        Me.midlbl.Location = New System.Drawing.Point(785, 14)
        Me.midlbl.Name = "midlbl"
        Me.midlbl.Size = New System.Drawing.Size(77, 13)
        Me.midlbl.TabIndex = 34
        Me.midlbl.Text = "Moved in Date"
        '
        'q31cbox
        '
        Me.q31cbox.AutoCompleteCustomSource.AddRange(New String() {"Good", "Poor state of repair", "Winterised"})
        Me.q31cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.q31cbox.FormattingEnabled = True
        Me.q31cbox.Items.AddRange(New Object() {"Good", "Poor state of repair", "Winterised", "Answer required"})
        Me.q31cbox.Location = New System.Drawing.Point(370, 506)
        Me.q31cbox.Name = "q31cbox"
        Me.q31cbox.Size = New System.Drawing.Size(314, 21)
        Me.q31cbox.TabIndex = 33
        '
        'q31tbox
        '
        Me.q31tbox.Location = New System.Drawing.Point(106, 506)
        Me.q31tbox.Name = "q31tbox"
        Me.q31tbox.ReadOnly = True
        Me.q31tbox.Size = New System.Drawing.Size(247, 20)
        Me.q31tbox.TabIndex = 32
        Me.q31tbox.Text = "2"
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(44, 506)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(48, 20)
        Me.TextBox3.TabIndex = 31
        Me.TextBox3.Text = "31"
        '
        'nosavebtn
        '
        Me.nosavebtn.Enabled = False
        Me.nosavebtn.Location = New System.Drawing.Point(447, 420)
        Me.nosavebtn.Name = "nosavebtn"
        Me.nosavebtn.Size = New System.Drawing.Size(90, 40)
        Me.nosavebtn.TabIndex = 30
        Me.nosavebtn.Text = "Do not save and Clear"
        Me.nosavebtn.UseVisualStyleBackColor = True
        '
        'savebtn
        '
        Me.savebtn.Enabled = False
        Me.savebtn.Location = New System.Drawing.Point(803, 420)
        Me.savebtn.Name = "savebtn"
        Me.savebtn.Size = New System.Drawing.Size(75, 40)
        Me.savebtn.TabIndex = 29
        Me.savebtn.Text = "Save Details and Clear"
        Me.savebtn.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(499, 334)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(82, 13)
        Me.Label8.TabIndex = 28
        Me.Label8.Text = "Inspection Time"
        '
        'insptimedtp
        '
        Me.insptimedtp.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.insptimedtp.Location = New System.Drawing.Point(499, 359)
        Me.insptimedtp.Name = "insptimedtp"
        Me.insptimedtp.Size = New System.Drawing.Size(101, 20)
        Me.insptimedtp.TabIndex = 27
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(496, 291)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(85, 13)
        Me.Label7.TabIndex = 26
        Me.Label7.Text = "Inspection Date "
        '
        'inspdatedtp
        '
        Me.inspdatedtp.Location = New System.Drawing.Point(476, 307)
        Me.inspdatedtp.Name = "inspdatedtp"
        Me.inspdatedtp.Size = New System.Drawing.Size(145, 20)
        Me.inspdatedtp.TabIndex = 25
        '
        'yesnodg
        '
        Me.yesnodg.AllowUserToAddRows = False
        Me.yesnodg.AllowUserToDeleteRows = False
        Me.yesnodg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.yesnodg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewCheckBoxColumn1})
        Me.yesnodg.Location = New System.Drawing.Point(10, 236)
        Me.yesnodg.Name = "yesnodg"
        Me.yesnodg.Size = New System.Drawing.Size(919, 253)
        Me.yesnodg.TabIndex = 24
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.HeaderText = "Question No"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 60
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.HeaderText = "Question"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 250
        '
        'DataGridViewCheckBoxColumn1
        '
        Me.DataGridViewCheckBoxColumn1.HeaderText = "Tick=Yes"
        Me.DataGridViewCheckBoxColumn1.Name = "DataGridViewCheckBoxColumn1"
        Me.DataGridViewCheckBoxColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridViewCheckBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.DataGridViewCheckBoxColumn1.Width = 60
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(473, 14)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(42, 13)
        Me.Label6.TabIndex = 23
        Me.Label6.Text = "Answer"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(225, 14)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 13)
        Me.Label5.TabIndex = 22
        Me.Label5.Text = "Question"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(37, 14)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 13)
        Me.Label2.TabIndex = 21
        Me.Label2.Text = "Question No"
        '
        'q3cbox
        '
        Me.q3cbox.AutoCompleteCustomSource.AddRange(New String() {"SOMR form signed", "SOMR Verbal declaration", "SOMR Third party info ie Neighbour/Site owner", "HUO form signed", "HUO Verbal declaration", "Occupant Refused to cooperate", " No answer ", "For Sale/Rent", "Visible signs of occupancy within 24 hours", "Vacant pitch"})
        Me.q3cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.q3cbox.FormattingEnabled = True
        Me.q3cbox.Items.AddRange(New Object() {"SOMR form signed", "SOMR Verbal declaration", "SOMR Third party info i.e. Neighbour/Site Owner", "HUO form signed", "HUO Verbal declaration", "Occupant Refused to cooperate", "No answer ", "For Sale/Rent", "Visible signs of occupancy within 24 hours", "Vacant pitch", "Answer required"})
        Me.q3cbox.Location = New System.Drawing.Point(381, 67)
        Me.q3cbox.Name = "q3cbox"
        Me.q3cbox.Size = New System.Drawing.Size(314, 21)
        Me.q3cbox.TabIndex = 20
        '
        'q3tbox
        '
        Me.q3tbox.Location = New System.Drawing.Point(117, 67)
        Me.q3tbox.Name = "q3tbox"
        Me.q3tbox.ReadOnly = True
        Me.q3tbox.Size = New System.Drawing.Size(247, 20)
        Me.q3tbox.TabIndex = 19
        Me.q3tbox.Text = "2"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(55, 67)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(48, 20)
        Me.TextBox2.TabIndex = 18
        Me.TextBox2.Text = "3"
        '
        'q2cbox
        '
        Me.q2cbox.AutoCompleteCustomSource.AddRange(New String() {"SOMR", "Holiday Use Only", "Unable to Confirm", "   "})
        Me.q2cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.q2cbox.FormattingEnabled = True
        Me.q2cbox.Items.AddRange(New Object() {"SOMR", "Holiday use only", "Unable to Confirm", "Answer required"})
        Me.q2cbox.Location = New System.Drawing.Point(381, 30)
        Me.q2cbox.Name = "q2cbox"
        Me.q2cbox.Size = New System.Drawing.Size(314, 21)
        Me.q2cbox.TabIndex = 17
        '
        'q2tbox
        '
        Me.q2tbox.Location = New System.Drawing.Point(117, 30)
        Me.q2tbox.Name = "q2tbox"
        Me.q2tbox.ReadOnly = True
        Me.q2tbox.Size = New System.Drawing.Size(247, 20)
        Me.q2tbox.TabIndex = 16
        Me.q2tbox.Text = "2"
        '
        'tbox1
        '
        Me.tbox1.Location = New System.Drawing.Point(55, 30)
        Me.tbox1.Name = "tbox1"
        Me.tbox1.ReadOnly = True
        Me.tbox1.Size = New System.Drawing.Size(48, 20)
        Me.tbox1.TabIndex = 15
        Me.tbox1.Text = "2"
        '
        'relationdg
        '
        Me.relationdg.AllowUserToAddRows = False
        Me.relationdg.AllowUserToDeleteRows = False
        Me.relationdg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.relationdg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.answer_relationship})
        Me.relationdg.Location = New System.Drawing.Point(6, 120)
        Me.relationdg.Name = "relationdg"
        Me.relationdg.Size = New System.Drawing.Size(913, 110)
        Me.relationdg.TabIndex = 14
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Question No"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 60
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "Question"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 250
        '
        'answer_relationship
        '
        Me.answer_relationship.HeaderText = "Relationship (Owner/Tenant/Other)"
        Me.answer_relationship.Items.AddRange(New Object() {"Owner", "Tenant", "Other"})
        Me.answer_relationship.Name = "answer_relationship"
        Me.answer_relationship.Width = 150
        '
        'reptbtn
        '
        Me.reptbtn.Location = New System.Drawing.Point(1042, 64)
        Me.reptbtn.Name = "reptbtn"
        Me.reptbtn.Size = New System.Drawing.Size(102, 23)
        Me.reptbtn.TabIndex = 15
        Me.reptbtn.Text = "Produce Report"
        Me.reptbtn.UseVisualStyleBackColor = True
        '
        'addrtbox
        '
        Me.addrtbox.Location = New System.Drawing.Point(348, 66)
        Me.addrtbox.Name = "addrtbox"
        Me.addrtbox.ReadOnly = True
        Me.addrtbox.Size = New System.Drawing.Size(555, 20)
        Me.addrtbox.TabIndex = 16
        '
        'Form1
        '
        Me.AcceptButton = Me.checkbtn
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1242, 672)
        Me.Controls.Add(Me.addrtbox)
        Me.Controls.Add(Me.reptbtn)
        Me.Controls.Add(Me.qtc)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.clienttbox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.schemetbox)
        Me.Controls.Add(Me.namelbl)
        Me.Controls.Add(Me.nametbox)
        Me.Controls.Add(Me.checkbtn)
        Me.Controls.Add(Me.casetbox)
        Me.Controls.Add(Me.Label1)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MHR Questions"
        CType(Me.textdg,System.ComponentModel.ISupportInitialize).EndInit
        Me.qtc.ResumeLayout(false)
        Me.TabPage1.ResumeLayout(false)
        Me.TabPage2.ResumeLayout(false)
        Me.TabPage2.PerformLayout
        CType(Me.yesnodg,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.relationdg,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents casetbox As System.Windows.Forms.TextBox
    Friend WithEvents checkbtn As System.Windows.Forms.Button
    Friend WithEvents nametbox As System.Windows.Forms.TextBox
    Friend WithEvents namelbl As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents schemetbox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents clienttbox As System.Windows.Forms.TextBox
    Friend WithEvents textdg As System.Windows.Forms.DataGridView
    Friend WithEvents qtc As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents relationdg As System.Windows.Forms.DataGridView
    Friend WithEvents q2tbox As System.Windows.Forms.TextBox
    Friend WithEvents tbox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents q3cbox As System.Windows.Forms.ComboBox
    Friend WithEvents q3tbox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents q2cbox As System.Windows.Forms.ComboBox
    Friend WithEvents yesnodg As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewCheckBoxColumn1 As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents inspdatedtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents insptimedtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents nosavebtn As System.Windows.Forms.Button
    Friend WithEvents savebtn As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents answer_relationship As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents question_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents question As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents answer_text As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents reptbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents q31cbox As System.Windows.Forms.ComboBox
    Friend WithEvents q31tbox As System.Windows.Forms.TextBox
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents addrtbox As System.Windows.Forms.TextBox
    Friend WithEvents midDTP As System.Windows.Forms.DateTimePicker
    Friend WithEvents midlbl As System.Windows.Forms.Label

End Class

﻿Imports CommonLibrary
Public Class Form1
    Dim upd_txt As String
    Dim questionNo As Integer = 1
    Dim debtorID As Integer
    Dim user As String = My.User.Name
    Dim idx As Integer = 0
    Dim nullDate As Date = CDate("Jan 1, 1900")
    Public ascii As New System.Text.ASCIIEncoding()
    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs)
        'upd_txt = "insert into MHRQuestions (mhr_questionNo,mhr_question,mhr_answerType) values (" & _
        '    "30,'Condition',1)"
        'update_sql(upd_txt)
        'upd_txt = "insert into MHRQuestions (mhr_questionNo,mhr_question,mhr_answerType) values (" & _
        '   "31,'Authority',2)"
        'update_sql(upd_txt)

        'upd_txt = "insert into MHRQuestions (mhr_questionNo,mhr_question,mhr_answerType) values (" & _
        '   "32,'Inspection Comments',2)"
        'update_sql(upd_txt)
        'upd_txt = "insert into MHRQuestions (mhr_questionNo,mhr_question,mhr_answerType) values (" & _
        '   "33,'Inspector Name',2)"
        'update_sql(upd_txt)
        'upd_txt = "insert into MHRQuestions (mhr_questionNo,mhr_question,mhr_answerType) values (" & _
        '   "99,'Moved in Date',3)"
        'update_sql(upd_txt)
        'Me.Close()

      
    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        casetbox.Text = ""
        'upd_txt = "delete from mhrAnswers where mhr_questionNo > 0"
        'update_sql(upd_txt)
    End Sub

    Private Sub checkbtn_Click(sender As System.Object, e As System.EventArgs) Handles checkbtn.Click

        Try
            debtorID = casetbox.Text
        Catch ex As Exception
            MsgBox("Not a valid number")
            Exit Sub
        End Try

        'check on onestep
        Dim caseArray As Object
        caseArray = GetSQLResultsArray("DebtRecovery", "select C.name,S.name,name_fore, name_sur,D.status,status_open_closed, D.address " & _
                                     " from Debtor D, client C, scheme S, clientscheme CS " & _
                                     " where D._rowID = " & debtorID & _
                                     " and D.clientschemeID = CS._rowID" & _
                                     " and CS.clientID = C._rowID " & _
                                     " and CS.schemeID = S._rowID")
        Dim clientName As String
        Try
            clientName = caseArray(0)
        Catch ex As Exception
            MsgBox("Case does not appear on onestep")
            Exit Sub
        End Try

        Dim schemeName As String
        schemeName = caseArray(1)
       
        Dim casename As String = ""
        Try
            casename = caseArray(2) & " "
        Catch ex As Exception

        End Try
        Try
            casename &= caseArray(3)
        Catch ex As Exception

        End Try
        Dim status As String = caseArray(4)
        Dim statusOpenClosed As String = caseArray(5)
        Dim address As String = caseArray(6)
        Dim newAddress As String = ""
        For addridx = 1 To address.Length
            If Mid(address, addridx, 1) = Chr(10) Or Mid(address, addridx, 1) = Chr(13) Then
                newAddress &= ", "
            Else
                newAddress &= Mid(address, addridx, 1)
            End If
        Next
        addrtbox.Text = newAddress

        clienttbox.Text = clientName
        schemetbox.Text = schemeName
        nametbox.Text = casename

        textdg.Rows.Clear()

        Dim question As String
        Dim question_dt As New DataTable
        LoadDataTable("FeesSQL", "select mhr_questionNo, mhr_question from mhrQuestions " & _
                      " where mhr_answertype = 2 " & _
                      " order by mhr_questionNo", question_dt, False)
        For Each questionRow In question_dt.Rows
            Dim questionNo As Integer = questionRow(0)
            question = questionRow(1)
            textdg.Rows.Add(questionNo, question)
        Next

        'populate question 1 answer
        textdg.Rows(0).Cells(2).Value = newAddress

        'get question 2 and 3
        question = GetSQLResults("FeesSQL", " select mhr_question from mhrQuestions where mhr_questionNo=2")
        q2tbox.Text = question

        midlbl.Visible = False
        midDTP.Visible = False

        question = GetSQLResults("FeesSQL", " select mhr_question from mhrQuestions where mhr_questionNo=3")
        q3tbox.Text = question

        relationdg.Rows.Clear()
        Dim relation_dt As New DataTable
        LoadDataTable("FeesSQL", "select mhr_questionNo, mhr_question from mhrQuestions " & _
                      " where mhr_answertype = 1 " & _
                      " and mhr_questionNo in (5,10,12) " & _
                      " order by mhr_questionNo", relation_dt, False)
        For Each multRow In relation_dt.Rows
            Dim questionNo As Integer = multRow(0)
            question = multRow(1)
            relationdg.Rows.Add(questionNo, question)
        Next

        yesnodg.Rows.Clear()
        Dim yesno_dt As New DataTable
        LoadDataTable("FeesSQL", "select mhr_questionNo, mhr_question from mhrQuestions " & _
                      " where mhr_answertype = 1 " & _
                      " and mhr_questionNo > 3 " & _
                      " and not(mhr_questionNo in (5,10,12,31)) " & _
                      " order by mhr_questionNo", yesno_dt, False)
        For Each yesnoRow In yesno_dt.Rows
            Dim questionNo As Integer = yesnoRow(0)
            question = yesnoRow(1)
            yesnodg.Rows.Add(questionNo, question)
        Next

        'get question 31
        question = GetSQLResults("FeesSQL", " select mhr_question from mhrQuestions where mhr_questionNo=31")
        q31tbox.Text = question

        savebtn.Enabled = True
        nosavebtn.Enabled = True

    End Sub

    Private Sub multdg_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles relationdg.CellContentClick

    End Sub

    Private Sub multdg_RowEnter(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles relationdg.RowEnter
        'multdg.Rows(e.RowIndex).Cells(2).ReadOnly = True
        'multdg.Rows(e.RowIndex).Cells(3).ReadOnly = True
        'multdg.Rows(e.RowIndex).Cells(4).ReadOnly = True
        'multdg.Rows(e.RowIndex).Cells(5).ReadOnly = True
        'Select Case multdg.Rows(e.RowIndex).Cells(0).Value
        '    Case 2
        '        multdg.Rows(e.RowIndex).Cells(3).ReadOnly = False
        '    Case 3
        '        multdg.Rows(e.RowIndex).Cells(4).ReadOnly = False
        '    Case 5
        '        multdg.Rows(e.RowIndex).Cells(5).ReadOnly = False
        '    Case Else
        '        multdg.Rows(e.RowIndex).Cells(2).ReadOnly = False
        'End Select

    End Sub

    Private Sub Button1_Click_1(sender As System.Object, e As System.EventArgs)
        'upd_txt = "insert into MHRQuestions (mhr_questionNo,mhr_question,mhr_answerType) values (" & _
        '    "3,'Action Status',1)"
        'update_sql(upd_txt)
        'Me.Close()
    End Sub

    Private Sub savebtn_Click(sender As System.Object, e As System.EventArgs) Handles savebtn.Click
        Dim answerBlank As Boolean = False
        If q2cbox.Text = "" Or q3cbox.Text = "" Or q31cbox.Text = "" Or q2cbox.Text = "Answer required" Or q3cbox.Text = "Answer required" Or q31cbox.Text = "Answer required" Then
            answerBlank = True
        End If


        Dim answer, question As String
        idx = 0
        For Each Row In relationdg.Rows
            answer = relationdg.Rows(idx).Cells(2).Value
            If answer = "" Then
                answerBlank = True
                Exit For
            End If
            idx += 1
        Next
        If answerBlank Then
            MsgBox("Question 2,3,31 and All Relationship questions have not been answered")
            Exit Sub
        End If

        'check not future date or more than 2 months ago
        If inspdatedtp.Value > Now Then
            MsgBox("Inspection date can't be in the future")
            Exit Sub
        End If
        Dim twoMonthsAgo As Date = DateAdd(DateInterval.Month, -2, Now)
        If inspdatedtp.Value < twoMonthsAgo Then
            MsgBox("Inspection date can't be more than 2 months ago")
            Exit Sub
        End If

        'Update mhrANSWERS
        idx = 0
        textdg.EndEdit()
        For Each Row In textdg.Rows
            questionNo = textdg.Rows(idx).Cells(0).Value
            question = textdg.Rows(idx).Cells(1).Value
            answer = textdg.Rows(idx).Cells(2).Value
            Dim amendedAnswer As String = ""
            'T81571 replace any single quotes with 3 single quotes
            For idx2 = 1 To Len(answer)
                If Mid(answer, idx2, 1) <> "'" Then
                    amendedAnswer &= Mid(answer, idx2, 1)
                End If
            Next

            upd_txt = "insert into mhrAnswers (mhr_debtorID,mhr_questionNo,mhr_answerText, mhr_createdby, mhr_createddate, mhr_reporteddate)" & _
                " values(" & debtorID & "," & questionNo & ",'" & amendedAnswer & "','" & user & "','" & _
                Format(Now, "yyyy-MM-dd") & "','" & Format(nullDate, "yyyy-MM-dd") & "')"
            update_sql(upd_txt)
            idx += 1
        Next

        'update question 2
        upd_txt = "insert into mhrAnswers (mhr_debtorID,mhr_questionNo,mhr_answerText, mhr_createdby, mhr_createddate, mhr_reporteddate)" & _
              " values(" & debtorID & ",2,'" & q2cbox.Text & "','" & user & "','" & _
              Format(Now, "yyyy-MM-dd") & "','" & Format(nullDate, "yyyy-MM-dd") & "')"
        update_sql(upd_txt)

        'update moved in date if SOMR selected
        If q2cbox.Text = "SOMR" Then
            upd_txt = "insert into mhrAnswers (mhr_debtorID,mhr_questionNo, mhr_answerdate, mhr_createdby, mhr_createddate, mhr_reporteddate)" & _
               " values(" & debtorID & ",99,'" & Format(midDTP.Value, "yyyy-MM-dd") & "','" & user & "','" & _
                Format(Now, "yyyy-MM-dd") & "','" & Format(nullDate, "yyyy-MM-dd") & "')"
            update_sql(upd_txt)
        End If

        'update question 3
        upd_txt = "insert into mhrAnswers (mhr_debtorID,mhr_questionNo,mhr_answerText, mhr_createdby, mhr_createddate, mhr_reporteddate)" & _
              " values(" & debtorID & ",3,'" & q3cbox.Text & "','" & user & "','" & _
              Format(Now, "yyyy-MM-dd") & "','" & Format(nullDate, "yyyy-MM-dd") & "')"
        update_sql(upd_txt)

        idx = 0
        relationdg.EndEdit()
        For Each Row In relationdg.Rows
            questionNo = relationdg.Rows(idx).Cells(0).Value
            question = relationdg.Rows(idx).Cells(1).Value
            answer = relationdg.Rows(idx).Cells(2).Value
            upd_txt = "insert into mhrAnswers (mhr_debtorID,mhr_questionNo,mhr_answerText, mhr_createdby, mhr_createddate, mhr_reporteddate)" & _
                " values(" & debtorID & "," & questionNo & ",'" & answer & "','" & user & "','" & _
               Format(Now, "yyyy-MM-dd") & "','" & Format(nullDate, "yyyy-MM-dd") & "')"
            update_sql(upd_txt)
            idx += 1
        Next

        idx = 0
        yesnodg.EndEdit()
        For Each Row In yesnodg.Rows
            questionNo = yesnodg.Rows(idx).Cells(0).Value
            question = yesnodg.Rows(idx).Cells(1).Value
            If yesnodg.Rows(idx).Cells(2).Value Then
                answer = "Yes"
            Else
                answer = "No"
            End If
            upd_txt = "insert into mhrAnswers (mhr_debtorID,mhr_questionNo,mhr_answerText, mhr_createdby, mhr_createddate, mhr_reporteddate)" & _
                " values(" & debtorID & "," & questionNo & ",'" & answer & "','" & user & "','" & _
                Format(Now, "yyyy-MM-dd") & "','" & Format(nullDate, "yyyy-MM-dd") & "')"
            update_sql(upd_txt)
            idx += 1
        Next

        'update inspection date and time
        upd_txt = "insert into mhrAnswers (mhr_debtorID,mhr_questionNo, mhr_answerdate, mhr_createdby, mhr_createddate, mhr_reporteddate)" & _
               " values(" & debtorID & ",35,'" & Format(inspdatedtp.Value, "yyyy-MM-dd") & Format(insptimedtp.Value, " HH:mm:ss") & "','" & user & "','" & _
                Format(Now, "yyyy-MM-dd") & "','" & Format(nullDate, "yyyy-MM-dd") & "')"
        update_sql(upd_txt)

        'update question 31
        upd_txt = "insert into mhrAnswers (mhr_debtorID,mhr_questionNo,mhr_answerText, mhr_createdby, mhr_createddate, mhr_reporteddate)" & _
              " values(" & debtorID & ",31,'" & q31cbox.Text & "','" & user & "','" & _
              Format(Now, "yyyy-MM-dd") & "','" & Format(nullDate, "yyyy-MM-dd") & "')"
        update_sql(upd_txt)

        reset_rows()
    End Sub

    Private Sub reset_rows()
        casetbox.Text = ""
        idx = 0
        For Each row In textdg.Rows
            textdg.Rows(idx).Cells(2).Value = ""
            idx += 1
        Next

        q2cbox.Text = "Answer required"
        q3cbox.Text = "Answer required"
        idx = 0
        For Each row In relationdg.Rows
            relationdg.Rows(idx).Cells(2).Value = ""
            idx += 1
        Next
        idx = 0
        For Each row In yesnodg.Rows
            yesnodg.Rows(idx).Cells(2).Value = False
            idx += 1
        Next
        q31cbox.Text = "Answer required"
        clienttbox.Text = ""
        schemetbox.Text = ""
        nametbox.Text = ""
        addrtbox.Text = ""
        savebtn.Enabled = False
        nosavebtn.Enabled = False

    End Sub
    
   

   
    Private Sub nosavebtn_Click(sender As System.Object, e As System.EventArgs) Handles nosavebtn.Click
        reset_rows()
    End Sub

    Private Sub casetbox_TextChanged(sender As System.Object, e As System.EventArgs) Handles casetbox.TextChanged

    End Sub

    Private Sub reptbtn_Click(sender As System.Object, e As System.EventArgs) Handles reptbtn.Click
        Dim outfile As String = ""
        
        'get all answers not yet reported
        Dim answerdt As New DataTable
        Dim updateDate As Date = Now
        LoadDataTable("FeesSQL", "select mhr_debtorID, Q.mhr_questionNo, mhr_question, mhr_answerText, mhr_answerDate " & _
                      " from mhrAnswers A, mhrQuestions Q " & _
                      " where A.mhr_questionNo = Q.mhr_questionNo " & _
                      " and mhr_reportedDate = '" & Format(nullDate, "yyyy-MM-dd") & "'" & _
                      " order by mhr_debtorID,Q.mhr_questionNo", answerdt, False)
        For Each row In answerdt.Rows
            Dim debtorID As Integer = row(0)
            Dim questionNo As Integer = row(1)
            Dim question As String = row(2)

            If questionNo = 35 Then
                Dim answerDate As Date = row(4)
                outfile &= debtorID & "|Inspection Date:" & Format(answerDate, "dd/MM/yyyy") & ";" & vbNewLine
                outfile &= debtorID & "|Inspection Time:" & Format(answerDate, "HH:mm") & ";" & vbNewLine
            ElseIf questionNo = 99 Then  'moved in date
                Dim answerDate As Date = row(4)
                outfile &= debtorID & "|Move in Date:" & Format(answerDate, "dd/MM/yyyy") & ";" & vbNewLine
            Else
                Dim answer As String = row(3)
                outfile &= debtorID & "|" & question & ":" & Trim(answer) & ";" & vbNewLine
            End If
        Next
        If outfile = "" Then
            MsgBox("Nothing to report")
        Else
            'update report date
            upd_txt = "update mhrAnswers set mhr_reportedDate = '" & Format(updateDate, "yyyy-MM-dd HH:mm:ss") & "'" & _
                " where mhr_reportedDate = '" & Format(nullDate, "yyyy-MM-dd") & "'"
            update_sql(upd_txt)

            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "TXT files |*.txt"
                .DefaultExt = ".txt"
                .OverwritePrompt = True
                .FileName = "MHR_upload_" & Format(Now, "yyyy-MM-dd") & ".txt"
            End With
            If Not SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                MsgBox("No file name selected for save ")
            Else
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False, ascii)
                MsgBox("File saved")
            End If
           
        End If
        Me.Close()
    End Sub

    Private Sub q2cbox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles q2cbox.SelectedIndexChanged
        If q2cbox.Text = "SOMR" Then
            midlbl.Visible = True
            midDTP.Visible = True
        Else
            midlbl.Visible = False
            midDTP.Visible = False
        End If
    End Sub

End Class

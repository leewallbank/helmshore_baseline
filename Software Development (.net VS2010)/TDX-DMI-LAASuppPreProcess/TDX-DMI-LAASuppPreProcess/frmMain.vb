﻿Imports CommonLibrary
Imports System.IO
Public Class frmMain
    Private TDXDMILAAData As New TDXDMILAAData
    Private InputFilePath As String, FileName As String, FileExt As String

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try

            Dim FileDialog As New OpenFileDialog
            Dim LineNumber As Integer
            Dim OutputLine As String
            Dim AuditLog As String, ErrorLog As String = "", InputLineArray() As String, DebtorID As String
            Dim AccountID = New List(Of String) ' added TS 08/Sep/2015. Request ref 57978

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            lblReadingFile.Visible = True
            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents)

            For Each OldFile As String In Directory.GetFiles(InputFilePath, FileName & "_*.txt")
                File.Delete(OldFile)
            Next OldFile

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                If LineNumber = 1 Then Continue For ' skip header line. This file has no footer.

                InputLine.Replace("|", "")
                OutputLine = ""
                InputLineArray = InputLine.Split(",")

                If UBound(InputLineArray) <> 13 Then
                    ErrorLog &= "Unexpected line length of " & (UBound(InputLineArray) + 1).ToString & " items found at line number " & LineNumber.ToString & ". Line not loaded." & vbCrLf
                    Continue For
                End If

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                AccountID.Add(InputLineArray(0))

                DebtorID = TDXDMILAAData.GetDebtorID(InputLineArray(1))

                If DebtorID <> "" Then
                    OutputLine &= DebtorID & "|"

                    OutputLine &= String.Join("", ToNote(InputLineArray(3), "Brand", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(4), "Opponent Name", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(5), "Proceeding Nature", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(6), "Court Number", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(7), "Court Name", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(8), "Solicitor", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(9), "Reason", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(10), "Additional Information", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(11), "Due Date", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(12), "Final Bill Date", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(13), "Original Balance", ";"))

                    OutputLine &= vbCrLf

                Else
                    ErrorLog &= "Cannot find open DebtorID for client ref " & InputLineArray(1).Replace("""", "") & " at line number " & LineNumber.ToString & vbCrLf
                End If

                AppendToFile(InputFilePath & FileName & "_PreProcessed.txt", OutputLine)

            Next InputLine

            InputLineArray = FileContents(0).Split(",") ' Check header record case count
            If AccountID.ToArray.Distinct.ToArray.Count <> InputLineArray(3).Replace("""", "") Then ErrorLog &= "Expecting " & InputLineArray(3) & " cases, found " & (AccountID.ToArray.Distinct.ToArray.Count + 1).ToString

            AuditLog = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
            AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

            AuditLog &= "Number of new supplementary details: " & UBound(FileContents).ToString & vbCrLf ' No -1 as per the other supplementary pre processors as this file has no footers

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
                MessageBox.Show("Errors found.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            MessageBox.Show("Pre-processing complete.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("notepad.exe", """" & InputFilePath & FileName & "_PreProcessed.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
            If File.Exists(InputFilePath & FileName & "_Balance_discrepancies.xls") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Balance_discrepancies.xls")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class

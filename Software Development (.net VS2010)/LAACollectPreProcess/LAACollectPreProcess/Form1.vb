Imports System.Xml.Schema
Imports System.Configuration


Public Class Form1
    Dim comments As String
    Dim exceptions_file As String
    Dim xml_valid As Boolean = True
    Dim new_file As String = ""
    'Dim outcome_file As String
    Dim input_file As String
    Dim write_audit As Boolean = False
    Dim record As String = ""
    Dim filename, filename_id As String
    Dim first_error As Boolean = True
    Dim first_change As Boolean = True
    Dim on_onestep, equity_check As Boolean
    Dim contrib_id As Double
    Dim flag_id As String
    Dim no_changes As Integer = 0
    Dim changes As Integer = 0
    Dim spaces As Integer = 250
    Dim test_date As Date
    Dim appeal_case As Boolean
    Dim cf_sep As String = "|"
    Dim dr_sep As String = "*"
    Dim ma_sep As String = "`"
    Dim fn_sep As String = "~"
    Dim rd_sep As String = "#"
    Dim rk_sep As String = "%"
    Dim sc_sep As String = "<"
    Dim wd_sep As String = ">"

    Dim cf_cases As Integer = 0
    Dim dr_cases As Integer = 0
    Dim ma_cases As Integer = 0
    Dim fn_cases As Integer = 0
    Dim rd_cases As Integer = 0
    Dim rk_cases As Integer = 0
    Dim sc_cases As Integer = 0
    Dim wd_cases As Integer = 0
    'T60888 add costs
    Dim cs_cases As Integer = 0

    Dim cf_heading As String = ""
    Dim dr_heading As String = ""
    Dim ma_heading As String = ""
    Dim fn_heading As String = ""
    Dim rd_heading As String = ""
    Dim rk_heading As String = ""
    Dim sc_heading As String = ""
    Dim wd_heading As String = ""

    Dim ascii As New System.Text.ASCIIEncoding()
    Dim debtor_name, accountNumber As String
    Dim debt_addr1, debt_addr2, debt_addr3, debt_addr4, debt_postcode, debt_phone As String
    Dim FCFullName, certificateNumber, proceedingNature, reason As String
    Dim courtNumber, courtName, dateRevoked, solicitor, instructions, debtType, debtCode, amount As String
    Dim finalBilldate As String
   
    Dim valid_recs As Integer = 0
    Dim rej_recs As Integer = 0
    Dim no_of_errors As Integer = 0
    Dim contrib_recs As Integer

    Dim change_found As Boolean
    Private Sub readbtn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn1.Click
        conn_open = False
        Dim msg As String = ""
        readbtn1.Enabled = False
        exitbtn.Enabled = False
        Try
            With OpenFileDialog1
                .Title = "Read XML file"
                .Filter = "XML file|*.xml"
                .FileName = ""
                .CheckFileExists = True
            End With
            Dim xml_dataSet As New DataSet()
            Dim doc As New Xml.XmlDocument()

            Dim heading As String = "Name" & "|" & "Address1" & "|" & "Address2" & _
                        "|" & "Address3" & "|" & "Address4" & "|" & "PostCode" & "|" & "Tel No" & _
                        "|" & "CWXAccountNumber" & "|" & "FCFullname" & "|" & "CertificateNumber" & "|" & _
                        "ProceedingNature" & "|" & "CourtNumber" & "|" & "Courtname" & "|" & "DateRevoked" & _
                        "|" & "Solicitor" & "|" & "FinalBilldate" & "|" & "DebtType" & _
                        "|" & "Amount" & "|" & "Reason" & "|" & "Instructions" & "|" & "Comments" & vbNewLine

            prod_run = False
            Dim idx As Integer = 0
            cf_outfile = Nothing
            dr_outfile = Nothing
            ma_outfile = Nothing
            fn_outfile = Nothing
            rd_outfile = Nothing
            rk_outfile = Nothing
            sc_outfile = Nothing
            wd_outfile = Nothing
            'T60888
            cs_outfile = Nothing

            If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                validate_xml_file()
                If xml_valid = False Then
                    MsgBox("XML file has failed validation - see error file")
                    If MsgBox("Do you want to continue processing the file?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                        Me.Close()
                        Exit Sub
                    End If
                End If
                Try
                    filename = OpenFileDialog1.FileName
                    Dim ln2 As Integer = Microsoft.VisualBasic.Len(filename)
                    reset_fields()
                    Dim rdr_name As String = ""
                    Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
                    reader.Read()
                    ProgressBar1.Value = 5
                    Dim record_count As Integer = 0

                    While (reader.ReadState <> Xml.ReadState.EndOfFile)
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        Try
                            rdr_name = reader.Name
                        Catch
                            Continue While
                        End Try

                        'Note ReadElementContentAsString moves focus to next element so don't need read
                        ProgressBar1.Value = record_count
                        Application.DoEvents()
                        Select Case rdr_name
                            Case "contribution_file"
                                filename_id = reader.Item(0)
                                'Dim env_str As String = ""
                                'Try
                                '    env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
                                'Catch ex As Exception
                                '    prod_run = False
                                'End Try
                                'If env_str = "Prod" Then
                                '    prod_run = True
                                'End If
                                'If prod_run = False Then
                                '    MsgBox("TEST RUN ONLY")
                                'End If
                                reader.Read()

                            Case "ns1:CONTRIBUTIONS_LIST"
                                reader.Read()
                            Case "ns1:CONTRIBUTIONS"
                                record_count += 1
                                If record_count > 100 Then
                                    record_count = 0
                                End If
                                If idx > 0 Then
                                    end_of_record()
                                    input_file = input_file & record & vbNewLine
                                    input_file = record & vbNewLine
                                    'If validate_record() = False Then
                                    '    rej_recs += 1
                                    'Else
                                    valid_recs += 1
                                    'End If
                                    reset_fields()
                                End If
                                idx += 1
                                reader.Read()
                                While reader.Name <> "ns1:CONTRIBUTIONS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "ns1:ClientDetails"
                                            reader.Read()
                                            While reader.Name <> "ns1:ClientDetails"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "ns1:Name"
                                                        debtor_name = reader.ReadElementContentAsString
                                                    Case "ns1:Address1"
                                                        debt_addr1 = reader.ReadElementContentAsString
                                                    Case "ns1:Address2"
                                                        debt_addr2 = reader.ReadElementContentAsString
                                                    Case "ns1:Address3"
                                                        debt_addr3 = reader.ReadElementContentAsString
                                                    Case "ns1:Address4"
                                                        debt_addr4 = reader.ReadElementContentAsString
                                                    Case "ns1:PostCode"
                                                        debt_postcode = reader.ReadElementContentAsString
                                                    Case "ns1:TelephoneNumber"
                                                        debt_phone = reader.ReadElementContentAsString
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case "ns1:CaseDetails"
                                            reader.Read()
                                            While reader.Name <> "ns1:CaseDetails"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "ns1:CWXAccountNumber"
                                                        accountNumber = reader.ReadElementContentAsString
                                                    Case "ns1:FCFullName"
                                                        FCFullName = Trim(reader.ReadElementContentAsString)
                                                        If FCFullName.Length > 0 Then
                                                            comments = comments & "FCFullName:" & FCFullName & ";"
                                                            space_comments()
                                                        End If
                                                    Case "ns1:CertificateNumber"
                                                        certificateNumber = reader.ReadElementContentAsString
                                                    Case "ns1:ProceedingDetails"
                                                        reader.Read()
                                                        While reader.Name <> "ns1:ProceedingDetails"
                                                            rdr_name = reader.Name
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            Select Case rdr_name
                                                                Case "ns1:ProceedingNature"
                                                                    proceedingNature = Trim(reader.ReadElementContentAsString)
                                                                    If proceedingNature.Length > 0 Then
                                                                        comments = comments & "ProceedingNature:" & proceedingNature & ";"
                                                                        space_comments()
                                                                    End If
                                                                Case "ns1:Court"
                                                                    reader.Read()
                                                                    While reader.Name <> "ns1:Court"
                                                                        rdr_name = reader.Name
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        Select Case rdr_name
                                                                            Case "ns1:CourtNumber"
                                                                                courtNumber = reader.ReadElementContentAsString
                                                                            Case "ns1:CourtName"
                                                                                courtName = reader.ReadElementContentAsString
                                                                            Case Else
                                                                                MsgBox("What is this tag?" & rdr_name)
                                                                                reader.Read()
                                                                        End Select
                                                                    End While
                                                                Case "ns1:DateRevoked"
                                                                    dateRevoked = reader.ReadElementContentAsString
                                                                Case "ns1:Solicitor"
                                                                    solicitor = Trim(reader.ReadElementContentAsString)
                                                                    If solicitor.Length > 0 Then
                                                                        solicitor = "Solicitor:" & solicitor
                                                                    End If
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                    reader.Read()
                                                            End Select
                                                        End While
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case "ns1:TransactionType"
                                            reader.Read()
                                            While reader.Name <> "ns1:TransactionType"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "ns1:FinalBillDate"
                                                        finalBilldate = reader.ReadElementContentAsString
                                                    Case "ns1:DebtType"
                                                        debtType = Trim(reader.ReadElementContentAsString)
                                                        If debtType.Length > 0 Then
                                                            comments = comments & "DebtType:" & debtType & ";"
                                                            space_comments()
                                                        End If
                                                        'validate debt type to get debt_code
                                                        debtCode = validate_debtType(debtType)
                                                        If debtCode = "??" Then
                                                            MsgBox("Debt type code not found for - " & debtType)
                                                            Exit While
                                                        End If
                                                    Case "ns1:Amount"
                                                        amount = reader.ReadElementContentAsString
                                                    Case "ns1:Reason"
                                                        reason = Trim(reader.ReadElementContentAsString)
                                                        If reason.Length > 0 Then
                                                            comments = comments & "Reason:" & reason & ";"
                                                            space_comments()
                                                        End If
                                                    Case "ns1:Instructions"
                                                        instructions = Trim(reader.ReadElementContentAsString)
                                                        If instructions.Length > 0 Then
                                                            comments = comments & "Instructions:" & instructions & ";"
                                                            space_comments()
                                                        End If
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case Else
                                MsgBox("what is this? " & rdr_name)
                                reader.Read()
                        End Select
                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Else
                MsgBox("XML file not processed")
                Exit Sub
            End If
            'write file including last applicant
            end_of_record()
            input_file = input_file & record & vbNewLine
            'If validate_record() = False Then
            '    rej_recs += 1
            'Else
            valid_recs += 1

            Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4)
            If cf_outfile <> Nothing Then
                msg = "CF CONFLOC cases = " & cf_cases & vbNewLine
                My.Computer.FileSystem.WriteAllText(new_file & "_preprocessCF.txt", cf_outfile, False, ascii)
            End If
            If dr_outfile <> Nothing Then
                msg &= "DR Costs cases = " & dr_cases & vbNewLine
                My.Computer.FileSystem.WriteAllText(new_file & "_preprocessDR.txt", dr_outfile, False, ascii)
            End If
            If ma_outfile <> Nothing Then
                msg &= "MA DAMAGE AWARD cases = " & ma_cases & vbNewLine
                My.Computer.FileSystem.WriteAllText(new_file & "_preprocessMA.txt", ma_outfile, False, ascii)
            End If
            If fn_outfile <> Nothing Then
                msg &= "FN ????? = " & fn_cases & vbNewLine
                My.Computer.FileSystem.WriteAllText(new_file & "_preprocessFN.txt", fn_outfile, False, ascii)
            End If
            If rd_outfile <> Nothing Then
                msg &= "RD Recovery of Defence cases = " & rd_cases & vbNewLine
                My.Computer.FileSystem.WriteAllText(new_file & "_preprocessRD.txt", rd_outfile, False, ascii)
            End If
            If rk_outfile <> Nothing Then
                msg &= "RK Revoked Leagal Aid cases = " & rk_cases & vbNewLine
                My.Computer.FileSystem.WriteAllText(new_file & "_preprocessRK.txt", rk_outfile, False, ascii)
            End If
            If sc_outfile <> Nothing Then
                msg &= "SC Statutory Charge cases = " & sc_cases & vbNewLine
                My.Computer.FileSystem.WriteAllText(new_file & "_preprocessSC.txt", sc_outfile, False, ascii)
            End If
            If wd_outfile <> Nothing Then
                msg &= "WD Staff Debt cases = " & wd_cases & vbNewLine
                My.Computer.FileSystem.WriteAllText(new_file & "_preprocessWD.txt", wd_outfile, False, ascii)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        ProgressBar1.Value = 100
        MsgBox(msg)
        Me.Close()
    End Sub
    Private Sub space_comments()
        Dim spaces As Integer
        'remove any non ascii character from comments
        Dim comm_idx As Integer
        Dim new_comments As String = ""
        For comm_idx = 1 To comments.Length
            If AscW(Mid(comments, comm_idx, 1)) > 127 Then
                new_comments &= " "
            Else
                new_comments &= Mid(comments, comm_idx, 1)
            End If
        Next
        comments = new_comments
        Try

            spaces = (Int((comments.Length / 250)) + 1) * 250
            comments = comments & Space(spaces - comments.Length)
        Catch ex As Exception
            MsgBox("error in spacing comments")
        End Try

    End Sub
    Private Sub save_error(ByVal error_text As String)
        If error_text = "invalid account number" Then
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).accountNumber = Nothing
            error_table(no_of_errors).error_text = error_text
        Else
            no_of_errors += 1
            error_table(no_of_errors).contrib_id = contrib_id
            error_table(no_of_errors).accountNumber = accountNumber
            error_table(no_of_errors).error_text = error_text
        End If


    End Sub
    'Private Function validate_record() As Boolean
    '    'Dim orig_no_of_errors As Integer = no_of_errors
    '    'If accountNumber = "" Then
    '    '    save_error("invalid account number")
    '    'End If

    '    'If debtor_name = "" Then
    '    '    save_error("invalid name")
    '    'End If

    '    'If orig_no_of_errors = no_of_errors Then
    '    '    Return (True)
    '    'Else
    '    '    If no_of_errors = UBound(error_table) Then
    '    '        ReDim Preserve error_table(no_of_errors + 10)
    '    '    End If
    '    '    Return (False)
    '    'End If

    'End Function

    Private Sub Write_ack(ByVal writer As Xml.XmlWriter, _
    ByVal filename_id_tag As String, ByVal valid_recs As Integer, ByVal rej_recs As Integer, _
    ByVal no_of_errors As Integer)
        writer.WriteStartElement("CONTRIBUTIONS_FILE_ACK")
        writer.WriteStartElement("FILENAME")
        writer.WriteAttributeString("ID", filename_id_tag)
        writer.WriteString(filename_id)
        writer.WriteEndElement()
        writer.WriteElementString("DATE_LOADED", Format(Now, "dd-MM-yyyy"))
        writer.WriteElementString("NO_OF_RECORDS_LOADED", valid_recs)
        writer.WriteElementString("NO_OF_RECORDS_REJECTED", rej_recs)

        Dim idx As Integer
        Dim contrib_id As Double = 0
        Dim last_contrib_id As Double = 0
        Dim applicant_id As Double = 0
        Dim accountNumber As Double = 0
        Dim error_text As String = ""
        For idx = 1 To no_of_errors
            If idx = 1 Then
                writer.WriteStartElement("REJECTED_RECORD_LIST")
            End If
            contrib_id = error_table(idx).contrib_id
            If contrib_id <> last_contrib_id Then
                last_contrib_id = contrib_id
                writer.WriteStartElement("CONTRIBUTIONS")
                writer.WriteAttributeString("ID", contrib_id)
                accountNumber = error_table(idx).accountNumber
                writer.WriteElementString("APPLICANT_ID", applicant_id)
                writer.WriteElementString("AccountNumber", accountNumber)
            End If

            error_text = error_table(idx).error_text
            writer.WriteElementString("ERROR_TEXT", error_text)

            If idx = no_of_errors Then
                writer.WriteEndElement()
                writer.WriteEndElement()
            ElseIf contrib_id <> error_table(idx + 1).contrib_id Then
                writer.WriteEndElement()
            End If
        Next
        writer.WriteEndElement()
    End Sub
    
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub end_of_record()
        'use separator for debt type
        Dim sep As String = ""
        Select Case debtCode
            Case "CF"
                sep = cf_sep
            Case "DR"
                sep = dr_sep
            Case "MA"
                sep = ma_sep
            Case "FN"
                sep = fn_sep
            Case "RD"
                sep = rd_sep
            Case "RK"
                sep = rk_sep
            Case "SC"
                sep = sc_sep
            Case "WD"
                sep = wd_sep
        End Select
        record = debtor_name & sep & debt_addr1 & sep & debt_addr2 & sep & _
                                        debt_addr3 & sep & debt_addr4 & sep & debt_postcode & sep & debt_phone & sep & accountNumber & sep & _
                                        FCFullName & sep & certificateNumber & sep & proceedingNature & sep & courtNumber & sep & courtName & sep & _
                                        dateRevoked & sep & solicitor & sep & finalBilldate & sep & debtType & sep & _
                                        amount & sep & reason & sep & instructions & sep & comments
        Select Case debtCode
            Case "CF"
                If cf_outfile = Nothing Then
                    cf_outfile = "Name" & sep & "Address1" & sep & "Address2" & _
                        sep & "Address3" & sep & "Address4" & sep & "PostCode" & sep & "Tel No" & _
                        sep & "CWXAccountNumber" & sep & "FCFullname" & sep & "CertificateNumber" & sep & _
                        "ProceedingNature" & sep & "CourtNumber" & sep & "Courtname" & sep & "DateRevoked" & _
                        sep & "Solicitor" & sep & "FinalBilldate" & sep & "DebtType" & _
                        sep & "Amount" & sep & "Reason" & sep & "Instructions" & sep & "Comments" & vbNewLine
                End If
                cf_outfile &= record & vbNewLine
                cf_cases += 1
            Case "DR"
                If dr_outfile = Nothing Then
                    dr_outfile = "Name" & sep & "Address1" & sep & "Address2" & _
                        sep & "Address3" & sep & "Address4" & sep & "PostCode" & sep & "Tel No" & _
                        sep & "CWXAccountNumber" & sep & "FCFullname" & sep & "CertificateNumber" & sep & _
                        "ProceedingNature" & sep & "CourtNumber" & sep & "Courtname" & sep & "DateRevoked" & _
                        sep & "Solicitor" & sep & "FinalBilldate" & sep & "DebtType" & _
                        sep & "Amount" & sep & "Reason" & sep & "Instructions" & sep & "Comments" & vbNewLine
                End If
                dr_outfile &= record & vbNewLine
                dr_cases += 1
            Case "MA"
                If ma_outfile = Nothing Then
                    ma_outfile = "Name" & sep & "Address1" & sep & "Address2" & _
                        sep & "Address3" & sep & "Address4" & sep & "PostCode" & sep & "Tel No" & _
                        sep & "CWXAccountNumber" & sep & "FCFullname" & sep & "CertificateNumber" & sep & _
                        "ProceedingNature" & sep & "CourtNumber" & sep & "Courtname" & sep & "DateRevoked" & _
                        sep & "Solicitor" & sep & "FinalBilldate" & sep & "DebtType" & _
                        sep & "Amount" & sep & "Reason" & sep & "Instructions" & sep & "Comments" & vbNewLine
                End If
                ma_outfile &= record & vbNewLine
                ma_cases += 1
            Case "FN"
                If fn_outfile = Nothing Then
                    fn_outfile = "Name" & sep & "Address1" & sep & "Address2" & _
                        sep & "Address3" & sep & "Address4" & sep & "PostCode" & sep & "Tel No" & _
                        sep & "CWXAccountNumber" & sep & "FCFullname" & sep & "CertificateNumber" & sep & _
                        "ProceedingNature" & sep & "CourtNumber" & sep & "Courtname" & sep & "DateRevoked" & _
                        sep & "Solicitor" & sep & "FinalBilldate" & sep & "DebtType" & _
                        sep & "Amount" & sep & "Reason" & sep & "Instructions" & sep & "Comments" & vbNewLine
                End If
                fn_outfile &= record & vbNewLine
                fn_cases += 1
            Case "RD"
                If rd_outfile = Nothing Then
                    rd_outfile = "Name" & sep & "Address1" & sep & "Address2" & _
                        sep & "Address3" & sep & "Address4" & sep & "PostCode" & sep & "Tel No" & _
                        sep & "CWXAccountNumber" & sep & "FCFullname" & sep & "CertificateNumber" & sep & _
                        "ProceedingNature" & sep & "CourtNumber" & sep & "Courtname" & sep & "DateRevoked" & _
                        sep & "Solicitor" & sep & "FinalBilldate" & sep & "DebtType" & _
                        sep & "Amount" & sep & "Reason" & sep & "Instructions" & sep & "Comments" & vbNewLine
                End If
                rd_outfile &= record & vbNewLine
                rd_cases += 1
            Case "RK"
                If rk_outfile = Nothing Then
                    rk_outfile = "Name" & sep & "Address1" & sep & "Address2" & _
                        sep & "Address3" & sep & "Address4" & sep & "PostCode" & sep & "Tel No" & _
                        sep & "CWXAccountNumber" & sep & "FCFullname" & sep & "CertificateNumber" & sep & _
                        "ProceedingNature" & sep & "CourtNumber" & sep & "Courtname" & sep & "DateRevoked" & _
                        sep & "Solicitor" & sep & "FinalBilldate" & sep & "DebtType" & _
                        sep & "Amount" & sep & "Reason" & sep & "Instructions" & sep & "Comments" & vbNewLine
                End If
                rk_outfile &= record & vbNewLine
                rk_cases += 1
            Case "SC"
                If sc_outfile = Nothing Then
                    sc_outfile = "Name" & sep & "Address1" & sep & "Address2" & _
                        sep & "Address3" & sep & "Address4" & sep & "PostCode" & sep & "Tel No" & _
                        sep & "CWXAccountNumber" & sep & "FCFullname" & sep & "CertificateNumber" & sep & _
                        "ProceedingNature" & sep & "CourtNumber" & sep & "Courtname" & sep & "DateRevoked" & _
                        sep & "Solicitor" & sep & "FinalBilldate" & sep & "DebtType" & _
                        sep & "Amount" & sep & "Reason" & sep & "Instructions" & sep & "Comments" & vbNewLine
                End If
                sc_outfile &= record & vbNewLine
                sc_cases += 1
            Case "WD"
                If wd_outfile = Nothing Then
                    wd_outfile = "Name" & sep & "Address1" & sep & "Address2" & _
                        sep & "Address3" & sep & "Address4" & sep & "PostCode" & sep & "Tel No" & _
                        sep & "CWXAccountNumber" & sep & "FCFullname" & sep & "CertificateNumber" & sep & _
                        "ProceedingNature" & sep & "CourtNumber" & sep & "Courtname" & sep & "DateRevoked" & _
                        sep & "Solicitor" & sep & "FinalBilldate" & sep & "DebtType" & _
                        sep & "Amount" & sep & "Reason" & sep & "Instructions" & sep & "Comments" & vbNewLine
                End If
                wd_outfile &= record & vbNewLine
                wd_cases += 1
        End Select
        record = ""
    End Sub
    Private Function validate_debtType(ByVal debtType As String) As String
        Select Case debtType
            Case "CCMS FIN AWARD INV"
                debtCode = "MA"
            Case "CCMS DM UP F AWD INV"
                debtCode = "MA"
            Case "CCMS DM PD F AWD INV"
                debtCode = "MA"
            Case "CCMS CST AWARD INV"
                debtCode = "DR"
            Case "CCMS DM UP C AWD INV"
                debtCode = "DR"
            Case "CCMS DM PD C AWD INV"
                debtCode = "DR"
            Case "CCMS STAT CHG INV"
                debtCode = "SC"
            Case "CCMS DM UP ST CH INV"
                debtCode = "SC"
            Case "CCMS DM PD ST CH INV"
                debtCode = "SC"
            Case "CCMS RDCO INV"
                debtCode = "RD"
            Case "CCMS REVO INV"
                debtCode = "RK"
            Case "CCMS DM UP REVO INV"
                debtCode = "RK"
            Case "CCMS DM PD REVOC INV"
                debtCode = "RK"
            Case "CCMS DEBIT NT INV"
                debtCode = "FN"
            Case "CCMS DM UP DB NT INV"
                debtCode = "FN"
            Case "CCMS DM PD DB NT INV"
                debtCode = "FN"
            Case "CCMS INIT CNT INV"
                debtCode = "CF"
            Case "CCMS DM UP INC C INV"
                debtCode = "CF"
            Case "CCMS DM PD INC C INV"
                debtCode = "CF"
            Case "CCMS REG CNT INV"
                debtCode = "CF"
            Case "CCMS CAP CNT INV"
                debtCode = "CF"
            Case "CCMS AR DEBT RE INV"
                debtCode = "FN"
            Case "CCMS CAP CNT ADJ INV"
                debtCode = "CF"
            Case "CCMS ERR REF INV"
                debtCode = "EN"
            Case "CCMS EXT DEBT INV"
                debtCode = "EP"
            Case "CCMS INC CNT ADJ INV"
                debtCode = "CF"
            Case "CCMS INC CNT TRF INV"
                debtCode = "CF"
            Case "CCMS INT DEPOSIT INV"
                debtCode = "??"
            Case "CCMS STAFF DEBT INV"
                debtCode = "EM"
            Case "CCMS TRF GRANTS INV"
                debtCode = "TCG"
            Case "CCMS CST AWRD INV"
                debtCode = "DR"
            Case Else
                debtCode = "??"
        End Select
        Return debtCode
    End Function

    'Private Function date_range_valid(ByVal dte As Date) As Boolean
    '    Static low_date As Date = "jan 1 1900"
    '    Static high_date As Date = "jan 1 2050"
    '    If dte < low_date Or dte > high_date Then
    '        invalid_date_found = True
    '        Return (False)
    '    Else
    '        Return (True)
    '    End If
    'End Function

    Private Sub write_error(ByVal error_message As String)
        Static Dim error_no As Integer
        error_no += 1
        Dim ln As Integer = Microsoft.VisualBasic.Len(OpenFileDialog1.FileName)
        new_file = Microsoft.VisualBasic.Left(OpenFileDialog1.FileName, ln - 4) & "_error.txt"
        error_message = error_message & vbNewLine
        If error_no = 1 Then
            My.Computer.FileSystem.WriteAllText(new_file, error_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(new_file, error_message, True)
        End If

    End Sub

    Private Sub remove_strange_chrs(ByRef in_field As String)
        Dim idx2 As Integer
        Dim out_field As String = ""
        For idx2 = 1 To Microsoft.VisualBasic.Len(in_field)
            Dim test As Char = Mid(in_field, idx2, 1)
            If Not Char.IsSymbol(test) Then
                out_field = out_field & Mid(in_field, idx2, 1)
            End If
        Next
        in_field = out_field
    End Sub
    Private Sub reset_fields()

        debtor_name = ""
        debt_addr1 = ""
        debt_addr2 = ""
        debt_addr3 = ""
        debt_addr4 = ""
        debt_postcode = ""
        debt_phone = ""
        accountNumber = ""
        comments = ""
        courtName = ""
        courtNumber = ""
        dateRevoked = ""
        debtType = ""
        amount = ""
        solicitor = ""
        FCFullName = ""
        accountNumber = ""
        certificateNumber = ""
        finalBilldate = ""
        proceedingNature = ""
        reason = ""
    End Sub
    Private Sub get_comment_break(ByVal new_comment As String)
        Dim idx, idx2 As Integer
        'break comments into 250 chunks broken at ;
        Dim no_of_250s As Integer = Int(new_comment.Length / 250) + 1
        Dim init_length As Integer = new_comment.Length
        For idx = 1 To no_of_250s
            For idx2 = 250 To 1 Step -1
                If Mid(new_comment, idx2, 1) = ";" Then
                    Exit For
                End If
            Next
            comments = comments & Microsoft.VisualBasic.Left(new_comment, idx2)
            space_comments()
            new_comment = Microsoft.VisualBasic.Right(new_comment, new_comment.Length - idx2)
            If new_comment.Length <= 250 Then
                comments = comments & new_comment
                space_comments()
                Exit For
            End If
        Next
    End Sub

    Private Sub validate_xml_file()
        'open file as text first to remove any � signs
        Dim txt_doc As String
        txt_doc = My.Computer.FileSystem.ReadAllText(OpenFileDialog1.FileName)
        txt_doc = Replace(txt_doc, "�", "")
        My.Computer.FileSystem.WriteAllText(OpenFileDialog1.FileName, txt_doc, False)
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(OpenFileDialog1.FileName)
        'MsgBox("Using new XSD file")
        myDocument.Schemas.Add("", "R:\vb.net\LAA Collect\LAA Collect XSD File.xsd")
        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
    End Sub

    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_error("Error: " & e.Message)
            Case XmlSeverityType.Warning
                write_error("Warning " & e.Message)
        End Select
    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        log_user = My.User.Name
        Dim slash_idx As Integer = InStr(log_user, "\")
        If slash_idx > 0 Then
            log_user = Microsoft.VisualBasic.Right(log_user, log_user.Length - slash_idx)
        End If
    End Sub




End Class

﻿Public Class Form1
    Public ascii As New System.Text.ASCIIEncoding()
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    
    Private Sub collectbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles collectbtn1.Click
        collect_cases = True
        bailiff_cases = False
        process_phone_1()
    End Sub
    Private Sub process_phone_1()
       disable_buttons()
        param2 = "select _rowid, add_phone, clientSchemeID from Debtor " &
            " where _rowid > 1000 and status_open_closed = 'O' and status = 'L' and not(isnull(add_phone)) " &
            " order by clientSchemeID"
        ProgressBar1.Value = 5
        Dim debt_ds As DataSet = get_dataset("onestep", param2)
        Dim debt_rows As Integer = no_of_rows - 1
        Dim upd_file As String = ""
        Dim error_file As String = ""
        Dim record_count As Integer = 0
        Dim file_count As Integer = 0
        Dim last_csID As Integer = 0
        Dim cs_required As Boolean = True
        For idx = 0 To debt_rows

            Try
                ProgressBar1.Value = (idx / debt_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            Dim csid As Integer = debt_ds.Tables(0).Rows(idx).Item(2)
            'T67659 ignore 3171 and 3163 dvla overseas
            If csid = 3171 Or csid = 3163 Then
                Continue For
            End If
            If csid = last_csID Then
                If cs_required = False Then
                    Continue For
                End If
            Else
                last_csID = csid
                param2 = "select branchID from clientScheme where _rowid = " & csid
                Dim cs_ds As DataSet = get_dataset("onestep", param2)
                Dim branch_no As Integer = cs_ds.Tables(0).Rows(0).Item(0)
                If collect_cases Then
                    If branch_no <> 2 Then
                        cs_required = False
                        Continue For
                    End If
                ElseIf bailiff_cases Then
                    If branch_no <> 1 Then
                        cs_required = False
                        Continue For
                    End If
                ElseIf dvla_cases Then
                    If branch_no <> 12 Then
                        cs_required = False
                        Continue For
                    End If
                ElseIf dmi_cases Then
                    If branch_no <> 25 And branch_no <> 27 Then
                        cs_required = False
                        Continue For
                    End If
                ElseIf br10_cases Then
                    If branch_no <> 10 Then
                        cs_required = False
                        Continue For
                    End If
                ElseIf br24_cases Then
                    If branch_no <> 24 Then
                        cs_required = False
                        Continue For
                    End If
                Else
                    If branch_no <> 3 And branch_no <> 4 And branch_no <> 8 And branch_no <> 23 And branch_no <> 26 Then
                        cs_required = False
                        Continue For
                    End If
                End If
            End If
            'Threading.Thread.Sleep(50)
            cs_required = True
            Dim debtor As Integer = debt_ds.Tables(0).Rows(idx).Item(0)
            Dim phone_number As String = Trim(debt_ds.Tables(0).Rows(idx).Item(1))
            If phone_number.Length = 0 Then
                Continue For
            End If
            If phone_number.Length < 10 Then
                phone_number = remove_invalid_chars(phone_number)
                error_file = error_file & debtor & "," & phone_number & ",Less than 10 characters"
                If invalid_char_found Then
                    error_file &= " with invalid character"
                End If
                error_file &= vbNewLine
                Continue For
            End If
            Dim new_phone_number As String = ""
            Dim changeMade As Boolean = False
            If Microsoft.VisualBasic.Left(phone_number, 3) = "440" Then
                phone_number = Microsoft.VisualBasic.Right(phone_number, phone_number.Length - 2)
                changeMade = True
            End If
            'replace +44 with blank
            phone_number = Replace(phone_number, "+44", "")
            'replace + with blank
            phone_number = Replace(phone_number, "+", "")
            'remove any leading zeroes then put just one back
            Dim phoneIDX As Integer
            For phoneIDX = 1 To phone_number.Length
                If Mid(phone_number, phoneIDX, 1) <> "0" Then
                    Exit For
                End If
            Next
            phone_number = "0" & Microsoft.VisualBasic.Right(phone_number, phone_number.Length - phoneIDX + 1)
            If phoneIDX > 2 Then
                changeMade = True
            End If
            If phone_number.Length > 11 Then
                phone_number = remove_invalid_chars(phone_number)
                error_file = error_file & debtor & "," & phone_number & ",More than 11 characters"
                If invalid_char_found Then
                    error_file &= " with invalid character"
                End If
                error_file &= vbNewLine
                Continue For
            End If
            If IsNumeric(phone_number) Then
                Dim ph_number_numeric As Long
                Try
                    ph_number_numeric = phone_number
                Catch ex As Exception
                    phone_number = remove_invalid_chars(phone_number)
                    error_file = error_file & debtor & "," & phone_number & ",Invalid phone number"
                    If invalid_char_found Then
                        error_file &= " with invalid character"
                    End If
                    error_file &= vbNewLine
                    Continue For
                End Try
                If Not changeMade Then
                    Continue For
                End If
            End If

            'check phone number for invalid characters
            Dim truncate_number As Boolean = False
            For idx2 = 1 To phone_number.Length
                Dim asc_no As Integer = Asc(Mid(phone_number, idx2, 1))
                If asc_no = 32 Then  'space
                    Continue For
                ElseIf asc_no >= 48 And asc_no <= 57 Then 'valid number
                    new_phone_number = new_phone_number & Mid(phone_number, idx2, 1)
                    Continue For
                ElseIf asc_no = 79 Or asc_no = 111 Then   'letter o/O
                    new_phone_number = new_phone_number & "0"
                ElseIf asc_no = 38 Or asc_no = 47 Then  '& and / (rest of number to be ignored)
                    truncate_number = True
                    Exit For
                End If
            Next

            ''check phone number starts with a zero
            'If Microsoft.VisualBasic.Left(new_phone_number, 1) <> "0" Then
            '    new_phone_number = "0" & new_phone_number
            'End If
            Dim test_amt As Long
            Try
                test_amt = new_phone_number
            Catch ex As Exception
                test_amt = Nothing
            End Try
            If test_amt = 0 Then
                new_phone_number = ""
            End If
            If IsNumeric(new_phone_number) And new_phone_number.Length = 10 Then
                'phone number OK
                If changeMade Then
                    upd_file = upd_file & debtor & "," & new_phone_number & vbNewLine
                End If
            ElseIf IsNumeric(new_phone_number) And new_phone_number.Length > 10 Then
                upd_file = upd_file & debtor & "," & new_phone_number & vbNewLine
                record_count += 1
                If truncate_number Then
                    phone_number = remove_invalid_chars(phone_number)
                    error_file = error_file & debtor & "," & phone_number & ",first number only being updated" & vbNewLine
                End If
            Else
                phone_number = remove_invalid_chars(phone_number)
                If phone_number.Length < 10 Then
                    error_file = error_file & debtor & "," & phone_number & ",phone number less than 10 characters"
                Else
                    error_file = error_file & debtor & "," & phone_number & ",phone number not numeric"
                End If

                If invalid_char_found Then
                    error_file &= " with invalid character"
                End If
                error_file &= vbNewLine
                Continue For
            End If


            If record_count = 1000 Then
                file_count += 1
                If file_count = 1 Then
                    With SaveFileDialog1
                        .Title = "Save file"
                        .Filter = "CSV files |*.csv"
                        .DefaultExt = ".csv"
                        .OverwritePrompt = True
                        .FileName = "phone_number_upd01.csv"
                    End With

                    If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, upd_file, True, ascii)
                    End If
                Else
                    SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) &
                        file_count & ".csv"
                    My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, upd_file, True, ascii)
                End If
                record_count = 0
                upd_file = ""
            End If
        Next
        'save last file
        file_count += 1
        If file_count = 1 Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "CSV files |*.csv"
                .DefaultExt = ".csv"
                .OverwritePrompt = True
                .FileName = "phone_number_upd1.csv"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, upd_file, True, ascii)
            End If
        Else
            If upd_file.Length > 0 Then

                SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) &
                            file_count & ".csv"
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, upd_file, True, ascii)
            End If
        End If

        SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) &
                        "_error.csv"
        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, error_file, True, ascii)
        MsgBox("Files saved")
        Me.Close()
    End Sub

    Private Sub collectbtn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles collectbtn2.Click
        collect_cases = True
        bailiff_cases = False
        process_phone_2()
    End Sub
    Sub process_phone_2()
        disable_buttons()
        param2 = "select _rowid, add_fax, clientSchemeID from Debtor " &
            " where _rowid > 1000 and status_open_closed = 'O' and status = 'L' and not(isnull(add_fax))" &
             " order by clientSchemeID"
        ProgressBar1.Value = 5
        Dim debt_ds As DataSet = get_dataset("onestep", param2)
        Dim debt_rows As Integer = no_of_rows - 1
        Dim upd_file As String = ""
        Dim error_file As String = ""
        Dim record_count As Integer = 0
        Dim file_count As Integer = 0
        Dim last_csID As Integer = 0
        Dim cs_required As Boolean = True
        For idx = 0 To debt_rows
            Try
                ProgressBar1.Value = (idx / debt_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
           
            Dim csid As Integer = debt_ds.Tables(0).Rows(idx).Item(2)
            'T67659 ignore 3171 and 3163 dvla overseas
            If csid = 3171 Or csid = 3163 Then
                Continue For
            End If
            If csid = last_csID Then
                If cs_required = False Then
                    Continue For
                End If
            Else
                last_csID = csid
                param2 = "select branchID from clientScheme where _rowid = " & csid
                Dim cs_ds As DataSet = get_dataset("onestep", param2)
                Dim branch_no As Integer = cs_ds.Tables(0).Rows(0).Item(0)
                If collect_cases Then
                    If branch_no <> 2 Then
                        cs_required = False
                        Continue For
                    End If
                ElseIf bailiff_cases Then
                    If branch_no <> 1 Then
                        cs_required = False
                        Continue For
                    End If
                ElseIf dvla_cases Then
                    If branch_no <> 12 Then
                        cs_required = False
                        Continue For
                    End If
                ElseIf dmi_cases Then
                    If branch_no <> 25 And branch_no <> 27 Then
                        cs_required = False
                        Continue For
                    End If
                ElseIf br10_cases Then
                    If branch_no <> 10 Then
                        cs_required = False
                        Continue For
                    End If
                ElseIf br24_cases Then
                    If branch_no <> 24 Then
                        cs_required = False
                        Continue For
                    End If
                Else
                    If branch_no <> 3 And branch_no <> 4 And branch_no <> 8 And branch_no <> 23 And branch_no <> 26 Then
                        cs_required = False
                        Continue For
                    End If
                End If
            End If
            cs_required = True
            Dim phone_number As String = Trim(debt_ds.Tables(0).Rows(idx).Item(1))
            If phone_number.Length = 0 Then
                Continue For
            End If
            Dim debtor As Integer = debt_ds.Tables(0).Rows(idx).Item(0)
            'If debtor = 8869612 Then
            '    debtor = 8869612
            'End If
            If phone_number.Length < 10 Then
                phone_number = remove_invalid_chars(phone_number)
                error_file = error_file & debtor & "," & phone_number & ",Less than 10 characters"
                If invalid_char_found Then
                    error_file &= " with invalid character"
                End If
                error_file &= vbNewLine
                Continue For
            End If
            Dim new_phone_number As String = ""
            Dim changeMade As Boolean = False
            If Microsoft.VisualBasic.Left(phone_number, 3) = "440" Then
                phone_number = Microsoft.VisualBasic.Right(phone_number, phone_number.Length - 2)
                changeMade = True
            End If
            If phone_number.Length >= 12 And Microsoft.VisualBasic.Left(phone_number, 3) = "447" Then
                changeMade = True
                phone_number = Microsoft.VisualBasic.Right(phone_number, phone_number.Length - 2)
            End If
            'replace +44 with blank
            If InStr(phone_number, "+44") > 0 Then
                phone_number = Replace(phone_number, "+44", "")
                changeMade = True
            End If

            'replace + with blank
            If InStr(phone_number, "+") > 0 Then
                phone_number = Replace(phone_number, "+", "")
                changeMade = True
            End If
            'remove any leading zeroes then put just one back
            Dim phoneIDX As Integer
            For phoneIDX = 1 To phone_number.Length
                If Mid(phone_number, phoneIDX, 1) <> "0" Then
                    Exit For
                End If
            Next
            phone_number = "0" & Microsoft.VisualBasic.Right(phone_number, phone_number.Length - phoneIDX + 1)
            If phoneIDX > 2 Then
                changeMade = True
            End If
            If phone_number.Length > 11 Then
                phone_number = remove_invalid_chars(phone_number)
                error_file = error_file & debtor & "," & phone_number & ",More than 11 characters"
                If invalid_char_found Then
                    error_file &= " with invalid character"
                End If
                error_file &= vbNewLine
                Continue For
            End If
            If IsNumeric(phone_number) Then
                Dim ph_number_numeric As Long
                Try
                    ph_number_numeric = phone_number
                Catch ex As Exception
                    phone_number = remove_invalid_chars(phone_number)
                    error_file = error_file & debtor & "," & phone_number & ",Invalid phone number"
                    If invalid_char_found Then
                        error_file &= " with invalid character"
                    End If
                    error_file &= vbNewLine
                    Continue For
                End Try
                If Not changeMade Then
                    Continue For
                End If
            End If

            'check phone number for invalid characters
            Dim truncate_number As Boolean = False
            For idx2 = 1 To phone_number.Length
                Dim asc_no As Integer = Asc(Mid(phone_number, idx2, 1))
                If asc_no = 32 Then  'space
                    Continue For
                ElseIf asc_no >= 48 And asc_no <= 57 Then 'valid number
                    new_phone_number = new_phone_number & Mid(phone_number, idx2, 1)
                    Continue For
                ElseIf asc_no = 79 Or asc_no = 111 Then   'letter o/O
                    new_phone_number = new_phone_number & "0"
                ElseIf asc_no = 38 Or asc_no = 47 Then  '& and / (rest of number to be ignored)
                    truncate_number = True
                    Exit For
                End If
            Next

            ''check phone number starts with a zero
            'If Microsoft.VisualBasic.Left(new_phone_number, 1) <> "0" Then
            '    new_phone_number = "0" & new_phone_number
            'End If
            Dim test_amt As Long
            Try
                test_amt = new_phone_number
            Catch ex As Exception
                test_amt = Nothing
            End Try
            If test_amt = 0 Then
                new_phone_number = ""
            End If
            If IsNumeric(new_phone_number) And new_phone_number.Length = 10 Then
                'phone number OK
                If changeMade Then
                    upd_file = upd_file & debtor & "," & new_phone_number & vbNewLine
                End If
            ElseIf IsNumeric(new_phone_number) And new_phone_number.Length > 10 Then
                upd_file = upd_file & debtor & "," & new_phone_number & vbNewLine
                record_count += 1
                If truncate_number Then
                    phone_number = remove_invalid_chars(phone_number)
                    error_file = error_file & debtor & "," & phone_number & ",first number only being updated" & vbNewLine
                End If
            Else
                phone_number = remove_invalid_chars(phone_number)
                If phone_number.Length < 10 Then
                    error_file = error_file & debtor & "," & phone_number & ",phone number less than 10 characters"
                Else
                    error_file = error_file & debtor & "," & phone_number & ",phone number not numeric"
                End If

                If invalid_char_found Then
                    error_file &= " with invalid character"
                End If
                error_file &= vbNewLine
                Continue For
            End If


            If record_count = 1000 Then
                file_count += 1
                If file_count = 1 Then
                    With SaveFileDialog1
                        .Title = "Save file"
                        .Filter = "CSV files |*.csv"
                        .DefaultExt = ".csv"
                        .OverwritePrompt = True
                        .FileName = "phone_number_upd01.csv"
                    End With

                    If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, upd_file, False, ascii)
                    End If
                Else
                    SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) &
                        file_count & ".csv"
                    My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, upd_file, True, ascii)
                End If
                record_count = 0
                upd_file = ""
            End If
        Next
        'save last file
        file_count += 1
        If file_count = 1 Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "CSV files |*.csv"
                .DefaultExt = ".csv"
                .OverwritePrompt = True
                .FileName = "phone_number_upd1.csv"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, upd_file, False, ascii)
            End If
        Else
            If upd_file.Length > 0 Then

                SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) &
                            file_count & ".csv"
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, upd_file, True, ascii)
            End If
        End If

        SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) &
                        "_error.csv"
        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, error_file, False, ascii)
        MsgBox("Files saved")
        Me.Close()
        '    If phone_number.Length < 10 Then
        '        phone_number = remove_invalid_chars(phone_number)
        '        error_file = error_file & debtor & "," & phone_number & ",Less than 10 characters"
        '        If invalid_char_found Then
        '            error_file &= " with invalid character"
        '        End If
        '        error_file &= vbNewLine
        '        Continue For
        '        Continue For
        '    End If
        '    If phone_number.Length > 20 Then
        '        phone_number = remove_invalid_chars(phone_number)
        '        error_file = error_file & debtor & "," & phone_number & ",More than 20 characters"
        '        If invalid_char_found Then
        '            error_file &= " with invalid character"
        '        End If
        '        error_file &= vbNewLine
        '        Continue For
        '    End If
        '    If IsNumeric(phone_number) Then
        '        Dim ph_number_numeric As Long
        '        Try
        '            ph_number_numeric = phone_number
        '        Catch ex As Exception
        '            phone_number = remove_invalid_chars(phone_number)
        '            error_file = error_file & debtor & "," & phone_number & ",Invalid phone number"
        '            If invalid_char_found Then
        '                error_file &= " with invalid character"
        '            End If
        '            error_file &= vbNewLine
        '            Continue For
        '        End Try

        '        If ph_number_numeric > 0 Then
        '            Continue For
        '        End If
        '    End If

        '    'check phone number for invalid characters
        '    Dim new_phone_number As String = ""
        '    Dim truncate_number As Boolean = False
        '    For idx2 = 1 To phone_number.Length
        '        Dim asc_no As Integer = Asc(Mid(phone_number, idx2, 1))
        '        If asc_no = 32 Then  'space
        '            Continue For
        '        ElseIf asc_no >= 48 And asc_no <= 57 Then 'valid number
        '            new_phone_number = new_phone_number & Mid(phone_number, idx2, 1)
        '            Continue For
        '        ElseIf asc_no = 79 Or asc_no = 111 Then   'letter o/O
        '            new_phone_number = new_phone_number & "0"
        '        ElseIf asc_no = 38 Or asc_no = 47 Then  '& and / (rest of number to be ignored)
        '            truncate_number = True
        '            Exit For
        '        End If
        '    Next
        '    If IsNumeric(new_phone_number) And new_phone_number.Length > 10 Then
        '        upd_file = upd_file & debtor & "," & new_phone_number & vbNewLine
        '        record_count += 1
        '        If truncate_number Then
        '            phone_number = remove_invalid_chars(phone_number)
        '            error_file = error_file & debtor & "," & phone_number & ",first number only being updated" & vbNewLine
        '        End If
        '    Else
        '        phone_number = remove_invalid_chars(phone_number)
        '        error_file = error_file & debtor & "," & phone_number & ",phone number not valid"
        '        If invalid_char_found Then
        '            error_file &= " with invalid character"
        '        End If
        '        error_file &= vbNewLine
        '        Continue For
        '    End If

        '    If record_count = 1000 Then
        '        file_count += 1
        '        If file_count = 1 Then
        '            With SaveFileDialog1
        '                .Title = "Save file"
        '                .Filter = "CSV files |*.csv"
        '                .DefaultExt = ".csv"
        '                .OverwritePrompt = True
        '                .FileName = "phone_number_upd01.csv"
        '            End With

        '            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
        '                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, upd_file, True)
        '            End If
        '        Else
        '            SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) &
        '                file_count & ".csv"
        '            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, upd_file, True)
        '        End If
        '        record_count = 0
        '        upd_file = ""
        '    End If
        'Next
        ''save last file
        'file_count += 1
        'If file_count = 1 Then
        '    With SaveFileDialog1
        '        .Title = "Save file"
        '        .Filter = "CSV files |*.csv"
        '        .DefaultExt = ".csv"
        '        .OverwritePrompt = True
        '        .FileName = "phone_number_upd1.csv"
        '    End With
        '    If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
        '        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, upd_file, True)
        '    End If
        'Else
        '    If upd_file.Length > 0 Then

        '        SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) &
        '                    file_count & ".csv"
        '        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, upd_file, True)
        '    End If
        'End If

        'SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, SaveFileDialog1.FileName.Length - 5) &
        '                "_error.csv"
        'My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, error_file, True)
        'MsgBox("Files saved")
        'Me.Close()
    End Sub

    Private Sub lscbtn1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lscbtn1.Click
        collect_cases = False
        bailiff_cases = False
        dvla_cases = False
        dmi_cases = False
        process_phone_1()
    End Sub
    Private Function remove_invalid_chars(ByVal phone_number As String) As String
        invalid_char_found = False
        Dim new_phone_number As String = ""
        Dim idx As Integer
        For idx = 1 To phone_number.Length
            If Mid(phone_number, idx, 1) = Chr(10) Or Mid(phone_number, idx, 1) = Chr(13) Then
                new_phone_number &= " "
                invalid_char_found = True
            Else
                new_phone_number &= Mid(phone_number, idx, 1)
            End If
        Next
        Return (new_phone_number)
    End Function

    Private Sub lscbtn2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lscbtn2.Click
        collect_cases = False
        bailiff_cases = False
        dvla_cases = False
        dmi_cases = False
        process_phone_2()
    End Sub

    
    Private Sub br1_ph1btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles br1_ph1btn.Click
        collect_cases = False
        bailiff_cases = True
        dvla_cases = False
        dmi_cases = False
        process_phone_1()
    End Sub

    Private Sub br1_ph2btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles br1_ph2btn.Click
        collect_cases = False
        bailiff_cases = True
        dvla_cases = False
        dmi_cases = False
        process_phone_2()
    End Sub
    Private Sub disable_buttons()
        br10_ph1btn.Enabled = False
        br10_ph2btn.Enabled = False
        br24_ph1btn.Enabled = False
        br24_ph2btn.Enabled = False
        exitbtn.Enabled = False
        collectbtn1.Enabled = False
        collectbtn2.Enabled = False
        lscbtn1.Enabled = False
        lscbtn2.Enabled = False
        br1_ph1btn.Enabled = False
        br1_ph2btn.Enabled = False
        dvla_ph1.Enabled = False
        dvla_ph2.Enabled = False
        dmi_ph1.Enabled = False
        dmi_ph2.Enabled = False
    End Sub

    Private Sub dvla_ph1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dvla_ph1.Click
        collect_cases = False
        bailiff_cases = False
        dvla_cases = True
        dmi_cases = False
        process_phone_1()
    End Sub

    Private Sub dvla_ph2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dvla_ph2.Click
        collect_cases = False
        bailiff_cases = False
        dvla_cases = True
        dmi_cases = False
        process_phone_2()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        br10_cases = False
        br24_cases = False
    End Sub

    Private Sub dmi_ph1_Click(sender As System.Object, e As System.EventArgs) Handles dmi_ph1.Click
        collect_cases = False
        bailiff_cases = False
        dvla_cases = False
        dmi_cases = True
        process_phone_1()
    End Sub

    Private Sub dmi_ph2_Click(sender As System.Object, e As System.EventArgs) Handles dmi_ph2.Click
        collect_cases = False
        bailiff_cases = False
        dvla_cases = False
        dmi_cases = True
        process_phone_2()
    End Sub

   
    Private Sub br10_ph1btn_Click(sender As System.Object, e As System.EventArgs) Handles br10_ph1btn.Click
        collect_cases = False
        bailiff_cases = False
        dvla_cases = False
        dmi_cases = False
        br10_cases = True
        process_phone_1()
    End Sub

    Private Sub br24_ph1btn_Click(sender As System.Object, e As System.EventArgs) Handles br24_ph1btn.Click
        collect_cases = False
        bailiff_cases = False
        dvla_cases = False
        dmi_cases = False
        br24_cases = True
        process_phone_1()
    End Sub

    Private Sub br10_ph2btn_Click(sender As System.Object, e As System.EventArgs) Handles br10_ph2btn.Click
        collect_cases = False
        bailiff_cases = False
        dvla_cases = False
        dmi_cases = False
        br10_cases = True
        process_phone_2()
    End Sub

    Private Sub br24_ph2btn_Click(sender As System.Object, e As System.EventArgs) Handles br24_ph2btn.Click
        collect_cases = False
        bailiff_cases = False
        dvla_cases = False
        dmi_cases = False
        br24_cases = True
        process_phone_2()
    End Sub
End Class

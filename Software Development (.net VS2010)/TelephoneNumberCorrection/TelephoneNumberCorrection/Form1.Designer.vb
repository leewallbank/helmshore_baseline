﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.collectbtn1 = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.collectbtn2 = New System.Windows.Forms.Button()
        Me.lscbtn2 = New System.Windows.Forms.Button()
        Me.lscbtn1 = New System.Windows.Forms.Button()
        Me.br1_ph1btn = New System.Windows.Forms.Button()
        Me.br1_ph2btn = New System.Windows.Forms.Button()
        Me.dvla_ph1 = New System.Windows.Forms.Button()
        Me.dvla_ph2 = New System.Windows.Forms.Button()
        Me.dmi_ph1 = New System.Windows.Forms.Button()
        Me.dmi_ph2 = New System.Windows.Forms.Button()
        Me.br10_ph1btn = New System.Windows.Forms.Button()
        Me.br10_ph2btn = New System.Windows.Forms.Button()
        Me.br24_ph1btn = New System.Windows.Forms.Button()
        Me.br24_ph2btn = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'collectbtn1
        '
        Me.collectbtn1.Location = New System.Drawing.Point(44, 29)
        Me.collectbtn1.Name = "collectbtn1"
        Me.collectbtn1.Size = New System.Drawing.Size(135, 23)
        Me.collectbtn1.TabIndex = 0
        Me.collectbtn1.Text = "Collect Phone Number 1"
        Me.collectbtn1.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(417, 507)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 14
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 507)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(173, 23)
        Me.ProgressBar1.TabIndex = 6
        '
        'collectbtn2
        '
        Me.collectbtn2.Location = New System.Drawing.Point(44, 58)
        Me.collectbtn2.Name = "collectbtn2"
        Me.collectbtn2.Size = New System.Drawing.Size(135, 23)
        Me.collectbtn2.TabIndex = 1
        Me.collectbtn2.Text = "Collect Phone Number 2"
        Me.collectbtn2.UseVisualStyleBackColor = True
        '
        'lscbtn2
        '
        Me.lscbtn2.Location = New System.Drawing.Point(44, 142)
        Me.lscbtn2.Name = "lscbtn2"
        Me.lscbtn2.Size = New System.Drawing.Size(250, 23)
        Me.lscbtn2.TabIndex = 3
        Me.lscbtn2.Text = "LSC/HC/HMRC/TDX HMRC Phone Number 2"
        Me.lscbtn2.UseVisualStyleBackColor = True
        '
        'lscbtn1
        '
        Me.lscbtn1.Location = New System.Drawing.Point(44, 113)
        Me.lscbtn1.Name = "lscbtn1"
        Me.lscbtn1.Size = New System.Drawing.Size(250, 23)
        Me.lscbtn1.TabIndex = 2
        Me.lscbtn1.Text = "LSC/HC/HMRC/TDX HMRC Phone Number 1"
        Me.lscbtn1.UseVisualStyleBackColor = True
        '
        'br1_ph1btn
        '
        Me.br1_ph1btn.Location = New System.Drawing.Point(44, 199)
        Me.br1_ph1btn.Name = "br1_ph1btn"
        Me.br1_ph1btn.Size = New System.Drawing.Size(179, 23)
        Me.br1_ph1btn.TabIndex = 4
        Me.br1_ph1btn.Text = "Branch 1 Phone Number 1"
        Me.br1_ph1btn.UseVisualStyleBackColor = True
        '
        'br1_ph2btn
        '
        Me.br1_ph2btn.Location = New System.Drawing.Point(44, 228)
        Me.br1_ph2btn.Name = "br1_ph2btn"
        Me.br1_ph2btn.Size = New System.Drawing.Size(179, 23)
        Me.br1_ph2btn.TabIndex = 5
        Me.br1_ph2btn.Text = "Branch 1 Phone Number 2"
        Me.br1_ph2btn.UseVisualStyleBackColor = True
        '
        'dvla_ph1
        '
        Me.dvla_ph1.Location = New System.Drawing.Point(44, 281)
        Me.dvla_ph1.Name = "dvla_ph1"
        Me.dvla_ph1.Size = New System.Drawing.Size(179, 23)
        Me.dvla_ph1.TabIndex = 8
        Me.dvla_ph1.Text = "DVLA Phone Number 1"
        Me.dvla_ph1.UseVisualStyleBackColor = True
        '
        'dvla_ph2
        '
        Me.dvla_ph2.Location = New System.Drawing.Point(44, 310)
        Me.dvla_ph2.Name = "dvla_ph2"
        Me.dvla_ph2.Size = New System.Drawing.Size(179, 23)
        Me.dvla_ph2.TabIndex = 9
        Me.dvla_ph2.Text = "DVLA Phone Number 2"
        Me.dvla_ph2.UseVisualStyleBackColor = True
        '
        'dmi_ph1
        '
        Me.dmi_ph1.Location = New System.Drawing.Point(44, 361)
        Me.dmi_ph1.Name = "dmi_ph1"
        Me.dmi_ph1.Size = New System.Drawing.Size(179, 23)
        Me.dmi_ph1.TabIndex = 12
        Me.dmi_ph1.Text = "DMI (BR 25 27) Phone 1"
        Me.dmi_ph1.UseVisualStyleBackColor = True
        '
        'dmi_ph2
        '
        Me.dmi_ph2.Location = New System.Drawing.Point(44, 390)
        Me.dmi_ph2.Name = "dmi_ph2"
        Me.dmi_ph2.Size = New System.Drawing.Size(179, 23)
        Me.dmi_ph2.TabIndex = 13
        Me.dmi_ph2.Text = "DMI (BR 25 27) Phone 2"
        Me.dmi_ph2.UseVisualStyleBackColor = True
        '
        'br10_ph1btn
        '
        Me.br10_ph1btn.Location = New System.Drawing.Point(284, 199)
        Me.br10_ph1btn.Name = "br10_ph1btn"
        Me.br10_ph1btn.Size = New System.Drawing.Size(179, 23)
        Me.br10_ph1btn.TabIndex = 6
        Me.br10_ph1btn.Text = "Branch 10 Phone Number 1"
        Me.br10_ph1btn.UseVisualStyleBackColor = True
        '
        'br10_ph2btn
        '
        Me.br10_ph2btn.Location = New System.Drawing.Point(284, 228)
        Me.br10_ph2btn.Name = "br10_ph2btn"
        Me.br10_ph2btn.Size = New System.Drawing.Size(179, 23)
        Me.br10_ph2btn.TabIndex = 7
        Me.br10_ph2btn.Text = "Branch 10 Phone Number 2"
        Me.br10_ph2btn.UseVisualStyleBackColor = True
        '
        'br24_ph1btn
        '
        Me.br24_ph1btn.Location = New System.Drawing.Point(284, 281)
        Me.br24_ph1btn.Name = "br24_ph1btn"
        Me.br24_ph1btn.Size = New System.Drawing.Size(179, 23)
        Me.br24_ph1btn.TabIndex = 10
        Me.br24_ph1btn.Text = "Branch 24 Phone Number 1"
        Me.br24_ph1btn.UseVisualStyleBackColor = True
        '
        'br24_ph2btn
        '
        Me.br24_ph2btn.Location = New System.Drawing.Point(284, 310)
        Me.br24_ph2btn.Name = "br24_ph2btn"
        Me.br24_ph2btn.Size = New System.Drawing.Size(179, 23)
        Me.br24_ph2btn.TabIndex = 11
        Me.br24_ph2btn.Text = "Branch 24 Phone Number 2"
        Me.br24_ph2btn.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(521, 551)
        Me.Controls.Add(Me.br24_ph2btn)
        Me.Controls.Add(Me.br24_ph1btn)
        Me.Controls.Add(Me.br10_ph2btn)
        Me.Controls.Add(Me.br10_ph1btn)
        Me.Controls.Add(Me.dmi_ph2)
        Me.Controls.Add(Me.dmi_ph1)
        Me.Controls.Add(Me.dvla_ph2)
        Me.Controls.Add(Me.dvla_ph1)
        Me.Controls.Add(Me.br1_ph2btn)
        Me.Controls.Add(Me.br1_ph1btn)
        Me.Controls.Add(Me.lscbtn2)
        Me.Controls.Add(Me.lscbtn1)
        Me.Controls.Add(Me.collectbtn2)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.collectbtn1)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "telephone Number Correction"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents collectbtn1 As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents collectbtn2 As System.Windows.Forms.Button
    Friend WithEvents lscbtn2 As System.Windows.Forms.Button
    Friend WithEvents lscbtn1 As System.Windows.Forms.Button
    Friend WithEvents br1_ph1btn As System.Windows.Forms.Button
    Friend WithEvents br1_ph2btn As System.Windows.Forms.Button
    Friend WithEvents dvla_ph1 As System.Windows.Forms.Button
    Friend WithEvents dvla_ph2 As System.Windows.Forms.Button
    Friend WithEvents dmi_ph1 As System.Windows.Forms.Button
    Friend WithEvents dmi_ph2 As System.Windows.Forms.Button
    Friend WithEvents br10_ph1btn As System.Windows.Forms.Button
    Friend WithEvents br10_ph2btn As System.Windows.Forms.Button
    Friend WithEvents br24_ph1btn As System.Windows.Forms.Button
    Friend WithEvents br24_ph2btn As System.Windows.Forms.Button

End Class

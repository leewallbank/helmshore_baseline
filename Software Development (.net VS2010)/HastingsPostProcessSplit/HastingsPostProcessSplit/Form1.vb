﻿Imports System.IO

Public Class Form1

    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub openbtn_Click(sender As System.Object, e As System.EventArgs) Handles openbtn.Click
        Dim InputFilePath, auditfile As String, FileName As String, FileExt As String
        Dim ErrorLog As String = "", InputLineArray() As String
        Dim recycledCases As String = ""
        Dim NewCases As String = ""
        Dim recycledNo, newNo As Integer
        Dim recycledBal, newBal As Decimal
        Dim FileDialog As New OpenFileDialog
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        Dim apr2015 As Date = CDate("Apr 1, 2015 00:00:00")
        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)
        Dim lineNumber As Integer = 0
        Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
        For Each InputLine As String In FileContents
            InputLineArray = InputLine.Split("|")
            'check column U date - pre 1.4.2015 is recycle case
            lineNumber += 1
            'ignore heading but add to out files
            If lineNumber = 1 Then
                recycledCases = InputLine & vbNewLine
                NewCases = recycledCases
                Continue For
            End If
            Dim testDate As Date
            Dim testDateString As String = ""
            Try
                testDateString = InputLineArray(20)
                testDateString = Microsoft.VisualBasic.Right(testDateString, testDateString.Length - InStr(testDateString, "day") - 3)
                testDateString = Replace(testDateString, "th", "")
                testDateString = Replace(testDateString, "1st", "1")
                testDateString = Replace(testDateString, "nd", "")
                testDateString = Replace(testDateString, "rd", "")
                testDate = CDate(testDateString)
            Catch ex As Exception
                MsgBox("Invalidate date at line " & lineNumber & " - " & testDateString)
            End Try
            If Format(testDate, "yyyy-MM-dd") < apr2015 Then
                recycledCases &= InputLine & vbNewLine
                recycledBal += InputLineArray(17)
                recycledNo += 1
            Else
                NewCases &= InputLine & vbNewLine
                newNo += 1
                newBal += InputLineArray(17)
            End If
        Next
        auditfile = "Recycled Cases = " & recycledNo & " balance = " & recycledBal & vbNewLine
        auditfile &= "New cases = " & newNo & " balance = " & newBal & vbNewLine
        My.Computer.FileSystem.WriteAllText(InputFilePath & FileName & "_audit.txt", auditfile, False)
        My.Computer.FileSystem.WriteAllText(InputFilePath & FileName & "_recycled.txt", recycledCases, False)
        My.Computer.FileSystem.WriteAllText(InputFilePath & FileName & "_newcases.txt", NewCases, False)

        MsgBox("reports saved")
        Me.Close()
    End Sub
End Class

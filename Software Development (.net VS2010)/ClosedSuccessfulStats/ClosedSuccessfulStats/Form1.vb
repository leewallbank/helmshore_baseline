﻿Public Class Form1
    Dim emp_no_cases, self_emp_no_cases As Integer
    Dim tot_remit_fees_emp As Decimal
    Dim tot_remit_fees_self_emp As Decimal
    Dim emp_bail_array(5000) As Integer
    Dim self_emp_bail_array(5000) As Integer
    Dim outfile As String = "Month,Emp No of cases,Emp remitted fees, emp min, emp avg, emp max," &
        "Self Emp No of cases, self emp remitted fees,self emp min, self emp avg, self emp max" & vbNewLine
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        For idx = 1 To 5000
            emp_bail_array(idx) = 0
            self_emp_bail_array(idx) = 0
        Next
        Dim emp_min As Integer = 9999
        Dim emp_max As Integer = 0
        Dim emp_no_bailiffs As Decimal = 0
        Dim emp_total As Integer = 0
        Dim self_emp_min As Integer = 9999
        Dim self_emp_max As Integer = 0
        Dim self_emp_no_bailiffs As Decimal = 0
        Dim self_emp_total As Integer = 0
        Dim start_date As Date = CDate("Feb 1 2012")
        Dim end_date As Date = CDate("Feb 1 2013")
        Dim last_return_date As Date = Nothing
        param2 = "select _rowid, clientSchemeID, return_date from Debtor" &
            " where status = 'S' and status_open_closed = 'C'" &
            " and return_date >= '" & Format(start_date, "yyyy-MM-dd") & "'" &
            " order by return_date"
        Dim debt_ds As DataSet = get_dataset("onestep", param2)
        Dim debt_rows As Integer = no_of_rows - 1
        For debt_idx = 0 To debt_rows
            Try
                ProgressBar1.Value = (debt_idx / debt_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            Dim return_date As Date = debt_ds.Tables(0).Rows(debt_idx).Item(2)
            If last_return_date <> Nothing Then
                If Format(last_return_date, "yyyy-MM") <> Format(return_date, "yyyy-MM") Then
                    emp_min = 9999
                    emp_max = 0
                    emp_no_bailiffs = 0
                    emp_total = 0
                    self_emp_min = 9999
                    self_emp_max = 0
                    self_emp_no_bailiffs = 0
                    self_emp_total = 0
                    For idx = 1 To 5000
                        If emp_bail_array(idx) > 0 Then
                            emp_total += emp_bail_array(idx)
                            emp_no_bailiffs += 1
                            If emp_bail_array(idx) < emp_min Then
                                emp_min = emp_bail_array(idx)
                            End If
                            If emp_bail_array(idx) > emp_max Then
                                emp_max = emp_bail_array(idx)
                            End If
                        End If
                        If self_emp_bail_array(idx) > 0 Then
                            self_emp_no_bailiffs += 1
                            self_emp_total += self_emp_bail_array(idx)
                            If self_emp_bail_array(idx) < self_emp_min Then
                                self_emp_min = self_emp_bail_array(idx)
                            End If
                            If self_emp_bail_array(idx) > self_emp_max Then
                                self_emp_max = self_emp_bail_array(idx)
                            End If
                        End If
                    Next
                    outfile = outfile & Format(last_return_date, "MM-yyyy") & "," & emp_no_cases & "," & Format(tot_remit_fees_emp / emp_no_cases, "n") & "," &
           emp_min & "," & Format(emp_total / emp_no_bailiffs, "n") & "," & emp_max & "," & self_emp_no_cases & "," &
           Format(tot_remit_fees_self_emp / self_emp_no_cases, "n") & "," & self_emp_min & "," & Format(self_emp_total / self_emp_no_bailiffs, "n") &
           "," & self_emp_max & vbNewLine
                    For idx = 1 To 5000
                        emp_bail_array(idx) = 0
                        self_emp_bail_array(idx) = 0
                    Next
                    emp_no_cases = 0
                    self_emp_no_cases = 0
                    tot_remit_fees_emp = 0
                    tot_remit_fees_self_emp = 0
                End If
            End If
            last_return_date = return_date
            If Format(return_date, "yyyy-MM-dd") >= Format(end_date, "yyyy-MM-dd") Then
                Exit For
            End If
            Dim cs_ID As Integer = debt_ds.Tables(0).Rows(debt_idx).Item(1)
            param2 = "select clientID, schemeID, branchID from clientScheme where _rowid = " & cs_ID
            Dim cs_ds As DataSet = get_dataset("onestep", param2)
            Dim cl_ID As Integer = cs_ds.Tables(0).Rows(0).Item(0)
            If cl_ID = 1 Or cl_ID = 2 Or cl_ID = 24 Then
                Continue For
            End If
            If cs_ds.Tables(0).Rows(0).Item(2) <> 1 Then
                Continue For
            End If
            Dim sch_id As Integer = cs_ds.Tables(0).Rows(0).Item(1)
            param2 = "select work_type from Scheme where _rowid = " & sch_id
            Dim sch_ds As DataSet = get_dataset("onestep", param2)
            If sch_ds.Tables(0).Rows(0).Item(0) <> 2 Then
                Continue For
            End If
            Dim debtorID As Integer = debt_ds.Tables(0).Rows(debt_idx).Item(0)
            'no van visit
            param2 = "select bailiffID from Visit where debtorID = " & debtorID &
                " and date_visited is not null " &
                " order by _rowid"
            Dim visit_ds As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim visit_rows As Integer = no_of_rows - 1
            Dim van_found As Boolean = False
            Dim bailiff_ID As Integer
            Dim bail_type As String = ""
            Dim last_bailiff_id As Integer = 0
            For visit_idx = 0 To visit_rows
                bailiff_ID = visit_ds.Tables(0).Rows(visit_idx).Item(0)
                If last_bailiff_id = bailiff_ID Then
                    Continue For
                End If
                param2 = "select internalExternal, typeSub from Bailiff where _rowid = " & bailiff_ID
                Dim bail_ds As DataSet = get_dataset("onestep", param2)
                If bail_ds.Tables(0).Rows(0).Item(0) = "E" Then
                    van_found = True
                    Exit For
                End If
                Try
                    bail_type = bail_ds.Tables(0).Rows(0).Item(1)
                Catch ex As Exception
                    bail_type = ""
                End Try
                last_bailiff_id = bailiff_ID
            Next
            If van_found Then
                Continue For
            End If
            'fees
            param2 = "Select type, fee_amount, fee_remit_col, remited_fee from Fee where debtorID = " & debtorID &
                " and fee_remit_col >=3 and fee_remit_col <=4 and fee_amount > 0"
            Dim fee_ds As DataSet = get_dataset("onestep", param2)
            Dim fee_rows As Integer = no_of_rows - 1
            Dim remit_fees As Decimal = 0
            For fee_idx = 0 To fee_rows
                If InStr(LCase(fee_ds.Tables(0).Rows(fee_idx).Item(0)), "van") > 0 Then
                    van_found = True
                    Exit For
                End If
                If fee_ds.Tables(0).Rows(fee_idx).Item(2) = 3 Then
                    remit_fees += fee_ds.Tables(0).Rows(fee_idx).Item(3)
                End If
            Next
            If van_found Then
                Continue For
            End If
            If bail_type = "Employed" Then
                emp_no_cases += 1
                tot_remit_fees_emp += remit_fees
                emp_bail_array(bailiff_ID) += 1
            Else
                self_emp_no_cases += 1
                tot_remit_fees_self_emp += remit_fees
                self_emp_bail_array(bailiff_ID) += 1
            End If
        Next
       
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "CSV files |*.csv"
            .DefaultExt = ".csv"
            .OverwritePrompt = True
            .FileName = "stats.csv"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
        End If
        MsgBox("Report saved")
        Me.Close()
    End Sub
End Class

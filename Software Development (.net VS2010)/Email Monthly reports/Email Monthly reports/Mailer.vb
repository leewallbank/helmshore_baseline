﻿'---------------------------------------------------------------------
'  This file is part of the Microsoft .NET Framework SDK Code Samples.
' 
'  Copyright (C) Microsoft Corporation.  All rights reserved.
' 
'This source code is intended only as a supplement to Microsoft
'Development Tools and/or on-line documentation.  See these other
'materials for detailed information regarding Microsoft code samples.
' 
'THIS CODE AND INFORMATION ARE PROVIDED AS IS WITHOUT WARRANTY OF ANY
'KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
'IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
'PARTICULAR PURPOSE.
'---------------------------------------------------------------------

Imports System.net.mail

Module Mailer
    Function email(ByVal toaddress, ByVal fromaddress, ByVal subject, ByVal body, _
       Optional ByVal attachment1 = " ", _
       Optional ByVal attachment2 = " ", _
        Optional ByVal attachment3 = " ", _
        Optional ByVal attachment4 = " ", _
        Optional ByVal attachment5 = " ", _
        Optional ByVal attachment6 = " ", _
        Optional ByVal attachment7 = " ", _
        Optional ByVal attachment8 = " ", _
        Optional ByVal attachment9 = " ", _
       Optional ByVal cc_array = "") As Integer
        Dim mailServerName As String = "192.168.103.241"
        log_message = client_document & " sent to " & toaddress
        error_message = ""    'client_document & " due to be sent to := " & toaddress & vbNewLine
        Dim attachmentFound As Boolean = False
        Try
            Using message As New MailMessage(fromaddress, toaddress, subject, body)
                Dim mailClient As New SmtpClient(mailServerName)
                mailClient.UseDefaultCredentials = True
                'Dim idx As Integer
                'For idx = 0 To 100
                '    Dim cc As String = cc_array(idx)
                '    If cc Is Nothing Then
                '        Exit For
                '    End If
                '    message.CC.Add(cc)
                '    log_message = log_message & " and " & cc
                '    error_message = error_message & " and " & cc
                'Next

                If cc_all > " " Then
                    message.CC.Add(cc_all)
                End If
                If attachment1 <> " " Then
                    Try
                        Dim att1 As New Attachment(attachment1)
                        message.Attachments.Add(att1)
                        attachmentFound = True
                    Catch ex As Exception
                        error_message &= "attachment not sent for " & attachment1 & vbNewLine
                    End Try
                End If
                If attachment2 <> " " Then
                    Try
                        Dim att2 As New Attachment(attachment2)
                        message.Attachments.Add(att2)
                        attachmentFound = True
                    Catch ex As Exception
                        error_message &= "attachment not sent for " & attachment2 & vbNewLine
                    End Try
                    
                End If
                If attachment3 <> " " Then
                    Try
                        Dim att3 As New Attachment(attachment3)
                        message.Attachments.Add(att3)
                        attachmentFound = True
                    Catch ex As Exception
                        error_message &= "attachment not sent for " & attachment3 & vbNewLine
                    End Try
                   
                End If
                If attachment4 <> " " Then
                    Try
                        Dim att4 As New Attachment(attachment4)
                        message.Attachments.Add(att4)
                        attachmentFound = True
                    Catch ex As Exception
                        error_message &= "attachment not sent for " & attachment4 & vbNewLine
                    End Try
                    
                End If
                If attachment5 <> " " Then
                    Try
                        Dim att5 As New Attachment(attachment5)
                        message.Attachments.Add(att5)
                        attachmentFound = True
                    Catch ex As Exception
                        error_message &= "attachment not sent for " & attachment5 & vbNewLine
                    End Try
                    
                End If
                If attachment6 <> " " Then
                    Try
                        Dim att6 As New Attachment(attachment6)
                        message.Attachments.Add(att6)
                        attachmentFound = True
                    Catch ex As Exception
                        error_message &= "attachment not sent for " & attachment6 & vbNewLine
                    End Try
                    
                End If
                If attachment7 <> " " Then
                    Try
                        Dim att7 As New Attachment(attachment7)
                        message.Attachments.Add(att7)
                        attachmentFound = True
                    Catch ex As Exception
                        error_message &= "attachment not sent for " & attachment7 & vbNewLine
                    End Try
                    
                End If
                If attachment8 <> " " Then
                    Try
                        Dim att8 As New Attachment(attachment8)
                        message.Attachments.Add(att8)
                        attachmentFound = True
                    Catch ex As Exception
                        error_message &= "attachment not sent for " & attachment8 & vbNewLine
                    End Try
                End If
                If attachment9 <> " " Then
                    Try
                        Dim att9 As New Attachment(attachment9)
                        message.Attachments.Add(att9)
                        attachmentFound = True
                    Catch ex As Exception
                        error_message &= "attachment not sent for " & attachment9 & vbNewLine
                    End Try
                End If
                If attachmentFound Then
                    mailClient.Send(message)
                Else
                    log_message = client_document & " NOT sent to " & toaddress & " as no attachment"
                End If

            End Using
            log_message = log_message & vbNewLine
            write_log()
            If attachmentfound Then
                Return (0)
            Else
                Return (10)
            End If
            If error_message <> "" Then
                write_error()
            End If
        Catch ex As Exception
            error_message = client_document & " due to be sent to := " & _
            toaddress & vbNewLine & "not sent due to := " & ex.Message
            write_error()
            Return (10)
        End Try
    End Function
End Module

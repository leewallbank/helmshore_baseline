<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ccform
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.cc_cc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ccDataGridView = New System.Windows.Forms.DataGridView
        Me.cc2_cc = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cl_lbl = New System.Windows.Forms.Label
        Me.sch_lbl = New System.Windows.Forms.Label
        Me.FeesSQLDataSet = New Email_Monthly_reports.FeesSQLDataSet
        Me.Client_email_ccsTableAdapter = New Email_Monthly_reports.FeesSQLDataSetTableAdapters.client_email_ccsTableAdapter
        Me.Client_emailsTableAdapter = New Email_Monthly_reports.FeesSQLDataSetTableAdapters.client_emailsTableAdapter
        Me.Client_email_ccsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Client_emailsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ccDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Client_email_ccsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Client_emailsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cc_cc})
        Me.DataGridView1.Location = New System.Drawing.Point(12, 119)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(368, 282)
        Me.DataGridView1.TabIndex = 0
        '
        'cc_cc
        '
        Me.cc_cc.HeaderText = "CC email address"
        Me.cc_cc.Name = "cc_cc"
        Me.cc_cc.Width = 250
        '
        'ccDataGridView
        '
        Me.ccDataGridView.AllowUserToOrderColumns = True
        Me.ccDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ccDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cc2_cc})
        Me.ccDataGridView.Location = New System.Drawing.Point(2, 37)
        Me.ccDataGridView.Name = "ccDataGridView"
        Me.ccDataGridView.Size = New System.Drawing.Size(252, 210)
        Me.ccDataGridView.TabIndex = 0
        '
        'cc2_cc
        '
        Me.cc2_cc.HeaderText = "CC"
        Me.cc2_cc.Name = "cc2_cc"
        Me.cc2_cc.Width = 200
        '
        'cl_lbl
        '
        Me.cl_lbl.AutoSize = True
        Me.cl_lbl.Location = New System.Drawing.Point(12, 9)
        Me.cl_lbl.Name = "cl_lbl"
        Me.cl_lbl.Size = New System.Drawing.Size(39, 13)
        Me.cl_lbl.TabIndex = 1
        Me.cl_lbl.Text = "Label1"
        '
        'sch_lbl
        '
        Me.sch_lbl.AutoSize = True
        Me.sch_lbl.Location = New System.Drawing.Point(141, 9)
        Me.sch_lbl.Name = "sch_lbl"
        Me.sch_lbl.Size = New System.Drawing.Size(39, 13)
        Me.sch_lbl.TabIndex = 2
        Me.sch_lbl.Text = "Label2"
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Client_email_ccsTableAdapter
        '
        Me.Client_email_ccsTableAdapter.ClearBeforeFill = True
        '
        'Client_emailsTableAdapter
        '
        Me.Client_emailsTableAdapter.ClearBeforeFill = True
        '
        'Client_email_ccsBindingSource
        '
        Me.Client_email_ccsBindingSource.DataMember = "client email ccs"
        Me.Client_email_ccsBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'Client_emailsBindingSource
        '
        Me.Client_emailsBindingSource.DataMember = "client emails"
        Me.Client_emailsBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'ccform
        '
        Me.ClientSize = New System.Drawing.Size(310, 372)
        Me.Controls.Add(Me.sch_lbl)
        Me.Controls.Add(Me.cl_lbl)
        Me.Controls.Add(Me.ccDataGridView)
        Me.Name = "ccform"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Maintain list of cc's"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ccDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Client_email_ccsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Client_emailsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents cl_namelbl As System.Windows.Forms.Label
    Friend WithEvents sch_namelbl As System.Windows.Forms.Label
    Friend WithEvents cc_cc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ccDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents cl_lbl As System.Windows.Forms.Label
    Friend WithEvents sch_lbl As System.Windows.Forms.Label
    Friend WithEvents cc2_cc As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FeesSQLDataSet As Email_Monthly_reports.FeesSQLDataSet
    Friend WithEvents Client_email_ccsTableAdapter As Email_Monthly_reports.FeesSQLDataSetTableAdapters.client_email_ccsTableAdapter
    Friend WithEvents Client_emailsTableAdapter As Email_Monthly_reports.FeesSQLDataSetTableAdapters.client_emailsTableAdapter
    Friend WithEvents Client_email_ccsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Client_emailsBindingSource As System.Windows.Forms.BindingSource
End Class

Imports System.Collections
Public Class mainfrm
    Private RA863report As RA863
    Private Const PARAMETER_FIELD_NAME1 As String = "parm_debtor"
    Dim myArrayList1 As ArrayList = New ArrayList()
    Dim folder_name, folder_name2, bail_type, xref_fname As String
    Dim start_date, end_date As Date


    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim days As Integer = 6 + Weekday(Now)
        start_Date_picker.Value = DateAdd(DateInterval.Day, -days, Now)
        end_date_picker.Value = DateAdd(DateInterval.Day, 6, start_Date_picker.Value)
    End Sub

    Private Sub datesbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub filebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles filebtn.Click
        exitbtn.Enabled = False
        Dim start_day, end_day As String
        start_date = start_Date_picker.Value
        end_date = end_date_picker.Value
        start_day = Format(start_date, "ddd")
        end_day = Format(end_date, "ddd")
        If MsgBox("Run from " & start_day & " " & Format(start_date, "dd.MM.yyyy") & " to " & end_day & " " & Format(end_date, "dd.MM.yyyy"), _
        MsgBoxStyle.OkCancel, "Create files") <> MsgBoxResult.Ok Then
            MsgBox("Files not created")
            exitbtn.Enabled = True
            Exit Sub
        End If
        ProgressBar1.Value = 5
        'get all csids for North Somerset branch 1 
        param1 = "onestep"
        param2 = "select _rowid from ClientScheme where clientID = 599 and branchID = 1"
        Dim csid_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("There are no schemes for North Somerset")
            Exit Sub
        End If
        'save csids in an array
        Dim csid_array(20) As Integer
        Dim csid_idx As Integer = 0
        Dim idx4 As Integer
        For idx4 = 0 To no_of_rows - 1
            csid_idx += 1
            csid_array(csid_idx) = csid_dataset.Tables(0).Rows(idx4).Item(0)
        Next
        'add 1 day to end_date as return date is a datetime field
        end_date = DateAdd(DateInterval.Day, 1, end_date)
        param2 = "select _rowid, client_ref, clientschemeID from Debtor " & _
        " where status_open_closed = 'C' and status = 'S'" & _
        " and return_date >='" & Format(start_date, "yyyy.MM.dd") & _
        "' and return_date <'" & Format(end_date, "yyyy.MM.dd") & "' order by client_ref"
        Dim debtor_dataset = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("There are no cases for this period")
            Exit Sub
        End If
        filebtn.Enabled = False
        Dim debtor_rows As Integer = no_of_rows
        Dim idx As Integer
        Dim fname As String = ""
        Dim first_case As Boolean = True
        For idx = 0 To debtor_rows - 1
            ProgressBar1.Value = (idx / debtor_rows) * 100
            Application.DoEvents()
            Dim csid As Integer
            csid = debtor_dataset.tables(0).rows(idx).item(2)
            Dim idx3 As Integer
            Dim csid_found As Boolean = False
            For idx3 = 1 To csid_idx
                If csid = csid_array(idx3) Then
                    csid_found = True
                    Exit For
                End If
            Next
            If Not csid_found Then
                Continue For
            End If
            Dim cl_ref As String
            cl_ref = Trim(debtor_dataset.tables(0).rows(idx).item(1).ToString)
            Dim debtor As Integer = debtor_dataset.tables(0).rows(idx).item(0)
            If first_case Then
                first_case = False
                With SaveFileDialog1
                    .Title = "Save files"
                    .Filter = "pdf|*.pdf"
                    .DefaultExt = ".pdf"
                    .OverwritePrompt = True
                    .FileName = debtor & "-" & cl_ref & ".pdf"
                    .Title = "Select folder for saving files (New folder NorthSomerset will be created)"
                End With
                If SaveFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
                    MsgBox("Files not created")
                    exitbtn.Enabled = True
                    Exit Sub
                Else
                    fname = SaveFileDialog1.FileName
                    Dim idx2 As Integer
                    For idx2 = fname.Length To 1 Step -1
                        If Mid(fname, idx2, 1) = "\" Then
                            fname = Microsoft.VisualBasic.Left(fname, idx2) & "NorthSomerset\"
                            'check directory exists
                            Try
                                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(fname)
                                If System.IO.Directory.Exists(fname) = False Then
                                    di = System.IO.Directory.CreateDirectory(fname)
                                Else
                                    System.IO.Directory.Delete(fname, True)
                                    di = System.IO.Directory.CreateDirectory(fname)
                                End If
                            Catch ex As Exception
                                MsgBox("Unable to create folder")
                                End
                            End Try
                            Exit For
                        End If
                    Next
                End If
            End If
            write_report(debtor, cl_ref, fname)
        Next
        MsgBox("Files created in " & fname)
        Me.Close()
    End Sub
    Sub write_report(ByVal debtor As Integer, ByVal cl_ref As String, ByVal fname As String)
        Dim RA863Report = New RA863
        myArrayList1.Add(debtor)
        SetCurrentValuesForParameterField1(RA863Report, myArrayList1)
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA863Report)
        Dim file As String = fname & cl_ref & "-" & debtor & ".pdf"
        RA863Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, file)
        RA863Report.close()
    End Sub
    Public Sub SetDBLogonForReport(ByVal myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo, ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
            myTableLogonInfo.ConnectionInfo = myConnectionInfo
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub
    Private Sub SetCurrentValuesForParameterField1(ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myArrayList As ArrayList)
        Dim currentParameterValues As CrystalDecisions.Shared.ParameterValues = New CrystalDecisions.Shared.ParameterValues()
        For Each submittedValue As Object In myArrayList
            Dim myParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue()
            myParameterDiscreteValue.Value = submittedValue.ToString()
            currentParameterValues.Add(myParameterDiscreteValue)
        Next
        Dim myParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        Dim myParameterFieldDefinition As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME1)
        myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)
    End Sub
   
    
End Class

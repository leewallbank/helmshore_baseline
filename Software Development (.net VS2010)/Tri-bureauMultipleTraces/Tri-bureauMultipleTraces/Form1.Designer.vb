﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tablebtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.FeesSQLDataSet = New Tri_bureauMultipleTraces.FeesSQLDataSet()
        Me.Tri_bureau_multiple_tracesTableAdapter = New Tri_bureauMultipleTraces.FeesSQLDataSetTableAdapters.tri_bureau_multiple_tracesTableAdapter()
        Me.TableAdapterManager = New Tri_bureauMultipleTraces.FeesSQLDataSetTableAdapters.TableAdapterManager()
        Me.TriBureauxTraceResultsTableAdapter = New Tri_bureauMultipleTraces.FeesSQLDataSetTableAdapters.TriBureauxTraceResultsTableAdapter()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tablebtn
        '
        Me.tablebtn.Location = New System.Drawing.Point(71, 83)
        Me.tablebtn.Name = "tablebtn"
        Me.tablebtn.Size = New System.Drawing.Size(97, 23)
        Me.tablebtn.TabIndex = 0
        Me.tablebtn.Text = "populate table"
        Me.tablebtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(220, 212)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Tri_bureau_multiple_tracesTableAdapter
        '
        Me.Tri_bureau_multiple_tracesTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.tri_bureau_multiple_tracesTableAdapter = Me.Tri_bureau_multiple_tracesTableAdapter
        Me.TableAdapterManager.UpdateOrder = Tri_bureauMultipleTraces.FeesSQLDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'TriBureauxTraceResultsTableAdapter
        '
        Me.TriBureauxTraceResultsTableAdapter.ClearBeforeFill = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(2, 212)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(320, 266)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.tablebtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Tri-bureau multiple traces"
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tablebtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents FeesSQLDataSet As Tri_bureauMultipleTraces.FeesSQLDataSet
    Friend WithEvents Tri_bureau_multiple_tracesTableAdapter As Tri_bureauMultipleTraces.FeesSQLDataSetTableAdapters.tri_bureau_multiple_tracesTableAdapter
    Friend WithEvents TableAdapterManager As Tri_bureauMultipleTraces.FeesSQLDataSetTableAdapters.TableAdapterManager
    Friend WithEvents TriBureauxTraceResultsTableAdapter As Tri_bureauMultipleTraces.FeesSQLDataSetTableAdapters.TriBureauxTraceResultsTableAdapter
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar

End Class

﻿Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub tablebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tablebtn.Click
        Tri_bureau_multiple_tracesTableAdapter.DeleteQuery()
        Me.TriBureauxTraceResultsTableAdapter.Fill(Me.FeesSQLDataSet.TriBureauxTraceResults)
        Dim trace_rows As Integer = Me.FeesSQLDataSet.TriBureauxTraceResults.Rows.Count - 1
        Dim last_debtorID As Integer = 0
        Dim seq_no As Integer = 0
        Dim tri_end_date As Date
        Dim last_batch_no As Integer
        Dim last_supplier_no As Integer
        For trace_idx = 0 To trace_rows
            Try
                ProgressBar1.Value = (trace_idx / trace_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            Dim debtorid As Integer = Me.FeesSQLDataSet.TriBureauxTraceResults.Rows(trace_idx).Item(0)
            Dim batch_no As Integer = Me.FeesSQLDataSet.TriBureauxTraceResults.Rows(trace_idx).Item(2)
            Dim supplier_no As Integer = Me.FeesSQLDataSet.TriBureauxTraceResults.Rows(trace_idx).Item(1)
            If debtorid = last_debtorID Then
                seq_no += 1
            Else
                seq_no = 1
            End If
            last_debtorID = debtorid

            tri_end_date = CDate("jan 1 2100")
            Try
                Tri_bureau_multiple_tracesTableAdapter.InsertQuery(debtorid, batch_no, supplier_no, seq_no, tri_end_date)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
            If seq_no > 1 Then
                'update tri-end_date on last seq no
                tri_end_date = Me.FeesSQLDataSet.TriBureauxTraceResults.Rows(trace_idx).Item(6)
                Try
                    Tri_bureau_multiple_tracesTableAdapter.UpdateQuery(tri_end_date, debtorid, last_batch_no, last_supplier_no, seq_no - 1)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            End If
            last_batch_no = batch_no
            last_supplier_no = supplier_no
        Next
        MsgBox("completed")
        Me.Close()
    End Sub

End Class

﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration

Public Class frmMain
    Private InputFilePath As String, FileName As String, FileExt As String
    Dim outfile_asset, outfile_first, outfile_sb_asset, outfile_sb_first, outfile_zug, outfile_sb_zug As String
    Dim outline As String = ""
    Dim client_found As Integer = 0
    Dim statute_barred_case As Boolean = False
    Dim lodate As Date
    Dim clref As String = ""
    Dim off_number, batch_no, off_location As String
    Dim off_court As Date
    Dim title, forename, surname As String
    Dim dob As Date
    Dim addr1, addr2, addr3, addr4, pcode As String
    Dim d_addr1, d_addr2, d_addr3, d_addr4, d_addr5, d_pcode As String
    Dim phone, emp_phone, fax As String
    Dim notes As String
    Dim joint_name As String
    Dim joint_address As String
    Dim prev_addr As String
    Dim statute_barred As String
    Dim statute_barred_date As Date
    Dim ignore_row As Boolean
    Dim CaseBalance As Decimal
    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim colval As String = ""
        Dim test_date As Date
        Dim test_amt As Decimal
        Dim spaces As Integer = 250
        Try
            Dim FileDialog As New OpenFileDialog
            Dim LineNumber As Integer = 0
            Dim AuditLog As String = "", ErrorLog As String = "", InputLineArray() As String
            Dim first_TotalBalance As Decimal = 0
            Dim first_TotalBalanceSB As Decimal = 0
            Dim asset_TotalBalance As Decimal = 0
            Dim asset_TotalBalanceSB As Decimal = 0
            Dim zug_TotalBalance As Decimal = 0
            Dim zug_TotalBalanceSB As Decimal = 0
            Dim first_Cases As Integer = 0
            Dim first_casesSB As Integer = 0
            Dim asset_Cases As Integer = 0
            Dim asset_casesSB As Integer = 0
            Dim zug_Cases As Integer = 0
            Dim zug_casesSB As Integer = 0
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            lblReadingFile.Visible = True

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            lblReadingFile.Visible = False
            ProgressBar.Maximum = UBound(FileContents)
            batch_no = Nothing
            batch_no = InputBox("Enter Batch Number", "Batch Number")
            If batch_no = Nothing Then
                MsgBox("Incorrect batch number")
                Exit Sub
            End If
            'write out headings
            outfile_sb_asset = "Client Ref|OffenceNumber|OffenceCourt|CurrentBalance|Title|Forename|Surname|DOB|" & _
                        "Add1|Add2|Add3|Add4|Pcode|Phone|EmpPhone|Fax|DebtAdd1|DebtAdd2|DebtAdd3|" & _
                        "DebtAdd4|DebtAdd5|DebtPC|Batch No|StatuteBarred|OffenceLocation|Notes" & vbNewLine
            outfile_sb_first = outfile_sb_asset
            outfile_sb_zug = outfile_sb_asset

            outfile_asset = "Client Ref|OffenceNumber|OffenceCourt|CurrentBalance|Title|Forename|Surname|DOB|" & _
                    "Add1|Add2|Add3|Add4|Pcode|Phone|EmpPhone|Fax|DebtAdd1|DebtAdd2|DebtAdd3|" & _
                    "DebtAdd4|DebtAdd5|DebtPC|Batch No|Notes" & vbNewLine
            outfile_first = outfile_asset
            outfile_zug = outfile_asset
            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                Application.DoEvents()
                LineNumber += 1
                'ignore first row
                If LineNumber = 1 Then
                    Continue For
                End If
                If LineNumber - 1 > UBound(FileContents) Then
                    Continue For
                End If
                Dim columnidx As Integer = 0
                Dim columnNumber As Integer
                'InputLineArray = InputLine.Split(",")
                InputLineArray = Split(InputLine, ",", """")
                For columnidx = 0 To 52
                    Try
                        colval = Trim(InputLineArray(columnidx))
                    Catch ex As Exception
                        MsgBox("error on line " & LineNumber & " column " & columnidx)
                        Me.Close()
                    End Try
                    colval = Trim(InputLineArray(columnidx))
                    'remove any quotes
                    Dim letIDX As Integer
                    Dim new_colval As String = ""
                    For letIDX = 1 To colval.Length
                        If Mid(colval, letIDX, 1) <> Chr(34) Then
                            new_colval &= Mid(colval, letIDX, 1)
                        End If
                    Next
                    colval = new_colval
                    columnNumber = columnidx + 1
                    Select Case columnNumber
                        Case 1
                            If colval = "" Then
                                ignore_row = True
                                Exit For
                            End If
                            spaces = 250
                            notes = "ClientCode:" & colval & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        Case 2
                            notes = notes & "ClientName:" & colval & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        Case 3
                            client_found = 0
                            If colval = "Aktiv Kapital First Investment Limited" Then
                                client_found = 1
                            ElseIf colval = "Aktiv Kapital Asset Investments Limited" Then
                                client_found = 2
                            ElseIf colval = "Aktiv Kapital Portfolio AS Zug Branch" Then
                                client_found = 3
                            Else
                                AuditLog &= "Invalid Master Client - " & colval & " Line " & LineNumber & vbNewLine
                            End If
                            notes = notes & "MasterClient:" & colval & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        Case 4
                            clref = colval
                        Case 5
                            off_number = colval
                        Case 6
                            Try
                                off_court = colval
                            Catch ex As Exception
                                off_court = Nothing
                            End Try
                        Case 7
                            'notes = notes & "DebtType:" & colval & ";"
                            'notes = notes & Space(spaces - notes.Length)
                            'spaces += 250
                            off_location = colval
                        Case 8
                            If colval.Length > 0 Then
                                notes = notes & "DebtType2:" & colval & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                        Case 9
                            CaseBalance = 0
                            Try
                                CaseBalance = colval
                            Catch ex As Exception
                                AuditLog &= "Invalid debt amount " & colval & " on line - " & LineNumber & vbNewLine
                            End Try

                        Case 10
                            notes = notes & "DefaultBal:" & colval & ";"
                            notes = notes & Space(spaces - notes.Length)
                            spaces += 250
                        Case 11
                            If colval.Length > 0 Then
                                Try
                                    test_date = colval
                                Catch ex As Exception
                                    test_date = Nothing
                                End Try
                                '19.6.2014 moved inside if/end if
                                If test_date <> Nothing Then
                                    notes = notes & "DefaultDate:" & Format(test_date, "dd/MM/yyyy") & ";"
                                    notes = notes & Space(spaces - notes.Length)
                                    spaces += 250
                                End If
                            End If

                        Case 12
                            title = colval
                        Case 13
                            forename = colval
                        Case 14
                            surname = colval
                        Case 15
                            Try
                                dob = colval
                            Catch ex As Exception
                                dob = Nothing
                            End Try
                        Case 16
                            addr1 = colval
                        Case 17
                            addr2 = colval
                        Case 18
                            addr3 = colval
                        Case 19
                            addr4 = colval
                        Case 20
                            pcode = colval
                        Case 21
                            phone = colval
                        Case 22
                            emp_phone = colval
                        Case 23
                            fax = colval
                        Case 24
                            joint_name = ""
                            If colval.Length > 0 Then
                                joint_name = colval
                            End If
                        Case 25
                            If colval.Length > 0 Then
                                joint_name = Trim(joint_name & " " & colval)
                            End If
                        Case 26
                            If colval.Length > 0 Then
                                joint_name = Trim(joint_name & " " & colval)
                            End If
                            If joint_name.Length > 0 Then
                                notes = notes & "JointName:" & joint_name & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                        Case 27
                            If colval.Length > 0 Then
                                Try
                                    test_date = colval
                                Catch ex As Exception
                                    test_date = Nothing
                                End Try
                                If test_date <> Nothing Then
                                    notes = notes & "JointDOB:" & Format(test_date, "dd/MM/yyyy") & ";"
                                    notes = notes & Space(spaces - notes.Length)
                                    spaces += 250
                                End If
                            End If
                        Case 28
                            If colval.Length > 0 Then
                                joint_address = colval
                            Else
                                joint_address = ""
                            End If
                        Case 29
                            If colval.Length > 0 Then
                                joint_address = Trim(joint_address & " " & colval)
                            End If
                        Case 30
                            If colval.Length > 0 Then
                                joint_address = Trim(joint_address & " " & colval)
                            End If
                        Case 31
                            If colval.Length > 0 Then
                                joint_address = Trim(joint_address & " " & colval)
                            End If
                        Case 32
                            If colval.Length > 0 Then
                                joint_address = Trim(joint_address & " " & colval)
                            End If
                            If joint_address.Length > 0 Then
                                notes = notes & "JointAddress:" & joint_address & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                        Case 33
                            If colval.Length > 0 Then
                                notes = notes & "JointTelNo:" & colval & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                        Case 34
                            If colval.Length > 0 Then
                                notes = notes & "JointEmpTelNo:" & colval & ";"
                            End If
                        Case 35
                            If colval.Length > 0 Then
                                notes = notes & "JointOtherTelNo:" & colval & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                        Case 36
                            Try
                                test_date = colval
                            Catch ex As Exception
                                test_date = Nothing
                            End Try
                            If test_date <> Nothing Then
                                notes = notes & "LastPayDate:" & Format(test_date, "dd/MM/yyyy") & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                        Case 37
                            If colval.Length > 0 Then
                                Try
                                    test_amt = colval
                                Catch ex As Exception
                                    test_amt = Nothing
                                End Try
                                If test_amt <> Nothing Then
                                    notes = notes & "LastPayVal:" & Format(test_amt, "fixed") & ";"
                                    notes = notes & Space(spaces - notes.Length)
                                    spaces += 250
                                End If
                            End If
                        Case 38
                            If colval.Length > 0 Then
                                Try
                                    test_date = colval
                                Catch ex As Exception
                                    test_date = Nothing
                                End Try
                                If test_date <> Nothing Then
                                    notes = notes & "ContractStartDate:" & Format(test_date, "dd/MM/yyyy") & ";"
                                    notes = notes & Space(spaces - notes.Length)
                                    spaces += 250
                                End If
                            End If
                        Case 39
                            If colval.Length > 0 Then
                                notes = notes & "CourtCode:" & colval & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                        Case 40
                            If colval.Length > 0 Then
                                notes = notes & "CaseNumber:" & colval & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                        Case 41
                            If colval.Length > 0 Then
                                Try
                                    test_date = colval
                                Catch ex As Exception
                                    test_date = Nothing
                                End Try
                                If test_date <> Nothing Then
                                    notes = notes & "JudgementDate:" & Format(test_date, "dd/MM/yyyy") & ";"
                                    notes = notes & Space(spaces - notes.Length)
                                    spaces += 250
                                End If
                            End If
                        Case 42
                            If colval.Length > 0 Then
                                prev_addr = colval
                            Else
                                prev_addr = ""
                            End If
                        Case 43
                            If colval.Length > 0 Then
                                prev_addr = Trim(prev_addr & " " & colval)
                            End If
                        Case 44
                            If colval.Length > 0 Then
                                prev_addr = Trim(prev_addr & " " & colval)
                            End If
                        Case 45
                            If colval.Length > 0 Then
                                prev_addr = Trim(prev_addr & " " & colval)
                            End If
                        Case 46
                            If colval.Length > 0 Then
                                prev_addr = Trim(prev_addr & " " & colval)
                            End If
                            If prev_addr.Length > 0 Then
                                'remove any commas from prev address
                                prev_addr = Replace(prev_addr, ",", " ")
                                notes = notes & "PrevAddr:" & prev_addr & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                        Case 47
                            d_addr1 = colval
                        Case 48
                            d_addr2 = colval
                        Case 49
                            d_addr3 = colval
                        Case 50
                            d_addr4 = colval
                        Case 51
                            d_pcode = colval
                        Case 52
                            statute_barred_date = Nothing
                            Try
                                test_date = colval
                            Catch ex As Exception
                                test_date = Nothing
                            End Try
                            If test_date <> Nothing Then
                                statute_barred_date = test_date
                            End If
                        Case 53
                            statute_barred = ""
                            If colval.Length > 0 Then
                                statute_barred = colval
                                notes = notes & "StatuteBarredDate:" & Format(statute_barred_date, "dd/MM/yyyy") & ";"
                                notes = notes & Space(spaces - notes.Length)
                                spaces += 250
                            End If
                    End Select
                Next
                If ignore_row Then
                    Exit For
                End If
                Select Case (client_found)
                    Case 1
                        If statute_barred = "1" Then
                            statute_barred_case = True
                            first_casesSB += 1
                            first_TotalBalanceSB += CaseBalance
                        Else
                            statute_barred_case = False
                            first_Cases += 1
                            first_TotalBalance += CaseBalance
                        End If
                    Case 2
                        If statute_barred = "1" Then
                            statute_barred_case = True
                            asset_casesSB += 1
                            asset_TotalBalanceSB += CaseBalance
                        Else
                            statute_barred_case = False
                            asset_Cases += 1
                            asset_TotalBalance += CaseBalance
                        End If
                    Case 3
                        If statute_barred = "1" Then
                            statute_barred_case = True
                            zug_casesSB += 1
                            zug_TotalBalanceSB += CaseBalance
                        Else
                            statute_barred_case = False
                            zug_Cases += 1
                            zug_TotalBalance += CaseBalance
                        End If
                End Select

                'shuffle addresses up
                If addr2.Length = 0 Then
                    addr2 = addr3
                    addr3 = addr4
                    addr4 = ""
                    If addr2.Length = 0 Then
                        addr2 = addr3
                        addr3 = ""
                    End If
                End If
                If addr3.Length = 0 Then
                    addr3 = addr4
                    addr4 = ""
                End If
                If d_addr2.Length = 0 Then
                    d_addr2 = d_addr3
                    d_addr3 = d_addr4
                    d_addr4 = d_addr5
                    d_addr5 = ""
                    If d_addr2.Length = 0 Then
                        d_addr2 = addr3
                        d_addr3 = d_addr4
                        d_addr4 = ""
                    End If
                End If
                If d_addr3 <> Nothing Then
                    If d_addr3.Length = 0 Then
                        d_addr3 = d_addr4
                        d_addr4 = d_addr5
                        d_addr5 = ""
                    End If
                End If

                'remove any commas from address
                addr1 = Replace(addr1, ",", " ")
                addr2 = Replace(addr2, ",", " ")
                addr3 = Replace(addr3, ",", " ")
                addr4 = Replace(addr4, ",", " ")
                d_addr1 = Replace(d_addr1, ",", " ")
                d_addr2 = Replace(d_addr2, ",", " ")
                d_addr3 = Replace(d_addr3, ",", " ")
                d_addr4 = Replace(d_addr4, ",", " ")
                d_addr5 = Replace(d_addr5, ",", " ")

                Try
                    outline = clref & "|" & off_number & "|"
                    If off_court = Nothing Then
                        outline = outline & "|"
                    Else
                        outline = outline & Format(off_court, "dd/MM/yyyy") & "|"
                    End If
                    outline = outline & Format(CaseBalance, "fixed") & "|" & title & "|" & forename & "|" & surname & "|"
                    If dob = Nothing Then
                        outline = outline & "|"
                    Else
                        outline = outline & Format(dob, "dd/MM/yyyy") & "|"
                    End If
                    outline = outline & addr1 & "|" & addr2 & "|" & addr3 & "|" & addr4 & "|" & _
                    pcode & "|" & phone & "|" & emp_phone & "|" & fax & "|" & d_addr1 & "|" & _
                    d_addr2 & "|" & d_addr3 & "|" & d_addr4 & "|" & d_addr5 & "|" & d_pcode & "|" & batch_no & _
                     "|" & statute_barred & "|" & off_location

                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                Try
                    If client_found = 2 Then
                        If statute_barred_case Then
                            outfile_sb_asset &= outline & "|" & notes & vbNewLine
                        Else
                            outfile_asset &= outline & "|" & notes & vbNewLine
                        End If
                    ElseIf client_found = 1 Then
                        If statute_barred_case Then
                            outfile_sb_first &= outline & "|" & notes & vbNewLine
                        Else
                            outfile_first &= outline & "|" & notes & vbNewLine
                        End If
                    ElseIf client_found = 3 Then
                        If statute_barred_case Then
                            outfile_sb_zug &= outline & "|" & notes & vbNewLine
                        Else
                            outfile_zug &= outline & "|" & notes & vbNewLine
                        End If
                    End If
                    outline = ""
                    notes = ""
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            Next InputLine

            'write out files in batches of 750
            Dim limit As Integer = 750
            Dim OutFileName As String
            Dim outfile As String
            If asset_Cases > 0 Then
                OutFileName = InputFilePath & FileName & "_asset_preprocess"
                outfile = outfile_asset
                write_file(limit, OutFileName, outfile)
            End If

            If first_Cases > 0 Then
                OutFileName = InputFilePath & FileName & "_first_preprocess"
                outfile = outfile_first
                write_file(limit, OutFileName, outfile)
            End If

            If zug_Cases > 0 Then
                OutFileName = InputFilePath & FileName & "_zug_preprocess"
                outfile = outfile_zug
                write_file(limit, OutFileName, outfile)
            End If

            If asset_casesSB > 0 Then
                OutFileName = InputFilePath & FileName & "_asset_sb_preprocess"
                outfile = outfile_sb_asset
                write_file(limit, OutFileName, outfile)
            End If

            If first_casesSB > 0 Then
                OutFileName = InputFilePath & FileName & "_first_sb_preprocess"
                outfile = outfile_sb_first
                write_file(limit, OutFileName, outfile)
            End If

            If zug_casesSB > 0 Then
                OutFileName = InputFilePath & FileName & "_zug_sb_preprocess"
                outfile = outfile_sb_zug
                write_file(limit, OutFileName, outfile)
            End If


            AuditLog &= "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
            AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

            If first_Cases > 0 Then
                AuditLog &= "Number of First cases: " & first_Cases & vbCrLf
                AuditLog &= "Total balance of First cases: " & first_TotalBalance & vbCrLf
            End If

            If first_casesSB > 0 Then
                AuditLog &= "Number of First SB cases: " & first_casesSB & vbCrLf
                AuditLog &= "Total balance of First SB cases: " & first_TotalBalanceSB & vbCrLf
            End If

            If asset_Cases > 0 Then
                AuditLog &= "Number of Asset cases: " & asset_Cases & vbCrLf
                AuditLog &= "Total balance of Asset cases: " & asset_TotalBalance & vbCrLf
            End If

            If asset_casesSB > 0 Then
                AuditLog &= "Number of Asset SB cases: " & asset_casesSB & vbCrLf
                AuditLog &= "Total balance of Asset SB cases: " & asset_TotalBalanceSB & vbCrLf
            End If

            If zug_Cases > 0 Then
                AuditLog &= "Number of Zug cases: " & zug_Cases & vbCrLf
                AuditLog &= "Total balance of Zug cases: " & zug_TotalBalance & vbCrLf
            End If

            If zug_casesSB > 0 Then
                AuditLog &= "Number of Zug SB cases: " & zug_casesSB & vbCrLf
                AuditLog &= "Total balance of Zug SB cases: " & zug_TotalBalanceSB & vbCrLf
            End If

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)

            MsgBox("Pre-processing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
    Private Sub write_file(ByVal limit As Integer, ByVal filename As String, ByVal outfile As String)
        Dim LineNumber As Integer, FileCount As Integer = 1, RowCount As Integer = 0
        Dim OutputFile As String = ""
        Dim FileContents() As String = outfile.Split(vbNewLine)

        If UBound(FileContents) = 0 Then
            Return
        End If
        For Each InputLine As String In FileContents
            OutputFile &= InputLine
            LineNumber += 1
            RowCount += 1

            If RowCount > limit Then ' Not equal so as not to include the header

                WriteFile(filename & FileCount.ToString & ".txt", OutputFile)
                FileCount += 1

                OutputFile = FileContents(0)  ' Add header again
                RowCount = 1
            End If

        Next InputLine

        WriteFile(filename & FileCount.ToString & ".txt", OutputFile)

    End Sub
    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed_wrp.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

Module Module1
    Public Const PARAMETER_FIELD_NAME1 As String = "start_date"
    Public Const PARAMETER_FIELD_NAME2 As String = "clientschemeID"
    Public Const PARAMETER_FIELD_NAME3 As String = "end_date"
    Public Sub SetCurrentValuesForParameterField1(ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myArrayList As ArrayList)
        Dim currentParameterValues As CrystalDecisions.Shared.ParameterValues = New CrystalDecisions.Shared.ParameterValues()
        For Each submittedValue As Object In myArrayList
            Dim myParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue()
            myParameterDiscreteValue.Value = submittedValue.ToString()
            currentParameterValues.Add(myParameterDiscreteValue)
        Next
        Dim myParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        Dim myParameterFieldDefinition As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME1)
        myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)
    End Sub
    Public Sub SetCurrentValuesForParameterField2(ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myArrayList As ArrayList)
        Dim currentParameterValues As CrystalDecisions.Shared.ParameterValues = New CrystalDecisions.Shared.ParameterValues()
        For Each submittedValue As Object In myArrayList
            Dim myParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue()
            myParameterDiscreteValue.Value = submittedValue.ToString()
            currentParameterValues.Add(myParameterDiscreteValue)
        Next
        Dim myParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        Dim myParameterFieldDefinition As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME2)
        myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)
    End Sub
    Public Sub SetCurrentValuesForParameterField3(ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myArrayList As ArrayList)
        Dim currentParameterValues As CrystalDecisions.Shared.ParameterValues = New CrystalDecisions.Shared.ParameterValues()
        For Each submittedValue As Object In myArrayList
            Dim myParameterDiscreteValue As CrystalDecisions.Shared.ParameterDiscreteValue = New CrystalDecisions.Shared.ParameterDiscreteValue()
            myParameterDiscreteValue.Value = submittedValue.ToString()
            currentParameterValues.Add(myParameterDiscreteValue)
        Next
        Dim myParameterFieldDefinitions As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinitions = myReportDocument.DataDefinition.ParameterFields
        Dim myParameterFieldDefinition As CrystalDecisions.CrystalReports.Engine.ParameterFieldDefinition = myParameterFieldDefinitions(PARAMETER_FIELD_NAME3)
        myParameterFieldDefinition.ApplyCurrentValues(currentParameterValues)
    End Sub
    Public Sub SetDBLogonForReport(ByVal myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo, ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument, ByVal myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
            If InStr(myTable.Name.ToString, "FeePayment") > 0 Then
                myTableLogonInfo.ConnectionInfo = myConnectionInfo2
            Else
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
            End If
            myTable.ApplyLogOnInfo(myTableLogonInfo)
        Next
    End Sub
    Public Function InputFromExcel(ByVal FileName As String, Optional ByVal WorkSheetName As String = "") As String(,)
        Dim ExcelApp As Object, Workbook As Object, Worksheet As Object
        Dim ColumnIndex As Integer, RowIndex As Integer
        Dim Sheet As String(,) = Nothing

        ExcelApp = CreateObject("Excel.Application")
        Workbook = ExcelApp.Workbooks.Open(FileName)
        If WorkSheetName = "" Then
            Worksheet = Workbook.Sheets(1)
        Else
            Worksheet = Workbook.Sheets(WorkSheetName)
        End If

        Try

            ReDim Sheet(Worksheet.UsedRange.Rows.Count - 1, Worksheet.UsedRange.Columns.Count - 1)

            For ColumnIndex = 1 To Worksheet.UsedRange.Columns.Count
                For RowIndex = 1 To Worksheet.UsedRange.Rows.Count
                    Application.DoEvents()
                    Sheet(RowIndex - 1, ColumnIndex - 1) = Worksheet.Cells(RowIndex, ColumnIndex).Value
                Next RowIndex
            Next ColumnIndex

        Catch ex As Exception
            MsgBox(ex.ToString)

        Finally
            CloseComObject(ExcelApp.ActiveWorkbook)
            ExcelApp.ActiveWorkbook.Close()
            ExcelApp.Quit()
            CloseComObject(Worksheet)
            Worksheet = Nothing
            CloseComObject(Workbook)
            Workbook = Nothing
            CloseComObject(ExcelApp)
            ExcelApp = Nothing

        End Try

        Return Sheet

    End Function
    Public Sub CloseComObject(ByVal o As Object)
        Try
            While (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0)
            End While
        Catch
        Finally
            o = Nothing
        End Try

    End Sub
End Module

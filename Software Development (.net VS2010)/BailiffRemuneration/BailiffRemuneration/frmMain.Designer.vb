﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.dtpCalcMonth = New System.Windows.Forms.DateTimePicker()
        Me.prgMain = New System.Windows.Forms.ProgressBar()
        Me.rtxtAudit = New System.Windows.Forms.RichTextBox()
        Me.lblProgress = New System.Windows.Forms.Label()
        Me.btnGenerateInvoices = New System.Windows.Forms.Button()
        Me.btnImportTriggers = New System.Windows.Forms.Button()
        Me.btnSage = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(12, 52)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(112, 30)
        Me.btnCalculate.TabIndex = 0
        Me.btnCalculate.Text = "Calculate"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'dtpCalcMonth
        '
        Me.dtpCalcMonth.CustomFormat = "MMMM yyyy"
        Me.dtpCalcMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpCalcMonth.Location = New System.Drawing.Point(12, 12)
        Me.dtpCalcMonth.Name = "dtpCalcMonth"
        Me.dtpCalcMonth.Size = New System.Drawing.Size(112, 20)
        Me.dtpCalcMonth.TabIndex = 11
        '
        'prgMain
        '
        Me.prgMain.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.prgMain.Location = New System.Drawing.Point(7, 414)
        Me.prgMain.Name = "prgMain"
        Me.prgMain.Size = New System.Drawing.Size(661, 28)
        Me.prgMain.TabIndex = 12
        '
        'rtxtAudit
        '
        Me.rtxtAudit.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtxtAudit.HideSelection = False
        Me.rtxtAudit.Location = New System.Drawing.Point(141, 12)
        Me.rtxtAudit.Name = "rtxtAudit"
        Me.rtxtAudit.Size = New System.Drawing.Size(527, 384)
        Me.rtxtAudit.TabIndex = 13
        Me.rtxtAudit.Text = ""
        '
        'lblProgress
        '
        Me.lblProgress.BackColor = System.Drawing.Color.White
        Me.lblProgress.Location = New System.Drawing.Point(17, 420)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(640, 16)
        Me.lblProgress.TabIndex = 14
        Me.lblProgress.Text = "Label1"
        Me.lblProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblProgress.Visible = False
        '
        'btnGenerateInvoices
        '
        Me.btnGenerateInvoices.Location = New System.Drawing.Point(12, 111)
        Me.btnGenerateInvoices.Name = "btnGenerateInvoices"
        Me.btnGenerateInvoices.Size = New System.Drawing.Size(112, 30)
        Me.btnGenerateInvoices.TabIndex = 15
        Me.btnGenerateInvoices.Text = "Generate invoices"
        Me.btnGenerateInvoices.UseVisualStyleBackColor = True
        '
        'btnImportTriggers
        '
        Me.btnImportTriggers.Location = New System.Drawing.Point(12, 366)
        Me.btnImportTriggers.Name = "btnImportTriggers"
        Me.btnImportTriggers.Size = New System.Drawing.Size(112, 30)
        Me.btnImportTriggers.TabIndex = 16
        Me.btnImportTriggers.Text = "Import triggers"
        Me.btnImportTriggers.UseVisualStyleBackColor = True
        '
        'btnSage
        '
        Me.btnSage.Location = New System.Drawing.Point(12, 168)
        Me.btnSage.Name = "btnSage"
        Me.btnSage.Size = New System.Drawing.Size(112, 30)
        Me.btnSage.TabIndex = 17
        Me.btnSage.Text = "Generate Sage"
        Me.btnSage.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(680, 456)
        Me.Controls.Add(Me.btnSage)
        Me.Controls.Add(Me.btnImportTriggers)
        Me.Controls.Add(Me.btnGenerateInvoices)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.rtxtAudit)
        Me.Controls.Add(Me.prgMain)
        Me.Controls.Add(Me.dtpCalcMonth)
        Me.Controls.Add(Me.btnCalculate)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bailiff remuneration"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCalculate As System.Windows.Forms.Button
    Friend WithEvents dtpCalcMonth As System.Windows.Forms.DateTimePicker
    Friend WithEvents prgMain As System.Windows.Forms.ProgressBar
    Friend WithEvents rtxtAudit As System.Windows.Forms.RichTextBox
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents btnGenerateInvoices As System.Windows.Forms.Button
    Friend WithEvents btnImportTriggers As System.Windows.Forms.Button
    Friend WithEvents btnSage As System.Windows.Forms.Button

End Class

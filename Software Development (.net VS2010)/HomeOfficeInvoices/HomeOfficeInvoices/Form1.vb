﻿Imports System.IO

Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        Dim testValue As Decimal
        Try
            testValue = ndir_value.Text
        Catch ex As Exception
            MsgBox("Invalid amount for Non-direct transaction value")
            Exit Sub
        End Try

        Try
            testValue = dir_value.Text
        Catch ex As Exception
            MsgBox("Invalid amount for Direct transaction value")
            Exit Sub
        End Try

        runbtn.Enabled = False
        exitbtn.Enabled = False
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD253EPreport = New RD253EP
        Dim myArrayList1 As ArrayList = New ArrayList()
        RD253EPreport.SetParameterValue("start_date", start_dtp.Value)
        RD253EPreport.SetParameterValue("end_date", end_dtp.Value)
        RD253EPreport.SetParameterValue("trans_name", ndir_filename.Text)
        RD253EPreport.SetParameterValue("trans_value", ndir_value.Text)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253EPreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RD253EP TDX Home Office non-direct invoice.pdf"
        End With

        If SaveFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("Reports not run")
            Me.Close()
            Exit Sub
        End If
        Dim pathname As String = Path.GetDirectoryName(SaveFileDialog1.FileName) & "\"
        RD253EPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, SaveFileDialog1.FileName)
        RD253EPreport.Close()

        Dim RD448DPreport = New RD448DP
        RD448DPreport.SetParameterValue("start_date", start_dtp.Value)
        RD448DPreport.SetParameterValue("end_date", end_dtp.Value)
        RD448DPreport.SetParameterValue("trans_name", dir_filename.Text)
        RD448DPreport.SetParameterValue("trans_value", dir_value.Text)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD448DPreport)
        Dim filename As String = pathname & "RD448DP TDX Home Office direct invoice.pdf"
        RD448DPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD448DPreport.Close()

        Dim RD448FPreport = New RD448FP
        'Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD448FPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD448FPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD448FPreport)
        filename = pathname & "RD448FP TDX Home office Fee invoice.pdf"
        RD448FPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD448FPreport.Close()
        MsgBox("Reports saved")
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        start_dtp.Value = DateAdd(DateInterval.Day, -4 - Weekday(Now), Now)
        end_dtp.Value = DateAdd(DateInterval.Day, -Weekday(Now) + 2, Now)
    End Sub
End Class

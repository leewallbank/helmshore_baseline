﻿Imports CommonLibrary
Public Class clients

    Private Sub clients_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        client_dg.Rows.Clear()
        Dim dt As New DataTable
        Dim searchName As String = Trim(mainfrm.client_tbox.Text) & "%"
        LoadDataTable("DebtRecovery", "SELECT _rowid, ref, name " & _
                                                "FROM client " & _
                                                "WHERE name like '" & searchName & "'" & _
                                               " order by name", dt, False)
        Dim row As DataRow
        For Each row In dt.Rows
            client_dg.Rows.Add(row(0), row(1), row(2))
        Next
    End Sub

    Private Sub client_dg_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles client_dg.DoubleClick
        Me.Close()
    End Sub

    Private Sub client_dg_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles client_dg.RowEnter
        clientNo = client_dg.Rows(e.RowIndex).Cells(0).Value
    End Sub
End Class
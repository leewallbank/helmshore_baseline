﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class clients
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.client_dg = New System.Windows.Forms.DataGridView()
        Me.client_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.client_ref = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.client_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.client_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'client_dg
        '
        Me.client_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.client_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.client_no, Me.client_ref, Me.client_name})
        Me.client_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.client_dg.Location = New System.Drawing.Point(0, 0)
        Me.client_dg.Name = "client_dg"
        Me.client_dg.Size = New System.Drawing.Size(587, 266)
        Me.client_dg.TabIndex = 0
        '
        'client_no
        '
        Me.client_no.HeaderText = "Client No"
        Me.client_no.Name = "client_no"
        Me.client_no.ReadOnly = True
        '
        'client_ref
        '
        Me.client_ref.HeaderText = "Client Ref"
        Me.client_ref.Name = "client_ref"
        Me.client_ref.ReadOnly = True
        '
        'client_name
        '
        Me.client_name.HeaderText = "Client name"
        Me.client_name.Name = "client_name"
        Me.client_name.ReadOnly = True
        Me.client_name.Width = 300
        '
        'clients
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(587, 266)
        Me.Controls.Add(Me.client_dg)
        Me.Name = "clients"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Double-click to select client"
        CType(Me.client_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents client_dg As System.Windows.Forms.DataGridView
    Friend WithEvents client_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents client_ref As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents client_name As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

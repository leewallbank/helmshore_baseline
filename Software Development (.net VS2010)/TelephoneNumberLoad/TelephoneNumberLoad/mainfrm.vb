﻿Imports CommonLibrary
Imports System.IO
Public Class mainfrm


    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub clntbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click
        clients.ShowDialog()
        Dim FileDialog As New OpenFileDialog
        readbtn.Enabled = False

        Dim outline As String = ""
        Dim outfile2 As String = ""
        Dim outfileNotes As String = ""
        Dim lastClientRef As String = ""
        Dim savedphoneNumber As String = ""
        Dim savedphoneNumber2 As String = ""
        Dim savedphoneNumber3 As String = ""
        Dim savedEmail As String = ""
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        Dim inputfilepath, filename, fileext As String
        inputfilepath = Path.GetDirectoryName(FileDialog.FileName)
        inputfilepath &= "\"
        filename = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        fileext = Path.GetExtension(FileDialog.FileName)
        Dim excel_file_contents(,) As String = InputFromExcel(FileDialog.FileName)

        Dim lastRow As Integer = UBound(excel_file_contents)
        Dim rowIDX As Integer
        Dim inputline As String = ""
        For rowIDX = 1 To lastRow
            Try
                ProgressBar1.Value = (rowIDX / lastRow) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            'client ref is in first column
            Dim clientREf As String = Trim(excel_file_contents(rowIDX, 0))
            If clientREf.Length < 2 Then
                Continue For
            End If

            'phone number in col 2
            Dim phoneNumber As String = Trim(excel_file_contents(rowIDX, 2))
            Dim email As String = ""

            If phoneNumber.Length < 4 Then
                phoneNumber = Nothing
            End If
            If InStr(phoneNumber, "@") > 0 Then
                email = phoneNumber
                phoneNumber = ""
            End If

            If phoneNumber = Nothing And email = Nothing Then
                Continue For
            End If

            If clientREf = lastClientRef Or lastClientRef = "" Then
                If phoneNumber <> Nothing Then
                    If savedphoneNumber = Nothing Then
                        savedphoneNumber = phoneNumber
                    ElseIf savedphoneNumber2 = Nothing Then
                        If phoneNumber <> savedphoneNumber Then savedphoneNumber2 = phoneNumber
                    Else
                        If phoneNumber <> savedphoneNumber And phoneNumber <> savedphoneNumber2 Then
                            savedphoneNumber3 = phoneNumber
                        End If
                    End If
                End If
                If email <> Nothing Then
                    If InStr(email, ",") > 0 Then
                        savedEmail = ""
                    Else
                        savedEmail = email
                    End If

                End If
                lastClientRef = clientREf
                Continue For
            End If
            'not same client ref so process last one
            Dim dt As New DataTable
            LoadDataTable("DebtRecovery", "SELECT _rowid, linkID, clientSchemeID " & _
                                                    "FROM debtor " & _
                                                    "WHERE client_ref = '" & lastClientRef & "'" & _
                                                    " and status_open_closed = 'O'" & _
                                                    " order by linkID", dt, False)
            Dim row2 As DataRow
            Dim lastLinkID2 As Integer = 0
            For Each row2 In dt.Rows
                Dim CSID As Integer = row2(2)
                Dim CaseclientNo As Integer
                Try
                    CaseclientNo = GetSQLResults("DebtRecovery", "select clientID from clientScheme where _rowid = " & CSID)
                Catch ex As Exception
                    MsgBox("Unable to read clientScheme for " & CSID)
                End Try
                If CaseclientNo <> clientNo Then
                    Continue For
                End If
                Dim debtorID As Integer = row2(0)
                Dim linkID As Integer
                Try
                    linkID = row2.Item(1)
                Catch ex As Exception
                    'linkID is blank
                    If savedphoneNumber <> "" Then
                        outline = debtorID & "," & savedphoneNumber & "," & savedphoneNumber2 & vbNewLine
                        save_outfile(outline)
                    End If
                    If savedphoneNumber3 <> "" Then
                        outfileNotes &= debtorID & "," & "3rd Phone Number:=" & savedphoneNumber3 & vbNewLine
                    End If
                    If savedEmail <> "" Then
                        outfile2 &= debtorID & "," & savedEmail & vbNewLine
                    End If
                    Continue For
                End Try
                If linkID = lastLinkID2 Then
                    Continue For
                End If
                lastLinkID2 = linkID
                'get all open cases for this linkID
                Dim dt2 As New DataTable
                LoadDataTable("DebtRecovery", "SELECT _rowid " & _
                                                        "FROM debtor " & _
                                                        " WHERE linkId = " & linkID & _
                                                        " AND status_open_closed = 'O'", dt2, False)
                Dim linkRow As DataRow
                For Each linkRow In dt2.Rows
                    Dim linkDebtorID As Integer = linkRow.Item(0)
                    If savedphoneNumber <> "" Then
                        outline = debtorID & "," & savedphoneNumber & "," & savedphoneNumber2 & vbNewLine
                        save_outfile(outline)
                    End If
                    If savedphoneNumber3 <> "" Then
                        outfileNotes &= debtorID & "," & "3rd Phone Number:=" & savedphoneNumber3 & vbNewLine
                    End If
                    If savedEmail <> "" Then
                        outfile2 &= debtorID & "," & savedEmail & vbNewLine
                    End If
                Next
            Next
            savedEmail = email
            savedphoneNumber = phoneNumber
            savedphoneNumber2 = ""
            savedphoneNumber3 = ""
            lastClientRef = clientREf
        Next
        'do last client ref
        Dim dt3 As New DataTable
        LoadDataTable("DebtRecovery", "SELECT _rowid, linkID, clientSchemeID " & _
                                                "FROM debtor " & _
                                                "WHERE client_ref = '" & lastClientRef & "'" & _
                                                " and status_open_closed = 'O'" & _
                                                " order by linkID", dt3, False)
        Dim row As DataRow
        Dim lastLinkID As Integer = 0
        For Each row In dt3.Rows
            Dim CSID As Integer = row(2)
            Dim clientID As Integer
            Try
                clientID = GetSQLResults("DebtRecovery", "select clientID from clientScheme where _rowid = " & CSID)
            Catch ex As Exception
                MsgBox("Unable to read clientScheme for " & CSID)
            End Try
            If clientID <> clientNo Then
                Continue For
            End If
            Dim debtorID As Integer = row(0)
            Dim linkID As Integer
            Try
                linkID = row.Item(1)
            Catch ex As Exception
                'linkID is blank
                If savedphoneNumber <> "" Then
                    outline = debtorID & "," & savedphoneNumber & "," & savedphoneNumber2 & vbNewLine
                    save_outfile(outline)
                End If
                If savedphoneNumber3 <> "" Then
                    outfileNotes &= debtorID & "," & "3rd Phone Number:=" & savedphoneNumber3 & vbNewLine
                End If
                If savedEmail <> "" Then
                    outfile2 &= debtorID & "," & savedEmail & vbNewLine
                End If
                Continue For
            End Try
            If linkID = lastLinkID Then
                Continue For
            End If
            lastLinkID = linkID
            'get all open cases for this linkID
            Dim dt4 As New DataTable
            LoadDataTable("DebtRecovery", "SELECT _rowid " & _
                                                    "FROM debtor " & _
                                                    " WHERE linkId = " & linkID & _
                                                    " AND status_open_closed = 'O'", dt4, False)
            Dim linkRow As DataRow
            For Each linkRow In dt4.Rows
                Dim linkDebtorID As Integer = linkRow.Item(0)
                If savedphoneNumber <> "" Then
                    outline = debtorID & "," & savedphoneNumber & "," & savedphoneNumber2 & vbNewLine
                    save_outfile(outline)
                End If
                If savedphoneNumber3 <> "" Then
                    outfileNotes &= debtorID & "," & "3rd Phone Number:=" & savedphoneNumber3 & vbNewLine
                End If
                If savedEmail <> "" Then
                    outfile2 &= debtorID & "," & savedEmail & vbNewLine
                End If
            Next
        Next
        WriteFile(inputfilepath & filename & "_loadtel.txt", outfile)
        WriteFile(inputfilepath & filename & "_loademail.txt", outfile2)
        WriteFile(inputfilepath & filename & "_loadnotes.txt", outfileNotes)
        MsgBox("files written")
        Me.Close()
    End Sub
    Private Sub save_outfile(ByVal outline As String)
        If outline <> lastOutline Then
            outfile &= outline
        End If
        lastOutline = outline
    End Sub
End Class

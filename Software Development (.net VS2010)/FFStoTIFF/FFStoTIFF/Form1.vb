﻿Imports System.Drawing.Printing
Imports System.IO
Imports CommonLibrary
Imports Microsoft.Office.Interop.Excel
Imports Microsoft.Office.Interop

Public Class Form1

    Dim objUDC As UDC.IUDC
    Dim itfPrinter As UDC.IUDCPrinter
    Dim defaultprinter As String
    Dim itfProfile As UDC.IProfile
    Dim default_debtor As Integer = 0
    Dim ProfilePath As String
    Dim adCmdText As ADODB.CommandTypeEnum

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub set_up_printer()
        'get default printer
        Dim printDoc As New PrintDocument
        If (printDoc.PrinterSettings.IsDefaultPrinter()) Then
            defaultprinter = printDoc.PrinterSettings.PrinterName
        End If

        ' Use Universal Document Converter API to change settings of converted document
        objUDC = New UDC.APIWrapper

        itfPrinter = objUDC.Printers("Universal Document Converter")
        itfProfile = itfPrinter.Profile

        ProfilePath = "\\ross-helm-fp001\Rossendales Shared\vb.net shortcuts\UDC SettingsJun2015.xml"

        'ProfilePath = "R:\vb.net shortcuts\PDF to TIFF Remittances.xml"

        itfProfile.Load(ProfilePath)

        itfProfile.OutputLocation.FolderPath = "\\ross-helm-fp001\Rossendales Shared\FFS to TIFF OUT\"


    End Sub
    Private Function set_default_printer(ByVal strPrinterName As String)
        Dim strOldPrinter As String = ""
        Dim WshNetwork As Object = Nothing
        Dim pd As New PrintDocument

        Try
            strOldPrinter = pd.PrinterSettings.PrinterName
            WshNetwork = Microsoft.VisualBasic.CreateObject("WScript.Network")
            WshNetwork.SetDefaultPrinter(strPrinterName)
            pd.PrinterSettings.PrinterName = strPrinterName
            If pd.PrinterSettings.IsValid Then
                Return True
            Else
                WshNetwork.SetDefaultPrinter(strOldPrinter)
                Return False
            End If
        Catch exptd As Exception
            WshNetwork.SetDefaultPrinter(strOldPrinter)
            Return False
        Finally
            WshNetwork = Nothing
            pd = Nothing
        End Try

    End Function

    Private Sub new_PrintXLSToTIFF(ByVal strFilePath As String)

        Try
            Dim MyProcess As New Process
            MyProcess.StartInfo.CreateNoWindow = True
            MyProcess.StartInfo.Verb = "print"

            MyProcess.StartInfo.FileName = strFilePath
            MyProcess.Start()
            MyProcess.WaitForExit(1000)
            MyProcess.CloseMainWindow()
            MyProcess.Close()
        Catch ex As Exception

        End Try



    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim inDirectoryName As String = "\\ross-helm-fp001\Rossendales Shared\FFS to TIFF IN\"
        Dim outDirectoryName As String = "\\ross-helm-fp001\Rossendales Shared\FFS to TIFF OUT\"
        Dim archiveDirectoryName As String = "\\ross-helm-fp001\Rossendales Shared\FFS to TIFF Archive\"
        set_up_printer()
        'set default printer to udc
        set_default_printer("Universal Document Converter")
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(outDirectoryName)
            If System.IO.Directory.Exists(outDirectoryName) = False Then
                di = System.IO.Directory.CreateDirectory(outDirectoryName)
            Else
                System.IO.Directory.Delete(outDirectoryName, True)
                di = System.IO.Directory.CreateDirectory(outDirectoryName)
            End If
        Catch ex As Exception
            MsgBox("Unable to create TIFF OUT folder")
            End
        End Try

        Dim no_of_files As Integer = 0
        Dim last_file_name As String = ""
        Dim new_filename As String
        Dim debtor As Integer = 0
        Dim pbar As Integer = 0
        For Each foundFile As String In My.Computer.FileSystem.GetFiles _
            (inDirectoryName, FileIO.SearchOption.SearchTopLevelOnly, "*.xlsx")

            'get debtor from file name

            Try
                debtor = Path.GetFileNameWithoutExtension(foundFile)
            Catch ex As Exception
                'MsgBox("Unable to get debtorID for file " & foundFile)
                default_debtor += 1
                debtor = default_debtor
            End Try
            'save in archive filder with timestamp
            Dim archiveFilename As String = Path.GetFileNameWithoutExtension(foundFile)
            archiveFilename &= "_" & Format(Now, "yyyy-MM-dd-HHmmss") & ".xls"
            archiveFilename = archiveDirectoryName & archiveFilename
            My.Computer.FileSystem.CopyFile(foundFile, archiveFilename, False)
            Dim xlApp As New Excel.Application
            Dim xlWorkBook As Excel.Workbook
            Dim xlWorkSheet As Excel.Worksheet = Nothing
            xlWorkBook = xlApp.Workbooks.Open(foundFile)
            Try
                xlWorkSheet = xlWorkBook.Sheets("FFS")
            Catch ex As Exception
                Continue For
            End Try

            new_filename = "\\ross-helm-fp001\Rossendales Shared\FFS to TIFF OUT\" & debtor & ".xls"
            xlWorkSheet.SaveAs(new_filename, 39)
            xlApp.Quit()
            no_of_files += 1
            new_PrintXLSToTIFF(new_filename)
            'delete foundfile
            My.Computer.FileSystem.DeleteFile(new_filename)
        Next

        'reset default printer
        set_default_printer(defaultprinter)

        'MsgBox("Completed")
        Me.Close()

    End Sub

    Private Sub start_remit_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

﻿Imports CommonLibrary
Imports System.IO

Public Class Form1
    Dim start_date As Date
    Dim end_date As Date
    Dim filename As String = ""
    Dim filepath As String = ""
    Private Sub runbtn_Click(sender As System.Object, e As System.EventArgs) Handles runbtn.Click
        Dim password As String
        password = InputBox("Enter password", "Enter Password")
        If password <> "SWEARDRUM" Then
            MsgBox("Invalid password")
            Me.Close()
            Exit Sub
        End If
        
        runbtn.Enabled = False
        Dim EAdt As New DataTable
        LoadDataTable("DebtRecovery", "SELECT _rowid, name_fore, name_sur from bailiff " & _
                                                "WHERE agent_type='B'" & _
                                                " and (typesub is null or typesub <> 'Employed')" & _
                                                " and branchID in (24)", EAdt, False)
        ProgressBar1.Maximum = EAdt.Rows.Count
        Dim rowCount As Integer
        For Each EARow In EAdt.Rows
            rowCount += 1
            ProgressBar1.Value = rowCount
            Application.DoEvents()
            'see if any payments last month
            Dim EAFees As Integer = GetSQLResults("SwiftRemuneration", "select count(InvoiceSeq) from EAInvoice" & _
                          " where BailiffID = " & EARow(0) & _
            " and StartPeriod = '" & Format(start_date, "yyyy-MM-dd") & "'" & _
            " and EndPeriod = '" & Format(end_date, "yyyy-MM-dd") & "'")
            If EAFees > 0 Then
                Dim EAName As String = Microsoft.VisualBasic.Left(EARow(1), 1) & " " & EARow(2) & " (" & EARow(0) & ")" & " "
                run_report(EARow(0), EAName)
            End If
        Next
        MsgBox("Reports Completed")
        Me.Close()
    End Sub
    Private Sub run_report(ByVal EAID As Integer, ByVal EAName As String)
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2138SPreport = New RA2138SP
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_date)
        SetCurrentValuesForParameterField1(RA2138SPreport, myArrayList1)
        myArrayList1.Add(end_date)
        SetCurrentValuesForParameterField2(RA2138SPreport, myArrayList1)
        myArrayList1.Add(EAID)
        SetCurrentValuesForParameterField3(RA2138SPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        myConnectionInfo2.ServerName = "SwiftRemuneration"
        myConnectionInfo2.DatabaseName = "SwiftRemuneration"
        myConnectionInfo2.UserID = "sa"
        myConnectionInfo2.Password = "sa"

        SetDBLogonForReport(myConnectionInfo, RA2138SPreport, myConnectionInfo2)
        filename = "RA2138SP " & EAName & Format(start_date, "dd MMM yy") & " EA Invoices.pdf"
        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "PDF files |*.pdf"
                .DefaultExt = ".pdf"
                .OverwritePrompt = True
                .FileName = filename
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                filepath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                filepath &= "\"
            Else
                MsgBox("Report not saved")
            End If
        Else
            filename = filepath & "RA2138SP " & EAName & Format(start_date, "dd MMM yy") & " EA Invoices.pdf"
        End If

        RA2138SPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RA2138SPreport.Close()
    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        end_date = DateAdd(DateInterval.Day, -Weekday(Now) + 1, Now)
        end_date = CDate(Format(end_date, "MMM yyyy dd") & " 00:00:00")
        start_date = DateAdd(DateInterval.Day, -6, end_date)
        start_date = CDate(Format(start_date, "MMM yyyy dd") & " 00:00:00")
        start_dtp.Value = start_date
        end_dtp.Value = end_date
    End Sub
End Class

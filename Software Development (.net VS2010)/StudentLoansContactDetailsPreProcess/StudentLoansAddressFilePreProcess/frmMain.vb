﻿Imports System.IO
Imports System.Collections
Imports CommonLibrary
Imports System.Globalization

Public Class frmMain
    Const Separator As String = "|"
    Private ContactData As New clsContactData
    Private InputFilePath As String, FileName As String, FileExt As String, ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String, AddressChangeFileName As String, PhoneNumberChangeFileName As String, ContactDetailChangeNotesFileName As String

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim LineNumber As Integer
        Dim AddressChange As String, PhoneNumberChange(2) As String, Debtor As Object(), ExistingPhoneNumber(2) As String, AddressChangeFile As String = "", PhoneNumberChangeFile As String = "", ContactDetailChangeNotesFile As String = ""
        Dim TotalAddressChangeCount As Integer, TotalPhoneNumberChangeCount As Integer

        Try
            FileDialog.Filter = "Excel spreadsheets|*.xls;*.xlsx"
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            ErrorLog = ""
            ExceptionLog = ""
            AuditLog = ""

            Cursor = Cursors.WaitCursor

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            lblReadingFile.Visible = True

            Dim FileContents(,) As String = InputFromExcel(FileDialog.FileName)
            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents) + 4 ' audit file and output files

            For LineNumber = 1 To UBound(FileContents)
                ProgressBar.Value = LineNumber

                Application.DoEvents() ' without this line, the button disappears until processing is complete
                AddressChange = ""
                Array.Clear(PhoneNumberChange, 0, PhoneNumberChange.Length)
                If IsNothing(FileContents(LineNumber, 0)) Then Continue For

                Debtor = ContactData.GetDebtorDetails(FileContents(LineNumber, 0).PadLeft(11, "0"))

                If Not IsNothing(Debtor) Then

                    AddressChange = ConcatFields({FileContents(LineNumber, 5), _
                                                  FileContents(LineNumber, 6), _
                                                  ConcatFields({FileContents(LineNumber, 7), FileContents(LineNumber, 8)}, " "), _
                                                  FileContents(LineNumber, 9), _
                                                  FileContents(LineNumber, 10), _
                                                  FileContents(LineNumber, 11), _
                                                  FileContents(LineNumber, 12)}, ",") ' Concat house number and street

                    If Not IsDBNull(Debtor(1)) Then ExistingPhoneNumber(0) = Debtor(1)
                    If Not IsDBNull(Debtor(2)) Then ExistingPhoneNumber(1) = Debtor(2)
                    If Not IsDBNull(Debtor(3)) Then ExistingPhoneNumber(2) = Debtor(3)

                    ' It would be easier to check against Debtor but there is a small risk that a phone number could match a case id
                    If Not IsNothing(FileContents(LineNumber, 13)) AndAlso Not ExistingPhoneNumber.RContains(FileContents(LineNumber, 13)) Then PhoneNumberChange(0) = FileContents(LineNumber, 13)
                    If Not IsNothing(FileContents(LineNumber, 14)) AndAlso Not ExistingPhoneNumber.RContains(FileContents(LineNumber, 14)) Then PhoneNumberChange(1) = FileContents(LineNumber, 14)
                    If Not IsNothing(FileContents(LineNumber, 15)) AndAlso Not ExistingPhoneNumber.RContains(FileContents(LineNumber, 15)) Then PhoneNumberChange(2) = FileContents(LineNumber, 15)

                    If AddressChange <> "" Then
                        AddressChangeFile &= Debtor(0) & Separator & AddressChange & vbCrLf
                        ContactDetailChangeNotesFile &= Debtor(0) & Separator & "New Address Provided By Client" & vbCrLf
                        TotalAddressChangeCount += 1
                    End If

                    If String.Join("", PhoneNumberChange).Length > 0 Then
                        PhoneNumberChangeFile &= Debtor(0) & Separator & Join(PhoneNumberChange, Separator) & vbCrLf
                        If Join(PhoneNumberChange, "").Length > 0 Then ContactDetailChangeNotesFile &= Debtor(0) & Separator & "New Telephone Number Provided By Client" & vbCrLf
                        TotalPhoneNumberChangeCount += 1
                    End If

                Else
                    ErrorLog &= "Cannot find unique DebtorID for client ref " & FileContents(LineNumber, 0) & " at line number " & (LineNumber + 1).ToString & vbCrLf
                End If

            Next LineNumber

            ProgressBar.Value += 1
            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf
            AuditLog = "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Contact detail changes: " & (TotalAddressChangeCount + TotalPhoneNumberChangeCount).ToString

            ProgressBar.Value += 1
            If AddressChangeFile <> "" Then
                AddressChangeFileName = InputFilePath & FileName & "_AddressChanges.txt"
                WriteFile(AddressChangeFileName, AddressChangeFile)
            End If

            ProgressBar.Value += 1
            If PhoneNumberChangeFile <> "" Then
                PhoneNumberChangeFileName = InputFilePath & FileName & "_PhoneNumberChanges.txt"
                WriteFile(PhoneNumberChangeFileName, PhoneNumberChangeFile)
                WriteFile(InputFilePath & FileName & "_ContactDetailsNotes.txt", ContactDetailChangeNotesFile)
            End If

            ProgressBar.Value += 1
            If ContactDetailChangeNotesFile <> "" Then
                ContactDetailChangeNotesFileName = InputFilePath & FileName & "_ContactDetailsNotes.txt"
                WriteFile(ContactDetailChangeNotesFileName, ContactDetailChangeNotesFile)
            End If

            WriteFile(InputFilePath & FileName & "_Audit.txt", LogHeader & AuditLog)
            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", LogHeader & ExceptionLog)

            Cursor = Cursors.Default

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If AuditLog <> "" And File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If ErrorLog <> "" And File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If ExceptionLog <> "" And File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        If File.Exists(AddressChangeFileName) Then System.Diagnostics.Process.Start(AddressChangeFileName)
        If File.Exists(PhoneNumberChangeFileName) Then System.Diagnostics.Process.Start(PhoneNumberChangeFileName)
        If File.Exists(ContactDetailChangeNotesFileName) Then System.Diagnostics.Process.Start(ContactDetailChangeNotesFileName)
    End Sub

End Class

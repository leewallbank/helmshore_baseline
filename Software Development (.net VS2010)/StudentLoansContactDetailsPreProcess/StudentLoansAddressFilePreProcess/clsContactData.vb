﻿Imports CommonLibrary

Public Class clsContactData
    Public Function GetDebtorDetails(ByVal ClientRef As String) As Object()

        Dim Sql As String
        Dim Debtor As New DataTable
        GetDebtorDetails = Nothing

        Try

            Sql = "SELECT d._rowID, d.add_phone, d.add_fax, d.empPhone " & _
                  "FROM debtor AS d " & _
                  "INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID " & _
                  "WHERE d.client_ref = '" & ClientRef & "' " & _
                  "  AND d.status_open_closed = 'O' " & _
                  "  AND cs.clientID = 1572"

            LoadDataTable("DebtRecovery", Sql, Debtor, False)

            If Debtor.Rows.Count = 1 Then GetDebtorDetails = Debtor.Rows(0).ItemArray ' check row count to catch duplicates or no match

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Function
End Class

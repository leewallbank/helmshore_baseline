﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization


'
'
' The executable for this is triplicated in R:\vb.net shortcuts as 
'      LowellInboundTransactionsPreProcess.exe
'      AgilisysInboundTransactionsPreProcess.exe
'      TDXInboundTransactionsPreProcess.exe
' These are all the same but exist to clarify work instructions

Public Class frmMain

    Private TDXInboundTransaction As New clsTDXInboundTransactionData
    Private InputFilePath As String, FileName As String, FileExt As String
    Private OutputFiles As New ArrayList()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        btnViewInputFile.Enabled = False
        btnViewLogFile.Enabled = False
        btnViewOutputFile.Enabled = False
    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim ErrorLog As String = "", AuditLog As String = "", CaseID As String, ClientRef As String
        Dim InputLineArray() As String, TransactionFile(1) As String, FileDetails As String(,) = {{"Commissionable", "|", ".txt"}, {"NonCommissionable", ",", ".csv"}}, InputHeader() As String = Nothing
        Dim TransactionSumm(1, 1) As Decimal, TotalTransactionValue As Decimal
        Dim FileNum As Integer, LineNumber As Integer, TotalTransactionCount As Integer

        btnViewInputFile.Enabled = False
        btnViewLogFile.Enabled = False
        btnViewOutputFile.Enabled = False

        Try
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then File.Delete(InputFilePath & FileName & "_Error.txt")

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf
            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents) + 4 ' log files and two output files

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                Application.DoEvents() ' without this line, the button disappears until processing is complete

                If LineNumber = UBound(FileContents) + 1 Then Continue For
                InputLineArray = InputLine.Split(",")

                If LineNumber = 1 Then
                    If IsNumeric(InputLineArray(3)) And IsNumeric(InputLineArray(4)) Then ' transaction total value and count
                        InputHeader = InputLineArray
                    Else
                        ErrorLog &= "Invalid file header" & vbCrLf
                    End If
                    Continue For
                End If

                ClientRef = InputLineArray(1)

                ' if client refs do not being with a character, they need to be left padded to 10 digits with 0. This does not apply to Lowell
                If Not Char.IsLetter(ClientRef, 1) And Not UCase(FileName).Contains("LOWELL") Then ClientRef = ClientRef.PadLeft(10, "0")
                CaseID = TDXInboundTransaction.GetCaseID(ClientRef)

                If CaseID <> "" Then

                    If InputLineArray(9) = "PAY" Then FileNum = 0 Else FileNum = 1

                    InputLineArray(7) = InputLineArray(7) * -1 ' the values supplied in the file are negatives and need to be made positive

                    TransactionSumm(FileNum, 0) += 1
                    TransactionSumm(FileNum, 1) += InputLineArray(7)
                    TransactionFile(FileNum) &= CaseID & FileDetails(FileNum, 1) & InputLineArray(7) & FileDetails(FileNum, 1) & InputLineArray(1) & vbCrLf
                Else
                    ErrorLog &= "Case ID not found for client ref " & ClientRef & " at line number " & LineNumber.ToString & vbCrLf
                End If
            Next InputLine

            AuditLog = LogHeader

            For NewDebtFileCount As Integer = 0 To UBound(TransactionFile)

                ProgressBar.Value += 1

                If TransactionFile(NewDebtFileCount) <> "" Then
                    WriteFile(InputFilePath & FileDetails(NewDebtFileCount, 0) & FileName & FileDetails(NewDebtFileCount, 2), TransactionFile(NewDebtFileCount))
                    OutputFiles.Add(InputFilePath & FileDetails(NewDebtFileCount, 0) & FileName & ".txt")

                    AuditLog &= FileDetails(NewDebtFileCount, 0) & vbCrLf
                    AuditLog &= "Number of transactions: " & TransactionSumm(NewDebtFileCount, 0).ToString & vbCrLf
                    AuditLog &= "Total balance of transactions: " & TransactionSumm(NewDebtFileCount, 1).ToString & vbCrLf & vbCrLf
                    TotalTransactionCount += TransactionSumm(NewDebtFileCount, 0)
                    TotalTransactionValue += TransactionSumm(NewDebtFileCount, 1)
                End If

            Next NewDebtFileCount

            If Not IsNothing(InputHeader) Then
                If TotalTransactionValue * -1 <> InputHeader(3) Then ErrorLog &= "Header states total transaction value of " & InputHeader(3).ToString & ". " & (TotalTransactionValue * -1).ToString & " found in file" & vbCrLf
                If TotalTransactionCount <> InputHeader(4) Then ErrorLog &= "Header states total transaction count of " & InputHeader(4).ToString & ". " & TotalTransactionCount.ToString & " found in file" & vbCrLf
            End If

            ProgressBar.Value += 1

            AuditLog &= "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            ProgressBar.Value = ProgressBar.Maximum

            If ErrorLog <> "" Then
                ErrorLog = LogHeader & ErrorLog
                WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    'Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
    '    Using Writer As StreamWriter = New StreamWriter(FileName)
    '        Writer.Write(Content)
    '    End Using
    'End Sub

    Private Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, Optional ByVal NoteLength As Integer = 250) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 2) ' + 2 is for :;
        Dim PreviousLength As Integer
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = (NoteTag & ":" & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)) & ";").PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try
            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try
            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try
            For Each Filename As String In OutputFiles
                If File.Exists(Filename) Then System.Diagnostics.Process.Start("notepad.exe", Filename)
            Next Filename

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
End Class

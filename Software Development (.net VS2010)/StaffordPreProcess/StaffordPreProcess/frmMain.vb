﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String
    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim OutputFile As String = ""
            Dim totValue As Decimal = 0
            Dim totCases As Integer = 0
            Dim InputLineArray() As String
            Dim inputLine As String
            Dim auditFile As String = "Match No,Client Ref,Balance" & vbNewLine
            Dim matchNo As Integer = 0
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents)
            Dim rowMax As Integer = UBound(FileContents)
            Dim matchBalance As Decimal = 0
            Dim notes As String = ""
            For rowIDX = 0 To rowMax
                inputLine = FileContents(rowIDX)
                InputLineArray = Split(inputLine, "|", Chr(34))
                Dim clientRef As String = InputLineArray(0)
                Dim LODate As Date = InputLineArray(23)
                Dim balance As Decimal = InputLineArray(1)
                'see if next line has same client ref and LO date
                If rowIDX = rowMax Then
                    'no next line so write out this line
                    If matchBalance > 0 Then
                        OutputFile &= clientRef & "|" & matchBalance + balance & "|"
                        For colidx = 2 To 23
                            OutputFile &= InputLineArray(colidx) & "|"
                        Next
                        OutputFile &= notes & vbNewLine
                        matchNo += 1
                        auditFile &= matchNo & "," & clientRef & "," & balance & vbNewLine
                    Else 'no change
                        For colIDX = 0 To 23
                            OutputFile &= InputLineArray(colIDX) & "|"
                        Next
                        OutputFile &= vbNewLine
                    End If
                    'add balance to total
                    totValue += balance
                    totCases += 1
                    Exit For
                End If
                Dim nextInputLine = FileContents(rowIDX + 1)
                Dim nextInputLineArray = Split(nextInputLine, "|", Chr(34))
                Dim nextclientRef As String = nextInputLineArray(0)
                Dim nextLODate As Date = nextInputLineArray(23)

                If clientRef = nextclientRef And LODate = nextLODate Then
                    'accumulate balance and notes
                    'match found so accumulate lines
                    If matchBalance = 0 Then
                        matchNo += 1
                    End If
                    matchBalance += balance
                    If notes = "" Then
                        notes &= "Other Year period:"
                    End If
                    notes &= nextInputLineArray(22) & ";"
                    totCases += 1
                    totValue += balance
                    auditFile &= matchNo & "," & clientRef & "," & balance & vbNewLine
                Else
                    'no match with next one so write out current
                    If matchBalance > 0 Then
                        OutputFile &= clientRef & "|" & matchBalance + balance & "|"
                        For colidx = 2 To 23
                            OutputFile &= InputLineArray(colidx) & "|"
                        Next
                        notes &= nextInputLineArray(22) & ";"
                        OutputFile &= notes & vbNewLine
                        auditFile &= matchNo & "," & clientRef & "," & balance & vbNewLine
                    Else 'no change
                        For colIDX = 0 To 23
                            OutputFile &= InputLineArray(colIDX) & "|"
                        Next
                        OutputFile &= vbNewLine
                    End If
                    'add balance to total

                    totValue += balance
                    totCases += 1
                    matchBalance = 0
                    notes = ""
                End If
            Next
            btnViewInputFile.Enabled = True
            If totCases > 0 Then
                WriteFile(InputFilePath & FileName & "_preprocess.txt", OutputFile)
                btnViewOutputFile.Enabled = True
                WriteFile(InputFilePath & FileName & "_audit.csv", auditFile)
            End If


            MsgBox("Cases = " & totCases & " Value " & totValue & vbNewLine)
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_preprocess.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_preprocess.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & "_nonWRP_4200.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_nonWRP_4200.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

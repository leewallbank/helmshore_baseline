Public Class mainform
    Dim outline As String = ""

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Excel files | *.xls;*.xlsx"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                load_vals(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then

        Else
            MsgBox("File not opened")
            Exit Sub
        End If
        'do manipulation here
        openbtn.Enabled = False
        exitbtn.Enabled = False
        Dim idx As Integer
        ProgressBar1.Value = 5
        For idx = 2 To finalrow
            ProgressBar1.Value = (idx / finalrow) * 100
            Dim debtorID As Decimal
            Try
                debtorID = vals(idx, 2)
            Catch ex As Exception
                Continue For
            End Try
            If debtorID = 0 Then
                Continue For
            End If
            Dim disp_capital As Decimal = vals(idx, 67)  'BO
            Dim disp_equity As Decimal = vals(idx, 68)   'BP
            Dim disp_tot As Decimal = vals(idx, 69)      'BQ
            Dim param2 = "select _rowid, clientschemeID from Debtor where _rowid = " & debtorID
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to find debtorID = " & debtorID & " on onestep")
                Me.Close()
                Exit Sub
            End If

            Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(1)
            param2 = "select _rowid from ClientScheme where _rowid = " & csid & _
            " and clientID = 909"
            Dim csid_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("debtorID = " & debtorID & " is not in the LSC scheme on onestep")
                Me.Close()
                Exit Sub
            End If

           
            outfile = outfile & debtorID & "|Verified Total Capital:" & Format(disp_capital, "f") & _
               ";from spreadsheet on " & Format(Now, "dd/MM/yyyy") & vbNewLine
            outfile = outfile & debtorID & "|Verified Total Equity:" & Format(disp_equity, "f") & _
                ";from spreadsheet on " & Format(Now, "dd/MM/yyyy") & vbNewLine
            outfile = outfile & debtorID & "|Total Verified Capital and Equity less 30000:" & Format(disp_tot, "f") & _
               ";from spreadsheet on " & Format(Now, "dd/MM/yyyy") & vbNewLine
        Next
        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_note.txt", outfile, False)
        MsgBox("Note load file written")
        Me.Close()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        
    End Sub

   

End Class

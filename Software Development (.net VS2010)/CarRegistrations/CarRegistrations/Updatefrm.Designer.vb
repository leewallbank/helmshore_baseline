﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Updatefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.foretbox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.surtbox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.phonetbox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.deptcbox = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.regtbox = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.colourcbox = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.modelcbox = New System.Windows.Forms.ComboBox()
        Me.delbtn = New System.Windows.Forms.Button()
        Me.delcarbtn = New System.Windows.Forms.Button()
        Me.updbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.anotherbtn = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'foretbox
        '
        Me.foretbox.Location = New System.Drawing.Point(37, 65)
        Me.foretbox.Name = "foretbox"
        Me.foretbox.Size = New System.Drawing.Size(100, 20)
        Me.foretbox.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(69, 49)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Forenames"
        '
        'surtbox
        '
        Me.surtbox.Location = New System.Drawing.Point(185, 65)
        Me.surtbox.Name = "surtbox"
        Me.surtbox.Size = New System.Drawing.Size(100, 20)
        Me.surtbox.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(201, 49)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Surname"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(462, 49)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Phone"
        '
        'phonetbox
        '
        Me.phonetbox.Location = New System.Drawing.Point(446, 65)
        Me.phonetbox.Name = "phonetbox"
        Me.phonetbox.Size = New System.Drawing.Size(100, 20)
        Me.phonetbox.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(320, 49)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(30, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Dept"
        '
        'deptcbox
        '
        Me.deptcbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.deptcbox.FormattingEnabled = True
        Me.deptcbox.Items.AddRange(New Object() {"????", "Accounts", "Allocations", "Audit", "Business Support", "Client Services", "Collect Admin", "Collect Support", "Complaints", "Contact Centre", "Director", "EA", "HR", "IT", "LAA", "Operations", "Performance", "Visitor", "Welfare"})
        Me.deptcbox.Location = New System.Drawing.Point(309, 65)
        Me.deptcbox.Name = "deptcbox"
        Me.deptcbox.Size = New System.Drawing.Size(121, 21)
        Me.deptcbox.Sorted = True
        Me.deptcbox.TabIndex = 2
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(69, 147)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 13)
        Me.Label5.TabIndex = 10
        Me.Label5.Text = "Reg No"
        '
        'regtbox
        '
        Me.regtbox.Location = New System.Drawing.Point(37, 163)
        Me.regtbox.Name = "regtbox"
        Me.regtbox.Size = New System.Drawing.Size(100, 20)
        Me.regtbox.TabIndex = 4
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(355, 148)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(37, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Colour"
        '
        'colourcbox
        '
        Me.colourcbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.colourcbox.FormattingEnabled = True
        Me.colourcbox.Items.AddRange(New Object() {"????", "Black", "Blue", "Green", "Grey", "Orange", "Other", "Pink", "Purple", "Red", "Silver", "White", "Yellow"})
        Me.colourcbox.Location = New System.Drawing.Point(311, 164)
        Me.colourcbox.Name = "colourcbox"
        Me.colourcbox.Size = New System.Drawing.Size(121, 21)
        Me.colourcbox.Sorted = True
        Me.colourcbox.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(201, 147)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(68, 13)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Make/Model"
        '
        'modelcbox
        '
        Me.modelcbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.modelcbox.FormattingEnabled = True
        Me.modelcbox.Items.AddRange(New Object() {"????", "Audi", "BMW", "Citroen", "Citroen Saxo", "Fiat", "Fiat Punto", "Ford", "Ford", "Ford CMAX", "Ford Fiesta", "Ford Focus", "Ford Focus", "Ford Ka", "Ford Mondeo", "Honda", "Honda Civic", "Hyundai", "Jaguar", "Mercedes", "Mini", "Mini Cooper", "Nissan", "Peugeot", "Proton", "Renault", "Renault Clio", "Renault Magane", "Skoda", "Toyota", "Vauxhall", "Vauxhall Adam", "Vauxhall Astra", "Vauxhall Corsa", "Vauxhall Zafira", "VW", "VW Beetle", "VW Golf", "VW passat"})
        Me.modelcbox.Location = New System.Drawing.Point(171, 163)
        Me.modelcbox.Name = "modelcbox"
        Me.modelcbox.Size = New System.Drawing.Size(121, 21)
        Me.modelcbox.Sorted = True
        Me.modelcbox.TabIndex = 5
        '
        'delbtn
        '
        Me.delbtn.Location = New System.Drawing.Point(12, 341)
        Me.delbtn.Name = "delbtn"
        Me.delbtn.Size = New System.Drawing.Size(98, 23)
        Me.delbtn.TabIndex = 19
        Me.delbtn.Text = "Delete Person"
        Me.delbtn.UseVisualStyleBackColor = True
        Me.delbtn.Visible = False
        '
        'delcarbtn
        '
        Me.delcarbtn.Location = New System.Drawing.Point(15, 304)
        Me.delcarbtn.Name = "delcarbtn"
        Me.delcarbtn.Size = New System.Drawing.Size(98, 23)
        Me.delcarbtn.TabIndex = 7
        Me.delcarbtn.Text = "Delete Car Reg"
        Me.delcarbtn.UseVisualStyleBackColor = True
        '
        'updbtn
        '
        Me.updbtn.Location = New System.Drawing.Point(530, 304)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(98, 23)
        Me.updbtn.TabIndex = 10
        Me.updbtn.Text = "Update"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(332, 304)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(98, 34)
        Me.exitbtn.TabIndex = 9
        Me.exitbtn.Text = "Exit without changes"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'anotherbtn
        '
        Me.anotherbtn.Location = New System.Drawing.Point(175, 304)
        Me.anotherbtn.Name = "anotherbtn"
        Me.anotherbtn.Size = New System.Drawing.Size(75, 34)
        Me.anotherbtn.TabIndex = 8
        Me.anotherbtn.Text = "Add another car"
        Me.anotherbtn.UseVisualStyleBackColor = True
        '
        'Updatefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(661, 376)
        Me.Controls.Add(Me.anotherbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.updbtn)
        Me.Controls.Add(Me.delcarbtn)
        Me.Controls.Add(Me.delbtn)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.colourcbox)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.modelcbox)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.regtbox)
        Me.Controls.Add(Me.deptcbox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.phonetbox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.surtbox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.foretbox)
        Me.Name = "Updatefrm"
        Me.Text = "Update Car Regs"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents foretbox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents surtbox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents phonetbox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents deptcbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents regtbox As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents colourcbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents modelcbox As System.Windows.Forms.ComboBox
    Friend WithEvents delbtn As System.Windows.Forms.Button
    Friend WithEvents delcarbtn As System.Windows.Forms.Button
    Friend WithEvents updbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents anotherbtn As System.Windows.Forms.Button
End Class

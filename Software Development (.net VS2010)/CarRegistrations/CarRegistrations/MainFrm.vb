﻿Imports CommonLibrary
Public Class MainFrm

    Dim InsertEmpNo As Integer


    Private Sub MainFrm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim user As String = My.User.Name
        allowUpdate = False
        If InStr(user, "jblundell") + InStr(user, "jpovey") + InStr(user, "ecotgrave") > 0 Then
            allowUpdate = True
        End If
        savebtn.Enabled = allowUpdate
        refresh_crn()
        colour_search_cbox.Text = "All"
        make_searchcbox.Text = "All"
    End Sub

    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles clearbtn.Click
        foretbox.Text = ""
        surtbox.Text = ""
        deptcbox.Text = "????"
        phonetbox.Text = ""
        regtbox.Text = ""
        modelcbox.Text = "????"
        colourcbox.Text = "????"
    End Sub

    Private Sub savebtn_Click(sender As System.Object, e As System.EventArgs) Handles savebtn.Click
        'check if on table already
        Dim empExists As Boolean = False
        Dim empCheck As Object
        If foretbox.Enabled Then
            Dim forenames As String = Trim(foretbox.Text)
            Dim surname As String = Trim(surtbox.Text)
            empCheck = GetSQLResults("FeesSQL", "SELECT crn_emp_no " & _
                                                    "FROM CarRegName2 " & _
                                                    "WHERE crn_emp_forenames = '" & forenames & "'" & _
                                                   " And crn_emp_surname = '" & surname & "'")

            Try
                If empCheck > 0 Then
                    MsgBox("Name " & forenames & " " & surname & " already exists")
                    empExists = True
                End If
            Catch ex As Exception

            End Try
            If empExists = False Then
                InsertEmpNo = GetSQLResults("FeesSQL", "select max(crn_emp_no) from CarRegName2")
                InsertEmpNo += 1
                upd_txt = "insert into CarRegName2 (crn_emp_no, crn_emp_forenames, crn_emp_surname,crn_emp_dept, crn_emp_ext) " & _
                           " values(" & InsertEmpNo & ",'" & forenames & "','" & surname & "','" & deptcbox.Text & "','" & phonetbox.Text & "')"
                update_sql(upd_txt)
            End If
        End If
        If empExists = False Then
            'update car reg details
            Dim searchRegNo As String = Trim(regtbox.Text)
            searchRegNo = Replace(searchRegNo, " ", "")
            searchRegNo = Replace(searchRegNo, "0", "O")
            'check if car reg already exists
            empCheck = GetSQLResults("FeesSQL", "SELECT crn_emp_no " & _
                                               "FROM CarRegNo2 " & _
                                               "WHERE crn_reg_no = '" & searchRegNo & "'")
            If empCheck > 0 Then
                MsgBox("Car Reg " & regtbox.Text & " already exists")
            Else
                upd_txt = "insert into CarRegNo2 (crn_emp_no,crn_reg_no,crn_disp_reg_no,crn_model,crn_colour) " & _
               " values (" & InsertEmpNo & ",'" & searchRegNo & "','" & regtbox.Text & "','" & modelcbox.Text & "','" & colourcbox.Text & "')"
                update_sql(upd_txt)
                If MsgBox("Add another Car reg", MsgBoxStyle.YesNo, "Another Reg") = MsgBoxResult.No Then
                    foretbox.Enabled = True
                    surtbox.Enabled = True
                    deptcbox.Enabled = True
                    phonetbox.Enabled = True
                    foretbox.Text = ""
                    surtbox.Text = ""
                    deptcbox.Text = "????"
                    phonetbox.Text = ""
                    regtbox.Text = ""
                    modelcbox.Text = "????"
                    colourcbox.Text = "????"
                    refresh_crn()
                Else
                    foretbox.Enabled = False
                    surtbox.Enabled = False
                    deptcbox.Enabled = False
                    phonetbox.Enabled = False
                    regtbox.Text = ""
                    modelcbox.Text = "????"
                    colourcbox.Text = "????"
                End If
            End If
        End If

    End Sub

    Private Sub refresh_crn()
        crndg.Rows.Clear()
        deptcbox.Text = "????"
        modelcbox.Text = "????"
        colourcbox.Text = "????"
        foretbox.Enabled = True
        surtbox.Enabled = True
        deptcbox.Enabled = True
        phonetbox.Enabled = True
        crndg.Rows.Clear()
        Dim crndt As New DataTable
        LoadDataTable("FeesSQL", "select crn_emp_no, crn_disp_reg_no, crn_reg_no,crn_model,crn_colour from carRegNo2" &
                      " order by crn_reg_no", crndt, False)
        For Each crnRow In crndt.Rows
            Dim empNo As Integer = crnRow(0)
            Dim empArray As Object
            empArray = GetSQLResultsArray("FeesSQL", "select crn_emp_forenames, crn_emp_surname,crn_emp_dept, crn_emp_ext from carRegname2" &
                          " where crn_emp_no = " & empNo)
            Dim model As String = ""
            Try
                model = crnRow(3)
            Catch ex As Exception

            End Try
            Dim colour As String = ""
            Try
                colour = crnRow(4)
            Catch ex As Exception

            End Try
            crndg.Rows.Add(crnRow(0), crnRow(1), model, colour, empArray(0), empArray(1), empArray(2), empArray(3))
        Next

    End Sub

   

    Private Sub crndg_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles crndg.CellContentClick

    End Sub

    Private Sub crndg_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles crndg.CellDoubleClick
        Updatefrm.ShowDialog()
        refresh_crn()
    End Sub

    Private Sub crn_search_TextChanged(sender As System.Object, e As System.EventArgs) Handles crn_searchtbox.TextChanged

       
    End Sub

    Private Sub crndg_RowEnter(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles crndg.RowEnter
        updateEmpNo = crndg.Rows(e.RowIndex).Cells(0).Value
        Try
            updateRegNo = crndg.Rows(e.RowIndex).Cells(1).Value
        Catch ex As Exception
            updateRegNo = ""
        End Try

    End Sub

    Private Sub fore_searchtbox_TextChanged(sender As System.Object, e As System.EventArgs) Handles fore_searchtbox.TextChanged

    End Sub
    Private Sub refresh_search()
        crndg.Rows.Clear()
        Dim crn_search As String = crn_searchtbox.Text & "%"
        crn_search = Replace(crn_search, "0", "O")
        crn_search = Replace(crn_search, " ", "")
        Dim fore_search As String = fore_searchtbox.Text & "%"
        fore_search = Replace(fore_search, "0", "O")
        Dim sur_search As String = sur_searchtbox.Text & "%"
        sur_search = Replace(sur_search, "0", "O")

        Dim make_search As String = make_searchcbox.Text
        Dim colour_search As String = colour_search_cbox.Text
        Dim crndt As New DataTable
        Dim search_text As String = "select crn_emp_no,  crn_disp_reg_no, crn_model, crn_colour from carRegNo2" &
                      " where crn_reg_no like '" & crn_search & "'"
        If make_search <> "All" Then
            search_text &= " and crn_model like '" & make_search & "%'"
        End If
        If colour_search <> "All" Then
            search_text &= " and crn_colour = '" & colour_search & "'"
        End If

        search_text &= " order by crn_reg_no"

        LoadDataTable("FeesSQL", search_text, crndt, False)
        For Each crnRow In crndt.Rows
            Dim empNo As Integer = crnRow(0)
            Dim empArray As Object
            empArray = GetSQLResultsArray("FeesSQL", "select crn_emp_forenames, crn_emp_surname,crn_emp_dept,crn_emp_ext from carRegname2" &
                          " where crn_emp_no = " & empNo &
                          " and crn_emp_forenames like '" & fore_search & "'" &
                             " and crn_emp_surname like '" & sur_search & "'")
            Try
                Dim test As String = empArray(0)
            Catch ex As Exception
                Continue For
            End Try
            crndg.Rows.Add(crnRow(0), crnRow(1), crnRow(2), crnRow(3), empArray(0), empArray(1), empArray(2), empArray(3))
        Next
    End Sub

    Private Sub sur_searchtbox_TextChanged(sender As System.Object, e As System.EventArgs) Handles sur_searchtbox.TextChanged

    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs)
        
    End Sub

    Private Sub Button1_Click_1(sender As System.Object, e As System.EventArgs)
        'read spreadsheet
        Dim filename As String = "C:\AATemp\Car regs.xlsx"
        Dim excel_file_contents(,) As String = InputFromExcel(filename)
        Dim maxRow As Integer = UBound(excel_file_contents)
        For idx = 1 To maxRow
            Dim empNo As Integer = excel_file_contents(idx, 0)
            Dim forenames As String = excel_file_contents(idx, 5)
            Dim surname As String = excel_file_contents(idx, 4)
            Dim regNo As String = excel_file_contents(idx, 6)
            Dim searchregno As String = Trim(regNo)
            searchregno = Replace(searchregno, " ", "")
            searchregno = Replace(searchregno, "0", "O")
            Dim ext As String = excel_file_contents(idx, 1)
            'upd_txt = "insert into CarRegName2 (crn_emp_no,crn_emp_forenames,crn_emp_surname,crn_emp_dept,crn_emp_ext)" & _
            '    " values (" & empNo & ",'" & forenames & "','" & surname & "'," & "'EA'" & ",'" & ext & "')"
            'upd_txt = "delete from CarRegNo2 where crn_emp_no = " & empNo
            'update_sql(upd_txt)
            'upd_txt = "insert into CarRegNo2 (crn_emp_no,crn_reg_no, crn_disp_reg_no)" & _
            '    " values (" & empNo & ",'" & searchregno & "','" & regNo & "')"
            'update_sql(upd_txt)
            Me.Close()
        Next
    End Sub

    Private Sub searchbtn_Click(sender As System.Object, e As System.EventArgs) Handles searchbtn.Click
        refresh_search()
    End Sub

    Private Sub resetbtn_Click(sender As System.Object, e As System.EventArgs) Handles resetbtn.Click
        crn_searchtbox.Text = ""
        fore_searchtbox.Text = ""
        sur_searchtbox.Text = ""
        make_searchcbox.Text = "All"
        colour_search_cbox.Text = "All"
        refresh_search()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainFrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.crndg = New System.Windows.Forms.DataGridView()
        Me.emp_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.emp_reg_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.model = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colour = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.crn_emp_forenames = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.crn_emp_surname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.emp_dept = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.emp_phone = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.colourcbox = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.modelcbox = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.regtbox = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.phonetbox = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.deptcbox = New System.Windows.Forms.ComboBox()
        Me.savebtn = New System.Windows.Forms.Button()
        Me.clearbtn = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.foretbox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.surtbox = New System.Windows.Forms.TextBox()
        Me.crn_searchtbox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.fore_searchtbox = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.sur_searchtbox = New System.Windows.Forms.TextBox()
        Me.searchbtn = New System.Windows.Forms.Button()
        Me.resetbtn = New System.Windows.Forms.Button()
        Me.make_searchcbox = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.colour_search_cbox = New System.Windows.Forms.ComboBox()
        Me.TabControl1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        CType(Me.crndg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Location = New System.Drawing.Point(36, 84)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(808, 448)
        Me.TabControl1.TabIndex = 3
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.crndg)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(800, 422)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Car Reg No"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'crndg
        '
        Me.crndg.AllowUserToAddRows = False
        Me.crndg.AllowUserToDeleteRows = False
        Me.crndg.AllowUserToOrderColumns = True
        Me.crndg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.crndg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.emp_no, Me.emp_reg_no, Me.model, Me.colour, Me.crn_emp_forenames, Me.crn_emp_surname, Me.emp_dept, Me.emp_phone})
        Me.crndg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crndg.Location = New System.Drawing.Point(3, 3)
        Me.crndg.Name = "crndg"
        Me.crndg.ReadOnly = True
        Me.crndg.Size = New System.Drawing.Size(794, 416)
        Me.crndg.TabIndex = 0
        '
        'emp_no
        '
        Me.emp_no.HeaderText = "Emp No"
        Me.emp_no.Name = "emp_no"
        Me.emp_no.ReadOnly = True
        Me.emp_no.Visible = False
        '
        'emp_reg_no
        '
        Me.emp_reg_no.HeaderText = "Reg No"
        Me.emp_reg_no.Name = "emp_reg_no"
        Me.emp_reg_no.ReadOnly = True
        '
        'model
        '
        Me.model.HeaderText = "Make/Model"
        Me.model.Name = "model"
        Me.model.ReadOnly = True
        '
        'colour
        '
        Me.colour.HeaderText = "Colour"
        Me.colour.Name = "colour"
        Me.colour.ReadOnly = True
        '
        'crn_emp_forenames
        '
        Me.crn_emp_forenames.HeaderText = "Forenames"
        Me.crn_emp_forenames.Name = "crn_emp_forenames"
        Me.crn_emp_forenames.ReadOnly = True
        '
        'crn_emp_surname
        '
        Me.crn_emp_surname.HeaderText = "Surname"
        Me.crn_emp_surname.Name = "crn_emp_surname"
        Me.crn_emp_surname.ReadOnly = True
        '
        'emp_dept
        '
        Me.emp_dept.HeaderText = "Dept"
        Me.emp_dept.Name = "emp_dept"
        Me.emp_dept.ReadOnly = True
        '
        'emp_phone
        '
        Me.emp_phone.HeaderText = "Phone"
        Me.emp_phone.Name = "emp_phone"
        Me.emp_phone.ReadOnly = True
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.colourcbox)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.modelcbox)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.regtbox)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.phonetbox)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.deptcbox)
        Me.TabPage1.Controls.Add(Me.savebtn)
        Me.TabPage1.Controls.Add(Me.clearbtn)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.foretbox)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.surtbox)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(800, 422)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Add Name"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(392, 129)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(37, 13)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "Colour"
        '
        'colourcbox
        '
        Me.colourcbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.colourcbox.FormattingEnabled = True
        Me.colourcbox.Items.AddRange(New Object() {"????", "Black", "Blue", "Green", "Grey", "Orange", "Other", "Pink", "Purple", "Red", "Silver", "White", "Yellow"})
        Me.colourcbox.Location = New System.Drawing.Point(348, 145)
        Me.colourcbox.Name = "colourcbox"
        Me.colourcbox.Size = New System.Drawing.Size(121, 21)
        Me.colourcbox.Sorted = True
        Me.colourcbox.TabIndex = 6
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(238, 128)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(68, 13)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Make/Model"
        '
        'modelcbox
        '
        Me.modelcbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.modelcbox.FormattingEnabled = True
        Me.modelcbox.Items.AddRange(New Object() {"????", "Audi", "BMW", "Citroen", "Citroen Saxo", "Fiat", "Fiat Punto", "Ford", "Ford", "Ford CMAX", "Ford Fiesta", "Ford Focus", "Ford Focus", "Ford Ka", "Ford Mondeo", "Honda", "Honda Civic", "Hyundai", "Jaguar", "Mercedes", "Mini", "Mini Cooper", "Nissan", "Peugeot", "Proton", "Renault", "Renault Clio", "Renault Magane", "Skoda", "Toyota", "Vauxhall", "Vauxhall Adam", "Vauxhall Astra", "Vauxhall Corsa", "Vauxhall Zafira", "VW", "VW Beetle", "VW Golf", "VW passat"})
        Me.modelcbox.Location = New System.Drawing.Point(208, 144)
        Me.modelcbox.Name = "modelcbox"
        Me.modelcbox.Size = New System.Drawing.Size(121, 21)
        Me.modelcbox.Sorted = True
        Me.modelcbox.TabIndex = 5
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(86, 129)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(63, 13)
        Me.Label7.TabIndex = 10
        Me.Label7.Text = "Car Reg No"
        '
        'regtbox
        '
        Me.regtbox.Location = New System.Drawing.Point(67, 145)
        Me.regtbox.Name = "regtbox"
        Me.regtbox.Size = New System.Drawing.Size(100, 20)
        Me.regtbox.TabIndex = 4
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(552, 56)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(38, 13)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Phone"
        '
        'phonetbox
        '
        Me.phonetbox.Location = New System.Drawing.Point(517, 72)
        Me.phonetbox.Name = "phonetbox"
        Me.phonetbox.Size = New System.Drawing.Size(100, 20)
        Me.phonetbox.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(382, 54)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(30, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Dept"
        '
        'deptcbox
        '
        Me.deptcbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.deptcbox.FormattingEnabled = True
        Me.deptcbox.Items.AddRange(New Object() {"????", "Accounts", "Allocations", "Audit", "Business Support", "Client Services", "Collect Admin", "Collect Support", "Complaints", "Contact Centre", "Director", "HR", "IT", "LAA", "Operations", "Performance", "Visitor", "Welfare"})
        Me.deptcbox.Location = New System.Drawing.Point(348, 70)
        Me.deptcbox.Name = "deptcbox"
        Me.deptcbox.Size = New System.Drawing.Size(121, 21)
        Me.deptcbox.Sorted = True
        Me.deptcbox.TabIndex = 2
        '
        'savebtn
        '
        Me.savebtn.Location = New System.Drawing.Point(632, 283)
        Me.savebtn.Name = "savebtn"
        Me.savebtn.Size = New System.Drawing.Size(108, 23)
        Me.savebtn.TabIndex = 8
        Me.savebtn.Text = "Save "
        Me.savebtn.UseVisualStyleBackColor = True
        '
        'clearbtn
        '
        Me.clearbtn.Location = New System.Drawing.Point(67, 283)
        Me.clearbtn.Name = "clearbtn"
        Me.clearbtn.Size = New System.Drawing.Size(117, 23)
        Me.clearbtn.TabIndex = 7
        Me.clearbtn.Text = "Clear boxes"
        Me.clearbtn.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(238, 58)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "SurName"
        '
        'foretbox
        '
        Me.foretbox.Location = New System.Drawing.Point(67, 74)
        Me.foretbox.Name = "foretbox"
        Me.foretbox.Size = New System.Drawing.Size(100, 20)
        Me.foretbox.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(86, 58)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "ForeName"
        '
        'surtbox
        '
        Me.surtbox.Location = New System.Drawing.Point(217, 74)
        Me.surtbox.Name = "surtbox"
        Me.surtbox.Size = New System.Drawing.Size(100, 20)
        Me.surtbox.TabIndex = 1
        '
        'crn_searchtbox
        '
        Me.crn_searchtbox.Location = New System.Drawing.Point(107, 49)
        Me.crn_searchtbox.Name = "crn_searchtbox"
        Me.crn_searchtbox.Size = New System.Drawing.Size(83, 20)
        Me.crn_searchtbox.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(126, 30)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(46, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Car Reg"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(401, 30)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(59, 13)
        Me.Label10.TabIndex = 8
        Me.Label10.Text = "Forenames"
        '
        'fore_searchtbox
        '
        Me.fore_searchtbox.Location = New System.Drawing.Point(391, 50)
        Me.fore_searchtbox.Name = "fore_searchtbox"
        Me.fore_searchtbox.Size = New System.Drawing.Size(83, 20)
        Me.fore_searchtbox.TabIndex = 7
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(498, 30)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(49, 13)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Surname"
        '
        'sur_searchtbox
        '
        Me.sur_searchtbox.Location = New System.Drawing.Point(480, 50)
        Me.sur_searchtbox.Name = "sur_searchtbox"
        Me.sur_searchtbox.Size = New System.Drawing.Size(83, 20)
        Me.sur_searchtbox.TabIndex = 9
        '
        'searchbtn
        '
        Me.searchbtn.Location = New System.Drawing.Point(642, 44)
        Me.searchbtn.Name = "searchbtn"
        Me.searchbtn.Size = New System.Drawing.Size(75, 23)
        Me.searchbtn.TabIndex = 11
        Me.searchbtn.Text = "Search"
        Me.searchbtn.UseVisualStyleBackColor = True
        '
        'resetbtn
        '
        Me.resetbtn.Location = New System.Drawing.Point(11, 46)
        Me.resetbtn.Name = "resetbtn"
        Me.resetbtn.Size = New System.Drawing.Size(75, 23)
        Me.resetbtn.TabIndex = 15
        Me.resetbtn.Text = "Clear"
        Me.resetbtn.UseVisualStyleBackColor = True
        '
        'make_searchcbox
        '
        Me.make_searchcbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.make_searchcbox.FormattingEnabled = True
        Me.make_searchcbox.Items.AddRange(New Object() {"All", "Audi", "BMW", "Citroen", "Citroen Saxo", "Fiat", "Fiat Punto", "Ford", "Ford", "Ford CMAX", "Ford Fiesta", "Ford Focus", "Ford Focus", "Ford Ka", "Ford Mondeo", "Honda", "Honda Civic", "Hyundai", "Jaguar", "Mercedes", "Mini", "Mini Cooper", "Nissan", "Peugeot", "Proton", "Renault", "Renault Clio", "Renault Magane", "Skoda", "Toyota", "Vauxhall", "Vauxhall Corsa", "Vauxhall Adam", "Vauxhall Astra", "Vauxhall Zafira", "VW", "VW Beetle", "VW Golf", "VW passat"})
        Me.make_searchcbox.Location = New System.Drawing.Point(196, 49)
        Me.make_searchcbox.Name = "make_searchcbox"
        Me.make_searchcbox.Size = New System.Drawing.Size(95, 21)
        Me.make_searchcbox.TabIndex = 16
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(211, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Make/Model"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(323, 30)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(37, 13)
        Me.Label12.TabIndex = 18
        Me.Label12.Text = "Colour"
        '
        'colour_search_cbox
        '
        Me.colour_search_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.colour_search_cbox.FormattingEnabled = True
        Me.colour_search_cbox.Items.AddRange(New Object() {"All", "Black", "Blue", "Green", "Grey", "Orange", "Other", "Pink", "Purple", "Red", "Silver", "White", "Yellow"})
        Me.colour_search_cbox.Location = New System.Drawing.Point(297, 49)
        Me.colour_search_cbox.Name = "colour_search_cbox"
        Me.colour_search_cbox.Size = New System.Drawing.Size(88, 21)
        Me.colour_search_cbox.TabIndex = 19
        '
        'MainFrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1011, 544)
        Me.Controls.Add(Me.colour_search_cbox)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.make_searchcbox)
        Me.Controls.Add(Me.resetbtn)
        Me.Controls.Add(Me.searchbtn)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.sur_searchtbox)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.fore_searchtbox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.crn_searchtbox)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "MainFrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Car Registration Numbers"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        CType(Me.crndg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents crndg As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents surtbox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents foretbox As System.Windows.Forms.TextBox
    Friend WithEvents savebtn As System.Windows.Forms.Button
    Friend WithEvents clearbtn As System.Windows.Forms.Button
    Friend WithEvents crn_searchtbox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents deptcbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents phonetbox As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents colourcbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents modelcbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents regtbox As System.Windows.Forms.TextBox
    Friend WithEvents emp_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents emp_reg_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents model As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colour As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents crn_emp_forenames As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents crn_emp_surname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents emp_dept As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents emp_phone As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents fore_searchtbox As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents sur_searchtbox As System.Windows.Forms.TextBox
    Friend WithEvents searchbtn As System.Windows.Forms.Button
    Friend WithEvents resetbtn As System.Windows.Forms.Button
    Friend WithEvents make_searchcbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents colour_search_cbox As System.Windows.Forms.ComboBox
End Class

﻿Imports CommonLibrary
Public Class Updatefrm

    Private Sub Updatefrm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        updbtn.Enabled = allowUpdate
        anotherbtn.Enabled = allowUpdate
        delcarbtn.Enabled = allowUpdate

        Dim empArray As Object
        anotherCar = False
        empArray = GetSQLResultsArray("FeesSQL", "select crn_emp_forenames, crn_emp_surname, crn_emp_dept, crn_emp_ext from carRegname2" &
                      " where crn_emp_no = " & updateEmpNo)
        Try
            foretbox.Text = empArray(0)
        Catch ex As Exception
            foretbox.Text = ""
        End Try

        surtbox.Text = empArray(1)
        Try
            deptcbox.Text = empArray(2)
        Catch ex As Exception
            deptcbox.Text = "????"
        End Try
        Try
            phonetbox.Text = empArray(3)
        Catch ex As Exception
            phonetbox.Text = ""
        End Try
        Dim crnArray As Object
        If updateRegNo <> "" Then
            crnArray = GetSQLResultsArray("FeesSQL", "select crn_disp_reg_no, crn_model, crn_colour from carRegNo2" &
                          " where crn_disp_reg_no = '" & updateRegNo & "'")
            regtbox.Text = crnArray(0)
            Try
                modelcbox.Text = crnArray(1)
            Catch ex As Exception
                modelcbox.Text = ""
            End Try
            Try
                colourcbox.Text = crnArray(2)
            Catch ex As Exception
                colourcbox.Text = ""
            End Try

        Else
            regtbox.Text = ""
            modelcbox.Text = ""
            colourcbox.Text = ""
        End If

    End Sub

    Private Sub delbtn_Click(sender As System.Object, e As System.EventArgs) Handles delbtn.Click
        If MsgBox("Delete " & foretbox.Text & " " & surtbox.Text & " and any car regs?", MsgBoxStyle.YesNo, "Delete person and related car regs") = MsgBoxResult.Yes Then
            upd_txt = "delete from carRegNo2 where crn_emp_no = " & updateEmpNo
            update_sql(upd_txt)
            upd_txt = "delete from carRegName2 where crn_emp_no = " & updateEmpNo
            update_sql(upd_txt)
            Me.Close()
        End If
    End Sub

    Private Sub delcarbtn_Click(sender As System.Object, e As System.EventArgs) Handles delcarbtn.Click
        If MsgBox("Delete Car Reg No " & regtbox.Text, MsgBoxStyle.YesNo, "Delete Car Reg No") = MsgBoxResult.Yes Then
            upd_txt = "delete from carRegNo2 where crn_reg_no = '" & regtbox.Text & "'" & _
                " and crn_emp_no = " & updateEmpNo
            update_sql(upd_txt)
            Me.Close()
        End If
    End Sub

    Private Sub updbtn_Click(sender As System.Object, e As System.EventArgs) Handles updbtn.Click
        Dim searchRegNo As String = Trim(regtbox.Text)
        searchRegNo = Replace(searchRegNo, " ", "")
        searchRegNo = UCase(Replace(searchRegNo, "0", "O"))
        If anotherCar Then
            upd_txt = "insert into CarRegNo2 (crn_emp_no,crn_reg_no,crn_disp_reg_no,crn_model,crn_colour) " & _
                 " values (" & updateEmpNo & ",'" & searchRegNo & "','" & regtbox.Text & "','" & modelcbox.Text & "','" & colourcbox.Text & "')"
            update_sql(upd_txt)
        Else

            upd_txt = "update CarRegName2 set crn_emp_forenames = '" & foretbox.Text &
         "', crn_emp_surname = '" & surtbox.Text &
         "', crn_emp_dept = '" & deptcbox.Text &
         "',crn_emp_ext = '" & phonetbox.Text &
     "' where crn_emp_no = " & updateEmpNo
            update_sql(upd_txt)
            upd_txt = "update CarRegNo2 set crn_reg_no = '" & searchRegNo &
                "', crn_disp_reg_no = '" & UCase(regtbox.Text) &
               "', crn_model = '" & modelcbox.Text &
               "', crn_colour = '" & colourcbox.Text &
                "' where crn_emp_no = " & updateEmpNo
            update_sql(upd_txt)
        End If
        Me.Close()
    End Sub

    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub anotherbtn_Click(sender As System.Object, e As System.EventArgs) Handles anotherbtn.Click
        foretbox.Enabled = False
        surtbox.Enabled = False
        deptcbox.Enabled = False
        phonetbox.Enabled = False
        regtbox.Text = ""
        modelcbox.Text = "????"
        colourcbox.Text = "????"
        anotherbtn.Enabled = False
        anotherCar = True
    End Sub
End Class
﻿

Public Class frmMain

    Private Forms() As String = {"frm1", "frm2", "frm3"}
    Private FormNum As Integer = 0
    Private ChildLocation As New Point(0, 0)

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        For Each Control As Control In Me.Controls
            If TryCast(Control, MdiClient) IsNot Nothing Then Control.BackColor = Me.BackColor
        Next Control
        OpenChildForm(0)

    End Sub

    Private Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.Click
        OpenChildForm(-1)
    End Sub

    Private Sub btnNext_Click(sender As Object, e As System.EventArgs) Handles btnNext.Click
        OpenChildForm(1)
    End Sub

    Private Sub OpenChildForm(Navigation As Integer)
        For Each OpenChild As Form In Me.MdiChildren ' There should only be one
            OpenChild.Close()
        Next OpenChild

        FormNum += Navigation

        If FormNum <= 0 Then ' The two = signs mean the same If block can be used to enable buttons and to keep FormNum with the valid range. Enabled logic assumes that there will be more than two forms
            FormNum = 0
            btnBack.Enabled = False
            btnNext.Enabled = True
        ElseIf FormNum >= UBound(Forms) Then
            FormNum = UBound(Forms)
            btnBack.Enabled = True
            btnNext.Enabled = False
        Else
            btnBack.Enabled = True
            btnNext.Enabled = True
        End If

        Dim Child As Form = Activator.CreateInstance(Type.GetType(Application.ProductName & "." & Forms(FormNum), True, True))

        Child.MdiParent = Me
        Child.StartPosition = FormStartPosition.Manual
        Child.Location = ChildLocation
        Child.Size = ChildSize()
        Child.Show()

        lblStep.Text = "Step " & (FormNum + 1).ToString & " of " & (UBound(Forms) + 1).ToString
    End Sub

    Private Sub frmMain_SizeChanged(sender As Object, e As System.EventArgs) Handles Me.SizeChanged
        For Each Child As Form In Me.MdiChildren ' There should only be one
            Child.Size = ChildSize()
        Next Child
    End Sub

    Private Function ChildSize() As Size
        ChildSize = New Size(Me.Width - 12, Me.Height - 100)
    End Function
End Class

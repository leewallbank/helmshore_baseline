﻿Imports CommonLibrary

Public Class diaClientAllocationLevels

    Private SummaryData As New clsCaseSummaryData

    Public Sub New(ByRef ClientAllocationLevelDV As DataView)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        SetFormIcon(Me)

        dgvClientAllocationLevel.DataSource = ClientAllocationLevelDV

    End Sub

    Private Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        Try
            For Each dr As DataRowView In dgvClientAllocationLevel.DataSource
                If dr.Row.RowState = DataRowState.Modified Then SummaryData.SetClientAllocation(dr.Item("ClientName").ToString, dr.Item("AllocationDays").ToString, dr.Item("AllocationQuantity").ToString)
            Next dr

            RemoveHandler Me.FormClosing, AddressOf diaClientAllocationLevels_FormClosing

            Me.Close()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Try
            Dim HasChanges As Boolean = False

            For Each dr As DataRowView In dgvClientAllocationLevel.DataSource
                If dr.Row.RowState = DataRowState.Modified Then HasChanges = True
            Next dr

            If HasChanges AndAlso MsgBox("Discard changes?", vbYesNo + vbDefaultButton2, "Allocation Summary") = MsgBoxResult.Yes Then
                Me.Close()
            ElseIf Not HasChanges Then
                Me.Close()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvClientAllocationLevel_CellValidating(ByVal sender As Object, ByVal e As DataGridViewCellValidatingEventArgs) Handles dgvClientAllocationLevel.CellValidating
        Try
            If e.ColumnIndex > 0 AndAlso (Not IsNumeric(e.FormattedValue) OrElse IsDBNull(e.FormattedValue) OrElse e.FormattedValue = "") Then
                MsgBox("Please enter a valid number.", vbCritical + vbOKOnly)
                e.Cancel = True
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub diaClientAllocationLevels_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            Dim HasChanges As Boolean = False

            For Each dr As DataRowView In dgvClientAllocationLevel.DataSource
                If dr.Row.RowState = DataRowState.Modified Then HasChanges = True
            Next dr

            If HasChanges AndAlso MsgBox("Discard changes?", vbYesNo + vbDefaultButton2, "Allocation Summary") = MsgBoxResult.No Then
                e.Cancel = True
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class
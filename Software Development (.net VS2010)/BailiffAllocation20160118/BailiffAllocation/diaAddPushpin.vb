﻿Imports CommonLibrary

Public Class diaAddPushpin

    Public ReadOnly Property PushpinName
        Get
            Return txtName.Text
        End Get
    End Property

    Public ReadOnly Property PushpinLocation
        Get
            Return txtLocation.Text
        End Get
    End Property

    Private Sub btnOK_Click(sender As Object, e As System.EventArgs) Handles btnOK.Click
        Try
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs) Handles btnCancel.Click
        Try
            Me.Close()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class
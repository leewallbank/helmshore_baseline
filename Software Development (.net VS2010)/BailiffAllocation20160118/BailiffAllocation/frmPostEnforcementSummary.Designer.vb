﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPostEnforcementSummary
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPostEnforcementSummary))
        Me.cboCompany = New System.Windows.Forms.ComboBox()
        Me.PostcodeArea = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmsForm = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmsList = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.WorkType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblSummary = New System.Windows.Forms.Label()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.dgvLinkedPIF = New System.Windows.Forms.DataGridView()
        Me.LinkedPIF = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmsSummary = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmdArrangementBrokenStageClear = New System.Windows.Forms.Button()
        Me.dgvArrangementBrokenStage = New System.Windows.Forms.DataGridView()
        Me.ArrangementBrokenStage = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdArrangementBrokenStageAll = New System.Windows.Forms.Button()
        Me.cmdLinkedPIFClear = New System.Windows.Forms.Button()
        Me.RefreshCheckTimer = New System.Windows.Forms.Timer(Me.components)
        Me.cmdLinkedPIFAll = New System.Windows.Forms.Button()
        Me.lblPeriodType = New System.Windows.Forms.Label()
        Me.dgvResidencyScore = New System.Windows.Forms.DataGridView()
        Me.ResidencyScore = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cboPeriodType = New System.Windows.Forms.ComboBox()
        Me.dgvPostcodeArea = New System.Windows.Forms.DataGridView()
        Me.dgvWorkType = New System.Windows.Forms.DataGridView()
        Me.cmdRefreshDB = New System.Windows.Forms.Button()
        Me.cmdPostcodeAreaClear = New System.Windows.Forms.Button()
        Me.cmdResidencyScoreClear = New System.Windows.Forms.Button()
        Me.cmdPostcodeAreaAll = New System.Windows.Forms.Button()
        Me.cmdWorkTypeClear = New System.Windows.Forms.Button()
        Me.cmdResidencyScoreAll = New System.Windows.Forms.Button()
        Me.chkTopClients = New System.Windows.Forms.CheckBox()
        Me.ClientName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colStageNameTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdPaymentStageClear = New System.Windows.Forms.Button()
        Me.cmdPaymentStageAll = New System.Windows.Forms.Button()
        Me.colClientNameTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.pnlAbsPerc = New System.Windows.Forms.Panel()
        Me.radPerc = New System.Windows.Forms.RadioButton()
        Me.radAbs = New System.Windows.Forms.RadioButton()
        Me.lblLine1 = New System.Windows.Forms.Label()
        Me.cmdWorkTypeAll = New System.Windows.Forms.Button()
        Me.cmdClearAll = New System.Windows.Forms.Button()
        Me.cmdStageClear = New System.Windows.Forms.Button()
        Me.cmdStageAll = New System.Windows.Forms.Button()
        Me.cmdClientClear = New System.Windows.Forms.Button()
        Me.dgvAddConfirmed = New System.Windows.Forms.DataGridView()
        Me.AddConfirmed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdCGAClear = New System.Windows.Forms.Button()
        Me.cmdCGAAll = New System.Windows.Forms.Button()
        Me.cmdNumberOfVisitsAddressAll = New System.Windows.Forms.Button()
        Me.cmdClientAll = New System.Windows.Forms.Button()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.StageName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdNumberOfVisitsAddressClear = New System.Windows.Forms.Button()
        Me.dgvClientName = New System.Windows.Forms.DataGridView()
        Me.dgvStageName = New System.Windows.Forms.DataGridView()
        Me.cmdAddConfirmedClear = New System.Windows.Forms.Button()
        Me.cmdAddConfirmedAll = New System.Windows.Forms.Button()
        Me.dgvPaymentStage = New System.Windows.Forms.DataGridView()
        Me.PaymentStage = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvSummary = New System.Windows.Forms.DataGridView()
        Me.cmdPhoneNumberAll = New System.Windows.Forms.Button()
        Me.cmdPhoneNumberClear = New System.Windows.Forms.Button()
        Me.dgvNumberOfVisitsAddress = New System.Windows.Forms.DataGridView()
        Me.NumberOfVisitsAddress = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvCGA = New System.Windows.Forms.DataGridView()
        Me.dgvPhoneNumber = New System.Windows.Forms.DataGridView()
        Me.PhoneNumber = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvDebtYear = New System.Windows.Forms.DataGridView()
        Me.DebtYear = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdDebtYearClear = New System.Windows.Forms.Button()
        Me.cmdDebtYearAll = New System.Windows.Forms.Button()
        Me.cmdInYearClear = New System.Windows.Forms.Button()
        Me.dgvInYear = New System.Windows.Forms.DataGridView()
        Me.InYear = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdInYearAll = New System.Windows.Forms.Button()
        Me.dgvLinkedPayment = New System.Windows.Forms.DataGridView()
        Me.LinkedPayment = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn17 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdLinkedPaymentClear = New System.Windows.Forms.Button()
        Me.cmdLinkedPaymentAll = New System.Windows.Forms.Button()
        Me.pnlBalance = New System.Windows.Forms.Panel()
        Me.radHigh = New System.Windows.Forms.RadioButton()
        Me.radLow = New System.Windows.Forms.RadioButton()
        Me.cboBailiffEmp = New System.Windows.Forms.ComboBox()
        Me.cmdLastVisitBailiffNameClear = New System.Windows.Forms.Button()
        Me.cmdLastVisitBailiffNameAll = New System.Windows.Forms.Button()
        Me.dgvLastVisitBailiffName = New System.Windows.Forms.DataGridView()
        Me.LastVisitBailiffName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdOutOfHoursVisitAll = New System.Windows.Forms.Button()
        Me.dgvOutOfHoursVisit = New System.Windows.Forms.DataGridView()
        Me.OutOfHoursVisit = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdOutOfHoursVisitClear = New System.Windows.Forms.Button()
        Me.cmdSchemeNameClear = New System.Windows.Forms.Button()
        Me.dgvSchemeName = New System.Windows.Forms.DataGridView()
        Me.SchemeName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdSchemeNameAll = New System.Windows.Forms.Button()
        CType(Me.dgvLinkedPIF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvArrangementBrokenStage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvResidencyScore, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvWorkType, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAbsPerc.SuspendLayout()
        CType(Me.dgvAddConfirmed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvStageName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPaymentStage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvNumberOfVisitsAddress, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCGA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPhoneNumber, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDebtYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvInYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLinkedPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBalance.SuspendLayout()
        CType(Me.dgvLastVisitBailiffName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvOutOfHoursVisit, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSchemeName, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(6, 7)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(148, 21)
        Me.cboCompany.TabIndex = 161
        '
        'PostcodeArea
        '
        Me.PostcodeArea.DataPropertyName = "PostcodeArea"
        Me.PostcodeArea.HeaderText = "Area"
        Me.PostcodeArea.Name = "PostcodeArea"
        Me.PostcodeArea.ReadOnly = True
        Me.PostcodeArea.Width = 55
        '
        'cmsForm
        '
        Me.cmsForm.Name = "ContextMenuStrip1"
        Me.cmsForm.Size = New System.Drawing.Size(61, 4)
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 40
        '
        'cmsList
        '
        Me.cmsList.Name = "ContextMenuStrip1"
        Me.cmsList.Size = New System.Drawing.Size(61, 4)
        '
        'WorkType
        '
        Me.WorkType.DataPropertyName = "WorkType"
        Me.WorkType.HeaderText = "Work Type"
        Me.WorkType.Name = "WorkType"
        Me.WorkType.ReadOnly = True
        '
        'lblSummary
        '
        Me.lblSummary.AutoSize = True
        Me.lblSummary.Location = New System.Drawing.Point(87, 350)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(39, 13)
        Me.lblSummary.TabIndex = 148
        Me.lblSummary.Text = "Label1"
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 40
        '
        'dgvLinkedPIF
        '
        Me.dgvLinkedPIF.AllowUserToAddRows = False
        Me.dgvLinkedPIF.AllowUserToDeleteRows = False
        Me.dgvLinkedPIF.AllowUserToResizeColumns = False
        Me.dgvLinkedPIF.AllowUserToResizeRows = False
        Me.dgvLinkedPIF.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLinkedPIF.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LinkedPIF, Me.DataGridViewTextBoxColumn13})
        Me.dgvLinkedPIF.Location = New System.Drawing.Point(764, 161)
        Me.dgvLinkedPIF.Name = "dgvLinkedPIF"
        Me.dgvLinkedPIF.ReadOnly = True
        Me.dgvLinkedPIF.RowHeadersVisible = False
        Me.dgvLinkedPIF.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvLinkedPIF.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLinkedPIF.Size = New System.Drawing.Size(112, 65)
        Me.dgvLinkedPIF.TabIndex = 158
        '
        'LinkedPIF
        '
        Me.LinkedPIF.DataPropertyName = "LinkedPIF"
        Me.LinkedPIF.HeaderText = "Linked PIF"
        Me.LinkedPIF.Name = "LinkedPIF"
        Me.LinkedPIF.ReadOnly = True
        Me.LinkedPIF.Width = 70
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Width = 40
        '
        'cmsSummary
        '
        Me.cmsSummary.Name = "ContextMenuStrip1"
        Me.cmsSummary.Size = New System.Drawing.Size(61, 4)
        '
        'cmdArrangementBrokenStageClear
        '
        Me.cmdArrangementBrokenStageClear.Location = New System.Drawing.Point(826, 140)
        Me.cmdArrangementBrokenStageClear.Name = "cmdArrangementBrokenStageClear"
        Me.cmdArrangementBrokenStageClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdArrangementBrokenStageClear.TabIndex = 151
        Me.cmdArrangementBrokenStageClear.Text = "Clear"
        Me.cmdArrangementBrokenStageClear.UseVisualStyleBackColor = True
        '
        'dgvArrangementBrokenStage
        '
        Me.dgvArrangementBrokenStage.AllowUserToAddRows = False
        Me.dgvArrangementBrokenStage.AllowUserToDeleteRows = False
        Me.dgvArrangementBrokenStage.AllowUserToResizeColumns = False
        Me.dgvArrangementBrokenStage.AllowUserToResizeRows = False
        Me.dgvArrangementBrokenStage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvArrangementBrokenStage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ArrangementBrokenStage, Me.DataGridViewTextBoxColumn10})
        Me.dgvArrangementBrokenStage.Location = New System.Drawing.Point(764, 7)
        Me.dgvArrangementBrokenStage.Name = "dgvArrangementBrokenStage"
        Me.dgvArrangementBrokenStage.ReadOnly = True
        Me.dgvArrangementBrokenStage.RowHeadersVisible = False
        Me.dgvArrangementBrokenStage.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvArrangementBrokenStage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvArrangementBrokenStage.Size = New System.Drawing.Size(112, 133)
        Me.dgvArrangementBrokenStage.TabIndex = 149
        '
        'ArrangementBrokenStage
        '
        Me.ArrangementBrokenStage.DataPropertyName = "ArrangementBrokenStage"
        Me.ArrangementBrokenStage.HeaderText = "Broken"
        Me.ArrangementBrokenStage.Name = "ArrangementBrokenStage"
        Me.ArrangementBrokenStage.ReadOnly = True
        Me.ArrangementBrokenStage.Width = 70
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 40
        '
        'cmdArrangementBrokenStageAll
        '
        Me.cmdArrangementBrokenStageAll.Location = New System.Drawing.Point(764, 140)
        Me.cmdArrangementBrokenStageAll.Name = "cmdArrangementBrokenStageAll"
        Me.cmdArrangementBrokenStageAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdArrangementBrokenStageAll.TabIndex = 150
        Me.cmdArrangementBrokenStageAll.Text = "All"
        Me.cmdArrangementBrokenStageAll.UseVisualStyleBackColor = True
        '
        'cmdLinkedPIFClear
        '
        Me.cmdLinkedPIFClear.Location = New System.Drawing.Point(826, 226)
        Me.cmdLinkedPIFClear.Name = "cmdLinkedPIFClear"
        Me.cmdLinkedPIFClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedPIFClear.TabIndex = 160
        Me.cmdLinkedPIFClear.Text = "Clear"
        Me.cmdLinkedPIFClear.UseVisualStyleBackColor = True
        '
        'RefreshCheckTimer
        '
        Me.RefreshCheckTimer.Interval = 30000
        '
        'cmdLinkedPIFAll
        '
        Me.cmdLinkedPIFAll.Location = New System.Drawing.Point(764, 226)
        Me.cmdLinkedPIFAll.Name = "cmdLinkedPIFAll"
        Me.cmdLinkedPIFAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedPIFAll.TabIndex = 159
        Me.cmdLinkedPIFAll.Text = "All"
        Me.cmdLinkedPIFAll.UseVisualStyleBackColor = True
        '
        'lblPeriodType
        '
        Me.lblPeriodType.Location = New System.Drawing.Point(710, 349)
        Me.lblPeriodType.Name = "lblPeriodType"
        Me.lblPeriodType.Size = New System.Drawing.Size(119, 16)
        Me.lblPeriodType.TabIndex = 146
        Me.lblPeriodType.Text = "based on:"
        Me.lblPeriodType.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'dgvResidencyScore
        '
        Me.dgvResidencyScore.AllowUserToAddRows = False
        Me.dgvResidencyScore.AllowUserToDeleteRows = False
        Me.dgvResidencyScore.AllowUserToResizeColumns = False
        Me.dgvResidencyScore.AllowUserToResizeRows = False
        Me.dgvResidencyScore.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvResidencyScore.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ResidencyScore, Me.DataGridViewTextBoxColumn11})
        Me.dgvResidencyScore.Location = New System.Drawing.Point(1022, 114)
        Me.dgvResidencyScore.Name = "dgvResidencyScore"
        Me.dgvResidencyScore.ReadOnly = True
        Me.dgvResidencyScore.RowHeadersVisible = False
        Me.dgvResidencyScore.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvResidencyScore.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvResidencyScore.Size = New System.Drawing.Size(112, 112)
        Me.dgvResidencyScore.TabIndex = 152
        '
        'ResidencyScore
        '
        Me.ResidencyScore.DataPropertyName = "ResidencyScore"
        Me.ResidencyScore.HeaderText = "Res score"
        Me.ResidencyScore.Name = "ResidencyScore"
        Me.ResidencyScore.ReadOnly = True
        Me.ResidencyScore.Width = 70
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 40
        '
        'cboPeriodType
        '
        Me.cboPeriodType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriodType.FormattingEnabled = True
        Me.cboPeriodType.Location = New System.Drawing.Point(835, 344)
        Me.cboPeriodType.Name = "cboPeriodType"
        Me.cboPeriodType.Size = New System.Drawing.Size(110, 21)
        Me.cboPeriodType.TabIndex = 145
        '
        'dgvPostcodeArea
        '
        Me.dgvPostcodeArea.AllowUserToAddRows = False
        Me.dgvPostcodeArea.AllowUserToDeleteRows = False
        Me.dgvPostcodeArea.AllowUserToResizeColumns = False
        Me.dgvPostcodeArea.AllowUserToResizeRows = False
        Me.dgvPostcodeArea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPostcodeArea.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PostcodeArea, Me.DataGridViewTextBoxColumn9})
        Me.dgvPostcodeArea.Location = New System.Drawing.Point(637, 161)
        Me.dgvPostcodeArea.Name = "dgvPostcodeArea"
        Me.dgvPostcodeArea.ReadOnly = True
        Me.dgvPostcodeArea.RowHeadersVisible = False
        Me.dgvPostcodeArea.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvPostcodeArea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPostcodeArea.Size = New System.Drawing.Size(112, 151)
        Me.dgvPostcodeArea.TabIndex = 142
        '
        'dgvWorkType
        '
        Me.dgvWorkType.AllowUserToAddRows = False
        Me.dgvWorkType.AllowUserToDeleteRows = False
        Me.dgvWorkType.AllowUserToResizeColumns = False
        Me.dgvWorkType.AllowUserToResizeRows = False
        Me.dgvWorkType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvWorkType.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WorkType, Me.DataGridViewTextBoxColumn5})
        Me.dgvWorkType.Location = New System.Drawing.Point(345, 201)
        Me.dgvWorkType.Name = "dgvWorkType"
        Me.dgvWorkType.ReadOnly = True
        Me.dgvWorkType.RowHeadersVisible = False
        Me.dgvWorkType.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvWorkType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvWorkType.Size = New System.Drawing.Size(148, 111)
        Me.dgvWorkType.TabIndex = 134
        '
        'cmdRefreshDB
        '
        Me.cmdRefreshDB.Location = New System.Drawing.Point(6, 346)
        Me.cmdRefreshDB.Name = "cmdRefreshDB"
        Me.cmdRefreshDB.Size = New System.Drawing.Size(75, 20)
        Me.cmdRefreshDB.TabIndex = 137
        Me.cmdRefreshDB.Text = "Refresh DB"
        Me.cmdRefreshDB.UseVisualStyleBackColor = True
        '
        'cmdPostcodeAreaClear
        '
        Me.cmdPostcodeAreaClear.Location = New System.Drawing.Point(699, 312)
        Me.cmdPostcodeAreaClear.Name = "cmdPostcodeAreaClear"
        Me.cmdPostcodeAreaClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaClear.TabIndex = 144
        Me.cmdPostcodeAreaClear.Text = "Clear"
        Me.cmdPostcodeAreaClear.UseVisualStyleBackColor = True
        '
        'cmdResidencyScoreClear
        '
        Me.cmdResidencyScoreClear.Location = New System.Drawing.Point(1084, 226)
        Me.cmdResidencyScoreClear.Name = "cmdResidencyScoreClear"
        Me.cmdResidencyScoreClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdResidencyScoreClear.TabIndex = 154
        Me.cmdResidencyScoreClear.Text = "Clear"
        Me.cmdResidencyScoreClear.UseVisualStyleBackColor = True
        '
        'cmdPostcodeAreaAll
        '
        Me.cmdPostcodeAreaAll.Location = New System.Drawing.Point(637, 312)
        Me.cmdPostcodeAreaAll.Name = "cmdPostcodeAreaAll"
        Me.cmdPostcodeAreaAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaAll.TabIndex = 143
        Me.cmdPostcodeAreaAll.Text = "All"
        Me.cmdPostcodeAreaAll.UseVisualStyleBackColor = True
        '
        'cmdWorkTypeClear
        '
        Me.cmdWorkTypeClear.Location = New System.Drawing.Point(425, 312)
        Me.cmdWorkTypeClear.Name = "cmdWorkTypeClear"
        Me.cmdWorkTypeClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdWorkTypeClear.TabIndex = 136
        Me.cmdWorkTypeClear.Text = "Clear"
        Me.cmdWorkTypeClear.UseVisualStyleBackColor = True
        '
        'cmdResidencyScoreAll
        '
        Me.cmdResidencyScoreAll.Location = New System.Drawing.Point(1022, 226)
        Me.cmdResidencyScoreAll.Name = "cmdResidencyScoreAll"
        Me.cmdResidencyScoreAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdResidencyScoreAll.TabIndex = 153
        Me.cmdResidencyScoreAll.Text = "All"
        Me.cmdResidencyScoreAll.UseVisualStyleBackColor = True
        '
        'chkTopClients
        '
        Me.chkTopClients.AutoSize = True
        Me.chkTopClients.Location = New System.Drawing.Point(201, 349)
        Me.chkTopClients.Name = "chkTopClients"
        Me.chkTopClients.Size = New System.Drawing.Size(79, 17)
        Me.chkTopClients.TabIndex = 133
        Me.chkTopClients.Text = "Top Clients"
        Me.chkTopClients.UseVisualStyleBackColor = True
        '
        'ClientName
        '
        Me.ClientName.DataPropertyName = "ClientName"
        Me.ClientName.HeaderText = "Client"
        Me.ClientName.Name = "ClientName"
        Me.ClientName.ReadOnly = True
        '
        'CGA
        '
        Me.CGA.DataPropertyName = "CGA"
        Me.CGA.HeaderText = "CGA"
        Me.CGA.Name = "CGA"
        Me.CGA.ReadOnly = True
        Me.CGA.Width = 70
        '
        'colStageNameTotal
        '
        Me.colStageNameTotal.DataPropertyName = "Total"
        Me.colStageNameTotal.HeaderText = "Total"
        Me.colStageNameTotal.Name = "colStageNameTotal"
        Me.colStageNameTotal.ReadOnly = True
        Me.colStageNameTotal.Width = 40
        '
        'cmdPaymentStageClear
        '
        Me.cmdPaymentStageClear.Location = New System.Drawing.Point(699, 140)
        Me.cmdPaymentStageClear.Name = "cmdPaymentStageClear"
        Me.cmdPaymentStageClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPaymentStageClear.TabIndex = 126
        Me.cmdPaymentStageClear.Text = "Clear"
        Me.cmdPaymentStageClear.UseVisualStyleBackColor = True
        '
        'cmdPaymentStageAll
        '
        Me.cmdPaymentStageAll.Location = New System.Drawing.Point(637, 140)
        Me.cmdPaymentStageAll.Name = "cmdPaymentStageAll"
        Me.cmdPaymentStageAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPaymentStageAll.TabIndex = 125
        Me.cmdPaymentStageAll.Text = "All"
        Me.cmdPaymentStageAll.UseVisualStyleBackColor = True
        '
        'colClientNameTotal
        '
        Me.colClientNameTotal.DataPropertyName = "Total"
        Me.colClientNameTotal.HeaderText = "Total"
        Me.colClientNameTotal.Name = "colClientNameTotal"
        Me.colClientNameTotal.ReadOnly = True
        Me.colClientNameTotal.Width = 40
        '
        'pnlAbsPerc
        '
        Me.pnlAbsPerc.Controls.Add(Me.radPerc)
        Me.pnlAbsPerc.Controls.Add(Me.radAbs)
        Me.pnlAbsPerc.Location = New System.Drawing.Point(957, 344)
        Me.pnlAbsPerc.Name = "pnlAbsPerc"
        Me.pnlAbsPerc.Size = New System.Drawing.Size(99, 25)
        Me.pnlAbsPerc.TabIndex = 132
        '
        'radPerc
        '
        Me.radPerc.AutoSize = True
        Me.radPerc.Location = New System.Drawing.Point(55, 4)
        Me.radPerc.Name = "radPerc"
        Me.radPerc.Size = New System.Drawing.Size(33, 17)
        Me.radPerc.TabIndex = 4
        Me.radPerc.TabStop = True
        Me.radPerc.Text = "%"
        Me.radPerc.UseVisualStyleBackColor = True
        '
        'radAbs
        '
        Me.radAbs.AutoSize = True
        Me.radAbs.Location = New System.Drawing.Point(6, 4)
        Me.radAbs.Name = "radAbs"
        Me.radAbs.Size = New System.Drawing.Size(43, 17)
        Me.radAbs.TabIndex = 3
        Me.radAbs.TabStop = True
        Me.radAbs.Text = "Abs"
        Me.radAbs.UseVisualStyleBackColor = True
        '
        'lblLine1
        '
        Me.lblLine1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLine1.Location = New System.Drawing.Point(6, 337)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(1128, 1)
        Me.lblLine1.TabIndex = 131
        '
        'cmdWorkTypeAll
        '
        Me.cmdWorkTypeAll.Location = New System.Drawing.Point(363, 312)
        Me.cmdWorkTypeAll.Name = "cmdWorkTypeAll"
        Me.cmdWorkTypeAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdWorkTypeAll.TabIndex = 135
        Me.cmdWorkTypeAll.Text = "All"
        Me.cmdWorkTypeAll.UseVisualStyleBackColor = True
        '
        'cmdClearAll
        '
        Me.cmdClearAll.Location = New System.Drawing.Point(1064, 345)
        Me.cmdClearAll.Name = "cmdClearAll"
        Me.cmdClearAll.Size = New System.Drawing.Size(68, 20)
        Me.cmdClearAll.TabIndex = 130
        Me.cmdClearAll.Text = "Clear All"
        Me.cmdClearAll.UseVisualStyleBackColor = True
        '
        'cmdStageClear
        '
        Me.cmdStageClear.Location = New System.Drawing.Point(425, 180)
        Me.cmdStageClear.Name = "cmdStageClear"
        Me.cmdStageClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageClear.TabIndex = 114
        Me.cmdStageClear.Text = "Clear"
        Me.cmdStageClear.UseVisualStyleBackColor = True
        '
        'cmdStageAll
        '
        Me.cmdStageAll.Location = New System.Drawing.Point(363, 180)
        Me.cmdStageAll.Name = "cmdStageAll"
        Me.cmdStageAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageAll.TabIndex = 113
        Me.cmdStageAll.Text = "All"
        Me.cmdStageAll.UseVisualStyleBackColor = True
        '
        'cmdClientClear
        '
        Me.cmdClientClear.Location = New System.Drawing.Point(254, 135)
        Me.cmdClientClear.Name = "cmdClientClear"
        Me.cmdClientClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientClear.TabIndex = 118
        Me.cmdClientClear.Text = "Clear"
        Me.cmdClientClear.UseVisualStyleBackColor = True
        '
        'dgvAddConfirmed
        '
        Me.dgvAddConfirmed.AllowUserToAddRows = False
        Me.dgvAddConfirmed.AllowUserToDeleteRows = False
        Me.dgvAddConfirmed.AllowUserToResizeColumns = False
        Me.dgvAddConfirmed.AllowUserToResizeRows = False
        Me.dgvAddConfirmed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAddConfirmed.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AddConfirmed, Me.DataGridViewTextBoxColumn3})
        Me.dgvAddConfirmed.Location = New System.Drawing.Point(890, 247)
        Me.dgvAddConfirmed.Name = "dgvAddConfirmed"
        Me.dgvAddConfirmed.ReadOnly = True
        Me.dgvAddConfirmed.RowHeadersVisible = False
        Me.dgvAddConfirmed.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvAddConfirmed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAddConfirmed.Size = New System.Drawing.Size(118, 65)
        Me.dgvAddConfirmed.TabIndex = 127
        '
        'AddConfirmed
        '
        Me.AddConfirmed.DataPropertyName = "AddConfirmed"
        Me.AddConfirmed.HeaderText = "Confirmed"
        Me.AddConfirmed.Name = "AddConfirmed"
        Me.AddConfirmed.ReadOnly = True
        Me.AddConfirmed.Width = 70
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 40
        '
        'cmdCGAClear
        '
        Me.cmdCGAClear.Location = New System.Drawing.Point(571, 312)
        Me.cmdCGAClear.Name = "cmdCGAClear"
        Me.cmdCGAClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdCGAClear.TabIndex = 122
        Me.cmdCGAClear.Text = "Clear"
        Me.cmdCGAClear.UseVisualStyleBackColor = True
        '
        'cmdCGAAll
        '
        Me.cmdCGAAll.Location = New System.Drawing.Point(509, 312)
        Me.cmdCGAAll.Name = "cmdCGAAll"
        Me.cmdCGAAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdCGAAll.TabIndex = 121
        Me.cmdCGAAll.Text = "All"
        Me.cmdCGAAll.UseVisualStyleBackColor = True
        '
        'cmdNumberOfVisitsAddressAll
        '
        Me.cmdNumberOfVisitsAddressAll.Location = New System.Drawing.Point(893, 226)
        Me.cmdNumberOfVisitsAddressAll.Name = "cmdNumberOfVisitsAddressAll"
        Me.cmdNumberOfVisitsAddressAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdNumberOfVisitsAddressAll.TabIndex = 123
        Me.cmdNumberOfVisitsAddressAll.Text = "All"
        Me.cmdNumberOfVisitsAddressAll.UseVisualStyleBackColor = True
        '
        'cmdClientAll
        '
        Me.cmdClientAll.Location = New System.Drawing.Point(192, 135)
        Me.cmdClientAll.Name = "cmdClientAll"
        Me.cmdClientAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientAll.TabIndex = 117
        Me.cmdClientAll.Text = "All"
        Me.cmdClientAll.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 40
        '
        'StageName
        '
        Me.StageName.DataPropertyName = "StageName"
        Me.StageName.HeaderText = "Stage"
        Me.StageName.Name = "StageName"
        Me.StageName.ReadOnly = True
        Me.StageName.Width = 90
        '
        'cmdNumberOfVisitsAddressClear
        '
        Me.cmdNumberOfVisitsAddressClear.Location = New System.Drawing.Point(955, 226)
        Me.cmdNumberOfVisitsAddressClear.Name = "cmdNumberOfVisitsAddressClear"
        Me.cmdNumberOfVisitsAddressClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdNumberOfVisitsAddressClear.TabIndex = 124
        Me.cmdNumberOfVisitsAddressClear.Text = "Clear"
        Me.cmdNumberOfVisitsAddressClear.UseVisualStyleBackColor = True
        '
        'dgvClientName
        '
        Me.dgvClientName.AllowUserToAddRows = False
        Me.dgvClientName.AllowUserToDeleteRows = False
        Me.dgvClientName.AllowUserToResizeColumns = False
        Me.dgvClientName.AllowUserToResizeRows = False
        Me.dgvClientName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClientName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClientName, Me.colClientNameTotal})
        Me.dgvClientName.Location = New System.Drawing.Point(169, 7)
        Me.dgvClientName.Name = "dgvClientName"
        Me.dgvClientName.ReadOnly = True
        Me.dgvClientName.RowHeadersVisible = False
        Me.dgvClientName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClientName.Size = New System.Drawing.Size(160, 128)
        Me.dgvClientName.TabIndex = 107
        '
        'dgvStageName
        '
        Me.dgvStageName.AllowUserToAddRows = False
        Me.dgvStageName.AllowUserToDeleteRows = False
        Me.dgvStageName.AllowUserToResizeColumns = False
        Me.dgvStageName.AllowUserToResizeRows = False
        Me.dgvStageName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStageName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StageName, Me.colStageNameTotal})
        Me.dgvStageName.Location = New System.Drawing.Point(345, 7)
        Me.dgvStageName.Name = "dgvStageName"
        Me.dgvStageName.ReadOnly = True
        Me.dgvStageName.RowHeadersVisible = False
        Me.dgvStageName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvStageName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvStageName.Size = New System.Drawing.Size(148, 173)
        Me.dgvStageName.TabIndex = 106
        '
        'cmdAddConfirmedClear
        '
        Me.cmdAddConfirmedClear.Location = New System.Drawing.Point(955, 312)
        Me.cmdAddConfirmedClear.Name = "cmdAddConfirmedClear"
        Me.cmdAddConfirmedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdAddConfirmedClear.TabIndex = 129
        Me.cmdAddConfirmedClear.Text = "Clear"
        Me.cmdAddConfirmedClear.UseVisualStyleBackColor = True
        '
        'cmdAddConfirmedAll
        '
        Me.cmdAddConfirmedAll.Location = New System.Drawing.Point(893, 312)
        Me.cmdAddConfirmedAll.Name = "cmdAddConfirmedAll"
        Me.cmdAddConfirmedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdAddConfirmedAll.TabIndex = 128
        Me.cmdAddConfirmedAll.Text = "All"
        Me.cmdAddConfirmedAll.UseVisualStyleBackColor = True
        '
        'dgvPaymentStage
        '
        Me.dgvPaymentStage.AllowUserToAddRows = False
        Me.dgvPaymentStage.AllowUserToDeleteRows = False
        Me.dgvPaymentStage.AllowUserToResizeColumns = False
        Me.dgvPaymentStage.AllowUserToResizeRows = False
        Me.dgvPaymentStage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPaymentStage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PaymentStage, Me.DataGridViewTextBoxColumn2})
        Me.dgvPaymentStage.Location = New System.Drawing.Point(637, 7)
        Me.dgvPaymentStage.Name = "dgvPaymentStage"
        Me.dgvPaymentStage.ReadOnly = True
        Me.dgvPaymentStage.RowHeadersVisible = False
        Me.dgvPaymentStage.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvPaymentStage.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPaymentStage.Size = New System.Drawing.Size(112, 133)
        Me.dgvPaymentStage.TabIndex = 108
        '
        'PaymentStage
        '
        Me.PaymentStage.DataPropertyName = "PaymentStage"
        Me.PaymentStage.HeaderText = "Payment"
        Me.PaymentStage.Name = "PaymentStage"
        Me.PaymentStage.ReadOnly = True
        Me.PaymentStage.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 40
        '
        'dgvSummary
        '
        Me.dgvSummary.AllowUserToAddRows = False
        Me.dgvSummary.AllowUserToDeleteRows = False
        Me.dgvSummary.AllowUserToOrderColumns = True
        Me.dgvSummary.AllowUserToResizeRows = False
        Me.dgvSummary.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSummary.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSummary.Location = New System.Drawing.Point(6, 375)
        Me.dgvSummary.Name = "dgvSummary"
        Me.dgvSummary.ReadOnly = True
        Me.dgvSummary.RowHeadersVisible = False
        Me.dgvSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvSummary.Size = New System.Drawing.Size(1128, 198)
        Me.dgvSummary.TabIndex = 105
        '
        'cmdPhoneNumberAll
        '
        Me.cmdPhoneNumberAll.Location = New System.Drawing.Point(1022, 312)
        Me.cmdPhoneNumberAll.Name = "cmdPhoneNumberAll"
        Me.cmdPhoneNumberAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPhoneNumberAll.TabIndex = 115
        Me.cmdPhoneNumberAll.Text = "All"
        Me.cmdPhoneNumberAll.UseVisualStyleBackColor = True
        '
        'cmdPhoneNumberClear
        '
        Me.cmdPhoneNumberClear.Location = New System.Drawing.Point(1084, 312)
        Me.cmdPhoneNumberClear.Name = "cmdPhoneNumberClear"
        Me.cmdPhoneNumberClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPhoneNumberClear.TabIndex = 116
        Me.cmdPhoneNumberClear.Text = "Clear"
        Me.cmdPhoneNumberClear.UseVisualStyleBackColor = True
        '
        'dgvNumberOfVisitsAddress
        '
        Me.dgvNumberOfVisitsAddress.AllowUserToAddRows = False
        Me.dgvNumberOfVisitsAddress.AllowUserToDeleteRows = False
        Me.dgvNumberOfVisitsAddress.AllowUserToResizeColumns = False
        Me.dgvNumberOfVisitsAddress.AllowUserToResizeRows = False
        Me.dgvNumberOfVisitsAddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvNumberOfVisitsAddress.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumberOfVisitsAddress, Me.DataGridViewTextBoxColumn6})
        Me.dgvNumberOfVisitsAddress.Location = New System.Drawing.Point(890, 7)
        Me.dgvNumberOfVisitsAddress.Name = "dgvNumberOfVisitsAddress"
        Me.dgvNumberOfVisitsAddress.ReadOnly = True
        Me.dgvNumberOfVisitsAddress.RowHeadersVisible = False
        Me.dgvNumberOfVisitsAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvNumberOfVisitsAddress.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvNumberOfVisitsAddress.Size = New System.Drawing.Size(118, 219)
        Me.dgvNumberOfVisitsAddress.TabIndex = 111
        '
        'NumberOfVisitsAddress
        '
        Me.NumberOfVisitsAddress.DataPropertyName = "NumberOfVisitsAddress"
        Me.NumberOfVisitsAddress.HeaderText = "# Visits"
        Me.NumberOfVisitsAddress.Name = "NumberOfVisitsAddress"
        Me.NumberOfVisitsAddress.ReadOnly = True
        Me.NumberOfVisitsAddress.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.NumberOfVisitsAddress.Width = 70
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 40
        '
        'dgvCGA
        '
        Me.dgvCGA.AllowUserToAddRows = False
        Me.dgvCGA.AllowUserToDeleteRows = False
        Me.dgvCGA.AllowUserToResizeColumns = False
        Me.dgvCGA.AllowUserToResizeRows = False
        Me.dgvCGA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCGA.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CGA, Me.DataGridViewTextBoxColumn4})
        Me.dgvCGA.Location = New System.Drawing.Point(509, 247)
        Me.dgvCGA.Name = "dgvCGA"
        Me.dgvCGA.ReadOnly = True
        Me.dgvCGA.RowHeadersVisible = False
        Me.dgvCGA.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvCGA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCGA.Size = New System.Drawing.Size(112, 65)
        Me.dgvCGA.TabIndex = 109
        '
        'dgvPhoneNumber
        '
        Me.dgvPhoneNumber.AllowUserToAddRows = False
        Me.dgvPhoneNumber.AllowUserToDeleteRows = False
        Me.dgvPhoneNumber.AllowUserToResizeColumns = False
        Me.dgvPhoneNumber.AllowUserToResizeRows = False
        Me.dgvPhoneNumber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPhoneNumber.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PhoneNumber, Me.DataGridViewTextBoxColumn8})
        Me.dgvPhoneNumber.Location = New System.Drawing.Point(1022, 247)
        Me.dgvPhoneNumber.Name = "dgvPhoneNumber"
        Me.dgvPhoneNumber.ReadOnly = True
        Me.dgvPhoneNumber.RowHeadersVisible = False
        Me.dgvPhoneNumber.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvPhoneNumber.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPhoneNumber.Size = New System.Drawing.Size(112, 65)
        Me.dgvPhoneNumber.TabIndex = 110
        '
        'PhoneNumber
        '
        Me.PhoneNumber.DataPropertyName = "PhoneNumber"
        Me.PhoneNumber.HeaderText = "Phone"
        Me.PhoneNumber.Name = "PhoneNumber"
        Me.PhoneNumber.ReadOnly = True
        Me.PhoneNumber.Width = 70
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 40
        '
        'dgvDebtYear
        '
        Me.dgvDebtYear.AllowUserToAddRows = False
        Me.dgvDebtYear.AllowUserToDeleteRows = False
        Me.dgvDebtYear.AllowUserToResizeColumns = False
        Me.dgvDebtYear.AllowUserToResizeRows = False
        Me.dgvDebtYear.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDebtYear.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DebtYear, Me.DataGridViewTextBoxColumn1})
        Me.dgvDebtYear.Location = New System.Drawing.Point(509, 93)
        Me.dgvDebtYear.Name = "dgvDebtYear"
        Me.dgvDebtYear.ReadOnly = True
        Me.dgvDebtYear.RowHeadersVisible = False
        Me.dgvDebtYear.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvDebtYear.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDebtYear.Size = New System.Drawing.Size(112, 133)
        Me.dgvDebtYear.TabIndex = 165
        '
        'DebtYear
        '
        Me.DebtYear.DataPropertyName = "DebtYear"
        Me.DebtYear.HeaderText = "Year"
        Me.DebtYear.Name = "DebtYear"
        Me.DebtYear.ReadOnly = True
        Me.DebtYear.Width = 55
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 40
        '
        'cmdDebtYearClear
        '
        Me.cmdDebtYearClear.Location = New System.Drawing.Point(571, 226)
        Me.cmdDebtYearClear.Name = "cmdDebtYearClear"
        Me.cmdDebtYearClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdDebtYearClear.TabIndex = 167
        Me.cmdDebtYearClear.Text = "Clear"
        Me.cmdDebtYearClear.UseVisualStyleBackColor = True
        '
        'cmdDebtYearAll
        '
        Me.cmdDebtYearAll.Location = New System.Drawing.Point(509, 226)
        Me.cmdDebtYearAll.Name = "cmdDebtYearAll"
        Me.cmdDebtYearAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdDebtYearAll.TabIndex = 166
        Me.cmdDebtYearAll.Text = "All"
        Me.cmdDebtYearAll.UseVisualStyleBackColor = True
        '
        'cmdInYearClear
        '
        Me.cmdInYearClear.Location = New System.Drawing.Point(572, 72)
        Me.cmdInYearClear.Name = "cmdInYearClear"
        Me.cmdInYearClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdInYearClear.TabIndex = 170
        Me.cmdInYearClear.Text = "Clear"
        Me.cmdInYearClear.UseVisualStyleBackColor = True
        '
        'dgvInYear
        '
        Me.dgvInYear.AllowUserToAddRows = False
        Me.dgvInYear.AllowUserToDeleteRows = False
        Me.dgvInYear.AllowUserToResizeColumns = False
        Me.dgvInYear.AllowUserToResizeRows = False
        Me.dgvInYear.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvInYear.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.InYear, Me.DataGridViewTextBoxColumn15})
        Me.dgvInYear.Location = New System.Drawing.Point(509, 7)
        Me.dgvInYear.Name = "dgvInYear"
        Me.dgvInYear.ReadOnly = True
        Me.dgvInYear.RowHeadersVisible = False
        Me.dgvInYear.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvInYear.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvInYear.Size = New System.Drawing.Size(112, 65)
        Me.dgvInYear.TabIndex = 168
        '
        'InYear
        '
        Me.InYear.DataPropertyName = "InYear"
        Me.InYear.HeaderText = "In year"
        Me.InYear.Name = "InYear"
        Me.InYear.ReadOnly = True
        Me.InYear.Width = 55
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn15.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Width = 40
        '
        'cmdInYearAll
        '
        Me.cmdInYearAll.Location = New System.Drawing.Point(509, 72)
        Me.cmdInYearAll.Name = "cmdInYearAll"
        Me.cmdInYearAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdInYearAll.TabIndex = 169
        Me.cmdInYearAll.Text = "All"
        Me.cmdInYearAll.UseVisualStyleBackColor = True
        '
        'dgvLinkedPayment
        '
        Me.dgvLinkedPayment.AllowUserToAddRows = False
        Me.dgvLinkedPayment.AllowUserToDeleteRows = False
        Me.dgvLinkedPayment.AllowUserToResizeColumns = False
        Me.dgvLinkedPayment.AllowUserToResizeRows = False
        Me.dgvLinkedPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLinkedPayment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LinkedPayment, Me.DataGridViewTextBoxColumn17})
        Me.dgvLinkedPayment.Location = New System.Drawing.Point(764, 247)
        Me.dgvLinkedPayment.Name = "dgvLinkedPayment"
        Me.dgvLinkedPayment.ReadOnly = True
        Me.dgvLinkedPayment.RowHeadersVisible = False
        Me.dgvLinkedPayment.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvLinkedPayment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLinkedPayment.Size = New System.Drawing.Size(112, 65)
        Me.dgvLinkedPayment.TabIndex = 171
        '
        'LinkedPayment
        '
        Me.LinkedPayment.DataPropertyName = "LinkedPayment"
        Me.LinkedPayment.HeaderText = "Linked Pay"
        Me.LinkedPayment.Name = "LinkedPayment"
        Me.LinkedPayment.ReadOnly = True
        Me.LinkedPayment.Width = 70
        '
        'DataGridViewTextBoxColumn17
        '
        Me.DataGridViewTextBoxColumn17.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn17.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn17.Name = "DataGridViewTextBoxColumn17"
        Me.DataGridViewTextBoxColumn17.ReadOnly = True
        Me.DataGridViewTextBoxColumn17.Width = 40
        '
        'cmdLinkedPaymentClear
        '
        Me.cmdLinkedPaymentClear.Location = New System.Drawing.Point(826, 312)
        Me.cmdLinkedPaymentClear.Name = "cmdLinkedPaymentClear"
        Me.cmdLinkedPaymentClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedPaymentClear.TabIndex = 173
        Me.cmdLinkedPaymentClear.Text = "Clear"
        Me.cmdLinkedPaymentClear.UseVisualStyleBackColor = True
        '
        'cmdLinkedPaymentAll
        '
        Me.cmdLinkedPaymentAll.Location = New System.Drawing.Point(764, 312)
        Me.cmdLinkedPaymentAll.Name = "cmdLinkedPaymentAll"
        Me.cmdLinkedPaymentAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedPaymentAll.TabIndex = 172
        Me.cmdLinkedPaymentAll.Text = "All"
        Me.cmdLinkedPaymentAll.UseVisualStyleBackColor = True
        '
        'pnlBalance
        '
        Me.pnlBalance.Controls.Add(Me.radHigh)
        Me.pnlBalance.Controls.Add(Me.radLow)
        Me.pnlBalance.Location = New System.Drawing.Point(312, 344)
        Me.pnlBalance.Name = "pnlBalance"
        Me.pnlBalance.Size = New System.Drawing.Size(197, 25)
        Me.pnlBalance.TabIndex = 174
        '
        'radHigh
        '
        Me.radHigh.AutoCheck = False
        Me.radHigh.AutoSize = True
        Me.radHigh.Location = New System.Drawing.Point(106, 4)
        Me.radHigh.Name = "radHigh"
        Me.radHigh.Size = New System.Drawing.Size(73, 17)
        Me.radHigh.TabIndex = 1
        Me.radHigh.TabStop = True
        Me.radHigh.Text = "> £10,000"
        Me.radHigh.UseVisualStyleBackColor = True
        '
        'radLow
        '
        Me.radLow.AutoCheck = False
        Me.radLow.AutoSize = True
        Me.radLow.Location = New System.Drawing.Point(6, 4)
        Me.radLow.Name = "radLow"
        Me.radLow.Size = New System.Drawing.Size(86, 17)
        Me.radLow.TabIndex = 0
        Me.radLow.TabStop = True
        Me.radLow.Text = "< £50 no link"
        Me.radLow.UseVisualStyleBackColor = True
        '
        'cboBailiffEmp
        '
        Me.cboBailiffEmp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboBailiffEmp.FormattingEnabled = True
        Me.cboBailiffEmp.Items.AddRange(New Object() {"All", "Employed", "Self employed"})
        Me.cboBailiffEmp.Location = New System.Drawing.Point(6, 291)
        Me.cboBailiffEmp.Name = "cboBailiffEmp"
        Me.cboBailiffEmp.Size = New System.Drawing.Size(148, 21)
        Me.cboBailiffEmp.TabIndex = 178
        '
        'cmdLastVisitBailiffNameClear
        '
        Me.cmdLastVisitBailiffNameClear.Location = New System.Drawing.Point(86, 312)
        Me.cmdLastVisitBailiffNameClear.Name = "cmdLastVisitBailiffNameClear"
        Me.cmdLastVisitBailiffNameClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdLastVisitBailiffNameClear.TabIndex = 177
        Me.cmdLastVisitBailiffNameClear.Text = "Clear"
        Me.cmdLastVisitBailiffNameClear.UseVisualStyleBackColor = True
        '
        'cmdLastVisitBailiffNameAll
        '
        Me.cmdLastVisitBailiffNameAll.Location = New System.Drawing.Point(24, 312)
        Me.cmdLastVisitBailiffNameAll.Name = "cmdLastVisitBailiffNameAll"
        Me.cmdLastVisitBailiffNameAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdLastVisitBailiffNameAll.TabIndex = 176
        Me.cmdLastVisitBailiffNameAll.Text = "All"
        Me.cmdLastVisitBailiffNameAll.UseVisualStyleBackColor = True
        '
        'dgvLastVisitBailiffName
        '
        Me.dgvLastVisitBailiffName.AllowUserToAddRows = False
        Me.dgvLastVisitBailiffName.AllowUserToDeleteRows = False
        Me.dgvLastVisitBailiffName.AllowUserToResizeColumns = False
        Me.dgvLastVisitBailiffName.AllowUserToResizeRows = False
        Me.dgvLastVisitBailiffName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvLastVisitBailiffName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LastVisitBailiffName, Me.DataGridViewTextBoxColumn14})
        Me.dgvLastVisitBailiffName.Location = New System.Drawing.Point(6, 33)
        Me.dgvLastVisitBailiffName.Name = "dgvLastVisitBailiffName"
        Me.dgvLastVisitBailiffName.ReadOnly = True
        Me.dgvLastVisitBailiffName.RowHeadersVisible = False
        Me.dgvLastVisitBailiffName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLastVisitBailiffName.Size = New System.Drawing.Size(148, 256)
        Me.dgvLastVisitBailiffName.TabIndex = 175
        '
        'LastVisitBailiffName
        '
        Me.LastVisitBailiffName.DataPropertyName = "LastVisitBailiffName"
        Me.LastVisitBailiffName.HeaderText = "Name"
        Me.LastVisitBailiffName.Name = "LastVisitBailiffName"
        Me.LastVisitBailiffName.ReadOnly = True
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn14.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Width = 40
        '
        'cmdOutOfHoursVisitAll
        '
        Me.cmdOutOfHoursVisitAll.Location = New System.Drawing.Point(1022, 72)
        Me.cmdOutOfHoursVisitAll.Name = "cmdOutOfHoursVisitAll"
        Me.cmdOutOfHoursVisitAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdOutOfHoursVisitAll.TabIndex = 180
        Me.cmdOutOfHoursVisitAll.Text = "All"
        Me.cmdOutOfHoursVisitAll.UseVisualStyleBackColor = True
        '
        'dgvOutOfHoursVisit
        '
        Me.dgvOutOfHoursVisit.AllowUserToAddRows = False
        Me.dgvOutOfHoursVisit.AllowUserToDeleteRows = False
        Me.dgvOutOfHoursVisit.AllowUserToResizeColumns = False
        Me.dgvOutOfHoursVisit.AllowUserToResizeRows = False
        Me.dgvOutOfHoursVisit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvOutOfHoursVisit.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OutOfHoursVisit, Me.DataGridViewTextBoxColumn12})
        Me.dgvOutOfHoursVisit.Location = New System.Drawing.Point(1022, 7)
        Me.dgvOutOfHoursVisit.Name = "dgvOutOfHoursVisit"
        Me.dgvOutOfHoursVisit.ReadOnly = True
        Me.dgvOutOfHoursVisit.RowHeadersVisible = False
        Me.dgvOutOfHoursVisit.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvOutOfHoursVisit.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvOutOfHoursVisit.Size = New System.Drawing.Size(112, 65)
        Me.dgvOutOfHoursVisit.TabIndex = 179
        '
        'OutOfHoursVisit
        '
        Me.OutOfHoursVisit.DataPropertyName = "OutOfHoursVisit"
        Me.OutOfHoursVisit.HeaderText = "OOH"
        Me.OutOfHoursVisit.Name = "OutOfHoursVisit"
        Me.OutOfHoursVisit.ReadOnly = True
        Me.OutOfHoursVisit.Width = 70
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 40
        '
        'cmdOutOfHoursVisitClear
        '
        Me.cmdOutOfHoursVisitClear.Location = New System.Drawing.Point(1084, 72)
        Me.cmdOutOfHoursVisitClear.Name = "cmdOutOfHoursVisitClear"
        Me.cmdOutOfHoursVisitClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdOutOfHoursVisitClear.TabIndex = 181
        Me.cmdOutOfHoursVisitClear.Text = "Clear"
        Me.cmdOutOfHoursVisitClear.UseVisualStyleBackColor = True
        '
        'cmdSchemeNameClear
        '
        Me.cmdSchemeNameClear.Location = New System.Drawing.Point(255, 312)
        Me.cmdSchemeNameClear.Name = "cmdSchemeNameClear"
        Me.cmdSchemeNameClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdSchemeNameClear.TabIndex = 184
        Me.cmdSchemeNameClear.Text = "Clear"
        Me.cmdSchemeNameClear.UseVisualStyleBackColor = True
        '
        'dgvSchemeName
        '
        Me.dgvSchemeName.AllowUserToAddRows = False
        Me.dgvSchemeName.AllowUserToDeleteRows = False
        Me.dgvSchemeName.AllowUserToResizeColumns = False
        Me.dgvSchemeName.AllowUserToResizeRows = False
        Me.dgvSchemeName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSchemeName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SchemeName, Me.Total})
        Me.dgvSchemeName.Location = New System.Drawing.Point(169, 156)
        Me.dgvSchemeName.Name = "dgvSchemeName"
        Me.dgvSchemeName.ReadOnly = True
        Me.dgvSchemeName.RowHeadersVisible = False
        Me.dgvSchemeName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSchemeName.Size = New System.Drawing.Size(160, 156)
        Me.dgvSchemeName.TabIndex = 182
        '
        'SchemeName
        '
        Me.SchemeName.DataPropertyName = "SchemeName"
        Me.SchemeName.HeaderText = "Scheme"
        Me.SchemeName.Name = "SchemeName"
        Me.SchemeName.ReadOnly = True
        '
        'Total
        '
        Me.Total.DataPropertyName = "Total"
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        Me.Total.Width = 40
        '
        'cmdSchemeNameAll
        '
        Me.cmdSchemeNameAll.Location = New System.Drawing.Point(193, 312)
        Me.cmdSchemeNameAll.Name = "cmdSchemeNameAll"
        Me.cmdSchemeNameAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdSchemeNameAll.TabIndex = 183
        Me.cmdSchemeNameAll.Text = "All"
        Me.cmdSchemeNameAll.UseVisualStyleBackColor = True
        '
        'frmPostEnforcementSummary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1141, 579)
        Me.Controls.Add(Me.cmdSchemeNameClear)
        Me.Controls.Add(Me.dgvSchemeName)
        Me.Controls.Add(Me.cmdSchemeNameAll)
        Me.Controls.Add(Me.cmdOutOfHoursVisitAll)
        Me.Controls.Add(Me.dgvOutOfHoursVisit)
        Me.Controls.Add(Me.cmdOutOfHoursVisitClear)
        Me.Controls.Add(Me.cboBailiffEmp)
        Me.Controls.Add(Me.cmdLastVisitBailiffNameClear)
        Me.Controls.Add(Me.cmdLastVisitBailiffNameAll)
        Me.Controls.Add(Me.dgvLastVisitBailiffName)
        Me.Controls.Add(Me.dgvDebtYear)
        Me.Controls.Add(Me.dgvLinkedPayment)
        Me.Controls.Add(Me.dgvInYear)
        Me.Controls.Add(Me.cmdDebtYearClear)
        Me.Controls.Add(Me.cmdLinkedPaymentClear)
        Me.Controls.Add(Me.cmdLinkedPaymentAll)
        Me.Controls.Add(Me.pnlBalance)
        Me.Controls.Add(Me.cmdDebtYearAll)
        Me.Controls.Add(Me.cboCompany)
        Me.Controls.Add(Me.lblSummary)
        Me.Controls.Add(Me.cmdInYearClear)
        Me.Controls.Add(Me.cmdInYearAll)
        Me.Controls.Add(Me.dgvLinkedPIF)
        Me.Controls.Add(Me.cmdArrangementBrokenStageClear)
        Me.Controls.Add(Me.dgvArrangementBrokenStage)
        Me.Controls.Add(Me.lblPeriodType)
        Me.Controls.Add(Me.cmdArrangementBrokenStageAll)
        Me.Controls.Add(Me.cmdLinkedPIFClear)
        Me.Controls.Add(Me.cboPeriodType)
        Me.Controls.Add(Me.cmdLinkedPIFAll)
        Me.Controls.Add(Me.dgvResidencyScore)
        Me.Controls.Add(Me.dgvPostcodeArea)
        Me.Controls.Add(Me.dgvWorkType)
        Me.Controls.Add(Me.cmdRefreshDB)
        Me.Controls.Add(Me.chkTopClients)
        Me.Controls.Add(Me.cmdResidencyScoreClear)
        Me.Controls.Add(Me.cmdWorkTypeClear)
        Me.Controls.Add(Me.cmdPostcodeAreaClear)
        Me.Controls.Add(Me.cmdPostcodeAreaAll)
        Me.Controls.Add(Me.pnlAbsPerc)
        Me.Controls.Add(Me.cmdResidencyScoreAll)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdClearAll)
        Me.Controls.Add(Me.cmdClientClear)
        Me.Controls.Add(Me.cmdWorkTypeAll)
        Me.Controls.Add(Me.cmdPaymentStageClear)
        Me.Controls.Add(Me.cmdPaymentStageAll)
        Me.Controls.Add(Me.dgvAddConfirmed)
        Me.Controls.Add(Me.cmdClientAll)
        Me.Controls.Add(Me.cmdStageClear)
        Me.Controls.Add(Me.cmdStageAll)
        Me.Controls.Add(Me.dgvClientName)
        Me.Controls.Add(Me.dgvStageName)
        Me.Controls.Add(Me.dgvPaymentStage)
        Me.Controls.Add(Me.cmdNumberOfVisitsAddressAll)
        Me.Controls.Add(Me.cmdCGAClear)
        Me.Controls.Add(Me.cmdCGAAll)
        Me.Controls.Add(Me.cmdNumberOfVisitsAddressClear)
        Me.Controls.Add(Me.dgvNumberOfVisitsAddress)
        Me.Controls.Add(Me.cmdAddConfirmedClear)
        Me.Controls.Add(Me.cmdAddConfirmedAll)
        Me.Controls.Add(Me.dgvSummary)
        Me.Controls.Add(Me.cmdPhoneNumberAll)
        Me.Controls.Add(Me.dgvPhoneNumber)
        Me.Controls.Add(Me.dgvCGA)
        Me.Controls.Add(Me.cmdPhoneNumberClear)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmPostEnforcementSummary"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Post Enforcement Summary"
        CType(Me.dgvLinkedPIF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvArrangementBrokenStage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvResidencyScore, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvWorkType, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAbsPerc.ResumeLayout(False)
        Me.pnlAbsPerc.PerformLayout()
        CType(Me.dgvAddConfirmed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvStageName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPaymentStage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvNumberOfVisitsAddress, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCGA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPhoneNumber, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDebtYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvInYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLinkedPayment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBalance.ResumeLayout(False)
        Me.pnlBalance.PerformLayout()
        CType(Me.dgvLastVisitBailiffName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvOutOfHoursVisit, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSchemeName, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents PostcodeArea As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmsForm As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmsList As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents WorkType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents dgvLinkedPIF As System.Windows.Forms.DataGridView
    Friend WithEvents LinkedPIF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmsSummary As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmdArrangementBrokenStageClear As System.Windows.Forms.Button
    Friend WithEvents dgvArrangementBrokenStage As System.Windows.Forms.DataGridView
    Friend WithEvents cmdArrangementBrokenStageAll As System.Windows.Forms.Button
    Friend WithEvents cmdLinkedPIFClear As System.Windows.Forms.Button
    Friend WithEvents RefreshCheckTimer As System.Windows.Forms.Timer
    Friend WithEvents cmdLinkedPIFAll As System.Windows.Forms.Button
    Friend WithEvents lblPeriodType As System.Windows.Forms.Label
    Friend WithEvents dgvResidencyScore As System.Windows.Forms.DataGridView
    Friend WithEvents cboPeriodType As System.Windows.Forms.ComboBox
    Friend WithEvents dgvPostcodeArea As System.Windows.Forms.DataGridView
    Friend WithEvents dgvWorkType As System.Windows.Forms.DataGridView
    Friend WithEvents cmdRefreshDB As System.Windows.Forms.Button
    Friend WithEvents cmdPostcodeAreaClear As System.Windows.Forms.Button
    Friend WithEvents cmdResidencyScoreClear As System.Windows.Forms.Button
    Friend WithEvents cmdPostcodeAreaAll As System.Windows.Forms.Button
    Friend WithEvents cmdWorkTypeClear As System.Windows.Forms.Button
    Friend WithEvents cmdResidencyScoreAll As System.Windows.Forms.Button
    Friend WithEvents chkTopClients As System.Windows.Forms.CheckBox
    Friend WithEvents ClientName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colStageNameTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdPaymentStageClear As System.Windows.Forms.Button
    Friend WithEvents cmdPaymentStageAll As System.Windows.Forms.Button
    Friend WithEvents colClientNameTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlAbsPerc As System.Windows.Forms.Panel
    Friend WithEvents radPerc As System.Windows.Forms.RadioButton
    Friend WithEvents radAbs As System.Windows.Forms.RadioButton
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents cmdWorkTypeAll As System.Windows.Forms.Button
    Friend WithEvents cmdClearAll As System.Windows.Forms.Button
    Friend WithEvents cmdStageClear As System.Windows.Forms.Button
    Friend WithEvents cmdStageAll As System.Windows.Forms.Button
    Friend WithEvents cmdClientClear As System.Windows.Forms.Button
    Friend WithEvents dgvAddConfirmed As System.Windows.Forms.DataGridView
    Friend WithEvents AddConfirmed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdCGAClear As System.Windows.Forms.Button
    Friend WithEvents cmdCGAAll As System.Windows.Forms.Button
    Friend WithEvents cmdNumberOfVisitsAddressAll As System.Windows.Forms.Button
    Friend WithEvents cmdClientAll As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StageName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdNumberOfVisitsAddressClear As System.Windows.Forms.Button
    Friend WithEvents dgvClientName As System.Windows.Forms.DataGridView
    Friend WithEvents dgvStageName As System.Windows.Forms.DataGridView
    Friend WithEvents cmdAddConfirmedClear As System.Windows.Forms.Button
    Friend WithEvents cmdAddConfirmedAll As System.Windows.Forms.Button
    Friend WithEvents dgvPaymentStage As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSummary As System.Windows.Forms.DataGridView
    Friend WithEvents cmdPhoneNumberAll As System.Windows.Forms.Button
    Friend WithEvents cmdPhoneNumberClear As System.Windows.Forms.Button
    Friend WithEvents dgvNumberOfVisitsAddress As System.Windows.Forms.DataGridView
    Friend WithEvents dgvCGA As System.Windows.Forms.DataGridView
    Friend WithEvents dgvPhoneNumber As System.Windows.Forms.DataGridView
    Friend WithEvents PhoneNumber As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaymentStage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ResidencyScore As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ArrangementBrokenStage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvDebtYear As System.Windows.Forms.DataGridView
    Friend WithEvents DebtYear As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdDebtYearClear As System.Windows.Forms.Button
    Friend WithEvents cmdDebtYearAll As System.Windows.Forms.Button
    Friend WithEvents cmdInYearClear As System.Windows.Forms.Button
    Friend WithEvents dgvInYear As System.Windows.Forms.DataGridView
    Friend WithEvents InYear As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdInYearAll As System.Windows.Forms.Button
    Friend WithEvents dgvLinkedPayment As System.Windows.Forms.DataGridView
    Friend WithEvents LinkedPayment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn17 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdLinkedPaymentClear As System.Windows.Forms.Button
    Friend WithEvents cmdLinkedPaymentAll As System.Windows.Forms.Button
    Friend WithEvents NumberOfVisitsAddress As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlBalance As System.Windows.Forms.Panel
    Friend WithEvents radHigh As System.Windows.Forms.RadioButton
    Friend WithEvents radLow As System.Windows.Forms.RadioButton
    Friend WithEvents cboBailiffEmp As System.Windows.Forms.ComboBox
    Friend WithEvents cmdLastVisitBailiffNameClear As System.Windows.Forms.Button
    Friend WithEvents cmdLastVisitBailiffNameAll As System.Windows.Forms.Button
    Friend WithEvents dgvLastVisitBailiffName As System.Windows.Forms.DataGridView
    Friend WithEvents LastVisitBailiffName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdOutOfHoursVisitAll As System.Windows.Forms.Button
    Friend WithEvents dgvOutOfHoursVisit As System.Windows.Forms.DataGridView
    Friend WithEvents cmdOutOfHoursVisitClear As System.Windows.Forms.Button
    Friend WithEvents OutOfHoursVisit As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdSchemeNameClear As System.Windows.Forms.Button
    Friend WithEvents dgvSchemeName As System.Windows.Forms.DataGridView
    Friend WithEvents SchemeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdSchemeNameAll As System.Windows.Forms.Button
End Class

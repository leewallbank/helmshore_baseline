﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class diaBailiff
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(diaBailiff))
        Me.dgvBailiff = New System.Windows.Forms.DataGridView()
        Me.cmsBailiff = New System.Windows.Forms.ContextMenuStrip(Me.components)
        CType(Me.dgvBailiff, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvBailiff
        '
        Me.dgvBailiff.AllowUserToAddRows = False
        Me.dgvBailiff.AllowUserToDeleteRows = False
        Me.dgvBailiff.AllowUserToResizeRows = False
        Me.dgvBailiff.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvBailiff.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBailiff.Location = New System.Drawing.Point(10, 12)
        Me.dgvBailiff.MultiSelect = False
        Me.dgvBailiff.Name = "dgvBailiff"
        Me.dgvBailiff.ReadOnly = True
        Me.dgvBailiff.RowHeadersVisible = False
        Me.dgvBailiff.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBailiff.Size = New System.Drawing.Size(484, 242)
        Me.dgvBailiff.TabIndex = 1
        '
        'cmsBailiff
        '
        Me.cmsBailiff.Name = "ContextMenuStrip1"
        Me.cmsBailiff.Size = New System.Drawing.Size(61, 4)
        '
        'diaBailiff
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(506, 266)
        Me.Controls.Add(Me.dgvBailiff)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "diaBailiff"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Bailiffs"
        CType(Me.dgvBailiff, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvBailiff As System.Windows.Forms.DataGridView
    Friend WithEvents cmsBailiff As System.Windows.Forms.ContextMenuStrip
End Class

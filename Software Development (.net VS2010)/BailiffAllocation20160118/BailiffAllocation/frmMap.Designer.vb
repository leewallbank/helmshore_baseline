﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMap
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMap))
        Me.cmdPlotRoute = New System.Windows.Forms.Button()
        Me.chkSelect = New System.Windows.Forms.CheckBox()
        Me.lstLegend = New System.Windows.Forms.ListView()
        Me.imgPushpin = New System.Windows.Forms.ImageList(Me.components)
        Me.cmsSelected = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.chkShowBailiffs = New System.Windows.Forms.CheckBox()
        Me.cmsMap = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.spltMain = New System.Windows.Forms.SplitContainer()
        Me.lstSelected = New System.Windows.Forms.ListView()
        Me.CaseRef = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Postcode = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.lblSummary = New System.Windows.Forms.Label()
        Me.mapMain = New AxMapPoint.AxMappointControl()
        CType(Me.spltMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.spltMain.Panel1.SuspendLayout()
        Me.spltMain.Panel2.SuspendLayout()
        Me.spltMain.SuspendLayout()
        CType(Me.mapMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmdPlotRoute
        '
        Me.cmdPlotRoute.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdPlotRoute.Location = New System.Drawing.Point(657, 69)
        Me.cmdPlotRoute.Name = "cmdPlotRoute"
        Me.cmdPlotRoute.Size = New System.Drawing.Size(99, 25)
        Me.cmdPlotRoute.TabIndex = 1
        Me.cmdPlotRoute.Text = "Plot Route"
        Me.cmdPlotRoute.UseVisualStyleBackColor = True
        '
        'chkSelect
        '
        Me.chkSelect.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkSelect.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkSelect.Location = New System.Drawing.Point(657, 11)
        Me.chkSelect.Name = "chkSelect"
        Me.chkSelect.Size = New System.Drawing.Size(99, 25)
        Me.chkSelect.TabIndex = 8
        Me.chkSelect.Text = "Select"
        Me.chkSelect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkSelect.UseVisualStyleBackColor = True
        '
        'lstLegend
        '
        Me.lstLegend.Alignment = System.Windows.Forms.ListViewAlignment.[Default]
        Me.lstLegend.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstLegend.CheckBoxes = True
        Me.lstLegend.FullRowSelect = True
        Me.lstLegend.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lstLegend.Location = New System.Drawing.Point(6, 12)
        Me.lstLegend.MultiSelect = False
        Me.lstLegend.Name = "lstLegend"
        Me.lstLegend.ShowGroups = False
        Me.lstLegend.Size = New System.Drawing.Size(645, 80)
        Me.lstLegend.TabIndex = 9
        Me.lstLegend.UseCompatibleStateImageBehavior = False
        '
        'imgPushpin
        '
        Me.imgPushpin.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.imgPushpin.ImageSize = New System.Drawing.Size(16, 16)
        Me.imgPushpin.TransparentColor = System.Drawing.Color.Lime
        '
        'cmsSelected
        '
        Me.cmsSelected.Name = "ContextMenuStrip1"
        Me.cmsSelected.Size = New System.Drawing.Size(61, 4)
        '
        'chkShowBailiffs
        '
        Me.chkShowBailiffs.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.chkShowBailiffs.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkShowBailiffs.Location = New System.Drawing.Point(657, 40)
        Me.chkShowBailiffs.Name = "chkShowBailiffs"
        Me.chkShowBailiffs.Size = New System.Drawing.Size(99, 25)
        Me.chkShowBailiffs.TabIndex = 11
        Me.chkShowBailiffs.Text = "Show Bailiffs"
        Me.chkShowBailiffs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkShowBailiffs.UseVisualStyleBackColor = True
        '
        'cmsMap
        '
        Me.cmsMap.Name = "ContextMenuStrip1"
        Me.cmsMap.Size = New System.Drawing.Size(61, 4)
        '
        'spltMain
        '
        Me.spltMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.spltMain.Location = New System.Drawing.Point(6, 93)
        Me.spltMain.Name = "spltMain"
        '
        'spltMain.Panel1
        '
        Me.spltMain.Panel1.Controls.Add(Me.lstSelected)
        Me.spltMain.Panel1.Controls.Add(Me.lblSummary)
        '
        'spltMain.Panel2
        '
        Me.spltMain.Panel2.Controls.Add(Me.mapMain)
        Me.spltMain.Size = New System.Drawing.Size(748, 275)
        Me.spltMain.SplitterDistance = 138
        Me.spltMain.TabIndex = 14
        '
        'lstSelected
        '
        Me.lstSelected.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstSelected.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.CaseRef, Me.Postcode})
        Me.lstSelected.FullRowSelect = True
        Me.lstSelected.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lstSelected.Location = New System.Drawing.Point(3, 18)
        Me.lstSelected.Name = "lstSelected"
        Me.lstSelected.Scrollable = False
        Me.lstSelected.Size = New System.Drawing.Size(134, 254)
        Me.lstSelected.TabIndex = 15
        Me.lstSelected.UseCompatibleStateImageBehavior = False
        Me.lstSelected.View = System.Windows.Forms.View.Details
        '
        'CaseRef
        '
        Me.CaseRef.Text = "Case ID"
        '
        'Postcode
        '
        Me.Postcode.Text = "Postcode"
        Me.Postcode.Width = 70
        '
        'lblSummary
        '
        Me.lblSummary.AutoSize = True
        Me.lblSummary.Location = New System.Drawing.Point(6, 2)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(39, 13)
        Me.lblSummary.TabIndex = 14
        Me.lblSummary.Text = "Label1"
        '
        'mapMain
        '
        Me.mapMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.mapMain.Enabled = True
        Me.mapMain.Location = New System.Drawing.Point(1, 1)
        Me.mapMain.Name = "mapMain"
        Me.mapMain.OcxState = CType(resources.GetObject("mapMain.OcxState"), System.Windows.Forms.AxHost.State)
        Me.mapMain.Size = New System.Drawing.Size(602, 271)
        Me.mapMain.TabIndex = 13
        '
        'frmMap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(761, 370)
        Me.Controls.Add(Me.spltMain)
        Me.Controls.Add(Me.chkShowBailiffs)
        Me.Controls.Add(Me.lstLegend)
        Me.Controls.Add(Me.chkSelect)
        Me.Controls.Add(Me.cmdPlotRoute)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMap"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Map"
        Me.spltMain.Panel1.ResumeLayout(False)
        Me.spltMain.Panel1.PerformLayout()
        Me.spltMain.Panel2.ResumeLayout(False)
        CType(Me.spltMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.spltMain.ResumeLayout(False)
        CType(Me.mapMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdPlotRoute As System.Windows.Forms.Button
    Friend WithEvents chkSelect As System.Windows.Forms.CheckBox
    Friend WithEvents lstLegend As System.Windows.Forms.ListView
    Friend WithEvents imgPushpin As System.Windows.Forms.ImageList
    Friend WithEvents cmsSelected As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents chkShowBailiffs As System.Windows.Forms.CheckBox
    Friend WithEvents cmsMap As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents spltMain As System.Windows.Forms.SplitContainer
    Friend WithEvents lstSelected As System.Windows.Forms.ListView
    Friend WithEvents CaseRef As System.Windows.Forms.ColumnHeader
    Friend WithEvents Postcode As System.Windows.Forms.ColumnHeader
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents mapMain As AxMapPoint.AxMappointControl

End Class

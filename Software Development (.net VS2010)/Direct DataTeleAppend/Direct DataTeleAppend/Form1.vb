﻿Imports CommonLibrary
Imports System.IO
Public Class Form1

    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click
        Dim FileDialog As New OpenFileDialog
        Dim outfile As String = ""
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        Dim inputfilepath, filename, fileext As String
        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)
        Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
        Dim lastRow As Integer = UBound(FileContents)
        Dim rowIDX As Integer
        Dim InputLineArray() As String
        Dim inputline As String = ""
        For rowIDX = 0 To lastRow
            Try
                ProgressBar1.Value = (rowIDX / lastRow) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            inputline = FileContents(rowIDX)
            InputLineArray = inputline.Split(",")
            'debtorID is in first column
            Dim debtorID As Integer = InputLineArray(0)
            'phone number in K col
            Dim phoneNumber As String = InputLineArray(10)
            Dim dt As New DataTable
            LoadDataTable("DebtRecovery", "SELECT linkID " & _
                                                    "FROM debtor " & _
                                                    "WHERE _rowid = " & debtorID, dt, False)
            Dim row As DataRow
            For Each row In dt.Rows
                Dim linkID As Integer
                Try
                    linkID = row.Item(0)
                Catch ex As Exception
                    Continue For
                End Try

                'get all open cases for this linkID
                Dim dt2 As New DataTable
                LoadDataTable("DebtRecovery", "SELECT _rowid " & _
                                                        "FROM debtor " & _
                                                        " WHERE linkId = " & linkID & _
                                                        " AND status_open_closed = 'O'" & _
                                                        " AND _rowid <> " & debtorID, dt2, False)
                Dim linkRow As DataRow
                For Each linkRow In dt2.Rows
                    Dim linkDebtorID As Integer = linkRow.Item(0)
                    outfile &= linkDebtorID & "," & phoneNumber & vbNewLine
                Next
            Next
        Next

        WriteFile(inputfilepath & filename & "_links.txt", outfile)
        MsgBox("Links file written")
        Me.Close()
    End Sub
End Class

﻿Imports CommonLibrary
Imports System.IO

Public Class Form1
    Dim start_date As Date
    Dim end_date As Date
    Dim filename As String = ""
    Dim filepath As String = ""
    Private Sub runbtn_Click(sender As System.Object, e As System.EventArgs) Handles runbtn.Click
        Dim password As String
        password = InputBox("Enter password", "Enter Password")
        If password <> "SWIFT2138" Then
            MsgBox("Invalid password")
            Me.Close()
            Exit Sub
        End If
        runbtn.Enabled = False
        statuslbl.Text = "Starting report"
        run_report()
        
        Me.Close()
    End Sub
    Private Sub run_report()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2138Sreport = New RA2138S
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_date)
        SetCurrentValuesForParameterField1(RA2138Sreport, myArrayList1)
        myArrayList1.Add(end_date)
        SetCurrentValuesForParameterField2(RA2138Sreport, myArrayList1)
        myConnectionInfo.ServerName = "SwiftRemuneration"
        myConnectionInfo.DatabaseName = "SwiftRemuneration"
        myConnectionInfo.UserID = "sa"
        myConnectionInfo.Password = "sa"
        myConnectionInfo2.ServerName = "DebtRecovery"
        myConnectionInfo2.DatabaseName = "DebtRecovery"
        myConnectionInfo2.UserID = "vbnet"
        myConnectionInfo2.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2138Sreport, myConnectionInfo2)
        filename = "RA2138S All Swift EA Invoices.pdf"
        Dim savefiledialog1 As New SaveFileDialog
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = filename
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            statuslbl.Text = "Running report ... please wait "
            filename = SaveFileDialog1.FileName
            MsgBox("Saving file to " & SaveFileDialog1.FileName)
            RA2138Sreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, SaveFileDialog1.FileName)
            RA2138Sreport.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If

       
    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        end_date = DateAdd(DateInterval.Day, -Weekday(Now) + 1, Now)
        end_date = CDate(Format(end_date, "MMM yyyy dd") & " 00:00:00")
        start_date = DateAdd(DateInterval.Day, -6, end_date)
        start_date = CDate(Format(start_date, "MMM yyyy dd") & " 00:00:00")
        start_dtp.Value = start_date
        end_dtp.Value = end_date
    End Sub
End Class

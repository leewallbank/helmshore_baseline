﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.lblReadingFile = New System.Windows.Forms.Label()
        Me.btnViewLogFile = New System.Windows.Forms.Button()
        Me.btnViewOutputFile = New System.Windows.Forms.Button()
        Me.btnViewInputFile = New System.Windows.Forms.Button()
        Me.btnProcessFile = New System.Windows.Forms.Button()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.btnViewOutputFil = New System.Windows.Forms.Button()
        Me.btnReconcile = New System.Windows.Forms.Button()
        Me.rtxtErrorLog = New System.Windows.Forms.RichTextBox()
        Me.txtMaxLinesInOutputFile = New System.Windows.Forms.TextBox()
        Me.txtBatchRejectPercentageThreshold = New System.Windows.Forms.TextBox()
        Me.txtMaxDebtLevel = New System.Windows.Forms.TextBox()
        Me.txtMinDebtLevel = New System.Windows.Forms.TextBox()
        Me.lblOutputLines = New System.Windows.Forms.Label()
        Me.lblRejectionThreshold = New System.Windows.Forms.Label()
        Me.lblMinDebt = New System.Windows.Forms.Label()
        Me.lblMaximumDebt = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblReadingFile
        '
        Me.lblReadingFile.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblReadingFile.BackColor = System.Drawing.Color.White
        Me.lblReadingFile.Location = New System.Drawing.Point(283, 554)
        Me.lblReadingFile.Name = "lblReadingFile"
        Me.lblReadingFile.Size = New System.Drawing.Size(416, 13)
        Me.lblReadingFile.TabIndex = 30
        Me.lblReadingFile.Text = "Reading file..."
        Me.lblReadingFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblReadingFile.Visible = False
        '
        'btnViewLogFile
        '
        Me.btnViewLogFile.Enabled = False
        Me.btnViewLogFile.Location = New System.Drawing.Point(10, 156)
        Me.btnViewLogFile.Name = "btnViewLogFile"
        Me.btnViewLogFile.Size = New System.Drawing.Size(192, 42)
        Me.btnViewLogFile.TabIndex = 29
        Me.btnViewLogFile.Text = "View &Log File"
        Me.btnViewLogFile.UseVisualStyleBackColor = True
        '
        'btnViewOutputFile
        '
        Me.btnViewOutputFile.Enabled = False
        Me.btnViewOutputFile.Location = New System.Drawing.Point(10, 108)
        Me.btnViewOutputFile.Name = "btnViewOutputFile"
        Me.btnViewOutputFile.Size = New System.Drawing.Size(192, 42)
        Me.btnViewOutputFile.TabIndex = 28
        Me.btnViewOutputFile.Text = "View &Output File"
        Me.btnViewOutputFile.UseVisualStyleBackColor = True
        '
        'btnViewInputFile
        '
        Me.btnViewInputFile.Enabled = False
        Me.btnViewInputFile.Location = New System.Drawing.Point(10, 60)
        Me.btnViewInputFile.Name = "btnViewInputFile"
        Me.btnViewInputFile.Size = New System.Drawing.Size(192, 42)
        Me.btnViewInputFile.TabIndex = 26
        Me.btnViewInputFile.Text = "View &Input File"
        Me.btnViewInputFile.UseVisualStyleBackColor = True
        '
        'btnProcessFile
        '
        Me.btnProcessFile.Location = New System.Drawing.Point(10, 12)
        Me.btnProcessFile.Name = "btnProcessFile"
        Me.btnProcessFile.Size = New System.Drawing.Size(192, 42)
        Me.btnProcessFile.TabIndex = 24
        Me.btnProcessFile.Text = "&Process File"
        Me.btnProcessFile.UseVisualStyleBackColor = True
        '
        'ProgressBar
        '
        Me.ProgressBar.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock
        Me.ProgressBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProgressBar.Location = New System.Drawing.Point(10, 549)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(962, 22)
        Me.ProgressBar.TabIndex = 25
        '
        'btnViewOutputFil
        '
        Me.btnViewOutputFil.Enabled = False
        Me.btnViewOutputFil.Location = New System.Drawing.Point(10, 108)
        Me.btnViewOutputFil.Name = "btnViewOutputFil"
        Me.btnViewOutputFil.Size = New System.Drawing.Size(192, 42)
        Me.btnViewOutputFil.TabIndex = 27
        Me.btnViewOutputFil.Text = "View &Output File"
        Me.btnViewOutputFil.UseVisualStyleBackColor = True
        '
        'btnReconcile
        '
        Me.btnReconcile.Location = New System.Drawing.Point(10, 204)
        Me.btnReconcile.Name = "btnReconcile"
        Me.btnReconcile.Size = New System.Drawing.Size(192, 42)
        Me.btnReconcile.TabIndex = 31
        Me.btnReconcile.Text = "&Reconcile"
        Me.btnReconcile.UseVisualStyleBackColor = True
        '
        'rtxtErrorLog
        '
        Me.rtxtErrorLog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtxtErrorLog.Location = New System.Drawing.Point(208, 12)
        Me.rtxtErrorLog.Name = "rtxtErrorLog"
        Me.rtxtErrorLog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.rtxtErrorLog.Size = New System.Drawing.Size(764, 531)
        Me.rtxtErrorLog.TabIndex = 32
        Me.rtxtErrorLog.Text = ""
        '
        'txtMaxLinesInOutputFile
        '
        Me.txtMaxLinesInOutputFile.Location = New System.Drawing.Point(125, 252)
        Me.txtMaxLinesInOutputFile.Name = "txtMaxLinesInOutputFile"
        Me.txtMaxLinesInOutputFile.Size = New System.Drawing.Size(77, 20)
        Me.txtMaxLinesInOutputFile.TabIndex = 33
        '
        'txtBatchRejectPercentageThreshold
        '
        Me.txtBatchRejectPercentageThreshold.Location = New System.Drawing.Point(125, 278)
        Me.txtBatchRejectPercentageThreshold.Name = "txtBatchRejectPercentageThreshold"
        Me.txtBatchRejectPercentageThreshold.Size = New System.Drawing.Size(77, 20)
        Me.txtBatchRejectPercentageThreshold.TabIndex = 34
        '
        'txtMaxDebtLevel
        '
        Me.txtMaxDebtLevel.Location = New System.Drawing.Point(125, 330)
        Me.txtMaxDebtLevel.Name = "txtMaxDebtLevel"
        Me.txtMaxDebtLevel.Size = New System.Drawing.Size(77, 20)
        Me.txtMaxDebtLevel.TabIndex = 35
        '
        'txtMinDebtLevel
        '
        Me.txtMinDebtLevel.Location = New System.Drawing.Point(125, 304)
        Me.txtMinDebtLevel.Name = "txtMinDebtLevel"
        Me.txtMinDebtLevel.Size = New System.Drawing.Size(77, 20)
        Me.txtMinDebtLevel.TabIndex = 36
        '
        'lblOutputLines
        '
        Me.lblOutputLines.AutoSize = True
        Me.lblOutputLines.Location = New System.Drawing.Point(9, 255)
        Me.lblOutputLines.Name = "lblOutputLines"
        Me.lblOutputLines.Size = New System.Drawing.Size(111, 13)
        Me.lblOutputLines.TabIndex = 37
        Me.lblOutputLines.Text = "Max num lines per file:"
        Me.lblOutputLines.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblRejectionThreshold
        '
        Me.lblRejectionThreshold.AutoSize = True
        Me.lblRejectionThreshold.Location = New System.Drawing.Point(15, 281)
        Me.lblRejectionThreshold.Name = "lblRejectionThreshold"
        Me.lblRejectionThreshold.Size = New System.Drawing.Size(105, 13)
        Me.lblRejectionThreshold.TabIndex = 38
        Me.lblRejectionThreshold.Text = "Batch rejection perc:"
        Me.lblRejectionThreshold.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMinDebt
        '
        Me.lblMinDebt.AutoSize = True
        Me.lblMinDebt.Location = New System.Drawing.Point(19, 307)
        Me.lblMinDebt.Name = "lblMinDebt"
        Me.lblMinDebt.Size = New System.Drawing.Size(101, 13)
        Me.lblMinDebt.TabIndex = 39
        Me.lblMinDebt.Text = "Minimum case debt:"
        Me.lblMinDebt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblMaximumDebt
        '
        Me.lblMaximumDebt.AutoSize = True
        Me.lblMaximumDebt.Location = New System.Drawing.Point(16, 333)
        Me.lblMaximumDebt.Name = "lblMaximumDebt"
        Me.lblMaximumDebt.Size = New System.Drawing.Size(104, 13)
        Me.lblMaximumDebt.TabIndex = 40
        Me.lblMaximumDebt.Text = "Maximum case debt:"
        Me.lblMaximumDebt.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 578)
        Me.Controls.Add(Me.lblMaximumDebt)
        Me.Controls.Add(Me.lblMinDebt)
        Me.Controls.Add(Me.lblRejectionThreshold)
        Me.Controls.Add(Me.lblOutputLines)
        Me.Controls.Add(Me.txtMinDebtLevel)
        Me.Controls.Add(Me.txtMaxDebtLevel)
        Me.Controls.Add(Me.txtBatchRejectPercentageThreshold)
        Me.Controls.Add(Me.txtMaxLinesInOutputFile)
        Me.Controls.Add(Me.rtxtErrorLog)
        Me.Controls.Add(Me.btnReconcile)
        Me.Controls.Add(Me.lblReadingFile)
        Me.Controls.Add(Me.btnViewLogFile)
        Me.Controls.Add(Me.btnViewOutputFile)
        Me.Controls.Add(Me.btnViewInputFile)
        Me.Controls.Add(Me.btnProcessFile)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.btnViewOutputFil)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(500, 425)
        Me.Name = "frmMain"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "HMRC Tax Credits Dual Liability Pre Process"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblReadingFile As System.Windows.Forms.Label
    Friend WithEvents btnViewLogFile As System.Windows.Forms.Button
    Friend WithEvents btnViewOutputFile As System.Windows.Forms.Button
    Friend WithEvents btnViewInputFile As System.Windows.Forms.Button
    Friend WithEvents btnProcessFile As System.Windows.Forms.Button
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents btnViewOutputFil As System.Windows.Forms.Button
    Friend WithEvents btnReconcile As System.Windows.Forms.Button
    Friend WithEvents rtxtErrorLog As System.Windows.Forms.RichTextBox
    Friend WithEvents txtMaxLinesInOutputFile As System.Windows.Forms.TextBox
    Friend WithEvents txtBatchRejectPercentageThreshold As System.Windows.Forms.TextBox
    Friend WithEvents txtMaxDebtLevel As System.Windows.Forms.TextBox
    Friend WithEvents txtMinDebtLevel As System.Windows.Forms.TextBox
    Friend WithEvents lblOutputLines As System.Windows.Forms.Label
    Friend WithEvents lblRejectionThreshold As System.Windows.Forms.Label
    Friend WithEvents lblMinDebt As System.Windows.Forms.Label
    Friend WithEvents lblMaximumDebt As System.Windows.Forms.Label

End Class

﻿Imports CommonLibrary
Imports System.IO
Imports System.Text.RegularExpressions

Public Class frmMain
    Private HMRCData As New clsHMRCData
    Private InputFilePath As String, VersionNum As String, FileName As String, FileExt As String, ErrorLog As String, OutputHeader As String, FileGenerationReference As String, FileType As String
    Private FileContents(,) As String
    Private TotalErrorCasesCount As Integer, RowNum As Integer
    Public TotalLoadedCasesCount As Integer
    Private TotalErrorWorkItemsCount As Integer
    Private TotalErrorCasesValue As Decimal
    Public TotalLoadedCasesValue As Decimal
    Private OutputFiles As New ArrayList()
    Private RowValid As Boolean, FileValid As Boolean

    ' File attributes
    Private Const NumOfFixedCols As Integer = 48
    Private Const FirstDataRowPos As Integer = 3 ' zero based
    Private Const NumOfWorkFlowItemCols As Integer = 5
    Private Const Separator As String = "|"

    ' Column positions - all zero based
    Private Const HouseholdRefPos As Integer = 0
    Private Const HODPos As Integer = 1
    Private Const Claim1NINOPos As Integer = 2
    Private Const Claim1DOBPos As Integer = 3
    Private Const Claim1TaxPayerNameTitlePos As Integer = 4
    Private Const Claim1TaxPayerNameLine1Pos As Integer = 5
    Private Const Claim1TaxPayerNameLine2Pos As Integer = 6
    Private Const Claim1AddressLine1Pos As Integer = 7
    Private Const Claim1AddressLine2Pos As Integer = 8
    Private Const Claim1AddressLine3Pos As Integer = 9
    Private Const Claim1AddressLine4Pos As Integer = 10
    Private Const Claim1PostCodePos As Integer = 11
    Private Const Claim1TelNumPos As Integer = 12
    Private Const Claim1Alt1Pos As Integer = 13
    Private Const Claim1Alt2Pos As Integer = 14
    Private Const Claim1RecovNameLine1Pos As Integer = 15
    Private Const Claim1RecovNameLine2Pos As Integer = 16
    Private Const Claim1RecovAddressLine1Pos As Integer = 17
    Private Const Claim1RecovAddressLine2Pos As Integer = 18
    Private Const Claim1RecovAddressLine3Pos As Integer = 19
    Private Const Claim1RecovAddressLine4Pos As Integer = 20
    Private Const Claim1RecovPostCodePos As Integer = 21
    Private Const Claim1RecovTelNumPos As Integer = 22
    Private Const Claim1BusTelNumPos As Integer = 23
    Private Const Claim2NINOPos As Integer = 24
    Private Const Claim2DOBPos As Integer = 25
    Private Const Claim2TaxPayerNameTitlePos As Integer = 26
    Private Const Claim2TaxPayerNameLine1Pos As Integer = 27
    Private Const Claim2TaxPayerNameLine2Pos As Integer = 28
    Private Const Claim2AddressLine1Pos As Integer = 29
    Private Const Claim2AddressLine2Pos As Integer = 30
    Private Const Claim2AddressLine3Pos As Integer = 31
    Private Const Claim2AddressLine4Pos As Integer = 32
    Private Const Claim2PostCodePos As Integer = 33
    Private Const Claim2TelNumPos As Integer = 34
    Private Const Claim2Alt1Pos As Integer = 35
    Private Const Claim2Alt2Pos As Integer = 36
    Private Const Claim2RecovNameLine1Pos As Integer = 37
    Private Const Claim2RecovNameLine2Pos As Integer = 38
    Private Const Claim2RecovAddressLine1Pos As Integer = 39
    Private Const Claim2RecovAddressLine2Pos As Integer = 40
    Private Const Claim2RecovAddressLine3Pos As Integer = 41
    Private Const Claim2RecovAddressLine4Pos As Integer = 42
    Private Const Claim2RecovPostCodePos As Integer = 43
    Private Const Claim2RecovTelNumPos As Integer = 44
    Private Const Claim2BusTelNumPos As Integer = 45
    Private Const TotalDebtPos As Integer = 46
    Private Const NumWorkItemsPos As Integer = 47

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        btnViewInputFile.Enabled = False
        btnViewLogFile.Enabled = False
        btnViewOutputFile.Enabled = False
        btnReconcile.Enabled = False

        txtMaxLinesInOutputFile.Text = My.Settings.MaxLinesInOutputFile.ToString
        txtBatchRejectPercentageThreshold.Text = My.Settings.BatchRejectPercentageThreshold.ToString
        txtMinDebtLevel.Text = My.Settings.MinDebtLevel.ToString
        txtMaxDebtLevel.Text = My.Settings.MaxDebtLevel.ToString

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim FileNum As Integer, DebtColsStart As Integer, TotalValidCasesCount As Integer, TotalValidWorkItemsCount As Integer, CaseWorkItemsCount As Integer
            Dim NewDebtNotes As String = "", NewWorkItemNotes As String = "", NewDebtLine(NumOfFixedCols) As String
            Dim AuditLog As String
            Dim CaseBalance As Decimal, TotalBalance As Decimal, RejectionPerc As Decimal
            Dim NewDebtFile(0) As String, RowError(1) As String, InputHeader() As String = Nothing
            Dim NewDebtSumm(2, 0) As Decimal ' 0 Case count, 1 Work items count, 2 Total balance

            btnViewInputFile.Enabled = False
            btnViewLogFile.Enabled = False
            btnViewOutputFile.Enabled = False
            btnReconcile.Enabled = False

            rtxtErrorLog.Text = ""
            ErrorLog = ""
            TotalErrorCasesCount = 0
            TotalErrorWorkItemsCount = 0
            TotalErrorCasesValue = 0
            RowNum = 0
            TotalLoadedCasesCount = 0
            TotalLoadedCasesValue = 0
            OutputFiles.Clear()
            FileType = ""

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            If Not MsgBox("Selected file " & FileDialog.FileName, MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton2, Me.Text) = MsgBoxResult.Ok Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            For Each OldFile As String In Directory.GetFiles(InputFilePath, FileName & "_*.txt")
                File.Delete(OldFile)
            Next OldFile

            lblReadingFile.Visible = True
            Application.DoEvents()
            FileContents = InputFromExcel(FileDialog.FileName)

            FileValid = True

            ' Validate the number of columns
            If UBound(FileContents, 2) < NumOfFixedCols - 1 Then AddToErrorLog("Insufficient number of columns (" & (UBound(FileContents, 2) + 1).ToString & ") found at row no. " & (RowNum + FirstDataRowPos).ToString & ".", False)

            If FileValid AndAlso Not ValidHeaders() Then
                AddToErrorLog("Invalid spreadsheet column headers", False)
                lblReadingFile.Visible = False
            Else
                NewDebtFile(0) = "BatchNumber" & Separator & "SequenceNumber" & Separator & OutputHeader & vbCrLf
                OutputFiles.Add(InputFilePath & FileName & "_001_PreProcessed.txt")
                lblReadingFile.Visible = False

                ProgressBar.Maximum = UBound(FileContents) + 2 - (FirstDataRowPos + 1) ' plus audit file and output file minus header and trailer rows

                ' Validate header
                InputHeader = FileContents(0, 0).Split(CChar(","))
                If UBound(InputHeader) = 1 And InputHeader(0) = "H" Then
                    FileGenerationReference = InputHeader(1)

                    Dim HeaderDetail() As String = InputHeader(1).Split(CChar("_"))
                    If UBound(HeaderDetail) <> 2 Then
                        AddToErrorLog("Invalid header structure.", False)
                    ElseIf HeaderDetail(0) <> "ROSS" Then
                        AddToErrorLog("Invalid DCA identifer - " & HeaderDetail(0) & ".", False)
                    ElseIf Not Regex.IsMatch(HeaderDetail(1), "^[0-9]{4}$") Then
                        AddToErrorLog("Invalid version number - " & HeaderDetail(1) & ".", False)
                    ElseIf Not Regex.IsMatch(HeaderDetail(2), "^[0-9]{4}$") OrElse CInt(HeaderDetail(2)) = 0 Then
                        AddToErrorLog("Invalid sequence number - " & HeaderDetail(2) & ".", False)
                    End If
                    If FileValid Then VersionNum = HeaderDetail(1)
                Else
                    AddToErrorLog("Invalid header structure.", False)
                End If

                If FileValid And FileGenerationReferenceExists() Then
                    If MsgBox("Batch " & FileGenerationReference & " has already been pre processed. Overwrite?", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, Me.Text) = MsgBoxResult.Yes Then
                        HMRCData.ClearBatch(FileGenerationReference)
                    Else
                        AddToErrorLog("Batch already loaded", False)
                    End If
                End If

                If FileValid Then

                    For Me.RowNum = FirstDataRowPos To UBound(FileContents) - 1 ' skip header and trailer lines
                        Application.DoEvents() ' without this line, the button disappears until processing is complete
                        ProgressBar.Value = RowNum - FirstDataRowPos

                        RowValid = True
                        CaseBalance = 0
                        CaseWorkItemsCount = 0
                        Array.Clear(NewDebtLine, 0, NewDebtLine.Length)
                        NewWorkItemNotes = ""
                        RowError = Nothing

                        ' Check valid work item make up
                        If (UBound(FileContents, 2) - (NumOfFixedCols - 1)) Mod NumOfWorkFlowItemCols <> 0 Then AddToErrorLog("Incomplete number of work items columns found at row no. " & (RowNum + 1).ToString & ". Row not loaded.", True)

                        If RowValid Then
                            'Validate fixed columns
                            For ColNum As Integer = 0 To NumOfFixedCols - 1
                                If Not RowError Is Nothing Then Exit For
                                Select Case ColNum

                                    Case HouseholdRefPos
                                        If String.IsNullOrEmpty(FileContents(RowNum, ColNum)) OrElse FileContents(RowNum, ColNum).Length <> 33 Then RowError = {"1", "Invalid Customer Ref, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                        If RowError Is Nothing AndAlso Not (IsPaymentRef("N", FileContents(RowNum, ColNum).Substring(0, 16)) And FileContents(RowNum, ColNum).Substring(16, 1) = "/" And IsPaymentRef("N", FileContents(RowNum, ColNum).Substring(17, 16))) Then RowError = {"1", "Invalid Customer Ref, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                        'If RowError Is Nothing AndAlso Not (IsPaymentRef(FileContents(RowNum, HODPos), FileContents(RowNum, ColNum).Substring(0, 16)) And FileContents(RowNum, HouseholdRefPos).Substring(16, 1) = "/" And IsPaymentRef(FileContents(RowNum, HODPos), FileContents(RowNum, ColNum).Substring(17, 16))) Then RowError = {"1", "Invalid Customer Ref, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}

                                    Case HODPos
                                        If String.IsNullOrEmpty(FileContents(RowNum, ColNum)) Then RowError = {"2", "Invalid HOD, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                        If FileValid And FileType = "" Then FileType = HOD(FileContents(RowNum, HODPos)) ' This should always be the first data row. Coded this way to cater for the first data row being invalid

                                    Case Claim1NINOPos
                                        If Not IsNINumber(FileContents(RowNum, ColNum)) Then RowError = {-1, "Invalid NI no., column No. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}

                                    Case Claim1DOBPos
                                        If Not String.IsNullOrEmpty(FileContents(RowNum, ColNum)) Then
                                            If IsNumeric(FileContents(RowNum, ColNum)) Then
                                                If CDec(FileContents(RowNum, ColNum)) > 0 Then
                                                    If DateTime.Today.Year - DateTime.FromOADate(CInt(FileContents(RowNum, ColNum))).Year < 16 Then RowError = {"0", "Debtor < 16 years of age, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                                Else
                                                    RowError = {"0", "Invalid DOB, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                                End If
                                            End If
                                            If IsDate(FileContents(RowNum, ColNum)) Then
                                                If DateTime.Today.Year - CDate(FileContents(RowNum, ColNum)).Year < 16 Then RowError = {"0", "Debtor < 16 years of age, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                            Else
                                                RowError = {"0", "Invalid DOB, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                            End If
                                        End If

                                    Case Claim1TaxPayerNameLine1Pos
                                        If String.IsNullOrEmpty(FileContents(RowNum, ColNum)) Then RowError = {"4", "Invalid TaxPayerNameLine1, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}

                                    Case Claim1AddressLine1Pos
                                        If String.IsNullOrEmpty(FileContents(RowNum, ColNum)) Then RowError = {"5", "Invalid AddressNameLine1, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}

                                    Case Claim1TelNumPos, Claim1Alt1Pos, Claim1Alt2Pos
                                        If Not String.IsNullOrEmpty(FileContents(RowNum, ColNum)) AndAlso Not IsTelNumber(FileContents(RowNum, ColNum)) Then RowError = {"-1", "Invalid claimant 1 contact details, row no. = " & (RowNum + 1).ToString}

                                    Case Claim2NINOPos
                                        If Not IsNINumber(FileContents(RowNum, ColNum)) Then RowError = {-1, "Invalid NI no., column No. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}

                                    Case Claim2DOBPos
                                        If Not String.IsNullOrEmpty(FileContents(RowNum, ColNum)) Then
                                            If IsNumeric(FileContents(RowNum, ColNum)) Then
                                                If CDec(FileContents(RowNum, ColNum)) > 0 Then
                                                    If DateTime.Today.Year - DateTime.FromOADate(CInt(FileContents(RowNum, ColNum))).Year < 16 Then RowError = {"0", "Debtor < 16 years of age, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                                Else
                                                    RowError = {"0", "Invalid DOB, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                                End If
                                            End If
                                            If IsDate(FileContents(RowNum, ColNum)) Then
                                                If DateTime.Today.Year - CDate(FileContents(RowNum, ColNum)).Year < 16 Then RowError = {"0", "Debtor < 16 years of age, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                            Else
                                                RowError = {"0", "Invalid DOB, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                            End If
                                        End If

                                    Case Claim2TaxPayerNameLine1Pos
                                        If String.IsNullOrEmpty(FileContents(RowNum, ColNum)) Then RowError = {"4", "Invalid TaxPayerNameLine1, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}

                                    Case Claim2AddressLine1Pos
                                        If String.IsNullOrEmpty(FileContents(RowNum, ColNum)) Then RowError = {"5", "Invalid AddressNameLine1, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}

                                    Case Claim2TelNumPos, Claim2Alt1Pos, Claim2Alt2Pos
                                        If Not String.IsNullOrEmpty(FileContents(RowNum, ColNum)) AndAlso Not IsTelNumber(FileContents(RowNum, ColNum)) Then RowError = {"-1", "Invalid claimant 2 contact details, row no. = " & (RowNum + 1).ToString}

                                    Case TotalDebtPos
                                        If String.IsNullOrEmpty(FileContents(RowNum, ColNum)) Then RowError = {"6", "No outstanding debt, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                        If RowError Is Nothing AndAlso Not IsNumeric(FileContents(RowNum, ColNum)) Then RowError = {"6", "Invalid outstanding debt, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                        If RowError Is Nothing AndAlso (CDec(FileContents(RowNum, ColNum)) < My.Settings.MinDebtLevel) Then RowError = {"6", "Outstanding debt out of specified range, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                        If RowError Is Nothing AndAlso (CDec(FileContents(RowNum, ColNum)) > My.Settings.MaxDebtLevel) Then RowError = {"6", "Outstanding debt out of specified range, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}

                                    Case NumWorkItemsPos
                                        If String.IsNullOrEmpty(FileContents(RowNum, ColNum)) Then RowError = {"7", "No number of workitems, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                        If RowError Is Nothing AndAlso Not IsNumeric(FileContents(RowNum, ColNum)) Then RowError = {"7", "Invalid number of work items, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                        If RowError Is Nothing AndAlso CInt(FileContents(RowNum, ColNum)) <= 0 Then RowError = {"7", "Invalid number of work items, column no. = " & (ColNum + 1).ToString & ", row no. = " & (RowNum + 1).ToString}

                                End Select
                            Next ColNum

                            ' Validate where at least one field is populated in a collection
                            ' Claimant 1 address lines 2, 3, 4 and postcode
                            If RowError Is Nothing And Join({FileContents(RowNum, Claim1AddressLine2Pos), FileContents(RowNum, Claim1AddressLine3Pos), FileContents(RowNum, Claim1AddressLine4Pos), FileContents(RowNum, Claim1PostCodePos)}, "").Length = 0 Then RowError = {"5", "Invalid claimant 1 address, row no. = " & (RowNum + 1).ToString}
                            ' Claimant 2 address lines 2, 3, 4 and postcode
                            If RowError Is Nothing And Join({FileContents(RowNum, Claim2AddressLine2Pos), FileContents(RowNum, Claim2AddressLine3Pos), FileContents(RowNum, Claim2AddressLine4Pos), FileContents(RowNum, Claim2PostCodePos)}, "").Length = 0 Then RowError = {"5", "Invalid claimant 1 address, row no. = " & (RowNum + 1).ToString}

                            ' Work through and build notes for work items, validating at the same time. Notes are built regardless of validation for speed.
                            For WorkItemNum As Integer = 1 To CInt((UBound(FileContents, 2) - NumOfFixedCols) / NumOfWorkFlowItemCols)
                                DebtColsStart = NumOfFixedCols + ((WorkItemNum - 1) * NumOfWorkFlowItemCols)
                                If FileContents(RowNum, DebtColsStart) = "" Then Exit For

                                CaseWorkItemsCount += 1

                                For WorkItemCol As Integer = 0 To NumOfWorkFlowItemCols - 1 ' loop through the five work item columns and add to notes
                                    ' Exit loop if errors found. No point in continuing as the note will not be added to the case
                                    If Not RowError Is Nothing Then Exit For

                                    ' Start validating
                                    If WorkItemCol = 0 And String.IsNullOrEmpty(FileContents(RowNum, DebtColsStart + WorkItemCol)) Then RowError = {"-1", "Tax year = NULL, column no. = " & (DebtColsStart + WorkItemCol + 1).ToString & ", row no. = " & (RowNum + 1).ToString}

                                    If WorkItemCol = 2 Then
                                        ' Validate collectable amount
                                        If String.IsNullOrEmpty(FileContents(RowNum, DebtColsStart + WorkItemCol)) Then RowError = {"8", "Collectable amount of work item = NULL, column no. = " & (DebtColsStart + WorkItemCol + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                        If RowError Is Nothing AndAlso Not IsNumeric(FileContents(RowNum, DebtColsStart + WorkItemCol)) Then RowError = {"9", "Invalid Currency Value, column no. = " & (DebtColsStart + WorkItemCol + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                        If RowError Is Nothing AndAlso CDec(FileContents(RowNum, DebtColsStart + WorkItemCol)) <= 0 Then RowError = {"8", "Collectable amount of work item < = 0, column no. = " & (DebtColsStart + WorkItemCol + 1).ToString & ", row no. = " & (RowNum + 1).ToString}
                                        If RowError Is Nothing Then CaseBalance += CDec(FileContents(RowNum, DebtColsStart + 2))
                                    End If

                                    ' Validate payment refs
                                    If WorkItemCol >= 3 And Not IsPaymentRef(FileContents(RowNum, HODPos), FileContents(RowNum, DebtColsStart + WorkItemCol)) Then RowError = {"3", "Invalid " & FileContents(FirstDataRowPos - 1, DebtColsStart + WorkItemCol)}

                                    ' Build up notes 
                                    NewWorkItemNotes &= String.Join("", ToNote(" " & FileContents(RowNum, DebtColsStart + WorkItemCol), FileContents(FirstDataRowPos - 1, DebtColsStart + WorkItemCol), ";"))

                                Next WorkItemCol
                            Next WorkItemNum

                            If RowError Is Nothing AndAlso Not IsNumeric(FileContents(RowNum, TotalDebtPos)) Then RowError = {"9", "Invalid total debt, column no. = " & TotalDebtPos + 1 & ", row no. = " & (RowNum + 1).ToString}
                            If RowError Is Nothing AndAlso CaseBalance <> CDec(FileContents(RowNum, TotalDebtPos)) Then RowError = {"9", "Total debt <> work items, column no. = " & TotalDebtPos + 1 & ", row no. = " & (RowNum + 1).ToString}
                            If RowError Is Nothing AndAlso Not IsNumeric(FileContents(RowNum, NumWorkItemsPos)) Then RowError = {"7", "Invalid number of work items, column no. = " & TotalDebtPos + 1 & ", row no. = " & (RowNum + 1).ToString}
                            If RowError Is Nothing And CaseWorkItemsCount <> CInt(FileContents(RowNum, NumWorkItemsPos)) Then RowError = {"7", "Incorrect number of work items, column no. = " & NumWorkItemsPos + 1 & ", row no. = " & (RowNum + 1).ToString}
                        End If

                        If Not RowError Is Nothing Then AddToErrorLog(RowError(1), True)

                        ' Now insert into the SQL Server database
                        If Not HMRCData.AddHMRCCaseBatchDetail(FileGenerationReference, ProgressBar.Value + 1, FileContents(RowNum, HouseholdRefPos), RowError) Then AddToErrorLog("Unable to add record in FeesSQL.HMRCCaseBatchDetail, row no. = " & (RowNum + 1).ToString, True)

                        If RowValid Then
                            ' Start to build the output file
                            For ColNum As Integer = 0 To NumOfFixedCols - 1
                                Select Case ColNum
                                    Case Claim1DOBPos, Claim2DOBPos
                                        ' DOB needs converting from Excel serial date or normal date depending on how the spreadsheet is populated
                                        If FileContents(RowNum, ColNum) <> "" Then
                                            If IsNumeric(FileContents(RowNum, ColNum)) Then NewDebtLine(ColNum) = DateTime.FromOADate(CInt(FileContents(RowNum, ColNum))).ToString
                                            If IsDate(FileContents(RowNum, ColNum)) Then NewDebtLine(ColNum) = CDate(FileContents(RowNum, ColNum)).ToString
                                        End If

                                    Case Claim1TaxPayerNameTitlePos, Claim2TaxPayerNameTitlePos
                                        ' Concatenate title and the two address lines
                                        NewDebtLine(ColNum) = ConcatFields({FileContents(RowNum, ColNum).ToString, FileContents(RowNum, ColNum + 1).ToString, FileContents(RowNum, ColNum + 2).ToString}, " ")

                                    Case Claim1TaxPayerNameLine1Pos, Claim1TaxPayerNameLine2Pos, Claim2TaxPayerNameLine1Pos, Claim2TaxPayerNameLine2Pos
                                        ' leave the field blank - catered for in ClaimXTaxPayerNameTitlePos

                                    Case Claim1AddressLine1Pos
                                        NewDebtLine(ColNum) = ConcatFields({FileContents(RowNum, ColNum).ToString _
                                                                          , FileContents(RowNum, ColNum + 1).ToString _
                                                                          , FileContents(RowNum, ColNum + 2).ToString _
                                                                          , FileContents(RowNum, ColNum + 3).ToString _
                                                                          , FileContents(RowNum, ColNum + 4).ToString} _
                                                                          , ","
                                                                          )

                                        NewDebtNotes &= String.Join("", ToNote(ConcatFields({FileContents(RowNum, ColNum).ToString _
                                                                                           , FileContents(RowNum, ColNum + 1).ToString _
                                                                                           , FileContents(RowNum, ColNum + 2).ToString _
                                                                                           , FileContents(RowNum, ColNum + 3).ToString _
                                                                                           , FileContents(RowNum, ColNum + 4).ToString} _
                                                                                           , ", "
                                                                                           ) _
                                                                              , "Claimant 1 Address" _
                                                                              , ";" _
                                                                              ) _
                                                                   )

                                    Case Claim1RecovAddressLine1Pos
                                        NewDebtNotes &= String.Join("", ToNote(ConcatFields({FileContents(RowNum, ColNum).ToString _
                                                                                           , FileContents(RowNum, ColNum + 1).ToString _
                                                                                           , FileContents(RowNum, ColNum + 2).ToString _
                                                                                           , FileContents(RowNum, ColNum + 3).ToString _
                                                                                           , FileContents(RowNum, ColNum + 4).ToString} _
                                                                                           , ", "
                                                                                           ) _
                                                                              , "Claimant 1 Recovery Address" _
                                                                              , ";" _
                                                                              ) _
                                                                   )

                                    Case Claim2AddressLine1Pos
                                        NewDebtNotes &= String.Join("", ToNote(ConcatFields({FileContents(RowNum, ColNum).ToString _
                                                                                           , FileContents(RowNum, ColNum + 1).ToString _
                                                                                           , FileContents(RowNum, ColNum + 2).ToString _
                                                                                           , FileContents(RowNum, ColNum + 3).ToString _
                                                                                           , FileContents(RowNum, ColNum + 4).ToString} _
                                                                                           , ", "
                                                                                           ) _
                                                                              , "Claimant 2 Address" _
                                                                              , ";" _
                                                                              ) _
                                                                   )

                                    Case Claim2RecovAddressLine1Pos
                                        NewDebtNotes &= String.Join("", ToNote(ConcatFields({FileContents(RowNum, ColNum).ToString _
                                                                                           , FileContents(RowNum, ColNum + 1).ToString _
                                                                                           , FileContents(RowNum, ColNum + 2).ToString _
                                                                                           , FileContents(RowNum, ColNum + 3).ToString _
                                                                                           , FileContents(RowNum, ColNum + 4).ToString} _
                                                                                           , ", "
                                                                                           ) _
                                                                              , "Claimant 2 Recovery Address" _
                                                                              , ";" _
                                                                              ) _
                                                                   )

                                    Case Claim1AddressLine2Pos, Claim1AddressLine3Pos, Claim1RecovAddressLine2Pos, Claim2AddressLine2Pos, Claim2RecovAddressLine2Pos, Claim1RecovAddressLine3Pos, Claim2AddressLine3Pos, Claim2RecovAddressLine3Pos, Claim1AddressLine4Pos, Claim1RecovAddressLine4Pos, Claim2AddressLine4Pos, Claim2RecovAddressLine4Pos, Claim1PostCodePos, Claim1RecovPostCodePos, Claim2PostCodePos, Claim2RecovPostCodePos
                                        ' leave the field blank - catered for in Claim1AddressLine1Pos, Claim1RecovAddressLine1Pos, Claim2AddressLine1Pos, Claim2RecovAddressLine1Pos

                                    Case Claim1TelNumPos, Claim2TelNumPos
                                        Dim TelNums() As String = ConcatFields({FileContents(RowNum, ColNum), FileContents(RowNum, ColNum + 1), FileContents(RowNum, ColNum + 2)}, ",").Replace(" ", "").Split(",")
                                        For TelNumCount As Integer = 0 To UBound(TelNums)
                                            NewDebtLine(ColNum + TelNumCount) = TelNums(TelNumCount)
                                        Next TelNumCount

                                    Case Claim1Alt1Pos, Claim1Alt2Pos, Claim2Alt1Pos, Claim2Alt2Pos
                                        ' leave the field blank - catered for in ClaimXTelNumPos

                                    Case Claim1RecovNameLine1Pos
                                        NewDebtNotes &= String.Join("", ToNote(ConcatFields({FileContents(RowNum, ColNum).ToString, FileContents(RowNum, ColNum + 1).ToString}, " "), "Claimant 1 Recovery Name", ";"))

                                    Case Claim2RecovNameLine1Pos
                                        NewDebtNotes &= String.Join("", ToNote(ConcatFields({FileContents(RowNum, ColNum).ToString, FileContents(RowNum, ColNum + 1).ToString}, " "), "Claimant 2 Recovery Name", ";"))

                                    Case Claim1RecovNameLine2Pos, Claim2RecovNameLine2Pos
                                        ' leave the field blank - catered for in Claim1RecovNameLine1Pos, Claim2RecovNameLine1Pos

                                    Case Claim1RecovTelNumPos, Claim1BusTelNumPos, Claim2RecovTelNumPos, Claim2BusTelNumPos
                                        ' add to notes
                                        If Not String.IsNullOrEmpty(FileContents(RowNum, ColNum)) Then NewDebtNotes &= String.Join("", ToNote(" " & FileContents(RowNum, ColNum), FileContents(FirstDataRowPos - 1, ColNum), ";"))

                                    Case Else
                                        NewDebtLine(ColNum) = FileContents(RowNum, ColNum)
                                End Select
                            Next ColNum

                            NewDebtLine(48) = FileContents(RowNum, 51) ' Add the first payment reference for claimant 1

                            FileNum = CInt(Math.Floor(TotalValidCasesCount / My.Settings.MaxLinesInOutputFile))

                            If FileNum > UBound(NewDebtFile) Then
                                OutputFiles.Add(InputFilePath & FileName & "_" & (FileNum + 1).ToString.PadLeft(3, CChar("0")) & "_PreProcessed.txt")
                                ReDim Preserve NewDebtFile(FileNum)
                                NewDebtFile(UBound(NewDebtFile)) = OutputHeader & vbCrLf
                                ReDim Preserve NewDebtSumm(2, FileNum)
                            End If

                            NewDebtFile(FileNum) &= FileGenerationReference & Separator & (RowNum - FirstDataRowPos + 1).ToString & Separator & Join(NewDebtLine, Separator) & Separator & NewDebtNotes & NewWorkItemNotes & vbCrLf
                            AppendToFile(OutputFiles(FileNum).ToString, NewDebtFile(FileNum))
                            NewDebtFile(FileNum) = ""

                            ' Update running totals
                            TotalValidCasesCount += 1
                            NewDebtSumm(0, FileNum) += 1
                            TotalValidWorkItemsCount += CaseWorkItemsCount
                            NewDebtSumm(1, FileNum) += CaseWorkItemsCount
                            TotalBalance += CaseBalance
                            NewDebtSumm(2, FileNum) += CaseBalance
                        End If ' RowValid

                    Next RowNum

                    ' Validate totals
                    If TotalValidCasesCount = 0 Then AddToErrorLog("No valid cases found in file.", False)
                    If TotalValidWorkItemsCount = 0 Then AddToErrorLog("No valid work items found in file.", False)
                    If TotalBalance = 0 Then AddToErrorLog("No valid debt stated found in file.", False)

                    If FileContents(UBound(FileContents), 0) = "" Then
                        AddToErrorLog("Trailer record missing.", False)
                    Else
                        ' Validate against trailer
                        Dim Trailer() As String = FileContents(UBound(FileContents), 0).Split(CChar(","))
                        If UBound(Trailer) <> 3 Then
                            AddToErrorLog("Unexpected number of columns in trailer. Totals cannot be validated.", False)
                        Else
                            If Trailer(0) <> "T" Then AddToErrorLog("Invalid trailer identifier found in file.", False)
                            If Not IsNumeric(Trailer(1)) Then AddToErrorLog("Invalid number of cases found in trailer.", False)
                            If Not IsNumeric(Trailer(2)) Then AddToErrorLog("Invalid number of work items found in trailer.", False)
                            If Not IsNumeric(Trailer(3)) Then AddToErrorLog("Invalid total debt found in trailer.", False)

                            If FileValid Then
                                If (TotalValidCasesCount + TotalErrorCasesCount) <> CInt(Trailer(1)) Then AddToErrorLog(Trailer(1) & " cases stated in trailer, found " & (TotalValidCasesCount + TotalErrorCasesCount).ToString & " in file.", False)
                                If (TotalValidWorkItemsCount + TotalErrorWorkItemsCount) <> CInt(Trailer(2)) Then AddToErrorLog(Trailer(2) & " work items stated in trailer, found " & (TotalValidWorkItemsCount + TotalErrorWorkItemsCount).ToString & " in file.", False)
                                If (TotalBalance + TotalErrorCasesValue) <> CDec(Trailer(3)) Then AddToErrorLog(Trailer(3) & " total debt stated in trailer, found " & (TotalBalance + TotalErrorCasesValue).ToString & " in file.", False)
                            End If
                        End If

                        If TotalErrorCasesCount + TotalValidCasesCount > 0 Then
                            RejectionPerc = CDec(Math.Round((TotalErrorCasesCount * 100) / (TotalErrorCasesCount + TotalValidCasesCount), 2))
                        Else
                            RejectionPerc = 0
                        End If

                        If Not HMRCData.AddNewHMRCBatch(FileGenerationReference, TotalValidCasesCount, TotalErrorCasesCount, "Y", "Y", "Y", RejectionPerc, FileType, TotalErrorCasesValue, HMRCFinancialYear) Then AddToErrorLog("Unable to add record in FeesSQL.HMRCCaseBatch", False)

                    End If

                End If ' ValidHeaders

                    ' Update audit log
                    AuditLog = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
                    AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
                    AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

                    AuditLog &= "Number of new cases: " & TotalValidCasesCount.ToString & vbCrLf
                    AuditLog &= "Number of new work items: " & TotalValidWorkItemsCount.ToString & vbCrLf
                    AuditLog &= "Total balance of new cases: " & TotalBalance.ToString & vbCrLf & vbCrLf
                    AuditLog &= "Rejected cases: " & TotalErrorCasesCount.ToString & vbCrLf
                    AuditLog &= "Case rejection: " & RejectionPerc.ToString & "%" & vbCrLf
                    AuditLog &= vbCrLf & vbCrLf & "Files created: " & (UBound(NewDebtSumm, 2) + 1).ToString & vbCrLf

                    If NewDebtSumm(0, 0) > 0 Then ' Check to see if any output files have been created
                    For FileNum = 0 To UBound(NewDebtSumm, 2)
                        AuditLog &= vbCrLf & vbCrLf & "File " & (FileNum + 1).ToString & vbCrLf & vbCrLf
                        AuditLog &= "Number of new cases: " & NewDebtSumm(0, FileNum).ToString & vbCrLf
                        AuditLog &= "Number of new work items: " & NewDebtSumm(1, FileNum).ToString & vbCrLf
                        AuditLog &= "Total balance of new cases: " & NewDebtSumm(2, FileNum).ToString & vbCrLf
                    Next FileNum
                    End If

                    WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

                    If RejectionPerc > My.Settings.BatchRejectPercentageThreshold Then
                        MsgBox(Decimal.Round(RejectionPerc).ToString & "% could not be loaded.", MsgBoxStyle.Critical, Me.Text)
                        FileValid = False
                    End If

                    ProgressBar.Value += 1

            End If

            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
            ProgressBar.Value = ProgressBar.Maximum

            If FileValid Then
                MsgBox("Value loaded £" & TotalBalance.ToString, MsgBoxStyle.Information, Me.Text)
                MsgBox("Value rejected £" & TotalErrorCasesValue.ToString, MsgBoxStyle.Information, Me.Text)
                btnViewOutputFile.Enabled = True
                btnReconcile.Enabled = True
            Else
                MsgBox("The batch has been rejected. Please see the error log for details", MsgBoxStyle.Critical, Me.Text)
            End If

            btnViewInputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try
            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try
            For Each Filename As String In OutputFiles
                If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
            Next Filename

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try
            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnReconcile_Click(sender As Object, e As System.EventArgs) Handles btnReconcile.Click
        Try
            TotalLoadedCasesCount = 0
            TotalLoadedCasesValue = 0
            HMRCData.GetHMRCCaseBatch(FileGenerationReference)

            Dim ReceiptFile As New ArrayList, ReconciliationFile As New ArrayList
            Dim ColumnName(HMRCData.HMRCCaseBatch.Columns.Count - 1) As String

            ' Update records in the database
            For Each Account As DataRow In HMRCData.HMRCCaseBatch.Rows
                HMRCData.UpdateHMRCCaseBatchDetail(FileGenerationReference, Account.Item("RecordSequence").ToString)
            Next Account

            HMRCData.GetHMRCCaseBatch(FileGenerationReference) ' needs to run again to get the case IDs

            ' Populate receipt file
            ' Concatenate all column headers into any array
            For Each Column As DataColumn In HMRCData.HMRCCaseBatch.Columns ' Concatenate all column headers into any array
                ColumnName(Column.Ordinal) = Column.ColumnName
            Next Column
            ColumnName(UBound(ColumnName)) = "ShortClientRef" ' Override the last column header

            ' use records from the database
            For Each Account As DataRow In HMRCData.HMRCCaseBatch.Rows
                ReceiptFile.Add(Account.ItemArray)
                ReceiptFile(ReceiptFile.Count - 1)(13) = Strings.Right(ReceiptFile(ReceiptFile.Count - 1)(13), 4) ' Client ref to be truncated to the last 4 characters
            Next Account

            ' Populate reconciliation file
            Dim RejectionPerc As Decimal
            Dim ReconciliationMsg As String = ""
            If TotalErrorCasesCount + TotalLoadedCasesCount > 0 Then
                RejectionPerc = CDec(Math.Round((TotalErrorCasesCount * 100) / (TotalErrorCasesCount + TotalLoadedCasesCount), 2))
                If RejectionPerc <= My.Settings.BatchRejectPercentageThreshold Then
                    ReconciliationMsg = "Error rate of " & (My.Settings.BatchRejectPercentageThreshold.ToString("G") - RejectionPerc).ToString("G") & "% less than or equal to " & My.Settings.BatchRejectPercentageThreshold.ToString("G") & "% - valid vecords processed"
                Else
                    ReconciliationMsg = "Error rate of " & RejectionPerc.ToString("G") & "% more than " & My.Settings.BatchRejectPercentageThreshold.ToString("G") & "% - valid vecords not processed"
                End If
            End If

            ReconciliationFile.Add({"Date:", DateTime.Today.ToShortDateString})
            ReconciliationFile.Add({"File Type", FileType})
            ReconciliationFile.Add({"Package Number", FileGenerationReference})
            ReconciliationFile.Add({"Status", "Volume", "Value"})
            ReconciliationFile.Add({"Accounts Loaded", TotalLoadedCasesCount.ToString, TotalLoadedCasesValue.ToString("C")})
            ReconciliationFile.Add({"Accounts Rejected", TotalErrorCasesCount.ToString, TotalErrorCasesValue.ToString("C")})
            ReconciliationFile.Add({"Total", (TotalLoadedCasesCount + TotalErrorCasesCount).ToString, (TotalLoadedCasesValue + TotalErrorCasesValue).ToString("C")})
            ReconciliationFile.Add({"Header Validates", "Y"})
            ReconciliationFile.Add({"Trailer Validates", "Y"})
            ReconciliationFile.Add({ReconciliationMsg, RejectionPerc.ToString("G")})

            OutputToExcel(InputFilePath, FileType & "_receipt_" & VersionNum, "Sheet1", ColumnName, ReceiptFile)
            OutputToExcel(InputFilePath, FileType & "_reconciliation_" & VersionNum, "Sheet1", Nothing, ReconciliationFile)

            HMRCData.UpdateHMRCCaseBatch(FileGenerationReference)

            If File.Exists(InputFilePath & FileName & "_receipt_" & VersionNum & ".xls") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_receipt_" & VersionNum & ".xls")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function InputFromExcel(ByVal FileName As String) As String(,)

        InputFromExcel = Nothing

        Try
            Dim TempTable As New DataTable
            ' HDR=NO to not skip the first line and IMEX=1 as the columns can have mixed data types
            Dim xlsConn As System.Data.OleDb.OleDbConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & FileName & "';Extended Properties=""Excel 8.0;HDR=NO;IMEX=1;""")
            Dim xlsComm As System.Data.OleDb.OleDbDataAdapter = New System.Data.OleDb.OleDbDataAdapter("select * from [ROSS$]", xlsConn)

            xlsComm.Fill(TempTable)
            xlsConn.Close()

            Dim Output As String(,)
            ReDim Output(TempTable.Rows.Count - 1, TempTable.Columns.Count - 1)

            For RowIdx As Integer = 0 To TempTable.Rows.Count - 1
                For ColIdx As Integer = 0 To TempTable.Columns.Count - 1
                    Application.DoEvents()
                    Output(RowIdx, ColIdx) = TempTable.Rows(RowIdx).Item(ColIdx).ToString
                Next ColIdx
            Next RowIdx

            Return Output

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Sub AddToErrorLog(ErrorMessage As String, RowLevel As Boolean)
        Try
            ErrorLog &= ErrorMessage & vbCrLf
            rtxtErrorLog.AppendText(ErrorMessage & vbCrLf)
            If RowLevel Then
                TotalErrorCasesCount += 1
                If IsNumeric(FileContents(RowNum, NumWorkItemsPos)) Then TotalErrorWorkItemsCount += CDec(FileContents(RowNum, NumWorkItemsPos))
                If IsNumeric(FileContents(RowNum, TotalDebtPos)) Then TotalErrorCasesValue += CDec(FileContents(RowNum, TotalDebtPos))
                RowValid = False
            Else
                FileValid = False
                MsgBox(ErrorMessage, MsgBoxStyle.Critical, Me.Text)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function IsPaymentRef(HODIndicator As String, RefNo As String) As Boolean

        IsPaymentRef = False

        Try
            Dim z As Integer
            Dim AsciiValue As Integer
            Dim MonthNumber As Integer

            Select Case HODIndicator

                Case "K"    ' The format of the SA reference is 9999999999 ' Note that HMRC dropped the K after initial testing
                    IsPaymentRef = Regex.IsMatch(RefNo, "^[0-9]{10}$")

                Case "D"    ' NINO
                    IsPaymentRef = IsNINumber(RefNo)

                Case "P"    ' PAYE PAYMENT REFERENCE  -   For TAXPAYER.HOD_TYPE 'P'
                    '   Reference field – Total 18 Characters
                    '   The format of the BROCS reference is 999PC99999999YYMMA
                    '   This is constructed as follows:
                    '       999 = District Number (TAXPAYER.DIST_NO - 3 characters)
                    '       P = File Identifier (LAN_WORK_ITEM.FILE_IND, always ‘P’ – 1 character)
                    '       C = Check Character (TAXPAYER.HOD_CHECK_CHAR – 1 character)
                    '       99999999 = Register Number (TAXPAYER.HOD_REG_NO – 8 characters, if less than 8 this field should be preceded by zeros)
                    '       YY = PAYE Year (3rd and 4th characters of LAN_WORK_ITEM.ASN – 2 characters, this should be from the ASN with the earliest outstanding year)
                    '       MM = Month Number (LAN_WORK_ITEM.INSTAL_NO) – 2 characters
                    '       A = Indicator (derived from LAN_WORK_ITEM.INSTAL_NO, values 01 –12 will have indicator value of ‘0’ and 13 or greater will have indicator of ‘A’ – 1 character.

                    ' IMPORTANT - Note example in first file was 992PB00003178 1213

                    If Len(Trim(RefNo)) = 18 Then
                        IsPaymentRef = True
                        For z = 1 To Len(Trim(RefNo))
                            AsciiValue = Asc(UCase(Mid$(Trim(RefNo), z, 1)))
                            Select Case z
                                Case 1 To 3, 6 To 13
                                    If AsciiValue < 48 Or AsciiValue > 57 Then
                                        IsPaymentRef = False
                                    End If
                                Case 4
                                    If UCase(Mid$(Trim(RefNo), z, 1)) <> "P" Then
                                        IsPaymentRef = False
                                    End If
                                Case 5
                                    If AsciiValue < 65 Or AsciiValue > 90 Then
                                        IsPaymentRef = False
                                    End If
                                Case 14 ' Should be a space
                                    If AsciiValue <> 32 Then
                                        IsPaymentRef = False
                                    End If
                                Case 15, 16 '   YY = PAYE Year
                                    If AsciiValue < 48 Or AsciiValue > 57 Then
                                        IsPaymentRef = False
                                    End If
                                Case 17, 18 '   MM = Month Number
                                    If AsciiValue < 48 Or AsciiValue > 57 Then
                                        IsPaymentRef = False
                                    Else
                                        If IsPaymentRef And z >= 17 Then
                                            MonthNumber = Val(Mid$(Trim(RefNo), z - 1, 2))
                                        Else
                                            IsPaymentRef = False
                                        End If
                                    End If

                                Case Else
                                    IsPaymentRef = False

                            End Select
                        Next z
                    Else
                        IsPaymentRef = False
                    End If

                Case "B"    ' VAT   VAT: 9 numerics format 999 9999 99
                    IsPaymentRef = Regex.IsMatch(RefNo, "^[0-9]{3}[ ][0-9]{4}[ ][0-9]{2}$")

                Case "N"
                    IsPaymentRef = Regex.IsMatch(RefNo, "^[A-Z]{2}[0-9]{12}[A-Z]{2}$")

                Case "C"    ' Not getting this debt stream in the first instance
                    IsPaymentRef = False

                Case Else
                    IsPaymentRef = False

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Function IsHOD(HODIndicator As String) As Boolean

        ' HOD Indicators
        '
        '   SA              =   K
        '   NIC2            =   D
        '   NIC2 Penalty    =   P
        '   PAYE            =   P   !!!!!!issue here as HOD's are not unique, this has been raised with HMRC
        '   VAT             =   B
        '   TAX Credits     =   N
        '   COTAX           =   C

        IsHOD = False

        Try

            If {"K", "D", "P", "B", "N", "C"}.Contains(HODIndicator) Then IsHOD = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Function HOD(HODIndicator As String) As String

        HOD = ""

        Try

            Select Case HODIndicator

                Case "K"
                    HOD = "SA"

                Case "D"
                    HOD = "NIC2"

                Case "P"
                    HOD = "PAYE"  ' or NIC2 Penalty!!!!!!!! (HMRC Phil Lee has advised we shouldn't get NIC2 Penalty 05.09.2012

                Case "B"
                    HOD = "VAT"

                Case "N"
                    HOD = "TAX Credits"

                Case "C"
                    HOD = "COTAX"

                Case Else
                    HOD = "XXX"

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function ValidHeaders() As Boolean

        ValidHeaders = False

        Try

            Dim ColumHeader As String
            Dim ErrorCount As Integer = 0

            For ColIdx As Integer = 0 To NumOfFixedCols - 1
                ColumHeader = FileContents(FirstDataRowPos - 1, ColIdx).Replace("  ", " ")

                If FileContents(FirstDataRowPos - 1, ColIdx) <> ColumHeader Then ErrorCount += 1

                OutputHeader &= ColumHeader & Separator
            Next ColIdx

            OutputHeader &= "PaymentReference" & Separator & "NOTES"

            If ErrorCount = 0 Then ValidHeaders = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function HMRCFinancialYear() As Integer

        HMRCFinancialYear = -1

        Try

            HMRCFinancialYear = DateTime.Today.AddMonths(-3).Year

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function FileGenerationReferenceExists() As Boolean

        FileGenerationReferenceExists = False

        Try

            If HMRCData.GetFileGenerationReferenceCount(FileGenerationReference) > 0 Then FileGenerationReferenceExists = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Sub txtMaxLinesInOutputFile_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtMaxLinesInOutputFile.Validating
        Try
            Dim ErrorMsg As String = ""

            If Not IsNumeric(txtMaxLinesInOutputFile.Text) Then
                ErrorMsg = "Please enter a valid number of lines."
            ElseIf CInt(txtMaxLinesInOutputFile.Text) <= 0 Then
                ErrorMsg = "Please enter a positve number of lines."
            End If

            If ErrorMsg = "" Then
                My.Settings.Item("MaxLinesInOutputFile") = CInt(txtMaxLinesInOutputFile.Text)
            Else
                e.Cancel = True
                MsgBox(ErrorMsg, MsgBoxStyle.Critical, Me.Text)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub txtBatchRejectPercentageThreshold_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtBatchRejectPercentageThreshold.Validating
        Try
            Dim ErrorMsg As String = ""

            If Not IsNumeric(txtBatchRejectPercentageThreshold.Text) Then
                ErrorMsg = "Please enter a valid percentage threshold."
            ElseIf CInt(txtBatchRejectPercentageThreshold.Text) <= 0 Then
                ErrorMsg = "Threshold cannot be less than zero."
            ElseIf CInt(txtBatchRejectPercentageThreshold.Text) >= 100 Then
                ErrorMsg = "Threshold must be less than 100."
            End If

            If ErrorMsg = "" Then
                My.Settings.Item("BatchRejectPercentageThreshold") = CDec(txtBatchRejectPercentageThreshold.Text)
            Else
                e.Cancel = True
                MsgBox(ErrorMsg, MsgBoxStyle.Critical, Me.Text)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub txtMinDebtLevel_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtMinDebtLevel.Validating
        Try
            Dim ErrorMsg As String = ""

            If Not IsNumeric(txtMinDebtLevel.Text) Then
                ErrorMsg = "Please enter a valid minimum debt value."
            ElseIf CDec(txtMinDebtLevel.Text) <= 0 Then
                ErrorMsg = "Minimum debt cannot be less than zero."
            ElseIf CDec(txtMaxDebtLevel.Text) < CDec(txtMinDebtLevel.Text) Then
                ErrorMsg = "Minimum debt cannot be greater than maximum debt."
            End If

            If ErrorMsg = "" Then
                My.Settings.Item("MinDebtLevel") = CDec(txtMinDebtLevel.Text)
            Else
                e.Cancel = True
                MsgBox(ErrorMsg, MsgBoxStyle.Critical, Me.Text)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub txtMaxDebtLevel_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtMaxDebtLevel.Validating
        Try
            Dim ErrorMsg As String = ""

            If Not IsNumeric(txtMaxDebtLevel.Text) Then
                ErrorMsg = "Please enter a valid maximum debt value."
            ElseIf CDec(txtMaxDebtLevel.Text) < 0 Then
                ErrorMsg = "Maximum debt cannot be less than zero."
            ElseIf CDec(txtMaxDebtLevel.Text) < CDec(txtMinDebtLevel.Text) Then
                ErrorMsg = "Maximum debt cannot be less than minimum debt."
            End If

            If ErrorMsg = "" Then
                My.Settings.Item("MaxDebtLevel") = CDec(txtMaxDebtLevel.Text)
            Else
                e.Cancel = True
                MsgBox(ErrorMsg, MsgBoxStyle.Critical, Me.Text)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class


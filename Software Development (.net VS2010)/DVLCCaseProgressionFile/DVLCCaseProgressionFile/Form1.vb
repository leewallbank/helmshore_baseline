﻿Imports System.IO

Public Class Form1
    Private InputFilePath As String, FileName As String, FileExt As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim FileDialog As New OpenFileDialog
        Dim InputLineArray() As String
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        Dim paymentFile As String = ""
        Dim statusFile As String = ""
        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)
        Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
        For Each InputLine As String In FileContents
            InputLineArray = InputLine.Split(",")
            Dim clientRef As String = InputLineArray(0)
            'ignore heading and trailer
            If Microsoft.VisualBasic.Left(clientRef, 3) = "UHL" Or
                Microsoft.VisualBasic.Left(clientRef, 3) = "UTL" Then
                Continue For
            End If

            Dim VRM As String = InputLineArray(1)
            Dim payment As Decimal = 0
            Try
                payment = InputLineArray(2) / 100
            Catch ex As Exception

            End Try

            Dim status As Integer = InputLineArray(3)
            Dim statusDesc As String = ""
            Select Case status
                Case 1
                    statusDesc = "Open"
                Case 2
                    statusDesc = "Paying"
                Case 3
                    statusDesc = "Hold"
                Case 4
                    statusDesc = "Resume"
                Case 5
                    statusDesc = "Closed"
            End Select
            If payment <> 0 Then
                paymentFile &= clientRef & "," & VRM & "," & payment & vbNewLine
            End If
            statusFile &= clientRef & "," & VRM & "," & statusDesc & vbNewLine
        Next
        If paymentFile.Length > 0 Then
            My.Computer.FileSystem.WriteAllText(InputFilePath & "DVLC_PaymentFile_" & Format(Now, "ddMMyyyy") & ".txt", paymentFile, False)
        End If
        If statusFile.Length > 0 Then
            My.Computer.FileSystem.WriteAllText(InputFilePath & "DVLC_StatusFile_" & Format(Now, "ddMMyyyy") & ".txt", statusFile, False)
        End If
        MsgBox("Files written")
        Me.Close()
    End Sub
End Class

Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Odbc
Module data_access_module
    Public no_of_rows, last_rowid As Integer
    Public conn As New Odbc.OdbcConnection()

    Sub update_sql(ByVal upd_txt As String)
        Try
            If conn.State = ConnectionState.Closed Then
                Connect_sqlDb()
            End If
            Dim ODBCCMD As Odbc.OdbcCommand

            'Define attachment to database table specifics
            ODBCCMD = New Odbc.OdbcCommand
            With ODBCCMD
                .Connection = conn
                .CommandText = upd_txt
                .ExecuteNonQuery()
            End With

            'Clean up the connection
            ODBCCMD = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub Connect_sqlDb()

        Try
            If Not IsNothing(conn) Then
                'This is only necessary following an exception...
                If conn.State = ConnectionState.Open Then conn.Close()
            End If
            conn.ConnectionString = ""
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("FeesSQL").ConnectionString
            conn.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings("FeesSQL").ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings("feesSQL").ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub
    

End Module

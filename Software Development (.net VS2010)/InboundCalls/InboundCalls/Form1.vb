﻿Imports CommonLibrary
Public Class MainForm
    Dim bail_dt As New DataTable
    Dim null_date As Date = CDate("Jan 1, 1900")
    Dim upd_txt As String
    Dim errorFile As String = ""

    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim calls_dt As New DataTable
        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("FeesSQL")
        ConnectDb2Fees("Touchstar")
        Dim outFile As String = "Calldate| CallTime|Client|Scheme|TalkTime|Agent|IBOB|CRC|DebtorID|Client Ref|Phone No|DebtorName" & vbNewLine
        ' get all inbound calls for yesterday
        Dim start_date As Date = DateAdd(DateInterval.Day, -1, Now)
        start_date = CDate(Format(start_date, "MMM dd, yyyy" & " 00:00:00"))
        Dim end_date As Date = CDate(Format(Now, "MMM dd, yyyy" & " 00:00:00"))

        Dim startID As Integer = 27413911
        'upd_txt = "delete from InboundCalls where ib_ID >= " & startID
        'update_sql(upd_txt)

        'get all calls for yesterday
        'look for call on touchstar
        Dim phone_dt As New DataTable
        LoadDataTable2("Touchstar", "select historyID, calldatetime, phoneNum, CRC, talkTime, agentID from history" & _
                       " where CalldateTime >= '" & Format(start_date, "yyyy-MM-dd") & "'" & _
                       " and IbOb = 0" & _
                       " and CalldateTime < '" & Format(end_date, "yyyy-MM-dd") & "'" & _
                       " order by calldatetime", phone_dt, False)

        For Each phoneRow In phone_dt.Rows
            Dim historyID As Integer = phoneRow(0)
            Dim calldateTime As Date = phoneRow(1)
            Dim phoneNumber As String = phoneRow(2)
            If phoneNumber = 0 Then
                Continue For
            End If
            Dim CRC As String = ""
            Try
                CRC = phoneRow(3)
            Catch ex As Exception

            End Try
            Dim talkTime As String = phoneRow(4)
            Dim agentID As String = phoneRow(5)
            'see if agent login is on touchstar
            Dim agentLogin As String = ""
            agentLogin = GetSQLResults2("Touchstar", "SELECT login from agent " & _
                                                   " WHERE agentID = " & agentID)
           
            'see if phone number on a case
            Dim debt_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select D._rowID, D.client_ref, D.name_fore, D.name_sur, N._createdDate," & _
                           "C.name,S.name,N._createdBy,N.text " & _
                           "from debtor D, clientscheme CS, note N, client C, scheme S " & _
                           "where D.clientschemeID = CS._rowid " & _
                           " and CS.branchID in (2,8,12,23,25,26,27,28,29)" & _
                           " and CS.clientID=C._rowID" & _
                           " and CS.schemeID=S._rowID" & _
                           " and N.debtorID=D._rowID" & _
                            " and (D.add_phone= '" & phoneNumber & "'" & _
                           " or D.add_fax = '" & phoneNumber & "'" & _
                           " or empFax = '" & phoneNumber & "'" & _
                           " or empPhone = '" & phoneNumber & "')" & _
                           " and (N.text like 'INB%'  or N.text like 'INC%')" & _
                           " and N._createdDate >= '" & Format(start_date, "yyyy-MM-dd") & "'" & _
                           " and N._createddate < '" & Format(end_date, "yyyy-MM-dd") & "'", debt_dt, False)

            For Each debtRow In debt_dt.Rows
                Dim debtorID As Integer
                Try
                    debtorID = debtRow(0)
                Catch ex As Exception
                    Continue For
                End Try

                Dim client_ref As String = debtRow(1)
                Dim debtorName As String = ""
                Try
                    debtorName = debtRow(2) & " "
                Catch ex As Exception

                End Try
                debtorName &= debtRow(3)
                Dim clientname As String = debtRow(5)
                Dim schemeName As String = debtRow(6)
                Dim agentname As String = debtRow(7)
                Dim callText As String = debtRow(8)
                outFile &= Format(calldateTime, "dd/MM/yyyy") & "|" & Format(calldateTime, "HH:mm:ss") & "|" & clientname & "|" & schemeName & "|" & _
                    talkTime & "|" & agentLogin & "|" & "IB" & "|" & CRC & "|" & debtorID & "|" & client_ref & "|" & phoneNumber & "|" & debtorName & vbNewLine
                Exit For
            Next


        Next



        If errorFile <> "" Then
            WriteFile("C:/AATemp/InboundCallsError" & Format(Now, "yyyy-MM-dd") & ".txt", errorFile)
        End If
        WriteFile("C:/AATemp/InboundCalls.txt", outFile)
        MsgBox("done - remove later")
        Me.Close()
    End Sub
End Class

﻿Imports System.Xml.Schema
Imports System.IO
Imports commonlibrary

Public Class Main
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim new_file As String = ""
    Dim CSID As Integer
    Dim prod_run As Boolean = False
    Dim filename As String

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If
    End Sub

    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub processbtn_Click(sender As System.Object, e As System.EventArgs) Handles processbtn.Click
        With OpenFileDialog1
            .Title = "Read XLS file"
            .Filter = "XLS file|*.xls"
            .FileName = ""
            .CheckFileExists = True
        End With
        Dim xml_dataSet As New DataSet()
        Dim doc As New Xml.XmlDocument()

        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("File not opened")
            Me.Close()
            Exit Sub
        End If
        processbtn.Enabled = False
        exitbtn.Enabled = False
        Dim InputFilePath As String = Path.GetDirectoryName(OpenFileDialog1.FileName)
        InputFilePath &= "\"
        filename = Path.GetFileNameWithoutExtension(OpenFileDialog1.FileName)
        Dim filecontents(,) As String = InputFromExcel(OpenFileDialog1.FileName)
        Dim phoneFile As String = ""
        Dim phoneChanges As Integer = 0

        ProgressBar1.Value = 5
        Dim record_count As Integer = 0
        Dim clientID As Integer = 1347
        If prod_run = False Then
            MsgBox("TEST RUN ONLY")
            clientID = 24
        End If
        Dim errorFound As Boolean = False
        Dim LineNumber As Integer = 0
        Dim lastClientRef As String = ""
        Dim rowMax As Integer = UBound(filecontents)
        ProgressBar1.Maximum = rowMax
        For rowIDX = 1 To rowMax
            Try
                ProgressBar1.Value = rowIDX
            Catch ex As Exception

            End Try
            Application.DoEvents()
            Dim clientREf As String = Trim(filecontents(rowIDX, 0))
            If clientREf = lastClientRef Then
                Continue For
            End If
            lastClientRef = clientREf
            Dim telePhoneNumber As String = ""
            Try
                telePhoneNumber = Trim(filecontents(rowIDX, 6))
            Catch ex As Exception

            End Try
            If telePhoneNumber = "" Then
                Continue For
            End If
            telePhoneNumber = Replace(telePhoneNumber, " ", "")
            If telePhoneNumber.Length < 6 Then
                Continue For
            End If
            If Microsoft.VisualBasic.Left(telePhoneNumber, 1) <> "0" Then
                telePhoneNumber = "0" & telePhoneNumber
            End If
            'get debtorID
            Dim debtor_dt As New DataTable
            LoadDataTable("DebtRecovery", "SELECT D._rowid, add_phone, add_fax, empphone, empfax " & _
                       "FROM debtor D, clientscheme CS " & _
                       "WHERE client_ref = '" & clientREf & "'" & _
                       " and D.clientschemeID = CS._rowID" & _
                       " and status_open_closed = 'O' " & _
                       " and CS.clientID = " & clientID, debtor_dt, False)

            For Each DebtRow In debtor_dt.Rows
                'see if phone number is already on onestep
                Dim addPhone As String = ""
                Try
                    addPhone = DebtRow(1)
                Catch ex As Exception

                End Try
                If telePhoneNumber = addPhone Then
                    Continue For
                End If
                Dim addfAX As String = ""
                Try
                    addfAX = DebtRow(2)
                Catch ex As Exception

                End Try
                If telePhoneNumber = addfAX Then
                    Continue For
                End If
                Dim empPhone As String = ""
                Try
                    empPhone = DebtRow(3)
                Catch ex As Exception

                End Try
                If telePhoneNumber = empPhone Then
                    Continue For
                End If
                Dim empFax As String = ""
                Try
                    empFax = DebtRow(4)
                Catch ex As Exception

                End Try
                If telePhoneNumber = empFax Then
                    Continue For
                End If
                'phone number not on onestep - put in first available slot
                If addPhone = "" Then
                    phoneFile &= DebtRow(0) & "," & telePhoneNumber & ",,," & vbNewLine
                    phoneChanges += 1
                    Continue For
                End If
                If addfAX = "" Then
                    phoneFile &= DebtRow(0) & ",," & telePhoneNumber & ",," & vbNewLine
                    phoneChanges += 1
                    Continue For
                End If
                If empPhone = "" Then
                    phoneFile &= DebtRow(0) & ",,," & telePhoneNumber & "," & vbNewLine
                    phoneChanges += 1
                    Continue For
                End If
                If empFax = "" Then
                    phoneFile &= DebtRow(0) & ",,,," & telePhoneNumber & vbNewLine
                    phoneChanges += 1
                End If

            Next
        Next

        If phoneChanges > 0 Then
            filename = InputFilePath & "Telephone_Changes_" & Format(Now, "ddMMyyyy") & ".txt"
            My.Computer.FileSystem.WriteAllText(filename, phoneFile, False)
        End If
        If phoneChanges > 0 Then
            MsgBox("Phone changes = " & phoneChanges)
        Else
            MsgBox("No changes found")
        End If

        Me.Close()
    End Sub
   
   
End Class

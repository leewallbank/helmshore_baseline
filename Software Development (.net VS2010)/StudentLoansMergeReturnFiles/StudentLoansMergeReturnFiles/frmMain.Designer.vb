﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.lblAddressFolder = New System.Windows.Forms.Label()
        Me.lblReturnFolder = New System.Windows.Forms.Label()
        Me.txtAddressFolder = New System.Windows.Forms.TextBox()
        Me.txtReturnFolder = New System.Windows.Forms.TextBox()
        Me.btnAddressFolder = New System.Windows.Forms.Button()
        Me.btnReturnFolder = New System.Windows.Forms.Button()
        Me.btnMerge = New System.Windows.Forms.Button()
        Me.btnOutputFolder = New System.Windows.Forms.Button()
        Me.txtOutputFolder = New System.Windows.Forms.TextBox()
        Me.lblOutputFolder = New System.Windows.Forms.Label()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.SuspendLayout()
        '
        'lblAddressFolder
        '
        Me.lblAddressFolder.AutoSize = True
        Me.lblAddressFolder.Location = New System.Drawing.Point(22, 27)
        Me.lblAddressFolder.Name = "lblAddressFolder"
        Me.lblAddressFolder.Size = New System.Drawing.Size(80, 13)
        Me.lblAddressFolder.TabIndex = 0
        Me.lblAddressFolder.Text = "Address Folder:"
        '
        'lblReturnFolder
        '
        Me.lblReturnFolder.AutoSize = True
        Me.lblReturnFolder.Location = New System.Drawing.Point(22, 84)
        Me.lblReturnFolder.Name = "lblReturnFolder"
        Me.lblReturnFolder.Size = New System.Drawing.Size(74, 13)
        Me.lblReturnFolder.TabIndex = 1
        Me.lblReturnFolder.Text = "Return Folder:"
        '
        'txtAddressFolder
        '
        Me.txtAddressFolder.Location = New System.Drawing.Point(25, 50)
        Me.txtAddressFolder.Name = "txtAddressFolder"
        Me.txtAddressFolder.Size = New System.Drawing.Size(579, 20)
        Me.txtAddressFolder.TabIndex = 2
        '
        'txtReturnFolder
        '
        Me.txtReturnFolder.Location = New System.Drawing.Point(25, 107)
        Me.txtReturnFolder.Name = "txtReturnFolder"
        Me.txtReturnFolder.Size = New System.Drawing.Size(579, 20)
        Me.txtReturnFolder.TabIndex = 3
        '
        'btnAddressFolder
        '
        Me.btnAddressFolder.Location = New System.Drawing.Point(610, 50)
        Me.btnAddressFolder.Name = "btnAddressFolder"
        Me.btnAddressFolder.Size = New System.Drawing.Size(26, 20)
        Me.btnAddressFolder.TabIndex = 0
        Me.btnAddressFolder.Text = "..."
        Me.btnAddressFolder.UseVisualStyleBackColor = True
        '
        'btnReturnFolder
        '
        Me.btnReturnFolder.Location = New System.Drawing.Point(610, 107)
        Me.btnReturnFolder.Name = "btnReturnFolder"
        Me.btnReturnFolder.Size = New System.Drawing.Size(26, 20)
        Me.btnReturnFolder.TabIndex = 1
        Me.btnReturnFolder.Text = "..."
        Me.btnReturnFolder.UseVisualStyleBackColor = True
        '
        'btnMerge
        '
        Me.btnMerge.Location = New System.Drawing.Point(200, 223)
        Me.btnMerge.Name = "btnMerge"
        Me.btnMerge.Size = New System.Drawing.Size(228, 31)
        Me.btnMerge.TabIndex = 3
        Me.btnMerge.Text = "Merge"
        Me.btnMerge.UseVisualStyleBackColor = True
        '
        'btnOutputFolder
        '
        Me.btnOutputFolder.Location = New System.Drawing.Point(610, 160)
        Me.btnOutputFolder.Name = "btnOutputFolder"
        Me.btnOutputFolder.Size = New System.Drawing.Size(26, 20)
        Me.btnOutputFolder.TabIndex = 2
        Me.btnOutputFolder.Text = "..."
        Me.btnOutputFolder.UseVisualStyleBackColor = True
        '
        'txtOutputFolder
        '
        Me.txtOutputFolder.Location = New System.Drawing.Point(25, 160)
        Me.txtOutputFolder.Name = "txtOutputFolder"
        Me.txtOutputFolder.Size = New System.Drawing.Size(579, 20)
        Me.txtOutputFolder.TabIndex = 8
        '
        'lblOutputFolder
        '
        Me.lblOutputFolder.AutoSize = True
        Me.lblOutputFolder.Location = New System.Drawing.Point(22, 137)
        Me.lblOutputFolder.Name = "lblOutputFolder"
        Me.lblOutputFolder.Size = New System.Drawing.Size(61, 13)
        Me.lblOutputFolder.TabIndex = 7
        Me.lblOutputFolder.Text = "Output File:"
        '
        'ProgressBar
        '
        Me.ProgressBar.Location = New System.Drawing.Point(25, 195)
        Me.ProgressBar.Maximum = 1000
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(578, 17)
        Me.ProgressBar.TabIndex = 10
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(650, 266)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.btnOutputFolder)
        Me.Controls.Add(Me.txtOutputFolder)
        Me.Controls.Add(Me.lblOutputFolder)
        Me.Controls.Add(Me.btnMerge)
        Me.Controls.Add(Me.btnReturnFolder)
        Me.Controls.Add(Me.btnAddressFolder)
        Me.Controls.Add(Me.txtReturnFolder)
        Me.Controls.Add(Me.txtAddressFolder)
        Me.Controls.Add(Me.lblReturnFolder)
        Me.Controls.Add(Me.lblAddressFolder)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Merge return files"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblAddressFolder As System.Windows.Forms.Label
    Friend WithEvents lblReturnFolder As System.Windows.Forms.Label
    Friend WithEvents txtAddressFolder As System.Windows.Forms.TextBox
    Friend WithEvents txtReturnFolder As System.Windows.Forms.TextBox
    Friend WithEvents btnAddressFolder As System.Windows.Forms.Button
    Friend WithEvents btnReturnFolder As System.Windows.Forms.Button
    Friend WithEvents btnMerge As System.Windows.Forms.Button
    Friend WithEvents btnOutputFolder As System.Windows.Forms.Button
    Friend WithEvents txtOutputFolder As System.Windows.Forms.TextBox
    Friend WithEvents lblOutputFolder As System.Windows.Forms.Label
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar

End Class

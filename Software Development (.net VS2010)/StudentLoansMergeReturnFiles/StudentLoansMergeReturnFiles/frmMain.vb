﻿Imports CommonLibrary
Imports System.IO
Imports System.Math

Public Class frmMain

    Dim MasterList As New DataTable, Address As New DataTable, Payment As New DataTable, TempTable As New DataTable, CheckTable As New DataTable ' Reused for each file

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        Try
            MasterList.Columns.Add("ClientRef", Type.GetType("System.String"))
            MasterList.Columns.Add("LineHeader", Type.GetType("System.String"))

            Address.Columns.Add("ClientRef", Type.GetType("System.String"))
            Address.Columns.Add("LineHeader", Type.GetType("System.String"))
            Address.Columns.Add("LineText", Type.GetType("System.String"))

            Payment.Columns.Add("ClientRef", Type.GetType("System.String"))
            Payment.Columns.Add("LineHeader", Type.GetType("System.String"))
            Payment.Columns.Add("LineText", Type.GetType("System.String"))

            TempTable.Columns.Add("ClientRef", Type.GetType("System.String"))
            TempTable.Columns.Add("LineHeader", Type.GetType("System.String"))

            CheckTable.Columns.Add("ClientRef", Type.GetType("System.String"))

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnAddressFolder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddressFolder.Click
        Try
            txtAddressFolder.Text = GetFolderName(False)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnReturnFolder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReturnFolder.Click
        Try
            txtReturnFolder.Text = GetFolderName(False)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
    Private Sub btnOutputFolder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOutputFolder.Click
        Try
            txtOutputFolder.Text = GetFolderName(True)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnMerge_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMerge.Click
        Try
            If Not Directory.Exists(txtAddressFolder.Text) OrElse Not Directory.Exists(txtReturnFolder.Text) Then
                MsgBox("Please enter valid address and return folders", vbCritical + vbOKOnly, Me.Text)
                Return
            End If

            If Not Directory.Exists(txtOutputFolder.Text) Then Directory.CreateDirectory(txtOutputFolder.Text)

            Dim MergedFile As String, FileNames As String(), ExceptionLog As String = "", ExceptionMsg As String = "", ExceptionFile As String = txtOutputFolder.Text & "\Merge_exceptions.txt"
            Dim AddressDV As DataView = New DataView(Address)
            Dim PaymentDV As DataView = New DataView(Payment)
            Dim FileCount As Integer, CaseCount As Integer

            FileNames = GetFileNames(txtAddressFolder.Text).Union(GetFileNames(txtReturnFolder.Text)).ToArray ' Merge the two arrays. The Union method also removes any duplicates

            For Each FileName As String In FileNames
                ' reset the reusable objects
                MergedFile = ""
                TempTable.Clear()
                MasterList.Clear()
                CheckTable.Clear()
                Address.Clear()
                Payment.Clear()
                CaseCount = 0

                LoadFile(txtAddressFolder.Text & "\" & FileName, Address)
                LoadFile(txtReturnFolder.Text & "\" & FileName, Payment)

                ' Append both sets of headers to temp table. UCase to avoid duplicates names
                For Each AddressLine As DataRow In Address.Rows
                    TempTable.Rows.Add({AddressLine("ClientRef"), UCase(AddressLine("LineHeader"))})
                Next AddressLine

                For Each PaymentLine As DataRow In Payment.Rows
                    TempTable.Rows.Add({PaymentLine("ClientRef"), UCase(PaymentLine("LineHeader"))})
                Next PaymentLine

                ' Get distinct line headers
                MasterList = TempTable.DefaultView.ToTable(True)

                ' The number of unique line headers should equal the number of client refs i.e. we should not have lines with different detail per client ref
                CheckTable = TempTable.DefaultView.ToTable(True, "ClientRef")
                If CheckTable.Rows.Count <> MasterList.Rows.Count Then
                    ExceptionMsg = "Files " & FileName & " contain conflicting case details"
                    MsgBox(ExceptionMsg, vbCritical + vbOKOnly, "Exception found")
                    ExceptionLog &= ExceptionMsg & vbCrLf
                End If

                For Each ReturnLine As DataRow In MasterList.Rows
                    Application.DoEvents()
                    ' vbCrLf added here so that there is not one after the last row
                    If MergedFile <> "" Then MergedFile &= (vbCrLf)

                    AddressDV.RowFilter = ("ClientRef = '" & ReturnLine("ClientRef") & "'")
                    PaymentDV.RowFilter = ("ClientRef = '" & ReturnLine("ClientRef") & "'")

                    ' The following two checks should never fail but just in case...
                    If AddressDV.Count > 1 Then
                        ExceptionMsg = "Address file " & FileName & " contains duplicate lines for client ref " & ReturnLine("ClientRef")
                        MsgBox(ExceptionMsg, vbCritical + vbOKOnly, "Exception found")
                        ExceptionLog &= ExceptionMsg & vbCrLf
                    End If
                    If PaymentDV.Count > 1 Then
                        ExceptionMsg = "Payment file " & FileName & " contains duplicate lines for client ref " & ReturnLine("ClientRef")
                        MsgBox(ExceptionMsg, vbCritical + vbOKOnly, "Exception found")
                        ExceptionLog &= ExceptionMsg & vbCrLf
                    End If

                    ' Start building up the line
                    MergedFile &= ReturnLine("LineHeader")

                    If AddressDV.Count = 0 Then
                        MergedFile &= Strings.Space(511)
                    Else
                        MergedFile &= AddressDV.Item(0).Item("LineText").ToString.Substring(137, 511)
                    End If

                    If PaymentDV.Count = 0 Then
                        MergedFile &= Strings.Space(13)
                    Else
                        MergedFile &= PaymentDV.Item(0).Item("LineText").ToString.Substring(648, 13)
                    End If

                    CaseCount += 1
                    ProgressBar.Value = Truncate((1000 / (UBound(FileNames) + 1)) * FileCount) + Truncate(((1000 / (UBound(FileNames) + 1)) / MasterList.Rows.Count) * CaseCount) ' Always round down so that the max is not exceeded

                Next ReturnLine

                WriteFile(txtOutputFolder.Text & "\" & FileName, MergedFile)

                FileCount += 1
            Next FileName

            If File.Exists(ExceptionFile) Then File.Delete(ExceptionFile)

            If ExceptionLog <> "" Then WriteFile(ExceptionFile, ExceptionLog)

            ProgressBar.Value = ProgressBar.Maximum ' As the value is rounded down we may not have hit the maximum at the end

            MsgBox("Merge complete.", vbInformation + vbOKOnly, Me.Text)

            If File.Exists(ExceptionFile) Then Process.Start(ExceptionFile)
            If Directory.Exists(txtOutputFolder.Text) Then Process.Start(txtOutputFolder.Text)

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub LoadFile(ByVal Filename As String, ByRef TargetTable As DataTable)
        If Not File.Exists(Filename) Then Return

        Dim Line As String

        Try
            Dim StrReader As New StreamReader(Filename)

            Do While StrReader.Peek() <> -1
                Line = StrReader.ReadLine()
                TargetTable.Rows.Add({Line.Substring(0, 11), Line.Substring(0, 137), Line})
            Loop

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Function GetFolderName(ShowNewFolderButton As Boolean) As String
        Dim FolderDialog As New FolderBrowserDialog
        Try
            FolderDialog.Description = "Select folder"
            FolderDialog.ShowNewFolderButton = ShowNewFolderButton
            FolderDialog.ShowDialog()
            Return FolderDialog.SelectedPath

        Catch ex As Exception
            MsgBox(ex.ToString)
            Return ""
        End Try
    End Function

    Private Function GetFileNames(FolderName As String) As String()
        Dim ReturnList As New ArrayList()

        If IO.Directory.Exists(FolderName) Then

            Dim FileList As FileInfo() = New DirectoryInfo(FolderName).GetFiles()

            For Each File As FileInfo In FileList
                ReturnList.Add(File.Name)
            Next File
        End If

        GetFileNames = ReturnList.ToArray(GetType(String))

        Return GetFileNames
    End Function

End Class

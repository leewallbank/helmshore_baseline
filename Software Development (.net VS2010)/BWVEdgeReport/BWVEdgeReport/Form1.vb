﻿Imports CommonLibrary
Imports System.IO
Public Class Form1
    Private addresses, visits, bwvy, bwvn1, bwvn2, bwvn3, bwvn4, bwvother As Integer
    Private totVisits, totAddresses, totBWV, totbwvY, totbwvN1, totbwvN2, totbwvN3, totbwvN4, totbwvOther As Integer
    Private firstVisit, lastVisit As Date
    Private bwvnoteSearch As String = "%Body worn video:%"
    Private bwvnoteY As String = "Body worn video: Y"
    Private bwvnoteN1 As String = "Body worn video: N1"
    Private bwvnoteN2 As String = "Body worn video: N2"
    Private bwvnoteN3 As String = "Body worn video: N3"
    Private bwvnoteN4 As String = "Body worn video: N4"
    Private InputFilePath As String, FileName As String, FileExt As String
    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub readbtn_Click(sender As System.Object, e As System.EventArgs) Handles readbtn.Click
        Dim FileDialog As New OpenFileDialog
        Dim OutputFile As String = "Operator Name,Device Number,Date,Number Of Videos,Time of First Video,Time of Last Video," & _
            "Number of videos < 20 secs,% of videos < 20 secs,Message,EAID,Camera Number,Onestep cases, Onestep Addresses,First Visit,Last Visit,%upload V addresses," & _
            "BWVY,BWVN1,BWVN2,BWVN3,BWVN4,Other,BWVPercentage" & vbNewLine
       
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        exitbtn.Enabled = False
        readbtn.Enabled = False

        Dim HighDate As Date = CDate("Jan 1, 2100")
        Dim lowDate As Date = CDate("Jan 1, 1900")
        Dim message As String = ""
        Dim lastmessage As String = ""
        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)
        Dim lastBWVDate As Date = lowDate

        Dim lastEAName As String = ""
        Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
        Dim InputLineArray() As String
        Dim InputLineArray2() As String
        Dim testdate As Date
        Dim OSEAName As String
        Dim cameraNumber As String = ""
        Dim lastCameraNumber As String = ""
        Dim ignoreEA As Boolean = True
        Dim rowMax As Integer = UBound(FileContents)
        Dim rowID, lastBailiffID As Integer
        Dim lastLoginName As String = ""
        Dim EAIDList As String = ""
        Dim OSCameraNumber As String = ""
        Dim lastOSCameraNumber As String = ""
        ProgressBar1.Maximum = rowMax
        For Each InputLine As String In FileContents
            rowID += 1
            Try
                ProgressBar1.Value = rowID
                Application.DoEvents()
            Catch ex As Exception
                rowID = 0
            End Try
            InputLineArray = Split(InputLine, ",", Chr(34))
            For colIDX = 0 To 6
                Try
                    InputLineArray(colIDX) = Replace(InputLineArray(colIDX), Chr(34), "")
                Catch ex As Exception
                    Exit For
                End Try

            Next
            If InputLineArray(0) = "Total" And ignoreEA = False Then
                'Need to do extra days to end date on current EA
                testdate = DateAdd(DateInterval.Day, 1, lastBWVDate)
                While Format(testdate, "yyyy-MM-dd") <= Format(end_dtp.Value, "yyyy-MM-dd")
                    OutputFile &= lastEAName & "," & lastCameraNumber & "," & testdate & ",0,,,,,"
                    EAIDList = ""
                    Dim eadt3 As New DataTable
                    LoadDataTable("DebtRecovery", "SELECT _rowid, name_fore, name_sur, login_name from bailiff " & _
                                                      "WHERE add_fax = '" & lastCameraNumber & "'", eadt3, False)
                    If eadt3.Rows.Count = 0 Then
                        ignoreEA = True
                        Continue For
                    Else
                        ignoreEA = False
                    End If

                    'get onestep cases visited on day
                    get_visits(eadt3.Rows(0).Item(0), testdate)
                    get_bwv(eadt3.Rows(0).Item(3), testdate)
                    If EAIDList = "" Then
                        EAIDList &= eadt3.Rows(0).Item(0)
                    Else
                        EAIDList &= ";" & eadt3.Rows(0).Item(0)
                    End If

                    'Next
                    OutputFile &= message & "," & EAIDList & "," & OSCameraNumber & "," & visits & "," & addresses & ","
                    If firstVisit = HighDate Then
                        OutputFile &= ","
                    Else
                        OutputFile &= Format(firstVisit, "HH:mm:ss") & ","
                    End If
                    If lastVisit = lowDate Then
                        OutputFile &= ","
                    Else
                        OutputFile &= Format(lastVisit, "HH:mm:ss") & ","
                    End If
                    'upload V addresses
                    OutputFile &= "0,"

                    OutputFile &= bwvy & "," & bwvn1 & "," & bwvn2 & "," & bwvn3 & "," & bwvn4 & "," & bwvother

                    'BWVPercentage()
                    OutputFile &= ","
                    OutputFile &= vbNewLine
                    testdate = DateAdd(DateInterval.Day, 1, testdate)
                    visits = 0
                    addresses = 0
                    bwvy = 0
                    bwvn1 = 0
                    bwvn2 = 0
                    bwvn3 = 0
                    bwvn4 = 0
                    bwvother = 0
                    firstVisit = HighDate
                    lastVisit = lowDate
                    message = ""
                End While

                'now do total row
                For colIDX = 0 To 6
                    OutputFile &= InputLineArray(colIDX) & ","
                Next
                Try
                    OutputFile &= InputLineArray(7) & ",,,,"
                Catch ex As Exception
                    OutputFile &= ",,,,"
                End Try

                OutputFile &= totVisits & "," & totAddresses & ",,," & Format((InputLineArray(3) / totAddresses) * 100, "#") & "," & totbwvY & "," & totbwvN1 & "," & _
                    totbwvN2 & "," & totbwvN3 & "," & totbwvN4 & "," & totbwvOther & "," & Format((totBWV / totVisits) * 100, "#") & vbNewLine
                totAddresses = 0
                totBWV = 0
                totbwvN1 = 0
                totbwvN2 = 0
                totbwvN3 = 0
                totbwvN4 = 0
                totbwvOther = 0
                totbwvY = 0
                totVisits = 0
                lastBWVDate = lowDate
                Continue For
            End If
            visits = 0
            addresses = 0
            bwvy = 0
            bwvn1 = 0
            bwvn2 = 0
            bwvn3 = 0
            bwvn4 = 0
            bwvother = 0
            firstVisit = HighDate
            lastVisit = lowDate
            EAIDList = ""
            message = ""
            Dim bwvDate As Date

            Try
                bwvDate = InputLineArray(2)
            Catch ex As Exception
                If InputLineArray(0) <> "Total" Then
                    Continue For
                End If
            End Try
            Dim EAName As String = InputLineArray(0)
            cameraNumber = InputLineArray(1)
            cameraNumber = "Edge - " & cameraNumber

            'T136835
            'only include EA if a camera exists on onestep
            'read through file to see if any camera number exists on onestep

            If EAName <> lastEAName Then
                ignoreEA = True
                Dim lastcameranumber2 As String = ""
                For Each InputLine2 As String In FileContents
                    InputLineArray2 = Split(InputLine2, ",", Chr(34))
                    For colIDX = 0 To 6
                        Try
                            InputLineArray2(colIDX) = Replace(InputLineArray2(colIDX), Chr(34), "")
                        Catch ex As Exception
                            Exit For
                        End Try
                    Next
                    If InputLineArray2(0) <> EAName Then
                        Continue For
                    End If
                    Dim cameraNumber2 As String = InputLineArray2(1)
                    cameraNumber2 = "Edge - " & cameraNumber2
                    If cameraNumber2 = lastcameranumber2 Then
                        Continue For
                    End If
                    lastcameranumber2 = cameraNumber2
                    Dim eadt2 As New DataTable
                    LoadDataTable("DebtRecovery", "SELECT _rowid, name_fore, name_sur, login_name from bailiff " & _
                                                     "WHERE add_fax = '" & cameraNumber2 & "'", eadt2, False)
                    If eadt2.Rows.Count > 0 Then
                        ignoreEA = False
                        lastBailiffID = eadt2.Rows(0).Item(0)
                        lastLoginName = eadt2.Rows(0).Item(3)
                        Exit For
                    End If
                Next
            End If

            If ignoreEA Then
                Continue For
            End If

            lastCameraNumber = cameraNumber
            lastEAName = EAName


            If EAName = "Total" And ignoreEA = False Then
                'write out last lines to end of period
                bwvDate = end_dtp.Value
                While Format(testdate, "yyyy-MM-dd") <= Format(bwvDate, "yyyy-MM-dd")
                    OutputFile &= lastEAName & "," & lastCameraNumber & ","
                    OutputFile &= testdate & ","
                    OutputFile &= "0,0,0,0,0" & vbNewLine
                    testdate = DateAdd(DateInterval.Day, 1, testdate)
                End While
                lastBWVDate = lowDate
                Continue For
            End If
            lastEAName = EAName
            lastCameraNumber = cameraNumber

            OSEAName = ""
            'get camera number from onestep


            Dim eadt As New DataTable
            Dim bailiffID As Integer

            If lastBWVDate = lowDate Then
                testdate = start_dtp.Value
            Else
                testdate = DateAdd(DateInterval.Day, 1, lastBWVDate)
            End If

            While Format(testdate, "yyyy-MM-dd") < Format(bwvDate, "yyyy-MM-dd")
                'Dates not on spreadsheet
                EAIDList = ""
                Dim loginName As String = ""
              
                'write out any case details for missing dates

                OutputFile &= InputLineArray(0) & "," & cameraNumber & ","
                OutputFile &= testdate & ","
                OutputFile &= "0,,,,," & lastmessage & "," & lastBailiffID & "," & lastOSCameraNumber & ","
                get_visits(lastBailiffID, testdate)
                get_bwv(lastLoginName, testdate)
                OutputFile &= visits & "," & addresses & ","
                If firstVisit = HighDate Then
                    OutputFile &= ","
                Else
                    OutputFile &= Format(firstVisit, "HH:mm:ss") & ","
                End If
                If lastVisit = lowDate Then
                    OutputFile &= ","
                Else
                    OutputFile &= Format(lastVisit, "HH:mm:ss") & ","
                End If
                'upload V addresses
                OutputFile &= "0,"

                OutputFile &= bwvy & "," & bwvn1 & "," & bwvn2 & "," & bwvn3 & "," & bwvn4 & "," & bwvother

                'BWVPercentage()
                OutputFile &= ","
                OutputFile &= vbNewLine

                testdate = DateAdd(DateInterval.Day, 1, testdate)
                visits = 0
                addresses = 0
                bwvy = 0
                bwvn1 = 0
                bwvn2 = 0
                bwvn3 = 0
                bwvn4 = 0
                bwvother = 0
                firstVisit = HighDate
                lastVisit = lowDate
                lastBWVDate = bwvDate
                message = ""
            End While
            'DATE FOUND
            EAIDList = ""
            LoadDataTable("DebtRecovery", "SELECT _rowid, name_fore, name_sur, login_name from bailiff " & _
                                                       "WHERE add_fax = '" & cameraNumber & "'", eadt, False)

            Dim loginname2 As String = ""
            If eadt.Rows.Count = 0 Then
                bailiffID = 0
            Else
                bailiffID = eadt.Rows(0).Item(0)
                loginname2 = eadt.Rows(0).Item(3)
            End If

            OutputFile &= InputLineArray(0) & "," & cameraNumber & ","
            For colIDX = 2 To 6
                OutputFile &= InputLineArray(colIDX) & ","
            Next
            Try
                OutputFile &= InputLineArray(7) & ","
            Catch ex As Exception
                OutputFile &= ","
            End Try


            lastBailiffID = bailiffID
            lastLoginName = loginname2
            
            'If eadt.Rows.Count = 0 Then
            '    'look for name on onestep
            '    Dim forename, surname As String
            '    Dim idx As Integer = InStr(EAName, " ")
            '    Try
            '        forename = Microsoft.VisualBasic.Left(EAName, idx - 1)
            '    Catch ex As Exception
            '        forename = ""
            '    End Try

            '    surname = Microsoft.VisualBasic.Right(EAName, EAName.Length - idx)

            '    Dim EA2dt As New DataTable
            '    LoadDataTable("DebtRecovery", "SELECT _rowid, name_fore, name_sur, add_fax, login_name from bailiff " & _
            '                                            "WHERE name_fore = '" & forename & "'" & _
            '                                            " and status = 'O' " & _
            '                                            " and add_fax is not null" & _
            '                                            " and name_sur = '" & surname & "'", EA2dt, False)
            '    If EA2dt.Rows.Count = 0 Then
            '        'write out file without any onestep details
            '        message = "Not Found on onestep"
            '    Else
            '        bailiffID = EA2dt.Rows(0).Item(0)
            '        lastBailiffID = bailiffID
            '        OSCameraNumber = EA2dt.Rows(0).Item(3)
            '        loginname2 = EA2dt.Rows(0).Item(4)
            '        lastLoginName = loginname2
            '        If EAIDList = "" Then
            '            EAIDList &= EA2dt.Rows(0).Item(0)
            '        Else
            '            EAIDList &= ";" & EA2dt.Rows(0).Item(0)
            '        End If
            '        OSEAName = EA2dt.Rows(0).Item(1) & " " & EA2dt.Rows(0).Item(2)
            '        If EAName <> OSEAName Then
            '            message = "camera number " & cameraNumber & " on onestep is for " & OSEAName & vbNewLine
            '        Else
            '            message = "Camera Number on onestep is " & OSCameraNumber
            '        End If
            '    End If
            'End If
            OutputFile &= message & "," & bailiffID & "," & OSCameraNumber & ","
            lastOSCameraNumber = OSCameraNumber
            Dim smallbwv As Integer = 0
            Try
                smallbwv = InputLineArray(6)
            Catch ex As Exception

            End Try


            'get onestep cases visited on day
            If bailiffID > 0 Then
                get_visits(bailiffID, bwvDate)
                get_bwv(loginname2, bwvDate)
            End If

            'If EAIDList = "" Then
            '    EAIDList &= EARow(0)
            'Else
            '    EAIDList &= ";" & EARow(0)
            'End If

            OutputFile &= visits & "," & addresses & ","
            If firstVisit = HighDate Then
                OutputFile &= ","
            Else
                OutputFile &= Format(firstVisit, "HH:mm:ss") & ","
            End If
            If lastVisit = lowDate Then
                OutputFile &= ","
            Else
                OutputFile &= Format(lastVisit, "HH:mm:ss") & ","
            End If
            'upload V addresses
            OutputFile &= "0,"

            OutputFile &= bwvy & "," & bwvn1 & "," & bwvn2 & "," & bwvn3 & "," & bwvn4 & "," & bwvother

            'BWVPercentage = ""
            OutputFile &= ","
            OutputFile &= vbNewLine
            lastBWVDate = bwvDate
            visits = 0
            addresses = 0
            bwvy = 0
            bwvn1 = 0
            bwvn2 = 0
            bwvn3 = 0
            bwvn4 = 0
            bwvother = 0
            firstVisit = HighDate
            lastVisit = lowDate
            lastmessage = message
            message = ""

        Next
        Dim outFileName As String = InputFilePath & "BWVEdge_Report.csv"
        My.Computer.FileSystem.WriteAllText(outFileName, OutputFile, False)
        MsgBox("Report Produced")
        Me.Close()
    End Sub
    Private Sub get_visits(ByVal EAID As Integer, ByVal dte As Date)
        Dim visitdt As New DataTable
        LoadDataTable("DebtRecovery", "select V.debtorID, D.linkID, V.date_visited from visit V, debtor D, clientscheme CS" & _
                      " where V.bailiffID = " & EAID & _
                      " and D.clientschemeID = CS._rowID " & _
                      " and not(CS.clientID in (1,2,24))" & _
                      " and V.debtorID = D._rowID " & _
                      " and date(V.date_visited) = '" & Format(dte, "yyyy-MM-dd") & "'" & _
                      " order by D.linkID", visitdt, False)
        Dim lastLinkID As Integer = 0
        For Each visitRow In visitdt.Rows
            visits += 1
            totVisits += 1
            Dim visitDate As Date = visitRow(2)
            If visitDate < firstVisit Then
                firstVisit = visitDate
            End If
            If visitDate > lastVisit Then
                lastVisit = visitDate
            End If
            Dim linkID As Integer
            Try
                linkID = visitRow(1)
            Catch ex As Exception
                addresses += 1
                totAddresses += 1
                Continue For
            End Try
            If linkID <> lastLinkID Then
                addresses += 1
                totAddresses += 1
            End If
            lastLinkID = linkID
        Next

    End Sub

    Private Sub get_bwv(ByVal EAloginName As String, ByVal dte As Date)

        'get any BWV notes
        Dim dteplus1 As Date = DateAdd(DateInterval.Day, 1, dte)
        Dim bwvdt As New DataTable
        LoadDataTable("DebtRecovery", "select N.debtorID, N.text from note N " & _
                      " where N._createdby = '" & EAloginName & "'" & _
                      " and N.type = 'Enf. Agent note'" & _
                      " and N._createdDate >='" & Format(dte, "yyyy-MM-dd") & "'" & _
                      " and N._createdDate < '" & Format(dteplus1, "yyyy-MM-dd") & "'" & _
                      " and N.text like '" & bwvnoteSearch & "'", bwvdt, False)
        For Each bwvRow In bwvdt.Rows
            Dim debtorID As Integer = bwvRow(0)
            Dim noteText As String = bwvRow(1)
            If InStr(noteText, bwvnoteY) > 0 Then
                bwvy += 1
                totbwvY += 1
            ElseIf InStr(noteText, bwvnoteN1) > 0 Then
                bwvn1 += 1
                totbwvN1 += 1
            ElseIf InStr(noteText, bwvnoteN2) > 0 Then
                bwvn2 += 1
                totbwvN2 += 1
            ElseIf InStr(noteText, bwvnoteN3) > 0 Then
                bwvn3 += 1
                totbwvN3 += 1
            ElseIf InStr(noteText, bwvnoteN4) > 0 Then
                bwvn4 += 1
                totbwvN4 += 1
            Else
                bwvother += 1
                totbwvOther += 1
            End If
            totBWV += 1
        Next


    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim startDate As Date = DateAdd(DateInterval.Month, -1, Now)
        startDate = CDate(Format(startDate, "yyyy-MM-") & "01 00:00:00")
        Dim endDate As Date = CDate(Format(Now, "yyyy-MM-") & "01 00:00:00")
        endDate = DateAdd(DateInterval.Day, -1, endDate)
        start_dtp.Value = startDate
        end_dtp.Value = endDate
    End Sub
End Class

Public Class mainfrm
    Dim files_found As Boolean = False
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cb_cbox.SelectedIndex = 0
        Dim days As Integer = Weekday(Now)
        date_picker.Value = DateAdd(DateInterval.Day, -days, Now)
    End Sub

    Private Sub retnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles retnbtn.Click
        retn_month = Format(date_picker.Value, "MM_MMM")
        retn_day = Format(date_picker.Value, "dd_ddd")

        retnbtn.Enabled = False
        date_picker.Enabled = False
        Dim branch_id As Integer
        If cb_cbox.SelectedIndex = 0 Then
            branch_id = 2
        Else
            branch_id = 1
        End If

        'get all  schemes for Rotherham
        param2 = "select _rowid, schemeID from ClientScheme where (clientID = 119 or clientID = 941) " &
            " and branchID = " & branch_id
        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        Dim cs_rows As Integer = no_of_rows - 1
        Dim folder_suffix As String
        For cs_idx = 0 To cs_rows
            If branch_id = 1 Then
                'get work type
                param2 = "select work_type from Scheme where _rowid = " & cs_ds.Tables(0).Rows(cs_idx).Item(1)
                Dim sch_ds As DataSet = get_dataset("onestep", param2)
                If sch_ds.Tables(0).Rows(0).Item(0) = 2 Then
                    folder_suffix = "C"
                ElseIf sch_ds.Tables(0).Rows(0).Item(0) = 3 Then
                    folder_suffix = "N"
                Else
                    Continue For
                End If
                selected_csid = cs_ds.Tables(0).Rows(cs_idx).Item(0)
                process_csid(selected_csid, 1, folder_suffix)
            End If
            'branch 2 - only require csid = 558 (FTA), 560 (HBOP), 3284 (HBOP2) & 3278 (FTA2)
            selected_csid = cs_ds.Tables(0).Rows(cs_idx).Item(0)
            If selected_csid = 558 Or selected_csid = 560 Or selected_csid = 3284 Or selected_csid = 3278 Then
                process_csid(selected_csid, 2, "")
            End If
        Next
        MsgBox("All reports moved to directories")
        Me.Close()
    End Sub
    Private Sub process_csid(ByVal selected_csid As Integer, ByVal branch_id As Integer, ByVal folder_suffix As String)
        param1 = "onestep"
        param2 = "select _rowid from Remit where clientschemeID = " & selected_csid & _
             " and date = '" & Format(date_picker.Value, "yyyy-MM-dd") & "'"
        Dim remit_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            Exit Sub
        End If
        Dim remit_no As Integer = remit_dataset.Tables(0).Rows(0).Item(0)
        file_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                               retn_day & "\Remittances\" & Format(remit_no, "#") & "\Returns\"

        Dim file_name, client_ref As String
        Dim cf_closed As String = ""
        Dim cf_trace As String = ""
        Dim cf_nb As String = ""
        Dim cf_fta As String = ""
        Dim cf_hbop As String = ""
        'create directories for returns
        Dim dir_name As String
        If branch_id = 1 Then
            dir_name = file_path & "RONB" & folder_suffix
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
            dir_name = file_path & "ROCL" & folder_suffix
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
            dir_name = file_path & "ROTR" & folder_suffix
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
        Else
            If selected_csid = 558 Then
                dir_name = file_path & "ROCOAF"
                Try
                    Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                    If System.IO.Directory.Exists(dir_name) = False Then
                        di = System.IO.Directory.CreateDirectory(dir_name)
                    Else
                        System.IO.Directory.Delete(dir_name, True)
                        di = System.IO.Directory.CreateDirectory(dir_name)
                    End If
                Catch ex As Exception
                    MsgBox("Unable to create folder")
                    End
                End Try
            ElseIf selected_csid = 260 Or selected_csid = 3284 Then
                dir_name = file_path & "ROCAOP"
                Try
                    Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                    If System.IO.Directory.Exists(dir_name) = False Then
                        di = System.IO.Directory.CreateDirectory(dir_name)
                    Else
                        System.IO.Directory.Delete(dir_name, True)
                        di = System.IO.Directory.CreateDirectory(dir_name)
                    End If
                Catch ex As Exception
                    MsgBox("Unable to create folder")
                    End
                End Try
            End If
        End If
       
        Dim debtor, retn_codeid As Integer
        Try
            For Each foundFile As String In My.Computer.FileSystem.GetFiles _
            (file_path, FileIO.SearchOption.SearchAllSubDirectories, "*.pdf")
                'get debtor number
                files_found = True
                'cater for 8 digit number
                Dim dashIDX As Integer
                For dashidx = foundFile.Length To 1 Step -1
                    If Mid(foundFile, dashIDX, 1) = "-" Then
                        Exit For
                    End If
                Next

                debtor = Mid(foundFile, dashIDX + 1, foundFile.Length - dashIDX - 4)
                'get return code for this debtor
                param2 = "select return_codeID, client_ref, status_open_closed, offence_number, return_form from Debtor " & _
                " where _rowid = " & debtor
                Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 0 Then
                    MsgBox("Unable to find case number" & debtor)
                    Exit Sub
                End If
                Dim status_open_closed As String = debtor_dataset.Tables(0).Rows(0).Item(2)
                If status_open_closed = "O" Then
                    Continue For
                End If
                client_ref = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
                Dim retn_group As String = ""
                If branch_id = 1 Then
                    Try
                        retn_codeid = debtor_dataset.Tables(0).Rows(0).Item(0)
                    Catch ex As Exception
                        retn_codeid = 0
                    End Try
                    'get returns category
                    param2 = "select fee_category from CodeReturns where _rowid = " & retn_codeid
                    Dim retn_ds As DataSet = get_dataset("onestep", param2)
                    Dim retn_cat As Integer = retn_ds.Tables(0).Rows(0).Item(0)
                    'get retn_group 
                    Select Case retn_cat
                        Case 1
                            retn_group = "ROTR" & folder_suffix
                            cf_trace = cf_trace & client_ref & "," & Format(date_picker.Value, "yyyy-MM-dd") & "," & debtor & "," & client_ref & "-" & debtor & ".pdf," & retn_group & vbNewLine
                        Case 2
                            retn_group = "ROCL" & folder_suffix
                            cf_closed = cf_closed & client_ref & "," & Format(date_picker.Value, "yyyy-MM-dd") & "," & debtor & "," & client_ref & "-" & debtor & ".pdf," & retn_group & vbNewLine
                        Case 3
                            retn_group = "RONB" & folder_suffix
                            cf_nb = cf_nb & client_ref & "," & Format(date_picker.Value, "yyyy-MM-dd") & "," & debtor & "," & client_ref & "-" & debtor & ".pdf," & retn_group & vbNewLine
                        Case 4
                            retn_group = "ROCL" & folder_suffix
                            cf_closed = cf_closed & client_ref & "," & Format(date_picker.Value, "yyyy-MM-dd") & "," & debtor & "," & client_ref & "-" & debtor & ".pdf," & retn_group & vbNewLine
                        Case 6
                            'T106678 id return code=10 check for nulla bona
                            Dim nullaBona As Boolean = False
                            If retn_codeid = 10 Then
                                If InStr(debtor_dataset.Tables(0).Rows(0).Item(4), "Nulla") > 0 Then
                                    retn_group = "RONB" & folder_suffix
                                    nullaBona = True
                                    cf_nb = cf_nb & client_ref & "," & Format(date_picker.Value, "yyyy-MM-dd") & "," & debtor & "," & client_ref & "-" & debtor & ".pdf," & retn_group & vbNewLine
                                End If
                            End If
                            If Not nullaBona Then
                                retn_group = "ROCL" & folder_suffix
                                cf_closed = cf_closed & client_ref & "," & Format(date_picker.Value, "yyyy-MM-dd") & "," & debtor & "," & client_ref & "-" & debtor & ".pdf," & retn_group & vbNewLine
                            End If
                    End Select
                    If retn_group = "" Then
                        MsgBox("Return code not found for case number " & debtor)
                    End If

                    file_name = file_path & retn_group & "\" & client_ref & "-" & debtor & ".pdf"
                    My.Computer.FileSystem.CopyFile(foundFile, file_name)
                Else
                    'branch 2
                    If selected_csid = 558 Then  'FTA
                        retn_group = "ROCOAF"
                        cf_fta = cf_fta & client_ref & "," & Format(date_picker.Value, "yyyy-MM-dd") & "," & debtor & "," & client_ref & "-" & debtor & ".pdf," & retn_group & vbNewLine
                        file_name = file_path & retn_group & "\" & client_ref & "-" & debtor & ".pdf"
                        My.Computer.FileSystem.CopyFile(foundFile, file_name)
                    ElseIf selected_csid = 560 Then 'HBOP
                        retn_group = "ROCAOP"
                        cf_hbop = cf_hbop & client_ref & "," & Format(date_picker.Value, "yyyy-MM-dd") & "," & debtor & "," & client_ref & "-" & debtor & ".pdf," & retn_group & vbNewLine
                        file_name = file_path & retn_group & "\" & client_ref & "-" & debtor & ".pdf"
                        Try
                            My.Computer.FileSystem.CopyFile(foundFile, file_name)
                        Catch ex As Exception

                        End Try
                    ElseIf selected_csid = 3284 Then 'HBOP2
                        Dim summons_number As String
                        Try
                            summons_number = debtor_dataset.Tables(0).Rows(0).Item(3)
                        Catch ex As Exception
                            summons_number = ""
                        End Try
                        retn_group = "ROCAOP"
                        cf_hbop = cf_hbop & client_ref & "," & summons_number & "," & Format(date_picker.Value, "yyyy-MM-dd") & "," & debtor & "," & client_ref & "-" & debtor & ".pdf," & retn_group & vbNewLine
                        file_name = file_path & retn_group & "\" & client_ref & "-" & debtor & ".pdf"
                        Try
                            My.Computer.FileSystem.CopyFile(foundFile, file_name)
                        Catch ex As Exception

                        End Try
                    ElseIf selected_csid = 3278 Then 'FTA2
                        Dim summons_number As String
                        Try
                            summons_number = debtor_dataset.Tables(0).Rows(0).Item(3)
                        Catch ex As Exception
                            summons_number = ""
                        End Try
                        retn_group = "ROCOAF"
                        cf_fta = cf_fta & summons_number & "," & client_ref & "," & Format(date_picker.Value, "yyyy-MM-dd") & "," & debtor & "," & client_ref & "-" & debtor & ".pdf," & retn_group & vbNewLine
                        file_name = file_path & retn_group & "\" & client_ref & "-" & debtor & ".pdf"
                        Try
                            My.Computer.FileSystem.CopyFile(foundFile, file_name)
                        Catch ex As Exception

                        End Try
                    End If
                End If

            Next
        Catch ex As Exception

        End Try

        'write control files

        If cf_closed <> "" Then
            cf_closed = "HEADER RECORD" & vbNewLine & cf_closed
            file_name = file_path & "ROCl" & folder_suffix & "\cf_ROCL" & folder_suffix & ".txt"
            My.Computer.FileSystem.WriteAllText(file_name, cf_closed, False)
        End If

        If cf_nb <> "" Then
            cf_nb = "HEADER RECORD" & vbNewLine & cf_nb
            file_name = file_path & "RONB" & folder_suffix & "\cf_RONB" & folder_suffix & ".txt"
            My.Computer.FileSystem.WriteAllText(file_name, cf_nb, False)
        End If

        If cf_trace <> "" Then
            cf_trace = "HEADER RECORD" & vbNewLine & cf_trace
            file_name = file_path & "ROTR" & folder_suffix & "\cf_ROTR" & folder_suffix & ".txt"
            My.Computer.FileSystem.WriteAllText(file_name, cf_trace, False)
        End If
        If cf_fta <> "" Then
            cf_fta = "HEADER RECORD" & vbNewLine & cf_fta
            file_name = file_path & "ROCOAF" & folder_suffix & "\cf_ROCOAF" & folder_suffix & ".txt"
            My.Computer.FileSystem.WriteAllText(file_name, cf_fta, False)
        End If
        If cf_hbop <> "" Then
            cf_hbop = "HEADER RECORD" & vbNewLine & cf_hbop
            file_name = file_path & "ROCAOP" & folder_suffix & "\cf_ROCAOP" & folder_suffix & ".txt"
            My.Computer.FileSystem.WriteAllText(file_name, cf_hbop, False)
        End If
       
        'If files_found Then
        '    MsgBox("All reports moved to directories")
        'Else
        '    MsgBox("No reports were found")
        'End If
        'Me.Close()
    End Sub

End Class

﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim OutputFile As String = ""
            Dim TraceFile As String = ""
            Dim totValue As Decimal = 0
            Dim totTraceValue As Decimal = 0
            Dim totCases As Integer = 0
            Dim totTraceCases As Integer = 0
            Dim comments As String = ""
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)
            Dim traceCase As Boolean
            Dim Filecontents(,) As String = InputFromExcel(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents)
            Dim rowMax As Integer = UBound(Filecontents)
            Dim field As String
            Dim ignoreRow As Boolean = False
            For rowIDX = 0 To rowMax
                ProgressBar.Value = rowIDX
                Application.DoEvents()
                traceCase = False
                If Filecontents(rowIDX, 28) = "ROSSENDALES TRACING" Then
                    traceCase = True
                End If
                For colIDX = 0 To 28
                    field = Filecontents(rowIDX, colIDX)
                    If field = Nothing Then
                        If colIDX = 0 Then
                            ignoreRow = True
                            Exit For
                        End If
                    End If
                    'merge columns A and B

                    'add cols E and F comments except for line 0 - headings
                    If colIDX = 0 Then
                        field &= Filecontents(rowIDX, 1)
                    End If
                    If traceCase Then
                        TraceFile &= field & "|"
                    Else
                        OutputFile &= field & "|"
                    End If
                    If rowIDX <> 0 Then
                        If colIDX = 4 And field <> Nothing Then
                            comments &= "Name 1:" & field & ";"
                        End If
                        If colIDX = 5 And field <> Nothing Then
                            comments &= "Name 2:" & field & ";"
                        End If
                    End If
                Next
                'add comments to headings
                If rowIDX = 0 Then
                    If traceCase Then
                        TraceFile &= "Comments" & vbNewLine
                        OutputFile = TraceFile
                    Else
                        OutputFile &= "Comments" & vbNewLine
                        TraceFile = OutputFile
                    End If
                Else
                    If ignoreRow = False Then
                        If traceCase Then
                            totTraceCases += 1
                            totTraceValue += Filecontents(rowIDX, 15)
                            TraceFile &= comments & vbNewLine
                        Else
                            totCases += 1
                            totValue += Filecontents(rowIDX, 15)
                            OutputFile &= comments & vbNewLine
                        End If
                    End If
                End If
                comments = ""
                ignoreRow = False
            Next
            btnViewInputFile.Enabled = True
            If totCases > 0 Then
                WriteFile(InputFilePath & FileName & "_4948preprocess.txt", OutputFile)
                btnViewOutputFile.Enabled = True
            End If
            If totTraceCases > 0 Then
                WriteFile(InputFilePath & FileName & "_trace_preprocess.txt", TraceFile)
            End If

            MsgBox("Cases = " & totCases & " Value " & totValue & vbNewLine & _
                   "Trace cases = " & totTraceCases & " value " & totTraceValue)
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_preprocess.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_preprocess.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & "_nonWRP_4200.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_nonWRP_4200.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

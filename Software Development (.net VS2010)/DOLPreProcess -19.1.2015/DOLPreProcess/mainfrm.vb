﻿
Public Class mainfrm
    Dim ascii As New System.Text.ASCIIEncoding()
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub collatebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles collatebtn.Click

        Dim prod_run As Boolean = False
        Dim env_str As String = ""
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then prod_run = True
        'look for RCL001 CFWD file or IN file in previous days folders
        collatebtn.Enabled = False
        exitbtn.Enabled = False
        Dim search_date As Date = process_dtp.Value
        Dim prev_files_found As Integer = 0
        Dim document_name As String = ""
        Dim saved_file_path As String = ""
        Dim search_month As String
        Dim search_day As String
        Dim file_path As String
        Dim ignored_file As String = ""
        Dim exception_file As String = ""
        Dim audit_file As String = ""
        Dim collect_letters As Integer = 0
        Dim hmrc_letters As Integer = 0
        Dim dvla_letters As Integer = 0
        Dim bailiff_letters As Integer = 0
        Dim marston_collect_letters As Integer = 0
        Dim marston_bailiff_letters As Integer = 0

        While prev_files_found = 0
            search_date = DateAdd(DateInterval.Day, -1, search_date)
            search_month = Format(search_date, "MM") & "_" & Format(search_date, "MMM")
            search_day = Format(search_date, "dd") & "_" & Format(search_date, "ddd")
            file_path = "O:\DebtRecovery\Archives\" & Format(search_date, "yyyy") & "\" & search_month &
                "\" & search_day & "\PrintFiles"
            Try
                For Each foundFile As String In My.Computer.FileSystem.GetFiles(
              file_path,
               Microsoft.VisualBasic.FileIO.SearchOption.SearchTopLevelOnly, "*.txt")
                    prev_files_found += 1
                    Dim idx As Integer
                    For idx = foundFile.Length To 1 Step -1
                        If Mid(foundFile, idx, 1) = "\" Then
                            Exit For
                        End If
                    Next
                    document_name = UCase(Microsoft.VisualBasic.Right(foundFile, foundFile.Length - idx))
                    If document_name = "RCL001_CFWD.TXT" _
                            Or (InStr(document_name, "RCL001_") > 0 And InStr(document_name, ".IN") > 0) Then
                        saved_file_path = file_path
                        Exit For
                    End If
                Next
            Catch ex As Exception

            End Try

        End While
        If document_name = "" Then
            MsgBox("NO CFWD or IN file was found in PrintFiles directory for " & Format(search_date, "dd/MM/yyyy"))
            Exit Sub
        End If

        'now look in today's folder
        Dim new_file As String = ""
        'add in cfwd file if found
        If document_name = "RCL001_CFWD.TXT" Then
            new_file = My.Computer.FileSystem.ReadAllText(saved_file_path & "\" & document_name)
        End If

        search_date = process_dtp.Value
        search_month = Format(search_date, "MM") & "_" & Format(search_date, "MMM")
        search_day = Format(search_date, "dd") & "_" & Format(search_date, "ddd")
        file_path = "O:\DebtRecovery\Archives\" & Format(search_date, "yyyy") & "\" & search_month &
            "\" & search_day & "\PrintFiles"
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(
          file_path,
           Microsoft.VisualBasic.FileIO.SearchOption.SearchTopLevelOnly, "*.IN")
            Dim idx As Integer
            For idx = foundFile.Length To 1 Step -1
                If Mid(foundFile, idx, 1) = "\" Then
                    Exit For
                End If
            Next
            document_name = UCase(Microsoft.VisualBasic.Right(foundFile, foundFile.Length - idx))
            If InStr(document_name, "RCL001_") > 0 And InStr(document_name, ".IN") > 0 Then
                MsgBox("There is already an RCL001 .IN file in PrintFiles for " & Format(search_date, "dd/MM/yyyy"))
                If prod_run Then
                    Me.Close()
                    Exit Sub
                Else
                    MsgBox("Test carry on")
                End If
            End If
        Next
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(
          file_path,
           Microsoft.VisualBasic.FileIO.SearchOption.SearchTopLevelOnly, "*.txt")
            Dim idx As Integer
            For idx = foundFile.Length To 1 Step -1
                If Mid(foundFile, idx, 1) = "\" Then
                    Exit For
                End If
            Next
            document_name = UCase(Microsoft.VisualBasic.Right(foundFile, foundFile.Length - idx))

            If document_name = "RCL001_BFWD.TXT" Then
                MsgBox("There is already a BFWD file in PrintFiles for " & Format(search_date, "dd/MM/yyyy"))
                Me.Close()
                Exit Sub
            End If
            If document_name = "RCL001_CFWD.TXT" Then
                MsgBox("There is already a CFWD file in PrintFiles for " & Format(search_date, "dd/MM/yyyy"))
                If prod_run Then
                    Me.Close()
                    Exit Sub
                Else
                    MsgBox("TEST so continuing")
                End If
                
            End If
            If document_name = "RCL001_IGNORED_FILE.TXT" Then
                Continue For
            End If
            If document_name = "RCL001_EXCEPTION_FILE.TXT" Then
                Continue For
            End If
            If document_name = "RCL001_AUDIT_FILE.TXT" Then
                Continue For
            End If
            'add files to new_file after removing any invalid charcters in address columns
            new_file = new_file & My.Computer.FileSystem.ReadAllText(foundFile)
        Next
        Dim new_file_path As String = ""
        new_file = Replace(new_file, "~", "")

        'read file and check no test cases and balance is at least £1
        Dim letters As Integer = 0
        Dim letters_RCL10 As Integer = 0
        Dim ETC_letters As Integer = 0
        Dim DMI_letters As Integer = 0
        Dim linetext As String = ""
        Dim outfile As String = ""
        Dim outfile_526 As String = ""
        Dim outfile_RCL10 As String = ""
        For idx = 1 To Len(new_file)
            Try
                ProgressBar1.Value = (idx / Len(new_file)) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            If Mid(new_file, idx, 2) = vbNewLine Then
                'get debtor ID
                If linetext.Length = 1 Then
                    idx += 1
                    linetext = ""
                    Continue For
                End If
                Dim test_letter() As String = Split(linetext, "|")
                If UBound(test_letter) < 50 Then
                    idx += 1
                    Continue For
                End If

                Dim debtorID As Integer
                Dim start_idx As Integer = InStr(linetext, "|")
                Dim test_line As String = Microsoft.VisualBasic.Right(linetext, linetext.Length - start_idx)
                Dim end_idx As Integer = InStr(test_line, "|")
                Try
                    debtorID = Mid(test_line, 1, end_idx - 1)
                Catch ex As Exception
                    ignored_file = ignored_file & "Unable to find debtorID on line no = " & idx & vbNewLine
                    linetext = ""
                    Continue For
                End Try
                debtorID = test_letter(1)

                param2 = "select debt_balance, clientschemeID, bail_current, bailiffID from Debtor where _rowid = " & debtorID
                Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    ignored_file = ignored_file & "Unable to find debtorID = " & debtorID & vbNewLine
                    linetext = ""
                    Continue For
                End If
                Dim CSID As Integer = debtor_dataset.Tables(0).Rows(0).Item(1)
                param2 = "select clientID, branchID from clientScheme where _rowid = " & CSID
                Dim csid_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    ignored_file = ignored_file & "Unable to find client scheme ID for debtor = " & debtorID & vbNewLine
                    linetext = ""
                    Continue For
                End If
                Dim balance As Decimal = debtor_dataset.Tables(0).Rows(0).Item(0)
                Dim branchID As Integer
                If balance < 1 Then
                    branchID = csid_dataset.Tables(0).Rows(0).Item(1)
                    ignored_file = ignored_file & "DebtorID = " & debtorID & " Branch = " & branchID & " ignored as balance less than a pound" & vbNewLine
                    linetext = ""
                    Continue For
                End If
                Dim cl_id As Integer = csid_dataset.Tables(0).Rows(0).Item(0)
                If cl_id = 1 Or cl_id = 2 Or cl_id = 24 Then
                    linetext = ""
                    Continue For
                End If
                'if letter type is 507 ignore if case is currently active with bailiff who has a pen
                If test_letter(0) = 507 Then
                    Dim bail_current As String = debtor_dataset.Tables(0).Rows(0).Item(2)
                    If bail_current = "Y" Then
                        Dim bailiffID As Integer = debtor_dataset.Tables(0).Rows(0).Item(3)
                        param2 = "select agent_type, hasPen from Bailiff where _rowid = " & bailiffID
                        Dim bail_ds As DataSet = get_dataset("onestep", param2)
                        If bail_ds.Tables(0).Rows(0).Item(0) = "B" And
                            bail_ds.Tables(0).Rows(0).Item(1) = "Y" Then
                            ignored_file = ignored_file & "DebtorID = " & debtorID & " ignored as broken letter(507) allocated to a bailff" & vbNewLine
                            linetext = ""
                            Continue For
                        End If
                    End If
                End If

                'read linetext and replace any invalid characters in the address by a space
                Dim new_linetext As String = ""
                Dim pipe_no As Integer = 1
                For idx2 = 1 To linetext.Length
                    If Mid(linetext, idx2, 1) = "|" Then
                        pipe_no += 1
                        new_linetext = new_linetext & "|"
                        Continue For
                    End If
                    Dim test_char As String = Mid(linetext, idx2, 1)
                    If pipe_no >= 13 And pipe_no < 17 Then
                        If Asc(test_char) = 44 Or Asc(test_char) = 42 Or Asc(test_char) = 33 _
                            Or Asc(test_char) = 40 Or Asc(test_char) = 41 Or Asc(test_char) = 58 _
                            Or Asc(test_char) = 9 Or Asc(test_char) = 59 Or Asc(test_char) = 64 _
                            Or Asc(test_char) = 63 Then
                            'change to a space  44(,)  42(*)  33(!)  40(()  41()) 58(:) 09(tab) 59(;) 64(@) 63(?)
                            new_linetext = new_linetext & " "
                        Else
                            If Asc(test_char) = 39 Then  'single quote
                                'do nothing!
                            Else
                                new_linetext = new_linetext & test_char
                                'write exception if not in valid ranges
                                If Asc(test_char) = 32 Or (Asc(test_char) >= 65 And Asc(test_char) <= 90) _
                                     Or (Asc(test_char) >= 97 And Asc(test_char) <= 122) _
                                     Or (Asc(test_char) >= 48 And Asc(test_char) <= 57) _
                                     Or Asc(test_char) = 38 _
                                     Or Asc(test_char) = 45 _
                                     Or Asc(test_char) = 46 _
                                    Or Asc(test_char) = 47 Then  '38(&) 45(-) 46(.) 47(/)
                                    'character OK
                                Else
                                    exception_file = exception_file & "Debtor|" & debtorID & "|invalid char|" & test_char & "|in text|" & linetext & vbNewLine
                                End If
                            End If
                        End If
                    Else
                        new_linetext = new_linetext & test_char
                    End If

                Next
                '28.5.2013 letter 526 to go to RCL007 file
                '30.5.2013 remove this change
                'If test_letter(0) = 526 Then
                '    linetext = new_linetext + vbNewLine
                '    outfile_526 = outfile_526 & linetext
                'Else
                '10.2.2014 letter 097 to go to RCL010 file
                'T56709 add 115 and 116
                If test_letter(0) = 97 Or test_letter(0) = 115 Or test_letter(0) = 116 Then
                    linetext = new_linetext + vbNewLine
                    outfile_RCL10 &= linetext
                Else
                    letters += 1
                    linetext = new_linetext + vbNewLine
                    outfile = outfile & linetext
                End If

                linetext = ""
                idx += 1
                'accumulate letters totals
                '18.6.2015 add letters 115 and 116 to RCL10
                If test_letter(0) = 97 Or test_letter(0) = 115 Or test_letter(0) = 116 Then
                    letters_RCL10 += 1
                Else
                    '29.10.2015 use branch 26 for dmi letters and use etc for ext tax credit 2 - CSID=4471
                    If branchID = 26 Then
                        DMI_letters += 1
                    ElseIf CSID = 4471 Then
                        ETC_letters += 1
                    ElseIf test_letter(0) >= 81 And test_letter(0) <= 92 Then
                        dvla_letters += 1
                    ElseIf test_letter(0) >= 362 And test_letter(0) <= 364 Then
                        ETC_letters += 1
                    ElseIf test_letter(0) >= 350 And test_letter(0) <= 399 Then
                        hmrc_letters += 1
                    ElseIf test_letter(0) < 500 Then
                        collect_letters += 1
                    ElseIf test_letter(0) < 1000 Then
                        bailiff_letters += 1
                    ElseIf test_letter(0) < 1500 Then
                        marston_collect_letters += 1
                    Else
                        marston_bailiff_letters += 1
                    End If
                End If
            Else
                    If Mid(new_file, idx, 1) <> Chr(10) _
                    And Mid(new_file, idx, 1) <> Chr(13) Then
                        linetext = linetext + Mid(new_file, idx, 1)
                    End If
                End If
        Next
        

        If letters >= min_letters_tbox.Text Then
            new_file_path = file_path & "\RCL001_" & Format(Now, "ddMMyyyy" & ".IN")

            'get test letters to add to file
            Dim test_file_path = "X:\Document Outsourcing"
            Try
                For Each foundFile As String In My.Computer.FileSystem.GetFiles(
              test_file_path,
               Microsoft.VisualBasic.FileIO.SearchOption.SearchTopLevelOnly, "Testletter.txt")
                    Dim idx As Integer
                    For idx = foundFile.Length To 1 Step -1
                        If Mid(foundFile, idx, 1) = "\" Then
                            Exit For
                        End If
                    Next
                    document_name = Microsoft.VisualBasic.Right(foundFile, foundFile.Length - idx)
                    If document_name = "Testletter.txt" Then
                        Dim test_letters As String = My.Computer.FileSystem.ReadAllText(foundFile)
                        test_letters = Replace(test_letters, "~", "")
                        test_letters = Replace(test_letters, """", "")
                        For idx = 1 To Len(test_letters)
                            If Mid(test_letters, idx, 2) = vbNewLine Then
                                letters += 1
                                'add one for test letter
                                bailiff_letters += 1
                            End If
                        Next
                        outfile = outfile & test_letters
                    End If
                Next
                MsgBox("Enough letters to go out - " & letters)
            Catch
                MsgBox("No test file found!!")
                Exit Sub
            End Try
        Else
            new_file_path = file_path & "\RCL001_CFWD.TXT"
            MsgBox("Not enough letters to go out - " & letters)
        End If
        If Not prod_run Then
            MsgBox("Test to H:\temp")
            file_path = "H:\temp"
            new_file_path = file_path & "\RCL001_TEST.TXT"
        End If

        My.Computer.FileSystem.WriteAllText(new_file_path, outfile, False, ascii)

        '3.11.2015
        'De-dup letter file
        'read IN file and remove any rows with same letter and case number (cols A and B)
        Dim Infile As String = My.Computer.FileSystem.ReadAllText(new_file_path)
        Dim firstLine As Boolean = True
        Dim InArray() As String = Infile.Split(vbNewLine)
        Dim rows As Integer = 0
        For Each line In InArray.Distinct
            rows += 1
            If rows <> InArray.Distinct.Count Then
                line &= vbCr
            End If
            If firstLine Then
                firstLine = False
                My.Computer.FileSystem.WriteAllText(new_file_path, line, False, ascii)
            Else
                My.Computer.FileSystem.WriteAllText(new_file_path, line, True, ascii)
            End If
        Next

        'Task remove last0D

        new_file_path = file_path & "\RCL001_IGNORED_FILE.TXT"
        My.Computer.FileSystem.WriteAllText(new_file_path, ignored_file, False, ascii)

        If exception_file.Length > 0 Then
            new_file_path = file_path & "\RCL001_EXCEPTION_FILE.TXT"
            My.Computer.FileSystem.WriteAllText(new_file_path, exception_file, False, ascii)
            MsgBox("Exception file written")
        End If

        audit_file = "Collect Letters = " & collect_letters & vbNewLine &
            "HMRC    Letters = " & hmrc_letters & vbNewLine &
            "DVLA Letters = " & dvla_letters & vbNewLine &
            "bailiff Letters = " & bailiff_letters & vbNewLine &
            "Marston Collect = " & marston_collect_letters & vbNewLine &
            "Marston Bailiff = " & marston_bailiff_letters & vbNewLine &
            "Lowell NTA Letters     = " & letters_RCL10 & vbNewLine &
            "ETC Letters     = " & ETC_letters & vbNewLine &
            "DMI Letters     = " & DMI_letters & vbNewLine

        new_file_path = file_path & "\RCL001_AUDIT_FILE.TXT"
        My.Computer.FileSystem.WriteAllText(new_file_path, audit_file, False, ascii)



        '30.5.2013 remove RCL007 file
        'If outfile_526.Length > 0 Then
        '    new_file_path = file_path & "\RCL007_" & Format(Now, "ddMMyyyy" & ".IN")
        '    My.Computer.FileSystem.WriteAllText(new_file_path, outfile_526, False, ascii)
        'End If
        If outfile_RCL10.Length > 0 Then
            new_file_path = file_path & "\RCL010_" & Format(Now, "ddMMyyyy" & ".IN")
            My.Computer.FileSystem.WriteAllText(new_file_path, outfile_RCL10, False, ascii)
        End If
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        process_dtp.Value = Now
    End Sub
End Class

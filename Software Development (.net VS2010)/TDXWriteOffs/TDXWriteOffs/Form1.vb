Public Class Form1
    Dim outfile As String
    Public ascii As New System.Text.ASCIIEncoding()
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
       
        Dim listID As Integer
        Try
            listID = list_tbox.Text
        Catch ex As Exception
            MsgBox("Invalid List ID")
            Exit Sub
        End Try
        Dim debtorID As Integer
        Try
            debtorID = case_tbox.Text
        Catch ex As Exception
            MsgBox("Example Case number is invalid")
            Exit Sub
        End Try
        'check list is on onestep
        param2 = "select objectRowId from ListDetail where listID = " & listID
        Dim list_ds As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            MsgBox("List " & listID & " does not exist on onestep")
            Me.Close()
            Exit Sub
        End If

        Dim listIDX As Integer
        Dim listRows As Integer = no_of_rows - 1
        Dim caseFound As Boolean = False
        Dim totalBalance As Decimal
        Dim caseNo As Integer
        Dim user As String = My.User.Name
        'check all cases have status C, S or X and that example case exists in the list
        For listIDX = 0 To listRows
            Try
                ProgressBar.Value = (listIDX / listRows) * 100
            Catch ex As Exception

            End Try

            Application.DoEvents()
            Dim listDebtorID As Integer = list_ds.Tables(0).Rows(listIDX).Item(0)
            If listDebtorID = debtorID Then
                caseFound = True
            End If
            param2 = "select status from Debtor where _rowid = " & listDebtorID
            Dim list2_ds As DataSet = get_dataset("onestep", param2)
            If list2_ds.Tables(0).Rows(0).Item(0) <> "C" And _
                list2_ds.Tables(0).Rows(0).Item(0) <> "S" And _
                list2_ds.Tables(0).Rows(0).Item(0) <> "F" And _
                list2_ds.Tables(0).Rows(0).Item(0) <> "X" Then
                MsgBox("Case No " & listDebtorID & " status is not C, S, F or X")

            End If
        Next

        If Not caseFound Then
            MsgBox("Example case No is NOT in the list!")
            Exit Sub
        End If
        ProgressBar.Value = 50
        'check list has not already been run
        param2 = "select tdx_listId from TDXWriteOffs where tdx_listID = " & listID
        Dim tdx_ds As DataSet = get_dataset("Fees", param2)
        Dim saveDetails As Boolean = True
        If no_of_rows > 0 Then
            saveDetails = False
            If MsgBox("This list has already been run - do again?", MsgBoxStyle.YesNo, "List already run") = MsgBoxResult.No Then
                MsgBox("Report not produced")
                Me.Close()
                Exit Sub
            End If
        End If
        Dim errorFound As Boolean = False
        'save details in outfile
        For listIDX = 0 To listRows
            Dim listDebtorID As Integer = list_ds.Tables(0).Rows(listIDX).Item(0)
            'get details from debtor table
            param2 = "select offenceLocation, client_ref, offence_number, offenceCourtName, clientSchemeReturnID, D.status," & _
                "return_codeID, clientschemeID, CS.clientID from debtor D, clientScheme CS where D._rowid = " & listDebtorID & _
                " and D.clientschemeID = CS._rowID"
            Dim debt_ds As DataSet = get_dataset("onestep", param2)
            Dim clientID As Integer = debt_ds.Tables(0).Rows(0).Item(8)
            'T79435 add DMI client codes 1883,1885,1899,1912
            If clientID <> 1815 And clientID <> 1736 And clientID <> 1766 And clientID <> 1523 And clientID <> 1839 And
                clientID <> 1883 And clientID <> 1885 And clientID <> 1899 And clientID <> 1912 And clientID <> 1932 Then
                MsgBox("List contains non TDX case number = " & listDebtorID)
                errorFound = True
                Exit For
            End If
            Dim status As String = debt_ds.Tables(0).Rows(0).Item(5)

            'ignore fully paid cases
            ' If status = "F" Then Continue For
            caseNo += 1
            Dim offenceLocation As String = ""
            Try
                offenceLocation = debt_ds.Tables(0).Rows(0).Item(0)
            Catch ex As Exception

            End Try

            Dim clientRef As String = debt_ds.Tables(0).Rows(0).Item(1)

            Dim offenceNumber As String = ""
            Try
                offenceNumber = debt_ds.Tables(0).Rows(0).Item(2)
            Catch ex As Exception

            End Try
            Dim offenceCourtname As String
            Try
                offenceCourtname = debt_ds.Tables(0).Rows(0).Item(3)
            Catch ex As Exception
                offenceCourtname = ""
            End Try
            Dim clientSchemeReturnID As Integer
            Try
                clientSchemeReturnID = debt_ds.Tables(0).Rows(0).Item(4)
            Catch ex As Exception
                clientSchemeReturnID = 0
            End Try


            'get brand from notes
            param2 = "select text from Note where debtorID = " & listDebtorID & _
                " and text like 'Client Brand: %'" & _
                " order by _rowid desc"
            Dim note_ds As DataSet = get_dataset("onestep", param2)
            Dim brand As String = ""
            If no_of_rows > 0 Then
                brand = note_ds.Tables(0).Rows(0).Item(0)
                brand = Microsoft.VisualBasic.Right(brand, brand.Length - 13)
                'remove % at end
                brand = Trim(Microsoft.VisualBasic.Left(brand, brand.Length - 1))
            End If

            'get client balance
            Dim clientBalance As Decimal = 0
            param2 = "select sum(fee_amount), sum(remited_fee) from fee where debtorID = " & listDebtorID &
                " and fee_remit_col < 3"
            Dim fee_ds As DataSet = get_dataset("onestep", param2)
            clientBalance = fee_ds.Tables(0).Rows(0).Item(0) - fee_ds.Tables(0).Rows(0).Item(1)
            totalBalance += clientBalance
            'get assignment from notes
            param2 = "select text from Note where debtorID = " & listDebtorID & _
                " and text like 'Assignment ID: %'" & _
                " order by _rowid desc"
            Dim note2_ds As DataSet = get_dataset("onestep", param2)
            Dim assignment As String = ""
            If no_of_rows > 0 Then
                assignment = note2_ds.Tables(0).Rows(0).Item(0)
                assignment = Microsoft.VisualBasic.Right(assignment, assignment.Length - 15)
                'remove % at end
                assignment = Trim(Microsoft.VisualBasic.Left(assignment, assignment.Length - 1))
            End If

            'get client return code
            Dim clientReturnCode As String = ""
            If status = "S" Then
                clientReturnCode = "PIF"
            ElseIf status = "X" Then
                clientReturnCode = "Time Expired"
            Else
                param2 = "select clientReturnCode from clientSchemeReturn where _rowid = " & clientSchemeReturnID
                Dim cs_ret_ds As DataSet = get_dataset("onestep", param2)
                Try
                    clientReturnCode = cs_ret_ds.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    'use our return code
                    Dim ourReturnCode As Integer
                    Try
                        ourReturnCode = debt_ds.Tables(0).Rows(0).Item(6)
                    Catch ex2 As Exception
                        ourReturnCode = 0
                    End Try
                    If ourReturnCode > 0 Then
                        Dim clientSchemeID As Integer = debt_ds.Tables(0).Rows(0).Item(7)
                        param2 = "select clientReturnCode from clientSchemeReturn where returnID = " & ourReturnCode &
                            " and clientschemeID = " & clientSchemeID
                        Dim cs2_ret_ds As DataSet = get_dataset("onestep", param2)
                        Try
                            clientReturnCode = cs2_ret_ds.Tables(0).Rows(0).Item(0)
                        Catch ex2 As Exception
                            'use our return reason
                            param2 = "select reason_short from codeReturns where _rowid = " & ourReturnCode
                            Dim cr_ds As DataSet = get_dataset("onestep", param2)
                            clientReturnCode = cr_ds.Tables(0).Rows(0).Item(0)
                        End Try
                    Else
                        clientReturnCode = "PIF"
                    End If
                End Try
            End If

            outfile &= Chr(34) & offenceLocation & Chr(34) & "," &
                Chr(34) & clientRef & Chr(34) & "," &
                Chr(34) & offenceNumber & Chr(34) & "," &
                Chr(34) & offenceCourtname & Chr(34) & "," &
                Chr(34) & brand & Chr(34) & "," &
                clientBalance & "," &
                Format(Now, "dd/MM/yyyy") & "," &
                Chr(34) & assignment & Chr(34) & "," &
                Chr(34) & clientReturnCode & Chr(34) & vbNewLine

            'save details in fees table
            If Not saveDetails Then
                Continue For
            End If

            upd_txt = "insert into TDXWriteOffs (tdx_listID,tdx_debtorID,tdx_date,tdx_user) " & _
                    " values(" & listID & "," & listDebtorID & ",'" & Format(Now, "dd MMM yyyy") & "','" & user & "')"
            update_sql(upd_txt)
        Next
        If Not errorFound Then
            'add header to outfile
            outfile = Chr(34) & "WriteOff            " & Chr(34) & "," & Format(Now, "dd/MM/yyyy") & "," & _
            Chr(34) & "RossendalesWriteoffs" & Format(Now, "yyyyMMdd") & Space(22) & Chr(34) & "," & _
            totalBalance & "," & caseNo & vbNewLine & outfile

            'add trailer
            outfile &= Chr(34) & "ROSSENDALES         " & Chr(34)

            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "CSV files |*.csv"
                .DefaultExt = ".csv"
                .OverwritePrompt = True
                .FileName = "RossendalesWriteoffs" & Format(Now, "YYYYMMDD") & ".csv"
            End With
            Dim filename As String
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                Application.DoEvents()
                MsgBox("Completed")
                My.Computer.FileSystem.WriteAllText(filename, outfile, False, ascii)
            End If
        End If
        Me.Close()
    End Sub

End Class

Module Module1
    Public file_name, error_message, error_path, log_path, log_file, error_file As String
    
    Sub write_error()
        Dim dir_info As IO.DirectoryInfo
        Try
            If IO.Directory.Exists(error_path) = False Then
                dir_info = IO.Directory.CreateDirectory(error_path)
            End If
            'save document
        Catch ex As Exception
            MsgBox("Unable to save error file")
        End Try
        My.Computer.FileSystem.WriteAllText(error_file, error_message, True)

    End Sub
End Module

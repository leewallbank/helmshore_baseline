Imports System.Collections
Imports CommonLibrary
Public Class mainfrm

    Dim subject As String = ""
    Dim fromaddress As String
    Dim toaddress As String = ""
    Dim cc1 As String = ""
    Dim body As String
    Dim error_found As Boolean = False
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        
    End Sub
    Sub send_email()
        Try
            If email(toaddress, fromaddress, subject, body, cc1) = 0 Then
                ' MsgBox("Email sent")
                Me.Close()
            Else
                MsgBox("See error log at H:\EmailAddressChanges\error_log")
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        error_path = "H:\EmailAddressChanges\"
        error_file = error_path & "error_log_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"
        Dim env_str As String = ""
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            env_str = "Test"
        End Try
       
        Dim yesterday As Date = DateAdd(DateInterval.Day, -1, Now)

        'check Reigate and Banstead cases for address change
        Dim debt_dt As New DataTable

        LoadDataTable("DebtRecovery", "SELECT d._rowid, d.client_ref, d.address, n.text, n._createddate " & _
                                                "FROM debtor as d" & _
                                                " INNER JOIN Note as n ON d._rowid = n.debtorID " & _
                                                " INNER JOIN clientScheme as cs ON d.clientSchemeID = cs._rowID" & _
                                                " and n.type = 'Address'" & _
                                                " and n._createdDate >= ' " & Format(yesterday, "yyyy-MM-dd") & "'" & _
                                                " and cs.clientID = 1775", debt_dt, False)
        Dim debtRows As Integer = debt_dt.Rows.Count
        Dim row As DataRow
        For Each row In debt_dt.Rows
            Dim debtorID As Integer = row.Item(0)
            Dim clientREf As String = row.Item(1)
            Dim createdDate As Date = row.Item(4)
            If Format(createdDate, "yyyy-MM-dd") <> Format(yesterday, "yyyy-MM-dd") Then
                Continue For
            End If

            'GET old address
            Dim oldAddress As String = row.Item(3)
            If Microsoft.VisualBasic.Left(oldAddress, 13) = "changed from:" Then
                oldAddress = Microsoft.VisualBasic.Right(oldAddress, oldAddress.Length - 13)
            Else
                Continue For
            End If
            Dim newAddress As String = row.Item(2)
            'remove carriage returns from new address
            Dim amendedNewAddress As String = ""
            For idx = 1 To newAddress.Length
                If Mid(newAddress, idx, 1) = Chr(10) Or Mid(newAddress, idx, 1) = Chr(13) Then
                    amendedNewAddress &= ", "
                Else
                    amendedNewAddress &= Mid(newAddress, idx, 1)
                End If
            Next
            fromaddress = "crystal@rossendales.com"
            If env_str <> "Prod" Then
                toaddress = "JBlundell@rossendales.com"
                cc1 = "crystal@rossendales.com"
            Else
                toaddress = "Lorraine.Steele@reigate-banstead.gov.uk"
                cc1 = "Ctaxclients@marstongroup.co.uk"
            End If
            subject = "Change Contact"
            body = "Case Number: " & debtorID & ", Client Reference: " & clientRef & vbNewLine
            body &= vbNewLine
            body &= "Old case address: " & oldAddress & vbNewLine
            body &= vbNewLine
            body &= "New Case Address: " & amendedNewAddress & vbNewLine
            body &= vbNewLine
            send_email()
        Next
        Me.Close()
    End Sub

   
End Class

﻿Imports CommonLibrary

Public Class clsLowellPreProcessData

    Private LastReturnTable As New DataTable

    Public Function GetConnIDName(ByVal ConnID As String) As String

        GetConnIDName = GetSQLResults("DebtRecovery", "SELECT CONCAT(s.nameLong, ': ', s.name) " & _
                                                      "FROM clientscheme AS cs " & _
                                                      "INNER JOIN scheme AS s ON cs.schemeID = s._rowid " & _
                                                      "WHERE cs._rowid = " & ConnID)
    End Function

    Public ReadOnly Property LastReturnDetails() As LastReturnDetails
        Get

            LastReturnDetails = Nothing

            Try

                LastReturnDetails = New LastReturnDetails
                LastReturnDetails.FileSequence = LastReturnTable.Rows(0).Item("FileSequence")

            Catch ex As Exception
                HandleException(ex)
            End Try
        End Get
    End Property

    Public Function GetCaseCount(ByVal ClientRef As String) As Integer

        GetCaseCount = 0

        Try

            GetCaseCount = GetSQLResults("DebtRecovery", "SELECT COUNT(*) " & _
                                                         "FROM debtor AS d " & _
                                                         "INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID " & _
                                                         "WHERE d.client_ref = '" & ClientRef & "' AND cs.clientID = 1815")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Sub GetLastRunDetails()
        Dim Sql As String

        Try
            Sql = "SELECT COUNT(*) AS FileSequence " & _
                  "FROM dbo.LowellInbound " & _
                  "WHERE CreatedDate > DATEADD(Day, DATEDIFF(DAY, 0, GETDATE()), 0) " & _
                  "  AND FileType = 'ONB_CONF'"

            LoadDataTable("FeesSQL", Sql, LastReturnTable)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub SetLastFileSequence(ByVal FileSequence As String, FileName As String, ByVal CasesLoaded As Integer)
        Try

            ExecStoredProc("FeesSQL", " EXEC dbo.SetLowellInbound 'ONB_CONF'," & FileSequence & ", '" & FileName & "', " & CasesLoaded.ToString & ", NULL, NULL")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

Public Class LastReturnDetails
    Public Property FileSequence As Integer
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.runbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.invNo1TBox = New System.Windows.Forms.TextBox()
        Me.invNo1Label = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.startdtp = New System.Windows.Forms.DateTimePicker()
        Me.enddtp = New System.Windows.Forms.DateTimePicker()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.InvNo2label = New System.Windows.Forms.Label()
        Me.InvNo2Tbox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.disp_tbox = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(127, 315)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(75, 23)
        Me.runbtn.TabIndex = 4
        Me.runbtn.Text = "Run Reports"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(277, 362)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 5
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'invNo1TBox
        '
        Me.invNo1TBox.Location = New System.Drawing.Point(66, 62)
        Me.invNo1TBox.Name = "invNo1TBox"
        Me.invNo1TBox.Size = New System.Drawing.Size(100, 20)
        Me.invNo1TBox.TabIndex = 0
        '
        'invNo1Label
        '
        Me.invNo1Label.AutoSize = True
        Me.invNo1Label.Location = New System.Drawing.Point(62, 46)
        Me.invNo1Label.Name = "invNo1Label"
        Me.invNo1Label.Size = New System.Drawing.Size(104, 13)
        Me.invNo1Label.TabIndex = 3
        Me.invNo1Label.Text = "First Invoice Number"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(138, 115)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Start date"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(138, 173)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "End Date"
        '
        'startdtp
        '
        Me.startdtp.Location = New System.Drawing.Point(100, 131)
        Me.startdtp.Name = "startdtp"
        Me.startdtp.Size = New System.Drawing.Size(145, 20)
        Me.startdtp.TabIndex = 2
        '
        'enddtp
        '
        Me.enddtp.Location = New System.Drawing.Point(100, 189)
        Me.enddtp.Name = "enddtp"
        Me.enddtp.Size = New System.Drawing.Size(145, 20)
        Me.enddtp.TabIndex = 3
        '
        'InvNo2label
        '
        Me.InvNo2label.AutoSize = True
        Me.InvNo2label.Location = New System.Drawing.Point(215, 46)
        Me.InvNo2label.Name = "InvNo2label"
        Me.InvNo2label.Size = New System.Drawing.Size(103, 13)
        Me.InvNo2label.TabIndex = 9
        Me.InvNo2label.Text = "2nd Invoice Number"
        '
        'InvNo2Tbox
        '
        Me.InvNo2Tbox.Location = New System.Drawing.Point(219, 62)
        Me.InvNo2Tbox.Name = "InvNo2Tbox"
        Me.InvNo2Tbox.Size = New System.Drawing.Size(100, 20)
        Me.InvNo2Tbox.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(137, 240)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(76, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "Disbursements"
        Me.Label3.Visible = False
        '
        'disp_tbox
        '
        Me.disp_tbox.Location = New System.Drawing.Point(127, 256)
        Me.disp_tbox.Name = "disp_tbox"
        Me.disp_tbox.Size = New System.Drawing.Size(100, 20)
        Me.disp_tbox.TabIndex = 11
        Me.disp_tbox.Text = "0.00"
        Me.disp_tbox.Visible = False
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(364, 425)
        Me.Controls.Add(Me.disp_tbox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.InvNo2label)
        Me.Controls.Add(Me.InvNo2Tbox)
        Me.Controls.Add(Me.enddtp)
        Me.Controls.Add(Me.startdtp)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.invNo1Label)
        Me.Controls.Add(Me.invNo1TBox)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "HSE Invoices"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents invNo1TBox As System.Windows.Forms.TextBox
    Friend WithEvents invNo1Label As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents startdtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents enddtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents InvNo2label As System.Windows.Forms.Label
    Friend WithEvents InvNo2Tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents disp_tbox As System.Windows.Forms.TextBox

End Class

﻿Imports System.IO

Public Class Form1
    Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
    Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
    Dim filename As String = ""
    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(sender As System.Object, e As System.EventArgs) Handles runbtn.Click

        'Dim disbursements As Decimal = 0
        'Try
        '    disbursements = disp_tbox.Text
        'Catch ex As Exception
        '    MsgBox("Invalid disbursement amount")
        '    Exit Sub
        'End Try



        Dim RD516report = New RD516
        Dim myArrayList1 As ArrayList = New ArrayList()
        Dim parmInv As String = invNo1TBox.Text.ToString
        myArrayList1.Add(parmInv)
        SetCurrentValuesForParameterField1(RD516report, myArrayList1)
        myArrayList1.Add(startdtp.Value)
        SetCurrentValuesForParameterField2(RD516report, myArrayList1)
        myArrayList1.Add(enddtp.Value)
        SetCurrentValuesForParameterField3(RD516report, myArrayList1)
        'myArrayList1.Add(disbursements)
        'SetCurrentValuesForParameterField4(RD516report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        myConnectionInfo2.ServerName = "RossFeeTable"
        myConnectionInfo2.DatabaseName = "FeesSQL"
        myConnectionInfo2.UserID = "sa"
        myConnectionInfo2.Password = "sa"
        SetDBLogonForReport(myConnectionInfo, RD516report, myConnectionInfo2)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RD516 HSE Invoice.pdf"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = SaveFileDialog1.FileName
        Else
            MsgBox("Report not saved")
            Me.Close()
            Exit Sub
        End If
        exitbtn.Enabled = False
        runbtn.Enabled = False
        Application.DoEvents()
        Dim InputFilePath As String = Path.GetDirectoryName(filename)

        RD516report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD516report.Close()


        Dim RD517report = New RD517
        Dim myArrayList2 As ArrayList = New ArrayList()
        myArrayList2.Add(startdtp.Value)
        SetCurrentValuesForParameterField2(RD517report, myArrayList2)
        myArrayList2.Add(enddtp.Value)
        SetCurrentValuesForParameterField3(RD517report, myArrayList2)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        myConnectionInfo2.ServerName = "RossFeeTable"
        myConnectionInfo2.DatabaseName = "FeesSQL"
        myConnectionInfo2.UserID = "sa"
        myConnectionInfo2.Password = "sa"
        SetDBLogonForReport(myConnectionInfo, RD517report, myConnectionInfo2)

        filename = InputFilePath & "\RD517 HSE Payments.pdf"
        RD517report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD517report.Close()

        Dim RD516Breport = New RD516B
        Dim myArrayList3 As ArrayList = New ArrayList()
        myArrayList3.Add(InvNo2Tbox.Text)
        SetCurrentValuesForParameterField1(RD516Breport, myArrayList3)
        myArrayList3.Add(startdtp.Value)
        SetCurrentValuesForParameterField2(RD516Breport, myArrayList3)
        myArrayList3.Add(enddtp.Value)
        SetCurrentValuesForParameterField3(RD516Breport, myArrayList3)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        myConnectionInfo2.ServerName = "RossFeeTable"
        myConnectionInfo2.DatabaseName = "FeesSQL"
        myConnectionInfo2.UserID = "sa"
        myConnectionInfo2.Password = "sa"
        SetDBLogonForReport(myConnectionInfo, RD516Breport, myConnectionInfo2)
        filename = InputFilePath & "\RD516B HSE 2nd Invoice.pdf"
        RD516Breport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD516Breport.Close()

        Dim RD517Breport = New RD517B
        Dim myArrayList4 As ArrayList = New ArrayList()
        myArrayList4.Add(startdtp.Value)
        SetCurrentValuesForParameterField2(RD517Breport, myArrayList4)
        myArrayList4.Add(enddtp.Value)
        SetCurrentValuesForParameterField3(RD517Breport, myArrayList4)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        myConnectionInfo2.ServerName = "RossFeeTable"
        myConnectionInfo2.DatabaseName = "FeesSQL"
        myConnectionInfo2.UserID = "sa"
        myConnectionInfo2.Password = "sa"
        SetDBLogonForReport(myConnectionInfo, RD517Breport, myConnectionInfo2)
        filename = InputFilePath & "\RD517B HSE 2nd Payments.pdf"
        RD517Breport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD517Breport.Close()
        MsgBox("Reports saved")

        Me.Close()
    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

﻿Imports CommonLibrary

Public Class clsHMRCData
    Private ErrorField As String() = {"Error01DOB", "Error02FullUniqueRef", "Error03HOD", "Error04PaymentRefNo", "Error05TaxPayerName", "Error06TPAddressLine1", "Error07TotalDebtOutstanding", "Error08NoOfWorkItems", "Error10CollectableAmountOfWorkItem", "Error11SumTotalOfCollectableAmounts"}
    Private DTHMRCCaseBatch As DataTable = New DataTable

    Public ReadOnly Property HMRCCaseBatch As DataTable
        Get
            HMRCCaseBatch = DTHMRCCaseBatch
        End Get
    End Property

    Public Function AddHMRCCaseBatchDetail(FileGenerationReference As String, RecordSequence As Integer, ClientRef As String, RowError As String()) As Boolean

        AddHMRCCaseBatchDetail = False

        Try
            Dim SQL As String = "INSERT INTO dbo.HMRCCaseBatchDetail (FileGenerationReference, CreatedDate, CreatedBy, RecordSequence, ClientRef, RejectRecord"

            If Not RowError Is Nothing AndAlso CInt(RowError(0)) >= 0 Then SQL &= ", " & ErrorField(CInt(RowError(0)))

            SQL &= ") " & vbCrLf & _
                   "VALUES ('" & FileGenerationReference & "', GETDATE(), SYSTEM_USER, " & RecordSequence.ToString & ", '" & ClientRef & "'"

            If Not RowError Is Nothing AndAlso CInt(RowError(0)) >= 0 Then
                SQL &= ", 'Y', '" & RowError(1) & "')"
            Else
                SQL &= ", 'N')"
            End If

            ExecStoredProc("FeesSQL", SQL)

            AddHMRCCaseBatchDetail = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Function UpdateHMRCCaseBatchDetail(ByVal FileGenerationReference As String, RecordSequence As String) As Boolean

        UpdateHMRCCaseBatchDetail = False

        Try
            Dim Results As Object() = GetSQLResultsArray("DebtRecovery", "SELECT d._rowID, d.debt_original " & _
                                                         "FROM debtor AS d " & _
                                                         "INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID " & _
                                                         "WHERE d.prevReference = '" & RecordSequence & "' " & _
                                                         "  AND client_batch = '" & Microsoft.VisualBasic.Left(FileGenerationReference.ToString, 15) & "'" & _
                                                         "  AND cs.clientID = 1275")

            If Results.Length > 0 Then
                UpdateHMRCCaseBatchDetail = True
                ExecStoredProc("FeesSQL", "UPDATE HMRCCaseBatchDetail SET CaseRef = " & Results(0).ToString & " WHERE FileGenerationReference = '" & FileGenerationReference & "' AND RecordSequence = '" & RecordSequence & "'")
                frmMain.TotalLoadedCasesCount += 1
                frmMain.TotalLoadedCasesValue += CDec(Results(1))
            Else
                UpdateHMRCCaseBatchDetail = False
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Function AddNewHMRCBatch(FileGenerationReference As String, AccountsLoaded As Integer, AccountsRejected As Integer, HeaderValidation As String, TrailerValidation As String, ColumnsValidation As String, RejectPercentage As Double, FileType As String, AccountsRejectedValue As Decimal, HMRCFinancialYear As Integer) As Boolean

        AddNewHMRCBatch = False

        Try
            Dim SQL As String = "INSERT INTO dbo.HMRCCaseBatch ( FileGenerationReference, CreatedDate, CreatedBy, AccountsLoaded, AccountsRejected, HeaderValidation, TrailerValidation" & _
                                                               ", ColumnsValidation, RejectPercentage, FileType, AccountsLoadedValue, AccountsRejectedValue, HMRCFinancialYear) " & _
                                "VALUES ('" & FileGenerationReference & "', GETDATE(), SYSTEM_USER, 0, " & AccountsRejected.ToString & ", '" & HeaderValidation & "', '" & TrailerValidation & _
                                                               "', '" & ColumnsValidation & "', " & RejectPercentage.ToString & ", '" & FileType & "', 0, " & AccountsRejectedValue & ", " & HMRCFinancialYear & ")"

            ExecStoredProc("FeesSQL", SQL)

            AddNewHMRCBatch = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Function UpdateHMRCCaseBatch(ByVal FileGenerationReference As String) As Boolean

        UpdateHMRCCaseBatch = False

        Try
            Dim SQL As String = "UPDATE HMRCCaseBatch SET AccountsLoaded = " & frmMain.TotalLoadedCasesCount & ", AccountsLoadedValue = " & frmMain.TotalLoadedCasesValue & " WHERE FileGenerationReference = '" & FileGenerationReference & "'"

            ExecStoredProc("FeesSQL", SQL)

            UpdateHMRCCaseBatch = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Sub GetHMRCCaseBatch(ByVal FileGenerationReference As String)

        Try
            Dim SQL As String = "SELECT RecordSequence, 'ROSS' + CAST(CaseRef AS VARCHAR) AS CaseRef, RejectRecord, Error01DOB, Error02FullUniqueRef, Error03HOD, Error04PaymentRefNo, Error05TaxPayerName, Error06TPAddressLine1, " & _
                                "       Error07TotalDebtOutstanding, Error10CollectableAmountOfWorkItem, Error11SumTotalOfCollectableAmounts, Error08NoOfWorkItems, ClientRef " & _
                                "FROM dbo.HMRCCaseBatchDetail " & _
                                "WHERE FileGenerationReference = '" & FileGenerationReference & "' " & _
                                "ORDER BY RecordSequence"

            LoadDataTable("FeesSQL", SQL, DTHMRCCaseBatch)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function GetFileGenerationReferenceCount(ByVal FileGenerationReference As String) As Integer

        GetFileGenerationReferenceCount = 0

        Try
            GetFileGenerationReferenceCount += CInt(GetSQLResults("FeesSQL", "SELECT COUNT(*) " & _
                                                                             "FROM dbo.HMRCCaseBatchDetail " & _
                                                                             "WHERE FileGenerationReference = '" & FileGenerationReference & "'"))

            GetFileGenerationReferenceCount += CInt(GetSQLResults("FeesSQL", "SELECT COUNT(*) " & _
                                                                             "FROM dbo.HMRCCaseBatch " & _
                                                                             "WHERE FileGenerationReference = '" & FileGenerationReference & "'"))

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Function ClearBatch(ByVal FileGenerationReference As String) As Boolean

        ClearBatch = False

        Try
            ExecStoredProc("FeesSQL", "DELETE " & _
                                      "FROM dbo.HMRCCaseBatchDetail " & _
                                      "WHERE FileGenerationReference = '" & FileGenerationReference & "'")

            ExecStoredProc("FeesSQL", "DELETE " & _
                                      "FROM dbo.HMRCCaseBatch " & _
                                      "WHERE FileGenerationReference = '" & FileGenerationReference & "'")

            ClearBatch = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

End Class

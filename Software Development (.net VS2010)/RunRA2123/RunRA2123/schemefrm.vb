Public Class schemefrm

    Private Sub schemefrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'get all schemes for client
        
        sch_dg.Rows.Clear()
        Dim sch_ds As New DataTable
        LoadDataTable2("DebtRecovery", "select S._rowid, S.name from Scheme S, clientScheme CS " & _
                   " where S._rowid = CS.schemeID " & _
                   " and (CS.branchID = 1 or CS.branchID = 10 or CS.branchID = 21 or CS.branchID = 24) " & _
                   " and CS.clientID = " & clID, sch_ds, False)
        Dim schRow As DataRow
        For Each schRow In sch_ds.Rows
            sch_dg.Rows.Add(False, schRow(1), schRow(0))
        Next
    End Sub
End Class
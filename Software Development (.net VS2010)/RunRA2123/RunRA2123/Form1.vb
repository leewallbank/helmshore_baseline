Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        debtyearcbox.SelectedIndex = 0

    End Sub

    Private Sub schbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        
    End Sub

    Private Sub clbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clientbtn.Click
        clientfrm.ShowDialog()
        schemefrm.ShowDialog()
        schemefrm.sch_dg.EndEdit()
        clientbtn.Enabled = False
        disable_btns()

        'select report
        If valuesrbtn.Checked Then
            run_RA2123CV()
        Else
            run_RA2123CN()
        End If
      
        Me.Close()
    End Sub
    Private Sub disable_btns()
        datebtn.Enabled = False
        clientbtn.Enabled = False
        branchbtn.Enabled = False
        exitbtn.Enabled = False
    End Sub

    Private Sub run_RA2123CN()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123CNreport = New RA2123CN
        Dim myArrayList1 As ArrayList = New ArrayList()
      
        Dim debt_year As String = "2014"
        If debtyearcbox.SelectedIndex = 1 Then
            debt_year = "2015"
        End If
        If debtyearcbox.SelectedIndex = 2 Then
            debt_year = "2016"
        End If
        If debtyearcbox.SelectedIndex = 3 Then
            debt_year = "2017"
        End If
        myArrayList1.Add(debt_year)
        SetCurrentValuesForParameterField6(RA2123CNreport, myArrayList1)

        Dim startdate As Date = CDate("Jan 1, 1900")
        Dim endDate As Date = CDate("Jan 1, 2100")
        If startdtp.Visible Then
            startdate = startdtp.Value
            endDate = enddtp.Value
        End If
        myArrayList1.Add(startdate)
        SetCurrentValuesForParameterField3(RA2123CNreport, myArrayList1)
        myArrayList1.Add(endDate)
        SetCurrentValuesForParameterField4(RA2123CNreport, myArrayList1)

        myArrayList1.Add(clID)
        SetCurrentValuesForParameterField1(RA2123CNreport, myArrayList1)
        Dim myArrayList2 As ArrayList = New ArrayList()
        Dim schRow As DataGridViewRow
        For Each schRow In schemefrm.sch_dg.Rows
            If schRow.Cells(0).Value Then
                Dim schID As Integer = schRow.Cells(2).Value
                myArrayList2.Add(schID)
                SetCurrentValuesForParameterField2(RA2123CNreport, myArrayList2)
            End If
        Next

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123CNreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123CN Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123CNreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123CNreport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2123CV()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123CVreport = New RA2123CV
        Dim myArrayList1 As ArrayList = New ArrayList()

        Dim debt_year As String = "2014"
        If debtyearcbox.SelectedIndex = 1 Then
            debt_year = "2015"
        End If
        If debtyearcbox.SelectedIndex = 2 Then
            debt_year = "2016"
        End If
        If debtyearcbox.SelectedIndex = 3 Then
            debt_year = "2017"
        End If
        myArrayList1.Add(debt_year)
        SetCurrentValuesForParameterField6(RA2123CVreport, myArrayList1)

        Dim startdate As Date = CDate("Jan 1, 1900")
        Dim endDate As Date = CDate("Jan 1, 2100")
        If startdtp.Visible Then
            startdate = startdtp.Value
            endDate = enddtp.Value
        End If
        myArrayList1.Add(startdate)
        SetCurrentValuesForParameterField3(RA2123CVreport, myArrayList1)
        myArrayList1.Add(endDate)
        SetCurrentValuesForParameterField4(RA2123CVreport, myArrayList1)

        myArrayList1.Add(clID)
        SetCurrentValuesForParameterField1(RA2123CVreport, myArrayList1)
        Dim myArrayList2 As ArrayList = New ArrayList()
        Dim schRow As DataGridViewRow
        Dim schemeIDX As Integer = 0
        For Each schRow In schemefrm.sch_dg.Rows
            If schRow.Cells(0).Value Then
                Dim schID As Integer = schRow.Cells(2).Value
                myArrayList2.Add(schID)
            End If
        Next

        SetCurrentValuesForParameterField2(RA2123CVreport, myArrayList2)

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123CVreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123CV Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123CVreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123CVreport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2123XN()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123XNreport = New RA2123XN
        Dim myArrayList1 As ArrayList = New ArrayList()

        Dim debt_year As String = "2014"
        If debtyearcbox.SelectedIndex = 1 Then
            debt_year = "2015"
        End If
        If debtyearcbox.SelectedIndex = 2 Then
            debt_year = "2016"
        End If
        If debtyearcbox.SelectedIndex = 3 Then
            debt_year = "2017"
        End If
        myArrayList1.Add(debt_year)
        SetCurrentValuesForParameterField6(RA2123XNreport, myArrayList1)

        Dim startdate As Date = CDate("Jan 1, 1900")
        Dim endDate As Date = CDate("Jan 1, 2100")
        If startdtp.Visible Then
            startdate = startdtp.Value
            endDate = enddtp.Value
        End If
        myArrayList1.Add(startdate)
        SetCurrentValuesForParameterField3(RA2123XNreport, myArrayList1)
        myArrayList1.Add(endDate)
        SetCurrentValuesForParameterField4(RA2123XNreport, myArrayList1)

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123XNreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123XN Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123XNreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123XNreport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2123XV()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123XVreport = New RA2123XV
        Dim myArrayList1 As ArrayList = New ArrayList()

        Dim debt_year As String = "2014"
        If debtyearcbox.SelectedIndex = 1 Then
            debt_year = "2015"
        End If
        If debtyearcbox.SelectedIndex = 2 Then
            debt_year = "2016"
        End If
        If debtyearcbox.SelectedIndex = 3 Then
            debt_year = "2017"
        End If
        myArrayList1.Add(debt_year)
        SetCurrentValuesForParameterField6(RA2123XVreport, myArrayList1)

        Dim startdate As Date = CDate("Jan 1, 1900")
        Dim endDate As Date = CDate("Jan 1, 2100")
        If startdtp.Visible Then
            startdate = startdtp.Value
            endDate = enddtp.Value
        End If
        myArrayList1.Add(startdate)
        SetCurrentValuesForParameterField3(RA2123XVreport, myArrayList1)
        myArrayList1.Add(endDate)
        SetCurrentValuesForParameterField4(RA2123XVreport, myArrayList1)

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123XVreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123XV Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123XVreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123XVreport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2123BV(ByVal branch_no As Integer)
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123BVreport = New RA2123BV
        Dim myArrayList1 As ArrayList = New ArrayList()

        Dim debt_year As String = "2014"
        If debtyearcbox.SelectedIndex = 1 Then
            debt_year = "2015"
        End If
        If debtyearcbox.SelectedIndex = 2 Then
            debt_year = "2016"
        End If
        If debtyearcbox.SelectedIndex = 3 Then
            debt_year = "2017"
        End If
        myArrayList1.Add(debt_year)
        SetCurrentValuesForParameterField6(RA2123BVreport, myArrayList1)

        Dim startdate As Date = CDate("Jan 1, 1900")
        Dim endDate As Date = CDate("Jan 1, 2100")
        If startdtp.Visible Then
            startdate = startdtp.Value
            endDate = enddtp.Value
        End If
        myArrayList1.Add(startdate)
        SetCurrentValuesForParameterField3(RA2123BVreport, myArrayList1)
        myArrayList1.Add(endDate)
        SetCurrentValuesForParameterField4(RA2123BVreport, myArrayList1)
        myArrayList1.Add(branch_no)
        SetCurrentValuesForParameterField5(RA2123BVreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123BVreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123BV Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123BVreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123BVreport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2123BSV(ByVal branch_no As Integer)
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123BSVreport = New RA2123BSV
        Dim myArrayList1 As ArrayList = New ArrayList()

        Dim debt_year As String = "2014"
        If debtyearcbox.SelectedIndex = 1 Then
            debt_year = "2015"
        End If
        If debtyearcbox.SelectedIndex = 2 Then
            debt_year = "2016"
        End If
        If debtyearcbox.SelectedIndex = 3 Then
            debt_year = "2017"
        End If
        myArrayList1.Add(debt_year)
        SetCurrentValuesForParameterField6(RA2123BSVreport, myArrayList1)

        Dim startdate As Date = CDate("Jan 1, 1900")
        Dim endDate As Date = CDate("Jan 1, 2100")
        If startdtp.Visible Then
            startdate = startdtp.Value
            endDate = enddtp.Value
        End If
        myArrayList1.Add(startdate)
        SetCurrentValuesForParameterField3(RA2123BSVreport, myArrayList1)
        myArrayList1.Add(endDate)
        SetCurrentValuesForParameterField4(RA2123BSVreport, myArrayList1)
        myArrayList1.Add(branch_no)
        SetCurrentValuesForParameterField5(RA2123BSVreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123BSVreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123BSV Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123BSVreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123BSVreport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2123BSN(ByVal branch_no As Integer)
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123BSNreport = New RA2123BSN
        Dim myArrayList1 As ArrayList = New ArrayList()

        Dim debt_year As String = "2014"
        If debtyearcbox.SelectedIndex = 1 Then
            debt_year = "2015"
        End If
        If debtyearcbox.SelectedIndex = 2 Then
            debt_year = "2016"
        End If
        If debtyearcbox.SelectedIndex = 3 Then
            debt_year = "2017"
        End If
        myArrayList1.Add(debt_year)
        SetCurrentValuesForParameterField6(RA2123BSNreport, myArrayList1)

        Dim startdate As Date = CDate("Jan 1, 1900")
        Dim endDate As Date = CDate("Jan 1, 2100")
        If startdtp.Visible Then
            startdate = startdtp.Value
            endDate = enddtp.Value
        End If
        myArrayList1.Add(startdate)
        SetCurrentValuesForParameterField3(RA2123BSNreport, myArrayList1)
        myArrayList1.Add(endDate)
        SetCurrentValuesForParameterField4(RA2123BSNreport, myArrayList1)
        myArrayList1.Add(branch_no)
        SetCurrentValuesForParameterField5(RA2123BSNreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123BSNreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123BSN Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123BSNreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123BSNreport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2123BN(ByVal branch_no As Integer)
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2123BNreport = New RA2123BN
        Dim myArrayList1 As ArrayList = New ArrayList()

        Dim debt_year As String = "2014"
        If debtyearcbox.SelectedIndex = 1 Then
            debt_year = "2015"
        End If
        If debtyearcbox.SelectedIndex = 2 Then
            debt_year = "2016"
        End If
        If debtyearcbox.SelectedIndex = 3 Then
            debt_year = "2017"
        End If
        myArrayList1.Add(debt_year)
        SetCurrentValuesForParameterField6(RA2123BNreport, myArrayList1)

        Dim startdate As Date = CDate("Jan 1, 1900")
        Dim endDate As Date = CDate("Jan 1, 2100")
        If startdtp.Visible Then
            startdate = startdtp.Value
            endDate = enddtp.Value
        End If
        myArrayList1.Add(startdate)
        SetCurrentValuesForParameterField3(RA2123BNreport, myArrayList1)
        myArrayList1.Add(endDate)
        SetCurrentValuesForParameterField4(RA2123BNreport, myArrayList1)
        myArrayList1.Add(branch_no)
        SetCurrentValuesForParameterField5(RA2123BNreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2123BNreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2123BN Jacobs report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2123BNreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2123BNreport.close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub datebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles datebtn.Click
        If startlbl.Visible Then
            startlbl.Visible = False
            startdtp.Visible = False
            endlbl.Visible = False
            enddtp.Visible = False
        Else
            startlbl.Visible = True
            startdtp.Visible = True
            endlbl.Visible = True
            enddtp.Visible = True
        End If
    End Sub

    Private Sub selectrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles selectrbtn.CheckedChanged
        clientlbl.Visible = selectrbtn.Checked
        clientbtn.Visible = selectrbtn.Checked
        cl_tbox.Visible = selectrbtn.Checked
    End Sub

    Private Sub allrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allrbtn.CheckedChanged
        If allrbtn.Checked Then
            If MsgBox("Run for all clients", MsgBoxStyle.YesNo, "Run for ALL clients") = MsgBoxResult.Yes Then
                exitbtn.Enabled = False
                datebtn.Enabled = False
                If valuesrbtn.Checked Then
                    run_RA2123XV()
                Else
                    run_RA2123XN()
                End If
                Me.Close()
            End If

            End If
    End Sub

    Private Sub brrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles brrbtn.CheckedChanged
        branchbtn.Visible = brrbtn.Checked

    End Sub

    Private Sub branch_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub branchrbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles branchbtn.Click
        Try
            branch_no = InputBox("Enter branch no", "Enter branch no")
        Catch ex As Exception
            MsgBox("Invalid branch number")
            Exit Sub
        End Try
        disable_btns()
        Dim schemes As Boolean = False
        If MsgBox("Do you want breakdown by schemes?", MsgBoxStyle.YesNo, "Scheme Breakdown") = MsgBoxResult.Yes Then
            schemes = True
        End If

        If valuesrbtn.Checked Then
            If schemes Then
                run_RA2123BSV(branch_no)
            Else
                run_RA2123BV(branch_no)
            End If
        Else
            If schemes Then
                run_RA2123BSN(branch_no)
            Else
                run_RA2123BN(branch_no)
            End If
        End If
            Me.Close()
    End Sub

    Private Sub cl_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cl_tbox.TextChanged

    End Sub

    Private Sub debtyearcbox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles debtyearcbox.SelectedIndexChanged

    End Sub
End Class

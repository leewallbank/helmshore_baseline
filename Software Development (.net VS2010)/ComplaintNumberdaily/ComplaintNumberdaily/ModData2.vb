﻿Imports System.Configuration
Imports System.Data
Imports System.Data.Odbc
Imports System.Data.SqlClient

Module ModData2
    Public sqlCon As New SqlConnection
    
    Sub update_sql(ByVal upd_txt As String)
        Try
            If sqlCon.State = ConnectionState.Closed Then
                Connect_sqlDb()
            End If
            Dim sqlCMD As SqlCommand

            'Define attachment to database table specifics
            sqlCMD = New SqlCommand
            With sqlCMD
                .Connection = sqlCon
                .CommandText = upd_txt
                .ExecuteNonQuery()
            End With

            'Clean up the connection
            sqlCMD = Nothing

        Catch ex As Exception

        End Try
    End Sub
    Public Sub Connect_sqlDb()
        Dim conn_str As String = ""
        Try
            If Not IsNothing(sqlCon) Then
                'This is only necessary following an exception...
                If sqlCon.State = ConnectionState.Open Then sqlCon.Close()
            End If

            conn_str = "Complaints"

            sqlCon.ConnectionString = ConfigurationManager.ConnectionStrings(conn_str).ConnectionString
            sqlCon.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings(conn_str).ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings(conn_str).ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub

    

    

End Module

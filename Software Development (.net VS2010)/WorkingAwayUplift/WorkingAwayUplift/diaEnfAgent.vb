﻿Public Class diaEnfAgent

    Private UpliftData As New UpliftData

    Public ReadOnly Property BailiffID() As String
        Get
            Return lstEnfAgent.SelectedValue
        End Get
    End Property

    Public ReadOnly Property BailiffName() As String
        Get
            Return lstEnfAgent.SelectedItem("BailiffName").ToString
        End Get
    End Property

    Private Sub diaBailiff_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        UpliftData.GetBailiffInclude(False)

        lstEnfAgent.DataSource = UpliftData.BailiffInclude
        lstEnfAgent.ValueMember = "BailiffID"
        lstEnfAgent.DisplayMember = "BailiffName"

    End Sub

    Private Sub btnOK_Click(sender As Object, e As System.EventArgs) Handles btnOK.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As System.EventArgs)
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub
End Class
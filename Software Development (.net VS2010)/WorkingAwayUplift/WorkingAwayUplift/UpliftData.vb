﻿Imports CommonLibrary

Public Class UpliftData

    Private BailiffIncludeTable As New DataTable
    Private BailiffContinuousTable As New DataTable
    Private UpliftIncludeTable As New DataTable
    Private UpliftContinuousTable As New DataTable
    Private AuditTable As New DataTable

    Private BailiffIncludeView As New DataView
    Private BailiffContinuousView As New DataView
    Private UpliftIncludeView As New DataView
    Private UpliftContinuousView As New DataView
    Private AuditView As New DataView

    Public ReadOnly Property BailiffInclude() As DataView
        Get
            BailiffInclude = BailiffIncludeView
        End Get
    End Property

    Public ReadOnly Property BailiffContinuous() As DataView
        Get
            BailiffContinuous = BailiffContinuousView
        End Get
    End Property

    Public ReadOnly Property UpliftInclude() As DataView
        Get
            UpliftInclude = UpliftIncludeView
        End Get
    End Property

    Public ReadOnly Property UpliftContinuous() As DataView
        Get
            UpliftContinuous = UpliftContinuousView
        End Get
    End Property

    Public ReadOnly Property Audit() As DataView
        Get
            Audit = AuditView
        End Get
    End Property

    Public Sub GetBailiffInclude(IncludeAllOption As Boolean)
        Try

            BailiffIncludeTable.Clear()

            If IncludeAllOption Then LoadDataTable("BailiffRemuneration", "SELECT -1 AS BailiffID, '<All>' AS BailiffName", BailiffIncludeTable, False)

            LoadDataTable("BailiffRemuneration", "EXEC dbo.GetBailiff 0", BailiffIncludeTable, True)

            BailiffIncludeView = New DataView(BailiffIncludeTable)
            BailiffIncludeView.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetBailiffContinuous()
        Try

            LoadDataTable("BailiffRemuneration", "EXEC dbo.GetBailiff 1", BailiffContinuousTable, False)

            BailiffContinuousView = New DataView(BailiffContinuousTable)
            BailiffContinuousView.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetUpliftInclude(BailiffID As String, Optional ByRef DataTable As DataTable = Nothing)
        Try

            If IsNothing(DataTable) Then DataTable = UpliftIncludeTable

            LoadDataTable("BailiffRemuneration", "EXEC dbo.GetUpliftInclude " & BailiffID, DataTable, False)

            UpliftIncludeView = New DataView(UpliftIncludeTable)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetUpliftContinuous(ByVal BailiffID As String, Optional ByRef DataTable As DataTable = Nothing)
        Try

            If IsNothing(DataTable) Then DataTable = UpliftContinuousTable

            LoadDataTable("BailiffRemuneration", "EXEC dbo.GetUpliftContinuous " & BailiffID, DataTable, False)

            UpliftContinuousView = New DataView(UpliftContinuousTable)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub SetUpliftInclude(UpliftID As String, BailiffID As String, AllocationDate As String, UpliftAmount As String, EndDate As String)
        Try
            Dim Sql As String = "EXEC dbo.SetUpliftInclude "

            If UpliftID = "" Then
                Sql &= "NULL"
            Else
                Sql &= UpliftID.ToString
            End If

            Sql &= ", " & BailiffID.ToString
            Sql &= ", '" & CDate(AllocationDate).ToString("yyyy-MM-dd") & "'"
            Sql &= ", " & UpliftAmount.ToString

            If EndDate = "" Then
                Sql &= ", NULL"
            Else
                Sql &= ", '" & CDate(EndDate).ToString("yyyy-MM-dd") & "'"
            End If

            ExecStoredProc("BailiffRemuneration", Sql, 600)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub DeleteUpliftInclude(UpliftID As String)
        Try

            ExecStoredProc("BailiffRemuneration", "EXEC dbo.DeleteUpliftInclude " & UpliftID.ToString, 600)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub SetUpliftContinuous(UpliftID As String, BailiffID As String, AllocationDate As String, EndDate As String)
        Try
            Dim Sql As String = "EXEC dbo.SetUpliftContinuous "

            If UpliftID = "" Then
                Sql &= "NULL"
            Else
                Sql &= UpliftID.ToString
            End If

            Sql &= ", " & BailiffID.ToString
            Sql &= ", '" & CDate(AllocationDate).ToString("yyyy-MM-dd") & "'"

            If EndDate = "" Then
                Sql &= ", NULL"
            Else
                Sql &= ", '" & CDate(EndDate).ToString("yyyy-MM-dd") & "'"
            End If

            ExecStoredProc("BailiffRemuneration", Sql, 600)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub DeleteUpliftContinuous(UpliftID As String)
        Try

            ExecStoredProc("BailiffRemuneration", "EXEC dbo.DeleteUpliftContinuous " & UpliftID.ToString, 600)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub


    Public Sub GetAudit(BailiffID As String)
        Try

            LoadDataTable("BailiffRemuneration", "EXEC dbo.GetAudit " & BailiffID, AuditTable, False)

            AuditView = New DataView(AuditTable)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function ValidateDateAllocated(BailiffID As String, AllocationDate As DateTime) As Boolean
        ValidateDateAllocated = False
        Try

            Dim Sql As String = "SELECT COUNT(*) " & _
                                "FROM visit " & _
                                "WHERE BailiffID = " & BailiffID & " " & _
                                "  AND date_allocated = '" & AllocationDate.ToString("yyyy-MM-dd") & "'"

            If CInt(GetSQLResults("DebtRecovery", Sql)) > 0 Then ValidateDateAllocated = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Function IncludesContains(BailiffID As String, AllocationDate As DateTime) As Boolean
        IncludesContains = False
        Try
            Dim QueryTable As New DataTable

            GetUpliftInclude(BailiffID, QueryTable)

            Dim SelectResults() As DataRow = QueryTable.Select("BailiffID = " & BailiffID & " AND AllocationDate = #" & AllocationDate.ToString("dd-MMM-yyyy") & "#")

            If SelectResults.Length > 0 Then IncludesContains = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function


    Public Function ContinuousContains(BailiffID As String, AllocationDate As String) As Boolean
        ContinuousContains = False
        Try
            Dim QueryTable As New DataTable

            GetUpliftContinuous(BailiffID, QueryTable)

            Dim SelectResults() As DataRow = QueryTable.Select("AllocationDate = #" & CDate(AllocationDate).ToString("dd-MMM-yyyy") & "#")

            If SelectResults.Length > 0 Then ContinuousContains = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

End Class


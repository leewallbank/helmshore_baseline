﻿Imports CommonLibrary
Imports System.Globalization

Public Class frmMain
    Private UpliftData As New UpliftData
    Private SelectedBailiffID As Integer

    Private Sub frmMain_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try

            UpliftData.GetBailiffInclude(True)

            cboEnfAgent.DataSource = UpliftData.BailiffInclude
            cboEnfAgent.ValueMember = "BailiffID"
            cboEnfAgent.DisplayMember = "BailiffName"

            SelectedBailiffID = cboEnfAgent.SelectedValue

            UpliftData.GetBailiffContinuous()

            dgvEnfAgent.DataSource = UpliftData.BailiffContinuous

            UpliftData.GetUpliftInclude(cboEnfAgent.SelectedValue)

            dgvUpliftInclude.DataSource = UpliftData.UpliftInclude

            AddHandler dgvUpliftInclude.RowsAdded, AddressOf dgvUpliftInclude_RowsAdded
            AddHandler cboEnfAgent.SelectedValueChanged, AddressOf cboBailiff_SelectedValueChanged
            AddHandler dgvUpliftInclude.CellValidating, AddressOf dgvUpliftInclude_CellValidating

            dgvUpliftInclude.FirstDisplayedScrollingRowIndex = dgvUpliftInclude.RowCount - 1

            btnAuditInclude.Enabled = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            If Not DiscardChanges() Then e.Cancel = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboBailiff_SelectedValueChanged(sender As Object, e As System.EventArgs) 'Handles cboBailiff.SelectedValueChanged
        Try

            If Not DiscardChanges() Then

                RemoveHandler cboEnfAgent.SelectedValueChanged, AddressOf cboBailiff_SelectedValueChanged
                cboEnfAgent.SelectedValue = SelectedBailiffID
                AddHandler cboEnfAgent.SelectedValueChanged, AddressOf cboBailiff_SelectedValueChanged

            Else

                RefreshInclude()

            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub RefreshInclude()

        RemoveHandler dgvUpliftInclude.RowsAdded, AddressOf dgvUpliftInclude_RowsAdded

        UpliftData.GetUpliftInclude(cboEnfAgent.SelectedValue)
        dgvUpliftInclude.FirstDisplayedScrollingRowIndex = dgvUpliftInclude.RowCount - 1

        AddHandler dgvUpliftInclude.RowsAdded, AddressOf dgvUpliftInclude_RowsAdded

        SelectedBailiffID = cboEnfAgent.SelectedValue

        ' Only highlight dates with no allocations if a specific bailiff is selected.
        If SelectedBailiffID <> -1 Then
            For RowCount As Integer = 0 To dgvUpliftInclude.Rows.Count - 2 ' -2 to ignore the new row at the bottom
                HighlightDateAllocated(dgvUpliftInclude, SelectedBailiffID, RowCount, dgvUpliftInclude(3, RowCount).Value)
            Next RowCount
            btnAuditInclude.Enabled = True
        Else
            btnAuditInclude.Enabled = False

        End If
    End Sub


    Private Sub btnSaveInclude_Click(sender As Object, e As System.EventArgs) Handles btnSaveInclude.Click
        Dim ChangesSaved As Boolean = False
        Try

            For Each UpliftRow As DataRowView In dgvUpliftInclude.DataSource
                If UpliftRow.Row.RowState = DataRowState.Modified Or UpliftRow.Row.RowState = DataRowState.Added Then
                    UpliftData.SetUpliftInclude(IIf(IsDBNull(UpliftRow.Item("UpliftID")), "", UpliftRow.Item("UpliftID")), UpliftRow.Item("BailiffID"), UpliftRow.Item("AllocationDate"), UpliftRow.Item("UpliftAmount"), IIf(IsDBNull(UpliftRow.Item("EndDate")), "", UpliftRow.Item("EndDate")))
                    ChangesSaved = True
                End If
            Next UpliftRow

            If ChangesSaved Then
                MessageBox.Show("Changes saved.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

                'RemoveHandler dgvUpliftInclude.RowsAdded, AddressOf dgvUpliftInclude_RowsAdded

                'UpliftData.UpliftInclude.Table.AcceptChanges()

                'AddHandler dgvUpliftInclude.RowsAdded, AddressOf dgvUpliftInclude_RowsAdded
                RefreshInclude()
            Else
                MessageBox.Show("No changes saved.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnDeleteInclude_Click(sender As Object, e As System.EventArgs) Handles btnDeleteInclude.Click

        Try

            If MessageBox.Show("Are you sure?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                'if uplift ID is null the row has not been saved
                If Not IsDBNull(dgvUpliftInclude(0, dgvUpliftInclude.SelectedCells(0).RowIndex).Value) Then UpliftData.DeleteUpliftInclude(dgvUpliftInclude(0, dgvUpliftInclude.SelectedCells(0).RowIndex).Value)

                dgvUpliftInclude.Rows.Remove(dgvUpliftInclude.Rows(dgvUpliftInclude.SelectedCells(0).RowIndex))

            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnSaveContinuous_Click(sender As Object, e As System.EventArgs) Handles btnSaveContinuous.Click
        Dim ChangesSaved As Boolean = False
        Try

            For Each UpliftRow As DataRowView In dgvUpliftContinuous.DataSource
                If UpliftRow.Row.RowState = DataRowState.Modified Or UpliftRow.Row.RowState = DataRowState.Added Then
                    UpliftData.SetUpliftContinuous(IIf(IsDBNull(UpliftRow.Item("UpliftID")), "", UpliftRow.Item("UpliftID")), dgvEnfAgent(0, dgvEnfAgent.SelectedRows(0).Index).Value, UpliftRow.Item("AllocationDate"), IIf(IsDBNull(UpliftRow.Item("EndDate")), "", UpliftRow.Item("EndDate")))
                    ChangesSaved = True
                End If
            Next UpliftRow

            If ChangesSaved Then
                MessageBox.Show("Changes saved.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

                'UpliftData.UpliftContinuous.Table.AcceptChanges()
                RefreshContinuous()

            Else
                MessageBox.Show("No changes saved.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnDeleteContinuous_Click(sender As Object, e As System.EventArgs) Handles btnDeleteContinous.Click

        Try

            If MessageBox.Show("Are you sure?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

                'if uplift ID is null the row has not been saved
                If Not IsDBNull(dgvUpliftContinuous(0, dgvUpliftContinuous.SelectedCells(0).RowIndex).Value) Then UpliftData.DeleteUpliftContinuous(dgvUpliftContinuous(0, dgvUpliftContinuous.SelectedCells(0).RowIndex).Value)

                dgvUpliftContinuous.Rows.Remove(dgvUpliftContinuous.Rows(dgvUpliftContinuous.SelectedCells(0).RowIndex))

            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#Region "dgvUpliftInclude Events"

    Private Sub dgvUpliftInclude_RowsAdded(sender As Object, e As System.Windows.Forms.DataGridViewRowsAddedEventArgs) 'Handles dgvUpliftInclude.RowsAdded
        Try

            If cboEnfAgent.SelectedValue <> "-1" Then
                dgvUpliftInclude.Rows(e.RowIndex - 1).Cells("BailiffIDInclude").Value = cboEnfAgent.SelectedValue
                dgvUpliftInclude.Rows(e.RowIndex - 1).Cells("BailiffNameInclude").Value = cboEnfAgent.Text
            Else
                diaEnfAgent.ShowDialog()
                dgvUpliftInclude.Rows(e.RowIndex - 1).Cells("BailiffIDInclude").Value = diaEnfAgent.BailiffID
                dgvUpliftInclude.Rows(e.RowIndex - 1).Cells("BailiffNameInclude").Value = diaEnfAgent.BailiffName
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvUpliftInclude_CellValidating(sender As Object, e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) ' Handles dgvUpliftInclude.CellValidating
        Try

            If e.ColumnIndex = dgvUpliftInclude.Columns("AllocationDateInclude").Index AndAlso Not IsDBNull(e.FormattedValue) AndAlso e.FormattedValue <> "" Then

                If UpliftData.ContinuousContains(SelectedBailiffID, e.FormattedValue) Then
                    e.Cancel = True
                    MessageBox.Show("Allocation date has already been entered for continuous.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Else
                    HighlightDateAllocated(dgvUpliftInclude, SelectedBailiffID, e.RowIndex, e.FormattedValue)
                End If
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvUpliftInclude_ColumnHeaderMouseClick(sender As Object, e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvUpliftInclude.ColumnHeaderMouseClick
        Try

            RemoveHandler dgvUpliftInclude.RowsAdded, AddressOf dgvUpliftInclude_RowsAdded

            If dgvUpliftInclude.SortOrder = SortOrder.Ascending Then
                dgvUpliftInclude.Sort(dgvUpliftInclude.Columns(e.ColumnIndex), System.ComponentModel.ListSortDirection.Descending)
            Else
                dgvUpliftInclude.Sort(dgvUpliftInclude.Columns(e.ColumnIndex), System.ComponentModel.ListSortDirection.Ascending)
            End If

            dgvUpliftInclude.FirstDisplayedScrollingRowIndex = dgvUpliftInclude.RowCount - 1

            AddHandler dgvUpliftInclude.RowsAdded, AddressOf dgvUpliftInclude_RowsAdded

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvUpliftInclude_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvUpliftInclude.DataError
        Try

            MessageBox.Show("Please enter a valid entry.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "dgvUpliftContinuous Events"
    Private Sub dgvUpliftContinuous_CellValidating(sender As Object, e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles dgvUpliftContinuous.CellValidating
        Try

            If e.ColumnIndex = dgvUpliftContinuous.Columns("AllocationDateContinuous").Index AndAlso Not IsDBNull(e.FormattedValue) AndAlso e.FormattedValue <> "" Then
                If UpliftData.IncludesContains(dgvEnfAgent(0, dgvEnfAgent.SelectedRows(0).Index).Value.ToString, e.FormattedValue) Then
                    e.Cancel = True
                    MessageBox.Show("Allocation date has already been entered for includes.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Else
                    HighlightDateAllocated(dgvUpliftContinuous, dgvEnfAgent(0, dgvEnfAgent.SelectedRows(0).Index).Value, e.RowIndex, e.FormattedValue)

                End If
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvUpliftContinuous_DataError(sender As Object, e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvUpliftContinuous.DataError
        Try

            MessageBox.Show("Please enter a valid entry.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

    Private Function DiscardChanges()
        DiscardChanges = True

        Try
            Dim HasChanges As Boolean = False

            For Each UpliftRow As DataRowView In dgvUpliftInclude.DataSource
                If UpliftRow.Row.RowState = DataRowState.Modified Or UpliftRow.Row.RowState = DataRowState.Added Then HasChanges = True
            Next UpliftRow

            ' dgvUpliftExlude.Datasource is only populated once the Continuous tab is shown hence this test is needed
            If Not IsNothing(dgvUpliftContinuous.DataSource) Then
                For Each UpliftRow As DataRowView In dgvUpliftContinuous.DataSource
                    If UpliftRow.Row.RowState = DataRowState.Modified Or UpliftRow.Row.RowState = DataRowState.Added Then HasChanges = True
                Next UpliftRow
            End If

            If HasChanges AndAlso MessageBox.Show("Discard changes?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                DiscardChanges = False
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Sub dgvBailiff_SelectionChanged(sender As Object, e As System.EventArgs) Handles dgvEnfAgent.SelectionChanged
        Try

            If dgvEnfAgent.SelectedRows.Count > 0 Then
                'UpliftData.GetUpliftContinuous(dgvEnfAgent(0, dgvEnfAgent.SelectedRows(0).Index).Value)
                'dgvUpliftContinuous.DataSource = UpliftData.UpliftContinuous

                'For RowCount As Integer = 0 To dgvUpliftContinuous.Rows.Count - 2
                '    HighlightDateAllocated(dgvUpliftContinuous, dgvEnfAgent(0, dgvEnfAgent.SelectedRows(0).Index).Value, RowCount, dgvUpliftContinuous(1, RowCount).Value)
                'Next RowCount
                RefreshContinuous()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub RefreshContinuous()

        UpliftData.GetUpliftContinuous(dgvEnfAgent(0, dgvEnfAgent.SelectedRows(0).Index).Value)
        dgvUpliftContinuous.DataSource = UpliftData.UpliftContinuous

        For RowCount As Integer = 0 To dgvUpliftContinuous.Rows.Count - 2
            HighlightDateAllocated(dgvUpliftContinuous, dgvEnfAgent(0, dgvEnfAgent.SelectedRows(0).Index).Value, RowCount, dgvUpliftContinuous(1, RowCount).Value)
        Next RowCount

    End Sub

    Private Sub HighlightDateAllocated(ByRef UpliftDataGridView As DataGridView, BailiffID As Integer, RowIndex As Integer, DateAllocated As DateTime)
        Try
            Dim IncludeContinuous As String

            If Strings.Right(UpliftDataGridView.Name, 7) = "Include" Then
                IncludeContinuous = "Include"
            Else
                IncludeContinuous = "Continuous"
            End If

            If Not UpliftData.ValidateDateAllocated(BailiffID, DateAllocated) Then
                UpliftDataGridView.Rows(RowIndex).Cells("AllocationDate" & IncludeContinuous).Style.BackColor = Color.LightYellow
                UpliftDataGridView.Rows(RowIndex).Cells("AllocationDate" & IncludeContinuous).ToolTipText = "No allocations for this date."
                MessageBox.Show("No allocations for this date.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Else
                UpliftDataGridView.Rows(RowIndex).Cells("AllocationDate" & IncludeContinuous).Style.BackColor = Color.White
                UpliftDataGridView.Rows(RowIndex).Cells("AllocationDate" & IncludeContinuous).ToolTipText = ""
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub tabMain_Deselecting(sender As Object, e As System.Windows.Forms.TabControlCancelEventArgs) Handles tabMain.Deselecting
        Try

            If Not DiscardChanges() Then
                e.Cancel = True
            Else
                If tabMain.SelectedIndex = 0 Then
                    RemoveHandler dgvUpliftInclude.RowsAdded, AddressOf dgvUpliftInclude_RowsAdded

                    UpliftData.GetUpliftInclude(cboEnfAgent.SelectedValue)
                    dgvUpliftInclude.FirstDisplayedScrollingRowIndex = dgvUpliftInclude.RowCount - 1

                    AddHandler dgvUpliftInclude.RowsAdded, AddressOf dgvUpliftInclude_RowsAdded

                Else

                    'UpliftData.GetUpliftContinuous(dgvEnfAgent(0, dgvEnfAgent.SelectedRows(0).Index).Value)
                    'dgvUpliftContinuous.DataSource = UpliftData.UpliftContinuous
                    RefreshContinuous()
                End If

            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnAuditInclude_Click(sender As Object, e As System.EventArgs) Handles btnAuditInclude.Click
        Try

            Dim Audit As New diaAudit(SelectedBailiffID, cboEnfAgent.Text)
            Audit.Show()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnAuditContinuous_Click(sender As Object, e As System.EventArgs) Handles btnAuditContinuous.Click
        Try

            Dim Audit As New diaAudit(dgvEnfAgent(0, dgvEnfAgent.SelectedRows(0).Index).Value, dgvEnfAgent(1, dgvEnfAgent.SelectedRows(0).Index).Value)
            Audit.Show()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class


﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class diaAudit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(diaAudit))
        Me.dgvAudit = New System.Windows.Forms.DataGridView()
        Me.CreatedDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CreatedBy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SourceType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ActionType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AllocationDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpliftAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EndDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvAudit, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvAudit
        '
        Me.dgvAudit.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvAudit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAudit.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CreatedDate, Me.CreatedBy, Me.SourceType, Me.ActionType, Me.AllocationDate, Me.UpliftAmount, Me.EndDate})
        Me.dgvAudit.Location = New System.Drawing.Point(12, 12)
        Me.dgvAudit.Name = "dgvAudit"
        Me.dgvAudit.RowHeadersVisible = False
        Me.dgvAudit.Size = New System.Drawing.Size(621, 472)
        Me.dgvAudit.TabIndex = 0
        '
        'CreatedDate
        '
        Me.CreatedDate.DataPropertyName = "CreatedDate"
        DataGridViewCellStyle1.Format = "G"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.CreatedDate.DefaultCellStyle = DataGridViewCellStyle1
        Me.CreatedDate.HeaderText = "Amended"
        Me.CreatedDate.Name = "CreatedDate"
        Me.CreatedDate.Width = 120
        '
        'CreatedBy
        '
        Me.CreatedBy.DataPropertyName = "CreatedBy"
        Me.CreatedBy.HeaderText = "By"
        Me.CreatedBy.Name = "CreatedBy"
        Me.CreatedBy.Width = 160
        '
        'SourceType
        '
        Me.SourceType.DataPropertyName = "SourceType"
        Me.SourceType.HeaderText = "Type"
        Me.SourceType.Name = "SourceType"
        Me.SourceType.Width = 50
        '
        'ActionType
        '
        Me.ActionType.DataPropertyName = "ActionType"
        Me.ActionType.HeaderText = "Action"
        Me.ActionType.Name = "ActionType"
        Me.ActionType.Width = 50
        '
        'AllocationDate
        '
        Me.AllocationDate.DataPropertyName = "AllocationDate"
        Me.AllocationDate.HeaderText = "Allocated"
        Me.AllocationDate.Name = "AllocationDate"
        Me.AllocationDate.Width = 80
        '
        'UpliftAmount
        '
        Me.UpliftAmount.DataPropertyName = "UpliftAmount"
        Me.UpliftAmount.HeaderText = "Amount"
        Me.UpliftAmount.Name = "UpliftAmount"
        Me.UpliftAmount.Width = 60
        '
        'EndDate
        '
        Me.EndDate.DataPropertyName = "EndDate"
        Me.EndDate.HeaderText = "End Date"
        Me.EndDate.Name = "EndDate"
        Me.EndDate.Width = 80
        '
        'diaAudit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(645, 495)
        Me.Controls.Add(Me.dgvAudit)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "diaAudit"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Audit history"
        CType(Me.dgvAudit, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvAudit As System.Windows.Forms.DataGridView
    Friend WithEvents CreatedDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CreatedBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SourceType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ActionType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllocationDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UpliftAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EndDate As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.tabMain = New System.Windows.Forms.TabControl()
        Me.pageInclude = New System.Windows.Forms.TabPage()
        Me.btnAuditInclude = New System.Windows.Forms.Button()
        Me.btnSaveInclude = New System.Windows.Forms.Button()
        Me.btnDeleteInclude = New System.Windows.Forms.Button()
        Me.dgvUpliftInclude = New System.Windows.Forms.DataGridView()
        Me.cboEnfAgent = New System.Windows.Forms.ComboBox()
        Me.pageContinuous = New System.Windows.Forms.TabPage()
        Me.lblExclude = New System.Windows.Forms.Label()
        Me.btnAuditContinuous = New System.Windows.Forms.Button()
        Me.btnSaveContinuous = New System.Windows.Forms.Button()
        Me.btnDeleteContinous = New System.Windows.Forms.Button()
        Me.dgvEnfAgent = New System.Windows.Forms.DataGridView()
        Me.BailiffIDContinuous = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BailiffNameContinuous = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpliftAmountContinuous = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvUpliftContinuous = New System.Windows.Forms.DataGridView()
        Me.UpliftIDInclude = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BailiffIDInclude = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BailiffNameInclude = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AllocationDateInclude = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpliftAmount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EndDateInclude = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UpliftIDContinuous = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AllocationDateContinuous = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EndDateContinuous = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tabMain.SuspendLayout()
        Me.pageInclude.SuspendLayout()
        CType(Me.dgvUpliftInclude, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pageContinuous.SuspendLayout()
        CType(Me.dgvEnfAgent, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvUpliftContinuous, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tabMain
        '
        Me.tabMain.Controls.Add(Me.pageInclude)
        Me.tabMain.Controls.Add(Me.pageContinuous)
        Me.tabMain.Location = New System.Drawing.Point(12, 12)
        Me.tabMain.Name = "tabMain"
        Me.tabMain.SelectedIndex = 0
        Me.tabMain.Size = New System.Drawing.Size(496, 452)
        Me.tabMain.TabIndex = 4
        '
        'pageInclude
        '
        Me.pageInclude.Controls.Add(Me.btnAuditInclude)
        Me.pageInclude.Controls.Add(Me.btnSaveInclude)
        Me.pageInclude.Controls.Add(Me.btnDeleteInclude)
        Me.pageInclude.Controls.Add(Me.dgvUpliftInclude)
        Me.pageInclude.Controls.Add(Me.cboEnfAgent)
        Me.pageInclude.Location = New System.Drawing.Point(4, 22)
        Me.pageInclude.Name = "pageInclude"
        Me.pageInclude.Padding = New System.Windows.Forms.Padding(3)
        Me.pageInclude.Size = New System.Drawing.Size(488, 426)
        Me.pageInclude.TabIndex = 0
        Me.pageInclude.Text = "To include"
        Me.pageInclude.UseVisualStyleBackColor = True
        '
        'btnAuditInclude
        '
        Me.btnAuditInclude.Location = New System.Drawing.Point(186, 397)
        Me.btnAuditInclude.Name = "btnAuditInclude"
        Me.btnAuditInclude.Size = New System.Drawing.Size(116, 25)
        Me.btnAuditInclude.TabIndex = 8
        Me.btnAuditInclude.Text = "View Audit"
        Me.btnAuditInclude.UseVisualStyleBackColor = True
        '
        'btnSaveInclude
        '
        Me.btnSaveInclude.Location = New System.Drawing.Point(58, 397)
        Me.btnSaveInclude.Name = "btnSaveInclude"
        Me.btnSaveInclude.Size = New System.Drawing.Size(116, 25)
        Me.btnSaveInclude.TabIndex = 7
        Me.btnSaveInclude.Text = "Save"
        Me.btnSaveInclude.UseVisualStyleBackColor = True
        '
        'btnDeleteInclude
        '
        Me.btnDeleteInclude.Location = New System.Drawing.Point(314, 397)
        Me.btnDeleteInclude.Name = "btnDeleteInclude"
        Me.btnDeleteInclude.Size = New System.Drawing.Size(116, 25)
        Me.btnDeleteInclude.TabIndex = 6
        Me.btnDeleteInclude.Text = "Delete"
        Me.btnDeleteInclude.UseVisualStyleBackColor = True
        '
        'dgvUpliftInclude
        '
        Me.dgvUpliftInclude.AllowUserToDeleteRows = False
        Me.dgvUpliftInclude.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUpliftInclude.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.UpliftIDInclude, Me.BailiffIDInclude, Me.BailiffNameInclude, Me.AllocationDateInclude, Me.UpliftAmount, Me.EndDateInclude})
        Me.dgvUpliftInclude.Location = New System.Drawing.Point(7, 33)
        Me.dgvUpliftInclude.Name = "dgvUpliftInclude"
        Me.dgvUpliftInclude.RowHeadersVisible = False
        Me.dgvUpliftInclude.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvUpliftInclude.Size = New System.Drawing.Size(475, 358)
        Me.dgvUpliftInclude.TabIndex = 5
        '
        'cboEnfAgent
        '
        Me.cboEnfAgent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEnfAgent.FormattingEnabled = True
        Me.cboEnfAgent.Location = New System.Drawing.Point(168, 6)
        Me.cboEnfAgent.MaxDropDownItems = 16
        Me.cboEnfAgent.Name = "cboEnfAgent"
        Me.cboEnfAgent.Size = New System.Drawing.Size(157, 21)
        Me.cboEnfAgent.TabIndex = 4
        '
        'pageContinuous
        '
        Me.pageContinuous.Controls.Add(Me.lblExclude)
        Me.pageContinuous.Controls.Add(Me.btnAuditContinuous)
        Me.pageContinuous.Controls.Add(Me.btnSaveContinuous)
        Me.pageContinuous.Controls.Add(Me.btnDeleteContinous)
        Me.pageContinuous.Controls.Add(Me.dgvEnfAgent)
        Me.pageContinuous.Controls.Add(Me.dgvUpliftContinuous)
        Me.pageContinuous.Location = New System.Drawing.Point(4, 22)
        Me.pageContinuous.Name = "pageContinuous"
        Me.pageContinuous.Padding = New System.Windows.Forms.Padding(3)
        Me.pageContinuous.Size = New System.Drawing.Size(488, 426)
        Me.pageContinuous.TabIndex = 1
        Me.pageContinuous.Text = "Continuous Uplift"
        Me.pageContinuous.UseVisualStyleBackColor = True
        '
        'lblExclude
        '
        Me.lblExclude.AutoSize = True
        Me.lblExclude.Location = New System.Drawing.Point(263, 17)
        Me.lblExclude.Name = "lblExclude"
        Me.lblExclude.Size = New System.Drawing.Size(170, 13)
        Me.lblExclude.TabIndex = 11
        Me.lblExclude.Text = "Exclude uplift for these allocations:"
        '
        'btnAuditContinuous
        '
        Me.btnAuditContinuous.Location = New System.Drawing.Point(186, 397)
        Me.btnAuditContinuous.Name = "btnAuditContinuous"
        Me.btnAuditContinuous.Size = New System.Drawing.Size(116, 25)
        Me.btnAuditContinuous.TabIndex = 10
        Me.btnAuditContinuous.Text = "View Audit"
        Me.btnAuditContinuous.UseVisualStyleBackColor = True
        '
        'btnSaveContinuous
        '
        Me.btnSaveContinuous.Location = New System.Drawing.Point(58, 397)
        Me.btnSaveContinuous.Name = "btnSaveContinuous"
        Me.btnSaveContinuous.Size = New System.Drawing.Size(116, 25)
        Me.btnSaveContinuous.TabIndex = 9
        Me.btnSaveContinuous.Text = "Save"
        Me.btnSaveContinuous.UseVisualStyleBackColor = True
        '
        'btnDeleteContinous
        '
        Me.btnDeleteContinous.Location = New System.Drawing.Point(314, 397)
        Me.btnDeleteContinous.Name = "btnDeleteContinous"
        Me.btnDeleteContinous.Size = New System.Drawing.Size(116, 25)
        Me.btnDeleteContinous.TabIndex = 8
        Me.btnDeleteContinous.Text = "Delete"
        Me.btnDeleteContinous.UseVisualStyleBackColor = True
        '
        'dgvEnfAgent
        '
        Me.dgvEnfAgent.AllowUserToAddRows = False
        Me.dgvEnfAgent.AllowUserToDeleteRows = False
        Me.dgvEnfAgent.AllowUserToResizeColumns = False
        Me.dgvEnfAgent.AllowUserToResizeRows = False
        Me.dgvEnfAgent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEnfAgent.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BailiffIDContinuous, Me.BailiffNameContinuous, Me.UpliftAmountContinuous})
        Me.dgvEnfAgent.Location = New System.Drawing.Point(7, 33)
        Me.dgvEnfAgent.Name = "dgvEnfAgent"
        Me.dgvEnfAgent.ReadOnly = True
        Me.dgvEnfAgent.RowHeadersVisible = False
        Me.dgvEnfAgent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEnfAgent.Size = New System.Drawing.Size(221, 358)
        Me.dgvEnfAgent.TabIndex = 1
        '
        'BailiffIDContinuous
        '
        Me.BailiffIDContinuous.DataPropertyName = "BailiffID"
        Me.BailiffIDContinuous.HeaderText = "BailiffID"
        Me.BailiffIDContinuous.Name = "BailiffIDContinuous"
        Me.BailiffIDContinuous.ReadOnly = True
        Me.BailiffIDContinuous.Width = 50
        '
        'BailiffNameContinuous
        '
        Me.BailiffNameContinuous.DataPropertyName = "BailiffName"
        Me.BailiffNameContinuous.HeaderText = "Bailiff"
        Me.BailiffNameContinuous.Name = "BailiffNameContinuous"
        Me.BailiffNameContinuous.ReadOnly = True
        '
        'UpliftAmountContinuous
        '
        Me.UpliftAmountContinuous.DataPropertyName = "UpliftAmount"
        Me.UpliftAmountContinuous.HeaderText = "Amount"
        Me.UpliftAmountContinuous.Name = "UpliftAmountContinuous"
        Me.UpliftAmountContinuous.ReadOnly = True
        Me.UpliftAmountContinuous.Width = 50
        '
        'dgvUpliftContinuous
        '
        Me.dgvUpliftContinuous.AllowUserToResizeColumns = False
        Me.dgvUpliftContinuous.AllowUserToResizeRows = False
        Me.dgvUpliftContinuous.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUpliftContinuous.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.UpliftIDContinuous, Me.AllocationDateContinuous, Me.EndDateContinuous})
        Me.dgvUpliftContinuous.Location = New System.Drawing.Point(262, 33)
        Me.dgvUpliftContinuous.Name = "dgvUpliftContinuous"
        Me.dgvUpliftContinuous.RowHeadersVisible = False
        Me.dgvUpliftContinuous.Size = New System.Drawing.Size(220, 358)
        Me.dgvUpliftContinuous.TabIndex = 0
        '
        'UpliftIDInclude
        '
        Me.UpliftIDInclude.DataPropertyName = "UpliftID"
        Me.UpliftIDInclude.HeaderText = "UpliftID"
        Me.UpliftIDInclude.Name = "UpliftIDInclude"
        Me.UpliftIDInclude.ReadOnly = True
        Me.UpliftIDInclude.Visible = False
        '
        'BailiffIDInclude
        '
        Me.BailiffIDInclude.DataPropertyName = "BailiffID"
        Me.BailiffIDInclude.HeaderText = "BailiffID"
        Me.BailiffIDInclude.Name = "BailiffIDInclude"
        Me.BailiffIDInclude.ReadOnly = True
        Me.BailiffIDInclude.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.BailiffIDInclude.Width = 50
        '
        'BailiffNameInclude
        '
        Me.BailiffNameInclude.DataPropertyName = "BailiffName"
        Me.BailiffNameInclude.HeaderText = "Bailiff"
        Me.BailiffNameInclude.Name = "BailiffNameInclude"
        Me.BailiffNameInclude.ReadOnly = True
        Me.BailiffNameInclude.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        '
        'AllocationDateInclude
        '
        Me.AllocationDateInclude.DataPropertyName = "AllocationDate"
        DataGridViewCellStyle1.Format = "d"
        DataGridViewCellStyle1.NullValue = Nothing
        Me.AllocationDateInclude.DefaultCellStyle = DataGridViewCellStyle1
        Me.AllocationDateInclude.HeaderText = "Date allocated"
        Me.AllocationDateInclude.Name = "AllocationDateInclude"
        Me.AllocationDateInclude.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.AllocationDateInclude.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.AllocationDateInclude.Width = 105
        '
        'UpliftAmount
        '
        Me.UpliftAmount.DataPropertyName = "UpliftAmount"
        DataGridViewCellStyle2.Format = "N2"
        DataGridViewCellStyle2.NullValue = Nothing
        Me.UpliftAmount.DefaultCellStyle = DataGridViewCellStyle2
        Me.UpliftAmount.HeaderText = "Amount"
        Me.UpliftAmount.Name = "UpliftAmount"
        Me.UpliftAmount.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.UpliftAmount.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.UpliftAmount.Width = 95
        '
        'EndDateInclude
        '
        Me.EndDateInclude.DataPropertyName = "EndDate"
        Me.EndDateInclude.HeaderText = "End date"
        Me.EndDateInclude.Name = "EndDateInclude"
        Me.EndDateInclude.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Programmatic
        Me.EndDateInclude.Width = 105
        '
        'UpliftIDContinuous
        '
        Me.UpliftIDContinuous.DataPropertyName = "UpliftID"
        Me.UpliftIDContinuous.HeaderText = "UpliftID"
        Me.UpliftIDContinuous.Name = "UpliftIDContinuous"
        Me.UpliftIDContinuous.Visible = False
        '
        'AllocationDateContinuous
        '
        Me.AllocationDateContinuous.DataPropertyName = "AllocationDate"
        Me.AllocationDateContinuous.HeaderText = "Date Allocated"
        Me.AllocationDateContinuous.Name = "AllocationDateContinuous"
        '
        'EndDateContinuous
        '
        Me.EndDateContinuous.DataPropertyName = "EndDate"
        Me.EndDateContinuous.HeaderText = "End Date"
        Me.EndDateContinuous.Name = "EndDateContinuous"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(518, 478)
        Me.Controls.Add(Me.tabMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Working away uplift"
        Me.tabMain.ResumeLayout(False)
        Me.pageInclude.ResumeLayout(False)
        CType(Me.dgvUpliftInclude, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pageContinuous.ResumeLayout(False)
        Me.pageContinuous.PerformLayout()
        CType(Me.dgvEnfAgent, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvUpliftContinuous, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabMain As System.Windows.Forms.TabControl
    Friend WithEvents pageInclude As System.Windows.Forms.TabPage
    Friend WithEvents btnSaveInclude As System.Windows.Forms.Button
    Friend WithEvents btnDeleteInclude As System.Windows.Forms.Button
    Friend WithEvents dgvUpliftInclude As System.Windows.Forms.DataGridView
    Friend WithEvents cboEnfAgent As System.Windows.Forms.ComboBox
    Friend WithEvents pageContinuous As System.Windows.Forms.TabPage
    Friend WithEvents dgvEnfAgent As System.Windows.Forms.DataGridView
    Friend WithEvents dgvUpliftContinuous As System.Windows.Forms.DataGridView
    Friend WithEvents btnSaveContinuous As System.Windows.Forms.Button
    Friend WithEvents btnDeleteContinous As System.Windows.Forms.Button
    Friend WithEvents BailiffIDContinuous As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BailiffNameContinuous As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UpliftAmountContinuous As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnAuditInclude As System.Windows.Forms.Button
    Friend WithEvents btnAuditContinuous As System.Windows.Forms.Button
    Friend WithEvents lblExclude As System.Windows.Forms.Label
    Friend WithEvents UpliftIDInclude As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BailiffIDInclude As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BailiffNameInclude As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllocationDateInclude As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UpliftAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EndDateInclude As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UpliftIDContinuous As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AllocationDateContinuous As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EndDateContinuous As System.Windows.Forms.DataGridViewTextBoxColumn

End Class

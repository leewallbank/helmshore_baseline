﻿Imports System.IO
Public Class Form1

    Dim outfile, inputfilepath, outfilename As String
    Dim recordCount As Integer
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub
    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        With OpenFileDialog1
            .Title = "Read XML file"
            .Filter = "XML file|*.xml"
            .FileName = ""
            .CheckFileExists = True
        End With
        Dim xml_dataSet As New DataSet()
        Dim doc As New Xml.XmlDocument()
        Dim personref As Integer = 0
        Dim personAlias As String = ""
        Dim personName As String = ""
        Dim personAddr As String = ""
        Dim personACDesc As String = ""
        Dim personCount As Integer = 0
        Dim personPCode As String = ""
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("XML File not processed")
        Else
            openbtn.Enabled = False
            exitbtn.Enabled = False
            inputfilepath = Path.GetDirectoryName(OpenFileDialog1.FileName)
            inputfilepath &= "\"
            outfilename = inputfilepath & "LocalAuthority_Reformat.txt"
            outfile = "Account Ref|RecoveryControlRef|PersonTrailNumber1|Name1|Alias1|PostalAddress1|PostCode1|ACDescription1|PersonNumber2|Name2|Alias2|PostalAddress2|PostCode2|ACDescription2|Balance|PaidTodate|Reason|OrigAmt" & vbNewLine
            Dim rdr_name As String = ""
            Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
            reader.Read()
            While (reader.ReadState <> Xml.ReadState.EndOfFile)
                If reader.NodeType > 1 Then
                    reader.Read()
                    Continue While
                End If
                Try
                    rdr_name = reader.Name
                Catch
                    Continue While
                End Try
                recordCount += 1
                Select Case rdr_name
                    Case "BailiffSheriffChanges"
                        reader.Read()
                    Case "Header"
                        reader.Read()
                    Case "Module"
                        reader.Read()
                    Case "Trailer"
                        reader.Read()
                    Case "NumberOfRecords"
                        reader.Read()
                    Case "ExtractDate"
                        outfile &= "ExtractDate:" & reader.ReadElementContentAsString & vbNewLine
                    Case "LAName"
                        outfilename = inputfilepath & reader.ReadElementContentAsString & "_Reformat.txt"
                    Case "ChangedRecords"
                        reader.Read()
                        While reader.Name <> "ChangedRecords"
                            If reader.NodeType > 1 Then
                                reader.Read()
                                Continue While
                            End If
                            rdr_name = reader.Name
                            Select Case rdr_name
                                Case "ChangedRecord"
                                    reader.Read()
                                    While reader.Name <> "ChangedRecord"
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        rdr_name = reader.Name
                                        Select Case rdr_name
                                            Case "AccountReference"
                                                outfile &= reader.ReadElementContentAsString & "|"
                                                personref = 0
                                                personAddr = ""
                                                personPCode = ""
                                                personAlias = ""
                                                personName = ""
                                                personCount = 0
                                                personpcode = ""
                                            Case "RecoveryControl"
                                                outfile &= reader.ReadElementContentAsString & "|"
                                            Case "Associates"
                                                reader.Read()
                                                While reader.Name <> "Associates"
                                                    If reader.NodeType > 1 Then
                                                        reader.Read()
                                                        Continue While
                                                    End If
                                                    rdr_name = reader.Name
                                                    Select Case rdr_name
                                                        Case "Associate"
                                                            reader.Read()
                                                            personCount += 1
                                                            If personCount > 1 Then
                                                                Dim x As Integer
                                                                x = 55
                                                            End If
                                                            While reader.Name <> "Associate"
                                                                If reader.NodeType > 1 Then
                                                                    reader.Read()
                                                                    Continue While
                                                                End If
                                                                rdr_name = reader.Name
                                                                Select Case rdr_name
                                                                    Case "PersonReference"
                                                                        personref = reader.ReadElementContentAsString
                                                                    Case "PersonName"
                                                                        reader.Read()
                                                                        While reader.Name <> "PersonName"
                                                                            If reader.NodeType > 1 Then
                                                                                reader.Read()
                                                                                Continue While
                                                                            End If
                                                                            rdr_name = reader.Name
                                                                            Select Case rdr_name
                                                                                Case "PersonTitle"
                                                                                    personName &= reader.ReadElementContentAsString & " "
                                                                                Case "PersonForename"
                                                                                    personName &= reader.ReadElementContentAsString & " "
                                                                                Case "PersonSurname"
                                                                                    personName &= reader.ReadElementContentAsString
                                                                                Case "PersonAlias"
                                                                                    personAlias = reader.ReadElementContentAsString
                                                                                Case Else
                                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                                    reader.Read()
                                                                            End Select
                                                                        End While
                                                                    Case "PostalAddress"
                                                                        reader.Read()
                                                                        While reader.Name <> "PostalAddress"
                                                                            If reader.NodeType > 1 Then
                                                                                reader.Read()
                                                                                Continue While
                                                                            End If
                                                                            rdr_name = reader.Name
                                                                            Select Case rdr_name
                                                                                Case "PostalAddressLine"
                                                                                    personAddr &= reader.ReadElementContentAsString & " "
                                                                                Case "PostalPostCode"
                                                                                    personPCode = reader.ReadElementContentAsString
                                                                                Case Else
                                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                                    reader.Read()
                                                                            End Select
                                                                        End While
                                                                    Case "Deceased"
                                                                        'outfile &= reader.ReadElementContentAsString & ","
                                                                        reader.Read()
                                                                    Case "AssociateContactDetails"
                                                                        reader.Read()
                                                                        While reader.Name <> "AssociateContactDetails"
                                                                            If reader.NodeType > 1 Then
                                                                                reader.Read()
                                                                                Continue While
                                                                            End If
                                                                            rdr_name = reader.Name
                                                                            Select Case rdr_name
                                                                                Case "ACDescription"
                                                                                    personACDesc &= reader.ReadElementContentAsString & ";"
                                                                                Case "ACType"
                                                                                    reader.Read()
                                                                                Case "ACNumber"
                                                                                    reader.Read()
                                                                                Case "ACEmailAddress"
                                                                                    reader.Read()
                                                                                Case Else
                                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                                    reader.Read()
                                                                            End Select
                                                                        End While
                                                                       
                                                                    Case Else
                                                                        MsgBox("What is this tag?" & rdr_name)
                                                                        reader.Read()
                                                                End Select
                                                            End While
                                                            If personCount = 1 Then
                                                                outfile &= personref & "|" & personName & "|" & personAlias & "|" & personAddr & "|" & personPCode & "|" & personACDesc & "|"
                                                                personACDesc = ""
                                                            End If
                                                           
                                                        Case "RecoveryBalance"
                                                            outfile &= personref & "|" & personName & "|" & personAlias & "|" & personAddr & "|" & personPCode & "|" & personACDesc & "|"
                                                            Dim element As String = reader.ReadElementContentAsString
                                                            element = Replace(element, ",", "")
                                                            outfile &= element & "|"
                                                        Case "PaidToDate"
                                                            outfile &= reader.ReadElementContentAsString & "|"
                                                        Case "Reason"
                                                            outfile &= reader.ReadElementContentAsString & "|"
                                                        Case "OriginalLOAmount"
                                                            Dim element As String = reader.ReadElementContentAsString
                                                            element = Replace(element, ",", "")
                                                            outfile &= element & vbNewLine
                                                        Case "OriginalCostsAmount"
                                                            reader.Read()
                                                        Case "Transaction"
                                                            reader.Read()
                                                            While reader.Name <> "Transaction"
                                                                If reader.NodeType > 1 Then
                                                                    reader.Read()
                                                                    Continue While
                                                                End If
                                                                reader.Read()
                                                            End While
                                                        Case Else
                                                            MsgBox("What is this tag?" & rdr_name)
                                                            reader.Read()
                                                    End Select
                                                End While
                                        End Select
                                    End While

                                Case Else
                                    MsgBox("What is this tag?" & rdr_name)
                                    reader.Read()
                            End Select
                        End While
                    Case Else
                        MsgBox("What is this tag?" & rdr_name)
                        reader.Read()
                End Select
            End While
            My.Computer.FileSystem.WriteAllText(outfilename, outfile, False)
        End If
        MsgBox("Report saved")
        Me.Close()
    End Sub
End Class

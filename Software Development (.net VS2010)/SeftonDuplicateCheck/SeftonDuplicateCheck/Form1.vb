﻿Imports CommonLibrary
Public Class Form1
    Dim outputfile As String = "DebtorID" & vbNewLine
    Dim cases As Integer
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        Dim startDate, endDate As Date
        startDate = datedtp.Text
        endDate = DateAdd(DateInterval.Day, 1, startDate)

        'get all sefton cases loaded on selected date
        Dim debt_dt As New DataTable
        LoadDataTable("DebtRecovery", "SELECT D._rowID, client_ref, offence_number, offence_court  " & _
                                                "FROM Debtor D, clientScheme CS" & _
                                                " where CS.clientID = 1498 " & _
                                                " and D.clientSchemeID = CS._rowID " & _
                                                " and branchID = 1 " & _
                                                " and status_open_closed = 'O'" & _
                                                " and D._createdDate >='" & Format(startDate, "yyyy-MM-dd") & "'" & _
                                                " and D._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'", debt_dt, False)
        For Each row In debt_dt.Rows
            Dim debtorID As Integer = row(0)
            Dim clientRef As Integer = row(1)
            Dim summonsNO As Integer = row(2)
            Dim LODate As Date = row(3)
            'look for another open case matching this
            Dim debt2_dt As New DataTable
            LoadDataTable("DebtRecovery", "SELECT D._rowID  " & _
                                                "FROM Debtor D, clientScheme CS" & _
                                                " where CS.clientID = 1498 " & _
                                                " and D.clientSchemeID = CS._rowID " & _
                                                "and branchID = 1" & _
                                                " and status_open_closed = 'O'" & _
                                                " and client_ref = '" & clientRef & "'" & _
                                                " and offence_number = " & summonsNO & _
                                                " and offence_court = '" & Format(LODate, "yyyy-MM-dd") & "'" & _
                                                " and D._rowID <>" & debtorID, debt2_dt, False)
            If debt2_dt.Rows.Count > 0 Then
                outputfile &= debtorID & vbNewLine
                cases += 1
            End If
        Next
        MsgBox("Duplicate Cases found = " & cases)
        If cases > 0 Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "CSV files | *.csv"
                .FileName = "SeftonDuplicates.csv"
            End With

            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outputfile, False)
            End If
        End If
        Me.Close()
    End Sub
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.btnCreateReturns = New System.Windows.Forms.Button()
        Me.dgvMain = New System.Windows.Forms.DataGridView()
        Me.cmdRefresh = New System.Windows.Forms.Button()
        Me.cmsDGV = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmdValidate = New System.Windows.Forms.Button()
        Me.lblDateTo = New System.Windows.Forms.Label()
        Me.dtpDateTo = New System.Windows.Forms.DateTimePicker()
        Me.lblDateFrom = New System.Windows.Forms.Label()
        Me.dtpDateFrom = New System.Windows.Forms.DateTimePicker()
        Me.txtFileSequenceNumber = New System.Windows.Forms.TextBox()
        Me.lblFileSequenceNumber = New System.Windows.Forms.Label()
        Me.lblNoData = New System.Windows.Forms.Label()
        CType(Me.dgvMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnCreateReturns
        '
        Me.btnCreateReturns.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCreateReturns.Location = New System.Drawing.Point(916, 428)
        Me.btnCreateReturns.Name = "btnCreateReturns"
        Me.btnCreateReturns.Size = New System.Drawing.Size(100, 30)
        Me.btnCreateReturns.TabIndex = 8
        Me.btnCreateReturns.Text = "Create return file"
        Me.btnCreateReturns.UseVisualStyleBackColor = True
        '
        'dgvMain
        '
        Me.dgvMain.AllowDrop = True
        Me.dgvMain.AllowUserToAddRows = False
        Me.dgvMain.AllowUserToDeleteRows = False
        Me.dgvMain.AllowUserToResizeRows = False
        Me.dgvMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMain.Location = New System.Drawing.Point(12, 12)
        Me.dgvMain.MultiSelect = False
        Me.dgvMain.Name = "dgvMain"
        Me.dgvMain.RowHeadersVisible = False
        Me.dgvMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvMain.Size = New System.Drawing.Size(1004, 402)
        Me.dgvMain.TabIndex = 9
        '
        'cmdRefresh
        '
        Me.cmdRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdRefresh.Location = New System.Drawing.Point(233, 428)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(100, 30)
        Me.cmdRefresh.TabIndex = 10
        Me.cmdRefresh.Text = "Refresh"
        Me.cmdRefresh.UseVisualStyleBackColor = True
        '
        'cmsDGV
        '
        Me.cmsDGV.Name = "cmsDGV"
        Me.cmsDGV.Size = New System.Drawing.Size(61, 4)
        '
        'cmdValidate
        '
        Me.cmdValidate.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.cmdValidate.Location = New System.Drawing.Point(559, 428)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(101, 30)
        Me.cmdValidate.TabIndex = 12
        Me.cmdValidate.Text = "Validate"
        Me.cmdValidate.UseVisualStyleBackColor = True
        '
        'lblDateTo
        '
        Me.lblDateTo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblDateTo.AutoSize = True
        Me.lblDateTo.Location = New System.Drawing.Point(63, 450)
        Me.lblDateTo.Name = "lblDateTo"
        Me.lblDateTo.Size = New System.Drawing.Size(19, 13)
        Me.lblDateTo.TabIndex = 17
        Me.lblDateTo.Text = "to:"
        '
        'dtpDateTo
        '
        Me.dtpDateTo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtpDateTo.Location = New System.Drawing.Point(91, 446)
        Me.dtpDateTo.Name = "dtpDateTo"
        Me.dtpDateTo.Size = New System.Drawing.Size(126, 20)
        Me.dtpDateTo.TabIndex = 16
        '
        'lblDateFrom
        '
        Me.lblDateFrom.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblDateFrom.AutoSize = True
        Me.lblDateFrom.Location = New System.Drawing.Point(10, 424)
        Me.lblDateFrom.Name = "lblDateFrom"
        Me.lblDateFrom.Size = New System.Drawing.Size(73, 13)
        Me.lblDateFrom.TabIndex = 15
        Me.lblDateFrom.Text = "Modified from:"
        '
        'dtpDateFrom
        '
        Me.dtpDateFrom.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtpDateFrom.Location = New System.Drawing.Point(91, 420)
        Me.dtpDateFrom.Name = "dtpDateFrom"
        Me.dtpDateFrom.Size = New System.Drawing.Size(126, 20)
        Me.dtpDateFrom.TabIndex = 14
        '
        'txtFileSequenceNumber
        '
        Me.txtFileSequenceNumber.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtFileSequenceNumber.Location = New System.Drawing.Point(803, 434)
        Me.txtFileSequenceNumber.Name = "txtFileSequenceNumber"
        Me.txtFileSequenceNumber.Size = New System.Drawing.Size(70, 20)
        Me.txtFileSequenceNumber.TabIndex = 18
        '
        'lblFileSequenceNumber
        '
        Me.lblFileSequenceNumber.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFileSequenceNumber.AutoSize = True
        Me.lblFileSequenceNumber.Location = New System.Drawing.Point(734, 437)
        Me.lblFileSequenceNumber.Name = "lblFileSequenceNumber"
        Me.lblFileSequenceNumber.Size = New System.Drawing.Size(63, 13)
        Me.lblFileSequenceNumber.TabIndex = 19
        Me.lblFileSequenceNumber.Text = "File Seq no:"
        '
        'lblNoData
        '
        Me.lblNoData.AutoSize = True
        Me.lblNoData.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.lblNoData.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNoData.ForeColor = System.Drawing.Color.White
        Me.lblNoData.Location = New System.Drawing.Point(439, 145)
        Me.lblNoData.Name = "lblNoData"
        Me.lblNoData.Size = New System.Drawing.Size(151, 24)
        Me.lblNoData.TabIndex = 20
        Me.lblNoData.Text = "No data selected"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1028, 468)
        Me.Controls.Add(Me.lblNoData)
        Me.Controls.Add(Me.lblFileSequenceNumber)
        Me.Controls.Add(Me.txtFileSequenceNumber)
        Me.Controls.Add(Me.lblDateTo)
        Me.Controls.Add(Me.dtpDateTo)
        Me.Controls.Add(Me.lblDateFrom)
        Me.Controls.Add(Me.dtpDateFrom)
        Me.Controls.Add(Me.cmdValidate)
        Me.Controls.Add(Me.cmdRefresh)
        Me.Controls.Add(Me.dgvMain)
        Me.Controls.Add(Me.btnCreateReturns)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "United Utilities Returns"
        CType(Me.dgvMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnCreateReturns As System.Windows.Forms.Button
    Friend WithEvents dgvMain As System.Windows.Forms.DataGridView
    Friend WithEvents cmdRefresh As System.Windows.Forms.Button
    Friend WithEvents cmsDGV As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmdValidate As System.Windows.Forms.Button
    Friend WithEvents lblDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtFileSequenceNumber As System.Windows.Forms.TextBox
    Friend WithEvents lblFileSequenceNumber As System.Windows.Forms.Label
    Friend WithEvents lblNoData As System.Windows.Forms.Label

End Class

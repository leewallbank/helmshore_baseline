﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("HMRC Returns (auto sol)")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Rossendales")> 
<Assembly: AssemblyProduct("HMRC Returns")> 
<Assembly: AssemblyCopyright("Copyright © Rossendales 2014")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("dd5ed32b-1ebf-41f8-89f9-90d51c550dce")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.3.0.0")> 
<Assembly: AssemblyFileVersion("1.3.0.0")> 

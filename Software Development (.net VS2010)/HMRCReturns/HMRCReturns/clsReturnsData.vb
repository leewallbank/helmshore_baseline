﻿Imports CommonLibrary

Public Class clsReturnsData
    Private Debtor As New DataTable, MasterList As New DataTable, TempTable As New DataTable, AddressSource As New DataTable, Address As New DataTable, MergedSource As New DataTable, ContactSource As New DataTable
    Private DebtorDV As DataView, AddressDV As DataView
    'Private ConnID As String = "4402,4403,4404,4405"
    'Private ConnID As String = "4402,4403,4404,4405,4518,4519,4520,4521,4522,4523" ' added connIDs. TS 24/Feb/2015. Request ref 42999
    'Private ConnID As String = "4402,4403,4404,4405,4518,4519,4520,4521,4522" ' removed connID 4523. TS 19/Mar/2015. Request ref 45148
    'Private ConnID As String = "4402,4403,4404,4405,4518,4519,4520,4521,4522,4600,4602,4604,4606" ' added connIDs 4600, 4602, 4604, 4606. TS 13/Apr/2015. Request ref 47253
    Private ConnID As String = "4402,4403,4404,4405,4518,4519,4521,4522,4600,4602,4604,4606" ' removed connID 4520. TS 19/May/2015. Request ref 50300

    Public ReadOnly Property Debtors() As DataView
        Get
            Debtors = DebtorDV
        End Get
    End Property

    Public ReadOnly Property Addresses() As DataView
        Get
            Addresses = AddressDV
        End Get
    End Property

    Public Sub New()
        ' The following column definitions are also used to pad fields in the output file, so this section must match the return file spec from student loans
        ' These definitions are reference throughout the app
        With Address.Columns

            .Add("DebtorID", Type.GetType("System.String"))
            .Item("DebtorID").ExtendedProperties.Add("MaxLength", 7)

            .Add("client_ref", Type.GetType("System.String"))
            .Item("client_ref").ExtendedProperties.Add("MaxLength", 13)

            .Add("TrancheID", Type.GetType("System.String"))
            .Item("TrancheID").ExtendedProperties.Add("MaxLength", 10)

            .Add("QueryDate", Type.GetType("System.String"))
            .Item("QueryDate").ExtendedProperties.Add("MaxLength", 10)

            .Add("Title", Type.GetType("System.String"))
            .Item("Title").ExtendedProperties.Add("MaxLength", 20)

            .Add("Forename", Type.GetType("System.String"))
            .Item("Forename").ExtendedProperties.Add("MaxLength", 60)

            .Add("Surname", Type.GetType("System.String"))
            .Item("Surname").ExtendedProperties.Add("MaxLength", 60)

            .Add("AddressLine1", Type.GetType("System.String"))
            .Item("AddressLine1").ExtendedProperties.Add("MaxLength", 50)

            .Add("AddressLine2", Type.GetType("System.String"))
            .Item("AddressLine2").ExtendedProperties.Add("MaxLength", 50)

            .Add("AddressLine3", Type.GetType("System.String"))
            .Item("AddressLine3").ExtendedProperties.Add("MaxLength", 50)

            .Add("AddressLine4", Type.GetType("System.String"))
            .Item("AddressLine4").ExtendedProperties.Add("MaxLength", 50)

            .Add("PostCode", Type.GetType("System.String"))
            .Item("PostCode").ExtendedProperties.Add("MaxLength", 8)

            .Add("Phone", Type.GetType("System.String"))
            .Item("Phone").ExtendedProperties.Add("MaxLength", 12)

        End With

        TempTable.Columns.Add("client_ref", Type.GetType("System.String"))
    End Sub

    Public Sub GetDebtors(ByVal DateFrom As Date, ByVal DateTo As Date)
        Dim Sql As String

        Try
            ' Get updated cases
            ' replace added TS 05/Sep/2014. Request ref 29821
            Sql = "SELECT d._rowID AS debtorID " & _
                  "     , d.client_ref " & _
                  "     , d.client_batch AS TrancheID " & _
                  "     , CASE s._rowID" & _
                  "           WHEN 89 THEN 101 " & _
                  "           WHEN 90 THEN 102 " & _
                  "           WHEN 91 THEN 103 " & _
                  "           WHEN 92 THEN 104 " & _
                  "           WHEN 93 THEN 105 " & _
                  "           WHEN 94 THEN 106 " & _
                  "           WHEN 95 THEN 107 " & _
                  "           WHEN 96 THEN 108 " & _
                  "       END AS QueryCode " & _
                  "     , REPLACE(n.text, CHAR(163), '') AS return_details " & _
                  "     , n._createdDate AS QueryDate" & _
                  "     , CAST(NULL AS DECIMAL(12,2)) AS DebtBalance " & _
                  "     , CAST(NULL AS DATETIME) AS ReturnDate " & _
                  "FROM Debtor AS d " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "INNER JOIN CodeStatus AS s ON d.statusCode = s._rowid " & _
                  "INNER JOIN note AS n ON d._rowID = n.DebtorID " & _
            "INNER JOIN ( SELECT n_s.DebtorID " & _
                  "                  , MAX(n_s._rowID) AS LastNoteID " & _
                  "             FROM debtor AS d_s " & _
                  "             INNER JOIN note AS n_s ON d_s._rowID = n_s.DebtorID " & _
                  "             INNER JOIN clientscheme AS cs_s ON d_s.clientschemeID = cs_s._RowID " & _
                  "             WHERE n_s.type = 'Status changed' " & _
                  "               AND cs_s._rowID IN (" & ConnID & ") " & _
                  "               AND DATE(n_s._createdDate) >'" & DateFrom.AddDays(-1).ToString("yyyy-MM-dd") & "' " & _
                  "               AND DATE(n_s._createdDate) <'" & DateTo.AddDays(+1).ToString("yyyy-MM-dd") & "' " & _
                  "             GROUP BY n_s.DebtorID " & _
                  "           ) AS n_max ON n.DebtorID = n_max.DebtorID " & _
                  "                     AND n._rowID   = n_max.LastNoteID " & _
                   " WHERE cs._rowID IN (" & ConnID & ") " & _
                  "  AND s._rowID IN (89, 90, 91, 92, 93, 94, 95, 96)"

            LoadDataTable("DebtRecovery", Sql, Debtor, False)

            ' Get closed and recalled cases next.
            Sql = "SELECT d._rowID AS debtorID " & _
                  "     , d.client_ref " & _
                  "     , d.client_batch AS TrancheID " & _
                  "     , CASE WHEN d.status = 'S' THEN 6 ELSE IFNULL(csr.clientReturnNumber,0) END AS QueryCode " & _
                  "     , d.return_details " & _
                  "     , r.date AS QueryDate " & _
                  "     , d.debt_balance AS DebtBalance " & _
                  "     , d.return_date AS ReturnDate " & _
                  "FROM Debtor AS d " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "INNER JOIN remit AS r ON d.return_remitid = r._rowid " & _
                  "LEFT JOIN ClientSchemeReturn AS csr ON d.ClientSchemeID = csr.ClientSchemeid " & _
                  "                                   AND d.return_codeid  = csr.returnid " & _
                  "LEFT JOIN ( SELECT n_s.DebtorID " & _
                  "                 , n_s.text " & _
                  "            FROM note AS n_s " & _
                  "            INNER JOIN ( SELECT n_m_s.DebtorID " & _
                  "                              , MAX(n_m_s._rowID) AS _rowID " & _
                  "                         FROM debtor AS d_m_s " & _
                  "                         INNER JOIN clientscheme AS cs_m_s ON d_m_s.ClientSchemeID = cs_m_s._rowID " & _
                  "                         INNER JOIN note AS n_m_s ON d_m_s._rowID = n_m_s._rowID " & _
                  "                         WHERE cs_m_s._rowID IN (" & ConnID & ") " & _
                  "                           AND d_m_s.status_open_closed = 'C' " & _
                  "                           AND n_m_s.type = 'Cancelled' " & _
                  "                           AND n_m_s.text LIKE '%Re: Gone Away - %' " & _
                  "                         GROUP BY n_m_s.DebtorID " & _
                  "                       ) AS n_m ON n_s.DebtorID = n_m.DebtorID " & _
                  "                               AND n_s._rowID   = n_m._rowID" & _
                  "          ) AS n ON n.DebtorID = d._rowID " & _
                  "WHERE cs._rowID IN (" & ConnID & ")" & _
                  "  AND d.status_open_closed = 'C' " & _
                  "  AND DATE(r.date) BETWEEN '" & DateFrom.ToString("yyyy-MM-dd") & "' " & _
                  "                       AND '" & DateTo.ToString("yyyy-MM-dd") & "' "


            LoadDataTable("DebtRecovery", Sql, Debtor, True)

            ' Add ID's of each address / contact detail change so these are looped through 

            If Not IsNothing(Addresses) Then

                For Each Change As DataRow In Addresses.Table.Rows
                    Debtor.Rows.Add({Change(0), Change(1), Change(2), 999, "", Change(3)})
                Next Change
            End If

            DebtorDV = New DataView(Debtor)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub GetAddresses(ByVal DateFrom As Date, ByVal DateTo As Date)
        Dim OutputDataRow As DataRow
        Dim AddressLine() As String, Sql As String

        Try
            ' Load address changes
            Sql = "SELECT d._rowID AS debtorID " & _
                  "     , d.client_ref " & _
                  "     , d.client_batch AS TrancheID " & _
                  "     , 999 AS QueryCode " & _
                  "     , '' AS returnDetails " & _
                  "     , n._createdDate AS QueryDate " & _
                  "     , '' AS name_title " & _
                  "     , '' AS name_fore " & _
                  "     , '' AS name_sur " & _
                  "     , IFNULL(d.address,'')  AS address " & _
                  "     , IFNULL(d.add_postcode,'')  AS Postcode " & _
                  "     , '' AS add_phone " & _
                  "FROM debtor AS d " & _
                  "INNER JOIN note AS n ON d._rowID = n.DebtorID " & _
                  "INNER JOIN clientscheme AS cs ON d.clientschemeID = cs._RowID " & _
                  "INNER JOIN scheme AS s ON cs.schemeID = s._RowID " & _
                  "INNER JOIN ( SELECT n_s.DebtorID " & _
                  "                  , MAX(n_s._rowID) AS LastNoteID " & _
                  "             FROM debtor AS d_s " & _
                  "             INNER JOIN note AS n_s ON d_s._rowID = n_s.DebtorID " & _
                  "             INNER JOIN clientscheme AS cs_s ON d_s.clientschemeID = cs_s._RowID " & _
                  "             WHERE n_s.type = 'Address' " & _
                  "               AND n_s.text LIKE 'changed from:%'" & _
                  "               AND cs_s._rowID IN (" & ConnID & ") " & _
                  "               AND DATE(n_s._createdDate) BETWEEN '" & DateFrom.ToString("yyyy-MM-dd") & "' " & _
                  "                                              AND '" & DateTo.ToString("yyyy-MM-dd") & "' " & _
                  "             GROUP BY n_s.DebtorID " & _
                  "           ) AS n_max ON n.DebtorID = n_max.DebtorID " & _
                  "                     AND n._rowID   = n_max.LastNoteID " & _
                  "LEFT JOIN note AS nt ON n.debtorID           = nt.debtorID " & _
                  "                    AND 'Off trace'          = nt.type " & _
                  "                    AND DATE(n._createdDate) = DATE(nt._createdDate)" & _
                  "WHERE n.type = 'Address' " & _
                  "  AND n.text LIKE 'changed from:%'" & _
                  "  AND cs._rowID IN (" & ConnID & ")"

            LoadDataTable("DebtRecovery", Sql, AddressSource, False)

            ' Load contact detail changes
            Sql = "SELECT sd.DebtorID " & _
                  "     , sd.client_ref " & _
                  "     , sd.client_batch AS TrancheID " & _
                  "     , 999 AS QueryCode " & _
                  "     , '' AS returnDetails " & _
                  "     , d.BuildDate AS QueryDate " & _
                  "     , '' AS name_title " & _
                  "     , '' AS name_fore " & _
                  "     , '' AS name_sur " & _
                  "     , '' AS address " & _
                  "     , '' AS Postcode " & _
                  "     , d.add_phone " & _
                  "FROM rpt.HMRCDebtor AS d " & _
                  "INNER JOIN stg.Debtor AS sd ON d.DebtorID = sd.DebtorID " & _
                  "INNER JOIN stg.ClientScheme AS cs ON cs.ClientSchemeID = sd.ClientSchemeID " & _
                  "  AND CAST(FLOOR(CAST(d.BuildDate AS FLOAT)) AS DATETIME) BETWEEN '" & DateFrom.ToString("yyyy-MMM-dd") & "' " & _
                  "                                                              AND '" & DateTo.ToString("yyyy-MMM-dd") & "' "


            LoadDataTable("StudentLoans", Sql, ContactSource, False)

            MergeUpdates()

            Address.Clear()

            For Each SourceDataRow As DataRow In MergedSource.Rows
                OutputDataRow = Address.Rows.Add

                ' The following section performs the mapping from DebtRecovery fields to the output file

                OutputDataRow("DebtorID") = SourceDataRow("DebtorID")
                OutputDataRow("client_ref") = SourceDataRow("client_ref")
                OutputDataRow("TrancheID") = SourceDataRow("TrancheID")
                OutputDataRow("QueryDate") = SourceDataRow("QueryDate")
                OutputDataRow("Title") = SourceDataRow("name_title")
                OutputDataRow("Forename") = SourceDataRow("name_fore")
                OutputDataRow("Surname") = SourceDataRow("name_sur")

                ' Create an array of address lines. Note DebtRecovery sometimes just uses a line feed for new line with no carriage return
                If SourceDataRow("Address") <> "" Then
                    AddressLine = Replace(LTrim(SourceDataRow("Address")), vbCr, "").ToString.TrimEnd(vbLf).Split(vbLf)
                Else
                    AddressLine = Nothing
                End If

                ' DebtRecovery stored the address in one field. The following logic is to break it down into multiple
                If Not IsNothing(AddressLine) Then

                    ' If the last line of the address is the same as the postcode then drop it
                    If AddressLine(UBound(AddressLine)) = SourceDataRow("PostCode").ToString.Trim Then ReDim Preserve AddressLine(UBound(AddressLine) - 1)

                    ' put the lines in the address into the different fields. The logic used differs based on the number of lines in the address
                    Select Case UBound(AddressLine)
                        Case 0
                            OutputDataRow("AddressLine1") = AddressLine(0)
                            OutputDataRow("AddressLine2") = ""
                            OutputDataRow("AddressLine3") = ""
                            OutputDataRow("AddressLine4") = ""
                        Case 1
                            OutputDataRow("AddressLine1") = AddressLine(0)
                            OutputDataRow("AddressLine2") = AddressLine(1)
                            OutputDataRow("AddressLine3") = ""
                            OutputDataRow("AddressLine4") = ""
                        Case 2
                            OutputDataRow("AddressLine1") = AddressLine(0)
                            OutputDataRow("AddressLine2") = AddressLine(1)
                            OutputDataRow("AddressLine3") = AddressLine(2)
                            OutputDataRow("AddressLine4") = ""
                        Case 3
                            OutputDataRow("AddressLine1") = AddressLine(0)
                            OutputDataRow("AddressLine2") = AddressLine(1)
                            OutputDataRow("AddressLine3") = AddressLine(2)
                            OutputDataRow("AddressLine4") = AddressLine(3)
                        Case Else
                            OutputDataRow("AddressLine1") = AddressLine(0)
                            OutputDataRow("AddressLine2") = AddressLine(1)
                            OutputDataRow("AddressLine3") = AddressLine(2)
                            OutputDataRow("AddressLine4") = AddressLine(3)

                            For LineCount As Integer = 4 To UBound(AddressLine)
                                OutputDataRow("AddressLine4") &= " " & AddressLine(LineCount)
                            Next LineCount
                    End Select
                End If

                OutputDataRow("PostCode") = SourceDataRow("PostCode").ToString.Trim

                OutputDataRow("Phone") = SourceDataRow("add_phone").ToString.Replace(" ", "")

                For ColIndex As Integer = 0 To MergedSource.Columns.Count - 1
                    If IsDBNull(OutputDataRow.Item(ColIndex)) Then OutputDataRow.Item(ColIndex) = ""
                Next ColIndex

                '       End If

            Next SourceDataRow

            AddressDV = New DataView(Address)
            AddressDV.AllowNew = False

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub

    Private Sub MergeUpdates()
        Try
            Dim AddressDV As DataView = New DataView(AddressSource)
            Dim ContactDV As DataView = New DataView(ContactSource)

            MergedSource = AddressSource.Clone() ' Clone the record structure of one of the source files for the merge output
            TempTable.Clear()

            ' Append both sets of headers to temp table
            For Each dr As DataRow In AddressSource.Rows
                TempTable.Rows.Add(dr("client_ref"))
            Next dr

            For Each dr As DataRow In ContactSource.Rows
                TempTable.Rows.Add(dr("client_ref"))
            Next dr

            ' Get distinct line headers
            MasterList = TempTable.DefaultView.ToTable(True)

            ' Build the merged table
            For Each dr As DataRow In MasterList.Rows
                AddressDV.RowFilter = ("client_ref = '" & dr("client_ref") & "'")
                ContactDV.RowFilter = ("client_ref = '" & dr("client_ref") & "'")

                If AddressDV.Count = 0 And ContactDV.Count > 0 Then
                    MergedSource.ImportRow(ContactDV.Item(0).Row)
                ElseIf ContactDV.Count = 0 And AddressDV.Count > 0 Then
                    MergedSource.ImportRow(AddressDV.Item(0).Row)
                Else
                    MergedSource.Rows.Add({AddressDV.Item(0).Item(0), _
                                                             AddressDV.Item(0).Item(1), _
                                                             AddressDV.Item(0).Item(2), _
                                                             AddressDV.Item(0).Item(3), _
                                                             AddressDV.Item(0).Item(4), _
                                                             AddressDV.Item(0).Item(5), _
                                                             AddressDV.Item(0).Item(6), _
                                                             AddressDV.Item(0).Item(7), _
                                                             AddressDV.Item(0).Item(8), _
                                                             AddressDV.Item(0).Item(9), _
                                                             AddressDV.Item(0).Item(10), _
                                                             ContactDV.Item(0).Item(11)})
                End If

            Next dr

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
End Class

﻿Imports System.Configuration
Imports System.IO
Imports CommonLibrary

Public Class frmMain
    Private Returns As New clsReturnsData

    Private SourceRow As Integer, SourceCol As Integer

    Private Const DCAID As String = "7"
    Private Const LineTerminator As String = vbLf
    Private Const NumOfFixedCols As Integer = 57 ' in the input file
    Private Const NumOfWorkFlowItemCols As Integer = 5 ' in the input file
    Private Const Separator As String = "|" ' the same separator is for input and output files

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.


        dtpDateFrom.Value = DateAdd(DateInterval.Day, -8, Now)
        dtpDateTo.Value = DateAdd(DateInterval.Day, -1, Now)
       

        cmsDGV.Items.Add("Cut")
        cmsDGV.Items.Add("Copy")
        cmsDGV.Items.Add("Paste")

    End Sub

    Private Sub frmMain_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            RefreshData()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

#Region "DGVevents"

    Private Sub dgvMain_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMain.CellValueChanged
        Try
            If Not IsDBNull(dgvMain(e.ColumnIndex, e.RowIndex).Value) Then dgvMain(e.ColumnIndex, e.RowIndex).Value = LTrim(dgvMain(e.ColumnIndex, e.RowIndex).Value)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvMain.MouseDown
        Try
            Dim p As Point = dgvMain.PointToClient(Control.MousePosition)
            Dim hit As DataGridView.HitTestInfo = dgvMain.HitTest(p.X, p.Y)

            SourceRow = hit.RowIndex
            SourceCol = hit.ColumnIndex

            If hit.Type = DataGridViewHitTestType.Cell And e.Button = MouseButtons.Left Then
                dgvMain.BeginEdit(False)
                If Not dgvMain(hit.ColumnIndex, hit.RowIndex).IsInEditMode Then
                    dgvMain.DoDragDrop(dgvMain(hit.ColumnIndex, hit.RowIndex).Value, DragDropEffects.Copy Or DragDropEffects.Move)
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvMain.MouseUp
        Try
            If e.Button = MouseButtons.Right Then cmsDGV.Show(sender, e.Location)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvMain.DragEnter
        Try
            If (e.Data.GetDataPresent(DataFormats.Text)) Then
                e.Effect = DragDropEffects.Copy
            Else
                e.Effect = DragDropEffects.None
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvMain.DragDrop
        Try
            Dim p As System.Drawing.Point = dgvMain.PointToClient(Control.MousePosition)
            Dim hit As DataGridView.HitTestInfo = dgvMain.HitTest(p.X, p.Y)

            If hit.Type = DataGridViewHitTestType.Cell AndAlso SourceRow = hit.RowIndex AndAlso Not dgvMain(hit.ColumnIndex, hit.RowIndex).ReadOnly Then ' Check rows as we do not want data moving from one case to another
                dgvMain(SourceCol, SourceRow).Value = ""
                ' if drop position is in the left hand half of the cell, insert data before the existing contents, else insert after
                If p.X < dgvMain.GetCellDisplayRectangle(hit.ColumnIndex, hit.RowIndex, False).Left + (dgvMain.Columns(hit.ColumnIndex).Width / 2) Then
                    dgvMain(hit.ColumnIndex, hit.RowIndex).Value = e.Data.GetData(DataFormats.Text).ToString & dgvMain(hit.ColumnIndex, hit.RowIndex).Value
                Else
                    dgvMain(hit.ColumnIndex, hit.RowIndex).Value &= e.Data.GetData(DataFormats.Text).ToString
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub frmMain_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If dgvMain.SelectedCells.Count = 0 Then Return

        Try
            Dim ClickCell As DataGridViewCell = dgvMain.SelectedCells(0)
            If e.Control Then
                Select Case e.KeyCode
                    Case Keys.C
                        Clipboard.SetText(ClickCell.Value, TextDataFormat.Text)
                    Case Keys.V
                        If Not ClickCell.ReadOnly Then ClickCell.Value = Clipboard.GetText(TextDataFormat.Text)
                    Case Keys.X
                        If Not ClickCell.ReadOnly Then
                            Clipboard.SetText(ClickCell.Value)
                            ClickCell.Value = ""
                        End If
                End Select
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

#End Region

    Private Sub dtpRemitDate_CloseUp(ByVal sender As Object, ByVal e As System.EventArgs)
        RefreshData()
    End Sub

    Private Sub cmdRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        RefreshData()
    End Sub

    Private Sub cmsDGV_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsDGV.ItemClicked
        Try
            Dim ClickCell As DataGridViewCell = dgvMain.SelectedCells(0)
            Select Case e.ClickedItem.Text
                Case "Cut"
                    If Not ClickCell.ReadOnly Then
                        Clipboard.SetText(ClickCell.Value)
                        ClickCell.Value = ""
                    End If
                Case "Copy"
                    Clipboard.SetText(ClickCell.Value, TextDataFormat.Text)
                Case "Paste"
                    If Not ClickCell.ReadOnly Then ClickCell.Value = Clipboard.GetText(TextDataFormat.Text)
            End Select

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub RefreshData()
        Try
            Me.Cursor = Cursors.WaitCursor

            'Returns.GetAddresses(dtpDateFrom.Value, dtpDateTo.Value) commented out TS 12/Aug/2014. Request ref 28108
            Returns.GetDebtors(dtpDateFrom.Value, dtpDateTo.Value)

            With dgvMain
                .DataSource = Returns.Addresses

                For Each DataColumn As DataGridViewTextBoxColumn In .Columns
                    DataColumn.SortMode = DataGridViewColumnSortMode.NotSortable
                Next DataColumn

                '   .Columns("DebtorID").ReadOnly = True

            End With

            Me.Cursor = Cursors.Default

            MsgBox("Refresh complete.", vbInformation + vbOKOnly, Me.Text)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnCreateReturns_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateReturns.Click

        Try

            Dim FilePath As String

            If Returns.Debtors.Count = 0 Then
                MsgBox("No cases found")
                Return
            End If

            Dim FolderBrowserDialog As New FolderBrowserDialog

            If FolderBrowserDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                FilePath = FolderBrowserDialog.SelectedPath & "\"
            Else
                Return
            End If

            ' Now start creating the file
            Dim UpdateFileName As String = FilePath & "ReturnFiles\" & "DCA.DEBT_UPDATE.D." & DCAID.PadLeft(5, "0") & "." & DateTime.Today.ToString("yyyyMMdd") & ".dat"
            Dim UpdateLine(26) As String


            For Each Debtor As DataRowView In Returns.Debtors

                ReDim UpdateLine(26)

                UpdateLine(0) = DCAID
                UpdateLine(1) = "D"
                '2014-08-26 JB add ROSS
                UpdateLine(2) = "ROSS" & Debtor.Item("DebtorID")
                UpdateLine(3) = Debtor.Item("client_ref")
                UpdateLine(4) = Debtor.Item("TrancheID")

                UpdateLine(19) = CDate(Debtor.Item("QueryDate")).ToString("yyyyMMdd")
                UpdateLine(20) = Debtor.Item("QueryCode")

                Select Case Debtor.Item("QueryCode")
                    Case 1 To 28
                        UpdateLine(26) = Debtor.Item("DebtBalance")

                    Case 101 To 108

                        Dim ReturnDetails As String = ""
                        If Not IsDBNull(Debtor("return_details")) Then ReturnDetails = Debtor("return_details")
                        ' Replace in this order so that a CrLf does not become two spaces
                        ReturnDetails = ReturnDetails.Replace(vbCrLf, " ")
                        ReturnDetails = ReturnDetails.Replace(vbCr, " ")
                        ReturnDetails = ReturnDetails.Replace(vbLf, " ")
                        ReturnDetails = ReturnDetails.Replace(vbTab, " ")

                        UpdateLine(22) = Microsoft.VisualBasic.Left(ReturnDetails, 200)

                    Case 201 To 228
                        UpdateLine(17) = CDate(Debtor.Item("ReturnDate")).ToString("yyyyMMdd")
                        UpdateLine(18) = Debtor.Item("QueryCode")
                        UpdateLine(20) = "11"

                    Case 999

                        Returns.Addresses.RowFilter = "DebtorID = " & Debtor("DebtorID")

                        UpdateLine(5) = "1"
                        UpdateLine(6) = Returns.Addresses.Item(0).Item("Title")
                        UpdateLine(7) = Returns.Addresses.Item(0).Item("Forename")
                        UpdateLine(8) = Returns.Addresses.Item(0).Item("Surname")
                        UpdateLine(9) = "1"
                        UpdateLine(10) = Returns.Addresses.Item(0).Item("AddressLine1")
                        UpdateLine(11) = Returns.Addresses.Item(0).Item("AddressLine2")
                        UpdateLine(12) = Returns.Addresses.Item(0).Item("AddressLine3")
                        UpdateLine(13) = Returns.Addresses.Item(0).Item("AddressLine4")
                        UpdateLine(14) = Returns.Addresses.Item(0).Item("PostCode")
                        UpdateLine(15) = "C"
                        UpdateLine(16) = Returns.Addresses.Item(0).Item("Phone")

                        UpdateLine(20) = ""

                        'Case Else
                        '    ReturnsFile &= CommonFields & vbCrLf
                End Select

                AppendToFile(UpdateFileName, String.Join(Separator, UpdateLine) & LineTerminator)

            Next Debtor

            'Returns.Addresses.RowFilter = "" commented out TS 12/Aug/2014. Request ref 28108

            MsgBox("Returns file created.", vbOKOnly, Me.Text)

            Process.Start("explorer.exe", "/select," & UpdateFileName)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub CreateManifestFile(ByVal OutputFolder As String)
        Try
            Dim ManifestLine As String = ""
            Dim ManifestFileName As String = OutputFolder & "DCA.MANIFEST.D." & DCAID.PadLeft(5, "0") & "." & Date.Now.ToString("yyyyMMdd") & ".dat"

            If File.Exists(ManifestFileName) Then File.Delete(ManifestFileName)

            For Each File As IO.FileInfo In New IO.DirectoryInfo(OutputFolder).GetFiles("DCA.*.D." & DCAID.PadLeft(5, "0") & ".*.dat")

                ManifestLine = String.Join(Separator, {File.Name _
                                                      , IO.File.ReadAllBytes(File.FullName).Length.ToString _
                                                      , IO.File.ReadAllLines(File.FullName).Length.ToString _
                                                      , DCAID _
                                                      , "D" _
                                                      , "" _
                                                      , "" _
                                                      , Date.Now.ToString("yyyyMMdd")
                                                      } _
                                          ) & LineTerminator

                AppendToFile(ManifestFileName, ManifestLine)

            Next File

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

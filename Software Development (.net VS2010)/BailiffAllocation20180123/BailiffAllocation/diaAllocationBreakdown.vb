﻿Imports CommonLibrary

Public Class diaAllocationByUserBreakdown

    Private AllocationByRegionData As New clsAllocationByRegionData

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        SetFormIcon(Me)

    End Sub

    Public Sub GetAllocationByUserBreakdown(ByVal CompanyID As Integer, ByVal Username As String)
        Try
            AllocationByRegionData.GetAllocationsByUserBreakdown(CompanyID, Username)

            dgvAllocationBreakdown.DataSource = AllocationByRegionData.AllocationsByUserBreakdown

            FormatGridColumns(dgvAllocationBreakdown)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class
﻿Imports CommonLibrary

Public Class clsAllocationByRegionData

    Private LastLoadDT As New DataTable
    Private EASummDT As New DataTable
    Private SummaryDT As New DataTable
    Private DetailDT As New DataTable
    Private PeriodTypeDT As New DataTable
    Private CompanyDT As New DataTable
    Private AllocationsByUserDT As New DataTable
    Private AllocationsByUserBreakdownDT As New DataTable

    Private LastLoadDV As New DataView
    Private EASummDV As DataView
    Private SummaryDV As DataView
    Private DetailDV As DataView
    Private PeriodTypeDV As DataView
    Private CompanyDV As DataView
    Private AllocationsByUserDV As DataView
    Private AllocationsByUserBreakdownDV As DataView

    Public Sub New()
        Try

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPeriodTypes 'R'", PeriodTypeDT)
            PeriodTypeDV = New DataView(PeriodTypeDT)
            PeriodTypeDV.AllowDelete = False
            PeriodTypeDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetCompanies 'R', " & UserCompanyID.ToString, CompanyDT)
            CompanyDV = New DataView(CompanyDT)
            CompanyDV.AllowDelete = False
            CompanyDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#Region "Public Properties"

    Public ReadOnly Property LastLoadDataView() As DataView
        Get
            LastLoadDataView = LastLoadDV
        End Get
    End Property

    Public ReadOnly Property EASumm() As DataView
        Get
            EASumm = EASummDV
        End Get
    End Property

    Public ReadOnly Property SummaryDataView() As DataView
        Get
            SummaryDataView = SummaryDV
        End Get
    End Property

    Public ReadOnly Property PeriodTypeDataView() As DataView
        Get
            PeriodTypeDataView = PeriodTypeDV
        End Get
    End Property

    Public ReadOnly Property CompanyDataView() As DataView
        Get
            CompanyDataView = CompanyDV
        End Get
    End Property

    Public ReadOnly Property AllocationsByUser() As DataView
        Get
            AllocationsByUser = AllocationsByUserDV
        End Get
    End Property

    Public ReadOnly Property AllocationsByUserBreakdown() As DataView
        Get
            AllocationsByUserBreakdown = AllocationsByUserBreakdownDV
        End Get
    End Property

#End Region

#Region "Public Subs"

    Public Sub GetEASumm(ByVal RegionID As Integer, ByVal CompanyID As Integer)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAsByRegion " & RegionID.ToString & ", " & CompanyID.ToString, EASummDT)
            EASummDV = New DataView(EASummDT)
            EASummDV.AllowDelete = False
            EASummDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetSummary(ByVal ParamList As String, ByVal Abs As Boolean, ByVal DisplaySet As String)
        Try
            Dim SummaryParamList As String = ""

            SummaryDV = Nothing ' Needed to clear column list otherwise new columns eg clientname are added at the end

            If Abs Then SummaryParamList = ParamList & ",'A'" Else SummaryParamList = ParamList & ",'P'"

            SummaryParamList &= "," & DisplaySet

            SummaryDT.Clear()
            SummaryDT.Reset()

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetAllocationByRegionSummary " & SummaryParamList, SummaryDT)
            SummaryDV = New DataView(SummaryDT)
            SummaryDV.AllowDelete = False
            SummaryDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetCases(ByVal ParamList As String, ByVal Period As String)
        Try
            ParamList += "," & Period.ToString

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetCases " & ParamList, DetailDT, True)
            DetailDV = New DataView(DetailDT.DefaultView.ToTable(True))
            DetailDV.AllowDelete = False
            DetailDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetAllocationsByUser(ByVal CompanyID As String)
        Try

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetAllocationsByUser " & CompanyID, AllocationsByUserDT, False)
            AllocationsByUserDV = New DataView(AllocationsByUserDT)
            AllocationsByUserDV.AllowDelete = False
            AllocationsByUserDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub


    Public Sub GetAllocationsByUserBreakdown(ByVal CompanyID As String, ByVal Username As String)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetAllocationsByUserBreakdown " & CompanyID & ", '" & Username & "'", AllocationsByUserBreakdownDT, False)
            AllocationsByUserBreakdownDV = New DataView(AllocationsByUserBreakdownDT)
            AllocationsByUserBreakdownDV.AllowDelete = False
            AllocationsByUserBreakdownDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetLastLoad()
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetLastLoad", LastLoadDT)
            LastLoadDV = New DataView(LastLoadDT)
            LastLoadDV.AllowDelete = False
            LastLoadDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub RefreshDatabase()
        Try
            ExecStoredProc("BailiffAllocation", "EXEC dbo.BuildBailiffAllocation 'I'", 1800)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

End Class

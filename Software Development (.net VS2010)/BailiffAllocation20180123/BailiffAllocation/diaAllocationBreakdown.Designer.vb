﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class diaAllocationByUserBreakdown
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvAllocationBreakdown = New System.Windows.Forms.DataGridView()
        CType(Me.dgvAllocationBreakdown, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvAllocationBreakdown
        '
        Me.dgvAllocationBreakdown.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvAllocationBreakdown.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAllocationBreakdown.Location = New System.Drawing.Point(9, 11)
        Me.dgvAllocationBreakdown.Name = "dgvAllocationBreakdown"
        Me.dgvAllocationBreakdown.ReadOnly = True
        Me.dgvAllocationBreakdown.RowHeadersVisible = False
        Me.dgvAllocationBreakdown.Size = New System.Drawing.Size(697, 440)
        Me.dgvAllocationBreakdown.TabIndex = 0
        '
        'diaAllocationByUserBreakdown
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(715, 461)
        Me.Controls.Add(Me.dgvAllocationBreakdown)
        Me.Name = "diaAllocationByUserBreakdown"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Allocation Breakdown"
        CType(Me.dgvAllocationBreakdown, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvAllocationBreakdown As System.Windows.Forms.DataGridView
End Class

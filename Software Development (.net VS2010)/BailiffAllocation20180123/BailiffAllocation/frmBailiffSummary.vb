﻿Imports CommonLibrary

Public Class frmBailiffSummary

    Private SummaryData As New clsBailiffSummaryData
    Private ListData As New clsListData

    ' These cannot be instantiated here as the datagridviews have not been instantiated at this point but they need to be declared here to achieve the right scope...
    Private BailiffNameGridState As clsGridState
    Private ClientNameGridState As clsGridState
    Private WorkTypeGridState As clsGridState
    Private BailiffTypeGridState As clsGridState
    Private SchemeGridState As clsGridState
    Private EnforcementFeesAppliedGridState As clsGridState
    Private PaymentGridState As clsGridState
    Private VisitedGridState As clsGridState
    Private AddConfirmedGridState As clsGridState
    Private PostcodeAreaGridState As clsGridState
    Private StatusNameGridState As clsGridState
    Private FirstTwoVisitsPAWithinTwoDaysGridState As clsGridState
    Private NumberOfVisitsPAGridState As clsGridState
    Private CGAGridState As clsGridState
    Private AllVisitsPAWithinTwentyDaysGridState As clsGridState
    Private StageNameGridState As clsGridState

    Private ColSort As String

    Private ParamList As String

    Private RightClickColIdx As Integer ' Request ref 64648

#Region "New and open"

    Public Sub New()
        Try

            InitializeComponent()

            ' Now instantiate these

            SetFormIcon(Me)

            cmdRefreshDB.Visible = UserCanRefresh

            If cboCompanyID = -1 Then
                ParamList = UserCompanyID.ToString & ",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null," & BalanceType() & ",3" ' i.e. top level
            Else
                ParamList = cboCompanyID.ToString & ",'" & EAName & "',null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null," & BalanceType() & ",3" ' i.e. top level
            End If


            BailiffNameGridState = New clsGridState(dgvBailiffName)
            ClientNameGridState = New clsGridState(dgvClientName)
            WorkTypeGridState = New clsGridState(dgvWorkType)
            BailiffTypeGridState = New clsGridState(dgvBailiffType)
            SchemeGridState = New clsGridState(dgvSchemeName)
            EnforcementFeesAppliedGridState = New clsGridState(dgvEnforcementFeesApplied)
            PaymentGridState = New clsGridState(dgvPayment)
            VisitedGridState = New clsGridState(dgvVisited)
            AddConfirmedGridState = New clsGridState(dgvAddConfirmed)
            PostcodeAreaGridState = New clsGridState(dgvPostcodeArea)
            StatusNameGridState = New clsGridState(dgvStatusName)
            FirstTwoVisitsPAWithinTwoDaysGridState = New clsGridState(dgvFirstTwoVisitsPAWithinTwoDays)
            NumberOfVisitsPAGridState = New clsGridState(dgvNumberOfVisitsPA)
            CGAGridState = New clsGridState(dgvCGA)
            AllVisitsPAWithinTwentyDaysGridState = New clsGridState(dgvAllVisitsPAWithinTwentyDays)
            StageNameGridState = New clsGridState(dgvStageName)

            AddControlHandlers()

            cmsSummary.Items.Add("View Cases")
            cmsSummary.Items.Add("Copy")
            cmsSummary.Items.Add("Select All")
            cmsSummary.Items.Add("Select Column") ' Request ref 64648

            'cmsList.Items.Add("Consortiums...") commented out TS 09/Sep/2016. Request ref 90025
            cmsList.Items.Add("Regions")
            cmsList.Items.Add("Copy")
            cmsList.Items.Add("Select All")

            cmsForm.Items.Add("Allocation View")
            cmsForm.Items.Add("Post Enforcement View")
            cmsForm.Items.Add("EA Update View")
            cmsForm.Items.Add("Regional View")

            cboEAGroup.Items.Add("All")
            For TeamNum As Integer = 1 To 5
                cboEAGroup.Items.Add(TeamNum.ToString)
            Next TeamNum
            cboEAGroup.Items.Add("Employed")
            cboEAGroup.Items.Add("Self employed")
            cboEAGroup.SelectedIndex = 0

            ' The following commented out TS 09/Sep/2016. Request ref 90025
            'ListData.GetConsortiums(UserCompanyID)

            'Dim Consortium As ToolStripMenuItem = Convert.ChangeType(cmsList.Items(0), GetType(ToolStripMenuItem))

            'For Each Row As DataRow In ListData.ConsortiumDataView.ToTable(True, "Consortium").Rows
            '    Dim SubMenuItem As ToolStripMenuItem = New ToolStripMenuItem(Row.Item(0).ToString)

            '    AddHandler SubMenuItem.Click, AddressOf ConsortiumContextSubMenu_Click
            '    Consortium.DropDownItems.Add(SubMenuItem)
            'Next Row
            ' end of 90025 section

            ' Added TS 09/Feb/2015. Request ref 38930
            ListData.GetPostcodeAreaRegions()

            Dim Region As ToolStripMenuItem = Convert.ChangeType(cmsList.Items(0), GetType(ToolStripMenuItem)) ' 1 changed to 0 TS 09/Sep/2016. Request ref 90025

            For Each Row As DataRow In ListData.PostcodeAreaRegionDataView.ToTable(True, "RegionDesc").Rows
                Dim RegionSubMenuItem As ToolStripMenuItem = New ToolStripMenuItem(Row.Item(0).ToString)

                AddHandler RegionSubMenuItem.Click, AddressOf RegionContextSubMenu_Click
                Region.DropDownItems.Add(RegionSubMenuItem)
            Next Row
            ' end of 38930

            SummaryData.GetSummary(ParamList, True, "P")
            dgvSummary.DataSource = SummaryData.GridDataView
            FormatGridColumns(dgvSummary, GetSelectedBailiffTypes)

            ListData.GetBailiffList(ParamList & ",'Total'") ' total added TS 28/Apr/2017. Request ref 94672
            ListData.GetBailiffNameList(ParamList & ",'Total'") ' total added TS 28/Apr/2017. Request ref 94672
            SetListGrids()

            FormatListColumns()

            cboPeriodType.DataSource = SummaryData.PeriodTypeDataView
            cboPeriodType.ValueMember = "PeriodTypeID"
            cboPeriodType.DisplayMember = "PeriodTypeDesc"

            cboCompany.DataSource = SummaryData.CompanyDataView
            cboCompany.ValueMember = "CompanyID"
            cboCompany.DisplayMember = "CompanyDesc"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmSummary_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            RemoveSelectionHandlers()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmSummary_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            SetSelections() 'Arrays will be nothing so clear all selections - by default the first row in a datagridview is always selected
            dgvSummary.ClearSelection()

            radAbs.Checked = True
            If cboCompanyID = -1 Then
                cboCompany.Text = UserCompany
            Else
                cboCompany.Text = cboCompanyName

                For Each EA As DataGridViewRow In dgvBailiffName.Rows

                    If EA.Cells(0).Value = EAName Then
                        EA.Selected = True
                        dgvBailiffName.FirstDisplayedScrollingRowIndex = EA.Index
                    End If

                Next EA

                cboCompanyID = -1

            End If

            cboPeriodType.SelectedValue = 3
            cboDisplaySet.Text = "Periods"
            cboEAGroup.SelectedIndex = 0

            AddHandler radAbs.CheckedChanged, AddressOf radAbs_CheckedChanged
            AddHandler cboEAGroup.SelectedValueChanged, AddressOf cboEAGroup_SelectedValueChanged

            AddSelectionHandlers()

            SetcmdRefreshTip()
            cmdRefreshDB.Visible = False

            ' This section added TS 08/Dec/2015. Request ref 66905
            dgvEnforcementFeesApplied.Columns(0).ToolTipText = "Where fees added or on a link at stages 500, 550 and 575."
            dgvPayment.Columns(0).ToolTipText = "Has there been a remitted payment in the last year?"
            dgvVisited.Columns(0).ToolTipText = "Has the cases ever had a visit?"
            dgvNumberOfVisitsPA.Columns(0).ToolTipText = "The number of visits since the last allocation."
            dgvFirstTwoVisitsPAWithinTwoDays.Columns(0).ToolTipText = "Where the second visit is within 2 weekdays of the first."
            dgvAllVisitsPAWithinTwentyDays.Columns(0).ToolTipText = "Are all visits within 20 calendar days of each other?"
            ' end of 66905 section

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmBailiffSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        Try
            If e.Button = MouseButtons.Right Then
                cmsForm.Show(sender, e.Location)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmBailiff_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            RemoveSelectionHandlers()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Refresh"

    Private Sub RefreshGrid()
        Try
            ParamList = cboCompany.SelectedValue.ToString & ","
            ParamList &= GetParam(dgvBailiffName, "BailiffName") & ","
            ParamList &= cboEAGroup.SelectedIndex & ","
            ParamList &= GetParam(dgvClientName, "ClientName") & ","
            ParamList &= GetParam(dgvWorkType, "WorkType") & ","
            ParamList &= GetParam(dgvBailiffType, "BailiffType") & ","
            ParamList &= GetParam(dgvSchemeName, "SchemeName") & ","
            ParamList &= GetParam(dgvEnforcementFeesApplied, "EnforcementFeesApplied") & ","
            ParamList &= GetParam(dgvPayment, "Payment") & ","
            ParamList &= GetParam(dgvVisited, "Visited") & ","
            ParamList &= GetParam(dgvAddConfirmed, "AddConfirmed") & ","
            ParamList &= GetParam(dgvPostcodeArea, "PostcodeArea") & ","
            ParamList &= GetParam(dgvStatusName, "StatusName") & ","
            ParamList &= GetParam(dgvFirstTwoVisitsPAWithinTwoDays, "FirstTwoVisitsPAWithinTwoDays") & ","
            ParamList &= GetParam(dgvNumberOfVisitsPA, "NumberOfVisitsPA") & ","
            ParamList &= GetParam(dgvCGA, "CGA") & ","
            ParamList &= GetParam(dgvAllVisitsPAWithinTwentyDays, "AllVisitsPAWithinTwentyDays") & ","
            ParamList &= GetParam(dgvStageName, "StageName") & ","
            ParamList &= BalanceType() & "," ' added TS 20/Oct/2016. Request ref 71897
            ParamList &= cboPeriodType.SelectedValue.ToString

            PreRefresh(Me)

            RemoveSelectionHandlers()

            GetSelections()

            SummaryData.GetSummary(ParamList, radAbs.Checked, cboDisplaySet.Text.Substring(0, 1))
            dgvSummary.DataSource = SummaryData.GridDataView

            FormatGridColumns(dgvSummary, GetSelectedBailiffTypes)

            ListData.GetBailiffList(ParamList & ",'Total'") ' total added TS 28/Apr/2017. Request ref 94672
            SetListGrids()
            FormatListColumns()

            SetSelections()

            AddSelectionHandlers()

            PostRefresh(Me)

            dgvSummary.ClearSelection() ' The first cell always get selected

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub RefreshLists()
        ' added TS 28/Apr/2017. Request ref 94672
        Try
            Dim SelectedPeriod As String
            ParamList = cboCompany.SelectedValue.ToString & ","

            If dgvSummary.SelectedCells.Count = 1 And dgvBailiffName.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("BailiffName").Value & "',"
            Else
                ParamList &= GetParam(dgvBailiffName, "BailiffName") & ","
            End If

            ParamList &= cboEAGroup.SelectedIndex & ","

            If dgvSummary.SelectedCells.Count = 1 And dgvClientName.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("ClientName").Value & "',"
            Else
                ParamList &= GetParam(dgvClientName, "ClientName") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvWorkType.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("WorkType").Value & "',"
            Else
                ParamList &= GetParam(dgvWorkType, "WorkType") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvBailiffType.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("BailiffType").Value & "',"
            Else
                ParamList &= GetParam(dgvBailiffType, "BailiffType") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvSchemeName.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("SchemeName").Value & "',"
            Else
                ParamList &= GetParam(dgvSchemeName, "SchemeName") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvEnforcementFeesApplied.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("EnforcementFeesApplied").Value & "',"
            Else
                ParamList &= GetParam(dgvEnforcementFeesApplied, "EnforcementFeesApplied") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvPayment.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("Payment").Value & "',"
            Else
                ParamList &= GetParam(dgvPayment, "Payment") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvVisited.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("Visited").Value & "',"
            Else
                ParamList &= GetParam(dgvVisited, "Visited") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvAddConfirmed.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("AddConfirmed").Value & "',"
            Else
                ParamList &= GetParam(dgvAddConfirmed, "AddConfirmed") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvPostcodeArea.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("PostcodeArea").Value & "',"
            Else
                ParamList &= GetParam(dgvPostcodeArea, "PostcodeArea") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvStatusName.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("StatusName").Value & "',"
            Else
                ParamList &= GetParam(dgvStatusName, "StatusName") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvFirstTwoVisitsPAWithinTwoDays.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("FirstTwoVisitsPAWithinTwoDays").Value & "',"
            Else
                ParamList &= GetParam(dgvFirstTwoVisitsPAWithinTwoDays, "FirstTwoVisitsPAWithinTwoDays") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvNumberOfVisitsPA.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("NumberOfVisitsPA").Value & "',"
            Else
                ParamList &= GetParam(dgvNumberOfVisitsPA, "NumberOfVisitsPA") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvCGA.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("CGA").Value & "',"
            Else
                ParamList &= GetParam(dgvCGA, "CGA") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvAllVisitsPAWithinTwentyDays.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("AllVisitsPAWithinTwentyDays").Value & "',"
            Else
                ParamList &= GetParam(dgvAllVisitsPAWithinTwentyDays, "AllVisitsPAWithinTwentyDays") & ","
            End If

            If dgvSummary.SelectedCells.Count = 1 And dgvStageName.SelectedRows.Count > 1 Then
                ParamList &= "'" & dgvSummary.Rows(dgvSummary.SelectedCells(0).RowIndex).Cells("StageName").Value & "',"
            Else
                ParamList &= GetParam(dgvStageName, "StageName")
            End If

            ParamList &= "," & BalanceType()

            ParamList &= "," & cboPeriodType.SelectedValue.ToString

            PreRefresh(Me)

            RemoveSelectionHandlers()

            GetSelections()

            ' add the period to the param list if only one cell in dgvSummary is selected and it is a data column.
            If dgvSummary.SelectedCells.Count = 1 AndAlso DataColumnsList.Contains(dgvSummary.Columns(dgvSummary.SelectedCells(0).ColumnIndex).Name) Then
                SelectedPeriod = "," & dgvSummary.Columns(dgvSummary.SelectedCells(0).ColumnIndex).Name
            Else
                SelectedPeriod = ",'Total'"
            End If

            ListData.GetBailiffList(ParamList & SelectedPeriod)
            SetListGrids()
            FormatListColumns()

            SetSelections()

            AddSelectionHandlers()

            PostRefresh(Me)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function GetParam(ByRef DataGrid As DataGridView, ByVal ColumnName As String) As String
        GetParam = Nothing

        Try
            ' Used when selection criteria change
            Dim Param As String = Nothing

            For Each dr As DataGridViewRow In DataGrid.SelectedRows
                If Not IsNothing(Param) Then Param += vbTab
                Param += dr.Cells(ColumnName).Value
            Next dr

            If IsNothing(Param) Then
                Param = "null"
            Else
                Param = "'" & Param.Replace("'", "''''") & "'" ' Portal task ref 16716
            End If

            Return Param

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function GetParam(ByVal ParamColumn As String, ByRef Cell As DataGridViewCell, ByRef ListDataGridView As DataGridView)
        GetParam = Nothing

        Try
            ' Used when detail for a particular cell in the summary grid is retrieved
            Dim Param As String = ""

            If IsNothing(Cell.DataGridView.Columns(ParamColumn)) Then
                ' The column may not be present as only one criteria is applicable
                If ListDataGridView.SelectedRows.Count = 1 Then
                    Param = "'" & ListDataGridView.SelectedRows(0).Cells(0).Value.Replace("'", "''''") & "'" ' Portal task ref 16716
                Else
                    Param = "null"
                End If
            Else
                Param = "'" & Cell.DataGridView.Item(ParamColumn, Cell.RowIndex).Value.Replace("'", "''''") & "'" ' Portal task ref 16716
            End If

            Return Param

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function
#End Region

#Region "Grid selection events"

    Private Sub cboCompany_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboCompany.SelectedValueChanged
        Try
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    CType(Control, DataGridView).FirstDisplayedScrollingRowIndex = 0
                    CType(Control, DataGridView).ClearSelection()
                End If
            Next Control

            RefreshGrid()

            ' The following commented out TS 09/Sep/2016. Request ref 90025
            ' added TS 26/Apr/2016. Request ref 78340
            'ListData.GetConsortiums(cboCompany.SelectedValue)

            'Dim Consortium As ToolStripMenuItem = Convert.ChangeType(cmsList.Items(0), GetType(ToolStripMenuItem))

            'Consortium.DropDownItems.Clear()

            'For Each Row As DataRow In ListData.ConsortiumDataView.ToTable(True, "Consortium").Rows
            '    Dim ConsortiumSubMenuItem As ToolStripMenuItem = New ToolStripMenuItem(Row.Item(0).ToString)

            '    AddHandler ConsortiumSubMenuItem.Click, AddressOf ConsortiumContextSubMenu_Click
            '    Consortium.DropDownItems.Add(ConsortiumSubMenuItem)
            'Next Row
            ' end of 78340 section
            ' end of 90025 section

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboPeriodType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboPeriodType.SelectedValueChanged
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboDisplaySet_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboDisplaySet.SelectedValueChanged
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ' This is needed to make mousewheel scroll list items
            sender.Select()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                GetSelections()
                RemoveSelectionHandlers()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                SetSelections()
                AddSelectionHandlers()
            End If

            If e.Button = MouseButtons.Right Then
                'If sender.name <> "dgvClientName" And sender.name <> "dgvSchemeName" Then cmsList.Items(0).Visible = False Else cmsList.Items(0).Visible = True commented out TS 09/Sep/2016. Request ref 90025
                If sender.name <> "dgvPostcodeArea" Then cmsList.Items(0).Visible = False Else cmsList.Items(0).Visible = True ' added TS 09/Feb/2015. Request ref 38930 1 changed to 0. Request ref 90025
                cmsList.Show(sender, e.Location)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseDown
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
                dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected = True
            End If

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                ColSort = SummaryData.GridDataView.Sort
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseUp
        Try
            ' New section. Request ref 64648
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            RightClickColIdx = hti.ColumnIndex

            If RightClickColIdx <> -1 AndAlso DataColumnsList.Contains(dgvSummary.Columns(RightClickColIdx).Name) Then
                cmsSummary.Items(3).Visible = True
            Else
                cmsSummary.Items(3).Visible = False
            End If

            ' end of request ref 64648 section

            If e.Button = MouseButtons.Right Then cmsSummary.Show(dgvSummary, e.Location)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                If ColSort <> "" Then SummaryData.GridDataView.Sort += "," & ColSort
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles dgvSummary.SelectionChanged commented out TS 03/May/2017. Request ref 94672
        Try
            Dim TotalCases As Integer = 0

            If dgvSummary.SelectedCells.Count = 0 Then
                lblSummary.Text = ""
            Else
                For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                    If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then TotalCases += Cell.Value
                Next Cell
                lblSummary.Text = TotalCases.ToString & " case" & If(TotalCases > 1, "s", "")
            End If

            RefreshLists() ' Request ref 94672

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ShowDetail()
        Try
            Dim DetailParamList As String
            Dim Detail As New diaCaseDetail(Me)

            PreRefresh(Me)

            For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then

                    DetailParamList = cboCompany.SelectedValue.ToString & ","
                    DetailParamList &= GetParam("BailiffName", Cell, dgvBailiffName) & ","
                    DetailParamList &= cboEAGroup.SelectedIndex & ","
                    DetailParamList &= GetParam("ClientName", Cell, dgvClientName) & ","
                    DetailParamList &= GetParam("WorkType", Cell, dgvWorkType) & ","
                    DetailParamList &= GetParam("BailiffType", Cell, dgvBailiffType) & ","
                    DetailParamList &= GetParam("SchemeName", Cell, dgvSchemeName) & ","
                    DetailParamList &= GetParam("EnforcementFeesApplied", Cell, dgvEnforcementFeesApplied) & ","
                    DetailParamList &= GetParam("Payment", Cell, dgvPayment) & ","
                    DetailParamList &= GetParam("Visited", Cell, dgvVisited) & ","
                    DetailParamList &= GetParam("AddConfirmed", Cell, dgvAddConfirmed) & ","
                    DetailParamList &= GetParam("PostcodeArea", Cell, dgvPostcodeArea) & ","
                    DetailParamList &= GetParam("StatusName", Cell, dgvStatusName) & ","
                    DetailParamList &= GetParam("FirstTwoVisitsPAWithinTwoDays", Cell, dgvFirstTwoVisitsPAWithinTwoDays) & ","
                    DetailParamList &= GetParam("NumberOfVisitsPA", Cell, dgvNumberOfVisitsPA) & ","
                    DetailParamList &= GetParam("CGA", Cell, dgvCGA) & ","
                    DetailParamList &= GetParam("AllVisitsPAWithinTwentyDays", Cell, dgvAllVisitsPAWithinTwentyDays) & ","
                    DetailParamList &= GetParam("StageName", Cell, dgvStageName) & ","
                    DetailParamList &= BalanceType() & "," ' added TS 20/Oct/2016. Request ref 71897
                    DetailParamList &= cboPeriodType.SelectedValue.ToString

                    Detail.AddCases(DetailParamList, dgvSummary.Columns(Cell.ColumnIndex).Name)

                End If
            Next Cell

            PostRefresh(Me)

            If Detail.CaseCount > 0 Then Detail.ShowDialog() ' Portal task ref 16716     
            Detail.Dispose()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "'All' and 'Clear' button click events"

    Private Sub cmdBailiffNameAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBailiffNameAll.Click
        Try
            dgvBailiffName.SelectAll()
            BailiffNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdBailiffNameClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBailiffNameClear.Click
        Try
            dgvBailiffName.ClearSelection()
            BailiffNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientNameAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientNameAll.Click
        Try
            dgvClientName.SelectAll()
            ClientNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientNameClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientNameClear.Click
        Try
            dgvClientName.ClearSelection()
            ClientNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdWorkTypeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWorkTypeAll.Click
        Try
            dgvWorkType.SelectAll()
            WorkTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdWorkTypeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWorkTypeClear.Click
        Try
            dgvWorkType.ClearSelection()
            WorkTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdBailiffTypeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBailiffTypeAll.Click
        Try
            dgvBailiffType.SelectAll()
            BailiffTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdBailiffTypeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBailiffTypeClear.Click
        Try
            dgvBailiffType.ClearSelection()
            BailiffTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdSchemeNameAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeNameAll.Click
        Try
            dgvSchemeName.SelectAll()
            SchemeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdSchemeNameClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeNameClear.Click
        Try
            dgvSchemeName.ClearSelection()
            SchemeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdEnforcementFeesAppliedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEnforcementFeesAppliedAll.Click
        Try
            dgvEnforcementFeesApplied.SelectAll()
            EnforcementFeesAppliedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdEnforcementFeesAppliedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEnforcementFeesAppliedClear.Click
        Try
            dgvEnforcementFeesApplied.ClearSelection()
            EnforcementFeesAppliedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPaymentAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPaymentAll.Click
        Try
            dgvPayment.SelectAll()
            PaymentGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPaymentClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPaymentClear.Click
        Try
            dgvPayment.ClearSelection()
            PaymentGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdVisitedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVisitedAll.Click
        Try
            dgvVisited.SelectAll()
            VisitedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdVisitedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVisitedClear.Click
        Try
            dgvVisited.ClearSelection()
            VisitedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAddConfirmedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfirmedAll.Click
        Try
            dgvAddConfirmed.SelectAll()
            AddConfirmedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAddConfirmedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfirmedClear.Click
        Try
            dgvAddConfirmed.ClearSelection()
            AddConfirmedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeAreaAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaAll.Click
        Try
            dgvPostcodeArea.SelectAll()
            PostcodeAreaGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeAreaClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaClear.Click
        Try
            dgvPostcodeArea.ClearSelection()
            PostcodeAreaGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdStatusNameAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStatusNameAll.Click
        Try
            dgvStatusName.SelectAll()
            StatusNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdStatusNameClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStatusNameClear.Click
        Try
            dgvStatusName.ClearSelection()
            StatusNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdFirstTwoVisitsPAWithinTwoDaysAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFirstTwoVisitsPAWithinTwoDaysAll.Click
        Try
            dgvFirstTwoVisitsPAWithinTwoDays.SelectAll()
            FirstTwoVisitsPAWithinTwoDaysGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdFirstTwoVisitsPAWithinTwoDaysClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdFirstTwoVisitsPAWithinTwoDaysClear.Click
        Try
            dgvFirstTwoVisitsPAWithinTwoDays.ClearSelection()
            FirstTwoVisitsPAWithinTwoDaysGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdNumberOfVisitsPAAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNumberOfVisitsPAAll.Click
        Try
            dgvNumberOfVisitsPA.SelectAll()
            NumberOfVisitsPAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdNumberOfVisitsPAClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNumberOfVisitsPAClear.Click
        Try
            dgvNumberOfVisitsPA.ClearSelection()
            NumberOfVisitsPAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCGAAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCGAAll.Click
        Try
            dgvCGA.SelectAll()
            CGAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCGAClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCGAClear.Click
        Try
            dgvCGA.ClearSelection()
            CGAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAllVisitsPAWithinTwentyDaysAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAllVisitsPAWithinTwentyDaysAll.Click
        Try
            dgvAllVisitsPAWithinTwentyDays.SelectAll()
            AllVisitsPAWithinTwentyDaysGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAllVisitsPAWithinTwentyDaysClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAllVisitsPAWithinTwentyDaysClear.Click
        Try
            dgvAllVisitsPAWithinTwentyDays.ClearSelection()
            AllVisitsPAWithinTwentyDaysGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdStageNameAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageNameAll.Click
        Try
            dgvStageName.SelectAll()
            StageNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdStageNameClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageNameClear.Click
        Try
            dgvStageName.ClearSelection()
            StageNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "Handlers"

    Private Sub RemoveSelectionHandlers()
        Try
            ' Need to find a way to do these at once but these SelectionChanged event is specific to the datagridview class and not control - the collection on the form
            RemoveHandler dgvBailiffName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvWorkType.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvBailiffType.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvEnforcementFeesApplied.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPayment.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvVisited.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvAddConfirmed.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvStatusName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvFirstTwoVisitsPAWithinTwoDays.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvNumberOfVisitsPA.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvCGA.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvAllVisitsPAWithinTwentyDays.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvStageName.SelectionChanged, AddressOf dgvSelectionChanged

            RemoveHandler dgvSummary.SelectionChanged, AddressOf dgvSummary_SelectionChanged ' Added TS Request ref 94672

            RemoveHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            RemoveHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
            RemoveHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddSelectionHandlers()
        Try
            AddHandler dgvBailiffName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvWorkType.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvBailiffType.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvEnforcementFeesApplied.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPayment.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvVisited.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvAddConfirmed.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvStatusName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvFirstTwoVisitsPAWithinTwoDays.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvNumberOfVisitsPA.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvCGA.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvAllVisitsPAWithinTwentyDays.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvStageName.SelectionChanged, AddressOf dgvSelectionChanged

            AddHandler dgvSummary.SelectionChanged, AddressOf dgvSummary_SelectionChanged ' Added TS Request ref 94672

            AddHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            AddHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
            AddHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddControlHandlers()
        Try
            ' All these grids and buttons call the same events so no point declaring them all separately
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    AddHandler Control.MouseDown, AddressOf dgvMouseDown
                    AddHandler Control.MouseUp, AddressOf dgvMouseUp
                    AddHandler Control.MouseEnter, AddressOf dgvMouseEnter
                End If
            Next Control

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "DataGridView methods"

    Private Sub SetListGrids()
        Try
            dgvBailiffName.DataSource = ListData.BailiffName
            BailiffNameGridState.SetSort()

            dgvClientName.DataSource = ListData.ClientName
            ClientNameGridState.SetSort()

            dgvWorkType.DataSource = ListData.WorkType
            WorkTypeGridState.SetSort()

            dgvBailiffType.DataSource = ListData.BailiffType
            BailiffTypeGridState.SetSort()

            dgvSchemeName.DataSource = ListData.SchemeName
            SchemeGridState.SetSort()

            dgvCGA.DataSource = ListData.CGA
            CGAGridState.SetSort()

            dgvPayment.DataSource = ListData.Payment
            PaymentGridState.SetSort()

            dgvVisited.DataSource = ListData.Visited
            VisitedGridState.SetSort()

            dgvAddConfirmed.DataSource = ListData.AddConfirmed
            AddConfirmedGridState.SetSort()

            dgvPostcodeArea.DataSource = ListData.PostcodeArea
            PostcodeAreaGridState.SetSort()

            dgvStatusName.DataSource = ListData.StatusName
            StatusNameGridState.SetSort()

            dgvEnforcementFeesApplied.DataSource = ListData.EnforcementFeesApplied
            EnforcementFeesAppliedGridState.SetSort()

            dgvFirstTwoVisitsPAWithinTwoDays.DataSource = ListData.FirstTwoVisitsPAWithinTwoDays
            FirstTwoVisitsPAWithinTwoDaysGridState.SetSort()

            dgvNumberOfVisitsPA.DataSource = ListData.NumberOfVisitsPA
            NumberOfVisitsPAGridState.SetSort()

            dgvAllVisitsPAWithinTwentyDays.DataSource = ListData.AllVisitsPAWithinTwentyDays
            AllVisitsPAWithinTwentyDaysGridState.SetSort()

            dgvStageName.DataSource = ListData.StageName
            StageNameGridState.SetSort()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub FormatListColumns()
        Try
            ' This is slightly different to settings in modCommon as we want some of the fields wider than they need to be for presentation
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    If Control.Name <> "dgvSummary" Then ' This is not a list grid
                        For Each Column In CType(Control, DataGridView).Columns
                            Select Case Column.Name
                                Case "Total"
                                    Column.Width = 40
                                Case "BailiffName"
                                    Column.Width = 100
                                    Column.HeaderText = "Bailiff"
                                Case "ClientName"
                                    Column.Width = 100
                                    Column.HeaderText = "Client"
                                Case "WorkType"
                                    Column.Width = 84
                                    Column.HeaderText = "Work Type"
                                Case "SchemeName"
                                    Column.Width = 100
                                    Column.HeaderText = "Scheme"
                                Case "BailiffType"
                                    Column.Width = 84
                                    Column.HeaderText = "Type"
                                Case "EnforcementFeesApplied"
                                    Column.Width = 120
                                    Column.HeaderText = "Enforcement Fees"
                                Case "Payment"
                                    Column.Width = 70
                                Case "Visited"
                                    Column.Width = 70
                                Case "AddConfirmed"
                                    Column.Width = 70
                                    Column.HeaderText = "Confirmed"
                                Case "PostcodeArea"
                                    Column.Width = 55
                                    Column.HeaderText = "Area"
                                Case "StatusName"
                                    Column.Width = 90
                                    Column.HeaderText = "Status"
                                Case "FirstTwoVisitsPAWithinTwoDays"
                                    Column.width = 90
                                    Column.HeaderText = "Visit 2 < 2"
                                Case "NumberOfVisitsPA"
                                    Column.width = 60
                                    Column.HeaderText = "# Visits"
                                Case "CGA"
                                    Column.Width = 74
                                Case "AllVisitsPAWithinTwentyDays"
                                    Column.width = 74
                                    Column.HeaderText = "Visits < 20"
                                Case "StageName"
                                    Column.Width = 84
                                    Column.HeaderText = "Stage"
                                Case Else
                                    Column.Width = 40
                            End Select
                        Next Column
                    End If
                End If
            Next Control

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub GetSelections()
        Try
            BailiffNameGridState.GetState()
            ClientNameGridState.GetState()
            WorkTypeGridState.GetState()
            BailiffTypeGridState.GetState()
            SchemeGridState.GetState()
            EnforcementFeesAppliedGridState.GetState()
            PaymentGridState.GetState()
            VisitedGridState.GetState()
            AddConfirmedGridState.GetState()
            PostcodeAreaGridState.GetState()
            StatusNameGridState.GetState()
            FirstTwoVisitsPAWithinTwoDaysGridState.GetState()
            NumberOfVisitsPAGridState.GetState()
            CGAGridState.GetState()
            AllVisitsPAWithinTwentyDaysGridState.GetState()
            StageNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetSelections()
        Try
            BailiffNameGridState.SetState()
            ClientNameGridState.SetState()
            WorkTypeGridState.SetState()
            BailiffTypeGridState.SetState()
            SchemeGridState.SetState()
            EnforcementFeesAppliedGridState.SetState()
            PaymentGridState.SetState()
            VisitedGridState.SetState()
            AddConfirmedGridState.SetState()
            PostcodeAreaGridState.SetState()
            StatusNameGridState.SetState()
            FirstTwoVisitsPAWithinTwoDaysGridState.SetState()
            NumberOfVisitsPAGridState.SetState()
            CGAGridState.SetState()
            AllVisitsPAWithinTwentyDaysGridState.SetState()
            StageNameGridState.SetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Additional functionality"

    Private Sub cmdClearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClearAll.Click
        Try
            RemoveSelectionHandlers()

            cmdBailiffNameClear_Click(sender, New System.EventArgs)
            cmdClientNameClear_Click(sender, New System.EventArgs)
            cmdWorkTypeClear_Click(sender, New System.EventArgs)
            cmdBailiffTypeClear_Click(sender, New System.EventArgs)
            cmdSchemeNameClear_Click(sender, New System.EventArgs)
            cmdEnforcementFeesAppliedClear_Click(sender, New System.EventArgs)
            cmdPaymentClear_Click(sender, New System.EventArgs)
            cmdVisitedClear_Click(sender, New System.EventArgs)
            cmdAddConfirmedClear_Click(sender, New System.EventArgs)
            cmdPostcodeAreaClear_Click(sender, New System.EventArgs)
            cmdStatusNameClear_Click(sender, New System.EventArgs)
            cmdFirstTwoVisitsPAWithinTwoDaysClear_Click(sender, New System.EventArgs)
            cmdNumberOfVisitsPAClear_Click(sender, New System.EventArgs)
            cmdCGAClear_Click(sender, New System.EventArgs)
            cmdAllVisitsPAWithinTwentyDaysClear_Click(sender, New System.EventArgs)
            cmdStageNameClear_Click(sender, New System.EventArgs)

            dgvSummary.ClearSelection() ' Request ref 94672

            AddSelectionHandlers()

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radAbs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles radAbs.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radPerc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPerc.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.Click
        Try
            If MsgBox("This could take several minutes, are you sure?", vbYesNo + vbDefaultButton2 + vbQuestion) = vbYes Then
                PreRefresh(Me)
                SummaryData.RefreshDatabase()
                RefreshGrid()
                PostRefresh(Me)
                MsgBox("Refresh complete.", vbOKOnly + vbInformation)
                SetcmdRefreshTip()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.MouseLeave
        Try
            ' on XP machines there is a bug that prevents the tooltip showing a second time. This addresses the problem.
            ToolTip1.Active = False
            ToolTip1.Active = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetcmdRefreshTip()
        Try
            SummaryData.GetLastLoad()
            ToolTip1.SetToolTip(cmdRefreshDB, "Last refreshed at " & SummaryData.LastLoadDataView.Item(0).Item(0).ToString & " by " & SummaryData.LastLoadDataView.Item(0).Item(1).ToString)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsSummary_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsSummary.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "View Cases"
                    ShowDetail()
                Case "Copy"
                    dgvSummary.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    If Not dgvSummary.GetClipboardContent Is Nothing Then Clipboard.SetDataObject(dgvSummary.GetClipboardContent())
                Case "Select All"
                    dgvSummary.SelectAll()
                Case "Select Column" ' Request ref 64648

                    RemoveHandler dgvSummary.SelectionChanged, AddressOf dgvSummary_SelectionChanged ' Remove the handler to avoid refreshing the lists on the first select and recalculating the total for every select

                    If Not My.Computer.Keyboard.CtrlKeyDown Then dgvSummary.ClearSelection()

                    For Each DataRow As DataGridViewRow In dgvSummary.Rows
                        If DataRow.Index = dgvSummary.Rows.Count - 1 Then ' Before selecting the last cell reinstatate the handler so that the total is recalculated
                            AddHandler dgvSummary.SelectionChanged, AddressOf dgvSummary_SelectionChanged
                        End If

                        DataRow.Cells(RightClickColIdx).Selected = True
                    Next DataRow

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsList_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsList.ItemClicked
        Try
            ' Slightly more complicated than the summary grid as this event fires for all other datagridviews.
            Select Case e.ClickedItem.Text
                Case "Copy"
                    sender.SourceControl.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    If Not sender.SourceControl.GetClipboardContent() Is Nothing Then Clipboard.SetDataObject(sender.SourceControl.GetClipboardContent())
                Case "Select All"
                    sender.SourceControl.SelectAll()
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsForm_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsForm.ItemClicked
        Try
            ' The dispose is required to clear any forms that have called others. Dispose show changed from ShowDialog request ref 75670
            Select Case e.ClickedItem.Text
                Case "Allocation View"
                    If FormOpen("frmCaseSummary") Then
                        frmCaseSummary.Activate()
                    Else
                        frmCaseSummary.Dispose()
                        frmCaseSummary.Show()
                    End If
                Case "Post Enforcement View"
                    If FormOpen("frmPostEnforcementSummary") Then
                        frmPostEnforcementSummary.Activate()
                    Else
                        frmPostEnforcementSummary.Dispose()
                        frmPostEnforcementSummary.Show()
                    End If
                Case "EA Update View"
                    If FormOpen("frmEAUpdateSummary") Then
                        frmEAUpdateSummary.Activate()
                    Else
                        frmEAUpdateSummary.Dispose()
                        frmEAUpdateSummary.Show()
                    End If
                Case "Regional View"
                    If FormOpen("frmAllocationByRegionSummary") Then
                        frmAllocationByRegionSummary.Activate()
                    Else
                        frmAllocationByRegionSummary.Dispose()
                        frmAllocationByRegionSummary.Show()
                    End If
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    ' commented out TS 09/Sep/2016. Request ref 90026. The code to call this is commented out so this will never fire but for completeness I've commented this too.
    'Private Sub ConsortiumContextSubMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim ScrollbarSet As Boolean = False

    '        RemoveSelectionHandlers()

    '        dgvClientName.ClearSelection()
    '        dgvSchemeName.ClearSelection()

    '        ListData.ConsortiumDataView.RowFilter = "Consortium = '" & CType(sender, ToolStripItem).Text & "'"

    '        For Each ConsortiumDataRow As DataRow In ListData.ConsortiumDataView.ToTable.Rows
    '            For Each GridDataRow As DataGridViewRow In dgvClientName.Rows
    '                If GridDataRow.Cells("ClientName").Value = ConsortiumDataRow.Item("ClientName") Then
    '                    GridDataRow.Selected = True
    '                    If Not ScrollbarSet Then
    '                        dgvClientName.FirstDisplayedScrollingRowIndex = GridDataRow.Index
    '                        ScrollbarSet = True
    '                    End If
    '                End If
    '            Next GridDataRow

    '            ScrollbarSet = False

    '            For Each GridDataRow As DataGridViewRow In dgvSchemeName.Rows
    '                If GridDataRow.Cells("SchemeName").Value = ConsortiumDataRow.Item("SchemeName") Then
    '                    GridDataRow.Selected = True
    '                    If Not ScrollbarSet Then
    '                        dgvSchemeName.FirstDisplayedScrollingRowIndex = GridDataRow.Index
    '                        ScrollbarSet = True
    '                    End If
    '                End If
    '            Next GridDataRow
    '        Next ConsortiumDataRow

    '        AddSelectionHandlers()
    '        RefreshGrid()

    '    Catch ex As Exception
    '        HandleException(ex)
    '    End Try
    'End Sub

    Private Sub RegionContextSubMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' added TS 09/Feb/2015. Request ref 38930
        Try
            Dim ScrollbarSet As Boolean = False

            RemoveSelectionHandlers()

            ListData.PostcodeAreaRegionDataView.RowFilter = "RegionDesc = '" & CType(sender, ToolStripItem).Text & "'"

            For Each PostcodeAreaRegionDataRow As DataRow In ListData.PostcodeAreaRegionDataView.ToTable.Rows
                For Each GridDataRow As DataGridViewRow In dgvPostcodeArea.Rows
                    If GridDataRow.Cells("PostcodeArea").Value = PostcodeAreaRegionDataRow.Item("PostcodeArea") Then
                        GridDataRow.Selected = True
                        If Not ScrollbarSet Then
                            dgvPostcodeArea.FirstDisplayedScrollingRowIndex = GridDataRow.Index
                            ScrollbarSet = True
                        End If
                    End If
                Next GridDataRow
            Next PostcodeAreaRegionDataRow

            AddSelectionHandlers()

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboEAGroup_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboEAGroup.SelectedValueChanged
        Try
            RefreshScroll = False

            ' Not ideal but we need to refresh the bailiff list before the main refresh to exclude any bailiffs selected that would not be in the top list
            RemoveSelectionHandlers()

            cmdBailiffNameClear_Click(sender, New System.EventArgs) ' Added TS 15/Apr/2016. Request ref 79559

            GetSelections()
            ListData.GetBailiffNameList(ParamList)
            SetSelections()

            AddSelectionHandlers()

            RefreshGrid()

            RefreshScroll = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function GetSelectedBailiffTypes() As String() ' added TS 24/Apr/2013 Portal task ref 14922
        GetSelectedBailiffTypes = Nothing

        Try
            Dim SelectedBailiffTypes As New List(Of String)

            SummaryData.GetSelectedBailiffTypes(ParamList)

            For Each BailiffType As DataRow In SummaryData.SelectedBailiffTypeDataView.ToTable.Rows
                SelectedBailiffTypes.Add(BailiffType.Item("BailiffType"))
            Next BailiffType

            If Not IsNothing(SelectedBailiffTypes) Then GetSelectedBailiffTypes = SelectedBailiffTypes.ToArray

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function
    Private Sub radLow_Click(sender As Object, e As System.EventArgs) Handles radLow.Click
        Try
            If radLow.Checked Then
                radLow.Checked = False
            Else
                radLow.Checked = True
                radHigh.Checked = False
            End If

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radHigh_Click(sender As Object, e As System.EventArgs) Handles radHigh.Click
        Try
            If radHigh.Checked Then
                radHigh.Checked = False
            Else
                radHigh.Checked = True
                radLow.Checked = False
            End If

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function BalanceType() As String
        BalanceType = "A"

        Try
            If radLow.Checked Then
                BalanceType = "L"
            ElseIf radHigh.Checked Then
                BalanceType = "H"
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

#End Region

End Class

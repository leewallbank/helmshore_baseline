﻿Imports CommonLibrary

Public Class frmAllocationByRegionSummary

    Private AllocationByRegionData As New clsAllocationByRegionData
    Private ListData As New clsListData

    Private ClientGridState As clsGridState
    Private PostcodeAreaGridState As clsGridState
    Private PostcodeDistrictGridState As clsGridState
    Private StageComplianceGridState As clsGridState
    Private StageEnforcementGridState As clsGridState
    Private WorkTypeGridState As clsGridState
    Private SchemeGridState As clsGridState
    Private DebtYearGridState As clsGridState
    Private NumberOfVisitsGridState As clsGridState
    Private CGAGridState As clsGridState
    Private InYearGridState As clsGridState
    Private AddConfirmedGridState As clsGridState
    Private PaymentGridState As clsGridState
    Private LinkedPIFGridState As clsGridState
    Private ArrangementBrokenGridState As clsGridState

    Private ColSort As String

    Private ParamList As String
    Private RightClickColIdx As Integer

#Region "New and open"

    Public Sub New()

        InitializeComponent()

        Try

            SetFormIcon(Me)

            cmdRefreshDB.Visible = UserCanRefresh

            ParamList = UserCompanyID.ToString & ",-1,null,null,null,null,null,null,null,null,null,null,null,null,null,null," & BalanceType() & ",5,18" ' i.e. top level

            ' Now instantiate these

            ClientGridState = New clsGridState(dgvClientName)
            PostcodeAreaGridState = New clsGridState(dgvPostcodeArea)
            PostcodeDistrictGridState = New clsGridState(dgvPostcodeDistrict)
            StageComplianceGridState = New clsGridState(dgvStageNameCompliance)
            StageEnforcementGridState = New clsGridState(dgvStageNameEnforcement)
            WorkTypeGridState = New clsGridState(dgvWorkType)
            SchemeGridState = New clsGridState(dgvSchemeName)
            DebtYearGridState = New clsGridState(dgvDebtYear)
            NumberOfVisitsGridState = New clsGridState(dgvNumberOfVisits)
            CGAGridState = New clsGridState(dgvCGA)
            InYearGridState = New clsGridState(dgvInYear)
            AddConfirmedGridState = New clsGridState(dgvAddConfirmed)
            PaymentGridState = New clsGridState(dgvPayment)
            LinkedPIFGridState = New clsGridState(dgvLinkedPIF)
            ArrangementBrokenGridState = New clsGridState(dgvArrangementBroken)

            AddControlHandlers()

            cmsSummary.Items.Add("View Cases")
            cmsSummary.Items.Add("Copy")
            cmsSummary.Items.Add("Select All")
            cmsSummary.Items.Add("Select Column")

            cmsEASummary.Items.Add("View Live Cases")

            cmsAllocationsByUser.Items.Add("View Cases")
            cmsAllocationsByUser.Items.Add("View Breakdown")

            cmsForm.Items.Add("Allocation View")
            cmsForm.Items.Add("Bailiff View")
            cmsForm.Items.Add("Post Enforcement View")
            cmsForm.Items.Add("EA Update View")

            AllocationByRegionData.GetEASumm(-1, cboCompany.SelectedValue)
            dgvEASummary.DataSource = AllocationByRegionData.EASumm

            AllocationByRegionData.GetAllocationsByUser(UserCompanyID.ToString)
            dgvAllocationsByUser.DataSource = AllocationByRegionData.AllocationsByUser

            ListData.GetEAByRegionList(ParamList)
            ListData.GetPostcodeAreaRegions()
            dgvRegion.DataSource = ListData.Region
            SetListGrids()
            FormatListColumns()

            AllocationByRegionData.GetSummary(ParamList, True, "P")
            dgvSummary.DataSource = AllocationByRegionData.SummaryDataView
            FormatGridColumns(dgvSummary, GetStages)

            cboPeriodType.DataSource = AllocationByRegionData.PeriodTypeDataView
            cboPeriodType.ValueMember = "PeriodTypeID"
            cboPeriodType.DisplayMember = "PeriodTypeDesc"

            cboCompany.DataSource = AllocationByRegionData.CompanyDataView
            cboCompany.ValueMember = "CompanyID"
            cboCompany.DisplayMember = "CompanyDesc"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmSummary_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try

            SetSelections() 'Arrays will be nothing so clear all selections - by default the first row in a datagridview is always selected
            dgvEASummary.ClearSelection()
            dgvSummary.ClearSelection()
            dgvRegion.ClearSelection()
            dgvAllocationsByUser.ClearSelection()

            radAbs.Checked = True
            cboCompany.Text = UserCompany
            cboPeriodType.SelectedValue = 18
            cboDisplaySet.Text = "Periods"
            cboAvailability.SelectedIndex = 5

            AddHandler radAbs.CheckedChanged, AddressOf radAbs_CheckedChanged

            AddSelectionHandlers()

            SetcmdRefreshTip()

            HighlightNationalEAs()

            dgvInYear.Columns(0).ToolTipText = "Was the case loaded in this financial year?"
            dgvPayment.Columns(0).ToolTipText = "Has there been a remitted payment in the last year?"
            dgvLinkedPIF.Columns(0).ToolTipText = "Where a linked case has PIF in the last year."
            dgvNumberOfVisits.Columns(0).ToolTipText = "The total number of visits on the case."

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmAllocationByRegion_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            RemoveSelectionHandlers()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmCaseSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        Try
            If e.Button = MouseButtons.Right Then
                cmsForm.Show(sender, e.Location)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "Refresh"

    Private Sub RefreshGrid(Optional ByVal RefreshEASumm As Boolean = False)
        Try
            ParamList = cboCompany.SelectedValue.ToString & ","

            If dgvRegion.SelectedRows.Count > 0 Then
                ParamList &= dgvRegion.SelectedRows(0).Cells("RegionID").Value.ToString & ","
            Else
                ParamList &= "-1,"
            End If

            ParamList &= GetParam(dgvClientName, "ClientName") & ","
            ParamList &= GetParam(dgvPostcodeArea, "PostcodeArea") & ","
            ParamList &= GetParam(dgvPostcodeDistrict, "PostcodeDistrict") & ","
            ParamList &= GetParam(dgvStageNameCompliance, "StageNameCompliance") & ","
            ParamList &= GetParam(dgvWorkType, "WorkType") & ","
            ParamList &= GetParam(dgvSchemeName, "SchemeName") & ","
            ParamList &= GetParam(dgvDebtYear, "DebtYear") & ","
            ParamList &= GetParam(dgvNumberOfVisits, "NumberOfVisits") & ","
            ParamList &= GetParam(dgvCGA, "CGA") & ","
            ParamList &= GetParam(dgvInYear, "InYear") & ","
            ParamList &= GetParam(dgvAddConfirmed, "AddConfirmed") & ","
            ParamList &= GetParam(dgvPayment, "Payment") & ","
            ParamList &= GetParam(dgvLinkedPIF, "LinkedPIF") & ","
            ParamList &= GetParam(dgvArrangementBroken, "ArrangementBroken") & ","

            ParamList &= BalanceType() & ","

            ParamList &= cboAvailability.SelectedIndex & ","
            ParamList &= cboPeriodType.SelectedValue.ToString

            PreRefresh(Me)

            RemoveSelectionHandlers()

            GetSelections()

            If RefreshEASumm Then
                If dgvRegion.SelectedRows.Count > 0 Then
                    AllocationByRegionData.GetEASumm(dgvRegion.SelectedRows(0).Cells("RegionID").Value, cboCompany.SelectedValue)
                Else
                    AllocationByRegionData.GetEASumm(-1, cboCompany.SelectedValue)
                End If
            End If

            HighlightNationalEAs()

            AllocationByRegionData.GetSummary(ParamList, radAbs.Checked, cboDisplaySet.Text.Substring(0, 1))
            dgvSummary.DataSource = AllocationByRegionData.SummaryDataView

            FormatGridColumns(dgvSummary, GetStages)

            ListData.GetEAByRegionList(ParamList)
            SetListGrids()
            FormatListColumns()

            SetSelections()

            dgvEASummary.ClearSelection()

            AddSelectionHandlers()

            PostRefresh(Me)

            dgvSummary.ClearSelection() ' The first cell always get selected


        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function GetParam(ByRef DataGrid As DataGridView, ByVal ColumnName As String) As String
        GetParam = Nothing

        Try
            ' Used when selection criteria change
            Dim Param As String = Nothing

            For Each dr As DataGridViewRow In DataGrid.SelectedRows
                If Not IsNothing(Param) Then Param += vbTab
                Param &= dr.Cells(ColumnName).Value
            Next dr

            If DataGrid.Name = "dgvStageNameCompliance" Then
                For Each dr As DataGridViewRow In dgvStageNameEnforcement.SelectedRows
                    If Not IsNothing(Param) Then Param += vbTab
                    Param += dr.Cells("StageNameEnforcement").Value
                Next dr
            End If

            If IsNothing(Param) Then
                Param = "null"
            Else
                Param = "'" & Param.Replace("'", "''''") & "'" ' Portal task ref 16716
            End If

            Return Param

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

#End Region

#Region "Grid selection events"

    Private Sub cboCompany_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboCompany.SelectedValueChanged
        Try
            RemoveSelectionHandlers()

            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView And Control.Name <> "dgvSummary" Then
                    If CType(Control, DataGridView).RowCount > 0 Then
                        CType(Control, DataGridView).FirstDisplayedScrollingRowIndex = 0
                        CType(Control, DataGridView).ClearSelection()
                    End If
                End If
            Next Control

            AddSelectionHandlers()

            RefreshGrid(True)

            AllocationByRegionData.GetAllocationsByUser(cboCompany.SelectedValue.ToString)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvRegion_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles dgvRegion.SelectedionChanged
        Try
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView And Not {"dgvRegion", "dgvSummary"}.Contains(Control.Name) Then
                    If CType(Control, DataGridView).RowCount > 0 Then
                        CType(Control, DataGridView).FirstDisplayedScrollingRowIndex = 0
                        CType(Control, DataGridView).ClearSelection()
                    End If
                End If
            Next Control

            RefreshGrid(True)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboPeriodType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboPeriodType.SelectedValueChanged
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboDisplaySet_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboDisplaySet.SelectedValueChanged
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboAvailability_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboAvailability.SelectedValueChanged
        Try
            If cboAvailability.SelectedIndex = 0 Then
                cboPeriodType.SelectedValue = 18
                cboPeriodType.Enabled = False
            Else
                cboPeriodType.Enabled = True
            End If
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ' This is needed to make mousewheel scroll list items
            sender.Select()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                GetSelections()
                RemoveSelectionHandlers()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                SetSelections()
                AddSelectionHandlers()
            End If

            If e.Button = MouseButtons.Right And hti.Type = DataGridViewHitTestType.Cell Then ' only show the context menu if a cell is being clicked on - not headers or scroll bars
                If sender.name = "dgvEASummary" Then
                    cmsEASummary.Show(sender, e.Location)
                ElseIf sender.name = "dgvAllocationsByUser" Then
                    cmsAllocationsByUser.Show(sender, e.Location)
                End If
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvSummary.DataBindingComplete
        Try
            ' This needs to be handled differently. I'm not sure why but doing this later causes events to fire. Recommended practice on the following url
            ' http://msdn.microsoft.com/en-us/library/system.windows.forms.datagridviewcolumn.visible.aspx

            If dgvSummary.Columns.Contains("RowID") Then dgvSummary.Columns("RowID").Visible = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseDown
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
                dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected = True
            End If

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                ColSort = AllocationByRegionData.SummaryDataView.Sort
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseUp
        Try
            ' New section. Request ref 64648
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            RightClickColIdx = hti.ColumnIndex

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                If ColSort <> "" Then AllocationByRegionData.SummaryDataView.Sort += "," & ColSort
            End If

            If e.Button = MouseButtons.Right Then cmsSummary.Show(dgvSummary, e.Location)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSummary.SelectionChanged
        Try
            Dim TotalCases As Integer = 0

            If dgvSummary.SelectedCells.Count = 0 Or radAbs.Checked = False Then
                lblSummary.Text = ""
            Else
                For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                    If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then TotalCases += Cell.Value
                Next Cell
                lblSummary.Text = TotalCases.ToString & " case" & If(TotalCases > 1, "s", "")
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvEASummary_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvEASummary.MouseDown
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvEASummary.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
                dgvEASummary.Item(hti.ColumnIndex, hti.RowIndex).Selected = True
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvAllocationsByUser_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvAllocationsByUser.MouseDown
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvAllocationsByUser.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
                dgvAllocationsByUser.Item(hti.ColumnIndex, hti.RowIndex).Selected = True
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub


    Private Sub dgvEASummary_SelectionChanged(sender As Object, e As System.EventArgs) 'Handles dgvEASummary.SelectionChanged

        ' added TS 09/Jan/2018
        cboCompanyID = cboCompany.SelectedValue
        cboCompanyName = cboCompany.Text
        EAName = dgvEASummary.SelectedRows(0).Cells(1).Value

        Dim frmbailiffsummary As New frmBailiffSummary
        frmBailiffSummary.Show()

    End Sub

#End Region

#Region "DataGridView methods"

    Private Sub SetListGrids()
        Try

            dgvClientName.DataSource = ListData.ClientName
            ClientGridState.SetSort()

            dgvPostcodeArea.DataSource = ListData.PostcodeArea
            PostcodeAreaGridState.SetSort()

            dgvPostcodeDistrict.DataSource = ListData.PostcodeDistrict
            PostcodeDistrictGridState.SetSort()

            dgvStageNameCompliance.DataSource = ListData.StageNameCompliance
            StageComplianceGridState.SetSort()

            dgvStageNameEnforcement.DataSource = ListData.StageNameEnforcement
            StageEnforcementGridState.SetSort()

            dgvWorkType.DataSource = ListData.WorkType
            WorkTypeGridState.SetSort()

            dgvSchemeName.DataSource = ListData.SchemeName
            SchemeGridState.SetSort()

            dgvDebtYear.DataSource = ListData.DebtYear
            DebtYearGridState.SetSort()

            dgvNumberOfVisits.DataSource = ListData.NumberOfVisits
            NumberOfVisitsGridState.SetSort()

            dgvCGA.DataSource = ListData.CGA
            CGAGridState.SetSort()

            dgvInYear.DataSource = ListData.InYear
            InYearGridState.SetSort()

            dgvAddConfirmed.DataSource = ListData.AddConfirmed
            AddConfirmedGridState.SetSort()

            dgvPayment.DataSource = ListData.Payment
            PaymentGridState.SetSort()

            dgvLinkedPIF.DataSource = ListData.LinkedPIF
            LinkedPIFGridState.SetSort()

            dgvArrangementBroken.DataSource = ListData.ArrangementBroken
            ArrangementBrokenGridState.SetSort()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub FormatListColumns()
        Try
            ' This is slightly different to settings in modCommon as we want some of the fields wider than they need to be for presentation
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    If Control.Name <> "dgvSummary" Then ' This is not a list grid
                        For Each Column In CType(Control, DataGridView).Columns
                            Select Case Column.Name
                                Case "RegionDesc"
                                    Column.Width = 160
                                Case "RegionID"
                                    Column.Visible = False
                                Case "BailiffID"
                                    Column.Visible = False ' needed to show EA cases
                                Case "BailiffName"
                                    Column.Width = 100
                                    Column.HeaderText = "EA Name"
                                Case "add_postcode"
                                    Column.Width = 80
                                    Column.HeaderText = "Postcode"
                                Case "Allocated"
                                    Column.Width = 65
                                    Column.HeaderText = "# Addr"
                                Case "NotVisited"
                                    Column.Width = 65
                                    Column.HeaderText = "0 Visits"
                                Case "ClientName"
                                    Column.Width = 100
                                    Column.HeaderText = "Client"
                                Case "PostcodeArea"
                                    Column.Width = 60
                                    Column.HeaderText = "Area"
                                Case "PostcodeDistrict"
                                    Column.Width = 55
                                    Column.HeaderText = "District"
                                Case "StageNameCompliance"
                                    Column.Width = 121
                                    Column.HeaderText = "Compliance"
                                Case "StageNameEnforcement"
                                    Column.Width = 121
                                    Column.HeaderText = "Enforcement"
                                Case "WorkType"
                                    Column.Width = 100
                                    Column.HeaderText = "Work Type"
                                Case "SchemeName"
                                    Column.Width = 100
                                    Column.HeaderText = "Scheme"
                                Case "DebtYear"
                                    Column.Width = 55
                                    Column.HeaderText = "Year"
                                Case "NumberOfVisits"
                                    Column.Width = 55
                                Case "CGA"
                                    Column.Width = 70
                                Case "InYear"
                                    Column.Width = 70
                                Case "AddConfirmed"
                                    Column.Width = 70
                                    Column.HeaderText = "Confirmed"
                                Case "Payment"
                                    Column.Width = 70
                                Case "LinkedPIF"
                                    Column.Width = 76
                                    Column.HeaderText = "Linked PIF"
                                Case "ArrangementBroken"
                                    Column.Width = 70
                                    Column.HeaderText = "Broken"
                                Case "TotalAddress"
                                    Column.width = 40
                                    Column.HeaderText = "Addr"
                                Case "CreatedBy"
                                    Column.Width = 78
                                    Column.HeaderText = "User"
                                Case "IsNational"
                                    Column.Visible = False
                                Case Else
                                    Column.Width = 40
                            End Select
                        Next Column
                    End If
                End If
            Next Control

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub GetSelections()
        Try
            ClientGridState.GetState()
            PostcodeAreaGridState.GetState()
            PostcodeDistrictGridState.GetState()
            StageComplianceGridState.GetState()
            StageEnforcementGridState.GetState()
            WorkTypeGridState.GetState()
            SchemeGridState.GetState()
            DebtYearGridState.GetState()
            NumberOfVisitsGridState.GetState()
            CGAGridState.GetState()
            InYearGridState.GetState()
            AddConfirmedGridState.GetState()
            PaymentGridState.GetState()
            LinkedPIFGridState.GetState()
            ArrangementBrokenGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetSelections()
        Try
            ClientGridState.SetState()
            PostcodeAreaGridState.SetState()
            PostcodeDistrictGridState.SetState()
            StageComplianceGridState.SetState()
            StageEnforcementGridState.SetState()
            WorkTypeGridState.SetState()
            SchemeGridState.SetState()
            DebtYearGridState.SetState()
            NumberOfVisitsGridState.SetState()
            CGAGridState.SetState()
            InYearGridState.SetState()
            AddConfirmedGridState.SetState()
            PaymentGridState.SetState()
            LinkedPIFGridState.SetState()
            ArrangementBrokenGridState.SetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ShowDetail()
        Try
            Dim DetailParamList As String
            Dim Detail As New diaCaseDetail(Me)

            PreRefresh(Me)

            For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then
                    DetailParamList = cboCompany.SelectedValue.ToString & ","

                    If dgvRegion.SelectedRows.Count > 0 Then
                        DetailParamList &= dgvRegion.SelectedRows(0).Cells("RegionID").Value.ToString & ","
                    Else
                        DetailParamList &= "-1,"
                    End If

                    DetailParamList &= GetParam(dgvClientName, "ClientName") & ","
                    DetailParamList &= GetParam(dgvPostcodeArea, "PostcodeArea") & ","
                    DetailParamList &= GetParam(dgvPostcodeDistrict, "PostcodeDistrict") & ","
                    DetailParamList &= GetParam(dgvStageNameCompliance, "StageNameCompliance") & ","
                    DetailParamList &= GetParam(dgvWorkType, "WorkType") & ","
                    DetailParamList &= GetParam(dgvSchemeName, "SchemeName") & ","
                    DetailParamList &= GetParam(dgvDebtYear, "DebtYear") & ","
                    DetailParamList &= GetParam(dgvNumberOfVisits, "NumberOfVisits") & ","
                    DetailParamList &= GetParam(dgvCGA, "CGA") & ","
                    DetailParamList &= GetParam(dgvInYear, "InYear") & ","
                    DetailParamList &= GetParam(dgvAddConfirmed, "AddConfirmed") & ","
                    DetailParamList &= GetParam(dgvPayment, "Payment") & ","
                    DetailParamList &= GetParam(dgvLinkedPIF, "LinkedPIF") & ","
                    DetailParamList &= GetParam(dgvArrangementBroken, "ArrangementBroken") & ","


                    DetailParamList &= BalanceType() & ","

                    DetailParamList &= cboAvailability.SelectedIndex.ToString & ","
                    DetailParamList &= cboPeriodType.SelectedValue.ToString


                    Detail.AddCases(DetailParamList, dgvSummary.Columns(Cell.ColumnIndex).Name)

                End If
            Next Cell

            PostRefresh(Me)

            If Detail.CaseCount > 0 Then Detail.ShowDialog() ' Portal task ref 16716
            Detail.Dispose()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "'All' and 'Clear' button click events"

    Private Sub cmdClearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClearAll.Click
        Try
            RemoveSelectionHandlers()

            cmdClientClear_Click(sender, New System.EventArgs)
            cmdPostcodeAreaClear_Click(sender, New System.EventArgs)
            cmdPostcodeDistrictClear_Click(sender, New System.EventArgs)
            cmdStageComplianceClear_Click(sender, New System.EventArgs)
            cmdStageEnforcementClear_Click(sender, New System.EventArgs)
            cmdWorkTypeClear_Click(sender, New System.EventArgs)
            cmdSchemeClear_Click(sender, New System.EventArgs)
            cmdDebtYearClear_Click(sender, New System.EventArgs)
            cmdNumberOfVisitsClear_Click(sender, New System.EventArgs)
            cmdCGAClear_Click(sender, New System.EventArgs)
            cmdInYearClear_Click(sender, New System.EventArgs)
            cmdAddConfirmedClear_Click(sender, New System.EventArgs)
            cmdPaymentClear_Click(sender, New System.EventArgs)
            cmdLinkedPIFClear_Click(sender, New System.EventArgs)
            cmdArrangementBrokenClear_Click(sender, New System.EventArgs)

            cmdRegionClear_Click(sender, New System.EventArgs)

            AddSelectionHandlers()

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientAll.Click
        Try
            dgvClientName.SelectAll()
            ClientGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientClear.Click
        Try
            dgvClientName.ClearSelection()
            ClientGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeAreaAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaAll.Click
        Try
            dgvPostcodeArea.SelectAll()
            PostcodeAreaGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeAreaClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaClear.Click
        Try
            dgvPostcodeArea.ClearSelection()
            PostcodeAreaGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeDistrictAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeDistrictAll.Click
        Try
            dgvPostcodeDistrict.SelectAll()
            PostcodeDistrictGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeDistrictClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeDistrictClear.Click
        Try
            dgvPostcodeDistrict.ClearSelection()
            PostcodeDistrictGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdStageComplianceAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageComplianceAll.Click
        Try
            dgvStageNameCompliance.SelectAll()
            StageComplianceGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdStageComplianceClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageComplianceClear.Click
        Try
            dgvStageNameCompliance.ClearSelection()
            StageComplianceGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdStageEnforcementAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageEnforcementAll.Click
        Try
            dgvStageNameEnforcement.SelectAll()
            StageEnforcementGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdStageEnforcementClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageEnforcementClear.Click
        Try
            dgvStageNameEnforcement.ClearSelection()
            StageEnforcementGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdWorkTypeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWorkTypeAll.Click
        Try
            dgvWorkType.SelectAll()
            WorkTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdWorkTypeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWorkTypeClear.Click
        Try
            dgvWorkType.ClearSelection()
            WorkTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdSchemeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeAll.Click
        Try
            dgvSchemeName.SelectAll()
            SchemeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdSchemeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeClear.Click
        Try
            dgvSchemeName.ClearSelection()
            SchemeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdDebtYearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDebtYearAll.Click
        Try
            dgvDebtYear.SelectAll()
            DebtYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdDebtYearClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDebtYearClear.Click
        Try
            dgvDebtYear.ClearSelection()
            DebtYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdNumberOfVisitsAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNumberOfVisitsAll.Click
        Try
            dgvNumberOfVisits.SelectAll()
            NumberOfVisitsGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdNumberOfVisitsClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNumberOfVisitsClear.Click
        Try
            dgvNumberOfVisits.ClearSelection()
            NumberOfVisitsGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCGAAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCGAAll.Click
        Try
            dgvCGA.SelectAll()
            CGAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCGAClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCGAClear.Click
        Try
            dgvCGA.ClearSelection()
            CGAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdInYearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdInYearAll.Click
        Try
            dgvInYear.SelectAll()
            InYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdInYearClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdInYearClear.Click
        Try
            dgvInYear.ClearSelection()
            InYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAddConfirmedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfirmedAll.Click
        Try
            dgvAddConfirmed.SelectAll()
            AddConfirmedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAddConfirmedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfirmedClear.Click
        Try
            dgvAddConfirmed.ClearSelection()
            AddConfirmedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPaymentAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPaymentAll.Click
        Try
            dgvPayment.SelectAll()
            PaymentGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPaymentClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPaymentClear.Click
        Try
            dgvPayment.ClearSelection()
            PaymentGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLinkedPIFAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLinkedPIFAll.Click
        Try
            dgvLinkedPIF.SelectAll()
            LinkedPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLinkedPIFClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLinkedPIFClear.Click
        Try
            dgvLinkedPIF.ClearSelection()
            LinkedPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdArrangementBrokenAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdArrangementBrokenAll.Click
        Try
            dgvArrangementBroken.SelectAll()
            ArrangementBrokenGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdArrangementBrokenClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdArrangementBrokenClear.Click
        Try
            dgvArrangementBroken.ClearSelection()
            ArrangementBrokenGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRegionClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRegionClear.Click
        Try
            dgvRegion.ClearSelection()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "Handlers"

    Private Sub RemoveSelectionHandlers()
        Try
            ' Need to find a way to do these at once but these SelectionChanged event is specific to the datagridview class and not control - the collection on the form
            RemoveHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPostcodeDistrict.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvStageNameCompliance.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvStageNameEnforcement.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvWorkType.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvDebtYear.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvNumberOfVisits.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvCGA.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvInYear.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvAddConfirmed.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPayment.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvLinkedPIF.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvArrangementBroken.SelectionChanged, AddressOf dgvSelectionChanged

            RemoveHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            RemoveHandler dgvRegion.SelectionChanged, AddressOf dgvRegion_SelectionChanged
            RemoveHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
            RemoveHandler cboAvailability.SelectedValueChanged, AddressOf cboAvailability_SelectedValueChanged
            RemoveHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged

            RemoveHandler dgvEASummary.SelectionChanged, AddressOf dgvEASummary_SelectionChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddSelectionHandlers()
        Try
            AddHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPostcodeDistrict.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvStageNameCompliance.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvStageNameEnforcement.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvWorkType.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvDebtYear.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvNumberOfVisits.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvCGA.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvInYear.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvAddConfirmed.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPayment.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvLinkedPIF.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvArrangementBroken.SelectionChanged, AddressOf dgvSelectionChanged

            AddHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            AddHandler dgvRegion.SelectionChanged, AddressOf dgvRegion_SelectionChanged
            AddHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
            AddHandler cboAvailability.SelectedValueChanged, AddressOf cboAvailability_SelectedValueChanged
            AddHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged

            AddHandler dgvEASummary.SelectionChanged, AddressOf dgvEASummary_SelectionChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddControlHandlers()
        Try
            ' All these grids and buttons call the same events so no point declaring them all separately
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView And Control.Name <> "dgvSummary" Then
                    AddHandler Control.MouseDown, AddressOf dgvMouseDown
                    AddHandler Control.MouseUp, AddressOf dgvMouseUp
                    AddHandler Control.MouseEnter, AddressOf dgvMouseEnter
                End If
            Next Control

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Other functionality"
    Private Sub radAbs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles radAbs.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radPerc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPerc.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.Click
        Try
            If MsgBox("This could take several minutes, are you sure?", vbYesNo + vbDefaultButton2 + vbQuestion) = vbYes Then
                PreRefresh(Me)
                AllocationByRegionData.RefreshDatabase()
                RefreshGrid()
                PostRefresh(Me)
                MsgBox("Refresh complete.", vbOKOnly + vbInformation)
                SetcmdRefreshTip()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.MouseLeave
        Try
            ' on XP machines there is a bug that prevents the tooltip showing a second time. This addresses the problem.
            ToolTip1.Active = False
            ToolTip1.Active = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetcmdRefreshTip()
        Try
            AllocationByRegionData.GetLastLoad()
            ToolTip1.SetToolTip(cmdRefreshDB, "Last refreshed at " & AllocationByRegionData.LastLoadDataView.Item(0).Item(0).ToString & " by " & AllocationByRegionData.LastLoadDataView.Item(0).Item(1).ToString)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsSummary_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsSummary.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "View Cases"
                    ShowDetail()
                Case "Copy"
                    dgvSummary.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    If Not dgvSummary.GetClipboardContent Is Nothing Then Clipboard.SetDataObject(dgvSummary.GetClipboardContent())
                Case "Select All"
                    dgvSummary.SelectAll()
                Case "Select Column"

                    RemoveHandler dgvSummary.SelectionChanged, AddressOf dgvSummary_SelectionChanged ' Remove the handler to avoid refreshing the lists on the first select and recalculating the total for every select

                    If Not My.Computer.Keyboard.CtrlKeyDown Then dgvSummary.ClearSelection()

                    For Each DataRow As DataGridViewRow In dgvSummary.Rows
                        If DataRow.Index = dgvSummary.Rows.Count - 1 Then ' Before selecting the last cell reinstatate the handler so that the total is recalculated
                            AddHandler dgvSummary.SelectionChanged, AddressOf dgvSummary_SelectionChanged
                        End If

                        DataRow.Cells(RightClickColIdx).Selected = True
                    Next DataRow

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Sub

    Private Sub cmsEASummary_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsEASummary.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "View Live Cases"
                    If dgvEASummary.SelectedRows.Count > 0 Then ' This is not really necessary as cmsEASummary will only show when a cell is right clicked on which automatically selects it
                        Dim Detail As New diaCaseDetail(Me)
                        Detail.AddCasesByBailiff(dgvEASummary.SelectedRows(0).Cells(0).Value)

                        If Detail.CaseCount > 0 Then Detail.ShowDialog()
                        Detail.Dispose()
                    End If
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsAllocationsByUser_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsAllocationsByUser.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "View Cases"
                    If dgvAllocationsByUser.SelectedRows.Count > 0 Then ' This is not really necessary as cmsAllocationsByUser will only show when a cell is right clicked on which automatically selects it
                        Dim Detail As New diaCaseDetail(Me)
                        Detail.AddCasesByAllocationUser(cboCompany.SelectedValue, dgvAllocationsByUser.SelectedRows(0).Cells(0).Value)

                        If Detail.CaseCount > 0 Then Detail.ShowDialog()
                        Detail.Dispose()
                    End If
                Case "View Breakdown"
                    Dim AllocationByUserBreakdown As New diaAllocationByUserBreakdown()
                    AllocationByUserBreakdown.GetAllocationByUserBreakdown(cboCompany.SelectedValue, dgvAllocationsByUser.SelectedRows(0).Cells(0).Value)

                    AllocationByUserBreakdown.ShowDialog()

                    AllocationByUserBreakdown.Dispose()
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsForm_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsForm.ItemClicked
        Try
            ' The dispose is required to clear any forms that have called others. Dispose show changed from ShowDialog request ref 75670
            Select Case e.ClickedItem.Text
                Case "Allocation View"
                    If FormOpen("frmCaseSummary") Then
                        frmCaseSummary.Activate()
                    Else
                        frmCaseSummary.Dispose()
                        frmCaseSummary.Show()
                    End If
                Case "Bailiff View"
                    If FormOpen("frmBailiffSummary") Then
                        frmBailiffSummary.Activate()
                    Else
                        frmBailiffSummary.Dispose()
                        frmBailiffSummary.Show()
                    End If
                Case "Post Enforcement View"
                    If FormOpen("frmPostEnforcementSummary") Then
                        frmPostEnforcementSummary.Activate()
                    Else
                        frmPostEnforcementSummary.Dispose()
                        frmPostEnforcementSummary.Show()
                    End If
                Case "EA Update View"
                    If FormOpen("frmEAUpdateSummary") Then
                        frmEAUpdateSummary.Activate()
                    Else
                        frmEAUpdateSummary.Dispose()
                        frmEAUpdateSummary.Show()
                    End If
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radLow_Click(sender As Object, e As System.EventArgs) Handles radLow.Click
        Try
            If radLow.Checked Then
                radLow.Checked = False
            Else
                radLow.Checked = True
                radHigh.Checked = False
            End If

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radHigh_Click(sender As Object, e As System.EventArgs) Handles radHigh.Click
        Try
            If radHigh.Checked Then
                radHigh.Checked = False
            Else
                radHigh.Checked = True
                radLow.Checked = False
            End If

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub HighlightNationalEAs()
        Try
            For Each Row As DataGridViewRow In dgvEASummary.Rows
                If Row.Cells("IsNational").Value.ToString = "Y" Then
                    Row.DefaultCellStyle.BackColor = Color.LightCyan
                End If

            Next Row

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Functions"

    Private Function BalanceType() As String
        BalanceType = "A"

        Try
            If radLow.Checked Then
                BalanceType = "L"
            ElseIf radHigh.Checked Then
                BalanceType = "H"
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function GetStages() As String()
        GetStages = Nothing

        Try
            Dim SelectedSorts As New List(Of String)

            For Each Item As DataGridViewRow In dgvStageNameCompliance.SelectedRows
                SelectedSorts.Add(Item.Cells("StageNameCompliance").Value.ToString)
            Next Item

            For Each Item As DataGridViewRow In dgvStageNameEnforcement.SelectedRows
                SelectedSorts.Add(Item.Cells("StageNameEnforcement").Value.ToString)
            Next Item

            If Not IsNothing(SelectedSorts) Then GetStages = SelectedSorts.ToArray

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

#End Region

End Class
﻿Imports CommonLibrary

Public Class clsUnitedUtilitiesData
   
    'Public Function GetCaseCount(ByVal ClientRef As String, ByVal ChargeEndDate As String) As String
    '    ' Used to check if a case is already loaded
    '    GetCaseCount = GetSQLResults("DebtRecovery", "SELECT COUNT(*) FROM debtor AS d INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID INNER JOIN note AS n ON d._rowID = n.DebtorID WHERE d.client_ref = '" & ClientRef & "' AND d.status_open_closed = 'O' AND cs.clientID = 1662 AND n.type = 'Client note' AND n.text = 'Charge End Date:" & Date.ParseExact(ChargeEndDate, "yyyyMMdd", Nothing).ToString("dd/MMM/yyyy") & ";'")
    'End Function

    Public Function GetCaseLive(ByVal ClientRef As String) As String
        ' Used to check if a case is already loaded
        GetCaseLive = GetSQLResults("DebtRecovery", "SELECT COUNT(*) FROM debtor AS d INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID WHERE d.client_ref = '" & ClientRef & "' AND d.status_open_closed = 'O' AND cs.clientID = 1662")
    End Function

    Public Function GetCaseCount(ByVal ClientRef As String) As String
        ' Used to check if a case is already loaded
        GetCaseCount = GetSQLResults("DebtRecovery", "SELECT COUNT(*) FROM debtor AS d INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID WHERE d.client_ref = '" & ClientRef & "' AND cs.clientID = 1662")
    End Function

    Public Function GetCaseID(ByVal ClientRef As String) As String
        ' Used for main billing charge notes
        GetCaseID = GetSQLResults("DebtRecovery", "SELECT d._rowID FROM debtor AS d INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID WHERE d.client_ref = '" & ClientRef & "' AND cs.clientID = 1662")
    End Function

    Public Function GetLastFileSequence() As String
        GetLastFileSequence = GetSQLResults("FeesSQL", "SELECT ISNULL(MAX(FileSequence), 0) FROM dbo.UUCaseBatch")
    End Function

    Public Sub SetLastFileSequence(ByVal FileSequence As Integer, ByVal CasesLoaded As Integer, ByVal CasesLoadedValue As Decimal)
        ExecStoredProc("FeesSQL", " EXEC dbo.SetUUCaseBatch " & FileSequence.ToString & ", " & CasesLoaded.ToString & ", " & CasesLoadedValue.ToString)
    End Sub

    Public Sub SetMainBillingCharge(ByVal FileLine As String)
        ExecStoredProc("FeesSQL", " EXEC dbo.SetUUMainBillingCharge '" & FileLine.Replace("'", "''") & "'") ' Replace added TS 12/Mar/2013
    End Sub
End Class

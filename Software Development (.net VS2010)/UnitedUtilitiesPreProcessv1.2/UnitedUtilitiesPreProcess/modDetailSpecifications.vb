﻿Module modDetailSpecifications

    Public NewDebtHeader As String = "DCA_CODE|New_Charge_Type|Account_Number|Account_Balance|Fees_Balance|Costs_Balance|NFA_Indicator|Placement_Indicator|Previous_Recall|Debt_Status|Customer_Code|Customer_name|Customer_Address|Filler|Court_Case_Number|Judgement_Date|Leaving_Date|DWP_Indicator|Premise_Address|Charge_End_Date|Customer_status|Customer_Type|Tel_Number_1|Tel_Number_2|Tel_Number_3|Tel_Number_4|Associated_customer_1|Associated_customer_2|Associated_customer_3|Associated_customer_4|Commission_rate|FAO|Date_Of_Birth|Last_Payment_Date|Last_Payment_Amount|Behavioural_Score|Charge_start_Date_|Latest_Bill_Date|Latest_Bill_Value|Date_of_oldest_Debt|Account_Type|Current_Year_Debt|Previous_Year_Debt|Legacy_Debt" & vbCrLf
    Public QueryHistoryHeader As String() = {"DCA_CODE", "Record_Type", "Account_Number", "Dummy_Balance", "Filler", "Record_Title", "Query_sequence_number", "Type", "Query/Response"}
    Public FinancialTransactionHeader As String() = {"DCA_CODE", "New_Charge_Type", "Account_Number", "Transaction_Value", "Payment_Indicator", "Filler", "Placement_Indicator", "Filler", "Debt_Status", "Customer_Code", "Customer_name", "Customer_Address_Line_1", "Customer_Address_Line_2", "Customer_Address_Line_3", "Customer_Address_Town", "Customer_Address_County", "Customer_Postcode", "Customer_Tel_Number", "Court_Case_Number", "Judgement_Date", "Leaving_Date", "DWP_Indicator", "Charge_End_Date", "File_Sequence"}
    Public InformationStatusHeader As String() = {"DCA_CODE", "Record_Type", "Account_Number", "Customer_name", "Message_Status", "Account_Balance"}
    Public QueryResponseHeader As String() = {"DCA_CODE", "Record_Type", "Account_Number", "Customer_name", "Query_Response"}
    Public ChangesToCustomerHeader As String() = {"DCA_CODE", "New_Charge_Type", "Account_Number", "Filler", "NFA_Indicator", "Filler", "Customer_name", "Customer_Address_Line_1", "Customer_Address_Line_2", "Customer_Address_Line_3", "CustomerAddress_Town", "CustomerAddress_County", "Customer_Postcode", "Filler", "Filler", "Leaving_Date", "Filler", "Premise_Address_Line_1", "Premise_Address_Line_2", "Premise_Address_Line_3", "Premise_Address_Town", "Premise_County", "Premise_Postcode", "Filler", "Customer_status", "Bankruptcy_flag", "Deceased_flag", "Tel_Number_1", "Tel_Number_2", "Tel_Number_3", "Tel_Number_4", "Associated_customer_1", "Associated_customer_2", "Associated_customer_3", "Associated_customer_4", "FAO", "Customer_DOB", "Account_Type"}
    Public AdHocHeader As String() = {"Agency_Code", "New_Charge_Type", "ALTO_Account_Number", "Customer_name", "Customer_Address_Line_1", "Customer_Address_Line_2", "Customer_Address_Line_3", "Customer_Address_Town", "Customer_Address_County", "Customer_Postcode", "Filler", "Filler", "Premise_Address_Line_1", "Premise_Address_Line_2", "Premise_Address_Line_3", "Premise_Address_Town", "Premise_County", "Premise_Postcode", "Ad-Hoc_Value", "Ad-Hoc_Date", "Ad-Hoc_Text"}

    Public Function Header(ByVal Line As String) As String()
        Dim OutputLine() As String

        OutputLine = {Line.Substring(0, 6).Trim _
                    , Line.Substring(6, 1).Trim _
                    , Line.Substring(7, 3).Trim _
                    , Line.Substring(10, 1).Trim _
                    , Line.Substring(11, 6).Trim _
                    , Line.Substring(17, 1).Trim _
                    , Line.Substring(18, 6).Trim _
                    , Line.Substring(24, 4).Trim _
                    , Line.Substring(28, 10).Trim _
                    , Line.Substring(38, 1).Trim _
                    , Line.Substring(39, 8).Trim _
                    , Line.Substring(47, 1).Trim _
                    , Line.Substring(48, 5).Trim _
                    }

        Header = OutputLine
    End Function

    Public Function NewDebt(ByVal Line As String) As String()
        Dim OutputLine() As String

        OutputLine = {Line.Substring(0, 6).Trim _
                    , Line.Substring(6, 1).Trim _
                    , Line.Substring(7, 20).Trim _
                    , Line.Substring(27, 13).Trim _
                    , Line.Substring(40, 13).Trim _
                    , Line.Substring(53, 13).Trim _
                    , Line.Substring(66, 1).Trim _
                    , Line.Substring(67, 1).Trim _
                    , Line.Substring(68, 1).Trim _
                    , Line.Substring(69, 20).Trim _
                    , Line.Substring(89, 9).Trim _
                    , Line.Substring(98, 68).Trim _
                    , String.Join(",", {Line.Substring(166, 40).Trim & " " & Line.Substring(206, 40).Trim, Line.Substring(246, 40).Trim, Line.Substring(286, 25).Trim, Line.Substring(311, 25).Trim, Line.Substring(336, 10).Trim}) _
                    , Line.Substring(346, 12).Trim _
                    , Line.Substring(358, 15).Trim _
                    , Line.Substring(373, 8).Trim _
                    , Line.Substring(381, 8).Trim _
                    , Line.Substring(389, 1).Replace(" ", "N") _
                    , String.Join(",", {Line.Substring(390, 40).Trim & " " & Line.Substring(430, 40).Trim, Line.Substring(470, 40).Trim, Line.Substring(510, 25).Trim, Line.Substring(535, 25).Trim, Line.Substring(560, 8).Trim}) _
                    , Line.Substring(568, 8).Trim _
                    , Line.Substring(576, 1).Trim _
                    , Line.Substring(577, 2).Trim _
                    , Line.Substring(579, 12).Replace(" ", "") _
                    , Line.Substring(591, 12).Replace(" ", "") _
                    , Line.Substring(603, 12).Replace(" ", "") _
                    , Line.Substring(615, 12).Replace(" ", "") _
                    , Line.Substring(627, 50).Trim _
                    , Line.Substring(677, 50).Trim _
                    , Line.Substring(727, 50).Trim _
                    , Line.Substring(777, 50).Trim _
                    , Line.Substring(827, 9).Trim _
                    , Line.Substring(836, 50).Trim _
                    , Line.Substring(886, 8).Trim _
                    , Line.Substring(894, 8).Trim _
                    , Line.Substring(902, 13).Trim _
                    , Line.Substring(915, 1).Trim _
                    , Line.Substring(916, 8).Trim _
                    , Line.Substring(924, 8).Trim _
                    , Line.Substring(932, 13).Trim _
                    , Line.Substring(945, 8).Trim _
                    , Line.Substring(953, 2).Trim _
                    , Line.Substring(955, 13).Trim _
                    , Line.Substring(968, 13).Trim _
                    , Line.Substring(981).Trim _
                    }

        NewDebt = OutputLine
    End Function

    Public Function QueryHistory(ByVal Line As String) As String()
        Dim OutputLine() As String

        OutputLine = {Line.Substring(0, 6).Trim _
                    , Line.Substring(6, 1).Trim _
                    , Line.Substring(7, 20).Trim _
                    , Line.Substring(27, 13).Trim _
                    , Line.Substring(40, 29).Trim _
                    , Line.Substring(69, 26).Trim _
                    , Line.Substring(95, 3).Trim _
                    , Line.Substring(98, 1).Trim _
                    , Line.Substring(99).Trim
                    }

        QueryHistory = OutputLine
    End Function

    Public Function FinancialTransaction(ByVal Line As String) As String()
        Dim OutputLine() As String

        OutputLine = {Line.Substring(0, 6).Trim _
                    , Line.Substring(6, 1).Trim _
                    , Line.Substring(7, 20).Trim _
                    , Line.Substring(27, 13).Trim _
                    , Line.Substring(40, 1).Trim _
                    , Line.Substring(41, 26).Trim _
                    , Line.Substring(67, 1).Trim _
                    , Line.Substring(68, 1).Trim _
                    , Line.Substring(69, 20).Trim _
                    , Line.Substring(89, 9).Trim _
                    , Line.Substring(98, 68).Trim _
                    , Line.Substring(166, 40).Trim _
                    , Line.Substring(206, 40).Trim _
                    , Line.Substring(246, 40).Trim _
                    , Line.Substring(286, 25).Trim _
                    , Line.Substring(311, 25).Trim _
                    , Line.Substring(336, 10).Trim _
                    , Line.Substring(346, 12).Trim _
                    , Line.Substring(358, 15).Trim _
                    , Line.Substring(373, 8).Trim _
                    , Line.Substring(381, 8).Trim _
                    , Line.Substring(389, 1).Trim _
                    , Line.Substring(390).Trim
                    }

        FinancialTransaction = OutputLine
    End Function

    Public Function InformationStatus(ByVal Line As String) As String()
        Dim OutputLine() As String

        OutputLine = {Line.Substring(0, 6).Trim _
                    , Line.Substring(6, 1).Trim _
                    , Line.Substring(7, 20).Trim _
                    , Line.Substring(27, 38).Trim _
                    , frmMain.InfoStatus(Line.Substring(65, 1).Trim) _
                    , Line.Substring(66).Trim
                    }

        InformationStatus = OutputLine
    End Function

    Public Function QueryResponse(ByVal Line As String) As String()
        Dim OutputLine() As String

        OutputLine = {Line.Substring(0, 6).Trim _
                    , Line.Substring(6, 1).Trim _
                    , Line.Substring(7, 20).Trim _
                    , Line.Substring(27, 38).Trim _
                    , Line.Substring(65).Trim _
                    }

        QueryResponse = OutputLine
    End Function

    Public Function ChangesToCustomer(ByVal Line As String) As String()
        Dim OutputLine() As String

        OutputLine = {Line.Substring(0, 6).Trim _
                    , Line.Substring(6, 1).Trim _
                    , Line.Substring(7, 20).Trim _
                    , Line.Substring(27, 39).Trim _
                    , Line.Substring(66, 1).Trim _
                    , Line.Substring(67, 31).Trim _
                    , Line.Substring(98, 68).Trim _
                    , Line.Substring(166, 40).Trim _
                    , Line.Substring(206, 40).Trim _
                    , Line.Substring(246, 40).Trim _
                    , Line.Substring(286, 25).Trim _
                    , Line.Substring(311, 25).Trim _
                    , Line.Substring(336, 10).Trim _
                    , Line.Substring(346, 12).Trim _
                    , Line.Substring(358, 23).Trim _
                    , Line.Substring(381, 8).Trim _
                    , Line.Substring(389, 1).Trim _
                    , Line.Substring(390, 40).Trim _
                    , Line.Substring(430, 40).Trim _
                    , Line.Substring(470, 40).Trim _
                    , Line.Substring(510, 25).Trim _
                    , Line.Substring(535, 25).Trim _
                    , Line.Substring(560, 8).Trim _
                    , Line.Substring(568, 8).Trim _
                    , Line.Substring(576, 1).Trim _
                    , Line.Substring(577, 1).Trim _
                    , Line.Substring(578, 1).Trim _
                    , Line.Substring(579, 12).Trim _
                    , Line.Substring(591, 12).Trim _
                    , Line.Substring(603, 12).Trim _
                    , Line.Substring(615, 12).Trim _
                    , Line.Substring(627, 50).Trim _
                    , Line.Substring(677, 50).Trim _
                    , Line.Substring(727, 50).Trim _
                    , Line.Substring(777, 50).Trim _
                    , Line.Substring(827, 50).Trim _
                    , Line.Substring(877, 8).Trim _
                    , Line.Substring(885).Trim
                    }

        ChangesToCustomer = OutputLine
    End Function

    Public Function AdHoc(ByVal Line As String) As String()
        Dim OutputLine() As String

        OutputLine = {Line.Substring(0, 6).Trim _
                    , Line.Substring(6, 1).Trim _
                    , Line.Substring(7, 20).Trim _
                    , Line.Substring(27, 68).Trim _
                    , Line.Substring(95, 40).Trim _
                    , Line.Substring(135, 40).Trim _
                    , Line.Substring(175, 40).Trim _
                    , Line.Substring(215, 25).Trim _
                    , Line.Substring(240, 25).Trim _
                    , Line.Substring(265, 10).Trim _
                    , Line.Substring(275, 12).Trim _
                    , Line.Substring(287, 32).Trim _
                    , Line.Substring(319, 40).Trim _
                    , Line.Substring(359, 40).Trim _
                    , Line.Substring(399, 40).Trim _
                    , Line.Substring(439, 25).Trim _
                    , Line.Substring(464, 25).Trim _
                    , Line.Substring(489, 8).Trim _
                    , Line.Substring(497, 13).Trim _
                    , Line.Substring(510, 8).Trim _
                    , Line.Substring(518).Trim
                    }

        AdHoc = OutputLine
    End Function

    Public Function Trailer(ByVal Line As String) As String()
        Dim OutputLine() As String

        OutputLine = {Line.Substring(0, 6).Trim _
                    , Line.Substring(6, 1).Trim _
                    , Line.Substring(7, 5).Trim _
                    , Line.Substring(12, 13).Trim _
                     }

        Trailer = OutputLine
    End Function
End Module

﻿Imports System.IO

Public Class Form1
    Dim start_date As Date
    Dim end_date As Date
    Dim filename As String = ""
    Dim filepath As String = ""
    Private Sub runbtn_Click(sender As System.Object, e As System.EventArgs) Handles runbtn.Click
       
        runbtn.Enabled = False
        statuslbl.Text = "Starting report"
        start_date = start_dtp.Value
        end_date = end_dtp.Value
        Select Case report_cbox.SelectedIndex
            Case 0
                run_report_RA609()
            Case 1
                run_report_RA609C()
            Case 2
                run_report_RA1090_Gordon()
            Case 3
                run_report_RA1090_Steve()

        End Select


        Me.Close()
    End Sub
    Private Sub run_report_RA609()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()

        Dim RA609report = New RA609
        Dim myArrayList1 As ArrayList = New ArrayList()

        myArrayList1.Add(start_date)
        SetCurrentValuesForParameterField1(RA609report, myArrayList1)

        filename = "RA609 Vat Report " & Format(start_date, "dd-MM-yyyy") & " to " & Format(end_date, "dd-MM-yyyy") & ".xls"
        'add 1 day to end date as report checks for < end date
        end_date = DateAdd(DateInterval.Day, 1, end_date)
        myArrayList1.Add(end_date)
        SetCurrentValuesForParameterField2(RA609report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA609report)

        Dim savefiledialog1 As New SaveFileDialog
        With savefiledialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = filename
        End With
        If savefiledialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            statuslbl.Text = "Running report ... please wait "
            filename = savefiledialog1.FileName
            RA609report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, savefiledialog1.FileName)
            RA609report.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If


    End Sub
    Private Sub run_report_RA609C()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()

        Dim RA609Creport = New RA609C
        Dim myArrayList1 As ArrayList = New ArrayList()

        myArrayList1.Add(start_date)
        SetCurrentValuesForParameterField1(RA609Creport, myArrayList1)

        filename = "RA609C Vat Report " & Format(start_date, "dd-MM-yyyy") & " to " & Format(end_date, "dd-MM-yyyy") & ".xls"
        'add 1 day to end date as report checks for < end date
        end_date = DateAdd(DateInterval.Day, 1, end_date)
        myArrayList1.Add(end_date)
        SetCurrentValuesForParameterField2(RA609Creport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA609Creport)

        Dim savefiledialog1 As New SaveFileDialog
        With savefiledialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = filename
        End With
        If savefiledialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            statuslbl.Text = "Running report ... please wait "
            filename = savefiledialog1.FileName
            RA609Creport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, savefiledialog1.FileName)
            RA609Creport.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If


    End Sub
    Private Sub run_report_RA1090_Gordon()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()

        Dim RA1090Greport = New RA1090Gordon
        Dim myArrayList1 As ArrayList = New ArrayList()

        myArrayList1.Add(start_date)
        SetCurrentValuesForParameterField3(RA1090Greport, myArrayList1)

        filename = "RA1090 Gordons Report " & Format(start_date, "dd-MM-yyyy") & ".xls"

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA1090Greport)

        Dim savefiledialog1 As New SaveFileDialog
        With savefiledialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = filename
        End With
        If savefiledialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            statuslbl.Text = "Running report ... please wait "
            filename = savefiledialog1.FileName
            RA1090Greport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, savefiledialog1.FileName)
            RA1090Greport.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If


    End Sub
    Private Sub run_report_RA1090_Steve()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()

        Dim RA1090Sreport = New RA1090Steve
        Dim myArrayList1 As ArrayList = New ArrayList()

        myArrayList1.Add(start_date)
        SetCurrentValuesForParameterField3(RA1090Sreport, myArrayList1)

        filename = "RA1090 Steves Report " & Format(start_date, "dd-MM-yyyy") & ".xls"

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA1090Sreport)

        Dim savefiledialog1 As New SaveFileDialog
        With savefiledialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = filename
        End With
        If savefiledialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            statuslbl.Text = "Running report ... please wait "
            filename = savefiledialog1.FileName
            RA1090Sreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, savefiledialog1.FileName)
            RA1090Sreport.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If


    End Sub
    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        end_date = DateAdd(DateInterval.Day, -Weekday(Now) + 1, Now)
        end_date = CDate(Format(end_date, "MMM yyyy dd") & " 00:00:00")
        start_date = DateAdd(DateInterval.Day, -6, end_date)
        start_date = CDate(Format(start_date, "MMM yyyy dd") & " 00:00:00")
        start_dtp.Value = start_date
        end_dtp.Value = end_date
        report_cbox.SelectedIndex = 0
    End Sub
End Class

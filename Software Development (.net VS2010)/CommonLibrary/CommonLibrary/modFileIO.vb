﻿Imports System.IO
Imports Shell32
Imports System.Text

Public Module modFileIO

#Region "Public functions"

    Public Function InputFromExcel(ByVal FileName As String, Optional ByVal WorkSheetName As String = "") As String(,)
        Dim ExcelApp As Object, Workbook As Object, Worksheet As Object
        Dim ColumnIndex As Integer, RowIndex As Integer
        Dim Sheet As String(,) = Nothing

        ExcelApp = CreateObject("Excel.Application")
        Workbook = ExcelApp.Workbooks.Open(FileName)
        If WorkSheetName = "" Then
            Worksheet = Workbook.Sheets(1)
        Else
            Worksheet = Workbook.Sheets(WorkSheetName)
        End If

        Try

            ReDim Sheet(Worksheet.UsedRange.Rows.Count - 1, Worksheet.UsedRange.Columns.Count - 1)

            For ColumnIndex = 1 To Worksheet.UsedRange.Columns.Count
                For RowIndex = 1 To Worksheet.UsedRange.Rows.Count
                    Application.DoEvents()
                    Sheet(RowIndex - 1, ColumnIndex - 1) = Worksheet.Cells(RowIndex, ColumnIndex).Value
                Next RowIndex
            Next ColumnIndex

        Catch ex As Exception
            HandleException(ex)

        Finally
            CloseComObject(ExcelApp.ActiveWorkbook)
            ExcelApp.ActiveWorkbook.Close()
            ExcelApp.Quit()
            CloseComObject(Worksheet)
            Worksheet = Nothing
            CloseComObject(Workbook)
            Workbook = Nothing
            CloseComObject(ExcelApp)
            ExcelApp = Nothing

        End Try

        Return Sheet

    End Function

    Public Function InputFromSeparatedFile(ByVal FileName As String, ByVal Separator As String) As String(,)
        ' Returns an array of (row index, column index) to be consistent with Excel sheet references but works the opposite way due to ReDim restrictions

        Dim FileContents As String(,) = Nothing, OutputArray As String(,) = Nothing
        Dim LineArray As String()
        Dim ColCount As Integer, RowCount As Integer

        Try
            Dim sr As New System.IO.StreamReader(FileName, Encoding.Default)

            Do While sr.Peek <> -1
                LineArray = sr.ReadLine.Split(Separator)

                If Not IsNothing(FileContents) Then
                    ReDim Preserve FileContents(UBound(FileContents, 1), UBound(FileContents, 2) + 1)
                Else
                    ReDim FileContents(UBound(LineArray), 0)
                End If

                For ColCount = 0 To UBound(LineArray)
                    FileContents(ColCount, UBound(FileContents, 2)) = LineArray(ColCount)
                Next ColCount
            Loop

            'Transpose the array to be consistent with the Excel structure
            ReDim OutputArray(UBound(FileContents, 2), UBound(FileContents, 1))

            For ColCount = 0 To UBound(FileContents, 1)
                For RowCount = 0 To UBound(FileContents, 2)
                    OutputArray(RowCount, ColCount) = FileContents(ColCount, RowCount)
                Next RowCount
            Next ColCount

        Catch ex As Exception
            HandleException(ex)

        End Try

        Return OutputArray

    End Function


#End Region

#Region "Public subs"

    Public Sub OutputToSeparatedFile(ByVal Path As String, ByVal FileName As String, ByVal Separator As String, ByVal Header As String(), ByVal Contents As ArrayList)

        Dim OutputFile As String = Nothing

        Try
            If Not IsNothing(Header) Then OutputFile = String.Join(Separator, Header) & vbCrLf

            For Each Line As String() In Contents
                OutputFile &= String.Join(Separator, Line) & vbCrLf
            Next Line

            WriteFile(Path & "\" & FileName, OutputFile)

        Catch ex As Exception
            MsgBox(ex.ToString)

        End Try
    End Sub

    Public Sub OutputToExcel(ByVal Path As String, ByVal FileName As String, ByVal WorksheetName As String, ByVal Header As String(), ByVal Range As ArrayList)
        Dim ExcelApp As Object, Workbook As Object, Worksheet As Object
        Dim ColumnIndex As Integer, RowIndex As Integer

        ExcelApp = CreateObject("Excel.Application")
        Workbook = ExcelApp.Workbooks.Add()
        Worksheet = Workbook.Sheets(1)
        Try
            ExcelApp.DisplayAlerts = False

            For Each Sheet As Object In Workbook.Worksheets
                If Sheet.Index > 1 Then Sheet.Delete()
            Next Sheet

            Worksheet.Name = WorksheetName

            If Not Header Is Nothing Then ' Added TS 10/Sep/2013

                Worksheet.Range("A1").Resize(1, UBound(Header) + 1) = Header
                Worksheet.Range(Worksheet.Cells(1, 1), Worksheet.Cells(1, UBound(Header) + 1)).Font.Bold = True

                For ColumnIndex = 1 To Header.Count
                    For RowIndex = 1 To Range.Count
                        Application.DoEvents()
                        Worksheet.Cells(RowIndex + 1, ColumnIndex) = Range(RowIndex - 1)(ColumnIndex - 1)
                    Next RowIndex
                Next ColumnIndex
            Else ' Added TS 10/Sep/2013
                For RowIndex = 1 To Range.Count
                    For ColumnIndex = 1 To UBound(Range(RowIndex - 1)) + 1
                        Application.DoEvents()
                        Worksheet.Cells(RowIndex, ColumnIndex) = Range(RowIndex - 1)(ColumnIndex - 1)
                    Next ColumnIndex
                Next RowIndex
            End If

            Worksheet.Columns.Autofit()

            Directory.CreateDirectory(Path)
            Workbook.SaveAs(Path & FileName, 39) ' 39  Is Excel version 8. A bit old but more likely to be compatible

        Catch ex As Exception
            MsgBox(ex.ToString)

        Finally
            CloseComObject(ExcelApp.ActiveWorkbook)
            ExcelApp.ActiveWorkbook.Close()
            ExcelApp.Quit()
            CloseComObject(Worksheet)
            Worksheet = Nothing
            CloseComObject(Workbook)
            Workbook = Nothing
            CloseComObject(ExcelApp)
            ExcelApp = Nothing


        End Try
    End Sub

    Public Sub WriteFile(ByVal FileName As String, ByVal Content As String)

        If (Not System.IO.Directory.Exists(Path.GetDirectoryName(FileName))) Then System.IO.Directory.CreateDirectory(Path.GetDirectoryName(FileName))

        Using Writer As StreamWriter = New StreamWriter(FileName, False, System.Text.Encoding.ASCII)

            Writer.Write(Content)
        End Using
    End Sub

    Public Sub AppendToFile(ByVal FileName As String, ByVal Content As String)

        If (Not System.IO.Directory.Exists(Path.GetDirectoryName(FileName))) Then System.IO.Directory.CreateDirectory(Path.GetDirectoryName(FileName))

        Using Writer As StreamWriter = New StreamWriter(FileName, True, System.Text.Encoding.ASCII)

            Writer.Write(Content)
        End Using
    End Sub

    Public Sub CloseComObject(ByVal o As Object)
        Try
            While (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0)
            End While
        Catch
        Finally
            o = Nothing
        End Try

    End Sub

    Public Sub CreateZipFile(ByVal ZipFileName As String, ByVal SourceFolder As String)
        ' http://www.codeproject.com/Tips/257193/Easily-zip-unzip-files-using-Windows-Shell32

        '1) Lets create an empty Zip File .
        'The following data represents an empty zip file .
        Dim startBuffer() As Byte = {80, 75, 5, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
        ' Data for an empty zip file .
        FileIO.FileSystem.WriteAllBytes(ZipFileName, startBuffer, False)

        'We have successfully made the empty zip file .

        '2) Use the Shell32 to zip your files .
        ' Declare new shell class
        Dim sc As New Shell32.Shell()
        'Declare the folder which contains the files you want to zip .
        Dim input As Shell32.Folder = sc.NameSpace(SourceFolder)
        'Declare  your created empty zip file as folder  .
        Dim output As Shell32.Folder = sc.NameSpace(ZipFileName)
        'Copy the files into the empty zip file using the CopyHere command .
        output.CopyHere(input.Items, 4)

    End Sub
#End Region

End Module

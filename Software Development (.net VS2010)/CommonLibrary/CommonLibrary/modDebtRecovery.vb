﻿Public Module modDebtRecovery

    Public Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, ByVal Terminator As String, Optional ByVal NoteLength As Integer = 250) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 1 + Terminator.Length) ' + 1 is for :
        Dim PreviousLength As Integer, CharPos As Integer, EndPos As Integer
        Dim NotePadded As Boolean
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            If UBound(Note) > 0 Then
                For NoteCount = 0 To UBound(Note)
                    NotePadded = False
                    EndPos = Math.Min(NoteLength * (NoteCount + 1), FreeText.Length)
                    CharPos = EndPos
                    Do
                        If FreeText.Substring(CharPos - 1, 1) = " " Then
                            FreeText = FreeText.Insert(CharPos, Space(NoteLength * (NoteCount + 1) - CharPos))
                            NotePadded = True
                        End If
                        CharPos += -1
                    Loop Until CharPos = NoteLength * NoteCount Or NotePadded

                Next NoteCount
            End If

            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = NoteTag & ":" & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)).Trim ' this and the line below could be done in one line but becomes difficult to read
                Note(NoteCount) = (Note(NoteCount) & Terminator).PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function

    Public Function ConcatAddressFields(ByVal InputFields() As String, Optional ByVal Separator As String = ",") As String ' This needs to be removed, superceded by ConcatFields
        ' Cannot use join as an empty field would result in two adjacent separators
        Dim Address As String = ""

        If Not IsNothing(InputFields) Then
            For Each InputField As String In InputFields
                If Not IsNothing(InputField) AndAlso InputField <> "" Then
                    If Address <> "" Then
                        Address &= Separator & InputField
                    Else
                        Address = InputField
                    End If
                End If
            Next InputField

        End If

        Return Address

    End Function

    Public Function ConcatFields(ByVal InputFields() As String, ByVal Separator As String) As String
        ' Cannot use join as an empty field would result in two adjacent separators
        Dim Output As String = ""

        If Not IsNothing(InputFields) Then
            For Each InputField As String In InputFields
                If Not IsNothing(InputField) AndAlso InputField <> "" Then
                    If Output <> "" Then
                        Output &= Separator & InputField
                    Else
                        Output = InputField
                    End If
                End If
            Next InputField

        End If

        Return Output

    End Function
End Module

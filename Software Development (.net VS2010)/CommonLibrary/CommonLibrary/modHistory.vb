﻿Module modHistory

    ' 10/Apr/2013 TS  1.1.0    modHistory and modDependants added
    ' 27/Jun/2013 TS  1.2.0    Option param of sheetname added to modFileIO.InputFromExcel
    ' 20/Aug/2013 TS  1.3.0    modCommon.IsNINumber and modCommon.IsTelNumber added
    ' 10/Sep/2013 TS  1.4.0    modFileIO.OutputToExcel amended to cater for a header of nothing being passed
End Module

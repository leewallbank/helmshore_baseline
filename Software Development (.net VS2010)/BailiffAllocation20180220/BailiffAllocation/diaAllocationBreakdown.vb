﻿Imports CommonLibrary

Public Class diaAllocationByUserBreakdown
    Private LocalUsername As String
    Private AllocationByRegionData As New clsAllocationByRegionData

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        SetFormIcon(Me)

        cmsBreakdown.Items.Add("View Cases")

    End Sub

    Private Sub diaAllocationByUserBreakdown_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        Try

            dgvAllocationBreakdown.ClearSelection()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetAllocationByUserBreakdown(ByVal CompanyID As Integer, ByVal Username As String)
        Try
            AllocationByRegionData.GetAllocationsByUserBreakdown(CompanyID, Username)

            dgvAllocationBreakdown.DataSource = AllocationByRegionData.AllocationsByUserBreakdown

            FormatGridColumns(dgvAllocationBreakdown)

            LocalUsername = Username

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsBreakdown_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsBreakdown.ItemClicked
        Try

            If dgvAllocationBreakdown.SelectedCells.Count > 0 And dgvAllocationBreakdown.SelectedCells(0).Value > 0 Then

                Dim Day As Integer, Month As Integer, Year As Integer

                Day = dgvAllocationBreakdown.Columns(dgvAllocationBreakdown.SelectedCells(0).ColumnIndex).HeaderText.Substring(dgvAllocationBreakdown.Columns(dgvAllocationBreakdown.SelectedCells(0).ColumnIndex).HeaderText.Length - 2)

                If Day > DateTime.Now.Day Then

                    Month = DateTime.Now.Month - 1

                    If Month > DateTime.Now.Month Then
                        Year = DateTime.Now.Year - 1
                    Else
                        Year = DateTime.Now.Year
                    End If

                Else

                    Month = DateTime.Now.Month
                    Year = DateTime.Now.Year

                End If

                Dim Detail As New diaCaseDetail(Me)
                Detail.AddCasesByAllocationUserBreakdown(frmAllocationByRegionSummary.cboCompany.SelectedValue, _
                                                         LocalUsername, _
                                                         dgvAllocationBreakdown.Item(0, dgvAllocationBreakdown.SelectedCells(0).RowIndex).Value, _
                                                         Date.Parse(Day.ToString & "/" & Month.ToString & "/" & Year.ToString).ToString)

                If Detail.CaseCount > 0 Then Detail.ShowDialog()
                Detail.Dispose()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvAllocationBreakdown_MouseUp(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles dgvAllocationBreakdown.MouseUp
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If e.Button = MouseButtons.Right And hti.Type = DataGridViewHitTestType.Cell Then ' only show the context menu if a cell is being clicked on - not headers or scroll bars

                dgvAllocationBreakdown.Item(hti.ColumnIndex, hti.RowIndex).Selected = True
                cmsBreakdown.Show(sender, e.Location)

            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class
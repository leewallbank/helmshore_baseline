﻿Option Explicit On

Imports CommonLibrary
Imports MapPoint

Public Class frmMap

    Private mpGUI As MapPointGUI
    Private SendingDGV As DataGridView
    Private CaseData As New clsCaseSummaryData
    Private BailiffData As New clsBailiffData
    Private ImagePath As String = "R:\Allocation Diary\BailiffAllocation\Pushpin Images\"
    Private Images() As String = {"001BlackPushpin.bmp", "018SmallRedCircle.bmp", "019SmallYellowCircle.bmp", "020SmallWhiteCircle.bmp", "021SmallBlueCircle.bmp", "022SmallTurquoiseCircle.bmp", "023SmallGreenCircle.bmp", "024SmallPurpleCircle.bmp", "034SmallRedSquare.bmp", "035SmallYellowSquare.bmp", "036SmallWhiteSquare.bmp", "037SmallBlueSquare.bmp", "038SmallTurquoiseSquare.bmp", "039SmallGreenSquare.bmp", "040SmallPurpleSquare.bmp", "050SmallRedTriangle.bmp", "051SmallYellowTriangle.bmp", "052SmallWhiteTriangle.bmp", "053SmallBlueTriangle.bmp", "054SmallTurquoiseTriangle.bmp", "055SmallGreenTriangle.bmp", "056SmallPurpleTriangle.bmp", "010RedTack.bmp", "011YellowTack.bmp", "012DarkBlueTack.bmp", "013BlueTack.bmp", "014MaroonTack.bmp", "015GreenTack.bmp", "016PurpleTack.bmp", "002RedPushpin.bmp", "003YellowPushpin.bmp", "004DarkBluePusphin.bmp", "005BluePushpin.bmp", "006MaroonPushpin.bmp", "007GreenPushpin.bmp", "008PurplePushpin.bmp", "026RedCircle.bmp", "027YellowCircle.bmp", "028WhiteCircle.bmp", "029BlueCircle.bmp", "030TurquoiseCircle.bmp", "031GreenCircle.bmp", "032PurpleCircle.bmp", "042RedSquare.bmp", "043YellowSquare.bmp", "044WhiteSquare.bmp", "045BlueSquare.mp", "046TurquoiseSquare.bmp", "047GreenSquare.bmp", "048PurpleSquare.bmp", "058RedTriangle.bmp", "059YellowTriangle.bmp", "060WhiteTriangle.bmp", "061BlueTriangle.bmp", "062TurquoiseTriangle.bmp", "063GreenTriangle.bmp", "064PurpleTriangle.bmp", "017SmallBlackCircle.bmp", "033SmallBlackSquare.bmp", "049SmallBlackTriangle.bmp", "009BlackTack.bmp", "025BlackCircle.bmp", "041BlackSquare.bmp", "057BlackTriangle.bmp"}
    Private MapDrag As Boolean = False, BailiffsAdded As Boolean = False
    Private MouseDownButton As Integer, MouseDownX As Integer, MouseDownY As Integer
    Private MouseDownLocation As MapPoint.Location

    Public Const WM_CLOSE As Short = &H10S

#Region "Form events"

    Public Sub New(ByRef sender As DataGridView)
        Try
            SendingDGV = sender

            ' This call is required by the designer.
            InitializeComponent()

            SetFormIcon(Me)

            mapMain.NewMap(GeoMapRegion.geoMapEurope)
            mapMain.ActiveMap.MapStyle = MapPoint.GeoMapStyle.geoMapStyleRoad
            mapMain.Units = MapPoint.GeoUnits.geoMiles
            mapMain.Focus()

            lblSummary.Text = "0 cases"

            lstLegend.SmallImageList = imgPushpin
            lstLegend.LargeImageList = imgPushpin
            lstLegend.View = View.List

            cmsSelected.Items.Add("Copy")
            cmsSelected.Items.Add("Copy All")

            cmsMap.Items.Add("Show details")
            cmsMap.Items.Add("Add pushpin here")
            cmsMap.Items.Add("Add pushpin")
            cmsMap.Items.Add("Find")
            cmsMap.Items.Add("Clear route")
            cmsMap.Items.Add("Clear selection")
            cmsMap.Items.Add("Hide roads")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMap_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            mapMain.ActiveMap.Saved = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMap_Resize(sender As Object, e As System.EventArgs) Handles Me.Resize
        Try
            ' keep the selected items panel width constant when resizing the form
            spltMain.FixedPanel = FixedPanel.Panel1

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMap_ResizeBegin(sender As Object, e As System.EventArgs) Handles Me.ResizeBegin
        Try
            ' keep the selected items panel width constant when resizing the form
            spltMain.FixedPanel = FixedPanel.Panel1

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMap_ResizeEnd(sender As Object, e As System.EventArgs) Handles Me.ResizeEnd
        Try
            ' this is need so that the splitter is enabled
            spltMain.FixedPanel = FixedPanel.None

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Public Subs"

    Public Sub AddCases(ByVal ParamList As String, ByVal Period As String)
        Try
            ' This adds every case for each selected cell in frmCasesummary.dgvMain 
            CaseData.GetCases(ParamList, Period)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetCasesForMap()
        Try
            Dim Setnames As DataTable = CaseData.DetailDataView.ToTable(True, "Dataset")
            Dim InvalidCases As New ArrayList
            Dim DataSetExists As Boolean


            With mapMain.ActiveMap

                ' DataSets have to be deleted one at a time - no bulk delete
                Do Until .DataSets.Count = 0
                    .DataSets(1).Delete()
                Loop

                For Each DataSet As DataRow In Setnames.Rows
                    DataSetExists = False
                    For Each ds As MapPoint.DataSet In mapMain.ActiveMap.DataSets
                        If ds.Name = DataSet.Item("Dataset") Then DataSetExists = True
                    Next ds
                    If Not DataSetExists Then mapMain.ActiveMap.DataSets.AddPushpinSet(DataSet.Item("Dataset"))
                Next DataSet

                For Each MapCase As DataRow In CaseData.DetailDataView.Table.Rows
                    Dim NewPushpin As Pushpin = Nothing
                    Dim NewLocation As Location = Nothing

                    ' The quickest way to add pushpins is to use the eastings and northings
                    If Not IsDBNull(MapCase.Item("add_os_easting")) AndAlso MapCase.Item("add_os_easting").ToString.Length = 6 AndAlso Not IsDBNull(MapCase.Item("add_os_northing")) AndAlso MapCase.Item("add_os_northing").ToString.Length = 6 Then
                        NewPushpin = .AddPushpin(.LocationFromOSGridReference(MapCase.Item("add_os_easting") & MapCase.Item("add_os_northing")), MapCase.Item("DebtorID"))
                        NewPushpin.Note = MapCase.Item("add_postcode")
                    Else ' now try postcode
                        Dim Results As FindResults = .FindAddressResults(, , , , MapCase.Item("add_postcode"))
                        If Results.Count = 0 Then
                            If Not InvalidCases.Contains(MapCase.Item("DebtorID")) Then InvalidCases.Add(MapCase.Item("DebtorID")) ' Check so we don't load duplicates
                        Else
                            NewPushpin = .AddPushpin(Results(1), MapCase.Item("DebtorID"))
                            NewPushpin.Note = MapCase.Item("add_postcode")
                        End If
                    End If

                    If Not IsNothing(NewPushpin) Then NewPushpin.MoveTo(.DataSets(MapCase.Item("Dataset")))

                Next MapCase

                SetSymbols()

                If .DataSets.Count > 0 Then .DataSets.ZoomTo()

                If InvalidCases.Count > 0 Then MsgBox("The following case" & If(InvalidCases.Count = 1, "", "s") & " cannot be loaded:" & vbCrLf & String.Join(vbCrLf, InvalidCases.ToArray))
            End With

            PopulateLegend()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Private Subs"

    Private Sub PopulateLegend()
        Try
            Dim LoopCount As Integer = 0

            lstLegend.Items.Clear()

            RemoveHandler lstLegend.ItemChecked, AddressOf lstLegend_ItemChecked

            For Each DataSet As DataSet In mapMain.ActiveMap.DataSets
                If DataSet.RecordCount > 0 Then
                    If Not {"First call", "Van"}.Contains(DataSet.Name) Or chkShowBailiffs.Checked Then
                        Dim NewItem As New ListViewItem
                        NewItem = lstLegend.Items.Add(DataSet.Name, LoopCount.ToString)
                        NewItem.Checked = True
                    End If
                End If
                LoopCount += 1
            Next DataSet

            AddHandler lstLegend.ItemChecked, AddressOf lstLegend_ItemChecked

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetSymbols()
        Try
            Dim Symbol As MapPoint.Symbol
            Dim Symbols As MapPoint.Symbols = mapMain.ActiveMap.Symbols

            For ImageCount As Integer = 0 To mapMain.ActiveMap.DataSets.Count - 1
                Symbol = Symbols.Add(ImagePath & Images(ImageCount))
                mapMain.ActiveMap.DataSets(ImageCount + 1).Symbol = Symbol.ID
                imgPushpin.Images.Add(ImageCount.ToString, Image.FromFile(ImagePath & Images(ImageCount)))
            Next ImageCount

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ListSelection()
        Try
            Dim Records As MapPoint.Recordset
            Dim Shape As MapPoint.Shape

            If mapMain.ActiveMap.Shapes.Count > 0 Then

                lstSelected.Items.Clear()
                Shape = mapMain.ActiveMap.Shapes(1)

                For Each DataSet As MapPoint.DataSet In mapMain.ActiveMap.DataSets
                    If DataSet.PushpinsVisible Then
                        Records = DataSet.QueryShape(Shape)
                        Records.MoveFirst()
                        Do While Not Records.EOF
                            lstSelected.Items.Add(New ListViewItem({Records.Pushpin.Name, Records.Pushpin.Note}))
                            Records.MoveNext()
                        Loop
                    End If
                Next DataSet

                lstSelected.Sorting = SortOrder.Ascending

                lblSummary.Text = lstSelected.Items.Count.ToString & " case" & If(lstSelected.Items.Count <> 1, "s", "")
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub xClearHighlights()
        Try
            Dim Records As MapPoint.Recordset
            For Each DataSet As MapPoint.DataSet In mapMain.ActiveMap.DataSets
                Records = DataSet.QueryAllRecords
                Records.MoveFirst()
                Do While Not Records.EOF
                    Records.Pushpin.Highlight = False
                    Records.MoveNext()
                Loop
            Next DataSet

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ClearShapes()
        Try
            Do Until mapMain.ActiveMap.Shapes.Count = 0 ' Delete all shapes. There should only be one.
                mapMain.ActiveMap.Shapes(1).Delete()
            Loop

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ClearSelection()
        Try
            Dim Records As MapPoint.Recordset
            For Each DataSet As MapPoint.DataSet In mapMain.ActiveMap.DataSets
                Records = DataSet.QueryAllRecords
                Records.MoveFirst()
                Do While Not Records.EOF
                    Records.Pushpin.Highlight = False
                    Records.MoveNext()
                Loop
            Next DataSet

            lstSelected.Items.Clear()
            lblSummary.Text = ""
            ClearShapes()
            mapMain.ActiveMap.ActiveRoute.Clear()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub CopyListItems(ByRef List As Object)
        Try
            Dim CopyItems As String = ""
            Clipboard.Clear()
            For Each Item As ListViewItem In List
                CopyItems &= Item.SubItems(0).Text & vbCrLf
            Next Item
            Clipboard.SetText(CopyItems)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "Map events"

    Private Sub mapMain_BeforeClick(sender As Object, e As AxMapPoint._IMappointCtrlEvents_BeforeClickEvent) Handles mapMain.BeforeClick
        Try
            MouseDownLocation = mapMain.ActiveMap.XYToLocation(e.x, e.y)
            MouseDownX = e.x
            MouseDownY = e.y
            cmsMap.Items(0).Enabled = False ' Show details
            cmsMap.Items(4).Enabled = False ' Clear route

            For Each objResult As Object In mapMain.ActiveMap.ObjectsFromPoint(e.x, e.y)
                Dim Test As Pushpin = TryCast(objResult, Pushpin)
                If Not IsNothing(Test) Then
                    If e.button = MapPoint.GeoMouseButtonConstants.geoLeftButton Then e.cancel = True
                    If e.button = MapPoint.GeoMouseButtonConstants.geoRightButton Then cmsMap.Items(0).Enabled = True

                End If
            Next objResult

            If e.button = MapPoint.GeoMouseButtonConstants.geoRightButton Then
                If mapMain.ActiveMap.ActiveRoute.IsCalculated Then cmsMap.Items(4).Enabled = True
                cmsMap.Show(sender, New System.Drawing.Point(e.x, e.y + mapMain.ActiveMap.Top - mapMain.Top))
                e.cancel = True ' this stops the MapPoint context menu appearing
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub mapMain_MouseMoveEvent(sender As Object, e As AxMapPoint._IMappointCtrlEvents_MouseMoveEvent) Handles mapMain.MouseMoveEvent
        Try
            If e.button = MapPoint.GeoMouseButtonConstants.geoLeftButton Then
                MapDrag = True
            Else
                MapDrag = False
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub MapMain_MouseUpEvent(sender As Object, e As AxMapPoint._IMappointCtrlEvents_MouseUpEvent) Handles mapMain.MouseUpEvent
        Try
            If chkSelect.Checked Then
                ListSelection()
                chkSelect.Checked = False
            ElseIf MapDrag = False Then

                Dim ItemsSelected As Boolean = False

                If My.Computer.Keyboard.CtrlKeyDown = False Then ClearSelection()

                lblSummary.Text = ""
                For Each objResult As Object In mapMain.ActiveMap.ObjectsFromPoint(e.x, e.y)
                    Dim Test As Pushpin = TryCast(objResult, Pushpin)
                    If Not IsNothing(Test) Then
                        If Not {"First call", "Van"}.Contains(Test.Parent.Name) Then
                            objResult.highlight = True
                            lstSelected.Items.Add(New ListViewItem({objResult.Name, objResult.Note}))
                            ItemsSelected = True
                        Else
                            If objResult.balloonstate = MapPoint.GeoBalloonState.geoDisplayName Then
                                objResult.balloonstate = MapPoint.GeoBalloonState.geoDisplayNone
                            Else
                                objResult.balloonstate = MapPoint.GeoBalloonState.geoDisplayName
                            End If
                        End If
                    End If
                Next objResult

                If ItemsSelected Then
                    ClearShapes()
                    mapMain.ActiveMap.ActiveRoute.Clear()
                End If

                lstSelected.Sorting = SortOrder.Ascending
                lblSummary.Text = lstSelected.Items.Count.ToString & " case" & If(lstSelected.Items.Count <> 1, "s", "")
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub mapMain_SelectionChange(pNewSelection As Object, pOldSelection As Object) Handles mapMain.SelectionChange
        Try
            ' This stops anyone moving pushpins
            'http://www.mp2kmag.com/a97--pushpins.immoveable.mappoint.html

            Dim objShape As MapPoint.Shape
            Dim objLocation As MapPoint.Location

            If TypeOf (pNewSelection) Is MapPoint.Pushpin Then
                objLocation = mapMain.ActiveMap.GetLocation(80, 0) 'get a location over at the arctic ocean
                objShape = mapMain.ActiveMap.Shapes.AddShape(MapPoint.GeoAutoShapeType.geoShapeRectangle, objLocation, 1, 1)
                objShape.Select()
                objShape.Delete()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Other control events"

    Private Sub cmdPlotRoute_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPlotRoute.Click
        Try
            Dim Waypoints As MapPoint.Waypoints
            mapMain.ActiveMap.ActiveRoute.Clear()
            Waypoints = mapMain.ActiveMap.ActiveRoute.Waypoints

            For Each Item As ListViewItem In lstSelected.Items
                Waypoints.Add(mapMain.ActiveMap.FindPushpin(Item.SubItems(0).Text))
            Next Item

            If Waypoints.Count > 1 Then
                mapMain.ActiveMap.ActiveRoute.Waypoints.Optimize()
                mapMain.ActiveMap.ActiveRoute.Calculate()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub chkSelect_CheckChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelect.CheckedChanged
        Try
            If chkSelect.Checked Then
                ClearSelection()
                'lstSelected.Items.Clear()
                'lblSummary.Text = ""

                'ClearShapes()
                'mapMain.ActiveMap.ActiveRoute.Clear()

                mpGUI = New MapPointGUI(Me.mapMain)
                mpGUI.BeginScribbleTool()
                mapMain.ActiveMap.Shapes(1).Line.Weight = 1 ' Set line to 0.5 pt
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub lstLegend_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lstLegend.ItemChecked
        Try
            mapMain.ActiveMap.DataSets(e.Item.Text).PushpinsVisible = e.Item.Checked
            ListSelection()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub lstSelected_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lstSelected.MouseUp
        Try
            If e.Button = MouseButtons.Right Then cmsSelected.Show(sender, e.Location)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub lstSelected_Resize(sender As Object, e As System.EventArgs) Handles lstSelected.Resize
        Try
            lstSelected.Columns(0).Width = lstSelected.Width * (60 / 134) ' this preserves the ratio of the column widths
            lstSelected.Columns(1).Width = lstSelected.Width - lstSelected.Columns(0).Width

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub chkShowBailiffs_CheckChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowBailiffs.CheckedChanged
        Try
            Dim NewPushpin As Pushpin = Nothing
            Dim Symbol As MapPoint.Symbol
            Dim Symbols As MapPoint.Symbols = mapMain.ActiveMap.Symbols

            PreRefresh(Me)

            If chkShowBailiffs.Checked Then

                If Not BailiffsAdded Then

                    With mapMain.ActiveMap
                        BailiffData.GetBailiffs({""})
                        mapMain.ActiveMap.DataSets.AddPushpinSet("First call")
                        Symbol = Symbols.Add(ImagePath & "083RedCar.bmp")
                        mapMain.ActiveMap.DataSets("First call").Symbol = Symbol.ID
                        imgPushpin.Images.Add(imgPushpin.Images.Count.ToString, Image.FromFile(ImagePath & "083RedCar.bmp"))

                        mapMain.ActiveMap.DataSets.AddPushpinSet("Van")
                        Symbol = Symbols.Add(ImagePath & "087BlueTruck.bmp")
                        mapMain.ActiveMap.DataSets("Van").Symbol = Symbol.ID
                        imgPushpin.Images.Add(imgPushpin.Images.Count.ToString, Image.FromFile(ImagePath & "087BlueTruck.bmp"))

                        For Each dr As DataRow In BailiffData.BailiffDataView.Table.Rows

                            If {"First call", "Van"}.Contains(dr.Item("BailiffType")) Then
                                If Not IsDBNull(dr.Item("add_os_easting")) AndAlso dr.Item("add_os_easting").ToString.Length = 6 AndAlso Not IsDBNull(dr.Item("add_os_northing")) AndAlso dr.Item("add_os_northing").ToString.Length = 6 Then ' try eastings and northings
                                    NewPushpin = .AddPushpin(.LocationFromOSGridReference(dr.Item("add_os_easting") & dr.Item("add_os_northing")), dr.Item("name_fore") & " " & dr.Item("name_sur"))
                                Else ' try postcode
                                    Dim Results As FindResults = Nothing
                                    If Not IsDBNull(dr.Item("add_postcode")) Then Results = .FindAddressResults(, , , , dr.Item("add_postcode"))
                                    If Not IsNothing(Results) AndAlso Results.Count > 0 Then
                                        NewPushpin = .AddPushpin(Results(1), dr.Item("name_fore") & " " & dr.Item("name_sur"))
                                        Results = Nothing
                                    End If
                                End If
                            End If

                            If Not IsNothing(NewPushpin) Then NewPushpin.MoveTo(mapMain.ActiveMap.DataSets(dr.Item("BailiffType")))
                            NewPushpin = Nothing
                        Next dr
                    End With

                    BailiffsAdded = True
                Else
                    mapMain.ActiveMap.DataSets("First call").PushpinsVisible = True
                    mapMain.ActiveMap.DataSets("Van").PushpinsVisible = True
                End If
            Else
                mapMain.ActiveMap.DataSets("First call").PushpinsVisible = False
                mapMain.ActiveMap.DataSets("Van").PushpinsVisible = False

            End If
            PopulateLegend()

            PostRefresh(Me)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Context menu events"

    Private Sub cmsMap_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsMap.ItemClicked
        Try
            cmsMap.Hide()

            Select Case e.ClickedItem.Text
                Case "Show details"
                    For Each objResult As Object In mapMain.ActiveMap.ObjectsFromPoint(MouseDownX, MouseDownY)
                        Dim Test As Pushpin = TryCast(objResult, Pushpin)
                        If Not IsNothing(Test) Then Test.BalloonState = GeoBalloonState.geoDisplayBalloon
                    Next objResult

                Case "Add pushpin here"
                    Dim AddPushpin As New diaAddPushpin

                    If AddPushpin.ShowDialog = DialogResult.OK Then
                        Dim NewPushpin As Pushpin = mapMain.ActiveMap.AddPushpin(MouseDownLocation, AddPushpin.PushpinName)
                        NewPushpin.Note = AddPushpin.PushpinLocation
                        PopulateLegend()
                    End If

                    AddPushpin.Dispose() ' ShowDialog() requires an explicit Dispose()

                Case "Add pushpin"
                    Dim FindLocation As Location = mapMain.ActiveMap.ShowFindDialog()
                    If Not IsNothing(FindLocation) Then
                        Dim NewPushpin As Pushpin = mapMain.ActiveMap.AddPushpin(FindLocation, FindLocation.Name)
                        NewPushpin.Note = FindLocation.Name
                        mapMain.ActiveMap.DataSets.ZoomTo()
                    End If

                Case "Find"
                    Dim FindLocation As Location = mapMain.ActiveMap.ShowFindDialog()
                    If Not IsNothing(FindLocation) Then FindLocation.Select()

                Case "Clear route"
                    mapMain.ActiveMap.ActiveRoute.Clear()

                Case "Clear selection"
                    ClearSelection()

                Case "Hide roads"
                    mapMain.ActiveMap.MapStyle = MapPoint.GeoMapStyle.geoMapStyleData
                    cmsMap.Items(6).Text = "Show roads"

                Case "Show roads"
                    mapMain.ActiveMap.MapStyle = MapPoint.GeoMapStyle.geoMapStyleRoad
                    cmsMap.Items(6).Text = "Hide roads"

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsSelected_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsSelected.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "Copy"
                    CopyListItems(lstSelected.SelectedItems)

                Case "Copy All"
                    For LoopCount As Integer = 0 To lstSelected.Items.Count - 1
                        lstSelected.Items(LoopCount).Selected = True
                    Next LoopCount
                    CopyListItems(lstSelected.Items)

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region


End Class

Public Class Form1
    ' Dim run_date As Date TS 3/Jan/2013
    Dim all_reports As Boolean = False
    Dim filepath, filename As String
    Private Sub rd229btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd229btn.Click
        run_RD229()
    End Sub
    Private Sub run_RD229()
        disable_buttons()
        ProgressBar1.Value = 10
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD229report = New RD229P
        Dim myArrayList1 As ArrayList = New ArrayList()
        ' myArrayList1.Add(run_date) TS 3/Jan/2013
        myArrayList1.Add(run_dtp.Value) ' TS 3/Jan/2013
        SetCurrentValuesForParameterField1(RD229report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD229report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RD229 TDX Write off file.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = SaveFileDialog1.FileName
            ProgressBar1.Value = 20
            Application.DoEvents()
            RD229report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
            RD229report.close()
            If Not all_reports Then
                MsgBox("Completed")
                Me.Close()
            End If
        Else
            MsgBox("Report not saved")
            Me.Close()
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub RD230btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RD230btn.Click
        run_Rd230()
    End Sub
    Private Sub run_RD230()
        disable_buttons()
        ProgressBar1.Value = 30
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD230report = New RD230P
        Dim myArrayList1 As ArrayList = New ArrayList()
        ' myArrayList1.Add(run_date) TS 3/Jan/2013
        myArrayList1.Add(run_dtp.Value) ' TS 3/Jan/2013
        SetCurrentValuesForParameterField1(RD230report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD230report)
        If Not all_reports Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = "RD230 TDX BT Payment file.xls"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
            Else
                MsgBox("Report not saved")
                Me.Close()
                Exit Sub
            End If
        Else
            'replace RD229 name by RD230 name 
            Dim idx As Integer
            For idx = filename.Length To 1 Step -1
                If Mid(filename, idx, 1) = "\" Then
                    Exit For
                End If
            Next
            filepath = Microsoft.VisualBasic.Left(filename, idx)
            filename = filepath & "RD230 TDX BT Payment file.xls"
        End If

        ProgressBar1.Value = 40
        Application.DoEvents()
        RD230report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
        RD230report.close()
        If Not all_reports Then
            MsgBox("Completed")
            Me.Close()
        End If

    End Sub


    Private Sub RD253btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RD253btn.Click

        run_RD253()
    End Sub
    Private Sub run_RD253()
        Dim start_date As Date
        Try
            start_date = InputBox("Enter start date for period display of invoice(DD/MM/YYYY)", "Enter period start date")
        Catch ex As Exception
            MsgBox("Invalid period start date")
            Exit Sub
        End Try
        disable_buttons()
        ProgressBar1.Value = 50
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD253report = New RD253P
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(run_dtp.Value)
        SetCurrentValuesForParameterField1(RD253report, myArrayList1)
        myArrayList1.Add(start_date)
        SetCurrentValuesForParameterField2(RD253report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253report)
        If Not all_reports Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "PDF files |*.pdf"
                .DefaultExt = ".pdf"
                .OverwritePrompt = True
                .FileName = "RD253 TDX Invoice.pdf"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
            Else
                MsgBox("Report not saved")
                Me.Close()
                Exit Sub
            End If
        Else
            filename = filepath & "RD253 TDX Invoice.pdf"
        End If

        ProgressBar1.Value = 60
        Application.DoEvents()
        RD253report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD253report.close()

        If Not all_reports Then
            MsgBox("Completed")
            Me.Close()
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ' run_date = Now TS 03/Jan/2013
        ' run_date = DateAdd(DateInterval.Day, -Weekday(Now), run_date) TS 03/Jan/2012
        ' run_dtp.Value = run_date TS 03/Jan/2012

        run_dtp.Value = DateAdd(DateInterval.Day, -Weekday(Now), Now) ' TS 03/Jan/2012
    End Sub

    Private Sub allbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allbtn.Click
        disable_buttons()
        all_reports = True
        run_RD229()
        run_Rd230()
        run_RD253()

        MsgBox("Completed")
        Me.Close()

    End Sub
    Private Sub disable_buttons()
        allbtn.Enabled = False
        rd229btn.Enabled = False
        RD230btn.Enabled = False
        RD253btn.Enabled = False
        exitbtn.Enabled = False
        run_dtp.Enabled = False
    End Sub

    Private Sub RD253Dbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

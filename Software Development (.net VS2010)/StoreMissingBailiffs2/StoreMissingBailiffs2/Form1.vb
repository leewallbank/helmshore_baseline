﻿Imports CommonLibrary
'Imports System.Configuration
'Imports System.IO
Public Class MainForm
    Dim bail_dt As New DataTable
    Dim null_date As Date = CDate("Jan 1, 1900")
    Dim fourtyDaysAgo As Date = DateAdd(DateInterval.Day, -40, Now)
    'Dim eightyDaysAgo As Date = DateAdd(DateInterval.Day, -80, Now)


    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim clientArray As Object()
        ConnectDb2("DebtRecoveryLocal")
        ConnectDb2Fees("FeesSQL")
        Dim start_date As Date = CDate("2014 Apr 6")
        Dim upd_txt As String
        'upd_txt = "delete from BailiffAllocationTable " & _
        '          " WHERE bail_seq_no =789605 "
        'update_sql(upd_txt)
        'check allocations on visit table are in BAT
        Dim visit_dt As New DataTable
        LoadDataTable2("DebtRecoveryLocal", "SELECT V.bailiffID, V.debtorID, V.date_allocated, D.clientSchemeID, V.stageName, D.bailiffID, D.bail_allocated, bail_current " & _
                                            "FROM visit V, debtor D " & _
                                            "WHERE D._rowID = V.DebtorID" & _
                                             " and date_allocated >= '" & Format(fourtyDaysAgo, "yyyy-MM-dd") & "'" & _
                                             " order by D._rowID", visit_dt, False)
        For Each visitRow In visit_dt.Rows
            Dim bail_allocated As Date
            Try
                bail_allocated = visitRow(2)
            Catch ex As Exception
                Continue For
            End Try
            Dim debtorID As Integer = visitRow(1)
            Dim bailID As Integer
            Try
                bailID = visitRow(0)
            Catch ex As Exception
                Continue For
            End Try
            'check proper bailiff

            Dim bailCnt As Integer = GetSQLResults2("DebtRecoveryLocal", "SELECT count(*) " & _
                                                   "FROM bailiff " & _
                                                   "WHERE _rowid= " & bailID & _
                                               " and agent_type = 'B'" & _
                                               " and add_mobile_name <> 'none'")
            If bailCnt > 0 Then
                'check not test case
                Dim CSID As Integer = visitRow(3)
                clientArray = GetSQLResultsArray2("DebtRecoveryLocal", "SELECT clientID " & _
                                                   "FROM clientScheme " & _
                                                   "WHERE _rowid= " & CSID)
                Dim clientID As Integer = clientArray(0)
                If clientID = 1 Or clientID = 2 Or clientID = 24 Then
                    Continue For
                End If
                'see if on BAT
                Dim bail4_dt As New DataTable
                LoadDataTable2("FeesSQL", "SELECT bail_seq_no, bail_ID, bail_de_allocation_found_date " & _
                                                "FROM BailiffAllocationTable " & _
                                                "WHERE bail_debtorID = " & debtorID & _
                                                " AND bail_allocation_date = '" & Format(bail_allocated, "yyyy-MM-dd") & "'", bail4_dt, False)
                If bail4_dt.Rows.Count > 0 Then
                    'found on BAT
                    'if de-alloc date < allocation date correct de-alloc date
                    Dim bailDeAllocationDate As Date = bail4_dt.Rows(0).Item(2)
                    Dim bailSeqNo As Integer = bail4_dt.Rows(0).Item(0)
                    If Format(bailDeAllocationDate, "yyyy-MM-dd") < Format(bail_allocated, "yyyy-MM-dd") Then
                        'get correct de-allocation date
                        If visitRow(5) = bailID Then
                            If visitRow(7) = "Y" Then
                                Continue For
                            End If
                            'look in notes for de-allocation date
                            Dim note_dt As New DataTable
                            LoadDataTable2("DebtRecoveryLocal", "SELECT _createdDate " & _
                                                                "FROM note " & _
                                                                "WHERE type = 'Removed agent'" & _
                                                                " and DebtorID = " & debtorID & _
                                                                " and _createdDate > '" & Format(bail_allocated, "yyyy-MM-dd") & "'" & _
                                                                " order by _rowID desc", note_dt, False)
                            Dim dateFound As Boolean = False
                            For Each noteRow In note_dt.Rows
                                upd_txt = "update BailiffAllocationTable set bail_de_allocation_found_date = '" & Format(noteRow(0), "yyyy-MM-dd") & "'" & _
                             " where bail_seq_no = " & bailSeqNo
                                update_sql(upd_txt)
                                dateFound = True
                                Exit For
                            Next
                            If dateFound = False Then
                                'set date to todays date
                                upd_txt = "update BailiffAllocationTable set bail_de_allocation_found_date = '" & Format(Now, "yyyy-MM-dd") & "'" & _
                             " where bail_seq_no = " & bailSeqNo
                                update_sql(upd_txt)
                            End If
                        Else
                            'use bail allocated date 
                            upd_txt = "update BailiffAllocationTable set bail_de_allocation_found_date = '" & Format(visitRow(6), "yyyy-MM-dd") & "'" & _
                            " where bail_seq_no = " & bailSeqNo
                            update_sql(upd_txt)
                        End If
                    End If
                Else
                    'Not found on BAT
                    'insert BAT
                    Dim stagename As String = ""
                    Try
                        stagename = visitRow(4)
                    Catch ex As Exception

                    End Try
                    If visitRow(7) = "Y" Then
                        'still allocated
                        upd_txt = "insert into BailiffAllocationTable (bail_ID, bail_debtorID, " & _
                    "bail_allocation_date, bail_de_alloc_for_remit, bail_stage_name, bail_de_allocation_found_date)" & _
                    "values (" & bailID & "," & debtorID & ",'" & Format(bail_allocated, "yyyy-MM-dd") & "'," & 0 & ",'" &
                    stagename & "','" & Format(null_date, "yyyy-MM-dd") & "')"
                        update_sql(upd_txt)
                        Continue For
                    End If
                    'get de-allocated date
                    'first try BAT to get later entry
                    Dim bail5_dt As New DataTable
                    LoadDataTable2("FeesSQL", "SELECT bail_allocation_date, bail_ID, bail_seq_no " & _
                                                    "FROM BailiffAllocationTable " & _
                                                    "WHERE bail_debtorID = " & debtorID & _
                                                    " AND bail_allocation_date > '" & Format(bail_allocated, "yyyy-MMM-dd") & "'" & _
                                                    " order by bail_allocation_date", bail5_dt, False)
                    Dim laterEntryFound As Boolean = False
                    For Each bail5Row In bail5_dt.Rows
                        Dim bail5ID As Integer = bail5Row(1)
                        laterEntryFound = True
                        Dim bailDeAllocatedDate As Date = bail5Row(0)
                        'get next allocation date on visit record
                        Dim visit2_dt As New DataTable
                        LoadDataTable2("DebtRecoveryLocal", "SELECT bailiffID, date_allocated, stageName " & _
                                                            "FROM visit " & _
                                                            " WHERE DebtorID = " & debtorID & _
                                                            " and date_allocated > '" & Format(bail_allocated, "yyyy-MM-dd") & "'" & _
                                                            " order by date_allocated", visit2_dt, False)
                        Dim bail2ID As Integer
                        Dim bail2DeAllocatedDate As Date = Nothing
                        For Each visit2Row In visit2_dt.Rows
                            bail2ID = visit2Row(0)
                            bail2DeAllocatedDate = visit2Row(1)
                            Exit For
                        Next
                        If bail2DeAllocatedDate < null_date Then
                            bail2DeAllocatedDate = Now
                        End If
                        If Format(bail2DeAllocatedDate, "yyyy-MM-dd") < Format(bailDeAllocatedDate, "yyyy-MM-dd") Then
                            upd_txt = "insert into BailiffAllocationTable (bail_ID, bail_debtorID, " & _
                     "bail_allocation_date, bail_de_alloc_for_remit, bail_stage_name, bail_de_allocation_found_date)" & _
                     "values (" & bailID & "," & debtorID & ",'" & Format(bail_allocated, "yyyy-MM-dd") & "'," & 0 & ",'" &
                     stagename & "','" & Format(bail2DeAllocatedDate, "yyyy-MM-dd") & "')"
                            If bail2DeAllocatedDate > Now Then
                                'MsgBox("future bailiff allocated for case " & debtorID)
                            End If
                            If bail2DeAllocatedDate < bail_allocated Then
                                'MsgBox("SSS")
                            Else
                                update_sql(upd_txt)
                                Continue For
                            End If

                        Else
                            upd_txt = "insert into BailiffAllocationTable (bail_ID, bail_debtorID, " & _
                     "bail_allocation_date, bail_de_alloc_for_remit, bail_stage_name, bail_de_allocation_found_date)" & _
                     "values (" & bailID & "," & debtorID & ",'" & Format(bail_allocated, "yyyy-MM-dd") & "'," & 0 & ",'" &
                     stagename & "','" & Format(bailDeAllocatedDate, "yyyy-MM-dd") & "')"
                            If bailDeAllocatedDate > Now Then
                                'MsgBox("future bailiff allocated for case " & debtorID)
                            End If
                            update_sql(upd_txt)
                            Continue For
                        End If
                    Next
                    If laterEntryFound = False Then
                        'see if still in debtor record
                        Try
                            Dim test As Integer = visitRow(5)
                        Catch ex As Exception
                            Continue For
                        End Try
                        If visitRow(5) = bailID Then
                            If visitRow(7) = "Y" Then
                                Continue For
                            End If
                            'look in notes for de-allocation date
                            Dim note_dt As New DataTable
                            LoadDataTable2("DebtRecoveryLocal", "SELECT _createdDate " & _
                                                                "FROM note " & _
                                                                "WHERE type = 'Removed agent'" & _
                                                                " and DebtorID = " & debtorID & _
                                                                " and _createdDate > '" & Format(bail_allocated, "yyyy-MM-dd") & "'" & _
                                                                " order by _rowID desc", note_dt, False)
                            Dim noteFound As Boolean = False
                            For Each noteRow In note_dt.Rows
                                upd_txt = "insert into BailiffAllocationTable (bail_ID, bail_debtorID, " & _
                     "bail_allocation_date, bail_de_alloc_for_remit, bail_stage_name, bail_de_allocation_found_date)" & _
                     "values (" & bailID & "," & debtorID & ",'" & Format(bail_allocated, "yyyy-MM-dd") & "'," & 0 & ",'" &
                     stagename & "','" & Format(noteRow(0), "yyyy-MM-dd") & "')"
                                update_sql(upd_txt)
                                noteFound = True
                                Exit For
                            Next
                            If Not noteFound Then
                                upd_txt = "insert into BailiffAllocationTable (bail_ID, bail_debtorID, " & _
                    "bail_allocation_date, bail_de_alloc_for_remit, bail_stage_name, bail_de_allocation_found_date)" & _
                    "values (" & bailID & "," & debtorID & ",'" & Format(bail_allocated, "yyyy-MM-dd") & "'," & 0 & ",'" &
                    stagename & "','" & Format(Now, "yyyy-MM-dd") & "')"
                                update_sql(upd_txt)
                            End If
                        Else
                            'use bail allocated date
                            upd_txt = "insert into BailiffAllocationTable (bail_ID, bail_debtorID, " & _
                     "bail_allocation_date, bail_de_alloc_for_remit, bail_stage_name, bail_de_allocation_found_date)" & _
                     "values (" & bailID & "," & debtorID & ",'" & Format(bail_allocated, "yyyy-MM-dd") & "'," & 0 & ",'" &
                     stagename & "','" & Format(visitRow(6), "yyyy-MM-dd") & "')"
                            update_sql(upd_txt)
                        End If

                    End If
                End If
            End If
        Next

        

        'MsgBox("done - remove later")
        Me.Close()
    End Sub
End Class

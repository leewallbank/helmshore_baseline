<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.exitbtn = New System.Windows.Forms.Button
        Me.retnbtn = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.date_picker = New System.Windows.Forms.DateTimePicker
        Me.sch_combobox = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(205, 297)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 4
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'retnbtn
        '
        Me.retnbtn.Location = New System.Drawing.Point(66, 236)
        Me.retnbtn.Name = "retnbtn"
        Me.retnbtn.Size = New System.Drawing.Size(125, 23)
        Me.retnbtn.TabIndex = 3
        Me.retnbtn.Text = "Generate reports"
        Me.retnbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(10, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = " "
        '
        'date_picker
        '
        Me.date_picker.Location = New System.Drawing.Point(66, 179)
        Me.date_picker.Name = "date_picker"
        Me.date_picker.Size = New System.Drawing.Size(125, 20)
        Me.date_picker.TabIndex = 2
        Me.date_picker.Value = New Date(2010, 11, 12, 0, 0, 0, 0)
        '
        'sch_combobox
        '
        Me.sch_combobox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.sch_combobox.FormattingEnabled = True
        Me.sch_combobox.Items.AddRange(New Object() {"C TAX", "C TAX-TCE", "NNDR", "NNDR-TCE-FT"})
        Me.sch_combobox.Location = New System.Drawing.Point(66, 97)
        Me.sch_combobox.Name = "sch_combobox"
        Me.sch_combobox.Size = New System.Drawing.Size(121, 21)
        Me.sch_combobox.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(86, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Choose scheme"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(101, 152)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Choose date"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(15, 297)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 8
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 355)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.sch_combobox)
        Me.Controls.Add(Me.date_picker)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.retnbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Wakefield Returns"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents retnbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents date_picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents sch_combobox As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar

End Class

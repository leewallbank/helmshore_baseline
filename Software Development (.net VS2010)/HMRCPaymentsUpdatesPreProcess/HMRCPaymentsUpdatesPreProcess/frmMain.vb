﻿Imports CommonLibrary
Imports System.IO
Imports System.Collections

Public Class frmMain

    Private PaymentHeader As String() = {"DCA ID", "SOURCE_TYPE", "DCA INTERNAL ID", "DEBT REFERENCE NUMBER", "TRANCHE ID", "PAYMENT DATE", "EFFECTIVE DATE OF PAYMENT", "AMOUNT PAID", "COMPOSITE PAYMENT", "DIRECT PAYMENT", "PAYMENT METHOD", "PAYER NAME", "PAYER_REFERENCE", "DCA PAYMENT REFERENCE", "COMMISSION AMOUNT", "HMRC_COMMISSION_REFERENCE", "RECONCILIATION_SIGNAL"}
    Private UpdateHeader As String() = {"DCA ID", "SOURCE_TYPE", "DCA INTERNAL ID", "DEBT REFERENCE NUMBER", "TRANCHE ID", "UPDATE CONTACT TYPE", "UPDATE CONTACT TYPE", "UPDATED TITLE", "UPDATED FORENAMES", "UPDATED SURNAME", "UPDATED ADDRESS TYPE", "UPDATED ADDRESS TYPE", "UPDATED ADDRESS LINE1", "UPDATED ADDRESS LINE2", "UPDATED ADDRESS LINE3", "UPDATED ADDRESS LINE4", "UPDATED POSTCODE", "UPDATED TEL TYPE", "UPDATED TEL TYPE", "UPDATED CONTACT TEL NO1", "RECALL REQUEST DATE", "RECALL REASON", "RECALL REASON", "QUERY DATE", "QUERY TYPE", "QUERY TYPE", "QUERY OUTCOME", "QUERY OUTCOME", "QUERY DETAILS", "ADJUSTMENT DATE", "ADJUSTMENT TYPE", "ADJUSTMENT TYPE", "ADJUSTMENT AMOUNT", "ADJUSTMENT BALANCE"}

    Private OutputPath As String = "", OutputFilename As String = ""

    Private AddressType As New Dictionary(Of Integer, String)
    Private AdjustmentType As New Dictionary(Of Integer, String)
    Private ContactType As New Dictionary(Of Integer, String)
    Private QueryOutcome As New Dictionary(Of Integer, String)
    Private QueryType As New Dictionary(Of Integer, String)
    Private RecallReason As New Dictionary(Of Integer, String)
    Private TelephoneType As New Dictionary(Of String, String)

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        AddressType.Add(1, "Taxpayer")
        AddressType.Add(2, "2nd Claimant")
        AddressType.Add(3, "Business Address")
        AddressType.Add(4, "Recovery")

        AdjustmentType.Add(1, "System Adjustment")
        AdjustmentType.Add(2, "User Adjustment")
        AdjustmentType.Add(3, "Commissionable Payment")
        AdjustmentType.Add(4, "Non-Commissionable Payment")
        AdjustmentType.Add(5, "Other")

        ContactType.Add(1, "Taxpayer")
        ContactType.Add(2, "2nd Claimant")
        ContactType.Add(3, "Business Address")
        ContactType.Add(4, "Recovery")

        QueryOutcome.Add(301, "Customer claims paid or part paid")
        QueryOutcome.Add(302, "Query concerning Customer disputing outstanding balance")
        QueryOutcome.Add(303, "Alleges already has TTP agreement with HMRC")
        QueryOutcome.Add(304, "The DCA have been unable to trace an alternative address for the customer, no phone contact available, check with HMRC for an updated address")
        QueryOutcome.Add(305, "Customer has failed security check and claims information provided is incorrect or more than one person with the same name living the same address")
        QueryOutcome.Add(306, "Query Payment rejected by IT system due to date or amount match, possible duplication")
        QueryOutcome.Add(307, "Query adjustment rejected by IT system due to date or amount match, possible duplication")
        QueryOutcome.Add(308, "Query withdrawal rejected by IT system not covered in withdrawal reasons")
        QueryOutcome.Add(309, "Case accepted by DCA")
        QueryOutcome.Add(310, "Case rejected by DCA - mandatory field issue")
        QueryOutcome.Add(311, "Case rejected by DCA - other issue")
        QueryOutcome.Add(312, "Customer details update")

        QueryType.Add(101, "Customer claims paid or part paid")
        QueryType.Add(102, "Query concerning Customer disputing outstanding balance")
        QueryType.Add(103, "Alleges already has TTP agreement with HMRC")
        QueryType.Add(104, "The DCA have been unable to trace an alternative address for the customer, no phone contact available, check with HMRC for an updated address")
        QueryType.Add(105, "Customer has failed security check and claims information provided is incorrect or more than one person with the same name living the same address")
        QueryType.Add(106, "Query Payment rejected by IT system due to date or amount match, possible duplication")
        QueryType.Add(107, "Query adjustment rejected by IT system due to date or amount match, possible duplication")
        QueryType.Add(108, "Query withdrawal rejected by IT system not covered in withdrawal reasons")
        QueryType.Add(109, "Case accepted by DCA")
        QueryType.Add(110, "Case rejected by DCA - mandatory field issue")
        QueryType.Add(111, "Case rejected by DCA - other issue")
        QueryType.Add(112, "Customer details update")

        RecallReason.Add(201, "Insolvent")
        RecallReason.Add(202, "Deceased")
        RecallReason.Add(203, "Household Breakdown")
        RecallReason.Add(204, "In Prison")
        RecallReason.Add(205, "Abroad")
        RecallReason.Add(206, "Paid In Full")
        RecallReason.Add(207, "Can’t Pay ")
        RecallReason.Add(208, "Untraceable / RLS/ DLO")
        RecallReason.Add(209, "No Contact DCA efforts exhausted ")
        RecallReason.Add(210, "Company Ceased / Struck Off ")
        RecallReason.Add(211, "Requested By HMRC")
        RecallReason.Add(212, "Debt settled (via direct payment) ")
        RecallReason.Add(213, "Alleges Domestic Violence")
        RecallReason.Add(214, "Debt < £32 & no payment made in last 60 days")
        RecallReason.Add(215, "Long Term Illness")
        RecallReason.Add(216, "Debt Management Companies")
        RecallReason.Add(217, "Unable to negotiate a TTP under HMRC guidelines")
        RecallReason.Add(218, "Unable to renegotiate a TTP under HMRC guidelines")
        RecallReason.Add(219, "Renegotiated TTP now broken down")
        RecallReason.Add(220, "Debt amended to NIL")
        RecallReason.Add(221, "HMRC debt paid in full prior to DCA action")
        RecallReason.Add(222, "TTP in place prior to DCA action")
        RecallReason.Add(223, "Dissolved company ")
        RecallReason.Add(224, "Complaint ")
        RecallReason.Add(225, "Exemption ")
        RecallReason.Add(226, "HMRC Enforcement action")
        RecallReason.Add(227, "Assessment withdrawn")
        RecallReason.Add(228, "Other")
        RecallReason.Add(230, "Financial Hardship")
        RecallReason.Add(231, "Vulnerable Customer")
        RecallReason.Add(232, "DOB incorrect")
        RecallReason.Add(233, "Customer requests a TTP lasting 36 months")
        RecallReason.Add(234, "HMRC debt paid in full after referral to DCA")

        TelephoneType.Add("C", "Contact Telephone")
        TelephoneType.Add("D", "Alternative Telephone1")
        TelephoneType.Add("E", "Alternative Telephone2")
        TelephoneType.Add("R", "Recovery Telephone")
        TelephoneType.Add("B", "Business Telephone")

    End Sub

    Private Sub frmMain_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        Try

            ' Hide these until a file has been processed
            btnViewOutputFile.Enabled = False
            lblOutputtingFile.Visible = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try

            Dim FileDialog As New OpenFileDialog
            Dim FileContents As String(,), FileHeader() As String
            Dim PaymentOutputFile As New ArrayList(), UpdateOutputFile As New ArrayList(), RecallOutputFile As New ArrayList(), QueryOutputFile As New ArrayList(), AdjustmentOutputFile As New ArrayList()
            Dim WorksheetNames() As String = {"Payments", "Updates", "Recalls", "Queries", "Adjustments"}

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            ' Establish that the file as an update or payment file and set the relevant column headers and worksheet name
            If Not (FileDialog.FileName.Contains("UPDATE") Or FileDialog.FileName.Contains("PAYMENT")) Then
                MsgBox("Please select a valid file", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, Me.Text)
                Return
            End If

            ' Read in the file
            FileContents = InputFromSeparatedFile(FileDialog.FileName, "|")

            ' Validate the number of columns in the file 
            If FileDialog.FileName.Contains("PAYMENT") Then
                If UBound(FileContents, 2) <> UBound(PaymentHeader) Then
                    MsgBox("Incorrect file layout.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, Me.Text)
                    Return
                End If
            Else
                If UBound(FileContents, 2) <> 26 Then ' Cannot use UBound(UpdateHeader) as this has extract fields for code mappings
                    MsgBox("Incorrect file layout.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, Me.Text)
                    Return
                End If
            End If

            Dim InputLine As String()

            ' Loop through the array and convert it to an array list. There is no one liner for multidimensional arrays
            ProgressBar.Maximum = UBound(FileContents, 1)

            For RowCount As Integer = 0 To UBound(FileContents, 1)
                ProgressBar.Value = RowCount
                Application.DoEvents()

                ReDim InputLine(UBound(FileContents, 2))
                For ColCount As Integer = 0 To UBound(FileContents, 2)
                    InputLine(ColCount) = FileContents(RowCount, ColCount)
                Next ColCount

                If FileDialog.FileName.Contains("PAYMENT") Then

                    PaymentOutputFile.Add(InputLine)

                Else ' must be an UPDATE file
                    Dim Outputline(33) As String

                    Outputline(0) = InputLine(0)
                    Outputline(1) = InputLine(1)
                    Outputline(2) = InputLine(2)
                    Outputline(3) = InputLine(3)

                    If InputLine(5) <> "" Then
                        ' create Update file
                        Outputline(5) = InputLine(5)
                        Outputline(6) = If(Not ContactType.ContainsKey(InputLine(5)), "Unknown", ContactType(InputLine(5)))
                        Outputline(7) = InputLine(6)
                        Outputline(8) = InputLine(7)
                        Outputline(9) = InputLine(8)
                        Outputline(10) = InputLine(9)
                        Outputline(11) = If(Not AddressType.ContainsKey(InputLine(9)), "Unknown", AddressType(InputLine(9)))
                        Outputline(12) = InputLine(10)
                        Outputline(13) = InputLine(11)
                        Outputline(14) = InputLine(12)
                        Outputline(15) = InputLine(13)
                        Outputline(16) = InputLine(14)
                        Outputline(17) = InputLine(15)
                        Outputline(18) = If(Not TelephoneType.ContainsKey(InputLine(15)), "Unknown", TelephoneType(InputLine(15)))
                        Outputline(19) = InputLine(16)

                        UpdateOutputFile.Add(Outputline)

                    ElseIf InputLine(17) <> "" Then
                        ' create Recall file
                        Outputline(20) = InputLine(17)
                        Outputline(21) = InputLine(18)
                        Outputline(22) = If(Not RecallReason.ContainsKey(InputLine(18)), "Unknown", RecallReason(InputLine(18)))

                        ' add Query lines
                        Outputline(23) = InputLine(19)
                        Outputline(24) = InputLine(20)
                        Outputline(25) = If(Not QueryType.ContainsKey(InputLine(20)), "Unknown", QueryType(InputLine(20)))
                        Outputline(26) = InputLine(21)
                        Outputline(27) = If(Not QueryOutcome.ContainsKey(InputLine(21)), "Unknown", QueryOutcome(InputLine(21)))
                        Outputline(28) = InputLine(22)

                        RecallOutputFile.Add(Outputline)

                    ElseIf InputLine(23) <> "" Then
                        ' create Adjustment file
                        ' add Query lines
                        Outputline(23) = InputLine(19)
                        Outputline(24) = InputLine(20)
                        Outputline(25) = If(Not QueryType.ContainsKey(InputLine(20)), "Unknown", QueryType(InputLine(20)))
                        Outputline(26) = InputLine(21)
                        Outputline(27) = If(Not QueryOutcome.ContainsKey(InputLine(21)), "Unknown", QueryOutcome(InputLine(21)))
                        Outputline(28) = InputLine(22)

                        ' now adjustment specific
                        Outputline(29) = InputLine(23)
                        Outputline(30) = InputLine(24)
                        Outputline(31) = If(Not AdjustmentType.ContainsKey(InputLine(24)), "Unknown", AdjustmentType(InputLine(24)))
                        Outputline(32) = InputLine(25)
                        Outputline(33) = InputLine(26)

                        AdjustmentOutputFile.Add(Outputline)

                    Else
                        ' create Query file
                        Outputline(23) = InputLine(19)
                        Outputline(24) = InputLine(20)
                        Outputline(25) = If(Not QueryType.ContainsKey(InputLine(20)), "Unknown", QueryType(InputLine(20)))
                        Outputline(26) = InputLine(21)
                        Outputline(27) = If(Not QueryOutcome.ContainsKey(InputLine(21)), "Unknown", QueryOutcome(InputLine(21)))
                        Outputline(28) = InputLine(22)

                        QueryOutputFile.Add(Outputline)

                    End If

                End If

            Next RowCount

            ' Now output the file
            OutputPath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            ProgressBar.Value = 0
            lblOutputtingFile.Visible = True

            Dim OutputFileCount As Integer = 0
            For Each OutputFile As ArrayList In {PaymentOutputFile, UpdateOutputFile, RecallOutputFile, QueryOutputFile, AdjustmentOutputFile}

                If OutputFile.Count > 0 Then

                    OutputFilename = System.IO.Path.GetFileNameWithoutExtension(FileDialog.FileName) & "_" & WorksheetNames(OutputFileCount) & ".xls"

                    If OutputFileCount = 0 Then
                        FileHeader = PaymentHeader
                    Else
                        FileHeader = UpdateHeader
                    End If

                    OutputToExcel(OutputPath, OutputFilename, WorksheetNames(OutputFileCount), FileHeader, OutputFile)

                End If

                OutputFileCount += 1

            Next OutputFile

            lblOutputtingFile.Visible = False

            MsgBox("Preprocessing complete.", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, Me.Text)

            btnViewOutputFile.Enabled = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(OutputPath & OutputFilename) Then System.Diagnostics.Process.Start(OutputPath & OutputFilename)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class

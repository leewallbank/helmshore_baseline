﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("HMRC Payments && Updates Pre process")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Rossendales")> 
<Assembly: AssemblyProduct("HMRCPaymentsUpdatesPreProcess")> 
<Assembly: AssemblyCopyright("Copyright © Rossendales 2014")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("2ebd47ba-4610-4b9f-a3fd-a1f5fd172daf")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.1.0.0")> 
<Assembly: AssemblyFileVersion("1.1.0.0")> 

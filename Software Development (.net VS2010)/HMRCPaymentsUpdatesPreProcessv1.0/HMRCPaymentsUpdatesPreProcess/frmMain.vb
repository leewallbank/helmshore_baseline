﻿Imports CommonLibrary
Imports System.IO
Imports System.Collections

Public Class frmMain

    Private PaymentHeader As String() = {"DCA ID", "SOURCE_TYPE", "DCA INTERNAL ID", "DEBT REFERENCE NUMBER", "TRANCHE ID", "PAYMENT DATE", "EFFECTIVE DATE OF PAYMENT", "AMOUNT PAID", "COMPOSITE PAYMENT", "DIRECT PAYMENT", "PAYMENT METHOD", "PAYER NAME", "PAYER_REFERENCE", "DCA PAYMENT REFERENCE", "COMMISSION AMOUNT", "HMRC_COMMISSION_REFERENCE", "RECONCILIATION_SIGNAL"}
    Private UpdateHeader As String() = {"DCA ID", "SOURCE_TYPE", "DCA INTERNAL ID", "DEBT REFERENCE NUMBER", "TRANCHE ID", "UPDATE CONTACT TYPE", "UPDATED TITLE", "UPDATED FORENAMES", "UPDATED SURNAME", "UPDATED ADDRESS TYPE", "UPDATED ADDRESS LINE1", "UPDATED ADDRESS LINE2", "UPDATED ADDRESS LINE3", "UPDATED ADDRESS LINE4", "UPDATED POSTCODE", "UPDATED TEL TYPE", "UPDATED CONTACT TEL NO1", "RECALL REQUEST DATE", "RECALL REASON", "QUERY DATE", "QUERY TYPE", "QUERY OUTCOME", "QUERY DETAILS", "ADJUSTMENT DATE", "ADJUSTMENT TYPE", "ADJUSTMENT AMOUNT", "ADJUSTMENT BALANCE"}

    Private OutputPath As String = "", OutputFilename As String = ""

    Private Sub frmMain_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        Try

            ' Hide these until a file has been processed
            btnViewOutputFile.Enabled = False
            lblOutputtingFile.Visible = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try

            Dim FileDialog As New OpenFileDialog
            Dim FileContents As String(,), FileHeader() As String, WorksheetName As String
            Dim OutputFile As New ArrayList()

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            ' Establish that the file as an update or payment file and set the relevant column headers and worksheet name
            If Not (FileDialog.FileName.Contains("UPDATE") Or FileDialog.FileName.Contains("PAYMENT")) Then
                MsgBox("Please select a valid file", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, Me.Text)
                Return
            ElseIf FileDialog.FileName.Contains("UPDATE") Then
                FileHeader = UpdateHeader
                WorksheetName = "Updates"
            Else
                FileHeader = PaymentHeader
                WorksheetName = "Payments"
            End If

            ' Read in the file
            FileContents = InputFromSeparatedFile(FileDialog.FileName, "|")

            Dim InputLineArray As String()


            ' Validate the number of columns in the file 
            If UBound(FileContents, 2) <> UBound(FileHeader) Then 
                MsgBox("Incorrect file layout.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, Me.Text)
                Return
            End If

            ' Loop through the array and convert it to an array list. There is no one liner for multidimensional arrays
            ProgressBar.Maximum = UBound(FileContents, 1)

            For RowCount As Integer = 0 To UBound(FileContents, 1)
                ProgressBar.Value = RowCount
                ReDim InputLineArray(UBound(FileContents, 2))
                For ColCount As Integer = 0 To UBound(FileContents, 2)
                    InputLineArray(ColCount) = FileContents(RowCount, ColCount)
                Next ColCount

                OutputFile.Add(InputLineArray)

            Next RowCount

            ' Now output the file
            OutputPath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            OutputFilename = System.IO.Path.GetFileNameWithoutExtension(FileDialog.FileName) & ".xls"

            ProgressBar.Value = 0
            lblOutputtingFile.Visible = True

            OutputToExcel(OutputPath, OutputFilename, WorksheetName, FileHeader, OutputFile)

            lblOutputtingFile.Visible = False

            MsgBox("Preprocessing complete.", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, Me.Text)

            btnViewOutputFile.Enabled = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(OutputPath & OutputFilename) Then System.Diagnostics.Process.Start(OutputPath & OutputFilename)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

﻿Imports CommonLibrary
Imports System.IO
Imports System.Text.RegularExpressions

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String
    Private OutputFiles As New ArrayList()

    Private Const Separator As String = "|"
    Private Const ConnID As String = "4673"

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        btnViewInputFile.Enabled = False
        btnViewLogFile.Enabled = False
        btnViewOutputFile.Enabled = False

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click

        '  Try
        Dim FileDialog As New OpenFileDialog
        Dim ExcelFile As New ArrayList, VCFile As New ArrayList
        Dim AuditLog As String, FileContents(,) As String, ErrorLog As String = "", NewDebtNotes As String

        Dim NewDebtCaseCount As Decimal

        btnViewInputFile.Enabled = False
        btnViewLogFile.Enabled = False
        btnViewOutputFile.Enabled = False

        FileDialog.Filter = "New business files|*.xls;*.xlsx|All files (*.*)|*.*"
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)

        Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

        If File.Exists(InputFilePath & FileName & "_Audit.txt") Then File.Delete(InputFilePath & FileName & "_Audit.txt")
        If File.Exists(InputFilePath & FileName & "_Error.txt") Then File.Delete(InputFilePath & FileName & "_Error.txt")
        If File.Exists(InputFilePath & ConnID & "_" & FileName & ".txt") Then File.Delete(InputFilePath & ConnID & "_" & FileName & ".txt")

        lblReadingFile.Visible = True
        Application.DoEvents()
        FileContents = InputFromExcel(FileDialog.FileName)

        lblReadingFile.Visible = False

        ProgressBar.Maximum = UBound(FileContents)

        For RowNum = 1 To UBound(FileContents)

            Application.DoEvents() ' without this line, the button disappears until processing is complete
            ProgressBar.Value = RowNum
            NewDebtNotes = ""

            If String.IsNullOrEmpty(FileContents(RowNum, 0)) Then Continue For

            If UBound(FileContents, 2) <> 18 Then ErrorLog &= "Unexpected row length at line " & RowNum.ToString & vbCrLf

            Dim NewDebtRow(UBound(FileContents, 2)) As String
            For ColNum As Integer = 0 To UBound(NewDebtRow)
                NewDebtRow(ColNum) = FileContents(RowNum, ColNum)
                If NewDebtRow(ColNum) = Nothing Then NewDebtRow(ColNum) = ""
            Next ColNum

            NewDebtNotes = ToNote(NewDebtRow(16), "EQ Result", ";")(0)
            NewDebtNotes &= ToNote(NewDebtRow(17), "C Tax Check Result", ";")(0)

            AppendToFile(InputFilePath & ConnID & "_" & FileName & ".txt", Join(NewDebtRow, Separator) & Separator & NewDebtNotes & vbCrLf)

            If Not OutputFiles.Contains(InputFilePath & ConnID & "_" & FileName & ".txt") Then OutputFiles.Add(InputFilePath & ConnID & "_" & FileName & ".txt")

            ' Update running totals
            NewDebtCaseCount += 1

        Next RowNum

        ' Update audit log
        AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf & vbCrLf & vbCrLf

        AuditLog &= "Number of new cases: " & NewDebtCaseCount.ToString & vbCrLf & vbCrLf & vbCrLf

        WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

        If ErrorLog <> "" Then
            WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
            MsgBox("Errors found.", vbCritical, Me.Text)
        End If

        ProgressBar.Value = ProgressBar.Maximum

        MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

        btnViewInputFile.Enabled = True
        btnViewOutputFile.Enabled = True
        btnViewLogFile.Enabled = True

        ProgressBar.Value = 0

        'Catch ex As Exception
        '    HandleException(ex)
        'End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try
            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try
            For Each Filename As String In OutputFiles
                If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
            Next Filename

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try
            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class




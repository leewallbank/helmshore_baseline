﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration

Public Class frmMain
    Private InputFilePath As String, FileName As String, FileExt As String
    Private outFileName As String
    Dim recordType As String = ""
    Dim FileDialog As New OpenFileDialog
    Dim LineNumber As Integer = 0
    Dim OutputFile As String = ""
    Dim AuditLog As String, ErrorLog As String = ""
    Public ascii As New System.Text.ASCIIEncoding()


    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click

        Try
            
            process_file()

            lblReadingFile.Visible = True
            AuditLog = "File post-processed: " & InputFilePath & FileName & ".txt" & vbCrLf
            AuditLog &= "Date post-processed: " & DateTime.Now.ToString & vbCrLf
            AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

            AuditLog &= "Number of rows: " & LineNumber & vbCrLf


            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)


            MsgBox("Post-processing complete.", vbInformation + vbOKOnly, Me.Text)
            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
    
   
    Private Sub process_file()
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        LineNumber = 0
        InputFilePath = Path.GetDirectoryName((FileDialog.FileName))
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension((FileDialog.FileName))
        outFileName = InputFilePath & FileName & "_DOB_PostProcess.csv"

        Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
        lblReadingFile.Visible = False
        ProgressBar.Maximum = UBound(FileContents)

        For Each InputLine As String In FileContents
            ProgressBar.Value = LineNumber
            Application.DoEvents()
            LineNumber += 1

            If LineNumber > UBound(FileContents) + 1 Then
                Continue For
            End If
            Dim columns() As String = InputLine.Split(",")
            Dim colIDX As Integer
            For colIDX = 0 To UBound(columns)
                If colIDX = 6 Then
                    Dim DOB As String = ""
                    Try
                        DOB = Trim(columns(6))
                    Catch ex As Exception

                    End Try
                    If DOB.Length = 7 Then
                        DOB = "0" & DOB
                    End If
                    OutputFile &= DOB & ","
                Else
                    If colIDX = UBound(columns) Then
                        OutputFile &= columns(colIDX) & vbNewLine
                    Else
                        OutputFile &= columns(colIDX) & ","
                    End If

                End If
            Next
            If LineNumber = 1 Then
                My.Computer.FileSystem.WriteAllText(outFileName, OutputFile, False, ascii)
            Else
                My.Computer.FileSystem.WriteAllText(outFileName, OutputFile, True, ascii)
            End If

            OutputFile = ""

        Next InputLine

    End Sub
    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try
           
            If File.Exists(InputFilePath & FileName & ".csv") Then System.Diagnostics.Process.Start("notepad.exe", """" & InputFilePath & FileName & ".csv" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(outFileName) Then System.Diagnostics.Process.Start("notepad.exe", """" & outFileName & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed_wrp.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFil_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

End Class

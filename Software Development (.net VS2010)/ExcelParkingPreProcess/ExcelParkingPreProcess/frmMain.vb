﻿Imports CommonLibrary
Imports System.IO
Imports System.Text.RegularExpressions

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String
    Private OutputFiles As New ArrayList()

    Private Const Separator As String = "|"
    Private ConnID(,) As Integer = {{3757, 3758}, {3803, 3816}}
    Private Const OutstandingBalanceCol As Integer = 2 ' Added TS 03/Dec/2014. Request ref 

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        btnProcessFile.Enabled = False
        btnViewInputFile.Enabled = False
        btnViewLogFile.Enabled = False
        btnViewOutputFile.Enabled = False

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click

        Try
            Dim FileDialog As New OpenFileDialog
            Dim ExcelFile As New ArrayList, VCFile As New ArrayList
            Dim AuditLog As String, FileContents(,) As String, ErrorLog As String = ""

            Dim NewDebtSumm(1, 1) As Decimal ' 0 Case count, 1 Total balance
            Dim SchemeID As Integer

            btnViewInputFile.Enabled = False
            btnViewLogFile.Enabled = False
            btnViewOutputFile.Enabled = False

            FileDialog.Filter = "New business files|*.xls;*.xlsx|All files (*.*)|*.*"
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then File.Delete(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then File.Delete(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & ConnID(cboScheme.SelectedIndex, 0) & "_" & FileName & ".txt") Then File.Delete(InputFilePath & ConnID(cboScheme.SelectedIndex, 0) & "_" & FileName & ".txt")
            If File.Exists(InputFilePath & ConnID(cboScheme.SelectedIndex, 1) & "_" & FileName & ".txt") Then File.Delete(InputFilePath & ConnID(cboScheme.SelectedIndex, 1) & "_" & FileName & ".txt")

            lblReadingFile.Visible = True
            Application.DoEvents()
            FileContents = InputFromExcel(FileDialog.FileName)

            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents)

            Dim HeaderRow(UBound(FileContents, 2)) As String
            For ColNum As Integer = 0 To UBound(HeaderRow)
                HeaderRow(ColNum) = FileContents(0, ColNum)
            Next ColNum

            For RowNum = 1 To UBound(FileContents)

                Application.DoEvents() ' without this line, the button disappears until processing is complete
                ProgressBar.Value = RowNum

                If String.IsNullOrEmpty(FileContents(RowNum, 0)) Then Continue For

                If String.IsNullOrEmpty(FileContents(RowNum, OutstandingBalanceCol)) OrElse Not IsNumeric(FileContents(RowNum, OutstandingBalanceCol)) Then ' OutstandingBalanceCol changed from 1. TS 03/Dec/2014
                    ErrorLog &= "Invalid balance outstanding at line number " & (RowNum + 1).ToString & vbCrLf
                    Continue For
                End If

                Dim NewDebtRow(UBound(FileContents, 2)) As String
                For ColNum As Integer = 0 To UBound(NewDebtRow)
                    NewDebtRow(ColNum) = FileContents(RowNum, ColNum)
                Next ColNum

                If FileContents(RowNum, 0).Substring(0, 2) <> "VC" Then
                    SchemeID = 0
                Else
                    SchemeID = 1
                End If

                AppendToFile(InputFilePath & ConnID(cboScheme.SelectedIndex, SchemeID) & "_" & FileName & ".txt", Join(NewDebtRow, Separator) & vbCrLf)

                If Not OutputFiles.Contains(InputFilePath & ConnID(cboScheme.SelectedIndex, SchemeID) & "_" & FileName & ".txt") Then OutputFiles.Add(InputFilePath & ConnID(cboScheme.SelectedIndex, SchemeID) & "_" & FileName & ".txt")

                ' Update running totals
                NewDebtSumm(SchemeID, 0) += 1
                NewDebtSumm(SchemeID, 1) += CDec(FileContents(RowNum, OutstandingBalanceCol)) ' OutstandingBalanceCol changed from 1. TS 03/Dec/2014

            Next RowNum

            ' Update audit log
            AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf & vbCrLf & vbCrLf

            AuditLog &= "Conn ID " & ConnID(cboScheme.SelectedIndex, 0) & ": Excel Parking " & cboScheme.Text & vbCrLf & vbCrLf
            AuditLog &= "Number of new cases: " & NewDebtSumm(0, 0).ToString & vbCrLf
            AuditLog &= "Total balance of new cases: " & NewDebtSumm(0, 1).ToString & vbCrLf & vbCrLf & vbCrLf

            AuditLog &= "Conn ID " & ConnID(cboScheme.SelectedIndex, 1) & ": Vehicle Control Services " & cboScheme.Text & vbCrLf & vbCrLf
            AuditLog &= "Number of new cases: " & NewDebtSumm(1, 0).ToString & vbCrLf
            AuditLog &= "Total balance of new cases: " & NewDebtSumm(1, 1).ToString & vbCrLf

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            ProgressBar.Value = ProgressBar.Maximum

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try
            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try
            For Each Filename As String In OutputFiles
                If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
            Next Filename

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try
            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function InputFromExcel(ByVal FileName As String) As String(,)

        InputFromExcel = Nothing

        Try

            Dim TempTable As New DataTable
            ' HDR=NO to not skip the first line
            Dim xlsConn As System.Data.OleDb.OleDbConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileName & ";Extended Properties=""Excel 12.0 Xml;IMEX=1;HDR=NO;TypeGuessRows = 0;ImportMixedTypes = Text;""")
            xlsConn.Open()
            Dim xlsComm As System.Data.OleDb.OleDbDataAdapter = New System.Data.OleDb.OleDbDataAdapter("select * from [" & xlsConn.GetSchema("Tables").Rows(0)("TABLE_NAME") & "]", xlsConn)

            xlsComm.Fill(TempTable)
            xlsConn.Close()

            Dim Output As String(,)
            ReDim Output(TempTable.Rows.Count - 1, TempTable.Columns.Count - 1)

            For RowIdx As Integer = 0 To TempTable.Rows.Count - 1
                For ColIdx As Integer = 0 To TempTable.Columns.Count - 1
                    Application.DoEvents()
                    Output(RowIdx, ColIdx) = TempTable.Rows(RowIdx).Item(ColIdx).ToString
                Next ColIdx
            Next RowIdx

            Return Output

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Sub cboScheme_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles cboScheme.SelectedIndexChanged
        Try
            btnProcessFile.Enabled = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class



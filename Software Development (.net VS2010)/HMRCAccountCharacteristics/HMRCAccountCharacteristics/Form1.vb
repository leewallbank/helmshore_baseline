﻿Public Class Form1

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ConnectDb2("DebtRecovery")
        Dim lowdate As Date = CDate("Jan 1, 1900")
        Dim highdate As Date = CDate("Jan 1, 2100")
        Dim startDebtorID As Integer = 0
        'get all cases for clientID = 1275 and
        Dim outFile As String = "AccountID,TranchID,PlacementDate,DateofBirth,EmpNI,Title,ForeName,Surname,Address Line1,Address Line2, Address Line3, Address Line4," & _
            "Postcode,Contact Tel1, Contact Tel 2, contact Tel3,Recovery Title,Recovery Forename,Recovery Surname,Recovery Addr line 1,Recovery Addr Line2," & _
            "Recovery Addr Line3,Recovery Addr Line 4,Recovery Postcode,Recovery Tel 1,Business Tel,Placement Balance,Debt Type,Work items,Most Recent Tax year," & _
            "Oldest Tax Year,Most Recent Due date,Oldest Due date"
        outFile &= vbNewLine
        My.Computer.FileSystem.WriteAllText("H:temp\HMRCAccountCharacteristics.txt", outFile, False)
        outFile = ""
        Dim debt_dt As New DataTable
        Dim startDate As Date = CDate("Jan 1, 2012")
        Dim endDate As Date = CDate("Feb 1, 2015")
        LoadDataTable2("DebtRecovery", "select D._rowid, D.client_batch,D._createdDate,D.dateofBirth, D.empNI, D.name_title,D.name_fore, D.name_sur, D.address," & _
                       "D.add_postcode,add_phone, add_fax,empPhone,debt_original, S.name " & _
                      " from Debtor D, clientScheme CS, scheme S" & _
                      " where D.clientSchemeID = CS._rowID" & _
                      " and CS.clientID = 1275" & _
                      " and S._rowID = CS.schemeID" & _
                      " and D._rowID >= " & startDebtorID & _
                      " and D._createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                      " and D._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                      " order by D._rowID", debt_dt, False)
        Dim debtCount As Integer = 0

        For Each debtRow In debt_dt.Rows
            outFile &= debtRow(0) & "," & debtRow(1) & "," & Format(debtRow(2), "dd/MM/yyyy") & "," & debtRow(3) & "," & debtRow(4) & "," & _
                debtRow(5) & "," & debtRow(6) & "," & debtRow(7) & ","
            'look for first address change
            Dim debtaddr1 As String = ""
            Dim debtaddr2 As String = ""
            Dim debtaddr3 As String = ""
            Dim debtaddr4 As String = ""
            Dim debtpostcode As String = ""
            Dim origTel1 As String = ""
            Dim origTel2 As String = ""
            Dim origTel3 As String = ""
            Dim note_dt As New DataTable
            Dim addressChange As Boolean = False
            LoadDataTable2("DebtRecovery", "select text from note" & _
                           " where DebtorID = " & debtRow(0) & _
                           " and type = 'Address'" & _
                           " and (text like 'changed from:%' or" & _
                           " text like 'cleaned from:%')" & _
                           " order by _rowID", note_dt, False)
            For Each noteRow In note_dt.Rows
                Dim addr1 As String = ""
                Dim addr2 As String = ""
                Dim addr3 As String = ""
                Dim addr4 As String = ""
                Dim addr5 As String = ""
                Dim postcode As String = ""
                Dim noteText As String = Microsoft.VisualBasic.Right(noteRow(0), noteRow(0).length - 13)
                'split into address lines by comma
                Dim addrArray() As String
                addrArray = noteText.Split(",")
                addr1 = addrArray(0)
                Try
                    addr2 = addrArray(1)
                Catch ex As Exception

                End Try

                Try
                    addr3 = addrArray(2)
                Catch ex As Exception

                End Try

                Try
                    addr4 = addrArray(3)
                Catch ex As Exception
                   
                End Try
                Try
                    addr5 = addrArray(4)
                Catch ex As Exception

                End Try
                Try
                    postcode = addrArray(5)
                Catch ex As Exception

                End Try
                If postcode = "" Then
                    If addr5 <> "" Then
                        postcode = addr5
                    ElseIf addr4 <> "" Then
                        postcode = addr4
                    Else
                        postcode = addr3
                    End If
                Else
                    addr4 &= addr5
                End If


                If postcode = addr4 Then
                    addr4 = ""
                End If
                If postcode = addr3 Then
                    addr3 = ""
                End If
                outFile &= addr1 & "," & addr2 & "," & addr3 & "," & addr4 & "," & postcode & ","
                addressChange = True
                Exit For
            Next
            'get debt address
            Dim debtAddr As String = ""
            Try
                debtAddr = debtRow(8)
            Catch ex As Exception

            End Try

            debtAddr = Replace(debtAddr, ",", " ")
            Dim addr As String = ""
            'split by lines
            Dim addrLine As Integer = 0
            If debtAddr <> "" Then
                For addridx = 1 To debtAddr.Length
                    If Mid(debtAddr, addridx, 1) = Chr(10) Then
                        addrLine += 1
                        Select Case addrLine
                            Case 1
                                debtaddr1 = addr
                            Case 2
                                debtaddr2 = addr
                            Case 3
                                debtaddr3 = addr
                            Case 4
                                debtaddr4 = addr
                            Case Else
                                debtaddr4 &= " " & addr
                        End Select
                        addr = ""
                    Else
                        addr &= Mid(debtAddr, addridx, 1)
                    End If
                Next
            End If
            addrLine += 1
            Select Case addrLine
                Case 1
                    debtaddr1 = addr
                Case 2
                    debtaddr2 = addr
                Case 3
                    debtaddr3 = addr
                Case 4
                    debtaddr4 = addr
            End Select
            Try
                debtpostcode = debtRow(9)
            Catch ex As Exception
                debtpostcode = debtaddr4
                debtaddr4 = ""
            End Try
            If debtpostcode = debtaddr3 Then
                debtaddr3 = ""
            End If
            If debtpostcode = debtaddr4 Then
                debtaddr4 = ""
            End If
            If Not addressChange Then
                outFile &= debtaddr1 & "," & debtaddr2 & "," & debtaddr3 & "," & debtaddr4 & "," & debtpostcode & ","
            End If

            'get any contact tel numbers in notes
            '"Debtor 1 Contact Tel No 1:
            Dim note2_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select text from note" & _
                           " where DebtorID = " & debtRow(0) & _
                           " and type = 'Client note'" & _
                           " and text like '%Contact Tel No%'", note2_dt, False)
            Dim phoneNumberIdx As Integer = 0
            For Each note2Row In note2_dt.Rows
                Dim colonIdx As Integer = InStr(note2Row(0), ":")
                Dim phoneNumber As String = Microsoft.VisualBasic.Right(note2Row(0), note2Row(0).length - colonIdx)
                phoneNumber = Replace(phoneNumber, ";", "")
                phoneNumberIdx += 1
                Select Case phoneNumberIdx
                    Case 1
                        origTel1 = phoneNumber
                    Case 2
                        origTel2 = phoneNumber
                    Case 3
                        origTel3 = phoneNumber
                End Select
            Next
            outFile &= origTel1 & "," & origTel2 & "," & origTel3 & ","

            'title forename surname
            outFile &= ",,,"

            If addressChange Then
                outFile &= debtaddr1 & "," & debtaddr2 & "," & debtaddr3 & "," & debtaddr4 & "," & debtpostcode & ","
            Else
                outFile &= ",,,,,"
            End If

            'tel number
            Dim tel1 As String = ""
            Dim tel2 As String = ""
            Dim telEmp As String = ""
            Try
                tel1 = debtRow(10)
            Catch ex As Exception

            End Try
            Try
                tel2 = debtRow(11)
            Catch ex As Exception

            End Try

            Try
                telEmp = debtRow(12)
            Catch ex As Exception

            End Try

            If tel1 <> "" Then
                If tel1 = origTel1 Or tel1 = origTel2 Or tel1 = origTel3 Then
                    tel1 = ""
                End If
            End If
            If tel2 <> "" Then
                If tel2 = origTel1 Or tel2 = origTel2 Or tel2 = origTel3 Then
                    tel2 = ""
                End If
            End If
            If tel1 = telEmp Then
                tel1 = tel2
            End If
            If tel1 = "" Then
                tel1 = tel2
                tel2 = ""
            End If
            If telEmp <> "" Then
                If telEmp = origTel1 Or telEmp = origTel2 Or telEmp = origTel3 Then
                    telEmp = ""
                End If
            End If
            outFile &= tel1 & "," & telEmp & ","

            'placement debt
            outFile &= debtRow(13) & ","

            'debt type
            outFile &= debtRow(14) & ","

            'work items
            Dim note3_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select text from note" & _
                           " where DebtorID = " & debtRow(0) & _
                           " and type = 'Client note'" & _
                           " and text like 'No. of Items: %'", note3_dt, False)
            For Each note3Row In note3_dt.Rows
                Dim workItems As String = Microsoft.VisualBasic.Right(note3Row(0), note3Row(0).length - 13)
                Dim testInt As Integer
                Try
                    testInt = workItems
                Catch ex As Exception
                    workItems = ""
                End Try
                outFile &= workItems & ","
            Next

            'get tax year
            Dim note4_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select text from note" & _
                           " where DebtorID = " & debtRow(0) & _
                           " and type = 'Client note'" & _
                           " and text like 'Tax Year Due%'", note4_dt, False)
            Dim latestTaxYear As Integer = 1900
            Dim oldestTaxYear As Integer = 2100
            For Each note4Row In note4_dt.Rows
                Dim colonIDX As Integer = InStr(note4Row(0), ":")
                Dim testYear As String = Microsoft.VisualBasic.Right(note4Row(0), note4Row(0).length - colonIDX)
                If IsNumeric(testYear) Then
                    If testYear > latestTaxYear Then
                        latestTaxYear = testYear
                    End If
                    If testYear < oldestTaxYear Then
                        oldestTaxYear = testYear
                    End If
                End If
            Next
            If latestTaxYear = 1900 Then
                outFile &= ","
            Else
                outFile &= latestTaxYear & ","
            End If
            If oldestTaxYear = 2100 Then
                outFile &= ","
            Else
                outFile &= oldestTaxYear & ","
            End If


            'get due date
            Dim note5_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select text from note" & _
                           " where DebtorID = " & debtRow(0) & _
                           " and type = 'Client note'" & _
                           " and text like 'Latest Due Date%'", note5_dt, False)
            Dim latestDueDate As Date = lowdate
            Dim oldestDueDate As Date = highdate
            For Each note5Row In note5_dt.Rows
                Dim colonIDX As Integer = InStr(note5Row(0), ":")
                Dim testDate As String = Microsoft.VisualBasic.Right(note5Row(0), note5Row(0).length - colonIDX)
                If IsDate(testDate) Then
                    If testDate > latestDueDate Then
                        latestDueDate = testDate
                    End If
                    If testDate < oldestDueDate Then
                        oldestDueDate = testDate
                    End If
                End If
            Next
            If latestDueDate = lowdate Then
                outFile &= ","
            Else
                outFile &= latestDueDate & ","
            End If
            If oldestDueDate = highdate Then
                outFile &= ","
            Else
                outFile &= oldestDueDate & ","
            End If

            outFile &= vbNewLine
            debtCount += 1
            'If debtCount > 100 Then
            '    Exit For
            'End If
            My.Computer.FileSystem.WriteAllText("H:temp\HMRCAccountCharacteristics.txt", outFile, True)
            outFile = ""
        Next

        Me.Close()
    End Sub
End Class

﻿Imports System.Configuration
Imports System.Data
Imports System.Data.Odbc

Public Module modData
    Public DBCon As New OdbcConnection

    Public Sub ConnectDb(ByVal ConnStr As String)
        Try
            If Not IsNothing(DBCon) Then
                'This is only necessary following an exception...
                If DBCon.State = ConnectionState.Open Then DBCon.Close()
            End If

            DBCon.ConnectionString = ConfigurationManager.ConnectionStrings(ConnStr).ConnectionString
            DBCon.Open()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub DisconnectDb()
        DBCon.Close()
        DBCon.Dispose()
    End Sub

    Public Sub LoadDataTable(ByVal ConnStr As String, ByVal Sql As String, ByRef DataTable As DataTable, Optional ByVal Preserve As Boolean = False)
        Try
            Dim da As New OdbcDataAdapter(Sql, DBCon)

            ConnectDb(ConnStr)

            If Preserve = False Then DataTable.Clear()

            da.Fill(DataTable)
            da.Dispose()
            da = Nothing

            DisconnectDb()

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Sub

    Public Sub ExecStoredProc(ByVal ConnStr As String, ByVal SQL As String, Optional ByVal TimeoutSeconds As Short = 60)

        Try
            ConnectDb(ConnStr)

            Dim Comm As New OdbcCommand(SQL, DBCon)
            Comm.CommandTimeout = TimeoutSeconds
            Comm.ExecuteNonQuery()

            DisconnectDb()

            Comm.Dispose()
            Comm = Nothing

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Sub

    Public Function GetSQLResults(ByVal ConnStr As String, ByVal SQL As String) As String
        GetSQLResults = Nothing

        Try
            ConnectDb(ConnStr)
            Dim Reader As OdbcDataReader
            Dim Comm As New OdbcCommand(SQL, DBCon)

            Reader = Comm.ExecuteReader

            If Reader.HasRows Then
                Reader.Read()
                GetSQLResults = Reader(0)
            End If

            DisconnectDb()

            Comm.Dispose()
            Comm = Nothing

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Function GetSQLResultsArray(ByVal ConnStr As String, ByVal SQL As String) As Object()
        GetSQLResultsArray = Nothing
        Dim ArrayList As New ArrayList()
        Dim FieldCount As Integer

        Try
            ConnectDb(ConnStr)
            Dim Reader As OdbcDataReader
            Dim Comm As New OdbcCommand(SQL, DBCon)

            Reader = Comm.ExecuteReader

            If Reader.HasRows Then
                Reader.Read()
                For FieldCount = 1 To Reader.FieldCount
                    ArrayList.Add(Reader(FieldCount - 1))
                Next FieldCount
            End If

            DisconnectDb()

            Comm.Dispose()
            Comm = Nothing

            If Not IsNothing(ArrayList) Then GetSQLResultsArray = ArrayList.ToArray

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

End Module


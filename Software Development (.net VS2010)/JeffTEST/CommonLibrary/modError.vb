﻿Imports System.IO

Public Module modError

    Public Sub HandleException(ByVal ex As Exception)
        Try

            MsgBox(ex.ToString, vbOKOnly + vbCritical, Path.GetFileNameWithoutExtension(Process.GetCurrentProcess().MainModule.FileName))
            If Process.GetCurrentProcess().MainModule.FileName.IndexOf(".vshost.") = -1 Then ' Only log errors for live executables and not from Visual Studio solutions
                Dim ErrWriter As New StreamWriter("R:\IT User Logs\" & Path.GetFileNameWithoutExtension(Process.GetCurrentProcess().MainModule.FileName) & ".log", True, System.Text.Encoding.ASCII)
                ErrWriter.WriteLine("DateTime: " & Now.ToString)
                ErrWriter.WriteLine("UserName: " & Environment.UserName)
                ErrWriter.WriteLine("MachineName: " & Environment.MachineName)
                ErrWriter.WriteLine("Executable: " & Process.GetCurrentProcess().MainModule.FileName)
                ErrWriter.WriteLine()
                ErrWriter.WriteLine(ex.ToString)
                ErrWriter.WriteLine()
                ErrWriter.WriteLine()
                ErrWriter.Close()
            End If

        Catch
            MsgBox("Error handling failed", vbOKOnly + vbCritical, "Error")
        End Try

    End Sub

End Module

﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration
Imports System.IO.Compression

Public Class frmMain
    'Private TDXHMRCData As New clsJeffTestData
    Private InputFilePath As String, FileName As String, FileExt As String


    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click

        ''fees sql delete some rows
        'ConnectDb2Fees("FeesSQL")
        'Dim upd_txt As String = "delete from fee" & _
        '                    " where rowID = 354876996"

        'update_sql(upd_txt)
        Dim exePath As String = "C:\Program Files\7-Zip\7z.exe"
        Dim args As String = "e C:\AATemp\Test.zip" + " -o""C:\AATemp\Zipped\"""
        System.Diagnostics.Process.Start(exePath, args)

        Try
            Dim prod_run As Boolean = False
            Dim env_str As String = ""
            Try
                env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
            Catch ex As Exception
                prod_run = False
            End Try
            Dim ClientID As Integer
            If env_str = "Prod" Then prod_run = True
            Dim start_remit_no As Integer
            Try
                start_remit_no = GetSQLResults("FeesSQL", "select last_remit_no from LastRemitPdf2Tiff")
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            Dim clref As String = GetSQLResults("DebtRecoveryLocalX", "SELECT min(client_ref) " & _
                                                    "FROM debtor  " & _
                                                    "WHERE _rowid = " & 123456)

            ClientID = 24
            If prod_run Then ClientID = 1736
            Dim debtor1 As Integer = 4007534
            Dim debtor2 As Integer = 4007545
            Dim Array As Object()
            Dim test2 As String = "¬"
            Dim char2 As Integer
            char2 = AscW(Chr(test2))


            'ONE row ONLY
            Array = GetSQLResultsArray("DebtRecoveryLocal", "SELECT _rowid, client_ref, clientSchemeID, debt_balance " & _
                                                    "FROM debtor " & _
                                                    "WHERE _rowid= " & debtor1)
            Dim cref As String = Array(1)
            'Dim dt4 As New DataTable
            'LoadDataTable("Touchstar", "SELECT Login " & _
            '                                        "FROM Agent " & _
            '                                        "WHERE AgentID = 5", dt4, False)
            'Dim aa4 As Integer = dt4.Rows.Count
            'Dim ff4 As String = dt4.Rows(0).Item(0)


            Dim dt3 As New DataTable
            LoadDataTable("LSCReporting", "SELECT client_ref " & _
                                                    "FROM Debtor " & _
                                                    "WHERE DebtorID = 5202449", dt3, False)
            Dim aa3 As Integer = dt3.Rows.Count
            Dim ff3 As String = dt3.Rows(0).Item(0)


            'Complaints table
            Dim dt2 As New DataTable
            LoadDataTable("PraiseAndComplaintsSQL", "SELECT comp_date " & _
                                                    "FROM Complaints " & _
                                                    "WHERE comp_no = 100", dt2, False)
            Dim aa As Integer = dt2.Rows.Count
            Dim ff As Date = dt2.Rows(0).Item(0)
            'access agency database
            'Dim EnquiryArray As Object()
            'ONE row ONLY
            'EnquiryArray = GetSQLResultsArray("Enquiries", "SELECT enq_date " & _
            '   "FROM Enquiries " & _
            '  "WHERE enq_no= 100")

            'Dim Enquirydate As Date = EnquiryArray(0)
            Dim FileDialog As New OpenFileDialog
            Dim LineNumber As Integer, DebtNumber As Integer, DebtColsStart As Integer
            Dim OutputFile As String = ""
            Dim AuditLog As String, ErrorLog As String = "", InputLineArray() As String, DebtNotes As String
            Dim debtorID As String = ""
            Dim CaseBalance As Decimal = 0, TotalBalance As Decimal = 0

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            lblReadingFile.Visible = True
            'read excel file - ZERO based
            Dim excel_file_contents(,) As String = InputFromExcel(FileDialog.FileName)
            Dim x As Integer = 0
            Dim y As Integer = 3
            'abend test
            'Dim zero_divide As Integer = y / x

            'below for text file
            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents)

            'ONE row ONLY
            Array = GetSQLResultsArray("DebtRecovery", "SELECT _rowid, client_ref, clientSchemeID, debt_balance " & _
                                                    "FROM debtor " & _
                                                    "WHERE _rowid= " & debtor1)
            Dim l As Integer = UBound(Array)
            Dim xx As String = Array(3)

            'get multiple rows - datatable
            Dim dt As New DataTable
            LoadDataTable("DebtRecovery", "SELECT _rowid, client_ref, clientSchemeID, debt_balance " & _
                                                    "FROM debtor " & _
                                                    "WHERE _rowid>= " & debtor1 & _
                                                    " AND _rowid <= " & debtor2, dt, False)
            Dim aa2 As Integer = dt2.Rows.Count
            Dim ff2 As String = dt2.Rows(2).Item(3)
            Dim row As DataRow
            For Each row In dt.Rows
                Dim debtID As Integer = row.Item(0)
                Dim test As Integer = 0
            Next

            'Fees table
            LoadDataTable("FeesSQL", "SELECT debtorID, fee_amount, type " & _
                                                    "FROM Fee " & _
                                                    "WHERE debtorID = " & debtor1, dt2, False)

            For Each row In dt2.Rows
                Dim debtID As Integer = row.Item(0)
                Dim test As Integer = 0
            Next
            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber

                LineNumber += 1
                If LineNumber = 1 Then Continue For ' skip header line

                CaseBalance = 0
                DebtNotes = ""
                InputLineArray = InputLine.Split(",")

                Application.DoEvents() ' without this line, the button disappears until processing is complete
                For DebtNumber = 1 To (UBound(InputLineArray) - 20) / 4
                    DebtColsStart = 20 + ((DebtNumber - 1) * 4)

                    If InputLineArray(DebtColsStart) = "" Then Exit For

                    CaseBalance += CDec(InputLineArray(DebtColsStart + 2))
                    DebtNotes &= String.Join("", ToNote(" " & InputLineArray(DebtColsStart), "Tax Year Due " & DebtNumber.ToString.PadLeft(2, "0"), ";"))
                    DebtNotes &= String.Join("", ToNote(" " & InputLineArray(DebtColsStart + 1), "Latest Due Date " & DebtNumber.ToString.PadLeft(2, "0"), ";"))
                    DebtNotes &= String.Join("", ToNote(" " & InputLineArray(DebtColsStart + 2), "Collectable Amount of Work Item " & DebtNumber.ToString.PadLeft(2, "0"), ";"))
                    DebtNotes &= String.Join("", ToNote(" " & InputLineArray(DebtColsStart + 3), "Payment Ref " & DebtNumber.ToString.PadLeft(2, "0"), ";"))

                Next DebtNumber

                If DebtNumber - 1 <> InputLineArray(19) Then ErrorLog &= "Expecting " & InputLineArray(19) & " debt items, found " & (DebtNumber - 1).ToString & " at line number " & LineNumber.ToString & vbCrLf

                ' DebtorID = TDXHMRCData.GetDebtorID(ClientID, InputLineArray(1), CaseBalance)

                If DebtorID <> "" Then

                    OutputFile &= DebtorID & "|"

                    If InputLineArray(2) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(2), "Client name", ";"))
                    If InputLineArray(3) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(3), "Debt type", ";"))
                    If InputLineArray(5) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(5), "Debt package number", ";"))
                    If InputLineArray(6) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(6), "Tax payer also known as", ";"))
                    If InputLineArray(15) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(15), "Business tel no", ";"))
                    If InputLineArray(19) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(19), "Total number of debts in new business file", ";"))

                    OutputFile &= DebtNotes & vbCrLf
                    TotalBalance += CaseBalance
                Else
                    ErrorLog &= "Cannot find DebtorID for client ref " & InputLineArray(1) & " at line number " & LineNumber.ToString & vbCrLf
                End If

            Next InputLine

            InputLineArray = FileContents(0).Split(",") ' Check header record case count
            If LineNumber - 1 <> InputLineArray(3) Then ErrorLog &= "Expecting " & InputLineArray(3) & " cases, found " & LineNumber.ToString

            WriteFile(InputFilePath & FileName & "_PreProcessed.txt", OutputFile)

            AuditLog = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
            AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

            AuditLog &= "Number of new cases: " & UBound(FileContents).ToString & vbCrLf
            AuditLog &= "Total balance of new cases: " & TotalBalance.ToString

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)

            MsgBox("Pre-processing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

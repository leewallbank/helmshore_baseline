﻿Imports CommonLibrary

Public Class clsJeffTestData
    Public Function GetDebtorID(ByVal ClientID As Integer, ByVal ClientRef As String, ByVal TotalDebt As Decimal) As String
        ' Used for main billing charge notes

        GetDebtorID = GetSQLResults("DebtRecovery", "SELECT d._rowID " & _
                                                    "FROM debtor AS d " & _
                                                    "INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID " & _
                                                    "WHERE d.client_ref = '" & ClientRef & "' " & _
                                                    "  AND d.debt_amount = " & TotalDebt.ToString & _
                                                    "  AND d.status_open_closed = 'O' " & _
                                                    "  AND cs.clientID = " & ClientID)
    End Function
End Class

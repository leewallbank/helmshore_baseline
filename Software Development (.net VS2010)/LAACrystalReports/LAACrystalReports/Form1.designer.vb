<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.RD890LSCbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.allbtn = New System.Windows.Forms.Button
        Me.ra890cmecbtn = New System.Windows.Forms.Button
        Me.ra1624btn = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'RD890LSCbtn
        '
        Me.RD890LSCbtn.Location = New System.Drawing.Point(75, 97)
        Me.RD890LSCbtn.Name = "RD890LSCbtn"
        Me.RD890LSCbtn.Size = New System.Drawing.Size(126, 23)
        Me.RD890LSCbtn.TabIndex = 3
        Me.RD890LSCbtn.Text = "Run RA890 LSC"
        Me.RD890LSCbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(247, 340)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 7
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'allbtn
        '
        Me.allbtn.Location = New System.Drawing.Point(76, 35)
        Me.allbtn.Name = "allbtn"
        Me.allbtn.Size = New System.Drawing.Size(125, 23)
        Me.allbtn.TabIndex = 2
        Me.allbtn.Text = "Run ALL reports"
        Me.allbtn.UseVisualStyleBackColor = True
        '
        'ra890cmecbtn
        '
        Me.ra890cmecbtn.Location = New System.Drawing.Point(76, 147)
        Me.ra890cmecbtn.Name = "ra890cmecbtn"
        Me.ra890cmecbtn.Size = New System.Drawing.Size(126, 23)
        Me.ra890cmecbtn.TabIndex = 4
        Me.ra890cmecbtn.Text = "Run RA890 CMEC"
        Me.ra890cmecbtn.UseVisualStyleBackColor = True
        '
        'ra1624btn
        '
        Me.ra1624btn.Location = New System.Drawing.Point(76, 204)
        Me.ra1624btn.Name = "ra1624btn"
        Me.ra1624btn.Size = New System.Drawing.Size(126, 23)
        Me.ra1624btn.TabIndex = 6
        Me.ra1624btn.Text = "Run RA1624"
        Me.ra1624btn.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(334, 375)
        Me.Controls.Add(Me.ra1624btn)
        Me.Controls.Add(Me.ra890cmecbtn)
        Me.Controls.Add(Me.allbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.RD890LSCbtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "LAA Crystal reports"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents RD890LSCbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents allbtn As System.Windows.Forms.Button
    Friend WithEvents ra890cmecbtn As System.Windows.Forms.Button
    Friend WithEvents ra1624btn As System.Windows.Forms.Button

End Class

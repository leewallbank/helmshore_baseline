﻿Imports CommonLibrary
Imports System.IO
Public Class Form1
    Private addresses, visits As Integer
    Private totVisits, totAddresses As Integer
    Private InputFilePath As String, FileName As String, FileExt As String
    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub readbtn_Click(sender As System.Object, e As System.EventArgs) Handles readbtn.Click
        Dim FileDialog As New OpenFileDialog
       
        Dim OutputFile As String = "Bailiff Name, BailiffID,Camera Number,Date,Number Of Videos,Total Size,Full size videos," & _
           "Onestep cases, Onestep Addresses" & vbNewLine
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        exitbtn.Enabled = False
        readbtn.Enabled = False

        Dim HighDate As Date = CDate("Jan 1, 2100")
        Dim lowDate As Date = CDate("Jan 1, 1900")
        Dim message As String = ""
        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)
        Dim lastBWVDate As Date = lowDate

        Dim lastEAName As String = ""
        Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
        Dim InputLineArray() As String
        Dim InputLineArray2() As String
        Dim testdate As Date
        Dim cameraNumber As String = ""
        Dim lastCameraNumber As String = ""
        Dim rowMax As Integer = UBound(FileContents)
        Dim rowID, lastBailiffID As Integer
        Dim ignoreEA As Boolean = True
        Dim OSCameraNumber As String = ""
        ProgressBar1.Maximum = rowMax
        For Each InputLine As String In FileContents
            rowID += 1
            Try
                ProgressBar1.Value = rowID
                Application.DoEvents()
            Catch ex As Exception
                rowID = 0
            End Try
            InputLineArray = Split(InputLine, ",", Chr(34))
            For colIDX = 0 To 6
                Try
                    InputLineArray(colIDX) = Replace(InputLineArray(colIDX), Chr(34), "")
                Catch ex As Exception
                    Exit For
                End Try

            Next
           
            visits = 0
            addresses = 0
        

            message = ""
            Dim bwvDate As Date

            Try
                bwvDate = InputLineArray(2)
            Catch ex As Exception
                If InputLineArray(0) <> "Total" Then
                    Continue For
                End If
            End Try
            Dim EAName As String = InputLineArray(0)
            cameraNumber = InputLineArray(1)

            If EAName = "Total" And ignoreEA = False Then
                'write out last lines to end of period
                bwvDate = end_dtp.Value
                While Format(testdate, "yyyy-MM-dd") <= Format(bwvDate, "yyyy-MM-dd")
                    OutputFile &= lastEAName & "," & lastBailiffID & "," & lastCameraNumber & ","
                    OutputFile &= testdate & ","
                    OutputFile &= "0,0,0,0,0" & vbNewLine
                    testdate = DateAdd(DateInterval.Day, 1, testdate)
                End While
                lastBWVDate = lowDate
                ignoreEA = True
                Continue For
            End If

            cameraNumber = "Edge - " & cameraNumber

            'only include EA if a camera exists on onestep
            'read through file to see if any camera number exists on onestep
            If EAName <> lastEAName Then
                Dim lastcameranumber2 As String = ""
                For Each InputLine2 As String In FileContents
                    InputLineArray2 = Split(InputLine2, ",", Chr(34))
                    For colIDX = 0 To 6
                        Try
                            InputLineArray2(colIDX) = Replace(InputLineArray2(colIDX), Chr(34), "")
                        Catch ex As Exception
                            Exit For
                        End Try
                    Next
                    If InputLineArray2(0) <> EAName Then
                        Continue For
                    End If
                    Dim cameraNumber2 As String = InputLineArray2(1)
                    cameraNumber2 = "Edge - " & cameraNumber2
                    If cameraNumber2 = lastcameranumber2 Then
                        Continue For
                    End If
                    lastcameranumber2 = cameraNumber2
                    Dim eadt2 As New DataTable
                    LoadDataTable("DebtRecovery", "SELECT _rowid, name_fore, name_sur, login_name from bailiff " & _
                                                     "WHERE add_fax = '" & cameraNumber2 & "'", eadt2, False)
                    'write out any case details for missing dates
                    If eadt2.Rows.Count > 0 Then
                        ignoreEA = False
                        Exit For
                    End If
                Next
            End If

            If ignoreEA Then
                Continue For
            End If

            lastCameraNumber = cameraNumber
            lastEAName = EAName

            Dim eadt As New DataTable
            Dim bailiffID As Integer

            If lastBWVDate = lowDate Then
                testdate = start_dtp.Value
            Else
                testdate = DateAdd(DateInterval.Day, 1, lastBWVDate)
            End If

            While Format(testdate, "yyyy-MM-dd") < Format(bwvDate, "yyyy-MM-dd")
                LoadDataTable("DebtRecovery", "SELECT _rowid, name_fore, name_sur, login_name from bailiff " & _
                                                   "WHERE add_fax = '" & cameraNumber & "'", eadt, False)
                'write out any case details for missing dates
                If eadt.Rows.Count > 0 Then
                    bailiffID = eadt.Rows(0).Item(0)
                Else
                    bailiffID = 0
                End If
                lastBailiffID = bailiffID
                OutputFile &= InputLineArray(0) & "," & bailiffID & "," & cameraNumber & ","
                OutputFile &= testdate & ","
                Dim smallbwv2 As Integer = 0
                Try
                    smallbwv2 = InputLineArray(6)
                Catch ex As Exception

                End Try
                OutputFile &= "0,0,0,"

                If bailiffID > 0 Then
                  
                    get_visits(bailiffID, testdate)
                  

                End If
                OutputFile &= visits & "," & addresses
               
                OutputFile &= vbNewLine
                testdate = DateAdd(DateInterval.Day, 1, testdate)
                visits = 0
                addresses = 0
              

                message = ""
            End While


            LoadDataTable("DebtRecovery", "SELECT _rowid, name_fore, name_sur, login_name from bailiff " & _
                                                        "WHERE add_fax = '" & cameraNumber & "'", eadt, False)
            If eadt.Rows.Count > 0 Then
                bailiffID = eadt.Rows(0).Item(0)
            Else
                bailiffID = 0
            End If
            lastBailiffID = bailiffID
          
            OutputFile &= InputLineArray(0) & "," & bailiffID & "," & cameraNumber & ","
            OutputFile &= testdate & ","
            Dim smallbwv As Integer = 0
            Try
                smallbwv = InputLineArray(6)
            Catch ex As Exception

            End Try
            OutputFile &= InputLineArray(3) & ",0," & (InputLineArray(3) - smallbwv) & ","
            If bailiffID > 0 Then
               
                get_visits(bailiffID, bwvDate)
               
            End If
            OutputFile &= visits & "," & addresses
           
            OutputFile &= vbNewLine
            lastBWVDate = bwvDate
            visits = 0
            addresses = 0
          
            message = ""

        Next
        Dim outFileName As String = InputFilePath & "BWVEdge_Report.csv"
        My.Computer.FileSystem.WriteAllText(outFileName, OutputFile, False)
        MsgBox("Report Produced")
        Me.Close()
    End Sub
    Private Sub get_visits(ByVal EAID As Integer, ByVal dte As Date)
        Dim visitdt As New DataTable
        LoadDataTable("DebtRecovery", "select V.debtorID, D.linkID, V.date_visited from visit V, debtor D, clientscheme CS" & _
                      " where V.bailiffID = " & EAID & _
                      " and D.clientschemeID = CS._rowID " & _
                      " and not(CS.clientID in (1,2,24))" & _
                      " and V.debtorID = D._rowID " & _
                      " and date(V.date_visited) = '" & Format(dte, "yyyy-MM-dd") & "'" & _
                      " order by D.linkID", visitdt, False)
        Dim lastLinkID As Integer = 0
        For Each visitRow In visitdt.Rows
            visits += 1
            totVisits += 1
            Dim visitDate As Date = visitRow(2)
           
            Dim linkID As Integer
            Try
                linkID = visitRow(1)
            Catch ex As Exception
                addresses += 1
                totAddresses += 1
                Continue For
            End Try
            If linkID <> lastLinkID Then
                addresses += 1
                totAddresses += 1
            End If
            lastLinkID = linkID
        Next

    End Sub

   

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim startDate As Date = DateAdd(DateInterval.Month, -1, Now)
        startDate = CDate(Format(startDate, "yyyy-MM-") & "01 00:00:00")
        Dim endDate As Date = CDate(Format(Now, "yyyy-MM-") & "01 00:00:00")
        endDate = DateAdd(DateInterval.Day, -1, endDate)
        start_dtp.Value = startDate
        end_dtp.Value = endDate
    End Sub
End Class

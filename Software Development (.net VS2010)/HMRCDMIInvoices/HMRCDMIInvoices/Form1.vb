﻿Imports System.IO

Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        runbtn.Enabled = False
        exitbtn.Enabled = False
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD253IPreport = New RD253IP
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD253IPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD253IPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253IPreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RD253I TDX HMRC DMI non-direct invoice.pdf"
        End With

        If SaveFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("Reports not run")
            Exit Sub
        End If
        Dim pathname As String = Path.GetDirectoryName(SaveFileDialog1.FileName) & "\"
        RD253IPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, SaveFileDialog1.FileName)
        RD253IPreport.Close()

        Dim RD253IDPreport = New RD253IDP
        'Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD253IDPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD253IDPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253IDPreport)
        Dim filename As String = pathname & "RD253ID TDX HMRC DMI direct invoice.pdf"
        RD253IDPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD253IDPreport.Close()

        Dim RD253JDPreport = New RD253JDP
        'Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD253JDPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD253JDPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253JDPreport)
        filename = pathname & "RD253JD TDX TCPC direct invoice.pdf"
        RD253JDPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD253JDPreport.Close()

        Dim RD253JPreport = New RD253JP
        'Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD253JPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD253JPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253JDPreport)
        filename = pathname & "RD253J TDX TCPC non-direct invoice.pdf"
        RD253JPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD253JPreport.Close()

        Dim RD448IPreport = New RD448IP
        'Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD448IPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD448IPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253IDPreport)
        filename = pathname & "RD448IP TDX HMRC DMI Fee invoice.pdf"
        RD448IPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD448IPreport.Close()

        Dim RD448JPreport = New RD448JP
        'Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD448JPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD448JPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253IDPreport)
        filename = pathname & "RD448JP TCPC Fee invoice.pdf"
        RD448JPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD448JPreport.Close()

        MsgBox("Reports saved")
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        start_dtp.Value = DateAdd(DateInterval.Day, -4 - Weekday(Now), Now)
        end_dtp.Value = DateAdd(DateInterval.Day, -Weekday(Now) + 2, Now)
    End Sub
End Class

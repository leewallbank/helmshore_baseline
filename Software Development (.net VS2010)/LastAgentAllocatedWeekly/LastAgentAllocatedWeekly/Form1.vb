﻿Imports System.IO

Public Class Form1
    Dim runDate As Date = Now
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ConnectDb2("DebtRecovery")

        Dim report As String = "CaseID, Scheme,Status,ReturnDate,LastPaidDate,LastAgent,LastAgentID,PrevAgent1,PrevAgent1ID,PrevAgent2,PrevAgent2ID" & vbNewLine
        'Dim startDate As Date = CDate("jun 1, 2013 00:00:00")
        Dim startDate As Date = DateAdd(DateInterval.Day, -6 - Weekday(Now), Now)
        startDate = CDate(Format(startDate, "yyyy-MM-dd") & ",00:00:00")
        Dim endDate As Date = DateAdd(DateInterval.Day, 8, startDate)
        'get all preTCE cases with remitted fee closed since start date
        'Dim report As String = "CaseID, Scheme,Status,ReturnDate,LastPaidDate,LastAgent,LastAgentID,PrevAgent1,PrevAgent1ID,PrevAgent2,PrevAgent2ID" & vbNewLine
        'Dim startDate As Date = CDate("jun 1, 2013 00:00:00")

        'get all preTCE cases with remitted fee closed since start date
        Dim debt2_dt As New DataTable
        LoadDataTable2("DebtRecovery", "select D._rowid, D.status, D.return_date, S.name " & _
                       " from debtor D, Payment P, clientscheme CS, scheme S" & _
                       " where d._rowID = P.DebtorID and P.status = 'R' and P.split_fees + P.split_van > 0 " & _
                       " and D.clientSchemeID = CS._rowID" & _
                       " and S.work_type in (2,3,16,20,12) " & _
                       " and D.status_open_closed = 'C'" & _
                       " and D.return_date >='" & Format(startDate, "yyyy-MM-dd") & "'" & _
                       " and D.return_date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                       " and CS.schemeID = S._rowID" & _
                       " and CS.clientID not in (1,2,24)" & _
                       " and CS._rowID < 3856 " & _
                       " order by D._rowID", debt2_dt, False)
        Dim rowIDX As Integer = 0
        ProgressBar1.Maximum = debt2_dt.Rows.Count
        For Each debtRow In debt2_dt.Rows
            Try
                ProgressBar1.Value = rowIDX
            Catch ex As Exception

            End Try
            rowIDX += 1
            Application.DoEvents()
            Dim debtorID As Integer = debtRow(0)
            Dim schemeName As String = debtRow(3)
            Dim status As String = debtRow(1)
            Dim returnDate As Date = Nothing
            Try
                returnDate = debtRow(2)
            Catch ex As Exception

            End Try
            'get last paid date
            Dim lastPaidDate As Date
            lastPaidDate = GetSQLResults2("DebtRecovery", "select max(date) " & _
                               " from payment " & _
                               " where debtorID = " & debtorID & _
                               " and status = 'R'")

            report &= debtorID & "," & schemeName & "," & status & "," & Format(returnDate, "dd/MM/yyyy") & "," & _
            lastPaidDate

            'get last 3 agents
            Dim note_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select text from Note" & _
                           " where debtorID = " & debtorID & _
                           " and type = 'Allocated'" & _
                           " order by _rowID desc", note_dt, False)
            Dim agents As Integer = 0
            For Each noteRow In note_dt.Rows
                agents += 1
                Dim text As String = noteRow(0)
                'extract part in brackets
                Dim startIDX As Integer = InStr(text, "(")
                Dim endIDX As Integer = InStr(text, "ID")
                If startIDX = 0 Then
                    Continue For
                End If
                Dim agentName As String = Mid(text, startIDX + 1, endIDX - startIDX - 1)
                agentName = Replace(agentName, ",", " ")
                report &= "," & agentName & ","
                'now getID
                Dim agentID As String = Microsoft.VisualBasic.Right(text, text.Length - endIDX - 2)
                Dim end2IDX As Integer = InStr(agentID, ")")
                agentID = Microsoft.VisualBasic.Left(agentID, end2IDX - 1)
                report &= agentID
                If agents >= 30 Then Exit For
            Next
            report &= vbNewLine
            My.Computer.FileSystem.WriteAllText("\\ross-helm-fp001\Rossendales Shared\LastAgentAllocatedWeekly\LastAgentpreTCE_" & Format(runDate, "yyyy-MM-dd") & ".txt", report, True)
            report = ""
        Next

        'Get all  cases closed since 1.6.2013 with enforcement fee
        Dim debt_dt As New DataTable
        LoadDataTable2("DebtRecovery", "select D._rowid, D.status, D.return_date, S.name " & _
                       " from debtor D, fee F, Payment P, clientscheme CS, scheme S" & _
                       " where D._rowid = F.DebtorID " & _
                       " and F.fee_amount > 0 and F.type = 'Enforcement'" & _
                       " and d._rowID = P.DebtorID and P.status = 'R' and P.split_fees + P.split_van > 0 " & _
                       " and D.clientSchemeID = CS._rowID" & _
                       " and S.work_type in (2,3,16,20,12) " & _
                       " and D.status_open_closed = 'C'" & _
                       " and D.return_date >'" & Format(startDate, "yyyy-MM-dd") & "'" & _
                       " and D.return_date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                       " and CS.schemeID = S._rowID" & _
                       " and CS.clientID not in (1,2,24)" & _
                       " group by D._rowid, D.status, D.return_date, S.name", debt_dt, False)
        ProgressBar1.Maximum = debt_dt.Rows.Count
        rowIDX = 0
        For Each debtRow In debt_dt.Rows
            Try
                ProgressBar1.Value = rowIDX
            Catch ex As Exception

            End Try
            rowIDX += 1
            Application.DoEvents()
            Dim debtorID As Integer = debtRow(0)
            Dim schemeName As String = debtRow(3)
            Dim status As String = debtRow(1)
            Dim returnDate As Date = Nothing
            Try
                returnDate = debtRow(2)
            Catch ex As Exception

            End Try
            'get last paid date
            Dim lastPaidDate As Date
            lastPaidDate = GetSQLResults2("DebtRecovery", "select max(date) " & _
                               " from payment " & _
                               " where debtorID = " & debtorID & _
                               " and status = 'R'")

            report &= debtorID & "," & schemeName & "," & status & "," & Format(returnDate, "dd/MM/yyyy") & "," & _
            lastPaidDate

            'get last 3 agents
            Dim note_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select text from Note" & _
                           " where debtorID = " & debtorID & _
                           " and type = 'Allocated'" & _
                           " order by _rowID desc", note_dt, False)
            Dim agents As Integer = 0
            For Each noteRow In note_dt.Rows
                agents += 1
                Dim text As String = noteRow(0)
                'extract part in brackets
                Dim startIDX As Integer = InStr(text, "(")
                Dim endIDX As Integer = InStr(text, "ID")
                If startIDX = 0 Then
                    Continue For
                End If
                Dim agentName As String
                Try
                    agentName = Mid(text, startIDX + 1, endIDX - startIDX - 1)
                Catch ex As Exception
                    Continue For
                End Try

                agentName = Replace(agentName, ",", " ")
                report &= "," & agentName & ","
                'now getID
                Dim agentID As String = Microsoft.VisualBasic.Right(text, text.Length - endIDX - 2)
                Dim end2IDX As Integer = InStr(agentID, ")")
                agentID = Microsoft.VisualBasic.Left(agentID, end2IDX - 1)
                report &= agentID
                If agents >= 30 Then Exit For
            Next
            report &= vbNewLine
            My.Computer.FileSystem.WriteAllText("\\ross-helm-fp001\Rossendales Shared\LastAgentAllocatedWeekly\LastAgentpostTCE_" & Format(runDate, "yyyy-MM-dd") & ".txt", report, True)
            report = ""
        Next
        'Dim inputfilepath As String
        'inputfilepath = Path.GetDirectoryName(SaveFileDialog1.FileName)
        'inputfilepath &= "\"



        Me.Close()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
       
    End Sub
End Class

﻿Public Class dispfrm

    Private Sub dispbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dispbtn.Click
        Try
            search_enq_no = InputBox("Enter enquiry number", "Enquiry Number Entry")
        Catch ex As Exception
            MsgBox("Invalid enquiry No")
            Me.Close()
            Exit Sub
        End Try
        If search_enq_no < 8373 Then
            MsgBox("Invalid enquiry No")
            Exit Sub
        End If
        param2 = "select enq_no from Enquiries where enq_no = " & search_enq_no
        Dim enq_ds As DataSet = get_dataset("complaints", param2)
        If no_of_rows = 0 Then
            MsgBox("Enquiry number " & search_enq_no & " does not exist")
            Exit Sub
        End If
        disp3frm.ShowDialog()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
        clear_selections()
    End Sub

    Private Sub selectbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles selectbtn.Click
        search_case_no = 0
        search_cl_no = 0
        search_branch_no = 0
        search_cab_no = 0
        search_adv_agency = 0
        If case_rbtn.Checked Then
            Try
                search_case_no = InputBox("Enter case no", "Case number entry")
            Catch ex As Exception
                MsgBox("Invalid case number")
                Exit Sub
            End Try
        ElseIf cl_rbtn.Checked Then
            clientfrm.ShowDialog()
        ElseIf branch_rbtn.Checked Then
            branchfrm.ShowDialog()
        ElseIf contact_rbtn.Checked Then
            cabfrm.ShowDialog()
            'ElseIf adv_agencyrbtn.Checked Then
            '    adv_agencyfrm.showdialog()
        End If
        disp2frm.ShowDialog()
    End Sub

    Private Sub dispfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        param2 = "select type_name from enquiry_receipt_types order by type_code"
        Dim recpt_ds As DataSet = get_dataset("complaints", param2)
        recpt_cbox.Items.Clear()
        recpt_cbox.SelectedIndex = -1
        For idx = 0 To no_of_rows - 1
            recpt_cbox.Items.Add(recpt_ds.Tables(0).Rows(idx).Item(0))
        Next
        param2 = "select recvd_from_name from enquiry_recvd_from order by recvd_from_code"
        Dim recvd_ds As DataSet = get_dataset("complaints", param2)
        recvd_from_cbox.Items.Clear()
        recvd_from_cbox.SelectedIndex = -1
        For idx = 0 To no_of_rows - 1
            recvd_from_cbox.Items.Add(recvd_ds.Tables(0).Rows(idx).Item(0))
        Next
        param2 = "select nat_name from enquiry_nature_types where nat_code > 16 order by nat_code"
        Dim nat_ds As DataSet = get_dataset("complaints", param2)
        nature_cbox.Items.Clear()
        nature_cbox.SelectedIndex = -1
        For idx = 0 To no_of_rows - 1
            nature_cbox.Items.Add(nat_ds.Tables(0).Rows(idx).Item(0))
        Next
        param2 = "select stat_name from enquiry_status order by stat_code"
        Dim stat_ds As DataSet = get_dataset("complaints", param2)
        status_cbox.Items.Clear()
        status_cbox.SelectedIndex = -1
        For idx = 0 To no_of_rows - 1
            status_cbox.Items.Add(stat_ds.Tables(0).Rows(idx).Item(0))
        Next
        param2 = "select wo_name from Welfare_officers where wo_deleted=0 order by wo_name"
        Dim wo_ds As DataSet = get_dataset("complaints", param2)
        alloc_to_cbox.Items.Clear()
        alloc_to_cbox.SelectedIndex = -1
        For idx = 0 To no_of_rows - 1
            alloc_to_cbox.Items.Add(wo_ds.Tables(0).Rows(idx).Item(0))
        Next
    End Sub

    Private Sub clearbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clearbtn.Click
        clear_selections()
    End Sub
    Private Sub clear_selections()
        recpt_cbox.SelectedIndex = -1
        recvd_from_cbox.SelectedIndex = -1
        status_cbox.SelectedIndex = -1
        all_rbtn.Checked = True
        nature_cbox.SelectedIndex = -1
        alloc_to_cbox.SelectedIndex = -1
        recvd_cbox.Checked = False
        comp_cbox.Checked = False
        ProgressBar1.Value = 0
    End Sub
    Private Sub recvd_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recvd_cbox.CheckedChanged
        If recvd_cbox.Checked Then
            recvd_dtp.Visible = True
            recvd_dtp.Value = Now
        Else
            recvd_dtp.Visible = False
        End If
    End Sub

    Private Sub comp_cbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles comp_cbox.CheckedChanged
        If comp_cbox.Checked Then
            comp_dtp.Visible = True
            comp_dtp.Value = Now
        Else
            comp_dtp.Visible = False
        End If
    End Sub
End Class
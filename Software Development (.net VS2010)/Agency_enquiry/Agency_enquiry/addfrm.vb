﻿
Public Class addfrm

    Private Sub enq_dateDateTimePicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles enq_DateTimePicker.Validating
        ErrorProvider1.SetError(enq_DateTimePicker, "")
        If enq_DateTimePicker.Text < DateAdd("d", -10, Now) Then
            ErrorProvider1.SetError(enq_DateTimePicker, "Date can only go back 10 days")
            e.Cancel = True
        ElseIf enq_DateTimePicker.Text > Now Then
            ErrorProvider1.SetError(enq_DateTimePicker, "Date can't be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub enq_dateDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles enq_DateTimePicker.ValueChanged

    End Sub

    Private Sub addfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'clear fields
        enq_DateTimePicker.Value = Format(Now, "dd/MM/yyyy")
        recpt_type_cbox.SelectedIndex = -1
        recvd_from_cbox.SelectedIndex = -1
        nature_cbox.SelectedIndex = -1
        case_no_tbox.Text = ""
        cl_ref_lbl.Text = ""
        cl_name_tbox.Text = ""
        surname_lbl.Text = ""
        details_tbox.Text = ""
        ' contact_cbox.Enabled = False
        'contact_lbl.Text = ""
        'cabbtn.Enabled = False


        'get enquiry receipt types

        param2 = "select type_code, type_name from Enquiry_receipt_types where type_code < 3"
        Dim recpt_types_ds As DataSet = get_dataset("complaints", param2)
        Dim idx As Integer
        recpt_type_cbox.Items.Clear()
        For idx = 0 To no_of_rows - 1
            Dim type_name As String = recpt_types_ds.Tables(0).Rows(idx).Item(1)
            recpt_type_cbox.Items.Add(type_name)
        Next

        'get nature types

        param2 = "select nat_code, nat_name from Enquiry_nature_types where nat_code > 16 order by nat_code"
        Dim nature_ds As DataSet = get_dataset("complaints", param2)
        nature_cbox.Items.Clear()
        For idx = 0 To no_of_rows - 1
            Dim nat_name As String = nature_ds.Tables(0).Rows(idx).Item(1)
            nature_cbox.Items.Add(nat_name)
        Next

        'get enquiry received from
        param2 = "select recvd_from_code, recvd_from_name from Enquiry_recvd_from where recvd_from_code <>5"
        Dim recvd_from_ds As DataSet = get_dataset("complaints", param2)
        recvd_from_cbox.Items.Clear()
        For idx = 0 To no_of_rows - 1
            Dim recvd_from_name As String = recvd_from_ds.Tables(0).Rows(idx).Item(1)
            recvd_from_cbox.Items.Add(recvd_from_name)
        Next

        'get branches
        param2 = "select branch_name from enquiry_branches order by branch_code"
        Dim br_ds As DataSet = get_dataset("complaints", param2)
        branch_cbox.Items.Clear()
        For idx = 0 To no_of_rows - 1
            branch_cbox.Items.Add(br_ds.Tables(0).Rows(idx).Item(0))
        Next

        'get welfare officers for allocation cbox
        param2 = "select wo_name from Welfare_officers where wo_deleted = 0 order by wo_name"
        Dim wo_dataset As DataSet = get_dataset("complaints", param2)
        alloc_to_cbox.Items.Clear()

        For idx = 0 To no_of_rows - 1
            Dim wo_name As String = wo_dataset.Tables(0).Rows(idx).Item(0)
            alloc_to_cbox.Items.Add(wo_name)
        Next
        alloc_to_cbox.SelectedIndex = -1

        contact_cbox.Items.Clear()
        cabbtn.Enabled = True
        contact_cbox.SelectedIndex = -1
        param2 = "select cab_no, cab_name from CAB_names order by cab_name"
        Dim cab_ds As DataSet = get_dataset("complaints", param2)
        For idx = 0 To no_of_rows - 1
            contact_cbox.Items.Add(cab_ds.Tables(0).Rows(idx).Item(1))
        Next

    End Sub

    Private Sub savebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles savebtn.Click
        Dim error_found As Boolean = False
        ErrorProvider1.SetError(recpt_type_cbox, "")
        ErrorProvider1.SetError(nature_cbox, "")
        ErrorProvider1.SetError(recvd_from_cbox, "")
        ErrorProvider1.SetError(case_no_tbox, "")
        ErrorProvider1.SetError(details_tbox, "")
        ErrorProvider1.SetError(contact_cbox, "")
        ErrorProvider1.SetError(branch_cbox, "")
        If recpt_type_cbox.SelectedIndex < 0 Then
            ErrorProvider1.SetError(recpt_type_cbox, "Please select receipt type")
            error_found = True
        End If

        If nature_cbox.SelectedIndex < 0 Then
            ErrorProvider1.SetError(nature_cbox, "Please enter nature of enquiry")
            error_found = True
        End If

        If recvd_from_cbox.SelectedIndex < 0 Then
            ErrorProvider1.SetError(recvd_from_cbox, "Please enter received from")
            error_found = True
        End If

        If Trim(case_no_tbox.Text).Length = 0 Then
            ErrorProvider1.SetError(case_no_tbox, "Please enter case number")
            error_found = True
        End If

        If Trim(details_tbox.Text).Length = 0 Then
            ErrorProvider1.SetError(details_tbox, "Please enter details of enquiry")
            error_found = True
        End If

        If branch_cbox.SelectedIndex < 0 Then
            ErrorProvider1.SetError(branch_cbox, "Please select the branch")
            error_found = True
        End If

        'If recvd_from_cbox.SelectedIndex = 1 Then
        '    If contact_cbox.SelectedIndex < 0 Then
        '        ErrorProvider1.SetError(contact_cbox, "Please enter CAB office")
        '        error_found = True
        '    End If
        'End If

        'If recvd_from_cbox.SelectedIndex = 0 Then
        '    If contact_cbox.SelectedIndex < 0 Then
        '        ErrorProvider1.SetError(contact_cbox, "Please enter Advice Agency office")
        '        error_found = True
        '    End If
        'End If

        If error_found Then
            Exit Sub
        End If

        Dim cab_no As Integer = 0
        'Get cab number
        cab_no = 0
        If contact_cbox.Text <> "" Then
            param2 = "select cab_no from CAB_names where cab_name = '" & contact_cbox.Text & "'"
            Dim cab_ds As DataSet = get_dataset("complaints", param2)
            If no_of_rows <> 1 Then
                MsgBox("Contact IT error message CAB1")
                cab_no = 0
            Else
                cab_no = cab_ds.Tables(0).Rows(0).Item(0)
            End If
        End If

        Dim adv_no As Integer = 0
        'If recvd_from_cbox.SelectedIndex = 0 Then
        '    'Get adv number
        '    param2 = "select adv_no from Advice_agencies where adv_name = '" & agency_cbox.Text & "'"
        '    Dim adv_ds As DataSet = get_dataset("complaints", param2)
        '    If no_of_rows <> 1 Then
        '        MsgBox("Contact IT error message ADV1")
        '        adv_no = 0
        '    Else
        '        adv_no = adv_ds.Tables(0).Rows(0).Item(0)
        '    End If
        'End If

        Dim alloc_to_code As Integer
        If alloc_to_cbox.SelectedIndex = -1 Then
            alloc_to_code = 0
        Else
            'get code using name from alloc_to_cbox
            param2 = "select wo_code from Welfare_officers where wo_name = '" & alloc_to_cbox.Text & "'"
            Dim wo_ds As DataSet = get_dataset("complaints", param2)
            If no_of_rows <> 1 Then
                MsgBox("Contact IT error message WO1")
                alloc_to_code = 0
            Else
                alloc_to_code = wo_ds.Tables(0).Rows(0).Item(0)
            End If
        End If
        'need to replace any quotes in details or response
        Dim details As String = ""
        For det_idx = 1 To details_tbox.Text.Length
            If Mid(details_tbox.Text, det_idx, 1) = "'" Or Mid(details_tbox.Text, det_idx, 1) = Chr(34) Then
                details &= "''"
            Else
                details &= Mid(details_tbox.Text, det_idx, 1)
            End If
        Next

        upd_txt = "insert into Enquiries (enq_receipt_type,enq_date, enq_recvd_from, enq_nature, enq_case_no," &
            " enq_recvd_by, enq_details, enq_response, enq_cab_no, enq_status_no, enq_completed_by," &
            "enq_completed_date,enq_hold_reason,enq_hold_end_date,enq_action_taken_no,enq_arrangement_broken," &
         "enq_no_of_contacts, enq_branch_no, enq_allocated_to, enq_advice_agency_no) values (" & recpt_type_cbox.SelectedIndex & ",'" &
        Format(enq_DateTimePicker.Value, "dd/MMM/yyyy") & "'," &
        recvd_from_cbox.SelectedIndex & "," & nature_cbox.SelectedIndex + 17 & "," &
        case_no_tbox.Text & "," & login_no & "," &
        "'" & details & "',null," & cab_no & "," & 0 & ",null,'" & Format(null_date, "dd/MMM/yyyy") &
        "',null,'" & Format(null_date, "dd/MMM/yyyy") & "'," & 0 & ",null," & 1 & "," & branch_cbox.SelectedIndex &
        "," & alloc_to_code & "," & adv_no & ")"

        update_sql(upd_txt)
        'get enquiry number
        param2 = "select max(enq_no) from Enquiries where enq_recvd_by = " & login_no
        Dim enq_ds As DataSet = get_dataset("complaints", param2)
        If no_of_rows = 0 Then
            MsgBox("Enquiry not Inserted")
        Else
            Dim enq_no As Integer = enq_ds.Tables(0).Rows(0).Item(0)
            MsgBox("Enquiry has been saved as enquiry No = " & enq_no)
        End If

        Me.Close()
    End Sub

    Private Sub no_savebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles no_savebtn.Click
        Me.Close()
    End Sub

    Private Sub case_no_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles case_no_tbox.TextChanged
        cl_ref_lbl.Text = ""
        cl_name_tbox.Text = ""
        surname_lbl.Text = ""
        If case_no_tbox.TextLength < 4 Then
            Exit Sub
        End If
        If Not IsNumeric(case_no_tbox.Text) Then
            Exit Sub
        End If
        param2 = "select client_ref, clientSchemeID, name_sur , linkID from Debtor where _rowid = " & case_no_tbox.Text
        Dim case_ds As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            ErrorProvider1.SetError(case_no_tbox, "This case is not on onestep")
            cl_ref_lbl.Text = ""
            cl_name_tbox.Text = ""
            surname_lbl.Text = ""
        Else
            ErrorProvider1.SetError(case_no_tbox, "")
            surname_lbl.Text = "Surname:" & case_ds.Tables(0).Rows(0).Item(2)
            Dim csid As Integer = case_ds.Tables(0).Rows(0).Item(1)
            cl_ref_lbl.Text = "Client Ref:" & case_ds.Tables(0).Rows(0).Item(0)
            param2 = "select clientID, branchID from clientScheme where _rowid = " & csid
            Dim csid_ds As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                cl_name_tbox.Text = ""
            Else
                param2 = "select name from Client where _rowid = " & csid_ds.Tables(0).Rows(0).Item(0)
                Dim cl_ds As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 1 Then
                    cl_name_tbox.Text = cl_ds.Tables(0).Rows(0).Item(0)
                End If
            End If
            Dim linkID As Integer
            Try
                linkID = case_ds.Tables(0).Rows(0).Item(3)
            Catch ex As Exception
                linkID = 0
            End Try

            If linkID > 0 Then
                param2 = "select count(*) from Debtor where linkID = " & linkID & _
                        " and _rowID <> " & case_no_tbox.Text & _
                        " and status_open_closed = 'O'"
                Dim link2_ds As DataSet = get_dataset("onestep", param2)
                openLink_lbl.Text = "Open Links:" & link2_ds.Tables(0).Rows(0).Item(0)
            Else
                openLink_lbl.Text = "Open Links:0"
            End If
            'give warning if case number already has an enquiry
            param2 = "select enq_no from Enquiries where enq_case_no = " & case_no_tbox.Text
            Dim enq2_ds As DataSet = get_dataset("complaints", param2)
            If enq2_ds.Tables(0).Rows.Count > 0 Then
                MsgBox("Enquiry already exists for this case")
                
            Else
                'check for linked case enquiry
                If linkID > 0 Then
                    param2 = "select _rowid from Debtor where linkID = " & linkID
                    Dim link_ds As DataSet = get_dataset("onestep", param2)
                    For link_idx = 0 To no_of_rows - 1
                        Dim link_debtor As Integer = link_ds.Tables(0).Rows(link_idx).Item(0)
                        param2 = "select enq_no from Enquiries where enq_case_no = " & link_debtor
                        Dim enq3_ds As DataSet = get_dataset("complaints", param2)
                        If enq3_ds.Tables(0).Rows.Count > 0 Then
                            MsgBox("Enquiry already exists for a link on this case - " & link_debtor)
                            Exit For
                        End If
                    Next
                End If
            End If
        End If
    End Sub

    Private Sub recpt_type_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recpt_type_cbox.SelectedIndexChanged
        ErrorProvider1.SetError(recpt_type_cbox, "")
    End Sub

    Private Sub nature_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nature_cbox.SelectedIndexChanged
        ErrorProvider1.SetError(nature_cbox, "")
    End Sub

    Private Sub recvd_from_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recvd_from_cbox.SelectedIndexChanged
        ErrorProvider1.SetError(recvd_from_cbox, "")
        'If recvd_from_cbox.SelectedIndex = 0 Then
        '    contact_lbl.Text = "Advice Agency:"
        '    agency_cbox.Enabled = True
        '    agency_cbox.Items.Clear()
        '    cabbtn.Enabled = False
        '    agency_cbox.SelectedIndex = -1
        '    param2 = "select adv_no, adv_name from Advice_agencies order by adv_name"
        '    Dim adv_ds As DataSet = get_dataset("complaints", param2)
        '    For idx = 0 To no_of_rows - 1
        '        agency_cbox.Items.Add(adv_ds.Tables(0).Rows(idx).Item(1))
        '    Next
        'ElseIf recvd_from_cbox.SelectedIndex = 1 Then
        'contact_lbl.Text = "CAB:"
        'agency_cbox.Enabled = True
        'agency_cbox.Items.Clear()
        'cabbtn.Enabled = True
        'agency_cbox.SelectedIndex = -1
        'param2 = "select cab_no, cab_name from CAB_names order by cab_name"
        'Dim cab_ds As DataSet = get_dataset("complaints", param2)
        'For idx = 0 To no_of_rows - 1
        '    agency_cbox.Items.Add(cab_ds.Tables(0).Rows(idx).Item(1))
        'Next
        'Else
        'contact_lbl.Text = ""
        'agency_cbox.Enabled = False
        'cabbtn.Enabled = False
        'end if
    End Sub

    Private Sub details_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles details_tbox.TextChanged
        ErrorProvider1.SetError(details_tbox, "")
    End Sub

    Private Sub cabbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cabbtn.Click
        Dim new_cab_name As String = Trim(InputBox("Enter new Contact name", "Contact entry"))
        If new_cab_name.Length = 0 Then
            Exit Sub
        End If
        'see if cab name already exists
        param2 = "select count(cab_no) from cab_names where cab_name = '" & new_cab_name & "'"
        Dim cab_ds2 As DataSet = get_dataset("complaints", param2)
        If cab_ds2.Tables(0).Rows(0).Item(0) > 0 Then
            MsgBox("This cab name already exists")
            Exit Sub
        End If
        Try
            upd_txt = "insert into CAB_names (cab_name) values ('" & new_cab_name & "')"
            update_sql(upd_txt)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        contact_cbox.Items.Clear()
        contact_cbox.SelectedIndex = -1
        param2 = "select cab_no, cab_name from CAB_names order by cab_name"
        Dim cab_ds As DataSet = get_dataset("complaints", param2)
        For idx = 0 To no_of_rows - 1
            contact_cbox.Items.Add(cab_ds.Tables(0).Rows(idx).Item(1))
        Next
    End Sub

    Private Sub agency_cbox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles contact_cbox.SelectedIndexChanged

    End Sub
End Class
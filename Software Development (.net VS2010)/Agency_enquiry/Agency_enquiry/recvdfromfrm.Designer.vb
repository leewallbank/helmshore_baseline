﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class recvdfromfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.updrecvd_from_dg = New System.Windows.Forms.DataGridView()
        Me.recvd_from_code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.recvd_from_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.updrecvd_from_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'updrecvd_from_dg
        '
        Me.updrecvd_from_dg.AllowUserToDeleteRows = False
        Me.updrecvd_from_dg.AllowUserToOrderColumns = True
        Me.updrecvd_from_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.updrecvd_from_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.recvd_from_code, Me.recvd_from_name})
        Me.updrecvd_from_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.updrecvd_from_dg.Location = New System.Drawing.Point(0, 0)
        Me.updrecvd_from_dg.Name = "updrecvd_from_dg"
        Me.updrecvd_from_dg.Size = New System.Drawing.Size(223, 348)
        Me.updrecvd_from_dg.TabIndex = 0
        '
        'recvd_from_code
        '
        Me.recvd_from_code.HeaderText = "Column1"
        Me.recvd_from_code.Name = "recvd_from_code"
        Me.recvd_from_code.Visible = False
        '
        'recvd_from_name
        '
        Me.recvd_from_name.HeaderText = "Received From"
        Me.recvd_from_name.Name = "recvd_from_name"
        Me.recvd_from_name.Width = 150
        '
        'recvdfromfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(223, 348)
        Me.Controls.Add(Me.updrecvd_from_dg)
        Me.Name = "recvdfromfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Received from "
        CType(Me.updrecvd_from_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents updrecvd_from_dg As System.Windows.Forms.DataGridView
    Friend WithEvents recvd_from_code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents recvd_from_name As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class statusfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.stat_dg = New System.Windows.Forms.DataGridView()
        Me.stat_code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.stat_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.stat_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'stat_dg
        '
        Me.stat_dg.AllowUserToDeleteRows = False
        Me.stat_dg.AllowUserToOrderColumns = True
        Me.stat_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.stat_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.stat_code, Me.stat_name})
        Me.stat_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.stat_dg.Location = New System.Drawing.Point(0, 0)
        Me.stat_dg.Name = "stat_dg"
        Me.stat_dg.Size = New System.Drawing.Size(281, 326)
        Me.stat_dg.TabIndex = 0
        '
        'stat_code
        '
        Me.stat_code.HeaderText = "Column1"
        Me.stat_code.Name = "stat_code"
        Me.stat_code.Visible = False
        '
        'stat_name
        '
        Me.stat_name.HeaderText = "Status"
        Me.stat_name.Name = "stat_name"
        Me.stat_name.Width = 150
        '
        'statusfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(281, 326)
        Me.Controls.Add(Me.stat_dg)
        Me.Name = "statusfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Status"
        CType(Me.stat_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents stat_dg As System.Windows.Forms.DataGridView
    Friend WithEvents stat_code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents stat_name As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

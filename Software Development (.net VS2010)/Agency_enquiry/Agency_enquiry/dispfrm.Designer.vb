﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class dispfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dispbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.contact_rbtn = New System.Windows.Forms.RadioButton()
        Me.branch_rbtn = New System.Windows.Forms.RadioButton()
        Me.cl_rbtn = New System.Windows.Forms.RadioButton()
        Me.case_rbtn = New System.Windows.Forms.RadioButton()
        Me.all_rbtn = New System.Windows.Forms.RadioButton()
        Me.selectbtn = New System.Windows.Forms.Button()
        Me.recpt_cbox = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.recvd_from_cbox = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.nature_cbox = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.status_cbox = New System.Windows.Forms.ComboBox()
        Me.clearbtn = New System.Windows.Forms.Button()
        Me.alloc_to_cbox = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.recvd_dtp = New System.Windows.Forms.DateTimePicker()
        Me.recvd_cbox = New System.Windows.Forms.CheckBox()
        Me.comp_cbox = New System.Windows.Forms.CheckBox()
        Me.comp_dtp = New System.Windows.Forms.DateTimePicker()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dispbtn
        '
        Me.dispbtn.Location = New System.Drawing.Point(54, 25)
        Me.dispbtn.Name = "dispbtn"
        Me.dispbtn.Size = New System.Drawing.Size(105, 23)
        Me.dispbtn.TabIndex = 0
        Me.dispbtn.Text = "Display Enquiry"
        Me.dispbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(370, 444)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 9
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.contact_rbtn)
        Me.GroupBox1.Controls.Add(Me.branch_rbtn)
        Me.GroupBox1.Controls.Add(Me.cl_rbtn)
        Me.GroupBox1.Controls.Add(Me.case_rbtn)
        Me.GroupBox1.Controls.Add(Me.all_rbtn)
        Me.GroupBox1.Location = New System.Drawing.Point(33, 79)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(158, 177)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "OR select criteria as required"
        '
        'contact_rbtn
        '
        Me.contact_rbtn.AutoSize = True
        Me.contact_rbtn.Location = New System.Drawing.Point(6, 123)
        Me.contact_rbtn.Name = "contact_rbtn"
        Me.contact_rbtn.Size = New System.Drawing.Size(62, 17)
        Me.contact_rbtn.TabIndex = 3
        Me.contact_rbtn.Text = "Contact"
        Me.contact_rbtn.UseVisualStyleBackColor = True
        '
        'branch_rbtn
        '
        Me.branch_rbtn.AutoSize = True
        Me.branch_rbtn.Location = New System.Drawing.Point(6, 100)
        Me.branch_rbtn.Name = "branch_rbtn"
        Me.branch_rbtn.Size = New System.Drawing.Size(59, 17)
        Me.branch_rbtn.TabIndex = 4
        Me.branch_rbtn.Text = "Branch"
        Me.branch_rbtn.UseVisualStyleBackColor = True
        '
        'cl_rbtn
        '
        Me.cl_rbtn.AutoSize = True
        Me.cl_rbtn.Location = New System.Drawing.Point(6, 77)
        Me.cl_rbtn.Name = "cl_rbtn"
        Me.cl_rbtn.Size = New System.Drawing.Size(51, 17)
        Me.cl_rbtn.TabIndex = 5
        Me.cl_rbtn.Text = "Client"
        Me.cl_rbtn.UseVisualStyleBackColor = True
        '
        'case_rbtn
        '
        Me.case_rbtn.AutoSize = True
        Me.case_rbtn.Location = New System.Drawing.Point(6, 54)
        Me.case_rbtn.Name = "case_rbtn"
        Me.case_rbtn.Size = New System.Drawing.Size(66, 17)
        Me.case_rbtn.TabIndex = 6
        Me.case_rbtn.Text = "Case No"
        Me.case_rbtn.UseVisualStyleBackColor = True
        '
        'all_rbtn
        '
        Me.all_rbtn.AutoSize = True
        Me.all_rbtn.Checked = True
        Me.all_rbtn.Location = New System.Drawing.Point(6, 31)
        Me.all_rbtn.Name = "all_rbtn"
        Me.all_rbtn.Size = New System.Drawing.Size(44, 17)
        Me.all_rbtn.TabIndex = 7
        Me.all_rbtn.TabStop = True
        Me.all_rbtn.Text = "ALL"
        Me.all_rbtn.UseVisualStyleBackColor = True
        '
        'selectbtn
        '
        Me.selectbtn.Location = New System.Drawing.Point(33, 444)
        Me.selectbtn.Name = "selectbtn"
        Me.selectbtn.Size = New System.Drawing.Size(106, 23)
        Me.selectbtn.TabIndex = 8
        Me.selectbtn.Text = "Select Enquiries"
        Me.selectbtn.UseVisualStyleBackColor = True
        '
        'recpt_cbox
        '
        Me.recpt_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.recpt_cbox.FormattingEnabled = True
        Me.recpt_cbox.Location = New System.Drawing.Point(286, 73)
        Me.recpt_cbox.Name = "recpt_cbox"
        Me.recpt_cbox.Size = New System.Drawing.Size(159, 21)
        Me.recpt_cbox.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(329, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(71, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Receipt Type"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(329, 114)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Receipt From"
        '
        'recvd_from_cbox
        '
        Me.recvd_from_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.recvd_from_cbox.FormattingEnabled = True
        Me.recvd_from_cbox.Location = New System.Drawing.Point(286, 130)
        Me.recvd_from_cbox.Name = "recvd_from_cbox"
        Me.recvd_from_cbox.Size = New System.Drawing.Size(159, 21)
        Me.recvd_from_cbox.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(312, 183)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(88, 13)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Nature of enquiry"
        '
        'nature_cbox
        '
        Me.nature_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.nature_cbox.FormattingEnabled = True
        Me.nature_cbox.Location = New System.Drawing.Point(286, 199)
        Me.nature_cbox.Name = "nature_cbox"
        Me.nature_cbox.Size = New System.Drawing.Size(159, 21)
        Me.nature_cbox.TabIndex = 5
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(329, 243)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(37, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Status"
        '
        'status_cbox
        '
        Me.status_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.status_cbox.FormattingEnabled = True
        Me.status_cbox.Location = New System.Drawing.Point(286, 259)
        Me.status_cbox.Name = "status_cbox"
        Me.status_cbox.Size = New System.Drawing.Size(159, 21)
        Me.status_cbox.TabIndex = 6
        '
        'clearbtn
        '
        Me.clearbtn.Location = New System.Drawing.Point(332, 12)
        Me.clearbtn.Name = "clearbtn"
        Me.clearbtn.Size = New System.Drawing.Size(95, 23)
        Me.clearbtn.TabIndex = 1
        Me.clearbtn.Text = "Clear selections"
        Me.clearbtn.UseVisualStyleBackColor = True
        '
        'alloc_to_cbox
        '
        Me.alloc_to_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.alloc_to_cbox.FormattingEnabled = True
        Me.alloc_to_cbox.Location = New System.Drawing.Point(33, 308)
        Me.alloc_to_cbox.Name = "alloc_to_cbox"
        Me.alloc_to_cbox.Size = New System.Drawing.Size(126, 21)
        Me.alloc_to_cbox.TabIndex = 7
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(51, 280)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(63, 13)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Allocated to"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(186, 444)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 14
        '
        'recvd_dtp
        '
        Me.recvd_dtp.Location = New System.Drawing.Point(297, 331)
        Me.recvd_dtp.Name = "recvd_dtp"
        Me.recvd_dtp.Size = New System.Drawing.Size(130, 20)
        Me.recvd_dtp.TabIndex = 15
        Me.recvd_dtp.Visible = False
        '
        'recvd_cbox
        '
        Me.recvd_cbox.AutoSize = True
        Me.recvd_cbox.Location = New System.Drawing.Point(315, 308)
        Me.recvd_cbox.Name = "recvd_cbox"
        Me.recvd_cbox.Size = New System.Drawing.Size(115, 17)
        Me.recvd_cbox.TabIndex = 16
        Me.recvd_cbox.Text = "Received in month"
        Me.recvd_cbox.UseVisualStyleBackColor = True
        '
        'comp_cbox
        '
        Me.comp_cbox.AutoSize = True
        Me.comp_cbox.Location = New System.Drawing.Point(315, 367)
        Me.comp_cbox.Name = "comp_cbox"
        Me.comp_cbox.Size = New System.Drawing.Size(119, 17)
        Me.comp_cbox.TabIndex = 18
        Me.comp_cbox.Text = "Completed in month"
        Me.comp_cbox.UseVisualStyleBackColor = True
        '
        'comp_dtp
        '
        Me.comp_dtp.Location = New System.Drawing.Point(298, 388)
        Me.comp_dtp.Name = "comp_dtp"
        Me.comp_dtp.Size = New System.Drawing.Size(130, 20)
        Me.comp_dtp.TabIndex = 17
        Me.comp_dtp.Visible = False
        '
        'dispfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(471, 498)
        Me.Controls.Add(Me.comp_cbox)
        Me.Controls.Add(Me.comp_dtp)
        Me.Controls.Add(Me.recvd_cbox)
        Me.Controls.Add(Me.recvd_dtp)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.alloc_to_cbox)
        Me.Controls.Add(Me.clearbtn)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.status_cbox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.nature_cbox)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.recvd_from_cbox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.recpt_cbox)
        Me.Controls.Add(Me.selectbtn)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.dispbtn)
        Me.Name = "dispfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "dispfrm"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dispbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents contact_rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents branch_rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents cl_rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents case_rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents all_rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents selectbtn As System.Windows.Forms.Button
    Friend WithEvents recpt_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents recvd_from_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents nature_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents status_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents clearbtn As System.Windows.Forms.Button
    Friend WithEvents alloc_to_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents recvd_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents recvd_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents comp_cbox As System.Windows.Forms.CheckBox
    Friend WithEvents comp_dtp As System.Windows.Forms.DateTimePicker
End Class

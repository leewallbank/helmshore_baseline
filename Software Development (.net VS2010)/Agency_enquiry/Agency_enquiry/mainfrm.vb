﻿
Public Class mainfrm

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        adminbtn.Enabled = wo_admin
    End Sub

    Private Sub addbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles addbtn.Click
        addfrm.ShowDialog()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        DisconnectDb()
        Me.Close()
    End Sub

    Private Sub dispbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dispbtn.Click
        dispfrm.ShowDialog()
    End Sub

    Private Sub updbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updbtn.Click
        Try
            search_enq_no = InputBox("Enter enquiry number for Update", "Enquiry Number Entry")
        Catch ex As Exception
            MsgBox("Invalid enquiry No")
            Exit Sub
        End Try
        If search_enq_no < 8373 Then
            MsgBox("Invalid enquiry No")
            Exit Sub
        End If
        param2 = "select enq_no from Enquiries where enq_no = " & search_enq_no
        Dim enq_ds As DataSet = get_dataset("complaints", param2)
        If no_of_rows = 0 Then
            MsgBox("Enquiry number " & search_enq_no & " does not exist")
            Exit Sub
        End If
        updatefrm.ShowDialog()
    End Sub

    Private Sub logbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles logbtn.Click
        Try
            search_enq_no = InputBox("Enter enquiry No", "Enquiry No entry")
        Catch ex As Exception
            MsgBox("Invalid enquiry number")
            Exit Sub
        End Try
        If search_enq_no < 8373 Then
            MsgBox("Invalid enquiry number")
            Exit Sub
        End If
        logfrm.ShowDialog()
    End Sub

    Private Sub adminbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles adminbtn.Click
        adminfrm.ShowDialog()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'upd_txt = "insert into Enquiry_action_taken (act_code, act_name) values (" & 12 & ",'Case on hold at clients request'" & ")"
        'update_sql(upd_txt)
        'upd_txt = "insert into Enquiry_branches (branch_code,branch_name) values (" & 6 & ",'Marstons Local Taxation'" & ")"
        'update_sql(upd_txt)
        'upd_txt = "update enquiries set enq_branch_no = 0 where enq_branch_no= -1"
        'upd_txt = "delete from enquiry_receipt_types where type_code >3"
        'upd_txt = "delete from enquiry_recvd_from where recvd_from_code >5"
        'update_sql(upd_txt)
        MsgBox("Done")
        Me.Close()
    End Sub
End Class

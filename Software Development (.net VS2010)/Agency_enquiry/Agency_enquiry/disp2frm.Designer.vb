﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class disp2frm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.enq_dg = New System.Windows.Forms.DataGridView()
        Me.disp_btn = New System.Windows.Forms.Button()
        Me.updbtn = New System.Windows.Forms.Button()
        Me.excelbtn = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.enq_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.recvd_date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.case_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.branch_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.status = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.alloc_to = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.recpt_type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nature = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.recvd_from = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hold_expires = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.enq_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'enq_dg
        '
        Me.enq_dg.AllowUserToAddRows = False
        Me.enq_dg.AllowUserToDeleteRows = False
        Me.enq_dg.AllowUserToOrderColumns = True
        Me.enq_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.enq_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.enq_no, Me.recvd_date, Me.case_no, Me.branch_name, Me.status, Me.alloc_to, Me.recpt_type, Me.nature, Me.recvd_from, Me.hold_expires})
        Me.enq_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.enq_dg.Location = New System.Drawing.Point(0, 0)
        Me.enq_dg.Name = "enq_dg"
        Me.enq_dg.ReadOnly = True
        Me.enq_dg.Size = New System.Drawing.Size(1000, 496)
        Me.enq_dg.TabIndex = 0
        '
        'disp_btn
        '
        Me.disp_btn.Location = New System.Drawing.Point(681, 267)
        Me.disp_btn.Name = "disp_btn"
        Me.disp_btn.Size = New System.Drawing.Size(99, 23)
        Me.disp_btn.TabIndex = 1
        Me.disp_btn.Text = "Display Enquiry"
        Me.disp_btn.UseVisualStyleBackColor = True
        '
        'updbtn
        '
        Me.updbtn.Location = New System.Drawing.Point(681, 315)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(99, 23)
        Me.updbtn.TabIndex = 2
        Me.updbtn.Text = "Update Enquiry"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'excelbtn
        '
        Me.excelbtn.Location = New System.Drawing.Point(681, 361)
        Me.excelbtn.Name = "excelbtn"
        Me.excelbtn.Size = New System.Drawing.Size(99, 23)
        Me.excelbtn.TabIndex = 3
        Me.excelbtn.Text = "Write to excel"
        Me.excelbtn.UseVisualStyleBackColor = True
        '
        'enq_no
        '
        Me.enq_no.HeaderText = "Enq No"
        Me.enq_no.Name = "enq_no"
        Me.enq_no.ReadOnly = True
        Me.enq_no.Width = 50
        '
        'recvd_date
        '
        Me.recvd_date.HeaderText = "Recvd date"
        Me.recvd_date.Name = "recvd_date"
        Me.recvd_date.ReadOnly = True
        '
        'case_no
        '
        Me.case_no.HeaderText = "Case No"
        Me.case_no.Name = "case_no"
        Me.case_no.ReadOnly = True
        '
        'branch_name
        '
        Me.branch_name.HeaderText = "Branch"
        Me.branch_name.Name = "branch_name"
        Me.branch_name.ReadOnly = True
        '
        'status
        '
        Me.status.HeaderText = "Status"
        Me.status.Name = "status"
        Me.status.ReadOnly = True
        '
        'alloc_to
        '
        Me.alloc_to.HeaderText = "Allocated To"
        Me.alloc_to.Name = "alloc_to"
        Me.alloc_to.ReadOnly = True
        '
        'recpt_type
        '
        Me.recpt_type.HeaderText = "Receipt Type"
        Me.recpt_type.Name = "recpt_type"
        Me.recpt_type.ReadOnly = True
        '
        'nature
        '
        Me.nature.HeaderText = "Nature"
        Me.nature.Name = "nature"
        Me.nature.ReadOnly = True
        '
        'recvd_from
        '
        Me.recvd_from.HeaderText = "Recvd from"
        Me.recvd_from.Name = "recvd_from"
        Me.recvd_from.ReadOnly = True
        '
        'hold_expires
        '
        Me.hold_expires.HeaderText = "Hold Expires"
        Me.hold_expires.Name = "hold_expires"
        Me.hold_expires.ReadOnly = True
        '
        'disp2frm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1000, 496)
        Me.Controls.Add(Me.excelbtn)
        Me.Controls.Add(Me.updbtn)
        Me.Controls.Add(Me.disp_btn)
        Me.Controls.Add(Me.enq_dg)
        Me.Name = "disp2frm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Display Enquiries"
        CType(Me.enq_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents enq_dg As System.Windows.Forms.DataGridView
    Friend WithEvents disp_btn As System.Windows.Forms.Button
    Friend WithEvents updbtn As System.Windows.Forms.Button
    Friend WithEvents excelbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents enq_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents recvd_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents case_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents branch_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents alloc_to As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents recpt_type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nature As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents recvd_from As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hold_expires As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class adminfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.wobtn = New System.Windows.Forms.Button()
        Me.rectypbtn = New System.Windows.Forms.Button()
        Me.recvd_frombtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.statusbtn = New System.Windows.Forms.Button()
        Me.cabbtn = New System.Windows.Forms.Button()
        Me.naturebtn = New System.Windows.Forms.Button()
        Me.reset_passbtn = New System.Windows.Forms.Button()
        Me.delcabbtn = New System.Windows.Forms.Button()
        Me.upd_adv_agencybtn = New System.Windows.Forms.Button()
        Me.ins_adv_agencybtn = New System.Windows.Forms.Button()
        Me.bulkbtn = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'wobtn
        '
        Me.wobtn.Location = New System.Drawing.Point(49, 49)
        Me.wobtn.Name = "wobtn"
        Me.wobtn.Size = New System.Drawing.Size(106, 23)
        Me.wobtn.TabIndex = 0
        Me.wobtn.Text = "&Welfare Officers"
        Me.wobtn.UseVisualStyleBackColor = True
        '
        'rectypbtn
        '
        Me.rectypbtn.Enabled = False
        Me.rectypbtn.Location = New System.Drawing.Point(205, 49)
        Me.rectypbtn.Name = "rectypbtn"
        Me.rectypbtn.Size = New System.Drawing.Size(106, 23)
        Me.rectypbtn.TabIndex = 1
        Me.rectypbtn.Text = "&Receipt Types"
        Me.rectypbtn.UseVisualStyleBackColor = True
        '
        'recvd_frombtn
        '
        Me.recvd_frombtn.Location = New System.Drawing.Point(49, 111)
        Me.recvd_frombtn.Name = "recvd_frombtn"
        Me.recvd_frombtn.Size = New System.Drawing.Size(106, 23)
        Me.recvd_frombtn.TabIndex = 2
        Me.recvd_frombtn.Text = "Received &From"
        Me.recvd_frombtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(278, 439)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 11
        Me.exitbtn.Text = "&Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'statusbtn
        '
        Me.statusbtn.Location = New System.Drawing.Point(49, 312)
        Me.statusbtn.Name = "statusbtn"
        Me.statusbtn.Size = New System.Drawing.Size(106, 23)
        Me.statusbtn.TabIndex = 8
        Me.statusbtn.Text = "&Status"
        Me.statusbtn.UseVisualStyleBackColor = True
        '
        'cabbtn
        '
        Me.cabbtn.Location = New System.Drawing.Point(49, 172)
        Me.cabbtn.Name = "cabbtn"
        Me.cabbtn.Size = New System.Drawing.Size(106, 23)
        Me.cabbtn.TabIndex = 4
        Me.cabbtn.Text = "Update &CAB name"
        Me.cabbtn.UseVisualStyleBackColor = True
        '
        'naturebtn
        '
        Me.naturebtn.Location = New System.Drawing.Point(205, 111)
        Me.naturebtn.Name = "naturebtn"
        Me.naturebtn.Size = New System.Drawing.Size(106, 23)
        Me.naturebtn.TabIndex = 3
        Me.naturebtn.Text = "&Nature of Enquiry"
        Me.naturebtn.UseVisualStyleBackColor = True
        '
        'reset_passbtn
        '
        Me.reset_passbtn.Location = New System.Drawing.Point(205, 312)
        Me.reset_passbtn.Name = "reset_passbtn"
        Me.reset_passbtn.Size = New System.Drawing.Size(103, 23)
        Me.reset_passbtn.TabIndex = 9
        Me.reset_passbtn.Text = "&Password Reset"
        Me.reset_passbtn.UseVisualStyleBackColor = True
        '
        'delcabbtn
        '
        Me.delcabbtn.Location = New System.Drawing.Point(205, 172)
        Me.delcabbtn.Name = "delcabbtn"
        Me.delcabbtn.Size = New System.Drawing.Size(103, 23)
        Me.delcabbtn.TabIndex = 5
        Me.delcabbtn.Text = "&Delete CAB name"
        Me.delcabbtn.UseVisualStyleBackColor = True
        '
        'upd_adv_agencybtn
        '
        Me.upd_adv_agencybtn.Location = New System.Drawing.Point(30, 241)
        Me.upd_adv_agencybtn.Name = "upd_adv_agencybtn"
        Me.upd_adv_agencybtn.Size = New System.Drawing.Size(125, 23)
        Me.upd_adv_agencybtn.TabIndex = 6
        Me.upd_adv_agencybtn.Text = "&Update Advice Agency"
        Me.upd_adv_agencybtn.UseVisualStyleBackColor = True
        '
        'ins_adv_agencybtn
        '
        Me.ins_adv_agencybtn.Location = New System.Drawing.Point(205, 241)
        Me.ins_adv_agencybtn.Name = "ins_adv_agencybtn"
        Me.ins_adv_agencybtn.Size = New System.Drawing.Size(117, 23)
        Me.ins_adv_agencybtn.TabIndex = 7
        Me.ins_adv_agencybtn.Text = "&Insert Advice Agency"
        Me.ins_adv_agencybtn.UseVisualStyleBackColor = True
        '
        'bulkbtn
        '
        Me.bulkbtn.Location = New System.Drawing.Point(119, 379)
        Me.bulkbtn.Name = "bulkbtn"
        Me.bulkbtn.Size = New System.Drawing.Size(99, 23)
        Me.bulkbtn.TabIndex = 10
        Me.bulkbtn.Text = "&Bulk Completion"
        Me.bulkbtn.UseVisualStyleBackColor = True
        '
        'adminfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(365, 494)
        Me.Controls.Add(Me.bulkbtn)
        Me.Controls.Add(Me.ins_adv_agencybtn)
        Me.Controls.Add(Me.upd_adv_agencybtn)
        Me.Controls.Add(Me.delcabbtn)
        Me.Controls.Add(Me.reset_passbtn)
        Me.Controls.Add(Me.statusbtn)
        Me.Controls.Add(Me.cabbtn)
        Me.Controls.Add(Me.naturebtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.recvd_frombtn)
        Me.Controls.Add(Me.rectypbtn)
        Me.Controls.Add(Me.wobtn)
        Me.Name = "adminfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Maintain master tables/reset password"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents wobtn As System.Windows.Forms.Button
    Friend WithEvents rectypbtn As System.Windows.Forms.Button
    Friend WithEvents recvd_frombtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents statusbtn As System.Windows.Forms.Button
    Friend WithEvents cabbtn As System.Windows.Forms.Button
    Friend WithEvents naturebtn As System.Windows.Forms.Button
    Friend WithEvents reset_passbtn As System.Windows.Forms.Button
    Friend WithEvents delcabbtn As System.Windows.Forms.Button
    Friend WithEvents upd_adv_agencybtn As System.Windows.Forms.Button
    Friend WithEvents ins_adv_agencybtn As System.Windows.Forms.Button
    Friend WithEvents bulkbtn As System.Windows.Forms.Button
End Class

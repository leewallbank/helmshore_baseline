﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class resetpassfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.wo_cbox = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.resetbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'wo_cbox
        '
        Me.wo_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.wo_cbox.FormattingEnabled = True
        Me.wo_cbox.Location = New System.Drawing.Point(55, 84)
        Me.wo_cbox.Name = "wo_cbox"
        Me.wo_cbox.Size = New System.Drawing.Size(142, 21)
        Me.wo_cbox.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(88, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Select officer"
        '
        'resetbtn
        '
        Me.resetbtn.Location = New System.Drawing.Point(55, 147)
        Me.resetbtn.Name = "resetbtn"
        Me.resetbtn.Size = New System.Drawing.Size(111, 23)
        Me.resetbtn.TabIndex = 1
        Me.resetbtn.Text = "&Reset Password"
        Me.resetbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(155, 242)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(125, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "&Exit without update"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'resetpassfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 297)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.resetbtn)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.wo_cbox)
        Me.Name = "resetpassfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reset Password"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents wo_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents resetbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
End Class

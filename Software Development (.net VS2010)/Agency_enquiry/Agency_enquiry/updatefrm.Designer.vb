﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class updatefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Label3 As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Comp_dateLabel As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim Label14 As System.Windows.Forms.Label
        Me.enq_no_tbox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.nature_cbox = New System.Windows.Forms.ComboBox()
        Me.recpt_cbox = New System.Windows.Forms.ComboBox()
        Me.enq_Date_dtp = New System.Windows.Forms.DateTimePicker()
        Me.no_savebtn = New System.Windows.Forms.Button()
        Me.savebtn = New System.Windows.Forms.Button()
        Me.entered_by_tbox = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.recvd_from_cbox = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.details_tbox = New System.Windows.Forms.TextBox()
        Me.upd_cl_ref_lbl = New System.Windows.Forms.Label()
        Me.upd_surname_lbl = New System.Windows.Forms.Label()
        Me.upd_cl_name_tbox = New System.Windows.Forms.TextBox()
        Me.case_no_tbox = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.resp_tbox = New System.Windows.Forms.TextBox()
        Me.contact_cbox = New System.Windows.Forms.ComboBox()
        Me.contact_lbl = New System.Windows.Forms.Label()
        Me.status_cbox = New System.Windows.Forms.ComboBox()
        Me.hold_dtpicker = New System.Windows.Forms.DateTimePicker()
        Me.hold_lbl = New System.Windows.Forms.Label()
        Me.hold_reason_tbox = New System.Windows.Forms.TextBox()
        Me.hold_reason_lbl = New System.Windows.Forms.Label()
        Me.comp_by_lbl = New System.Windows.Forms.Label()
        Me.comp_by_cbox = New System.Windows.Forms.ComboBox()
        Me.comp_date_lbl = New System.Windows.Forms.Label()
        Me.comp_date_dtp = New System.Windows.Forms.DateTimePicker()
        Me.contacts_ud = New System.Windows.Forms.NumericUpDown()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.actions_cbox = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.arr_broken_chk = New System.Windows.Forms.CheckBox()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cabbtn = New System.Windows.Forms.Button()
        Me.upd_branch_cbox = New System.Windows.Forms.ComboBox()
        Me.upd_alloc_to_cbox = New System.Windows.Forms.ComboBox()
        Me.openLink_lbl = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Comp_dateLabel = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        Label14 = New System.Windows.Forms.Label()
        CType(Me.contacts_ud, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Location = New System.Drawing.Point(226, 105)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(92, 13)
        Label3.TabIndex = 21
        Label3.Text = "Nature of Enquiry:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Location = New System.Drawing.Point(62, 104)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(74, 13)
        Label2.TabIndex = 19
        Label2.Text = "Receipt Type:"
        '
        'Comp_dateLabel
        '
        Comp_dateLabel.AutoSize = True
        Comp_dateLabel.Location = New System.Drawing.Point(168, 36)
        Comp_dateLabel.Name = "Comp_dateLabel"
        Comp_dateLabel.Size = New System.Drawing.Size(82, 13)
        Comp_dateLabel.TabIndex = 17
        Comp_dateLabel.Text = "Date Received:"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Location = New System.Drawing.Point(396, 105)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(79, 13)
        Label5.TabIndex = 28
        Label5.Text = "Received from:"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Location = New System.Drawing.Point(252, 169)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(36, 13)
        Label7.TabIndex = 41
        Label7.Text = "Client:"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Location = New System.Drawing.Point(466, 169)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(44, 13)
        Label6.TabIndex = 40
        Label6.Text = "Branch:"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Location = New System.Drawing.Point(41, 169)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(51, 13)
        Label9.TabIndex = 39
        Label9.Text = "Case No:"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Location = New System.Drawing.Point(490, 36)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(40, 13)
        Label11.TabIndex = 52
        Label11.Text = "Status:"
        '
        'Label14
        '
        Label14.AutoSize = True
        Label14.Location = New System.Drawing.Point(673, 36)
        Label14.Name = "Label14"
        Label14.Size = New System.Drawing.Size(66, 13)
        Label14.TabIndex = 67
        Label14.Text = "Allocated to:"
        '
        'enq_no_tbox
        '
        Me.enq_no_tbox.Location = New System.Drawing.Point(44, 52)
        Me.enq_no_tbox.Name = "enq_no_tbox"
        Me.enq_no_tbox.ReadOnly = True
        Me.enq_no_tbox.Size = New System.Drawing.Size(100, 20)
        Me.enq_no_tbox.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(74, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Enquiry No:"
        '
        'nature_cbox
        '
        Me.nature_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.nature_cbox.FormattingEnabled = True
        Me.nature_cbox.Location = New System.Drawing.Point(196, 121)
        Me.nature_cbox.Name = "nature_cbox"
        Me.nature_cbox.Size = New System.Drawing.Size(134, 21)
        Me.nature_cbox.TabIndex = 6
        '
        'recpt_cbox
        '
        Me.recpt_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.recpt_cbox.FormattingEnabled = True
        Me.recpt_cbox.Location = New System.Drawing.Point(43, 119)
        Me.recpt_cbox.Name = "recpt_cbox"
        Me.recpt_cbox.Size = New System.Drawing.Size(121, 21)
        Me.recpt_cbox.TabIndex = 5
        '
        'enq_Date_dtp
        '
        Me.enq_Date_dtp.Location = New System.Drawing.Point(155, 51)
        Me.enq_Date_dtp.Name = "enq_Date_dtp"
        Me.enq_Date_dtp.Size = New System.Drawing.Size(131, 20)
        Me.enq_Date_dtp.TabIndex = 1
        '
        'no_savebtn
        '
        Me.no_savebtn.Location = New System.Drawing.Point(33, 498)
        Me.no_savebtn.Name = "no_savebtn"
        Me.no_savebtn.Size = New System.Drawing.Size(147, 23)
        Me.no_savebtn.TabIndex = 22
        Me.no_savebtn.Text = "DO NOT SAVE and EXIT"
        Me.no_savebtn.UseVisualStyleBackColor = True
        '
        'savebtn
        '
        Me.savebtn.Location = New System.Drawing.Point(712, 498)
        Me.savebtn.Name = "savebtn"
        Me.savebtn.Size = New System.Drawing.Size(109, 23)
        Me.savebtn.TabIndex = 24
        Me.savebtn.Text = "SAVE and EXIT"
        Me.savebtn.UseVisualStyleBackColor = True
        '
        'entered_by_tbox
        '
        Me.entered_by_tbox.Location = New System.Drawing.Point(317, 52)
        Me.entered_by_tbox.Name = "entered_by_tbox"
        Me.entered_by_tbox.ReadOnly = True
        Me.entered_by_tbox.Size = New System.Drawing.Size(114, 20)
        Me.entered_by_tbox.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(345, 36)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 13)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "Entered By:"
        '
        'recvd_from_cbox
        '
        Me.recvd_from_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.recvd_from_cbox.FormattingEnabled = True
        Me.recvd_from_cbox.Location = New System.Drawing.Point(361, 120)
        Me.recvd_from_cbox.Name = "recvd_from_cbox"
        Me.recvd_from_cbox.Size = New System.Drawing.Size(172, 21)
        Me.recvd_from_cbox.TabIndex = 7
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(153, 233)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(42, 13)
        Me.Label8.TabIndex = 31
        Me.Label8.Text = "Details:"
        '
        'details_tbox
        '
        Me.details_tbox.Location = New System.Drawing.Point(22, 249)
        Me.details_tbox.Multiline = True
        Me.details_tbox.Name = "details_tbox"
        Me.details_tbox.Size = New System.Drawing.Size(354, 93)
        Me.details_tbox.TabIndex = 14
        '
        'upd_cl_ref_lbl
        '
        Me.upd_cl_ref_lbl.AutoSize = True
        Me.upd_cl_ref_lbl.Location = New System.Drawing.Point(24, 208)
        Me.upd_cl_ref_lbl.Name = "upd_cl_ref_lbl"
        Me.upd_cl_ref_lbl.Size = New System.Drawing.Size(56, 13)
        Me.upd_cl_ref_lbl.TabIndex = 46
        Me.upd_cl_ref_lbl.Text = "Client Ref:"
        '
        'upd_surname_lbl
        '
        Me.upd_surname_lbl.AutoSize = True
        Me.upd_surname_lbl.Location = New System.Drawing.Point(153, 208)
        Me.upd_surname_lbl.Name = "upd_surname_lbl"
        Me.upd_surname_lbl.Size = New System.Drawing.Size(52, 13)
        Me.upd_surname_lbl.TabIndex = 45
        Me.upd_surname_lbl.Text = "Surname:"
        '
        'upd_cl_name_tbox
        '
        Me.upd_cl_name_tbox.Location = New System.Drawing.Point(153, 185)
        Me.upd_cl_name_tbox.Name = "upd_cl_name_tbox"
        Me.upd_cl_name_tbox.ReadOnly = True
        Me.upd_cl_name_tbox.Size = New System.Drawing.Size(212, 20)
        Me.upd_cl_name_tbox.TabIndex = 11
        '
        'case_no_tbox
        '
        Me.case_no_tbox.Location = New System.Drawing.Point(24, 185)
        Me.case_no_tbox.Name = "case_no_tbox"
        Me.case_no_tbox.Size = New System.Drawing.Size(100, 20)
        Me.case_no_tbox.TabIndex = 10
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(539, 233)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(58, 13)
        Me.Label10.TabIndex = 48
        Me.Label10.Text = "Response:"
        '
        'resp_tbox
        '
        Me.resp_tbox.Location = New System.Drawing.Point(408, 249)
        Me.resp_tbox.Multiline = True
        Me.resp_tbox.Name = "resp_tbox"
        Me.resp_tbox.Size = New System.Drawing.Size(354, 93)
        Me.resp_tbox.TabIndex = 15
        '
        'contact_cbox
        '
        Me.contact_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.contact_cbox.FormattingEnabled = True
        Me.contact_cbox.Location = New System.Drawing.Point(546, 119)
        Me.contact_cbox.Name = "contact_cbox"
        Me.contact_cbox.Size = New System.Drawing.Size(151, 21)
        Me.contact_cbox.TabIndex = 8
        '
        'contact_lbl
        '
        Me.contact_lbl.AutoSize = True
        Me.contact_lbl.Location = New System.Drawing.Point(580, 105)
        Me.contact_lbl.Name = "contact_lbl"
        Me.contact_lbl.Size = New System.Drawing.Size(47, 13)
        Me.contact_lbl.TabIndex = 50
        Me.contact_lbl.Text = "Contact:"
        '
        'status_cbox
        '
        Me.status_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.status_cbox.FormattingEnabled = True
        Me.status_cbox.Location = New System.Drawing.Point(460, 52)
        Me.status_cbox.Name = "status_cbox"
        Me.status_cbox.Size = New System.Drawing.Size(121, 21)
        Me.status_cbox.TabIndex = 3
        '
        'hold_dtpicker
        '
        Me.hold_dtpicker.Location = New System.Drawing.Point(33, 392)
        Me.hold_dtpicker.Name = "hold_dtpicker"
        Me.hold_dtpicker.Size = New System.Drawing.Size(132, 20)
        Me.hold_dtpicker.TabIndex = 16
        '
        'hold_lbl
        '
        Me.hold_lbl.AutoSize = True
        Me.hold_lbl.Location = New System.Drawing.Point(74, 374)
        Me.hold_lbl.Name = "hold_lbl"
        Me.hold_lbl.Size = New System.Drawing.Size(32, 13)
        Me.hold_lbl.TabIndex = 54
        Me.hold_lbl.Text = "Hold:"
        '
        'hold_reason_tbox
        '
        Me.hold_reason_tbox.Location = New System.Drawing.Point(172, 392)
        Me.hold_reason_tbox.Name = "hold_reason_tbox"
        Me.hold_reason_tbox.Size = New System.Drawing.Size(171, 20)
        Me.hold_reason_tbox.TabIndex = 17
        '
        'hold_reason_lbl
        '
        Me.hold_reason_lbl.AutoSize = True
        Me.hold_reason_lbl.Location = New System.Drawing.Point(226, 377)
        Me.hold_reason_lbl.Name = "hold_reason_lbl"
        Me.hold_reason_lbl.Size = New System.Drawing.Size(72, 13)
        Me.hold_reason_lbl.TabIndex = 56
        Me.hold_reason_lbl.Text = "Hold Reason:"
        '
        'comp_by_lbl
        '
        Me.comp_by_lbl.AutoSize = True
        Me.comp_by_lbl.Location = New System.Drawing.Point(444, 374)
        Me.comp_by_lbl.Name = "comp_by_lbl"
        Me.comp_by_lbl.Size = New System.Drawing.Size(75, 13)
        Me.comp_by_lbl.TabIndex = 57
        Me.comp_by_lbl.Text = "Completed By:"
        '
        'comp_by_cbox
        '
        Me.comp_by_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.comp_by_cbox.FormattingEnabled = True
        Me.comp_by_cbox.Location = New System.Drawing.Point(423, 392)
        Me.comp_by_cbox.Name = "comp_by_cbox"
        Me.comp_by_cbox.Size = New System.Drawing.Size(121, 21)
        Me.comp_by_cbox.TabIndex = 18
        '
        'comp_date_lbl
        '
        Me.comp_date_lbl.AutoSize = True
        Me.comp_date_lbl.Location = New System.Drawing.Point(444, 430)
        Me.comp_date_lbl.Name = "comp_date_lbl"
        Me.comp_date_lbl.Size = New System.Drawing.Size(86, 13)
        Me.comp_date_lbl.TabIndex = 59
        Me.comp_date_lbl.Text = "Completed Date:"
        '
        'comp_date_dtp
        '
        Me.comp_date_dtp.Location = New System.Drawing.Point(423, 446)
        Me.comp_date_dtp.Name = "comp_date_dtp"
        Me.comp_date_dtp.Size = New System.Drawing.Size(132, 20)
        Me.comp_date_dtp.TabIndex = 19
        '
        'contacts_ud
        '
        Me.contacts_ud.Location = New System.Drawing.Point(615, 186)
        Me.contacts_ud.Name = "contacts_ud"
        Me.contacts_ud.Size = New System.Drawing.Size(82, 20)
        Me.contacts_ud.TabIndex = 13
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(616, 169)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(81, 13)
        Me.Label12.TabIndex = 62
        Me.Label12.Text = "No of Contacts:"
        '
        'actions_cbox
        '
        Me.actions_cbox.FormattingEnabled = True
        Me.actions_cbox.Location = New System.Drawing.Point(583, 393)
        Me.actions_cbox.Name = "actions_cbox"
        Me.actions_cbox.Size = New System.Drawing.Size(209, 21)
        Me.actions_cbox.TabIndex = 20
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(647, 377)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(75, 13)
        Me.Label13.TabIndex = 64
        Me.Label13.Text = "Actions taken:"
        '
        'arr_broken_chk
        '
        Me.arr_broken_chk.AutoSize = True
        Me.arr_broken_chk.Location = New System.Drawing.Point(623, 435)
        Me.arr_broken_chk.Name = "arr_broken_chk"
        Me.arr_broken_chk.Size = New System.Drawing.Size(127, 17)
        Me.arr_broken_chk.TabIndex = 21
        Me.arr_broken_chk.Text = "Arrangements broken"
        Me.arr_broken_chk.UseVisualStyleBackColor = True
        Me.arr_broken_chk.Visible = False
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'cabbtn
        '
        Me.cabbtn.Location = New System.Drawing.Point(728, 117)
        Me.cabbtn.Name = "cabbtn"
        Me.cabbtn.Size = New System.Drawing.Size(75, 23)
        Me.cabbtn.TabIndex = 9
        Me.cabbtn.Text = "Add Contact"
        Me.cabbtn.UseVisualStyleBackColor = True
        '
        'upd_branch_cbox
        '
        Me.upd_branch_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.upd_branch_cbox.FormattingEnabled = True
        Me.upd_branch_cbox.Location = New System.Drawing.Point(423, 184)
        Me.upd_branch_cbox.Name = "upd_branch_cbox"
        Me.upd_branch_cbox.Size = New System.Drawing.Size(145, 21)
        Me.upd_branch_cbox.TabIndex = 12
        '
        'upd_alloc_to_cbox
        '
        Me.upd_alloc_to_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.upd_alloc_to_cbox.FormattingEnabled = True
        Me.upd_alloc_to_cbox.Location = New System.Drawing.Point(615, 52)
        Me.upd_alloc_to_cbox.Name = "upd_alloc_to_cbox"
        Me.upd_alloc_to_cbox.Size = New System.Drawing.Size(160, 21)
        Me.upd_alloc_to_cbox.TabIndex = 4
        '
        'openLink_lbl
        '
        Me.openLink_lbl.AutoSize = True
        Me.openLink_lbl.Location = New System.Drawing.Point(24, 221)
        Me.openLink_lbl.Name = "openLink_lbl"
        Me.openLink_lbl.Size = New System.Drawing.Size(64, 13)
        Me.openLink_lbl.TabIndex = 78
        Me.openLink_lbl.Text = "Open Links:"
        '
        'updatefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(833, 544)
        Me.Controls.Add(Me.openLink_lbl)
        Me.Controls.Add(Label14)
        Me.Controls.Add(Me.upd_alloc_to_cbox)
        Me.Controls.Add(Me.upd_branch_cbox)
        Me.Controls.Add(Me.cabbtn)
        Me.Controls.Add(Me.arr_broken_chk)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.actions_cbox)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.contacts_ud)
        Me.Controls.Add(Me.comp_date_dtp)
        Me.Controls.Add(Me.comp_date_lbl)
        Me.Controls.Add(Me.comp_by_cbox)
        Me.Controls.Add(Me.comp_by_lbl)
        Me.Controls.Add(Me.hold_reason_lbl)
        Me.Controls.Add(Me.hold_reason_tbox)
        Me.Controls.Add(Me.hold_lbl)
        Me.Controls.Add(Me.hold_dtpicker)
        Me.Controls.Add(Label11)
        Me.Controls.Add(Me.status_cbox)
        Me.Controls.Add(Me.contact_lbl)
        Me.Controls.Add(Me.contact_cbox)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.resp_tbox)
        Me.Controls.Add(Me.upd_cl_ref_lbl)
        Me.Controls.Add(Me.upd_surname_lbl)
        Me.Controls.Add(Me.upd_cl_name_tbox)
        Me.Controls.Add(Me.case_no_tbox)
        Me.Controls.Add(Label7)
        Me.Controls.Add(Label6)
        Me.Controls.Add(Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.details_tbox)
        Me.Controls.Add(Me.recvd_from_cbox)
        Me.Controls.Add(Label5)
        Me.Controls.Add(Me.entered_by_tbox)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.savebtn)
        Me.Controls.Add(Me.no_savebtn)
        Me.Controls.Add(Label3)
        Me.Controls.Add(Me.nature_cbox)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.recpt_cbox)
        Me.Controls.Add(Comp_dateLabel)
        Me.Controls.Add(Me.enq_Date_dtp)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.enq_no_tbox)
        Me.Name = "updatefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Update Enquiry"
        CType(Me.contacts_ud, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents enq_no_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents nature_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents recpt_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents enq_Date_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents no_savebtn As System.Windows.Forms.Button
    Friend WithEvents savebtn As System.Windows.Forms.Button
    Friend WithEvents entered_by_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents recvd_from_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents details_tbox As System.Windows.Forms.TextBox
    Friend WithEvents upd_cl_ref_lbl As System.Windows.Forms.Label
    Friend WithEvents upd_surname_lbl As System.Windows.Forms.Label
    Friend WithEvents upd_cl_name_tbox As System.Windows.Forms.TextBox
    Friend WithEvents case_no_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents resp_tbox As System.Windows.Forms.TextBox
    Friend WithEvents contact_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents contact_lbl As System.Windows.Forms.Label
    Friend WithEvents status_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents hold_dtpicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents hold_lbl As System.Windows.Forms.Label
    Friend WithEvents hold_reason_tbox As System.Windows.Forms.TextBox
    Friend WithEvents hold_reason_lbl As System.Windows.Forms.Label
    Friend WithEvents comp_by_lbl As System.Windows.Forms.Label
    Friend WithEvents comp_by_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents comp_date_lbl As System.Windows.Forms.Label
    Friend WithEvents comp_date_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents contacts_ud As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents actions_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents arr_broken_chk As System.Windows.Forms.CheckBox
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents cabbtn As System.Windows.Forms.Button
    Friend WithEvents upd_branch_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents upd_alloc_to_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents openLink_lbl As System.Windows.Forms.Label
End Class

﻿Public Class disp2frm

    Private Sub disp2frm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        param2 = "select enq_no, enq_receipt_type,enq_date, enq_recvd_from, enq_nature, enq_case_no," &
                    " enq_recvd_by, enq_details, enq_response, enq_cab_no, enq_status_no, enq_completed_by," &
                    "enq_completed_date,enq_hold_reason,enq_hold_end_date,enq_action_taken_no,enq_arrangement_broken," &
                     "enq_no_of_contacts,enq_branch_no,enq_allocated_to,enq_advice_agency_no, enq_hold_reason from Enquiries " & _
                     " where enq_no >= 8373" & _
                     "order by enq_no"

        
        Dim enq_ds As DataSet = get_dataset("complaints", param2)
        enq_dg.Rows.Clear()
        excel_file = "Enq No,receipt type,date,recvd from,nature,case no,Open Links, Client Name,branch,status,Allocated to,completed date," &
            "Hold end date,Hold Reason,Contact,Detail, Broken Arrangement" & vbNewLine
        Dim enq_rows As Integer = no_of_rows - 1
        For idx = 0 To enq_rows
            Try
                dispfrm.ProgressBar1.Value = (idx / enq_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            If search_case_no <> 0 Then
                If search_case_no <> enq_ds.Tables(0).Rows(idx).Item(5) Then
                    Continue For
                End If
            End If
            If search_cab_no <> 0 Then
                If search_cab_no <> enq_ds.Tables(0).Rows(idx).Item(9) Then
                    Continue For
                End If
            End If
            param2 = "select clientSchemeID, linkID, arrange_broken from Debtor where _rowid = " & enq_ds.Tables(0).Rows(idx).Item(5)
            Dim debt_ds As DataSet = get_dataset("onestep", param2)

            param2 = "select CS.clientID, C.name from clientScheme CS, client C" & _
                " where CS._rowid = " & debt_ds.Tables(0).Rows(0).Item(0) & _
                " and CS.clientID = C._rowID"
            Dim cs_ds As DataSet = get_dataset("onestep", param2)

            If search_cl_no <> 0 Then
                If search_cl_no <> cs_ds.Tables(0).Rows(0).Item(0) Then
                    Continue For
                End If
            End If

            If search_branch_no <> 0 Then
                If search_branch_no <> enq_ds.Tables(0).Rows(idx).Item(18) Then
                    Continue For
                End If
            End If

            Dim receipt_type As String
            param2 = "select type_name from enquiry_receipt_types where type_code = " & enq_ds.Tables(0).Rows(idx).Item(1)
            Dim recpt_ds As DataSet = get_dataset("complaints", param2)
            receipt_type = recpt_ds.Tables(0).Rows(0).Item(0)
            If dispfrm.recpt_cbox.SelectedIndex >= 0 Then
                If dispfrm.recpt_cbox.SelectedIndex <> enq_ds.Tables(0).Rows(idx).Item(1) Then
                    Continue For
                End If
            End If

            Dim status As String
            param2 = "select stat_name from enquiry_status where stat_code = " & enq_ds.Tables(0).Rows(idx).Item(10)
            Dim stat_ds As DataSet = get_dataset("complaints", param2)
            status = stat_ds.Tables(0).Rows(0).Item(0)
            If dispfrm.status_cbox.SelectedIndex >= 0 Then
                If dispfrm.status_cbox.SelectedIndex <> enq_ds.Tables(0).Rows(idx).Item(10) Then
                    Continue For
                End If
            End If

            Dim nature As String
            param2 = "select nat_name from enquiry_nature_types where nat_code = " & enq_ds.Tables(0).Rows(idx).Item(4)
            Dim nat_ds As DataSet = get_dataset("complaints", param2)
            nature = nat_ds.Tables(0).Rows(0).Item(0)
            If dispfrm.nature_cbox.SelectedIndex >= 0 Then
                If dispfrm.nature_cbox.SelectedIndex + 17 <> enq_ds.Tables(0).Rows(idx).Item(4) Then
                    Continue For
                End If
            End If

            Dim recvd_from As String
            param2 = "select recvd_from_name from enquiry_recvd_from where recvd_from_code = " & enq_ds.Tables(0).Rows(idx).Item(3)
            Dim recvd_ds As DataSet = get_dataset("complaints", param2)
            recvd_from = recvd_ds.Tables(0).Rows(0).Item(0)
            If dispfrm.recvd_from_cbox.SelectedIndex >= 0 Then
                If dispfrm.recvd_from_cbox.SelectedIndex <> enq_ds.Tables(0).Rows(idx).Item(3) Then
                    Continue For
                End If
            End If

            If dispfrm.recvd_cbox.Checked Then
                Dim enq_date As Date = enq_ds.Tables(0).Rows(idx).Item(2)
                If Format(enq_date, "MMyyyy") <> Format(dispfrm.recvd_dtp.Value, "MMyyyy") Then
                    Continue For
                End If
            End If

            If dispfrm.comp_cbox.Checked Then
                Dim comp_date As Date = enq_ds.Tables(0).Rows(idx).Item(12)
                If Format(comp_date, "MMyyyy") <> Format(dispfrm.comp_dtp.Value, "MMyyyy") Then
                    Continue For
                End If
            End If

            'Dim adv_agency_name As String

            'If adv_agency_no <> 0 Then 'advice agency 
            '    param2 = "select adv_name from Advice_agencies where adv_no = " & adv_agency_no
            '    Dim adv_ds As DataSet = get_dataset("complaints", param2)
            '    adv_agency_name = adv_ds.Tables(0).Rows(0).Item(0)
            'Else
            '    adv_agency_name = ""
            'End If


            Dim cab_name As String
            ' If enq_ds.Tables(0).Rows(idx).Item(3) = 1 Then 'CAB
            Try
                param2 = "select cab_name from CAB_names where cab_no = " & enq_ds.Tables(0).Rows(idx).Item(9)
                Dim cab_ds As DataSet = get_dataset("complaints", param2)
                cab_name = cab_ds.Tables(0).Rows(0).Item(0)
            Catch ex As Exception
                cab_name = ""
            End Try
            'Else
            ' cab_name = ""
            ' End If

            param2 = "select branch_name from enquiry_branches where branch_code = " & enq_ds.Tables(0).Rows(idx).Item(18)
            Dim br_ds As DataSet = get_dataset("complaints", param2)
            Dim branch_name As String = br_ds.Tables(0).Rows(0).Item(0)

            Dim hold_expires As String = ""
            If enq_ds.Tables(0).Rows(idx).Item(10) = 1 Then  'on hold
                hold_expires = Format(enq_ds.Tables(0).Rows(idx).Item(14), "dd/MM/yyyy")
            End If

            Dim allocated_to As Integer
            Try
                allocated_to = enq_ds.Tables(0).Rows(idx).Item(19)
            Catch ex As Exception
                allocated_to = 0
            End Try
            Dim allocated_to_name As String
            If allocated_to = 0 Then
                allocated_to_name = ""
            Else
                param2 = "select wo_name from Welfare_officers where wo_code = " & allocated_to
                Dim wo_ds As DataSet = get_dataset("complaints", param2)
                If no_of_rows = 0 Then
                    MsgBox("Contact IT error WODS3")
                    allocated_to_name = ""
                Else
                    allocated_to_name = wo_ds.Tables(0).Rows(0).Item(0)
                End If
            End If

            param2 = "select act_name from enquiry_action_taken where act_code = " & enq_ds.Tables(0).Rows(idx).Item(15)
            Dim act_ds As DataSet = get_dataset("complaints", param2)
            Dim action_taken As String = ""
            Try
                action_taken = act_ds.Tables(0).Rows(0).Item(0)
            Catch ex As Exception

            End Try

            Dim arrange_broken As String = debt_ds.Tables(0).Rows(0).Item(2)

            Dim detail As String = enq_ds.Tables(0).Rows(idx).Item(7)
            detail = Replace(detail, ",", " ")  'replace any commas with a space
            'also remove any carriage return characters
            Dim new_detail As String = ""
            For narr_idx = 1 To detail.Length
                If Mid(detail, narr_idx, 1) = Chr(10) Or Mid(detail, narr_idx, 1) = Chr(13) Then
                    new_detail &= " "
                Else
                    new_detail &= Mid(detail, narr_idx, 1)
                End If
            Next
            Dim response As String = ""
            Try
                response = enq_ds.Tables(0).Rows(idx).Item(8)
            Catch ex As Exception

            End Try

            response = Replace(response, ",", " ")  'replace any commas with a space
            'also remove any carriage return characters
            Dim new_response As String = ""
            For narr_idx = 1 To detail.Length
                If Mid(response, narr_idx, 1) = Chr(10) Or Mid(response, narr_idx, 1) = Chr(13) Then
                    new_response &= " "
                Else
                    new_response &= Mid(response, narr_idx, 1)
                End If
            Next

            'get number of open links
            Dim linkID As Integer = 0
            Try
                linkID = debt_ds.Tables(0).Rows(0).Item(1)
            Catch ex As Exception

            End Try
            Dim open_links As Integer = 0
            If linkID > 0 Then
                param2 = "select count(*) from Debtor where linkID = " & linkID & _
                    " and _rowID <> " & enq_ds.Tables(0).Rows(idx).Item(5) & _
                    " and status_open_closed = 'O'"
                Dim link_ds As DataSet = get_dataset("onestep", param2)
                open_links = link_ds.Tables(0).Rows(0).Item(0)
            End If

            Dim client_name As String = cs_ds.Tables(0).Rows(0).Item(1)

            enq_dg.Rows.Add(enq_ds.Tables(0).Rows(idx).Item(0), Format(enq_ds.Tables(0).Rows(idx).Item(2), "dd/MM/yyyy"), _
           enq_ds.Tables(0).Rows(idx).Item(5), branch_name, status, allocated_to_name, receipt_type, nature, recvd_from, hold_expires)
            excel_file = excel_file & enq_ds.Tables(0).Rows(idx).Item(0) & "," &
                receipt_type & "," & Format(enq_ds.Tables(0).Rows(idx).Item(2), "dd/MM/yyyy") & "," &
                recvd_from & "," & nature & "," & enq_ds.Tables(0).Rows(idx).Item(5) & "," &
                open_links & "," & client_name & "," & _
                branch_name & "," & status & "," & allocated_to_name & ","
            If enq_ds.Tables(0).Rows(idx).Item(10) = 5 Then 'completed
                excel_file = excel_file & Format(enq_ds.Tables(0).Rows(idx).Item(12), "dd/MM/yyyy") & ","
            Else
                excel_file = excel_file & ","
            End If
            If enq_ds.Tables(0).Rows(idx).Item(10) = 1 Then 'on hold
                excel_file = excel_file & Format(enq_ds.Tables(0).Rows(idx).Item(14), "dd/MM/yyyy") & "," & enq_ds.Tables(0).Rows(idx).Item(21)
            Else
                excel_file = excel_file & ","
            End If
            excel_file = excel_file & "," & cab_name & "," & new_detail & "," &
                arrange_broken & vbNewLine
        Next
        If enq_dg.Rows.Count = 0 Then
            MsgBox("There are no enquiries that meet your criteria")
            Me.Close()
        End If
    End Sub

    Private Sub enq_dg_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles enq_dg.CellContentClick

    End Sub

    Private Sub disp_btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles disp_btn.Click
        disp3frm.ShowDialog()
    End Sub

    Private Sub updbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updbtn.Click
        updatefrm.ShowDialog()
    End Sub

    Private Sub enq_dg_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles enq_dg.CellEnter
        search_enq_no = enq_dg.Rows(e.RowIndex).Cells(0).Value
    End Sub

    Private Sub excelbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles excelbtn.Click


        With SaveFileDialog1
            .Title = "Save to xls"
            .Filter = "excel|*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "enquiries.xls"
        End With
        If Len(excel_file) = 0 Then
            MessageBox.Show("There are no enquiries selected")
        Else
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, excel_file, False)
            End If
        End If
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wofrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.wo_dg = New System.Windows.Forms.DataGridView()
        Me.wo_code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.wo_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.wo_admin = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.wo_deleted = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        CType(Me.wo_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'wo_dg
        '
        Me.wo_dg.AllowUserToDeleteRows = False
        Me.wo_dg.AllowUserToOrderColumns = True
        Me.wo_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.wo_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.wo_code, Me.wo_name, Me.wo_admin, Me.wo_deleted})
        Me.wo_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.wo_dg.Location = New System.Drawing.Point(0, 0)
        Me.wo_dg.Name = "wo_dg"
        Me.wo_dg.Size = New System.Drawing.Size(458, 332)
        Me.wo_dg.TabIndex = 0
        '
        'wo_code
        '
        Me.wo_code.HeaderText = "Code"
        Me.wo_code.Name = "wo_code"
        Me.wo_code.Visible = False
        '
        'wo_name
        '
        Me.wo_name.HeaderText = "Name"
        Me.wo_name.Name = "wo_name"
        Me.wo_name.Width = 200
        '
        'wo_admin
        '
        Me.wo_admin.HeaderText = "Admin"
        Me.wo_admin.Name = "wo_admin"
        '
        'wo_deleted
        '
        Me.wo_deleted.HeaderText = "Deleted"
        Me.wo_deleted.Name = "wo_deleted"
        Me.wo_deleted.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.wo_deleted.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        '
        'wofrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(458, 332)
        Me.Controls.Add(Me.wo_dg)
        Me.Name = "wofrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Welfare Officers"
        CType(Me.wo_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents wo_dg As System.Windows.Forms.DataGridView
    Friend WithEvents wo_code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents wo_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents wo_admin As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents wo_deleted As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class

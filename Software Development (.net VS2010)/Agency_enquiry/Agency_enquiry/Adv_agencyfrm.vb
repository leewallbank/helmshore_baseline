﻿Public Class Adv_agencyfrm

    Private Sub Adv_agencyfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        adv_agency_dg.Rows.Clear()
        param2 = "select adv_no, adv_name from Advice_agencies order by adv_name"
        Dim adv_ds As DataSet = get_dataset("complaints", param2)
        For idx = 0 To no_of_rows - 1
            Dim adv_code As Integer = adv_ds.Tables(0).Rows(idx).Item(0)
            Dim adv_name As String = adv_ds.Tables(0).Rows(idx).Item(1)
            adv_agency_dg.Rows.Add(adv_code, adv_name)
        Next
    End Sub

    Private Sub adv_agency_dg_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles adv_agency_dg.CellContentClick

    End Sub

    Private Sub adv_agency_dg_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles adv_agency_dg.CellDoubleClick
        search_adv_agency = adv_agency_dg.Rows(e.RowIndex).Cells(0).Value
        Me.Close()
    End Sub
End Class
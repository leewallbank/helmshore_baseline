﻿Public Class logfrm

    Private Sub logfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        param2 = "select log_code, log_user, log_date, log_type, log_text from enquiry_log where log_enq_no = " & search_enq_no &
            " order by log_code"
        Dim log_ds As DataSet = get_dataset("complaints", param2)
        If no_of_rows = 0 Then
            MsgBox("No log entries for enquiry no = " & search_enq_no)
            Me.Close()
            Exit Sub
        End If
        Dim log_rows As Integer = no_of_rows - 1
        log_dg.Rows.Clear()
        Dim log_user As String
        param2 = "select wo_name from welfare_officers where wo_code = " & log_ds.Tables(0).Rows(0).Item(1)
        Dim wo_ds As DataSet = get_dataset("complaints", param2)
        log_user = wo_ds.Tables(0).Rows(0).Item(0)
        For idx = 0 To log_rows
            log_dg.Rows.Add(log_ds.Tables(0).Rows(idx).Item(0), log_user, Format(log_ds.Tables(0).Rows(idx).Item(2), "dd/MM/yyyy"), _
            log_ds.Tables(0).Rows(idx).Item(3), log_ds.Tables(0).Rows(idx).Item(4))
        Next
        Me.Text = "Enquiry No " & search_enq_no
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(mainfrm))
        Me.addbtn = New System.Windows.Forms.Button()
        Me.updbtn = New System.Windows.Forms.Button()
        Me.dispbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.logbtn = New System.Windows.Forms.Button()
        Me.adminbtn = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'addbtn
        '
        Me.addbtn.Location = New System.Drawing.Point(114, 43)
        Me.addbtn.Name = "addbtn"
        Me.addbtn.Size = New System.Drawing.Size(110, 23)
        Me.addbtn.TabIndex = 0
        Me.addbtn.Text = "&Add Enquiry"
        Me.addbtn.UseVisualStyleBackColor = True
        '
        'updbtn
        '
        Me.updbtn.Location = New System.Drawing.Point(114, 101)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(110, 23)
        Me.updbtn.TabIndex = 1
        Me.updbtn.Text = "&Update Enquiry"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'dispbtn
        '
        Me.dispbtn.Location = New System.Drawing.Point(114, 158)
        Me.dispbtn.Name = "dispbtn"
        Me.dispbtn.Size = New System.Drawing.Size(110, 23)
        Me.dispbtn.TabIndex = 2
        Me.dispbtn.Text = "&Display Enquiries"
        Me.dispbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(302, 308)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 5
        Me.exitbtn.Text = "&Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'logbtn
        '
        Me.logbtn.Location = New System.Drawing.Point(114, 212)
        Me.logbtn.Name = "logbtn"
        Me.logbtn.Size = New System.Drawing.Size(110, 23)
        Me.logbtn.TabIndex = 3
        Me.logbtn.Text = "Display &Log"
        Me.logbtn.UseVisualStyleBackColor = True
        '
        'adminbtn
        '
        Me.adminbtn.Location = New System.Drawing.Point(114, 272)
        Me.adminbtn.Name = "adminbtn"
        Me.adminbtn.Size = New System.Drawing.Size(110, 23)
        Me.adminbtn.TabIndex = 4
        Me.adminbtn.Text = "Ad&min"
        Me.adminbtn.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(282, 124)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(95, 57)
        Me.Button1.TabIndex = 6
        Me.Button1.Text = "Update Tables"
        Me.Button1.UseVisualStyleBackColor = True
        Me.Button1.Visible = False
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(400, 373)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.adminbtn)
        Me.Controls.Add(Me.logbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.dispbtn)
        Me.Controls.Add(Me.updbtn)
        Me.Controls.Add(Me.addbtn)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Agency Enquiry"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents addbtn As System.Windows.Forms.Button
    Friend WithEvents updbtn As System.Windows.Forms.Button
    Friend WithEvents dispbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents logbtn As System.Windows.Forms.Button
    Friend WithEvents adminbtn As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button

End Class

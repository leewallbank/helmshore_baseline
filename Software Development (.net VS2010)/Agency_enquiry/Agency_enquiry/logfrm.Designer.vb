﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class logfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.log_dg = New System.Windows.Forms.DataGridView()
        Me.log_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.log_user = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.log_date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.log_type = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.log_text = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.log_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'log_dg
        '
        Me.log_dg.AllowUserToAddRows = False
        Me.log_dg.AllowUserToDeleteRows = False
        Me.log_dg.AllowUserToOrderColumns = True
        Me.log_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.log_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.log_no, Me.log_user, Me.log_date, Me.log_type, Me.log_text})
        Me.log_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.log_dg.Location = New System.Drawing.Point(0, 0)
        Me.log_dg.Name = "log_dg"
        Me.log_dg.ReadOnly = True
        Me.log_dg.Size = New System.Drawing.Size(782, 472)
        Me.log_dg.TabIndex = 0
        '
        'log_no
        '
        Me.log_no.HeaderText = "Log No"
        Me.log_no.Name = "log_no"
        Me.log_no.ReadOnly = True
        Me.log_no.Width = 35
        '
        'log_user
        '
        Me.log_user.HeaderText = "Who"
        Me.log_user.Name = "log_user"
        Me.log_user.ReadOnly = True
        '
        'log_date
        '
        Me.log_date.HeaderText = "Date"
        Me.log_date.Name = "log_date"
        Me.log_date.ReadOnly = True
        '
        'log_type
        '
        Me.log_type.HeaderText = "Type"
        Me.log_type.Name = "log_type"
        Me.log_type.ReadOnly = True
        '
        'log_text
        '
        Me.log_text.HeaderText = "Text"
        Me.log_text.Name = "log_text"
        Me.log_text.ReadOnly = True
        Me.log_text.Width = 350
        '
        'logfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(782, 472)
        Me.Controls.Add(Me.log_dg)
        Me.Name = "logfrm"
        Me.Text = "Display Log"
        CType(Me.log_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents log_dg As System.Windows.Forms.DataGridView
    Friend WithEvents log_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents log_user As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents log_date As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents log_type As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents log_text As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

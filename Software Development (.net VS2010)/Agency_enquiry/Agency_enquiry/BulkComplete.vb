﻿Public Class BulkComplete

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub BulkComplete_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        comp_dtp.Value = DateAdd(DateInterval.Month, -1, Now)
    End Sub

    Private Sub compbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles compbtn.Click
        param2 = "select enq_no, enq_date from Enquiries where enq_status_no =0 " &
            " and enq_date < '" & Format(comp_dtp.Value, "yyyy-MM-dd") & "'"
        Dim enq_ds As DataSet = get_dataset("complaints", param2)
        If no_of_rows = 0 Then
            MsgBox("No enquiries to complete")
        Else
            
            If MsgBox("Complete " & no_of_rows & " enquires", vbYesNo, "Bulk complete enquiries") = vbYes Then
                For enq_idx = 0 To no_of_rows - 1
                    upd_txt = "update Enquiries set enq_status_no = '5'" &
                      ", enq_completed_by = " & LoginForm.username_cbox.SelectedIndex + 2 &
                       ",enq_completed_date = '" & Format(Now, "dd/MMM/yyyy") & "'" &
                     "  where enq_no = " & enq_ds.Tables(0).Rows(enq_idx).Item(0)
                    update_sql(upd_txt)
                Next
                MsgBox("Enquiries Updated")
            Else
                MsgBox("enquiries not completed")
            End If
        End If
        Me.Close()
    End Sub
End Class
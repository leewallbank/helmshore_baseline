﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class branchfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.br_dg = New System.Windows.Forms.DataGridView()
        Me.br_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.br_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.br_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'br_dg
        '
        Me.br_dg.AllowUserToAddRows = False
        Me.br_dg.AllowUserToDeleteRows = False
        Me.br_dg.AllowUserToOrderColumns = True
        Me.br_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.br_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.br_no, Me.br_name})
        Me.br_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.br_dg.Location = New System.Drawing.Point(0, 0)
        Me.br_dg.Name = "br_dg"
        Me.br_dg.ReadOnly = True
        Me.br_dg.Size = New System.Drawing.Size(362, 266)
        Me.br_dg.TabIndex = 0
        '
        'br_no
        '
        Me.br_no.HeaderText = "Branch No"
        Me.br_no.Name = "br_no"
        Me.br_no.ReadOnly = True
        Me.br_no.Visible = False
        '
        'br_name
        '
        Me.br_name.HeaderText = "Branch Name"
        Me.br_name.Name = "br_name"
        Me.br_name.ReadOnly = True
        Me.br_name.Width = 200
        '
        'branchfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(362, 266)
        Me.Controls.Add(Me.br_dg)
        Me.Name = "branchfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Double-click to select branch"
        CType(Me.br_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents br_dg As System.Windows.Forms.DataGridView
    Friend WithEvents br_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents br_name As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

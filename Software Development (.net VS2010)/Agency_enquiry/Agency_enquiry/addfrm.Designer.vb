﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class addfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim Comp_dateLabel As System.Windows.Forms.Label
        Dim Label2 As System.Windows.Forms.Label
        Dim Label3 As System.Windows.Forms.Label
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Me.savebtn = New System.Windows.Forms.Button()
        Me.no_savebtn = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.enq_DateTimePicker = New System.Windows.Forms.DateTimePicker()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.recpt_type_cbox = New System.Windows.Forms.ComboBox()
        Me.nature_cbox = New System.Windows.Forms.ComboBox()
        Me.recvd_from_cbox = New System.Windows.Forms.ComboBox()
        Me.case_no_tbox = New System.Windows.Forms.TextBox()
        Me.cl_name_tbox = New System.Windows.Forms.TextBox()
        Me.cl_ref_lbl = New System.Windows.Forms.Label()
        Me.details_tbox = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.surname_lbl = New System.Windows.Forms.Label()
        Me.contact_cbox = New System.Windows.Forms.ComboBox()
        Me.contact_lbl = New System.Windows.Forms.Label()
        Me.cabbtn = New System.Windows.Forms.Button()
        Me.branch_cbox = New System.Windows.Forms.ComboBox()
        Me.alloc_to_cbox = New System.Windows.Forms.ComboBox()
        Me.openLink_lbl = New System.Windows.Forms.Label()
        Comp_dateLabel = New System.Windows.Forms.Label()
        Label2 = New System.Windows.Forms.Label()
        Label3 = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Comp_dateLabel
        '
        Comp_dateLabel.AutoSize = True
        Comp_dateLabel.Location = New System.Drawing.Point(51, 59)
        Comp_dateLabel.Name = "Comp_dateLabel"
        Comp_dateLabel.Size = New System.Drawing.Size(82, 13)
        Comp_dateLabel.TabIndex = 11
        Comp_dateLabel.Text = "Date Received:"
        '
        'Label2
        '
        Label2.AutoSize = True
        Label2.Location = New System.Drawing.Point(217, 59)
        Label2.Name = "Label2"
        Label2.Size = New System.Drawing.Size(74, 13)
        Label2.TabIndex = 13
        Label2.Text = "Receipt Type:"
        '
        'Label3
        '
        Label3.AutoSize = True
        Label3.Location = New System.Drawing.Point(374, 59)
        Label3.Name = "Label3"
        Label3.Size = New System.Drawing.Size(92, 13)
        Label3.TabIndex = 15
        Label3.Text = "Nature of Enquiry:"
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Location = New System.Drawing.Point(523, 59)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(79, 13)
        Label4.TabIndex = 17
        Label4.Text = "Received from:"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Location = New System.Drawing.Point(51, 115)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(51, 13)
        Label5.TabIndex = 19
        Label5.Text = "Case No:"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Location = New System.Drawing.Point(558, 115)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(44, 13)
        Label6.TabIndex = 21
        Label6.Text = "Branch:"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Location = New System.Drawing.Point(209, 115)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(36, 13)
        Label7.TabIndex = 23
        Label7.Text = "Client:"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Location = New System.Drawing.Point(548, 179)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(66, 13)
        Label9.TabIndex = 31
        Label9.Text = "Allocated to:"
        '
        'savebtn
        '
        Me.savebtn.Location = New System.Drawing.Point(583, 380)
        Me.savebtn.Name = "savebtn"
        Me.savebtn.Size = New System.Drawing.Size(98, 23)
        Me.savebtn.TabIndex = 13
        Me.savebtn.Text = "Save and Exit"
        Me.savebtn.UseVisualStyleBackColor = True
        '
        'no_savebtn
        '
        Me.no_savebtn.Location = New System.Drawing.Point(44, 380)
        Me.no_savebtn.Name = "no_savebtn"
        Me.no_savebtn.Size = New System.Drawing.Size(139, 23)
        Me.no_savebtn.TabIndex = 12
        Me.no_savebtn.Text = "DO NOT Save and Exit"
        Me.no_savebtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(209, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(90, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "ENQUIRY FORM"
        '
        'enq_DateTimePicker
        '
        Me.enq_DateTimePicker.Location = New System.Drawing.Point(38, 74)
        Me.enq_DateTimePicker.Name = "enq_DateTimePicker"
        Me.enq_DateTimePicker.Size = New System.Drawing.Size(131, 20)
        Me.enq_DateTimePicker.TabIndex = 0
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'recpt_type_cbox
        '
        Me.recpt_type_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.recpt_type_cbox.FormattingEnabled = True
        Me.recpt_type_cbox.Location = New System.Drawing.Point(198, 74)
        Me.recpt_type_cbox.Name = "recpt_type_cbox"
        Me.recpt_type_cbox.Size = New System.Drawing.Size(121, 21)
        Me.recpt_type_cbox.TabIndex = 1
        '
        'nature_cbox
        '
        Me.nature_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.nature_cbox.FormattingEnabled = True
        Me.nature_cbox.Location = New System.Drawing.Point(355, 74)
        Me.nature_cbox.Name = "nature_cbox"
        Me.nature_cbox.Size = New System.Drawing.Size(121, 21)
        Me.nature_cbox.TabIndex = 2
        '
        'recvd_from_cbox
        '
        Me.recvd_from_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.recvd_from_cbox.FormattingEnabled = True
        Me.recvd_from_cbox.Location = New System.Drawing.Point(504, 74)
        Me.recvd_from_cbox.Name = "recvd_from_cbox"
        Me.recvd_from_cbox.Size = New System.Drawing.Size(154, 21)
        Me.recvd_from_cbox.TabIndex = 3
        '
        'case_no_tbox
        '
        Me.case_no_tbox.Location = New System.Drawing.Point(38, 131)
        Me.case_no_tbox.Name = "case_no_tbox"
        Me.case_no_tbox.Size = New System.Drawing.Size(100, 20)
        Me.case_no_tbox.TabIndex = 4
        '
        'cl_name_tbox
        '
        Me.cl_name_tbox.Location = New System.Drawing.Point(196, 131)
        Me.cl_name_tbox.Name = "cl_name_tbox"
        Me.cl_name_tbox.ReadOnly = True
        Me.cl_name_tbox.Size = New System.Drawing.Size(280, 20)
        Me.cl_name_tbox.TabIndex = 5
        '
        'cl_ref_lbl
        '
        Me.cl_ref_lbl.AutoSize = True
        Me.cl_ref_lbl.Location = New System.Drawing.Point(51, 163)
        Me.cl_ref_lbl.Name = "cl_ref_lbl"
        Me.cl_ref_lbl.Size = New System.Drawing.Size(56, 13)
        Me.cl_ref_lbl.TabIndex = 24
        Me.cl_ref_lbl.Text = "Client Ref:"
        '
        'details_tbox
        '
        Me.details_tbox.Location = New System.Drawing.Point(33, 228)
        Me.details_tbox.Multiline = True
        Me.details_tbox.Name = "details_tbox"
        Me.details_tbox.Size = New System.Drawing.Size(354, 93)
        Me.details_tbox.TabIndex = 8
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(141, 212)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(42, 13)
        Me.Label8.TabIndex = 26
        Me.Label8.Text = "Details:"
        '
        'surname_lbl
        '
        Me.surname_lbl.AutoSize = True
        Me.surname_lbl.Location = New System.Drawing.Point(195, 163)
        Me.surname_lbl.Name = "surname_lbl"
        Me.surname_lbl.Size = New System.Drawing.Size(87, 13)
        Me.surname_lbl.TabIndex = 27
        Me.surname_lbl.Text = "Debtor Surname:"
        '
        'contact_cbox
        '
        Me.contact_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.contact_cbox.FormattingEnabled = True
        Me.contact_cbox.Location = New System.Drawing.Point(501, 270)
        Me.contact_cbox.Name = "contact_cbox"
        Me.contact_cbox.Size = New System.Drawing.Size(166, 21)
        Me.contact_cbox.TabIndex = 9
        '
        'contact_lbl
        '
        Me.contact_lbl.AutoSize = True
        Me.contact_lbl.Location = New System.Drawing.Point(536, 254)
        Me.contact_lbl.Name = "contact_lbl"
        Me.contact_lbl.Size = New System.Drawing.Size(47, 13)
        Me.contact_lbl.TabIndex = 29
        Me.contact_lbl.Text = "Contact:"
        '
        'cabbtn
        '
        Me.cabbtn.Location = New System.Drawing.Point(539, 314)
        Me.cabbtn.Name = "cabbtn"
        Me.cabbtn.Size = New System.Drawing.Size(75, 23)
        Me.cabbtn.TabIndex = 10
        Me.cabbtn.Text = "Add Contact"
        Me.cabbtn.UseVisualStyleBackColor = True
        '
        'branch_cbox
        '
        Me.branch_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.branch_cbox.FormattingEnabled = True
        Me.branch_cbox.Location = New System.Drawing.Point(504, 131)
        Me.branch_cbox.Name = "branch_cbox"
        Me.branch_cbox.Size = New System.Drawing.Size(177, 21)
        Me.branch_cbox.TabIndex = 6
        '
        'alloc_to_cbox
        '
        Me.alloc_to_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.alloc_to_cbox.FormattingEnabled = True
        Me.alloc_to_cbox.Location = New System.Drawing.Point(501, 195)
        Me.alloc_to_cbox.Name = "alloc_to_cbox"
        Me.alloc_to_cbox.Size = New System.Drawing.Size(157, 21)
        Me.alloc_to_cbox.TabIndex = 7
        '
        'openLink_lbl
        '
        Me.openLink_lbl.AutoSize = True
        Me.openLink_lbl.Location = New System.Drawing.Point(51, 179)
        Me.openLink_lbl.Name = "openLink_lbl"
        Me.openLink_lbl.Size = New System.Drawing.Size(64, 13)
        Me.openLink_lbl.TabIndex = 78
        Me.openLink_lbl.Text = "Open Links:"
        '
        'addfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(711, 426)
        Me.Controls.Add(Me.openLink_lbl)
        Me.Controls.Add(Label9)
        Me.Controls.Add(Me.alloc_to_cbox)
        Me.Controls.Add(Me.branch_cbox)
        Me.Controls.Add(Me.cabbtn)
        Me.Controls.Add(Me.contact_lbl)
        Me.Controls.Add(Me.contact_cbox)
        Me.Controls.Add(Me.surname_lbl)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.details_tbox)
        Me.Controls.Add(Me.cl_ref_lbl)
        Me.Controls.Add(Label7)
        Me.Controls.Add(Me.cl_name_tbox)
        Me.Controls.Add(Label6)
        Me.Controls.Add(Label5)
        Me.Controls.Add(Me.case_no_tbox)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Me.recvd_from_cbox)
        Me.Controls.Add(Label3)
        Me.Controls.Add(Me.nature_cbox)
        Me.Controls.Add(Label2)
        Me.Controls.Add(Me.recpt_type_cbox)
        Me.Controls.Add(Comp_dateLabel)
        Me.Controls.Add(Me.enq_DateTimePicker)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.no_savebtn)
        Me.Controls.Add(Me.savebtn)
        Me.Name = "addfrm"
        Me.Text = "Add Enquiry"
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents savebtn As System.Windows.Forms.Button
    Friend WithEvents no_savebtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents enq_DateTimePicker As System.Windows.Forms.DateTimePicker
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents recpt_type_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents nature_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents recvd_from_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents cl_name_tbox As System.Windows.Forms.TextBox
    Friend WithEvents case_no_tbox As System.Windows.Forms.TextBox
    Friend WithEvents cl_ref_lbl As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents details_tbox As System.Windows.Forms.TextBox
    Friend WithEvents surname_lbl As System.Windows.Forms.Label
    Friend WithEvents cabbtn As System.Windows.Forms.Button
    Friend WithEvents contact_lbl As System.Windows.Forms.Label
    Friend WithEvents contact_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents branch_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents alloc_to_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents openLink_lbl As System.Windows.Forms.Label
End Class

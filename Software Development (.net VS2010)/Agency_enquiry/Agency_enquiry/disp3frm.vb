﻿Public Class disp3frm
    Dim initial_load As Boolean
    Private Sub disp2frm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        initial_load = True
        load_case_details()
        initial_load = False
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub enq_no_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles enq_no_tbox.TextChanged
        clear_fields()
        Try
            search_enq_no = enq_no_tbox.Text
        Catch ex As Exception
            error_lbl.Text = "Invalid enquiry number"
            Exit Sub
        End Try
        If search_enq_no < 8373 Then
            error_lbl.Text = "Invalid enquiry number"
            Exit Sub
        End If
        param2 = "select enq_no from Enquiries where enq_no = " & search_enq_no
        Dim enq_ds As DataSet = get_dataset("complaints", param2)
        If no_of_rows = 0 Then
            error_lbl.Text = "Enquiry number does not exist"
            Exit Sub
        End If
        If Not initial_load Then
            load_case_details()
        End If
    End Sub
    Sub load_case_details()
        param2 = "select enq_no, enq_receipt_type,enq_date, enq_recvd_from, enq_nature, enq_case_no," &
                    " enq_recvd_by, enq_details, enq_response, enq_cab_no, enq_status_no, enq_completed_by," &
                    "enq_completed_date,enq_hold_reason,enq_hold_end_date,enq_action_taken_no,enq_arrangement_broken," &
                     "enq_no_of_contacts, enq_branch_no, enq_allocated_to, enq_advice_agency_no from Enquiries where enq_no = " & search_enq_no
        Dim enq_ds As DataSet = get_dataset("complaints", param2)
        enq_no_tbox.Text = search_enq_no
        enq_date.Text = Format(enq_ds.Tables(0).Rows(0).Item(2), "dd/MM/yyyy")

        param2 = "select wo_name from Welfare_officers where wo_code = " & enq_ds.Tables(0).Rows(0).Item(6)
        Dim wo_ds As DataSet = get_dataset("complaints", param2)
        Try
            entered_by_tbox.Text = wo_ds.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            entered_by_tbox.Text = "unknown"
        End Try


        param2 = "select type_name from Enquiry_receipt_types where type_code = " & enq_ds.Tables(0).Rows(0).Item(1)
        Dim recpt_ds As DataSet = get_dataset("complaints", param2)
        Try
            recpt_tbox.Text = recpt_ds.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            recpt_tbox.Text = ""
        End Try


        param2 = "select nat_name from Enquiry_nature_types where nat_code = " & enq_ds.Tables(0).Rows(0).Item(4)
        Dim nat_ds As DataSet = get_dataset("complaints", param2)
        nature_tbox.Text = nat_ds.Tables(0).Rows(0).Item(0)

        param2 = "select recvd_from_name from Enquiry_recvd_from where recvd_from_code = " & enq_ds.Tables(0).Rows(0).Item(3)
        Dim recvd_ds As DataSet = get_dataset("complaints", param2)
        recvd_from_tbox.Text = recvd_ds.Tables(0).Rows(0).Item(0)
        'If enq_ds.Tables(0).Rows(0).Item(3) = 0 Then  '(asdvice agency)
        '    contact_lbl.Visible = True
        '    agency_tbox.Visible = True
        '    contact_lbl.Text = "Advice Agency:"
        '    Dim adv_no As Integer
        '    Try
        '        adv_no = enq_ds.Tables(0).Rows(0).Item(20)
        '    Catch ex As Exception
        '        adv_no = 0
        '    End Try

        '    param2 = "select adv_name from Advice_agencies where adv_no = " & adv_no
        '    Dim adv_ds As DataSet = get_dataset("complaints", param2)
        '    Try
        '        agency_tbox.Text = adv_ds.Tables(0).Rows(0).Item(0)
        '    Catch ex As Exception
        '        agency_tbox.Text = "unknown"
        '    End Try
        'If enq_ds.Tables(0).Rows(0).Item(3) = 1 Then  '(CAB)
        ' contact_lbl.Visible = True
        ' agency_tbox.Visible = True
        'contact_lbl.Text = "CAB:"
        Dim cab_no As Integer = enq_ds.Tables(0).Rows(0).Item(9)
        param2 = "select cab_name from CAB_names where cab_no = " & cab_no
        Dim cab_ds As DataSet = get_dataset("complaints", param2)
        Try
            contact_tbox.Text = cab_ds.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            contact_tbox.Text = ""
        End Try
        ' Else
        'contact_lbl.Visible = False
        ' agency_tbox.Visible = False
        ' End If

        param2 = "select stat_name from Enquiry_status where stat_code = " & enq_ds.Tables(0).Rows(0).Item(10)
        Dim stat_ds As DataSet = get_dataset("complaints", param2)
        status_tbox.Text = stat_ds.Tables(0).Rows(0).Item(0)

        contacts_tbox.Text = enq_ds.Tables(0).Rows(0).Item(17)

        If enq_ds.Tables(0).Rows(0).Item(10) = 1 Then  'on hold
            hold_lbl.Visible = True
            hold_tbox.Visible = True
            hold_reason_lbl.Visible = True
            hold_reason_tbox.Visible = True
            hold_lbl.Text = "Hold Expires:"
            hold_tbox.Text = Format(enq_ds.Tables(0).Rows(0).Item(14), "dd/MM/yyyy")
            If hold_tbox.Text = "01/01/1900" Then
                hold_tbox.Text = ""
            End If
            Try
                hold_reason_tbox.Text = enq_ds.Tables(0).Rows(0).Item(13)
            Catch ex As Exception
                hold_reason_tbox.Text = ""
            End Try

        Else
            hold_lbl.Visible = False
            hold_tbox.Visible = False
            hold_reason_lbl.Visible = False
            hold_reason_tbox.Visible = False
        End If

        If enq_ds.Tables(0).Rows(0).Item(10) = 5 Then  'completed
            comp_by_lbl.Visible = True
            comp_date_lbl.Visible = True
            comp_by_tbox.Visible = True
            comp_date_tbox.Visible = True
            Try
                comp_date_tbox.Text = Format(enq_ds.Tables(0).Rows(0).Item(12))
            Catch ex As Exception
                comp_date_tbox.Text = ""
            End Try
            If comp_date_tbox.Text = "01/01/1900" Then
                comp_date_tbox.Text = ""
            End If
            Dim comp_by As Integer
            Try
                comp_by = enq_ds.Tables(0).Rows(0).Item(11)
                param2 = "select wo_name from welfare_officers where wo_code = " & comp_by
                Dim wo_ds2 As DataSet = get_dataset("complaints", param2)
                If no_of_rows = 1 Then
                    comp_by_tbox.Text = wo_ds2.Tables(0).Rows(0).Item(0)
                Else
                    comp_by_tbox.Text = ""
                End If
            Catch ex As Exception
                comp_by_tbox.Text = ""
            End Try
        Else
            comp_by_lbl.Visible = False
            comp_date_lbl.Visible = False
            comp_by_tbox.Visible = False
            comp_date_tbox.Visible = False
        End If

        param2 = "select act_name from enquiry_action_taken where act_code = " & enq_ds.Tables(0).Rows(0).Item(15)
        Dim act_ds As DataSet = get_dataset("complaints", param2)
        Try
            actions_tbox.Text = act_ds.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            actions_tbox.Text = ""
        End Try

        Dim arr_broken As Boolean = False
        Try
            arr_broken = enq_ds.Tables(0).Rows(0).Item(16)
        Catch ex As Exception

        End Try

        arr_broken_chk.Checked = arr_broken

        details_tbox.Text = enq_ds.Tables(0).Rows(0).Item(7)
        Try
            response_tbox.Text = enq_ds.Tables(0).Rows(0).Item(8)
        Catch ex As Exception
            response_tbox.Text = ""
        End Try

        case_no_tbox.Text = enq_ds.Tables(0).Rows(0).Item(5)

        param2 = "select client_ref, clientSchemeID, name_sur, linkID from Debtor where _rowid = " & case_no_tbox.Text
        Dim case_ds As DataSet = get_dataset("onestep", param2)
        disp_surname_lbl.Text = "Surname:" & case_ds.Tables(0).Rows(0).Item(2)

        'get number of open links
        Dim linkID As Integer = 0
        Try
            linkID = case_ds.Tables(0).Rows(0).Item(3)
        Catch ex As Exception

        End Try
        If linkID > 0 Then
            param2 = "select count(*) from Debtor where linkID = " & linkID & _
                " and _rowID <> " & case_no_tbox.Text & _
                " and status_open_closed = 'O'"
            Dim link_ds As DataSet = get_dataset("onestep", param2)
            openLink_lbl.Text = "Open Links:" & link_ds.Tables(0).Rows(0).Item(0)
        Else
            openLink_lbl.Text = "Open Links:0"
        End If



        Dim csid As Integer = case_ds.Tables(0).Rows(0).Item(1)
        disp_cl_ref_lbl.Text = "Client Ref:" & case_ds.Tables(0).Rows(0).Item(0)
        param2 = "select clientID, branchID from clientScheme where _rowid = " & csid
        Dim csid_ds As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            disp_cl_name_tbox.Text = ""
            branch_tbox.Text = ""
        Else
            param2 = "select name from Client where _rowid = " & csid_ds.Tables(0).Rows(0).Item(0)
            Dim cl_ds As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 1 Then
                disp_cl_name_tbox.Text = cl_ds.Tables(0).Rows(0).Item(0)
            End If
        End If

        'get branch name
        param2 = "select branch_name from enquiry_branches where branch_code = " & enq_ds.Tables(0).Rows(0).Item(18)
        Dim br_ds As DataSet = get_dataset("complaints", param2)
        branch_tbox.Text = br_ds.Tables(0).Rows(0).Item(0)

        Dim allocated_to As Integer
        'get allocated to
        Try
            allocated_to = enq_ds.Tables(0).Rows(0).Item(19)
        Catch ex As Exception
            allocated_to = 0
        End Try
        If allocated_to = 0 Then
            alloc_to_tbox.Text = ""
        Else
            param2 = "select wo_name from Welfare_officers where wo_code = " & allocated_to
            Dim wo2_ds As DataSet = get_dataset("complaints", param2)
            alloc_to_tbox.Text = wo2_ds.Tables(0).Rows(0).Item(0)
        End If

    End Sub
    Sub clear_fields()
        error_lbl.Text = ""
        enq_date.Text = ""
        entered_by_tbox.Text = ""
        recvd_from_tbox.Text = ""
        recpt_tbox.Text = ""
        nature_tbox.Text = ""
        case_no_tbox.Text = ""
        details_tbox.Text = ""
        disp_cl_ref_lbl.Text = ""
        disp_cl_name_tbox.Text = ""
        disp_surname_lbl.Text = ""
        branch_tbox.Text = ""
        status_tbox.Text = ""
    End Sub

    Private Sub updbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles updbtn.Click
        updatefrm.ShowDialog()
        Me.Close()
    End Sub

    Private Sub case_no_tbox_TextChanged(sender As System.Object, e As System.EventArgs) Handles case_no_tbox.TextChanged

    End Sub
End Class
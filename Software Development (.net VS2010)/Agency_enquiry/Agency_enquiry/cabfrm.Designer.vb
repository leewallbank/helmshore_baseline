﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cabfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cab_dg = New System.Windows.Forms.DataGridView()
        Me.cab_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cab_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.cab_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cab_dg
        '
        Me.cab_dg.AllowUserToAddRows = False
        Me.cab_dg.AllowUserToDeleteRows = False
        Me.cab_dg.AllowUserToOrderColumns = True
        Me.cab_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.cab_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cab_no, Me.cab_name})
        Me.cab_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cab_dg.Location = New System.Drawing.Point(0, 0)
        Me.cab_dg.Name = "cab_dg"
        Me.cab_dg.ReadOnly = True
        Me.cab_dg.Size = New System.Drawing.Size(329, 435)
        Me.cab_dg.TabIndex = 0
        '
        'cab_no
        '
        Me.cab_no.HeaderText = "No"
        Me.cab_no.Name = "cab_no"
        Me.cab_no.ReadOnly = True
        Me.cab_no.Visible = False
        '
        'cab_name
        '
        Me.cab_name.HeaderText = "Contact"
        Me.cab_name.Name = "cab_name"
        Me.cab_name.ReadOnly = True
        Me.cab_name.Width = 250
        '
        'cabfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(329, 435)
        Me.Controls.Add(Me.cab_dg)
        Me.Name = "cabfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Double-click to select Contact"
        CType(Me.cab_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cab_dg As System.Windows.Forms.DataGridView
    Friend WithEvents cab_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cab_name As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

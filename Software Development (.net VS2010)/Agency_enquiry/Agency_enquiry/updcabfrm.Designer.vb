﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class updcabfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.updcab_dg = New System.Windows.Forms.DataGridView()
        Me.cab_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cab_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.updcab_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'updcab_dg
        '
        Me.updcab_dg.AllowUserToAddRows = False
        Me.updcab_dg.AllowUserToDeleteRows = False
        Me.updcab_dg.AllowUserToOrderColumns = True
        Me.updcab_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.updcab_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cab_no, Me.cab_name})
        Me.updcab_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.updcab_dg.Location = New System.Drawing.Point(0, 0)
        Me.updcab_dg.Name = "updcab_dg"
        Me.updcab_dg.Size = New System.Drawing.Size(328, 474)
        Me.updcab_dg.TabIndex = 0
        '
        'cab_no
        '
        Me.cab_no.HeaderText = "Column1"
        Me.cab_no.Name = "cab_no"
        Me.cab_no.Visible = False
        '
        'cab_name
        '
        Me.cab_name.HeaderText = "CAB Name"
        Me.cab_name.Name = "cab_name"
        Me.cab_name.Width = 250
        '
        'updcabfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(328, 474)
        Me.Controls.Add(Me.updcab_dg)
        Me.Name = "updcabfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Update CAB"
        CType(Me.updcab_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents updcab_dg As System.Windows.Forms.DataGridView
    Friend WithEvents cab_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cab_name As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

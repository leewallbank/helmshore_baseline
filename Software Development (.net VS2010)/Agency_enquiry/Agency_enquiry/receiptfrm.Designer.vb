﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class receiptfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.recpt_dg = New System.Windows.Forms.DataGridView()
        Me.type_code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.type_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.recpt_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'recpt_dg
        '
        Me.recpt_dg.AllowUserToDeleteRows = False
        Me.recpt_dg.AllowUserToOrderColumns = True
        Me.recpt_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.recpt_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.type_code, Me.type_name})
        Me.recpt_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.recpt_dg.Location = New System.Drawing.Point(0, 0)
        Me.recpt_dg.Name = "recpt_dg"
        Me.recpt_dg.Size = New System.Drawing.Size(290, 367)
        Me.recpt_dg.TabIndex = 0
        '
        'type_code
        '
        Me.type_code.HeaderText = "Code"
        Me.type_code.Name = "type_code"
        Me.type_code.Visible = False
        '
        'type_name
        '
        Me.type_name.HeaderText = "Name"
        Me.type_name.Name = "type_name"
        Me.type_name.Width = 200
        '
        'receiptfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(290, 367)
        Me.Controls.Add(Me.recpt_dg)
        Me.Name = "receiptfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Receipt Types"
        CType(Me.recpt_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents recpt_dg As System.Windows.Forms.DataGridView
    Friend WithEvents type_code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents type_name As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class naturefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.updnature_dg = New System.Windows.Forms.DataGridView()
        Me.nat_code = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.nat_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.updnature_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'updnature_dg
        '
        Me.updnature_dg.AllowUserToDeleteRows = False
        Me.updnature_dg.AllowUserToOrderColumns = True
        Me.updnature_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.updnature_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.nat_code, Me.nat_name})
        Me.updnature_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.updnature_dg.Location = New System.Drawing.Point(0, 0)
        Me.updnature_dg.Name = "updnature_dg"
        Me.updnature_dg.Size = New System.Drawing.Size(323, 351)
        Me.updnature_dg.TabIndex = 0
        '
        'nat_code
        '
        Me.nat_code.HeaderText = "Column1"
        Me.nat_code.Name = "nat_code"
        Me.nat_code.Visible = False
        '
        'nat_name
        '
        Me.nat_name.HeaderText = "Nature of Enquiry"
        Me.nat_name.Name = "nat_name"
        Me.nat_name.Width = 250
        '
        'naturefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(323, 351)
        Me.Controls.Add(Me.updnature_dg)
        Me.Name = "naturefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nature of Enquiry"
        CType(Me.updnature_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents updnature_dg As System.Windows.Forms.DataGridView
    Friend WithEvents nat_code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents nat_name As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

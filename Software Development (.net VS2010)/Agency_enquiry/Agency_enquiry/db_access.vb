﻿Imports System.Configuration
Imports System.Data.SqlClient
Module db_access
    Public sqlCon As New SqlConnection
    Public param2 As String
    Public no_of_rows As Integer
    Public os_con As New Odbc.OdbcConnection
    Public Sub Connect_sqlDb()

        Try
            If Not IsNothing(sqlCon) Then
                'This is only necessary following an exception...
                If sqlCon.State = ConnectionState.Open Then sqlCon.Close()
            End If
            sqlCon.ConnectionString = ""
            sqlCon.ConnectionString = ConfigurationManager.ConnectionStrings("EnquiryDatabase").ConnectionString
            sqlCon.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings("EnquiryDatabase").ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings("EnquiryDatabase").ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub
    Public Sub Connect_os_Db()

        Try
            If Not IsNothing(os_con) Then
                'This is only necessary following an exception...
                If os_con.State = ConnectionState.Open Then os_con.Close()
            End If
            os_con.ConnectionString = ""
            os_con.ConnectionString = ConfigurationManager.ConnectionStrings("DebtRecovery").ConnectionString
            os_con.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings("DebtRecovery").ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings("DebtRecovery").ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub
    Public Sub DisconnectDb()
        sqlCon.Close()
        sqlCon.Dispose()
        os_con.Close()
        os_con.Dispose()
    End Sub
    Function get_dataset(ByVal database_name As String, Optional ByVal select_string As String = Nothing) As DataSet
        Dim dataset As New DataSet
        Try
            If database_name = "onestep" Then
                'If os_conn.State = ConnectionState.Closed Then
                '    os_conn.ConnectionString = _
                '           "Integrated Security=True;Dsn=DebtRecovery;db=DebtRecovery;uid=vbnet;password=tenbv;"
                '    Try
                '        os_conn.Open()
                '    Catch ex As Exception
                '        MsgBox("Unable to open onestep connection")
                '    End Try
                'End If
                'Dim adapter As New Odbc.OdbcDataAdapter(select_string, os_conn)
                'adapter.Fill(dataset)
                If os_con.State = ConnectionState.Closed Then
                    Connect_os_Db()
                End If
                Dim adapter As New Odbc.OdbcDataAdapter(select_string, os_con)
                adapter.Fill(dataset)
            Else
                If sqlCon.State = ConnectionState.Closed Then
                    Connect_sqlDb()
                End If
                Dim adapter As New SqlDataAdapter(select_string, sqlCon)
                adapter.Fill(dataset)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Try
            no_of_rows = dataset.Tables(0).Rows.Count
        Catch
            no_of_rows = 0
        End Try
        Return dataset

    End Function
    Sub update_sql(ByVal upd_txt As String)
        Try
            Dim sqlCMD As SqlCommand

            'Define attachment to database table specifics
            sqlCMD = New SqlCommand
            With sqlCMD
                .Connection = sqlCon
                .CommandText = upd_txt
                .ExecuteNonQuery()
            End With

            'Clean up the connection
            sqlCMD = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Module



﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class newpassfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.savebtn = New System.Windows.Forms.Button()
        Me.pw2_tbox = New System.Windows.Forms.TextBox()
        Me.pw1_tbox = New System.Windows.Forms.TextBox()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'savebtn
        '
        Me.savebtn.Location = New System.Drawing.Point(34, 208)
        Me.savebtn.Name = "savebtn"
        Me.savebtn.Size = New System.Drawing.Size(111, 23)
        Me.savebtn.TabIndex = 2
        Me.savebtn.Text = "&Save new password"
        Me.savebtn.UseVisualStyleBackColor = True
        '
        'pw2_tbox
        '
        Me.pw2_tbox.Location = New System.Drawing.Point(87, 107)
        Me.pw2_tbox.Name = "pw2_tbox"
        Me.pw2_tbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.pw2_tbox.Size = New System.Drawing.Size(100, 20)
        Me.pw2_tbox.TabIndex = 1
        '
        'pw1_tbox
        '
        Me.pw1_tbox.Location = New System.Drawing.Point(87, 48)
        Me.pw1_tbox.Name = "pw1_tbox"
        Me.pw1_tbox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.pw1_tbox.Size = New System.Drawing.Size(100, 20)
        Me.pw1_tbox.TabIndex = 0
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(242, 208)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 3
        Me.exitbtn.Text = "&Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(92, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(96, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Re-enter password"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(92, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(80, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Enter password"
        '
        'newpassfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(329, 266)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.pw1_tbox)
        Me.Controls.Add(Me.pw2_tbox)
        Me.Controls.Add(Me.savebtn)
        Me.Name = "newpassfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Save new password"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents savebtn As System.Windows.Forms.Button
    Friend WithEvents pw2_tbox As System.Windows.Forms.TextBox
    Friend WithEvents pw1_tbox As System.Windows.Forms.TextBox
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class

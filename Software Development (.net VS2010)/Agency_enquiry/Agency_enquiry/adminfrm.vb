﻿Public Class adminfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub wobtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles wobtn.Click
        wofrm.ShowDialog()
        wofrm.wo_dg.EndEdit()
        'update database with changes
        For idx = 0 To wofrm.wo_dg.Rows.Count
            Dim wo_name As String
            Try
                wo_name = wofrm.wo_dg.Rows(idx).Cells(1).Value
            Catch ex As Exception
                Continue For
            End Try
            If wo_name = Nothing Then
                Continue For
            End If
            Dim wo_code As Integer
            Try
                wo_code = wofrm.wo_dg.Rows(idx).Cells(0).Value
            Catch ex As Exception
                wo_code = 0
            End Try

            Dim wo_admin As Integer = wofrm.wo_dg.Rows(idx).Cells(2).Value
            Dim wo_deleted As Integer = wofrm.wo_dg.Rows(idx).Cells(3).Value

            'see if wo_code is already on the table
            upd_txt = "insert into welfare_officers values ('" & wo_name & "'," &
                    "'password'," & wo_admin & "," & wo_deleted & ")"
            If wo_code > 0 Then
                param2 = "select wo_code from welfare_officers where wo_code = " & wo_code
                Dim wo_ds As DataSet = get_dataset("complaints", param2)
                If no_of_rows > 0 Then
                    upd_txt = "update welfare_officers set wo_name = '" & wo_name & "'," &
               "wo_admin = " & wo_admin & ",wo_deleted = " & wo_deleted & " where wo_code = " & wo_code
                End If
            End If
            update_sql(upd_txt)
        Next
        Me.Close()
    End Sub

    Private Sub reset_passbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reset_passbtn.Click
        resetpassfrm.ShowDialog()
        Me.Close()
    End Sub

    Private Sub rectypbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rectypbtn.Click
        receiptfrm.ShowDialog()
        receiptfrm.recpt_dg.EndEdit()
        'update database with changes
        For idx = 0 To orig_no_of_rows - 1
            Dim recpt_name As String
            Try
                recpt_name = receiptfrm.recpt_dg.Rows(idx).Cells(1).Value
            Catch ex As Exception
                Continue For
            End Try
            If recpt_name = Nothing Then
                Continue For
            End If
            Dim recpt_code As Integer
            Try
                recpt_code = receiptfrm.recpt_dg.Rows(idx).Cells(0).Value
            Catch ex As Exception
                Continue For
            End Try

            upd_txt = "update enquiry_receipt_types set type_name = '" & recpt_name & _
              "' where type_code = " & recpt_code
            update_sql(upd_txt)
        Next
        'insert any new receipt types
        For idx = orig_no_of_rows To receiptfrm.recpt_dg.Rows.Count
            Dim recpt_name As String
            Try
                recpt_name = receiptfrm.recpt_dg.Rows(idx).Cells(1).Value
            Catch ex As Exception
                Continue For
            End Try
            If recpt_name = Nothing Then
                Continue For
            End If
            upd_txt = "insert into enquiry_receipt_types (type_code, type_name) values (" & idx & ",'" & recpt_name & "')"
            update_sql(upd_txt)
        Next

        Me.Close()
    End Sub

    Private Sub recvd_frombtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recvd_frombtn.Click
        recvdfromfrm.ShowDialog()

        recvdfromfrm.updrecvd_from_dg.EndEdit()
        'update database with changes
        For idx = 0 To orig_no_of_rows - 1
            Dim recvd_name As String
            Try
                recvd_name = recvdfromfrm.updrecvd_from_dg.Rows(idx).Cells(1).Value
            Catch ex As Exception
                Continue For
            End Try
            If recvd_name = Nothing Then
                Continue For
            End If
            Dim recvd_code As Integer
            Try
                recvd_code = recvdfromfrm.updrecvd_from_dg.Rows(idx).Cells(0).Value
            Catch ex As Exception
                Continue For
            End Try

            upd_txt = "update enquiry_recvd_from set recvd_from_name = '" & recvd_name & _
              "' where recvd_from_code = " & recvd_code
            update_sql(upd_txt)
        Next
        'insert any new recvd from
        For idx = orig_no_of_rows To recvdfromfrm.updrecvd_from_dg.Rows.Count
            Dim recvd_name As String
            Try
                recvd_name = recvdfromfrm.updrecvd_from_dg.Rows(idx).Cells(1).Value
            Catch ex As Exception
                Continue For
            End Try
            If recvd_name = Nothing Then
                Continue For
            End If
            upd_txt = "insert into enquiry_recvd_from (recvd_from_code, recvd_from_name) values (" & idx & ",'" & recvd_name & "')"
            update_sql(upd_txt)
        Next

        Me.Close()
    End Sub

    Private Sub cabbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cabbtn.Click
        updcabfrm.ShowDialog()
        'update cab names
        updcabfrm.updcab_dg.EndEdit()
        'update database with changes
        For idx = 0 To updcabfrm.updcab_dg.Rows.Count - 1
            Dim cab_name As String
            Try
                cab_name = updcabfrm.updcab_dg.Rows(idx).Cells(1).Value
            Catch ex As Exception
                Continue For
            End Try
            If cab_name = Nothing Then
                Continue For
            End If
            Dim cab_no As Integer
            Try
                cab_no = updcabfrm.updcab_dg.Rows(idx).Cells(0).Value
            Catch ex As Exception
                Continue For
            End Try

            upd_txt = "update cab_names set cab_name = '" & cab_name & _
              "' where cab_no = " & cab_no
            update_sql(upd_txt)
        Next
        Me.Close()
    End Sub

   
    Private Sub delcabbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles delcabbtn.Click
        cabfrm.ShowDialog()
        'check if cab no is used in an enquiry
        param2 = "select count(enq_no) from enquiries where enq_cab_no = " & search_cab_no
        Dim cab_ds As DataSet = get_dataset("complaints", param2)
        If cab_ds.Tables(0).Rows(0).Item(0) > 0 Then
            MsgBox("Unable to delete as this is used in an enquiry")
        Else
            If MsgBox("Delete CAB - " & search_cab_name, MsgBoxStyle.YesNo, "Delete CAB name") = MsgBoxResult.Yes Then
                upd_txt = "delete from cab_names where cab_no = " & search_cab_no
                update_sql(upd_txt)
            Else
                MsgBox("Cab not deleted")
            End If
        End If
        
        Me.Close()
    End Sub

    Private Sub naturebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles naturebtn.Click
        naturefrm.ShowDialog()
        naturefrm.updnature_dg.EndEdit()
        'update database with changes
        For idx = 0 To orig_no_of_rows - 1
            Dim nat_name As String
            Try
                nat_name = naturefrm.updnature_dg.Rows(idx).Cells(1).Value
            Catch ex As Exception
                Continue For
            End Try
            If nat_name = Nothing Then
                Continue For
            End If
            Dim nat_code As Integer
            Try
                nat_code = naturefrm.updnature_dg.Rows(idx).Cells(0).Value
            Catch ex As Exception
                Continue For
            End Try

            upd_txt = "update enquiry_nature_types set nat_name = '" & nat_name & _
              "' where nat_code = " & nat_code
            update_sql(upd_txt)
        Next
        'insert any new nature types
        For idx = orig_no_of_rows To naturefrm.updnature_dg.Rows.Count
            Dim nat_name As String
            Try
                nat_name = naturefrm.updnature_dg.Rows(idx).Cells(1).Value
            Catch ex As Exception
                Continue For
            End Try
            If nat_name = Nothing Then
                Continue For
            End If
            upd_txt = "insert into enquiry_nature_types (nat_code, nat_name) values (" & idx & ",'" & nat_name & "')"
            update_sql(upd_txt)
        Next
        Me.Close()
    End Sub

    Private Sub statusbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles statusbtn.Click
        statusfrm.ShowDialog()
        statusfrm.stat_dg.EndEdit()
        'update database with changes
        For idx = 0 To orig_no_of_rows - 1
            Dim stat_name As String
            Try
                stat_name = statusfrm.stat_dg.Rows(idx).Cells(1).Value
            Catch ex As Exception
                Continue For
            End Try
            If stat_name = Nothing Then
                Continue For
            End If
            Dim stat_code As Integer
            Try
                stat_code = statusfrm.stat_dg.Rows(idx).Cells(0).Value
            Catch ex As Exception
                Continue For
            End Try

            upd_txt = "update enquiry_status set stat_name = '" & stat_name & _
              "' where stat_code = " & stat_code
            update_sql(upd_txt)
        Next
        'insert any new statuses
        For idx = orig_no_of_rows To statusfrm.stat_dg.Rows.Count
            Dim stat_name As String
            Try
                stat_name = statusfrm.stat_dg.Rows(idx).Cells(1).Value
            Catch ex As Exception
                Continue For
            End Try
            If stat_name = Nothing Then
                Continue For
            End If
            upd_txt = "insert into enquiry_status (stat_code, stat_name) values (" & idx & ",'" & stat_name & "')"
            update_sql(upd_txt)
        Next
        Me.Close()
    End Sub

    Private Sub upd_adv_agencybtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles upd_adv_agencybtn.Click
        update_adv_agencyfrm.ShowDialog()
        'update advice agency names
        update_adv_agencyfrm.adv_agencydg.EndEdit()
        'update database with changes
        For idx = 0 To update_adv_agencyfrm.adv_agencydg.Rows.Count - 1
            Dim adv_name As String
            Try
                adv_name = update_adv_agencyfrm.adv_agencydg.Rows(idx).Cells(1).Value
            Catch ex As Exception
                Continue For
            End Try
            If adv_name = Nothing Then
                Continue For
            End If
            Dim adv_no As Integer
            Try
                adv_no = update_adv_agencyfrm.adv_agencydg.Rows(idx).Cells(0).Value
            Catch ex As Exception
                Continue For
            End Try

            upd_txt = "update Advice_agencies set adv_name = '" & adv_name & _
              "' where adv_no = " & adv_no
            update_sql(upd_txt)
        Next
        Me.Close()
    End Sub

    Private Sub ins_adv_agencybtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ins_adv_agencybtn.Click
        Dim new_adv_name As String = Trim(InputBox("Enter new Advice Agency name", "Advice agency entry"))
        If new_adv_name.Length = 0 Then
            Exit Sub
        End If
        'see if adv agency name already exists
        param2 = "select count(adv_no) from Advice_agencies where adv_name = '" & new_adv_name & "'"
        Dim adv_ds2 As DataSet = get_dataset("complaints", param2)
        If adv_ds2.Tables(0).Rows(0).Item(0) > 0 Then
            MsgBox("This Advice Agency name already exists")
            Exit Sub
        End If
        'get highest adv_no in the table
        param2 = "select max(adv_no) from Advice_agencies"
        Dim adv_ds3 As DataSet = get_dataset("complaints", param2)
        Dim next_adv_no = adv_ds3.Tables(0).Rows(0).Item(0) + 1
        Try
            upd_txt = "insert into Advice_agencies (adv_no, adv_name) values (" & next_adv_no & ",'" & new_adv_name & "')"
            update_sql(upd_txt)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
       
    End Sub

    Private Sub bulkbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bulkbtn.Click
        BulkComplete.ShowDialog()
        Me.Close()
    End Sub

    Private Sub adminfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
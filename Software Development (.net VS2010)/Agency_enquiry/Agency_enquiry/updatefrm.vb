﻿Public Class updatefrm
    Dim enq_ds As DataSet
    Dim log_text, orig_cab_name, orig_adv_name As String
    Private Sub no_savebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles no_savebtn.Click
        Me.Close()
    End Sub

    Private Sub savebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles savebtn.Click
        'If recvd_from_cbox.SelectedIndex = 0 Then 'Advice agency
        '    If contact_cbox.SelectedIndex = -1 Then
        '        MsgBox("Select Advice Agency")
        '        Exit Sub
        '    End If
        'End If

        'If recvd_from_cbox.SelectedIndex = 1 Then 'CAB
        '    If contact_cbox.SelectedIndex = -1 Then
        '        MsgBox("Select CAB office")
        '        Exit Sub
        '    End If
        'End If

        If status_cbox.SelectedIndex = 5 Then  'completed
            If comp_by_cbox.SelectedIndex = -1 Then
                MsgBox("Enter who completed the enquiry")
                Exit Sub
            End If
        End If

        Dim change_found As Boolean = False

        If enq_ds.Tables(0).Rows(0).Item(10) = status_cbox.SelectedIndex Then
            If MsgBox("Did you forget to change the status?", MsgBoxStyle.YesNo, "Status Change required?") = MsgBoxResult.Yes Then
                Exit Sub
            End If
        Else
            log_text = "Status changed from " & enq_ds.Tables(0).Rows(0).Item(10) & " to " &
            status_cbox.SelectedIndex
            write_log()
            change_found = True
        End If

        If enq_ds.Tables(0).Rows(0).Item(1) <> recpt_cbox.SelectedIndex Then
            log_text = "Receipt type changed from " & enq_ds.Tables(0).Rows(0).Item(1) & " to " &
            recpt_cbox.SelectedIndex
            write_log()
            change_found = True
        End If


        If enq_ds.Tables(0).Rows(0).Item(2) <> enq_Date_dtp.Value Then
            log_text = "Enquiry Date changed from " & enq_ds.Tables(0).Rows(0).Item(2) & " to " &
            enq_Date_dtp.Value
            write_log()
            change_found = True
        End If

        If enq_ds.Tables(0).Rows(0).Item(3) <> recvd_from_cbox.SelectedIndex Then
            log_text = "Received from changed from " & enq_ds.Tables(0).Rows(0).Item(3) & " to " &
            recvd_from_cbox.SelectedIndex
            write_log()
            change_found = True
        End If

        If enq_ds.Tables(0).Rows(0).Item(4) <> nature_cbox.SelectedIndex + 17 Then
            log_text = "Nature of Enquiry changed from " & enq_ds.Tables(0).Rows(0).Item(4) & " to " &
            nature_cbox.SelectedIndex + 17
            write_log()
            change_found = True
        End If

        If enq_ds.Tables(0).Rows(0).Item(5) <> case_no_tbox.Text Then
            log_text = "Case number changed from " & enq_ds.Tables(0).Rows(0).Item(5) & " to " &
            case_no_tbox.Text
            write_log()
            change_found = True
        End If

        'need to check for quotes - change ' to ''
        Dim details As String = ""
        For det_idx = 1 To details_tbox.Text.Length
            If Mid(details_tbox.Text, det_idx, 1) = "'" Or Mid(details_tbox.Text, det_idx, 1) = Chr(34) Then
                details &= "''"
            Else
                details &= Mid(details_tbox.Text, det_idx, 1)
            End If
        Next

        If enq_ds.Tables(0).Rows(0).Item(7) <> details_tbox.Text Then
            'need to check original details as well"
            Dim orig_details As String = ""
            For det_idx = 1 To enq_ds.Tables(0).Rows(0).Item(7).Length
                If Mid(enq_ds.Tables(0).Rows(0).Item(7), det_idx, 1) = "'" Or Mid(enq_ds.Tables(0).Rows(0).Item(7), det_idx, 1) = Chr(34) Then
                    orig_details &= "''"
                Else
                    orig_details &= Mid(enq_ds.Tables(0).Rows(0).Item(7), det_idx, 1)
                End If
            Next
            log_text = "Details changed from " & orig_details & " to " & details
            write_log()
            change_found = True
        End If
        Dim response As String = ""
        Dim orig_response As String
        Try
            orig_response = enq_ds.Tables(0).Rows(0).Item(8)
        Catch ex As Exception
            orig_response = ""
        End Try
        If orig_response <> resp_tbox.Text Then
            Dim orig_response2 As String = ""
            For resp_idx = 1 To orig_response.Length
                If Mid(orig_response, resp_idx, 1) = "'" Or Mid(orig_response, resp_idx, 1) = Chr(34) Then
                    orig_response2 &= "''"
                Else
                    orig_response2 &= Mid(orig_response, resp_idx, 1)
                End If
            Next
            For resp_idx = 1 To resp_tbox.Text.Length
                If Mid(resp_tbox.Text, resp_idx, 1) = "'" Or Mid(resp_tbox.Text, resp_idx, 1) = Chr(34) Then
                    response &= "''"
                Else
                    response &= Mid(resp_tbox.Text, resp_idx, 1)
                End If
            Next
            log_text = "Response changed from " & orig_response2 & " to " &
            response
            write_log()
            change_found = True
        End If

        If orig_cab_name <> contact_cbox.Text Then
            log_text = "Contact changed from " & orig_cab_name & " to " &
            contact_cbox.Text
            write_log()
            change_found = True
        End If

        'If recvd_from_cbox.SelectedIndex = 0 And orig_adv_name <> contact_cbox.Text Then
        '    log_text = "Advice agency changed from " & orig_adv_name & " to " &
        '    contact_cbox.Text
        '    write_log()
        '    change_found = True
        'End If

        If status_cbox.SelectedIndex = 5 Then 'completed
            Dim completed_by As Integer = 0
            Try
                completed_by = enq_ds.Tables(0).Rows(0).Item(11)
            Catch ex As Exception

            End Try
            Dim orig_completed_by_name As String = ""
            If completed_by <> 0 Then
                param2 = "select wo_name from welfare_officers where wo_code = " & completed_by
                Dim wo_ds As DataSet = get_dataset("complaints", param2)
                orig_completed_by_name = wo_ds.Tables(0).Rows(0).Item(0)
            End If
            If orig_completed_by_name <> comp_by_cbox.Text Then
                log_text = "Completed by changed from " & orig_completed_by_name & " to " &
                comp_by_cbox.Text
                write_log()
                change_found = True
            End If

            If enq_ds.Tables(0).Rows(0).Item(12) <> comp_date_dtp.Value Then
                log_text = "Completed date changed from " & Format(enq_ds.Tables(0).Rows(0).Item(12), "dd/MM/yyyy") & " to " &
                Format(comp_date_dtp.Value, "dd/MM/yyyy")
                write_log()
                change_found = True
            End If
        End If

        If status_cbox.SelectedIndex = 1 Then 'on hold
            Dim hold_reason As String = ""
            Try
                hold_reason = enq_ds.Tables(0).Rows(0).Item(13)
            Catch ex As Exception

            End Try
            If hold_reason <> hold_reason_tbox.Text Then
                log_text = "Hold reason changed from " & hold_reason & " to " &
                hold_reason_tbox.Text
                write_log()
                change_found = True
            End If

            If enq_ds.Tables(0).Rows(0).Item(14) <> hold_dtpicker.Value Then
                log_text = "Hold date changed from " & Format(enq_ds.Tables(0).Rows(0).Item(14), "dd/MM/yyyy") & " to " &
                Format(hold_dtpicker.Value, "dd/MM/yyyy")
                write_log()
                change_found = True
            End If
        End If

        If enq_ds.Tables(0).Rows(0).Item(15) <> actions_cbox.SelectedIndex Then
            log_text = "Action taken changed from " & enq_ds.Tables(0).Rows(0).Item(15) & " to " &
            actions_cbox.SelectedIndex
            write_log()
            change_found = True
        End If

        Dim arr_broken As Boolean
        Try
            arr_broken = enq_ds.Tables(0).Rows(0).Item(16)
        Catch ex As Exception
            arr_broken = False
        End Try
        If arr_broken <> arr_broken_chk.Checked Then
            log_text = "Arrangements broken changed to " & arr_broken_chk.Checked
            write_log()
            change_found = True
        End If

        If enq_ds.Tables(0).Rows(0).Item(18) <> upd_branch_cbox.SelectedIndex Then
            log_text = "Branch changed from " & enq_ds.Tables(0).Rows(0).Item(18) & " to " &
            upd_branch_cbox.SelectedIndex
            write_log()
            change_found = True
        End If

        Dim allocated_to As Integer
        If orig_alloc_to <> upd_alloc_to_cbox.Text Then
            log_text = "allocated to changed from " & orig_alloc_to & "to " & upd_alloc_to_cbox.Text
            write_log()
            change_found = True
            param2 = "select wo_code from Welfare_officers where wo_name = '" & upd_alloc_to_cbox.Text & "'"
            Dim wo_ds As DataSet = get_dataset("complaints", param2)
            If no_of_rows = 0 Then
                MsgBox("Contact IT error message WOD2")
                allocated_to = 0
            Else
                allocated_to = wo_ds.Tables(0).Rows(0).Item(0)
            End If
        Else
            allocated_to = 0
        End If

        If enq_ds.Tables(0).Rows(0).Item(17) <> contacts_ud.Value Then
            log_text = "No of contacts changed from " & enq_ds.Tables(0).Rows(0).Item(17) & " to " &
            contacts_ud.Value
            write_log()
            change_found = True
        Else
            If change_found Then
                If MsgBox("Increase number of contacts by 1?", MsgBoxStyle.YesNo, "Increase contact number?") = MsgBoxResult.Yes Then
                    contacts_ud.Value += 1
                End If
            End If
        End If

        If Not change_found Then
            MsgBox("No change has been made")
        Else
            Dim arr_broken_int As Integer = 0
            If arr_broken_chk.Checked Then
                arr_broken_int = 1
            End If

            upd_txt = "update Enquiries set enq_receipt_type = " & recpt_cbox.SelectedIndex &
            ",enq_date = '" & Format(enq_Date_dtp.Value, "dd/MMM/yyyy") &
            "', enq_recvd_from = " & recvd_from_cbox.SelectedIndex &
            ",enq_nature = " & nature_cbox.SelectedIndex + 17 &
            ", enq_case_no = " & case_no_tbox.Text &
            ", enq_details = '" & details &
            "', enq_status_no = " & status_cbox.SelectedIndex &
            ", enq_action_taken_no = " & actions_cbox.SelectedIndex &
            ", enq_arrangement_broken = " & arr_broken_int &
            ", enq_no_of_contacts = " & contacts_ud.Value &
            ", enq_branch_no = " & upd_branch_cbox.SelectedIndex &
            ", enq_allocated_to = " & allocated_to &
            "  where enq_no = " & search_enq_no
            update_sql(upd_txt)

            'update cab no if required
            Dim cab_no As Integer
            If orig_cab_name <> contact_cbox.Text Then
                param2 = "select cab_no from CAB_names where cab_name = '" & contact_cbox.Text & "'"
                Dim cab_ds As DataSet = get_dataset("complaints", param2)
                If no_of_rows = 0 Then
                    cab_no = 0
                Else
                    cab_no = cab_ds.Tables(0).Rows(0).Item(0)
                End If
            End If
            upd_txt = "update Enquiries set enq_cab_no = " & cab_no &
                "  where enq_no = " & search_enq_no
            update_sql(upd_txt)
            'update response if not null
            If response.Length <> 0 Then
                upd_txt = "update Enquiries set enq_response = '" & response &
                "'  where enq_no = " & search_enq_no
                update_sql(upd_txt)
            End If

            'update hold if required
            If status_cbox.SelectedIndex = 1 Then 'on hold
                upd_txt = "update Enquiries set enq_hold_reason = '" & hold_reason_tbox.Text &
                "', enq_hold_end_date = '" & Format(hold_dtpicker.Value, "dd/MMM/yyyy") & "'" &
                "  where enq_no = " & search_enq_no
                update_sql(upd_txt)
            End If

            'update completed by if required
            If status_cbox.SelectedIndex = 5 Then 'completed
                param2 = "select wo_code from welfare_officers where wo_name = '" & comp_by_cbox.Text & "'"
                Dim wo_ds As DataSet = get_dataset("complaints", param2)
                upd_txt = "update Enquiries set enq_completed_by = " & wo_ds.Tables(0).Rows(0).Item(0) &
                ", enq_completed_date = '" & Format(comp_date_dtp.Value, "dd/MMM/yyyy") & "'" &
                "  where enq_no = " & search_enq_no
                update_sql(upd_txt)
            End If

            MsgBox("Enquiry updated")
        End If
        Me.Close()
    End Sub
    Private Sub write_log()
        upd_txt = "insert into Enquiry_log (log_user, log_date, log_type, log_enq_no, log_text) values (" &
            login_no & ",'" & Format(Now, "dd/MMM/yyyy") & "'," & "'Update Enquiry'," & enq_no_tbox.Text & ",'" &
            log_text & "')"
        update_sql(upd_txt)


    End Sub

    Private Sub updatefrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        param2 = "select enq_no, enq_receipt_type,enq_date, enq_recvd_from, enq_nature, enq_case_no," &
                     " enq_recvd_by, enq_details, enq_response, enq_cab_no, enq_status_no, enq_completed_by," &
                     "enq_completed_date,enq_hold_reason,enq_hold_end_date,enq_action_taken_no,enq_arrangement_broken," &
                      "enq_no_of_contacts, enq_branch_no, enq_allocated_to, enq_advice_agency_no from Enquiries where enq_no = " & search_enq_no
        enq_ds = get_dataset("complaints", param2)
        enq_no_tbox.Text = search_enq_no
        enq_Date_dtp.Text = Format(enq_ds.Tables(0).Rows(0).Item(2), "dd/MM/yyyy")

        param2 = "select wo_name from Welfare_officers where wo_code = " & enq_ds.Tables(0).Rows(0).Item(6)
        Dim wo_ds As DataSet = get_dataset("complaints", param2)
        Try
            entered_by_tbox.Text = wo_ds.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            entered_by_tbox.Text = "unknown"
        End Try

        param2 = "select type_code, type_name from Enquiry_receipt_types where type_code < 3 order by type_code "
        Dim recpt_ds As DataSet = get_dataset("complaints", param2)
        recpt_cbox.Items.Clear()
        For idx = 0 To no_of_rows - 1
            recpt_cbox.Items.Add(recpt_ds.Tables(0).Rows(idx).Item(1))
        Next
        recpt_cbox.SelectedIndex = enq_ds.Tables(0).Rows(0).Item(1)

        contacts_ud.Value = enq_ds.Tables(0).Rows(0).Item(17)

        param2 = "select stat_code, stat_name from enquiry_status order by stat_code"
        Dim stat_ds As DataSet = get_dataset("complaints", param2)
        status_cbox.Items.Clear()
        For idx = 0 To no_of_rows - 1
            status_cbox.Items.Add(stat_ds.Tables(0).Rows(idx).Item(1))
        Next
        status_cbox.SelectedIndex = enq_ds.Tables(0).Rows(0).Item(10)
        If status_cbox.SelectedIndex = 1 Then 'on hold
            hold_lbl.Visible = True
            hold_reason_lbl.Visible = True
            hold_reason_tbox.Visible = True
            Try
                hold_reason_tbox.Text = enq_ds.Tables(0).Rows(0).Item(13)
            Catch ex As Exception
                hold_reason_tbox.Text = ""
            End Try
            hold_dtpicker.Visible = True
            hold_dtpicker.Value = enq_ds.Tables(0).Rows(0).Item(14)
            If hold_dtpicker.Value = null_date Then
                hold_dtpicker.Value = Now
            End If
        Else
            hold_dtpicker.Visible = False
            hold_lbl.Visible = False
            hold_reason_lbl.Visible = False
            hold_reason_tbox.Visible = False
        End If
        If status_cbox.SelectedIndex = 5 Then 'completed
            comp_by_cbox.Visible = True
            comp_by_lbl.Visible = True
            comp_date_dtp.Visible = True
            comp_date_lbl.Visible = True
            Try
                comp_date_dtp.Value = enq_ds.Tables(0).Rows(0).Item(12)
            Catch ex As Exception
                comp_date_dtp.Value = Now
            End Try
            param2 = "select wo_name from welfare_officers order by wo_code"
            comp_by_cbox.Items.Clear()
            Dim wo_ds2 As DataSet = get_dataset("complaints", param2)
            For idx = 0 To no_of_rows - 1
                comp_by_cbox.Items.Add(wo_ds2.Tables(0).Rows(idx).Item(0))
            Next
            Dim completed_by As Integer
            Try
                completed_by = enq_ds.Tables(0).Rows(0).Item(11)
            Catch ex As Exception
                completed_by = 0
            End Try
            If completed_by <> 0 Then
                param2 = "select wo_name from welfare_officers where wo_code = " & completed_by
                Dim wo_ds3 As DataSet = get_dataset("complaints", param2)
                comp_by_cbox.Text = wo_ds3.Tables(0).Rows(0).Item(0)
            End If
        Else
            comp_by_cbox.Visible = False
            comp_by_lbl.Visible = False
            comp_date_dtp.Visible = False
            comp_date_lbl.Visible = False
        End If

        param2 = "select nat_code, nat_name from Enquiry_nature_types where nat_code > 16 order by nat_code"
        Dim nat_ds As DataSet = get_dataset("complaints", param2)
        nature_cbox.Items.Clear()
        For idx = 0 To no_of_rows - 1
            nature_cbox.Items.Add(nat_ds.Tables(0).Rows(idx).Item(1))
        Next
        nature_cbox.SelectedIndex = enq_ds.Tables(0).Rows(0).Item(4) - 17

        param2 = "select recvd_from_code, recvd_from_name from Enquiry_recvd_from where recvd_from_code <>5 order by recvd_from_code"
        Dim recvd_ds As DataSet = get_dataset("complaints", param2)
        recvd_from_cbox.Items.Clear()
        For idx = 0 To no_of_rows - 1
            recvd_from_cbox.Items.Add(recvd_ds.Tables(0).Rows(idx).Item(1))
        Next

        recvd_from_cbox.SelectedIndex = enq_ds.Tables(0).Rows(0).Item(3)
        'If recvd_from_cbox.SelectedIndex = 0 Then 'Advice Agency
        '    agency_lbl.Text = "Advice Agency:"
        '    agency_lbl.Visible = True
        '    agency_cbox.Visible = True
        '    cabbtn.Enabled = False
        '    Dim adv_agency_no As Integer
        '    Try
        '        adv_agency_no = enq_ds.Tables(0).Rows(0).Item(20)
        '    Catch ex As Exception
        '        adv_agency_no = 0
        '    End Try
        '    param2 = "select adv_name from Advice_agencies where adv_no = " & adv_agency_no
        '    Dim adv_ds As DataSet = get_dataset("complaints", param2)
        '    orig_adv_name = ""
        '    If no_of_rows > 0 Then
        '        orig_adv_name = adv_ds.Tables(0).Rows(0).Item(0)
        '    End If
        '    agency_cbox.SelectedIndex = -1
        '    param2 = "select adv_name from Advice_agencies order by adv_name"
        '    Dim adv_ds2 As DataSet = get_dataset("complaints", param2)
        '    agency_cbox.Items.Clear()
        '    For idx = 0 To no_of_rows - 1
        '        agency_cbox.Items.Add(adv_ds2.Tables(0).Rows(idx).Item(0))
        '        If adv_ds2.Tables(0).Rows(idx).Item(0) = orig_adv_name Then
        '            agency_cbox.SelectedIndex = idx
        '        End If
        '    Next
        'agency_lbl.Text = "CAB:"
        'agency_lbl.Visible = True
        'agency_cbox.Visible = True
        'cabbtn.Enabled = True
        param2 = "select cab_name from CAB_names where cab_no = " & enq_ds.Tables(0).Rows(0).Item(9)
        Dim cab_ds As DataSet = get_dataset("complaints", param2)
        orig_cab_name = ""
        If no_of_rows > 0 Then
            orig_cab_name = cab_ds.Tables(0).Rows(0).Item(0)
        End If
        contact_cbox.SelectedIndex = -1
        param2 = "select cab_name from CAB_names order by cab_name"
        Dim cab_ds2 As DataSet = get_dataset("complaints", param2)
        contact_cbox.Items.Clear()
        For idx = 0 To no_of_rows - 1
            contact_cbox.Items.Add(cab_ds2.Tables(0).Rows(idx).Item(0))
            If cab_ds2.Tables(0).Rows(idx).Item(0) = orig_cab_name Then
                contact_cbox.SelectedIndex = idx
            End If
        Next
        'Else
        'contact_lbl.Visible = False
        'contact_cbox.Visible = False
        'cabbtn.Enabled = False
        'End If
        upd_branch_cbox.Items.Clear()
        param2 = "select branch_name from enquiry_branches order by branch_code "
        Dim br_ds As DataSet = get_dataset("complaints", param2)
        For idx = 0 To no_of_rows - 1
            upd_branch_cbox.Items.Add(br_ds.Tables(0).Rows(idx).Item(0))
        Next
        upd_branch_cbox.SelectedIndex = enq_ds.Tables(0).Rows(0).Item(18)

        Try
            details_tbox.Text = enq_ds.Tables(0).Rows(0).Item(7)
        Catch ex As Exception
            details_tbox.Text = ""
        End Try

        Try
            resp_tbox.Text = enq_ds.Tables(0).Rows(0).Item(8)
        Catch ex As Exception
            resp_tbox.Text = ""
        End Try

        param2 = "select act_name from enquiry_action_taken order by act_code"
        Dim act_ds As DataSet = get_dataset("complaints", param2)
        actions_cbox.Items.Clear()
        For idx = 0 To no_of_rows - 1
            actions_cbox.Items.Add(act_ds.Tables(0).Rows(idx).Item(0))
        Next
        actions_cbox.SelectedIndex = enq_ds.Tables(0).Rows(0).Item(15)
        Dim arr_broken As Boolean
        Try
            arr_broken = enq_ds.Tables(0).Rows(0).Item(16)
        Catch ex As Exception
            arr_broken = False
        End Try

        arr_broken_chk.Checked = arr_broken

        case_no_tbox.Text = enq_ds.Tables(0).Rows(0).Item(5)

        param2 = "select client_ref, clientSchemeID, name_sur from Debtor where _rowid = " & case_no_tbox.Text
        Dim case_ds As DataSet = get_dataset("onestep", param2)
        upd_surname_lbl.Text = "Surname:" & case_ds.Tables(0).Rows(0).Item(2)

        Dim csid As Integer = case_ds.Tables(0).Rows(0).Item(1)
        upd_cl_ref_lbl.Text = "Client Ref:" & case_ds.Tables(0).Rows(0).Item(0)
        param2 = "select clientID from clientScheme where _rowid = " & csid
        Dim csid_ds As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            upd_cl_name_tbox.Text = ""
            upd_branch_cbox.SelectedIndex = -1
        Else
            param2 = "select name from Client where _rowid = " & csid_ds.Tables(0).Rows(0).Item(0)
            Dim cl_ds As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 1 Then
                upd_cl_name_tbox.Text = cl_ds.Tables(0).Rows(0).Item(0)
            End If

        End If

        Dim allocated_to As Integer
        'get allocated to
        Try
            allocated_to = enq_ds.Tables(0).Rows(0).Item(19)
        Catch ex As Exception
            allocated_to = 0
        End Try

        'get welfare officers for allocation cbox
        param2 = "select wo_name, wo_code from Welfare_officers where wo_deleted = 0 order by wo_name"
        Dim wo_dataset As DataSet = get_dataset("complaints", param2)
        upd_alloc_to_cbox.Items.Clear()
        orig_alloc_to = ""
        For idx = 0 To no_of_rows - 1
            Dim wo_name As String = wo_dataset.Tables(0).Rows(idx).Item(0)
            upd_alloc_to_cbox.Items.Add(wo_name)
            If wo_dataset.Tables(0).Rows(idx).Item(1) = allocated_to Then
                upd_alloc_to_cbox.SelectedIndex = idx
                orig_alloc_to = wo_name
            End If
        Next

        If allocated_to = 0 Then
            upd_alloc_to_cbox.SelectedIndex = -1
        End If

    End Sub

    Private Sub status_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles status_cbox.SelectedIndexChanged
        If status_cbox.SelectedIndex = 1 Then 'on hold
            hold_lbl.Visible = True
            hold_reason_lbl.Visible = True
            hold_dtpicker.Visible = True
            hold_reason_tbox.Visible = True
            hold_reason_tbox.Text = ""
            hold_dtpicker.Value = DateAdd(DateInterval.Day, 14, Now)
        Else
            hold_lbl.Visible = False
            hold_reason_lbl.Visible = False
            hold_dtpicker.Visible = False
            hold_reason_tbox.Visible = False
        End If
        If status_cbox.SelectedIndex = 5 Then 'completed
            comp_by_cbox.Visible = True
            comp_by_lbl.Visible = True
            comp_date_dtp.Visible = True
            comp_date_lbl.Visible = True
            comp_date_dtp.Value = Now
            'get list of welfare officers
            param2 = "select wo_name from Welfare_officers where wo_deleted = 0 order by wo_code"
            Dim wo_dataset As DataSet = get_dataset("complaints", param2)
            comp_by_cbox.Items.Clear()
            For idx = 0 To no_of_rows - 1
                Dim wo_name As String = wo_dataset.Tables(0).Rows(idx).Item(0)
                comp_by_cbox.Items.Add(wo_name)
            Next
        Else
            comp_by_cbox.Visible = False
            comp_by_lbl.Visible = False
            comp_date_dtp.Visible = False
            comp_date_lbl.Visible = False
        End If
    End Sub

    Private Sub enq_DateTimePicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles enq_Date_dtp.Validating
        ErrorProvider1.SetError(enq_Date_dtp, "")
        If enq_Date_dtp.Text < DateAdd("d", -10, Now) Then
            ErrorProvider1.SetError(enq_Date_dtp, "Date can only go back 10 days")
            e.Cancel = True
        ElseIf enq_Date_dtp.Text > Now Then
            ErrorProvider1.SetError(enq_Date_dtp, "Date can't be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub enq_DateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles enq_Date_dtp.ValueChanged

    End Sub

    Private Sub recvd_from_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles recvd_from_cbox.SelectedIndexChanged
        'If recvd_from_cbox.SelectedIndex = 0 Then 'advice agency
        '    contact_lbl.Text = "Advice Agency:"
        '    contact_cbox.Visible = True
        '    contact_lbl.Visible = True
        '    contact_cbox.SelectedIndex = -1
        '    contact_cbox.Items.Clear()
        '    cabbtn.Enabled = False
        '    contact_cbox.SelectedIndex = -1
        '    param2 = "select adv_no, adv_name from Advice_agencies order by adv_name"
        '    Dim adv_ds As DataSet = get_dataset("complaints", param2)
        '    For idx = 0 To no_of_rows - 1
        '        contact_cbox.Items.Add(adv_ds.Tables(0).Rows(idx).Item(1))
        '    Next
        ' ElseIf recvd_from_cbox.SelectedIndex = 1 Then 'CAB
        'contact_lbl.Text = "CAB:"
        'contact_cbox.Visible = True
        'contact_lbl.Visible = True
        'contact_cbox.SelectedIndex = -1
        'contact_cbox.Items.Clear()
        'cabbtn.Enabled = True
        'contact_cbox.SelectedIndex = -1
        'param2 = "select cab_no, cab_name from CAB_names order by cab_name"
        'Dim cab_ds As DataSet = get_dataset("complaints", param2)
        'For idx = 0 To no_of_rows - 1
        '    contact_cbox.Items.Add(cab_ds.Tables(0).Rows(idx).Item(1))
        'Next
        'Else
        'contact_cbox.Visible = False
        'contact_lbl.Visible = False
        'cabbtn.Enabled = False
        'End If
    End Sub

    Private Sub case_no_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles case_no_tbox.TextChanged
        upd_cl_ref_lbl.Text = ""
        upd_cl_name_tbox.Text = ""
        upd_surname_lbl.Text = ""
        If case_no_tbox.TextLength < 4 Then
            Exit Sub
        End If
        If Not IsNumeric(case_no_tbox.Text) Then
            Exit Sub
        End If
        param2 = "select client_ref, clientSchemeID, name_sur, linkID from Debtor where _rowid = " & case_no_tbox.Text
        Dim case_ds As DataSet = get_dataset("onestep", param2)
        If no_of_rows = 0 Then
            ErrorProvider1.SetError(case_no_tbox, "This case is not on onestep")
            upd_cl_ref_lbl.Text = ""
            upd_cl_name_tbox.Text = ""
            upd_surname_lbl.Text = ""
            openLink_lbl.Text = ""
        Else
            ErrorProvider1.SetError(case_no_tbox, "")
            upd_surname_lbl.Text = "Surname:" & case_ds.Tables(0).Rows(0).Item(2)
            Dim csid As Integer = case_ds.Tables(0).Rows(0).Item(1)
            upd_cl_ref_lbl.Text = "Client Ref:" & case_ds.Tables(0).Rows(0).Item(0)
            param2 = "select clientID from clientScheme where _rowid = " & csid
            Dim csid_ds As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                upd_cl_name_tbox.Text = ""
            Else
                param2 = "select name from Client where _rowid = " & csid_ds.Tables(0).Rows(0).Item(0)
                Dim cl_ds As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 1 Then
                    upd_cl_name_tbox.Text = cl_ds.Tables(0).Rows(0).Item(0)
                End If
            End If
            'get number of open links
            Dim linkID As Integer = 0
            Try
                linkID = case_ds.Tables(0).Rows(0).Item(3)
            Catch ex As Exception

            End Try
            If linkID > 0 Then
                param2 = "select count(*) from Debtor where linkID = " & linkID & _
                    " and _rowID <> " & case_no_tbox.Text & _
                    " and status_open_closed = 'O'"
                Dim link_ds As DataSet = get_dataset("onestep", param2)
                openLink_lbl.Text = "Open Links:" & link_ds.Tables(0).Rows(0).Item(0)
            Else
                openLink_lbl.Text = "Open Links:0"
            End If
        End If
    End Sub

    Private Sub cab_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles contact_cbox.SelectedIndexChanged

    End Sub

    Private Sub cabbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cabbtn.Click
        Dim new_cab_name As String = Trim(InputBox("Enter new Contact name", "Contact entry"))
        If new_cab_name.Length = 0 Then
            Exit Sub
        End If
        'see if cab name already exists
        param2 = "select count(cab_no) from cab_names where cab_name = '" & new_cab_name & "'"
        Dim cab_ds2 As DataSet = get_dataset("complaints", param2)
        If cab_ds2.Tables(0).Rows(0).Item(0) > 0 Then
            MsgBox("This cab name already exists")
            Exit Sub
        End If
        Try
            upd_txt = "insert into CAB_names (cab_name) values ('" & new_cab_name & "')"
            update_sql(upd_txt)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        contact_cbox.Items.Clear()
        contact_cbox.SelectedIndex = -1
        param2 = "select cab_no, cab_name from CAB_names order by cab_name"
        Dim cab_ds As DataSet = get_dataset("complaints", param2)
        For idx = 0 To no_of_rows - 1
            contact_cbox.Items.Add(cab_ds.Tables(0).Rows(idx).Item(1))
        Next
    End Sub

    Private Sub hold_dtpicker_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles hold_dtpicker.Validating
        ErrorProvider1.SetError(hold_dtpicker, "")
        If Format(hold_dtpicker.Value, "yyyy-MM-dd") <= Format(Now, "yyyy-MM-dd") Then
            ErrorProvider1.SetError(hold_dtpicker, "hold date must be in the future")
            e.Cancel = True
        End If
    End Sub

    Private Sub hold_dtpicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hold_dtpicker.ValueChanged

    End Sub

    Private Sub actions_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles actions_cbox.SelectedIndexChanged

    End Sub

    Private Sub compbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        status_cbox.SelectedIndex = 5
        comp_by_cbox.Visible = True
        comp_by_lbl.Visible = True
        comp_date_dtp.Visible = True
        comp_date_dtp.Value = Now
        comp_date_lbl.Visible = True
        'get list of welfare officers
        param2 = "select wo_name from Welfare_officers where wo_deleted = 0 order by wo_code"
        Dim wo_dataset As DataSet = get_dataset("complaints", param2)
        comp_by_cbox.Items.Clear()
        For idx = 0 To no_of_rows - 1
            Dim wo_name As String = wo_dataset.Tables(0).Rows(idx).Item(0)
            comp_by_cbox.Items.Add(wo_name)
        Next
    End Sub

    Private Sub recpt_cbox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles recpt_cbox.SelectedIndexChanged

    End Sub

    Private Sub upd_alloc_to_cbox_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles upd_alloc_to_cbox.SelectedIndexChanged

    End Sub
End Class
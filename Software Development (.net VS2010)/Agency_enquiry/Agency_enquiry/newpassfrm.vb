﻿Public Class newpassfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub savebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles savebtn.Click
        If pw1_tbox.Text.Length < 6 Then
            MsgBox("Password should be at least 6 characters long")
            Exit Sub
        End If
        If pw1_tbox.Text <> pw2_tbox.Text Then
            MsgBox("Passwords are not the same")
            Exit Sub
        End If
        pw_reset = True
        Me.Close()
    End Sub

    Private Sub resetfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
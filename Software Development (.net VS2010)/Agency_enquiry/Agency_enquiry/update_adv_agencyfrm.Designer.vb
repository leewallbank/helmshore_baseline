﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class update_adv_agencyfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.adv_agencydg = New System.Windows.Forms.DataGridView()
        Me.agency_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.agency_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.adv_agencydg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'adv_agencydg
        '
        Me.adv_agencydg.AllowUserToAddRows = False
        Me.adv_agencydg.AllowUserToDeleteRows = False
        Me.adv_agencydg.AllowUserToOrderColumns = True
        Me.adv_agencydg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.adv_agencydg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.agency_no, Me.agency_name})
        Me.adv_agencydg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.adv_agencydg.Location = New System.Drawing.Point(0, 0)
        Me.adv_agencydg.Name = "adv_agencydg"
        Me.adv_agencydg.Size = New System.Drawing.Size(322, 266)
        Me.adv_agencydg.TabIndex = 0
        '
        'agency_no
        '
        Me.agency_no.HeaderText = "No"
        Me.agency_no.Name = "agency_no"
        Me.agency_no.Visible = False
        '
        'agency_name
        '
        Me.agency_name.HeaderText = "Agency"
        Me.agency_name.Name = "agency_name"
        Me.agency_name.Width = 250
        '
        'update_adv_agencyfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(322, 266)
        Me.Controls.Add(Me.adv_agencydg)
        Me.Name = "update_adv_agencyfrm"
        Me.Text = "Update Advice Agencies"
        CType(Me.adv_agencydg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents adv_agencydg As System.Windows.Forms.DataGridView
    Friend WithEvents agency_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents agency_name As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿Public Class resetpassfrm
    Dim wo_ds As DataSet
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub resetpassfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        param2 = "select wo_code, wo_name from welfare_officers where wo_deleted = 0 order by wo_name"
        wo_ds = get_dataset("complaints", param2)
        wo_cbox.Items.Clear()
        For idx = 0 To no_of_rows - 1
            wo_cbox.Items.Add(wo_ds.Tables(0).Rows(idx).Item(1))
        Next
    End Sub

    Private Sub resetbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles resetbtn.Click
        If MsgBox("Reset password for " & wo_cbox.Text, MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            upd_txt = "update welfare_officers set wo_password = 'password' where wo_name = '" & wo_cbox.Text & "'"
            update_sql(upd_txt)
            MsgBox("password has been reset to 'password'")
        Else
            MsgBox("Password NOT reset")
        End If
        Me.Close()
    End Sub
End Class
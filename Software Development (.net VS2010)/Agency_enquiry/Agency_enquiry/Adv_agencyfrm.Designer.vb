﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Adv_agencyfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.adv_agency_dg = New System.Windows.Forms.DataGridView()
        Me.adv_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.adv_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.adv_agency_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'adv_agency_dg
        '
        Me.adv_agency_dg.AllowUserToAddRows = False
        Me.adv_agency_dg.AllowUserToDeleteRows = False
        Me.adv_agency_dg.AllowUserToOrderColumns = True
        Me.adv_agency_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.adv_agency_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.adv_no, Me.adv_name})
        Me.adv_agency_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.adv_agency_dg.Location = New System.Drawing.Point(0, 0)
        Me.adv_agency_dg.Name = "adv_agency_dg"
        Me.adv_agency_dg.ReadOnly = True
        Me.adv_agency_dg.Size = New System.Drawing.Size(381, 266)
        Me.adv_agency_dg.TabIndex = 0
        '
        'adv_no
        '
        Me.adv_no.HeaderText = "ADV No"
        Me.adv_no.Name = "adv_no"
        Me.adv_no.ReadOnly = True
        Me.adv_no.Visible = False
        '
        'adv_name
        '
        Me.adv_name.HeaderText = "Advice Agency"
        Me.adv_name.Name = "adv_name"
        Me.adv_name.ReadOnly = True
        Me.adv_name.Width = 250
        '
        'Adv_agencyfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(381, 266)
        Me.Controls.Add(Me.adv_agency_dg)
        Me.Name = "Adv_agencyfrm"
        Me.Text = "Double-click to select agency"
        CType(Me.adv_agency_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents adv_agency_dg As System.Windows.Forms.DataGridView
    Friend WithEvents adv_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents adv_name As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class disp3frm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim Label4 As System.Windows.Forms.Label
        Dim Label5 As System.Windows.Forms.Label
        Dim Label6 As System.Windows.Forms.Label
        Dim Label7 As System.Windows.Forms.Label
        Dim Label8 As System.Windows.Forms.Label
        Dim Label9 As System.Windows.Forms.Label
        Dim Label10 As System.Windows.Forms.Label
        Dim Label11 As System.Windows.Forms.Label
        Dim actions_lbl As System.Windows.Forms.Label
        Me.enq_no_tbox = New System.Windows.Forms.TextBox()
        Me.enq_date = New System.Windows.Forms.TextBox()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.entered_by_tbox = New System.Windows.Forms.TextBox()
        Me.recvd_from_tbox = New System.Windows.Forms.TextBox()
        Me.nature_tbox = New System.Windows.Forms.TextBox()
        Me.recpt_tbox = New System.Windows.Forms.TextBox()
        Me.branch_tbox = New System.Windows.Forms.TextBox()
        Me.disp_cl_name_tbox = New System.Windows.Forms.TextBox()
        Me.case_no_tbox = New System.Windows.Forms.TextBox()
        Me.details_tbox = New System.Windows.Forms.TextBox()
        Me.disp_surname_lbl = New System.Windows.Forms.Label()
        Me.disp_cl_ref_lbl = New System.Windows.Forms.Label()
        Me.error_lbl = New System.Windows.Forms.Label()
        Me.response_tbox = New System.Windows.Forms.TextBox()
        Me.contact_tbox = New System.Windows.Forms.TextBox()
        Me.contact_lbl = New System.Windows.Forms.Label()
        Me.status_tbox = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.hold_tbox = New System.Windows.Forms.TextBox()
        Me.hold_lbl = New System.Windows.Forms.Label()
        Me.hold_reason_lbl = New System.Windows.Forms.Label()
        Me.hold_reason_tbox = New System.Windows.Forms.TextBox()
        Me.contacts_tbox = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.comp_by_tbox = New System.Windows.Forms.TextBox()
        Me.actions_tbox = New System.Windows.Forms.TextBox()
        Me.comp_date_tbox = New System.Windows.Forms.TextBox()
        Me.comp_by_lbl = New System.Windows.Forms.Label()
        Me.comp_date_lbl = New System.Windows.Forms.Label()
        Me.arr_broken_chk = New System.Windows.Forms.CheckBox()
        Me.updbtn = New System.Windows.Forms.Button()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.alloc_to_tbox = New System.Windows.Forms.TextBox()
        Me.openLink_lbl = New System.Windows.Forms.Label()
        Label4 = New System.Windows.Forms.Label()
        Label5 = New System.Windows.Forms.Label()
        Label6 = New System.Windows.Forms.Label()
        Label7 = New System.Windows.Forms.Label()
        Label8 = New System.Windows.Forms.Label()
        Label9 = New System.Windows.Forms.Label()
        Label10 = New System.Windows.Forms.Label()
        Label11 = New System.Windows.Forms.Label()
        actions_lbl = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label4
        '
        Label4.AutoSize = True
        Label4.Location = New System.Drawing.Point(315, 129)
        Label4.Name = "Label4"
        Label4.Size = New System.Drawing.Size(79, 13)
        Label4.TabIndex = 23
        Label4.Text = "Received from:"
        '
        'Label5
        '
        Label5.AutoSize = True
        Label5.Location = New System.Drawing.Point(150, 129)
        Label5.Name = "Label5"
        Label5.Size = New System.Drawing.Size(92, 13)
        Label5.TabIndex = 21
        Label5.Text = "Nature of Enquiry:"
        '
        'Label6
        '
        Label6.AutoSize = True
        Label6.Location = New System.Drawing.Point(29, 129)
        Label6.Name = "Label6"
        Label6.Size = New System.Drawing.Size(74, 13)
        Label6.TabIndex = 19
        Label6.Text = "Receipt Type:"
        '
        'Label7
        '
        Label7.AutoSize = True
        Label7.Location = New System.Drawing.Point(240, 191)
        Label7.Name = "Label7"
        Label7.Size = New System.Drawing.Size(36, 13)
        Label7.TabIndex = 29
        Label7.Text = "Client:"
        '
        'Label8
        '
        Label8.AutoSize = True
        Label8.Location = New System.Drawing.Point(454, 191)
        Label8.Name = "Label8"
        Label8.Size = New System.Drawing.Size(44, 13)
        Label8.TabIndex = 28
        Label8.Text = "Branch:"
        '
        'Label9
        '
        Label9.AutoSize = True
        Label9.Location = New System.Drawing.Point(29, 191)
        Label9.Name = "Label9"
        Label9.Size = New System.Drawing.Size(51, 13)
        Label9.TabIndex = 27
        Label9.Text = "Case No:"
        '
        'Label10
        '
        Label10.AutoSize = True
        Label10.Location = New System.Drawing.Point(115, 261)
        Label10.Name = "Label10"
        Label10.Size = New System.Drawing.Size(42, 13)
        Label10.TabIndex = 34
        Label10.Text = "Details:"
        '
        'Label11
        '
        Label11.AutoSize = True
        Label11.Location = New System.Drawing.Point(467, 261)
        Label11.Name = "Label11"
        Label11.Size = New System.Drawing.Size(58, 13)
        Label11.TabIndex = 41
        Label11.Text = "Response:"
        '
        'actions_lbl
        '
        actions_lbl.AutoSize = True
        actions_lbl.Location = New System.Drawing.Point(298, 403)
        actions_lbl.Name = "actions_lbl"
        actions_lbl.Size = New System.Drawing.Size(75, 13)
        actions_lbl.TabIndex = 68
        actions_lbl.Text = "Actions taken:"
        '
        'enq_no_tbox
        '
        Me.enq_no_tbox.Location = New System.Drawing.Point(12, 46)
        Me.enq_no_tbox.Name = "enq_no_tbox"
        Me.enq_no_tbox.Size = New System.Drawing.Size(100, 20)
        Me.enq_no_tbox.TabIndex = 0
        '
        'enq_date
        '
        Me.enq_date.Location = New System.Drawing.Point(141, 46)
        Me.enq_date.Name = "enq_date"
        Me.enq_date.ReadOnly = True
        Me.enq_date.Size = New System.Drawing.Size(114, 20)
        Me.enq_date.TabIndex = 1
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(763, 468)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(41, 30)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Enquiry No:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(172, 30)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(70, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Date Entered"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(315, 30)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(62, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "Entered By:"
        '
        'entered_by_tbox
        '
        Me.entered_by_tbox.Location = New System.Drawing.Point(287, 46)
        Me.entered_by_tbox.Name = "entered_by_tbox"
        Me.entered_by_tbox.ReadOnly = True
        Me.entered_by_tbox.Size = New System.Drawing.Size(114, 20)
        Me.entered_by_tbox.TabIndex = 6
        '
        'recvd_from_tbox
        '
        Me.recvd_from_tbox.Location = New System.Drawing.Point(287, 145)
        Me.recvd_from_tbox.Name = "recvd_from_tbox"
        Me.recvd_from_tbox.ReadOnly = True
        Me.recvd_from_tbox.Size = New System.Drawing.Size(141, 20)
        Me.recvd_from_tbox.TabIndex = 26
        '
        'nature_tbox
        '
        Me.nature_tbox.Location = New System.Drawing.Point(141, 145)
        Me.nature_tbox.Name = "nature_tbox"
        Me.nature_tbox.ReadOnly = True
        Me.nature_tbox.Size = New System.Drawing.Size(114, 20)
        Me.nature_tbox.TabIndex = 25
        '
        'recpt_tbox
        '
        Me.recpt_tbox.Location = New System.Drawing.Point(12, 145)
        Me.recpt_tbox.Name = "recpt_tbox"
        Me.recpt_tbox.ReadOnly = True
        Me.recpt_tbox.Size = New System.Drawing.Size(100, 20)
        Me.recpt_tbox.TabIndex = 24
        '
        'branch_tbox
        '
        Me.branch_tbox.Location = New System.Drawing.Point(374, 207)
        Me.branch_tbox.Name = "branch_tbox"
        Me.branch_tbox.ReadOnly = True
        Me.branch_tbox.Size = New System.Drawing.Size(195, 20)
        Me.branch_tbox.TabIndex = 32
        '
        'disp_cl_name_tbox
        '
        Me.disp_cl_name_tbox.Location = New System.Drawing.Point(141, 207)
        Me.disp_cl_name_tbox.Name = "disp_cl_name_tbox"
        Me.disp_cl_name_tbox.ReadOnly = True
        Me.disp_cl_name_tbox.Size = New System.Drawing.Size(212, 20)
        Me.disp_cl_name_tbox.TabIndex = 31
        '
        'case_no_tbox
        '
        Me.case_no_tbox.Location = New System.Drawing.Point(12, 207)
        Me.case_no_tbox.Name = "case_no_tbox"
        Me.case_no_tbox.ReadOnly = True
        Me.case_no_tbox.Size = New System.Drawing.Size(100, 20)
        Me.case_no_tbox.TabIndex = 30
        '
        'details_tbox
        '
        Me.details_tbox.Location = New System.Drawing.Point(12, 277)
        Me.details_tbox.Multiline = True
        Me.details_tbox.Name = "details_tbox"
        Me.details_tbox.ReadOnly = True
        Me.details_tbox.Size = New System.Drawing.Size(313, 110)
        Me.details_tbox.TabIndex = 33
        '
        'disp_surname_lbl
        '
        Me.disp_surname_lbl.AutoSize = True
        Me.disp_surname_lbl.Location = New System.Drawing.Point(160, 230)
        Me.disp_surname_lbl.Name = "disp_surname_lbl"
        Me.disp_surname_lbl.Size = New System.Drawing.Size(52, 13)
        Me.disp_surname_lbl.TabIndex = 37
        Me.disp_surname_lbl.Text = "Surname:"
        '
        'disp_cl_ref_lbl
        '
        Me.disp_cl_ref_lbl.AutoSize = True
        Me.disp_cl_ref_lbl.Location = New System.Drawing.Point(12, 230)
        Me.disp_cl_ref_lbl.Name = "disp_cl_ref_lbl"
        Me.disp_cl_ref_lbl.Size = New System.Drawing.Size(56, 13)
        Me.disp_cl_ref_lbl.TabIndex = 38
        Me.disp_cl_ref_lbl.Text = "Client Ref:"
        '
        'error_lbl
        '
        Me.error_lbl.AutoSize = True
        Me.error_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.error_lbl.Location = New System.Drawing.Point(12, 69)
        Me.error_lbl.Name = "error_lbl"
        Me.error_lbl.Size = New System.Drawing.Size(33, 13)
        Me.error_lbl.TabIndex = 39
        Me.error_lbl.Text = "error"
        '
        'response_tbox
        '
        Me.response_tbox.Location = New System.Drawing.Point(364, 277)
        Me.response_tbox.Multiline = True
        Me.response_tbox.Name = "response_tbox"
        Me.response_tbox.ReadOnly = True
        Me.response_tbox.Size = New System.Drawing.Size(313, 110)
        Me.response_tbox.TabIndex = 40
        '
        'contact_tbox
        '
        Me.contact_tbox.Location = New System.Drawing.Point(457, 145)
        Me.contact_tbox.Name = "contact_tbox"
        Me.contact_tbox.ReadOnly = True
        Me.contact_tbox.Size = New System.Drawing.Size(154, 20)
        Me.contact_tbox.TabIndex = 42
        '
        'contact_lbl
        '
        Me.contact_lbl.AutoSize = True
        Me.contact_lbl.Location = New System.Drawing.Point(494, 129)
        Me.contact_lbl.Name = "contact_lbl"
        Me.contact_lbl.Size = New System.Drawing.Size(47, 13)
        Me.contact_lbl.TabIndex = 43
        Me.contact_lbl.Text = "Contact:"
        '
        'status_tbox
        '
        Me.status_tbox.Location = New System.Drawing.Point(447, 46)
        Me.status_tbox.Name = "status_tbox"
        Me.status_tbox.ReadOnly = True
        Me.status_tbox.Size = New System.Drawing.Size(141, 20)
        Me.status_tbox.TabIndex = 44
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(506, 30)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(40, 13)
        Me.Label12.TabIndex = 45
        Me.Label12.Text = "Status:"
        '
        'hold_tbox
        '
        Me.hold_tbox.Location = New System.Drawing.Point(631, 46)
        Me.hold_tbox.Name = "hold_tbox"
        Me.hold_tbox.ReadOnly = True
        Me.hold_tbox.Size = New System.Drawing.Size(111, 20)
        Me.hold_tbox.TabIndex = 46
        '
        'hold_lbl
        '
        Me.hold_lbl.AutoSize = True
        Me.hold_lbl.Location = New System.Drawing.Point(661, 30)
        Me.hold_lbl.Name = "hold_lbl"
        Me.hold_lbl.Size = New System.Drawing.Size(69, 13)
        Me.hold_lbl.TabIndex = 47
        Me.hold_lbl.Text = "Hold Expires:"
        '
        'hold_reason_lbl
        '
        Me.hold_reason_lbl.AutoSize = True
        Me.hold_reason_lbl.Location = New System.Drawing.Point(670, 69)
        Me.hold_reason_lbl.Name = "hold_reason_lbl"
        Me.hold_reason_lbl.Size = New System.Drawing.Size(72, 13)
        Me.hold_reason_lbl.TabIndex = 60
        Me.hold_reason_lbl.Text = "Hold Reason:"
        '
        'hold_reason_tbox
        '
        Me.hold_reason_tbox.Location = New System.Drawing.Point(631, 85)
        Me.hold_reason_tbox.Name = "hold_reason_tbox"
        Me.hold_reason_tbox.ReadOnly = True
        Me.hold_reason_tbox.Size = New System.Drawing.Size(171, 20)
        Me.hold_reason_tbox.TabIndex = 59
        '
        'contacts_tbox
        '
        Me.contacts_tbox.Location = New System.Drawing.Point(630, 207)
        Me.contacts_tbox.Name = "contacts_tbox"
        Me.contacts_tbox.ReadOnly = True
        Me.contacts_tbox.Size = New System.Drawing.Size(100, 20)
        Me.contacts_tbox.TabIndex = 61
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(649, 191)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(81, 13)
        Me.Label13.TabIndex = 62
        Me.Label13.Text = "No of Contacts:"
        '
        'comp_by_tbox
        '
        Me.comp_by_tbox.Location = New System.Drawing.Point(44, 419)
        Me.comp_by_tbox.Name = "comp_by_tbox"
        Me.comp_by_tbox.ReadOnly = True
        Me.comp_by_tbox.Size = New System.Drawing.Size(100, 20)
        Me.comp_by_tbox.TabIndex = 63
        '
        'actions_tbox
        '
        Me.actions_tbox.Location = New System.Drawing.Point(277, 419)
        Me.actions_tbox.Name = "actions_tbox"
        Me.actions_tbox.ReadOnly = True
        Me.actions_tbox.Size = New System.Drawing.Size(173, 20)
        Me.actions_tbox.TabIndex = 64
        '
        'comp_date_tbox
        '
        Me.comp_date_tbox.Location = New System.Drawing.Point(44, 468)
        Me.comp_date_tbox.Name = "comp_date_tbox"
        Me.comp_date_tbox.ReadOnly = True
        Me.comp_date_tbox.Size = New System.Drawing.Size(100, 20)
        Me.comp_date_tbox.TabIndex = 65
        '
        'comp_by_lbl
        '
        Me.comp_by_lbl.AutoSize = True
        Me.comp_by_lbl.Location = New System.Drawing.Point(70, 403)
        Me.comp_by_lbl.Name = "comp_by_lbl"
        Me.comp_by_lbl.Size = New System.Drawing.Size(51, 13)
        Me.comp_by_lbl.TabIndex = 69
        Me.comp_by_lbl.Text = "Comp by:"
        '
        'comp_date_lbl
        '
        Me.comp_date_lbl.AutoSize = True
        Me.comp_date_lbl.Location = New System.Drawing.Point(70, 452)
        Me.comp_date_lbl.Name = "comp_date_lbl"
        Me.comp_date_lbl.Size = New System.Drawing.Size(63, 13)
        Me.comp_date_lbl.TabIndex = 70
        Me.comp_date_lbl.Text = "Comp Date:"
        '
        'arr_broken_chk
        '
        Me.arr_broken_chk.AutoSize = True
        Me.arr_broken_chk.Enabled = False
        Me.arr_broken_chk.Location = New System.Drawing.Point(497, 419)
        Me.arr_broken_chk.Name = "arr_broken_chk"
        Me.arr_broken_chk.Size = New System.Drawing.Size(127, 17)
        Me.arr_broken_chk.TabIndex = 73
        Me.arr_broken_chk.Text = "Arrangements broken"
        Me.arr_broken_chk.UseVisualStyleBackColor = True
        Me.arr_broken_chk.Visible = False
        '
        'updbtn
        '
        Me.updbtn.Location = New System.Drawing.Point(715, 302)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(123, 23)
        Me.updbtn.TabIndex = 74
        Me.updbtn.Text = "Update Enquiry"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(676, 129)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(66, 13)
        Me.Label14.TabIndex = 75
        Me.Label14.Text = "Allocated to:"
        '
        'alloc_to_tbox
        '
        Me.alloc_to_tbox.Location = New System.Drawing.Point(654, 145)
        Me.alloc_to_tbox.Name = "alloc_to_tbox"
        Me.alloc_to_tbox.ReadOnly = True
        Me.alloc_to_tbox.Size = New System.Drawing.Size(148, 20)
        Me.alloc_to_tbox.TabIndex = 76
        '
        'openLink_lbl
        '
        Me.openLink_lbl.AutoSize = True
        Me.openLink_lbl.Location = New System.Drawing.Point(9, 252)
        Me.openLink_lbl.Name = "openLink_lbl"
        Me.openLink_lbl.Size = New System.Drawing.Size(64, 13)
        Me.openLink_lbl.TabIndex = 77
        Me.openLink_lbl.Text = "Open Links:"
        '
        'disp3frm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(858, 523)
        Me.Controls.Add(Me.openLink_lbl)
        Me.Controls.Add(Me.alloc_to_tbox)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.updbtn)
        Me.Controls.Add(Me.arr_broken_chk)
        Me.Controls.Add(Me.comp_date_lbl)
        Me.Controls.Add(Me.comp_by_lbl)
        Me.Controls.Add(actions_lbl)
        Me.Controls.Add(Me.comp_date_tbox)
        Me.Controls.Add(Me.actions_tbox)
        Me.Controls.Add(Me.comp_by_tbox)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.contacts_tbox)
        Me.Controls.Add(Me.hold_reason_lbl)
        Me.Controls.Add(Me.hold_reason_tbox)
        Me.Controls.Add(Me.hold_lbl)
        Me.Controls.Add(Me.hold_tbox)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.status_tbox)
        Me.Controls.Add(Me.contact_lbl)
        Me.Controls.Add(Me.contact_tbox)
        Me.Controls.Add(Label11)
        Me.Controls.Add(Me.response_tbox)
        Me.Controls.Add(Me.error_lbl)
        Me.Controls.Add(Me.disp_cl_ref_lbl)
        Me.Controls.Add(Me.disp_surname_lbl)
        Me.Controls.Add(Label10)
        Me.Controls.Add(Me.details_tbox)
        Me.Controls.Add(Me.branch_tbox)
        Me.Controls.Add(Me.disp_cl_name_tbox)
        Me.Controls.Add(Me.case_no_tbox)
        Me.Controls.Add(Label7)
        Me.Controls.Add(Label8)
        Me.Controls.Add(Label9)
        Me.Controls.Add(Me.recvd_from_tbox)
        Me.Controls.Add(Me.nature_tbox)
        Me.Controls.Add(Me.recpt_tbox)
        Me.Controls.Add(Label4)
        Me.Controls.Add(Label5)
        Me.Controls.Add(Label6)
        Me.Controls.Add(Me.entered_by_tbox)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.enq_date)
        Me.Controls.Add(Me.enq_no_tbox)
        Me.Name = "disp3frm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Display Enquiry"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents enq_no_tbox As System.Windows.Forms.TextBox
    Friend WithEvents enq_date As System.Windows.Forms.TextBox
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents entered_by_tbox As System.Windows.Forms.TextBox
    Friend WithEvents recvd_from_tbox As System.Windows.Forms.TextBox
    Friend WithEvents nature_tbox As System.Windows.Forms.TextBox
    Friend WithEvents recpt_tbox As System.Windows.Forms.TextBox
    Friend WithEvents branch_tbox As System.Windows.Forms.TextBox
    Friend WithEvents disp_cl_name_tbox As System.Windows.Forms.TextBox
    Friend WithEvents case_no_tbox As System.Windows.Forms.TextBox
    Friend WithEvents details_tbox As System.Windows.Forms.TextBox
    Friend WithEvents disp_surname_lbl As System.Windows.Forms.Label
    Friend WithEvents disp_cl_ref_lbl As System.Windows.Forms.Label
    Friend WithEvents error_lbl As System.Windows.Forms.Label
    Friend WithEvents response_tbox As System.Windows.Forms.TextBox
    Friend WithEvents contact_tbox As System.Windows.Forms.TextBox
    Friend WithEvents contact_lbl As System.Windows.Forms.Label
    Friend WithEvents status_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents hold_tbox As System.Windows.Forms.TextBox
    Friend WithEvents hold_lbl As System.Windows.Forms.Label
    Friend WithEvents hold_reason_lbl As System.Windows.Forms.Label
    Friend WithEvents hold_reason_tbox As System.Windows.Forms.TextBox
    Friend WithEvents contacts_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents comp_by_tbox As System.Windows.Forms.TextBox
    Friend WithEvents actions_tbox As System.Windows.Forms.TextBox
    Friend WithEvents comp_date_tbox As System.Windows.Forms.TextBox
    Friend WithEvents comp_by_lbl As System.Windows.Forms.Label
    Friend WithEvents comp_date_lbl As System.Windows.Forms.Label
    Friend WithEvents arr_broken_chk As System.Windows.Forms.CheckBox
    Friend WithEvents updbtn As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents alloc_to_tbox As System.Windows.Forms.TextBox
    Friend WithEvents openLink_lbl As System.Windows.Forms.Label
End Class

﻿Imports CommonLibrary
Imports System.Configuration
Imports System.Xml.Schema
Imports System.IO

Public Class Form1
    Dim xml_valid As Boolean = True
    Dim auditFile As String = ""
    Dim error_no As Integer = 0
    Dim case_no As Integer = 0
    Dim prod_run As Boolean = False
    Dim fileDate As Date = Now
    Dim street1, street2, street3, town, city, postcode As String
    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        run_report()
    End Sub
    Private Sub run_report()
        ' <add name="DebtRecovery" connectionString= "Driver={MySql ODBC 3.51 Driver};Server=onestepdb-rep;Port=3306;Stmt=;DataBase=DebtRecovery;Uid=crystal;Pwd=latsyrc;" />
        ' <add name="DebtRecovery" connectionString= "Driver={MySql ODBC 5.3 ANSI Driver};Server=onestepdb-rep;Port=3306;Stmt=;DataBase=DebtRecovery;Uid=crystal;Pwd=latsyrc;" />
        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("StudentLoans")
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If
        Dim filename As String = "ADFRossendalesHMRC" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".xml"
        Dim activity_file As String = "\\ross-helm-fp001\Rossendales Shared\TDX Reports\" & filename
        auditFile = "\\ross-helm-fp001\Rossendales Shared\TDX Reports\TDX_Activity_audit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        If Not prod_run Then
            activity_file = "C:\AAtemp\" & filename
            auditFile = "C:\AAtemp\TDX_Activity_audit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        End If

        write_audit("report started at " & Now, False)
        Dim writer As New Xml.XmlTextWriter(activity_file, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)
        writer.Formatting = Xml.Formatting.Indented
        writer.WriteStartElement("ActvtyData")
        writer.WriteAttributeString("schemaVersion", "1.0")
        writer.WriteAttributeString("xmlns", "ActivityData")
        writer.WriteStartElement("MsgHdr")
        writer.WriteAttributeString("xmlns", "")
        writer.WriteElementString("MsgTp", "ADF")
        writer.WriteElementString("FlNm", filename)
        writer.WriteElementString("FlDt", Format(fileDate, "yyyy-MM-dd") & "T" & Format(fileDate, "HH:mm:ss"))
        writer.WriteElementString("DCACd", "Rossendales")
        writer.WriteEndElement()  'MsgHdr
        writer.WriteStartElement("Accts")
        writer.WriteAttributeString("xmlns", "")

        Dim startDate As Date = DateAdd(DateInterval.Day, -Weekday(Now) - 6, Now)
        startDate = CDate(Format(startDate, "MMM d, yyyy" & " 00:00:00"))
        Dim endDate As Date = DateAdd(DateInterval.Day, 8, startDate)

        'T56273 make daily
        startDate = DateAdd(DateInterval.Day, -1, Now)
        startDate = CDate(Format(startDate, "MMM d, yyyy" & " 00:00:00"))
        endDate = Now
        endDate = CDate(Format(endDate, "MMM d, yyyy" & " 00:00:00"))

        'T66658 Sundays run is at 4pm and should include Friday to <4pm Sunday
        'Tuesday run should include Sunday 4pm and Monday
        'Weds to Friday are previous day

        If Weekday(Now) = 1 Then  'Sunday
            startDate = DateAdd(DateInterval.Day, -2, Now)
            startDate = CDate(Format(startDate, "MMM d, yyyy" & " 00:00:00"))
            endDate = CDate(Format(Now, "MMM") & " " & Format(Now, "dd") & ", " & Year(Now) & " 16:00:00")
        ElseIf Weekday(Now) = 3 Then  'Tuesday
            startDate = DateAdd(DateInterval.Day, -2, Now)
            startDate = CDate(Format(startDate, "MMM") & " " & Format(startDate, "dd") & ", " & Year(startDate) & " 16:00:00")
        End If

        'if 3rd Jan 2016 go back a further day
        If Format(endDate, "yyyy-MM-dd") = Format(CDate("2016-01-03"), "yyyy-MM-dd") Then
            startDate = DateAdd(DateInterval.Day, -1, startDate)
        End If

        Dim clientID As Integer = 1736
        If Not prod_run Then
            'clientID = 24
            ' startDate = CDate("2015-12-22")
            'endDate = CDate("2015-12-29")
        End If
        Dim clientCode As String = "HMRC"
        'get all CSIDs for ETC HMRC
        'Dim cs_dt As New DataTable
        'T54828 just include client 1736
        'T59930 branch 26 now also required
        'so include all branch 23 and 26 except 1324 (HHBD)
        'T61034 now only branch 23
        'LoadDataTable2("DebtRecovery", "select _rowid " & _
        '              " from clientScheme" & _
        '              " where branchID =23", cs_dt, False)
        'Dim CSIDCount As Integer = cs_dt.Rows.Count
        'T67130 now include HHBD
        'For Each csRow In cs_dt.Rows
        'Dim CSID As Integer = csRow(0)
        'If CSID <> 4667 Then
        '    Continue For
        'End If
        'now get cases

        Dim debt_dt As New DataTable
        ' " group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15" & _
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, D.status, arrange_started, return_codeID, debt_balance, return_date, bail_current, bail_allocated," & _
                        "D.bailiffID, name_fore, name_sur, dateOfBirth, address, add_postcode" & _
                       " status_open_closed, return_date" & _
                      " from (Debtor D, clientScheme CS)" & _
                      " left join Note N on (N.DebtorID = D._rowID" & _
                      "    and N._createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                      "  and N._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "')" & _
                      " left join Payment P " & _
                        "   on (P.DebtorID =D._rowID " & _
                        " and P._createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                          " and P._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "')" & _
                      " where D.clientschemeID = CS._rowID" & _
                      " and CS.branchID = 23" & _
                      " and (N._rowID is not null or P._rowID is not null)" & _
                       " group by 1" & _
                      " order by D._rowid", debt_dt, False)
        Dim debtorRows As Integer = debt_dt.Rows.Count
        For Each debtRow In debt_dt.Rows
            Dim lastNoteRowid As Integer = 0
            Dim lastNoteType As String = ""
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            'T55090 check for phone number changes
            Dim phone_dt As New DataTable
            Dim phoneActionOpen As Boolean = False
            LoadDataTable2("StudentLoans", "select BuildDate, add_phone, add_fax, empphone,empfax, addEmail from rpt.HMRCETCDebtor" & _
                           " where DebtorID = " & debtorID & _
                           " and BuildDate >= '" & startDate.ToString("yyyy-MMM-dd") & "'" & _
                           " and BuildDate < '" & endDate.ToString("yyyy-MMM-dd") & "'", phone_dt, False)
            For Each phoneRow In phone_dt.Rows
                Dim buildDate As Date = phoneRow(0)
                Dim phone1 As String = ""
                Dim phone2 As String = ""
                Dim phone3 As String = ""
                Dim phone4 As String = ""
                Dim email As String = ""
                Try
                    phone1 = phoneRow(1)
                Catch ex As Exception

                End Try
                Try
                    phone2 = phoneRow(2)
                Catch ex As Exception

                End Try
                Try
                    phone3 = phoneRow(3)
                Catch ex As Exception

                End Try
                Try
                    phone4 = phoneRow(4)
                Catch ex As Exception

                End Try
                Try
                    email = phoneRow(5)
                Catch ex As Exception

                End Try
                If phone1 <> "" Then
                    phone1 = Replace(phone1, " ", "")
                    phone1 = Replace(phone1, Chr(9), "")
                    phone1 = Replace(phone1, Chr(10), "")
                    phone1 = Replace(phone1, Chr(12), "")
                    If phone1.Length > 20 Then
                        phone1 = Microsoft.VisualBasic.Left(phone1, 20)
                    End If

                    If Not (IsNumeric(phone1)) Then
                        Continue For
                    End If
                    If Not xmlWritten Then
                        writer.WriteStartElement("Acct")
                        writer.WriteElementString("AcctID", clientRef)
                        writer.WriteElementString("ClntCd", clientCode)
                        writer.WriteStartElement("Actns")
                        xmlWritten = True
                    End If
                    phoneActionOpen = True
                    writer.WriteStartElement("Actn")
                    writer.WriteElementString("ActnTS", Format(buildDate, "yyyy-MM-dd") & "T" & Format(buildDate, "HH:mm:ss"))
                    writer.WriteElementString("Tp", "DAP")
                    writer.WriteStartElement("Actvts")
                    writer.WriteStartElement("Actvty")
                    writer.WriteElementString("Dt", Format(buildDate, "yyyy-MM-dd") & "T" & Format(buildDate, "HH:mm:ss"))
                    writer.WriteElementString("Cd", "NPH")
                    writer.WriteStartElement("CtctMtd")
                    writer.WriteStartElement("Tel")
                    writer.WriteElementString("TelNb", phone1)
                    writer.WriteEndElement()  'Tel
                    writer.WriteEndElement()  'CtctMtd
                    writer.WriteEndElement()  'acvty
                    ' writer.WriteEndElement()  'actvts
                    'writer.WriteEndElement()  'actn
                End If
                If phone2 <> "" Then
                    phone2 = Replace(phone2, " ", "")
                    phone2 = Replace(phone2, Chr(9), "")
                    phone2 = Replace(phone2, Chr(10), "")
                    phone2 = Replace(phone2, Chr(12), "")
                    If phone2.Length > 20 Then
                        phone2 = Microsoft.VisualBasic.Left(phone2, 20)
                    End If

                    If Not (IsNumeric(phone2)) Then
                        Continue For
                    End If
                    If Not xmlWritten Then
                        writer.WriteStartElement("Acct")
                        writer.WriteElementString("AcctID", clientRef)
                        writer.WriteElementString("ClntCd", clientCode)
                        writer.WriteStartElement("Actns")
                        xmlWritten = True
                    End If
                    If phoneActionOpen = False Then
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(buildDate, "yyyy-MM-dd") & "T" & Format(buildDate, "HH:mm:ss"))
                        writer.WriteElementString("Tp", "DAP")
                        writer.WriteStartElement("Actvts")
                    End If
                    phoneActionOpen = True

                    writer.WriteStartElement("Actvty")
                    writer.WriteElementString("Dt", Format(buildDate, "yyyy-MM-dd") & "T" & Format(buildDate, "HH:mm:ss"))
                    writer.WriteElementString("Cd", "NPH")
                    writer.WriteStartElement("CtctMtd")
                    writer.WriteStartElement("Tel")
                    writer.WriteElementString("TelNb", phone2)
                    writer.WriteEndElement()  'Tel
                    writer.WriteEndElement()  'CtctMtd
                    writer.WriteEndElement()  'acvty
                    ' writer.WriteEndElement()  'actvts
                    'writer.WriteEndElement()  'actn
                End If
                If phone3 <> "" Then
                    phone3 = Replace(phone3, " ", "")
                    phone3 = Replace(phone3, Chr(9), "")
                    phone3 = Replace(phone3, Chr(10), "")
                    phone3 = Replace(phone3, Chr(12), "")
                    If phone3.Length > 20 Then
                        phone3 = Microsoft.VisualBasic.Left(phone3, 20)
                    End If

                    If Not (IsNumeric(phone3)) Then
                        Continue For
                    End If
                    If Not xmlWritten Then
                        writer.WriteStartElement("Acct")
                        writer.WriteElementString("AcctID", clientRef)
                        writer.WriteElementString("ClntCd", clientCode)
                        writer.WriteStartElement("Actns")
                        xmlWritten = True
                    End If
                    If phoneActionOpen = False Then
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(buildDate, "yyyy-MM-dd") & "T" & Format(buildDate, "HH:mm:ss"))
                        writer.WriteElementString("Tp", "DAP")
                        writer.WriteStartElement("Actvts")
                    End If
                    phoneActionOpen = True

                    writer.WriteStartElement("Actvty")
                    writer.WriteElementString("Dt", Format(buildDate, "yyyy-MM-dd") & "T" & Format(buildDate, "HH:mm:ss"))
                    writer.WriteElementString("Cd", "NPH")
                    writer.WriteStartElement("CtctMtd")
                    writer.WriteStartElement("Tel")
                    writer.WriteElementString("TelNb", phone3)
                    writer.WriteEndElement()  'Tel
                    writer.WriteEndElement()  'CtctMtd
                    writer.WriteEndElement()  'acvty
                    ' writer.WriteEndElement()  'actvts
                    'writer.WriteEndElement()  'actn
                End If
                If phone4 <> "" Then
                    phone4 = Replace(phone4, " ", "")
                    phone4 = Replace(phone4, Chr(9), "")
                    phone4 = Replace(phone4, Chr(10), "")
                    phone4 = Replace(phone4, Chr(12), "")
                    If phone4.Length > 20 Then
                        phone4 = Microsoft.VisualBasic.Left(phone4, 20)
                    End If

                    If Not (IsNumeric(phone4)) Then
                        Continue For
                    End If
                    If Not xmlWritten Then
                        writer.WriteStartElement("Acct")
                        writer.WriteElementString("AcctID", clientRef)
                        writer.WriteElementString("ClntCd", clientCode)
                        writer.WriteStartElement("Actns")
                        xmlWritten = True
                    End If
                    If phoneActionOpen = False Then
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(buildDate, "yyyy-MM-dd") & "T" & Format(buildDate, "HH:mm:ss"))
                        writer.WriteElementString("Tp", "DAP")
                        writer.WriteStartElement("Actvts")
                    End If
                    phoneActionOpen = True

                    writer.WriteStartElement("Actvty")
                    writer.WriteElementString("Dt", Format(buildDate, "yyyy-MM-dd") & "T" & Format(buildDate, "HH:mm:ss"))
                    writer.WriteElementString("Cd", "NPH")
                    writer.WriteStartElement("CtctMtd")
                    writer.WriteStartElement("Tel")
                    writer.WriteElementString("TelNb", phone4)
                    writer.WriteEndElement()  'Tel
                    writer.WriteEndElement()  'CtctMtd
                    writer.WriteEndElement()  'acvty
                    ' writer.WriteEndElement()  'actvts
                    'writer.WriteEndElement()  'actn
                End If
                If email <> "" Then
                    If Not xmlWritten Then
                        writer.WriteStartElement("Acct")
                        writer.WriteElementString("AcctID", clientRef)
                        writer.WriteElementString("ClntCd", clientCode)
                        writer.WriteStartElement("Actns")
                        xmlWritten = True
                    End If
                    If phoneActionOpen = False Then
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(buildDate, "yyyy-MM-dd") & "T" & Format(buildDate, "HH:mm:ss"))
                        writer.WriteElementString("Tp", "DAP")
                        writer.WriteStartElement("Actvts")
                    End If
                    phoneActionOpen = False

                    writer.WriteStartElement("Actvty")
                    writer.WriteElementString("Dt", Format(buildDate, "yyyy-MM-dd") & "T" & Format(buildDate, "HH:mm:ss"))
                    writer.WriteElementString("Cd", "NEM")
                    writer.WriteStartElement("CtctMtd")
                    writer.WriteStartElement("Eml")
                    writer.WriteElementString("EmlAdr", email)
                    writer.WriteEndElement()  'Tel
                    writer.WriteEndElement()  'CtctMtd
                    writer.WriteEndElement()  'acvty
                    writer.WriteEndElement()  'actvts
                    writer.WriteEndElement()  'actn
                End If
            Next
            If phoneActionOpen Then
                writer.WriteEndElement()  'actvts
                writer.WriteEndElement()  'actn
            End If


            Dim caseStatus As String = debtRow(2)
            Dim arrangeStarted As Date
            Try
                arrangeStarted = debtRow(3)
            Catch ex As Exception
                arrangeStarted = Nothing
            End Try
            Dim totalBalance As Decimal = debtRow(5)

            Dim cancelledNote As String = ""
            'get all notes for last week
            Dim note_dt As New DataTable
            LoadDataTable2("DebtRecovery", " select type, text, _createdDate, _createdBy, _rowid" & _
                          " from Note" & _
                          " where debtorID = " & debtorID & _
                          " and _createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                          " and _createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                          " order by _rowID", note_dt, False)

            For Each noteRow In note_dt.Rows
                Dim noteType As String = noteRow(0)
                Dim noteText As String = noteRow(1)
                Dim noteDate As Date = noteRow(2)
                Dim noteAgent As String = noteRow(3)
                Dim noteRowid As Integer = noteRow(4)
                Select Case noteType
                    Case "Allocated"
                        'T54418
                        'See If APY alleged payment made
                        If InStr(noteText, ":3784") Then
                            If Not xmlWritten Then
                                writer.WriteStartElement("Acct")
                                writer.WriteElementString("AcctID", clientRef)
                                writer.WriteElementString("ClntCd", clientCode)
                                writer.WriteStartElement("Actns")
                                xmlWritten = True
                            End If
                            writer.WriteStartElement("Actn")
                            writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Tp", "UNA")
                            writer.WriteStartElement("Actvts")
                            writer.WriteStartElement("Actvty")
                            writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Cd", "APY")
                            writer.WriteEndElement()  'acvty
                            writer.WriteEndElement()  'actvts
                            writer.WriteEndElement()  'actn
                        End If
                        'agent 2197 -query
                        If InStr(noteText, ":2197") Then
                            If Not xmlWritten Then
                                writer.WriteStartElement("Acct")
                                writer.WriteElementString("AcctID", clientRef)
                                writer.WriteElementString("ClntCd", clientCode)
                                writer.WriteStartElement("Actns")
                                xmlWritten = True
                            End If
                            writer.WriteStartElement("Actn")
                            writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Tp", "UNA")
                            writer.WriteStartElement("Actvts")
                            writer.WriteStartElement("Actvty")
                            writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Cd", "DIS")
                            writer.WriteEndElement()  'acvty
                            writer.WriteEndElement()  'actvts
                            writer.WriteEndElement()  'actn
                        End If
                        If InStr(noteText, "Field Collector") = 0 Then
                            Continue For
                        End If
                        If Not xmlWritten Then
                            writer.WriteStartElement("Acct")
                            writer.WriteElementString("AcctID", clientRef)
                            writer.WriteElementString("ClntCd", clientCode)
                            writer.WriteStartElement("Actns")
                            xmlWritten = True
                        End If
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        writer.WriteElementString("Tp", "STC")
                        writer.WriteEndElement()  'actn
                    Case "Arrangement"
                        If noteRowid <= lastNoteRowid + 16 Then
                            Continue For
                        End If
                        If Not xmlWritten Then
                            writer.WriteStartElement("Acct")
                            writer.WriteElementString("AcctID", clientRef)
                            writer.WriteElementString("ClntCd", clientCode)
                            writer.WriteStartElement("Actns")
                            xmlWritten = True
                        End If
                        Dim testText As String
                        If InStr(noteText, " payment ") = 0 Then
                            Continue For
                        End If
                        Dim startIDX As Integer
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        'T54418 add PPO and PPS
                        writer.WriteElementString("Tp", "UNA")
                        'writer.WriteElementString("Tp", "PPO")
                        writer.WriteStartElement("Actvts")
                        writer.WriteStartElement("Actvty")
                        writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        writer.WriteElementString("Cd", "PPS")
                        writer.WriteEndElement()  'acvty

                        'Tim  if an updated arrangement
                        Dim prevArrangeNoteID As Integer = GetSQLResults2("DebtRecovery", "SELECT IFNULL(MAX(_rowID),-1) FROM note WHERE debtorID = " & debtorID & " and type ='Arrangement' and _rowID < " & noteRowid)
                        Dim BrokenNoteID As Integer = GetSQLResults2("DebtRecovery", "SELECT IFNULL(MAX(_rowID),-1) FROM note WHERE debtorID = " & debtorID & " and type ='Broken'")
                        If prevArrangeNoteID > BrokenNoteID Then
                            writer.WriteStartElement("Actvty")
                            writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Cd", "PPU")
                            writer.WriteEndElement()  'acvty
                        End If
                        ' end of Tim section

                        writer.WriteStartElement("Actvty")
                        writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        'T54418 use PTP for one-off else PAR
                        startIDX = InStr(noteText, "every")
                        If startIDX = 0 Then
                            writer.WriteElementString("Cd", "PTP")
                        Else
                            writer.WriteElementString("Cd", "PAR")
                        End If

                        writer.WriteStartElement("PymtPln")
                        writer.WriteElementString("CrtnD", Format(noteDate, "yyyy-MM-dd"))
                        Dim firstPaymentDate As Date = Nothing
                        Dim firstPaymentAmount As Decimal = 0
                        Dim regPaymentAmount As Decimal = 0
                        Dim paymentFrequency As String = "P"
                        startIDX = InStr(noteText, "Initial payment of ")
                        If startIDX > 0 Then
                            testText = Microsoft.VisualBasic.Right(noteText, noteText.Length - startIDX - 19 + 1)
                            startIDX = InStr(testText, ".")
                            If startIDX > 0 Then
                                Try
                                    firstPaymentAmount = Microsoft.VisualBasic.Left(testText, startIDX + 2)
                                Catch ex As Exception
                                    write_audit(debtorID & " first payment amount??", False)
                                End Try
                                startIDX = InStr(testText, "due on")
                                If startIDX > 0 Then
                                    testText = Mid(testText, startIDX + 6, 13)
                                    Try
                                        firstPaymentDate = CDate(Trim(testText))
                                    Catch ex As Exception
                                        write_audit(debtorID & " first payment due on??", False)
                                    End Try
                                End If
                            End If
                        Else
                            startIDX = InStr(noteText, "Whole payment of ")
                            If startIDX > 0 Then
                                testText = Microsoft.VisualBasic.Right(noteText, noteText.Length - startIDX - 17 + 1)
                                startIDX = InStr(testText, ".")
                                If startIDX > 0 Then
                                    Try
                                        firstPaymentAmount = Microsoft.VisualBasic.Left(testText, startIDX + 2)
                                    Catch ex As Exception
                                        write_audit(debtorID & " whole payment amount??", False)

                                    End Try
                                    startIDX = InStr(testText, "due on")
                                    If startIDX > 0 Then
                                        testText = Mid(testText, startIDX + 6, 13)
                                        Try
                                            firstPaymentDate = CDate(Trim(testText))
                                        Catch ex As Exception
                                            write_audit(debtorID & " first payment due on??", False)
                                        End Try
                                    End If
                                End If
                            End If
                        End If
                        startIDX = InStr(noteText, "every")
                        Dim arrangementDays As Integer
                        If startIDX > 0 Then
                            Dim followIDX As Integer = InStr(noteText, "followed by")
                            If followIDX > 0 Then
                                noteText = Microsoft.VisualBasic.Right(noteText, noteText.Length - followIDX - 11)
                                startIDX = InStr(noteText, "every")
                            End If
                            Dim endIDX As Integer
                            endIDX = InStr(noteText, "days")
                            Try
                                regPaymentAmount = Microsoft.VisualBasic.Left(noteText, startIDX - 1)
                            Catch ex As Exception
                                write_audit(debtorID & " regular payment amount is??", False)
                            End Try
                            If endIDX > 0 Then
                                Try
                                    arrangementDays = Mid(noteText, startIDX + 5, endIDX - startIDX - 5)
                                Catch ex As Exception
                                    write_audit(debtorID & " arrangement days??", False)
                                End Try
                                If arrangementDays >= 29 Then
                                    paymentFrequency = "M"
                                ElseIf arrangementDays >= 13 Then
                                    paymentFrequency = "F"
                                Else
                                    paymentFrequency = "W"
                                End If
                            End If
                            startIDX = InStr(noteText, "First payment due on")
                            If startIDX > 0 And firstPaymentDate = Nothing Then
                                testText = Mid(noteText, startIDX + 20, 13)
                                Try
                                    firstPaymentDate = CDate(Trim(testText))
                                Catch ex As Exception
                                    write_audit(debtorID & "First payment due on is???", False)
                                End Try
                            End If
                        End If
                        Dim numPymnts As Integer = 0
                        writer.WriteElementString("FstPymtD", Format(firstPaymentDate, "yyyy-MM-dd"))


                        If regPaymentAmount > 0 Then
                            Dim testPymnts As Integer = 0
                            testPymnts = Int((totalBalance - firstPaymentAmount) / regPaymentAmount) + 1
                            If testPymnts > 0 Then
                                numPymnts += testPymnts
                            End If
                        End If
                        Dim lastPymtDate As Date = Nothing
                        Try
                            lastPymtDate = DateAdd(DateInterval.Day, numPymnts * arrangementDays, firstPaymentDate)
                        Catch ex As Exception

                        End Try


                        writer.WriteElementString("LstPymtD", Format(lastPymtDate, "yyyy-MM-dd"))

                        writer.WriteElementString("RegPymtAmt", Format(regPaymentAmount, "0.00"))
                        If firstPaymentAmount > 0 Then
                            numPymnts += 1
                            writer.WriteElementString("FstPymtAmt", Format(firstPaymentAmount, "0.00"))
                        End If
                        If firstPaymentAmount = 0 And regPaymentAmount = 0 Then
                            write_audit(debtorID & "First and reg payments are zero???", False)
                        End If
                        If numPymnts > 0 Then
                            writer.WriteElementString("NumPymts", Format(numPymnts, "0"))
                        End If

                        writer.WriteElementString("PymtFrqCd", paymentFrequency)

                        'Tim add when agent 2151
                        Dim lastAgentAllocatedNote As String = GetSQLResults2("DebtRecovery", "SELECT IFNULL(text,'') FROM note WHERE _rowID = (SELECT MAX(_rowID) FROM note WHERE debtorID = " & debtorID & " and type ='Allocated' and _rowID < " & noteRowid & ")")
                        If InStr(lastAgentAllocatedNote, ":2151") Then
                            writer.WriteStartElement("Orig")
                            writer.WriteElementString("TpCd", "DMC")
                            writer.WriteEndElement()  'orig
                        End If
                        'end of Tim

                        writer.WriteEndElement()  'PymtPln
                        writer.WriteEndElement()  'acvty
                        writer.WriteEndElement()  'actvts
                        writer.WriteEndElement()  'actn
                    Case "Broken"
                        If Not xmlWritten Then
                            writer.WriteStartElement("Acct")
                            writer.WriteElementString("AcctID", clientRef)
                            writer.WriteElementString("ClntCd", clientCode)
                            writer.WriteStartElement("Actns")
                            xmlWritten = True
                        End If
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        writer.WriteElementString("Tp", "UNA")
                        writer.WriteStartElement("Actvts")
                        writer.WriteStartElement("Actvty")
                        writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        'T54418 if payment received after arrangement started then ARB else PRB
                        Dim pay2_dt As New DataTable
                        LoadDataTable2("DebtRecovery", "Select count(_rowid)" & _
                                      " from Payment" & _
                                      " where debtorID = " & debtorID & _
                                      " and (status = 'W' or status = 'R')" & _
                                      " and date >= '" & Format(arrangeStarted, "yyyy-MM-dd") & "'", pay2_dt, False)
                        If pay2_dt.Rows(0).Item(0) = 0 Then
                            writer.WriteElementString("Cd", "PRB")
                        Else
                            writer.WriteElementString("Cd", "ARB")
                        End If

                        writer.WriteEndElement()  'acvty
                        writer.WriteEndElement()  'actvts
                        writer.WriteEndElement()  'actn
                    Case "Cancelled"
                        cancelledNote = noteText
                    Case "Clear arrange"
                        'T54418
                        If Not xmlWritten Then
                            writer.WriteStartElement("Acct")
                            writer.WriteElementString("AcctID", clientRef)
                            writer.WriteElementString("ClntCd", clientCode)
                            writer.WriteStartElement("Actns")
                            xmlWritten = True
                        End If
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        writer.WriteElementString("Tp", "UNA")
                        writer.WriteStartElement("Actvts")
                        writer.WriteStartElement("Actvty")
                        writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        writer.WriteElementString("Cd", "PPX")

                        writer.WriteEndElement()  'acvty
                        writer.WriteEndElement()  'actvts
                        writer.WriteEndElement()  'actn
                    Case "Complaint"
                        If Not xmlWritten Then
                            writer.WriteStartElement("Acct")
                            writer.WriteElementString("AcctID", clientRef)
                            writer.WriteElementString("ClntCd", clientCode)
                            writer.WriteStartElement("Actns")
                            xmlWritten = True
                        End If
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        writer.WriteElementString("Tp", "UNA")
                        writer.WriteStartElement("Actvts")
                        writer.WriteStartElement("Actvty")
                        writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        writer.WriteElementString("Cd", "CPT")
                        writer.WriteEndElement()  'acvty
                        writer.WriteEndElement()  'actvts
                        writer.WriteEndElement()  'actn
                    Case "Employment"
                        'Tim add occupation
                        If noteText.IndexOf("occupation: ") > -1 Then
                            Dim restOfNote As String = noteText.Substring(noteText.IndexOf("occupation: ") + 12)
                            Dim occupation As String = ""
                            If restOfNote.IndexOf(".") = -1 Then ' the occupation code is the last part of the note.
                                occupation = restOfNote.ToUpper
                            Else ' full stops are used to separate the different parts of the note
                                occupation = restOfNote.Substring(0, restOfNote.IndexOf(".")).ToUpper
                            End If

                            ' as per conversation with Charlie 11/Aug, only append this to the xml if it is a valid code
                            If {"EMP", "FT", "PT", "RET", "SEM", "STD", "UEM"}.Contains(occupation) Then
                                'T58342 only write xml when occupation found
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "DAP")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "UCD")
                                writer.WriteStartElement("AcctHldr")
                                'T54542 andd res status name and DOB
                                writer.WriteElementString("ResStsCd", "UNK")
                                writer.WriteStartElement("Dtls")
                                writer.WriteStartElement("Nm")
                                Dim firstName As String = ""
                                Try
                                    firstName = debtRow(10)
                                Catch ex As Exception

                                End Try
                                Dim surName As String = ""
                                Try
                                    surName = debtRow(11)
                                Catch ex As Exception

                                End Try
                                writer.WriteElementString("FrstNm", firstName)
                                writer.WriteElementString("Snm", surName)
                                writer.WriteEndElement()  'nm
                                Dim dob As String = "1990-01-01"
                                Dim testDate As Date
                                Try
                                    testDate = debtRow(12)
                                    dob = Format(testDate, "yyyy-MM-dd")
                                Catch ex As Exception

                                End Try
                                writer.WriteElementString("DOB", dob)

                                writer.WriteEndElement()  'Dtls
                                writer.WriteStartElement("Emplmt")
                                writer.WriteElementString("EmpSts", occupation)
                                writer.WriteEndElement()  'emplmt

                                writer.WriteEndElement()  'accthldr
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            End If
                        End If
                        ' end of Tim section


                    Case "Note"
                        Dim noteStart As String = UCase(Microsoft.VisualBasic.Left(noteText, 3))
                        Dim noteStart7 As String = ""
                        If noteText.Length > 6 Then
                            noteStart7 = UCase(Microsoft.VisualBasic.Left(noteText, 7))
                        End If
                        If Mid(noteText, 4, 1) <> " " Then
                            If noteText = "VoiceSage Contact Message Sent" Then
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "SMS")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "MEL")
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                                Continue For
                            End If
                        End If
                        Select Case noteStart
                            Case "ACL"
                                'account closed picked up by remit
                            Case "AMM"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "PCM")
                                writer.WriteElementString("OutTp", "NOA")
                                writer.WriteElementString("Notes", noteText)
                                'T57920
                                Dim PhoneText As String = Microsoft.VisualBasic.Right(noteText, noteText.Length - 3)
                                Dim phoneNum As String = ""
                                For phoneIDX = 1 To PhoneText.Length
                                    If IsNumeric(Mid(PhoneText, phoneIDX, 1)) Then
                                        phoneNum &= Mid(PhoneText, phoneIDX, 1)
                                    Else
                                        If Mid(PhoneText, phoneIDX, 1) <> " " And _
                                    Mid(PhoneText, phoneIDX, 1) <> "-" Then
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Microsoft.VisualBasic.Left(phoneNum, 1) <> "" And Microsoft.VisualBasic.Left(phoneNum, 1) <> "0" Then
                                    phoneNum = "0" & phoneNum
                                End If
                                If Not (IsNumeric(phoneNum)) Then
                                    phoneNum = ""
                                Else
                                    If phoneNum = 0 Then
                                        phoneNum = ""
                                    End If
                                End If
                                If phoneNum <> "" Then
                                    If phoneNum.Length <= 20 Then
                                        writer.WriteStartElement("CtctMtd")
                                        writer.WriteStartElement("Tel")
                                        writer.WriteElementString("TelNb", phoneNum)
                                        writer.WriteEndElement()  'Tel
                                        writer.WriteEndElement()  'CtctMtd
                                    End If
                                End If
                                writer.WriteEndElement()  'actn
                            Case "ANM"
                                'MsgBox("Check ANM " & debtorID)
                                'If Not xmlWritten Then
                                '    writer.WriteStartElement("Acct")
                                '    writer.WriteElementString("AcctID", clientref)
                                '    writer.WriteElementString("ClntCd","HMRC ETC")
                                '    writer.WriteStartElement("Actns")
                                '    xmlWritten = True
                                'End If
                                'writer.WriteStartElement("Actn")
                                'writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                'writer.WriteElementString("Tp", "PCM")
                                'writer.WriteStartElement("Actvts")
                                'writer.WriteStartElement("Actvty")
                                'writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                'writer.WriteElementString("Cd", "PCM")
                                'writer.WriteEndElement()  'acvty
                                'writer.WriteEndElement()  'actvts
                                'writer.WriteEndElement()  'actn
                            Case "ARB"
                                'MsgBox(debtorID & " use broken note or add code for " & noteStart)
                            Case "BDN"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "PCM")
                                writer.WriteElementString("OutTp", "BDN")
                                writer.WriteElementString("Notes", noteText)
                                'T57920
                                Dim PhoneText As String = Microsoft.VisualBasic.Right(noteText, noteText.Length - 3)
                                Dim phoneNum As String = ""
                                For phoneIDX = 1 To PhoneText.Length
                                    If IsNumeric(Mid(PhoneText, phoneIDX, 1)) Then
                                        phoneNum &= Mid(PhoneText, phoneIDX, 1)
                                    Else
                                        If Mid(PhoneText, phoneIDX, 1) <> " " And _
                                    Mid(PhoneText, phoneIDX, 1) <> "-" Then
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Microsoft.VisualBasic.Left(phoneNum, 1) <> "" And Microsoft.VisualBasic.Left(phoneNum, 1) <> "0" Then
                                    phoneNum = "0" & phoneNum
                                End If
                                If Not (IsNumeric(phoneNum)) Then
                                    phoneNum = ""
                                Else
                                    If phoneNum = 0 Then
                                        phoneNum = ""
                                    End If
                                End If
                                If phoneNum <> "" Then
                                    If phoneNum.Length <= 20 Then
                                        writer.WriteStartElement("CtctMtd")
                                        writer.WriteStartElement("Tel")
                                        writer.WriteElementString("TelNb", phoneNum)
                                        writer.WriteEndElement()  'Tel
                                        writer.WriteEndElement()  'CtctMtd
                                    End If
                                End If

                                writer.WriteEndElement()  'actn
                            Case "DIS"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "UNA")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "DIS")
                                'writer.WriteElementString("Notes", noteText)
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            Case "EMS"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "EMS")
                                writer.WriteEndElement()  'actn
                            Case "IAE"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "UNA")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "IAE")
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            Case "INB"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                If noteStart7 = "INB RPC" Then
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "INC")
                                    'T57920
                                    Dim PhoneText As String = Microsoft.VisualBasic.Right(noteText, noteText.Length - 7)
                                    Dim phoneNum As String = ""
                                    For phoneIDX = 1 To PhoneText.Length
                                        If IsNumeric(Mid(PhoneText, phoneIDX, 1)) Then
                                            phoneNum &= Mid(PhoneText, phoneIDX, 1)
                                        Else
                                            If Mid(PhoneText, phoneIDX, 1) <> " " And _
                                        Mid(PhoneText, phoneIDX, 1) <> "-" Then
                                                Exit For
                                            End If
                                        End If
                                    Next
                                    If Microsoft.VisualBasic.Left(phoneNum, 1) <> "" And Microsoft.VisualBasic.Left(phoneNum, 1) <> "0" Then
                                        phoneNum = "0" & phoneNum
                                    End If
                                    If Not (IsNumeric(phoneNum)) Then
                                        phoneNum = ""
                                    Else
                                        If phoneNum = 0 Then
                                            phoneNum = ""
                                        End If
                                    End If
                                    If phoneNum <> "" Then
                                        If phoneNum.Length <= 20 Then
                                            writer.WriteStartElement("CtctMtd")
                                            writer.WriteStartElement("Tel")
                                            writer.WriteElementString("TelNb", phoneNum)
                                            writer.WriteEndElement()  'Tel
                                            writer.WriteEndElement()  'CtctMtd
                                        End If
                                    End If
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "RPC")
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "PPO")
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Else
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "INC")
                                    'T57920
                                    Dim PhoneText As String = Microsoft.VisualBasic.Right(noteText, noteText.Length - 3)
                                    Dim phoneNum As String = ""
                                    For phoneIDX = 1 To PhoneText.Length
                                        If IsNumeric(Mid(PhoneText, phoneIDX, 1)) Then
                                            phoneNum &= Mid(PhoneText, phoneIDX, 1)
                                        Else
                                            If Mid(PhoneText, phoneIDX, 1) <> " " And _
                                        Mid(PhoneText, phoneIDX, 1) <> "-" Then
                                                Exit For
                                            End If
                                        End If
                                    Next
                                    If Microsoft.VisualBasic.Left(phoneNum, 1) <> "" And Microsoft.VisualBasic.Left(phoneNum, 1) <> "0" Then
                                        phoneNum = "0" & phoneNum
                                    End If
                                    If Not (IsNumeric(phoneNum)) Then
                                        phoneNum = ""
                                    Else
                                        If phoneNum = 0 Then
                                            phoneNum = ""
                                        End If
                                    End If
                                    If phoneNum <> "" Then
                                        If phoneNum.Length <= 20 Then
                                            writer.WriteStartElement("CtctMtd")
                                            writer.WriteStartElement("Tel")
                                            writer.WriteElementString("TelNb", phoneNum)
                                            writer.WriteEndElement()  'Tel
                                            writer.WriteEndElement()  'CtctMtd
                                        End If
                                    End If
                                    writer.WriteEndElement()  'actn
                                End If
                            Case "INC"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                If noteStart7 = "INC RPC" Then
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "INC")
                                    'T57920
                                    Dim PhoneText As String = Microsoft.VisualBasic.Right(noteText, noteText.Length - 7)
                                    Dim phoneNum As String = ""
                                    For phoneIDX = 1 To PhoneText.Length
                                        If IsNumeric(Mid(PhoneText, phoneIDX, 1)) Then
                                            phoneNum &= Mid(PhoneText, phoneIDX, 1)
                                        Else
                                            If Mid(PhoneText, phoneIDX, 1) <> " " And _
                                        Mid(PhoneText, phoneIDX, 1) <> "-" Then
                                                Exit For
                                            End If
                                        End If
                                    Next
                                    If Microsoft.VisualBasic.Left(phoneNum, 1) <> "" And Microsoft.VisualBasic.Left(phoneNum, 1) <> "0" Then
                                        phoneNum = "0" & phoneNum
                                    End If
                                    If phoneNum <> "" Then
                                        If phoneNum.Length <= 20 Then
                                            writer.WriteStartElement("CtctMtd")
                                            writer.WriteStartElement("Tel")
                                            writer.WriteElementString("TelNb", phoneNum)
                                            writer.WriteEndElement()  'Tel
                                            writer.WriteEndElement()  'CtctMtd
                                        End If
                                    End If

                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "RPC")
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "PPO")
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Else
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "INC")
                                    'T57920
                                    Dim PhoneText As String = Microsoft.VisualBasic.Right(noteText, noteText.Length - 3)
                                    Dim phoneNum As String = ""
                                    For phoneIDX = 1 To PhoneText.Length
                                        If IsNumeric(Mid(PhoneText, phoneIDX, 1)) Then
                                            phoneNum &= Mid(PhoneText, phoneIDX, 1)
                                        Else
                                            If Mid(PhoneText, phoneIDX, 1) <> " " And _
                                        Mid(PhoneText, phoneIDX, 1) <> "-" Then
                                                Exit For
                                            End If
                                        End If
                                    Next
                                    If Microsoft.VisualBasic.Left(phoneNum, 1) <> "" And Microsoft.VisualBasic.Left(phoneNum, 1) <> "0" Then
                                        phoneNum = "0" & phoneNum
                                    End If
                                    If Not (IsNumeric(phoneNum)) Then
                                        phoneNum = ""
                                    Else
                                        If phoneNum = 0 Then
                                            phoneNum = ""
                                        End If
                                    End If
                                    If phoneNum <> "" Then
                                        If phoneNum.Length <= 20 Then
                                            writer.WriteStartElement("CtctMtd")
                                            writer.WriteStartElement("Tel")
                                            writer.WriteElementString("TelNb", phoneNum)
                                            writer.WriteEndElement()  'Tel
                                            writer.WriteEndElement()  'CtctMtd
                                        End If
                                    End If
                                    writer.WriteEndElement()  'actn
                                End If
                            Case "INE"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "INE")
                                writer.WriteEndElement()  'actn
                            Case "INL"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "INL")
                                writer.WriteEndElement()  'actn
                            Case "MEL"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "PCM")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "MEL")
                                'T57920
                                Dim PhoneText As String = Microsoft.VisualBasic.Right(noteText, noteText.Length - 3)
                                Dim phoneNum As String = ""
                                For phoneIDX = 1 To PhoneText.Length
                                    If IsNumeric(Mid(PhoneText, phoneIDX, 1)) Then
                                        phoneNum &= Mid(PhoneText, phoneIDX, 1)
                                    Else
                                        If Mid(PhoneText, phoneIDX, 1) <> " " And _
                                    Mid(PhoneText, phoneIDX, 1) <> "-" Then
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Microsoft.VisualBasic.Left(phoneNum, 1) <> "" And Microsoft.VisualBasic.Left(phoneNum, 1) <> "0" Then
                                    phoneNum = "0" & phoneNum
                                End If
                                If Not (IsNumeric(phoneNum)) Then
                                    phoneNum = ""
                                Else
                                    If phoneNum = 0 Then
                                        phoneNum = ""
                                    End If
                                End If
                                If phoneNum <> "" Then
                                    If phoneNum.Length <= 20 Then
                                        writer.WriteStartElement("CtctMtd")
                                        writer.WriteStartElement("Tel")
                                        writer.WriteElementString("TelNb", phoneNum)
                                        writer.WriteEndElement()  'Tel
                                        writer.WriteEndElement()  'CtctMtd
                                    End If
                                End If
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            Case "NOA"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "PCM")
                                'writer.WriteStartElement("Actvts")
                                'writer.WriteStartElement("Actvty")
                                'writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("OutTp", "NOA")
                                writer.WriteElementString("Notes", noteText)
                                'T57920
                                Dim PhoneText As String = Microsoft.VisualBasic.Right(noteText, noteText.Length - 3)
                                Dim phoneNum As String = ""
                                For phoneIDX = 1 To PhoneText.Length
                                    If IsNumeric(Mid(PhoneText, phoneIDX, 1)) Then
                                        phoneNum &= Mid(PhoneText, phoneIDX, 1)
                                    Else
                                        If Mid(PhoneText, phoneIDX, 1) <> " " And _
                                    Mid(PhoneText, phoneIDX, 1) <> "-" Then
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Microsoft.VisualBasic.Left(phoneNum, 1) <> "" And Microsoft.VisualBasic.Left(phoneNum, 1) <> "0" Then
                                    phoneNum = "0" & phoneNum
                                End If
                                If Not (IsNumeric(phoneNum)) Then
                                    phoneNum = ""
                                Else
                                    If phoneNum = 0 Then
                                        phoneNum = ""
                                    End If
                                End If
                                If phoneNum <> "" Then
                                    If phoneNum.Length <= 20 Then
                                        writer.WriteStartElement("CtctMtd")
                                        writer.WriteStartElement("Tel")
                                        writer.WriteElementString("TelNb", phoneNum)
                                        writer.WriteEndElement()  'Tel
                                        writer.WriteEndElement()  'CtctMtd
                                    End If
                                End If
                                ' writer.WriteEndElement()  'acvty
                                'writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            Case "ONH"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "UNA")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "ONH")
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            Case "ONP"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "UNA")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "ONP")
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            Case "PAR"
                                'ignore
                            Case "PAY"
                                'ignore
                            Case "PCM"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "PCM")


                                writer.WriteElementString("OutTp", "PCA")
                                writer.WriteElementString("Notes", noteText)
                                'T57920
                                Dim PhoneText As String = Microsoft.VisualBasic.Right(noteText, noteText.Length - 3)
                                Dim phoneNum As String = ""
                                For phoneIDX = 1 To PhoneText.Length
                                    If IsNumeric(Mid(PhoneText, phoneIDX, 1)) Then
                                        phoneNum &= Mid(PhoneText, phoneIDX, 1)
                                    Else
                                        If Mid(PhoneText, phoneIDX, 1) <> " " And _
                                    Mid(PhoneText, phoneIDX, 1) <> "-" Then
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Microsoft.VisualBasic.Left(phoneNum, 1) <> "" And Microsoft.VisualBasic.Left(phoneNum, 1) <> "0" Then
                                    phoneNum = "0" & phoneNum
                                End If
                                If Not (IsNumeric(phoneNum)) Then
                                    phoneNum = ""
                                Else
                                    If phoneNum = 0 Then
                                        phoneNum = ""
                                    End If
                                End If
                                If phoneNum <> "" Then
                                    If phoneNum.Length <= 20 Then
                                        writer.WriteStartElement("CtctMtd")
                                        writer.WriteStartElement("Tel")
                                        writer.WriteElementString("TelNb", phoneNum)
                                        writer.WriteEndElement()  'Tel
                                        writer.WriteEndElement()  'CtctMtd
                                    End If
                                End If
                                writer.WriteEndElement()  'actn
                            Case "PTP"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "UNA")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "PTP")
                                'writer.WriteElementString("Notes", noteText)
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            Case "QRR"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "UNA")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "QRR")
                                'writer.WriteElementString("Notes", noteText)
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            Case "RPC"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "PCM")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "RPC")
                                'T57920
                                Dim PhoneText As String = Microsoft.VisualBasic.Right(noteText, noteText.Length - 3)
                                Dim phoneNum As String = ""
                                For phoneIDX = 1 To PhoneText.Length
                                    If IsNumeric(Mid(PhoneText, phoneIDX, 1)) Then
                                        phoneNum &= Mid(PhoneText, phoneIDX, 1)
                                    Else
                                        If Mid(PhoneText, phoneIDX, 1) <> " " And _
                                    Mid(PhoneText, phoneIDX, 1) <> "-" Then
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Microsoft.VisualBasic.Left(phoneNum, 1) <> "" And Microsoft.VisualBasic.Left(phoneNum, 1) <> "0" Then
                                    phoneNum = "0" & phoneNum
                                End If
                                If Not (IsNumeric(phoneNum)) Then
                                    phoneNum = ""
                                Else
                                    If phoneNum = 0 Then
                                        phoneNum = ""
                                    End If
                                End If
                                If phoneNum <> "" Then
                                    If phoneNum.Length <= 20 Then
                                        writer.WriteStartElement("CtctMtd")
                                        writer.WriteStartElement("Tel")
                                        writer.WriteElementString("TelNb", phoneNum)
                                        writer.WriteEndElement()  'Tel
                                        writer.WriteEndElement()  'CtctMtd
                                    End If
                                End If
                                writer.WriteEndElement()  'acvty
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "PPO")
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn

                            Case "RTP"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "UNA")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "RTP")
                                ' writer.WriteElementString("Notes", noteText)
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            Case "SET"
                                'If Microsoft.VisualBasic.Left(noteText, 3) = "SET" Then
                                '    If InStr(noteText, "rrang") = 0 Then
                                '        MsgBox(debtorID & " add code for " & noteStart)
                                '    End If
                                'End If
                            Case "SIF"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "UNA")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "SET")
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            Case "SMS"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "SMS")
                                writer.WriteEndElement()  'actn
                            Case "SRE"
                                'MsgBox(debtorID & " add code for " & noteStart)
                            Case "TPP"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "UNA")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "TPP")
                                ' writer.WriteElementString("Notes", noteText)
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            Case "WNO"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "PCM")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "WPH")
                                'T57920
                                Dim PhoneText As String = Microsoft.VisualBasic.Right(noteText, noteText.Length - 3)
                                Dim phoneNum As String = ""
                                For phoneIDX = 1 To PhoneText.Length
                                    If IsNumeric(Mid(PhoneText, phoneIDX, 1)) Then
                                        phoneNum &= Mid(PhoneText, phoneIDX, 1)
                                    Else
                                        If Mid(PhoneText, phoneIDX, 1) <> " " And _
                                    Mid(PhoneText, phoneIDX, 1) <> "-" Then
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Microsoft.VisualBasic.Left(phoneNum, 1) <> "" And Microsoft.VisualBasic.Left(phoneNum, 1) <> "0" Then
                                    phoneNum = "0" & phoneNum
                                End If
                                If Not (IsNumeric(phoneNum)) Then
                                    phoneNum = ""
                                Else
                                    If phoneNum = 0 Then
                                        phoneNum = ""
                                    End If
                                End If
                                If phoneNum <> "" Then
                                    If phoneNum.Length <= 20 Then
                                        writer.WriteStartElement("CtctMtd")
                                        writer.WriteStartElement("Tel")
                                        writer.WriteElementString("TelNb", phoneNum)
                                        writer.WriteEndElement()  'Tel
                                        writer.WriteEndElement()  'CtctMtd
                                    End If
                                End If
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            Case "WPC"
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "PCM")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "WPC")
                                'T57920
                                Dim PhoneText As String = Microsoft.VisualBasic.Right(noteText, noteText.Length - 3)
                                Dim phoneNum As String = ""
                                For phoneIDX = 1 To PhoneText.Length
                                    If IsNumeric(Mid(PhoneText, phoneIDX, 1)) Then
                                        phoneNum &= Mid(PhoneText, phoneIDX, 1)
                                    Else
                                        If Mid(PhoneText, phoneIDX, 1) <> " " And _
                                    Mid(PhoneText, phoneIDX, 1) <> "-" Then
                                            Exit For
                                        End If
                                    End If
                                Next
                                If Microsoft.VisualBasic.Left(phoneNum, 1) <> "" And Microsoft.VisualBasic.Left(phoneNum, 1) <> "0" Then
                                    phoneNum = "0" & phoneNum
                                End If
                                If Not (IsNumeric(phoneNum)) Then
                                    phoneNum = ""
                                Else
                                    If phoneNum = 0 Then
                                        phoneNum = ""
                                    End If
                                End If
                                If phoneNum <> "" Then
                                    If phoneNum.Length <= 20 Then
                                        writer.WriteStartElement("CtctMtd")
                                        writer.WriteStartElement("Tel")
                                        writer.WriteElementString("TelNb", phoneNum)
                                        writer.WriteEndElement()  'Tel
                                        writer.WriteEndElement()  'CtctMtd
                                    End If
                                End If
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                                'Case Else
                                '    If noteRowid <> lastNoteRowid + 1 Then
                                '        If Mid(noteText, 4, 1) = " " Then
                                '            Dim testText As String = Trim(noteStart)
                                '            testText = Replace(testText, " ", "")
                                '            If testText.Length = 3 Then
                                '                MsgBox(debtorID & " Note " & noteText)
                                '            End If
                                '        End If
                                '    End If
                        End Select
                    Case "Letter"
                        If noteRowid <= lastNoteRowid + 6 And
                            lastNoteType = "Letter" Then
                            Continue For
                        End If
                        If Not xmlWritten Then
                            writer.WriteStartElement("Acct")
                            writer.WriteElementString("AcctID", clientRef)
                            writer.WriteElementString("ClntCd", clientCode)
                            writer.WriteStartElement("Actns")
                            xmlWritten = True
                        End If
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        writer.WriteElementString("Tp", "LSE")
                        writer.WriteEndElement()  'actn
                    Case "Off trace"
                        If Not xmlWritten Then
                            writer.WriteStartElement("Acct")
                            writer.WriteElementString("AcctID", clientRef)
                            writer.WriteElementString("ClntCd", clientCode)
                            writer.WriteStartElement("Actns")
                            xmlWritten = True
                        End If
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        writer.WriteElementString("Tp", "DTP")
                        writer.WriteStartElement("Actvts")
                        writer.WriteStartElement("Actvty")
                        writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        writer.WriteElementString("Cd", "DTC")
                        writer.WriteEndElement()  'acvty
                        writer.WriteEndElement()  'actvts
                        writer.WriteEndElement()  'actn
                    Case "Phone"
                        'ignore phone as notes have phone call details - otherwise may be duplicated
                        'If Not xmlWritten Then
                        '    writer.WriteStartElement("Acct")
                        '    writer.WriteElementString("AcctID", clientref)
                        '    writer.WriteElementString("ClntCd","HMRC ETC")
                        '    writer.WriteStartElement("Actns")
                        '    xmlWritten = True
                        'End If
                        'writer.WriteStartElement("Actn")
                        'Dim textDate As Date
                        'Dim startText As Integer = InStr(noteText, "called on")
                        'Dim testText As String = noteText
                        'If startText = 0 Then
                        '    MsgBox("Phone date?")
                        'Else
                        '    testText = Microsoft.VisualBasic.Right(testText, testText.Length - startText - 9)
                        '    startText = InStr(testText, "-")
                        '    If startText = 0 Then
                        '        testText = Replace(testText, "at ", "")
                        '        Try
                        '            textDate = CDate(testText)
                        '        Catch ex As Exception
                        '            MsgBox("Phone date2?")
                        '        End Try
                        '    Else
                        '        testText = Microsoft.VisualBasic.Left(testText, startText - 1)
                        '        testText = Replace(testText, "at ", "")
                        '        Try
                        '            textDate = CDate(testText)
                        '        Catch ex As Exception
                        '            MsgBox("Phone date3?")
                        '        End Try
                        '    End If
                        'End If
                        'writer.WriteElementString("ActnTS", Format(textDate, "yyyy-MM-dd") & "T" & Format(textDate, "HH:mm:ss"))
                        'writer.WriteElementString("Tp", "PCM")
                        'writer.WriteStartElement("Actvts")
                        'writer.WriteStartElement("Actvty")
                        'writer.WriteElementString("Dt", Format(textDate, "yyyy-MM-dd") & "T" & Format(textDate, "HH:mm:ss"))
                        'If InStr(noteText, "Number Unobtainable") > 0 Then
                        '    writer.WriteElementString("Cd", "BDN")
                        'ElseIf InStr(noteText, "MsgLF") > 0 Then
                        '    writer.WriteElementString("Cd", "MEL")
                        'ElseIf InStr(noteText, "RPC") > 0 Then
                        '    writer.WriteElementString("Cd", "RPC")
                        'ElseIf InStr(noteText, "Call Terminated By Caller") > 0 Then
                        '    writer.WriteElementString("Cd", "PCA")
                        'ElseIf InStr(noteText, "No Answer") > 0 Then
                        '    writer.WriteElementString("Cd", "NOA")
                        'ElseIf InStr(noteText, "WNO") > 0 Then
                        '    writer.WriteElementString("Cd", "WPH")
                        'ElseIf InStr(noteText, "Wrong Number") > 0 Then
                        '    writer.WriteElementString("Cd", "WPH")
                        'ElseIf InStr(noteText, "PCM") > 0 Then
                        '    writer.WriteElementString("Cd", "PCA")
                        'ElseIf InStr(UCase(noteText), "ANSWER MACHINE") > 0 Then
                        '    writer.WriteElementString("Cd", "NOA")
                        'ElseIf InStr(UCase(noteText), "TRANSFER") > 0 Then
                        '    'ignore
                        'ElseIf InStr(noteText, "Left Message with third party") > 0 Then
                        '    writer.WriteElementString("Cd", "MEL")
                        'ElseIf InStr(noteText, "Absconded") > 0 Then
                        '    writer.WriteElementString("Cd", "WPC")
                        'Else
                        '    MsgBox("new phone type " & debtorID & " " & noteText)
                        'End If

                        ''agent name?
                        'writer.WriteEndElement()  'Actvty
                        'writer.WriteEndElement()  'Actvts
                        'writer.WriteEndElement()  'actn
                    Case "SMS to debtor"
                        If Not xmlWritten Then
                            writer.WriteStartElement("Acct")
                            writer.WriteElementString("AcctID", clientRef)
                            writer.WriteElementString("ClntCd", clientCode)
                            writer.WriteStartElement("Actns")
                            xmlWritten = True
                        End If
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        writer.WriteElementString("Tp", "SMS")
                        writer.WriteEndElement()  'actn
                    Case "Trace"
                        If Not xmlWritten Then
                            writer.WriteStartElement("Acct")
                            writer.WriteElementString("AcctID", clientRef)
                            writer.WriteElementString("ClntCd", clientCode)
                            writer.WriteStartElement("Actns")
                            xmlWritten = True
                        End If
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        writer.WriteElementString("Tp", "DTP")
                        writer.WriteStartElement("Actvts")
                        writer.WriteStartElement("Actvty")
                        writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                        writer.WriteElementString("Cd", "DTR")
                        writer.WriteEndElement()  'acvty
                        writer.WriteEndElement()  'actvts
                        writer.WriteEndElement()  'actn
                    Case Else
                        'If noteType <> "Stage" And noteType <> "Status changed" And noteType <> "Hold" And _
                        '     noteType <> "Unlinked" And noteType <> "Enf. Agent note" And noteType <> "Linked" And _
                        '     noteType <> "Removed agent" And noteType <> "Off hold" And _
                        '    noteType <> "Defer arrange" And noteType <> "Moved" And noteType <> "Clear arrange" And _
                        '    noteType <> "Client note" And noteType <> "Resolved" And noteType <> "Debt Changed" And _
                        '    noteType <> "Documents" And noteType <> "Address" And noteType <> "Employment" Then
                        '    MsgBox(debtorID & " New note type" & noteType)
                        'End If
                End Select
                lastNoteRowid = noteRowid
                lastNoteType = noteType
            Next

            'now get all Payments for last week
            'T73129 ignore direct payments
            Dim pay_dt As New DataTable
            'T54418 add payment type and source
            LoadDataTable2("DebtRecovery", "Select P.amount, P.date, P._rowid,P.amount_typeID,  P.amount_sourceID" & _
                          " from Payment P, PaySource PS" & _
                          " where debtorID = " & debtorID & _
                          " and amount <> 0 " & _
                          " and P.amount_sourceID = PS._rowID " & _
                          " and PS.direct = 'N'" & _
                          " and (P.status = 'W' or P.status = 'R')" & _
                          " and P._createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and P._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'", pay_dt, False)
            Dim firstPayment As Boolean = True
            Dim lastPayDate As Date = endDate
            For Each payRow In pay_dt.Rows
                Dim payDate As Date = payRow(1)
                Dim payAmount As Decimal = payRow(0)
                Dim payRowid As Integer = payRow(2)
                Dim payType As String = ""
                Dim onlinePayment As Boolean = False
                If (payRow(3) = 3 Or payRow(3) = 7) And payRow(4) = 49 Then
                    onlinePayment = True
                End If
                Dim firstPayRowid As Integer
                If Not xmlWritten Then
                    writer.WriteStartElement("Acct")
                    writer.WriteElementString("AcctID", clientRef)
                    writer.WriteElementString("ClntCd", clientCode)
                    writer.WriteStartElement("Actns")
                    xmlWritten = True
                End If
                firstPayment = False
                writer.WriteStartElement("Actn")
                writer.WriteElementString("ActnTS", Format(payDate, "yyyy-MM-dd") & "T" & Format(payDate, "HH:mm:ss"))
                writer.WriteElementString("Tp", "UNA")
                writer.WriteStartElement("Actvts")
                writer.WriteStartElement("Actvty")
                'T65854 if same payment date add a minute to time
                If lastPayDate = payDate Then
                    payDate = DateAdd("n", 1, payDate)
                End If
                writer.WriteElementString("Dt", Format(payDate, "yyyy-MM-dd") & "T" & Format(payDate, "HH:mm:ss"))
                lastPayDate = payDate
                'T54418
                If onlinePayment Then
                    writer.WriteElementString("Cd", "ONP")
                Else
                    writer.WriteElementString("Cd", "PAY")
                End If
                If caseStatus = "A" Then

                    Dim Array As Object()
                    Try
                        Array = GetSQLResultsArray2("DebtRecovery", "SELECT min(_rowid) " & _
                                            "FROM payment  " & _
                                            "WHERE debtorID = " & debtorID & _
                                            " and date >= '" & Format(arrangeStarted, "yyyy-MM-dd") & "'")
                        firstPayRowid = Array(0)
                    Catch ex As Exception
                        firstPayRowid = 0
                    End Try

                End If
                If caseStatus = "A" Then
                    If payDate < arrangeStarted Then
                        payType = "PAR"
                    Else
                        'is this first payment in arrangement?
                        firstPayRowid = GetSQLResults2("DebtRecovery", "SELECT min(_rowid) " & _
                                            "FROM payment  " & _
                                            "WHERE debtorID = " & debtorID & _
                                            " and date >= '" & Format(arrangeStarted, "yyyy-MM-dd") & "'")

                        If firstPayRowid = payRowid Then
                            payType = "AST"
                        Else
                            payType = "PAR"
                        End If
                    End If
                ElseIf caseStatus = "S" Or caseStatus = "F" Then
                    'pif if this is last payment
                    Dim lastPayRowid As Integer = GetSQLResults2("DebtRecovery", "SELECT max(_rowid) " & _
                                            "FROM payment  " & _
                                            "WHERE debtorID = " & debtorID)
                    If payRowid = lastPayRowid Then
                        payType = "PIF"
                    Else
                        payType = "PAR"
                    End If

                ElseIf caseStatus = "C" Then
                    'check for settlement
                    Try
                        If debtRow(4) = 30 Or debtRow(4) = 80 Then
                            payType = "SET"
                        End If
                    Catch ex As Exception
                        payType = "PAR"
                    End Try
                Else
                    payType = "PAR"
                End If
                writer.WriteStartElement("Pymt")
                writer.WriteElementString("PymtTp", payType)
                writer.WriteElementString("Val", Format(payAmount, "0.00"))
                Dim payMethod As String = "OTH"
                Select Case payRow(3)
                    Case 13, 65
                        payMethod = "BAC"
                    Case 3, 57
                        payMethod = "CC"
                    Case 1, 4, 51, 52, 61, 67
                        payMethod = "CHQ"
                    Case 2, 66
                        payMethod = "CSH"
                    Case 7, 58
                        payMethod = "DC"
                    Case 60
                        payMethod = "DD"
                    Case 11, 50
                        payMethod = "GRO"
                End Select
                writer.WriteElementString("Mtd", payMethod)
                writer.WriteEndElement()  'pymt
                writer.WriteEndElement()  'acvty

                'Tim put PPC here if required
                If caseStatus = "S" Or caseStatus = "F" Then
                    Dim prevArrangeNoteID As Integer = GetSQLResults2("DebtRecovery", "SELECT IFNULL(MAX(_rowID),-1) FROM note WHERE debtorID = " & debtorID & " and type ='Arrangement'")
                    Dim BrokenNoteID As Integer = GetSQLResults2("DebtRecovery", "SELECT IFNULL(MAX(_rowID),-1) FROM note WHERE debtorID = " & debtorID & " and type ='Broken'")
                    If prevArrangeNoteID > BrokenNoteID Then
                        writer.WriteStartElement("Actvty")
                        writer.WriteElementString("Dt", Format(payDate, "yyyy-MM-dd") & "T" & Format(payDate, "HH:mm:ss"))
                        writer.WriteElementString("Cd", "PPC")
                        writer.WriteEndElement()  'acvty
                    End If
                End If
                ' end of Tim section

                writer.WriteEndElement()  'actvts
                writer.WriteEndElement()  'actn
            Next 'payment
            'see if case returned last week
            If caseStatus = "C" Or caseStatus = "S" Then
                Dim returnDate As Date = Nothing
                Try
                    returnDate = debtRow(6)
                Catch ex As Exception

                End Try
                If returnDate <> Nothing Then
                    If Format(returnDate, "yyyy-MM-dd") >= Format(startDate, "yyyy-MM-dd") _
                        And Format(returnDate, "yyyy-MM-dd") <= Format(endDate, "yyyy-MM-dd") Then
                        If Not xmlWritten Then
                            writer.WriteStartElement("Acct")
                            writer.WriteElementString("AcctID", clientRef)
                            writer.WriteElementString("ClntCd", clientCode)
                            writer.WriteStartElement("Actns")
                            xmlWritten = True
                        End If
                        writer.WriteStartElement("Actn")
                        writer.WriteElementString("ActnTS", Format(returnDate, "yyyy-MM-dd") & "T" & Format(returnDate, "HH:mm:ss"))
                        'T54418()
                        Dim action As String = "UNA"
                        Dim activity As String = ""
                        If caseStatus = "C" Then
                            If debtRow(4) = 76 Then
                                action = "MAR"
                            End If
                            If debtRow(4) = 102 Then
                                activity = "FDF"
                            End If
                            If debtRow(4) = 24 Then
                                activity = "INS"
                            End If
                            If debtRow(4) = 11 Then
                                activity = "DCS"
                            End If
                        End If

                        If action = "MAR" Then
                            '5.11.2015 add address
                            Dim pcode As String
                            Try
                                pcode = debtRow(14)
                            Catch ex As Exception
                                pcode = ""
                            End Try
                            Dim address As String
                            Try
                                address = debtRow(13)
                            Catch ex As Exception
                                address = ""
                            End Try
                            get_address(address, pcode)
                            writer.WriteElementString("Tp", "MAR")
                            writer.WriteStartElement("CtctMtd")
                            writer.WriteStartElement("Adr")
                            writer.WriteElementString("Strt1", street1)
                            If street2 <> "" Then
                                writer.WriteElementString("Strt2", street2)
                            End If
                            If street3 <> "" Then
                                writer.WriteElementString("Strt3", street3)
                            End If
                            'T66170 if town is blank use city
                            If town = "" Then
                                town = city
                                city = ""
                            End If
                            writer.WriteElementString("Twn", town)
                            If city <> "" Then
                                writer.WriteElementString("Cty", city)
                            End If
                            'If postcode <> "" Then
                            writer.WriteElementString("PstCd", postcode)
                            'End If
                            writer.WriteEndElement()   'adr
                            writer.WriteEndElement()  'CtctMtd
                            writer.WriteStartElement("LtRet")
                            writer.WriteElementString("RtdGA", "true")
                            writer.WriteEndElement()  'LtRet
                            writer.WriteStartElement("Actvts")
                        Else
                            writer.WriteElementString("Tp", "UNA")
                            If activity <> "" Then
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(returnDate, "yyyy-MM-dd") & "T" & Format(returnDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", activity)
                                writer.WriteEndElement()  'acvty
                                'writer.WriteEndElement()  'actvts
                                'writer.WriteEndElement()  'actn
                            Else
                                writer.WriteStartElement("Actvts")
                            End If
                        End If

                        writer.WriteStartElement("Actvty")
                        writer.WriteElementString("Dt", Format(returnDate, "yyyy-MM-dd") & "T" & Format(returnDate, "HH:mm:ss"))
                        writer.WriteElementString("Cd", "ACL")

                        Dim closureReason As String = ""
                        If caseStatus = "C" Then
                            Dim startIDX As Integer = InStr(cancelledNote, "Note:")
                            If startIDX > 0 Then
                                closureReason = Microsoft.VisualBasic.Right(cancelledNote, cancelledNote.Length - startIDX - 5)
                            End If
                        Else
                            If caseStatus = "S" Then
                                closureReason = "PaidinFull"
                            End If
                        End If
                        'If closureReason <> "" Then
                        '    writer.WriteElementString("Notes", closureReason)
                        'End If
                        writer.WriteEndElement()  'acvty
                        writer.WriteEndElement()  'actvts
                        writer.WriteEndElement()  'actn
                    End If
                End If
            End If

            If xmlWritten Then
                case_no += 1
                writer.WriteEndElement()  'actns
                writer.WriteEndElement()  'acct
                'If case_no >= 10 Then
                '    Exit For
                'End If
            End If

        Next  'debtorID

        ' Next  'CSID

        writer.Close()
        'validate using xsd
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(activity_file)
        myDocument.Schemas.Add("ActivityData", "\\ross-helm-fp001\Rossendales Shared\vb.net\TDX XSD\Activity_Data.xsd")
        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
        write_audit(error_no & " - errors in report finished at " & Now, False)
        If error_no = 0 Then
            If prod_run Then
                'copy file to TDX HMRC ETC folder on R drive
                Dim newFilename As String = "\\ross-helm-fp001\Rossendales Shared\HMRC TDX ETC\" & Path.GetFileName(activity_file)
                My.Computer.FileSystem.CopyFile(activity_file, newFilename)

            End If
        End If
        Me.Close()
    End Sub
    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_audit("Error: " & e.Message, True)
            Case XmlSeverityType.Warning
                write_audit("Warning " & e.Message, True)
        End Select
    End Sub
    Private Sub write_audit(ByVal audit_message As String, ByVal errorMessage As Boolean)
        If errorMessage Then
            error_no += 1
        End If
        audit_message = audit_message & vbNewLine
        My.Computer.FileSystem.WriteAllText(auditFile, audit_message, True)
    End Sub
    Private Sub get_address(ByVal addr As String, ByVal pcode As String)
        street1 = ""
        street2 = ""
        street3 = ""
        town = ""
        city = ""
        postcode = ""
        Try
            postcode = pcode
        Catch ex As Exception

        End Try
        Dim address = ""
        Try
            address = addr
        Catch ex As Exception
            Return
        End Try
        Dim addressline As String = ""
        For idx = 1 To address.Length
            If Mid(address, idx, 1) = Chr(10) Or Mid(address, idx, 1) = Chr(13) Then
                If street1 = "" Then
                    street1 = addressline
                ElseIf street2 = "" Then
                    street2 = addressline
                ElseIf town = "" Then
                    town = addressline
                ElseIf city = "" Then
                    city = addressline
                End If
                addressline = ""
            Else
                addressline &= Mid(address, idx, 1)
            End If
        Next
        'check for postcode and shuffle town/city
        If city = postcode Then
            city = ""
        ElseIf town = postcode Then
            town = ""
        ElseIf street2 = postcode Then
            street2 = ""
        End If
        If UCase(city) = "GB" Then
            city = ""
        End If
        If UCase(town) = "GB" Then
            town = ""
        End If
        If city = "" Then
            city = town
            town = street2
            street2 = ""
        End If
        If city = "" Then
            city = town
            town = street2
            street2 = ""
        End If
        If IsNumeric(Microsoft.VisualBasic.Left(town, 1)) Then
            If street2 = "" Then
                street2 = town
                town = ""
            Else
                street3 = town
                town = ""
            End If
        End If

    End Sub
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        run_report()
    End Sub
End Class

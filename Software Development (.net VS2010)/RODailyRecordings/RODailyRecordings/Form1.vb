﻿Public Class Form1
    Dim filePath As String
    Dim foldername As String = ""
    Dim folderpath As String
    Dim outfile As String = ""
    Dim prod_run As Boolean = False
    Dim env_str As String = ""
    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        filePath = "\\mgl-abwd-fs001\eaaudio$\"
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If
        'get all folders starting with dev
        Dim runDate As Date = DateAdd(DateInterval.Day, -1, Now)
        Dim filename As String = "\\ross-helm-fp001\Rossendales Shared\ROD\RO_" & Format(Now, "yyyyMMdd") & ".csv"
        Dim searchString As String = "*" & Format(runDate, "dd-MM-yyyy") & ".wav"
        For Each foundFile As String In My.Computer.FileSystem.GetFiles _
               (filePath, FileIO.SearchOption.SearchAllSubDirectories, searchString)
            'split outfile into constituent parts
            Dim reportLine As String = Replace(foundFile, "\\mgl-abwd-fs001\eaaudio$\", "")
            reportLine = Replace(reportLine, ".wav", "")
            reportLine = Replace(reportLine, "_", ",")
            reportLine = Replace(reportLine, "\", ",")
            reportLine = Replace(reportLine, Format(runDate, "dd/MM/yyyy"), "")
            reportLine = Replace(reportLine, Format(runDate, "dd-MM-yyyy"), "")
            reportLine = Replace(reportLine, "-", ",")
            outfile &= reportLine & vbNewLine
        Next

        If prod_run Then
            My.Computer.FileSystem.WriteAllText(filename, outfile, False, System.Text.Encoding.ASCII)
        End If


        Me.Close()
    End Sub
End Class

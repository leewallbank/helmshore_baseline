﻿Imports CommonLibrary

Public Class clsBailiffData

    Private BailiffDT As New DataTable
    Private BailiffDV As DataView

    'Protected Overrides Sub Finalize()
    '    If Not IsNothing(BailiffDV) Then BailiffDV.Dispose()

    '    MyBase.Finalize()
    'End Sub

    Public ReadOnly Property BailiffDataView() As DataView
        Get
            BailiffDataView = BailiffDV
        End Get
    End Property

    Public Sub GetBailiffs(ByVal ParamList As String())
        Try
            BailiffDT.Clear()
            BailiffDT.Reset()

            For Each Param As String In ParamList
                LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffs " & Param, BailiffDT, True) ' was raw
            Next Param

            BailiffDV = New DataView(BailiffDT.DefaultView.ToTable(True)) ' The true option removes duplicates. Shouldn't be any but caters for future change
            BailiffDV.AllowDelete = False
            BailiffDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

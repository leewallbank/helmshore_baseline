﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String
    Private OutputFile As String


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        btnViewInputFile.Enabled = False
        btnViewLogFile.Enabled = False
        btnViewOutputFile.Enabled = False

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click

        Try
            Dim FileDialog As New OpenFileDialog
            Dim AuditLog As String, FileContents() As String, InputLineArray() As String, ErrorLog As String = ""
            Dim TotCases As Integer = 0
            Dim NewDebtBalance As Decimal = 0

            btnViewInputFile.Enabled = False
            btnViewLogFile.Enabled = False
            btnViewOutputFile.Enabled = False

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then File.Delete(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then File.Delete(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_PreProcess.txt") Then File.Delete(InputFilePath & FileName & "_PreProcess.txt")

            lblReadingFile.Visible = True
            Application.DoEvents()
            FileContents = System.IO.File.ReadAllLines(FileDialog.FileName)

            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents)

            For RowNum = 0 To UBound(FileContents)

                Application.DoEvents() ' without this line, the button disappears until processing is complete
                ProgressBar.Value = RowNum

                If String.IsNullOrEmpty(FileContents(RowNum)) Then Continue For
                inputLineArray = FileContents(RowNum).Split(vbTab)

                'For ColNum As Integer = 0 To 27
                '    'check for invalid characters
                '    'Dim colValue As String = inputLineArray(ColNum)
                '    'colValue = checkInvalidChars(colValue)
                '    OutputFile &= inputLineArray(ColNum) & "|"
                'Next ColNum

                'OutputFile &= inputLineArray(28) & vbNewLine

                'OutputFile = String.Join("|", InputLineArray) commented out TS 03/Sep/2015. Request ref 51657
                ' This section added TS 03/Sep/2015. Request ref 51657
                Dim OutputLineArray(UBound(InputLineArray) - 1)

                OutputLineArray(0) = InputLineArray(0) & InputLineArray(1)

                For LoopCount As Integer = 1 To UBound(OutputLineArray)
                    OutputLineArray(LoopCount) = InputLineArray(LoopCount + 1)
                Next LoopCount

                OutputFile = String.Join("|", OutputLineArray)
                ' end of new section

                If RowNum > 0 Then

                    OutputFile &= "|"

                    If inputLineArray(10) <> "GBP" Then
                        MsgBox("Notification current is not GBP at line " & (RowNum + 1).ToString, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, Me.Text)
                        ErrorLog &= "Notification current is not GBP at line " & (RowNum + 1).ToString & vbCrLf
                    End If

                    OutputFile &= ToNote(InputLineArray(11), "Posting Date", ";")(0)
                    OutputFile &= ToNote(InputLineArray(13), "1. Reminder Amt", ";")(0)
                    OutputFile &= ToNote(inputLineArray(14), "Reminded Date", ";")(0)
                    OutputFile &= ToNote(inputLineArray(15), "Reminder Due Date", ";")(0)
                    OutputFile &= ToNote(inputLineArray(16), "2. Reminder Amt", ";")(0)
                    OutputFile &= ToNote(InputLineArray(17), "Paid Amt", ";")(0)
                    OutputFile &= ToNote(inputLineArray(20), "Client Country", ";")(0)
                    OutputFile &= ToNote(inputLineArray(21), "Client City", ";")(0)
                    OutputFile &= ToNote(inputLineArray(24), "PCN", ";")(0)
                    OutputFile &= ToNote(inputLineArray(27), "Time Of Contravention", ";")(0)

                    OutputFile &= vbNewLine

                    NewDebtBalance += inputLineArray(18)
                    TotCases += 1

                    AppendToFile(InputFilePath & FileName & "_PreProcess.txt", OutputFile)

                End If

            Next RowNum

            ' Update audit log
            AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf & vbCrLf & vbCrLf

            AuditLog &= "Number of new cases: " & totCases & vbCrLf
            AuditLog &= "Total balance of new cases: " & NewDebtBalance.ToString & vbCrLf & vbCrLf & vbCrLf

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            ProgressBar.Value = ProgressBar.Maximum

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try
            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function checkInvalidChars(ByVal colValue As String) As String
        Dim idx As Integer
        Dim newColValue As String = ""
        If colValue <> "" Then
            For idx = 1 To colValue.Length
                Dim char2 As Integer
                char2 = AscW(Mid(colValue, idx, 1))
                If char2 <= 127 Then
                    newColValue &= newColValue & " "
                Else
                    newColValue &= Mid(colValue, idx, 1)
                End If
            Next
                  End If
        Return newColValue
    End Function

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try
            If File.Exists(InputFilePath & FileName & "_PreProcess.txt") Then System.Diagnostics.Process.Start((InputFilePath & FileName & "_PreProcess.txt"))

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try
            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub AppendToFile(ByVal FileName As String, ByVal Content As String)

        If (Not System.IO.Directory.Exists(Path.GetDirectoryName(FileName))) Then System.IO.Directory.CreateDirectory(Path.GetDirectoryName(FileName))

        Using Writer As StreamWriter = New StreamWriter(FileName, True, System.Text.Encoding.Unicode)

            Writer.Write(Content)
        End Using
    End Sub
End Class



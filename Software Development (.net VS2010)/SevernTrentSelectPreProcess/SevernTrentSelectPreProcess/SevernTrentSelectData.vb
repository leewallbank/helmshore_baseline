﻿Imports CommonLibrary

Public Class SevernTrentSelectData
    Public Function GetCaseLive(ByVal ClientRef As String) As String
        ' Used to check if a case is already loaded
        GetCaseLive = GetSQLResults("DebtRecovery", "SELECT COUNT(*) FROM debtor AS d INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID WHERE d.client_ref = '" & ClientRef & "' AND d.status_open_closed = 'O' AND cs.clientID = 1931")
    End Function

    Public Function GetLastFileSequence() As String
        GetLastFileSequence = GetSQLResults("FeesSQL", "SELECT ISNULL(MAX(FileSequence), 0) FROM dbo.SevernTrentSelectCaseBatch")
    End Function

    Public Sub SetLastFileSequence(ByVal FileSequence As Integer, ByVal CasesLoaded As Integer, ByVal CasesLoadedValue As Decimal)
        ExecStoredProc("FeesSQL", " EXEC dbo.SetSevernTrentSelectCaseBatch " & FileSequence.ToString & ", " & CasesLoaded.ToString & ", " & CasesLoadedValue.ToString)
    End Sub
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEAUpdateSummary
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEAUpdateSummary))
        Me.cboCompany = New System.Windows.Forms.ComboBox()
        Me.cmsForm = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmsList = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.lblSummary = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmsSummary = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmdArrangementMadeClear = New System.Windows.Forms.Button()
        Me.dgvArrangementMade = New System.Windows.Forms.DataGridView()
        Me.ArrangementMade = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdArrangementMadeAll = New System.Windows.Forms.Button()
        Me.RefreshCheckTimer = New System.Windows.Forms.Timer(Me.components)
        Me.dgvVisitedNotLive = New System.Windows.Forms.DataGridView()
        Me.VisitedNotLive = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdRefreshDB = New System.Windows.Forms.Button()
        Me.cmdVisitedNotLiveClear = New System.Windows.Forms.Button()
        Me.cmdVisitedNotLiveAll = New System.Windows.Forms.Button()
        Me.CGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdCollectedClear = New System.Windows.Forms.Button()
        Me.cmdCollectedAll = New System.Windows.Forms.Button()
        Me.pnlAbsPerc = New System.Windows.Forms.Panel()
        Me.radPerc = New System.Windows.Forms.RadioButton()
        Me.radAbs = New System.Windows.Forms.RadioButton()
        Me.lblLine1 = New System.Windows.Forms.Label()
        Me.cmdClearAll = New System.Windows.Forms.Button()
        Me.cmdCGAClear = New System.Windows.Forms.Button()
        Me.cmdCGAAll = New System.Windows.Forms.Button()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvCollected = New System.Windows.Forms.DataGridView()
        Me.Collected = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvSummary = New System.Windows.Forms.DataGridView()
        Me.dgvCGA = New System.Windows.Forms.DataGridView()
        Me.cboEATeam = New System.Windows.Forms.ComboBox()
        Me.cmdEANameClear = New System.Windows.Forms.Button()
        Me.cmdEANameAll = New System.Windows.Forms.Button()
        Me.dgvEAName = New System.Windows.Forms.DataGridView()
        Me.EAName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colClientNameTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvNOI = New System.Windows.Forms.DataGridView()
        Me.NOI = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdNOIClear = New System.Windows.Forms.Button()
        Me.cmdNOIAll = New System.Windows.Forms.Button()
        Me.cmdVisitedClear = New System.Windows.Forms.Button()
        Me.dgvVisited = New System.Windows.Forms.DataGridView()
        Me.Visited = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdVisitedAll = New System.Windows.Forms.Button()
        Me.dgvWorkType = New System.Windows.Forms.DataGridView()
        Me.WorkType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdWorkTypeClear = New System.Windows.Forms.Button()
        Me.cmdWorkTypeAll = New System.Windows.Forms.Button()
        Me.cmdAuditCheckAll = New System.Windows.Forms.Button()
        Me.cmdAuditCheckClear = New System.Windows.Forms.Button()
        Me.dgvAuditCheck = New System.Windows.Forms.DataGridView()
        Me.AuditCheck = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvGoneAway = New System.Windows.Forms.DataGridView()
        Me.GoneAway = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdGoneAwayAll = New System.Windows.Forms.Button()
        Me.cmdGoneAwayClear = New System.Windows.Forms.Button()
        Me.dgvVisitAfterPIF = New System.Windows.Forms.DataGridView()
        Me.VisitAfterPIF = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdVisitAfterPIFAll = New System.Windows.Forms.Button()
        Me.cmdVisitAfterPIFClear = New System.Windows.Forms.Button()
        Me.cmdAuditCheckByClear = New System.Windows.Forms.Button()
        Me.cmdAuditCheckByAll = New System.Windows.Forms.Button()
        Me.dgvAuditCheckBy = New System.Windows.Forms.DataGridView()
        Me.AuditCheckBy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblPeriodType = New System.Windows.Forms.Label()
        Me.cboPeriodType = New System.Windows.Forms.ComboBox()
        Me.dgvNumberOfVisitsPA = New System.Windows.Forms.DataGridView()
        Me.NumberOfVisitsPA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdNumberOfVisitsPAClear = New System.Windows.Forms.Button()
        Me.cmdNumberOfVisitsPAAll = New System.Windows.Forms.Button()
        Me.cmdClientNameClear = New System.Windows.Forms.Button()
        Me.cmdClientNameAll = New System.Windows.Forms.Button()
        Me.dgvClientName = New System.Windows.Forms.DataGridView()
        Me.ClientName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvArrangementMade, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvVisitedNotLive, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAbsPerc.SuspendLayout()
        CType(Me.dgvCollected, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCGA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEAName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvNOI, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvVisited, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvWorkType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAuditCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvGoneAway, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvVisitAfterPIF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAuditCheckBy, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvNumberOfVisitsPA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(6, 7)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(160, 21)
        Me.cboCompany.TabIndex = 161
        '
        'cmsForm
        '
        Me.cmsForm.Name = "ContextMenuStrip1"
        Me.cmsForm.Size = New System.Drawing.Size(61, 4)
        '
        'cmsList
        '
        Me.cmsList.Name = "ContextMenuStrip1"
        Me.cmsList.Size = New System.Drawing.Size(61, 4)
        '
        'lblSummary
        '
        Me.lblSummary.AutoSize = True
        Me.lblSummary.Location = New System.Drawing.Point(87, 350)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(10, 13)
        Me.lblSummary.TabIndex = 148
        Me.lblSummary.Text = " "
        '
        'cmsSummary
        '
        Me.cmsSummary.Name = "ContextMenuStrip1"
        Me.cmsSummary.Size = New System.Drawing.Size(61, 4)
        '
        'cmdArrangementMadeClear
        '
        Me.cmdArrangementMadeClear.Location = New System.Drawing.Point(800, 312)
        Me.cmdArrangementMadeClear.Name = "cmdArrangementMadeClear"
        Me.cmdArrangementMadeClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdArrangementMadeClear.TabIndex = 151
        Me.cmdArrangementMadeClear.Text = "Clear"
        Me.cmdArrangementMadeClear.UseVisualStyleBackColor = True
        '
        'dgvArrangementMade
        '
        Me.dgvArrangementMade.AllowUserToAddRows = False
        Me.dgvArrangementMade.AllowUserToDeleteRows = False
        Me.dgvArrangementMade.AllowUserToResizeColumns = False
        Me.dgvArrangementMade.AllowUserToResizeRows = False
        Me.dgvArrangementMade.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvArrangementMade.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ArrangementMade, Me.DataGridViewTextBoxColumn10})
        Me.dgvArrangementMade.Location = New System.Drawing.Point(738, 225)
        Me.dgvArrangementMade.Name = "dgvArrangementMade"
        Me.dgvArrangementMade.ReadOnly = True
        Me.dgvArrangementMade.RowHeadersVisible = False
        Me.dgvArrangementMade.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvArrangementMade.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvArrangementMade.Size = New System.Drawing.Size(112, 87)
        Me.dgvArrangementMade.TabIndex = 149
        '
        'ArrangementMade
        '
        Me.ArrangementMade.DataPropertyName = "ArrangementMade"
        Me.ArrangementMade.HeaderText = "Arrangement"
        Me.ArrangementMade.Name = "ArrangementMade"
        Me.ArrangementMade.ReadOnly = True
        Me.ArrangementMade.Width = 70
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 40
        '
        'cmdArrangementMadeAll
        '
        Me.cmdArrangementMadeAll.Location = New System.Drawing.Point(738, 312)
        Me.cmdArrangementMadeAll.Name = "cmdArrangementMadeAll"
        Me.cmdArrangementMadeAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdArrangementMadeAll.TabIndex = 150
        Me.cmdArrangementMadeAll.Text = "All"
        Me.cmdArrangementMadeAll.UseVisualStyleBackColor = True
        '
        'RefreshCheckTimer
        '
        Me.RefreshCheckTimer.Interval = 30000
        '
        'dgvVisitedNotLive
        '
        Me.dgvVisitedNotLive.AllowUserToAddRows = False
        Me.dgvVisitedNotLive.AllowUserToDeleteRows = False
        Me.dgvVisitedNotLive.AllowUserToResizeColumns = False
        Me.dgvVisitedNotLive.AllowUserToResizeRows = False
        Me.dgvVisitedNotLive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvVisitedNotLive.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.VisitedNotLive, Me.DataGridViewTextBoxColumn11})
        Me.dgvVisitedNotLive.Location = New System.Drawing.Point(621, 153)
        Me.dgvVisitedNotLive.Name = "dgvVisitedNotLive"
        Me.dgvVisitedNotLive.ReadOnly = True
        Me.dgvVisitedNotLive.RowHeadersVisible = False
        Me.dgvVisitedNotLive.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvVisitedNotLive.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVisitedNotLive.Size = New System.Drawing.Size(112, 159)
        Me.dgvVisitedNotLive.TabIndex = 152
        '
        'VisitedNotLive
        '
        Me.VisitedNotLive.DataPropertyName = "VisitedNotLive"
        Me.VisitedNotLive.HeaderText = "Not live"
        Me.VisitedNotLive.Name = "VisitedNotLive"
        Me.VisitedNotLive.ReadOnly = True
        Me.VisitedNotLive.Width = 70
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 40
        '
        'cmdRefreshDB
        '
        Me.cmdRefreshDB.Location = New System.Drawing.Point(6, 346)
        Me.cmdRefreshDB.Name = "cmdRefreshDB"
        Me.cmdRefreshDB.Size = New System.Drawing.Size(75, 20)
        Me.cmdRefreshDB.TabIndex = 137
        Me.cmdRefreshDB.Text = "Refresh DB"
        Me.cmdRefreshDB.UseVisualStyleBackColor = True
        '
        'cmdVisitedNotLiveClear
        '
        Me.cmdVisitedNotLiveClear.Location = New System.Drawing.Point(683, 312)
        Me.cmdVisitedNotLiveClear.Name = "cmdVisitedNotLiveClear"
        Me.cmdVisitedNotLiveClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdVisitedNotLiveClear.TabIndex = 154
        Me.cmdVisitedNotLiveClear.Text = "Clear"
        Me.cmdVisitedNotLiveClear.UseVisualStyleBackColor = True
        '
        'cmdVisitedNotLiveAll
        '
        Me.cmdVisitedNotLiveAll.Location = New System.Drawing.Point(621, 312)
        Me.cmdVisitedNotLiveAll.Name = "cmdVisitedNotLiveAll"
        Me.cmdVisitedNotLiveAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdVisitedNotLiveAll.TabIndex = 153
        Me.cmdVisitedNotLiveAll.Text = "All"
        Me.cmdVisitedNotLiveAll.UseVisualStyleBackColor = True
        '
        'CGA
        '
        Me.CGA.DataPropertyName = "CGA"
        Me.CGA.HeaderText = "CGA"
        Me.CGA.Name = "CGA"
        Me.CGA.ReadOnly = True
        Me.CGA.Width = 70
        '
        'cmdCollectedClear
        '
        Me.cmdCollectedClear.Location = New System.Drawing.Point(565, 312)
        Me.cmdCollectedClear.Name = "cmdCollectedClear"
        Me.cmdCollectedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdCollectedClear.TabIndex = 126
        Me.cmdCollectedClear.Text = "Clear"
        Me.cmdCollectedClear.UseVisualStyleBackColor = True
        '
        'cmdCollectedAll
        '
        Me.cmdCollectedAll.Location = New System.Drawing.Point(503, 312)
        Me.cmdCollectedAll.Name = "cmdCollectedAll"
        Me.cmdCollectedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdCollectedAll.TabIndex = 125
        Me.cmdCollectedAll.Text = "All"
        Me.cmdCollectedAll.UseVisualStyleBackColor = True
        '
        'pnlAbsPerc
        '
        Me.pnlAbsPerc.Controls.Add(Me.radPerc)
        Me.pnlAbsPerc.Controls.Add(Me.radAbs)
        Me.pnlAbsPerc.Location = New System.Drawing.Point(957, 344)
        Me.pnlAbsPerc.Name = "pnlAbsPerc"
        Me.pnlAbsPerc.Size = New System.Drawing.Size(99, 25)
        Me.pnlAbsPerc.TabIndex = 132
        '
        'radPerc
        '
        Me.radPerc.AutoSize = True
        Me.radPerc.Location = New System.Drawing.Point(55, 4)
        Me.radPerc.Name = "radPerc"
        Me.radPerc.Size = New System.Drawing.Size(33, 17)
        Me.radPerc.TabIndex = 4
        Me.radPerc.TabStop = True
        Me.radPerc.Text = "%"
        Me.radPerc.UseVisualStyleBackColor = True
        '
        'radAbs
        '
        Me.radAbs.AutoSize = True
        Me.radAbs.Location = New System.Drawing.Point(6, 4)
        Me.radAbs.Name = "radAbs"
        Me.radAbs.Size = New System.Drawing.Size(43, 17)
        Me.radAbs.TabIndex = 3
        Me.radAbs.TabStop = True
        Me.radAbs.Text = "Abs"
        Me.radAbs.UseVisualStyleBackColor = True
        '
        'lblLine1
        '
        Me.lblLine1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLine1.Location = New System.Drawing.Point(6, 337)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(1128, 1)
        Me.lblLine1.TabIndex = 131
        '
        'cmdClearAll
        '
        Me.cmdClearAll.Location = New System.Drawing.Point(1064, 345)
        Me.cmdClearAll.Name = "cmdClearAll"
        Me.cmdClearAll.Size = New System.Drawing.Size(68, 20)
        Me.cmdClearAll.TabIndex = 130
        Me.cmdClearAll.Text = "Clear All"
        Me.cmdClearAll.UseVisualStyleBackColor = True
        '
        'cmdCGAClear
        '
        Me.cmdCGAClear.Location = New System.Drawing.Point(919, 72)
        Me.cmdCGAClear.Name = "cmdCGAClear"
        Me.cmdCGAClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdCGAClear.TabIndex = 122
        Me.cmdCGAClear.Text = "Clear"
        Me.cmdCGAClear.UseVisualStyleBackColor = True
        '
        'cmdCGAAll
        '
        Me.cmdCGAAll.Location = New System.Drawing.Point(856, 72)
        Me.cmdCGAAll.Name = "cmdCGAAll"
        Me.cmdCGAAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdCGAAll.TabIndex = 121
        Me.cmdCGAAll.Text = "All"
        Me.cmdCGAAll.UseVisualStyleBackColor = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 40
        '
        'dgvCollected
        '
        Me.dgvCollected.AllowUserToAddRows = False
        Me.dgvCollected.AllowUserToDeleteRows = False
        Me.dgvCollected.AllowUserToResizeColumns = False
        Me.dgvCollected.AllowUserToResizeRows = False
        Me.dgvCollected.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCollected.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Collected, Me.DataGridViewTextBoxColumn2})
        Me.dgvCollected.Location = New System.Drawing.Point(503, 225)
        Me.dgvCollected.Name = "dgvCollected"
        Me.dgvCollected.ReadOnly = True
        Me.dgvCollected.RowHeadersVisible = False
        Me.dgvCollected.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvCollected.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCollected.Size = New System.Drawing.Size(112, 87)
        Me.dgvCollected.TabIndex = 108
        '
        'Collected
        '
        Me.Collected.DataPropertyName = "Collected"
        Me.Collected.HeaderText = "Collected"
        Me.Collected.Name = "Collected"
        Me.Collected.ReadOnly = True
        Me.Collected.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 40
        '
        'dgvSummary
        '
        Me.dgvSummary.AllowUserToAddRows = False
        Me.dgvSummary.AllowUserToDeleteRows = False
        Me.dgvSummary.AllowUserToOrderColumns = True
        Me.dgvSummary.AllowUserToResizeRows = False
        Me.dgvSummary.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSummary.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSummary.Location = New System.Drawing.Point(6, 375)
        Me.dgvSummary.Name = "dgvSummary"
        Me.dgvSummary.ReadOnly = True
        Me.dgvSummary.RowHeadersVisible = False
        Me.dgvSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvSummary.Size = New System.Drawing.Size(1128, 198)
        Me.dgvSummary.TabIndex = 105
        '
        'dgvCGA
        '
        Me.dgvCGA.AllowUserToAddRows = False
        Me.dgvCGA.AllowUserToDeleteRows = False
        Me.dgvCGA.AllowUserToResizeColumns = False
        Me.dgvCGA.AllowUserToResizeRows = False
        Me.dgvCGA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCGA.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CGA, Me.DataGridViewTextBoxColumn4})
        Me.dgvCGA.Location = New System.Drawing.Point(856, 7)
        Me.dgvCGA.Name = "dgvCGA"
        Me.dgvCGA.ReadOnly = True
        Me.dgvCGA.RowHeadersVisible = False
        Me.dgvCGA.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvCGA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCGA.Size = New System.Drawing.Size(112, 65)
        Me.dgvCGA.TabIndex = 109
        '
        'cboEATeam
        '
        Me.cboEATeam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEATeam.FormattingEnabled = True
        Me.cboEATeam.Location = New System.Drawing.Point(6, 291)
        Me.cboEATeam.Name = "cboEATeam"
        Me.cboEATeam.Size = New System.Drawing.Size(160, 21)
        Me.cboEATeam.TabIndex = 178
        '
        'cmdEANameClear
        '
        Me.cmdEANameClear.Location = New System.Drawing.Point(92, 312)
        Me.cmdEANameClear.Name = "cmdEANameClear"
        Me.cmdEANameClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdEANameClear.TabIndex = 177
        Me.cmdEANameClear.Text = "Clear"
        Me.cmdEANameClear.UseVisualStyleBackColor = True
        '
        'cmdEANameAll
        '
        Me.cmdEANameAll.Location = New System.Drawing.Point(30, 312)
        Me.cmdEANameAll.Name = "cmdEANameAll"
        Me.cmdEANameAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdEANameAll.TabIndex = 176
        Me.cmdEANameAll.Text = "All"
        Me.cmdEANameAll.UseVisualStyleBackColor = True
        '
        'dgvEAName
        '
        Me.dgvEAName.AllowUserToAddRows = False
        Me.dgvEAName.AllowUserToDeleteRows = False
        Me.dgvEAName.AllowUserToResizeColumns = False
        Me.dgvEAName.AllowUserToResizeRows = False
        Me.dgvEAName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEAName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EAName, Me.colClientNameTotal})
        Me.dgvEAName.Location = New System.Drawing.Point(6, 33)
        Me.dgvEAName.Name = "dgvEAName"
        Me.dgvEAName.ReadOnly = True
        Me.dgvEAName.RowHeadersVisible = False
        Me.dgvEAName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEAName.Size = New System.Drawing.Size(160, 257)
        Me.dgvEAName.TabIndex = 175
        '
        'EAName
        '
        Me.EAName.DataPropertyName = "EAName"
        Me.EAName.HeaderText = "Name"
        Me.EAName.Name = "EAName"
        Me.EAName.ReadOnly = True
        '
        'colClientNameTotal
        '
        Me.colClientNameTotal.DataPropertyName = "Total"
        Me.colClientNameTotal.HeaderText = "Total"
        Me.colClientNameTotal.Name = "colClientNameTotal"
        Me.colClientNameTotal.ReadOnly = True
        Me.colClientNameTotal.Width = 40
        '
        'dgvNOI
        '
        Me.dgvNOI.AllowUserToAddRows = False
        Me.dgvNOI.AllowUserToDeleteRows = False
        Me.dgvNOI.AllowUserToResizeColumns = False
        Me.dgvNOI.AllowUserToResizeRows = False
        Me.dgvNOI.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvNOI.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NOI, Me.DataGridViewTextBoxColumn6})
        Me.dgvNOI.Location = New System.Drawing.Point(739, 113)
        Me.dgvNOI.Name = "dgvNOI"
        Me.dgvNOI.ReadOnly = True
        Me.dgvNOI.RowHeadersVisible = False
        Me.dgvNOI.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvNOI.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvNOI.Size = New System.Drawing.Size(112, 65)
        Me.dgvNOI.TabIndex = 111
        '
        'NOI
        '
        Me.NOI.DataPropertyName = "NOI"
        Me.NOI.HeaderText = "NOI"
        Me.NOI.Name = "NOI"
        Me.NOI.ReadOnly = True
        Me.NOI.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.NOI.Width = 70
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 40
        '
        'cmdNOIClear
        '
        Me.cmdNOIClear.Location = New System.Drawing.Point(801, 178)
        Me.cmdNOIClear.Name = "cmdNOIClear"
        Me.cmdNOIClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdNOIClear.TabIndex = 124
        Me.cmdNOIClear.Text = "Clear"
        Me.cmdNOIClear.UseVisualStyleBackColor = True
        '
        'cmdNOIAll
        '
        Me.cmdNOIAll.Location = New System.Drawing.Point(739, 178)
        Me.cmdNOIAll.Name = "cmdNOIAll"
        Me.cmdNOIAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdNOIAll.TabIndex = 123
        Me.cmdNOIAll.Text = "All"
        Me.cmdNOIAll.UseVisualStyleBackColor = True
        '
        'cmdVisitedClear
        '
        Me.cmdVisitedClear.Location = New System.Drawing.Point(683, 114)
        Me.cmdVisitedClear.Name = "cmdVisitedClear"
        Me.cmdVisitedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdVisitedClear.TabIndex = 181
        Me.cmdVisitedClear.Text = "Clear"
        Me.cmdVisitedClear.UseVisualStyleBackColor = True
        '
        'dgvVisited
        '
        Me.dgvVisited.AllowUserToAddRows = False
        Me.dgvVisited.AllowUserToDeleteRows = False
        Me.dgvVisited.AllowUserToResizeColumns = False
        Me.dgvVisited.AllowUserToResizeRows = False
        Me.dgvVisited.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVisited.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Visited, Me.DataGridViewTextBoxColumn5})
        Me.dgvVisited.Location = New System.Drawing.Point(621, 7)
        Me.dgvVisited.Name = "dgvVisited"
        Me.dgvVisited.ReadOnly = True
        Me.dgvVisited.RowHeadersVisible = False
        Me.dgvVisited.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvVisited.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVisited.Size = New System.Drawing.Size(112, 107)
        Me.dgvVisited.TabIndex = 179
        '
        'Visited
        '
        Me.Visited.DataPropertyName = "Visited"
        Me.Visited.HeaderText = "Visited"
        Me.Visited.Name = "Visited"
        Me.Visited.ReadOnly = True
        Me.Visited.Width = 70
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 40
        '
        'cmdVisitedAll
        '
        Me.cmdVisitedAll.Location = New System.Drawing.Point(621, 114)
        Me.cmdVisitedAll.Name = "cmdVisitedAll"
        Me.cmdVisitedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdVisitedAll.TabIndex = 180
        Me.cmdVisitedAll.Text = "All"
        Me.cmdVisitedAll.UseVisualStyleBackColor = True
        '
        'dgvWorkType
        '
        Me.dgvWorkType.AllowUserToAddRows = False
        Me.dgvWorkType.AllowUserToDeleteRows = False
        Me.dgvWorkType.AllowUserToResizeColumns = False
        Me.dgvWorkType.AllowUserToResizeRows = False
        Me.dgvWorkType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvWorkType.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WorkType, Me.DataGridViewTextBoxColumn3})
        Me.dgvWorkType.Location = New System.Drawing.Point(338, 7)
        Me.dgvWorkType.Name = "dgvWorkType"
        Me.dgvWorkType.ReadOnly = True
        Me.dgvWorkType.RowHeadersVisible = False
        Me.dgvWorkType.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvWorkType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvWorkType.Size = New System.Drawing.Size(160, 111)
        Me.dgvWorkType.TabIndex = 182
        '
        'WorkType
        '
        Me.WorkType.DataPropertyName = "WorkType"
        Me.WorkType.HeaderText = "Work Type"
        Me.WorkType.Name = "WorkType"
        Me.WorkType.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 40
        '
        'cmdWorkTypeClear
        '
        Me.cmdWorkTypeClear.Location = New System.Drawing.Point(424, 119)
        Me.cmdWorkTypeClear.Name = "cmdWorkTypeClear"
        Me.cmdWorkTypeClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdWorkTypeClear.TabIndex = 184
        Me.cmdWorkTypeClear.Text = "Clear"
        Me.cmdWorkTypeClear.UseVisualStyleBackColor = True
        '
        'cmdWorkTypeAll
        '
        Me.cmdWorkTypeAll.Location = New System.Drawing.Point(362, 119)
        Me.cmdWorkTypeAll.Name = "cmdWorkTypeAll"
        Me.cmdWorkTypeAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdWorkTypeAll.TabIndex = 183
        Me.cmdWorkTypeAll.Text = "All"
        Me.cmdWorkTypeAll.UseVisualStyleBackColor = True
        '
        'cmdAuditCheckAll
        '
        Me.cmdAuditCheckAll.Location = New System.Drawing.Point(503, 94)
        Me.cmdAuditCheckAll.Name = "cmdAuditCheckAll"
        Me.cmdAuditCheckAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdAuditCheckAll.TabIndex = 186
        Me.cmdAuditCheckAll.Text = "All"
        Me.cmdAuditCheckAll.UseVisualStyleBackColor = True
        '
        'cmdAuditCheckClear
        '
        Me.cmdAuditCheckClear.Location = New System.Drawing.Point(565, 94)
        Me.cmdAuditCheckClear.Name = "cmdAuditCheckClear"
        Me.cmdAuditCheckClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdAuditCheckClear.TabIndex = 187
        Me.cmdAuditCheckClear.Text = "Clear"
        Me.cmdAuditCheckClear.UseVisualStyleBackColor = True
        '
        'dgvAuditCheck
        '
        Me.dgvAuditCheck.AllowUserToAddRows = False
        Me.dgvAuditCheck.AllowUserToDeleteRows = False
        Me.dgvAuditCheck.AllowUserToResizeColumns = False
        Me.dgvAuditCheck.AllowUserToResizeRows = False
        Me.dgvAuditCheck.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvAuditCheck.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AuditCheck, Me.DataGridViewTextBoxColumn8})
        Me.dgvAuditCheck.Location = New System.Drawing.Point(503, 7)
        Me.dgvAuditCheck.Name = "dgvAuditCheck"
        Me.dgvAuditCheck.ReadOnly = True
        Me.dgvAuditCheck.RowHeadersVisible = False
        Me.dgvAuditCheck.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvAuditCheck.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAuditCheck.Size = New System.Drawing.Size(112, 87)
        Me.dgvAuditCheck.TabIndex = 185
        '
        'AuditCheck
        '
        Me.AuditCheck.DataPropertyName = "AuditCheck"
        Me.AuditCheck.HeaderText = "Audit"
        Me.AuditCheck.Name = "AuditCheck"
        Me.AuditCheck.ReadOnly = True
        Me.AuditCheck.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.AuditCheck.Width = 70
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 40
        '
        'dgvGoneAway
        '
        Me.dgvGoneAway.AllowUserToAddRows = False
        Me.dgvGoneAway.AllowUserToDeleteRows = False
        Me.dgvGoneAway.AllowUserToResizeColumns = False
        Me.dgvGoneAway.AllowUserToResizeRows = False
        Me.dgvGoneAway.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvGoneAway.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.GoneAway, Me.DataGridViewTextBoxColumn9})
        Me.dgvGoneAway.Location = New System.Drawing.Point(738, 7)
        Me.dgvGoneAway.Name = "dgvGoneAway"
        Me.dgvGoneAway.ReadOnly = True
        Me.dgvGoneAway.RowHeadersVisible = False
        Me.dgvGoneAway.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvGoneAway.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvGoneAway.Size = New System.Drawing.Size(112, 65)
        Me.dgvGoneAway.TabIndex = 188
        '
        'GoneAway
        '
        Me.GoneAway.DataPropertyName = "GoneAway"
        Me.GoneAway.HeaderText = "Gone away"
        Me.GoneAway.Name = "GoneAway"
        Me.GoneAway.ReadOnly = True
        Me.GoneAway.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.GoneAway.Width = 70
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 40
        '
        'cmdGoneAwayAll
        '
        Me.cmdGoneAwayAll.Location = New System.Drawing.Point(738, 72)
        Me.cmdGoneAwayAll.Name = "cmdGoneAwayAll"
        Me.cmdGoneAwayAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdGoneAwayAll.TabIndex = 189
        Me.cmdGoneAwayAll.Text = "All"
        Me.cmdGoneAwayAll.UseVisualStyleBackColor = True
        '
        'cmdGoneAwayClear
        '
        Me.cmdGoneAwayClear.Location = New System.Drawing.Point(800, 72)
        Me.cmdGoneAwayClear.Name = "cmdGoneAwayClear"
        Me.cmdGoneAwayClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdGoneAwayClear.TabIndex = 190
        Me.cmdGoneAwayClear.Text = "Clear"
        Me.cmdGoneAwayClear.UseVisualStyleBackColor = True
        '
        'dgvVisitAfterPIF
        '
        Me.dgvVisitAfterPIF.AllowUserToAddRows = False
        Me.dgvVisitAfterPIF.AllowUserToDeleteRows = False
        Me.dgvVisitAfterPIF.AllowUserToResizeColumns = False
        Me.dgvVisitAfterPIF.AllowUserToResizeRows = False
        Me.dgvVisitAfterPIF.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvVisitAfterPIF.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.VisitAfterPIF, Me.DataGridViewTextBoxColumn13})
        Me.dgvVisitAfterPIF.Location = New System.Drawing.Point(856, 247)
        Me.dgvVisitAfterPIF.Name = "dgvVisitAfterPIF"
        Me.dgvVisitAfterPIF.ReadOnly = True
        Me.dgvVisitAfterPIF.RowHeadersVisible = False
        Me.dgvVisitAfterPIF.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvVisitAfterPIF.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVisitAfterPIF.Size = New System.Drawing.Size(112, 65)
        Me.dgvVisitAfterPIF.TabIndex = 191
        '
        'VisitAfterPIF
        '
        Me.VisitAfterPIF.DataPropertyName = "VisitAfterPIF"
        Me.VisitAfterPIF.HeaderText = "V after PIF"
        Me.VisitAfterPIF.Name = "VisitAfterPIF"
        Me.VisitAfterPIF.ReadOnly = True
        Me.VisitAfterPIF.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.VisitAfterPIF.Width = 70
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Width = 40
        '
        'cmdVisitAfterPIFAll
        '
        Me.cmdVisitAfterPIFAll.Location = New System.Drawing.Point(856, 312)
        Me.cmdVisitAfterPIFAll.Name = "cmdVisitAfterPIFAll"
        Me.cmdVisitAfterPIFAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdVisitAfterPIFAll.TabIndex = 192
        Me.cmdVisitAfterPIFAll.Text = "All"
        Me.cmdVisitAfterPIFAll.UseVisualStyleBackColor = True
        '
        'cmdVisitAfterPIFClear
        '
        Me.cmdVisitAfterPIFClear.Location = New System.Drawing.Point(918, 312)
        Me.cmdVisitAfterPIFClear.Name = "cmdVisitAfterPIFClear"
        Me.cmdVisitAfterPIFClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdVisitAfterPIFClear.TabIndex = 193
        Me.cmdVisitAfterPIFClear.Text = "Clear"
        Me.cmdVisitAfterPIFClear.UseVisualStyleBackColor = True
        '
        'cmdAuditCheckByClear
        '
        Me.cmdAuditCheckByClear.Location = New System.Drawing.Point(1060, 312)
        Me.cmdAuditCheckByClear.Name = "cmdAuditCheckByClear"
        Me.cmdAuditCheckByClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdAuditCheckByClear.TabIndex = 195
        Me.cmdAuditCheckByClear.Text = "Clear"
        Me.cmdAuditCheckByClear.UseVisualStyleBackColor = True
        '
        'cmdAuditCheckByAll
        '
        Me.cmdAuditCheckByAll.Location = New System.Drawing.Point(998, 312)
        Me.cmdAuditCheckByAll.Name = "cmdAuditCheckByAll"
        Me.cmdAuditCheckByAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdAuditCheckByAll.TabIndex = 194
        Me.cmdAuditCheckByAll.Text = "All"
        Me.cmdAuditCheckByAll.UseVisualStyleBackColor = True
        '
        'dgvAuditCheckBy
        '
        Me.dgvAuditCheckBy.AllowUserToAddRows = False
        Me.dgvAuditCheckBy.AllowUserToDeleteRows = False
        Me.dgvAuditCheckBy.AllowUserToResizeColumns = False
        Me.dgvAuditCheckBy.AllowUserToResizeRows = False
        Me.dgvAuditCheckBy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAuditCheckBy.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AuditCheckBy, Me.DataGridViewTextBoxColumn12})
        Me.dgvAuditCheckBy.Location = New System.Drawing.Point(974, 7)
        Me.dgvAuditCheckBy.Name = "dgvAuditCheckBy"
        Me.dgvAuditCheckBy.ReadOnly = True
        Me.dgvAuditCheckBy.RowHeadersVisible = False
        Me.dgvAuditCheckBy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAuditCheckBy.Size = New System.Drawing.Size(160, 305)
        Me.dgvAuditCheckBy.TabIndex = 196
        '
        'AuditCheckBy
        '
        Me.AuditCheckBy.DataPropertyName = "AuditCheckBy"
        Me.AuditCheckBy.HeaderText = "Name"
        Me.AuditCheckBy.Name = "AuditCheckBy"
        Me.AuditCheckBy.ReadOnly = True
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 40
        '
        'lblPeriodType
        '
        Me.lblPeriodType.Location = New System.Drawing.Point(760, 346)
        Me.lblPeriodType.Name = "lblPeriodType"
        Me.lblPeriodType.Size = New System.Drawing.Size(70, 19)
        Me.lblPeriodType.TabIndex = 198
        Me.lblPeriodType.Text = "based on:"
        Me.lblPeriodType.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboPeriodType
        '
        Me.cboPeriodType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriodType.FormattingEnabled = True
        Me.cboPeriodType.Location = New System.Drawing.Point(836, 344)
        Me.cboPeriodType.Name = "cboPeriodType"
        Me.cboPeriodType.Size = New System.Drawing.Size(110, 21)
        Me.cboPeriodType.TabIndex = 197
        '
        'dgvNumberOfVisitsPA
        '
        Me.dgvNumberOfVisitsPA.AllowUserToAddRows = False
        Me.dgvNumberOfVisitsPA.AllowUserToDeleteRows = False
        Me.dgvNumberOfVisitsPA.AllowUserToResizeColumns = False
        Me.dgvNumberOfVisitsPA.AllowUserToResizeRows = False
        Me.dgvNumberOfVisitsPA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvNumberOfVisitsPA.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumberOfVisitsPA, Me.DataGridViewTextBoxColumn14})
        Me.dgvNumberOfVisitsPA.Location = New System.Drawing.Point(361, 153)
        Me.dgvNumberOfVisitsPA.Name = "dgvNumberOfVisitsPA"
        Me.dgvNumberOfVisitsPA.ReadOnly = True
        Me.dgvNumberOfVisitsPA.RowHeadersVisible = False
        Me.dgvNumberOfVisitsPA.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvNumberOfVisitsPA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvNumberOfVisitsPA.Size = New System.Drawing.Size(115, 159)
        Me.dgvNumberOfVisitsPA.TabIndex = 199
        '
        'NumberOfVisitsPA
        '
        Me.NumberOfVisitsPA.DataPropertyName = "NumberOfVisitsPA"
        Me.NumberOfVisitsPA.HeaderText = "# Visits"
        Me.NumberOfVisitsPA.Name = "NumberOfVisitsPA"
        Me.NumberOfVisitsPA.ReadOnly = True
        Me.NumberOfVisitsPA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.NumberOfVisitsPA.ToolTipText = "Number of visits - all and out of hours"
        Me.NumberOfVisitsPA.Width = 74
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn14.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Width = 40
        '
        'cmdNumberOfVisitsPAClear
        '
        Me.cmdNumberOfVisitsPAClear.Location = New System.Drawing.Point(424, 312)
        Me.cmdNumberOfVisitsPAClear.Name = "cmdNumberOfVisitsPAClear"
        Me.cmdNumberOfVisitsPAClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdNumberOfVisitsPAClear.TabIndex = 201
        Me.cmdNumberOfVisitsPAClear.Text = "Clear"
        Me.cmdNumberOfVisitsPAClear.UseVisualStyleBackColor = True
        '
        'cmdNumberOfVisitsPAAll
        '
        Me.cmdNumberOfVisitsPAAll.Location = New System.Drawing.Point(362, 312)
        Me.cmdNumberOfVisitsPAAll.Name = "cmdNumberOfVisitsPAAll"
        Me.cmdNumberOfVisitsPAAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdNumberOfVisitsPAAll.TabIndex = 200
        Me.cmdNumberOfVisitsPAAll.Text = "All"
        Me.cmdNumberOfVisitsPAAll.UseVisualStyleBackColor = True
        '
        'cmdClientNameClear
        '
        Me.cmdClientNameClear.Location = New System.Drawing.Point(258, 312)
        Me.cmdClientNameClear.Name = "cmdClientNameClear"
        Me.cmdClientNameClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientNameClear.TabIndex = 204
        Me.cmdClientNameClear.Text = "Clear"
        Me.cmdClientNameClear.UseVisualStyleBackColor = True
        '
        'cmdClientNameAll
        '
        Me.cmdClientNameAll.Location = New System.Drawing.Point(196, 312)
        Me.cmdClientNameAll.Name = "cmdClientNameAll"
        Me.cmdClientNameAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientNameAll.TabIndex = 203
        Me.cmdClientNameAll.Text = "All"
        Me.cmdClientNameAll.UseVisualStyleBackColor = True
        '
        'dgvClientName
        '
        Me.dgvClientName.AllowUserToAddRows = False
        Me.dgvClientName.AllowUserToDeleteRows = False
        Me.dgvClientName.AllowUserToResizeColumns = False
        Me.dgvClientName.AllowUserToResizeRows = False
        Me.dgvClientName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClientName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClientName, Me.DataGridViewTextBoxColumn7})
        Me.dgvClientName.Location = New System.Drawing.Point(172, 7)
        Me.dgvClientName.Name = "dgvClientName"
        Me.dgvClientName.ReadOnly = True
        Me.dgvClientName.RowHeadersVisible = False
        Me.dgvClientName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClientName.Size = New System.Drawing.Size(160, 305)
        Me.dgvClientName.TabIndex = 202
        '
        'ClientName
        '
        Me.ClientName.DataPropertyName = "ClientName"
        Me.ClientName.HeaderText = "Client"
        Me.ClientName.Name = "ClientName"
        Me.ClientName.ReadOnly = True
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 40
        '
        'frmEAUpdateSummary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1141, 579)
        Me.Controls.Add(Me.cmdClientNameClear)
        Me.Controls.Add(Me.cmdClientNameAll)
        Me.Controls.Add(Me.dgvClientName)
        Me.Controls.Add(Me.dgvNumberOfVisitsPA)
        Me.Controls.Add(Me.cmdNumberOfVisitsPAClear)
        Me.Controls.Add(Me.cmdNumberOfVisitsPAAll)
        Me.Controls.Add(Me.lblPeriodType)
        Me.Controls.Add(Me.cboPeriodType)
        Me.Controls.Add(Me.dgvAuditCheckBy)
        Me.Controls.Add(Me.cmdAuditCheckByClear)
        Me.Controls.Add(Me.cmdAuditCheckByAll)
        Me.Controls.Add(Me.dgvVisitAfterPIF)
        Me.Controls.Add(Me.cmdVisitAfterPIFAll)
        Me.Controls.Add(Me.cmdVisitAfterPIFClear)
        Me.Controls.Add(Me.dgvGoneAway)
        Me.Controls.Add(Me.cmdGoneAwayAll)
        Me.Controls.Add(Me.cmdGoneAwayClear)
        Me.Controls.Add(Me.cmdAuditCheckAll)
        Me.Controls.Add(Me.dgvWorkType)
        Me.Controls.Add(Me.cmdAuditCheckClear)
        Me.Controls.Add(Me.dgvAuditCheck)
        Me.Controls.Add(Me.cmdWorkTypeClear)
        Me.Controls.Add(Me.cmdWorkTypeAll)
        Me.Controls.Add(Me.cboEATeam)
        Me.Controls.Add(Me.cmdEANameClear)
        Me.Controls.Add(Me.cmdEANameAll)
        Me.Controls.Add(Me.dgvVisited)
        Me.Controls.Add(Me.dgvEAName)
        Me.Controls.Add(Me.cboCompany)
        Me.Controls.Add(Me.cmdVisitedClear)
        Me.Controls.Add(Me.cmdVisitedAll)
        Me.Controls.Add(Me.lblSummary)
        Me.Controls.Add(Me.dgvVisitedNotLive)
        Me.Controls.Add(Me.cmdRefreshDB)
        Me.Controls.Add(Me.cmdVisitedNotLiveClear)
        Me.Controls.Add(Me.dgvArrangementMade)
        Me.Controls.Add(Me.pnlAbsPerc)
        Me.Controls.Add(Me.cmdVisitedNotLiveAll)
        Me.Controls.Add(Me.cmdArrangementMadeClear)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdArrangementMadeAll)
        Me.Controls.Add(Me.cmdClearAll)
        Me.Controls.Add(Me.cmdCollectedClear)
        Me.Controls.Add(Me.cmdCollectedAll)
        Me.Controls.Add(Me.dgvCollected)
        Me.Controls.Add(Me.cmdCGAClear)
        Me.Controls.Add(Me.cmdCGAAll)
        Me.Controls.Add(Me.dgvSummary)
        Me.Controls.Add(Me.dgvCGA)
        Me.Controls.Add(Me.dgvNOI)
        Me.Controls.Add(Me.cmdNOIAll)
        Me.Controls.Add(Me.cmdNOIClear)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmEAUpdateSummary"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "EA Update Summary"
        CType(Me.dgvArrangementMade, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvVisitedNotLive, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAbsPerc.ResumeLayout(False)
        Me.pnlAbsPerc.PerformLayout()
        CType(Me.dgvCollected, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCGA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEAName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvNOI, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvVisited, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvWorkType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAuditCheck, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvGoneAway, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvVisitAfterPIF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAuditCheckBy, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvNumberOfVisitsPA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents cmsForm As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmsList As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cmsSummary As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmdArrangementMadeClear As System.Windows.Forms.Button
    Friend WithEvents dgvArrangementMade As System.Windows.Forms.DataGridView
    Friend WithEvents cmdArrangementMadeAll As System.Windows.Forms.Button
    Friend WithEvents RefreshCheckTimer As System.Windows.Forms.Timer
    Friend WithEvents dgvVisitedNotLive As System.Windows.Forms.DataGridView
    Friend WithEvents cmdRefreshDB As System.Windows.Forms.Button
    Friend WithEvents cmdVisitedNotLiveClear As System.Windows.Forms.Button
    Friend WithEvents cmdVisitedNotLiveAll As System.Windows.Forms.Button
    Friend WithEvents CGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdCollectedClear As System.Windows.Forms.Button
    Friend WithEvents cmdCollectedAll As System.Windows.Forms.Button
    Friend WithEvents pnlAbsPerc As System.Windows.Forms.Panel
    Friend WithEvents radPerc As System.Windows.Forms.RadioButton
    Friend WithEvents radAbs As System.Windows.Forms.RadioButton
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents cmdClearAll As System.Windows.Forms.Button
    Friend WithEvents cmdCGAClear As System.Windows.Forms.Button
    Friend WithEvents cmdCGAAll As System.Windows.Forms.Button
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvCollected As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSummary As System.Windows.Forms.DataGridView
    Friend WithEvents dgvCGA As System.Windows.Forms.DataGridView
    Friend WithEvents cboEATeam As System.Windows.Forms.ComboBox
    Friend WithEvents cmdEANameClear As System.Windows.Forms.Button
    Friend WithEvents cmdEANameAll As System.Windows.Forms.Button
    Friend WithEvents dgvEAName As System.Windows.Forms.DataGridView
    Friend WithEvents Collected As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvNOI As System.Windows.Forms.DataGridView
    Friend WithEvents cmdNOIClear As System.Windows.Forms.Button
    Friend WithEvents cmdNOIAll As System.Windows.Forms.Button
    Friend WithEvents VisitedNotLive As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ArrangementMade As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EAName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClientNameTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdVisitedClear As System.Windows.Forms.Button
    Friend WithEvents dgvVisited As System.Windows.Forms.DataGridView
    Friend WithEvents Visited As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdVisitedAll As System.Windows.Forms.Button
    Friend WithEvents dgvWorkType As System.Windows.Forms.DataGridView
    Friend WithEvents WorkType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdWorkTypeClear As System.Windows.Forms.Button
    Friend WithEvents cmdWorkTypeAll As System.Windows.Forms.Button
    Friend WithEvents NOI As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdAuditCheckAll As System.Windows.Forms.Button
    Friend WithEvents cmdAuditCheckClear As System.Windows.Forms.Button
    Friend WithEvents dgvAuditCheck As System.Windows.Forms.DataGridView
    Friend WithEvents dgvGoneAway As System.Windows.Forms.DataGridView
    Friend WithEvents cmdGoneAwayAll As System.Windows.Forms.Button
    Friend WithEvents cmdGoneAwayClear As System.Windows.Forms.Button
    Friend WithEvents dgvVisitAfterPIF As System.Windows.Forms.DataGridView
    Friend WithEvents cmdVisitAfterPIFAll As System.Windows.Forms.Button
    Friend WithEvents cmdVisitAfterPIFClear As System.Windows.Forms.Button
    Friend WithEvents VisitAfterPIF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GoneAway As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdAuditCheckByClear As System.Windows.Forms.Button
    Friend WithEvents cmdAuditCheckByAll As System.Windows.Forms.Button
    Friend WithEvents dgvAuditCheckBy As System.Windows.Forms.DataGridView
    Friend WithEvents AuditCheckBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents lblPeriodType As System.Windows.Forms.Label
    Friend WithEvents cboPeriodType As System.Windows.Forms.ComboBox
    Friend WithEvents dgvNumberOfVisitsPA As System.Windows.Forms.DataGridView
    Friend WithEvents NumberOfVisitsPA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdNumberOfVisitsPAClear As System.Windows.Forms.Button
    Friend WithEvents cmdNumberOfVisitsPAAll As System.Windows.Forms.Button
    Friend WithEvents cmdClientNameClear As System.Windows.Forms.Button
    Friend WithEvents cmdClientNameAll As System.Windows.Forms.Button
    Friend WithEvents dgvClientName As System.Windows.Forms.DataGridView
    Friend WithEvents ClientName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AuditCheck As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

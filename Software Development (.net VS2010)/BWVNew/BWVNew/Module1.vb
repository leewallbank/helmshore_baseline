﻿Imports System.Data.SqlClient
Imports System.Configuration

Module Module1
    Public sqlCon As New Odbc.OdbcConnection()
    Public Sub Connect_sqlDb()
        Dim conn_str As String = ""
        Try
            If Not IsNothing(sqlCon) Then
                'This is only necessary following an exception...
                If sqlCon.State = ConnectionState.Open Then sqlCon.Close()
            End If

            conn_str = "BodyCamera"
            sqlCon.ConnectionString = ConfigurationManager.ConnectionStrings(conn_str).ConnectionString
            sqlCon.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings(conn_str).ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings(conn_str).ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub
End Module

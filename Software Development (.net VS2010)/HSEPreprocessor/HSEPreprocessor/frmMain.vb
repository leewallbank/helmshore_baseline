﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String
    Dim addName(10) As String
    Dim addAddr(10) As String

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim OutputFile As String = ""
            Dim totValue As Decimal = 0
            Dim caseValue As Decimal = 0
            Dim totCases As Integer = 0
            Dim lastClientRef As String = ""
            Dim comments As String = ""
            Dim LODate, offenceDate As Date
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
            infolbl.Text = "Processing file"
            Application.DoEvents()
            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)
            btnProcessFile.Enabled = False
            Dim filecontents(,) As String = InputFromExcel(FileDialog.FileName)
            Dim rowMax As Integer = UBound(filecontents)
            Dim matchFound As Boolean = False
            Dim matchesFound As Integer = 0

            For rowIDX = 0 To rowMax
                Dim clientRef As String = filecontents(rowIDX, 1)
                If clientRef = Nothing Then
                    Continue For
                End If
                If clientRef = "Total" Then
                    Continue For
                End If
                'Ignore if summons no is not numeric
                Dim summonsNo As Integer
                Try
                    summonsNo = filecontents(rowIDX, 2)
                Catch ex As Exception
                    Continue For
                End Try
                If lastClientRef <> "" Then
                    If clientRef <> lastClientRef Then
                        OutputFile &= caseValue & "|" & Format(LODate, "dd-MMM-yy") & "|"
                        If offenceDate = Nothing Then
                            OutputFile &= "||" & comments & vbNewLine
                        Else
                            OutputFile &= Format(offenceDate, "dd-MMM-yy") & "|" & comments & vbNewLine
                        End If
                      
                        comments = ""
                        caseValue = 0
                    Else
                        'add to comments
                        comments &= "Inv No:" & filecontents(rowIDX, 2) & ";"
                        comments &= "LO date:" & filecontents(rowIDX, 11) & ";"
                        Try
                            comments &= "Offence Date:" & filecontents(rowIDX, 13) & ";"
                        Catch ex As Exception

                        End Try

                        caseValue += filecontents(rowIDX, 12)
                        totValue += filecontents(rowIDX, 12)
                        Continue For
                    End If
                End If
                'T102002 switch round balance and off date
                caseValue += filecontents(rowIDX, 12)

                LODate = filecontents(rowIDX, 11)

                Try
                    offenceDate = filecontents(rowIDX, 13)
                Catch ex As Exception
                    offenceDate = Nothing
                End Try
                totCases += 1
                totValue += filecontents(rowIDX, 12)
                For colIDX = 0 To 10
                    Dim txt As String = filecontents(rowIDX, colIDX)
                    If txt = "NULL" Then
                        txt = ""
                    End If
                    OutputFile &= txt & "|"
                Next
                'add comments if any found
                'add to comments
                comments &= "Inv No:" & filecontents(rowIDX, 2) & ";"
                Try
                    If filecontents(rowIDX, 14) <> Nothing Then
                        comments &= "comments:" & filecontents(rowIDX, 14) & ";"
                    End If
                Catch ex As Exception

                End Try
               
                lastClientRef = clientRef
            Next
            'don't forget last one
            OutputFile &= caseValue & "|" & Format(LODate, "dd-MMM-yy") & "|"
            If offenceDate = Nothing Then
                OutputFile &= "||" & comments & vbNewLine
            Else
                OutputFile &= Format(offenceDate, "dd-MMM-yy") & "|" & comments & vbNewLine
            End If

            btnViewInputFile.Enabled = True
            If totCases > 0 Then
                WriteFile(InputFilePath & FileName & "_preprocess.txt", OutputFile)
                btnViewOutputFile.Enabled = True
            End If
            infolbl.Text = "Completed"

            MsgBox("Cases = " & totCases & " Value " & totValue & vbNewLine)
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_preprocess.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_preprocess.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & "_nonWRP_4200.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_nonWRP_4200.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

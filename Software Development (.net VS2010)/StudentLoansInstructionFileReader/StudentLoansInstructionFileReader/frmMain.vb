﻿Imports System.IO
Public Class frmMain
    Private InstructionFile As New DataTable
    Private InstructionFileDV As DataView


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        With InstructionFile.Columns
            .Add("1st LOAN PACK", Type.GetType("System.String"))
            .Add("PAYINBOOK REFERENCE", Type.GetType("System.String"))
            .Add("CUSTOMER FULL NAME", Type.GetType("System.String"))
            .Add("DOB", Type.GetType("System.String"))
            .Add("CUSTOMER GENDER", Type.GetType("System.String"))
            .Add("SUBBUILDING NAME", Type.GetType("System.String"))
            .Add("BUILDING NAME", Type.GetType("System.String"))
            .Add("BUILDING NUMBER", Type.GetType("System.String"))
            .Add("STREET", Type.GetType("System.String"))
            .Add("LOCALITY", Type.GetType("System.String"))
            .Add("TOWN", Type.GetType("System.String"))
            .Add("COUNTY", Type.GetType("System.String"))
            .Add("POSTCODE", Type.GetType("System.String"))
            .Add("CUSTOMER HOME PHONE", Type.GetType("System.String"))
            .Add("CUSTOMER MOBILE PHONE", Type.GetType("System.String"))
            .Add("CUSTOMER WORK PHONE", Type.GetType("System.String"))
            .Add("CUSTOMER EMAIL", Type.GetType("System.String"))
            .Add("TOTAL ARREARS", Type.GetType("System.String"))
            .Add("TOTAL CHARGES", Type.GetType("System.String"))
            .Add("TOTAL OVERDUE", Type.GetType("System.String"))
            .Add("TOTAL BALANCE", Type.GetType("System.String"))
            .Add("MONTHS DOWN", Type.GetType("System.String"))
            .Add("INTREST RATE", Type.GetType("System.String"))
            .Add("MONTHLY PYMNT DUE AMT", Type.GetType("System.String"))
            .Add("REPAYMENT DUE DAY", Type.GetType("System.String"))
            .Add("LAST PAID DATE", Type.GetType("System.String"))
            .Add("MATURITY DATE", Type.GetType("System.String"))
            .Add("DEFERMENT START DATE", Type.GetType("System.String"))
            .Add("DEFERMENT END DATE", Type.GetType("System.String"))
            .Add("CONTACT1 NAME", Type.GetType("System.String"))
            .Add("CONTACT1 RELATIONSHIP", Type.GetType("System.String"))
            .Add("CONTACT1 SUBBUILDING NAME", Type.GetType("System.String"))
            .Add("CONTACT1 BUILDING NAME", Type.GetType("System.String"))
            .Add("CONTACT1 BUILDING NUMBER", Type.GetType("System.String"))
            .Add("CONTACT1 STREET", Type.GetType("System.String"))
            .Add("CONTACT1 LOCALITY", Type.GetType("System.String"))
            .Add("CONTACT1 TOWN", Type.GetType("System.String"))
            .Add("CONTACT1 COUNTY", Type.GetType("System.String"))
            .Add("CONTACT1 POSTCODE", Type.GetType("System.String"))
            .Add("CONTACT1 HOME", Type.GetType("System.String"))
            .Add("CONTACT1 MOBILE", Type.GetType("System.String"))
            .Add("CONTACT1 WORK", Type.GetType("System.String"))
            .Add("CONTACT1 EMAIL", Type.GetType("System.String"))
            .Add("CONTACT2 NAME", Type.GetType("System.String"))
            .Add("CONTACT2 RELATIONSHIP", Type.GetType("System.String"))
            .Add("CONTACT2 SUBBUILDING NAME", Type.GetType("System.String"))
            .Add("CONTACT2 BUILDING NAME", Type.GetType("System.String"))
            .Add("CONTACT2 BUILDING NUMBER", Type.GetType("System.String"))
            .Add("CONTACT2 STREET", Type.GetType("System.String"))
            .Add("CONTACT2 LOCALITY", Type.GetType("System.String"))
            .Add("CONTACT2 TOWN", Type.GetType("System.String"))
            .Add("CONTACT2 COUNTY", Type.GetType("System.String"))
            .Add("CONTACT2 POSTCODE", Type.GetType("System.String"))
            .Add("CONTACT2 HOME", Type.GetType("System.String"))
            .Add("CONTACT2 MOBILE", Type.GetType("System.String"))
            .Add("CONTACT2 WORK", Type.GetType("System.String"))
            .Add("CONTACT2 EMAIL", Type.GetType("System.String"))

        End With

        InstructionFileDV = New DataView(InstructionFile)
        InstructionFileDV.AllowEdit = False
        InstructionFileDV.AllowDelete = False
        InstructionFileDV.AllowNew = False

        dgvMain.DataSource = InstructionFileDV

    End Sub

    Private Sub cmdLoadFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLoadFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim Filename As String, Line As String

        Try
            FileDialog.ShowDialog()
            Filename = FileDialog.FileName

            Dim objReader As New System.IO.StreamReader(Filename)

            InstructionFile.Clear()

            Do While objReader.Peek() <> -1
                Line = objReader.ReadLine()
                ' RTrim as the datafiles are right padded
                InstructionFile.Rows.Add({Line.Substring(0, 11) _
                                        , Line.Substring(11, 18) _
                                        , Line.Substring(29, 100) _
                                        , Line.Substring(129, 8) _
                                        , Line.Substring(137, 1) _
                                        , Line.Substring(138, 60) _
                                        , Line.Substring(198, 60) _
                                        , Line.Substring(258, 10) _
                                        , Line.Substring(268, 60) _
                                        , Line.Substring(328, 60) _
                                        , Line.Substring(388, 60) _
                                        , Line.Substring(448, 60) _
                                        , Line.Substring(508, 8) _
                                        , Line.Substring(516, 16) _
                                        , Line.Substring(532, 16) _
                                        , Line.Substring(548, 16) _
                                        , Line.Substring(564, 75) _
                                        , Line.Substring(639, 10) _
                                        , Line.Substring(649, 10) _
                                        , Line.Substring(659, 10) _
                                        , Line.Substring(669, 10) _
                                        , Line.Substring(679, 2) _
                                        , Line.Substring(681, 3) _
                                        , Line.Substring(684, 10) _
                                        , Line.Substring(694, 2) _
                                        , Line.Substring(696, 8) _
                                        , Line.Substring(704, 8) _
                                        , Line.Substring(712, 8) _
                                        , Line.Substring(720, 8) _
                                        , Line.Substring(728, 60) _
                                        , Line.Substring(788, 30) _
                                        , Line.Substring(818, 60) _
                                        , Line.Substring(878, 60) _
                                        , Line.Substring(938, 10) _
                                        , Line.Substring(948, 60) _
                                        , Line.Substring(1008, 60) _
                                        , Line.Substring(1068, 60) _
                                        , Line.Substring(1128, 60) _
                                        , Line.Substring(1188, 8) _
                                        , Line.Substring(1196, 16) _
                                        , Line.Substring(1212, 16) _
                                        , Line.Substring(1228, 16) _
                                        , Line.Substring(1244, 75) _
                                        , Line.Substring(1319, 60) _
                                        , Line.Substring(1379, 30) _
                                        , Line.Substring(1409, 60) _
                                        , Line.Substring(1469, 60) _
                                        , Line.Substring(1529, 10) _
                                        , Line.Substring(1539, 60) _
                                        , Line.Substring(1599, 60) _
                                        , Line.Substring(1659, 60) _
                                        , Line.Substring(1719, 60) _
                                        , Line.Substring(1779, 8) _
                                        , Line.Substring(1787, 16) _
                                        , Line.Substring(1803, 16) _
                                        , Line.Substring(1819, 16) _
                                        , Line.Substring(1835, 75) _
                                        })
            Loop
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

End Class

﻿Imports System.Configuration

Public Class Form1
    Dim upd_txt As String
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ConnectDb2("DebtRecoveryLocal")
        ConnectDb2Fees("FeesSQL")
        'First delete from table
        upd_txt = "delete from fees_on_cases_levied where lev_debtorID > 0"
        update_sql(upd_txt)

        Dim debt_dt As New DataTable
        Dim fee_dt As New DataTable
        Dim clientarray, levyarray As Object
        'get all cases with levies
        LoadDataTable2("DebtRecoveryLocal", "SELECT _rowid, clientSchemeID " & _
                                                "FROM debtor " & _
                                                "WHERE status_open_closed = 'O' " & _
                                                " AND _rowid >99 order by _rowid", debt_dt, False)

        For Each row In debt_dt.Rows
            Dim debtorID As Integer = row.Item(0)
            Dim CSID As Integer = row.item(1)
            clientarray = GetSQLResultsArray2("DebtRecoveryLocal", "SELECT clientID, branchID " & _
                                                    "FROM clientScheme " & _
                                                    "WHERE _rowid= " & CSID)
            Dim clientID As Integer = clientArray(0)
            If clientID = 1 Or clientID = 2 Or clientID = 24 Then
                Continue For
            End If
            Dim branchID As Integer = clientarray(1)
            If branchID <> 1 And branchID <> 9 And branchID <> 10 Then
                Continue For
            End If
            'see if case has a levy
            levyarray = GetSQLResultsArray2("DebtRecoveryLocal", "SELECT count(*) from Fee " & _
                                                    "WHERE debtorID = " & debtorID & _
                                                    " AND fee_amount > 0 and type like '%evy%'")

            If levyarray(0) = 0 Then
                Continue For
            End If
            'get and store all fees
            LoadDataTable2("DebtRecoveryLocal", "SELECT _rowid, fee_amount, remited_fee from Fee " & _
                                                   "  WHERE debtorID = " & debtorID & _
                                                    " AND fee_remit_col > 2 ", fee_dt, False)
            For Each debtrow In fee_dt.Rows
                Dim feeID As Integer = debtrow(0)
                Dim feeAmt As Decimal = 0
                feeAmt = debtrow(1)
                Dim remittedAmt As Decimal = debtrow(2)
                upd_txt = "insert into fees_on_cases_levied (lev_debtorID, " & _
                   "lev_fee_no, lev_fee_amount, lev_remitted_fee)" & _
                   "values (" & debtorID & "," & feeID & "," & feeAmt & "," & remittedAmt & ")"
                update_sql(upd_txt)
            Next
        Next

        'MsgBox("Completed")
        Me.Close()
    End Sub






    Public Sub ConnectDb2(ByVal ConnStr As String)
        Try
            If Not IsNothing(DBCon) Then
                'This is only necessary following an exception...
                If DBCon.State = ConnectionState.Open Then DBCon.Close()
            End If

            DBCon.ConnectionString = ConfigurationManager.ConnectionStrings(ConnStr).ConnectionString
            DBCon.Open()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub ConnectDb2Fees(ByVal ConnStr As String)
        Try
            If Not IsNothing(DBConFees) Then
                'This is only necessary following an exception...
                If DBConFees.State = ConnectionState.Open Then DBConFees.Close()
            End If

            DBConFees.ConnectionString = ConfigurationManager.ConnectionStrings(ConnStr).ConnectionString
            DBConFees.Open()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class

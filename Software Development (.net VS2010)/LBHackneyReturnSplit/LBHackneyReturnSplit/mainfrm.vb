Imports System.IO

Public Class mainfrm
    Dim first_letter As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim days As Integer = Weekday(Now)
        date_picker.Value = DateAdd(DateInterval.Day, -days, Now)
        selected_cl_name = ""
    End Sub

    Private Sub retnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles retnbtn.Click
        disable_buttons()
        ProgressBar1.Value = 5
        retn_month = Format(date_picker.Value, "MM_MMM")
        retn_day = Format(date_picker.Value, "dd_ddd")
        Dim files_found As Boolean = False
        Dim returnCases1 As String = ""
        Dim returncases2 As String = ""
        Dim returncases3 As String = ""
        'get all csids for client 1779
        param2 = "select _rowid, schemeID from clientScheme where clientID = 1779"
        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        Dim cs_rows As Integer = no_of_rows - 1
        Dim idx As Integer
        files_found = False

        For idx = 0 To cs_rows
            selected_csid = cs_ds.Tables(0).Rows(idx).Item(0)
            'T136431 see if CTAX or NNDR
            Dim schemeID As Integer = cs_ds.Tables(0).Rows(idx).Item(1)
            param2 = "select work_type from scheme where _rowID = " & schemeID
            Dim sc_ds As DataSet = get_dataset("onestep", param2)
            Dim NNDR As Boolean = True
            If sc_ds.Tables(0).Rows(0).Item(0) = 2 Then
                NNDR = False
            End If

            'get remittance number
            param2 = "select _rowid from Remit where clientschemeID = " & selected_csid & _
            " and date = '" & Format(date_picker.Value, "yyyy-MM-dd") & "'"
            Dim remit_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim remit_no As Integer = remit_dataset.Tables(0).Rows(0).Item(0)
            file_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                                       retn_day & "\Remittances\" & Format(remit_no, "#") & "\"
            Dim file_name, client_ref As String
            'create directories for returns
            Dim dir_name As String = file_path & "Returns Action"
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
            dir_name = file_path & "Returns No Action"
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
            Dim debtor, retn_codeid As Integer

            Dim indexFile As String = ""

            Dim FileContents() As String = Nothing

            Dim count1 As Integer = 0
            Dim count2 As Integer = 0
            Dim value1 As Decimal = 0
            Dim value2 As Decimal = 0


            file_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                                       retn_day & "\Remittances\" & Format(remit_no, "#") & "\Returns\"
            Try
                For Each foundFile As String In My.Computer.FileSystem.GetFiles _
           (file_path, FileIO.SearchOption.SearchTopLevelOnly, "*.pdf")
                    'get debtor number
                    files_found = True
                    Try
                        ProgressBar1.Value += 5
                    Catch ex As Exception
                        ProgressBar1.Value = 5
                    End Try

                    Try
                        debtor = Mid(foundFile, foundFile.Length - 11, 8)
                    Catch ex As Exception
                        Continue For
                    End Try
                    If debtor < 0 Then
                        debtor = debtor * -1
                    End If
                    'get return code for this debtor
                    param2 = "select return_codeID, client_ref, status_open_closed, status from Debtor " & _
                    " where _rowid = " & debtor
                    Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to find case number" & debtor)
                        Exit Sub
                    End If
                    Dim status_open_closed As String = debtor_dataset.Tables(0).Rows(0).Item(2)
                    If status_open_closed = "O" Then
                        Continue For
                    End If
                    Dim clientRef As String = debtor_dataset.Tables(0).Rows(0).Item(1)
                    Dim status As String = debtor_dataset.Tables(0).Rows(0).Item(3)
                    Dim retn_group As String = ""
                    If status = "S" Then
                        retn_group = "Returns No Action"
                    Else
                        Try
                            retn_codeid = debtor_dataset.Tables(0).Rows(0).Item(0)
                        Catch ex As Exception
                            retn_codeid = 0
                        End Try

                        'get override return code

                        If retn_codeid > 0 Then
                            param2 = "select clientreturnnumber from clientSchemeReturn" & _
                            " where returnID = " & retn_codeid & _
                            " and clientSchemeID = " & selected_csid
                            Dim cr_dataset As DataSet = get_dataset("onestep", param2)
                            If no_of_rows = 1 Then
                                If cr_dataset.Tables(0).Rows(0).Item(0) = "05" Or
                                     cr_dataset.Tables(0).Rows(0).Item(0) = "06" Or cr_dataset.Tables(0).Rows(0).Item(0) = "19" Then
                                    retn_group = "Returns Action"
                                Else
                                    retn_group = "Returns No Action"
                                End If
                            End If
                        End If
                    End If
                    If retn_group <> "" Then
                        client_ref = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
                        file_name = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                                          retn_day & "\Remittances\" & Format(remit_no, "#") & "\" & retn_group & "\" & client_ref & "-" & debtor & ".pdf"
                        Try
                            My.Computer.FileSystem.CopyFile(foundFile, file_name)
                        Catch ex As Exception

                        End Try
                        If retn_group = "Returns Action" Then
                            If NNDR Then
                                indexFile &= Path.GetFileName(foundFile) & "," & "NRBAACMA" & "," & clientRef & vbNewLine
                            Else
                                indexFile &= Path.GetFileName(foundFile) & "," & "CTBAACMA" & "," & clientRef & vbNewLine
                            End If

                        Else
                            If NNDR Then
                                indexFile &= Path.GetFileName(foundFile) & "," & "NRBANAMA" & "," & clientRef & vbNewLine
                            Else
                                indexFile &= Path.GetFileName(foundFile) & "," & "CTBANAMA" & "," & clientRef & vbNewLine
                            End If

                        End If

                    End If
                Next
            Catch ex As Exception

            End Try
            file_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                                      retn_day & "\Remittances\" & Format(remit_no, "#") & "\"
            Dim indexFileName As String = file_path & "Returned_Index.csv"
            My.Computer.FileSystem.WriteAllText(indexFileName, indexFile, False)
           
        Next

        If files_found Then
            MsgBox("All reports copied to directories")
        Else
            MsgBox("No reports were found")
        End If
        Me.Close()
    End Sub


    Private Sub disable_buttons()
        exitbtn.Enabled = False
        retnbtn.Enabled = False
    End Sub
    Private Sub enable_buttons()
        exitbtn.Enabled = True
        retnbtn.Enabled = True
    End Sub
End Class

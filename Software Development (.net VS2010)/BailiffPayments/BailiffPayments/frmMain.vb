﻿Imports CommonLibrary

Public Class frmMain
    Private Payments As New clsPaymentsData

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        Try

            dtpDateFrom.Value = dtpDateFrom.Value.AddMonths(-1)
            dtpDateTo.Value = Now

            Payments.GetBailiffs()

            cboBailiff.ValueMember = "BailiffID"
            cboBailiff.DisplayMember = "BailiffName"
            cboBailiff.DataSource = Payments.Bailiffs

            lblSummary.Text = ""

            cmsMain.Items.Add("Copy all case IDs")
            cmsMain.Items.Add("Copy selected cases' IDs")
            cmsMain.Items.Add("Copy")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Try

            Me.Cursor = Cursors.WaitCursor

            ' Validate parameters
            If IsNothing(cboBailiff.SelectedValue) Then
                Me.Cursor = Cursors.Default
                MsgBox("Please select a bailiff.", vbCritical + vbOKOnly, Me.Text)
                Return
            End If

            If dtpDateFrom.Value > dtpDateTo.Value Then
                Me.Cursor = Cursors.Default
                MsgBox("To date must be after From date.", vbCritical + vbOKOnly, Me.Text)
                Return
            End If

            ' Retrieve data
            Payments.GetPayments(dtpDateFrom.Value, dtpDateTo.Value, cboBailiff.SelectedValue.ToString)

            dgvMain.DataSource = Payments.Payments
            lblSummary.Text = Payments.Payments.Count & " payment" & If(Payments.Payments.Count <> 1, "s", "")

        Catch ex As Exception
            HandleException(ex)

        Finally
            Me.Cursor = Cursors.Default
        End Try
    End Sub

    Private Sub CopyAllDebtorIDs()
        Try

            If IsNothing(Payments.Payments) Then Return

            ' Store the current selected cells
            Dim SelectedCells As DataGridViewSelectedCellCollection = dgvMain.SelectedCells
            dgvMain.ClearSelection()

            'Change sort mode
            For Each Column As DataGridViewColumn In dgvMain.Columns
                Column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next Column

            dgvMain.SelectionMode = DataGridViewSelectionMode.FullColumnSelect
            dgvMain.Columns("DebtorID").Selected = True

            dgvMain.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText ' OneStep will not accept column headers
            Clipboard.SetDataObject(dgvMain.GetClipboardContent())

            dgvMain.SelectionMode = DataGridViewSelectionMode.CellSelect

            ' Change sortmode back
            For Each Column As DataGridViewColumn In dgvMain.Columns
                Column.SortMode = DataGridViewColumnSortMode.Automatic
            Next Column

            ' Reapply selected cells
            dgvMain.ClearSelection()
            For Each Cell As DataGridViewCell In SelectedCells
                dgvMain.Rows(Cell.RowIndex).Cells(Cell.ColumnIndex).Selected = True
            Next Cell

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub CopySelectedCasesDebtorIDs()
        Try

            If IsNothing(Payments.Payments) Then Return

            ' Store the current selected cells
            Dim SelectedCells As DataGridViewSelectedCellCollection = dgvMain.SelectedCells
            dgvMain.ClearSelection()

            'Change sort mode
            For Each Column As DataGridViewColumn In dgvMain.Columns
                Column.SortMode = DataGridViewColumnSortMode.NotSortable
            Next Column

            For Each Cell As DataGridViewCell In SelectedCells
                dgvMain.Rows(Cell.RowIndex).Cells("DebtorID").Selected = True
            Next (Cell)

            dgvMain.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText ' OneStep will not accept column headers
            Clipboard.SetDataObject(dgvMain.GetClipboardContent())

            dgvMain.SelectionMode = DataGridViewSelectionMode.CellSelect

            ' Change sortmode back
            For Each Column As DataGridViewColumn In dgvMain.Columns
                Column.SortMode = DataGridViewColumnSortMode.Automatic
            Next Column

            ' Reapply selected cells
            dgvMain.ClearSelection()
            For Each Cell As DataGridViewCell In SelectedCells
                dgvMain.Rows(Cell.RowIndex).Cells(Cell.ColumnIndex).Selected = True
            Next Cell

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMain_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvMain.MouseUp
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If e.Button = MouseButtons.Right Then cmsMain.Show(dgvMain, e.Location)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsMain_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsMain.ItemClicked
        Try

            Select Case e.ClickedItem.Text
                Case "Copy all case IDs"
                    CopyAllDebtorIDs()
                Case "Copy selected cases' IDs"
                    CopySelectedCasesDebtorIDs()
                Case "Copy"
                    dgvMain.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    Clipboard.SetDataObject(dgvMain.GetClipboardContent())
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class

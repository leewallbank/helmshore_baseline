﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.dgvMain = New System.Windows.Forms.DataGridView()
        Me.lblDateTo = New System.Windows.Forms.Label()
        Me.dtpDateTo = New System.Windows.Forms.DateTimePicker()
        Me.lblDateFrom = New System.Windows.Forms.Label()
        Me.dtpDateFrom = New System.Windows.Forms.DateTimePicker()
        Me.cboBailiff = New System.Windows.Forms.ComboBox()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.lblSummary = New System.Windows.Forms.Label()
        Me.lblBailiff = New System.Windows.Forms.Label()
        Me.cmsMain = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.PaymentID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DebtorID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BailiffName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaymentReceivedDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Amount = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PaymentType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvMain
        '
        Me.dgvMain.AllowUserToAddRows = False
        Me.dgvMain.AllowUserToDeleteRows = False
        Me.dgvMain.AllowUserToResizeRows = False
        Me.dgvMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMain.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PaymentID, Me.DebtorID, Me.BailiffName, Me.PaymentReceivedDate, Me.Amount, Me.PaymentType})
        Me.dgvMain.Location = New System.Drawing.Point(15, 67)
        Me.dgvMain.Name = "dgvMain"
        Me.dgvMain.ReadOnly = True
        Me.dgvMain.RowHeadersVisible = False
        Me.dgvMain.Size = New System.Drawing.Size(550, 250)
        Me.dgvMain.TabIndex = 2
        '
        'lblDateTo
        '
        Me.lblDateTo.AutoSize = True
        Me.lblDateTo.Location = New System.Drawing.Point(23, 42)
        Me.lblDateTo.Name = "lblDateTo"
        Me.lblDateTo.Size = New System.Drawing.Size(23, 13)
        Me.lblDateTo.TabIndex = 21
        Me.lblDateTo.Text = "To:"
        '
        'dtpDateTo
        '
        Me.dtpDateTo.Location = New System.Drawing.Point(51, 38)
        Me.dtpDateTo.Name = "dtpDateTo"
        Me.dtpDateTo.Size = New System.Drawing.Size(126, 20)
        Me.dtpDateTo.TabIndex = 20
        '
        'lblDateFrom
        '
        Me.lblDateFrom.AutoSize = True
        Me.lblDateFrom.Location = New System.Drawing.Point(12, 16)
        Me.lblDateFrom.Name = "lblDateFrom"
        Me.lblDateFrom.Size = New System.Drawing.Size(33, 13)
        Me.lblDateFrom.TabIndex = 19
        Me.lblDateFrom.Text = "From:"
        '
        'dtpDateFrom
        '
        Me.dtpDateFrom.Location = New System.Drawing.Point(51, 12)
        Me.dtpDateFrom.Name = "dtpDateFrom"
        Me.dtpDateFrom.Size = New System.Drawing.Size(126, 20)
        Me.dtpDateFrom.TabIndex = 18
        '
        'cboBailiff
        '
        Me.cboBailiff.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest
        Me.cboBailiff.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems
        Me.cboBailiff.DropDownHeight = 200
        Me.cboBailiff.FormattingEnabled = True
        Me.cboBailiff.IntegralHeight = False
        Me.cboBailiff.Location = New System.Drawing.Point(248, 39)
        Me.cboBailiff.Name = "cboBailiff"
        Me.cboBailiff.Size = New System.Drawing.Size(161, 21)
        Me.cboBailiff.TabIndex = 22
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(472, 35)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(94, 26)
        Me.btnSearch.TabIndex = 23
        Me.btnSearch.Text = "Search"
        Me.btnSearch.UseVisualStyleBackColor = True
        '
        'lblSummary
        '
        Me.lblSummary.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblSummary.Location = New System.Drawing.Point(23, 326)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(407, 14)
        Me.lblSummary.TabIndex = 24
        Me.lblSummary.Text = "lblSummary"
        '
        'lblBailiff
        '
        Me.lblBailiff.AutoSize = True
        Me.lblBailiff.Location = New System.Drawing.Point(206, 42)
        Me.lblBailiff.Name = "lblBailiff"
        Me.lblBailiff.Size = New System.Drawing.Size(35, 13)
        Me.lblBailiff.TabIndex = 25
        Me.lblBailiff.Text = "Bailiff:"
        '
        'cmsMain
        '
        Me.cmsMain.Name = "ContextMenuStrip1"
        Me.cmsMain.Size = New System.Drawing.Size(61, 4)
        '
        'PaymentID
        '
        Me.PaymentID.DataPropertyName = "PaymentID"
        Me.PaymentID.HeaderText = "PaymentID"
        Me.PaymentID.Name = "PaymentID"
        Me.PaymentID.ReadOnly = True
        Me.PaymentID.Width = 80
        '
        'DebtorID
        '
        Me.DebtorID.DataPropertyName = "DebtorID"
        Me.DebtorID.HeaderText = "DebtorID"
        Me.DebtorID.Name = "DebtorID"
        Me.DebtorID.ReadOnly = True
        Me.DebtorID.Width = 60
        '
        'BailiffName
        '
        Me.BailiffName.DataPropertyName = "BailiffName"
        Me.BailiffName.HeaderText = "Bailiff"
        Me.BailiffName.Name = "BailiffName"
        Me.BailiffName.ReadOnly = True
        Me.BailiffName.Width = 140
        '
        'PaymentReceivedDate
        '
        Me.PaymentReceivedDate.DataPropertyName = "PaymentReceivedDate"
        Me.PaymentReceivedDate.HeaderText = "Rec'd Date"
        Me.PaymentReceivedDate.Name = "PaymentReceivedDate"
        Me.PaymentReceivedDate.ReadOnly = True
        Me.PaymentReceivedDate.Width = 90
        '
        'Amount
        '
        Me.Amount.DataPropertyName = "Amount"
        Me.Amount.HeaderText = "Amount"
        Me.Amount.Name = "Amount"
        Me.Amount.ReadOnly = True
        Me.Amount.Width = 60
        '
        'PaymentType
        '
        Me.PaymentType.DataPropertyName = "Type"
        Me.PaymentType.HeaderText = "Type"
        Me.PaymentType.Name = "PaymentType"
        Me.PaymentType.ReadOnly = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(579, 349)
        Me.Controls.Add(Me.lblBailiff)
        Me.Controls.Add(Me.lblSummary)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.cboBailiff)
        Me.Controls.Add(Me.lblDateTo)
        Me.Controls.Add(Me.dtpDateTo)
        Me.Controls.Add(Me.lblDateFrom)
        Me.Controls.Add(Me.dtpDateFrom)
        Me.Controls.Add(Me.dgvMain)
        Me.Name = "frmMain"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.Text = "Bailiff Payments"
        CType(Me.dgvMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvMain As System.Windows.Forms.DataGridView
    Friend WithEvents lblDateTo As System.Windows.Forms.Label
    Friend WithEvents dtpDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboBailiff As System.Windows.Forms.ComboBox
    Friend WithEvents btnSearch As System.Windows.Forms.Button
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents lblBailiff As System.Windows.Forms.Label
    Friend WithEvents cmsMain As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents PaymentID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DebtorID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BailiffName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaymentReceivedDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Amount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaymentType As System.Windows.Forms.DataGridViewTextBoxColumn

End Class

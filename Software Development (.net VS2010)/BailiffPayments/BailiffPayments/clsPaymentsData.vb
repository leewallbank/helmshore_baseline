﻿Imports CommonLibrary

Public Class clsPaymentsData
    Private Bailiff As New DataTable, Payment As New DataTable
    Private BailiffDV As DataView, PaymentDV As DataView

    Public ReadOnly Property Bailiffs() As DataView
        Get
            Bailiffs = BailiffDV
        End Get
    End Property

    Public ReadOnly Property Payments() As DataView
        Get
            Payments = PaymentDV
        End Get
    End Property

    Public Sub GetBailiffs()
        Dim Sql As String

        Try

            Sql = "SELECT b._rowID AS BailiffID " & _
                  "     , CONCAT(b.name_fore, ' ', b.name_sur) AS BailiffName " & _
                  "FROM bailiff AS b " & _
                  "WHERE b.hasPen = 'Y'" & _
                  "ORDER BY CONCAT(b.name_fore, ' ', b.name_sur)"

            LoadDataTable("DebtRecovery", Sql, Bailiff, False)

            BailiffDV = New DataView(Bailiff)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetPayments(ByVal DateFrom As Date, ByVal DateTo As Date, ByVal BailiffID As String)
        Dim Sql As String

        Try

            Sql = "SELECT p._rowID AS PaymentID " & _
                  "     , CONCAT(b.name_fore, ' ', b.name_sur) AS BailiffName " & _
                  "     , DATE(p._createdDate) AS PaymentReceivedDate " & _
                  "     , p.DebtorID " & _
                  "     , p.Amount " & _
                  "     , pt.desc_long AS Type " & _
                  "FROM payment AS p " & _
                  "INNER JOIN PayType AS pt ON p.amount_typeID = pt._rowid " & _
                  "INNER JOIN visit AS v ON p.debtorID = v.debtorID " & _
                  "INNER JOIN bailiff AS b ON v.bailiffID = b._rowID " & _
                  "WHERE DATE(p._createddate) BETWEEN '" & DateFrom.ToString("yyyy-MM-dd") & "' AND '" & DateTo.ToString("yyyy-MM-dd") & "' " & _
                  "  AND v.bailiffID = " & BailiffID & " " & _
                  "  AND p.amount_sourceID IN (3, 72) " & _
                  "  AND v._rowID =  ( SELECT MAX(v_s._rowid) AS LastVisitID " & _
                  "                     FROM visit AS v_s " & _
                  "                     WHERE v_s.bailiffID = " & BailiffID & " " & _
                  "                       AND v_s.debtorID = p.debtorID " & _
                  "                       AND v_s.date_allocated <= p._createdDate " & _
                  "                  ) " & _
                  "ORDER BY p._createdDate"

            LoadDataTable("DebtRecovery", Sql, Payment, False)

            PaymentDV = New DataView(Payment)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class

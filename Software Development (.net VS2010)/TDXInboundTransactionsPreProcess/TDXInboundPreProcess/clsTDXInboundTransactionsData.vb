﻿Imports CommonLibrary

Public Class clsTDXInboundTransactionData
    Public Function GetCaseID(ByVal ClientRef As String) As String
        Dim Sql As String
        Dim CaseID As New DataTable
        GetCaseID = ""

        Try

            Sql = "SELECT d._rowID, d.ClientSchemeID FROM debtor AS d INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID WHERE d.client_ref = '" & ClientRef & "' AND cs.clientID IN (1523, 1736, 1766, 1815, 1839)"

            LoadDataTable("DebtRecovery", Sql, CaseID, False)

            If CaseID.Rows.Count = 1 And CaseID.Rows(0).Item(1).ToString <> "4667" Then GetCaseID = CaseID.Rows(0).Item(0).ToString ' check row count to catch duplicates or no match. V1.1 Check for HHBD

            If CaseID.Rows(0).Item(1).ToString = "4667" Then GetCaseID = "HHBD" ' Added v1.1 30/Dec/2015. Request ref 67694

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Function

End Class

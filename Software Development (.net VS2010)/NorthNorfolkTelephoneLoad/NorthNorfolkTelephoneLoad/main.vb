Imports commonlibrary
Public Class mainform



    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim cases As Integer
        Dim clientRef, LastClientRef As String
        Dim file, outline As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim idx, StartIdx, startTel, startMob As Integer
        Dim lines As Integer = 0
        Dim telphone1 As String = ""
        Dim mobphone1 As String = ""
        Dim telphone2 As String = ""
        Dim mobphone2 As String = ""
        Dim phone As String = ""
        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "DebtorID,Phone1,Phone2" & vbNewLine
        notefile = "DebtorID,Notes" & vbNewLine
        outfile = outline
        LastClientRef = ""
        'start looking for nine digit client ref
        'however just check 1st 7 for numeric as can have X suffix
        For idx = 0 To lines - 1
            clientRef = Trim(Mid(line(idx), 2, 7))
            If Not (IsNumeric(clientRef)) Then
                Continue For
            End If
            clientRef = Trim(Mid(line(idx), 2, 10))
            If clientRef = LastClientRef Then
                Continue For
            End If
            LastClientRef = clientRef
            'now look  for TEL or MBL
            'stop when you get to Court:
            StartIdx = 0
            While StartIdx = 0
                idx += 1
                startTel = InStr(line(idx), "TEL:")
                If startTel > 0 Then
                    'see if mobile number also on this line
                    startMob = InStr(line(idx), "MBL:")
                    If startMob > 0 Then
                        phone = Microsoft.VisualBasic.Right(line(idx), line(idx).Length - startMob - 4)
                        If mobphone1 = "" Then
                            mobphone1 = phone
                        ElseIf mobphone1 <> phone Then
                            mobphone2 = phone
                        End If
                        phone = Mid(line(idx), startTel + 4, startMob - startTel - 4)
                        If telphone1 = "" Then
                            telphone1 = phone
                        ElseIf telphone1 <> phone Then
                            telphone2 = phone
                        End If
                    Else
                        phone = Microsoft.VisualBasic.Right(line(idx), line(idx).Length - startTel - 4)
                        If telphone1 = "" Then
                            telphone1 = phone
                        ElseIf telphone1 <> phone Then
                            telphone2 = phone
                        End If
                    End If
                Else
                    'see if a mobile number
                    startMob = InStr(line(idx), "MBL:")
                    If startMob > 0 Then
                        phone = Microsoft.VisualBasic.Right(line(idx), line(idx).Length - startMob - 4)
                        If mobphone1 = "" Then
                            mobphone1 = phone
                        ElseIf mobphone1 <> phone Then
                            mobphone2 = phone
                        End If
                    End If
                    StartIdx = InStr(line(idx), "Court:")
                End If
            End While
            
            If telphone1 = "" And mobphone1 = "" Then
                Continue For
            End If
            'get open debtorIDs for North Norfolk
            Dim dt As New DataTable
            Dim tempNoteFile As String = ""
            LoadDataTable("DebtRecovery", "SELECT _rowID, clientSchemeID " & _
                                                    "FROM Debtor " & _
                                                    "WHERE client_ref = '" & clientRef & "'" & _
                                                    " and status_open_closed = 'O'", dt, False)
            For Each DebtRow In dt.Rows
                Dim csID As Integer = DebtRow(1)
                Dim clientID As Integer = GetSQLResults("DebtRecovery", "SELECT clientID " & _
                                                   " FROM clientScheme  " & _
                                                   " WHERE _rowid = " & csID)
                If clientID = 46 Then
                    Dim phoneCount As Integer = 0
                    Dim debtorID As Integer = DebtRow(0)
                    outfile &= debtorID
                    tempNoteFile = ""
                    If telphone1 <> "" Then
                        phoneCount += 1
                        outfile &= "," & telphone1
                    End If
                    If telphone2 <> "" Then
                        phoneCount += 1
                        outfile &= "," & telphone2
                    End If
                    If mobphone1 <> "" Then
                        If phoneCount >= 2 Then
                            phoneCount += 1
                            tempNoteFile &= debtorID & ",Tel No:" & mobphone1 & vbNewLine
                        Else
                            phoneCount += 1
                            outfile &= "," & mobphone1
                        End If
                    End If
                    If mobphone2 <> "" Then
                        If phoneCount >= 2 Then
                            phoneCount += 1
                            tempNoteFile &= debtorID & ",Tel No:" & mobphone2 & vbNewLine
                        Else
                            phoneCount += 1
                            outfile &= "," & mobphone2
                        End If
                    End If
                    cases += 1
                    If phoneCount > 2 Then
                        notefile &= tempNoteFile
                    End If
                    outfile &= vbNewLine
                End If

            Next
            telphone1 = ""
            telphone2 = ""
            mobphone1 = ""
            mobphone2 = ""
            tempNoteFile = ""
        Next

        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_tel_load.txt", outfile, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_note_load.txt", notefile, False)
        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
            MsgBox("Cases = " & cases & vbNewLine)
            Me.Close()
        End If

    End Sub


End Class

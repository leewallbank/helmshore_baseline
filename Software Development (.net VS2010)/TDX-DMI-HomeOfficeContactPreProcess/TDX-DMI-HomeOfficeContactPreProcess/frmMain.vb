﻿Imports CommonLibrary
Imports System.IO
Public Class frmMain
    Private TDXDMIHomeOfficeData As New clsTDXDMIHomeOfficeData
    Private InputFilePath As String, FileName As String, FileExt As String
    Private PersonType As New Dictionary(Of String, String)
    Private IDType As New Dictionary(Of String, String)
    Private PersonAddressType As New Dictionary(Of String, String)

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()
        'T84075 added TRN and FRN
        PersonType.Add("PAH", "Primary Account Holder")
        PersonType.Add("SAH", "Secondary Account Holder")
        PersonType.Add("JAH", "Joint Account Holder")
        PersonType.Add("GUA", "Guarantor")
        PersonType.Add("AUT", "Authorised User")
        PersonType.Add("SME", "Small to Medium Enterprise")
        PersonType.Add("PRN", "Primary Recovery Name")
        PersonType.Add("SRN", "Secondary Recovery Name")
        PersonType.Add("TRN", "Third Recovery Name")
        PersonType.Add("FRN", "Fourth Recovery Name")
        PersonType.Add("ILW", "Illegal Worker")

        IDType.Add("BC", "Birth Certificate")
        IDType.Add("BILL", "Bill (e.g.: utility)")
        IDType.Add("DVLC", "Drivers Licence")
        IDType.Add("NIN", "National Insurance Number")
        IDType.Add("OTH", "Other")
        IDType.Add("PAS", "Passport")
        IDType.Add("STA", "Statement (e.g.: Bank/Credit Card)")
        IDType.Add("UKN", "Unknown")

        PersonAddressType.Add("BAD", "Business Address")
        PersonAddressType.Add("CAD", "Current Address")
        PersonAddressType.Add("EAD", "Employers Address")
        PersonAddressType.Add("INT", "International Address")
        PersonAddressType.Add("PAD", "Previous Address")
        PersonAddressType.Add("PAF", "Post Office Address File formatted address")
        PersonAddressType.Add("PRA", "Primary Recovery Address")
        PersonAddressType.Add("SAD", "Service Address")
        PersonAddressType.Add("UKN", "Unknown")
        PersonAddressType.Add("SRA", "Secondary Recovery Address")
        PersonAddressType.Add("UNC", "Unconfirmed")
        PersonAddressType.Add("COM", "Comaker address")
        PersonAddressType.Add("DCO", "Debt Counselling")

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim LineNumber As Integer
            Dim OutputLine As String
            Dim AuditLog As String, ErrorLog As String = "", InputLineArray() As String, DebtNotes As String, DebtorID As String
            Dim AccountID = New List(Of String) ' added TS 08/Sep/2015. Request ref 57979

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            lblReadingFile.Visible = True
            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents)

            For Each OldFile As String In Directory.GetFiles(InputFilePath, FileName & "_*.txt")
                File.Delete(OldFile)
            Next OldFile

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                If LineNumber = 1 Or LineNumber = UBound(FileContents) + 1 Then Continue For ' skip header and trailer line

                DebtNotes = ""
                OutputLine = ""
                InputLine.Replace("|", "") ' Added TS 03/Feb/2016. Request ref 73061. The output file is pipe separated so remove just in case.
                InputLineArray = InputLine.Split(",")

                If UBound(InputLineArray) <> 27 Then
                    ErrorLog &= "Unexpected line length of " & (UBound(InputLineArray) + 1).ToString & " items found at line number " & LineNumber.ToString & ". Line not loaded." & vbCrLf
                    Continue For
                End If

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                AccountID.Add(InputLineArray(2)) ' added TS 08/Sep/2015. Request ref 57979

                DebtorID = TDXDMIHomeOfficeData.GetDebtorID(InputLineArray(2))

                If DebtorID <> "" Then
                    OutputLine &= DebtorID & "|"

                    'OutputLine &= String.Join("", ToNote(InputLineArray(3), "Client name", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(4), "Person Type Ranking", ";"))

                    If InputLineArray(5) <> "" Then ' added TS 03/Feb/2016. Request ref 73061
                        If PersonType.ContainsKey(InputLineArray(5)) Then
                            OutputLine &= ToNote(PersonType(InputLineArray(5)), "Person", ";")(0)
                        Else
                            OutputLine &= ToNote("??", "Person", ";")(0)
                            ErrorLog &= "Unexpected PersonTypeCode of " & InputLineArray(5) & " found at line number " & LineNumber.ToString & vbCrLf
                        End If
                    End If

                    OutputLine &= String.Join("", ToNote(InputLineArray(6), "Person ID", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(7), "Title", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(8), "Forename", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(9), "Surname", ";"))

                    If IDType.ContainsKey(InputLineArray(10)) Then
                        OutputLine &= ToNote(IDType(InputLineArray(10)), "ID Type", ";")(0)
                    Else
                        OutputLine &= ToNote(InputLineArray(10), "ID Type", ";")(0)
                    End If

                    OutputLine &= String.Join("", ToNote(InputLineArray(11), "Company Registration Number", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(12), "Address Type Ranking", ";"))

                    If PersonAddressType.ContainsKey(InputLineArray(13)) Then
                        OutputLine &= ToNote(PersonAddressType(InputLineArray(13)), "Address Type", ";")(0)
                    Else
                        OutputLine &= ToNote(InputLineArray(13), "Address Type", ";")(0)
                    End If

                    OutputLine &= String.Join("", ToNote(InputLineArray(14), "Address line 1", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(15), "Address line 2", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(16), "Address line 3", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(17), "Address line 4", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(18), "Address line 5", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(19), "Country", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(20), "Postcode", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(21), "Email Type Ranking", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(22), "Email Address Type Code", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(23), "Email Address", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(24), "Phone Type Ranking", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(25), "Phone Type", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(26), "Phone Number", ";"))
                    OutputLine &= String.Join("", ToNote(InputLineArray(27), "Extension", ";"))

                    OutputLine &= vbCrLf

                Else
                    ErrorLog &= "Cannot find open DebtorID for client ref " & InputLineArray(1).Replace("""", "") & " at line number " & LineNumber.ToString & vbCrLf
                End If

                AppendToFile(InputFilePath & FileName & "_PreProcessed.txt", OutputLine)

            Next InputLine

            InputLineArray = FileContents(0).Split(",") ' Check header record case count
            'If LineNumber - 2 <> InputLineArray(3).Replace("""", "") Then ErrorLog &= "Expecting " & InputLineArray(3) & " cases, found " & (LineNumber - 2).ToString ' commented out TS 08/Sep/2015. Request ref 57979
            If AccountID.ToArray.Distinct.ToArray.Count <> InputLineArray(3).Replace("""", "") Then ErrorLog &= "Expecting " & InputLineArray(3) & " cases, found " & (AccountID.ToArray.Distinct.ToArray.Count).ToString ' added TS 08/Sep/2015. Request ref 57979

            AuditLog = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
            AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

            AuditLog &= "Number of new contacts: " & (UBound(FileContents) - 1).ToString & vbCrLf

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
                MessageBox.Show("Errors found.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            MessageBox.Show("Pre-processing complete.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
            If File.Exists(InputFilePath & FileName & "_Balance_discrepancies.xls") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Balance_discrepancies.xls")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class

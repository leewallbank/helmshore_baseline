﻿Imports CommonLibrary
Public Class Form1
    Private StageData As New clsStageData
    Private ClientData As New clsClientData
    Dim OutputFile As String = ""
    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        'get multiple rows - datatable
        Dim DebtorDataTable As New DataTable
        LoadDataTable("DebtRecovery", "SELECT d._rowID, d.linkID, d.last_stageID, d.bailiffID, d.bail_current,d.postcode " & _
                                                    " cs.clientID " &
                                                    "FROM debtor AS d " & _
                                                    "INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID " & _
                                                    "WHERE d.status = 'L'" &
                                                    "  AND d.status_open_closed = 'O'" & _
                                                    "  AND cs.branchID = 1 " & _
                                                    "  AND cs.clientID <> 1" &
                                                    " AND cs.clientID <> 2" &
                                                    " AND cs.clientID <> 24", DebtorDataTable, False)

        For Each row In DebtorDataTable.Rows
            Dim stageID As Integer = row(2)
            If StageData.GetStageName(stageID) <> "(Return)" Then
                Continue For
            End If
            Dim debtorID As Integer = row.Item(0)
            Dim linkID As Integer = 0
            Try
                linkID = row.item(1)
            Catch ex As Exception

            End Try
            Dim ClientID As Integer = row.item(6)
            Dim ClientName As String = ClientData.GetClientName(ClientID)
            OutputFile &= debtorID & "," & linkID & "," & ClientName
        Next row


        Me.Close()
    End Sub
End Class

﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("TDX DMI HMRC Pre Process")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Rossendales")> 
<Assembly: AssemblyProduct("TDX DMI HMRC Pre Process")> 
<Assembly: AssemblyCopyright("Copyright © Rossendales 2015")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("c3f1817b-2a2f-4c3f-bd38-7ab1f4919210")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.4.0.0")> 
<Assembly: AssemblyFileVersion("1.4.0.0")> 

﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization
Imports System.Net.Mail

Public Class frmMain
    Private UnitedUtilitiesData As New clsUnitedUtilitiesData
    Const Separator As String = "|"
    Private ConnCode As New Dictionary(Of String, Integer)
    Private AccountType As New Dictionary(Of String, String)
    Public InfoStatus As New Dictionary(Of String, String)
    Public DWPIndicator As New Dictionary(Of String, String)
    Private ConnID As String() = {"3390", "3391", "3392", "3393", "3394", "3395", "3396", "3397", "3398", "3399", "3400", "4435"}
    Private InputFilePath As String, FileName As String, FileExt As String, ClientSupportFilePath As String
    Private OutputFiles As New ArrayList()
    Private NewCases As New ArrayList()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        ConnCode.Add("EGY1CO", 0)
        ConnCode.Add("EGY2CO", 0)
        ConnCode.Add("EGY3CO", 0)
        ConnCode.Add("EGY1DO", 0)
        ConnCode.Add("EGY2DO", 0)
        ConnCode.Add("EGY3DO", 0)
        ConnCode.Add("AGY1DO", 1)
        ConnCode.Add("AGY2DO", 2)
        ConnCode.Add("LGY1DO", 3)
        ConnCode.Add("LGY2DO", 4)
        ConnCode.Add("CGY1DO", 5)
        ConnCode.Add("CGY2DO", 6)
        ConnCode.Add("AGY1CO", 7)
        ConnCode.Add("AGY2CO", 8)
        ConnCode.Add("LGY1CO", 9)
        ConnCode.Add("LGY2CO", 10)
        ConnCode.Add("NGY1CO", 11) ' added TS 24/Jul/2014. Request ref 26281
        ConnCode.Add("NGY2CO", 11) ' added TS 24/Jul/2014. Request ref 26281

        AccountType.Add("ME", "Measured water services (metered)")
        AccountType.Add("UN", "Unmeasured water services (un metered)")
        AccountType.Add("MI", "Mixed water services (metered and un metered)")
        AccountType.Add("TR", "Trade effluent (Industrial Waste)")
        AccountType.Add("SU", "Sundry (Miscellaneous")

        InfoStatus.Add("A", "Applied DWP / Trust")
        InfoStatus.Add("B", "Account on Support Tariff")
        InfoStatus.Add("C", "Recall at clients request")
        InfoStatus.Add("D", "Failed Support Tariff")
        InfoStatus.Add("E", "invalid response code received")
        InfoStatus.Add("F", "Reinstate placement")
        InfoStatus.Add("G", "Granted DWP / Trust")
        InfoStatus.Add("H", "Account On Hold")
        InfoStatus.Add("I", "Placement period lapsed (Final)")
        InfoStatus.Add("J", "Customer has paid in full")
        InfoStatus.Add("K", "Reinstatement Rejected")
        InfoStatus.Add("L", "Hold released")
        InfoStatus.Add("M", "DRO Applied")
        InfoStatus.Add("N", "DRO rejected")
        InfoStatus.Add("P", "DRO Successful")
        InfoStatus.Add("Q", "No response received within agreed time limit")
        InfoStatus.Add("R", "Refused DWP / Trust/Arrears Allowance")
        InfoStatus.Add("S", "Contract suspended / ended - All accounts to be returned")
        InfoStatus.Add("T", "Placement period lapsed (Interim)")
        InfoStatus.Add("X", "Account on hold pending Arrears Allowance application")
        InfoStatus.Add("Z", "Account accepted on Arrears Allowance Scheme")
        InfoStatus.Add("V", "DWP Accepted") ' Added TS 01/May/2013
        InfoStatus.Add("W", "Request to agency to resend corrected DWP/AA criteria")

        DWPIndicator.Add("", "None")
        DWPIndicator.Add("A", "Applied")
        DWPIndicator.Add("G", "Agreed")
        DWPIndicator.Add("N", "None")
        DWPIndicator.Add("R", "Refused")
        DWPIndicator.Add("T", "Terminated")

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String, InputLineArray() As String, ConnKey As String, MBCNotes As String = ""
        Dim Balance As Decimal, AdjustmentBalance As Decimal, TotalNewDebt As Decimal
        Dim NewDebtFile(11) As String
        Dim NewDebtSumm(11, 1) As Decimal
        Dim QueryHistoryFile, FinancialAdjustmentFile, AdditionalChargeFile, DirectPaymentFile, InformationStatusFile, QueryResponseFile, ChangesToCustomerFile, AdHocFile, MissingChargeEndDateFile As New ArrayList()
        Dim LastFileSequence As Integer, FileSequence As Integer, LineNumber As Integer, DetailCount As Integer, AdjustmentCount As Integer
        Dim HasHeader As Boolean = False

        Try
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            ClientSupportFilePath = InputFilePath & "\To Client Support\"
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents) + 10 + (UBound(NewDebtFile) + 1) ' Include the generation of the 10 output files and new debt files. Changed from 9 portal task ref 18111

            LastFileSequence = UnitedUtilitiesData.GetLastFileSequence()

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                Application.DoEvents() ' without this line, the button disappears until processing is complete

                If InputLine.Substring(0, 6).Trim = "" Then ErrorLog &= "No DCA code supplied at line number " & LineNumber.ToString & vbCrLf
                If InputLine.Substring(3, 3) <> "023" Then ErrorLog &= "Unexpected agency code " & InputLine.Substring(3, 3) & " found at line number " & LineNumber.ToString & vbCrLf

                Select Case InputLine.Substring(6, 1)
                    Case "H"
                        HasHeader = True
                        If DetailCount <> 0 Then ErrorLog &= "Header not at the start of section at line number " & LineNumber.ToString & vbCrLf
                        InputLineArray = Header(InputLine)
                        FileSequence = CInt(InputLineArray(12))
                        If FileSequence <> LastFileSequence + 1 Then ErrorLog &= "Unexpected file sequence at line number " & LineNumber.ToString & ". Found in file " & FileSequence.ToString & ", last file sequence preprocessed was " & LastFileSequence.ToString & vbCrLf
                    Case "N"
                        DetailCount += 1
                        If InputLine.Substring(69, 13) <> "Query History" Then
                            InputLineArray = NewDebt(InputLine)

                            'Check for mandatory fields first
                            If InputLineArray(2) = "" Then ExceptionLog &= "No account number supplied at line number " & LineNumber.ToString & vbCrLf
                            If InputLineArray(3) = "" Then ExceptionLog &= "No account balance supplied at line number " & LineNumber.ToString & vbCrLf
                            If InputLineArray(7) = "" Then ExceptionLog &= "No placement indicator supplied at line number " & LineNumber.ToString & vbCrLf
                            If InputLineArray(9) = "" Then ExceptionLog &= "No debt status supplied at line number " & LineNumber.ToString & vbCrLf
                            If InputLineArray(11) = "" Then ExceptionLog &= "No customer name supplied at line number " & LineNumber.ToString & vbCrLf
                            If InputLine.Substring(336, 10).Trim = "" Then ExceptionLog &= "No customer postcode supplied at line number " & LineNumber.ToString & vbCrLf
                            If InputLine.Substring(560, 8).Trim = "" Then ExceptionLog &= "No premise postcode supplied at line number " & LineNumber.ToString & vbCrLf
                            If InputLineArray(19) = "" Then ExceptionLog &= "No charge end date supplied at line number " & LineNumber.ToString & vbCrLf
                            If InputLineArray(21) = "" Then ExceptionLog &= "No customer type supplied at line number " & LineNumber.ToString & vbCrLf

                            ' Identify the client scheme
                            ConnKey = InputLine.Substring(0, 3) & InputLineArray(7) & InputLineArray(21)
                            If ConnCode.ContainsKey(ConnKey) Then

                                If AccountType.ContainsKey(InputLineArray(40)) Then
                                    InputLineArray(40) = AccountType(InputLineArray(40))
                                Else
                                    ErrorLog &= "Unknown account type of " & InputLineArray(40) & " at line number " & LineNumber.ToString & vbCrLf
                                End If

                                If Not DWPIndicator.ContainsKey(InputLineArray(17)) Then ErrorLog &= "Unknown DWP Indicator of " & InputLineArray(17) & " at line number " & LineNumber.ToString & vbCrLf

                                NewDebtFile(ConnCode(ConnKey)) &= Join(InputLineArray, Separator) & Separator
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(CDec(InputLineArray(3)).ToString, "Account Balance")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(CDec(InputLineArray(4)).ToString, "Fees Balance")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(CDec(InputLineArray(5)).ToString, "Costs Balance")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(6), "NFA Indicator")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(7), "Placement Indicator")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(8), "Previous Recall")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(9), "Debt Status")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(10), "Customer Code")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(14), "Court Case Number")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(If(InputLineArray(15) <> "00000000", Date.ParseExact(InputLineArray(15), "yyyyMMdd", Nothing).ToString("dd/MMM/yyyy"), ""), "Judgement Date")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(If(InputLineArray(16) <> "00000000", Date.ParseExact(InputLineArray(16), "yyyyMMdd", Nothing).ToString("dd/MMM/yyyy"), ""), "Leaving Date")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(If(DWPIndicator.ContainsKey(InputLineArray(17)) = False, "Unknown", DWPIndicator(InputLineArray(17))), "DWP Indicator")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(If(InputLineArray(19) <> "00000000", Date.ParseExact(InputLineArray(19), "yyyyMMdd", Nothing).ToString("dd/MMM/yyyy"), ""), "Charge End Date")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(20), "Customer Status")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(21), "Customer Type")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(26), "Associated Customer 1")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(27), "Associated Customer 2")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(28), "Associated Customer 3")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(29), "Associated Customer 4")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(CDec(InputLineArray(30)).ToString, "Commission Rate")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(31), "FAO")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(If(InputLineArray(33) <> "00000000", Date.ParseExact(InputLineArray(33), "yyyyMMdd", Nothing).ToString("dd/MMM/yyyy"), ""), "Last Payment Date")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(CDec(InputLineArray(34)).ToString, "Last Payment Amount")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(35), "Behavioural Score")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(If(InputLineArray(36) <> "00000000", Date.ParseExact(InputLineArray(36), "yyyyMMdd", Nothing).ToString("dd/MMM/yyyy"), ""), "Charge Start Date")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(If(InputLineArray(37) <> "00000000", Date.ParseExact(InputLineArray(37), "yyyyMMdd", Nothing).ToString("dd/MMM/yyyy"), ""), "Latest Bill Date")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(CDec(InputLineArray(38)).ToString, "Latest Bill Value")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(If(InputLineArray(39) <> "00000000", Date.ParseExact(InputLineArray(39), "yyyyMMdd", Nothing).ToString("dd/MMM/yyyy"), ""), "Date of Oldest Debt")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(InputLineArray(40), "Account Type")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(CDec(InputLineArray(41)).ToString, "Current Year Debt")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(CDec(InputLineArray(42)).ToString, "Previous Year Debt")(0)
                                NewDebtFile(ConnCode(ConnKey)) &= ToNote(CDec(InputLineArray(43)).ToString, "Legacy Debt")(0) & vbCrLf

                                If UnitedUtilitiesData.GetCaseLive(InputLineArray(2)) > 0 Then ExceptionLog &= "Client ref " & InputLineArray(2) & " already loaded at line number " & LineNumber.ToString & vbCrLf
                                If InputLineArray(19) = "00000000" Then MissingChargeEndDateFile.Add({InputLineArray(2)}) ' Added TS portal task ref 18111
                                Balance += InputLineArray(3)
                                NewDebtSumm(ConnCode(ConnKey), 0) += 1
                                NewDebtSumm(ConnCode(ConnKey), 1) += InputLineArray(3)
                                NewCases.Add(InputLineArray(2))
                            Else
                                ErrorLog &= "Unable to identify client scheme at line number " & LineNumber.ToString & ". Placement type: " & InputLine.Substring(0, 3) & " Placement number: " & InputLineArray(7) & " Customer type: " & InputLineArray(21) & vbCrLf
                            End If
                        Else
                            QueryHistoryFile.Add(QueryHistory(InputLine))
                            If UnitedUtilitiesData.GetCaseCount(InputLine.Substring(7, 20).Trim) = 0 And Not NewCases.Contains(InputLine.Substring(7, 20).Trim) Then ExceptionLog &= "Client ref " & InputLine.Substring(7, 20).Trim & " not loaded at line number " & LineNumber.ToString & vbCrLf
                        End If

                    Case "J"
                        DetailCount += 1
                        InputLineArray = FinancialTransaction(InputLine)
                        ' Add file sequence
                        ReDim Preserve InputLineArray(UBound(InputLineArray) + 1)
                        InputLineArray(UBound(InputLineArray)) = FileSequence.ToString

                        FinancialAdjustmentFile.Add(InputLineArray)
                        Balance += InputLineArray(3)
                        AdjustmentBalance += InputLineArray(3)
                        AdjustmentCount += 1
                        If UnitedUtilitiesData.GetCaseCount(InputLineArray(2)) = 0 Then ExceptionLog &= "Client ref " & InputLineArray(2) & " not loaded at line number " & LineNumber.ToString & vbCrLf

                    Case "M"
                        DetailCount += 1
                        InputLineArray = FinancialTransaction(InputLine)
                        ' Add file sequence
                        ReDim Preserve InputLineArray(UBound(InputLineArray) + 1)
                        InputLineArray(UBound(InputLineArray)) = FileSequence.ToString

                        If UnitedUtilitiesData.GetCaseCount(InputLineArray(2)) = 0 Then ExceptionLog &= "Client ref " & InputLineArray(2) & " not loaded at line number " & LineNumber.ToString & vbCrLf

                        If InputLineArray(22) = (DateAdd(DateInterval.Month, -3, Date.Today).Year + 2).ToString & "0331" Then
                            UnitedUtilitiesData.SetMainBillingCharge(InputLine)
                            ExceptionLog &= "Client ref " & InputLineArray(2) & " Main Billing Charge not loaded as it is in the next financial year at line number " & LineNumber.ToString & vbCrLf
                            MBCNotes &= UnitedUtilitiesData.GetCaseID(InputLineArray(2)) & "|Main Billing Charge of " & CDec(InputLineArray(3)) & " chargeable from 01/Apr/" & (DateAdd(DateInterval.Month, -3, Date.Today).Year + 1).ToString & "." & vbCrLf
                            Balance += InputLineArray(3) ' This needs to be included so that the trailer reconciliation does not generate errors
                        Else
                            AdditionalChargeFile.Add(InputLineArray)
                            Balance += InputLineArray(3)
                            AdjustmentBalance += InputLineArray(3)
                            AdjustmentCount += 1
                        End If

                    Case "P"
                        DetailCount += 1
                        InputLineArray = FinancialTransaction(InputLine)
                        ' Add file sequence
                        ReDim Preserve InputLineArray(UBound(InputLineArray) + 1)
                        InputLineArray(UBound(InputLineArray)) = FileSequence.ToString

                        DirectPaymentFile.Add(InputLineArray)
                        Balance += InputLineArray(3)
                        AdjustmentBalance += InputLineArray(3)
                        AdjustmentCount += 1
                        If UnitedUtilitiesData.GetCaseCount(InputLineArray(2)) = 0 Then ExceptionLog &= "Client ref " & InputLineArray(2) & " not loaded at line number " & LineNumber.ToString & vbCrLf

                    Case "R"
                        DetailCount += 1
                        InformationStatusFile.Add(InformationStatus(InputLine))
                        If UnitedUtilitiesData.GetCaseCount(InputLine.Substring(7, 20).Trim) = 0 And Not NewCases.Contains(InputLine.Substring(7, 20).Trim) Then ExceptionLog &= "Client ref " & InputLine.Substring(7, 20).Trim & " not loaded at line number " & LineNumber.ToString & vbCrLf

                    Case "Q"
                        DetailCount += 1
                        QueryResponseFile.Add(QueryResponse(InputLine))
                        If UnitedUtilitiesData.GetCaseCount(InputLine.Substring(7, 20).Trim) = 0 And Not NewCases.Contains(InputLine.Substring(7, 20).Trim) Then ExceptionLog &= "Client ref " & InputLine.Substring(7, 20).Trim & " not loaded at line number " & LineNumber.ToString & vbCrLf

                    Case "S"
                        DetailCount += 1
                        ChangesToCustomerFile.Add(ChangesToCustomer(InputLine))
                        If UnitedUtilitiesData.GetCaseCount(InputLine.Substring(7, 20).Trim) = 0 And Not NewCases.Contains(InputLine.Substring(7, 20).Trim) Then ExceptionLog &= "Client ref " & InputLine.Substring(7, 20).Trim & " not loaded at line number " & LineNumber.ToString & vbCrLf

                    Case "I"
                        DetailCount += 1
                        AdHocFile.Add(AdHoc(InputLine))
                        If UnitedUtilitiesData.GetCaseCount(InputLine.Substring(7, 20).Trim) = 0 And Not NewCases.Contains(InputLine.Substring(7, 20).Trim) Then ExceptionLog &= "Client ref " & InputLine.Substring(7, 20).Trim & " not loaded at line number " & LineNumber.ToString & vbCrLf

                    Case "T"
                        InputLineArray = Trailer(InputLine)
                        If CInt(InputLineArray(2)) <> DetailCount Then ErrorLog &= "Incorrect trailer data record count at line number " & LineNumber.ToString & vbCrLf
                        If CDec(InputLineArray(3)) <> Decimal.Round(Balance, 2) Then ErrorLog &= "Incorrect trailer total balance at line number " & LineNumber.ToString & ". Trailer: " & CDec(InputLine.Substring(12, 13)).ToString & " Detail: " & Balance.ToString & vbCrLf ' MOD TS 4/Apr/2013 Round added to avoid floating point errors
                        If Not HasHeader Then ErrorLog &= "No corresponding header at line number " & LineNumber.ToString & vbCrLf
                        HasHeader = False

                        Balance = 0
                        DetailCount = 0
                    Case Else
                        ErrorLog &= "Unknown record type of '" & InputLine.Substring(6, 1) & "'at line number " & LineNumber.ToString & ". Line cannot be processed." & vbCrLf
                End Select

            Next InputLine

            ProgressBar.Value += 1
            AuditLog = "File processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "File sequence number: " & FileSequence & vbCrLf & vbCrLf

            AuditLog &= "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            For NewDebtFileCount As Integer = 0 To UBound(NewDebtFile)
                ProgressBar.Value += 1
                If NewDebtFile(NewDebtFileCount) <> "" Then
                    WriteFile(InputFilePath & ConnID(NewDebtFileCount) & "_" & FileName & "_NewDebt.txt", NewDebtHeader & NewDebtFile(NewDebtFileCount))
                    OutputFiles.Add(InputFilePath & ConnID(NewDebtFileCount) & "_" & FileName & "_NewDebt.txt")
                    AuditLog &= "Clientscheme " & ConnID(NewDebtFileCount) & vbCrLf
                    AuditLog &= "Number of new cases: " & NewDebtSumm(NewDebtFileCount, 0).ToString & vbCrLf
                    AuditLog &= "Total balance of new cases: " & NewDebtSumm(NewDebtFileCount, 1).ToString & vbCrLf & vbCrLf
                    TotalNewDebt += NewDebtSumm(NewDebtFileCount, 1)
                End If
            Next NewDebtFileCount

            If ConfigurationManager.AppSettings("Environment") = "Prod" Then ' Added TS 11/Apr/2013
                UnitedUtilitiesData.SetLastFileSequence(FileSequence, NewCases.Count, TotalNewDebt)
            Else
                MsgBox("Test version. Last file sequence not set.", vbInformation + vbOKOnly, Me.Text)
            End If

            AuditLog &= "Number of financial adjustments: " & AdjustmentCount.ToString & vbCrLf
            AuditLog &= "Total balance of financial adjustments: " & AdjustmentBalance.ToString & vbCrLf

            ProgressBar.Value += 1
            If QueryHistoryFile.Count > 0 Then
                OutputToSeparatedFile(ClientSupportFilePath, FileName & "_QueryHistory.txt", "|", QueryHistoryHeader, QueryHistoryFile)
                OutputFiles.Add(ClientSupportFilePath & FileName & "_QueryHistory.txt")
            End If

            ProgressBar.Value += 1
            If FinancialAdjustmentFile.Count > 0 Then
                OutputToExcel(ClientSupportFilePath, FileName & "_FinancialAdjustments", "Financial Adjustments", FinancialTransactionHeader, FinancialAdjustmentFile)
                OutputFiles.Add(ClientSupportFilePath & FileName & "_FinancialAdjustments.xls")
            End If

            ProgressBar.Value += 1
            If AdditionalChargeFile.Count > 0 Then
                OutputToExcel(ClientSupportFilePath, FileName & "_AdditionalCharges", "Additional Charges", FinancialTransactionHeader, AdditionalChargeFile)
                OutputFiles.Add(ClientSupportFilePath & FileName & "_AdditionalCharges.xls")
            End If

            ProgressBar.Value += 1
            If DirectPaymentFile.Count > 0 Then
                OutputToExcel(ClientSupportFilePath, FileName & "_DirectPayments", "Direct Payments", FinancialTransactionHeader, DirectPaymentFile)
                OutputFiles.Add(ClientSupportFilePath & FileName & "_DirectPayments.xls")
            End If

            ProgressBar.Value += 1
            If InformationStatusFile.Count > 0 Then
                OutputToExcel(ClientSupportFilePath, FileName & "_InformationStatus", "Information Status", InformationStatusHeader, InformationStatusFile)
                OutputFiles.Add(ClientSupportFilePath & FileName & "_InformationStatus.xls")
            End If

            ProgressBar.Value += 1
            If QueryResponseFile.Count > 0 Then
                'OutputToExcel(ClientSupportFilePath, FileName & "_QueryResponse", "Query Response", QueryResponseHeader, QueryResponseFile)
                OutputToSeparatedFile(ClientSupportFilePath, FileName & "_QueryResponse.txt", "|", QueryResponseHeader, QueryResponseFile)
                OutputFiles.Add(ClientSupportFilePath & FileName & "_QueryResponse.txt")
            End If

            ProgressBar.Value += 1
            If ChangesToCustomerFile.Count > 0 Then
                OutputToExcel(ClientSupportFilePath, FileName & "_ChangesToCustomer", "Changes to Customer", ChangesToCustomerHeader, ChangesToCustomerFile)
                OutputFiles.Add(ClientSupportFilePath & FileName & "_ChangesToCustomer.xls")
            End If

            ProgressBar.Value += 1
            If AdHocFile.Count > 0 Then
                'OutputToExcel(ClientSupportFilePath, FileName & "_AdHoc", "Ad hoc", AdHocHeader, AdHocFile)
                OutputToSeparatedFile(ClientSupportFilePath, FileName & "_AdHoc.txt", "|", AdHocHeader, AdHocFile)
                OutputFiles.Add(ClientSupportFilePath & FileName & "_AdHoc.txt")
            End If

            ProgressBar.Value += 1
            If MissingChargeEndDateFile.Count > 0 Then
                OutputToSeparatedFile(ClientSupportFilePath, FileName & "_MissingChargeEndDate.txt", "|", Nothing, MissingChargeEndDateFile)
                OutputFiles.Add(ClientSupportFilePath & FileName & "_MissingChargeEndDate.txt")
            End If

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", ExceptionLog)
            If MBCNotes <> "" Then WriteFile(ClientSupportFilePath & FileName & "_MBCNotes.txt", MBCNotes)

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub

    Private Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, Optional ByVal NoteLength As Integer = 250) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 2) ' + 2 is for :;
        Dim PreviousLength As Integer
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = (NoteTag & ":" & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)) & ";").PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Clipboard.SetText("notepad.exe " & InputFilePath & FileName & FileExt)
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub
End Class


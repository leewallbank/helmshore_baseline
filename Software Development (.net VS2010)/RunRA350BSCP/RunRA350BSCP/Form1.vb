﻿Imports System.IO

Public Class Form1
    Dim start_date As Date
    Dim end_date As Date
    Dim filename As String = ""
    Dim filepath As String = ""
    Private Sub runbtn_Click(sender As System.Object, e As System.EventArgs) Handles runbtn.Click
        'check no more than 14 days difference
        start_date = start_dtp.Value
        end_date = end_dtp.Value
        If DateDiff(DateInterval.Day, start_date, end_date) > 14 Then
            MsgBox("Up to 14 days only please")
            Exit Sub
        End If
        runbtn.Enabled = False
        statuslbl.Text = "Starting report"
        run_report()

        Me.Close()
    End Sub
    Private Sub run_report()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()

        Dim RA350BSCP2report = New RA350BSCP2
        Dim myArrayList1 As ArrayList = New ArrayList()

        myArrayList1.Add(start_date)
        SetCurrentValuesForParameterField1(RA350BSCP2report, myArrayList1)

        filename = "RA350BSCP2 Payments taken by CC " & Format(start_date, "dd-MM-yyyy") & " to " & Format(end_date, "dd-MM-yyyy") & ".xls"
        'add 1 day to end date as report checks for < end date
        end_date = DateAdd(DateInterval.Day, 1, end_date)
        myArrayList1.Add(end_date)
        SetCurrentValuesForParameterField2(RA350BSCP2report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA350BSCP2report)

        Dim savefiledialog1 As New SaveFileDialog
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = filename
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            statuslbl.Text = "Running report ... please wait "
            filename = SaveFileDialog1.FileName
            RA350BSCP2report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, savefiledialog1.FileName)
            RA350BSCP2report.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If

       
    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        end_date = DateAdd(DateInterval.Day, -Weekday(Now) + 1, Now)
        end_date = CDate(Format(end_date, "MMM yyyy dd") & " 00:00:00")
        start_date = DateAdd(DateInterval.Day, -6, end_date)
        start_date = CDate(Format(start_date, "MMM yyyy dd") & " 00:00:00")
        start_dtp.Value = start_date
        end_dtp.Value = end_date
    End Sub
End Class

﻿Imports CommonLibrary
Imports System.IO
Public Class Form1

    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click
        Dim FileDialog As New OpenFileDialog
        readbtn.Enabled = False
        Dim outfile As String = ""
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        Dim inputfilepath, filename, fileext As String
        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)
        Dim excel_file_contents(,) As String = InputFromExcel(FileDialog.FileName)
        Dim lastRow As Integer = UBound(excel_file_contents)
        Dim rowIDX As Integer
        Dim inputline As String = ""
        For rowIDX = 1 To lastRow
            Try
                ProgressBar1.Value = (rowIDX / lastRow) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            'client ref is in first column
            Dim clientREf As String = Trim(excel_file_contents(rowIDX, 0))
            If clientREf.Length < 2 Then
                Continue For
            End If

            'phone number in col 4 and 6
            Dim phoneNumber As String = Trim(excel_file_contents(rowIDX, 4))
            Dim phoneNumber2 As String = Trim(excel_file_contents(rowIDX, 6))
            If phoneNumber.Length < 4 Then
                phoneNumber = Nothing
            End If
            If phoneNumber2.Length < 4 Then
                phoneNumber2 = Nothing
            End If
            If phoneNumber = Nothing And phoneNumber2 = Nothing Then
                Continue For
            End If
            If phoneNumber = Nothing Then
                phoneNumber = phoneNumber2
                phoneNumber2 = Nothing
            End If
            Dim dt As New DataTable
            LoadDataTable("DebtRecovery", "SELECT _rowid, linkID, clientSchemeID " & _
                                                    "FROM debtor " & _
                                                    "WHERE client_ref = '" & clientREf & "'" & _
                                                    " and status_open_closed = 'O'" & _
                                                    " order by linkID", dt, False)
            Dim row As DataRow
            Dim lastLinkID As Integer = 0
            For Each row In dt.Rows
                Dim CSID As Integer = row(2)
                Dim clientID As Integer
                Try
                    clientID = GetSQLResults("DebtRecovery", "select clientID from clientScheme where _rowid = " & CSID)
                Catch ex As Exception
                    MsgBox("Unable to read clientScheme for " & CSID)
                End Try
                If clientID <> 112 Then
                    Continue For
                End If
                Dim debtorID As Integer = row(0)
                Dim linkID As Integer
                Try
                    linkID = row.Item(1)
                Catch ex As Exception
                    'linkID is blank
                    outfile &= debtorID & "," & phoneNumber
                    If phoneNumber2 <> Nothing Then
                        outfile &= "," & phoneNumber2 & vbNewLine
                    Else
                        outfile &= vbNewLine
                    End If
                    Continue For
                End Try
                If linkID = lastLinkID Then
                    Continue For
                End If
                lastLinkID = linkID
                'get all open cases for this linkID
                Dim dt2 As New DataTable
                LoadDataTable("DebtRecovery", "SELECT _rowid " & _
                                                        "FROM debtor " & _
                                                        " WHERE linkId = " & linkID & _
                                                        " AND status_open_closed = 'O'", dt2, False)
                Dim linkRow As DataRow
                For Each linkRow In dt2.Rows
                    Dim linkDebtorID As Integer = linkRow.Item(0)
                    outfile &= linkDebtorID & "," & phoneNumber
                    If phoneNumber2 <> Nothing Then
                        outfile &= "," & phoneNumber2 & vbNewLine
                    Else
                        outfile &= vbNewLine
                    End If
                Next
            Next
        Next

        WriteFile(inputfilepath & filename & "_loadtel.txt", outfile)
        MsgBox("Tel file written")
        Me.Close()
    End Sub
End Class

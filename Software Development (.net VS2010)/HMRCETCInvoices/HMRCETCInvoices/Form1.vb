﻿Imports System.IO

Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        runbtn.Enabled = False
        exitbtn.Enabled = False
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD253GPreport = New RD253GP
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD253GPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD253GPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253GPreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RD253G TDX HMRC ETC non-direct invoice.pdf"
        End With

        If SaveFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("Reports not run")
            Exit Sub
        End If
        Dim pathname As String = Path.GetDirectoryName(SaveFileDialog1.FileName) & "\"
        RD253GPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, SaveFileDialog1.FileName)
        RD253GPreport.Close()

        Dim RD253GDPreport = New RD253GDP
        'Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD253GDPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD253GDPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253GDPreport)
        Dim filename As String = pathname & "RD253GD TDX HMRC ETC direct invoice.pdf"
        RD253GDPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD253GDPreport.Close()

        Dim RD448FPreport = New RD448FP
        'Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD448FPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD448FPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD448FPreport)
        filename = pathname & "RD448F TDX HMRC ETC Fee invoice.pdf"
        RD448FPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD448FPreport.Close()
        MsgBox("Reports saved")
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        start_dtp.Value = DateAdd(DateInterval.Day, -4 - Weekday(Now), Now)
        end_dtp.Value = DateAdd(DateInterval.Day, -Weekday(Now) + 2, Now)
    End Sub
End Class

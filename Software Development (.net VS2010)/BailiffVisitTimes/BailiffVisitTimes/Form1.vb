﻿Imports CommonLibrary
Public Class Form1
    Dim type As String
    Dim start_date As Date = CDate("2013 Jun 1")
    Dim upd_txt As String
    Dim startDebtorID As Integer


    Private Sub run_fc(ByVal debtorid As Integer)
        Dim note_dt As New DataTable
        Dim noteText As String
        Dim days2FirstVisit As Integer = 99999
        Dim visited As Integer = 0
        Dim lastAllocationDate As Date = Nothing
        Dim allocationDate2Store As Date = Nothing
        Dim de_alloc_without_visit As Integer = 99999
        Dim trace_while_allocated As Integer = 99999
        Dim return_while_allocated As Integer = 99999
        LoadDataTable2("DebtRecovery", "SELECT text, _createdDate " & _
                                            "FROM note " & _
                                            "WHERE type = 'Stage' " & _
                                            " AND text like '%F C%'" & _
                                      " AND debtorID = " & debtorid & _
                                      " order by _createdDate", note_dt, False)
        For Each noterow In note_dt.Rows
            Dim stageDate As Date = noterow(1)
            'see if allocated after stage date
            Dim allocationDate As Date = Nothing
            Dim note2_dt As New DataTable
            LoadDataTable2("DebtRecovery", "SELECT text, _createdDate, type " & _
                                            "FROM note " & _
                                           "WHERE (((type = 'Allocated' or type = 'Note')" & _
                                            " AND text like 'Bailiff%') OR" & _
                                            "   type = 'Stage')" & _
                                            " And _createdDate >= '" & Format(stageDate, "yyyy-MM-dd") & "'" & _
                                      " AND debtorID = " & debtorid & _
                                      " ORDER BY _createdDate", note2_dt, False)
            For Each note2row In note2_dt.Rows
                noteText = note2row(0)
                Dim noteType As String = note2row(2)
                If noteType = "Stage" Then
                    If InStr(noteText, "F C") = 0 Then
                        Exit For
                    Else
                        Continue For
                    End If
                End If


                'get bailiffid
                Dim startIDX As Integer = InStr(noteText, "ID:")
                If startIDX = 0 Then
                    Continue For
                End If
                Dim endIDX As Integer = InStr(noteText, ")")
                If endIDX <= startIDX Then
                    Continue For
                End If
                Dim bailID As Integer
                Try
                    bailID = Mid(noteText, startIDX + 3, endIDX - startIDX - 3)
                Catch ex As Exception
                    MsgBox("Invalid bailID - debtorid = " & debtorid)
                End Try
                'check bailID is a real bailiff
                Dim addMobileName As String = ""
                addMobileName = GetSQLResults2("DebtRecovery", "SELECT add_mobile_name " & _
                                           "FROM bailiff " & _
                                           "WHERE _rowid= " & bailID)
                If addMobileName = "None" Then
                    Continue For
                End If
                allocationDate = note2row(1)
                If allocationDate <> Nothing Then
                    lastAllocationDate = allocationDate
                End If
                'get first visit date
                de_alloc_without_visit = 99999
                trace_while_allocated = 99999
                return_while_allocated = 99999
                Dim visitDate As Date = Nothing

                Try
                    visitDate = GetSQLResults2("DebtRecovery", "select date_visited from visit" & _
                                         " where debtorID = " & debtorid & _
                                         " and bailiffID = " & bailID & _
                                          " and date_visited >= '" & Format(allocationDate, "yyyy-MM-dd") & "'" & _
                                         " AND date_allocated = '" & Format(allocationDate, "yyyy-MM-dd") & "'" & _
                                         " ORDER BY date_visited")
                Catch ex As Exception

                End Try
                Dim testDays As Integer = 99999
                If visitDate <> Nothing Then
                    visitDate = CDate(Format(visitDate, "yyyy-MM-dd") & " 00:00:00")
                    allocationDate = CDate(Format(allocationDate, "yyyy-MM-dd") & " 00:00:00")
                    Try
                        testDays = DateDiff(DateInterval.Day, allocationDate, visitDate)
                        If testDays < 0 Then
                            testDays = 99999
                        End If
                        If testDays < days2FirstVisit Then
                            days2FirstVisit = testDays
                            allocationDate2Store = allocationDate
                        End If
                        visited = 1
                    Catch ex As Exception

                    End Try
                End If
                If visited = 0 And de_alloc_without_visit = 99999 And allocationDate <> Nothing Then
                    Dim deAllocation_dt As New DataTable
                    'no visit so see if bailiff has finished with the case
                    LoadDataTable2("FeesSQL", "SELECT bail_de_allocation_found_date, bail_allocation_date " & _
                                                       "FROM BailiffAllocationTable " & _
                                                       "WHERE bail_debtorID = " & debtorid, deAllocation_dt, False)
                    Dim deAllocationDate As Date
                    For Each deAllocRow In deAllocation_dt.Rows
                        Try
                            deAllocationDate = deAllocRow(0)
                        Catch ex As Exception
                            deAllocationDate = Nothing
                        End Try
                        If deAllocationDate = Nothing Then
                            Continue For
                        ElseIf Format(allocationDate, "yyyy-MM-dd") <> Format(deAllocRow(1), "yyyy-MM-dd") Then
                            Continue For
                        ElseIf Format(deAllocationDate, "yyyy-MM-dd") = CDate("Jan 1, 1900") Then
                            de_alloc_without_visit = 0
                            lastAllocationDate = allocationDate
                        Else
                            de_alloc_without_visit = 1
                        End If
                    Next
                    If deAllocationDate = Nothing Then
                        deAllocationDate = CDate("Jan 1, 2100")
                    End If
                    'see if returned or put into trace or allocated while bailiff had case
                    Dim note3_dt As New DataTable
                    LoadDataTable2("DebtRecovery", "SELECT type, _createdDate " & _
                                                    "FROM note " & _
                                                     "WHERE (type = 'Cancelled' or type = 'Trace' or type = 'Allocated' or type = 'Removed agent')" & _
                                                  " AND debtorID = " & debtorid & _
                                              " ORDER BY _createdDate", note3_dt, False)
                    For Each note3row In note3_dt.Rows
                        noteText = note3row(0)
                        Dim noteDate As String = note3row(1)
                        If noteDate <= allocationDate Then
                            Continue For
                        End If
                        If noteText = "Trace" Then
                            trace_while_allocated = 1
                        End If
                        If noteText = "Cancelled" Then
                            return_while_allocated = 1
                        End If
                        If noteText = "Allocated" Then
                            If Format(noteDate, "yyyy-MMM-dd") = Format(allocationDate, "yyyy-MMM-dd") Then
                                'ignore allocation
                                lastAllocationDate = Nothing
                                Exit For
                            End If
                            de_alloc_without_visit = 1
                        End If
                        If noteText = "Removed agent" Then
                            If Format(noteDate, "yyyy-MMM-dd") = Format(allocationDate, "yyyy-MMM-dd") Then
                                'ignore allocation
                                lastAllocationDate = Nothing
                                Exit For
                            End If
                        End If
                    Next
                End If
            Next
        Next
        If visited = 1 Or lastAllocationDate <> Nothing Then
            If visited = 1 Then
                de_alloc_without_visit = 0
                trace_while_allocated = 0
                return_while_allocated = 0
            Else
                If de_alloc_without_visit = 0 Then
                    trace_while_allocated = 0
                    return_while_allocated = 0
                End If
            End If
            If de_alloc_without_visit = 99999 Then
                de_alloc_without_visit = 0
            End If
            If trace_while_allocated = 1 Or return_while_allocated = 1 Then
                de_alloc_without_visit = 1
            End If
            If allocationDate2Store = Nothing Then
                allocationDate2Store = lastAllocationDate
            End If
            If trace_while_allocated = 99999 Then
                trace_while_allocated = 0
            End If
            If return_while_allocated = 99999 Then
                return_while_allocated = 0
            End If
            upd_txt = "insert into Visit_stats (debtorID, " & _
                   "stage, days_to_first_visit, visited, allocation_date,de_alloc_without_visit, trace_while_allocated, return_while_allocated)" & _
                   "values (" & debtorid & ",'FC'," & days2FirstVisit & "," & visited & "," & _
                   "'" & allocationDate2Store & "'," & de_alloc_without_visit & "," & trace_while_allocated & "," & return_while_allocated & ")"
            update_sql(upd_txt)
        End If
        
    End Sub
    Private Sub run_van(ByVal debtorid As Integer)
        Dim note_dt As New DataTable
        Dim noteText As String
        Dim days2FirstVisit As Integer = 99999
        Dim visited As Integer = 0
        Dim lastAllocationDate As Date = Nothing
        Dim allocationDate2Store As Date = Nothing
        Dim de_alloc_without_visit As Integer = 99999
        Dim trace_while_allocated As Integer = 99999
        Dim return_while_allocated As Integer = 99999
        LoadDataTable2("DebtRecovery", "SELECT text, _createdDate " & _
                                            "FROM note " & _
                                            "WHERE type = 'Stage' " & _
                                            " AND not text like '%urther%'" & _
                                                " AND text like '%Van Attendance%'" & _
                                      " AND debtorID = " & debtorid & _
                                      " order by _createdDate", note_dt, False)
        For Each noterow In note_dt.Rows
            Dim stageDate As Date = noterow(1)
            'see if allocated after stage date
            Dim allocationDate As Date = Nothing
            Dim note2_dt As New DataTable
            LoadDataTable2("DebtRecovery", "SELECT text, _createdDate, type " & _
                                            "FROM note " & _
                                           "WHERE (((type = 'Allocated' or type = 'Note')" & _
                                            " AND text like 'Bailiff%') OR" & _
                                            "   type = 'Stage')" & _
                                            " And _createdDate >= '" & Format(stageDate, "yyyy-MM-dd") & "'" & _
                                      " AND debtorID = " & debtorid & _
                                      " ORDER BY _createdDate", note2_dt, False)
            For Each note2row In note2_dt.Rows
               
                noteText = note2row(0)
                Dim noteType As String = note2row(2)
                If noteType = "Stage" Then
                    If InStr(noteText, "Van") = 0 Or _
                        InStr(noteText, "urther") > 0 Then
                        Exit For
                    Else
                        Continue For
                    End If
                End If

                'get bailiffid
                Dim startIDX As Integer = InStr(noteText, "ID:")
                If startIDX = 0 Then
                    Continue For
                End If
                Dim endIDX As Integer = InStr(noteText, ")")
                If endIDX <= startIDX Then
                    Continue For
                End If
                Dim bailID As Integer
                Try
                    bailID = Mid(noteText, startIDX + 3, endIDX - startIDX - 3)
                Catch ex As Exception
                    MsgBox("Invalid bailID - debtorid = " & debtorid)
                End Try
                'check bailID is a real bailiff
                Dim addMobileName As String = ""
                addMobileName = GetSQLResults2("DebtRecovery", "SELECT add_mobile_name " & _
                                           "FROM bailiff " & _
                                           "WHERE _rowid= " & bailID)
                If addMobileName = "None" Then
                    Continue For
                End If
                allocationDate = note2row(1)
                If allocationDate <> Nothing Then
                    lastAllocationDate = allocationDate
                End If
                'get first visit date
                de_alloc_without_visit = 99999
                trace_while_allocated = 99999
                return_while_allocated = 99999
                Dim visitDate As Date = Nothing

                Try
                    visitDate = GetSQLResults2("DebtRecovery", "select date_visited from visit" & _
                                         " where debtorID = " & debtorid & _
                                         " and bailiffID = " & bailID & _
                                          " and date_visited >= '" & Format(allocationDate, "yyyy-MM-dd") & "'" & _
                                         " AND date_allocated = '" & Format(allocationDate, "yyyy-MM-dd") & "'" & _
                                         " ORDER BY date_visited")
                Catch ex As Exception

                End Try
                Dim testDays As Integer = 99999
                If visitDate <> Nothing Then
                    visitDate = CDate(Format(visitDate, "yyyy-MM-dd") & " 00:00:00")
                    allocationDate = CDate(Format(allocationDate, "yyyy-MM-dd") & " 00:00:00")
                    Try
                        testDays = DateDiff(DateInterval.Day, allocationDate, visitDate)
                        If testDays < 0 Then
                            testDays = 99999
                        End If
                        If testDays < days2FirstVisit Then
                            days2FirstVisit = testDays
                            allocationDate2Store = allocationDate
                        End If
                        visited = 1
                    Catch ex As Exception

                    End Try
                End If
                If visited = 0 And de_alloc_without_visit = 99999 And allocationDate <> Nothing Then
                    Dim deAllocation_dt As New DataTable
                    'no visit so see if bailiff has finished with the case
                    LoadDataTable2("FeesSQL", "SELECT bail_de_allocation_found_date, bail_allocation_date " & _
                                                       "FROM BailiffAllocationTable " & _
                                                       "WHERE bail_debtorID = " & debtorid, deAllocation_dt, False)
                    Dim deAllocationDate As Date
                    For Each deAllocRow In deAllocation_dt.Rows
                        Try
                            deAllocationDate = deAllocRow(0)
                        Catch ex As Exception
                            deAllocationDate = Nothing
                        End Try
                        If deAllocationDate = Nothing Then
                            Continue For
                        ElseIf Format(allocationDate, "yyyy-MM-dd") <> Format(deAllocRow(1), "yyyy-MM-dd") Then
                            Continue For
                        ElseIf Format(deAllocationDate, "yyyy-MM-dd") = CDate("Jan 1, 1900") Then
                            de_alloc_without_visit = 0
                            lastAllocationDate = allocationDate
                        Else
                            de_alloc_without_visit = 1
                        End If
                    Next
                    If deAllocationDate = Nothing Then
                        deAllocationDate = CDate("Jan 1, 2100")
                    End If
                    'see if returned or put into trace or allocated while bailiff had case
                    Dim note3_dt As New DataTable
                    LoadDataTable2("DebtRecovery", "SELECT type, _createdDate " & _
                                                    "FROM note " & _
                                                    "WHERE (type = 'Cancelled' or type = 'Trace' or type = 'Allocated' or type = 'Removed agent')" & _
                                                   " AND debtorID = " & debtorid & _
                                              " ORDER BY _createdDate", note3_dt, False)
                    For Each note3row In note3_dt.Rows
                        noteText = note3row(0)
                        Dim noteDate As Date = note3row(1)
                        If noteDate <= allocationDate Then
                            Continue For
                        End If
                        If noteText = "Trace" Then
                            trace_while_allocated = 1
                        End If
                        If noteText = "Cancelled" Then
                            return_while_allocated = 1
                        End If
                        If noteText = "Allocated" Then
                            If Format(noteDate, "yyyy-MMM-dd") = Format(allocationDate, "yyyy-MMM-dd") Then
                                'ignore allocation
                                lastAllocationDate = Nothing
                                Exit For
                            End If
                            de_alloc_without_visit = 1
                        End If
                        If noteText = "Removed agent" Then
                            If Format(noteDate, "yyyy-MMM-dd") = Format(allocationDate, "yyyy-MMM-dd") Then
                                'ignore allocation
                                lastAllocationDate = Nothing
                                Exit For
                            End If
                        End If
                    Next
                End If
            Next
        Next
        If visited = 1 Or lastAllocationDate <> Nothing Then
            If visited = 1 Then
                de_alloc_without_visit = 0
                trace_while_allocated = 0
                return_while_allocated = 0
            Else
                If de_alloc_without_visit = 0 Then
                    trace_while_allocated = 0
                    return_while_allocated = 0
                End If
            End If
            If de_alloc_without_visit = 99999 Then
                de_alloc_without_visit = 0
            End If
            If trace_while_allocated = 1 Or return_while_allocated = 1 Then
                de_alloc_without_visit = 1
            End If
            If allocationDate2Store = Nothing Then
                allocationDate2Store = lastAllocationDate
            End If
            If trace_while_allocated = 99999 Then
                trace_while_allocated = 0
            End If
            If return_while_allocated = 99999 Then
                return_while_allocated = 0
            End If
            upd_txt = "insert into Visit_stats (debtorID, " & _
                   "stage, days_to_first_visit, visited, allocation_date,de_alloc_without_visit, trace_while_allocated, return_while_allocated)" & _
                   "values (" & debtorid & ",'VAN'," & days2FirstVisit & "," & visited & "," & _
                   "'" & allocationDate2Store & "'," & de_alloc_without_visit & "," & trace_while_allocated & "," & return_while_allocated & ")"
            update_sql(upd_txt)
        End If
    End Sub
    Private Sub run_fvan(ByVal debtorid As Integer)
        Dim note_dt As New DataTable
        Dim noteText As String
        Dim days2FirstVisit As Integer = 99999
        Dim visited As Integer = 0
        Dim lastAllocationDate As Date = Nothing
        Dim allocationDate2Store As Date = Nothing
        Dim de_alloc_without_visit As Integer = 99999
        Dim trace_while_allocated As Integer = 99999
        Dim return_while_allocated As Integer = 99999
        LoadDataTable2("DebtRecovery", "SELECT text, _createdDate " & _
                                            "FROM note " & _
                                            "WHERE type = 'Stage' " & _
                                            " AND text like '%urther%'" & _
                                            " AND debtorID = " & debtorid & _
                                      " order by _createdDate", note_dt, False)
        For Each noterow In note_dt.Rows
            Dim stageDate As Date = noterow(1)
            'see if allocated after stage date
            Dim allocationDate As Date = Nothing
            Dim note2_dt As New DataTable
            LoadDataTable2("DebtRecovery", "SELECT text, _createdDate, type " & _
                                            "FROM note " & _
                                           "WHERE (((type = 'Allocated' or type = 'Note')" & _
                                            " AND text like 'Bailiff%') OR" & _
                                            "   type = 'Stage')" & _
                                            " And _createdDate >= '" & Format(stageDate, "yyyy-MM-dd") & "'" & _
                                      " AND debtorID = " & debtorid & _
                                      " ORDER BY _createdDate", note2_dt, False)
            For Each note2row In note2_dt.Rows
                noteText = note2row(0)
                Dim noteType As String = note2row(2)
                If noteType = "Stage" Then
                    If InStr(noteText, "urther") = 0 Then
                        Exit For
                    Else
                        Continue For
                    End If
                End If

                'get bailiffid
                Dim startIDX As Integer = InStr(noteText, "ID:")
                If startIDX = 0 Then
                    Continue For
                End If
                Dim endIDX As Integer = InStr(noteText, ")")
                If endIDX <= startIDX Then
                    Continue For
                End If
                Dim bailID As Integer
                Try
                    bailID = Mid(noteText, startIDX + 3, endIDX - startIDX - 3)
                Catch ex As Exception
                    MsgBox("Invalid bailID - debtorid = " & debtorid)
                End Try
                'check bailID is a real bailiff
                Dim addMobileName As String = ""
                addMobileName = GetSQLResults2("DebtRecovery", "SELECT add_mobile_name " & _
                                           "FROM bailiff " & _
                                           "WHERE _rowid= " & bailID)
                If addMobileName = "None" Then
                    Continue For
                End If
                allocationDate = note2row(1)
                If allocationDate <> Nothing Then
                    lastAllocationDate = allocationDate
                End If
                'get first visit date
                de_alloc_without_visit = 99999
                trace_while_allocated = 99999
                return_while_allocated = 99999
                Dim visitDate As Date = Nothing

                Try
                    visitDate = GetSQLResults2("DebtRecovery", "select date_visited from visit" & _
                                         " where debtorID = " & debtorid & _
                                         " and bailiffID = " & bailID & _
                                          " and date_visited >= '" & Format(allocationDate, "yyyy-MM-dd") & "'" & _
                                         " AND date_allocated = '" & Format(allocationDate, "yyyy-MM-dd") & "'" & _
                                         " ORDER BY date_visited")
                Catch ex As Exception

                End Try
                Dim testDays As Integer = 99999
                If visitDate <> Nothing Then
                    visitDate = CDate(Format(visitDate, "yyyy-MM-dd") & " 00:00:00")
                    allocationDate = CDate(Format(allocationDate, "yyyy-MM-dd") & " 00:00:00")
                    Try
                        testDays = DateDiff(DateInterval.Day, allocationDate, visitDate)
                        If testDays < 0 Then
                            testDays = 99999
                        End If
                        If testDays < days2FirstVisit Then
                            days2FirstVisit = testDays
                            allocationDate2Store = allocationDate
                        End If
                        visited = 1
                    Catch ex As Exception

                    End Try
                End If
                If visited = 0 And de_alloc_without_visit = 99999 And allocationDate <> Nothing Then
                    Dim deAllocation_dt As New DataTable
                    'no visit so see if bailiff has finished with the case
                    LoadDataTable2("FeesSQL", "SELECT bail_de_allocation_found_date, bail_allocation_date " & _
                                                       "FROM BailiffAllocationTable " & _
                                                       "WHERE bail_debtorID = " & debtorid, deAllocation_dt, False)
                    Dim deAllocationDate As Date
                    For Each deAllocRow In deAllocation_dt.Rows
                        Try
                            deAllocationDate = deAllocRow(0)
                        Catch ex As Exception
                            deAllocationDate = Nothing
                        End Try
                        If deAllocationDate = Nothing Then
                            Continue For
                        ElseIf Format(allocationDate, "yyyy-MM-dd") <> Format(deAllocRow(1), "yyyy-MM-dd") Then
                            Continue For
                        ElseIf Format(deAllocationDate, "yyyy-MM-dd") = CDate("Jan 1, 1900") Then
                            de_alloc_without_visit = 0
                            lastAllocationDate = allocationDate
                        Else
                            de_alloc_without_visit = 1
                        End If
                    Next
                    If deAllocationDate = Nothing Then
                        deAllocationDate = CDate("Jan 1, 2100")
                    End If
                    'see if returned or put into trace or allocated while bailiff had case
                    'also look for removed agent - can ignore allocation if agent removed same day as allocation
                    Dim note3_dt As New DataTable
                    LoadDataTable2("DebtRecovery", "SELECT type, _createdDate " & _
                                                    "FROM note " & _
                                                   "WHERE (type = 'Cancelled' or type = 'Trace' or type = 'Allocated' or type = 'Removed agent')" & _
                                                  " AND debtorID = " & debtorid & _
                                              " ORDER BY _createdDate", note3_dt, False)
                    For Each note3row In note3_dt.Rows
                        noteText = note3row(0)
                        Dim noteDate As String = note3row(1)
                        If noteDate <= allocationDate Then
                            Continue For
                        End If
                        If noteText = "Trace" Then
                            trace_while_allocated = 1
                        End If
                        If noteText = "Cancelled" Then
                            return_while_allocated = 1
                        End If
                        If noteText = "Allocated" Then
                            If Format(noteDate, "yyyy-MMM-dd") = Format(allocationDate, "yyyy-MMM-dd") Then
                                'ignore allocation
                                lastAllocationDate = Nothing
                                Exit For
                            End If
                            de_alloc_without_visit = 1
                        End If
                        If noteText = "Removed agent" Then
                            If Format(noteDate, "yyyy-MMM-dd") = Format(allocationDate, "yyyy-MMM-dd") Then
                                'ignore allocation
                                lastAllocationDate = Nothing
                                Exit For
                            End If
                        End If
                    Next
                End If
            Next
        Next
        If visited = 1 Or lastAllocationDate <> Nothing Then
            If visited = 1 Then
                de_alloc_without_visit = 0
                trace_while_allocated = 0
                return_while_allocated = 0
            Else
                If de_alloc_without_visit = 0 Then
                    trace_while_allocated = 0
                    return_while_allocated = 0
                End If
            End If
            If de_alloc_without_visit = 99999 Then
                de_alloc_without_visit = 0
            End If
            If trace_while_allocated = 1 Or return_while_allocated = 1 Then
                de_alloc_without_visit = 1
            End If
            If allocationDate2Store = Nothing Then
                allocationDate2Store = lastAllocationDate
            End If
            If trace_while_allocated = 99999 Then
                trace_while_allocated = 0
            End If
            If return_while_allocated = 99999 Then
                return_while_allocated = 0
            End If
            upd_txt = "insert into Visit_stats (debtorID, " & _
                   "stage, days_to_first_visit, visited, allocation_date,de_alloc_without_visit, trace_while_allocated, return_while_allocated)" & _
                   "values (" & debtorid & ",'FVAN'," & days2FirstVisit & "," & visited & "," & _
                   "'" & allocationDate2Store & "'," & de_alloc_without_visit & "," & trace_while_allocated & "," & return_while_allocated & ")"
            update_sql(upd_txt)
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("FeesSQL")


        'first clear down table
        'type = "XX"
        'Dim upd_txt As String = "delete from Visit_stats " & _
        ' "where stage <> '" & type & "'"
        'update_sql(upd_txt)

        'van
        'get all cases allocated to van loaded since 1.6.2013


        Dim debtor_dt As New DataTable

        'get all cases loaded since 1.6.2013

        'RESET START DEBTOR NUMBER AND CLEAR DOWN TABLE

        startDebtorID = 7835288           '7449506
        LoadDataTable2("DebtRecovery", "SELECT _rowID, clientschemeID " & _
                                                "FROM debtor " & _
                                                "WHERE _rowid > " & startDebtorID & _
                                                " order by _rowid", debtor_dt, False)
        For Each Debtrow In debtor_dt.Rows
            'Dim ignore As Boolean
            Dim clientSchemeID As Integer = Debtrow.item(1)
            'check client scheme is branch 1
            Dim branchID As Integer
            Dim CSArray As Object
            CSArray = GetSQLResultsArray2("DebtRecovery", "SELECT branchID, clientID " & _
                                               "FROM clientScheme " & _
                                               "WHERE _rowid= " & clientSchemeID)
            branchID = CSArray(0)
            If branchID <> 1 Then
                Continue For
            End If
            'ignore test cases
            If CSArray(1) = 1 Or CSArray(1) = 2 Or CSArray(1) = 24 Then
                Continue For
            End If
            Dim debtorID As Integer = Debtrow.Item(0)

            run_fc(debtorID)
            run_van(debtorID)
            run_fvan(debtorID)
        Next
        'MsgBox("Completed")
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

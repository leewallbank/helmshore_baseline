﻿Imports CommonLibrary

Public Class clsReturnsData
    Private Debtor As New DataTable, MasterList As New DataTable, TempTable As New DataTable, AddressSource As New DataTable, Address As New DataTable, MergedSource As New DataTable, ContactSource As New DataTable, LastReturnTable As New DataTable ' LastRunTable added TS 12/Mar/2015. Request ref 44258
    Private DebtorDV As DataView, AddressDV As DataView
    Private ClientSchemeIDList As String = "3390, 3391, 3392, 3393, 3394, 3395, 3396"

    Public ReadOnly Property Debtors() As DataView
        Get
            Debtors = DebtorDV
        End Get
    End Property

    Public ReadOnly Property Addresses() As DataView
        Get
            Addresses = AddressDV
        End Get
    End Property

    Public ReadOnly Property LastReturnDetails() As LastReturnDetails  ' added TS 12/Mar/2015. Request ref 44285
        Get
            LastReturnDetails = New LastReturnDetails
            LastReturnDetails.FileSequence = LastReturnTable.Rows(0).Item("FileSequence")
            LastReturnDetails.DateTo = LastReturnTable.Rows(0).Item("DateTo")
        End Get
    End Property

    Public Sub New()
        ' The following column definitions are also used to pad fields in the output file, so this section must match the return file spec from student loans
        ' These definitions are reference throughout the app
        With Address.Columns

            .Add("DebtorID", Type.GetType("System.String"))
            .Item("DebtorID").ExtendedProperties.Add("MaxLength", 8) ' Changed to 8 from 7. TS 16/Dec/2014. Request ref 37895

            .Add("client_ref", Type.GetType("System.String"))
            .Item("client_ref").ExtendedProperties.Add("MaxLength", 13)

            .Add("defaultCourtCode", Type.GetType("System.String"))
            .Item("defaultCourtCode").ExtendedProperties.Add("MaxLength", 6)

            .Add("CustomerFullName", Type.GetType("System.String"))
            .Item("CustomerFullName").ExtendedProperties.Add("MaxLength", 68)

            .Add("ApartmentNumber", Type.GetType("System.String"))
            .Item("ApartmentNumber").ExtendedProperties.Add("MaxLength", 50)

            .Add("HouseName", Type.GetType("System.String"))
            .Item("HouseName").ExtendedProperties.Add("MaxLength", 50)

            .Add("HouseNumber", Type.GetType("System.String"))
            .Item("HouseNumber").ExtendedProperties.Add("MaxLength", 50)

            .Add("SubStreetName", Type.GetType("System.String"))
            .Item("SubStreetName").ExtendedProperties.Add("MaxLength", 50)

            .Add("Street", Type.GetType("System.String"))
            .Item("Street").ExtendedProperties.Add("MaxLength", 50)

            .Add("SubDistrict", Type.GetType("System.String"))
            .Item("SubDistrict").ExtendedProperties.Add("MaxLength", 50)

            .Add("District", Type.GetType("System.String"))
            .Item("District").ExtendedProperties.Add("MaxLength", 50)

            .Add("Town", Type.GetType("System.String"))
            .Item("Town").ExtendedProperties.Add("MaxLength", 50)

            .Add("County", Type.GetType("System.String"))
            .Item("County").ExtendedProperties.Add("MaxLength", 50)

            .Add("PostCode", Type.GetType("System.String"))
            .Item("PostCode").ExtendedProperties.Add("MaxLength", 8)

            .Add("Phone", Type.GetType("System.String"))
            .Item("Phone").ExtendedProperties.Add("MaxLength", 12)

            .Add("AssociateName", Type.GetType("System.String"))
            .Item("AssociateName").ExtendedProperties.Add("MaxLength", 50)

            .Add("DOB", Type.GetType("System.String"))
            .Item("DOB").ExtendedProperties.Add("MaxLength", 8)

            .Add("NI", Type.GetType("System.String"))
            .Item("NI").ExtendedProperties.Add("MaxLength", 12)
        End With

        TempTable.Columns.Add("client_ref", Type.GetType("System.String"))
    End Sub

    Public Sub GetDebtors(ByVal DateFrom As Date, ByVal DateTo As Date)
        Dim Sql As String
        '15.4.2014 JEB new join to pick up all notes not just max
        Try
            ' Get updated cases
            Sql = "SELECT d._rowID AS debtorID " & _
                  "     , d.client_ref " & _
                  "     , cs.defaultCourtCode " & _
                  "     , s.desc_long AS clientReturnNumber " & _
                  "     , n.text AS return_details " & _
                  "     , CAST(NULL AS DECIMAL(12,2)) AS arrange_amount " & _
                  "     , CAST(NULL AS SIGNED) AS arrange_interval " & _
                  "     , CAST(NULL AS DATETIME) AS arrange_next " & _
                  "     , CAST(NULL AS DECIMAL(12,2)) AS debtPaid " & _
                  "     , CAST(NULL AS DATETIME) AS GoneAwayDate " & _
                  "     , d.highCourtName AS AssociateName " & _
                  "     , d.judgmentDate AS AssociateDOB " & _
                  "     , d.highCourtActionNumber AS AssociateNI " & _
                  "     , d.empNI " & _
                  "FROM Debtor AS d " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "INNER JOIN CodeStatus AS s ON d.statusCode = s._rowid " & _
                  "INNER JOIN note AS n ON d._rowID = n.DebtorID " & _
            "INNER JOIN ( SELECT n_s.DebtorID " & _
                  "                  , MAX(n_s._rowID) AS LastNoteID " & _
                  "             FROM debtor AS d_s " & _
                  "             INNER JOIN note AS n_s ON d_s._rowID = n_s.DebtorID " & _
                  "             INNER JOIN clientscheme AS cs_s ON d_s.clientschemeID = cs_s._RowID " & _
                  "             WHERE n_s.type = 'Status changed' " & _
                  "               AND cs_s._rowID IN (" & ClientSchemeIDList & ") " & _
                  "               AND DATE(n_s._createdDate) >'" & DateFrom.AddDays(-1).ToString("yyyy-MM-dd") & "' " & _
                  "               AND DATE(n_s._createdDate) <'" & DateTo.AddDays(+1).ToString("yyyy-MM-dd") & "' " & _
                  "             GROUP BY n_s.DebtorID " & _
                  "           ) AS n_max ON n.DebtorID = n_max.DebtorID " & _
                  "                     AND n._rowID   = n_max.LastNoteID " & _
                  " WHERE cs._rowID IN (" & ClientSchemeIDList & ")" & _
                  "  AND s._rowID IN (35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51)"

            'jeb 17.4.2014 original date ranges
            '"               AND DATE(n_s._createdDate) BETWEEN '" & DateFrom.AddDays(-1).ToString("yyyy-MM-dd") & "' " & _
            '     "                                              AND '" & DateTo.AddDays(-1).ToString("yyyy-MM-dd") & "' " & _

            LoadDataTable("DebtRecovery", Sql, Debtor, False)

            ' Get closed cases next.
            ' MOD TS 10/Nov/2016 added check for note. Request ref 96831
            Sql = "SELECT d._rowID AS debtorID " & _
                  "     , d.client_ref " & _
                  "     , cs.defaultCourtCode " & _
                  "     , CASE WHEN d.status = 'S' THEN 7 ELSE csr.clientReturnNumber END AS clientReturnNumber " & _
                  "     , d.return_details " & _
                  "     , NULL AS arrange_amount " & _
                  "     , NULL AS arrange_interval " & _
                  "     , NULL AS arrange_next " & _
                  "     , d.debtPaid " & _
                  "     , STR_TO_DATE(REPLACE(SUBSTRING_INDEX(n.text, ' ', -1), '.', '/'), '%d/%m/%Y' ) AS GoneAwayDate " & _
                  "     , NULL AS AssociateName " & _
                  "     , NULL AS AssociateDOB " & _
                  "     , NULL AS AssociateNI " & _
                  "     , NULL AS empNI " & _
                  "FROM Debtor AS d " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "INNER JOIN remit AS r ON d.return_remitid = r._rowid " & _
                  "LEFT JOIN ClientSchemeReturn AS csr ON d.ClientSchemeID = csr.ClientSchemeid " & _
                  "                                   AND d.return_codeid  = csr.returnid " & _
                  "LEFT JOIN ( SELECT n_s.DebtorID " & _
                  "                 , n_s.text " & _
                  "            FROM note AS n_s " & _
                  "            INNER JOIN ( SELECT n_m_s.DebtorID " & _
                  "                              , MAX(n_m_s._rowID) AS _rowID " & _
                  "                         FROM debtor AS d_m_s " & _
                  "                         INNER JOIN clientscheme AS cs_m_s ON d_m_s.ClientSchemeID = cs_m_s._rowID " & _
                  "                         INNER JOIN note AS n_m_s ON d_m_s._rowID = n_m_s._rowID " & _
                  "                         WHERE cs_m_s._rowID IN (" & ClientSchemeIDList & ") " & _
                  "                           AND d_m_s.status_open_closed = 'C' " & _
                  "                           AND n_m_s.type = 'Cancelled' " & _
                  "                           AND n_m_s.text LIKE '%Re: Gone Away - %' " & _
                  "                         GROUP BY n_m_s.DebtorID " & _
                  "                       ) AS n_m ON n_s.DebtorID = n_m.DebtorID " & _
                  "                               AND n_s._rowID   = n_m._rowID" & _
                  "          ) AS n ON n.DebtorID = d._rowID " & _
                  "WHERE cs._rowID IN (" & ClientSchemeIDList & ") " & _
                  "  AND d.status_open_closed = 'C' " & _
                  "  AND DATE(r.date) BETWEEN '" & DateFrom.ToString("yyyy-MM-dd") & "' " & _
                  "                       AND '" & DateTo.ToString("yyyy-MM-dd") & "' " & _
                  "  AND NOT EXISTS ( SELECT 1 " & _
                  "                   FROM note AS n_s " & _
                  "                   WHERE n_s.DebtorID = d._rowID " & _
                  "                     AND n_s.text = 'client has requested we close file and do not report in closure report' " & _
                  "                 )"


            LoadDataTable("DebtRecovery", Sql, Debtor, True)

            ' Add ID's of each address / contact detail change so these are looped through 

            For Each Change As DataRow In Addresses.Table.Rows
                Debtor.Rows.Add({Change(0), Change(1), Change(2), 8}) ' 8 is the return code for addresses
            Next Change

            ' Add arrangement changes status
            Sql = "SELECT d._rowID AS debtorID " & _
                  "     , d.client_ref " & _
                  "     , cs.defaultCourtCode " & _
                  "     , 65 AS clientReturnNumber " & _
                  "     , NULL AS return_details " & _
                  "     , d.arrange_amount " & _
                  "     , d.arrange_interval " & _
                  "     , d.arrange_next " & _
                  "     , NULL AS debtPaid " & _
                  "     , NULL AS GoneAwayDate " & _
                  "     , NULL AS AssociateName " & _
                  "     , NULL AS AssociateDOB " & _
                  "     , NULL AS AssociateNI " & _
                  "     , NULL AS empNI " & _
                  "FROM Debtor AS d " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "INNER JOIN ( SELECT DISTINCT d_s._rowID " & _
                  "               FROM debtor AS d_s " & _
                  "               INNER JOIN note AS n_s ON d_s._rowID = n_s.DebtorID " & _
                  "               INNER JOIN clientscheme AS cs_s ON d_s.ClientSchemeID = cs_s._RowID " & _
                  "               WHERE n_s.type = 'Arrangement' " & _
                  "                 AND cs_s._rowID IN (" & ClientSchemeIDList & ") " & _
                  "                 AND DATE(n_s._createdDate) BETWEEN '" & DateFrom.AddDays(-1).ToString("yyyy-MM-dd") & "' " & _
                  "                                                AND '" & DateTo.AddDays(-1).ToString("yyyy-MM-dd") & "' " & _
                  "               GROUP BY n_s.DebtorID " & _
                  "             ) AS d_a ON d._rowID = d_a._rowID " & _
                  "WHERE cs._rowID IN (" & ClientSchemeIDList & ") " & _
                  "  AND d.status = 'A'" & _
                  "  AND d.arrange_amount > 0"

            LoadDataTable("DebtRecovery", Sql, Debtor, True)

            DebtorDV = New DataView(Debtor)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub GetAddresses(ByVal DateFrom As Date, ByVal DateTo As Date)
        Dim OutputDataRow As DataRow
        Dim FullName As String, AddressLine() As String, AddressWord() As String, Sql As String

        ' MOD 16/Jul/2014 - Blank out names and NI on address changes and names on contact detail changes. Request ref 24404

        Try
            ' Load address changes
            Sql = "SELECT d._rowID AS debtorID " & _
                  "     , d.client_ref " & _
                  "     , cs.defaultCourtCode " & _
                  "     , '' AS name_title " & _
                  "     , '' AS name_fore " & _
                  "     , '' AS name_sur " & _
                  "     , IFNULL(d.address,'')  AS address " & _
                  "     , '' AS add_phone " & _
                  "     , '' AS AssociateName " & _
                  "     , '' AS dateOfBirth " & _
                  "     , '' AS empNI " & _
                  "FROM debtor AS d " & _
                  "INNER JOIN note AS n ON d._rowID = n.DebtorID " & _
                  "INNER JOIN clientscheme AS cs ON d.clientschemeID = cs._RowID " & _
                  "INNER JOIN scheme AS s ON cs.schemeID = s._RowID " & _
                  "INNER JOIN ( SELECT n_s.DebtorID " & _
                  "                  , MAX(n_s._rowID) AS LastNoteID " & _
                  "             FROM debtor AS d_s " & _
                  "             INNER JOIN note AS n_s ON d_s._rowID = n_s.DebtorID " & _
                  "             INNER JOIN clientscheme AS cs_s ON d_s.clientschemeID = cs_s._RowID " & _
                  "             WHERE n_s.type = 'Address' " & _
                  "               AND n_s.text LIKE 'changed from:%'" & _
                  "               AND cs_s._rowID IN (" & ClientSchemeIDList & ") " & _
                  "               AND DATE(n_s._createdDate) BETWEEN '" & DateFrom.ToString("yyyy-MM-dd") & "' " & _
                  "                                              AND '" & DateTo.ToString("yyyy-MM-dd") & "' " & _
                  "             GROUP BY n_s.DebtorID " & _
                  "           ) AS n_max ON n.DebtorID = n_max.DebtorID " & _
                  "                     AND n._rowID   = n_max.LastNoteID " & _
                  "LEFT JOIN note AS nt ON n.debtorID           = nt.debtorID " & _
                  "                    AND 'Off trace'          = nt.type " & _
                  "                    AND DATE(n._createdDate) = DATE(nt._createdDate)" & _
                  "WHERE n.type = 'Address' " & _
                  "  AND n.text LIKE 'changed from:%'" & _
                  "  AND cs._rowID IN (" & ClientSchemeIDList & ")"

            LoadDataTable("DebtRecovery", Sql, AddressSource, False)

            ' Load contact detail changes
            Sql = "SELECT sd.DebtorID " & _
                  "     , sd.client_ref " & _
                  "     , cs.defaultCourtCode " & _
                  "     , '' AS name_title " & _
                  "     , '' AS name_fore " & _
                  "     , '' AS name_sur " & _
                  "     , '' AS address " & _
                  "     , ISNULL(d.add_phone,'') AS add_phone " & _
                  "     , '' AS AssociateName " & _
                  "     , ISNULL(CONVERT(VARCHAR,d.dateOfBirth, 103),'') AS dateOfBirth " & _
                  "     , ISNULL(d.empNI,'') AS empNI " & _
                  "FROM rpt.UUDebtor AS d " & _
                  "INNER JOIN stg.Debtor AS sd ON d.DebtorID = sd.DebtorID " & _
                  "INNER JOIN stg.ClientScheme AS cs ON cs.ClientSchemeID = sd.ClientSchemeID " & _
                  "  AND CAST(FLOOR(CAST(d.BuildDate AS FLOAT)) AS DATETIME) BETWEEN '" & DateFrom.ToString("yyyy-MMM-dd") & "' " & _
                  "                                                              AND '" & DateTo.ToString("yyyy-MMM-dd") & "' " & _
                  "  AND sd.ClientSchemeID IN (" & ClientSchemeIDList & ")"

            LoadDataTable("StudentLoans", Sql, ContactSource, False)

            MergeUpdates()

            Address.Clear()

            For Each SourceDataRow As DataRow In MergedSource.Rows
                OutputDataRow = Address.Rows.Add

                ' The following section performs the mapping from DebtRecovery fields to the output file

                OutputDataRow("DebtorID") = SourceDataRow("DebtorID")
                OutputDataRow("client_ref") = SourceDataRow("client_ref")
                OutputDataRow("defaultCourtCode") = SourceDataRow("defaultCourtCode")

                FullName = SourceDataRow("name_title")
                If SourceDataRow("name_title") <> "" Then FullName &= " "
                FullName &= SourceDataRow("name_fore")
                If SourceDataRow("name_fore") <> "" Then FullName &= " "
                FullName &= SourceDataRow("name_sur")

                OutputDataRow("CustomerFullName") = FullName

                ' Create an array of address lines. Note DebtRecovery sometimes just uses a line feed for new line with no carriage return
                If SourceDataRow("Address") <> "" Then
                    AddressLine = Replace(LTrim(SourceDataRow("Address")), vbCr, "").ToString.TrimEnd(vbLf).Split(vbLf)
                Else
                    AddressLine = Nothing
                End If

                ' DebtRecovery stored the address in one field. The following logic is to break it down into multiple
                If Not IsNothing(AddressLine) Then

                    AddressWord = AddressLine(0).ToString.Split(" ") ' Split first line into words to get house number and flat details

                    ' If the second word is a number assume a flat or appartment or if addressline contains the word flat assume a flat
                    If (UBound(AddressWord) > 1 AndAlso System.Text.RegularExpressions.Regex.Match(AddressWord(1), "\d").Success) Or AddressWord(0).ToUpper.Contains("FLAT") Then
                        OutputDataRow("ApartmentNumber") = AddressWord(0)

                        If UBound(AddressWord) > 0 Then
                            OutputDataRow("ApartmentNumber") &= " " & AddressWord(1) ' Append the first two words to ApartmentNumber
                        End If

                        ' If the flat and its number are the only line contents, assume the next line is the road and rebuild the array
                        If UBound(AddressWord) < 2 Then
                            OutputDataRow("Street") = AddressLine(1)
                            Dim objArrayList As New ArrayList(AddressLine)
                            objArrayList.RemoveAt(1)
                            AddressLine = objArrayList.ToArray(GetType(String))
                        Else
                            For WordLoop As Integer = 2 To UBound(AddressWord) ' 
                                OutputDataRow("Street") &= " " & AddressWord(WordLoop) ' Append the rest to Street
                            Next WordLoop
                        End If

                        If Not IsDBNull(OutputDataRow("Street")) Then OutputDataRow("Street") = LTrim(OutputDataRow("Street"))

                        If System.Text.RegularExpressions.Regex.Match(OutputDataRow("Street"), "\d").Success Then
                            ' If the first word is a number assume a normal house number
                            OutputDataRow("HouseNumber") = Left(OutputDataRow("Street"), OutputDataRow("Street").IndexOf(" "))
                            OutputDataRow("Street") = Right(OutputDataRow("Street"), OutputDataRow("Street").Length - OutputDataRow("Street").IndexOf(" ") - 1)
                        End If

                    ElseIf System.Text.RegularExpressions.Regex.Match(AddressWord(0), "\d").Success Then
                        ' If the first word is a number assume a normal house number
                        If AddressLine(0).IndexOf(" ") > 0 Then ' Added TS 16/Jul/2013 Portal task ref 16226
                            ' Original logic
                            OutputDataRow("HouseNumber") = Left(AddressLine(0), AddressLine(0).IndexOf(" "))
                            OutputDataRow("Street") = Right(AddressLine(0), AddressLine(0).Length - AddressLine(0).IndexOf(" ") - 1)
                        Else
                            OutputDataRow("HouseNumber") = AddressLine(0)
                        End If
                    Else

                        ' Nothing captured above
                        OutputDataRow("Street") = AddressLine(0)
                    End If

                    ' put the lines in the address into the different fields. The logic used differs based on the number of lines in the address
                    Select Case UBound(AddressLine)
                        Case 0
                            ' no need to do anything as the single line will go into street and postcode
                        Case 1
                            ' no need to do anything as the first line will go into street and the last into postcode
                        Case 2
                            OutputDataRow("Town") = AddressLine(1) ' MOD TS 30/Oct/2012 was county
                        Case 3
                            OutputDataRow("Town") = AddressLine(1)
                            OutputDataRow("County") = AddressLine(2)
                        Case 4
                            OutputDataRow("District") = AddressLine(1)
                            OutputDataRow("Town") = AddressLine(2)
                            OutputDataRow("County") = AddressLine(3)
                        Case Else
                            For LineCount As Integer = 1 To UBound(AddressLine) - 3
                                If Not IsDBNull(OutputDataRow("District")) Then OutputDataRow("District") += " "
                                OutputDataRow("District") += AddressLine(LineCount) ' Append the rest to District
                            Next LineCount

                            OutputDataRow("Town") = AddressLine(UBound(AddressLine) - 2)
                            OutputDataRow("County") = AddressLine(UBound(AddressLine) - 1)
                    End Select

                    OutputDataRow("PostCode") = AddressLine(UBound(AddressLine)).Trim
                End If
                OutputDataRow("Phone") = SourceDataRow("add_phone").ToString.Replace(" ", "")

                OutputDataRow("AssociateName") = SourceDataRow("AssociateName")

                If IsDBNull(SourceDataRow("dateOfBirth")) Then
                    OutputDataRow("DOB") = ""
                Else
                    If SourceDataRow("dateOfBirth") = "" Then
                        OutputDataRow("DOB") = ""
                    Else
                        OutputDataRow("DOB") = CDate(SourceDataRow("dateOfBirth")).ToString("yyyyMMdd")
                    End If
                End If

                OutputDataRow("NI") = SourceDataRow("empNI")

                For ColIndex As Integer = 0 To 15
                    If IsDBNull(OutputDataRow.Item(ColIndex)) Then OutputDataRow.Item(ColIndex) = ""
                Next ColIndex

                '       End If

            Next SourceDataRow

            AddressDV = New DataView(Address)
            AddressDV.AllowNew = False

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub

    'Public Function GetNextFileSequence() As String ' commented out TS 12/Mar/2015. Request ref 44285
    '    ' GetNextFileSequence = GetSQLResults("FeesSQL", "SELECT ISNULL(MAX(FileSequence),-1)+1 FROM dbo.UUReturn") commented out TS 05/Aug/2014. Request ref 27729
    '    GetNextFileSequence = GetSQLResults("FeesSQL", "SELECT TOP 1 ISNULL(FileSequence,-1) + 1 FROM dbo.UUReturn ORDER BY RowID DESC")
    'End Function

    Public Sub GetLastRunDetails() ' added TS 12/Mar/2015. Request ref 44285

        LoadDataTable("FeesSQL", "SELECT TOP 1 FileSequence, DateFrom, DateTo FROM dbo.UUReturn ORDER BY RowID DESC", LastReturnTable)

    End Sub

    Public Sub SetLastFileSequence(ByVal FileSequence As String, ByVal CasesLoaded As Integer, ByVal DateFrom As Date, ByVal DateTo As Date)
        ' Added DateFrom and DateTo TS 11/Mar/2015. Request ref 44285
        ExecStoredProc("FeesSQL", " EXEC dbo.SetUUReturn " & FileSequence & ", " & CasesLoaded.ToString & ", '" & DateFrom.ToString("dd/MMM/yyyy") & "', '" & DateTo.ToString("dd/MMM/yyyy") & "'")
    End Sub

    Private Sub MergeUpdates()
        Try
            Dim AddressDV As DataView = New DataView(AddressSource)
            Dim ContactDV As DataView = New DataView(ContactSource)

            MergedSource = AddressSource.Clone() ' Clone the record structure of one of the source files for the merge output
            TempTable.Clear()

            ' Append both sets of headers to temp table
            For Each dr As DataRow In AddressSource.Rows
                TempTable.Rows.Add(dr("client_ref"))
            Next dr

            For Each dr As DataRow In ContactSource.Rows
                TempTable.Rows.Add(dr("client_ref"))
            Next dr

            ' Get distinct line headers
            MasterList = TempTable.DefaultView.ToTable(True)

            ' Build the merged table
            For Each dr As DataRow In MasterList.Rows
                AddressDV.RowFilter = ("client_ref = '" & dr("client_ref") & "'")
                ContactDV.RowFilter = ("client_ref = '" & dr("client_ref") & "'")

                If AddressDV.Count = 0 And ContactDV.Count > 0 Then
                    MergedSource.ImportRow(ContactDV.Item(0).Row)
                ElseIf ContactDV.Count = 0 And AddressDV.Count > 0 Then
                    MergedSource.ImportRow(AddressDV.Item(0).Row)
                Else ' Assume data in both as the client ref must have been in at least one of the source tables
                    'MergedSource.Rows.Add({AddressDV.Item(0).Item(0), _
                    '                       AddressDV.Item(0).Item(1), _
                    '                       AddressDV.Item(0).Item(2), _
                    '                       AddressDV.Item(0).Item(3), _
                    '                       AddressDV.Item(0).Item(4), _
                    '                       AddressDV.Item(0).Item(5), _
                    '                       AddressDV.Item(0).Item(6), _
                    '                       AddressDV.Item(0).Item(7), _
                    '                       AddressDV.Item(0).Item(8), _
                    '                       ContactDV.Item(0).Item(9), _
                    '                       ContactDV.Item(0).Item(10)})
                    MergedSource.Rows.Add({AddressDV.Item(0).Item(0), _
                                          AddressDV.Item(0).Item(1), _
                                          AddressDV.Item(0).Item(2), _
                                          AddressDV.Item(0).Item(3), _
                                          AddressDV.Item(0).Item(4), _
                                          AddressDV.Item(0).Item(5), _
                                          AddressDV.Item(0).Item(6), _
                                          ContactDV.Item(0).Item(7), _
                                          ContactDV.Item(0).Item(8), _
                                          ContactDV.Item(0).Item(9), _
                                          ContactDV.Item(0).Item(10)})
                End If

            Next dr

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
End Class

Public Class LastReturnDetails ' added TS 12/Mar/2015. Request ref 44285
    Public Property FileSequence As Integer
    Public Property DateTo As Date
End Class
﻿Public Class Form1
    Dim upd_txt As String
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        ConnectDb2Fees("FeesSQL")
        'first clear down table
        Dim startdate As Date = CDate("Jun 1, 2012 00:00:00")
        Dim enddate As Date = DateAdd(DateInterval.Month, 1, startdate)
        'upd_txt = "delete from MonthlyArrangements " & _
        '            "where start_date < '" & enddate.ToString("dd/MMM/yyyy") & "'"
        'update_sql(upd_txt)
        'jan 12 6553423
        'dec 13 8373975
        'nov 13 8149165
        'oct 13 8010333
        'sep 13 7888166
        'Aug 13 7779545
        Dim startDebtorID As Integer = 10


        ConnectDb2("DebtRecovery")
        Dim debtor_dt As New DataTable
        LoadDataTable2("DebtRecovery", "SELECT _rowID, clientschemeID, return_date, debt_balance, _createdDate " & _
                                               "FROM debtor " & _
                                               "WHERE (status_open_closed = 'O'" & _
                                               " or (status_open_closed = 'C' and " & _
                                               "  return_date >= '" & startdate.ToString("dd/MMM/yyyy") & "'))" & _
                                              " AND arrange_started is not null" & _
                                              " and _rowid > " & startDebtorID & _
                                              " order by _rowid", debtor_dt, False)
        For Each Debtrow In debtor_dt.Rows
            Dim debtorID As Integer = Debtrow.Item(0)
            Dim createdDate As Date = Debtrow.item(4)
            If createdDate > enddate Then
                Exit For
            End If
            Try
                Dim returnDate As Date = Debtrow.item(2)
                If returnDate < startdate Then
                    Continue For
                End If
            Catch ex As Exception

            End Try
           
            Dim branchID As Integer
            Dim CSArray As Object
            Dim clientSchemeID As Integer = Debtrow.item(1)

            CSArray = GetSQLResultsArray2("DebtRecovery", "SELECT branchID, clientID " & _
                                               "FROM clientScheme " & _
                                               "WHERE _rowid= " & clientSchemeID)
            branchID = CSArray(0)
            If branchID <> 2 And branchID <> 8 And branchID <> 12 Then
                Continue For
            End If
            'ignore test cases
            If CSArray(1) = 1 Or CSArray(1) = 2 Or CSArray(1) = 24 Then
                Continue For
            End If

            Dim note_dt As New DataTable
            Dim arrFound As Boolean = False
            Dim debtvalue As Decimal
            Dim balance As Decimal = Debtrow.item(3)
            'see if case in arrangement at month start
            LoadDataTable2("DebtRecovery", "SELECT type, _createdDate " & _
                                           "FROM note " & _
                                           "WHERE (type = 'Arrangement' " & _
                                           " OR type = 'Broken' or type = 'Clear arrange')" & _
                                     " AND debtorID = " & debtorID & _
                                     " order by _rowID", note_dt, False)
            For Each noterow In note_dt.Rows
                Dim type As String = noterow.item(0)
                Dim noteDate As Date = noterow.item(1)

                If type = "Arrangement" Then
                    If noteDate < startdate Then
                        arrFound = True
                        Continue For
                    End If
                    If Format(noteDate, "dd/MMM/yyyy") = Format(startdate, "dd/MMM/yyyy") Then
                        arrFound = True
                        debtvalue = get_value(debtorID, noteDate, balance)
                        upd_txt = "insert into MonthlyArrangements (debtorID, " & _
                "type, start_date,start_value)" & _
                "values (" & debtorID & ",'A','" & noteDate & "'," & debtvalue & ")"
                        update_sql(upd_txt)
                        Continue For
                    End If
                    If noteDate > enddate Then
                        Exit For
                    End If
                    debtvalue = get_value(debtorID, noteDate, balance)
                    If debtvalue > 0 Then
                        upd_txt = "insert into MonthlyArrangements (debtorID, " & _
                "type, start_date,start_value)" & _
                "values (" & debtorID & ",'A','" & noteDate & "'," & debtvalue & ")"
                        update_sql(upd_txt)
                    End If
                Else
                    If noteDate < startdate Then
                        arrFound = False
                        Continue For
                    End If
                    If noteDate > enddate Then
                        Exit For
                    End If
                    debtvalue = get_value(debtorID, noteDate, balance)
                    If debtvalue > 0 Then
                        'save broken value
                        upd_txt = "insert into MonthlyArrangements (debtorID, " & _
                "type, start_date,start_value)" & _
                "values (" & debtorID & ",'B','" & noteDate & "'," & debtvalue & ")"
                        update_sql(upd_txt)
                    End If
                End If
            Next
            If arrFound Then
                debtvalue = get_value(debtorID, startdate, balance)
                If debtvalue > 0 Then
                    upd_txt = "insert into MonthlyArrangements (debtorID, " & _
                "type, start_date,start_value)" & _
                "values (" & debtorID & ",'F','" & startdate & "'," & debtvalue & ")"
                    update_sql(upd_txt)
                End If
            End If
        Next
        MsgBox("Completed")
        Me.Close()
    End Sub
    Private Function get_value(ByVal debtorid As Integer, ByVal notedate As Date, ByVal balance As Decimal) As Decimal
        'get outstanding balance at time of date
        Dim balatdate As Decimal
        Dim paidSince As Decimal = 0
        Dim feesSince As Decimal = 0
        'get balance as at date
        'Try
        '    paidSince = GetSQLResults2("DebtRecovery", "select sum(amount) from payment" & _
        '                         " where debtorID = " & debtorid & _
        '                         " and (status = 'W' or status = 'R')" & _
        '                         " AND _createdDate > '" & notedate.ToString("dd/MMM/yyyy") & "'")
        'Catch ex As Exception

        'End Try
        Dim dt As New DataTable
        LoadDataTable2("DebtRecovery", "select amount, _createdDate from payment" & _
                                 " where debtorID = " & debtorid & _
                                 " and (status = 'W' or status = 'R')" , dt, False)
       
        Dim row As DataRow
        For Each row In dt.Rows
            Dim amt As Decimal = row.Item(0)
            Dim createddate As Date = row.Item(1)
            If Format(createddate, "yyyy-MM-dd") >= Format(notedate, "yyyy-MM-dd") Then
                paidSince += amt
            End If
        Next
        'Try
        '    feesSince = GetSQLResults2("DebtRecovery", "select sum(fee_amount) from fee" & _
        '                         " where debtorID = " & debtorid & _
        '                         " AND feeWhoPays = 'D'" & _
        '                         " AND _createdDate > '" & notedate.ToString("dd/MMM/yyyy") & "'")
        'Catch ex As Exception

        'End Try
        Dim dt2 As New DataTable
        LoadDataTable2("DebtRecovery", "select fee_amount, _createdDate from fee" & _
                                 " where debtorID = " & debtorid & _
                                   " AND feeWhoPays = 'D'", dt2, False)

        Dim row2 As DataRow
        For Each row2 In dt2.Rows
            Dim amt As Decimal = row2.Item(0)
            Dim createddate As Date = row2.Item(1)
            If Format(createddate, "yyyy-MM-dd") >= Format(notedate, "yyyy-MM-dd") Then
                feesSince += amt
            End If
        Next
        balatdate = balance - feesSince + paidSince
        'If balatdate = 0 Then
        '    MsgBox("Bal = 0 on case = " & debtorid)
        'End If
        Return (balatdate)
    End Function
End Class

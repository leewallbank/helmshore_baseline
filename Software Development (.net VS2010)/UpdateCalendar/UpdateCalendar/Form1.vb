Public Class Form1

    

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'FeesSQLDataSet.Calendar' table. You can move, or remove it, as needed.
        'Me.CalendarTableAdapter.Fill(Me.FeesSQLDataSet.Calendar)

    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        Dim run_date As Date = CDate("January 1, 2018")
        Dim end_date As Date = CDate("December 31, 2018")
        Dim day, month, year As Integer
        While run_date <= end_date
            day = Format(run_date, "dd")
            month = Format(run_date, "MM")
            year = Format(run_date, "yyyy")
            Try
                Me.CalendarTableAdapter.InsertQuery(day, month, year)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            run_date = DateAdd(DateInterval.Day, 1, run_date)
        End While
        MsgBox("Calendar updated")
    End Sub
End Class

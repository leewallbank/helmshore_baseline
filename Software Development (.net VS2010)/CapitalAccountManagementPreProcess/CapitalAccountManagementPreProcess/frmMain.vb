﻿Imports CommonLibrary
Imports System.IO
Imports System.Text.RegularExpressions

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String

    Private Const Separator As String = "|"


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        btnViewInputFile.Enabled = False
        btnViewLogFile.Enabled = False
        btnViewOutputFile.Enabled = False

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        'Try
        Dim FileDialog As New OpenFileDialog

        Dim NewDebtNotes As String = "", NewDebtLine(44) As String
        Dim AuditLog As String
        Dim FileContents(,) As String, ErrorLog As String = ""
        Dim NewDebtFile As String = ""
        Dim NewDebtSumm(1) As Decimal ' 0 Case count, 1 Total balance
        Dim TotalIntChargesColPos As Integer = -1

        btnViewInputFile.Enabled = False
        btnViewLogFile.Enabled = False
        btnViewOutputFile.Enabled = False

        FileDialog.Filter = "New business files|*.xls;*.xlsx|All files (*.*)|*.*"
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)

        Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf
        Dim OutputFileName As String = InputFilePath & FileName & "_Preprocessed.txt"

        If File.Exists(OutputFileName) Then File.Delete(OutputFileName)
        If File.Exists(InputFilePath & FileName & "_Audit.txt") Then File.Delete(InputFilePath & FileName & "_Audit.txt")
        If File.Exists(InputFilePath & FileName & "_Error.txt") Then File.Delete(InputFilePath & FileName & "_Error.txt")

        lblReadingFile.Visible = True
        Application.DoEvents()
        FileContents = InputFromExcel(FileDialog.FileName, "TMS")

        lblReadingFile.Visible = False

        ProgressBar.Maximum = UBound(FileContents) + 2  ' plus audit file and output

        For RowNum = 1 To UBound(FileContents)

            Application.DoEvents() ' without this line, the button disappears until processing is complete
            ProgressBar.Value = RowNum
            NewDebtNotes = ""

            If String.IsNullOrEmpty(FileContents(RowNum, 0)) Then Continue For

            ' Strip out any separators in the data
            For ColNum As Integer = 0 To UBound(FileContents, 2)
                If Not IsNothing(FileContents(RowNum, ColNum)) Then
                    FileContents(RowNum, ColNum) = FileContents(RowNum, ColNum).Replace(Separator, "")
                Else
                    FileContents(RowNum, ColNum) = ""
                End If

            Next ColNum

            ' Read  columns
            For ColNum As Integer = 0 To 44
                Select Case ColNum
                    Case 5
                        ' Concatenate address fields
                        NewDebtLine(5) = ConcatFields({ConcatFields({FileContents(RowNum, 5), FileContents(RowNum, 6)}, " "), FileContents(RowNum, 7), FileContents(RowNum, 8), FileContents(RowNum, 9), FileContents(RowNum, 10)}, ",")

                    Case 6 To 10
                        ' leave these blank

                    Case 12, 22
                        If Not String.IsNullOrEmpty(FileContents(RowNum, ColNum)) Then
                            If IsDate(FileContents(RowNum, ColNum)) Then
                                NewDebtLine(ColNum) = CDate(FileContents(RowNum, ColNum)).ToString("dd/MM/yyyy")
                            ElseIf IsNumeric(FileContents(RowNum, ColNum)) Then
                                NewDebtLine(ColNum) = DateTime.FromOADate(CInt(FileContents(RowNum, ColNum))).ToString("dd/MM/yyyy")
                            Else
                                ErrorLog &= "Invalid date at line number " & (RowNum + 1).ToString & ", column " & (ColNum + 1).ToString & vbCrLf
                            End If
                        End If
                    Case Else
                        NewDebtLine(ColNum) = FileContents(RowNum, ColNum)
                End Select
            Next ColNum

            ' Now build up notes
            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 18), "Residence Type", ";"))
            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 19), "Employment Status", ";"))
            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 23), "Case Age", ";"))
            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 25), "Days Since Last Payment", ";"))
            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 26), "Days Since Account Opened", ";"))
            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 27), "Last Returned Date", ";"))
            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 28), "Last Paid", ";"))

            If Not String.IsNullOrEmpty(FileContents(RowNum, 29)) And IsNumeric(FileContents(RowNum, 29)) Then
                NewDebtNotes &= String.Join("", ToNote(CDec(FileContents(RowNum, 29)).ToString("F2"), "Last Paid Amount", ";"))
            Else
                ErrorLog &= "Invalid value at line number " & (RowNum + 1).ToString & ", column 30" & vbCrLf
            End If

            If Not String.IsNullOrEmpty(FileContents(RowNum, 30)) And IsNumeric(FileContents(RowNum, 30)) Then
                NewDebtNotes &= String.Join("", ToNote(CDec(FileContents(RowNum, 30)).ToString("F2"), "Original Amount", ";"))
            Else
                ErrorLog &= "Invalid value at line number " & (RowNum + 1).ToString & ", column 31" & vbCrLf
            End If

            If Not String.IsNullOrEmpty(FileContents(RowNum, 31)) And IsNumeric(FileContents(RowNum, 31)) Then
                NewDebtNotes &= String.Join("", ToNote(CDec(FileContents(RowNum, 31)).ToString("F2"), "Surcharge", ";"))
            Else
                ErrorLog &= "Invalid value at line number " & (RowNum + 1).ToString & ", column 32" & vbCrLf
            End If

            If Not String.IsNullOrEmpty(FileContents(RowNum, 32)) And IsNumeric(FileContents(RowNum, 32)) Then
                NewDebtNotes &= String.Join("", ToNote(CDec(FileContents(RowNum, 32)).ToString("F2"), "Total", ";"))
            Else
                ErrorLog &= "Invalid value at line number " & (RowNum + 1).ToString & ", column 33" & vbCrLf
            End If

            If Not String.IsNullOrEmpty(FileContents(RowNum, 33)) And IsNumeric(FileContents(RowNum, 33)) Then
                NewDebtNotes &= String.Join("", ToNote(CDec(FileContents(RowNum, 33)).ToString("F2"), "Paid", ";"))
            Else
                ErrorLog &= "Invalid value at line number " & (RowNum + 1).ToString & ", column 34" & vbCrLf
            End If

            If Not String.IsNullOrEmpty(FileContents(RowNum, 34)) And IsNumeric(FileContents(RowNum, 34)) Then
                NewDebtNotes &= String.Join("", ToNote(CDec(FileContents(RowNum, 34)).ToString("F2"), "Face Value", ";"))
            Else
                ErrorLog &= "Invalid value at line number " & (RowNum + 1).ToString & ", column 35" & vbCrLf
            End If

            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 35), "Cheque Count", ";"))
            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 37), "Last RPC", ";"))
            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 38), "Previous DCA", ";"))
            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 39), "Last Payment Method", ";"))
            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 39), "Creditor", ";"))

            If Not String.IsNullOrEmpty(FileContents(RowNum, 42)) And IsNumeric(FileContents(RowNum, 42)) Then
                NewDebtSumm(1) += CDec(FileContents(RowNum, 42))
                NewDebtNotes &= String.Join("", ToNote(CDec(FileContents(RowNum, 42)).ToString("F2"), "Updated Balance", ";"))
            Else
                ErrorLog &= "Invalid value at line number " & (RowNum + 1).ToString & ", column 43" & vbCrLf
            End If

            If (Not String.IsNullOrEmpty(FileContents(RowNum, 43)) And IsNumeric(FileContents(RowNum, 43))) Then
                NewDebtNotes &= String.Join("", ToNote(CDec(FileContents(RowNum, 43)).ToString("F2"), "Paid to previous DCA", ";"))
            ElseIf FileContents(RowNum, 43) = "-" Then
                NewDebtNotes &= String.Join("", ToNote("0.00", "Paid to previous DCA", ";"))
            Else
                ErrorLog &= "Invalid value at line number " & (RowNum + 1).ToString & ", column 44" & vbCrLf
            End If

            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 44), "Litigation", ";"))
            NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 45), "Client Notes", ";"))

            NewDebtFile = Join(NewDebtLine, Separator) & Separator & NewDebtNotes & vbCrLf

            AppendToFile(OutputFileName, NewDebtFile)

            NewDebtSumm(0) += 1

        Next RowNum

        ' Update audit log
        AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
        AuditLog &= "Number of new cases: " & NewDebtSumm(0).ToString & vbCrLf
        AuditLog &= "Total balance of new cases: " & NewDebtSumm(1).ToString & vbCrLf

        WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

        ProgressBar.Value += 1

        If ErrorLog <> "" Then
            WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
            MsgBox("Errors found.", vbCritical, Me.Text)
        End If

        ProgressBar.Value = ProgressBar.Maximum

        MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

        btnViewInputFile.Enabled = True
        btnViewOutputFile.Enabled = True
        btnViewLogFile.Enabled = True

        ProgressBar.Value = 0

        'Catch ex As Exception
        '    HandleException(ex)
        'End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try
            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try
            If File.Exists(InputFilePath & FileName & "_Preprocessed.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Preprocessed.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try
            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    'Private Function InputFromExcel(ByVal FileName As String) As String(,)

    '    InputFromExcel = Nothing

    '    Try

    '        Dim TempTable As New DataTable
    '        ' HDR=NO to not skip the first line
    '        Dim xlsConn As System.Data.OleDb.OleDbConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & FileName & "';Extended Properties=""Excel 8.0;IMEX=1;HDR=NO;TypeGuessRows = 0;ImportMixedTypes = Text;""") ';
    '        xlsConn.Open()

    '        Dim xlsComm As System.Data.OleDb.OleDbDataAdapter = New System.Data.OleDb.OleDbDataAdapter("select * from [TMS$]", xlsConn)

    '        xlsComm.Fill(TempTable)
    '        xlsConn.Close()

    '        Dim Output As String(,)
    '        ReDim Output(TempTable.Rows.Count - 1, TempTable.Columns.Count - 1)

    '        For RowIdx As Integer = 0 To TempTable.Rows.Count - 1
    '            For ColIdx As Integer = 0 To TempTable.Columns.Count - 1
    '                Application.DoEvents()
    '                Output(RowIdx, ColIdx) = TempTable.Rows(RowIdx).Item(ColIdx).ToString
    '            Next ColIdx
    '        Next RowIdx

    '        Return Output

    '    Catch ex As Exception
    '        HandleException(ex)
    '    End Try
    'End Function

End Class


﻿
Public Class Form1
    Dim EAID As Integer = 4546
    Dim outFile As String = "DebtorID,Commission" & vbNewLine
    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        ConnectDb2("DebtRecovery")
        'RUN FOR LAST MONTH
        Dim endDate As Date = CDate(Format(Now, "MMM 01, yyyy 00:00:00"))
        Dim startDate As Date = DateAdd(DateInterval.Month, -1, endDate)
        Dim April17 As Date = CDate("Apr 1, 2017 00:00:00")
        'startDate = DateAdd(DateInterval.Day, -1, startDate)

        'get all cases with EA visit since April 17
        Dim debt_dt As New DataTable
        LoadDataTable2("DebtRecovery", "SELECT D._rowid, D.linkID, V.date_allocated, D.status, d.debt_amount " & _
                                                "FROM debtor D, clientScheme CS, visit V " & _
                                                "WHERE CS._rowID = D.clientSchemeID" & _
                                                " AND V.DebtorID = D._rowID" & _
                                                 " and V.bailiffID = " & EAID & _
                                                 " and not(CS.clientID in (1,2,24))" & _
                                                 " and D.debtpaid > 0" & _
                                                  " AND V.date_visited >= '" & Format(April17, "yyyy-MM-dd") & "'" & _
                                                " AND V.date_visited < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                                                " order by D.linkID, D._rowID", debt_dt, False)
        Dim lastDebtorID As Integer = 0
        For Each debtRow In debt_dt.Rows
            Dim ignoreCase As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            If debtorID = lastDebtorID Then
                Continue For
            End If

            lastDebtorID = debtorID
            Dim linkID As Integer = 0
            Try
                linkID = debtRow(1)
            Catch ex As Exception

            End Try

            Dim allocatedDate As Date = debtRow(2)
            Dim caseStatus As String = debtRow(3)
            Dim debtAmount As Decimal = debtRow(4)
            'check EA is last to visit
            Dim vis_dt As New DataTable
            LoadDataTable2("DebtRecovery", " select bailiffID from visit " & _
                          " where debtorID = " & debtorID & _
                         " order by date_visited desc", vis_dt, False)
            For Each visRow In vis_dt.Rows
                Dim lastEA As Integer = visRow(0)
                If lastEA <> EAID Then
                    ignoreCase = True
                End If
                Exit For
            Next
            If ignoreCase Then
                Continue For
            End If
            ' check no EA was allocated after our EA
            Dim vis2_dt As New DataTable
            LoadDataTable2("DebtRecovery", " select V.bailiffID from visit V, bailiff B " & _
                          " where V.DebtorID = " & debtorID & _
                          " and V.bailiffID = B._rowID" & _
                          " and agent_type = 'B'" & _
                          " and date_allocated > '" & Format(allocatedDate, "yyyy-MM-dd") & "'", vis2_dt, False)
            For Each vis2Row In vis2_dt.Rows
                Dim allocatedEA As Integer = vis2Row(0)
                If allocatedEA <> EAID Then
                    ignoreCase = True
                    Exit For
                End If
            Next
            If ignoreCase Then
                Continue For
            End If

            'check for payments last month
            Dim pay_dt As New DataTable
            LoadDataTable2("DebtRecovery", " select sum(P.amount), sum(P.split_debt)  from payment P" & _
                      " where P.debtorID = " & debtorID & _
                      " and P.status = 'R'" & _
                      " and P.amount > 0" & _
                      " and P.status_date >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                      " and P.status_date < '" & Format(endDate, "yyyy-MM-dd") & "'", pay_dt, False)
            Dim PaidAmount As Decimal = 0
            Dim paidDebtAmount As Decimal = 0
            For Each PayRow In pay_dt.Rows

                Try
                    PaidAmount = PayRow(0)
                    paidDebtAmount = PayRow(1)
                Catch ex As Exception
                    Continue For
                End Try
            Next
            If PaidAmount = 0 Then
                Continue For
            End If

            
            'first do PIFS
            If caseStatus = "S" Then
                'check no waiting payments or remitted after end date
                Dim pay2_dt As New DataTable
                LoadDataTable2("DebtRecovery", "select count(*) from payment P" & _
                               " where P.debtorID = " & debtorID & _
                               " and (P.status = 'W' or " & _
                               " P.status_date >= '" & Format(endDate, "yyyy-MM-dd") & "')", pay2_dt, False)

                If pay2_dt.Rows(0).Item(0) > 0 Then
                    Continue For
                End If
                'check any links with split enforcement fees for also being PIFs
                If linkID > 0 Then
                    Dim fee_array As Object
                    fee_array = GetSQLResultsArray2("DebtRecovery", " select F.date from fee F" & _
                                  " where F.debtorID = " & debtorID & _
                                  " and F.type = 'Enforcement'" & _
                                  " and F.fee_amount > 0")
                    Dim EnfFeeDate As Date
                    Try
                        EnfFeeDate = fee_array(0)
                    Catch ex As Exception
                        EnfFeeDate = Nothing
                    End Try
                    If EnfFeeDate <> Nothing Then
                        'look for any links with same day enforcement fee
                        Dim link_dt As New DataTable
                        LoadDataTable2("DebtRecovery", " select D._rowID, D.status from debtor D, fee F" & _
                                      " where D.linkID=" & linkID & _
                                      " and D._rowID <> " & debtorID & _
                                      " and F.debtorID = D._rowID " & _
                                      " and F.type = 'Enforcement'" & _
                                      " and F.fee_amount > 0" & _
                                      " and date(F.date)= '" & Format(EnfFeeDate, "yyyy-MM-dd") & "'", link_dt, False)
                        For Each linkRow In link_dt.Rows
                            Dim linkDebtorID As Integer = linkRow(0)
                            Dim linkCaseStatus As String = linkRow(1)
                            If linkCaseStatus <> "S" Then
                                ignoreCase = True
                                Exit For
                            End If
                            'check EA was the last to visit on the case
                            Dim vis3_dt As New DataTable
                            LoadDataTable2("DebtRecovery", " select V.bailiffID from visit V, bailiff B " & _
                                          " where V.DebtorID = " & linkDebtorID & _
                                          " and V.bailiffID = B._rowID" & _
                                          " and agent_type = 'B'" & _
                                          " order by date_visited desc", vis3_dt, False)
                            If vis3_dt.Rows(0).Item(0) <> EAID Then
                                ignoreCase = True
                            End If
                        Next
                        If ignoreCase Then
                            Continue For
                        End If
                    End If
                End If
                'PIF CASE FOUND
                outFile &= debtorID & "," & 160 & vbNewLine
            Else
                'look for half paid cases
                'get amount paid since allocation
                Dim pay3_dt As New DataTable
                LoadDataTable2("DebtRecovery", "select sum(P.split_debt) from payment P" & _
                               " where P.debtorID = " & debtorID & _
                               " and P.status = 'R'" & _
                               " and P.status_date >= '" & Format(allocatedDate, "yyyy-MM-dd") & "'" & _
                               " and P.status_date < '" & Format(endDate, "yyyy-MM-dd") & "'", pay3_dt, False)
                Dim paidAfterAlloc As Decimal = 0
                Try
                    paidAfterAlloc = pay3_dt.Rows(0).Item(0)
                Catch ex As Exception

                End Try

                If paidAfterAlloc > (debtAmount / 2) Then
                    'check was not half paid before last month
                    If paidAfterAlloc - paidDebtAmount > (debtAmount / 2) Then
                        ignoreCase = True
                        Continue For
                    End If
                    'check any links with same enforcement date are half paid
                    If linkID > 0 Then
                        Dim fee_array As Object
                        fee_array = GetSQLResultsArray2("DebtRecovery", " select F.date from fee F" & _
                                      " where F.debtorID = " & debtorID & _
                                      " and F.type = 'Enforcement'" & _
                                      " and F.fee_amount > 0")
                        Dim EnfFeeDate As Date
                        Try
                            EnfFeeDate = fee_array(0)
                        Catch ex As Exception
                            EnfFeeDate = Nothing
                        End Try
                        If EnfFeeDate <> Nothing Then
                            'look for any links with same day enforcement fee
                            Dim link_dt As New DataTable
                            LoadDataTable2("DebtRecovery", " select D._rowID, D.status, D.debt_amount from debtor D, fee F" & _
                                          " where D.linkID=" & linkID & _
                                          " and D._rowID <> " & debtorID & _
                                          " and F.debtorID = D._rowID " & _
                                          " and F.type = 'Enforcement'" & _
                                          " and F.fee_amount > 0" & _
                                          " and date(F.date)= '" & Format(EnfFeeDate, "yyyy-MM-dd") & "'", link_dt, False)
                            For Each linkRow In link_dt.Rows
                                Dim linkDebtorID As Integer = linkRow(0)
                                'check EA was the last to visit on the case
                                Dim vis3_dt As New DataTable
                                LoadDataTable2("DebtRecovery", " select V.bailiffID from visit V, bailiff B " & _
                                              " where V.DebtorID = " & linkDebtorID & _
                                              " and V.bailiffID = B._rowID" & _
                                              " and agent_type = 'B'" & _
                                              " order by date_visited desc", vis3_dt, False)
                                If vis3_dt.Rows(0).Item(0) <> EAID Then
                                    ignoreCase = True
                                End If
                                Dim linkCaseStatus As String = linkRow(1)
                                Dim linkDebtAmount As Decimal = linkRow(2)
                                'get amount paid 
                                Dim pay4_dt As New DataTable
                                LoadDataTable2("DebtRecovery", "select sum(P.split_debt) from payment P" & _
                                               " where P.debtorID = " & debtorID & _
                                               " and P.status_date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                                               " and P.status = 'R'", pay4_dt, False)

                                Dim linkpaidAmt As Decimal = pay4_dt.Rows(0).Item(0)
                                If linkpaidAmt < (linkDebtAmount / 2) Then
                                    ignoreCase = True
                                    Exit For
                                End If
                            Next
                            If ignoreCase Then
                                Continue For
                            End If
                        End If
                    End If
                    'Half Paid CASE FOUND
                    outFile &= debtorID & "," & 80 & vbNewLine
                End If
            End If
        Next
        Dim filename As String = ""

        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".csv"
            .OverwritePrompt = True
            .FileName = "Boosey.xls"
        End With
        If Not SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            MsgBox("No file name selected for save ")
            Return
        End If
        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outFile, False)
        MsgBox("Report saved")
        Me.Close()
    End Sub
End Class

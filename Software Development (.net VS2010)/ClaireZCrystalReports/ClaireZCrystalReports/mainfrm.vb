﻿Public Class mainfrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub run1538btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles run1538btn.Click
        RA1538frm.ShowDialog()
        run1538btn.Enabled = False
        run1541btn.Enabled = False
        exitbtn.Enabled = False
        Me.Close()
    End Sub

    Private Sub run1541btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles run1541btn.Click
        RA1541frm.ShowDialog()
        run1538btn.Enabled = False
        run1541btn.Enabled = False
        exitbtn.Enabled = False
        Me.Close()
    End Sub
End Class

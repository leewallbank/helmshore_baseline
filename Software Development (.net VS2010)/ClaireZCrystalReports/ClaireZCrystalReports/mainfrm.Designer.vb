﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.run1538btn = New System.Windows.Forms.Button()
        Me.run1541btn = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(346, 298)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'run1538btn
        '
        Me.run1538btn.Location = New System.Drawing.Point(165, 87)
        Me.run1538btn.Name = "run1538btn"
        Me.run1538btn.Size = New System.Drawing.Size(118, 23)
        Me.run1538btn.TabIndex = 0
        Me.run1538btn.Text = "Run RA1538"
        Me.run1538btn.UseVisualStyleBackColor = True
        '
        'run1541btn
        '
        Me.run1541btn.Location = New System.Drawing.Point(165, 168)
        Me.run1541btn.Name = "run1541btn"
        Me.run1541btn.Size = New System.Drawing.Size(118, 23)
        Me.run1541btn.TabIndex = 1
        Me.run1541btn.Text = "Run RA1541"
        Me.run1541btn.UseVisualStyleBackColor = True
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(457, 363)
        Me.Controls.Add(Me.run1541btn)
        Me.Controls.Add(Me.run1538btn)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Run crystal reports"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents run1538btn As System.Windows.Forms.Button
    Friend WithEvents run1541btn As System.Windows.Forms.Button

End Class

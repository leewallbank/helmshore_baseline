Public Class mainform
    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "csv files | *.csv"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, addr1, addr2, addr3, addr4, addr5 As String
        Dim debtorID As Integer
        Dim linetext As String = ""
        Dim line(0) As String
       
        Dim idx, idx2 As Integer
        Dim lines As Integer = 0

        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                If Mid(linetext, 2, 11) = ",,,,,,,,,,," Then
                    Continue For
                End If
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        Dim three_years_ago As Date = DateAdd(DateInterval.Year, -3, Now)
        'write out headings
        outfile = "X" & vbTab & "DebtorID" & vbTab & "X" & vbTab & "M" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & _
        "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & _
          "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & _
          "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "Address1" & vbTab & "Address2" & _
        vbTab & "Address3" & vbTab & "Address4" & vbTab & "Address5" & vbTab & _
           "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbTab & "X" & vbNewLine
        Dim field As String = ""
        'fields comma separated
        For idx = 1 To lines - 1
            'ignore first line as it is a heading
            Dim start_idx As Integer = 1
            Dim field_no As Integer = 1
            'ignore commas inside quotes
            Dim inside_quote As Boolean = False
            debtorID = 0
            addr1 = ""
            addr2 = ""
            addr3 = ""
            addr4 = ""
            addr5 = ""
            Dim ignore_case As Boolean
            For field_no = 1 To 22
                For idx2 = start_idx To line(idx).Length
                    If Mid(line(idx), idx2, 1) = """" Then
                        If inside_quote Then
                            inside_quote = False
                        Else
                            inside_quote = True
                        End If
                    End If
                    If Mid(line(idx), idx2, 1) = "," And inside_quote = False Then
                        field = Trim(Mid(line(idx), start_idx, idx2 - start_idx))
                        start_idx = idx2 + 1
                        Exit For
                    End If
                Next
                Dim idx3 As Integer
                Dim new_field As String = ""
                For idx3 = 1 To field.Length
                    If Mid(field, idx3, 1) <> """" And _
                       Mid(field, idx3, 1) <> "," Then
                        new_field = new_field & Mid(field, idx3, 1)
                    End If
                Next
                field = new_field
                Select Case field_no
                    Case 1  'debtorid
                        debtorID = field
                        ignore_case = False
                    Case 9 'column I
                        If field.Length > 0 Then
                            addr1 = field
                        End If
                    Case 10 'column J
                        If field.Length > 0 Then
                            If addr1.Length > 0 Then
                                addr1 = addr1 & " " & field
                            Else
                                addr1 = field
                            End If
                        End If
                    Case 11 'column K
                        If field.Length > 0 Then
                            If addr1.Length > 0 Then
                                addr1 = addr1 & " " & field
                            Else
                                addr1 = field
                            End If
                        End If
                    Case 12 'column L
                        If field.Length > 0 Then
                            If addr1.Length > 0 Then
                                addr1 = addr1 & " " & field
                            Else
                                addr1 = field
                            End If
                        End If
                    Case 13 'column M
                        If field.Length > 0 Then
                            If addr1.Length > 0 Then
                                addr1 = addr1 & " " & field
                            Else
                                addr1 = field
                            End If
                        End If
                    Case 14 'column N
                        If field.Length > 0 Then
                            addr2 = field
                        End If
                    Case 15 'column O
                        If field.Length > 0 Then
                            If addr2.Length > 0 Then
                                addr2 = addr2 & " " & field
                            Else
                                addr2 = field
                            End If
                        End If
                    Case 16 'column P
                        If field.Length > 0 Then
                            addr3 = field
                        End If
                    Case 17 'column Q
                        If field.Length > 0 Then
                            addr4 = field
                        End If
                    Case 18 'column R
                        If field.Length > 0 Then
                            addr5 = field
                        End If
                    Case 22 'column v - last confirmation date
                        If field.Length > 0 Then
                            Dim last_date As Date = CDate(Microsoft.VisualBasic.Left(field, 4) & "," & Mid(field, 5, 2) & "," & Mid(field, 7, 2))
                            If last_date < three_years_ago Then
                                ignore_case = True
                            End If
                        End If
                End Select
            Next
            If addr2.Length = 0 Then
                addr2 = addr3
                addr3 = addr4
                addr4 = addr5
                addr5 = ""
                If addr2.Length = 0 Then
                    addr2 = addr3
                    addr3 = addr4
                    addr4 = ""
                End If
            ElseIf addr3.Length = 0 Then
                addr3 = addr4
                addr4 = addr5
                addr5 = ""
            End If
            If debtorID > 0 And Not ignore_case Then
                outfile = outfile & vbTab & debtorID & vbTab & vbTab & "M" & vbTab & vbTab & vbTab & vbTab & vbTab & _
                        vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & _
                        vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & _
                        vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & addr1 & vbTab & addr2 & _
                        vbTab & addr3 & vbTab & addr4 & vbTab & addr5 & vbNewLine
            End If
            
        Next
        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_successfultrace.txt", outfile, False)
      
        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
        MsgBox("File saved")
        Me.Close()
    End Sub

    Private Sub checkbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

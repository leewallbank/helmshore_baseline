<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.runbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.restartbtn = New System.Windows.Forms.Button
        Me.FeesSQLDataSet = New AOEStats.FeesSQLDataSet
        Me.AOE_statsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AOE_statsTableAdapter = New AOEStats.FeesSQLDataSetTableAdapters.AOE_statsTableAdapter
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AOE_statsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(71, 49)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(94, 23)
        Me.runbtn.TabIndex = 0
        Me.runbtn.Text = "Re-run all report"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(187, 202)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(23, 202)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'restartbtn
        '
        Me.restartbtn.Location = New System.Drawing.Point(71, 110)
        Me.restartbtn.Name = "restartbtn"
        Me.restartbtn.Size = New System.Drawing.Size(94, 23)
        Me.restartbtn.TabIndex = 3
        Me.restartbtn.Text = "Restart report"
        Me.restartbtn.UseVisualStyleBackColor = True
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'AOE_statsBindingSource
        '
        Me.AOE_statsBindingSource.DataMember = "AOE stats"
        Me.AOE_statsBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'AOE_statsTableAdapter
        '
        Me.AOE_statsTableAdapter.ClearBeforeFill = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.restartbtn)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Name = "Form1"
        Me.Text = "AOE statistics report"
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AOE_statsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents FeesSQLDataSet As AOEStats.FeesSQLDataSet
    Friend WithEvents AOE_statsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AOE_statsTableAdapter As AOEStats.FeesSQLDataSetTableAdapters.AOE_statsTableAdapter
    Friend WithEvents restartbtn As System.Windows.Forms.Button

End Class

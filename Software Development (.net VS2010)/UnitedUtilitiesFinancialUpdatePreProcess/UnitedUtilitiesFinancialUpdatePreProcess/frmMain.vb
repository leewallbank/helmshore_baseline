﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization
Imports System.Net.Mail

Public Class frmMain
    Private UnitedUtilitiesData As New clsUnitedUtilitiesData

    Private AccountType As New Dictionary(Of String, String)

    Private InputFilePath As String, FinancialFileName As String, FinancialFileExt As String, ClientSupportFilePath As String
    Private OutputFiles As New ArrayList()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FolderDialog As New FolderBrowserDialog
        Dim FileList() As IO.FileInfo
        Dim ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String, InputLineArray() As String, MBCNotes As String = ""
        Dim Balance As Decimal, AdjustmentBalance As Decimal

        Dim FinancialAdjustmentFile As New ArrayList(), AdditionalChargeFile As New ArrayList(), DirectPaymentFile As New ArrayList(), MissingChargeEndDateFile As New ArrayList()
        Dim FileSequence As Long
        Dim LineNumber As Integer, DetailCount As Integer, AdjustmentCount As Integer
        Dim FinancialTransactionHeader As String() = {"DCA_Marker", "New_Charge_Type", "Account_Number", "Transaction_Value", "Customer_name", "Invoice_Address_Line_1", "Invoice_Address_Line_2", "Invoice_Address_Line_3", "Invoice_Address_Line_4", "Invoice_Address_Line_5", "Invoice_Address_Line_6", "Phone_Number", "Leaving_Date", "Charge_End_Date", "File_Sequence"}

        'Try

        Dim FileDialog As New OpenFileDialog
        FileDialog.InitialDirectory = InputFilePath
        FileDialog.Filter = "UU finance files|DCA_Finance_*.csv"
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
        ClientSupportFilePath = InputFilePath & "To Client Support\"
        'InputFilePath &= "\"
        FinancialFileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FinancialFileExt = Path.GetExtension(FileDialog.FileName)

        Dim FileContents() = System.IO.File.ReadAllLines(InputFilePath & FinancialFileName & FinancialFileExt)

        ProgressBar.Maximum = UBound(FileContents) + 5  ' Include the generation of the 5 output files

        LineNumber = 0

        For Each InputLine As String In FileContents
            ProgressBar.Value = LineNumber
            LineNumber += 1
            Application.DoEvents() ' without this line, the button disappears until processing is complete

            InputLineArray = InputLine.Split(";")

            Select Case InputLineArray(1)
                Case "J"
                    DetailCount += 1
                    ' Add file sequence
                    ReDim Preserve InputLineArray(UBound(InputLineArray) + 1)
                    InputLineArray(UBound(InputLineArray)) = FileSequence.ToString

                    FinancialAdjustmentFile.Add(InputLineArray)
                    Balance += InputLineArray(3)
                    AdjustmentBalance += InputLineArray(3)
                    AdjustmentCount += 1
                    If UnitedUtilitiesData.GetCaseCount(InputLineArray(2)) = 0 Then ExceptionLog &= "Client ref " & InputLineArray(2) & " not loaded at line number " & LineNumber.ToString & vbCrLf

                Case "M"
                    DetailCount += 1
                    ' Add file sequence
                    ReDim Preserve InputLineArray(UBound(InputLineArray) + 1)
                    InputLineArray(UBound(InputLineArray)) = FileSequence.ToString

                    If UnitedUtilitiesData.GetCaseCount(InputLineArray(2)) = 0 Then ExceptionLog &= "Client ref " & InputLineArray(2) & " not loaded at line number " & LineNumber.ToString & vbCrLf

                    If InputLineArray(13) = "3103" & (DateAdd(DateInterval.Month, -3, Date.Today).Year + 2).ToString Then
                        UnitedUtilitiesData.SetMainBillingCharge(InputLine)
                        ExceptionLog &= "Client ref " & InputLineArray(2) & " Main Billing Charge not loaded as it is in the next financial year at line number " & LineNumber.ToString & vbCrLf
                        MBCNotes &= UnitedUtilitiesData.GetCaseID(InputLineArray(2)) & "|Main Billing Charge of " & CDec(InputLineArray(3)) & " chargeable from 01/Apr/" & (DateAdd(DateInterval.Month, -3, Date.Today).Year + 1).ToString & "." & vbCrLf
                        Balance += InputLineArray(3) ' This needs to be included so that the trailer reconciliation does not generate errors
                    Else
                        AdditionalChargeFile.Add(InputLineArray)
                        Balance += InputLineArray(3)
                        AdjustmentBalance += InputLineArray(3)
                        AdjustmentCount += 1
                    End If

                Case "P"
                    DetailCount += 1
                    ' Add file sequence
                    ReDim Preserve InputLineArray(UBound(InputLineArray) + 1)
                    InputLineArray(UBound(InputLineArray)) = FileSequence.ToString

                    DirectPaymentFile.Add(InputLineArray)
                    Balance += InputLineArray(3)
                    AdjustmentBalance += InputLineArray(3)
                    AdjustmentCount += 1
                    If UnitedUtilitiesData.GetCaseCount(InputLineArray(2)) = 0 Then ExceptionLog &= "Client ref " & InputLineArray(2) & " not loaded at line number " & LineNumber.ToString & vbCrLf

                Case "New Charge Type"
                    'ignore column headers

                Case Else
                    ErrorLog &= "Unknown record type of '" & InputLine.Substring(6, 1) & "'at line number " & LineNumber.ToString & ". Line cannot be processed." & vbCrLf
            End Select

        Next InputLine

        ProgressBar.Value += 1
        AuditLog = "File processed: " & InputFilePath & FinancialFileName & FinancialFileExt & vbCrLf
        AuditLog &= "File sequence number: " & FileSequence & vbCrLf & vbCrLf

        AuditLog &= "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
        AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
        AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

        AuditLog &= "Number of financial adjustments: " & AdjustmentCount.ToString & vbCrLf
        AuditLog &= "Total balance of financial adjustments: " & AdjustmentBalance.ToString & vbCrLf

        ProgressBar.Value += 1
        If FinancialAdjustmentFile.Count > 0 Then
            OutputToExcel(ClientSupportFilePath, FinancialFileName & "_FinancialAdjustments", "Financial Adjustments", FinancialTransactionHeader, FinancialAdjustmentFile)
            OutputFiles.Add(ClientSupportFilePath & FinancialFileName & "_FinancialAdjustments.xls")
        End If

        ProgressBar.Value += 1
        If AdditionalChargeFile.Count > 0 Then
            OutputToExcel(ClientSupportFilePath, FinancialFileName & "_AdditionalCharges", "Additional Charges", FinancialTransactionHeader, AdditionalChargeFile)
            OutputFiles.Add(ClientSupportFilePath & FinancialFileName & "_AdditionalCharges.xls")
        End If

        ProgressBar.Value += 1
        If DirectPaymentFile.Count > 0 Then
            OutputToExcel(ClientSupportFilePath, FinancialFileName & "_DirectPayments", "Direct Payments", FinancialTransactionHeader, DirectPaymentFile)
            OutputFiles.Add(ClientSupportFilePath & FinancialFileName & "_DirectPayments.xls")
        End If

        ProgressBar.Value += 1
        If MissingChargeEndDateFile.Count > 0 Then
            OutputToSeparatedFile(ClientSupportFilePath, FinancialFileName & "_MissingChargeEndDate.txt", "|", Nothing, MissingChargeEndDateFile)
            OutputFiles.Add(ClientSupportFilePath & FinancialFileName & "_MissingChargeEndDate.txt")
        End If

        WriteFile(InputFilePath & FinancialFileName & "_Audit.txt", AuditLog)

        If ErrorLog <> "" Then
            WriteFile(InputFilePath & FinancialFileName & "_Error.txt", ErrorLog)
            MsgBox("Errors found.", vbCritical, Me.Text)
        End If

        If ExceptionLog <> "" Then WriteFile(InputFilePath & FinancialFileName & "_Exception.txt", ExceptionLog)
        If MBCNotes <> "" Then WriteFile(ClientSupportFilePath & FinancialFileName & "_MBCNotes.txt", MBCNotes)

        MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

        btnViewInputFile.Enabled = True
        btnViewOutputFile.Enabled = True
        btnViewLogFiles.Enabled = True

        ProgressBar.Value = 0

        'Catch ex As Exception
        '    MsgBox(ex.ToString)
        'End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub

    Private Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, Optional ByVal NoteLength As Integer = 250) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 2) ' + 2 is for :;
        Dim PreviousLength As Integer
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = (NoteTag & ":" & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)) & ";").PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Clipboard.SetText("notepad.exe " & InputFilePath & FinancialFileName & FinancialFileExt)
        If File.Exists(InputFilePath & FinancialFileName & FinancialFileExt) Then System.Diagnostics.Process.Start("wordpad.exe", InputFilePath & FinancialFileName & FinancialFileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If File.Exists(InputFilePath & FinancialFileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FinancialFileName & "_Audit.txt")
        If File.Exists(InputFilePath & FinancialFileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FinancialFileName & "_Error.txt")
        If File.Exists(InputFilePath & FinancialFileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FinancialFileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub
End Class


﻿Imports CommonLibrary
Imports System.IO

Public Class Form1
    Dim start_date As Date
    Dim end_date As Date
    Dim filename As String = ""
    Dim filepath As String = ""
    Private Sub runbtn_Click(sender As System.Object, e As System.EventArgs) Handles runbtn.Click
        Dim password As String
        password = InputBox("Enter password", "Enter Password")
        If password <> "ALL2138C" Then
            MsgBox("Invalid password")
            Me.Close()
            Exit Sub
        End If
        runbtn.Enabled = False
        statuslbl.Text = "Starting BR 24 report"
        run_report()
        
        Me.Close()
    End Sub
    Private Sub run_report()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2138Creport = New RA2138C
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_date)
        SetCurrentValuesForParameterField1(RA2138Creport, myArrayList1)
        myArrayList1.Add(end_date)
        SetCurrentValuesForParameterField2(RA2138Creport, myArrayList1)
        myArrayList1.Add(24)
        SetCurrentValuesForParameterField3(RA2138Creport, myArrayList1)
        myConnectionInfo.ServerName = "TestFees"
        myConnectionInfo.DatabaseName = "TestBailiffRemuneration"
        myConnectionInfo.UserID = "sa"
        myConnectionInfo.Password = "sa"
        myConnectionInfo2.ServerName = "DebtRecovery"
        myConnectionInfo2.DatabaseName = "DebtRecovery"
        myConnectionInfo2.UserID = "vbnet"
        myConnectionInfo2.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2138Creport, myConnectionInfo2)
        filename = "RA2138C Branch 24 All EA Invoices.pdf"
        Dim savefiledialog1 As New SaveFileDialog
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = filename
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            statuslbl.Text = "Running BR 24 report ... please wait "
            filename = SaveFileDialog1.FileName

            RA2138Creport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, savefiledialog1.FileName)

            filename = Replace(filename, "24", "10")

            statuslbl.Text = "Running BR 10 report ... please wait "
            'now run branch 10
            RA2138Creport.Close()

            Dim RA2138C2report = New RA2138C
            Dim myArrayList2 As ArrayList = New ArrayList()
            myArrayList2.Add(start_date)
            SetCurrentValuesForParameterField1(RA2138C2report, myArrayList2)
            myArrayList2.Add(end_date)
            SetCurrentValuesForParameterField2(RA2138C2report, myArrayList2)
            myArrayList2.Add(10)
            SetCurrentValuesForParameterField3(RA2138C2report, myArrayList2)
            myConnectionInfo.ServerName = "TestFees"
            myConnectionInfo.DatabaseName = "TestBailiffRemuneration"
            myConnectionInfo.UserID = "sa"
            myConnectionInfo.Password = "sa"
            myConnectionInfo2.ServerName = "DebtRecovery"
            myConnectionInfo2.DatabaseName = "DebtRecovery"
            myConnectionInfo2.UserID = "vbnet"
            myConnectionInfo2.Password = "tenbv"
            SetDBLogonForReport(myConnectionInfo, RA2138C2report, myConnectionInfo2)
            RA2138C2report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)


            RA2138C2report.Close()
            statuslbl.Text = "Running BR 1 report ... please wait "
            'now run branch 1
            RA2138C2report.Close()
            filename = Replace(filename, "10", "1")

            Dim RA2138C3report = New RA2138C
            Dim myArrayList3 As ArrayList = New ArrayList()
            myArrayList3.Add(start_date)
            SetCurrentValuesForParameterField1(RA2138C3report, myArrayList3)
            myArrayList3.Add(end_date)
            SetCurrentValuesForParameterField2(RA2138C3report, myArrayList3)
            myArrayList3.Add(1)
            SetCurrentValuesForParameterField3(RA2138C3report, myArrayList3)
            myConnectionInfo.ServerName = "TestFees"
            myConnectionInfo.DatabaseName = "TestBailiffRemuneration"
            myConnectionInfo.UserID = "sa"
            myConnectionInfo.Password = "sa"
            myConnectionInfo2.ServerName = "DebtRecovery"
            myConnectionInfo2.DatabaseName = "DebtRecovery"
            myConnectionInfo2.UserID = "vbnet"
            myConnectionInfo2.Password = "tenbv"
            SetDBLogonForReport(myConnectionInfo, RA2138C3report, myConnectionInfo2)
            RA2138C3report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)


            RA2138C3report.Close()
            MsgBox("Reports saved")
        Else
            MsgBox("Report not saved")
        End If

       
    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        end_date = CDate(Format(Now, "MMM yyyy") & " 01 00:00:00")
        end_date = DateAdd(DateInterval.Day, -1, end_date)
        start_date = CDate(Format(end_date, "MMM yyyy") & " 01 00:00:00")
        start_dtp.Value = start_date
        end_dtp.Value = end_date
    End Sub
End Class

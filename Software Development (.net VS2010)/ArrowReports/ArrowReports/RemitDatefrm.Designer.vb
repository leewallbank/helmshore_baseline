<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class RemitDatefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.remit_dtp = New System.Windows.Forms.DateTimePicker
        Me.remitbtn = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'remit_dtp
        '
        Me.remit_dtp.Location = New System.Drawing.Point(41, 36)
        Me.remit_dtp.Name = "remit_dtp"
        Me.remit_dtp.Size = New System.Drawing.Size(137, 20)
        Me.remit_dtp.TabIndex = 0
        '
        'remitbtn
        '
        Me.remitbtn.Location = New System.Drawing.Point(65, 94)
        Me.remitbtn.Name = "remitbtn"
        Me.remitbtn.Size = New System.Drawing.Size(75, 23)
        Me.remitbtn.TabIndex = 1
        Me.remitbtn.Text = "Run report"
        Me.remitbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(86, 20)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(60, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Remit Date"
        '
        'RemitDatefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(232, 147)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.remitbtn)
        Me.Controls.Add(Me.remit_dtp)
        Me.Name = "RemitDatefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Remit Date"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents remit_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents remitbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class

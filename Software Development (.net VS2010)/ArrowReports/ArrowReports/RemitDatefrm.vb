Public Class RemitDatefrm

    Private Sub RemitDatefrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        remit_dtp.Value = DateAdd(DateInterval.Day, -Weekday(Now), Now)
    End Sub

    Private Sub remitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles remitbtn.Click
        remit_date = remit_dtp.Value
        Me.Close()
    End Sub
End Class
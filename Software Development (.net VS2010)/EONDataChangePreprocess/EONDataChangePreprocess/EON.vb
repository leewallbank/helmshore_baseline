﻿Imports System.Xml.Schema
Imports System.IO
Imports commonlibrary

Public Class EON
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim new_file As String = ""
    Dim CSID As Integer
    Dim prod_run As Boolean = False
    Dim filename As String

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If
    End Sub
   
    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub processbtn_Click(sender As System.Object, e As System.EventArgs) Handles processbtn.Click
        With OpenFileDialog1
            .Title = "Read XML file"
            .Filter = "XML file|*.xml"
            .FileName = ""
            .CheckFileExists = True
        End With
        Dim xml_dataSet As New DataSet()
        Dim doc As New Xml.XmlDocument()

        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("File not opened")
            Me.Close()
            Exit Sub
        End If
        Dim InputFilePath As String = Path.GetDirectoryName(OpenFileDialog1.FileName)
        InputFilePath &= "\"
        filename = Path.GetFileNameWithoutExtension(OpenFileDialog1.FileName)
        Dim phoneFile As String = ""
        Dim addressFile As String = "DebtorID|Addr Line1|Addr Line2|Addr Line3|Addr Line4|Postcode" & vbNewLine
        Dim addressChanges As String = 0
        Dim phoneChanges As Integer = 0
        Dim fieldValue As String
        Dim rdr_name As String = ""
        Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
        reader.Read()
        ProgressBar1.Value = 5
        Dim record_count As Integer = 0
        Dim clientID As Integer = 1938
        If prod_run = False Then
            MsgBox("TEST RUN ONLY")
            clientID = 24
        End If
        Dim errorFound As Boolean = False

        While (reader.ReadState <> Xml.ReadState.EndOfFile)
            If reader.NodeType > 1 Then
                reader.Read()
                Continue While
            End If
            Try
                rdr_name = reader.Name
            Catch
                Continue While
            End Try

            'Note ReadElementContentAsString moves focus to next element so don't need read
            ProgressBar1.Value = record_count
            Application.DoEvents()

            Select Case rdr_name
                Case "DEBTOR_DATA_CHANGE"
                    reader.Read()
                    While reader.Name <> "DEBTOR_DATA_CHANGE"
                        rdr_name = reader.Name
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        rdr_name = reader.Name
                        Select Case rdr_name
                            Case "DEBT"
                                Dim clientRef As String = ""
                                Dim addressType As String
                                Dim AddressLine1 As String = ""
                                Dim addressLine2 As String = ""
                                Dim addressLine3 As String = ""
                                Dim addressLine4 As String = ""
                                Dim telePhoneType As String = ""
                                Dim telePhoneNumber As String = ""
                                Dim postCode As String = ""
                                reader.Read()
                                While reader.Name <> "DEBT"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    rdr_name = reader.Name
                                    Select Case rdr_name
                                        Case "CLIENT_REFERENCE"
                                            fieldValue = reader.ReadElementContentAsString
                                            clientRef = fieldValue
                                        Case "ADDRESS_TYPE"
                                            fieldValue = reader.ReadElementContentAsString
                                            addressType = fieldValue
                                        Case "ADDRESS_LINE1"
                                            fieldValue = reader.ReadElementContentAsString
                                            fieldValue = Replace(fieldValue, Chr(10), "")
                                            fieldValue = Replace(fieldValue, Chr(13), "")
                                            AddressLine1 = fieldValue
                                        Case "ADDRESS_LINE2"
                                            fieldValue = reader.ReadElementContentAsString
                                            fieldValue = Replace(fieldValue, Chr(10), "")
                                            fieldValue = Replace(fieldValue, Chr(13), "")
                                            addressLine2 = fieldValue
                                        Case "ADDRESS_LINE3"
                                            fieldValue = reader.ReadElementContentAsString
                                            fieldValue = Replace(fieldValue, Chr(10), "")
                                            fieldValue = Replace(fieldValue, Chr(13), "")
                                            addressLine3 = fieldValue
                                        Case "ADDRESS_LINE4"
                                            fieldValue = reader.ReadElementContentAsString
                                            fieldValue = Replace(fieldValue, Chr(10), "")
                                            fieldValue = Replace(fieldValue, Chr(13), "")
                                            AddressLine4 = fieldValue
                                        Case "POST_CODE"
                                            fieldValue = reader.ReadElementContentAsString
                                            fieldValue = Replace(fieldValue, Chr(10), "")
                                            fieldValue = Replace(fieldValue, Chr(13), "")
                                            postCode = fieldValue
                                        Case "TELEPHONE_TYPE"
                                            fieldValue = reader.ReadElementContentAsString
                                            telephoneType = fieldValue
                                        Case "TELEPHONE_NUMBER"
                                            fieldValue = reader.ReadElementContentAsString
                                            telePhoneNumber = fieldValue
                                        Case Else
                                            reader.Read()
                                    End Select
                                    
                                End While
                                If AddressLine1 <> "" Or telePhoneType <> "" Then
                                    'write out address file
                                    'get debtorID
                                    Dim debtor_dt As New DataTable
                                    LoadDataTable("DebtRecovery", "SELECT D._rowid, add_phone, add_fax, empphone, empfax " & _
                                               "FROM debtor D, clientscheme CS " & _
                                               "WHERE client_ref = '" & clientRef & "'" & _
                                               " and D.clientschemeID = CS._rowID" & _
                                               " and CS.clientID =" & clientID, debtor_dt, False)

                                    For Each DebtRow In debtor_dt.Rows
                                        If AddressLine1 <> "" Then
                                            addressFile &= DebtRow(0) & "|" & AddressLine1 & "|" & addressLine2 & "|" & addressLine3 & "|" & _
                                                addressLine4 & "|" & postCode & vbNewLine
                                            addressChanges += 1
                                        End If
                                        If telePhoneType <> "" Then
                                            'see if phone number is already on onestep
                                            Dim addPhone As String = ""
                                            Try
                                                addPhone = DebtRow(1)
                                            Catch ex As Exception

                                            End Try
                                            If telePhoneNumber = addPhone Then
                                                Continue For
                                            End If
                                            Dim addfAX As String = ""
                                            Try
                                                addfAX = DebtRow(2)
                                            Catch ex As Exception

                                            End Try
                                            If telePhoneNumber = addfAX Then
                                                Continue For
                                            End If
                                            Dim empPhone As String = ""
                                            Try
                                                empPhone = DebtRow(3)
                                            Catch ex As Exception

                                            End Try
                                            If telePhoneNumber = empPhone Then
                                                Continue For
                                            End If
                                            Dim empFax As String = ""
                                            Try
                                                empFax = DebtRow(4)
                                            Catch ex As Exception

                                            End Try
                                            If telePhoneNumber = empFax Then
                                                Continue For
                                            End If
                                            'phone number not on onestep - put in first available slot
                                            If addPhone = "" Then
                                                phoneFile &= DebtRow(0) & "," & telePhoneNumber & ",,," & vbNewLine
                                                phoneChanges += 1
                                                Continue For
                                            End If
                                            If addfAX = "" Then
                                                phoneFile &= DebtRow(0) & ",," & telePhoneNumber & ",," & vbNewLine
                                                phoneChanges += 1
                                                Continue For
                                            End If
                                            If empPhone = "" Then
                                                phoneFile &= DebtRow(0) & ",,," & telePhoneNumber & "," & vbNewLine
                                                phoneChanges += 1
                                                Continue For
                                            End If
                                            If empFax = "" Then
                                                phoneFile &= DebtRow(0) & ",,,," & telePhoneNumber & vbNewLine
                                                phoneChanges += 1
                                            End If
                                        End If
                                    Next
                                End If
                            Case Else
                                'MsgBox("What is this tag?" & rdr_name)
                                reader.Read()
                        End Select
                    End While
            End Select
        End While

        If addressChanges > 0 Then
            filename = InputFilePath & "Address_Changes_" & Format(Now, "ddMMyyyy") & ".txt"
            My.Computer.FileSystem.WriteAllText(filename, addressFile, False)
        End If

        If phoneChanges > 0 Then
            filename = InputFilePath & "Telephone_Changes_" & Format(Now, "ddMMyyyy") & ".txt"
            My.Computer.FileSystem.WriteAllText(filename, phoneFile, False)
        End If
        If addressChanges + phoneChanges > 0 Then
            MsgBox("Address Changes = " & addressChanges & vbNewLine & "Phone changes = " & phoneChanges)
        Else
            MsgBox("No changes found")
        End If

        Me.Close()
    End Sub
    Private Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, Optional ByVal NoteLength As Integer = 250) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 2) ' + 2 is for :;
        Dim PreviousLength As Integer
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = (NoteTag & ":" & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)) & ";").PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function
    Private Function getCaseID(ByVal clientRef As String) As Integer
        Dim debtorID As Integer = 0
        debtorID = GetSQLResults("DebtRecovery", "SELECT D._rowID " & _
                                                   "FROM debtor D, clientscheme CS  " & _
                                                   "WHERE client_ref = '" & clientRef & "'" & _
                                                   " AND D.clientschemeID = CS._rowID " & _
                                                   " AND CS.clientID = 1662")

        Return debtorID
    End Function
End Class

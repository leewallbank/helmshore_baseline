﻿
Imports System.IO
Imports CommonLibrary

Public Class frmMain
    Const Separator As String = "|"
    Private ConnCode As New Dictionary(Of String, Integer)
    Private EmploymentStatus As New Dictionary(Of String, String)
    Private CustomerType As New Dictionary(Of String, String)
    Private BillingType As New Dictionary(Of String, String)
    Private TargetStatus As New Dictionary(Of String, String)
    Private ConnID As String() = {"4550", "4551"}
    Private InputFilePath As String, FileName As String, FileExt As String
    Private OutputFiles As New ArrayList()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        ConnCode.Add("C-1", 0)
        ConnCode.Add("C-2", 1)

        EmploymentStatus.Add("UNEMP", "Unemployed")
        EmploymentStatus.Add("SELF", "Self employed")
        EmploymentStatus.Add("STUD", "Student")
        EmploymentStatus.Add("EMP", "Employed")
        EmploymentStatus.Add("NOBEN", "No benefits")
        EmploymentStatus.Add("RET", "Retired")

        CustomerType.Add("D", "Domestic")
        CustomerType.Add("C", "Commercial")
        CustomerType.Add("M", "Mixed supply")
        
        BillingType.Add("U", "Unmeasured")
        BillingType.Add("M", "Measured")
        BillingType.Add("A", "Assessed volume")

        TargetStatus.Add("A", "Active account")
        TargetStatus.Add("F", "Finalled account")

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String, InputLineArray() As String, ConnKey As String, DebtNotes As String
        Dim NewDebtFile(UBound(ConnID)) As String
        Dim NewDebtSumm(UBound(ConnID), 1) As Decimal, TotalNewDebt As Decimal
        Dim LineNumber As Integer

        Try
            FileDialog.Filter = "Lowell assignment files|*.csv|All files (*.*)|*.*"
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            For Each OldFile As String In Directory.GetFiles(InputFilePath, "*" & FileName & "_*.txt")
                File.Delete(OldFile)
            Next OldFile

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents) + (UBound(NewDebtFile) + 1) + 1 ' +1 for audit log

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                DebtNotes = ""

                If LineNumber = 1 Then ' validate header
                    If InputLine.Split("|")(1) <> UBound(FileContents) Then ErrorLog &= "Count total in the header (" & InputLine.Split(",")(1) & ") does not match file contents (" & UBound(FileContents).ToString & ")." & vbCrLf
                    Continue For
                End If

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                InputLineArray = InputLine.Split("|")

                ConnKey = InputLineArray(35)

                If ConnCode.ContainsKey(ConnKey) Then

                    DebtNotes &= ToNote(InputLineArray(1), "Account reference", ";")(0)

                    If EmploymentStatus.ContainsKey(InputLineArray(4)) Then
                        InputLineArray(4) = EmploymentStatus(InputLineArray(4))
                    ElseIf InputLineArray(4) <> "" Then
                        ErrorLog &= "Unable to map employment status of " & InputLineArray(4) & " at line number " & LineNumber.ToString & vbCrLf
                    End If

                    If CustomerType.ContainsKey(InputLineArray(19)) Then
                        DebtNotes &= ToNote(CustomerType(InputLineArray(19)), "Customer type", ";")(0)
                    Else
                        ErrorLog &= "Unable to map customer type of " & InputLineArray(19) & " at line number " & LineNumber.ToString & vbCrLf
                    End If

                    If BillingType.ContainsKey(InputLineArray(20)) Then
                        DebtNotes &= ToNote(BillingType(InputLineArray(20)), "Billing type", ";")(0)
                    Else
                        ErrorLog &= "Unable to map billing type of " & InputLineArray(20) & " at line number " & LineNumber.ToString & vbCrLf
                    End If

                    ' move telephone numbers left in the output file
                    Dim TelNums() As String = ConcatFields({InputLineArray(21), InputLineArray(22), InputLineArray(23), InputLineArray(24)}, ",").Replace(" ", "").Split(",") ' Remove any spaces. ConcatFields then Split results in an array with no Nothing elements
                    ReDim Preserve TelNums(3) ' this clears down the remaining, unused fields

                    For TelNumCount As Integer = 0 To UBound(TelNums)
                        InputLineArray(21 + TelNumCount) = TelNums(TelNumCount)
                    Next TelNumCount

                    If InputLineArray(25) <> "" Then DebtNotes &= ToNote(Date.ParseExact(InputLineArray(25), "ddMMyyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo).ToString("dd/MM/yyyy"), "Account number start date", ";")(0)

                    If InputLineArray(26) <> "" Then DebtNotes &= ToNote(Date.ParseExact(InputLineArray(26), "ddMMyyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo).ToString("dd/MM/yyyy"), "Account number end date", ";")(0)

                    If InputLineArray(27) <> "" Then DebtNotes &= ToNote(Date.ParseExact(InputLineArray(27), "ddMMyyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo).ToString("dd/MM/yyyy"), "Bill period end date", ";")(0)

                    If TargetStatus.ContainsKey(InputLineArray(28)) Then
                        DebtNotes &= ToNote(TargetStatus(InputLineArray(28)), "Target status", ";")(0)
                    Else
                        ErrorLog &= "Unable to map target status of " & InputLineArray(28) & " at line number " & LineNumber.ToString & vbCrLf
                    End If

                    If InputLineArray(29) <> "" Then DebtNotes &= ToNote(Date.ParseExact(InputLineArray(29), "ddMMyyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo).ToString("dd/MM/yyyy"), "Last payment date", ";")(0)

                    If InputLineArray(30) <> ".00" Then DebtNotes &= ToNote(InputLineArray(30), "Last payment amount", ";")(0)

                    If InputLineArray(31) <> "" Then DebtNotes &= ToNote(Date.ParseExact(InputLineArray(31), "ddMMyyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo).ToString("dd/MM/yyyy"), "Last activity date", ";")(0)

                    If InputLineArray(32) <> ".00" Then DebtNotes &= ToNote(InputLineArray(32), "Claim balance", ";")(0)

                    If InputLineArray(33) <> "" Then DebtNotes &= ToNote(Date.ParseExact(InputLineArray(33), "ddMMyyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo).ToString("dd/MM/yyyy"), "Judgment date", ";")(0)

                    If ConnKey = "C-1" And InputLineArray(33) = "" Then ErrorLog &= "Missing judgment date at line number " & LineNumber.ToString & vbCrLf

                    DebtNotes &= ToNote(InputLineArray(34), "Customer representative", ";")(0)

                    DebtNotes &= ToNote(InputLineArray(35), "Placement type", ";")(0)

                    NewDebtFile(ConnCode(ConnKey)) = Join(InputLineArray, Separator) & Separator

                    NewDebtFile(ConnCode(ConnKey)) &= DebtNotes & vbCrLf

                    NewDebtSumm(ConnCode(ConnKey), 0) += 1
                    NewDebtSumm(ConnCode(ConnKey), 1) += InputLineArray(2)
                    TotalNewDebt += InputLineArray(2)
                    AppendToFile(InputFilePath & ConnID(ConnCode(ConnKey)) & "_" & FileName & "_PreProcessed.txt", NewDebtFile(ConnCode(ConnKey)))

                Else
                    ErrorLog &= "Unable to identify client scheme at line number " & LineNumber.ToString & vbCrLf

                End If

            Next InputLine

            If FileContents(0).Split("|")(2) <> TotalNewDebt Then ErrorLog &= "Debt total in the header (" & FileContents(0).Split("|")(2) & ") does not match file contents (" & TotalNewDebt.ToString & ")." & vbCrLf

            ProgressBar.Value += 1

            AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            For NewDebtFileCount As Integer = 0 To UBound(NewDebtFile)
                ProgressBar.Value += 1
                If NewDebtFile(NewDebtFileCount) <> "" Then
                    OutputFiles.Add(InputFilePath & ConnID(NewDebtFileCount) & "_" & FileName & "_PreProcessed.txt")
                    AuditLog &= "Clientscheme: " & ConnID(NewDebtFileCount) & vbCrLf
                    AuditLog &= "Number of new cases: " & NewDebtSumm(NewDebtFileCount, 0).ToString & vbCrLf
                    AuditLog &= "Total balance of new cases: " & NewDebtSumm(NewDebtFileCount, 1).ToString & vbCrLf & vbCrLf
                End If
            Next NewDebtFileCount

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", LogHeader & ExceptionLog)

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True
            btnCreateConfirmationFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            For Each Filename As String In OutputFiles
                If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
            Next Filename

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnCreateConfirmationFile_Click(sender As Object, e As System.EventArgs) Handles btnCreateConfirmationFile.Click
        Try

            Dim STWData As New clsSTWData
            Dim FileContents() As String = System.IO.File.ReadAllLines(InputFilePath & FileName & FileExt)
            Dim InputLineArray() As String, ExtractDate As String = "", OutputFileName As String = InputFilePath & "DCA_RO_Conf_" & DateTime.Now.ToString("yyyyMMddhhmmss") & ".csv"
            Dim LineNumber As Integer
            Dim CaseDetail As Object()

            If File.Exists(OutputFileName) Then File.Delete(OutputFileName) ' given the seconds timestamp this is unlikely to be fired but just in case...

            ProgressBar.Maximum = UBound(FileContents)

            For Each InputLine As String In FileContents

                ProgressBar.Value = LineNumber
                LineNumber += 1

                InputLineArray = InputLine.Split("|")

                If LineNumber = 1 Then ' ie header
                    ExtractDate = InputLineArray(3)
                    Continue For
                End If

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                CaseDetail = STWData.GetCaseDetail(InputLineArray(0))

                If CaseDetail.Length = 0 Then
                    MessageBox.Show("Cannot find case detail for case " & InputLineArray(0), Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else
                    AppendToFile(OutputFileName, InputLineArray(0) & Separator & CaseDetail(0) & Separator & DateTime.Parse(CaseDetail(1)).ToString("dd/MM/yyyy") & vbCrLf)
                End If
                
            Next InputLine

            ' Finally add the header
            Dim OutputFileContents = File.ReadAllLines(OutputFileName).ToList()
            OutputFileContents.Insert(0, Date.ParseExact(ExtractDate, "ddMMyyyy", System.Globalization.DateTimeFormatInfo.InvariantInfo).ToString("dd/MM/yyyy") & "|" & OutputFileContents.Count.ToString)
            File.WriteAllLines(OutputFileName, OutputFileContents)

            MessageBox.Show("Confirmation file created.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

            ProgressBar.Value = 0
            Process.Start("explorer.exe", "/select," & OutputFileName)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class


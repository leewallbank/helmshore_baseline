﻿Imports CommonLibrary
Imports System.IO
Imports System.Text.RegularExpressions

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String

    Private Const Separator As String = "|"


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        btnViewInputFile.Enabled = False
        btnViewLogFile.Enabled = False
        btnViewOutputFile.Enabled = False

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog

            Dim NewDebtNotes As String = "", NewDebtLine(20) As String
            Dim AuditLog As String
            Dim FileContents(,) As String, ErrorLog As String = ""
            Dim NewDebtFile As String = ""
            Dim NewDebtSumm(1) As Decimal ' 0 Case count, 1 Total balance
            Dim TotalIntChargesColPos As Integer = -1

            btnViewInputFile.Enabled = False
            btnViewLogFile.Enabled = False
            btnViewOutputFile.Enabled = False

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf
            Dim OutputFileName As String = InputFilePath & FileName & "_Preprocessed.txt"

            If File.Exists(OutputFileName) Then File.Delete(OutputFileName)
            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then File.Delete(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then File.Delete(InputFilePath & FileName & "_Error.txt")

            lblReadingFile.Visible = True
            Application.DoEvents()
            FileContents = InputFromExcel(FileDialog.FileName)

            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents) + 2  ' plus audit file and output

            For RowNum = 0 To UBound(FileContents)
                If RowNum > 0 Then
                    Application.DoEvents() ' without this line, the button disappears until processing is complete
                    ProgressBar.Value = RowNum
                    NewDebtNotes = ""

                    ' Strip out any pipes in the data
                    For ColNum As Integer = 0 To UBound(FileContents, 2)
                        FileContents(RowNum, ColNum) = FileContents(RowNum, ColNum).Replace("|", "")
                    Next ColNum

                    ' Read fixed columns
                    NewDebtLine(0) = GetExcelDate(FileContents(RowNum, 0)).ToShortDateString
                    NewDebtLine(1) = GetExcelDate(FileContents(RowNum, 1)).ToShortDateString
                    NewDebtLine(2) = FileContents(RowNum, 2)
                    NewDebtLine(3) = FileContents(RowNum, 3)
                    NewDebtLine(4) = FileContents(RowNum, 4)
                    NewDebtLine(5) = FileContents(RowNum, 5)
                    NewDebtLine(6) = FileContents(RowNum, 6)

                    If FileContents(RowNum, 7) <> "" Then
                        NewDebtLine(7) = If(FileContents(RowNum, 7).Length > 0 AndAlso FileContents(RowNum, 7).Substring(0, 1) <> "0", "0", "") & FileContents(RowNum, 7)
                        NewDebtLine(8) = If(FileContents(RowNum, 8).Length > 0 AndAlso FileContents(RowNum, 8).Substring(0, 1) <> "0", "0", "") & FileContents(RowNum, 8)
                    Else
                        NewDebtLine(7) = If(FileContents(RowNum, 8).Length > 0 AndAlso FileContents(RowNum, 8).Substring(0, 1) <> "0", "0", "") & FileContents(RowNum, 8)
                        NewDebtLine(8) = ""
                    End If

                    NewDebtLine(9) = LTrim(FileContents(RowNum, 9) & If(FileContents(RowNum, 9).Length > 0 AndAlso Not IsNumeric(FileContents(RowNum, 9).Substring(0, 1)), ",", " ") & ConcatFields({FileContents(RowNum, 10), FileContents(RowNum, 11), FileContents(RowNum, 12)}, ","))

                    If NewDebtLine(9) = "" Then ErrorLog &= "No address supplied at line number " & (RowNum + 1).ToString & ". Client ref " & FileContents(RowNum, UBound(FileContents, 2) - 1) & vbCrLf

                    NewDebtLine(10) = FileContents(RowNum, 13)
                    NewDebtLine(11) = FileContents(RowNum, 14)
                    NewDebtLine(12) = FileContents(RowNum, 15)
                    NewDebtLine(13) = FileContents(RowNum, 16)
                    NewDebtLine(14) = FileContents(RowNum, 17)
                    NewDebtLine(15) = FileContents(RowNum, 18)
                    NewDebtLine(16) = FileContents(RowNum, 19)
                    NewDebtLine(17) = FileContents(RowNum, 20)
                    NewDebtLine(18) = FileContents(RowNum, UBound(FileContents, 2) - 2)
                    NewDebtLine(19) = FileContents(RowNum, UBound(FileContents, 2) - 1)
                    NewDebtLine(20) = FileContents(RowNum, UBound(FileContents, 2))

                    ' Now build up notes
                    NewDebtNotes &= ToNote(FileContents(RowNum, 0), "App Date", ";")(0)
                    NewDebtNotes &= ToNote(FileContents(RowNum, 1), "Default Date", ";")(0)
                    NewDebtNotes &= ToNote(FileContents(RowNum, 13), "Time at address", ";")(0)
                    NewDebtNotes &= ToNote(FileContents(RowNum, 14), "Occupancy", ";")(0)
                    NewDebtNotes &= ToNote(FileContents(RowNum, 15), "Company name", ";")(0)
                    NewDebtNotes &= ToNote(FileContents(RowNum, 16), "Position held", ";")(0)
                    NewDebtNotes &= ToNote(FileContents(RowNum, 17), "Employment", ";")(0)
                    NewDebtNotes &= ToNote(FileContents(RowNum, 19), "Source of income", ";")(0)
                    NewDebtNotes &= ToNote(FileContents(RowNum, 20), "Frequency of wage", ";")(0)
                    NewDebtNotes &= ToNote(FileContents(RowNum, 21), "Capital", ";")(0)
                    NewDebtNotes &= ToNote(FileContents(RowNum, 22), "Fees", ";")(0)
                    NewDebtNotes &= ToNote(FileContents(RowNum, 23), "Total Interest and Capital", ";")(0)
                    If TotalIntChargesColPos > -1 Then NewDebtNotes &= ToNote(FileContents(RowNum, TotalIntChargesColPos), "Total Interest and Charges", ";")(0)
                    NewDebtNotes &= ToNote(FileContents(RowNum, UBound(FileContents, 2) - 3), "Total Paid to date", ";")(0)

                    NewDebtFile = Join(NewDebtLine, Separator) & Separator & NewDebtNotes & vbCrLf

                    AppendToFile(OutputFileName, NewDebtFile)

                    ' Update running totals
                    NewDebtSumm(0) += 1
                    NewDebtSumm(1) += CDec(FileContents(RowNum, UBound(FileContents, 2) - 2))

                Else
                    For ColNum As Integer = 0 To UBound(FileContents, 2)
                        If FileContents(0, ColNum) = "Total Int & Charges" Then TotalIntChargesColPos = ColNum
                    Next ColNum

                    If TotalIntChargesColPos = -1 Then ErrorLog &= "Cannot identify the column 'Total Int & Charges'" & vbCrLf
                End If
            Next RowNum

            ' Update audit log
            AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Number of new cases: " & NewDebtSumm(0).ToString & vbCrLf
            AuditLog &= "Total balance of new cases: " & NewDebtSumm(1).ToString & vbCrLf

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            ProgressBar.Value += 1

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            ProgressBar.Value = ProgressBar.Maximum

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try
            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Preprocessed.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Preprocessed.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try
            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function InputFromExcel(ByVal FileName As String) As String(,)

        InputFromExcel = Nothing

        Try

            Dim TempTable As New DataTable
            ' HDR=NO to not skip the first line
            'Dim xlsConn As System.Data.OleDb.OleDbConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & FileName & "';Extended Properties=""Excel 8.0;HDR=NO;TypeGuessRows = 0;ImportMixedTypes = Text;""")
            Dim xlsConn As System.Data.OleDb.OleDbConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & FileName & "';Extended Properties=""Excel 8.0;HDR=NO;IMEX=1;HDR=NO;TypeGuessRows = 0;""")
            xlsConn.Open()
            Dim xlsComm As System.Data.OleDb.OleDbDataAdapter = New System.Data.OleDb.OleDbDataAdapter("select * from [" & xlsConn.GetSchema("Tables").Rows(0)("TABLE_NAME") & "]", xlsConn)

            xlsComm.Fill(TempTable)
            xlsConn.Close()

            Dim Output As String(,)
            ReDim Output(TempTable.Rows.Count - 1, TempTable.Columns.Count - 1)

            For RowIdx As Integer = 0 To TempTable.Rows.Count - 1
                For ColIdx As Integer = 0 To TempTable.Columns.Count - 1
                    Application.DoEvents()
                    Output(RowIdx, ColIdx) = TempTable.Rows(RowIdx).Item(ColIdx).ToString
                Next ColIdx
            Next RowIdx

            Return Output

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function GetExcelDate(CellValue As String) As Date

        GetExcelDate = Nothing

        If CellValue <> "" Then
            If IsNumeric(CellValue) Then GetExcelDate = DateTime.FromOADate(CInt(CellValue)).ToString
            If IsDate(CellValue) Then GetExcelDate = CDate(CellValue).ToString
        End If

        Return GetExcelDate
    End Function


End Class


<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.invbtn = New System.Windows.Forms.Button
        Me.prodbtn = New System.Windows.Forms.Button
        Me.testbtn = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.dispbtn = New System.Windows.Forms.Button
        Me.workbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.datelbl = New System.Windows.Forms.Label
        Me.FeesSQLDataSet = New Employed_Bailiffs_Road_Traffic.FeesSQLDataSet
        Me.BailiffInvoicesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BailiffInvoicesTableAdapter = New Employed_Bailiffs_Road_Traffic.FeesSQLDataSetTableAdapters.BailiffInvoicesTableAdapter
        Me.BailiffPaymentsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.BailiffPaymentsTableAdapter = New Employed_Bailiffs_Road_Traffic.FeesSQLDataSetTableAdapters.BailiffPaymentsTableAdapter
        Me.Employed_bailiffs_calendarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Employed_bailiffs_calendarTableAdapter = New Employed_Bailiffs_Road_Traffic.FeesSQLDataSetTableAdapters.Employed_bailiffs_calendarTableAdapter
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BailiffInvoicesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.BailiffPaymentsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Employed_bailiffs_calendarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'invbtn
        '
        Me.invbtn.Enabled = False
        Me.invbtn.Location = New System.Drawing.Point(116, 246)
        Me.invbtn.Name = "invbtn"
        Me.invbtn.Size = New System.Drawing.Size(101, 23)
        Me.invbtn.TabIndex = 12
        Me.invbtn.Text = "Print Invoices"
        Me.invbtn.UseVisualStyleBackColor = True
        '
        'prodbtn
        '
        Me.prodbtn.Location = New System.Drawing.Point(196, 177)
        Me.prodbtn.Name = "prodbtn"
        Me.prodbtn.Size = New System.Drawing.Size(137, 23)
        Me.prodbtn.TabIndex = 11
        Me.prodbtn.Text = "Update in Production"
        Me.prodbtn.UseVisualStyleBackColor = True
        '
        'testbtn
        '
        Me.testbtn.Enabled = False
        Me.testbtn.Location = New System.Drawing.Point(47, 177)
        Me.testbtn.Name = "testbtn"
        Me.testbtn.Size = New System.Drawing.Size(99, 23)
        Me.testbtn.TabIndex = 10
        Me.testbtn.Text = "Update in Test"
        Me.testbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(46, 307)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 13
        Me.ProgressBar1.Visible = False
        '
        'dispbtn
        '
        Me.dispbtn.Location = New System.Drawing.Point(116, 122)
        Me.dispbtn.Name = "dispbtn"
        Me.dispbtn.Size = New System.Drawing.Size(114, 23)
        Me.dispbtn.TabIndex = 9
        Me.dispbtn.Text = "Display Commissions"
        Me.dispbtn.UseVisualStyleBackColor = True
        '
        'workbtn
        '
        Me.workbtn.Location = New System.Drawing.Point(116, 61)
        Me.workbtn.Name = "workbtn"
        Me.workbtn.Size = New System.Drawing.Size(101, 23)
        Me.workbtn.TabIndex = 8
        Me.workbtn.Text = "Set working days"
        Me.workbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(258, 307)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 14
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'datelbl
        '
        Me.datelbl.AutoSize = True
        Me.datelbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.datelbl.Location = New System.Drawing.Point(113, 21)
        Me.datelbl.Name = "datelbl"
        Me.datelbl.Size = New System.Drawing.Size(55, 16)
        Me.datelbl.TabIndex = 15
        Me.datelbl.Text = "Label1"
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'BailiffInvoicesBindingSource
        '
        Me.BailiffInvoicesBindingSource.DataMember = "BailiffInvoices"
        Me.BailiffInvoicesBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'BailiffInvoicesTableAdapter
        '
        Me.BailiffInvoicesTableAdapter.ClearBeforeFill = True
        '
        'BailiffPaymentsBindingSource
        '
        Me.BailiffPaymentsBindingSource.DataMember = "BailiffPayments"
        Me.BailiffPaymentsBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'BailiffPaymentsTableAdapter
        '
        Me.BailiffPaymentsTableAdapter.ClearBeforeFill = True
        '
        'Employed_bailiffs_calendarBindingSource
        '
        Me.Employed_bailiffs_calendarBindingSource.DataMember = "Employed_bailiffs calendar"
        Me.Employed_bailiffs_calendarBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'Employed_bailiffs_calendarTableAdapter
        '
        Me.Employed_bailiffs_calendarTableAdapter.ClearBeforeFill = True
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(381, 398)
        Me.Controls.Add(Me.datelbl)
        Me.Controls.Add(Me.invbtn)
        Me.Controls.Add(Me.prodbtn)
        Me.Controls.Add(Me.testbtn)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.dispbtn)
        Me.Controls.Add(Me.workbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "mainfrm"
        Me.Text = "Employed Bailiffs RTD"
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BailiffInvoicesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.BailiffPaymentsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Employed_bailiffs_calendarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FeesSQLDataSet As Employed_Bailiffs_Road_Traffic.FeesSQLDataSet
    Friend WithEvents BailiffInvoicesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BailiffInvoicesTableAdapter As Employed_Bailiffs_Road_Traffic.FeesSQLDataSetTableAdapters.BailiffInvoicesTableAdapter
    Friend WithEvents BailiffPaymentsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents BailiffPaymentsTableAdapter As Employed_Bailiffs_Road_Traffic.FeesSQLDataSetTableAdapters.BailiffPaymentsTableAdapter
    Friend WithEvents Employed_bailiffs_calendarBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Employed_bailiffs_calendarTableAdapter As Employed_Bailiffs_Road_Traffic.FeesSQLDataSetTableAdapters.Employed_bailiffs_calendarTableAdapter
    Friend WithEvents invbtn As System.Windows.Forms.Button
    Friend WithEvents prodbtn As System.Windows.Forms.Button
    Friend WithEvents testbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents dispbtn As System.Windows.Forms.Button
    Friend WithEvents workbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents datelbl As System.Windows.Forms.Label

End Class

Public Class commfrm

    Private Sub commfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim max_rows As Integer = 1000

        ReDim case_array(max_rows)
        'get all payments R/W between start date-14 days and end date
        '" and debtorID > 99  order by debtorID, date desc">=5237099
        param1 = "onestep"
        param2 = "select debtorID, status, status_date, amount_typeID, date, clientschemeID from Payment " & _
        " where date >= '" & Format(start_date_14, "yyyy.MM.dd") & "'" & _
        " and debtorID >= 6368090 order by debtorID, date desc"
        mainfrm.ProgressBar1.Value = 5
        Dim payment_dataset As DataSet = get_dataset(param1, param2)
        Dim idx As Integer
        Dim debtor As Integer = 0
        Dim case_no As Integer = 0
        Dim payment_rows As Integer = no_of_rows
        MsgBox("No of rows = " & no_of_rows)
        For idx = 0 To payment_rows - 1
            mainfrm.ProgressBar1.Value = (idx / payment_rows) * 100
            Application.DoEvents()
            Dim payment_status As String = payment_dataset.Tables(0).Rows(idx).Item(1)
            If payment_status <> "W" And payment_status <> "R" Then
                Continue For
            End If

            If payment_dataset.Tables(0).Rows(idx).Item(0) = debtor Then
                Continue For
            Else
                debtor = payment_dataset.Tables(0).Rows(idx).Item(0)
            End If
            Dim status_date As Date = payment_dataset.Tables(0).Rows(idx).Item(2)
            'non cheques must have date after start date
            'cheques must have status date >= start date
            Dim payment_date As Date = payment_dataset.Tables(0).Rows(idx).Item(4)
            If payment_dataset.Tables(0).Rows(idx).Item(3) = 1 Then  'cheque
                If status_date > end_date Then
                    Continue For
                End If
            Else
                If payment_date < start_date Then
                    Continue For
                End If
            End If
            If payment_date > end_date Then
                Continue For
            End If

            'If debtor = 5485988 Then
            '    Continue For
            'End If
            'check debtor status if F/S
            param2 = "select status from Debtor " & _
                        " where _rowid = " & debtor
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            Dim debtor_status As String = debtor_dataset.Tables(0).Rows(0).Item(0)
            If debtor_status <> "F" And debtor_status <> "S" Then
                Continue For
            End If

            'ignore if not road traffic
            Dim csid As Integer = payment_dataset.Tables(0).Rows(idx).Item(5)
            param2 = "select schemeID, branchID from ClientScheme where _rowid = " & csid
            Dim csid_dataset As DataSet = get_dataset(param1, param2)
            'ignore if branch is not = 1
            If csid_dataset.Tables(0).Rows(0).Item(1) <> 1 Then
                Continue For
            End If
            Dim sch_no As Integer = csid_dataset.Tables(0).Rows(0).Item(0)
            param2 = "select name from Scheme where _rowid = " & sch_no
            Dim sch_dataset As DataSet = get_dataset(param1, param2)
            If Microsoft.VisualBasic.Left(sch_dataset.Tables(0).Rows(0).Item(0), 4) <> "Road" Then
                Continue For
            End If
            'get last agent allocated
            param2 = "select bailiffID, date_allocated, date_visited from Visit" & _
            " where debtorID = " & debtor & _
            " order by _rowid desc"
            Dim visit_dataset As DataSet = get_dataset(param1, param2)
            Dim idx2 As Integer
            Dim bailiff_found As Boolean = False
            Dim bailiff As Integer
            Dim date_visited As Date
            Dim visit_rows As Integer = no_of_rows
            Dim first_bailiff As Integer = 0
            For idx2 = 0 To visit_rows - 1
                bailiff = visit_dataset.Tables(0).Rows(idx2).Item(0)
                'check if this is an employed  bailiff
                param2 = "select typeSub, agent_type, internalExternal, status from Bailiff " & _
                "where internalExternal = 'E' and _rowid = " & bailiff
                Dim bailiff_dataset As DataSet = get_dataset(param1, param2)
                If no_of_rows = 0 Then
                    Continue For
                End If
                Dim agent_type As String = bailiff_dataset.Tables(0).Rows(0).Item(1)
                If agent_type = "P" Then
                    Exit For
                End If
                If agent_type <> "A" And agent_type <> "B" Then
                    Continue For
                End If
                Dim type_sub As String = ""
                Try
                    type_sub = bailiff_dataset.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    Continue For
                End Try
                If agent_type = "A" Then
                    If Microsoft.VisualBasic.Left(type_sub, 3) = "Hit" Or _
                        Microsoft.VisualBasic.Left(type_sub, 3) = "NND" Then
                        Exit For
                    End If
                    Continue For
                End If
                If Microsoft.VisualBasic.Left(type_sub, 8) = "Employed" Then
                    Try
                        date_visited = visit_dataset.Tables(0).Rows(idx2).Item(2)
                    Catch ex As Exception
                        first_bailiff = bailiff
                        Continue For
                    End Try
                    If bailiff_dataset.Tables(0).Rows(0).Item(3) = "O" Then
                        bailiff_found = True
                    End If
                End If
                Exit For
            Next
            If bailiff_found = False Then
                Continue For
            ElseIf first_bailiff > 0 And first_bailiff <> bailiff Then
                Continue For
            End If
            'check last visit is within 60 days of last payment date
            If DateDiff(DateInterval.Day, date_visited, payment_date) > 59 Then
                Continue For
            End If
            Dim date_allocated As Date = visit_dataset.Tables(0).Rows(idx2).Item(1)
            'case found
            case_array(case_no).bail_id = bailiff
            case_array(case_no).debtor = debtor
            case_array(case_no).date_allocated = date_allocated
            case_array(case_no).date_visited = date_visited
            case_array(case_no).visit1_fees = 0
            case_array(case_no).visit2_fees = 0
            case_array(case_no).visit3_fees = 0
            case_no += 1
        Next
        tot_pif = case_no
        'process each employed bailiff
        param2 = "select _rowid, name_sur, name_fore from Bailiff where agent_type = 'B' " & _
        "  and status = 'O' and internalExternal = 'E' and left(typeSub,8) = 'Employed' order by name_sur"
        Dim bail_dataset As DataSet = get_dataset(param1, param2)
        ReDim doc_array(no_of_rows)
        ReDim bail_array(no_of_rows)
        Dim bail_name As String = ""
        Dim bail_id As Integer
        Dim bidx As Integer
        Dim bailiff_rows As Integer = no_of_rows
        For bidx = 0 To bailiff_rows - 1
            mainfrm.ProgressBar1.Value = (bidx / bailiff_rows) * 100
            Dim pif As Integer = 0
            Dim tot_levy_comm As Decimal = 0
            Dim tot_van_fees As Decimal = 0
            Dim tot_fc_fees As Decimal = 0
            Dim tot_unpaid_van As Decimal = 0
            Dim tot_unpaid_van_comm As Decimal = 0
            Dim tot_unpaid_swp As Decimal = 0
            Dim tot_unpaid_levy As Decimal = 0
            Dim tot_unpaid_visit1 As Decimal = 0
            Dim tot_unpaid_visit2 As Decimal = 0
            Dim first_visit_no As Integer = 0
            Dim second_visit_no As Integer = 0
            Dim tot_first_visit_comm As Decimal = 0
            Dim tot_second_visit_comm As Decimal = 0
            Dim swp_no As Integer = 0
            Dim tot_swp_comm As Decimal = 0
            Dim bail_deficit_cf As Decimal = 0
            Dim tot_already_paid As Decimal = 0
            bail_id = bail_dataset.Tables(0).Rows(bidx).Item(0)
            bail_array(bail_no).bail_id = bail_id
            bail_name = Trim(bail_dataset.Tables(0).Rows(bidx).Item(2)) & " " & Trim(bail_dataset.Tables(0).Rows(bidx).Item(1))
            bail_array(bail_no).bail_name = bail_name
            bail_array(bail_no).sage_acc = "vb" & LCase(Microsoft.VisualBasic.Left(bail_dataset.Tables(0).Rows(bidx).Item(1), 4) & _
                Microsoft.VisualBasic.Left(bail_dataset.Tables(0).Rows(bidx).Item(2), 1))
            ''get number of days and deficit from last month
            param1 = "Employed_RTD"
            param2 = "select bail_days, bail_deficit_bf from Employed_bailiffs_RTD " & _
            " where bail_id = " & bail_id & " and bail_year = " & start_year & _
            " and bail_month = " & start_month
            Dim emp_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                MessageBox.Show("Last month has not been run yet")
                Me.Close()
                Exit Sub
            End If
            Dim bail_days As Decimal = emp_dataset.Tables(0).Rows(0).Item(0)
            Dim bail_deficit_bf As Decimal = emp_dataset.Tables(0).Rows(0).Item(1)
            doctext.Text = doctext.Text & bail_name & " " & Format(bail_id, "#") & _
                        "   Number of working days = " & Format(bail_days, "#0.00") & vbNewLine
            Dim net_target As Decimal = bail_days * 140
            Dim target As Decimal = net_target + bail_deficit_bf
            bail_array(bail_no).deficit_bf = bail_deficit_bf
            Dim target2 As Decimal = (bail_days * 350) + bail_deficit_bf
            doctext.Text = doctext.Text & "Target commission " & bail_days & " days @ �140 = " & _
            Format(net_target, "�#0.00")
            doctext.Text = doctext.Text & "    Plus deficit B/F of " & Format(bail_deficit_bf, "�#0.00") & _
            "  = " & Format(target, "�#0.00") & vbNewLine & vbNewLine

            ' get cases for this bailiff
            Dim idx2 As Integer
            For idx2 = 0 To tot_pif
                If case_array(idx2).bail_id <> bail_id Then
                    Continue For
                End If

                'get fees
                debtor = case_array(idx2).debtor
                Dim levy_fees As Decimal = 0
                Dim levy_comm As Decimal = 0
                Dim other_fc_fees As Decimal = 0
                Dim van_fees As Decimal = 0
                Dim el_van_fees As Decimal = 0
                Dim already_paid As Decimal = 0
                Dim swp_comm As Decimal = 0
                Dim visit3_fees As Decimal = 0
                Dim visit2_fees As Decimal = 0
                Dim visit1_fees As Decimal = 0
                Dim case_van_status As String = " "
                'include all van fees
                param1 = "onestep"
                param2 = "select _rowid, fee_amount, type, fee_remit_col, bailiffID, remited_fee from Fee where debtorID = " & debtor & _
                " and (fee_remit_col = 3 or fee_remit_col = 4) and fee_amount <> 0"
                Dim fee_dataset As DataSet = get_dataset(param1, param2)
                Dim fidx As Integer = 0
                Dim no_of_fees As Integer = no_of_rows
                If no_of_fees = 0 Then
                    If get_paid(debtor, 12) >= 0 Then
                        case_van_status = "P"
                    Else
                        If get_paid(debtor, 13) >= 0 Then
                            case_van_status = "H"
                        End If
                    End If
                End If
                For fidx = 0 To no_of_fees - 1
                    Dim fees As Decimal
                    Dim fee_rowid As Decimal = fee_dataset.Tables(0).Rows(fidx).Item(0)
                    fees = fee_dataset.Tables(0).Rows(fidx).Item(1)
                    Dim fee_type As String = LCase(fee_dataset.Tables(0).Rows(fidx).Item(2))
                    If InStr(fee_type, "levy") > 0 Then
                        'ignore if not this bailiff
                        Try
                            If bail_id <> fee_dataset.Tables(0).Rows(fidx).Item(4) Then
                                Continue For
                            End If
                        Catch ex As Exception
                            Continue For
                        End Try
                        If bail_id <> fee_dataset.Tables(0).Rows(fidx).Item(4) Then
                            Continue For
                        End If
                        levy_fees = fees
                        'see if levy fees already paid
                        If get_paid(debtor, 1) >= 0 Then
                            Continue For
                        End If
                        If levy_fees > 33 Then
                            tot_levy_comm += 10
                            levy_comm = 10
                        Else
                            levy_comm = (levy_fees * 0.3)
                            tot_levy_comm = tot_levy_comm + levy_comm
                        End If
                    ElseIf fee_dataset.Tables(0).Rows(fidx).Item(3) = 3 Then
                        If InStr(fee_type, "visit") > 0 Then
                            Select Case Trim(fee_type)
                                'check fees remited before start date
                                Case "visit fee 1"
                                    visit1_fees = visit1_fees + get_fc_fees(fee_rowid)
                                    visit1_fees = visit1_fees + fees - fee_dataset.Tables(0).Rows(fidx).Item(5)
                                Case "visit fee 2"
                                    visit2_fees = visit2_fees + get_fc_fees(fee_rowid)
                                    visit2_fees = visit2_fees + fees - fee_dataset.Tables(0).Rows(fidx).Item(5)
                                Case "visit fee 3"
                                    visit3_fees = visit3_fees + get_fc_fees(fee_rowid)
                                    visit3_fees = visit3_fees + fees - fee_dataset.Tables(0).Rows(fidx).Item(5)
                            End Select
                        ElseIf InStr(fee_type, "walk") > 0 Then
                            'ignore if not this bailiff
                            If bail_id <> fee_dataset.Tables(0).Rows(fidx).Item(4) Then
                                Continue For
                            End If
                            If get_paid(debtor, 5) < 0 Then
                                swp_no += 1
                                tot_swp_comm += 2
                                swp_comm = 2
                                tot_unpaid_swp += 2
                            End If
                        End If
                    ElseIf fee_dataset.Tables(0).Rows(fidx).Item(3) = 4 Then
                        Dim case_van_fees As Decimal = fees
                        'If debtor = 5243270 Then
                        '    case_van_fees = 170
                        'End If
                        van_fees += case_van_fees
                        tot_van_fees += case_van_fees
                        'see if van fees already paid
                        If get_paid(debtor, 13) < 0 Then
                            If get_paid(debtor, 12) >= 0 Then
                                'el_van_fees += case_van_fees
                                case_van_status = "P"
                            Else
                                tot_unpaid_van += case_van_fees
                                el_van_fees += case_van_fees
                                case_van_status = "U"
                            End If
                        Else
                            If get_paid(debtor, 12) >= 0 Then
                                'el_van_fees += case_van_fees
                                case_van_status = "P"
                            Else
                                tot_unpaid_van += case_van_fees / 2
                                el_van_fees += case_van_fees / 2
                                case_van_status = "H"
                            End If
                        End If
                    End If
                Next
                If case_van_status <> "P" Then
                    pif += 1
                End If
                If visit1_fees < 0 Then
                    visit1_fees = 0
                End If
                If visit2_fees < 0 Then
                    visit2_fees = 0
                End If
                If visit3_fees < 0 Then
                    visit3_fees = 0
                End If
                case_array(idx2).van_fees = van_fees
                case_array(idx2).el_van_fees = el_van_fees
                case_array(idx2).levy_fees = levy_fees
                case_array(idx2).levy_comm = levy_comm
                case_array(idx2).visit1_fees = visit1_fees
                case_array(idx2).visit2_fees = visit2_fees
                case_array(idx2).visit3_fees = visit3_fees
                case_array(idx2).swp_comm = swp_comm
                If van_fees = 0 Then
                    case_van_status = "Z"
                End If
                case_array(idx2).status = case_van_status
                tot_fc_fees += visit1_fees + visit2_fees + visit3_fees
            Next
            Dim avg_daily_comm As Decimal
            Try
                avg_daily_comm = tot_van_fees / bail_days
            Catch ex As Exception
                avg_daily_comm = 0
            End Try

            Dim basic_comm As Decimal = 0
            Dim excess_comm As Decimal = 0
            Dim basic_rate As Decimal
            If tot_van_fees + tot_fc_fees < target Then
                bail_deficit_cf = target - tot_van_fees - tot_fc_fees
            Else
                excess_comm = tot_unpaid_van + tot_fc_fees - target
            End If
            If pif >= 48 And avg_daily_comm >= 460 Then
                basic_rate = 0.55
            ElseIf pif >= 40 And avg_daily_comm >= 360 Then
                basic_rate = 0.5
            ElseIf avg_daily_comm >= 251 Then
                basic_rate = 0.45
            Else
                basic_rate = 0.35
            End If
            basic_comm = excess_comm * basic_rate

            Dim tot_fc_comm As Decimal = tot_swp_comm + tot_levy_comm
            If bail_deficit_cf = 0 Then
                tot_unpaid_van_comm = tot_unpaid_van * basic_rate
            End If
            'If tot_unpaid_van_comm > basic_comm Then
            '    tot_unpaid_van_comm = basic_comm
            'End If
            Dim amt_payable As Decimal = tot_fc_comm + basic_comm
            If amt_payable < 0 Then
                amt_payable = 0
            End If
            doctext.Text = doctext.Text & "Total PIF's = " & pif & vbNewLine
            doctext.Text = doctext.Text & "Total Commissionable van fees = " & Format(tot_van_fees, "�#0.00")
            doctext.Text = doctext.Text & "     Total unpaid van fees = " & Format(tot_unpaid_van, "�#0.00") & vbNewLine
            'doctext.Text = doctext.Text & "Average daily commission earned = " & Format(avg_daily_comm, "�#0.00")
            doctext.Text = doctext.Text & " FC Fees = " & Format(tot_fc_fees, "�#0.00") & vbNewLine
            doctext.Text = doctext.Text & "Deficit C/F = " & Format(bail_deficit_cf, "�#0.00") & vbNewLine & vbNewLine
            doctext.Text = doctext.Text & "Basic commission on " & _
            Format(excess_comm, "�#0.00") & " @ " & Format(basic_rate, "##.0%") & " = " & Format(basic_comm, "�#.0") & vbNewLine
            doctext.Text = doctext.Text & "Total van commission earned = " & _
            Format(basic_comm, "�#0.00") & vbNewLine

            'doctext.Text = doctext.Text & "       Total unpaid van commission = " & Format(tot_unpaid_van_comm, "�#0.00") & vbNewLine & vbNewLine
            doctext.Text = doctext.Text & "FC FEES" & vbNewLine
            'doctext.Text = doctext.Text & "     No of first visit fees = " & Format(first_visit_no, "#0")
            'doctext.Text = doctext.Text & "     First visit commission earned = " & Format(tot_first_visit_comm, "�#0.00") & vbNewLine
            'doctext.Text = doctext.Text & "     No of second visit fees = " & Format(second_visit_no, "#0")
            'doctext.Text = doctext.Text & "     Second visit commission earned = " & Format(tot_second_visit_comm, "�#0.00") & vbNewLine
            doctext.Text = doctext.Text & "     No of SWP fees = " & Format(swp_no, "#0")
            doctext.Text = doctext.Text & "     SWP commission earned = " & Format(tot_swp_comm, "�#0.00") & vbNewLine
            doctext.Text = doctext.Text & "     Levy commission earned  = " & Format(tot_levy_comm, "�#0.00") & vbNewLine
            doctext.Text = doctext.Text & "     Total FC commission earned = " & Format(tot_fc_comm, "�#0.00") & vbNewLine & vbNewLine
            doctext.Text = doctext.Text & "Total Amount payable = " & Format(amt_payable, "�#0.00") & vbNewLine

            doc_array(bail_no) = doctext.Text
            doctext.Text = ""
            bail_array(bail_no).pif = pif
            bail_array(bail_no).amt_payable = amt_payable
            bail_array(bail_no).deficit_cf = bail_deficit_cf
            If tot_unpaid_van = 0 Then
                bail_array(bail_no).percent = 0
            Else
                bail_array(bail_no).percent = tot_unpaid_van_comm / (tot_unpaid_van + tot_fc_fees)
            End If
            bail_array(bail_no).inv_no = 0
            max_bail_no = bail_no
            bail_no += 1
        Next
        doctext.Text = doc_array(0)
        bail_no = 0

    End Sub

    Private Function get_paid(ByVal debtorid As Integer, ByVal paymentid As Integer) As Decimal
        'get amount already paid
        Dim ret_paid As Decimal
        param1 = "Fees"
        param2 = "select sum(PaymentAmount) from BailiffPayments where CaseId = " & debtorid & _
        " and PaymentTypeID = " & paymentid
        Dim feepay_dataset As DataSet = get_dataset(param1, param2)
        Try
            ret_paid = feepay_dataset.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            ret_paid = -1
        End Try
        Return (ret_paid)
    End Function
    Private Function get_fc_fees(ByVal fee_rowid As Integer) As Decimal
        Dim ret_paid As Decimal
        param1 = "Fees"
        param2 = "select sum(Amount) from FeePayments where FeeId = " & fee_rowid & _
        " and RemitedDate >= '" & Format(start_date_14, "yyyy.MM.dd") & "'"
        Dim feepay_dataset As DataSet = get_dataset(param1, param2)
        Try
            ret_paid = feepay_dataset.Tables(0).Rows(0).Item(0)
        Catch ex As Exception
            ret_paid = 0
        End Try
        Return (ret_paid)
    End Function
    Private Sub printbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles printbtn.Click
        open_print_form()
    End Sub
    

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub nextbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nextbtn.Click
        If bail_no >= max_bail_no Then
            pagelbl.Text = "No more bailiffs"
            Exit Sub
        End If
        pagelbl.Text = ""
        bail_no += 1
        doctext.Text = doc_array(bail_no)
    End Sub

    Private Sub prevbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles prevbtn.Click
        If bail_no = 0 Then
            pagelbl.Text = "At first bailiff"
            Exit Sub
        End If
        pagelbl.Text = ""
        bail_no -= 1
        doctext.Text = doc_array(bail_no)
    End Sub

    Private Sub casesbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles casesbtn.Click
        casesfrm.ShowDialog()
    End Sub

    Private Sub printallbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        open_print_form()
    End Sub
End Class
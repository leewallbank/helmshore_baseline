<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class daysfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.bail_id = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bail_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.bail_days = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Employed_bailiffs_RTDDataSet = New Employed_Bailiffs_Road_Traffic.Employed_bailiffs_RTDDataSet
        Me.Employed_bailiffs_RTDBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Employed_bailiffs_RTDTableAdapter = New Employed_Bailiffs_Road_Traffic.Employed_bailiffs_RTDDataSetTableAdapters.Employed_bailiffs_RTDTableAdapter
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Employed_bailiffs_RTDDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Employed_bailiffs_RTDBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.bail_id, Me.bail_name, Me.bail_days})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(551, 423)
        Me.DataGridView1.TabIndex = 0
        '
        'bail_id
        '
        Me.bail_id.HeaderText = "Bailiff ID"
        Me.bail_id.Name = "bail_id"
        Me.bail_id.ReadOnly = True
        Me.bail_id.Width = 50
        '
        'bail_name
        '
        Me.bail_name.HeaderText = "Name"
        Me.bail_name.Name = "bail_name"
        Me.bail_name.ReadOnly = True
        Me.bail_name.Width = 200
        '
        'bail_days
        '
        Me.bail_days.HeaderText = "Working days"
        Me.bail_days.Name = "bail_days"
        Me.bail_days.Width = 50
        '
        'Employed_bailiffs_RTDDataSet
        '
        Me.Employed_bailiffs_RTDDataSet.DataSetName = "Employed_bailiffs_RTDDataSet"
        Me.Employed_bailiffs_RTDDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Employed_bailiffs_RTDBindingSource
        '
        Me.Employed_bailiffs_RTDBindingSource.DataMember = "Employed_bailiffs_RTD"
        Me.Employed_bailiffs_RTDBindingSource.DataSource = Me.Employed_bailiffs_RTDDataSet
        '
        'Employed_bailiffs_RTDTableAdapter
        '
        Me.Employed_bailiffs_RTDTableAdapter.ClearBeforeFill = True
        '
        'daysfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(551, 423)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "daysfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "working days"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Employed_bailiffs_RTDDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Employed_bailiffs_RTDBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents bail_id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bail_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents bail_days As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Employed_bailiffs_RTDDataSet As Employed_Bailiffs_Road_Traffic.Employed_bailiffs_RTDDataSet
    Friend WithEvents Employed_bailiffs_RTDBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Employed_bailiffs_RTDTableAdapter As Employed_Bailiffs_Road_Traffic.Employed_bailiffs_RTDDataSetTableAdapters.Employed_bailiffs_RTDTableAdapter
End Class

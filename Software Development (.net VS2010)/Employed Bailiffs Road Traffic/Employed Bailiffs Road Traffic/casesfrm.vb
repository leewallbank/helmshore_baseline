Public Class casesfrm

    Private Sub casesfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim idx As Integer
        DataGridView1.Rows.Clear()
        Dim debtor, levy_fees, van_fees, case_van_status, el_van_fees, levy_comm, visit1_fees, _
        visit2_fees, visit3_fees, swp_fees As String
        For idx = 0 To tot_pif
            If case_array(idx).bail_id = bail_array(bail_no).bail_id Then
                debtor = case_array(idx).debtor
                case_van_status = case_array(idx).status
                levy_fees = Format(case_array(idx).levy_fees, "�#0.00")
                levy_comm = Format(case_array(idx).levy_comm, "�#0.00")
                van_fees = Format(case_array(idx).van_fees, "�#0.00")
                el_van_fees = Format(case_array(idx).el_van_fees, "�#0.00")
                visit1_fees = Format(case_array(idx).visit1_fees, "�#0.00")
                visit2_fees = Format(case_array(idx).visit2_fees, "�#0.00")
                visit3_fees = Format(case_array(idx).visit3_fees, "�#0.00")
                swp_fees = Format(case_array(idx).swp_comm, "�#0.00")
                DataGridView1.Rows.Add(debtor, van_fees, case_van_status, el_van_fees, _
                levy_fees, levy_comm, swp_fees, visit1_fees, visit2_fees, visit3_fees)
            End If
        Next
    End Sub
End Class
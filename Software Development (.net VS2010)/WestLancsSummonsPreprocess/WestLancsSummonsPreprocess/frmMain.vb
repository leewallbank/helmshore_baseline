﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String
   

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim OutputFile As String = ""
            Dim totValue As Decimal = 0
            Dim totCases As Integer = 0
        
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim filecontents(,) As String = InputFromExcel(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents)
            Dim rowMax As Integer = UBound(FileContents)
            Dim LineNumber As Integer
            OutputFile = "AccountNo|TotAmt|DebtAddr|CurrAddr|SessionNo|Name|Notes" & vbNewLine
            Dim sessionNumber As String = ""
            Dim accountNumber As String = ""
            Dim currAddr As String = ""
            Dim debtAddr As String = ""
            Dim totAmount As Decimal = 0
            Dim gtotAmount As Decimal = 0
            Dim notes As String = ""
            Dim name As String = ""
            For LineNumber = 0 To rowMax
                ProgressBar.Value = LineNumber
                If filecontents(LineNumber, 1) = "Session Number" Then
                    sessionNumber = filecontents(LineNumber, 2)
                End If

                'new case when col(0) is not blank
                If filecontents(LineNumber, 0) <> "" Then
                    accountNumber = filecontents(LineNumber, 1)
                    currAddr = filecontents(LineNumber, 2) & ","
                    debtAddr = filecontents(LineNumber, 3) & ","
                    'Total Amount
                    If filecontents(LineNumber, 4) = "Total Amount" Then
                        totAmount = filecontents(LineNumber, 5)
                    Else
                        'summons amount
                        If filecontents(LineNumber, 4) <> "" Then
                            notes &= filecontents(LineNumber, 4) & ":" & filecontents(LineNumber, 5) & ";"
                        End If
                    End If
                    Dim lineNumber2 As Integer
                    For lineNumber2 = LineNumber + 1 To rowMax
                        If filecontents(lineNumber2, 1) = "Liable Parties:" Then
                            name &= filecontents(lineNumber2, 2) & " "
                            Try
                                If filecontents(lineNumber2 + 1, 2) <> "" Then
                                    name &= filecontents(lineNumber2 + 1, 2)
                                End If
                            Catch ex As Exception

                            End Try

                            Exit For
                        End If
                        If filecontents(lineNumber2, 0) <> "" Then
                            Exit For
                        End If
                        If filecontents(lineNumber2, 2) <> "" Then
                            currAddr &= filecontents(lineNumber2, 2) & ","
                        End If
                        If filecontents(lineNumber2, 3) <> "" Then
                            debtAddr &= filecontents(lineNumber2, 3) & ","
                        End If
                        If filecontents(lineNumber2, 4) = "Total Amount" Then
                            totAmount = filecontents(lineNumber2, 5)
                        Else
                            'summons amount
                            If filecontents(lineNumber2, 4) <> "" Then
                                notes &= filecontents(lineNumber2, 4) & ":" & filecontents(lineNumber2, 5) & ";"
                            End If
                        End If
                    Next
                    LineNumber = LineNumber2
                    'remove last comma from addresses
                    debtAddr = Microsoft.VisualBasic.Left(debtAddr, debtAddr.Length - 1)
                    currAddr = Microsoft.VisualBasic.Left(currAddr, currAddr.Length - 1)
                    OutputFile &= accountNumber & "|" & totAmount & "|" & debtAddr & "|" & currAddr & "|" &
                        sessionNumber & "|" & name & "|" & notes & vbNewLine
                    totCases += 1
                    gtotAmount += totAmount

                    accountNumber = ""
                    totAmount = 0
                    debtAddr = ""
                    currAddr = ""
                    name = ""
                    notes = ""
                End If

            Next
            btnViewInputFile.Enabled = True
            If totCases > 0 Then
                WriteFile(InputFilePath & FileName & "_preprocess.txt", OutputFile)
                btnViewOutputFile.Enabled = True
            End If

            MsgBox("Cases = " & totCases & vbNewLine & _
                   "Value = " & gtotAmount)
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_preprocess.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_preprocess.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & "_nonWRP_4200.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_nonWRP_4200.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

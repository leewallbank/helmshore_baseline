﻿Imports CommonLibrary

Public Class clsBankAccount
    Public Function GetBankAccount(ByVal ClientSchemeID As Integer) As String

        GetBankAccount = GetSQLResults("FeesSQL", "SELECT bank_account_number " & _
                                                    "FROM BankAccounts " & _
                                                    "WHERE bank_csid = " & ClientSchemeID)
    End Function
End Class

﻿Imports CommonLibrary

Public Class clsSortCode
    Public Function GetSortCode(ByVal ClientSchemeID As Integer) As String

        GetSortCode = GetSQLResults("FeesSQL", "SELECT bank_sort_code " & _
                                                    "FROM BankAccounts " & _
                                                    "WHERE bank_csid = " & ClientSchemeID)
    End Function
End Class

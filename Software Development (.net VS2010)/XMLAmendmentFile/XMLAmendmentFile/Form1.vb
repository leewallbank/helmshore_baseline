﻿Imports System.IO
Public Class Form1

    Dim outfile, inputfilepath, outfilename As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        With OpenFileDialog1
            .Title = "Read XML file"
            .Filter = "XML file|*.xml"
            .FileName = ""
            .CheckFileExists = True
        End With
        Dim xml_dataSet As New DataSet()
        Dim doc As New Xml.XmlDocument()
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("XML File not processed")
        Else
            inputfilepath = Path.GetDirectoryName(OpenFileDialog1.FileName)
            inputfilepath &= "\"
            outfilename = inputfilepath & "XMLF_Reformat.csv"
            outfile = "ClientRef,Name,Address,Balance,PaidTodate,Reason,OrigAmt,OrigCosts,Deceased,Email.Phone1,Phone2,Phone3" & vbNewLine
            Dim rdr_name As String = ""
            Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
            reader.Read()
            While (reader.ReadState <> Xml.ReadState.EndOfFile)
                If reader.NodeType > 1 Then
                    reader.Read()
                    Continue While
                End If
                Try
                    rdr_name = reader.Name
                Catch
                    Continue While
                End Try
                Select Case rdr_name
                    Case "BailiffSheriffChanges"
                        reader.Read()
                    Case "Header"
                        reader.Read()
                    Case "Module"
                        reader.Read()
                    Case "Trailer"
                        reader.Read()
                    Case "NumberOfRecords"
                        reader.Read()
                    Case "ExtractDate"
                        outfile = "ExtractDate," & reader.ReadElementContentAsString & vbNewLine
                    Case "LAName"
                        outfilename = inputfilepath & reader.ReadElementContentAsString & "_Reformat.csv"
                    Case "ChangedRecords"
                        reader.Read()
                        While reader.Name <> "ChangedRecords"
                            If reader.NodeType > 1 Then
                                reader.Read()
                                Continue While
                            End If
                            rdr_name = reader.Name
                            Select Case rdr_name
                                Case "ChangedRecord"
                                    reader.Read()
                                    While reader.Name <> "ChangedRecord"
                                        If reader.NodeType > 1 Then
                                            reader.Read()
                                            Continue While
                                        End If
                                        rdr_name = reader.Name
                                        Select Case rdr_name
                                            Case "AccountReference"
                                                outfile &= reader.ReadElementContentAsString & ","
                                            Case "RecoveryControl"
                                                reader.Read()
                                            Case "Associates"
                                                reader.Read()
                                                While reader.Name <> "Associates"
                                                    If reader.NodeType > 1 Then
                                                        reader.Read()
                                                        Continue While
                                                    End If
                                                    rdr_name = reader.Name
                                                    Select Case rdr_name
                                                        Case "Associate"
                                                            reader.Read()
                                                            While reader.Name <> "Associate"
                                                                If reader.NodeType > 1 Then
                                                                    reader.Read()
                                                                    Continue While
                                                                End If
                                                                rdr_name = reader.Name
                                                                Select Case rdr_name
                                                                    Case "PersonReference"
                                                                        reader.Read()
                                                                    Case "PersonName"
                                                                        reader.Read()
                                                                        Dim personName As String = ""
                                                                        While reader.Name <> "PersonName"
                                                                            If reader.NodeType > 1 Then
                                                                                reader.Read()
                                                                                Continue While
                                                                            End If
                                                                            rdr_name = reader.Name

                                                                            Select Case rdr_name
                                                                                Case "PersonTitle"
                                                                                    personName = reader.ReadElementContentAsString & " "
                                                                                Case "PersonForename"
                                                                                    personName &= reader.ReadElementContentAsString & " "
                                                                                Case "PersonSurname"
                                                                                    personName &= reader.ReadElementContentAsString
                                                                                Case Else
                                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                                    reader.Read()
                                                                            End Select
                                                                        End While
                                                                        outfile &= personName & ","
                                                                    Case "PostalAddress"
                                                                        reader.Read()
                                                                        Dim postalAddress As String = ""
                                                                        While reader.Name <> "PostalAddress"
                                                                            If reader.NodeType > 1 Then
                                                                                reader.Read()
                                                                                Continue While
                                                                            End If
                                                                            rdr_name = reader.Name
                                                                            Select Case rdr_name
                                                                                Case "PostalAddressLine"
                                                                                    postalAddress &= reader.ReadElementContentAsString & " "
                                                                                Case "PostalPostCode"
                                                                                    postalAddress &= reader.ReadElementContentAsString
                                                                                Case Else
                                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                                    reader.Read()
                                                                            End Select
                                                                        End While
                                                                        postalAddress = Replace(postalAddress, ",", " ")
                                                                        outfile &= postalAddress & ","
                                                                    Case "Deceased"
                                                                        outfile &= reader.ReadElementContentAsString & ","
                                                                    Case "AssociateContactDetails"
                                                                        reader.Read()
                                                                        While reader.Name <> "AssociateContactDetails"
                                                                            If reader.NodeType > 1 Then
                                                                                reader.Read()
                                                                                Continue While
                                                                            End If
                                                                            rdr_name = reader.Name
                                                                            Select Case rdr_name
                                                                                Case "ACDescription"
                                                                                    reader.Read()
                                                                                Case "ACType"
                                                                                    reader.Read()
                                                                                Case "ACNumber"
                                                                                    outfile &= reader.ReadElementContentAsString & ","
                                                                                Case "ACEmailAddress"
                                                                                    outfile &= reader.ReadElementContentAsString & ","
                                                                                Case Else
                                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                                    reader.Read()
                                                                            End Select

                                                                        End While
                                                                    Case Else
                                                                        MsgBox("What is this tag?" & rdr_name)
                                                                        reader.Read()
                                                                End Select
                                                            End While
                                                            outfile &= vbNewLine
                                                        Case "RecoveryBalance"
                                                            Dim element As String = reader.ReadElementContentAsString
                                                            element = Replace(element, ",", "")
                                                            outfile &= element & ","
                                                        Case "PaidToDate"
                                                            outfile &= reader.ReadElementContentAsString & ","
                                                        Case "Reason"
                                                            outfile &= reader.ReadElementContentAsString & ","
                                                        Case "OriginalLOAmount"
                                                            Dim element As String = reader.ReadElementContentAsString
                                                            element = Replace(element, ",", "")
                                                            outfile &= element & ","
                                                        Case "OriginalCostsAmount"
                                                            Dim element As String = reader.ReadElementContentAsString
                                                            element = Replace(element, ",", "")
                                                            outfile &= element & ","
                                                        Case Else
                                                            MsgBox("What is this tag?" & rdr_name)
                                                            reader.Read()
                                                    End Select
                                                End While
                                        End Select
                                    End While

                                Case Else
                                    MsgBox("What is this tag?" & rdr_name)
                                    reader.Read()
                            End Select
                        End While
                    Case Else
                        MsgBox("What is this tag?" & rdr_name)
                        reader.Read()
                End Select
            End While
            My.Computer.FileSystem.WriteAllText(outfilename, outfile, False)
        End If
        MsgBox("Report saved")
        Me.Close()
    End Sub
End Class

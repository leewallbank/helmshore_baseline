Public Class mainform
    Dim NoOfCases As Integer


    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim debt_addr As String = ""
        Dim curraddr As String = ""
        Dim name As String = ""
        Dim idx As Integer
        Dim lines As Integer = 0
        Dim debt_amt As Decimal
        Dim Due_date, fromdate, todate, inv_date As Date
        Dim clref As String = ""
        Dim addrline As String = ""
        Dim inv_no As String
        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "Cust No|Name|CurrentAddress|Debt Amount|Inv No|Inv date|Due date|Ref|From date|To date|Debt Addr" & vbNewLine
        outfile = outline
        Dim start_idx As Integer
        'look for Customer Number
        Dim cust_no As Integer
        Dim line_idx As Integer
        For idx = 0 To lines - 1
            Dim check_line As String = LCase(line(idx))
            start_idx = InStr(check_line, "customer number")
            If start_idx > 0 Then
                cust_no = Trim(Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 15))
                NoOfCases += 1
                'get debtor name
                name = ""
                For line_idx = idx + 1 To lines
                    check_line = LCase(line(line_idx))
                    start_idx = InStr(check_line, "debtors name")
                    If start_idx > 0 Then
                        name = Trim(Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 23))
                        Exit For
                    End If
                Next
                If name = "" Then
                    errorfile = errorfile & "Line  " & line_idx & " - No debtor name" & vbNewLine
                    Continue For
                End If
                curraddr = ""
                For line_idx = line_idx + 1 To lines
                    check_line = Trim(line(line_idx))
                    start_idx = InStr(LCase(check_line), "total amount")
                    If start_idx > 0 Then
                        Exit For
                    End If
                    check_line = remove_chars(check_line)
                    If check_line.Length > 1 Then
                        curraddr = curraddr & check_line & ","
                    End If
                Next

                If curraddr = "" Then
                    errorfile = errorfile & "Line  " & line_idx & " - No Current Address" & vbNewLine
                    Continue For
                End If
                'remove last comma
                curraddr = Microsoft.VisualBasic.Left(curraddr, curraddr.Length - 1)
                debt_amt = 0
                For line_idx = line_idx + 1 To lines
                    check_line = LCase(line(line_idx))
                    start_idx = InStr(check_line, "balance outstanding")
                    If start_idx > 0 Then
                        debt_amt = Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 19)
                        Exit For
                    End If
                Next
                If debt_amt = 0 Then
                    errorfile = errorfile & "Line  " & line_idx & " - No debt amount" & vbNewLine
                    Continue For
                End If
                inv_no = ""
                For line_idx = line_idx + 1 To lines
                    check_line = LCase(line(line_idx))
                    start_idx = InStr(check_line, "invoice no")
                    If start_idx > 0 Then
                        inv_no = Trim(Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 11))
                        Exit For
                    End If
                Next
                If inv_no = "" Then
                    errorfile = errorfile & "Line  " & line_idx & " - No Invoice Number" & vbNewLine
                    Continue For
                End If
                inv_date = Nothing
                For line_idx = line_idx + 1 To lines
                    check_line = LCase(line(line_idx))
                    start_idx = InStr(check_line, "invoice date")
                    If start_idx > 0 Then
                        Try
                            inv_date = CDate(Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 13))
                        Catch ex As Exception
                            errorfile = errorfile & "Line  " & line_idx & " - Invalid invoice date" & vbNewLine
                        End Try
                        Exit For
                    End If
                Next
                Due_date = Nothing
                For line_idx = line_idx + 1 To lines
                    check_line = LCase(line(line_idx))
                    start_idx = InStr(check_line, "due date")
                    If start_idx > 0 Then
                        Try
                            Due_date = CDate(Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 9))
                        Catch ex As Exception
                            errorfile = errorfile & "Line  " & line_idx & " - Invalid Due date" & vbNewLine
                        End Try
                        Exit For
                    End If
                Next
                clref = ""
                For line_idx = line_idx + 1 To line_idx + 20
                    check_line = line(line_idx)
                    start_idx = InStr(check_line, "Re :")
                    If start_idx > 0 Then
                        clref = Trim(Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 4))
                        Exit For
                    End If
                    start_idx = InStr(check_line, "Re:")
                    If start_idx > 0 Then
                        clref = Trim(Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 3))
                        Exit For
                    End If
                Next
                
                'If clref = "" Then
                '    errorfile = errorfile & "Line  " & line_idx & " - No Client Ref on cust_no = " & cust_no & vbNewLine
                '    Continue For
                'End If

                fromdate = Nothing
                todate = Nothing
                For line_idx = line_idx + 1 To lines - 1
                    check_line = LCase(Trim(line(line_idx)))
                    start_idx = InStr(check_line, "from")
                    If check_line.Length < 2 Then
                        Continue For
                    End If
                    start_idx = InStr(check_line, "from")
                    If start_idx = 0 Then
                        Exit For
                    End If
                    Dim test_date As Date = Nothing
                    Dim to_idx As Integer = InStr(check_line, "to")
                    Try
                        test_date = CDate(Mid(check_line, start_idx + 4, to_idx - start_idx - 4))
                    Catch ex As Exception

                    End Try
                    If fromdate = Nothing Then
                        fromdate = test_date
                    End If
                    If test_date <> Nothing Then
                        If test_date < fromdate Then
                            fromdate = test_date
                        End If
                    End If
                    If fromdate = Nothing Then
                        errorfile = errorfile & "Line  " & idx & " - No From date on cust_no = " & cust_no & vbNewLine
                        Continue For
                    End If
                    test_date = Nothing
                    Try
                        test_date = Microsoft.VisualBasic.Right(check_line, check_line.Length - to_idx - 2)
                    Catch ex As Exception

                    End Try
                    If test_date <> Nothing Then
                        If test_date > todate Then
                            todate = test_date
                        End If
                    End If

                    If todate = Nothing Then
                        errorfile = errorfile & "Line  " & line_idx & " - No To date" & vbNewLine
                        Continue For
                    End If
                Next
                debt_addr = ""
                For line_idx = line_idx + 1 To lines - 1
                    check_line = Trim(line(line_idx))
                    If check_line.Length < 2 Then
                        Continue For
                    End If
                    start_idx = InStr(LCase(check_line), "council")
                    If start_idx > 0 Then
                        Exit For
                    End If
                    start_idx = InStr(LCase(check_line), "rent ref")
                    If start_idx > 0 Then
                        Exit For
                    End If
                    start_idx = InStr(LCase(check_line), "revenues")
                    If start_idx > 0 Then
                        Exit For
                    End If
                    check_line = remove_chars(check_line)
                    debt_addr = debt_addr & check_line & ","
                Next
                If debt_addr = "" Then
                    errorfile = errorfile & "Line  " & idx & " - No Debt address on cust_no = " & cust_no & vbNewLine
                    Continue For
                End If
                'remove last comma
                debt_addr = Microsoft.VisualBasic.Left(debt_addr, debt_addr.Length - 1)
            Else
                Continue For
            End If

            'save case in outline
            outfile = outfile & cust_no & "|" & name & "|" & curraddr & "|" & debt_amt & "|" _
            & inv_no & "|" & inv_date & "|" & Due_date & "|" & clref & "|" & fromdate & "|" & todate & _
            "|" & debt_addr & vbNewLine
        Next

        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
            MsgBox("Number of cases = " & NoOfCases)
            Me.Close()
        End If

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Function remove_chars(ByVal txt As String) As String
        Dim idx As Integer
        Dim new_txt As String = ""
        txt = Replace(txt, ",", " ")
        For idx = 1 To txt.Length
            If Mid(txt, idx, 1) <> Chr(10) And Mid(txt, idx, 1) <> Chr(13) Then
                new_txt = new_txt & Mid(txt, idx, 1)
            End If
        Next
        Return Trim(new_txt)
    End Function
End Class

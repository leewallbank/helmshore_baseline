Imports System.IO
Public Class mainfrm

    Dim filename, filename_id, header_id As String
    Dim new_file As String = ""
    Dim fdc_id As String
    Dim record_count, no_rejected, ln As Integer
    Dim maat_id As String
    Dim sentence_date, calc_date As Date
    Dim valid_recs As Integer = 0
    Public Structure error_str
        Dim fdc_id As Double
        Dim maat_id As Double
        Dim error_text As String
    End Structure
    Public error_table(8000) As error_str
    Dim final_cost, final_vat, lgfs_total, lgfs_vat, agfs_total, agfs_vat As Decimal

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub xmlbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles xmlbtn.Click

        Try
            With OpenFileDialog1
                .Title = "Read XML file"
                .Filter = "XML file|*.xml"
                .FileName = ""
                .CheckFileExists = True

            End With
            Dim xml_dataSet As New DataSet()
            Dim doc As New Xml.XmlDocument()
            If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
                MsgBox("File not opened")
                Exit Sub
            End If
            xmlbtn.Enabled = False
            exitbtn.Enabled = False
            filename = OpenFileDialog1.FileName
            ln = Microsoft.VisualBasic.Len(filename)
            Dim InputFilePath As String = Path.GetDirectoryName(filename)
            InputFilePath &= "\"
            Dim rdr_name As String = ""
            ProgressBar1.Value = 5
            Dim reader As New Xml.XmlTextReader(filename)
            reset_fields()
            Dim idx As Integer = 0
            no_rejected = 0
            While (reader.ReadState <> Xml.ReadState.EndOfFile)
                record_count += 1
                If record_count > 100 Then
                    record_count = 0
                End If
                ProgressBar1.Value = record_count
                Application.DoEvents()
                If reader.NodeType > 1 Then
                    reader.Read()
                    Continue While
                End If
                Try
                    rdr_name = reader.Name
                Catch
                    Continue While
                End Try
                If rdr_name.Length = 0 Then
                    reader.Read()
                    Continue While
                End If
                Select Case rdr_name
                    Case "header"
                        header_id = reader.Item(0)
                        reader.Read()
                    Case "filename"
                        filename_id = reader.ReadElementContentAsString
                    Case "fdc"
                        If idx > 0 Then
                            end_of_record()
                        End If
                        idx += 1
                        reset_fields()
                        fdc_id = reader.Item(0)
                        reader.Read()
                    Case "maat_id"
                        maat_id = reader.ReadElementContentAsString
                    Case "sentenceDate"
                        sentence_date = reader.ReadElementContentAsString
                    Case "calculationDate"
                        calc_date = reader.ReadElementContentAsString
                    Case "final_cost"
                        final_cost = reader.ReadElementContentAsString
                    Case "final_vat"
                        final_vat = reader.ReadElementContentAsString
                    Case "lgfs_total"
                        lgfs_total = reader.ReadElementContentAsString
                    Case "lgfs_vat"
                        lgfs_vat = reader.ReadElementContentAsString
                    Case "agfs_total"
                        agfs_total = reader.ReadElementContentAsString
                    Case "agfs_vat"
                        agfs_vat = reader.ReadElementContentAsString
                    Case Else
                        reader.Read()
                End Select

            End While
            'do last record
            end_of_record()
            Dim ack_file As String = InputFilePath & "CONTRIBUTIONS_FILE_ACK_FDC_" & Format(Now, "yyyyMMddHHmm") & ".xml"
            Dim writer As New Xml.XmlTextWriter(ack_file, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)
            writer.Formatting = Xml.Formatting.Indented
            'acknowledgement file includes all records not rejected
            Write_ack(writer, filename_id, valid_recs, no_rejected, no_rejected)
            writer.Close()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        MsgBox("Acknowledgement and Note file created")
        Me.Close()
    End Sub
    Private Sub end_of_record()
        If maat_id = "" Then
            no_rejected += 1
            error_table(no_rejected).fdc_id = fdc_id
            error_table(no_rejected).maat_id = Nothing
            error_table(no_rejected).error_text = "invalid maat id"
            Exit Sub
        End If

        'ignore certain maat ids
        If maat_id = 2247590 Or maat_id = 2104845 Or maat_id = 2285773 Or maat_id = 2209220 _
        Or maat_id = 2323846 Or maat_id = 2373783 Or maat_id = 2222988 Or maat_id = 2427319 _
        Or maat_id = 2113348 Or maat_id = 2566457 Then
            no_rejected += 1
            error_table(no_rejected).fdc_id = fdc_id
            error_table(no_rejected).maat_id = maat_id
            error_table(no_rejected).error_text = "rejected as instructed"
            Exit Sub
        End If

        'validate sentence order date
        If sentence_date <> Nothing Then
            If sentence_date < CDate("2001,1,1") Then
                no_rejected += 1
                error_table(no_rejected).fdc_id = fdc_id
                error_table(no_rejected).maat_id = maat_id
                error_table(no_rejected).error_text = "Sentence date is invalid"
                Exit Sub
            End If
        End If

        'validate calculation date
        If calc_date <> Nothing Then
            If calc_date < CDate("2001,1,1") Then
                no_rejected += 1
                error_table(no_rejected).fdc_id = fdc_id
                error_table(no_rejected).maat_id = maat_id
                error_table(no_rejected).error_text = "Calculation date is invalid"
                Exit Sub
            End If
        End If

        'check if maat-id already exists on onestep
        param1 = "onestep"
        param2 = "select  clientschemeID, _rowid from Debtor" & _
        " where  client_ref = '" & maat_id & "'"
        'instead of hard-coding csid, use csid to get client-id, if =909 then it is LCS
        '"' and  (clientschemeID = 1892 or clientschemeID = 2109 or clientschemeID = 2110)"
        Dim debtor_dataset As DataSet = get_dataset(param1, param2)
        Dim debtor As Integer = 0
        If no_of_rows > 0 Then
            Dim idx As Integer
            Dim csid_no As Integer = no_of_rows - 1
            For idx = 0 To csid_no
                Dim csid As Integer = debtor_dataset.Tables(0).Rows(idx).Item(0)
                param2 = "select clientID from ClientScheme where _rowid = " & csid
                Dim csid_datatset As DataSet = get_dataset(param1, param2)
                If no_of_rows <> 1 Then
                    MsgBox("Unable to read clientscheme for csid = " & csid)
                    Exit Sub
                End If

                If csid_datatset.Tables(0).Rows(0).Item(0) <> 909 Then
                    Continue For
                Else
                    debtor = debtor_dataset.Tables(0).Rows(idx).Item(1)
                    Exit For
                End If
            Next
        End If
        If no_of_rows = 0 Or debtor = 0 Then
            'not on onestep
            no_rejected += 1
            error_table(no_rejected).fdc_id = fdc_id
            error_table(no_rejected).maat_id = maat_id
            error_table(no_rejected).error_text = "Not on system"
        Else
            'check if this is the first time final cost has been sent
            param2 = "select count(*) from Note where debtorID = " & debtor &
                " and instr(text,'FinalCost:') > 0"
            Dim note_ds As DataSet = get_dataset("onestep", param2)
            Dim new_fdc As Boolean
            If note_ds.Tables(0).Rows(0).Item(0) > 0 Then
                new_fdc = False
            Else
                new_fdc = True
            End If
            write_note(debtor & "|" & "SentenceOrderDate:" & Format(sentence_date, "dd/MM/yyyy"), new_fdc)
            write_note(debtor & "|" & "CalculationDate:" & Format(calc_date, "dd/MM/yyyy"), new_fdc)
            write_note(debtor & "|" & "FinalCost:" & Format(final_cost, "F"), new_fdc)
            write_note(debtor & "|" & "FinalVat:" & Format(final_vat, "F"), new_fdc)
            write_note(debtor & "|" & "LGFS_Total:" & Format(lgfs_total, "F"), new_fdc)
            write_note(debtor & "|" & "LGFS_Vat:" & Format(lgfs_vat, "F"), new_fdc)
            write_note(debtor & "|" & "AGFS_Total:" & Format(agfs_total, "F"), new_fdc)
            write_note(debtor & "|" & "AGFS_Vat:" & Format(agfs_vat, "F"), new_fdc)

            valid_recs += 1
        End If
    End Sub
    Private Sub reset_fields()
        fdc_id = ""
        maat_id = ""
        sentence_date = Nothing
        calc_date = Nothing
        final_cost = Nothing
        final_vat = Nothing
        lgfs_total = Nothing
        lgfs_vat = Nothing
        agfs_total = Nothing
        agfs_vat = Nothing
    End Sub
    Private Sub write_note(ByVal note_message As String, ByVal new_fdc As Boolean)
        Static Dim note_count, new_FDC_note_count As Integer
        note_message = note_message & ";from feed on " & Format(Now, "dd.MM.yyyy") & vbNewLine
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        If new_fdc Then
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_new_FDC_note.txt"
            new_FDC_note_count += 1
            If new_FDC_note_count = 1 Then
                My.Computer.FileSystem.WriteAllText(new_file, note_message, False)
            Else
                My.Computer.FileSystem.WriteAllText(new_file, note_message, True)
            End If
        Else
            new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_note.txt"
            note_count += 1
            If note_count = 1 Then
                My.Computer.FileSystem.WriteAllText(new_file, note_message, False)
            Else
                My.Computer.FileSystem.WriteAllText(new_file, note_message, True)
            End If
        End If

    End Sub
    Private Sub Write_ack(ByVal writer As Xml.XmlWriter, ByVal filename_id As String, _
         ByVal valid_recs As Integer, ByVal rej_recs As Integer, _
        ByVal no_of_errors As Integer)
        writer.WriteStartElement("CONTRIBUTIONS_FILE_ACK")
        writer.WriteStartElement("FILENAME")
        writer.WriteAttributeString("ID", header_id)
        writer.WriteString(filename_id)
        writer.WriteEndElement()
        writer.WriteElementString("DATE_LOADED", Format(Now, "dd-MM-yyyy"))
        writer.WriteElementString("NO_OF_RECORDS_LOADED", valid_recs)
        writer.WriteElementString("NO_OF_RECORDS_REJECTED", rej_recs)

        Dim idx As Integer
        Dim fdc_id As Double = 0
        Dim last_fdc_id As Double = 0
        Dim maat_id As Double = 0
        Dim error_text As String = ""
        For idx = 1 To no_of_errors
            If idx = 1 Then
                writer.WriteStartElement("REJECTED_RECORD_LIST")
            End If
            fdc_id = error_table(idx).fdc_id
            If fdc_id <> last_fdc_id Then
                last_fdc_id = fdc_id
                writer.WriteStartElement("CONTRIBUTIONS")
                writer.WriteAttributeString("ID", fdc_id)
                maat_id = error_table(idx).maat_id
                writer.WriteElementString("MAAT_ID", maat_id)
            End If

            error_text = error_table(idx).error_text
            writer.WriteElementString("ERROR_TEXT", error_text)

            If idx = no_of_errors Then
                writer.WriteEndElement()
                writer.WriteEndElement()
            ElseIf fdc_id <> error_table(idx + 1).fdc_id Then
                writer.WriteEndElement()
            End If
        Next
        writer.WriteEndElement()
    End Sub
End Class

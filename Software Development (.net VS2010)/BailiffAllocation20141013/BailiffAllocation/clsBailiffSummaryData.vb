﻿Imports CommonLibrary

Public Class clsBailiffSummaryData

    Private LastLoadDT As New DataTable
    Private GridDT As New DataTable
    Private DetailDT As New DataTable
    Private CompanyDT As New DataTable
    Private PeriodTypeDT As New DataTable
    Private SelectedBailiffTypeDT As New DataTable ' added TS 24/Apr/2013 Portal task ref 14922

    Private LastLoadDV As New DataView
    Private GridDV As New DataView
    Private DetailDV As New DataView
    Private CompanyDV As New DataView
    Private PeriodTypeDV As New DataView
    Private SelectedBailiffTypeDV As New DataView ' added TS 24/Apr/2013 Portal task ref 14922

    Public Sub New()

        LoadDataTable("BailiffAllocation", "EXEC dbo.GetPeriodTypes 'B'", PeriodTypeDT)
        PeriodTypeDV = New DataView(PeriodTypeDT)
        PeriodTypeDV.AllowDelete = False
        PeriodTypeDV.AllowNew = False

        LoadDataTable("BailiffAllocation", "EXEC dbo.GetCompanies", CompanyDT)
        CompanyDV = New DataView(CompanyDT)
        CompanyDV.AllowDelete = False
        CompanyDV.AllowNew = False

    End Sub

    'Protected Overrides Sub Finalize()
    '    If Not IsNothing(LastLoadDV) Then LastLoadDV.Dispose()

    '    If Not IsNothing(GridDV) Then GridDV.Dispose()
    '    If Not IsNothing(DetailDV) Then DetailDV.Dispose()
    '    If Not IsNothing(PeriodTypeDV) Then PeriodTypeDV.Dispose()

    '    If Not IsNothing(SelectedBailiffTypeDV) Then SelectedBailiffTypeDV.Dispose() ' added TS 24/Apr/2013 Portal task ref 14922
    '    If Not IsNothing(ConsortiumDV) Then ConsortiumDV.Dispose()

    '    'If Not IsNothing(SummaryDS) Then SummaryDS.Dispose() : SummaryDS = Nothing
    '    MyBase.Finalize()
    'End Sub

#Region " Public Properties"

    Public ReadOnly Property LastLoadDataView() As DataView
        Get
            LastLoadDataView = LastLoadDV
        End Get
    End Property

    Public ReadOnly Property GridDataView() As DataView
        Get
            GridDataView = GridDV
        End Get
    End Property

    Public ReadOnly Property DetailDataView() As DataView
        Get
            DetailDataView = DetailDV
        End Get
    End Property

    Public ReadOnly Property CompanyDataView() As DataView
        Get
            CompanyDataView = CompanyDV
        End Get
    End Property

    Public ReadOnly Property PeriodTypeDataView() As DataView
        Get
            PeriodTypeDataView = PeriodTypeDV
        End Get
    End Property

    Public ReadOnly Property SelectedBailiffTypeDataView() As DataView ' added TS 24/Apr/2013 Portal task ref 14922
        Get
            SelectedBailiffTypeDataView = SelectedBailiffTypeDV
        End Get
    End Property

#End Region

#Region "Public Methods"

    Public Sub GetSummary(ByVal ParamList As String, ByVal Abs As Boolean, ByVal DisplaySet As String)
        Try
            GridDV = Nothing ' Needed to clear column list otherwise new columns eg clientname are added at the end
            'If TopClients Then ParamList += ",1" Else ParamList += ",0"
            If Abs Then ParamList += ",'A'" Else ParamList += ",'P'"

            ParamList += "," & DisplaySet
            GridDT.Clear()
            GridDT.Reset()

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffSummary " & ParamList, GridDT)
            GridDV = New DataView(GridDT)
            GridDV.AllowDelete = False
            GridDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetLastLoad()
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetLastLoad", LastLoadDT)
            LastLoadDV = New DataView(LastLoadDT)
            LastLoadDV.AllowDelete = False
            LastLoadDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub RefreshDatabase()
        Try
            ExecStoredProc("BailiffAllocation", "EXEC dbo.BuildBailiffAllocation 'I'", 1800)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetSelectedBailiffTypes(ByVal ParamList As String) ' added TS 24/Apr/2013 Portal task ref 14922
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetSelectedBailiffTypes " & ParamList, SelectedBailiffTypeDT)
            SelectedBailiffTypeDV = New DataView(SelectedBailiffTypeDT)
            SelectedBailiffTypeDV.AllowDelete = False
            SelectedBailiffTypeDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCaseSummary
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCaseSummary))
        Me.dgvSummary = New System.Windows.Forms.DataGridView()
        Me.dgvStageName = New System.Windows.Forms.DataGridView()
        Me.StageName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colStageNameTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvClientName = New System.Windows.Forms.DataGridView()
        Me.ClientName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colClientNameTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvPayment = New System.Windows.Forms.DataGridView()
        Me.Payment = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvCGA = New System.Windows.Forms.DataGridView()
        Me.CGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvNumberOfVisits = New System.Windows.Forms.DataGridView()
        Me.NumberOfVisits = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvAllocated = New System.Windows.Forms.DataGridView()
        Me.Allocated = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvSchemeName = New System.Windows.Forms.DataGridView()
        Me.SchemeName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdStageAll = New System.Windows.Forms.Button()
        Me.cmdStageClear = New System.Windows.Forms.Button()
        Me.cmdAllocatedClear = New System.Windows.Forms.Button()
        Me.cmdAllocatedAll = New System.Windows.Forms.Button()
        Me.cmdClientClear = New System.Windows.Forms.Button()
        Me.cmdClientAll = New System.Windows.Forms.Button()
        Me.cmdSchemeClear = New System.Windows.Forms.Button()
        Me.cmdSchemeAll = New System.Windows.Forms.Button()
        Me.cmdCGAClear = New System.Windows.Forms.Button()
        Me.cmdCGAAll = New System.Windows.Forms.Button()
        Me.cmdNumberOfVisitsClear = New System.Windows.Forms.Button()
        Me.cmdNumberOfVisitsAll = New System.Windows.Forms.Button()
        Me.cmdPaymentClear = New System.Windows.Forms.Button()
        Me.cmdPaymentAll = New System.Windows.Forms.Button()
        Me.cmdAddConfirmedClear = New System.Windows.Forms.Button()
        Me.cmdAddConfirmedAll = New System.Windows.Forms.Button()
        Me.dgvAddConfirmed = New System.Windows.Forms.DataGridView()
        Me.AddConfirmed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdClearAll = New System.Windows.Forms.Button()
        Me.lblLine1 = New System.Windows.Forms.Label()
        Me.pnlAbsPerc = New System.Windows.Forms.Panel()
        Me.radPerc = New System.Windows.Forms.RadioButton()
        Me.radAbs = New System.Windows.Forms.RadioButton()
        Me.chkTopClients = New System.Windows.Forms.CheckBox()
        Me.dgvWorkType = New System.Windows.Forms.DataGridView()
        Me.WorkType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdWorkTypeClear = New System.Windows.Forms.Button()
        Me.cmdWorkTypeAll = New System.Windows.Forms.Button()
        Me.cmdRefreshDB = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmsSummary = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmdInYearClear = New System.Windows.Forms.Button()
        Me.cmdInYearAll = New System.Windows.Forms.Button()
        Me.dgvInYear = New System.Windows.Forms.DataGridView()
        Me.InYear = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bxtnCoveredAll = New System.Windows.Forms.Button()
        Me.dgvPostcodeArea = New System.Windows.Forms.DataGridView()
        Me.PostcodeArea = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdPostcodeAreaClear = New System.Windows.Forms.Button()
        Me.cmdPostcodeAreaAll = New System.Windows.Forms.Button()
        Me.cmsList = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cboPeriodType = New System.Windows.Forms.ComboBox()
        Me.lblPeriodType = New System.Windows.Forms.Label()
        Me.cboDisplaySet = New System.Windows.Forms.ComboBox()
        Me.cmsForm = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.lblSummary = New System.Windows.Forms.Label()
        Me.cmdArrangementBrokenClear = New System.Windows.Forms.Button()
        Me.cmdArrangementBrokenAll = New System.Windows.Forms.Button()
        Me.dgvArrangementBroken = New System.Windows.Forms.DataGridView()
        Me.ArrangementBroken = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvEnforcementFeesApplied = New System.Windows.Forms.DataGridView()
        Me.EnforcementFeesApplied = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdEnforcementFeesAppliedClear = New System.Windows.Forms.Button()
        Me.cmdEnforcementFeesAppliedAll = New System.Windows.Forms.Button()
        Me.dgvDebtYear = New System.Windows.Forms.DataGridView()
        Me.DebtYear = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdDebtYearClear = New System.Windows.Forms.Button()
        Me.cmdDebtYearAll = New System.Windows.Forms.Button()
        Me.dgvLinkedPIF = New System.Windows.Forms.DataGridView()
        Me.LinkedPIF = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdLinkedPIFClear = New System.Windows.Forms.Button()
        Me.cmdLinkedPIFAll = New System.Windows.Forms.Button()
        Me.RefreshCheckTimer = New System.Windows.Forms.Timer(Me.components)
        Me.cboCompany = New System.Windows.Forms.ComboBox()
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvStageName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCGA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvNumberOfVisits, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAllocated, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSchemeName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAddConfirmed, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAbsPerc.SuspendLayout()
        CType(Me.dgvWorkType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvInYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvArrangementBroken, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEnforcementFeesApplied, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDebtYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLinkedPIF, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvSummary
        '
        Me.dgvSummary.AllowUserToAddRows = False
        Me.dgvSummary.AllowUserToDeleteRows = False
        Me.dgvSummary.AllowUserToOrderColumns = True
        Me.dgvSummary.AllowUserToResizeRows = False
        Me.dgvSummary.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSummary.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSummary.Location = New System.Drawing.Point(7, 367)
        Me.dgvSummary.Name = "dgvSummary"
        Me.dgvSummary.ReadOnly = True
        Me.dgvSummary.RowHeadersVisible = False
        Me.dgvSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvSummary.Size = New System.Drawing.Size(1128, 205)
        Me.dgvSummary.TabIndex = 0
        '
        'dgvStageName
        '
        Me.dgvStageName.AllowUserToAddRows = False
        Me.dgvStageName.AllowUserToDeleteRows = False
        Me.dgvStageName.AllowUserToResizeColumns = False
        Me.dgvStageName.AllowUserToResizeRows = False
        Me.dgvStageName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStageName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StageName, Me.colStageNameTotal})
        Me.dgvStageName.Location = New System.Drawing.Point(351, 7)
        Me.dgvStageName.Name = "dgvStageName"
        Me.dgvStageName.ReadOnly = True
        Me.dgvStageName.RowHeadersVisible = False
        Me.dgvStageName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvStageName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvStageName.Size = New System.Drawing.Size(160, 297)
        Me.dgvStageName.TabIndex = 1
        '
        'StageName
        '
        Me.StageName.DataPropertyName = "StageName"
        Me.StageName.HeaderText = "Stage"
        Me.StageName.Name = "StageName"
        Me.StageName.ReadOnly = True
        Me.StageName.Width = 90
        '
        'colStageNameTotal
        '
        Me.colStageNameTotal.DataPropertyName = "Total"
        Me.colStageNameTotal.HeaderText = "Total"
        Me.colStageNameTotal.Name = "colStageNameTotal"
        Me.colStageNameTotal.ReadOnly = True
        Me.colStageNameTotal.Width = 40
        '
        'dgvClientName
        '
        Me.dgvClientName.AllowUserToAddRows = False
        Me.dgvClientName.AllowUserToDeleteRows = False
        Me.dgvClientName.AllowUserToResizeColumns = False
        Me.dgvClientName.AllowUserToResizeRows = False
        Me.dgvClientName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClientName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClientName, Me.colClientNameTotal})
        Me.dgvClientName.Location = New System.Drawing.Point(7, 29)
        Me.dgvClientName.Name = "dgvClientName"
        Me.dgvClientName.ReadOnly = True
        Me.dgvClientName.RowHeadersVisible = False
        Me.dgvClientName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClientName.Size = New System.Drawing.Size(160, 275)
        Me.dgvClientName.TabIndex = 2
        '
        'ClientName
        '
        Me.ClientName.DataPropertyName = "ClientName"
        Me.ClientName.HeaderText = "Client"
        Me.ClientName.Name = "ClientName"
        Me.ClientName.ReadOnly = True
        '
        'colClientNameTotal
        '
        Me.colClientNameTotal.DataPropertyName = "Total"
        Me.colClientNameTotal.HeaderText = "Total"
        Me.colClientNameTotal.Name = "colClientNameTotal"
        Me.colClientNameTotal.ReadOnly = True
        Me.colClientNameTotal.Width = 40
        '
        'dgvPayment
        '
        Me.dgvPayment.AllowUserToAddRows = False
        Me.dgvPayment.AllowUserToDeleteRows = False
        Me.dgvPayment.AllowUserToResizeColumns = False
        Me.dgvPayment.AllowUserToResizeRows = False
        Me.dgvPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPayment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Payment, Me.DataGridViewTextBoxColumn2})
        Me.dgvPayment.Location = New System.Drawing.Point(777, 145)
        Me.dgvPayment.Name = "dgvPayment"
        Me.dgvPayment.ReadOnly = True
        Me.dgvPayment.RowHeadersVisible = False
        Me.dgvPayment.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvPayment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPayment.Size = New System.Drawing.Size(112, 65)
        Me.dgvPayment.TabIndex = 3
        '
        'Payment
        '
        Me.Payment.DataPropertyName = "Payment"
        Me.Payment.HeaderText = "Payment"
        Me.Payment.Name = "Payment"
        Me.Payment.ReadOnly = True
        Me.Payment.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 40
        '
        'dgvCGA
        '
        Me.dgvCGA.AllowUserToAddRows = False
        Me.dgvCGA.AllowUserToDeleteRows = False
        Me.dgvCGA.AllowUserToResizeColumns = False
        Me.dgvCGA.AllowUserToResizeRows = False
        Me.dgvCGA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCGA.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CGA, Me.DataGridViewTextBoxColumn4})
        Me.dgvCGA.Location = New System.Drawing.Point(900, 7)
        Me.dgvCGA.Name = "dgvCGA"
        Me.dgvCGA.ReadOnly = True
        Me.dgvCGA.RowHeadersVisible = False
        Me.dgvCGA.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvCGA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCGA.Size = New System.Drawing.Size(112, 65)
        Me.dgvCGA.TabIndex = 4
        '
        'CGA
        '
        Me.CGA.DataPropertyName = "CGA"
        Me.CGA.HeaderText = "CGA"
        Me.CGA.Name = "CGA"
        Me.CGA.ReadOnly = True
        Me.CGA.Width = 70
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 40
        '
        'dgvNumberOfVisits
        '
        Me.dgvNumberOfVisits.AllowUserToAddRows = False
        Me.dgvNumberOfVisits.AllowUserToDeleteRows = False
        Me.dgvNumberOfVisits.AllowUserToResizeColumns = False
        Me.dgvNumberOfVisits.AllowUserToResizeRows = False
        Me.dgvNumberOfVisits.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvNumberOfVisits.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumberOfVisits, Me.DataGridViewTextBoxColumn6})
        Me.dgvNumberOfVisits.Location = New System.Drawing.Point(900, 98)
        Me.dgvNumberOfVisits.Name = "dgvNumberOfVisits"
        Me.dgvNumberOfVisits.ReadOnly = True
        Me.dgvNumberOfVisits.RowHeadersVisible = False
        Me.dgvNumberOfVisits.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvNumberOfVisits.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvNumberOfVisits.Size = New System.Drawing.Size(112, 206)
        Me.dgvNumberOfVisits.TabIndex = 6
        '
        'NumberOfVisits
        '
        Me.NumberOfVisits.DataPropertyName = "NumberOfVisits"
        Me.NumberOfVisits.HeaderText = "# Visits"
        Me.NumberOfVisits.Name = "NumberOfVisits"
        Me.NumberOfVisits.ReadOnly = True
        Me.NumberOfVisits.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.NumberOfVisits.Width = 70
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 40
        '
        'dgvAllocated
        '
        Me.dgvAllocated.AllowUserToAddRows = False
        Me.dgvAllocated.AllowUserToDeleteRows = False
        Me.dgvAllocated.AllowUserToResizeColumns = False
        Me.dgvAllocated.AllowUserToResizeRows = False
        Me.dgvAllocated.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAllocated.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Allocated, Me.DataGridViewTextBoxColumn8})
        Me.dgvAllocated.Location = New System.Drawing.Point(1023, 7)
        Me.dgvAllocated.Name = "dgvAllocated"
        Me.dgvAllocated.ReadOnly = True
        Me.dgvAllocated.RowHeadersVisible = False
        Me.dgvAllocated.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvAllocated.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAllocated.Size = New System.Drawing.Size(112, 65)
        Me.dgvAllocated.TabIndex = 5
        '
        'Allocated
        '
        Me.Allocated.DataPropertyName = "Allocated"
        Me.Allocated.HeaderText = "Allocated"
        Me.Allocated.Name = "Allocated"
        Me.Allocated.ReadOnly = True
        Me.Allocated.Width = 70
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 40
        '
        'dgvSchemeName
        '
        Me.dgvSchemeName.AllowUserToAddRows = False
        Me.dgvSchemeName.AllowUserToDeleteRows = False
        Me.dgvSchemeName.AllowUserToResizeColumns = False
        Me.dgvSchemeName.AllowUserToResizeRows = False
        Me.dgvSchemeName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSchemeName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SchemeName, Me.Total})
        Me.dgvSchemeName.Location = New System.Drawing.Point(179, 148)
        Me.dgvSchemeName.Name = "dgvSchemeName"
        Me.dgvSchemeName.ReadOnly = True
        Me.dgvSchemeName.RowHeadersVisible = False
        Me.dgvSchemeName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSchemeName.Size = New System.Drawing.Size(160, 156)
        Me.dgvSchemeName.TabIndex = 7
        '
        'SchemeName
        '
        Me.SchemeName.DataPropertyName = "SchemeName"
        Me.SchemeName.HeaderText = "Scheme"
        Me.SchemeName.Name = "SchemeName"
        Me.SchemeName.ReadOnly = True
        '
        'Total
        '
        Me.Total.DataPropertyName = "Total"
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        Me.Total.Width = 40
        '
        'cmdStageAll
        '
        Me.cmdStageAll.Location = New System.Drawing.Point(375, 304)
        Me.cmdStageAll.Name = "cmdStageAll"
        Me.cmdStageAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageAll.TabIndex = 8
        Me.cmdStageAll.Text = "All"
        Me.cmdStageAll.UseVisualStyleBackColor = True
        '
        'cmdStageClear
        '
        Me.cmdStageClear.Location = New System.Drawing.Point(437, 304)
        Me.cmdStageClear.Name = "cmdStageClear"
        Me.cmdStageClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageClear.TabIndex = 9
        Me.cmdStageClear.Text = "Clear"
        Me.cmdStageClear.UseVisualStyleBackColor = True
        '
        'cmdAllocatedClear
        '
        Me.cmdAllocatedClear.Location = New System.Drawing.Point(1085, 72)
        Me.cmdAllocatedClear.Name = "cmdAllocatedClear"
        Me.cmdAllocatedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdAllocatedClear.TabIndex = 11
        Me.cmdAllocatedClear.Text = "Clear"
        Me.cmdAllocatedClear.UseVisualStyleBackColor = True
        '
        'cmdAllocatedAll
        '
        Me.cmdAllocatedAll.Location = New System.Drawing.Point(1023, 72)
        Me.cmdAllocatedAll.Name = "cmdAllocatedAll"
        Me.cmdAllocatedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdAllocatedAll.TabIndex = 10
        Me.cmdAllocatedAll.Text = "All"
        Me.cmdAllocatedAll.UseVisualStyleBackColor = True
        '
        'cmdClientClear
        '
        Me.cmdClientClear.Location = New System.Drawing.Point(92, 304)
        Me.cmdClientClear.Name = "cmdClientClear"
        Me.cmdClientClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientClear.TabIndex = 13
        Me.cmdClientClear.Text = "Clear"
        Me.cmdClientClear.UseVisualStyleBackColor = True
        '
        'cmdClientAll
        '
        Me.cmdClientAll.Location = New System.Drawing.Point(30, 304)
        Me.cmdClientAll.Name = "cmdClientAll"
        Me.cmdClientAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientAll.TabIndex = 12
        Me.cmdClientAll.Text = "All"
        Me.cmdClientAll.UseVisualStyleBackColor = True
        '
        'cmdSchemeClear
        '
        Me.cmdSchemeClear.Location = New System.Drawing.Point(265, 304)
        Me.cmdSchemeClear.Name = "cmdSchemeClear"
        Me.cmdSchemeClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdSchemeClear.TabIndex = 15
        Me.cmdSchemeClear.Text = "Clear"
        Me.cmdSchemeClear.UseVisualStyleBackColor = True
        '
        'cmdSchemeAll
        '
        Me.cmdSchemeAll.Location = New System.Drawing.Point(203, 304)
        Me.cmdSchemeAll.Name = "cmdSchemeAll"
        Me.cmdSchemeAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdSchemeAll.TabIndex = 14
        Me.cmdSchemeAll.Text = "All"
        Me.cmdSchemeAll.UseVisualStyleBackColor = True
        '
        'cmdCGAClear
        '
        Me.cmdCGAClear.Location = New System.Drawing.Point(962, 72)
        Me.cmdCGAClear.Name = "cmdCGAClear"
        Me.cmdCGAClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdCGAClear.TabIndex = 17
        Me.cmdCGAClear.Text = "Clear"
        Me.cmdCGAClear.UseVisualStyleBackColor = True
        '
        'cmdCGAAll
        '
        Me.cmdCGAAll.Location = New System.Drawing.Point(900, 72)
        Me.cmdCGAAll.Name = "cmdCGAAll"
        Me.cmdCGAAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdCGAAll.TabIndex = 16
        Me.cmdCGAAll.Text = "All"
        Me.cmdCGAAll.UseVisualStyleBackColor = True
        '
        'cmdNumberOfVisitsClear
        '
        Me.cmdNumberOfVisitsClear.Location = New System.Drawing.Point(962, 304)
        Me.cmdNumberOfVisitsClear.Name = "cmdNumberOfVisitsClear"
        Me.cmdNumberOfVisitsClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdNumberOfVisitsClear.TabIndex = 19
        Me.cmdNumberOfVisitsClear.Text = "Clear"
        Me.cmdNumberOfVisitsClear.UseVisualStyleBackColor = True
        '
        'cmdNumberOfVisitsAll
        '
        Me.cmdNumberOfVisitsAll.Location = New System.Drawing.Point(900, 304)
        Me.cmdNumberOfVisitsAll.Name = "cmdNumberOfVisitsAll"
        Me.cmdNumberOfVisitsAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdNumberOfVisitsAll.TabIndex = 18
        Me.cmdNumberOfVisitsAll.Text = "All"
        Me.cmdNumberOfVisitsAll.UseVisualStyleBackColor = True
        '
        'cmdPaymentClear
        '
        Me.cmdPaymentClear.Location = New System.Drawing.Point(839, 210)
        Me.cmdPaymentClear.Name = "cmdPaymentClear"
        Me.cmdPaymentClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPaymentClear.TabIndex = 21
        Me.cmdPaymentClear.Text = "Clear"
        Me.cmdPaymentClear.UseVisualStyleBackColor = True
        '
        'cmdPaymentAll
        '
        Me.cmdPaymentAll.Location = New System.Drawing.Point(777, 210)
        Me.cmdPaymentAll.Name = "cmdPaymentAll"
        Me.cmdPaymentAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPaymentAll.TabIndex = 20
        Me.cmdPaymentAll.Text = "All"
        Me.cmdPaymentAll.UseVisualStyleBackColor = True
        '
        'cmdAddConfirmedClear
        '
        Me.cmdAddConfirmedClear.Location = New System.Drawing.Point(1085, 304)
        Me.cmdAddConfirmedClear.Name = "cmdAddConfirmedClear"
        Me.cmdAddConfirmedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdAddConfirmedClear.TabIndex = 24
        Me.cmdAddConfirmedClear.Text = "Clear"
        Me.cmdAddConfirmedClear.UseVisualStyleBackColor = True
        '
        'cmdAddConfirmedAll
        '
        Me.cmdAddConfirmedAll.Location = New System.Drawing.Point(1023, 304)
        Me.cmdAddConfirmedAll.Name = "cmdAddConfirmedAll"
        Me.cmdAddConfirmedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdAddConfirmedAll.TabIndex = 23
        Me.cmdAddConfirmedAll.Text = "All"
        Me.cmdAddConfirmedAll.UseVisualStyleBackColor = True
        '
        'dgvAddConfirmed
        '
        Me.dgvAddConfirmed.AllowUserToAddRows = False
        Me.dgvAddConfirmed.AllowUserToDeleteRows = False
        Me.dgvAddConfirmed.AllowUserToResizeColumns = False
        Me.dgvAddConfirmed.AllowUserToResizeRows = False
        Me.dgvAddConfirmed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAddConfirmed.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AddConfirmed, Me.DataGridViewTextBoxColumn3})
        Me.dgvAddConfirmed.Location = New System.Drawing.Point(1023, 239)
        Me.dgvAddConfirmed.Name = "dgvAddConfirmed"
        Me.dgvAddConfirmed.ReadOnly = True
        Me.dgvAddConfirmed.RowHeadersVisible = False
        Me.dgvAddConfirmed.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvAddConfirmed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAddConfirmed.Size = New System.Drawing.Size(112, 65)
        Me.dgvAddConfirmed.TabIndex = 22
        '
        'AddConfirmed
        '
        Me.AddConfirmed.DataPropertyName = "AddConfirmed"
        Me.AddConfirmed.HeaderText = "Confirmed"
        Me.AddConfirmed.Name = "AddConfirmed"
        Me.AddConfirmed.ReadOnly = True
        Me.AddConfirmed.Width = 70
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 40
        '
        'cmdClearAll
        '
        Me.cmdClearAll.Location = New System.Drawing.Point(1065, 337)
        Me.cmdClearAll.Name = "cmdClearAll"
        Me.cmdClearAll.Size = New System.Drawing.Size(68, 20)
        Me.cmdClearAll.TabIndex = 25
        Me.cmdClearAll.Text = "Clear All"
        Me.cmdClearAll.UseVisualStyleBackColor = True
        '
        'lblLine1
        '
        Me.lblLine1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLine1.Location = New System.Drawing.Point(7, 329)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(1128, 1)
        Me.lblLine1.TabIndex = 26
        '
        'pnlAbsPerc
        '
        Me.pnlAbsPerc.Controls.Add(Me.radPerc)
        Me.pnlAbsPerc.Controls.Add(Me.radAbs)
        Me.pnlAbsPerc.Location = New System.Drawing.Point(958, 336)
        Me.pnlAbsPerc.Name = "pnlAbsPerc"
        Me.pnlAbsPerc.Size = New System.Drawing.Size(99, 25)
        Me.pnlAbsPerc.TabIndex = 29
        '
        'radPerc
        '
        Me.radPerc.AutoSize = True
        Me.radPerc.Location = New System.Drawing.Point(55, 4)
        Me.radPerc.Name = "radPerc"
        Me.radPerc.Size = New System.Drawing.Size(33, 17)
        Me.radPerc.TabIndex = 4
        Me.radPerc.TabStop = True
        Me.radPerc.Text = "%"
        Me.radPerc.UseVisualStyleBackColor = True
        '
        'radAbs
        '
        Me.radAbs.AutoSize = True
        Me.radAbs.Location = New System.Drawing.Point(6, 4)
        Me.radAbs.Name = "radAbs"
        Me.radAbs.Size = New System.Drawing.Size(43, 17)
        Me.radAbs.TabIndex = 3
        Me.radAbs.TabStop = True
        Me.radAbs.Text = "Abs"
        Me.radAbs.UseVisualStyleBackColor = True
        '
        'chkTopClients
        '
        Me.chkTopClients.AutoSize = True
        Me.chkTopClients.Location = New System.Drawing.Point(202, 341)
        Me.chkTopClients.Name = "chkTopClients"
        Me.chkTopClients.Size = New System.Drawing.Size(79, 17)
        Me.chkTopClients.TabIndex = 30
        Me.chkTopClients.Text = "Top Clients"
        Me.chkTopClients.UseVisualStyleBackColor = True
        '
        'dgvWorkType
        '
        Me.dgvWorkType.AllowUserToAddRows = False
        Me.dgvWorkType.AllowUserToDeleteRows = False
        Me.dgvWorkType.AllowUserToResizeColumns = False
        Me.dgvWorkType.AllowUserToResizeRows = False
        Me.dgvWorkType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvWorkType.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WorkType, Me.DataGridViewTextBoxColumn5})
        Me.dgvWorkType.Location = New System.Drawing.Point(179, 7)
        Me.dgvWorkType.Name = "dgvWorkType"
        Me.dgvWorkType.ReadOnly = True
        Me.dgvWorkType.RowHeadersVisible = False
        Me.dgvWorkType.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvWorkType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvWorkType.Size = New System.Drawing.Size(160, 111)
        Me.dgvWorkType.TabIndex = 31
        '
        'WorkType
        '
        Me.WorkType.DataPropertyName = "WorkType"
        Me.WorkType.HeaderText = "Work Type"
        Me.WorkType.Name = "WorkType"
        Me.WorkType.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 40
        '
        'cmdWorkTypeClear
        '
        Me.cmdWorkTypeClear.Location = New System.Drawing.Point(265, 118)
        Me.cmdWorkTypeClear.Name = "cmdWorkTypeClear"
        Me.cmdWorkTypeClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdWorkTypeClear.TabIndex = 33
        Me.cmdWorkTypeClear.Text = "Clear"
        Me.cmdWorkTypeClear.UseVisualStyleBackColor = True
        '
        'cmdWorkTypeAll
        '
        Me.cmdWorkTypeAll.Location = New System.Drawing.Point(203, 118)
        Me.cmdWorkTypeAll.Name = "cmdWorkTypeAll"
        Me.cmdWorkTypeAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdWorkTypeAll.TabIndex = 32
        Me.cmdWorkTypeAll.Text = "All"
        Me.cmdWorkTypeAll.UseVisualStyleBackColor = True
        '
        'cmdRefreshDB
        '
        Me.cmdRefreshDB.Location = New System.Drawing.Point(7, 338)
        Me.cmdRefreshDB.Name = "cmdRefreshDB"
        Me.cmdRefreshDB.Size = New System.Drawing.Size(75, 20)
        Me.cmdRefreshDB.TabIndex = 34
        Me.cmdRefreshDB.Text = "Refresh DB"
        Me.cmdRefreshDB.UseVisualStyleBackColor = True
        '
        'cmsSummary
        '
        Me.cmsSummary.Name = "ContextMenuStrip1"
        Me.cmsSummary.Size = New System.Drawing.Size(61, 4)
        '
        'cmdInYearClear
        '
        Me.cmdInYearClear.Location = New System.Drawing.Point(714, 304)
        Me.cmdInYearClear.Name = "cmdInYearClear"
        Me.cmdInYearClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdInYearClear.TabIndex = 37
        Me.cmdInYearClear.Text = "Clear"
        Me.cmdInYearClear.UseVisualStyleBackColor = True
        '
        'cmdInYearAll
        '
        Me.cmdInYearAll.Location = New System.Drawing.Point(652, 304)
        Me.cmdInYearAll.Name = "cmdInYearAll"
        Me.cmdInYearAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdInYearAll.TabIndex = 36
        Me.cmdInYearAll.Text = "All"
        Me.cmdInYearAll.UseVisualStyleBackColor = True
        '
        'dgvInYear
        '
        Me.dgvInYear.AllowUserToAddRows = False
        Me.dgvInYear.AllowUserToDeleteRows = False
        Me.dgvInYear.AllowUserToResizeColumns = False
        Me.dgvInYear.AllowUserToResizeRows = False
        Me.dgvInYear.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvInYear.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.InYear, Me.DataGridViewTextBoxColumn7})
        Me.dgvInYear.Location = New System.Drawing.Point(650, 239)
        Me.dgvInYear.Name = "dgvInYear"
        Me.dgvInYear.ReadOnly = True
        Me.dgvInYear.RowHeadersVisible = False
        Me.dgvInYear.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvInYear.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvInYear.Size = New System.Drawing.Size(115, 65)
        Me.dgvInYear.TabIndex = 35
        '
        'InYear
        '
        Me.InYear.DataPropertyName = "InYear"
        Me.InYear.HeaderText = "In year"
        Me.InYear.Name = "InYear"
        Me.InYear.ReadOnly = True
        Me.InYear.Width = 55
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 40
        '
        'bxtnCoveredAll
        '
        Me.bxtnCoveredAll.Location = New System.Drawing.Point(360, 160)
        Me.bxtnCoveredAll.Name = "bxtnCoveredAll"
        Me.bxtnCoveredAll.Size = New System.Drawing.Size(50, 20)
        Me.bxtnCoveredAll.TabIndex = 36
        Me.bxtnCoveredAll.Text = "All"
        Me.bxtnCoveredAll.UseVisualStyleBackColor = True
        '
        'dgvPostcodeArea
        '
        Me.dgvPostcodeArea.AllowUserToAddRows = False
        Me.dgvPostcodeArea.AllowUserToDeleteRows = False
        Me.dgvPostcodeArea.AllowUserToResizeColumns = False
        Me.dgvPostcodeArea.AllowUserToResizeRows = False
        Me.dgvPostcodeArea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPostcodeArea.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PostcodeArea, Me.DataGridViewTextBoxColumn9})
        Me.dgvPostcodeArea.Location = New System.Drawing.Point(523, 7)
        Me.dgvPostcodeArea.Name = "dgvPostcodeArea"
        Me.dgvPostcodeArea.ReadOnly = True
        Me.dgvPostcodeArea.RowHeadersVisible = False
        Me.dgvPostcodeArea.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvPostcodeArea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPostcodeArea.Size = New System.Drawing.Size(115, 297)
        Me.dgvPostcodeArea.TabIndex = 38
        '
        'PostcodeArea
        '
        Me.PostcodeArea.DataPropertyName = "PostcodeArea"
        Me.PostcodeArea.HeaderText = "Area"
        Me.PostcodeArea.Name = "PostcodeArea"
        Me.PostcodeArea.ReadOnly = True
        Me.PostcodeArea.Width = 55
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 40
        '
        'cmdPostcodeAreaClear
        '
        Me.cmdPostcodeAreaClear.Location = New System.Drawing.Point(587, 304)
        Me.cmdPostcodeAreaClear.Name = "cmdPostcodeAreaClear"
        Me.cmdPostcodeAreaClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaClear.TabIndex = 40
        Me.cmdPostcodeAreaClear.Text = "Clear"
        Me.cmdPostcodeAreaClear.UseVisualStyleBackColor = True
        '
        'cmdPostcodeAreaAll
        '
        Me.cmdPostcodeAreaAll.Location = New System.Drawing.Point(525, 304)
        Me.cmdPostcodeAreaAll.Name = "cmdPostcodeAreaAll"
        Me.cmdPostcodeAreaAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaAll.TabIndex = 39
        Me.cmdPostcodeAreaAll.Text = "All"
        Me.cmdPostcodeAreaAll.UseVisualStyleBackColor = True
        '
        'cmsList
        '
        Me.cmsList.Name = "ContextMenuStrip1"
        Me.cmsList.Size = New System.Drawing.Size(61, 4)
        '
        'cboPeriodType
        '
        Me.cboPeriodType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriodType.FormattingEnabled = True
        Me.cboPeriodType.Location = New System.Drawing.Point(836, 336)
        Me.cboPeriodType.Name = "cboPeriodType"
        Me.cboPeriodType.Size = New System.Drawing.Size(110, 21)
        Me.cboPeriodType.TabIndex = 41
        '
        'lblPeriodType
        '
        Me.lblPeriodType.Location = New System.Drawing.Point(711, 341)
        Me.lblPeriodType.Name = "lblPeriodType"
        Me.lblPeriodType.Size = New System.Drawing.Size(119, 16)
        Me.lblPeriodType.TabIndex = 42
        Me.lblPeriodType.Text = "based on:"
        Me.lblPeriodType.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboDisplaySet
        '
        Me.cboDisplaySet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisplaySet.FormattingEnabled = True
        Me.cboDisplaySet.Items.AddRange(New Object() {"Months", "Periods"})
        Me.cboDisplaySet.Location = New System.Drawing.Point(711, 336)
        Me.cboDisplaySet.Name = "cboDisplaySet"
        Me.cboDisplaySet.Size = New System.Drawing.Size(59, 21)
        Me.cboDisplaySet.TabIndex = 43
        '
        'cmsForm
        '
        Me.cmsForm.Name = "ContextMenuStrip1"
        Me.cmsForm.Size = New System.Drawing.Size(61, 4)
        '
        'lblSummary
        '
        Me.lblSummary.AutoSize = True
        Me.lblSummary.Location = New System.Drawing.Point(88, 342)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(39, 13)
        Me.lblSummary.TabIndex = 91
        Me.lblSummary.Text = "Label1"
        '
        'cmdArrangementBrokenClear
        '
        Me.cmdArrangementBrokenClear.Location = New System.Drawing.Point(839, 72)
        Me.cmdArrangementBrokenClear.Name = "cmdArrangementBrokenClear"
        Me.cmdArrangementBrokenClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdArrangementBrokenClear.TabIndex = 94
        Me.cmdArrangementBrokenClear.Text = "Clear"
        Me.cmdArrangementBrokenClear.UseVisualStyleBackColor = True
        '
        'cmdArrangementBrokenAll
        '
        Me.cmdArrangementBrokenAll.Location = New System.Drawing.Point(777, 72)
        Me.cmdArrangementBrokenAll.Name = "cmdArrangementBrokenAll"
        Me.cmdArrangementBrokenAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdArrangementBrokenAll.TabIndex = 93
        Me.cmdArrangementBrokenAll.Text = "All"
        Me.cmdArrangementBrokenAll.UseVisualStyleBackColor = True
        '
        'dgvArrangementBroken
        '
        Me.dgvArrangementBroken.AllowUserToAddRows = False
        Me.dgvArrangementBroken.AllowUserToDeleteRows = False
        Me.dgvArrangementBroken.AllowUserToResizeColumns = False
        Me.dgvArrangementBroken.AllowUserToResizeRows = False
        Me.dgvArrangementBroken.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvArrangementBroken.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ArrangementBroken, Me.DataGridViewTextBoxColumn10})
        Me.dgvArrangementBroken.Location = New System.Drawing.Point(777, 7)
        Me.dgvArrangementBroken.Name = "dgvArrangementBroken"
        Me.dgvArrangementBroken.ReadOnly = True
        Me.dgvArrangementBroken.RowHeadersVisible = False
        Me.dgvArrangementBroken.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvArrangementBroken.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvArrangementBroken.Size = New System.Drawing.Size(112, 65)
        Me.dgvArrangementBroken.TabIndex = 92
        '
        'ArrangementBroken
        '
        Me.ArrangementBroken.DataPropertyName = "ArrangementBroken"
        Me.ArrangementBroken.HeaderText = "Broken"
        Me.ArrangementBroken.Name = "ArrangementBroken"
        Me.ArrangementBroken.ReadOnly = True
        Me.ArrangementBroken.Width = 70
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 40
        '
        'dgvEnforcementFeesApplied
        '
        Me.dgvEnforcementFeesApplied.AllowUserToAddRows = False
        Me.dgvEnforcementFeesApplied.AllowUserToDeleteRows = False
        Me.dgvEnforcementFeesApplied.AllowUserToResizeColumns = False
        Me.dgvEnforcementFeesApplied.AllowUserToResizeRows = False
        Me.dgvEnforcementFeesApplied.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvEnforcementFeesApplied.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EnforcementFeesApplied, Me.DataGridViewTextBoxColumn11})
        Me.dgvEnforcementFeesApplied.Location = New System.Drawing.Point(1023, 123)
        Me.dgvEnforcementFeesApplied.Name = "dgvEnforcementFeesApplied"
        Me.dgvEnforcementFeesApplied.ReadOnly = True
        Me.dgvEnforcementFeesApplied.RowHeadersVisible = False
        Me.dgvEnforcementFeesApplied.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvEnforcementFeesApplied.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEnforcementFeesApplied.Size = New System.Drawing.Size(112, 87)
        Me.dgvEnforcementFeesApplied.TabIndex = 95
        '
        'EnforcementFeesApplied
        '
        Me.EnforcementFeesApplied.DataPropertyName = "EnforcementFeesApplied"
        Me.EnforcementFeesApplied.HeaderText = "Enf Fees"
        Me.EnforcementFeesApplied.Name = "EnforcementFeesApplied"
        Me.EnforcementFeesApplied.ReadOnly = True
        Me.EnforcementFeesApplied.Width = 70
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 40
        '
        'cmdEnforcementFeesAppliedClear
        '
        Me.cmdEnforcementFeesAppliedClear.Location = New System.Drawing.Point(1085, 210)
        Me.cmdEnforcementFeesAppliedClear.Name = "cmdEnforcementFeesAppliedClear"
        Me.cmdEnforcementFeesAppliedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdEnforcementFeesAppliedClear.TabIndex = 97
        Me.cmdEnforcementFeesAppliedClear.Text = "Clear"
        Me.cmdEnforcementFeesAppliedClear.UseVisualStyleBackColor = True
        '
        'cmdEnforcementFeesAppliedAll
        '
        Me.cmdEnforcementFeesAppliedAll.Location = New System.Drawing.Point(1023, 210)
        Me.cmdEnforcementFeesAppliedAll.Name = "cmdEnforcementFeesAppliedAll"
        Me.cmdEnforcementFeesAppliedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdEnforcementFeesAppliedAll.TabIndex = 96
        Me.cmdEnforcementFeesAppliedAll.Text = "All"
        Me.cmdEnforcementFeesAppliedAll.UseVisualStyleBackColor = True
        '
        'dgvDebtYear
        '
        Me.dgvDebtYear.AllowUserToAddRows = False
        Me.dgvDebtYear.AllowUserToDeleteRows = False
        Me.dgvDebtYear.AllowUserToResizeColumns = False
        Me.dgvDebtYear.AllowUserToResizeRows = False
        Me.dgvDebtYear.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDebtYear.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DebtYear, Me.DataGridViewTextBoxColumn12})
        Me.dgvDebtYear.Location = New System.Drawing.Point(650, 7)
        Me.dgvDebtYear.Name = "dgvDebtYear"
        Me.dgvDebtYear.ReadOnly = True
        Me.dgvDebtYear.RowHeadersVisible = False
        Me.dgvDebtYear.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvDebtYear.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDebtYear.Size = New System.Drawing.Size(115, 203)
        Me.dgvDebtYear.TabIndex = 98
        '
        'DebtYear
        '
        Me.DebtYear.DataPropertyName = "DebtYear"
        Me.DebtYear.HeaderText = "Year"
        Me.DebtYear.Name = "DebtYear"
        Me.DebtYear.ReadOnly = True
        Me.DebtYear.Width = 55
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 40
        '
        'cmdDebtYearClear
        '
        Me.cmdDebtYearClear.Location = New System.Drawing.Point(714, 210)
        Me.cmdDebtYearClear.Name = "cmdDebtYearClear"
        Me.cmdDebtYearClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdDebtYearClear.TabIndex = 100
        Me.cmdDebtYearClear.Text = "Clear"
        Me.cmdDebtYearClear.UseVisualStyleBackColor = True
        '
        'cmdDebtYearAll
        '
        Me.cmdDebtYearAll.Location = New System.Drawing.Point(652, 210)
        Me.cmdDebtYearAll.Name = "cmdDebtYearAll"
        Me.cmdDebtYearAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdDebtYearAll.TabIndex = 99
        Me.cmdDebtYearAll.Text = "All"
        Me.cmdDebtYearAll.UseVisualStyleBackColor = True
        '
        'dgvLinkedPIF
        '
        Me.dgvLinkedPIF.AllowUserToAddRows = False
        Me.dgvLinkedPIF.AllowUserToDeleteRows = False
        Me.dgvLinkedPIF.AllowUserToResizeColumns = False
        Me.dgvLinkedPIF.AllowUserToResizeRows = False
        Me.dgvLinkedPIF.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLinkedPIF.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LinkedPIF, Me.DataGridViewTextBoxColumn13})
        Me.dgvLinkedPIF.Location = New System.Drawing.Point(777, 239)
        Me.dgvLinkedPIF.Name = "dgvLinkedPIF"
        Me.dgvLinkedPIF.ReadOnly = True
        Me.dgvLinkedPIF.RowHeadersVisible = False
        Me.dgvLinkedPIF.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvLinkedPIF.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLinkedPIF.Size = New System.Drawing.Size(112, 65)
        Me.dgvLinkedPIF.TabIndex = 101
        '
        'LinkedPIF
        '
        Me.LinkedPIF.DataPropertyName = "LinkedPIF"
        Me.LinkedPIF.HeaderText = "Linked PIF"
        Me.LinkedPIF.Name = "LinkedPIF"
        Me.LinkedPIF.ReadOnly = True
        Me.LinkedPIF.Width = 70
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Width = 40
        '
        'cmdLinkedPIFClear
        '
        Me.cmdLinkedPIFClear.Location = New System.Drawing.Point(839, 304)
        Me.cmdLinkedPIFClear.Name = "cmdLinkedPIFClear"
        Me.cmdLinkedPIFClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedPIFClear.TabIndex = 103
        Me.cmdLinkedPIFClear.Text = "Clear"
        Me.cmdLinkedPIFClear.UseVisualStyleBackColor = True
        '
        'cmdLinkedPIFAll
        '
        Me.cmdLinkedPIFAll.Location = New System.Drawing.Point(777, 304)
        Me.cmdLinkedPIFAll.Name = "cmdLinkedPIFAll"
        Me.cmdLinkedPIFAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedPIFAll.TabIndex = 102
        Me.cmdLinkedPIFAll.Text = "All"
        Me.cmdLinkedPIFAll.UseVisualStyleBackColor = True
        '
        'RefreshCheckTimer
        '
        Me.RefreshCheckTimer.Interval = 30000
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(7, 7)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(160, 21)
        Me.cboCompany.TabIndex = 104
        '
        'frmCaseSummary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.ClientSize = New System.Drawing.Size(1141, 579)
        Me.Controls.Add(Me.cboCompany)
        Me.Controls.Add(Me.dgvDebtYear)
        Me.Controls.Add(Me.cmdDebtYearClear)
        Me.Controls.Add(Me.dgvLinkedPIF)
        Me.Controls.Add(Me.cmdArrangementBrokenClear)
        Me.Controls.Add(Me.cmdDebtYearAll)
        Me.Controls.Add(Me.lblSummary)
        Me.Controls.Add(Me.dgvArrangementBroken)
        Me.Controls.Add(Me.cboDisplaySet)
        Me.Controls.Add(Me.cmdArrangementBrokenAll)
        Me.Controls.Add(Me.cmdLinkedPIFClear)
        Me.Controls.Add(Me.cmdLinkedPIFAll)
        Me.Controls.Add(Me.lblPeriodType)
        Me.Controls.Add(Me.dgvEnforcementFeesApplied)
        Me.Controls.Add(Me.cboPeriodType)
        Me.Controls.Add(Me.dgvPostcodeArea)
        Me.Controls.Add(Me.cmdInYearClear)
        Me.Controls.Add(Me.dgvInYear)
        Me.Controls.Add(Me.cmdInYearAll)
        Me.Controls.Add(Me.dgvWorkType)
        Me.Controls.Add(Me.cmdRefreshDB)
        Me.Controls.Add(Me.cmdPostcodeAreaClear)
        Me.Controls.Add(Me.cmdEnforcementFeesAppliedClear)
        Me.Controls.Add(Me.cmdPostcodeAreaAll)
        Me.Controls.Add(Me.cmdEnforcementFeesAppliedAll)
        Me.Controls.Add(Me.cmdWorkTypeClear)
        Me.Controls.Add(Me.chkTopClients)
        Me.Controls.Add(Me.pnlAbsPerc)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdPaymentClear)
        Me.Controls.Add(Me.cmdWorkTypeAll)
        Me.Controls.Add(Me.cmdClearAll)
        Me.Controls.Add(Me.cmdSchemeClear)
        Me.Controls.Add(Me.cmdPaymentAll)
        Me.Controls.Add(Me.cmdStageClear)
        Me.Controls.Add(Me.cmdStageAll)
        Me.Controls.Add(Me.cmdClientClear)
        Me.Controls.Add(Me.dgvAddConfirmed)
        Me.Controls.Add(Me.dgvSchemeName)
        Me.Controls.Add(Me.cmdCGAClear)
        Me.Controls.Add(Me.cmdCGAAll)
        Me.Controls.Add(Me.cmdNumberOfVisitsAll)
        Me.Controls.Add(Me.cmdClientAll)
        Me.Controls.Add(Me.cmdSchemeAll)
        Me.Controls.Add(Me.cmdNumberOfVisitsClear)
        Me.Controls.Add(Me.dgvClientName)
        Me.Controls.Add(Me.dgvStageName)
        Me.Controls.Add(Me.cmdAddConfirmedClear)
        Me.Controls.Add(Me.cmdAddConfirmedAll)
        Me.Controls.Add(Me.dgvPayment)
        Me.Controls.Add(Me.dgvSummary)
        Me.Controls.Add(Me.cmdAllocatedAll)
        Me.Controls.Add(Me.dgvCGA)
        Me.Controls.Add(Me.cmdAllocatedClear)
        Me.Controls.Add(Me.dgvNumberOfVisits)
        Me.Controls.Add(Me.dgvAllocated)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCaseSummary"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Allocation Summary"
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvStageName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPayment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCGA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvNumberOfVisits, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAllocated, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSchemeName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAddConfirmed, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAbsPerc.ResumeLayout(False)
        Me.pnlAbsPerc.PerformLayout()
        CType(Me.dgvWorkType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvInYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvArrangementBroken, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEnforcementFeesApplied, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDebtYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLinkedPIF, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvSummary As System.Windows.Forms.DataGridView
    Friend WithEvents dgvStageName As System.Windows.Forms.DataGridView
    Friend WithEvents dgvClientName As System.Windows.Forms.DataGridView
    Friend WithEvents dgvPayment As System.Windows.Forms.DataGridView
    Friend WithEvents dgvCGA As System.Windows.Forms.DataGridView
    Friend WithEvents dgvNumberOfVisits As System.Windows.Forms.DataGridView
    Friend WithEvents dgvAllocated As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSchemeName As System.Windows.Forms.DataGridView
    Friend WithEvents ClientName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClientNameTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StageName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colStageNameTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdStageAll As System.Windows.Forms.Button
    Friend WithEvents cmdStageClear As System.Windows.Forms.Button
    Friend WithEvents cmdAllocatedClear As System.Windows.Forms.Button
    Friend WithEvents cmdAllocatedAll As System.Windows.Forms.Button
    Friend WithEvents cmdClientClear As System.Windows.Forms.Button
    Friend WithEvents cmdClientAll As System.Windows.Forms.Button
    Friend WithEvents cmdSchemeClear As System.Windows.Forms.Button
    Friend WithEvents cmdSchemeAll As System.Windows.Forms.Button
    Friend WithEvents cmdCGAClear As System.Windows.Forms.Button
    Friend WithEvents cmdCGAAll As System.Windows.Forms.Button
    Friend WithEvents cmdNumberOfVisitsClear As System.Windows.Forms.Button
    Friend WithEvents cmdNumberOfVisitsAll As System.Windows.Forms.Button
    Friend WithEvents cmdPaymentClear As System.Windows.Forms.Button
    Friend WithEvents cmdPaymentAll As System.Windows.Forms.Button
    Friend WithEvents cmdAddConfirmedClear As System.Windows.Forms.Button
    Friend WithEvents cmdAddConfirmedAll As System.Windows.Forms.Button
    Friend WithEvents dgvAddConfirmed As System.Windows.Forms.DataGridView
    Friend WithEvents cmdClearAll As System.Windows.Forms.Button
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents pnlAbsPerc As System.Windows.Forms.Panel
    Friend WithEvents radPerc As System.Windows.Forms.RadioButton
    Friend WithEvents radAbs As System.Windows.Forms.RadioButton
    Friend WithEvents chkTopClients As System.Windows.Forms.CheckBox
    Friend WithEvents dgvWorkType As System.Windows.Forms.DataGridView
    Friend WithEvents cmdWorkTypeClear As System.Windows.Forms.Button
    Friend WithEvents cmdWorkTypeAll As System.Windows.Forms.Button
    Friend WithEvents cmdRefreshDB As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cmsSummary As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmdInYearClear As System.Windows.Forms.Button
    Friend WithEvents cmdInYearAll As System.Windows.Forms.Button
    Friend WithEvents dgvInYear As System.Windows.Forms.DataGridView
    Friend WithEvents bxtnCoveredAll As System.Windows.Forms.Button
    Friend WithEvents SchemeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvPostcodeArea As System.Windows.Forms.DataGridView
    Friend WithEvents cmdPostcodeAreaClear As System.Windows.Forms.Button
    Friend WithEvents cmdPostcodeAreaAll As System.Windows.Forms.Button
    Friend WithEvents PostcodeArea As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmsList As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cboPeriodType As System.Windows.Forms.ComboBox
    Friend WithEvents lblPeriodType As System.Windows.Forms.Label
    Friend WithEvents cboDisplaySet As System.Windows.Forms.ComboBox
    Friend WithEvents cmsForm As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents cmdArrangementBrokenClear As System.Windows.Forms.Button
    Friend WithEvents cmdArrangementBrokenAll As System.Windows.Forms.Button
    Friend WithEvents dgvArrangementBroken As System.Windows.Forms.DataGridView
    Friend WithEvents dgvEnforcementFeesApplied As System.Windows.Forms.DataGridView
    Friend WithEvents cmdEnforcementFeesAppliedClear As System.Windows.Forms.Button
    Friend WithEvents cmdEnforcementFeesAppliedAll As System.Windows.Forms.Button
    Friend WithEvents dgvDebtYear As System.Windows.Forms.DataGridView
    Friend WithEvents cmdDebtYearClear As System.Windows.Forms.Button
    Friend WithEvents cmdDebtYearAll As System.Windows.Forms.Button
    Friend WithEvents DebtYear As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvLinkedPIF As System.Windows.Forms.DataGridView
    Friend WithEvents cmdLinkedPIFClear As System.Windows.Forms.Button
    Friend WithEvents cmdLinkedPIFAll As System.Windows.Forms.Button
    Friend WithEvents RefreshCheckTimer As System.Windows.Forms.Timer
    Friend WithEvents InYear As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WorkType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AddConfirmed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Allocated As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Payment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ArrangementBroken As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EnforcementFeesApplied As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkedPIF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents NumberOfVisits As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn

End Class

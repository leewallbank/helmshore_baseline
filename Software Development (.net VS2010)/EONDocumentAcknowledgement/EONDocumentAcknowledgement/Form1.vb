﻿Imports System.IO

Public Class Form1
    Dim ackFileName, zipName, inputFilePath As String
    Dim ascii As New System.Text.ASCIIEncoding()
    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub ackbtn_Click(sender As System.Object, e As System.EventArgs) Handles ackbtn.Click
        With OpenFileDialog1
            .Title = "Read CSV file"
            .Filter = "CSV file|*.csv"
            .FileName = ""
            .CheckFileExists = True
        End With
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("File not opened")
            Me.Close()
            Exit Sub
        End If
        ackFileName = OpenFileDialog1.FileName
        zipbtn.Enabled = True
        ackbtn.Enabled = False
    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub zipbtn_Click(sender As System.Object, e As System.EventArgs) Handles zipbtn.Click
        With OpenFileDialog1
            .Title = "Read ZIP file"
            .Filter = "ZIP file|*.zip"
            .FileName = ""
            .CheckFileExists = True
        End With
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("File not opened")
            Me.Close()
            Exit Sub
        End If
        zipbtn.Enabled = False
        exitbtn.Enabled = False
        zipName = OpenFileDialog1.FileName
        'zipName = FolderBrowserDialog1.SelectedPath
        'copy file to H drive
        Dim copyZipFileName As String = "H:\EONCopyZipFile.zip"
        My.Computer.FileSystem.CopyFile(zipName, copyZipFileName, True)
        'With FolderBrowserDialog1

        'End With
        'If FolderBrowserDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
        '    MsgBox("Folder not opened")
        '    Me.Close()
        '    Exit Sub
        'End If
      
        inputFilePath = zipName
        inputFilePath &= "\"
        Dim unzippedFilePath As String = "H:\EONUnZipped\"

        'first delete unzipped folder if exists
        Dim dir_name As String = unzippedFilePath
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = True Then
                System.IO.Directory.Delete(dir_name, True)
            End If
        Catch ex As Exception
            MsgBox("Unable to delete unzipped folder")
            Exit Sub
            End
        End Try

        Dim exePath As String = "C:\Program Files\7-Zip\7z.exe"
        Dim args As String = "e " & copyZipFileName & " -o" & Chr(34) & unzippedFilePath & Chr(34)
        Try
            System.Diagnostics.Process.Start(exePath, args)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        'now see how may documents there are in the folder
        Dim docNo As Integer = 0
        Dim fileExtension As String

        System.Threading.Thread.Sleep(10000)

        For Each foundFile As String In My.Computer.FileSystem.GetFiles(
          unzippedFilePath,
           Microsoft.VisualBasic.FileIO.SearchOption.SearchAllSubDirectories, "*.*")
            fileExtension = Path.GetExtension(foundFile)
            If fileExtension = ".pdf" Or fileExtension = "xls" Or fileExtension = ".xlsx" Then
                docNo += 1
            End If
        Next

        'read in acknowledgement file and update
        Dim InputLineArray() As String
        Dim FileContents() As String = System.IO.File.ReadAllLines(ackFileName)
        'now rename original file
        Try
            My.Computer.FileSystem.RenameFile(ackFileName, "original_acknowledgement.csv")
        Catch ex As Exception

        End Try
        Dim lineNumber As Integer
        Dim acknowledgementFile As String = ""

        'look for last hyphen
        Dim idx As Integer
        For idx = ackFileName.Length To 1 Step -1
            If Mid(ackFileName, idx, 1) = "-" Then
                Exit For
            End If
        Next
        ackFileName = Microsoft.VisualBasic.Left(ackFileName, idx)
        Dim runtime As Date = Now
        ackFileName &= Format(runtime, "ddMMyyyyHHmmss") & ".csv"
        For Each InputLine As String In FileContents
            InputLineArray = InputLine.Split(",")
            lineNumber += 1
            If lineNumber = 2 Then
                InputLineArray(2) = "0.00"
                InputLineArray(3) = docNo
            End If
            'write out new acknowledgement file
            Select Case lineNumber
                Case 1
                    For colIDX = 0 To 2
                        If colIDX < 2 Then
                            acknowledgementFile &= InputLineArray(colIDX) & ","
                        Else
                            acknowledgementFile &= Format(runtime, "yyyy-MM-dd HH:mm:ss") & ","
                        End If
                    Next
                    acknowledgementFile &= Path.GetFileName(ackFileName) & vbNewLine
                Case 2
                    For colIDX = 0 To 2
                        acknowledgementFile &= InputLineArray(colIDX) & ","
                    Next
                    acknowledgementFile &= InputLineArray(3) & vbNewLine
                Case 3
                    acknowledgementFile &= "ROS" & vbNewLine
            End Select

        Next
        My.Computer.FileSystem.WriteAllText(ackFileName, acknowledgementFile, False, ascii)
        ' delete unzipped folder if exists
        'dir_name = unzippedFilePath
        'Try
        '    System.IO.Directory.Delete(dir_name, True)
        'Catch ex As Exception

        'End Try
        MsgBox("Acknowledgement updated")

        Me.Close()
    End Sub
End Class

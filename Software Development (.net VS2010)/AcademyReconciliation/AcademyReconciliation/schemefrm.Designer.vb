<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class schemefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.selected = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.csid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.scheme_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.selected, Me.csid, Me.scheme_no})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(425, 266)
        Me.DataGridView1.TabIndex = 0
        '
        'selected
        '
        Me.selected.HeaderText = "Select"
        Me.selected.Name = "selected"
        Me.selected.Width = 50
        '
        'csid
        '
        Me.csid.HeaderText = "CSID"
        Me.csid.Name = "csid"
        Me.csid.ReadOnly = True
        Me.csid.Width = 70
        '
        'scheme_no
        '
        Me.scheme_no.HeaderText = "Scheme"
        Me.scheme_no.Name = "scheme_no"
        Me.scheme_no.ReadOnly = True
        Me.scheme_no.Width = 250
        '
        'schemefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(425, 266)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "schemefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "schemefrm"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents selected As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents csid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents scheme_no As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

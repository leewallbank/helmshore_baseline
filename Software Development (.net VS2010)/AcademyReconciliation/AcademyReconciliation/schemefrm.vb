Public Class schemefrm

    Private Sub schemefrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'get selected schemes
        Dim idx As Integer = 0
        Dim row As DataGridViewRow
        sch_rows = 0
        ReDim cs_table(10)
        For Each row In DataGridView1.Rows
            If DataGridView1.Rows(idx).Cells(0).Value = True Then
                sch_rows += 1
                cs_table(sch_rows) = DataGridView1.Rows(idx).Cells(1).Value
            End If
            idx += 1
        Next
    End Sub


    Private Sub schemefrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DataGridView1.Rows.Clear()
        'get schemes from onestep
        param1 = "onestep"
        param2 = "select _rowid, schemeID from ClientScheme where clientID = " & cl_id & _
        " and branchID = " & mainfrm.branch_cbox.Text
        Dim csid_dataset As DataSet = get_dataset(param1, param2)
        Dim sch_rows As Integer = no_of_rows
        Dim sch_name As String
        Dim idx, sch_id, cs_id As Integer
        For idx = 0 To sch_rows - 1
            cs_id = csid_dataset.Tables(0).Rows(idx).Item(0)
            sch_id = csid_dataset.Tables(0).Rows(idx).Item(1)
            'get scheme names
            param2 = "select name from Scheme where _rowid = " & sch_id
            Dim sch_dataset As DataSet = get_dataset(param1, param2)
            sch_name = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
            DataGridView1.Rows.Add(False, cs_id, sch_name)
        Next
    End Sub
End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class clientfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.p_cl_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.p_cl_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.p_cl_no, Me.p_cl_name})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(421, 510)
        Me.DataGridView1.TabIndex = 0
        '
        'p_cl_no
        '
        Me.p_cl_no.HeaderText = "Client ID"
        Me.p_cl_no.Name = "p_cl_no"
        Me.p_cl_no.ReadOnly = True
        Me.p_cl_no.Width = 70
        '
        'p_cl_name
        '
        Me.p_cl_name.HeaderText = "Client Name"
        Me.p_cl_name.Name = "p_cl_name"
        Me.p_cl_name.ReadOnly = True
        Me.p_cl_name.Width = 300
        '
        'clientfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(421, 510)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "clientfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Double-click to select Client"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents p_cl_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents p_cl_name As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

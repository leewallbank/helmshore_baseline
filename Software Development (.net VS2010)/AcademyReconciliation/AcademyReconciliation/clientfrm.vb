Public Class clientfrm

    Private Sub clientfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DataGridView1.Rows.Clear()
        'get clients from onestep
        param1 = "onestep"
        param2 = "select C._rowid, C.name from Client C, clientscheme CS " & _
        "  where CS.ClientID = C._rowID " & _
        " and CS.branchID = " & mainfrm.branch_cbox.Text & _
        " and C.name like '" & mainfrm.cl_nametbox.Text & "%'" & _
        " order by name"
        Dim cl_dataset As DataSet = get_dataset(param1, param2)
        Dim cl_id As Integer
        Dim cl_rows As Integer = no_of_rows
        Dim cl_name As String
        'check branch and display branch 
        Dim idx As Integer = 0
        For idx = 0 To cl_rows - 1
            cl_id = cl_dataset.Tables(0).Rows(idx).Item(0)
            cl_name = Trim(cl_dataset.Tables(0).Rows(idx).Item(1))
            DataGridView1.Rows.Add(cl_id, cl_name)
        Next
    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        Me.Close()
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter
        cl_id = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        cl_name = DataGridView1.Rows(e.RowIndex).Cells(1).Value
    End Sub
End Class
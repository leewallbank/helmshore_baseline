﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.lblReadingFile = New System.Windows.Forms.Label()
        Me.btnViewLogFile = New System.Windows.Forms.Button()
        Me.btnViewOutputFile = New System.Windows.Forms.Button()
        Me.btnViewInputFile = New System.Windows.Forms.Button()
        Me.btnProcessFile = New System.Windows.Forms.Button()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.btnViewOutputFil = New System.Windows.Forms.Button()
        Me.btnViewOutputFileWRP = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lblReadingFile
        '
        Me.lblReadingFile.AutoSize = True
        Me.lblReadingFile.BackColor = System.Drawing.Color.White
        Me.lblReadingFile.Location = New System.Drawing.Point(112, 304)
        Me.lblReadingFile.Name = "lblReadingFile"
        Me.lblReadingFile.Size = New System.Drawing.Size(72, 13)
        Me.lblReadingFile.TabIndex = 23
        Me.lblReadingFile.Text = "Reading file..."
        Me.lblReadingFile.Visible = False
        '
        'btnViewLogFile
        '
        Me.btnViewLogFile.Enabled = False
        Me.btnViewLogFile.Location = New System.Drawing.Point(68, 237)
        Me.btnViewLogFile.Name = "btnViewLogFile"
        Me.btnViewLogFile.Size = New System.Drawing.Size(153, 42)
        Me.btnViewLogFile.TabIndex = 4
        Me.btnViewLogFile.Text = "View &Log File"
        Me.btnViewLogFile.UseVisualStyleBackColor = True
        '
        'btnViewOutputFile
        '
        Me.btnViewOutputFile.Enabled = False
        Me.btnViewOutputFile.Location = New System.Drawing.Point(68, 125)
        Me.btnViewOutputFile.Name = "btnViewOutputFile"
        Me.btnViewOutputFile.Size = New System.Drawing.Size(153, 42)
        Me.btnViewOutputFile.TabIndex = 2
        Me.btnViewOutputFile.Text = "View &Output File"
        Me.btnViewOutputFile.UseVisualStyleBackColor = True
        '
        'btnViewInputFile
        '
        Me.btnViewInputFile.Enabled = False
        Me.btnViewInputFile.Location = New System.Drawing.Point(68, 77)
        Me.btnViewInputFile.Name = "btnViewInputFile"
        Me.btnViewInputFile.Size = New System.Drawing.Size(153, 42)
        Me.btnViewInputFile.TabIndex = 1
        Me.btnViewInputFile.Text = "View &Input File"
        Me.btnViewInputFile.UseVisualStyleBackColor = True
        '
        'btnProcessFile
        '
        Me.btnProcessFile.Location = New System.Drawing.Point(68, 29)
        Me.btnProcessFile.Name = "btnProcessFile"
        Me.btnProcessFile.Size = New System.Drawing.Size(153, 42)
        Me.btnProcessFile.TabIndex = 0
        Me.btnProcessFile.Text = "&Process File"
        Me.btnProcessFile.UseVisualStyleBackColor = True
        '
        'ProgressBar
        '
        Me.ProgressBar.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock
        Me.ProgressBar.Location = New System.Drawing.Point(12, 320)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(273, 22)
        Me.ProgressBar.TabIndex = 18
        '
        'btnViewOutputFil
        '
        Me.btnViewOutputFil.Enabled = False
        Me.btnViewOutputFil.Location = New System.Drawing.Point(68, 125)
        Me.btnViewOutputFil.Name = "btnViewOutputFil"
        Me.btnViewOutputFil.Size = New System.Drawing.Size(153, 42)
        Me.btnViewOutputFil.TabIndex = 20
        Me.btnViewOutputFil.Text = "View &Output File"
        Me.btnViewOutputFil.UseVisualStyleBackColor = True
        '
        'btnViewOutputFileWRP
        '
        Me.btnViewOutputFileWRP.Enabled = False
        Me.btnViewOutputFileWRP.Location = New System.Drawing.Point(68, 173)
        Me.btnViewOutputFileWRP.Name = "btnViewOutputFileWRP"
        Me.btnViewOutputFileWRP.Size = New System.Drawing.Size(153, 42)
        Me.btnViewOutputFileWRP.TabIndex = 3
        Me.btnViewOutputFileWRP.Text = "View &WRP Output File"
        Me.btnViewOutputFileWRP.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(79, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(156, 13)
        Me.Label1.TabIndex = 24
        Me.Label1.Text = "Splits on Balance <= £180"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(340, 354)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnViewOutputFileWRP)
        Me.Controls.Add(Me.lblReadingFile)
        Me.Controls.Add(Me.btnViewLogFile)
        Me.Controls.Add(Me.btnViewOutputFile)
        Me.Controls.Add(Me.btnViewInputFile)
        Me.Controls.Add(Me.btnProcessFile)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.btnViewOutputFil)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "North East Lincs Split PreProcess"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblReadingFile As System.Windows.Forms.Label
    Friend WithEvents btnViewLogFile As System.Windows.Forms.Button
    Friend WithEvents btnViewOutputFile As System.Windows.Forms.Button
    Friend WithEvents btnViewInputFile As System.Windows.Forms.Button
    Friend WithEvents btnProcessFile As System.Windows.Forms.Button
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents btnViewOutputFil As System.Windows.Forms.Button
    Friend WithEvents btnViewOutputFileWRP As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class

﻿Imports CommonLibrary
Public Class Form1
    Dim type As String
    Dim start_date As Date = CDate("2014 Apr 1")
    Dim upd_txt As String
    Dim startDebtorID As Integer
    Private Sub run_debtorID(ByVal debtorid As Integer, ByVal allocationDate As Date, ByVal stageDate As Date, ByVal bailID As Integer, ByVal deAllocDate As Date, ByVal runType As String)
        Dim days2FirstVisit As Integer = 99999
        Dim visited As Integer = 0
        Dim de_alloc_without_visit As Integer = 99999
        Dim trace_while_allocated As Integer = 99999
        Dim return_while_allocated As Integer = 99999
        'get first visit date
       
        Dim visitDate As Date = Nothing
        Dim visitdateTable As New DataTable

        LoadDataTable2("DebtRecovery", "select date_visited, date_allocated, stageName, bailiffID  from visit" & _
                             " where debtorID = " & debtorid & _
                             " and date_visited is not null " & _
                             " and date_allocated is not null " & _
                             " ORDER BY date_visited", visitdateTable, False)

        For Each visitrow In visitdateTable.Rows
            Dim visitbailiffID As Integer
            Try
                visitbailiffID = visitrow(3)
            Catch ex As Exception
                Continue For
            End Try

            If visitbailiffID <> bailID Then
                Continue For
            End If
            Dim VisitAllocationDate As Date = visitrow(1)
            If Format(VisitAllocationDate, "yyyy-MMM-dd") <> Format(allocationDate, "yyy-MMM-dd") Then
                Continue For
            End If
            visitDate = visitrow(0)
            Exit For
        Next
        'if no visit found see if there is a visit at required stage
        'eg case 8337129 has visit on wrong allocation date
        If visitDate = Nothing And runType = "Van" Then
            For Each visitrow In visitdateTable.Rows
                Dim stagename As String
                Try
                    stagename = LCase(visitrow(2))
                Catch ex As Exception
                    stagename = ""
                End Try
                If InStr(stagename, "van") + InStr(stagename, "further enforcement") > 0 Then
                    visitDate = visitrow(0)
                    allocationDate = visitrow(1)
                    Exit For
                End If
            Next
        End If

        Dim testDays As Integer = 99999
        If visitDate <> Nothing Then
            visitDate = CDate(Format(visitDate, "yyyy-MM-dd") & " 00:00:00")
            allocationDate = CDate(Format(allocationDate, "yyyy-MM-dd") & " 00:00:00")
            Try
                testDays = DateDiff(DateInterval.Day, allocationDate, visitDate)
                If testDays < -10 Then
                    testDays = 99999
                End If
                If testDays < 0 Then
                    testDays = 0
                End If
                If testDays < days2FirstVisit Then
                    days2FirstVisit = testDays
                End If
                If testDays >= 0 And testDays < 99999 Then
                    visited = 1
                End If
            Catch ex As Exception

            End Try
        End If
        If visited = 0 Then

            'no visit so see if bailiff has finished with the case
            If deAllocDate = CDate("Jan 1, 1900") Then
                de_alloc_without_visit = 0
            Else
                de_alloc_without_visit = 1
            End If

            'see if returned or put into trace or allocated while bailiff had case
            Dim note3_dt As New DataTable
            Dim noteText As String
            LoadDataTable2("DebtRecovery", "SELECT type, _createdDate " & _
                                            "FROM note " & _
                                             "WHERE (type = 'Cancelled' or type = 'Trace')" & _
                                          " AND debtorID = " & debtorid & _
                                      " ORDER BY _createdDate", note3_dt, False)
            For Each note3row In note3_dt.Rows
                noteText = note3row(0)
                Dim noteDate As String = note3row(1)
                If noteDate <= allocationDate Then
                    Continue For
                End If
                If noteDate > deAllocDate And deAllocDate <> CDate("Jan 1, 1900") Then
                    Continue For
                End If
                If noteText = "Trace" Then
                    trace_while_allocated = 1
                End If
                If noteText = "Cancelled" Then
                    return_while_allocated = 1
                End If
            Next
        End If

        If visited = 1 Then
            de_alloc_without_visit = 0
            trace_while_allocated = 0
            return_while_allocated = 0
        Else
            If de_alloc_without_visit = 0 Then
                trace_while_allocated = 0
                return_while_allocated = 0
            End If
        End If
        If de_alloc_without_visit = 99999 Then
            de_alloc_without_visit = 0
        End If
        If trace_while_allocated = 1 Or return_while_allocated = 1 Then
            de_alloc_without_visit = 1
        End If
        If trace_while_allocated = 99999 Then
            trace_while_allocated = 0
        End If
        If return_while_allocated = 99999 Then
            return_while_allocated = 0
        End If
        'see if an entry already exist for debtor and runtype
        Dim TCEarray As Object

        TCEarray = GetSQLResultsArray2("FeesSQL", "select TCEdays_to_first_visit, TCEde_alloc_without_visit" & _
                                   " from TCEVisit_stats" & _
                                   " where TCEDebtorID = " & debtorid & _
                                   " and TCEstage = '" & runType & "'")
        Dim TCEdays As Integer
        Dim TCEFound As Boolean = True
        Try
            TCEdays = TCEarray(0)
        Catch ex As Exception
            TCEdays = 99999
            TCEFound = False
        End Try
        If TCEFound Then
            Dim upd_txt As String
            If days2FirstVisit <= TCEdays Then
                If days2FirstVisit = 99999 Then
                    If TCEarray(1) = True Or _
                        de_alloc_without_visit Then
                        'delete prev entry
                        upd_txt = "delete from TCEVisit_stats " & _
                                                " where TCEstage = '" & runType & "'" & _
                                                " and TCEdebtorID = " & debtorid
                        update_sql(upd_txt)
                    Else
                        Exit Sub
                    End If
                Else
                    'delete prev entry
                    upd_txt = "delete from TCEVisit_stats " & _
                                            " where TCEstage = '" & runType & "'" & _
                                            " and TCEdebtorID = " & debtorid
                    update_sql(upd_txt)
                End If
            Else
                Exit Sub
            End If
        End If

        upd_txt = "insert into TCEVisit_stats (TCEdebtorID, " & _
               "TCEstage, TCEdays_to_first_visit, TCEvisited, TCEallocation_date,TCEde_alloc_without_visit, TCEtrace_while_allocated, TCEreturn_while_allocated)" & _
               "values (" & debtorid & ",'" & runType & "'," & days2FirstVisit & "," & visited & "," & _
               "'" & Format(allocationDate, "yyyy-MM-dd") & "'," & de_alloc_without_visit & "," & trace_while_allocated & "," & return_while_allocated & ")"
        update_sql(upd_txt)
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("FeesSQL")


        'first clear down table
        'type = "XX"
        'Dim upd_txt As String = "delete from TCEVisit_stats " & _
        ' "where TCEstage <> '" & type & "'"
        'update_sql(upd_txt)

        'get all cases allocated since 6.4.2014


        Dim debtor_dt As New DataTable

        'get all cases loaded since 6.4.2014

        'RESET START DEBTOR NUMBER AND CLEAR DOWN TABLE
        'Use bailiff allocation table to get allocated cases at stage
        'link to stage table to get stage sort
        'link to debtor and client scheme to get branch
        'all csids >=3856 are new TCE  FC sort=100, Van sort > 100
        'csid < 3856 Compliance sort < 300, enforcement sort >= 300
        startDebtorID = 8813594
        LoadDataTable2("FeesSQL", "SELECT bail_debtorID, bail_ID, bail_allocation_date, bail_stage_name, bail_de_allocation_found_date " & _
                                                "FROM BailiffAllocationTable " & _
                                                "WHERE bail_debtorID > " & startDebtorID & _
                                                " order by bail_debtorID", debtor_dt, False)
        For Each Debtrow In debtor_dt.Rows
            Dim debtorID As Integer = Debtrow(0)
            Dim ClientSchemeID As Integer
            ClientSchemeID = GetSQLResults2("DebtRecovery", "select clientschemeID " & _
                                " from debtor " & _
                                " where _rowid = " & debtorID)

            'check client scheme is branch 1
            Dim branchID As Integer
            Dim CSArray As Object
            CSArray = GetSQLResultsArray2("DebtRecovery", "SELECT branchID, clientID " & _
                                               "FROM clientScheme " & _
                                               "WHERE _rowid= " & ClientSchemeID)
            branchID = CSArray(0)
            If branchID <> 1 Then
                Continue For
            End If
            'ignore test cases
            If CSArray(1) = 1 Or CSArray(1) = 2 Or CSArray(1) = 24 Then
                Continue For
            End If

            'check bailiff is a proper bailiff
            Dim bailID As Integer = Debtrow(1)
            Dim addMobileName As String = "none"
            Try

                addMobileName = GetSQLResults2("DebtRecovery", "select add_mobile_name " & _
                                             " from Bailiff" & _
                                             " where _rowid = " & bailID)
            Catch ex As Exception

            End Try
            If addMobileName = "none" Then
                Continue For
            End If

            'get stage sort
            Dim stageName As String = Debtrow(3)
            Dim stageSort As Integer
            stageSort = GetSQLResults2("DebtRecovery", "select sort " & _
                                     " from stage " & _
                                     " where name = '" & stageName & "'")

            Dim allocationDate As Date = Debtrow.item(2)
            allocationDate = CDate(Format(allocationDate, "dd MMM, yyyy") & " 00:00:00")
            'need to get date case went to the stage
            'note could be more than on entry - choose first before allocation
            'could be none - if csid>= 3856 then assume 7.4.14
            Dim notetable As New DataTable
            LoadDataTable2("DebtRecovery", "select _createdDate " & _
                                          " from Note" & _
                                          " where debtorID = " & debtorID & _
                                          " and type = 'Stage'" & _
                                          " order by _createdDate desc", notetable, False)
            Dim stageDate As Date = Nothing
            For Each noteRow In notetable.Rows

                Try
                    stageDate = noteRow(0)
                Catch ex As Exception

                End Try
                If stageDate > Format(allocationDate, "yyyy-MMM-dd") Then
                    Continue For
                Else
                    Exit For
                End If
            Next
           

            If stageDate = Nothing And ClientSchemeID >= 3856 Then
                stageDate = CDate("Apr 7, 2014")
            End If
            If stageDate = Nothing Then
                MsgBox("stageDate = Nothing for case = " & debtorID)
            End If
            Dim deAllocDate As Date = Debtrow(4)
            Dim runType As String
            If ClientSchemeID < 3856 Or stageDate < CDate("apr 7, 2014") Then
                If stageSort <= 100 Then
                    runType = "FC"
                Else
                    runType = "Van"
                End If
            Else
                If stageSort < 300 Then
                    runType = "FC"
                Else
                    runType = "Van"
                End If
            End If
            run_debtorid(debtorID, allocationDate, stageDate, bailID, deAllocDate, runType)
        Next
        'MsgBox("Completed")
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

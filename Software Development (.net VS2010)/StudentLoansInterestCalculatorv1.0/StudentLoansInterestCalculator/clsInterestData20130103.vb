﻿Imports CommonLibrary

Public Class clsInterestData
    Private Payment As New DataTable, Debtor As New DataTable, Fee As New DataTable
    Private PaymentDV As DataView, DebtorDV As DataView, FeeDV As DataView

    Public ReadOnly Property DebtorDataView() As DataView
        Get
            DebtorDataView = DebtorDV
        End Get
    End Property

    Public ReadOnly Property PaymentDataView() As DataView
        Get
            PaymentDataView = PaymentDV
        End Get
    End Property

    Public ReadOnly Property FeeDataView() As DataView
        Get
            FeeDataView = FeeDV
        End Get
    End Property

    Public Sub GetDebtors(ByVal ModDate As Date)
        Dim Sql As String

        Try
            Sql = "SELECT d.ClientSchemeID " & _
                  "     , d._rowID AS debtorID " & _
                  "     , d._CreatedDate " & _
                  "     , CAST(SUBSTRING(ni.text, 31, LENGTH(ni.text)-31) AS DECIMAL(9,2)) AS Interest " & _
                  "     , CAST(SUBSTRING(nb.text, 16, LENGTH(nb.text)-16) AS DECIMAL(9,2)) AS Balance " & _
                  "FROM Debtor AS d " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "INNER JOIN Note AS nb ON d._rowID = nb.DebtorID " & _
                  "INNER JOIN Note AS ni ON d._rowID = ni.DebtorID " & _
                  "INNER JOIN ( SELECT d_b._rowID AS DebtorID " & _
                  "                  , MAX(n_b._rowID) AS NoteID " & _
                  "             FROM Debtor AS d_b " & _
                  "             INNER JOIN ClientScheme AS cs_b ON d_b.clientschemeID = cs_b._rowid " & _
                  "             INNER JOIN Note AS n_b ON d_b._rowID = n_b.DebtorID " & _
                  "             WHERE cs_b._rowID IN (3161, 3162, 3163, 3164, 3165, 3166, 3167, 3169, 3171, 3172, 3173, 3174, 3175, 3176, 3178, 3179, 3180, 3181, 3182) " & _
                  "               AND d_b.status_open_closed = 'O' " & _
                  "               AND n_b.type = 'Client note' " & _
                  "               AND n_b.text LIKE 'Total Balance: %' " & _
                  "               AND n_b._CreatedDate < '" & ModDate.AddMonths(1).ToString("yyyy-MM-dd") & "'" & _
                  "             GROUP BY d_b._rowID " & _
                  "           ) AS nb_m ON d._rowID  = nb_m.DebtorID " & _
                  "                    AND nb._rowID = nb_m.NoteID " & _
                  "INNER JOIN ( SELECT d_i._rowID AS DebtorID " & _
                  "                  , MAX(n_i._rowID) AS NoteID " & _
                  "             FROM Debtor AS d_i " & _
                  "             INNER JOIN ClientScheme AS cs_i ON d_i.clientschemeID = cs_i._rowid " & _
                  "             INNER JOIN Note AS n_i ON d_i._rowID = n_i.DebtorID " & _
                  "             WHERE cs_i._rowID IN (3161, 3162, 3163, 3164, 3165, 3166, 3167, 3169, 3171, 3172, 3173, 3174, 3175, 3176, 3178, 3179, 3180, 3181, 3182) " & _
                  "               AND d_i.status_open_closed = 'O' " & _
                  "               AND n_i.type = 'Client note' " & _
                  "               AND n_i.text LIKE 'Interest Rate Applied To Loan: %' " & _
                  "               AND n_i._CreatedDate < '" & ModDate.AddMonths(1).ToString("yyyy-MM-dd") & "'" & _
                  "             GROUP BY d_i._rowID " & _
                  "           ) AS ni_m ON d._rowID  = ni_m.DebtorID " & _
                  "                    AND ni._rowID = ni_m.NoteID " & _
                  "WHERE cs._rowID IN (3161, 3162, 3163, 3164, 3165, 3166, 3167, 3169, 3171, 3172, 3173, 3174, 3175, 3176, 3178, 3179, 3180, 3181, 3182) " & _
                  "  AND d.status_open_closed = 'O' " & _
                  "  AND d.status <> 'F' " & _
                  "  AND d._CreatedDate < '" & ModDate.AddMonths(1).ToString("yyyy-MM-dd") & "'" & _
                  "ORDER BY d.ClientSchemeID " & _
                  "       , d._rowID "

            LoadDataTable("DebtRecovery", Sql, Debtor, False)

            DebtorDV = New DataView(Debtor)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub GetPayments(ByVal ModDate As Date)
        Dim Sql As String
        ModDate = ModDate.AddDays(-ModDate.Day + 1)
        Try
            Sql = "SELECT p._rowID AS PaymentID " & _
                  "     , p.debtorID " & _
                  "     , p.date " & _
                  "     , p.amount " & _
                  "FROM Payment AS p " & _
                  "INNER JOIN Debtor AS d ON p.debtorID = d._rowid " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "WHERE cs._rowID IN (3161, 3162, 3163, 3164, 3165, 3166, 3167, 3169, 3171, 3172, 3173, 3174, 3175, 3176, 3178, 3179, 3180, 3181, 3182) " & _
                  "  AND d.status_open_closed = 'O' " & _
                  "  AND d.status <> 'F' " & _
                  "  AND p.date >= '" & ModDate.ToString("yyyy-MM-dd") & "'" & _
                  "  AND p.date < '" & ModDate.AddMonths(1).ToString("yyyy-MM-dd") & "'"

            LoadDataTable("DebtRecovery", Sql, Payment, False)

            PaymentDV = New DataView(Payment)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub GetFees(ByVal ModDate As Date)
        Dim Sql As String
        ModDate = ModDate.AddDays(-ModDate.Day + 1)
        Try
            Sql = "SELECT f._rowID AS FeeID " & _
                  "     , f.debtorID " & _
                  "     , f.date " & _
                  "     , f.fee_amount + f.fee_vatamount AS amount " & _
                  "FROM Fee AS f " & _
                  "INNER JOIN Debtor AS d ON f.debtorID = d._rowid " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "WHERE cs._rowID IN (3161, 3162, 3163, 3164, 3165, 3166, 3167, 3169, 3171, 3172, 3173, 3174, 3175, 3176, 3178, 3179, 3180, 3181, 3182) " & _
                  "  AND d.status_open_closed = 'O' " & _
                  "  AND d.status <> 'F' " & _
                  "  AND f.date >= '" & ModDate.ToString("yyyy-MM-dd") & "'" & _
                  "  AND f.date < '" & ModDate.AddMonths(1).ToString("yyyy-MM-dd") & "'" & _
                  "  AND f.fee_remit_col IN (3,4,5)"

            LoadDataTable("DebtRecovery", Sql, Fee, False)

            FeeDV = New DataView(Fee)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
End Class

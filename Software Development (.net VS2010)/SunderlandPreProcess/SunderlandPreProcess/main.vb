Public Class mainform
    Dim ascii As New System.Text.ASCIIEncoding()


    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If

        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline As String
        Dim save_case As String = ""
        Dim linetext As String = ""
        Dim line(0) As String
        Dim curr_addr1 As String = ""
        Dim curr_addr2 As String = ""
        Dim curr_addr3 As String = ""
        Dim curr_addr4 As String = ""
        Dim curr_addr5 As String = ""
        Dim curr_pcode As String = ""
        Dim debt_addr1 As String = ""
        Dim debt_addr2 As String = ""
        Dim debt_addr3 As String = ""
        Dim debt_addr4 As String = ""
        Dim debt_addr5 As String = ""
        Dim debt_addr6 As String = ""
        Dim name As String = ""
        Dim name2 As String = ""
        Dim court_name As String = ""
        Dim lo_date As Date
        Dim idx As Integer
        Dim lines As Integer = 0
        Dim debt_amt As Decimal
        Dim last_debt_amt As Decimal = 0
        Dim lodate, from_date, to_date As Date
        Dim last_from_date As Date = Nothing
        Dim last_to_date As Date = Nothing

        Dim clref As String = ""
        Dim last_clref As String = ""
        Dim comments As String = ""

        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "Whole name|Curr Addr1|Curr Addr2|Curr Addr3|Curr Addr4|Curr Addr5|Curr pcode|" &
            "Debt Addr1|Debt Addr2|DebtAddr3|Debt Addr4|Debt Addr5|Debt Addr6|Debt Amt|client ref|Offence From|" &
            "OffenceDate|OffenceCourt|Court Name|Name2|Comments"
        outfile = outline & vbNewLine
        Dim Curr_line() As String
        Dim no_of_duplicates As Integer = 0
        For idx = 1 To lines - 1
            'get details for this case - separated by pipes
            Curr_line = Split(line(idx), "|")
            name = Curr_line(0)
            curr_addr1 = Curr_line(2)
            curr_addr2 = Curr_line(3)
            curr_addr3 = Curr_line(4)
            curr_addr4 = Curr_line(5)
            curr_addr5 = Curr_line(6)
            curr_pcode = Curr_line(7)

            debt_addr1 = Curr_line(24)
            debt_addr2 = Curr_line(25)
            debt_addr3 = Curr_line(26)
            debt_addr4 = Curr_line(27)
            debt_addr5 = Curr_line(28)
            debt_addr6 = Curr_line(29)

            debt_amt = Curr_line(38)
            clref = Curr_line(44)
            Try
                from_date = Curr_line(57)
            Catch ex As Exception

            End Try
            Try
                to_date = Curr_line(58)
            Catch ex As Exception

            End Try

            Try
                lo_date = Curr_line(36)
            Catch ex As Exception

            End Try

            court_name = Curr_line(32)

            'validate case details
            If clref = Nothing Then
                errorfile = errorfile & "Line  " & idx & " - No client reference" & vbNewLine
            End If

            'check if this case has same clref, balance and dates as previous case
            'if so, this is a joint name so amend previous case details
            If clref = last_clref And debt_amt = last_debt_amt And _
                to_date = last_to_date And from_date = last_from_date Then
                no_of_duplicates += 1
                If no_of_duplicates = 1 Then
                    name2 = remove_chars(Curr_line(0))
                Else
                    comments = comments & "Other Name:" & remove_chars(Curr_line(0)) & ";"
                End If
            Else
                no_of_duplicates = 0
                If Len(save_case) > 0 Then
                    outfile = outfile & save_case & name2 & "|" & comments & vbNewLine
                End If
                comments = ""
                name2 = ""
                save_case = name & "|" & curr_addr1 & "|" & curr_addr2 & "|" & curr_addr3 & "|" &
                 curr_addr4 & "|" & curr_addr5 & "|" & curr_pcode & "|" & debt_addr1 & "|" & debt_addr2 & "|" &
                 debt_addr3 & "|" & debt_addr4 & "|" & debt_addr5 & "|" & debt_addr6 & "|" & debt_amt & "|" &
                 clref & "|" & from_date & "|" & to_date & "|" & lo_date & "|" & court_name & "|"
                save_case = remove_chars(save_case)
            End If


            'save clref and debt amt to compare to next case
            last_clref = clref
            last_debt_amt = debt_amt
            last_from_date = from_date
            last_to_date = to_date

            clref = ""
            name = ""
            curr_addr1 = ""
            curr_addr2 = ""
            curr_addr3 = ""
            curr_addr4 = ""
            curr_addr5 = ""
            curr_pcode = ""
            debt_addr1 = ""
            debt_addr2 = ""
            debt_addr3 = ""
            debt_addr4 = ""
            debt_addr5 = ""
            debt_addr6 = ""
            lodate = Nothing
            lodate = Nothing
            debt_amt = Nothing
            from_date = Nothing
            to_date = Nothing
            court_name = ""
        Next

        'save last case
        outfile = outfile & save_case & name2 & "|" & comments & vbNewLine
        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False, ascii)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText("error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
            MsgBox("File created")
            Me.Close()
        End If
    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
    Public Function remove_chars(ByVal txt As String) As String
        Dim new_txt As String = ""
        For idx = 1 To txt.Length
            If Mid(txt, idx, 1) = Chr(10) Or Mid(txt, idx, 1) = Chr(13) Then
                new_txt = new_txt & " "
            Else
                new_txt = new_txt & Mid(txt, idx, 1)
            End If
        Next

        Return (new_txt)
    End Function
End Class

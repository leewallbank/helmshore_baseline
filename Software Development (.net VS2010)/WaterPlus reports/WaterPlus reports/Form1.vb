﻿Imports CommonLibrary
Imports System.IO

Public Class Form1
    Dim start_date As Date
    Dim end_date As Date
    Dim filename As String = ""
    Dim filepath As String = ""
    Private Sub runbtn_Click(sender As System.Object, e As System.EventArgs) Handles runbtn.Click
      
        runbtn.Enabled = False
        statuslbl.Text = "Starting RD672 report"
        run_report()
        
        Me.Close()
    End Sub
    Private Sub run_report()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()

        Dim RD672report = New RD672
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(100)
        SetCurrentValuesForParameterField3(RD672report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD672report)
        filename = "RD672 WaterPlus adhoc report.xls"
        Dim savefiledialog1 As New SaveFileDialog
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = filename
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            statuslbl.Text = "Running RD672 report ... please wait "
            filename = SaveFileDialog1.FileName

            RD672report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook, savefiledialog1.FileName)

            filename = Replace(filename, "RD672", "RD672A")

            statuslbl.Text = "Running RD672A report ... please wait "
            'now run RD672A
            RD672report.Close()

            Dim RD672Areport = New RD672A
            Dim myArrayList2 As ArrayList = New ArrayList()
            myArrayList2.Add(start_date)
            SetCurrentValuesForParameterField1(RD672Areport, myArrayList2)
            myArrayList2.Add(end_date)
            SetCurrentValuesForParameterField2(RD672Areport, myArrayList2)
           
            myConnectionInfo.ServerName = "DebtRecovery"
            myConnectionInfo.DatabaseName = "DebtRecovery"
            myConnectionInfo.UserID = "vbnet"
            myConnectionInfo.Password = "tenbv"
            SetDBLogonForReport(myConnectionInfo, RD672Areport)
            RD672Areport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook, filename)


            RD672Areport.Close()
            statuslbl.Text = "Running RD672B report ... please wait "
            'now run RD672B
            filename = Replace(filename, "RD672A", "RD672B")

            Dim RD672Breport = New RD672B
            Dim myArrayList3 As ArrayList = New ArrayList()
            myArrayList3.Add(start_date)
            SetCurrentValuesForParameterField1(RD672Breport, myArrayList3)
            myArrayList3.Add(end_date)
            SetCurrentValuesForParameterField2(RD672Breport, myArrayList3)
           
            myConnectionInfo.ServerName = "DebtRecovery"
            myConnectionInfo.DatabaseName = "DebtRecovery"
            myConnectionInfo.UserID = "vbnet"
            myConnectionInfo.Password = "tenbv"
            SetDBLogonForReport(myConnectionInfo, RD672Breport)
            RD672Breport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook, filename)


            RD672Breport.Close()
            MsgBox("Reports saved")
        Else
            MsgBox("Report not saved")
        End If

       
    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        end_date = CDate(Format(Now, "MMM yyyy") & " 01 00:00:00")
        end_date = DateAdd(DateInterval.Day, -1, end_date)
        start_date = CDate(Format(end_date, "MMM yyyy") & " 01 00:00:00")
        start_dtp.Value = start_date
        end_dtp.Value = end_date
    End Sub
End Class

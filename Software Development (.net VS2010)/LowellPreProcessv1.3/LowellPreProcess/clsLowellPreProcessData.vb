﻿Imports CommonLibrary

Public Class clsLowellPreProcessData

    Public Function GetConnIDName(ByVal ConnID As String) As String

        GetConnIDName = GetSQLResults("DebtRecovery", "SELECT CONCAT(s.nameLong, ': ', s.name) " & _
                                                      "FROM clientscheme AS cs " & _
                                                      "INNER JOIN scheme AS s ON cs.schemeID = s._rowid " & _
                                                      "WHERE cs._rowid = " & ConnID)
    End Function
End Class

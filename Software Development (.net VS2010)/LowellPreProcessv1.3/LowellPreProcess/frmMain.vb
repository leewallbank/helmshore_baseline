﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization
Imports System.Net.Mail

Public Class frmMain
    Const Separator As String = "|"
    Private ConnCode As New Dictionary(Of String, Integer)
    Private CustomerType As New Dictionary(Of String, String)
    Private PersonType As New Dictionary(Of String, String)
    Private Nationality As New Dictionary(Of String, String)
    Private IDType As New Dictionary(Of String, String)
    Private PersonAddressType As New Dictionary(Of String, String)
    'Private ConnID As String() = {"3767", "3833", "3834", "3835", "3836", "3837", "3838", "3839", "3840", "3841", "3842", "3845", "4529", "4530", "4531", "4532", "4533", "4534", "4535", "4536", "4537", "4538", "4539", "4540", "4541"} ' commented out TS 19/Jan/2015. Request ref 39843
    'Private ConnID As String() = {"3767", "3836", "3840", "3845", "4529"} ' commented out TS 22/May/2015. Request ref 50525
    Private ConnID As String() = {"4529", "4623", "4624", "4625", "4626", "4627", "4628", "4629", "4630", "4631", "4632", "4639", "4640", "4641", "4642", "4643", "4644", "4645", "4646", "4647", "4648", "4649", "4650", "4651", "4652", "4653"}
    Private ConnIndex As Integer ' added TS 22/May/2015. Request ref 50525
    Private InputFilePath As String, FileName As String, FileExt As String
    Private OutputFiles As New ArrayList()
    Private NewCases As New ArrayList()

    Private LowellPreProcessData As New clsLowellPreProcessData

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        ' commented out TS 19/Jan/2015. Request ref 39843
        'ConnCode.Add("PFS1", 0)
        'ConnCode.Add("PFS2", 1)
        'ConnCode.Add("PFS3", 2)
        'ConnCode.Add("PFS4", 3)
        'ConnCode.Add("PMO1", 4)
        'ConnCode.Add("PMO2", 5)
        'ConnCode.Add("PMO3", 6)
        'ConnCode.Add("PMO4", 7)
        'ConnCode.Add("PTE1", 8)
        'ConnCode.Add("PTE2", 9)
        'ConnCode.Add("PTE3", 10)
        'ConnCode.Add("PTE4", 10)
        'ConnCode.Add("PSB", 11)
        'ConnCode.Add("PUT1", 12)
        'ConnCode.Add("PUT2", 13)
        'ConnCode.Add("QFS3", 14)
        'ConnCode.Add("QTE2", 15)
        'ConnCode.Add("QUT2", 16)
        'ConnCode.Add("TFS1", 17)
        'ConnCode.Add("TFS2", 18)
        'ConnCode.Add("TFS3", 19)
        'ConnCode.Add("SUT2", 20)
        'ConnCode.Add("TTE1", 21)
        'ConnCode.Add("TTE2", 22)
        'ConnCode.Add("TTE3", 23)
        'ConnCode.Add("TUT2", 24)

        ' commented out TS 22/May/2015. Request ref 50525
        'ConnCode.Add("FS", 0)
        'ConnCode.Add("MO", 1)
        'ConnCode.Add("TE", 2)
        'ConnCode.Add("SB", 3)
        'ConnCode.Add("UT", 4)

        ConnCode.Add("PFS1", 1)
        ConnCode.Add("SFS1", 2)
        ConnCode.Add("TFS1", 3)
        ConnCode.Add("QFS1", 4)
        ConnCode.Add("SFS3", 5)
        ConnCode.Add("TFS3", 6)
        ConnCode.Add("QFS3", 7)
        ConnCode.Add("QFS4", 8)
        ConnCode.Add("PTE1", 9)
        ConnCode.Add("PMO1", 10)
        ConnCode.Add("PFS2", 11)
        ConnCode.Add("PFS3", 12)
        ConnCode.Add("PTE2", 13)
        ConnCode.Add("PTE3", 14)
        ConnCode.Add("QFS2", 15)
        ConnCode.Add("QTE1", 16)
        ConnCode.Add("QTE2", 17)
        ConnCode.Add("QTE3", 18)
        ConnCode.Add("SFS2", 19)
        ConnCode.Add("STE2", 20)
        ConnCode.Add("STE3", 21)
        ConnCode.Add("TFS2", 22)
        ConnCode.Add("TTE2", 23)
        ConnCode.Add("TTE3", 24)
        ConnCode.Add("PMO3", 25)

        CustomerType.Add("COR", "Corporate")
        CustomerType.Add("IND", "Individual")
        CustomerType.Add("JSH", "Joint Account Holder")
        CustomerType.Add("RES", "Residential")
        CustomerType.Add("SME", "Small Business")

        PersonType.Add("AUT", "Authorised User")
        PersonType.Add("GUA", "Guarantor")
        PersonType.Add("JAH", "Joint Account Holder")
        PersonType.Add("PAH", "Primary Account Holder")
        PersonType.Add("SAH", "Secondary Account Holder")

        Nationality.Add("GB", "UK")
        Nationality.Add("BE", "Belgium")
        Nationality.Add("FI", "Finland")
        Nationality.Add("FR", "France")
        Nationality.Add("DE", "Germany")
        Nationality.Add("GR", "Greece")

        IDType.Add("NIF", "Spanish National ID Card")
        IDType.Add("NIE", "Resident Card")
        IDType.Add("CIF", "Company Registration")
        IDType.Add("PAS", "Passport")
        IDType.Add("NIN", "National Insurance Number")
        IDType.Add("SSN", "Social Security Number")
        IDType.Add("OTH", "Other")

        PersonAddressType.Add("BAD", "Business Address")
        PersonAddressType.Add("CAD", "Current Address")
        PersonAddressType.Add("EAD", "Employers Address")
        PersonAddressType.Add("INT", "International Address")
        PersonAddressType.Add("PAD", "Previous Address")
        PersonAddressType.Add("PAF", "Post Office Address File formatted address")
        PersonAddressType.Add("SAD", "Service Address")
        PersonAddressType.Add("UKN", "Unknown")
    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String, InputLineArray() As String, ConnKey As String, DebtNotes As String, RejectionFile As String = Nothing
        Dim NewDebtFile(UBound(ConnID)) As String
        Dim NewDebtSumm(UBound(ConnID), 1) As Decimal, TotalNewDebt As Decimal
        Dim LineNumber As Integer

        Try
            FileDialog.Filter = "Lowell assignment files|*.csv|All files (*.*)|*.*"
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            For Each OldFile As String In Directory.GetFiles(InputFilePath, "*" & FileName & "_*.txt")
                File.Delete(OldFile)
            Next OldFile

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents) + (UBound(NewDebtFile) + 1) + 1 ' +1 for audit log

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1

                If LineNumber = 1 Then ' validate header
                    If InputLine.Split(",")(4) <> UBound(FileContents) - 1 Then ErrorLog &= "Count total in the header (" & InputLine.Split(",")(4) & ") does not match file contents (" & (UBound(FileContents) - 1).ToString & ")." & vbCrLf
                    Continue For
                End If

                If LineNumber = UBound(FileContents) + 1 Then Continue For ' skip footer line

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                InputLineArray = InputLine.Split(",")

                ' This section added TS 24/Mar/2014 Commented out 28/Mar/2014 Uncommented 3/Apr/2014 recommented 7/Apr/2014

                'InputLineArray(4) = InputLineArray(8)

                'Dim TempInputLineArray(UBound(InputLineArray) - 1) As String
                'Dim TempCount As Integer

                'For TempCount = 0 To 7
                '    TempInputLineArray(TempCount) = InputLineArray(TempCount)
                'Next TempCount

                'For TempCount = 9 To UBound(InputLineArray)
                '    TempInputLineArray(TempCount - 1) = InputLineArray(TempCount)
                'Next TempCount
                'InputLineArray = TempInputLineArray

                'End of section

                ' Identify the client scheme
                'ConnKey = InputLineArray(12).Substring(1, 2) ' changed from 10 TS 27/Feb/2014 request 16946. substring added TS 19/Jan/2015. Request ref 39843. Commented out TS 22/May/2015. Request ref 50525
                ConnKey = InputLineArray(12)
                If ConnCode.ContainsKey(ConnKey) Or ConnKey.IndexOf("UT") > -1 Then ' IndexOf added TS 22/May/2015. Request ref 50525

                    If ConnKey.IndexOf("UT") > -1 Then
                        ConnIndex = 0
                    Else
                        ConnIndex = ConnCode(ConnKey)
                    End If

                    ' all occurrences of ConnCode(ConnKey) changed to ConnIndex after this point. TS 22/May/2015. Request ref 50525

                    If InputLineArray(19) = "" OrElse CurrentAge(DateTime.Parse(InputLineArray(19))) < 70 Then ' added TS 15/Sep/2014. Request ref 30543
                        If InputLineArray(37) = "" OrElse CurrentAge(DateTime.Parse(InputLineArray(37))) < 70 Then ' added TS 15/Sep/2014. Request ref 30543

                            If InputLineArray(56) = "UKN" Then InputLineArray(56) = "" ' Blank UKN / Unknown country code
                            If InputLineArray(64) = "UKN" Then InputLineArray(64) = "" ' Blank UKN / Unknown country code
                            If InputLineArray(72) = "UKN" Then InputLineArray(72) = "" ' Blank UKN / Unknown country code

                            'This section added TS 10/Feb/2014
                            If InputLineArray(27).StartsWith("TEL") Then InputLineArray(27) = ""
                            If InputLineArray(29).StartsWith("TEL") Then InputLineArray(29) = ""
                            If InputLineArray(31).StartsWith("TEL") Then InputLineArray(31) = ""

                            NewDebtFile(ConnIndex) = Join(InputLineArray, Separator) & _
                                                                Separator

                            DebtNotes = ToNote(InputLineArray(0), "AccountID", ";")(0) & _
                                        ToNote(InputLineArray(5), "Outstanding balance as per Clients system", ";")(0) & _
                                        ToNote(InputLineArray(7), "Client Brand", ";")(0) & _
                                        ToNote(InputLineArray(8), "Debt country of origin", ";")(0) & _
                                        ToNote(InputLineArray(9), "Assignment ID", ";")(0) & _
                                        ToNote(InputLineArray(10), "Assignment Type Code", ";")(0) & _
                                        ToNote(InputLineArray(11), "Total Amount Assigned", ";")(0) & _
                                        ToNote(InputLineArray(12), "Segment Name", ";")(0)

                            If CustomerType.ContainsKey(InputLineArray(13)) Then
                                DebtNotes &= ToNote(CustomerType(InputLineArray(13)), "Small Business or Residential Customer", ";")(0)
                            Else
                                DebtNotes &= ToNote("??", "Small Business or Residential Customer", ";")(0)
                                ErrorLog &= "Unexpected PrimaryCustomerTypeCode of " & InputLineArray(13) & " found at line number " & LineNumber.ToString & vbCrLf
                            End If

                            DebtNotes &= ToNote(InputLineArray(14), "Lowell unique identifier", ";")(0) & _
                                         ToNote(InputLineArray(15), "Person Type Code", ";")(0)

                            If PersonType.ContainsKey(InputLineArray(15)) Then
                                DebtNotes &= ToNote(PersonType(InputLineArray(15)), "Primary Person", ";")(0)
                            Else
                                DebtNotes &= ToNote("??", "Primary Person", ";")(0)
                                ErrorLog &= "Unexpected PersonTypeCode of " & InputLineArray(15) & " found at line number " & LineNumber.ToString & vbCrLf
                            End If

                            If Nationality.ContainsKey(InputLineArray(20)) Then
                                DebtNotes &= ToNote(Nationality(InputLineArray(20)), "Main Debtor Nationality", ";")(0)
                            Else
                                DebtNotes &= ToNote(InputLineArray(20), "Main Debtor Nationality", ";")(0)
                            End If

                            DebtNotes &= ToNote(InputLineArray(21), "PrimaryIDTypeCode", ";")(0)

                            If IDType.ContainsKey(InputLineArray(21)) Then
                                DebtNotes &= ToNote(IDType(InputLineArray(21)), "PrimaryIDTypeDescription", ";")(0)
                            Else
                                DebtNotes &= ToNote(InputLineArray(21), "PrimaryIDTypeDescription", ";")(0)
                            End If

                            DebtNotes &= ToNote(InputLineArray(22), "Primary ID Detail1", ";")(0) & _
                                         ToNote(InputLineArray(25), "Debtor 1 Bank Account Number", ";")(0) & _
                                         ToNote(InputLineArray(32), "Name 2 TDX unique identifier", ";")(0) & _
                                         ToNote(InputLineArray(33), "SecondaryPersonTypeCode", ";")(0)
                            'ToNote(InputLineArray(24), "Debtor 1 special needs", ";")(0) & _

                            If PersonType.ContainsKey(InputLineArray(33)) Then
                                DebtNotes &= ToNote(PersonType(InputLineArray(33)), "Name 2 type description", ";")(0)
                            ElseIf Not String.IsNullOrEmpty(InputLineArray(33)) Then
                                DebtNotes &= ToNote("??", "Name 2 type description", ";")(0)
                                ErrorLog &= "Unexpected SecondaryPersonTypeCode of " & InputLineArray(33) & " found at line number " & LineNumber.ToString & vbCrLf
                            End If

                            DebtNotes &= ToNote(InputLineArray(37), "Name 2 DOB", ";")(0)

                            If Nationality.ContainsKey(InputLineArray(38)) Then
                                DebtNotes &= ToNote(Nationality(InputLineArray(38)), "Debtor 2 Nationality", ";")(0)
                            Else
                                DebtNotes &= ToNote(InputLineArray(38), "Debtor 2 Nationality", ";")(0)
                            End If

                            DebtNotes &= ToNote(InputLineArray(39), "Debtor 2 ID type", ";")(0)

                            If IDType.ContainsKey(InputLineArray(39)) Then
                                DebtNotes &= ToNote(IDType(InputLineArray(39)), "SecondaryIDTypeDescription", ";")(0)
                            Else
                                DebtNotes &= ToNote(InputLineArray(39), "SecondaryIDTypeDescription", ";")(0)
                            End If

                            DebtNotes &= ToNote(InputLineArray(40), "Debtor 2 ID Detail 1", ";")(0) & _
                                         ToNote(InputLineArray(41), "Debtor 2 Email", ";")(0) & _
                                         ToNote(InputLineArray(42), "Debtor 2 special needs", ";")(0) & _
                                         ToNote(InputLineArray(43), "Debtor 2 Bank Account Number", ";")(0) & _
                                         ToNote(InputLineArray(44), "SecondaryPhoneTypeCode1", ";")(0) & _
                                         ToNote(InputLineArray(45), "Debtor 2 Home Tel Number", ";")(0) & _
                                         ToNote(InputLineArray(46), "SecondaryPhoneTypeCode2", ";")(0) & _
                                         ToNote(InputLineArray(47), "Debtor 2 Work Tel Number", ";")(0) & _
                                         ToNote(InputLineArray(48), "SecondaryPhoneTypeCode3", ";")(0) & _
                                         ToNote(InputLineArray(49), "SecondaryTelephoneNo3", ";")(0) & _
                                         ToNote(InputLineArray(50), "PersonAddressTypeCode1", ";")(0)

                            If PersonAddressType.ContainsKey(InputLineArray(50)) Then
                                DebtNotes &= ToNote(PersonAddressType(InputLineArray(50)), "PersonAddressType1", ";")(0)
                            Else
                                DebtNotes &= ToNote(InputLineArray(50), "PersonAddressType1", ";")(0)
                            End If

                            DebtNotes &= ToNote(InputLineArray(58), "PersonAddressTypeCode2", ";")(0)

                            If PersonAddressType.ContainsKey(InputLineArray(58)) Then
                                DebtNotes &= ToNote(PersonAddressType(InputLineArray(58)), "PersonAddressType2", ";")(0)
                            Else
                                DebtNotes &= ToNote(InputLineArray(58), "PersonAddressType2", ";")(0)
                            End If

                            DebtNotes &= ToNote(InputLineArray(66), "PersonAddressTypeCode3", ";")(0)

                            If PersonAddressType.ContainsKey(InputLineArray(66)) Then
                                DebtNotes &= ToNote(PersonAddressType(InputLineArray(66)), "PersonAddressType3", ";")(0)
                            Else
                                DebtNotes &= ToNote(InputLineArray(66), "PersonAddressType3", ";")(0)
                            End If

                            DebtNotes &= ToNote(ConcatFields({InputLineArray(67), InputLineArray(68), InputLineArray(69), InputLineArray(70), InputLineArray(71), InputLineArray(72), InputLineArray(73)}, ",") _
                                                , "Previous Address", ";")(0)

                            DebtNotes &= ToNote(InputLineArray(74), "Last Payment Amount", ";")(0) & _
                                         ToNote(InputLineArray(75), "Last Payment Date", ";")(0) & _
                                         ToNote(InputLineArray(76), "Total number of payments made by the debtor to date", ";")(0) & _
                                         ToNote(InputLineArray(77), "Value of payments made to date by debtor", ";")(0) & _
                                         ToNote(InputLineArray(78), "Method in which the debtor usually pays", ";")(0) & _
                                         ToNote(InputLineArray(79), "Default date", ";")(0) & _
                                         ToNote(InputLineArray(80), "Default amount", ";")(0) & _
                                         ToNote(InputLineArray(81), "Termination reason", ";")(0) & _
                                         ToNote(InputLineArray(82), "Amount owing to date not including fees", ";")(0) & _
                                         ToNote(InputLineArray(83), "Fees charged to date", ";")(0)

                            DebtNotes &= ToNote(InputLineArray(84), "Interest to date", ";")(0) & _
                                         ToNote(InputLineArray(85), "PrimaryProductNumber", ";")(0) & _
                                         ToNote(InputLineArray(86), "PrimaryProductDescription", ";")(0) & _
                                         ToNote(InputLineArray(87), "PrimaryProductValue", ";")(0) & _
                                         ToNote(InputLineArray(88), "PrimaryProductOpenDate", ";")(0) & _
                                         ToNote(InputLineArray(89), "PrimaryProductCloseDate", ";")(0) & _
                                         ToNote(InputLineArray(90), "Reading1", ";")(0) & _
                                         ToNote(InputLineArray(91), "Reading2", ";")(0) & _
                                         ToNote(InputLineArray(92), "Reading3", ";")(0) & _
                                         ToNote(InputLineArray(93), "AssociatedProduct1ProductNumber", ";")(0)

                            DebtNotes &= ToNote(InputLineArray(94), "AssociatedProduct1ProductDescription", ";")(0) & _
                                         ToNote(InputLineArray(95), "AssociatedProduct1ProductValue", ";")(0) & _
                                         ToNote(InputLineArray(96), "AssociatedProduct1ProductOpenDate", ";")(0) & _
                                         ToNote(InputLineArray(97), "AssociatedProduct1ProductCloseDate", ";")(0) & _
                                         ToNote(InputLineArray(98), "AssociatedProduct1Reading1", ";")(0) & _
                                         ToNote(InputLineArray(99), "AssociatedProduct1Reading2", ";")(0) & _
                                         ToNote(InputLineArray(100), "AssociatedProduct1Reading3", ";")(0) & _
                                         ToNote(InputLineArray(101), "AssociatedProduct2ProductNumber", ";")(0) & _
                                         ToNote(InputLineArray(102), "AssociatedProduct2ProductDescription", ";")(0) & _
                                         ToNote(InputLineArray(103), "AssociatedProduct2ProductValue", ";")(0)

                            DebtNotes &= ToNote(InputLineArray(104), "AssociatedProduct2OpenDate", ";")(0) & _
                                         ToNote(InputLineArray(105), "AssociatedProduct2CloseDate", ";")(0) & _
                                         ToNote(InputLineArray(106), "AssociatedProduct2Reading1", ";")(0) & _
                                         ToNote(InputLineArray(107), "AssociatedProduct2Reading2", ";")(0) & _
                                         ToNote(InputLineArray(108), "AssociatedProduct2Reading3", ";")(0) & _
                                         ToNote(InputLineArray(109), "AssociatedProduct3ProductNumber", ";")(0) & _
                                         ToNote(InputLineArray(110), "AssociatedProduct3ProductDescription", ";")(0) & _
                                         ToNote(InputLineArray(111), "AssociatedProduct3ProductValue", ";")(0) & _
                                         ToNote(InputLineArray(112), "AssociatedProduct3OpenDate", ";")(0) & _
                                         ToNote(InputLineArray(113), "AssociatedProduct3CloseDate", ";")(0)

                            DebtNotes &= ToNote(InputLineArray(114), "AssociatedProduct3Reading1", ";")(0) & _
                                         ToNote(InputLineArray(115), "AssociatedProduct3Reading2", ";")(0) & _
                                         ToNote(InputLineArray(116), "AssociatedProduct3Reading3", ";")(0) & _
                                         ToNote(InputLineArray(117), "Propensity score", ";")(0) & _
                                         ToNote(InputLineArray(118), "Litigation status", ";")(0) & _
                                         ToNote(InputLineArray(119), "Contract available", ";")(0) & _
                                         ToNote(InputLineArray(121), "Fee%", ";")(0) & _
                                         ToNote(InputLineArray(122), "Flat fee amount", ";")(0) & _
                                         ToNote(InputLineArray(123), "Vulnerable", ";")(0)

                            DebtNotes &= String.Join("", ToNote(InputLineArray(124), "Client Comments", ";")) & _
                                         ToNote(InputLineArray(125), "IGT Flag", ";")(0) & _
                                         ToNote(InputLineArray(126), "Disconnection flag", ";")(0) & _
                                         ToNote(InputLineArray(127), "Post litigation flag", ";")(0) & vbCrLf

                            NewDebtFile(ConnIndex) &= DebtNotes

                            NewDebtSumm(ConnIndex, 0) += 1
                            NewDebtSumm(ConnIndex, 1) += InputLineArray(11)
                            TotalNewDebt += InputLineArray(11)
                            AppendToFile(InputFilePath & ConnID(ConnIndex) & "_" & FileName & "_PreProcessed.txt", NewDebtFile(ConnIndex))

                        Else
                            RejectionFile &= InputLineArray(0) & "," & InputLineArray(1) & vbCrLf
                            ErrorLog &= "Debtor 2 aged 70 or more at line number " & LineNumber.ToString & ". Case not loaded." & vbCrLf
                        End If

                    Else
                        RejectionFile &= InputLineArray(0) & "," & InputLineArray(1) & vbCrLf
                        ErrorLog &= "Debtor 1 aged 70 or more at line number " & LineNumber.ToString & ". Case not loaded." & vbCrLf
                    End If

                Else
                    ErrorLog &= "Unable to identify client scheme at line number " & LineNumber.ToString & vbCrLf

                End If

            Next InputLine

            If FileContents(0).Split(",")(3) <> TotalNewDebt Then ErrorLog &= "Debt total in the header (" & FileContents(0).Split(",")(3) & ") does not match file contents (" & TotalNewDebt.ToString & ")." & vbCrLf

            ProgressBar.Value += 1

            AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            For NewDebtFileCount As Integer = 0 To UBound(NewDebtFile)
                ProgressBar.Value += 1
                If NewDebtFile(NewDebtFileCount) <> "" Then
                    OutputFiles.Add(InputFilePath & ConnID(NewDebtFileCount) & "_" & FileName & "_PreProcessed.txt")
                    AuditLog &= "Clientscheme: " & ConnID(NewDebtFileCount) & " - " & LowellPreProcessData.GetConnIDName(ConnID(NewDebtFileCount)) & vbCrLf
                    AuditLog &= "Number of new cases: " & NewDebtSumm(NewDebtFileCount, 0).ToString & vbCrLf
                    AuditLog &= "Total balance of new cases: " & NewDebtSumm(NewDebtFileCount, 1).ToString & vbCrLf & vbCrLf
                End If
            Next NewDebtFileCount

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)
            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", LogHeader & ExceptionLog)

            If Not RejectionFile Is Nothing Then
                MsgBox("Cases have been rejected due to debtor age.", vbCritical, Me.Text)
                WriteFile(InputFilePath & FileName & "_Rejections.csv", "TDX ID, Client ref" & vbCrLf & RejectionFile)
            End If

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub

    Private Function CurrentAge(ByVal DOB As Date) As Integer
        ' http://stackoverflow.com/questions/16874911/compute-age-from-given-birthdate
        Dim Age As Integer = Today.Year - DOB.Year
        If (DOB > Today.AddYears(-Age)) Then Age -= 1

        Return Age
    End Function
End Class

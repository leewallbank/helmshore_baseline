﻿Imports CommonLibrary
Imports System.IO
Imports System.Text.RegularExpressions

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String
    Private OutputFiles As New ArrayList()

    Private Const Separator As String = "|"
    Private ConnCode As New Dictionary(Of String, Integer)
    Private AccountType As New Dictionary(Of String, String)
    Private OccupancyNature As New Dictionary(Of String, String)
    Private ConnID() As String = {"4786"}

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        btnViewInputFile.Enabled = False
        btnViewLogFile.Enabled = False
        btnViewOutputFile.Enabled = False

        ConnCode.Add("???", 0)

        AccountType.Add("CM", "Commercial Metered")
        AccountType.Add("CU", "Commercial Unmetered")

        OccupancyNature.Add("U", "Unknown")
        OccupancyNature.Add("T", "Tenant")
        OccupancyNature.Add("O", "Owner")

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click

        Try
            Dim FileDialog As New OpenFileDialog
            Dim ExcelFile As New ArrayList, VCFile As New ArrayList
            Dim AuditLog As String, FileContents(,) As String, ErrorLog As String = "", ConnKey As String = "", DebtNotes As String

            Dim NewDebtSumm(0, 1) As Decimal ' 0 Case count, 1 Total balance

            btnViewInputFile.Enabled = False
            btnViewLogFile.Enabled = False
            btnViewOutputFile.Enabled = False

            FileDialog.Filter = "New business files|*.xls;*.xlsx|All files (*.*)|*.*"
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

            For Each OldFile As String In Directory.GetFiles(InputFilePath, "*" & FileName & "_*.txt")
                File.Delete(OldFile)
            Next OldFile

            lblReadingFile.Visible = True
            Application.DoEvents()
            FileContents = InputFromExcel(FileDialog.FileName)

            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents)

            For RowNum = 1 To UBound(FileContents)

                Application.DoEvents() ' without this line, the button disappears until processing is complete
                ProgressBar.Value = RowNum
                DebtNotes = ""

                If String.IsNullOrEmpty(FileContents(RowNum, 0)) Then Continue For

                ' Identify the client scheme
                ConnKey = "???"
                If ConnCode.ContainsKey(ConnKey) Then

                    If UBound(FileContents, 2) <> 56 Then
                        ErrorLog &= "Unexpected line length of " & (UBound(FileContents, 2) + 1).ToString & " items found at line number " & RowNum.ToString & ". Line not loaded." & vbCrLf
                        Continue For
                    End If

                    Dim NewDebtRow(56) As String
                    For ColNum As Integer = 0 To UBound(NewDebtRow)
                        NewDebtRow(ColNum) = FileContents(RowNum, ColNum)
                    Next ColNum

                    'remove question marks from fiscal columns
                    NewDebtRow(54) = NewDebtRow(54).Replace(Chr(163), "")
                    NewDebtRow(55) = NewDebtRow(55).Replace(Chr(163), "")
                    NewDebtRow(56) = NewDebtRow(56).Replace(Chr(163), "")

                    If AccountType.ContainsKey(NewDebtRow(4)) Then
                        DebtNotes &= ToNote(AccountType(NewDebtRow(4)), "Account Det ID", ";")(0)
                    Else
                        DebtNotes &= ToNote("??", "Account Det ID", ";")(0)
                        ErrorLog &= "Unexpected Account type of " & NewDebtRow(4) & " found at line number " & RowNum.ToString & vbCrLf
                    End If

                    DebtNotes &= ToNote(NewDebtRow(5), "CA Type", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(6), "Current Former", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(10), "Max Due Date", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(11), "Min Due Date", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(19), "Last Payment", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(20), "Installation", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(21), "Premise", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(22), "Premise Text", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(25), "Serial Number", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(26), "Last Meter Reading Date", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(27), "Meter Location", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(28), "Water Supplier", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(29), "Waste Water Receiver", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(30), "Payment Scheme Present", ";")(0)

                    If OccupancyNature.ContainsKey(NewDebtRow(32)) Then
                        DebtNotes &= ToNote(OccupancyNature(NewDebtRow(32)), "Occupancy Nature", ";")(0)
                    Else
                        DebtNotes &= ToNote(NewDebtRow(32), "Occupancy Nature", ";")(0)
                        ErrorLog &= "Unexpected Occupancy nature of " & NewDebtRow(32) & " found at line number " & RowNum.ToString & vbCrLf
                    End If

                    DebtNotes &= ToNote(NewDebtRow(53), "Claim No", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(54), "Claim Amount", ";")(0)
                    DebtNotes &= ToNote(NewDebtRow(55), "Claim Fees", ";")(0)

                    AppendToFile(InputFilePath & ConnID(ConnCode(ConnKey)) & "_" & FileName & "_Preprocessed.txt", Join(NewDebtRow, Separator) & Separator & DebtNotes & vbCrLf)

                    If Not OutputFiles.Contains(InputFilePath & ConnID(ConnCode(ConnKey)) & "_" & FileName & "_Preprocessed.txt") Then OutputFiles.Add(InputFilePath & ConnID(ConnCode(ConnKey)) & "_" & FileName & "_Preprocessed.txt")

                    ' Update running totals
                    NewDebtSumm(ConnCode(ConnKey), 0) += 1
                    NewDebtSumm(ConnCode(ConnKey), 1) += CDec(NewDebtRow(56))

                Else
                    ErrorLog &= "Unable to identify client scheme at line number " & RowNum.ToString & vbCrLf
                End If

            Next RowNum

            ' Update audit log
            AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf & vbCrLf & vbCrLf

            AuditLog &= "Conn ID " & ConnID(ConnCode(ConnKey)) & ": Southern Water " & vbCrLf & vbCrLf
            AuditLog &= "Number of new cases: " & NewDebtSumm(0, 0).ToString & vbCrLf
            AuditLog &= "Total balance of new cases: " & NewDebtSumm(0, 1).ToString & vbCrLf & vbCrLf & vbCrLf

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            ProgressBar.Value = ProgressBar.Maximum

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try
            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try
            For Each Filename As String In OutputFiles
                If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
            Next Filename

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try
            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function InputFromExcel(ByVal FileName As String) As String(,)

        InputFromExcel = Nothing

        Try

            Dim TempTable As New DataTable
            ' HDR=NO to not skip the first line
            Dim xlsConn As System.Data.OleDb.OleDbConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileName & ";Extended Properties=""Excel 12.0 Xml;IMEX=1;HDR=NO;TypeGuessRows = 0;ImportMixedTypes = Text;""")
            xlsConn.Open()
            Dim xlsComm As System.Data.OleDb.OleDbDataAdapter = New System.Data.OleDb.OleDbDataAdapter("select * from [" & xlsConn.GetSchema("Tables").Rows(0)("TABLE_NAME") & "]", xlsConn)

            xlsComm.Fill(TempTable)
            xlsConn.Close()

            Dim Output As String(,)
            ReDim Output(TempTable.Rows.Count - 1, TempTable.Columns.Count - 1)

            For RowIdx As Integer = 0 To TempTable.Rows.Count - 1
                For ColIdx As Integer = 0 To TempTable.Columns.Count - 1
                    Application.DoEvents()
                    Output(RowIdx, ColIdx) = TempTable.Rows(RowIdx).Item(ColIdx).ToString
                Next ColIdx
            Next RowIdx

            Return Output

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

End Class



﻿Imports System.IO

Public Class Form1

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'get all files in folder R:Home Office crystal reports
        'Rename YYYYMMDD to today's date
        'move to new folder for transport via PGP
        Dim file_path, file_path_archive, file_path_ready_activity, file_path_ready_other As String
        'use full path 
        file_path = "\\ross-helm-fp001\Rossendales Shared\Home Office Crystal Reports"
        file_path_archive = "\\ross-helm-fp001\Rossendales Shared\Home Office Crystal Reports\Archive\"
        file_path_ready_activity = "\\ross-helm-fp001\Rossendales Shared\Home Office Crystal Reports\ReadyToGo\Activity\"
        file_path_ready_other = "\\ross-helm-fp001\Rossendales Shared\Home Office Crystal Reports\ReadyToGo\Other\"
        Dim file_path_ready_NFU As String = "\\ross-helm-fp001\Rossendales Shared\Home Office Crystal Reports\ReadyToGo\NFU\"
        Dim file_path_ready_Recon As String = "\\ross-helm-fp001\Rossendales Shared\Home Office Crystal Reports\ReadyToGo\Reconciliation\"
        Dim file_path_ready_Trans As String = "\\ross-helm-fp001\Rossendales Shared\Home Office Crystal Reports\ReadyToGo\Transaction\"
        Dim file_path_ready_WO As String = "\\ross-helm-fp001\Rossendales Shared\Home Office Crystal Reports\ReadyToGo\Write Off\"
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(
          file_path,
           Microsoft.VisualBasic.FileIO.SearchOption.SearchTopLevelOnly, "*.*")
            'copy original to archive - first add timestamp to name
            Dim archiveFileName As String = file_path_archive & Path.GetFileNameWithoutExtension(foundFile) & "-" & Format(Now, "yyyyMMdd - HHmmss") & Path.GetExtension(foundFile)
            My.Computer.FileSystem.CopyFile(foundFile, archiveFileName, True)

            'RENAME FILE
            Dim newFileName As String = Replace(foundFile, "YYYYMMDD", Format(Now, "yyyyMMdd"))
            If newFileName <> foundFile Then
                Try
                    My.Computer.FileSystem.CopyFile(foundFile, newFileName)
                Catch ex As Exception
                    Continue For
                End Try

            End If
            Dim copyFileName As String = ""
            ' T58836 copy xml to CJI folder and others to Other folder
            If Microsoft.VisualBasic.Right(foundFile, 3) = "xml" Then
                copyFileName = file_path_ready_activity & Path.GetFileName(newFileName)
            Else
                If InStr(foundFile, "NonFinancialUpdate") > 0 Then
                    copyFileName = file_path_ready_NFU & Path.GetFileName(newFileName)
                End If
                If InStr(foundFile, "Reconciliation") > 0 Then
                    copyFileName = file_path_ready_Recon & Path.GetFileName(newFileName)
                End If
                If InStr(foundFile, "Transaction") > 0 Then
                    copyFileName = file_path_ready_Trans & Path.GetFileName(newFileName)
                End If
                If InStr(foundFile, "WriteOff") > 0 Then
                    copyFileName = file_path_ready_WO & Path.GetFileName(newFileName)
                End If
                If InStr(foundFile, "RecallDispute") > 0 Then
                    copyFileName = file_path_ready_activity & Path.GetFileName(newFileName)
                End If
            End If
            If copyFileName = "" Then
                copyFileName = file_path_ready_other & Path.GetFileName(newFileName)
            End If
            'copy to ReadyToGo

            My.Computer.FileSystem.CopyFile(newFileName, copyFileName, True)
            'delete renamed file if different
            If foundFile <> newFileName Then
                My.Computer.FileSystem.DeleteFile(newFileName)
            End If
            'delete foundfile
            My.Computer.FileSystem.DeleteFile(foundFile)

        Next

        'now get all files in folder R:HMRC TDX ETC
        'Rename YYYYMMDD to today's date
        'move to new folder for transport via PGP

        'use full path 
        file_path = "\\ross-helm-fp001\Rossendales Shared\HMRC TDX ETC"
        file_path_archive = "\\ross-helm-fp001\Rossendales Shared\HMRC TDX ETC\Archive\"
        file_path_ready_activity = "\\ross-helm-fp001\Rossendales Shared\HMRC TDX ETC\ReadyToGo\CJI\"
        file_path_ready_other = "\\ross-helm-fp001\Rossendales Shared\HMRC TDX ETC\ReadyToGo\Other\"
        file_path_ready_NFU = "\\ross-helm-fp001\Rossendales Shared\HMRC TDX ETC\ReadyToGo\Non Financial Update\"
        file_path_ready_Recon = "\\ross-helm-fp001\Rossendales Shared\HMRC TDX ETC\ReadyToGo\Reconciliation\"
        file_path_ready_Trans = "\\ross-helm-fp001\Rossendales Shared\HMRC TDX ETC\ReadyToGo\Transaction\"
        file_path_ready_WO = "\\ross-helm-fp001\Rossendales Shared\HMRC TDX ETC\ReadyToGo\Write Off\"
        For Each foundFile As String In My.Computer.FileSystem.GetFiles(
          file_path,
           Microsoft.VisualBasic.FileIO.SearchOption.SearchTopLevelOnly, "*.*")
            'copy original to archive - first add timestamp to name
            Dim archiveFileName As String = file_path_archive & Path.GetFileNameWithoutExtension(foundFile) & "-" & Format(Now, "yyyyMMdd - HHmmss") & Path.GetExtension(foundFile)
            My.Computer.FileSystem.CopyFile(foundFile, archiveFileName, True)

            'RENAME FILE
            Dim newFileName As String = Replace(foundFile, "YYYYMMDD", Format(Now, "yyyyMMdd"))
            If newFileName <> foundFile Then
                Try
                    My.Computer.FileSystem.CopyFile(foundFile, newFileName)
                Catch ex As Exception
                    Continue For
                End Try

            End If
            Dim copyFileName As String = ""
            '  copy xml to CJI folder and others to Other folder
            If Microsoft.VisualBasic.Right(foundFile, 3) = "xml" Then
                copyFileName = file_path_ready_activity & Path.GetFileName(newFileName)
            Else
                If InStr(foundFile, "NonFinancialUpdate") > 0 Then
                    copyFileName = file_path_ready_NFU & Path.GetFileName(newFileName)
                End If
                If InStr(foundFile, "Reconciliation") > 0 Then
                    copyFileName = file_path_ready_Recon & Path.GetFileName(newFileName)
                End If
                If InStr(foundFile, "Transaction") > 0 Then
                    copyFileName = file_path_ready_Trans & Path.GetFileName(newFileName)
                End If
                If InStr(foundFile, "WriteOff") > 0 Then
                    copyFileName = file_path_ready_WO & Path.GetFileName(newFileName)
                End If
                If InStr(foundFile, "RecallDispute") > 0 Then
                    copyFileName = file_path_ready_activity & Path.GetFileName(newFileName)
                End If
            End If
            If copyFileName = "" Then
                copyFileName = file_path_ready_other & Path.GetFileName(newFileName)
            End If
            'copy to ReadyToGo

            My.Computer.FileSystem.CopyFile(newFileName, copyFileName, True)
            'delete renamed file if different
            If foundFile <> newFileName Then
                My.Computer.FileSystem.DeleteFile(newFileName)
            End If
            'delete foundfile
            My.Computer.FileSystem.DeleteFile(foundFile)

        Next
        Me.Close()
    End Sub
End Class

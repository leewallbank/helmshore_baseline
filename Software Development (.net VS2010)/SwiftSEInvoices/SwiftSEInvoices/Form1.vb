﻿Imports CommonLibrary
Imports System.IO

Public Class Form1
    Dim start_date As Date
    Dim end_date As Date
    Dim filename As String = ""
    Dim filepath As String = ""
    Private Sub runbtn_Click(sender As System.Object, e As System.EventArgs) Handles runbtn.Click
        Dim password As String
        password = InputBox("Enter password", "Enter Password")
        If password <> "SWSE99" Then
            MsgBox("Invalid password")
            Me.Close()
            Exit Sub
        End If
        
        runbtn.Enabled = False
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2138Sreport = New RA2138S
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_date)
        SetCurrentValuesForParameterField1(RA2138Sreport, myArrayList1)
        myArrayList1.Add(end_date)
        SetCurrentValuesForParameterField2(RA2138Sreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        myConnectionInfo2.ServerName = "SwiftRemuneration"
        myConnectionInfo2.DatabaseName = "SwiftRemuneration"
        myConnectionInfo2.UserID = "sa"
        myConnectionInfo2.Password = "sa"

        SetDBLogonForReport(myConnectionInfo, RA2138Sreport, myConnectionInfo2)
        filename = "RA2138S " & Format(start_date, "dd MMM yy") & " Swift SE All Invoices.pdf"
        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "PDF files |*.pdf"
                .DefaultExt = ".pdf"
                .OverwritePrompt = True
                .FileName = filename
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                filepath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                filepath &= "\"
            Else
                MsgBox("Report not saved")
            End If
        Else
            filename = filepath & "RA2138S " & Format(start_date, "dd MMM yy") & " EA Invoices.pdf"
        End If

        RA2138Sreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RA2138Sreport.Close()
            
        MsgBox("Report Completed")
        Me.Close()
    End Sub
   

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        end_date = DateAdd(DateInterval.Day, -Weekday(Now) + 1, Now)
        end_date = CDate(Format(end_date, "MMM yyyy dd") & " 00:00:00")
        start_date = DateAdd(DateInterval.Day, -6, end_date)
        start_date = CDate(Format(start_date, "MMM yyyy dd") & " 00:00:00")
        start_dtp.Value = start_date
        end_dtp.Value = end_date
    End Sub
End Class

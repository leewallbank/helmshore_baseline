﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String
   

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim OutputFileOK As String = ""
            Dim outputFileDup As String = ""
            Dim totValueOK As Decimal = 0
            Dim totCasesOK As Integer = 0
            Dim totValueDup As Decimal = 0
            Dim totCasesDup As Integer = 0
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim filecontents(,) As String = InputFromExcel(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents)
            Dim rowMax As Integer = UBound(FileContents)
            Dim sameClientRef As Boolean = False
            Dim totalAmount As Decimal = 0
            Dim totalCosts As Decimal = 0
            Dim totalTotal As Decimal = 0
            Dim totalStageDebt As Decimal = 0
            Dim notes As String = ""
            Dim savedFile1 As String = ""
            Dim savedFile2 As String = ""
            Dim savedAddr As String = ""
            For rowIDX = 0 To rowMax
                ProgressBar.Value = rowIDX
                Dim clientRef As Integer
                Try
                    clientRef = filecontents(rowIDX, 0)
                Catch ex As Exception
                    Continue For
                End Try
                If clientRef <= 10000 Then
                    Continue For
                End If
                Dim summonsNo As Integer = filecontents(rowIDX, 20)
                'see if on onestep
                Dim caseCount As Integer = GetSQLResults("DebtRecovery", "SELECT count(*) from debtor D, clientscheme CS " & _
                                               " WHERE D.client_ref = '" & clientRef & "'" & _
                                               " and D.clientschemeID = CS._rowID " & _
                                                " and CS.clientID = 1972 " & _
                                               " and offence_number = " & summonsNo)
                If caseCount = 0 Then
                    'T123297 need to accumulate totals for multiple cases (same client ref)
                    'check if this client ref is same as next one

                    'if this is same as previous client ref, check if there is another one
                    If sameClientRef Then
                        'save totals
                        totalAmount += filecontents(rowIDX, 15)
                        totalCosts += filecontents(rowIDX, 16)
                        totalTotal += filecontents(rowIDX, 17)
                        totalStageDebt += filecontents(rowIDX, 18)
                        notes &= "Hearing Date:" & filecontents(rowIDX, 19) & ";"
                        notes &= "Liab Order:" & filecontents(rowIDX, 20) & ";"
                        'see if address has changed
                        Dim addr As String = filecontents(rowIDX, 3) & " " & filecontents(rowIDX, 4) & " " & _
                                    filecontents(rowIDX, 5) & " " & filecontents(rowIDX, 6) & " " & _
                                    filecontents(rowIDX, 7) & " " & filecontents(rowIDX, 8)
                        If addr <> savedAddr Then
                            notes &= "Address:" & addr & ";"
                            savedAddr = addr
                        End If
                        sameClientRef = False
                        If rowIDX <> rowMax Then
                            If clientRef = filecontents(rowIDX + 1, 0) Then ' And
                                'summonsNo = filecontents(rowIDX + 1, 20) Then
                                sameClientRef = True
                            End If
                        End If
                        If sameClientRef = False Then
                            'need to write out record
                            OutputFileOK &= savedFile1 & totalAmount & "|" & totalCosts & "|" & totalTotal & "|" & _
                                totalStageDebt & "|" & savedFile2 & notes & vbNewLine
                            totCasesOK += 1
                            totValueOK += totalStageDebt
                        End If
                    Else
                        'see if same client ref
                        If rowIDX <> rowMax Then
                            If clientRef = filecontents(rowIDX + 1, 0) Then '  And
                                'summonsNo = filecontents(rowIDX + 1, 20) Then
                                sameClientRef = True
                                'This is first duplicate found so save record and totals
                                savedFile1 = ""

                                For colidx = 0 To 14
                                    savedFile1 &= filecontents(rowIDX, colidx) & "|"
                                Next
                                totalAmount = filecontents(rowIDX, 15)
                                totalCosts = filecontents(rowIDX, 16)
                                totalTotal = filecontents(rowIDX, 17)
                                totalStageDebt = filecontents(rowIDX, 18)
                                savedFile2 = filecontents(rowIDX, 19) & "|" & filecontents(rowIDX, 20) & "|"
                                savedAddr = filecontents(rowIDX, 3) & " " & filecontents(rowIDX, 4) & " " & _
                                    filecontents(rowIDX, 5) & " " & filecontents(rowIDX, 6) & " " & _
                                    filecontents(rowIDX, 7) & " " & filecontents(rowIDX, 8)
                                notes = ""
                            End If
                        End If
                        If Not sameClientRef Then
                            For colidx = 0 To 14
                                OutputFileOK &= filecontents(rowIDX, colidx) & "|"
                            Next
                            For colidx = 15 To 18
                                Dim value As Decimal = filecontents(rowIDX, colidx)
                                OutputFileOK &= value & "|"
                            Next
                            For colidx = 19 To 20
                                OutputFileOK &= filecontents(rowIDX, colidx) & "|"
                            Next
                            OutputFileOK &= vbNewLine
                            totCasesOK += 1
                            totValueOK += filecontents(rowIDX, 18)
                        End If
                    End If
                Else
                    For colidx = 0 To 14
                        outputFileDup &= filecontents(rowIDX, colidx) & "|"
                    Next
                    For colidx = 15 To 18
                        Dim value As Decimal = filecontents(rowIDX, colidx)
                        outputFileDup &= value & "|"
                    Next
                    For colidx = 19 To 20
                        outputFileDup &= filecontents(rowIDX, colidx) & "|"
                    Next
                    outputFileDup &= vbNewLine
                    totCasesDup += 1
                    totValueDup += filecontents(rowIDX, 18)
                End If
            Next
            btnViewInputFile.Enabled = True
            If totCasesOK > 0 Then
                WriteFile(InputFilePath & FileName & "_preprocess.txt", OutputFileOK)
                btnViewOutputFile.Enabled = True
            End If
            If totCasesDup > 0 Then
                WriteFile(InputFilePath & FileName & "_duplicate.txt", outputFileDup)

            End If

            MsgBox("Cases = " & totCasesOK & " Value = " & totValueOK & vbNewLine & _
                   "Duplicate cases = " & totCasesDup & " Value = " & totValueDup)
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_preprocess.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_preprocess.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & "_nonWRP_4200.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_nonWRP_4200.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

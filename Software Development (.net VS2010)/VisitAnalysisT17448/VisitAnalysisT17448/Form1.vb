﻿Public Class Form1

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("FeesSQL")
        'first clear down table
        Dim startDate As Date = CDate("Jun 1, 2012 00:00:00")

        Dim upd_txt As String = "delete from VisitAnalysis2 " & _
                    "where debtorID > 0"
        update_sql(upd_txt)

        Dim debtor_dt As New DataTable

        'get all cases loaded since 1.6.2012
        'RESET START NUMBER
        Dim startDebtorID As Integer = 6754810                '6553422

        LoadDataTable2("DebtRecovery", "SELECT _rowID, clientschemeID " & _
                                                "FROM debtor " & _
                                                "WHERE _rowid > " & startDebtorID & _
                                                " order by _rowid", debtor_dt, False)
        For Each Debtrow In debtor_dt.Rows
            Dim clientSchemeID As Integer = Debtrow.item(1)
            'check client scheme is branch 1/9
            Dim branchID As Integer
            Dim CSArray As Object
            CSArray = GetSQLResultsArray2("DebtRecovery", "SELECT branchID, clientID " & _
                                               "FROM clientScheme " & _
                                               "WHERE _rowid= " & clientSchemeID)
            branchID = CSArray(0)
            If branchID <> 1 And branchID <> 9 Then
                Continue For
            End If
            'ignore test cases
            If CSArray(1) = 1 Or CSArray(1) = 2 Or CSArray(1) = 24 Then
                Continue For
            End If
            Dim debtorID As Integer = Debtrow.Item(0)

            run_debtor(debtorID)
        Next
        MsgBox("Completed")
        Me.Close()
    End Sub
    Private Sub run_debtor(ByVal debtorID As Integer)
        Dim lastAllocationdate As Date = Nothing
        Dim note_dt As New DataTable
        LoadDataTable2("DebtRecovery", "SELECT text, _createdDate " & _
                                           "FROM note " & _
                                           "WHERE type = 'Stage' " & _
                                           " AND (text like '%F C%' or " & _
                                           " text like '%Officer Attendance%') " & _
                                     " AND debtorID = " & debtorID, note_dt, False)
        For Each noterow In note_dt.Rows
            Dim stageDate As Date = noterow(1)
            'see if allocated after stage date
            Dim allocationDate As Date = Nothing
            Dim note2_dt As New DataTable
            LoadDataTable2("DebtRecovery", "SELECT text, _createdDate, type " & _
                                            "FROM note " & _
                                           "WHERE (((type = 'Allocated' or type = 'Note')" & _
                                            " AND text like 'Bailiff%') OR" & _
                                            "   type = 'Stage')" & _
                                            " And _createdDate >= '" & Format(stageDate, "yyyy-MM-dd") & "'" & _
                                      " AND debtorID = " & debtorID & _
                                      " ORDER BY _createdDate", note2_dt, False)
            For Each note2row In note2_dt.Rows
                Dim noteText As String = note2row(0)
                Dim noteType As String = note2row(2)
                If noteType = "Stage" Then
                    If InStr(noteText, "F C") = 0 And _
                        InStr(noteText, "Officer Attendance") = 0 Then
                        Exit For
                    Else
                        Continue For
                    End If
                End If
                allocationDate = note2row(1)
                If lastAllocationdate <> Nothing Then
                    If Format(allocationDate, "yyyy-MM-dd") = Format(lastAllocationdate, "yyyy-MM-dd") Then
                        Continue For
                    End If
                End If
                lastAllocationdate = allocationDate
                'get bailiffid
                Dim startIDX As Integer = InStr(noteText, "ID:")
                If startIDX = 0 Then
                    Continue For
                End If
                Dim endIDX As Integer = InStr(noteText, ")")
                If endIDX <= startIDX Then
                    Continue For
                End If
                Dim bailID As Integer
                Try
                    bailID = Mid(noteText, startIDX + 3, endIDX - startIDX - 3)
                Catch ex As Exception
                    MsgBox("Invalid bailID - debtorid = " & debtorID)
                End Try
                'check bailID is a real bailiff
                Dim addMobileName As String = ""
                addMobileName = GetSQLResults2("DebtRecovery", "SELECT add_mobile_name " & _
                                           "FROM bailiff " & _
                                           "WHERE _rowid= " & bailID)
                If addMobileName = "None" Then
                    Continue For
                End If
                'get visit dates
                Dim visitDates_dt As New DataTable

                LoadDataTable2("DebtRecovery", "SELECT  date_visited, date_allocated from visit" & _
                                     " where debtorID = " & debtorID & _
                                     " and bailiffID = " & bailID & _
                                      " and date_visited >= '" & Format(allocationDate, "yyyy-MM-dd") & "'" & _
                                      " ORDER BY date_visited", visitDates_dt, False)
                
                For Each visitrow In visitDates_dt.Rows
                    Dim visitDate As Date = visitrow(0)
                    Dim TestAllocDate As Date = visitrow(1)
                    If TestAllocDate.ToString("yyyy-MM-dd") <> allocationDate.ToString("yyyy-MM-dd") Then
                        Continue For
                    End If
                    'look for any swp
                    Dim swpAmt_dt As New DataTable
                    Dim swpAmt As Decimal = 0
                    Dim swpFound As Integer = 0
                    LoadDataTable2("DebtRecovery", "select fee_amount, date from Fee " & _
                                             " where debtorID = " & debtorID & _
                                             " and date >= '" & Format(visitDate, "yyyy-MM-dd") & "'" & _
                                             " and type like '%alking%'", swpAmt_dt, False)
                    For Each swprow In swpAmt_dt.Rows
                        Dim feeDate As Date = swprow(1)
                        If Format(feeDAte, "yyyy-MM-dd") <> Format(visitDate, "yyyy-MM-dd") Then
                            Exit For
                        End If
                        swpAmt += swprow(0)
                    Next
                    If swpAmt > 0 Then
                        swpfound = 1
                    End If

                    'save in table
                    upd_txt = "insert into VisitAnalysis2 (debtorID, visitdate, swp) " & _
                        "values (" & debtorID & ",'" & Format(visitDate, "yyyy-MM-dd") & "'," & swpFound & ")"
                    update_sql(upd_txt)
                Next
            Next
            Exit For
        Next
    End Sub

End Class

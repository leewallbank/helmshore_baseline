﻿Imports System.IO

Public Class Form1
    Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
    Dim filename As String = ""
    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(sender As System.Object, e As System.EventArgs) Handles runbtn.Click

        Dim RD531report = New RD531
        Dim myArrayList1 As ArrayList = New ArrayList()

        myArrayList1.Add(startdtp.Value)
        SetCurrentValuesForParameterField1(RD531report, myArrayList1)
        'add
        myArrayList1.Add(enddtp.Value)
        SetCurrentValuesForParameterField2(RD531report, myArrayList1)
        myArrayList1.Add(transFiletbox.Text)
        SetCurrentValuesForParameterField3(RD531report, myArrayList1)
        myArrayList1.Add(transvaluetbox.Text)
        SetCurrentValuesForParameterField4(RD531report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD531report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RD531 TDX LAA Invoice.pdf"
        End With
        Dim filename As String = ""
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = SaveFileDialog1.FileName
        Else
            MsgBox("Report not saved")
            Me.Close()
            Exit Sub
        End If
        exitbtn.Enabled = False
        runbtn.Enabled = False
        Application.DoEvents()
        Dim InputFilePath As String = Path.GetDirectoryName(filename)

        RD531report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD531report.Close()


        Dim RD531Freport = New RD531F
        Dim myArrayList2 As ArrayList = New ArrayList()


        myArrayList2.Add(startdtp.Value)
        SetCurrentValuesForParameterField1(RD531Freport, myArrayList2)
        myArrayList2.Add(enddtp.Value)
        SetCurrentValuesForParameterField2(RD531Freport, myArrayList2)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
       
        SetDBLogonForReport(myConnectionInfo, RD531Freport)
        filename = InputFilePath & "\RD531F TDX LAA Fee Invoice.pdf"

        RD531Freport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD531Freport.Close()

        Dim RD531F4report = New RD531F4
        Dim myArrayList3 As ArrayList = New ArrayList()

        myArrayList3.Add(startdtp.Value)
        SetCurrentValuesForParameterField1(RD531F4report, myArrayList3)
        myArrayList3.Add(enddtp.Value)
        SetCurrentValuesForParameterField2(RD531F4report, myArrayList3)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"

        SetDBLogonForReport(myConnectionInfo, RD531F4report)
        filename = InputFilePath & "\RD531F4 TDX LAA Letter 4 Fee Invoice.pdf"

        RD531F4report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD531F4report.Close()

        MsgBox("Reports saved")

        Me.Close()
    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim startDate As Date = DateAdd(DateInterval.Day, -Weekday(Now) - 5, Now)
        Dim enddate As Date = DateAdd(DateInterval.Day, 6, startDate)
        startdtp.Value = startDate
        enddtp.Value = enddate
    End Sub
End Class

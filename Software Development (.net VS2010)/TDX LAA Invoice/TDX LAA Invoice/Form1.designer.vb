﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.runbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.startdtp = New System.Windows.Forms.DateTimePicker()
        Me.enddtp = New System.Windows.Forms.DateTimePicker()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.transFiletbox = New System.Windows.Forms.TextBox()
        Me.transvaluetbox = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(140, 302)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(75, 23)
        Me.runbtn.TabIndex = 4
        Me.runbtn.Text = "Run Reports"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(277, 362)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 5
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(137, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Start date"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(137, 106)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(52, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "End Date"
        '
        'startdtp
        '
        Me.startdtp.Location = New System.Drawing.Point(99, 64)
        Me.startdtp.Name = "startdtp"
        Me.startdtp.Size = New System.Drawing.Size(145, 20)
        Me.startdtp.TabIndex = 0
        '
        'enddtp
        '
        Me.enddtp.Location = New System.Drawing.Point(99, 122)
        Me.enddtp.Name = "enddtp"
        Me.enddtp.Size = New System.Drawing.Size(145, 20)
        Me.enddtp.TabIndex = 1
        '
        'transFiletbox
        '
        Me.transFiletbox.Location = New System.Drawing.Point(12, 182)
        Me.transFiletbox.Name = "transFiletbox"
        Me.transFiletbox.Size = New System.Drawing.Size(340, 20)
        Me.transFiletbox.TabIndex = 2
        '
        'transvaluetbox
        '
        Me.transvaluetbox.Location = New System.Drawing.Point(115, 239)
        Me.transvaluetbox.Name = "transvaluetbox"
        Me.transvaluetbox.Size = New System.Drawing.Size(100, 20)
        Me.transvaluetbox.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(136, 166)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Transaction File"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(112, 223)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 13)
        Me.Label4.TabIndex = 9
        Me.Label4.Text = "Transaction Value"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(364, 425)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.transvaluetbox)
        Me.Controls.Add(Me.transFiletbox)
        Me.Controls.Add(Me.enddtp)
        Me.Controls.Add(Me.startdtp)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TDX LAA Invoice"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents startdtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents enddtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents transFiletbox As System.Windows.Forms.TextBox
    Friend WithEvents transvaluetbox As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label

End Class

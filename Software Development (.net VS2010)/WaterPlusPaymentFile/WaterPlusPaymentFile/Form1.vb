﻿Imports CommonLibrary
Public Class Form1

    Dim fileAGY As String
    Dim ascii As New System.Text.ASCIIEncoding()

    'Private UUData As New clsUUData
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub createbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles createbtn.Click
        exitbtn.Enabled = False
        createbtn.Enabled = False
        fileAGY = "HDRAGY023Pay" & Format(remit_dtp.Value, "yyyyMMdd") & vbNewLine
       
        'get all csids for STS
        Dim ClientSchemeDatatable As New DataTable
        LoadDataTable("DebtRecovery", "SELECT _rowid, schemeID " & _
                                                "FROM clientScheme " & _
                                                "WHERE clientID in (1931,1943)", ClientSchemeDatatable, False)
        Dim neg_countAGY As Integer = 0
        Dim neg_amtAGY As Integer = 0
        Dim pos_amtAGY As Integer = 0
        Dim pos_countAGY As Integer = 0
        Dim ClientSchemeRow As DataRow
        ProgressBar1.Style = ProgressBarStyle.Marquee

        For Each ClientSchemeRow In ClientSchemeDatatable.Rows
            Dim csID As Integer = ClientSchemeRow(0)
            Dim schemeID As Integer = ClientSchemeRow(1)

            'get all payments for remit date and csID
            Dim PaymentsDatatable As New DataTable
            LoadDataTable("DebtRecovery", "SELECT debtorID, split_debt, split_costs, amount_sourceID, date, client_ref " & _
                                               "FROM Payment P, paysource PS, debtor D " & _
                                               "WHERE P.clientSchemeID = " & csID &
                                               " AND P.status = 'R'" &
                                               " AND PS.direct = 'N'" & _
                                               " AND amount_sourceID = PS._rowID " &
                                               " AND P.DebtorID = D._rowID" &
                                               " AND split_debt <> 0" &
                                               " AND status_date ='" & Format(remit_dtp.Value, "yyyy-MM-dd") & "'", PaymentsDatatable, False)
            Dim PaymentRow As DataRow
            For Each PaymentRow In PaymentsDatatable.Rows
                Application.DoEvents()
                Dim pay_amt As Decimal = PaymentRow.Item(1)
                'get client ref from Debtor table
                Dim ClientRef As String = PaymentRow(5)

                'payment required as pence
                Dim pence As Integer = pay_amt * 100
                Dim sign As String = "+"
                Dim pay_date As Date = PaymentRow.Item(4)
                fileAGY &= ClientRef.PadRight(33)
                If pence < 0 Then
                    sign = "-"
                    neg_countAGY += 1
                    neg_amtAGY += pence
                    pence = pence * -1   'displays value without sign
                Else
                    pos_countAGY += 1
                    pos_amtAGY += pence
                End If

                fileAGY &= Format(pence, "000000000") & sign
                fileAGY &= Format(pay_date, "ddMMyyyy") & vbNewLine
                    
            Next
        Next

        'write trailer
        neg_amtAGY = neg_amtAGY * -1  'displays as positive
        fileAGY &= "TLR" & Format(pos_countAGY, "00000") & Format(neg_countAGY, "00000") & Format(pos_amtAGY, "000000000") & Format(neg_amtAGY, "000000000") & vbNewLine
       
        'write out file

        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "TXT files |*.txt"
            .DefaultExt = ".txt"
            .OverwritePrompt = True
            .FileName = "AGY023pay.txt"
        End With
        Dim filepath As String = SaveFileDialog1.FileName
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, fileAGY, False, ascii)
            SaveFileDialog1.FileName = Replace(SaveFileDialog1.FileName, "AGY023", "NGY023")
            MsgBox("report saved")
        Else
            MsgBox("report not saved")
        End If


        Me.Close()

    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

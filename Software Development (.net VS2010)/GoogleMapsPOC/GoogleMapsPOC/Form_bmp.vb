﻿Imports Google.Api.Maps.Service.Geocoding
Imports Google.Api.Maps.Service.StaticMaps
Imports System.Windows.Media.Imaging

Public Class Form_bmp
    'http://www.codeguru.com/columns/vb/using-google-maps-with-visual-basic-2010.htm
    'https://www.daniweb.com/programming/software-development/code/385622/google-map-control-vb-net

    'Private ehImage As System.Windows.Controls.Image = New System.Windows.Controls.Image() 'Added Element Host from WPF section in toolbox
    Private map = New StaticMap()

    Private Sub Form1_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        Dim PostCodes() As String = {"BB4 4JB", "BB4 4NB", "WN7 3QP"}
        Dim Markers As String = ""


        For Each PostCode As String In PostCodes



            Dim fmqRequest = New GeocodingRequest() 'Request geographical info
            fmqRequest.Address = PostCode 'Address that needs to be converted to geographic coordinates
            fmqRequest.Sensor = "false" 'No built in location sensor


            Dim fmqResponse = GeocodingService.GetResponse(fmqRequest) 'Get response from request

            If fmqResponse.Status = ServiceResponse.Ok Then 'Everything OK, add to Listview
                If Markers <> "" Then Markers &= "|"
                Markers &= fmqResponse.Results.Single().Geometry.Location.Latitude.ToString() + "," + fmqResponse.Results.Single().Geometry.Location.Longitude.ToString

            End If

        Next


        'map.Center = fmqResponse.Results.Single().Geometry.Location.Latitude.ToString() + "," + fmqResponse.Results.Single().Geometry.Location.Longitude.ToString
        'map.Zoom = 14

        map.Size = "400x400"
        map.Markers = Markers
        map.Sensor = "false"




        'Dim mpMap As New BitmapImage
        ' mpMap.BeginInit()
        ' mpMap.CacheOption = BitmapCacheOption.OnDemand 'Cahce only when needed
        ' mpMap.UriSource = map.ToUri() 'Source of map / image

        'mpMap.EndInit()

        ' ElementHost1.Child = ehImage 'Set child control in element host
        'ehImage.Source = mpMap 'Set Image source





    End Sub


    Private Enum ServiceResponse
        Unknown 'Unknown response
        Ok ' Everything is fine
        InvalidRequest 'Address requested was improperly formatted
        ZeroResults 'No results to return, possibly a non-existant address
        OverQueryLimit 'Too many request on a given day
        RequestDenied 'Reques denied
    End Enum

    Private Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click
        For Each Marker As Object In map.markers
            MsgBox("marker")
        Next
    End Sub
End Class

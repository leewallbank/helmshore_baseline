﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization
Imports System.Net.Mail

Public Class frmMain

    Const Separator As String = "|"

    Private AccountType As New Dictionary(Of String, String)

    Private InputFilePath As String, InputFileName As String, InputFileExt As String
    'Private OutputFiles As New ArrayList()
    'Private NewCases As New ArrayList()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String, InputLineArray() As String, OutputLineArray(5) As String, OutputTelNumber() As String
        Dim LineNumber As Integer

        Try

            FileDialog.Filter = "GB Results files|*.csv|All files (*.*)|*.*"
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            InputFileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            InputFileExt = Path.GetExtension(FileDialog.FileName)

            If System.IO.File.Exists(InputFilePath & InputFileName & "_PreProcessed.txt") Then System.IO.File.Delete(InputFilePath & InputFileName & "_PreProcessed.txt")

            Dim FileContents() As String = System.IO.File.ReadAllLines(InputFilePath & InputFileName & InputFileExt)
            ProgressBar.Maximum = UBound(FileContents) + 1

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                Application.DoEvents() ' without this line, the button disappears until processing is complete

                InputLineArray = Split(InputLine, ",", Chr(34)) ' the file has " qualifiers for each field

                For LoopCount As Integer = 0 To UBound(InputLineArray)
                    InputLineArray(LoopCount) = InputLineArray(LoopCount).Replace(Chr(34), "") ' remove the qualifiers
                Next LoopCount

                ReDim OutputLineArray(5) ' clears the array for reuse

                'Build up an array of phone numbers Landline 1, Mobile 1, Landline 2, Mobile 2 etc
                OutputTelNumber = (InputLineArray(15) & Separator & _
                                   InputLineArray(25) & Separator & _
                                   InputLineArray(17) & Separator & _
                                   InputLineArray(27) & Separator & _
                                   InputLineArray(19) & Separator & _
                                   InputLineArray(29) & Separator & _
                                   InputLineArray(21) & Separator & _
                                   InputLineArray(31) & Separator & _
                                   InputLineArray(23) & Separator & _
                                   InputLineArray(33)).Split({Separator}, StringSplitOptions.RemoveEmptyEntries) ' RemoveEmptyEntries to get rid of any gaps in the array

                If Not UBound(OutputTelNumber) = -1 Then
                    OutputLineArray(0) = InputLineArray(0) ' DebtorID
                    OutputLineArray(1) = OutputTelNumber(0) ' Phone number 1

                    If UBound(OutputTelNumber) >= 1 Then OutputLineArray(2) = OutputTelNumber(1) ' Phone number 2
                    If UBound(OutputTelNumber) >= 2 Then OutputLineArray(3) = OutputTelNumber(2) ' Phone number 3
                    If UBound(OutputTelNumber) >= 3 Then OutputLineArray(4) = OutputTelNumber(3) ' Phone number 4

                    If UBound(OutputTelNumber) >= 4 Then ' Append any remaining to the last column
                        For LoopCount As Integer = 4 To UBound(OutputTelNumber)
                            OutputLineArray(5) &= OutputTelNumber(LoopCount) & ";"
                        Next LoopCount
                    End If

                    AppendToFile(InputFilePath & InputFileName & "_PreProcessed.txt", String.Join(Separator, OutputLineArray) & vbCrLf)
                End If

            Next InputLine

            ProgressBar.Value += 1
            AuditLog = "File processed: " & InputFilePath & InputFileName & InputFileExt & vbCrLf
            AuditLog &= "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            WriteFile(InputFilePath & InputFileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & InputFileName & "_Error.txt", ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            If ExceptionLog <> "" Then WriteFile(InputFilePath & InputFileName & "_Exception.txt", ExceptionLog)

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Clipboard.SetText("notepad.exe " & InputFilePath & InputFileName & InputFileExt)
        If File.Exists(InputFilePath & InputFileName & InputFileExt) Then System.Diagnostics.Process.Start("wordpad.exe", InputFilePath & InputFileName & InputFileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If File.Exists(InputFilePath & InputFileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & InputFileName & "_Audit.txt")
        If File.Exists(InputFilePath & InputFileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & InputFileName & "_Error.txt")
        If File.Exists(InputFilePath & InputFileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & InputFileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        If File.Exists(InputFilePath & InputFileName & "_PreProcessed.txt") Then System.Diagnostics.Process.Start(InputFilePath & InputFileName & "_PreProcessed.txt")
    End Sub
End Class


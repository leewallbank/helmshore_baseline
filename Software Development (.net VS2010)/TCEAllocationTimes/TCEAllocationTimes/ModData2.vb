﻿Imports System.Configuration
Imports System.Data
Imports System.Data.Odbc
Module ModData2
    Public DBCon As New OdbcConnection
    Public DBConFees As New OdbcConnection
    Public conn As New Odbc.OdbcConnection()
    Public Sub ConnectDb2(ByVal ConnStr As String)
        Try
            If Not IsNothing(DBCon) Then
                'This is only necessary following an exception...
                If DBCon.State = ConnectionState.Open Then DBCon.Close()
            End If

            DBCon.ConnectionString = ConfigurationManager.ConnectionStrings(ConnStr).ConnectionString
            DBCon.Open()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub ConnectDb2Fees(ByVal ConnStr As String)
        Try
            If Not IsNothing(DBConFees) Then
                'This is only necessary following an exception...
                If DBConFees.State = ConnectionState.Open Then DBConFees.Close()
            End If

            DBConFees.ConnectionString = ConfigurationManager.ConnectionStrings(ConnStr).ConnectionString
            DBConFees.Open()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub DisconnectDb2()
        DBCon.Close()
        DBCon.Dispose()
    End Sub
    Sub update_sql(ByVal upd_txt As String)
        Try
            If conn.State = ConnectionState.Closed Then
                Connect_sqlDb()
            End If
            Dim ODBCCMD As Odbc.OdbcCommand

            'Define attachment to database table specifics
            ODBCCMD = New Odbc.OdbcCommand
            With ODBCCMD
                .Connection = conn
                .CommandText = upd_txt
                .ExecuteNonQuery()
            End With

            'Clean up the connection
            ODBCCMD = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub Connect_sqlDb()

        Try
            If Not IsNothing(conn) Then
                'This is only necessary following an exception...
                If conn.State = ConnectionState.Open Then conn.Close()
            End If
            conn.ConnectionString = ""
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("FeesSQL").ConnectionString
            conn.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings("FeesSQL").ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings("feesSQL").ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub

    Public Sub LoadDataTable2(ByVal ConnStr As String, ByVal Sql As String, ByRef DataTable As DataTable, Optional ByVal Preserve As Boolean = False)
       
        Try
            Dim conn As System.Data.Odbc.OdbcConnection
            If ConnStr = "DebtRecovery" Then
                conn = DBCon
            Else
                conn = DBConFees
            End If
            Dim da As New OdbcDataAdapter(Sql, conn)
            Application.DoEvents()
            If Preserve = False Then DataTable.Clear()

            da.Fill(DataTable)
            da.Dispose()
            da = Nothing

            'DisconnectDb2()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Public Sub ExecStoredProc(ByVal ConnStr As String, ByVal SQL As String, Optional ByVal TimeoutSeconds As Short = 60)

        Try
            ConnectDb2(ConnStr)

            Dim Comm As New OdbcCommand(SQL, DBCon)
            Comm.CommandTimeout = TimeoutSeconds
            Comm.ExecuteNonQuery()

            DisconnectDb2()

            Comm.Dispose()
            Comm = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub
    Public Function GetSQLResults2(ByVal ConnStr As String, ByVal SQL As String) As String
        GetSQLResults2 = Nothing
        
        Try
            Dim conn As System.Data.Odbc.OdbcConnection
            If ConnStr = "DebtRecovery" Then
                conn = DBCon
            Else
                conn = DBConFees
            End If

            Dim Reader As OdbcDataReader
            Dim Comm As New OdbcCommand(SQL, conn)

            Reader = Comm.ExecuteReader

            If Reader.HasRows Then
                Reader.Read()
                GetSQLResults2 = Reader(0)
            End If

            'DisconnectDb2()

            Comm.Dispose()
            Comm = Nothing

        Catch ex As Exception
            'MsgBox(ex.Message)
        End Try
    End Function

    Public Function GetSQLResultsArray2(ByVal ConnStr As String, ByVal SQL As String) As Object()
        GetSQLResultsArray2 = Nothing
        Dim ArrayList As New ArrayList()
        Dim FieldCount As Integer
       
        Try
            Dim conn As System.Data.Odbc.OdbcConnection
            If ConnStr = "DebtRecovery" Then
                conn = DBCon
            Else
                conn = DBConFees
            End If

            Dim Reader As OdbcDataReader
            Dim Comm As New OdbcCommand(SQL, conn)
            Application.DoEvents()
            Reader = Comm.ExecuteReader

            If Reader.HasRows Then
                Reader.Read()
                For FieldCount = 1 To Reader.FieldCount
                    ArrayList.Add(Reader(FieldCount - 1))
                Next FieldCount
            End If

            'DisconnectDb2()

            Comm.Dispose()
            Comm = Nothing

            If Not IsNothing(ArrayList) Then GetSQLResultsArray2 = ArrayList.ToArray

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Function

End Module

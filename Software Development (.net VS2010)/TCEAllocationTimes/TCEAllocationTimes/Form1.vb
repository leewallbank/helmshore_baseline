﻿Imports CommonLibrary
Public Class Form1
    Dim type As String
    Dim start_date As Date = CDate("2014 Apr 1")
    Dim upd_txt As String
    Dim startDebtorID As Integer
    Private Sub run_debtorID(ByVal debtorid As Integer, ByVal stageType As String, ByVal stageStart As Date, ByVal stageEnd As Date, ByVal allocationDate As Date)
        'get first allocation date
        'If debtorid = 8813611 Then
        '    debtorid = 8813611
        'End If
        Dim visitTable As New DataTable
        Dim days2Allocation As Integer = 0
        Dim allocated As Boolean = False
        'If allocated check entry on visit table
        If allocationDate <> Nothing Then
            allocated = True
            Dim allocatedPlus1 As Date = DateAdd(DateInterval.Day, 1, allocationDate)
            LoadDataTable2("DebtRecovery", "select bailiffID  from visit" & _
                            " where debtorID = " & debtorid & _
                            " and date_allocated >= '" & Format(allocationDate, "yyyy-MM-dd") & "'" & _
                            " and date_allocated < '" & Format(allocatedPlus1, "yyyy-MM-dd") & "'", visitTable, False)
            Dim bailiffFound As Boolean = False
            Dim visitCount As Integer = visitTable.Rows.Count
            For Each visitRow In visitTable.Rows
                Dim bailiffID As Integer
                Try
                    bailiffID = visitRow(0)
                Catch ex As Exception
                    Continue For
                End Try

                'check bailiff is kosher
                Dim addMobileName As String = "none"
                Try

                    addMobileName = GetSQLResults2("DebtRecovery", "select add_mobile_name " & _
                                                 " from Bailiff" & _
                                                 " where _rowid = " & bailiffID)
                Catch ex As Exception

                End Try
                If addMobileName = "none" Then
                    Continue For
                End If
                bailiffFound = True
                'see if an entry already exist for debtor and stagetype
                Dim stageArray As Array
                Try
                    stageArray = GetSQLResultsArray2("FeesSQL", "select TCEAdays_to_allocation" & _
                                           " from TCEAllocationstats" & _
                                           " where TCEADebtorID = " & debtorid & _
                                           " and TCEAstage = '" & stageType & "'")
                    days2Allocation = stageArray(0)
                Catch ex As Exception
                    days2Allocation = 99999
                End Try

                Dim newDays2Allocation As Integer = DateDiff(DateInterval.Day, stageStart, allocationDate)
                If newDays2Allocation < days2Allocation Then
                    'delete prev entry
                    upd_txt = "delete from TCEAllocationstats " & _
                                            " where TCEAstage = '" & stageType & "'" & _
                                            " and TCEAdebtorID = " & debtorid
                    update_sql(upd_txt)
                    'insert a new one
                    Dim allocatedNumber As String
                    If allocated Then
                        allocatedNumber = 1
                    Else
                        allocatedNumber = 0
                    End If
                    upd_txt = "insert into TCEAllocationstats (TCEAdebtorID, " & _
                           "TCEAstage, TCEAstage_date, TCEAallocation_date,TCEAdays_to_allocation,TCEAallocated)" & _
                           "values (" & debtorid & ",'" & stageType & "','" & Format(stageStart, "yyyy-MM-dd") & "','" & Format(allocationDate, "yyyy-MM-dd") & "'," & _
                           newDays2Allocation & "," & allocatedNumber & ")"
                    update_sql(upd_txt)
                End If
            Next
            If bailiffFound = False Then
                'see if entry on Bailiff Allocations table
                Dim BATTable As New DataTable
                Dim stageName As String
                If stageType = "E" Then
                    stageName = "Enforcement"
                Else
                    stageName = "Further Enforcement"
                End If
                Dim allocationdateplus1 As Date = DateAdd(DateInterval.Day, 1, allocationDate)
                LoadDataTable2("FeesSQL", "select bail_allocation_date from BailiffAllocationTable" & _
                           " where bail_debtorID = " & debtorid & _
                           " and bail_allocation_date >= '" & Format(allocationDate, "yyyy-MM-dd") & "'" & _
                           " and bail_allocation_date < '" & Format(allocationdateplus1, "yyyy-MM-dd") & "'" & _
                           " and bail_stage_name = '" & stageName & "'" & _
                           " ORDER BY bail_allocation_date", BATTable, False)
                For Each BATrow In BATTable.Rows
                    allocationDate = BATrow(0)
                    allocated = True
                    'see if an entry already exist for debtor and stagetype
                    Dim stageArray As Array
                    Try
                        stageArray = GetSQLResultsArray2("FeesSQL", "select TCEAdays_to_allocation" & _
                                               " from TCEAllocationstats" & _
                                               " where TCEADebtorID = " & debtorid & _
                                               " and TCEAstage = '" & stageType & "'")
                        days2Allocation = stageArray(0)
                    Catch ex As Exception
                        days2Allocation = 99999
                    End Try

                    Dim newDays2Allocation As Integer = DateDiff(DateInterval.Day, stageStart, allocationDate)
                    If newDays2Allocation < days2Allocation Then
                        'delete prev entry
                        upd_txt = "delete from TCEAllocationstats " & _
                                                " where TCEAstage = '" & stageType & "'" & _
                                                " and TCEAdebtorID = " & debtorid
                        update_sql(upd_txt)
                        'insert a new one
                        Dim allocatedNumber As String
                        If allocated Then
                            allocatedNumber = 1
                        Else
                            allocatedNumber = 0
                        End If
                        upd_txt = "insert into TCEAllocationstats (TCEAdebtorID, " & _
                               "TCEAstage, TCEAstage_date,TCEAallocation_date,TCEAdays_to_allocation,TCEAallocated)" & _
                               "values (" & debtorid & ",'" & stageType & "','" & Format(stageStart, "yyyy-MM-dd") & "','" & Format(allocationDate, "yyyy-MM-dd") & "'," & _
                               newDays2Allocation & "," & allocatedNumber & ")"
                        update_sql(upd_txt)
                    End If
                Next
            End If
        Else
            'no allocation note so check for entry on visit table
            LoadDataTable2("DebtRecovery", "select bailiffID, date_allocated  from visit" & _
                            " where debtorID = " & debtorid & _
                            " and date_allocated >= '" & Format(stageStart, "yyyy-MM-dd") & "'" & _
                            " and date_allocated < '" & Format(stageEnd, "yyyy-MM-dd") & "'" & _
                            " ORDER BY date_allocated", visitTable, False)
            Dim bailiff2Found As Boolean = False
            For Each visitRow In visitTable.Rows
                Dim bailiffID As Integer
                Try
                    bailiffID = visitRow(0)
                Catch ex As Exception
                    Continue For
                End Try

                'check bailiff is kosher
                Dim addMobileName As String = "none"
                Try

                    addMobileName = GetSQLResults2("DebtRecovery", "select add_mobile_name " & _
                                                 " from Bailiff" & _
                                                 " where _rowid = " & bailiffID)
                Catch ex As Exception

                End Try
                If addMobileName = "none" Then
                    Continue For
                End If
                bailiff2Found = True
                allocationDate = visitRow(1)
                allocated = True
                'see if an entry already exist for debtor and stagetype
                Dim stageArray As Array
                Try
                    stageArray = GetSQLResultsArray2("FeesSQL", "select TCEAdays_to_allocation" & _
                                           " from TCEAllocationstats" & _
                                           " where TCEADebtorID = " & debtorid & _
                                           " and TCEAstage = '" & stageType & "'")
                    days2Allocation = stageArray(0)
                Catch ex As Exception
                    days2Allocation = 99999
                End Try

                Dim newDays2Allocation As Integer = DateDiff(DateInterval.Day, stageStart, allocationDate)
                If newDays2Allocation < days2Allocation Then
                    'delete prev entry
                    upd_txt = "delete from TCEAllocationstats " & _
                                            " where TCEAstage = '" & stageType & "'" & _
                                            " and TCEAdebtorID = " & debtorid
                    update_sql(upd_txt)
                    'insert a new one
                    Dim allocatedNumber As String
                    If allocated Then
                        allocatedNumber = 1
                    Else
                        allocatedNumber = 0
                    End If
                    upd_txt = "insert into TCEAllocationstats (TCEAdebtorID, " & _
                           "TCEAstage, TCEAstage_date, TCEAallocation_date,TCEAdays_to_allocation,TCEAallocated)" & _
                           "values (" & debtorid & ",'" & stageType & "','" & Format(stageStart, "yyyy-MM-dd") & "','" & Format(allocationDate, "yyyy-MM-dd") & "'," & _
                           newDays2Allocation & "," & allocatedNumber & ")"
                    update_sql(upd_txt)
                End If
            Next
            If Not bailiff2Found Then
                'see if entry in bailiff allocations table
                Dim BATTable As New DataTable
                Dim stageName As String
                If stageType = "E" Then
                    stageName = "Enforcement"
                Else
                    stageName = "Further Enforcement"
                End If
                If stageEnd = Nothing Then
                    stageEnd = allocationDate
                End If

                LoadDataTable2("FeesSQL", "select bail_allocation_date from BailiffAllocationTable" & _
                           " where bail_debtorID = " & debtorid & _
                           " and bail_allocation_date >= '" & Format(stageStart, "yyyy-MM-dd") & "'" & _
                           " and bail_allocation_date < '" & Format(stageEnd, "yyyy-MM-dd") & "'" & _
                           " and bail_stage_name = '" & stageName & "'" & _
                           " ORDER BY bail_allocation_date", BATTable, False)
                For Each BATrow In BATTable.Rows
                    allocationDate = BATrow(0)
                    allocated = True
                    bailiff2Found = True
                    'see if an entry already exist for debtor and stagetype
                    Dim stageArray As Array
                    Try
                        stageArray = GetSQLResultsArray2("FeesSQL", "select TCEAdays_to_allocation" & _
                                               " from TCEAllocationstats" & _
                                               " where TCEADebtorID = " & debtorid & _
                                               " and TCEAstage = '" & stageType & "'")
                        days2Allocation = stageArray(0)
                    Catch ex As Exception
                        days2Allocation = 99999
                    End Try

                    Dim newDays2Allocation As Integer = DateDiff(DateInterval.Day, stageStart, allocationDate)
                    If newDays2Allocation < days2Allocation Then
                        'delete prev entry
                        upd_txt = "delete from TCEAllocationstats " & _
                                                " where TCEAstage = '" & stageType & "'" & _
                                                " and TCEAdebtorID = " & debtorid
                        update_sql(upd_txt)
                        'insert a new one
                        Dim allocatedNumber As String
                        If allocated Then
                            allocatedNumber = 1
                        Else
                            allocatedNumber = 0
                        End If
                        upd_txt = "insert into TCEAllocationstats (TCEAdebtorID, " & _
                               "TCEAstage, TCEAstage_date, TCEAallocation_date,TCEAdays_to_allocation,TCEAallocated)" & _
                               "values (" & debtorid & ",'" & stageType & "','" & Format(stageStart, "yyyy-MM-dd") & "','" & Format(allocationDate, "yyyy-MM-dd") & "'," & _
                               newDays2Allocation & "," & allocatedNumber & ")"
                        update_sql(upd_txt)
                    End If
                Next
                If Not bailiff2Found Then
                    'see if an entry already exist for debtor and stagetype
                    Dim stageArray As Array
                    Try
                        stageArray = GetSQLResultsArray2("FeesSQL", "select TCEAdays_to_allocation" & _
                                               " from TCEAllocationstats" & _
                                               " where TCEADebtorID = " & debtorid & _
                                               " and TCEAstage = '" & stageType & "'")
                        days2Allocation = stageArray(0)
                    Catch ex As Exception
                        days2Allocation = 99999
                    End Try
                    If days2Allocation = 99999 Then
                        'delete prev entry
                        upd_txt = "delete from TCEAllocationstats " & _
                                                " where TCEAstage = '" & stageType & "'" & _
                                                " and TCEAdebtorID = " & debtorid
                        update_sql(upd_txt)
                        Dim allocatedNumber As String = 0
                        Try
                            If allocationDate = "" Then
                                allocationDate = CDate("jan 1, 1900")
                            End If
                        Catch ex As Exception
                            allocationDate = CDate("jan 1, 1900")
                        End Try
                       
                        upd_txt = "insert into TCEAllocationstats (TCEAdebtorID, " & _
                               "TCEAstage, TCEAstage_date, TCEAallocation_date,TCEAdays_to_allocation,TCEAallocated)" & _
                               "values (" & debtorid & ",'" & stageType & "','" & Format(stageStart, "yyyy-MM-dd") & "','" & Format(allocationDate, "yyyy-MM-dd") & "'," & _
                               days2Allocation & "," & allocatedNumber & ")"
                        Try
                            update_sql(upd_txt)
                        Catch ex As Exception

                        End Try

                    End If
                End If
            End If
        End If
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("FeesSQL")

        'clear down table
        'upd_txt = "delete from TCEAllocationstats " & _
        '                " where TCEAdebtorID > 0 "
        'update_sql(upd_txt)

        'get all cases loaded since 6.4.2014
        Dim debtor_dt As New DataTable

        'get all cases loaded since 6.4.2014

        startDebtorID = 8813594

        'startDebtorID = 9331982

        LoadDataTable2("DebtRecovery", "SELECT _rowID, clientSchemeID" & _
                                  " FROM Debtor " & _
                                  " WHERE _rowID > " & startDebtorID & _
                                  " order by _rowID", debtor_dt, False)
        For Each Debtrow In debtor_dt.Rows
            Dim debtorID As Integer = Debtrow(0)
            Dim ClientSchemeID As Integer = Debtrow(1)
            'check client scheme is branch 1
            Dim branchID As Integer
            Dim CSArray As Object
            CSArray = GetSQLResultsArray2("DebtRecovery", "SELECT branchID, clientID " & _
                                          " FROM clientScheme " & _
                                          " WHERE _rowid= " & ClientSchemeID)
            branchID = CSArray(0)
            If branchID <> 1 Then
                Continue For
            End If
            'ignore test cases
            If CSArray(1) = 1 Or CSArray(1) = 2 Or CSArray(1) = 24 Then
                Continue For
            End If

            'see if allocated at stage enforcement or further enforcement
            Dim note_dt As New DataTable

            LoadDataTable2("DebtRecovery", "SELECT type, text, _createdDate" & _
                                      " FROM Note " & _
                                      " WHERE debtorID = " & debtorID & _
                                      " and (type = 'Stage' or type = 'Allocated')" & _
                                      " order by _rowID", note_dt, False)
            Dim stageType As String = ""
            Dim stageStart As Date = Nothing
            Dim stageEnd As Date = Nothing
            Dim allocationDate As Date = Nothing
            Dim noteCount As Integer = note_dt.Rows.Count - 1
            Dim noteIDX As Integer = 0
            For Each Noterow In note_dt.Rows
                Dim noteType As String = Noterow(0)
                Dim noteText As String = Noterow(1)
                Dim noteDate As Date = Noterow(2)
                noteDate = CDate(Format(noteDate, "yyyy-MMM-dd") & " 00:00:00")
                noteIDX += 1
                If noteType = "Stage" Then
                    If stageType <> "" Then
                        stageEnd = noteDate
                        run_debtorID(debtorID, stageType, stageStart, stageEnd, allocationDate)
                        stageType = ""
                        stageEnd = Nothing
                    End If
                    stageStart = noteDate
                    If InStr(noteText, "Enforcement") = 0 Then
                        Continue For
                    End If
                    If InStr(noteText, "Further") > 0 Then
                        stageType = "FE"
                    ElseIf InStr(noteText, "Letter") = 0 Then
                        stageType = "E"
                    End If
                    'don't forget last one
                    If stageType <> "" And noteIDX >= note_dt.Rows.Count Then
                        stageEnd = Now
                        run_debtorID(debtorID, stageType, stageStart, stageEnd, allocationDate)
                    End If

                Else
                    'allocated
                    If stageType <> "" Then
                        allocationDate = noteDate
                        run_debtorID(debtorID, stageType, stageStart, stageEnd, allocationDate)
                        allocationDate = Nothing
                    End If
                End If
            Next
        Next
        'MsgBox("Completed")
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

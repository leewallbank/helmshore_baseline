﻿Imports CommonLibrary

Public Class frmCaseSummary
    Private Map As frmMap
    Private SummaryData As New clsCaseSummaryData
    Private ListData As New clsListData
    ' Private Map As frmMap
    ' These cannot be instantiated here as the datagridviews have not been instantiated at this point but they need to be declared here to achieve the right scope...

    Private StageGridState As clsGridState
    Private ClientGridState As clsGridState
    Private SchemeGridState As clsGridState
    Private InYearGridState As clsGridState
    Private PostcodeAreaGridState As clsGridState
    Private DebtYearGridState As clsGridState
    Private LinkedPIFGridState As clsGridState
    Private LinkedTraceGridState As clsGridState
    Private IscompanyGridState As clsGridState
    Private PDLCheckGridState As clsGridState
    Private PDLCheckByGridState As clsGridState

    Private ColSort As String

    Private ParamList As String

    Private RightClickColIdx As Integer ' Request ref 64648

    'Private RefreshDBClicked As Boolean = False
    'Private RefreshRunningMsgShown As Boolean = False

#Region "New and open"

    Public Sub New()
        Try
            InitializeComponent()

            SetFormIcon(Me)

            cmdRefreshDB.Visible = UserCanRefresh

            ParamList = UserCompanyID.ToString & ",null,null,null,null,null,null,null,null,null,null,null,null," & BalanceType() ' i.e. top level ' BalanceType added TS 12/Feb/2015. Request ref 40282

            ' Now instantiate these
            '   Map = New frmMap(dgvSummary)
            StageGridState = New clsGridState(dgvStageName)
            ClientGridState = New clsGridState(dgvClientName)
            SchemeGridState = New clsGridState(dgvSchemeName)
            InYearGridState = New clsGridState(dgvInYear)
            PostcodeAreaGridState = New clsGridState(dgvPostcodeArea)
            DebtYearGridState = New clsGridState(dgvDebtYear)
            LinkedPIFGridState = New clsGridState(dgvLinkedPIF)
            LinkedTraceGridState = New clsGridState(dgvLinkedTrace)
            IscompanyGridState = New clsGridState(dgvIsCompany)
            PDLCheckGridState = New clsGridState(dgvPDLCheck)
            PDLCheckByGridState = New clsGridState(dgvPDLCheckBy)

            AddControlHandlers()

            cmsSummary.Items.Add("View Cases")
            cmsSummary.Items.Add("Map Cases")
            cmsSummary.Items.Add("Copy")
            cmsSummary.Items.Add("Select All")
            cmsSummary.Items.Add("Select Column") ' Request ref 64648

            'cmsList.Items.Add("Consortiums...") commented out TS 08/Sep/2016. Request ref 90025
            'cmsList.Items.Add("Set warning levels") commented out TS 08/Sep/2016. Request ref 90026
            cmsList.Items.Add("Regions")
            cmsList.Items.Add("Copy")
            cmsList.Items.Add("Select All")

            cmsForm.Items.Add("Bailiff View")
            cmsForm.Items.Add("Post Enforcement View")
            cmsForm.Items.Add("EA Update View")
            cmsForm.Items.Add("Regional View")

            ' The following commented out TS 08/Sep/2016. Request ref 90025
            'ListData.GetConsortiums(UserCompanyID)

            'Dim Consortium As ToolStripMenuItem = Convert.ChangeType(cmsList.Items(0), GetType(ToolStripMenuItem))

            'For Each Row As DataRow In ListData.ConsortiumDataView.ToTable(True, "Consortium").Rows
            '    Dim ConsortiumSubMenuItem As ToolStripMenuItem = New ToolStripMenuItem(Row.Item(0).ToString)

            '    AddHandler ConsortiumSubMenuItem.Click, AddressOf ConsortiumContextSubMenu_Click
            '    Consortium.DropDownItems.Add(ConsortiumSubMenuItem)
            'Next Row
            ' end of 90025 section

            ' Added TS 09/Feb/2015. Request ref 38930
            ListData.GetPostcodeAreaRegions()

            Dim Region As ToolStripMenuItem = Convert.ChangeType(cmsList.Items(0), GetType(ToolStripMenuItem)) ' 2 changed to 0 TS 08/Sep/2016. Request ref 90026 and 90025

            For Each Row As DataRow In ListData.PostcodeAreaRegionDataView.ToTable(True, {"RegionDesc", "RegionID"}).Rows
                Dim RegionSubMenuItem As ToolStripMenuItem = New ToolStripMenuItem(Row.Item(0).ToString)

                RegionSubMenuItem.Tag = Row.Item(1) ' Added TS 21/Jun/2016. Request ref 83034

                AddHandler RegionSubMenuItem.Click, AddressOf RegionContextSubMenu_Click
                Region.DropDownItems.Add(RegionSubMenuItem)
            Next Row
            ' end of 38930

            'SummaryData.GetSummary(ParamList, True, "P")
            SummaryData.GetSummary(ParamList, True)
            dgvSummary.DataSource = SummaryData.SummaryDataView

            FormatGridColumns(dgvSummary, GetStages)

            ListData.GetCaseList(ParamList)
            SetListGrids()
            FormatListColumns()

            'cboPeriodType.DataSource = SummaryData.PeriodTypeDataView
            'cboPeriodType.ValueMember = "PeriodTypeID"
            'cboPeriodType.DisplayMember = "PeriodTypeDesc"

            cboCompany.DataSource = SummaryData.CompanyDataView
            cboCompany.ValueMember = "CompanyID"
            cboCompany.DisplayMember = "CompanyDesc"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmSummary_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            RemoveSelectionHandlers()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmCaseSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        Try
            If e.Button = MouseButtons.Right Then
                cmsForm.Show(sender, e.Location)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmSummary_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            HighlightWarnings()
            SetSelections() 'Arrays will be nothing so clear all selections - by default the first row in a datagridview is always selected
            dgvSummary.ClearSelection()

            radAbs.Checked = True
            cboCompany.Text = UserCompany
            chkTopClients.Checked = False
            'cboPeriodType.SelectedValue = 8 ' changed from 1 TS 18/Dec/2013
            'cboDisplaySet.Text = "Periods"

            AddHandler radAbs.CheckedChanged, AddressOf radAbs_CheckedChanged
            AddHandler chkTopClients.CheckedChanged, AddressOf chkTopClients_CheckedChanged

            AddSelectionHandlers()

            SetcmdRefreshTip()

            ' The following section commented out TS 08/Sep/2016. Request ref 90026
            ' This is duplicated from FormatListColumns as that sub runs before the form is shown
            'For Each Row As DataGridViewRow In dgvClientName.Rows
            '    SummaryData.UnderAllocatedClientDataView.RowFilter = "ClientName = '" & Row.Cells("ClientName").Value & "'"
            '    If SummaryData.UnderAllocatedClientDataView.Count > 0 Then
            '        Row.DefaultCellStyle.ForeColor = Color.Red
            '    Else
            '        Row.DefaultCellStyle.ForeColor = Color.Black
            '    End If
            'Next Row
            ' end of 90026 section

            SetStageColour() ' Added TS 24/Jul/2014 26706

            ' This section added TS 08/Dec/2015. Request ref 66905
            dgvInYear.Columns(0).ToolTipText = "Was the case loaded in this financial year?"
            dgvLinkedPIF.Columns(0).ToolTipText = "Where a linked case has PIF in the last year."
            ' end of 66905 section

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "Refresh"

    Private Sub RefreshGrid()
        Try
            ParamList = cboCompany.SelectedValue.ToString & ","
            ParamList &= GetParam(dgvStageName, "StageName") & ","
            ParamList &= GetParam(dgvClientName, "ClientName") & ","
            ParamList &= GetParam(dgvSchemeName, "SchemeName") & ","
            ParamList &= GetParam(dgvInYear, "InYear") & ","
            ParamList &= GetParam(dgvPostcodeArea, "PostcodeArea") & ","
            ParamList &= GetParam(dgvDebtYear, "DebtYear") & ","
            ParamList &= GetParam(dgvLinkedPIF, "LinkedPIF") & ","
            ParamList &= GetParam(dgvLinkedTrace, "LinkedTrace") & ","
            ParamList &= GetParam(dgvIsCompany, "IsCompany") & ","
            ParamList &= GetParam(dgvPDLCheck, "PDLCheck") & ","
            ParamList &= GetParam(dgvPDLCheckBy, "PDLCheckBy") & ","

            ParamList &= chkTopClients.Checked.ToString

            ParamList &= "," & BalanceType() ' added TS 12/Feb/2015. Request ref 40282

            'ParamList &= "," & cboPeriodType.SelectedValue.ToString

            PreRefresh(Me)

            RemoveSelectionHandlers()

            GetSelections()

            SummaryData.GetSummary(ParamList, radAbs.Checked)
            dgvSummary.DataSource = SummaryData.SummaryDataView

            FormatGridColumns(dgvSummary, GetStages)
            HighlightWarnings()

            ListData.GetCaseList(ParamList)
            SetListGrids()
            FormatListColumns()

            SetSelections()

            AddSelectionHandlers()

            PostRefresh(Me)

            dgvSummary.ClearSelection() ' The first cell always get selected

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function GetParam(ByRef DataGrid As DataGridView, ByVal ColumnName As String) As String
        GetParam = Nothing

        Try
            ' Used when selection criteria change
            Dim Param As String = Nothing

            For Each dr As DataGridViewRow In DataGrid.SelectedRows
                If Not IsNothing(Param) Then Param += vbTab
                Param += dr.Cells(ColumnName).Value
            Next dr

            If IsNothing(Param) Then
                Param = "null"
            Else
                Param = "'" & Param.Replace("'", "''''") & "'" ' Portal task ref 16716
            End If

            Return Param

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function GetParam(ByVal ParamColumn As String, ByRef Cell As DataGridViewCell, ByRef ListDataGridView As DataGridView)
        GetParam = Nothing

        Try
            ' Used when detail for a particular cell in the summary grid is retrieved
            Dim Param As String = ""

            If IsNothing(Cell.DataGridView.Columns(ParamColumn)) Then
                ' The column may not be present as only one criteria is applicable
                If ListDataGridView.SelectedRows.Count = 1 Then
                    ' Param = "'" & ListDataGridView.SelectedRows(0).Cells(0).Value & "'"
                    Param = "'" & ListDataGridView.SelectedRows(0).Cells(0).Value.Replace("'", "''''") & "'" ' Portal task ref 16716
                Else
                    Param = "null"
                End If
            Else
                Param = "'" & Cell.DataGridView.Item(ParamColumn, Cell.RowIndex).Value.Replace("'", "''''") & "'" ' Portal task ref 16716
            End If

            Return Param

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function
#End Region

#Region "Grid selection events"

    Private Sub cboCompany_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboCompany.SelectedValueChanged
        Try
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    CType(Control, DataGridView).FirstDisplayedScrollingRowIndex = 0
                    CType(Control, DataGridView).ClearSelection()
                End If
            Next Control

            RefreshGrid()

            ' the following commented out TS 08/Sep/2016. Request ref 90025
            ' added TS 26/Apr/2016. Request ref 78340
            'ListData.GetConsortiums(cboCompany.SelectedValue)

            'Dim Consortium As ToolStripMenuItem = Convert.ChangeType(cmsList.Items(0), GetType(ToolStripMenuItem))

            'Consortium.DropDownItems.Clear()

            'For Each Row As DataRow In ListData.ConsortiumDataView.ToTable(True, "Consortium").Rows
            '    Dim ConsortiumSubMenuItem As ToolStripMenuItem = New ToolStripMenuItem(Row.Item(0).ToString)

            '    AddHandler ConsortiumSubMenuItem.Click, AddressOf ConsortiumContextSubMenu_Click
            '    Consortium.DropDownItems.Add(ConsortiumSubMenuItem)
            'Next Row
            ' end of 78340 section
            ' end of 90025 section

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboPeriodType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboPeriodType.SelectedValueChanged
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboDisplaySet_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboDisplaySet.SelectedValueChanged
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ' This is needed to make mousewheel scroll list items
            sender.Select()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                GetSelections()
                RemoveSelectionHandlers()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                SetSelections()
                AddSelectionHandlers()
                'HighlightUnderAllocation() commented out TS 08/Sep/2016. Request ref 90026
            End If

            If e.Button = MouseButtons.Right Then
                'If sender.name <> "dgvClientName" And sender.name <> "dgvSchemeName" Then cmsList.Items(0).Visible = False Else cmsList.Items(0).Visible = True commented out TS 08/Sep/2016. Request ref 90025
                'If sender.name <> "dgvClientName" Then cmsList.Items(1).Visible = False Else cmsList.Items(1).Visible = True commented out TS 08/Sep/2016. Request ref 90025
                If sender.name <> "dgvPostcodeArea" Then cmsList.Items(0).Visible = False Else cmsList.Items(0).Visible = True ' added TS 09/Feb/2015. Request ref 38930. 2 changed to 0. Request ref 90025
                cmsList.Show(sender, e.Location)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvSummary.DataBindingComplete
        Try
            ' This needs to be handled differently. I'm not sure why but doing this later causes events to fire. Recommended practice on the following url
            ' http://msdn.microsoft.com/en-us/library/system.windows.forms.datagridviewcolumn.visible.aspx

            If dgvSummary.Columns.Contains("RowID") Then dgvSummary.Columns("RowID").Visible = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseDown
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
                dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected = True
            End If

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                ColSort = SummaryData.SummaryDataView.Sort
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseUp
        Try
            ' New section. Request ref 64648
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            RightClickColIdx = hti.ColumnIndex

            If RightClickColIdx <> -1 AndAlso DataColumnsList.Contains(dgvSummary.Columns(RightClickColIdx).Name) Then
                cmsSummary.Items(3).Visible = True
            Else
                cmsSummary.Items(3).Visible = False
            End If

            ' end of request ref 64648 section

            If e.Button = MouseButtons.Right Then cmsSummary.Show(dgvSummary, e.Location)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                If ColSort <> "" Then SummaryData.SummaryDataView.Sort += "," & ColSort
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSummary.SelectionChanged
        Try
            Dim TotalCases As Integer = 0

            If dgvSummary.SelectedCells.Count = 0 Or radAbs.Checked = False Then
                lblSummary.Text = ""
            Else
                For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                    If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then TotalCases += Cell.Value
                Next Cell
                lblSummary.Text = TotalCases.ToString & " case" & If(TotalCases > 1, "s", "")
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ShowDetail()
        Try
            Dim DetailParamList As String
            Dim Detail As New diaCaseDetail(Me)

            PreRefresh(Me)

            For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then
                    DetailParamList = cboCompany.SelectedValue.ToString & ","
                    DetailParamList &= GetParam("StageName", Cell, dgvStageName) & ","
                    DetailParamList &= GetParam("ClientName", Cell, dgvClientName) & ","
                    DetailParamList &= GetParam("SchemeName", Cell, dgvSchemeName) & ","
                    DetailParamList &= GetParam("InYear", Cell, dgvInYear) & ","
                    DetailParamList &= GetParam("PostcodeArea", Cell, dgvPostcodeArea) & ","
                    DetailParamList &= GetParam("DebtYear", Cell, dgvDebtYear) & ","
                    DetailParamList &= GetParam("LinkedPIF", Cell, dgvLinkedPIF) & ","
                    DetailParamList &= GetParam("LinkedTrace", Cell, dgvLinkedTrace) & ","
                    DetailParamList &= GetParam("IsCompany", Cell, dgvIsCompany) & ","
                    DetailParamList &= GetParam("PDLCheck", Cell, dgvPDLCheck) & ","
                    DetailParamList &= GetParam("PDLCheckBy", Cell, dgvPDLCheckBy) & ","

                    DetailParamList &= chkTopClients.Checked.ToString

                    DetailParamList &= "," & BalanceType()  ' added TS 12/Feb/2015. Request ref 40282

                    'DetailParamList &= "," & cboPeriodType.SelectedValue.ToString

                    Detail.AddCases(DetailParamList, dgvSummary.Columns(Cell.ColumnIndex).Name.Replace("Day", ""))

                End If
            Next Cell

            PostRefresh(Me)

            If Detail.CaseCount > 0 Then Detail.ShowDialog() ' Portal task ref 16716
            Detail.Dispose()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ShowMap()
        Try
            Dim DetailParamList As String

            PreRefresh(Me)

            If IsNothing(map) OrElse map.IsDisposed Then map = New frmMap(dgvSummary)

            For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then
                    DetailParamList = cboCompany.SelectedValue.ToString & ","
                    DetailParamList &= GetParam("StageName", Cell, dgvStageName) & ","
                    DetailParamList &= GetParam("ClientName", Cell, dgvClientName) & ","
                    DetailParamList &= GetParam("SchemeName", Cell, dgvSchemeName) & ","
                    DetailParamList &= GetParam("InYear", Cell, dgvInYear) & ","
                    DetailParamList &= GetParam("PostcodeArea", Cell, dgvPostcodeArea) & ","
                    DetailParamList &= GetParam("DebtYear", Cell, dgvDebtYear) & ","
                    DetailParamList &= GetParam("LinkedPIF", Cell, dgvLinkedPIF) & ","

                    DetailParamList &= chkTopClients.Checked.ToString

                    DetailParamList &= "," & BalanceType()  ' added TS 12/Feb/2015. Request ref 40282

                    'DetailParamList &= "," & cboPeriodType.SelectedValue.ToString

                    Map.AddCases(DetailParamList, dgvSummary.Columns(Cell.ColumnIndex).Name)

                End If
            Next Cell

            ' Get the cases added ready to plot - this is where the different datasets are named
            map.GetCasesForMap()

            PostRefresh(Me)

            Map.Show()
            '   Map = Nothing

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "'All' and 'Clear' button click events"

    Private Sub cmdStageAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageAll.Click
        Try
            dgvStageName.SelectAll()
            StageGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdStageClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageClear.Click
        Try
            dgvStageName.ClearSelection()
            StageGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientAll.Click
        Try
            dgvClientName.SelectAll()
            ClientGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientClear.Click
        Try
            dgvClientName.ClearSelection()
            ClientGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdSchemeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeAll.Click
        Try
            dgvSchemeName.SelectAll()
            SchemeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdSchemeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeClear.Click
        Try
            dgvSchemeName.ClearSelection()
            SchemeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdInYearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdInYearAll.Click
        Try
            dgvInYear.SelectAll()
            InYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdInYearClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdInYearClear.Click
        Try
            dgvInYear.ClearSelection()
            InYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeAreaAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaAll.Click
        Try
            dgvPostcodeArea.SelectAll()
            PostcodeAreaGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeAreaClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaClear.Click
        Try
            dgvPostcodeArea.ClearSelection()
            PostcodeAreaGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdDebtYearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDebtYearAll.Click
        Try
            dgvDebtYear.SelectAll()
            DebtYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdDebtYearClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDebtYearClear.Click
        Try
            dgvDebtYear.ClearSelection()
            DebtYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLinkedPIFAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLinkedPIFAll.Click
        Try
            dgvLinkedPIF.SelectAll()
            LinkedPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLinkedPIFClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLinkedPIFClear.Click
        Try
            dgvLinkedPIF.ClearSelection()
            LinkedPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLinkedTraceAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLinkedTraceAll.Click
        Try
            dgvLinkedTrace.SelectAll()
            LinkedTraceGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLinkedTraceClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLinkedTraceClear.Click
        Try
            dgvLinkedTrace.ClearSelection()
            LinkedTraceGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdIsCompanyAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIsCompanyAll.Click
        Try
            dgvIsCompany.SelectAll()
            IscompanyGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdIsCompanyClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdIsCompanyClear.Click
        Try
            dgvIsCompany.ClearSelection()
            IscompanyGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPDLCheckAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPDLCheckAll.Click
        Try
            dgvPDLCheck.SelectAll()
            PDLCheckGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPDLCheckClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPDLCheckClear.Click
        Try
            dgvPDLCheck.ClearSelection()
            PDLCheckGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPDLCheckByAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPDLCheckByAll.Click
        Try
            dgvPDLCheckBy.SelectAll()
            PDLCheckByGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPDLCheckByClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPDLCheckByClear.Click
        Try
            dgvPDLCheckBy.ClearSelection()
            PDLCheckByGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "Handlers"

    Private Sub RemoveSelectionHandlers()
        Try
            ' Need to find a way to do these at once but these SelectionChanged event is specific to the datagridview class and not control - the collection on the form
            RemoveHandler dgvStageName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvInYear.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvDebtYear.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvLinkedPIF.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvLinkedTrace.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvIsCompany.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPDLCheck.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPDLCheckBy.SelectionChanged, AddressOf dgvSelectionChanged

            RemoveHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            'RemoveHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
            'RemoveHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddSelectionHandlers()
        Try
            AddHandler dgvStageName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvInYear.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvDebtYear.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvLinkedPIF.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvLinkedTrace.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvIsCompany.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPDLCheck.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPDLCheckBy.SelectionChanged, AddressOf dgvSelectionChanged

            AddHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            'AddHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
            'AddHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddControlHandlers()
        Try
            ' All these grids and buttons call the same events so no point declaring them all separately
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView And Control.Name <> "dgvSummary" Then
                    AddHandler Control.MouseDown, AddressOf dgvMouseDown
                    AddHandler Control.MouseUp, AddressOf dgvMouseUp
                    AddHandler Control.MouseEnter, AddressOf dgvMouseEnter
                    'AddHandler Control.MouseHover, AddressOf dgvMouseHover
                ElseIf TypeOf Control Is ComboBox Or TypeOf Control Is Button Or TypeOf Control Is RadioButton Or TypeOf Control Is CheckBox Then
                    'AddHandler Control.MouseHover, AddressOf dgvMouseHover
                End If
            Next Control

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "DataGridView methods"

    Private Sub SetListGrids()
        Try
            dgvStageName.DataSource = ListData.StageName
            StageGridState.SetSort()

            dgvClientName.DataSource = ListData.ClientName
            ClientGridState.SetSort()

            dgvSchemeName.DataSource = ListData.SchemeName
            SchemeGridState.SetSort()

            dgvInYear.DataSource = ListData.InYear
            InYearGridState.SetSort()

            dgvPostcodeArea.DataSource = ListData.PostcodeArea
            PostcodeAreaGridState.SetSort()

            dgvDebtYear.DataSource = ListData.DebtYear
            DebtYearGridState.SetSort()

            dgvLinkedPIF.DataSource = ListData.LinkedPIF
            LinkedPIFGridState.SetSort()

            dgvLinkedTrace.DataSource = ListData.LinkedTrace
            LinkedTraceGridState.SetSort()

            dgvIsCompany.DataSource = ListData.IsCompany
            IscompanyGridState.SetSort()

            dgvPDLCheck.DataSource = ListData.PDLCheck
            PDLCheckGridState.SetSort()

            dgvPDLCheckBy.DataSource = ListData.PDLCheckBy
            PDLCheckByGridState.SetSort()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub FormatListColumns()
        Try
            ' This is slightly different to settings in modCommon as we want some of the fields wider than they need to be for presentation
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    If Control.Name <> "dgvSummary" Then ' This is not a list grid
                        For Each Column In CType(Control, DataGridView).Columns
                            Select Case Column.Name
                                Case "Total"
                                    Column.Width = 40
                                Case "StageName"
                                    Column.Width = 121
                                    Column.HeaderText = "Stage"
                                Case "ClientName"
                                    Column.Width = 100
                                    Column.HeaderText = "Client"
                                Case "LinkedPIF"
                                    Column.Width = 70
                                    Column.HeaderText = "Linked PIF"
                                Case "SchemeName"
                                    Column.Width = 100
                                    Column.HeaderText = "Scheme"
                                Case "Allocated"
                                    Column.Width = 70
                                Case "EnforcementFeesApplied"
                                    Column.Width = 70
                                    Column.HeaderText = "Enf Fees"
                                Case "InYear"
                                    Column.Width = 73
                                Case "PostcodeArea"
                                    Column.Width = 55
                                    Column.HeaderText = "Area"
                                Case "DebtYear"
                                    Column.Width = 55
                                    Column.HeaderText = "Year"
                                Case "LinkedTrace"
                                    Column.Width = 70
                                    Column.HeaderText = "Linked Trc"
                                Case "IsCompany"
                                    Column.Width = 70
                                    Column.HeaderText = "Is Compa"
                                Case "PDLCheck"
                                    Column.Width = 70
                                    Column.HeaderText = "PDL Check"
                                Case "PDLCheckBy"
                                    Column.Width = 70
                                    Column.HeaderText = "Name"
                                Case Else
                                    Column.Width = 40
                            End Select
                        Next Column
                    End If
                End If
            Next Control

            'SummaryData.GetUnderAllocatedClients() commented out TS 08/Sep/2016. Request ref 90026

            'HighlightUnderAllocation() commented out TS 08/Sep/2016. Request ref 90026

            SetStageColour() ' Added TS 24/Jul/2014 26706

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub HighlightWarnings()
        Try
            Dim RowIndex As Integer

            For Each DataRow As DataRow In SummaryData.WarningSummaryDataView.ToTable.Rows
                ' Find the row with warnings on the datagridview
                For Each GridRow As DataGridViewRow In dgvSummary.Rows
                    If GridRow.Cells("RowID").Value = DataRow.Item("RowID") Then RowIndex = GridRow.Index
                Next GridRow

                ' Loop through periods and highlight if any warnings exist
                For Each Item As DataColumn In DataRow.Table.Columns
                    If DataColumnsList.Contains(Item.ColumnName) AndAlso DataRow.Item(Item.ColumnName) > 0 Then
                        dgvSummary.Rows(RowIndex).Cells(Item.ColumnName).Style.ForeColor = Color.Red
                    End If
                Next Item
            Next DataRow

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetStageColour()
        Try
            For Each Row As DataGridViewRow In dgvStageName.Rows
                Select Case Row.Cells("StageName").Value.ToString.Substring(0, 3)
                    Case "050", "075"
                        Row.DefaultCellStyle.ForeColor = Color.Orange

                    Case "150", "200", "250", "300"
                        Row.DefaultCellStyle.ForeColor = Color.Purple

                    Case "350", "500", "550"
                        Row.DefaultCellStyle.ForeColor = Color.Green

                    Case "525"
                        Row.DefaultCellStyle.ForeColor = Color.DodgerBlue

                End Select

            Next Row

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub GetSelections()
        Try
            StageGridState.GetState()
            ClientGridState.GetState()
            SchemeGridState.GetState()
            InYearGridState.GetState()
            PostcodeAreaGridState.GetState()
            DebtYearGridState.GetState()
            LinkedPIFGridState.GetState()
            LinkedTraceGridState.GetState()
            IscompanyGridState.GetState()
            PDLCheckGridState.GetState()
            PDLCheckByGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetSelections()
        Try
            StageGridState.SetState()
            ClientGridState.SetState()
            SchemeGridState.SetState()
            InYearGridState.SetState()
            PostcodeAreaGridState.SetState()
            DebtYearGridState.SetState()
            LinkedPIFGridState.SetState()
            LinkedTraceGridState.SetState()
            IscompanyGridState.SetState()
            PDLCheckGridState.SetState()
            PDLCheckByGridState.SetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Additional functionality"

    Private Sub cmdClearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClearAll.Click
        Try
            RemoveSelectionHandlers()

            cmdStageClear_Click(sender, New System.EventArgs)
            cmdClientClear_Click(sender, New System.EventArgs)
            cmdSchemeClear_Click(sender, New System.EventArgs)
            cmdInYearClear_Click(sender, New System.EventArgs)
            cmdPostcodeAreaClear_Click(sender, New System.EventArgs)
            cmdDebtYearClear_Click(sender, New System.EventArgs)
            cmdLinkedPIFClear_Click(sender, New System.EventArgs)
            cmdLinkedTraceClear_Click(sender, New System.EventArgs)
            cmdIsCompanyClear_Click(sender, New System.EventArgs)
            cmdPDLCheckClear_Click(sender, New System.EventArgs)
            cmdPDLCheckByClear_Click(sender, New System.EventArgs)

            AddSelectionHandlers()

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radAbs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles radAbs.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radPerc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPerc.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub chkTopClients_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles chkTopClients.CheckedChanged
        Try
            RefreshScroll = False

            If chkTopClients.Checked Then
                ' Not ideal but we need to refresh the client list before the main refresh to exclude any clients selected that would not be in the top list
                RemoveSelectionHandlers()

                GetSelections()
                ListData.GetClientList(ParamList)
                SetSelections()

                AddSelectionHandlers()
            End If

            RefreshGrid()

            RefreshScroll = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.Click
        Try
            If MsgBox("This could take several minutes, are you sure?", vbYesNo + vbDefaultButton2 + vbQuestion) = vbYes Then
                'RefreshDBClicked = True
                PreRefresh(Me)
                SummaryData.RefreshDatabase()
                RefreshGrid()
                PostRefresh(Me)
                MsgBox("Refresh complete.", vbOKOnly + vbInformation)
                SetcmdRefreshTip()
                'RefreshDBClicked = False
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.MouseLeave
        Try
            ' on XP machines there is a bug that prevents the tooltip showing a second time. This addresses the problem.
            ToolTip1.Active = False
            ToolTip1.Active = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetcmdRefreshTip()
        Try
            SummaryData.GetLastLoad()
            ToolTip1.SetToolTip(cmdRefreshDB, "Last refreshed at " & SummaryData.LastLoadDataView.Item(0).Item(0).ToString & " by " & SummaryData.LastLoadDataView.Item(0).Item(1).ToString)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsSummary_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsSummary.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "View Cases"
                    ShowDetail()
                Case "Map Cases"
                    ShowMap()
                Case "Copy"
                    dgvSummary.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    If Not dgvSummary.GetClipboardContent Is Nothing Then Clipboard.SetDataObject(dgvSummary.GetClipboardContent())
                Case "Select All"
                    dgvSummary.SelectAll()
                Case "Select Column" ' Request ref 64648

                    RemoveHandler dgvSummary.SelectionChanged, AddressOf dgvSummary_SelectionChanged ' Remove the handler to avoid refreshing the lists on the first select and recalculating the total for every select

                    If Not My.Computer.Keyboard.CtrlKeyDown Then dgvSummary.ClearSelection()

                    For Each DataRow As DataGridViewRow In dgvSummary.Rows
                        If DataRow.Index = dgvSummary.Rows.Count - 1 Then ' Before selecting the last cell reinstatate the handler so that the total is recalculated
                            AddHandler dgvSummary.SelectionChanged, AddressOf dgvSummary_SelectionChanged
                        End If

                        DataRow.Cells(RightClickColIdx).Selected = True
                    Next DataRow

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsList_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsList.ItemClicked
        Try
            ' Slightly more complicated than the summary grid as this event fires for all other datagridviews.
            Select Case e.ClickedItem.Text
                ' commented out TS 08/Sep/2016. Request ref 90026. The code to call this is commented out so this will never fire but for completeness I've commented this too.
                'Case "Set warning levels"
                '    Dim dia As New diaClientAllocationLevels(SummaryData.ClientAllocationLevelDataView)
                '    dia.ShowDialog()
                '    SummaryData.GetClientAllocationLevels()
                '    If dia.DialogResult = DialogResult.OK Then FormatListColumns()
                '    dia.Dispose()
                ' end of 90026 section
                Case "Copy"
                    sender.SourceControl.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    If Not sender.SourceControl.GetClipboardContent() Is Nothing Then Clipboard.SetDataObject(sender.SourceControl.GetClipboardContent())

                Case "Select All"
                    sender.SourceControl.SelectAll()

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsForm_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsForm.ItemClicked
        Try
            ' The dispose is required to clear any forms that have called others. Dispose show changed from ShowDialog request ref 75670
            Select Case e.ClickedItem.Text
                Case "Bailiff View"
                    If FormOpen("frmBailiffSummary") Then
                        frmBailiffSummary.Activate()
                    Else
                        frmBailiffSummary.Dispose()
                        frmBailiffSummary.Show()
                    End If
                Case "Post Enforcement View"
                    If FormOpen("frmPostEnforcementSummary") Then
                        frmPostEnforcementSummary.Activate()
                    Else
                        frmPostEnforcementSummary.Dispose()
                        frmPostEnforcementSummary.Show()
                    End If
                Case "EA Update View"
                    If FormOpen("frmEAUpdateSummary") Then
                        frmEAUpdateSummary.Activate()
                    Else
                        frmEAUpdateSummary.Dispose()
                        frmEAUpdateSummary.Show()
                    End If
                Case "Regional View"
                    If FormOpen("frmAllocationByRegionSummary") Then
                        frmAllocationByRegionSummary.Activate()
                    Else
                        frmAllocationByRegionSummary.Dispose()
                        frmAllocationByRegionSummary.Show()
                    End If
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    ' commented out TS 08/Sep/2016. Request ref 90026. The code to call this is commented out so this will never fire but for completeness I've commented this too.
    'Private Sub ConsortiumContextSubMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim ScrollbarSet As Boolean = False

    '        RemoveSelectionHandlers()

    '        dgvClientName.ClearSelection()
    '        dgvSchemeName.ClearSelection()

    '        ListData.ConsortiumDataView.RowFilter = "Consortium = '" & CType(sender, ToolStripItem).Text & "'"

    '        For Each ConsortiumDataRow As DataRow In ListData.ConsortiumDataView.ToTable.Rows
    '            For Each GridDataRow As DataGridViewRow In dgvClientName.Rows
    '                If GridDataRow.Cells("ClientName").Value = ConsortiumDataRow.Item("ClientName") Then
    '                    GridDataRow.Selected = True
    '                    If Not ScrollbarSet Then
    '                        dgvClientName.FirstDisplayedScrollingRowIndex = GridDataRow.Index
    '                        ScrollbarSet = True
    '                    End If
    '                End If
    '            Next GridDataRow

    '            ScrollbarSet = False

    '            For Each GridDataRow As DataGridViewRow In dgvSchemeName.Rows
    '                If GridDataRow.Cells("SchemeName").Value = ConsortiumDataRow.Item("SchemeName") Then
    '                    GridDataRow.Selected = True
    '                    If Not ScrollbarSet Then
    '                        dgvSchemeName.FirstDisplayedScrollingRowIndex = GridDataRow.Index
    '                        ScrollbarSet = True
    '                    End If
    '                End If
    '            Next GridDataRow
    '        Next ConsortiumDataRow

    '        AddSelectionHandlers()
    '        RefreshGrid()

    '    Catch ex As Exception
    '        HandleException(ex)
    '    End Try
    'End Sub

    Private Sub RegionContextSubMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' added TS 09/Feb/2015. Request ref 38930
        Try
            Dim ScrollbarSet As Boolean = False

            RemoveSelectionHandlers()

            ListData.PostcodeAreaRegionDataView.RowFilter = "RegionDesc = '" & CType(sender, ToolStripItem).Text & "'"

            For Each PostcodeAreaRegionDataRow As DataRow In ListData.PostcodeAreaRegionDataView.ToTable.Rows
                For Each GridDataRow As DataGridViewRow In dgvPostcodeArea.Rows
                    If GridDataRow.Cells("PostcodeArea").Value = PostcodeAreaRegionDataRow.Item("PostcodeArea") Then
                        GridDataRow.Selected = True
                        If Not ScrollbarSet Then
                            dgvPostcodeArea.FirstDisplayedScrollingRowIndex = GridDataRow.Index
                            ScrollbarSet = True
                        End If
                    End If
                Next GridDataRow
            Next PostcodeAreaRegionDataRow

            AddSelectionHandlers()
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radLow_Click(sender As Object, e As System.EventArgs) Handles radLow.Click
        Try
            If radLow.Checked Then
                radLow.Checked = False
            Else
                radLow.Checked = True
                radHigh.Checked = False
            End If

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radHigh_Click(sender As Object, e As System.EventArgs) Handles radHigh.Click
        Try
            If radHigh.Checked Then
                radHigh.Checked = False
            Else
                radHigh.Checked = True
                radLow.Checked = False
            End If

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function BalanceType() As String
        BalanceType = "A"

        Try
            If radLow.Checked Then
                BalanceType = "L"
            ElseIf radHigh.Checked Then
                BalanceType = "H"
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function GetStages() As String()
        GetStages = Nothing

        Try
            Dim SelectedSorts As New List(Of String)


            For Each Item As DataGridViewRow In dgvStageName.SelectedRows
                SelectedSorts.Add(Item.Cells("StageName").Value.ToString)
            Next Item

            If Not IsNothing(SelectedSorts) Then GetStages = SelectedSorts.ToArray

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function
#End Region



    'Private Sub dgvMouseHover(sender As Object, e As System.EventArgs)

    '    If Not RefreshDBClicked Then Return

    '    If Not ListData.IsRefreshRunning Then Return

    '    If Not RefreshRunningMsgShown Then

    '        Me.Enabled = False

    '        MsgBox("Refresh running.", vbOKOnly + vbInformation)
    '        RefreshRunningMsgShown = True
    '        RefreshCheckTimer.Enabled = True
    '    End If
    'End Sub

    'Private Sub RefreshCheckTimer_Tick(sender As Object, e As System.EventArgs) Handles RefreshCheckTimer.Tick

    '    If ListData.IsRefreshRunning Then Return

    '    MsgBox("Refresh complete.", vbOKOnly + vbInformation)
    '    RefreshRunningMsgShown = False
    '    RefreshCheckTimer.Enabled = False
    '    Me.Enabled = True
    'End Sub


End Class


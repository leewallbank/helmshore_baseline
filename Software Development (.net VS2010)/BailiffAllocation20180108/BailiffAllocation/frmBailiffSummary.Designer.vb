﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBailiffSummary
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBailiffSummary))
        Me.cboDisplaySet = New System.Windows.Forms.ComboBox()
        Me.radPerc = New System.Windows.Forms.RadioButton()
        Me.cmdBailiffTypeClear = New System.Windows.Forms.Button()
        Me.cmdRefreshDB = New System.Windows.Forms.Button()
        Me.dgvBailiffType = New System.Windows.Forms.DataGridView()
        Me.BailiffType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdBailiffTypeAll = New System.Windows.Forms.Button()
        Me.radAbs = New System.Windows.Forms.RadioButton()
        Me.pnlAbsPerc = New System.Windows.Forms.Panel()
        Me.lblLine1 = New System.Windows.Forms.Label()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmdClearAll = New System.Windows.Forms.Button()
        Me.cboPeriodType = New System.Windows.Forms.ComboBox()
        Me.dgvPostcodeArea = New System.Windows.Forms.DataGridView()
        Me.PostcodeArea = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmsList = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.lblPeriodType = New System.Windows.Forms.Label()
        Me.cmdPostcodeAreaAll = New System.Windows.Forms.Button()
        Me.cmdPostcodeAreaClear = New System.Windows.Forms.Button()
        Me.cmsSummary = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmdBailiffNameClear = New System.Windows.Forms.Button()
        Me.cmdBailiffNameAll = New System.Windows.Forms.Button()
        Me.dgvBailiffName = New System.Windows.Forms.DataGridView()
        Me.BailiffName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colClientNameTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvSummary = New System.Windows.Forms.DataGridView()
        Me.cmdStatusNameClear = New System.Windows.Forms.Button()
        Me.dgvStatusName = New System.Windows.Forms.DataGridView()
        Me.StatusName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdStatusNameAll = New System.Windows.Forms.Button()
        Me.cmsForm = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.lblSummary = New System.Windows.Forms.Label()
        Me.cmdEnforcementFeesAppliedClear = New System.Windows.Forms.Button()
        Me.dgvEnforcementFeesApplied = New System.Windows.Forms.DataGridView()
        Me.EnforcementFeesApplied = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdEnforcementFeesAppliedAll = New System.Windows.Forms.Button()
        Me.cmdCGAClear = New System.Windows.Forms.Button()
        Me.dgvCGA = New System.Windows.Forms.DataGridView()
        Me.CGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdCGAAll = New System.Windows.Forms.Button()
        Me.cboEAGroup = New System.Windows.Forms.ComboBox()
        Me.cmdWorkTypeClear = New System.Windows.Forms.Button()
        Me.dgvWorkType = New System.Windows.Forms.DataGridView()
        Me.WorkType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdWorkTypeAll = New System.Windows.Forms.Button()
        Me.cmdPaymentClear = New System.Windows.Forms.Button()
        Me.cmdPaymentAll = New System.Windows.Forms.Button()
        Me.cmdVisitedClear = New System.Windows.Forms.Button()
        Me.dgvAddConfirmed = New System.Windows.Forms.DataGridView()
        Me.AddConfirmed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdSchemeNameClear = New System.Windows.Forms.Button()
        Me.cmdVisitedAll = New System.Windows.Forms.Button()
        Me.cmdAddConfirmedClear = New System.Windows.Forms.Button()
        Me.cmdAddConfirmedAll = New System.Windows.Forms.Button()
        Me.dgvSchemeName = New System.Windows.Forms.DataGridView()
        Me.SchemeName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvPayment = New System.Windows.Forms.DataGridView()
        Me.Payment = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdClientNameClear = New System.Windows.Forms.Button()
        Me.cmdSchemeNameAll = New System.Windows.Forms.Button()
        Me.cmdClientNameAll = New System.Windows.Forms.Button()
        Me.dgvVisited = New System.Windows.Forms.DataGridView()
        Me.Visited = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvClientName = New System.Windows.Forms.DataGridView()
        Me.ClientName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvAllVisitsPAWithinTwentyDays = New System.Windows.Forms.DataGridView()
        Me.AllVisitsPAWithinTwentyDays = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdAllVisitsPAWithinTwentyDaysClear = New System.Windows.Forms.Button()
        Me.cmdAllVisitsPAWithinTwentyDaysAll = New System.Windows.Forms.Button()
        Me.dgvFirstTwoVisitsPAWithinTwoDays = New System.Windows.Forms.DataGridView()
        Me.FirstTwoVisitsPAWithinTwoDays = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdFirstTwoVisitsPAWithinTwoDaysClear = New System.Windows.Forms.Button()
        Me.cmdFirstTwoVisitsPAWithinTwoDaysAll = New System.Windows.Forms.Button()
        Me.dgvNumberOfVisitsPA = New System.Windows.Forms.DataGridView()
        Me.NumberOfVisitsPA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn14 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdNumberOfVisitsPAClear = New System.Windows.Forms.Button()
        Me.cmdNumberOfVisitsPAAll = New System.Windows.Forms.Button()
        Me.cboCompany = New System.Windows.Forms.ComboBox()
        Me.dgvStageName = New System.Windows.Forms.DataGridView()
        Me.StageName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn15 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdStageNameClear = New System.Windows.Forms.Button()
        Me.cmdStageNameAll = New System.Windows.Forms.Button()
        Me.pnlBalance = New System.Windows.Forms.Panel()
        Me.radHigh = New System.Windows.Forms.RadioButton()
        Me.radLow = New System.Windows.Forms.RadioButton()
        CType(Me.dgvBailiffType, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAbsPerc.SuspendLayout()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBailiffName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvStatusName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEnforcementFeesApplied, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCGA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvWorkType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAddConfirmed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSchemeName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvVisited, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAllVisitsPAWithinTwentyDays, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvFirstTwoVisitsPAWithinTwoDays, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvNumberOfVisitsPA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvStageName, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBalance.SuspendLayout()
        Me.SuspendLayout()
        '
        'cboDisplaySet
        '
        Me.cboDisplaySet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisplaySet.FormattingEnabled = True
        Me.cboDisplaySet.Items.AddRange(New Object() {"Months", "Periods"})
        Me.cboDisplaySet.Location = New System.Drawing.Point(698, 348)
        Me.cboDisplaySet.Name = "cboDisplaySet"
        Me.cboDisplaySet.Size = New System.Drawing.Size(72, 21)
        Me.cboDisplaySet.TabIndex = 86
        '
        'radPerc
        '
        Me.radPerc.AutoSize = True
        Me.radPerc.Location = New System.Drawing.Point(55, 4)
        Me.radPerc.Name = "radPerc"
        Me.radPerc.Size = New System.Drawing.Size(33, 17)
        Me.radPerc.TabIndex = 4
        Me.radPerc.TabStop = True
        Me.radPerc.Text = "%"
        Me.radPerc.UseVisualStyleBackColor = True
        '
        'cmdBailiffTypeClear
        '
        Me.cmdBailiffTypeClear.Location = New System.Drawing.Point(410, 316)
        Me.cmdBailiffTypeClear.Name = "cmdBailiffTypeClear"
        Me.cmdBailiffTypeClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdBailiffTypeClear.TabIndex = 75
        Me.cmdBailiffTypeClear.Text = "Clear"
        Me.cmdBailiffTypeClear.UseVisualStyleBackColor = True
        '
        'cmdRefreshDB
        '
        Me.cmdRefreshDB.Location = New System.Drawing.Point(7, 350)
        Me.cmdRefreshDB.Name = "cmdRefreshDB"
        Me.cmdRefreshDB.Size = New System.Drawing.Size(75, 20)
        Me.cmdRefreshDB.TabIndex = 76
        Me.cmdRefreshDB.Text = "Refresh DB"
        Me.cmdRefreshDB.UseVisualStyleBackColor = True
        '
        'dgvBailiffType
        '
        Me.dgvBailiffType.AllowUserToAddRows = False
        Me.dgvBailiffType.AllowUserToDeleteRows = False
        Me.dgvBailiffType.AllowUserToResizeColumns = False
        Me.dgvBailiffType.AllowUserToResizeRows = False
        Me.dgvBailiffType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBailiffType.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BailiffType, Me.DataGridViewTextBoxColumn5})
        Me.dgvBailiffType.Location = New System.Drawing.Point(341, 251)
        Me.dgvBailiffType.Name = "dgvBailiffType"
        Me.dgvBailiffType.ReadOnly = True
        Me.dgvBailiffType.RowHeadersVisible = False
        Me.dgvBailiffType.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvBailiffType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBailiffType.Size = New System.Drawing.Size(126, 65)
        Me.dgvBailiffType.TabIndex = 73
        '
        'BailiffType
        '
        Me.BailiffType.DataPropertyName = "BailiffType"
        Me.BailiffType.HeaderText = "Type"
        Me.BailiffType.Name = "BailiffType"
        Me.BailiffType.ReadOnly = True
        Me.BailiffType.Width = 84
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 40
        '
        'cmdBailiffTypeAll
        '
        Me.cmdBailiffTypeAll.Location = New System.Drawing.Point(348, 316)
        Me.cmdBailiffTypeAll.Name = "cmdBailiffTypeAll"
        Me.cmdBailiffTypeAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdBailiffTypeAll.TabIndex = 74
        Me.cmdBailiffTypeAll.Text = "All"
        Me.cmdBailiffTypeAll.UseVisualStyleBackColor = True
        '
        'radAbs
        '
        Me.radAbs.AutoSize = True
        Me.radAbs.Location = New System.Drawing.Point(6, 4)
        Me.radAbs.Name = "radAbs"
        Me.radAbs.Size = New System.Drawing.Size(43, 17)
        Me.radAbs.TabIndex = 3
        Me.radAbs.TabStop = True
        Me.radAbs.Text = "Abs"
        Me.radAbs.UseVisualStyleBackColor = True
        '
        'pnlAbsPerc
        '
        Me.pnlAbsPerc.Controls.Add(Me.radPerc)
        Me.pnlAbsPerc.Controls.Add(Me.radAbs)
        Me.pnlAbsPerc.Location = New System.Drawing.Point(958, 348)
        Me.pnlAbsPerc.Name = "pnlAbsPerc"
        Me.pnlAbsPerc.Size = New System.Drawing.Size(99, 25)
        Me.pnlAbsPerc.TabIndex = 71
        '
        'lblLine1
        '
        Me.lblLine1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLine1.Location = New System.Drawing.Point(7, 341)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(1128, 1)
        Me.lblLine1.TabIndex = 70
        '
        'cmdClearAll
        '
        Me.cmdClearAll.Location = New System.Drawing.Point(1065, 349)
        Me.cmdClearAll.Name = "cmdClearAll"
        Me.cmdClearAll.Size = New System.Drawing.Size(68, 20)
        Me.cmdClearAll.TabIndex = 69
        Me.cmdClearAll.Text = "Clear All"
        Me.cmdClearAll.UseVisualStyleBackColor = True
        '
        'cboPeriodType
        '
        Me.cboPeriodType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriodType.FormattingEnabled = True
        Me.cboPeriodType.Location = New System.Drawing.Point(836, 348)
        Me.cboPeriodType.Name = "cboPeriodType"
        Me.cboPeriodType.Size = New System.Drawing.Size(110, 21)
        Me.cboPeriodType.TabIndex = 84
        '
        'dgvPostcodeArea
        '
        Me.dgvPostcodeArea.AllowUserToAddRows = False
        Me.dgvPostcodeArea.AllowUserToDeleteRows = False
        Me.dgvPostcodeArea.AllowUserToResizeColumns = False
        Me.dgvPostcodeArea.AllowUserToResizeRows = False
        Me.dgvPostcodeArea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPostcodeArea.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PostcodeArea, Me.DataGridViewTextBoxColumn9})
        Me.dgvPostcodeArea.Location = New System.Drawing.Point(759, 7)
        Me.dgvPostcodeArea.Name = "dgvPostcodeArea"
        Me.dgvPostcodeArea.ReadOnly = True
        Me.dgvPostcodeArea.RowHeadersVisible = False
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        Me.dgvPostcodeArea.RowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPostcodeArea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPostcodeArea.Size = New System.Drawing.Size(115, 309)
        Me.dgvPostcodeArea.TabIndex = 81
        '
        'PostcodeArea
        '
        Me.PostcodeArea.DataPropertyName = "PostcodeArea"
        Me.PostcodeArea.HeaderText = "Area"
        Me.PostcodeArea.Name = "PostcodeArea"
        Me.PostcodeArea.ReadOnly = True
        Me.PostcodeArea.Width = 55
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 40
        '
        'cmsList
        '
        Me.cmsList.Name = "ContextMenuStrip1"
        Me.cmsList.Size = New System.Drawing.Size(61, 4)
        '
        'lblPeriodType
        '
        Me.lblPeriodType.Location = New System.Drawing.Point(711, 353)
        Me.lblPeriodType.Name = "lblPeriodType"
        Me.lblPeriodType.Size = New System.Drawing.Size(119, 16)
        Me.lblPeriodType.TabIndex = 85
        Me.lblPeriodType.Text = "based on:"
        Me.lblPeriodType.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cmdPostcodeAreaAll
        '
        Me.cmdPostcodeAreaAll.Location = New System.Drawing.Point(761, 316)
        Me.cmdPostcodeAreaAll.Name = "cmdPostcodeAreaAll"
        Me.cmdPostcodeAreaAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaAll.TabIndex = 82
        Me.cmdPostcodeAreaAll.Text = "All"
        Me.cmdPostcodeAreaAll.UseVisualStyleBackColor = True
        '
        'cmdPostcodeAreaClear
        '
        Me.cmdPostcodeAreaClear.Location = New System.Drawing.Point(823, 316)
        Me.cmdPostcodeAreaClear.Name = "cmdPostcodeAreaClear"
        Me.cmdPostcodeAreaClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaClear.TabIndex = 83
        Me.cmdPostcodeAreaClear.Text = "Clear"
        Me.cmdPostcodeAreaClear.UseVisualStyleBackColor = True
        '
        'cmsSummary
        '
        Me.cmsSummary.Name = "ContextMenuStrip1"
        Me.cmsSummary.Size = New System.Drawing.Size(61, 4)
        '
        'cmdBailiffNameClear
        '
        Me.cmdBailiffNameClear.Location = New System.Drawing.Point(93, 316)
        Me.cmdBailiffNameClear.Name = "cmdBailiffNameClear"
        Me.cmdBailiffNameClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdBailiffNameClear.TabIndex = 57
        Me.cmdBailiffNameClear.Text = "Clear"
        Me.cmdBailiffNameClear.UseVisualStyleBackColor = True
        '
        'cmdBailiffNameAll
        '
        Me.cmdBailiffNameAll.Location = New System.Drawing.Point(31, 316)
        Me.cmdBailiffNameAll.Name = "cmdBailiffNameAll"
        Me.cmdBailiffNameAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdBailiffNameAll.TabIndex = 56
        Me.cmdBailiffNameAll.Text = "All"
        Me.cmdBailiffNameAll.UseVisualStyleBackColor = True
        '
        'dgvBailiffName
        '
        Me.dgvBailiffName.AllowUserToAddRows = False
        Me.dgvBailiffName.AllowUserToDeleteRows = False
        Me.dgvBailiffName.AllowUserToResizeColumns = False
        Me.dgvBailiffName.AllowUserToResizeRows = False
        Me.dgvBailiffName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBailiffName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BailiffName, Me.colClientNameTotal})
        Me.dgvBailiffName.Location = New System.Drawing.Point(7, 29)
        Me.dgvBailiffName.Name = "dgvBailiffName"
        Me.dgvBailiffName.ReadOnly = True
        Me.dgvBailiffName.RowHeadersVisible = False
        Me.dgvBailiffName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBailiffName.Size = New System.Drawing.Size(160, 265)
        Me.dgvBailiffName.TabIndex = 46
        '
        'BailiffName
        '
        Me.BailiffName.DataPropertyName = "BailiffName"
        Me.BailiffName.HeaderText = "Name"
        Me.BailiffName.Name = "BailiffName"
        Me.BailiffName.ReadOnly = True
        '
        'colClientNameTotal
        '
        Me.colClientNameTotal.DataPropertyName = "Total"
        Me.colClientNameTotal.HeaderText = "Total"
        Me.colClientNameTotal.Name = "colClientNameTotal"
        Me.colClientNameTotal.ReadOnly = True
        Me.colClientNameTotal.Width = 40
        '
        'dgvSummary
        '
        Me.dgvSummary.AllowUserToAddRows = False
        Me.dgvSummary.AllowUserToDeleteRows = False
        Me.dgvSummary.AllowUserToOrderColumns = True
        Me.dgvSummary.AllowUserToResizeRows = False
        Me.dgvSummary.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSummary.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvSummary.Location = New System.Drawing.Point(7, 379)
        Me.dgvSummary.Name = "dgvSummary"
        Me.dgvSummary.ReadOnly = True
        Me.dgvSummary.RowHeadersVisible = False
        Me.dgvSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvSummary.Size = New System.Drawing.Size(1128, 205)
        Me.dgvSummary.TabIndex = 44
        '
        'cmdStatusNameClear
        '
        Me.cmdStatusNameClear.Location = New System.Drawing.Point(953, 191)
        Me.cmdStatusNameClear.Name = "cmdStatusNameClear"
        Me.cmdStatusNameClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdStatusNameClear.TabIndex = 89
        Me.cmdStatusNameClear.Text = "Clear"
        Me.cmdStatusNameClear.UseVisualStyleBackColor = True
        '
        'dgvStatusName
        '
        Me.dgvStatusName.AllowUserToAddRows = False
        Me.dgvStatusName.AllowUserToDeleteRows = False
        Me.dgvStatusName.AllowUserToResizeColumns = False
        Me.dgvStatusName.AllowUserToResizeRows = False
        Me.dgvStatusName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStatusName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StatusName, Me.DataGridViewTextBoxColumn2})
        Me.dgvStatusName.Location = New System.Drawing.Point(881, 7)
        Me.dgvStatusName.Name = "dgvStatusName"
        Me.dgvStatusName.ReadOnly = True
        Me.dgvStatusName.RowHeadersVisible = False
        Me.dgvStatusName.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvStatusName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvStatusName.Size = New System.Drawing.Size(132, 184)
        Me.dgvStatusName.TabIndex = 87
        '
        'StatusName
        '
        Me.StatusName.DataPropertyName = "StatusName"
        Me.StatusName.HeaderText = "Status"
        Me.StatusName.Name = "StatusName"
        Me.StatusName.ReadOnly = True
        Me.StatusName.Width = 90
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 40
        '
        'cmdStatusNameAll
        '
        Me.cmdStatusNameAll.Location = New System.Drawing.Point(891, 191)
        Me.cmdStatusNameAll.Name = "cmdStatusNameAll"
        Me.cmdStatusNameAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdStatusNameAll.TabIndex = 88
        Me.cmdStatusNameAll.Text = "All"
        Me.cmdStatusNameAll.UseVisualStyleBackColor = True
        '
        'cmsForm
        '
        Me.cmsForm.Name = "ContextMenuStrip1"
        Me.cmsForm.Size = New System.Drawing.Size(61, 4)
        '
        'lblSummary
        '
        Me.lblSummary.AutoSize = True
        Me.lblSummary.Location = New System.Drawing.Point(88, 354)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(39, 13)
        Me.lblSummary.TabIndex = 90
        Me.lblSummary.Text = "Label1"
        '
        'cmdEnforcementFeesAppliedClear
        '
        Me.cmdEnforcementFeesAppliedClear.Location = New System.Drawing.Point(560, 316)
        Me.cmdEnforcementFeesAppliedClear.Name = "cmdEnforcementFeesAppliedClear"
        Me.cmdEnforcementFeesAppliedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdEnforcementFeesAppliedClear.TabIndex = 96
        Me.cmdEnforcementFeesAppliedClear.Text = "Clear"
        Me.cmdEnforcementFeesAppliedClear.UseVisualStyleBackColor = True
        '
        'dgvEnforcementFeesApplied
        '
        Me.dgvEnforcementFeesApplied.AllowUserToAddRows = False
        Me.dgvEnforcementFeesApplied.AllowUserToDeleteRows = False
        Me.dgvEnforcementFeesApplied.AllowUserToResizeColumns = False
        Me.dgvEnforcementFeesApplied.AllowUserToResizeRows = False
        Me.dgvEnforcementFeesApplied.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEnforcementFeesApplied.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EnforcementFeesApplied, Me.DataGridViewTextBoxColumn3})
        Me.dgvEnforcementFeesApplied.Location = New System.Drawing.Point(474, 229)
        Me.dgvEnforcementFeesApplied.Name = "dgvEnforcementFeesApplied"
        Me.dgvEnforcementFeesApplied.ReadOnly = True
        Me.dgvEnforcementFeesApplied.RowHeadersVisible = False
        Me.dgvEnforcementFeesApplied.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvEnforcementFeesApplied.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEnforcementFeesApplied.Size = New System.Drawing.Size(160, 87)
        Me.dgvEnforcementFeesApplied.TabIndex = 94
        '
        'EnforcementFeesApplied
        '
        Me.EnforcementFeesApplied.DataPropertyName = "EnforcementFeesApplied"
        Me.EnforcementFeesApplied.HeaderText = "Enforcement Fees"
        Me.EnforcementFeesApplied.Name = "EnforcementFeesApplied"
        Me.EnforcementFeesApplied.ReadOnly = True
        Me.EnforcementFeesApplied.Width = 120
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 40
        '
        'cmdEnforcementFeesAppliedAll
        '
        Me.cmdEnforcementFeesAppliedAll.Location = New System.Drawing.Point(498, 316)
        Me.cmdEnforcementFeesAppliedAll.Name = "cmdEnforcementFeesAppliedAll"
        Me.cmdEnforcementFeesAppliedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdEnforcementFeesAppliedAll.TabIndex = 95
        Me.cmdEnforcementFeesAppliedAll.Text = "All"
        Me.cmdEnforcementFeesAppliedAll.UseVisualStyleBackColor = True
        '
        'cmdCGAClear
        '
        Me.cmdCGAClear.Location = New System.Drawing.Point(1080, 228)
        Me.cmdCGAClear.Name = "cmdCGAClear"
        Me.cmdCGAClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdCGAClear.TabIndex = 93
        Me.cmdCGAClear.Text = "Clear"
        Me.cmdCGAClear.UseVisualStyleBackColor = True
        '
        'dgvCGA
        '
        Me.dgvCGA.AllowUserToAddRows = False
        Me.dgvCGA.AllowUserToDeleteRows = False
        Me.dgvCGA.AllowUserToResizeColumns = False
        Me.dgvCGA.AllowUserToResizeRows = False
        Me.dgvCGA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCGA.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CGA, Me.DataGridViewTextBoxColumn6})
        Me.dgvCGA.Location = New System.Drawing.Point(1018, 163)
        Me.dgvCGA.Name = "dgvCGA"
        Me.dgvCGA.ReadOnly = True
        Me.dgvCGA.RowHeadersVisible = False
        Me.dgvCGA.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvCGA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCGA.Size = New System.Drawing.Size(115, 65)
        Me.dgvCGA.TabIndex = 91
        '
        'CGA
        '
        Me.CGA.DataPropertyName = "CGA"
        Me.CGA.HeaderText = "CGA"
        Me.CGA.Name = "CGA"
        Me.CGA.ReadOnly = True
        Me.CGA.Width = 74
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 40
        '
        'cmdCGAAll
        '
        Me.cmdCGAAll.Location = New System.Drawing.Point(1020, 228)
        Me.cmdCGAAll.Name = "cmdCGAAll"
        Me.cmdCGAAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdCGAAll.TabIndex = 92
        Me.cmdCGAAll.Text = "All"
        Me.cmdCGAAll.UseVisualStyleBackColor = True
        '
        'cboEAGroup
        '
        Me.cboEAGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEAGroup.FormattingEnabled = True
        Me.cboEAGroup.Location = New System.Drawing.Point(7, 295)
        Me.cboEAGroup.Name = "cboEAGroup"
        Me.cboEAGroup.Size = New System.Drawing.Size(160, 21)
        Me.cboEAGroup.TabIndex = 97
        '
        'cmdWorkTypeClear
        '
        Me.cmdWorkTypeClear.Location = New System.Drawing.Point(409, 118)
        Me.cmdWorkTypeClear.Name = "cmdWorkTypeClear"
        Me.cmdWorkTypeClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdWorkTypeClear.TabIndex = 115
        Me.cmdWorkTypeClear.Text = "Clear"
        Me.cmdWorkTypeClear.UseVisualStyleBackColor = True
        '
        'dgvWorkType
        '
        Me.dgvWorkType.AllowUserToAddRows = False
        Me.dgvWorkType.AllowUserToDeleteRows = False
        Me.dgvWorkType.AllowUserToResizeColumns = False
        Me.dgvWorkType.AllowUserToResizeRows = False
        Me.dgvWorkType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvWorkType.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WorkType, Me.DataGridViewTextBoxColumn1})
        Me.dgvWorkType.Location = New System.Drawing.Point(341, 7)
        Me.dgvWorkType.Name = "dgvWorkType"
        Me.dgvWorkType.ReadOnly = True
        Me.dgvWorkType.RowHeadersVisible = False
        DataGridViewCellStyle3.BackColor = System.Drawing.Color.White
        Me.dgvWorkType.RowsDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvWorkType.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvWorkType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvWorkType.Size = New System.Drawing.Size(126, 111)
        Me.dgvWorkType.TabIndex = 113
        '
        'WorkType
        '
        Me.WorkType.DataPropertyName = "WorkType"
        Me.WorkType.HeaderText = "Work Type"
        Me.WorkType.Name = "WorkType"
        Me.WorkType.ReadOnly = True
        Me.WorkType.Width = 84
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 40
        '
        'cmdWorkTypeAll
        '
        Me.cmdWorkTypeAll.Location = New System.Drawing.Point(347, 118)
        Me.cmdWorkTypeAll.Name = "cmdWorkTypeAll"
        Me.cmdWorkTypeAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdWorkTypeAll.TabIndex = 114
        Me.cmdWorkTypeAll.Text = "All"
        Me.cmdWorkTypeAll.UseVisualStyleBackColor = True
        '
        'cmdPaymentClear
        '
        Me.cmdPaymentClear.Location = New System.Drawing.Point(702, 72)
        Me.cmdPaymentClear.Name = "cmdPaymentClear"
        Me.cmdPaymentClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPaymentClear.TabIndex = 109
        Me.cmdPaymentClear.Text = "Clear"
        Me.cmdPaymentClear.UseVisualStyleBackColor = True
        '
        'cmdPaymentAll
        '
        Me.cmdPaymentAll.Location = New System.Drawing.Point(640, 72)
        Me.cmdPaymentAll.Name = "cmdPaymentAll"
        Me.cmdPaymentAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPaymentAll.TabIndex = 108
        Me.cmdPaymentAll.Text = "All"
        Me.cmdPaymentAll.UseVisualStyleBackColor = True
        '
        'cmdVisitedClear
        '
        Me.cmdVisitedClear.Location = New System.Drawing.Point(702, 228)
        Me.cmdVisitedClear.Name = "cmdVisitedClear"
        Me.cmdVisitedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdVisitedClear.TabIndex = 107
        Me.cmdVisitedClear.Text = "Clear"
        Me.cmdVisitedClear.UseVisualStyleBackColor = True
        '
        'dgvAddConfirmed
        '
        Me.dgvAddConfirmed.AllowUserToAddRows = False
        Me.dgvAddConfirmed.AllowUserToDeleteRows = False
        Me.dgvAddConfirmed.AllowUserToResizeColumns = False
        Me.dgvAddConfirmed.AllowUserToResizeRows = False
        Me.dgvAddConfirmed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAddConfirmed.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AddConfirmed, Me.DataGridViewTextBoxColumn4})
        Me.dgvAddConfirmed.Location = New System.Drawing.Point(640, 251)
        Me.dgvAddConfirmed.Name = "dgvAddConfirmed"
        Me.dgvAddConfirmed.ReadOnly = True
        Me.dgvAddConfirmed.RowHeadersVisible = False
        Me.dgvAddConfirmed.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvAddConfirmed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAddConfirmed.Size = New System.Drawing.Size(113, 65)
        Me.dgvAddConfirmed.TabIndex = 110
        '
        'AddConfirmed
        '
        Me.AddConfirmed.DataPropertyName = "AddConfirmed"
        Me.AddConfirmed.HeaderText = "Confirmed"
        Me.AddConfirmed.Name = "AddConfirmed"
        Me.AddConfirmed.ReadOnly = True
        Me.AddConfirmed.Width = 60
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 40
        '
        'cmdSchemeNameClear
        '
        Me.cmdSchemeNameClear.Location = New System.Drawing.Point(560, 203)
        Me.cmdSchemeNameClear.Name = "cmdSchemeNameClear"
        Me.cmdSchemeNameClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdSchemeNameClear.TabIndex = 105
        Me.cmdSchemeNameClear.Text = "Clear"
        Me.cmdSchemeNameClear.UseVisualStyleBackColor = True
        '
        'cmdVisitedAll
        '
        Me.cmdVisitedAll.Location = New System.Drawing.Point(640, 228)
        Me.cmdVisitedAll.Name = "cmdVisitedAll"
        Me.cmdVisitedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdVisitedAll.TabIndex = 106
        Me.cmdVisitedAll.Text = "All"
        Me.cmdVisitedAll.UseVisualStyleBackColor = True
        '
        'cmdAddConfirmedClear
        '
        Me.cmdAddConfirmedClear.Location = New System.Drawing.Point(702, 316)
        Me.cmdAddConfirmedClear.Name = "cmdAddConfirmedClear"
        Me.cmdAddConfirmedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdAddConfirmedClear.TabIndex = 112
        Me.cmdAddConfirmedClear.Text = "Clear"
        Me.cmdAddConfirmedClear.UseVisualStyleBackColor = True
        '
        'cmdAddConfirmedAll
        '
        Me.cmdAddConfirmedAll.Location = New System.Drawing.Point(640, 316)
        Me.cmdAddConfirmedAll.Name = "cmdAddConfirmedAll"
        Me.cmdAddConfirmedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdAddConfirmedAll.TabIndex = 111
        Me.cmdAddConfirmedAll.Text = "All"
        Me.cmdAddConfirmedAll.UseVisualStyleBackColor = True
        '
        'dgvSchemeName
        '
        Me.dgvSchemeName.AllowUserToAddRows = False
        Me.dgvSchemeName.AllowUserToDeleteRows = False
        Me.dgvSchemeName.AllowUserToResizeColumns = False
        Me.dgvSchemeName.AllowUserToResizeRows = False
        Me.dgvSchemeName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSchemeName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SchemeName, Me.Total})
        Me.dgvSchemeName.Location = New System.Drawing.Point(474, 7)
        Me.dgvSchemeName.Name = "dgvSchemeName"
        Me.dgvSchemeName.ReadOnly = True
        Me.dgvSchemeName.RowHeadersVisible = False
        Me.dgvSchemeName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSchemeName.Size = New System.Drawing.Size(160, 196)
        Me.dgvSchemeName.TabIndex = 101
        '
        'SchemeName
        '
        Me.SchemeName.DataPropertyName = "SchemeName"
        Me.SchemeName.HeaderText = "Scheme"
        Me.SchemeName.Name = "SchemeName"
        Me.SchemeName.ReadOnly = True
        '
        'Total
        '
        Me.Total.DataPropertyName = "Total"
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        Me.Total.Width = 40
        '
        'dgvPayment
        '
        Me.dgvPayment.AllowUserToAddRows = False
        Me.dgvPayment.AllowUserToDeleteRows = False
        Me.dgvPayment.AllowUserToResizeColumns = False
        Me.dgvPayment.AllowUserToResizeRows = False
        Me.dgvPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPayment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Payment, Me.DataGridViewTextBoxColumn7})
        Me.dgvPayment.Location = New System.Drawing.Point(640, 7)
        Me.dgvPayment.Name = "dgvPayment"
        Me.dgvPayment.ReadOnly = True
        Me.dgvPayment.RowHeadersVisible = False
        Me.dgvPayment.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvPayment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPayment.Size = New System.Drawing.Size(112, 65)
        Me.dgvPayment.TabIndex = 99
        '
        'Payment
        '
        Me.Payment.DataPropertyName = "Payment"
        Me.Payment.HeaderText = "Payment"
        Me.Payment.Name = "Payment"
        Me.Payment.ReadOnly = True
        Me.Payment.Width = 60
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 40
        '
        'cmdClientNameClear
        '
        Me.cmdClientNameClear.Location = New System.Drawing.Point(259, 316)
        Me.cmdClientNameClear.Name = "cmdClientNameClear"
        Me.cmdClientNameClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientNameClear.TabIndex = 103
        Me.cmdClientNameClear.Text = "Clear"
        Me.cmdClientNameClear.UseVisualStyleBackColor = True
        '
        'cmdSchemeNameAll
        '
        Me.cmdSchemeNameAll.Location = New System.Drawing.Point(498, 203)
        Me.cmdSchemeNameAll.Name = "cmdSchemeNameAll"
        Me.cmdSchemeNameAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdSchemeNameAll.TabIndex = 104
        Me.cmdSchemeNameAll.Text = "All"
        Me.cmdSchemeNameAll.UseVisualStyleBackColor = True
        '
        'cmdClientNameAll
        '
        Me.cmdClientNameAll.Location = New System.Drawing.Point(197, 316)
        Me.cmdClientNameAll.Name = "cmdClientNameAll"
        Me.cmdClientNameAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientNameAll.TabIndex = 102
        Me.cmdClientNameAll.Text = "All"
        Me.cmdClientNameAll.UseVisualStyleBackColor = True
        '
        'dgvVisited
        '
        Me.dgvVisited.AllowUserToAddRows = False
        Me.dgvVisited.AllowUserToDeleteRows = False
        Me.dgvVisited.AllowUserToResizeColumns = False
        Me.dgvVisited.AllowUserToResizeRows = False
        Me.dgvVisited.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvVisited.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Visited, Me.DataGridViewTextBoxColumn8})
        Me.dgvVisited.Location = New System.Drawing.Point(640, 163)
        Me.dgvVisited.Name = "dgvVisited"
        Me.dgvVisited.ReadOnly = True
        Me.dgvVisited.RowHeadersVisible = False
        Me.dgvVisited.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvVisited.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvVisited.Size = New System.Drawing.Size(112, 65)
        Me.dgvVisited.TabIndex = 100
        '
        'Visited
        '
        Me.Visited.DataPropertyName = "Visited"
        Me.Visited.HeaderText = "Visited"
        Me.Visited.Name = "Visited"
        Me.Visited.ReadOnly = True
        Me.Visited.Width = 60
        '
        'DataGridViewTextBoxColumn8
        '
        Me.DataGridViewTextBoxColumn8.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn8.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn8.Name = "DataGridViewTextBoxColumn8"
        Me.DataGridViewTextBoxColumn8.ReadOnly = True
        Me.DataGridViewTextBoxColumn8.Width = 40
        '
        'dgvClientName
        '
        Me.dgvClientName.AllowUserToAddRows = False
        Me.dgvClientName.AllowUserToDeleteRows = False
        Me.dgvClientName.AllowUserToResizeColumns = False
        Me.dgvClientName.AllowUserToResizeRows = False
        Me.dgvClientName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClientName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClientName, Me.DataGridViewTextBoxColumn10})
        Me.dgvClientName.Location = New System.Drawing.Point(174, 7)
        Me.dgvClientName.Name = "dgvClientName"
        Me.dgvClientName.ReadOnly = True
        Me.dgvClientName.RowHeadersVisible = False
        Me.dgvClientName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClientName.Size = New System.Drawing.Size(160, 309)
        Me.dgvClientName.TabIndex = 98
        '
        'ClientName
        '
        Me.ClientName.DataPropertyName = "ClientName"
        Me.ClientName.HeaderText = "Client"
        Me.ClientName.Name = "ClientName"
        Me.ClientName.ReadOnly = True
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 40
        '
        'dgvAllVisitsPAWithinTwentyDays
        '
        Me.dgvAllVisitsPAWithinTwentyDays.AllowUserToAddRows = False
        Me.dgvAllVisitsPAWithinTwentyDays.AllowUserToDeleteRows = False
        Me.dgvAllVisitsPAWithinTwentyDays.AllowUserToResizeColumns = False
        Me.dgvAllVisitsPAWithinTwentyDays.AllowUserToResizeRows = False
        Me.dgvAllVisitsPAWithinTwentyDays.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAllVisitsPAWithinTwentyDays.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AllVisitsPAWithinTwentyDays, Me.DataGridViewTextBoxColumn12})
        Me.dgvAllVisitsPAWithinTwentyDays.Location = New System.Drawing.Point(1018, 251)
        Me.dgvAllVisitsPAWithinTwentyDays.Name = "dgvAllVisitsPAWithinTwentyDays"
        Me.dgvAllVisitsPAWithinTwentyDays.ReadOnly = True
        Me.dgvAllVisitsPAWithinTwentyDays.RowHeadersVisible = False
        Me.dgvAllVisitsPAWithinTwentyDays.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvAllVisitsPAWithinTwentyDays.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAllVisitsPAWithinTwentyDays.Size = New System.Drawing.Size(115, 65)
        Me.dgvAllVisitsPAWithinTwentyDays.TabIndex = 116
        '
        'AllVisitsPAWithinTwentyDays
        '
        Me.AllVisitsPAWithinTwentyDays.DataPropertyName = "AllVisitsPAWithinTwentyDays"
        Me.AllVisitsPAWithinTwentyDays.HeaderText = "Visits < 20"
        Me.AllVisitsPAWithinTwentyDays.Name = "AllVisitsPAWithinTwentyDays"
        Me.AllVisitsPAWithinTwentyDays.ReadOnly = True
        Me.AllVisitsPAWithinTwentyDays.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.AllVisitsPAWithinTwentyDays.ToolTipText = "Are all visits since allocation with twenty days?"
        Me.AllVisitsPAWithinTwentyDays.Width = 74
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 40
        '
        'cmdAllVisitsPAWithinTwentyDaysClear
        '
        Me.cmdAllVisitsPAWithinTwentyDaysClear.Location = New System.Drawing.Point(1082, 316)
        Me.cmdAllVisitsPAWithinTwentyDaysClear.Name = "cmdAllVisitsPAWithinTwentyDaysClear"
        Me.cmdAllVisitsPAWithinTwentyDaysClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdAllVisitsPAWithinTwentyDaysClear.TabIndex = 118
        Me.cmdAllVisitsPAWithinTwentyDaysClear.Text = "Clear"
        Me.cmdAllVisitsPAWithinTwentyDaysClear.UseVisualStyleBackColor = True
        '
        'cmdAllVisitsPAWithinTwentyDaysAll
        '
        Me.cmdAllVisitsPAWithinTwentyDaysAll.Location = New System.Drawing.Point(1020, 316)
        Me.cmdAllVisitsPAWithinTwentyDaysAll.Name = "cmdAllVisitsPAWithinTwentyDaysAll"
        Me.cmdAllVisitsPAWithinTwentyDaysAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdAllVisitsPAWithinTwentyDaysAll.TabIndex = 117
        Me.cmdAllVisitsPAWithinTwentyDaysAll.Text = "All"
        Me.cmdAllVisitsPAWithinTwentyDaysAll.UseVisualStyleBackColor = True
        '
        'dgvFirstTwoVisitsPAWithinTwoDays
        '
        Me.dgvFirstTwoVisitsPAWithinTwoDays.AllowUserToAddRows = False
        Me.dgvFirstTwoVisitsPAWithinTwoDays.AllowUserToDeleteRows = False
        Me.dgvFirstTwoVisitsPAWithinTwoDays.AllowUserToResizeColumns = False
        Me.dgvFirstTwoVisitsPAWithinTwoDays.AllowUserToResizeRows = False
        Me.dgvFirstTwoVisitsPAWithinTwoDays.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFirstTwoVisitsPAWithinTwoDays.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.FirstTwoVisitsPAWithinTwoDays, Me.DataGridViewTextBoxColumn13})
        Me.dgvFirstTwoVisitsPAWithinTwoDays.Location = New System.Drawing.Point(881, 229)
        Me.dgvFirstTwoVisitsPAWithinTwoDays.Name = "dgvFirstTwoVisitsPAWithinTwoDays"
        Me.dgvFirstTwoVisitsPAWithinTwoDays.ReadOnly = True
        Me.dgvFirstTwoVisitsPAWithinTwoDays.RowHeadersVisible = False
        Me.dgvFirstTwoVisitsPAWithinTwoDays.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvFirstTwoVisitsPAWithinTwoDays.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFirstTwoVisitsPAWithinTwoDays.Size = New System.Drawing.Size(132, 87)
        Me.dgvFirstTwoVisitsPAWithinTwoDays.TabIndex = 119
        '
        'FirstTwoVisitsPAWithinTwoDays
        '
        Me.FirstTwoVisitsPAWithinTwoDays.DataPropertyName = "FirstTwoVisitsPAWithinTwoDays"
        Me.FirstTwoVisitsPAWithinTwoDays.HeaderText = "V2 < 2"
        Me.FirstTwoVisitsPAWithinTwoDays.Name = "FirstTwoVisitsPAWithinTwoDays"
        Me.FirstTwoVisitsPAWithinTwoDays.ReadOnly = True
        Me.FirstTwoVisitsPAWithinTwoDays.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.FirstTwoVisitsPAWithinTwoDays.ToolTipText = "Is the second visit within two days of the first since allocation?"
        Me.FirstTwoVisitsPAWithinTwoDays.Width = 74
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Width = 40
        '
        'cmdFirstTwoVisitsPAWithinTwoDaysClear
        '
        Me.cmdFirstTwoVisitsPAWithinTwoDaysClear.Location = New System.Drawing.Point(953, 316)
        Me.cmdFirstTwoVisitsPAWithinTwoDaysClear.Name = "cmdFirstTwoVisitsPAWithinTwoDaysClear"
        Me.cmdFirstTwoVisitsPAWithinTwoDaysClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdFirstTwoVisitsPAWithinTwoDaysClear.TabIndex = 121
        Me.cmdFirstTwoVisitsPAWithinTwoDaysClear.Text = "Clear"
        Me.cmdFirstTwoVisitsPAWithinTwoDaysClear.UseVisualStyleBackColor = True
        '
        'cmdFirstTwoVisitsPAWithinTwoDaysAll
        '
        Me.cmdFirstTwoVisitsPAWithinTwoDaysAll.Location = New System.Drawing.Point(891, 316)
        Me.cmdFirstTwoVisitsPAWithinTwoDaysAll.Name = "cmdFirstTwoVisitsPAWithinTwoDaysAll"
        Me.cmdFirstTwoVisitsPAWithinTwoDaysAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdFirstTwoVisitsPAWithinTwoDaysAll.TabIndex = 120
        Me.cmdFirstTwoVisitsPAWithinTwoDaysAll.Text = "All"
        Me.cmdFirstTwoVisitsPAWithinTwoDaysAll.UseVisualStyleBackColor = True
        '
        'dgvNumberOfVisitsPA
        '
        Me.dgvNumberOfVisitsPA.AllowUserToAddRows = False
        Me.dgvNumberOfVisitsPA.AllowUserToDeleteRows = False
        Me.dgvNumberOfVisitsPA.AllowUserToResizeColumns = False
        Me.dgvNumberOfVisitsPA.AllowUserToResizeRows = False
        Me.dgvNumberOfVisitsPA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvNumberOfVisitsPA.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumberOfVisitsPA, Me.DataGridViewTextBoxColumn14})
        Me.dgvNumberOfVisitsPA.Location = New System.Drawing.Point(1019, 7)
        Me.dgvNumberOfVisitsPA.Name = "dgvNumberOfVisitsPA"
        Me.dgvNumberOfVisitsPA.ReadOnly = True
        Me.dgvNumberOfVisitsPA.RowHeadersVisible = False
        Me.dgvNumberOfVisitsPA.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvNumberOfVisitsPA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvNumberOfVisitsPA.Size = New System.Drawing.Size(115, 133)
        Me.dgvNumberOfVisitsPA.TabIndex = 122
        '
        'NumberOfVisitsPA
        '
        Me.NumberOfVisitsPA.DataPropertyName = "NumberOfVisitsPA"
        Me.NumberOfVisitsPA.HeaderText = "# Visits"
        Me.NumberOfVisitsPA.Name = "NumberOfVisitsPA"
        Me.NumberOfVisitsPA.ReadOnly = True
        Me.NumberOfVisitsPA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.NumberOfVisitsPA.Width = 74
        '
        'DataGridViewTextBoxColumn14
        '
        Me.DataGridViewTextBoxColumn14.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn14.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn14.Name = "DataGridViewTextBoxColumn14"
        Me.DataGridViewTextBoxColumn14.ReadOnly = True
        Me.DataGridViewTextBoxColumn14.Width = 40
        '
        'cmdNumberOfVisitsPAClear
        '
        Me.cmdNumberOfVisitsPAClear.Location = New System.Drawing.Point(1080, 140)
        Me.cmdNumberOfVisitsPAClear.Name = "cmdNumberOfVisitsPAClear"
        Me.cmdNumberOfVisitsPAClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdNumberOfVisitsPAClear.TabIndex = 124
        Me.cmdNumberOfVisitsPAClear.Text = "Clear"
        Me.cmdNumberOfVisitsPAClear.UseVisualStyleBackColor = True
        '
        'cmdNumberOfVisitsPAAll
        '
        Me.cmdNumberOfVisitsPAAll.Location = New System.Drawing.Point(1020, 140)
        Me.cmdNumberOfVisitsPAAll.Name = "cmdNumberOfVisitsPAAll"
        Me.cmdNumberOfVisitsPAAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdNumberOfVisitsPAAll.TabIndex = 123
        Me.cmdNumberOfVisitsPAAll.Text = "All"
        Me.cmdNumberOfVisitsPAAll.UseVisualStyleBackColor = True
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(7, 7)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(160, 21)
        Me.cboCompany.TabIndex = 125
        '
        'dgvStageName
        '
        Me.dgvStageName.AllowUserToAddRows = False
        Me.dgvStageName.AllowUserToDeleteRows = False
        Me.dgvStageName.AllowUserToResizeColumns = False
        Me.dgvStageName.AllowUserToResizeRows = False
        Me.dgvStageName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStageName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StageName, Me.DataGridViewTextBoxColumn15})
        Me.dgvStageName.Location = New System.Drawing.Point(341, 141)
        Me.dgvStageName.Name = "dgvStageName"
        Me.dgvStageName.ReadOnly = True
        Me.dgvStageName.RowHeadersVisible = False
        Me.dgvStageName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvStageName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvStageName.Size = New System.Drawing.Size(126, 87)
        Me.dgvStageName.TabIndex = 126
        '
        'StageName
        '
        Me.StageName.DataPropertyName = "StageName"
        Me.StageName.HeaderText = "Stage"
        Me.StageName.Name = "StageName"
        Me.StageName.ReadOnly = True
        Me.StageName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.StageName.Width = 74
        '
        'DataGridViewTextBoxColumn15
        '
        Me.DataGridViewTextBoxColumn15.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn15.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn15.Name = "DataGridViewTextBoxColumn15"
        Me.DataGridViewTextBoxColumn15.ReadOnly = True
        Me.DataGridViewTextBoxColumn15.Width = 40
        '
        'cmdStageNameClear
        '
        Me.cmdStageNameClear.Location = New System.Drawing.Point(409, 228)
        Me.cmdStageNameClear.Name = "cmdStageNameClear"
        Me.cmdStageNameClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageNameClear.TabIndex = 128
        Me.cmdStageNameClear.Text = "Clear"
        Me.cmdStageNameClear.UseVisualStyleBackColor = True
        '
        'cmdStageNameAll
        '
        Me.cmdStageNameAll.Location = New System.Drawing.Point(347, 228)
        Me.cmdStageNameAll.Name = "cmdStageNameAll"
        Me.cmdStageNameAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageNameAll.TabIndex = 127
        Me.cmdStageNameAll.Text = "All"
        Me.cmdStageNameAll.UseVisualStyleBackColor = True
        '
        'pnlBalance
        '
        Me.pnlBalance.Controls.Add(Me.radHigh)
        Me.pnlBalance.Controls.Add(Me.radLow)
        Me.pnlBalance.Location = New System.Drawing.Point(314, 348)
        Me.pnlBalance.Name = "pnlBalance"
        Me.pnlBalance.Size = New System.Drawing.Size(197, 25)
        Me.pnlBalance.TabIndex = 129
        '
        'radHigh
        '
        Me.radHigh.AutoCheck = False
        Me.radHigh.AutoSize = True
        Me.radHigh.Location = New System.Drawing.Point(106, 4)
        Me.radHigh.Name = "radHigh"
        Me.radHigh.Size = New System.Drawing.Size(73, 17)
        Me.radHigh.TabIndex = 1
        Me.radHigh.TabStop = True
        Me.radHigh.Text = "> £10,000"
        Me.radHigh.UseVisualStyleBackColor = True
        '
        'radLow
        '
        Me.radLow.AutoCheck = False
        Me.radLow.AutoSize = True
        Me.radLow.Location = New System.Drawing.Point(6, 4)
        Me.radLow.Name = "radLow"
        Me.radLow.Size = New System.Drawing.Size(86, 17)
        Me.radLow.TabIndex = 0
        Me.radLow.TabStop = True
        Me.radLow.Text = "< £50 no link"
        Me.radLow.UseVisualStyleBackColor = True
        '
        'frmBailiffSummary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1141, 591)
        Me.Controls.Add(Me.pnlBalance)
        Me.Controls.Add(Me.dgvStageName)
        Me.Controls.Add(Me.cmdStageNameClear)
        Me.Controls.Add(Me.cmdStageNameAll)
        Me.Controls.Add(Me.dgvPostcodeArea)
        Me.Controls.Add(Me.dgvWorkType)
        Me.Controls.Add(Me.cboCompany)
        Me.Controls.Add(Me.dgvNumberOfVisitsPA)
        Me.Controls.Add(Me.cmdNumberOfVisitsPAClear)
        Me.Controls.Add(Me.cmdNumberOfVisitsPAAll)
        Me.Controls.Add(Me.dgvAllVisitsPAWithinTwentyDays)
        Me.Controls.Add(Me.dgvFirstTwoVisitsPAWithinTwoDays)
        Me.Controls.Add(Me.cmdAllVisitsPAWithinTwentyDaysClear)
        Me.Controls.Add(Me.cmdAllVisitsPAWithinTwentyDaysAll)
        Me.Controls.Add(Me.cmdWorkTypeClear)
        Me.Controls.Add(Me.cmdFirstTwoVisitsPAWithinTwoDaysClear)
        Me.Controls.Add(Me.cmdFirstTwoVisitsPAWithinTwoDaysAll)
        Me.Controls.Add(Me.cmdWorkTypeAll)
        Me.Controls.Add(Me.cmdPaymentClear)
        Me.Controls.Add(Me.cmdPaymentAll)
        Me.Controls.Add(Me.cmdVisitedClear)
        Me.Controls.Add(Me.dgvAddConfirmed)
        Me.Controls.Add(Me.cmdSchemeNameClear)
        Me.Controls.Add(Me.cmdVisitedAll)
        Me.Controls.Add(Me.cmdAddConfirmedClear)
        Me.Controls.Add(Me.cmdAddConfirmedAll)
        Me.Controls.Add(Me.dgvSchemeName)
        Me.Controls.Add(Me.dgvPayment)
        Me.Controls.Add(Me.cmdClientNameClear)
        Me.Controls.Add(Me.cmdSchemeNameAll)
        Me.Controls.Add(Me.cmdClientNameAll)
        Me.Controls.Add(Me.dgvVisited)
        Me.Controls.Add(Me.dgvClientName)
        Me.Controls.Add(Me.cboEAGroup)
        Me.Controls.Add(Me.cmdEnforcementFeesAppliedClear)
        Me.Controls.Add(Me.dgvEnforcementFeesApplied)
        Me.Controls.Add(Me.cmdEnforcementFeesAppliedAll)
        Me.Controls.Add(Me.lblSummary)
        Me.Controls.Add(Me.cmdStatusNameClear)
        Me.Controls.Add(Me.dgvStatusName)
        Me.Controls.Add(Me.cmdStatusNameAll)
        Me.Controls.Add(Me.cboDisplaySet)
        Me.Controls.Add(Me.dgvCGA)
        Me.Controls.Add(Me.cmdRefreshDB)
        Me.Controls.Add(Me.pnlAbsPerc)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdCGAClear)
        Me.Controls.Add(Me.cmdClearAll)
        Me.Controls.Add(Me.cmdCGAAll)
        Me.Controls.Add(Me.cboPeriodType)
        Me.Controls.Add(Me.dgvBailiffType)
        Me.Controls.Add(Me.cmdBailiffTypeClear)
        Me.Controls.Add(Me.lblPeriodType)
        Me.Controls.Add(Me.cmdBailiffTypeAll)
        Me.Controls.Add(Me.cmdBailiffNameClear)
        Me.Controls.Add(Me.cmdBailiffNameAll)
        Me.Controls.Add(Me.dgvBailiffName)
        Me.Controls.Add(Me.cmdPostcodeAreaAll)
        Me.Controls.Add(Me.cmdPostcodeAreaClear)
        Me.Controls.Add(Me.dgvSummary)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmBailiffSummary"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Bailiff Summary"
        CType(Me.dgvBailiffType, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAbsPerc.ResumeLayout(False)
        Me.pnlAbsPerc.PerformLayout()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBailiffName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvStatusName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEnforcementFeesApplied, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCGA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvWorkType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAddConfirmed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSchemeName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPayment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvVisited, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAllVisitsPAWithinTwentyDays, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvFirstTwoVisitsPAWithinTwoDays, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvNumberOfVisitsPA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvStageName, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBalance.ResumeLayout(False)
        Me.pnlBalance.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboDisplaySet As System.Windows.Forms.ComboBox
    Friend WithEvents radPerc As System.Windows.Forms.RadioButton
    Friend WithEvents cmdBailiffTypeClear As System.Windows.Forms.Button
    Friend WithEvents cmdRefreshDB As System.Windows.Forms.Button
    Friend WithEvents dgvBailiffType As System.Windows.Forms.DataGridView
    Friend WithEvents cmdBailiffTypeAll As System.Windows.Forms.Button
    Friend WithEvents radAbs As System.Windows.Forms.RadioButton
    Friend WithEvents pnlAbsPerc As System.Windows.Forms.Panel
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cmdClearAll As System.Windows.Forms.Button
    Friend WithEvents cboPeriodType As System.Windows.Forms.ComboBox
    Friend WithEvents dgvPostcodeArea As System.Windows.Forms.DataGridView
    Friend WithEvents PostcodeArea As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmsList As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents lblPeriodType As System.Windows.Forms.Label
    Friend WithEvents cmdPostcodeAreaAll As System.Windows.Forms.Button
    Friend WithEvents cmdPostcodeAreaClear As System.Windows.Forms.Button
    Friend WithEvents cmsSummary As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmdBailiffNameClear As System.Windows.Forms.Button
    Friend WithEvents cmdBailiffNameAll As System.Windows.Forms.Button
    Friend WithEvents dgvBailiffName As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSummary As System.Windows.Forms.DataGridView
    Friend WithEvents BailiffName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClientNameTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdStatusNameClear As System.Windows.Forms.Button
    Friend WithEvents dgvStatusName As System.Windows.Forms.DataGridView
    Friend WithEvents cmdStatusNameAll As System.Windows.Forms.Button
    Friend WithEvents cmsForm As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents cmdEnforcementFeesAppliedClear As System.Windows.Forms.Button
    Friend WithEvents dgvEnforcementFeesApplied As System.Windows.Forms.DataGridView
    Friend WithEvents cmdEnforcementFeesAppliedAll As System.Windows.Forms.Button
    Friend WithEvents cmdCGAClear As System.Windows.Forms.Button
    Friend WithEvents dgvCGA As System.Windows.Forms.DataGridView
    Friend WithEvents cmdCGAAll As System.Windows.Forms.Button
    Friend WithEvents cboEAGroup As System.Windows.Forms.ComboBox
    Friend WithEvents StatusName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BailiffType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdWorkTypeClear As System.Windows.Forms.Button
    Friend WithEvents dgvWorkType As System.Windows.Forms.DataGridView
    Friend WithEvents WorkType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdWorkTypeAll As System.Windows.Forms.Button
    Friend WithEvents cmdPaymentClear As System.Windows.Forms.Button
    Friend WithEvents cmdPaymentAll As System.Windows.Forms.Button
    Friend WithEvents cmdVisitedClear As System.Windows.Forms.Button
    Friend WithEvents dgvAddConfirmed As System.Windows.Forms.DataGridView
    Friend WithEvents AddConfirmed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdSchemeNameClear As System.Windows.Forms.Button
    Friend WithEvents cmdVisitedAll As System.Windows.Forms.Button
    Friend WithEvents cmdAddConfirmedClear As System.Windows.Forms.Button
    Friend WithEvents cmdAddConfirmedAll As System.Windows.Forms.Button
    Friend WithEvents dgvSchemeName As System.Windows.Forms.DataGridView
    Friend WithEvents SchemeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvPayment As System.Windows.Forms.DataGridView
    Friend WithEvents Payment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdClientNameClear As System.Windows.Forms.Button
    Friend WithEvents cmdSchemeNameAll As System.Windows.Forms.Button
    Friend WithEvents cmdClientNameAll As System.Windows.Forms.Button
    Friend WithEvents dgvVisited As System.Windows.Forms.DataGridView
    Friend WithEvents Visited As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn8 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvClientName As System.Windows.Forms.DataGridView
    Friend WithEvents ClientName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvAllVisitsPAWithinTwentyDays As System.Windows.Forms.DataGridView
    Friend WithEvents cmdAllVisitsPAWithinTwentyDaysClear As System.Windows.Forms.Button
    Friend WithEvents cmdAllVisitsPAWithinTwentyDaysAll As System.Windows.Forms.Button
    Friend WithEvents dgvFirstTwoVisitsPAWithinTwoDays As System.Windows.Forms.DataGridView
    Friend WithEvents cmdFirstTwoVisitsPAWithinTwoDaysClear As System.Windows.Forms.Button
    Friend WithEvents cmdFirstTwoVisitsPAWithinTwoDaysAll As System.Windows.Forms.Button
    Friend WithEvents dgvNumberOfVisitsPA As System.Windows.Forms.DataGridView
    Friend WithEvents cmdNumberOfVisitsPAClear As System.Windows.Forms.Button
    Friend WithEvents cmdNumberOfVisitsPAAll As System.Windows.Forms.Button
    Friend WithEvents AllVisitsPAWithinTwentyDays As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FirstTwoVisitsPAWithinTwoDays As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EnforcementFeesApplied As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents NumberOfVisitsPA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn14 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvStageName As System.Windows.Forms.DataGridView
    Friend WithEvents cmdStageNameClear As System.Windows.Forms.Button
    Friend WithEvents cmdStageNameAll As System.Windows.Forms.Button
    Friend WithEvents StageName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn15 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents pnlBalance As System.Windows.Forms.Panel
    Friend WithEvents radHigh As System.Windows.Forms.RadioButton
    Friend WithEvents radLow As System.Windows.Forms.RadioButton
End Class

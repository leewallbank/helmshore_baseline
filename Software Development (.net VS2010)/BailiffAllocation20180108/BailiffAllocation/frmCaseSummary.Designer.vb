﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCaseSummary
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCaseSummary))
        Me.dgvSummary = New System.Windows.Forms.DataGridView()
        Me.dgvStageName = New System.Windows.Forms.DataGridView()
        Me.StageName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colStageNameTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvClientName = New System.Windows.Forms.DataGridView()
        Me.ClientName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colClientNameTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvSchemeName = New System.Windows.Forms.DataGridView()
        Me.SchemeName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdStageAll = New System.Windows.Forms.Button()
        Me.cmdStageClear = New System.Windows.Forms.Button()
        Me.cmdClientClear = New System.Windows.Forms.Button()
        Me.cmdClientAll = New System.Windows.Forms.Button()
        Me.cmdSchemeClear = New System.Windows.Forms.Button()
        Me.cmdSchemeAll = New System.Windows.Forms.Button()
        Me.cmdClearAll = New System.Windows.Forms.Button()
        Me.lblLine1 = New System.Windows.Forms.Label()
        Me.pnlAbsPerc = New System.Windows.Forms.Panel()
        Me.radPerc = New System.Windows.Forms.RadioButton()
        Me.radAbs = New System.Windows.Forms.RadioButton()
        Me.chkTopClients = New System.Windows.Forms.CheckBox()
        Me.cmdRefreshDB = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmsSummary = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmdInYearClear = New System.Windows.Forms.Button()
        Me.cmdInYearAll = New System.Windows.Forms.Button()
        Me.dgvInYear = New System.Windows.Forms.DataGridView()
        Me.InYear = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bxtnCoveredAll = New System.Windows.Forms.Button()
        Me.dgvPostcodeArea = New System.Windows.Forms.DataGridView()
        Me.PostcodeArea = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdPostcodeAreaClear = New System.Windows.Forms.Button()
        Me.cmdPostcodeAreaAll = New System.Windows.Forms.Button()
        Me.cmsList = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmsForm = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.lblSummary = New System.Windows.Forms.Label()
        Me.dgvDebtYear = New System.Windows.Forms.DataGridView()
        Me.DebtYear = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdDebtYearClear = New System.Windows.Forms.Button()
        Me.cmdDebtYearAll = New System.Windows.Forms.Button()
        Me.dgvLinkedPIF = New System.Windows.Forms.DataGridView()
        Me.LinkedPIF = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdLinkedPIFClear = New System.Windows.Forms.Button()
        Me.cmdLinkedPIFAll = New System.Windows.Forms.Button()
        Me.RefreshCheckTimer = New System.Windows.Forms.Timer(Me.components)
        Me.cboCompany = New System.Windows.Forms.ComboBox()
        Me.pnlBalance = New System.Windows.Forms.Panel()
        Me.radHigh = New System.Windows.Forms.RadioButton()
        Me.radLow = New System.Windows.Forms.RadioButton()
        Me.dgvLinkedTrace = New System.Windows.Forms.DataGridView()
        Me.LinkedTrace = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdLinkedTraceClear = New System.Windows.Forms.Button()
        Me.cmdLinkedTraceAll = New System.Windows.Forms.Button()
        Me.dgvIsCompany = New System.Windows.Forms.DataGridView()
        Me.IsCompany = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdIsCompanyClear = New System.Windows.Forms.Button()
        Me.cmdIsCompanyAll = New System.Windows.Forms.Button()
        Me.dgvPDLCheck = New System.Windows.Forms.DataGridView()
        Me.PDLCheck = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdPDLCheckClear = New System.Windows.Forms.Button()
        Me.cmdPDLCheckAll = New System.Windows.Forms.Button()
        Me.dgvPDLCheckBy = New System.Windows.Forms.DataGridView()
        Me.cmdPDLCheckByClear = New System.Windows.Forms.Button()
        Me.cmdPDLCheckByAll = New System.Windows.Forms.Button()
        Me.PDLCheckBy = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvStageName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSchemeName, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlAbsPerc.SuspendLayout()
        CType(Me.dgvInYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDebtYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLinkedPIF, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBalance.SuspendLayout()
        CType(Me.dgvLinkedTrace, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvIsCompany, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPDLCheck, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPDLCheckBy, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvSummary
        '
        Me.dgvSummary.AllowUserToAddRows = False
        Me.dgvSummary.AllowUserToDeleteRows = False
        Me.dgvSummary.AllowUserToOrderColumns = True
        Me.dgvSummary.AllowUserToResizeRows = False
        Me.dgvSummary.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSummary.DefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSummary.Location = New System.Drawing.Point(7, 367)
        Me.dgvSummary.Name = "dgvSummary"
        Me.dgvSummary.ReadOnly = True
        Me.dgvSummary.RowHeadersVisible = False
        Me.dgvSummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvSummary.Size = New System.Drawing.Size(1128, 205)
        Me.dgvSummary.TabIndex = 0
        '
        'dgvStageName
        '
        Me.dgvStageName.AllowUserToAddRows = False
        Me.dgvStageName.AllowUserToDeleteRows = False
        Me.dgvStageName.AllowUserToResizeColumns = False
        Me.dgvStageName.AllowUserToResizeRows = False
        Me.dgvStageName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStageName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StageName, Me.colStageNameTotal})
        Me.dgvStageName.Location = New System.Drawing.Point(391, 7)
        Me.dgvStageName.Name = "dgvStageName"
        Me.dgvStageName.ReadOnly = True
        Me.dgvStageName.RowHeadersVisible = False
        Me.dgvStageName.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvStageName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvStageName.Size = New System.Drawing.Size(160, 297)
        Me.dgvStageName.TabIndex = 1
        '
        'StageName
        '
        Me.StageName.DataPropertyName = "StageName"
        Me.StageName.HeaderText = "Stage"
        Me.StageName.Name = "StageName"
        Me.StageName.ReadOnly = True
        Me.StageName.Width = 90
        '
        'colStageNameTotal
        '
        Me.colStageNameTotal.DataPropertyName = "Total"
        Me.colStageNameTotal.HeaderText = "Total"
        Me.colStageNameTotal.Name = "colStageNameTotal"
        Me.colStageNameTotal.ReadOnly = True
        Me.colStageNameTotal.Width = 40
        '
        'dgvClientName
        '
        Me.dgvClientName.AllowUserToAddRows = False
        Me.dgvClientName.AllowUserToDeleteRows = False
        Me.dgvClientName.AllowUserToResizeColumns = False
        Me.dgvClientName.AllowUserToResizeRows = False
        Me.dgvClientName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClientName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClientName, Me.colClientNameTotal})
        Me.dgvClientName.Location = New System.Drawing.Point(7, 29)
        Me.dgvClientName.Name = "dgvClientName"
        Me.dgvClientName.ReadOnly = True
        Me.dgvClientName.RowHeadersVisible = False
        Me.dgvClientName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClientName.Size = New System.Drawing.Size(160, 275)
        Me.dgvClientName.TabIndex = 2
        '
        'ClientName
        '
        Me.ClientName.DataPropertyName = "ClientName"
        Me.ClientName.HeaderText = "Client"
        Me.ClientName.Name = "ClientName"
        Me.ClientName.ReadOnly = True
        '
        'colClientNameTotal
        '
        Me.colClientNameTotal.DataPropertyName = "Total"
        Me.colClientNameTotal.HeaderText = "Total"
        Me.colClientNameTotal.Name = "colClientNameTotal"
        Me.colClientNameTotal.ReadOnly = True
        Me.colClientNameTotal.Width = 40
        '
        'dgvSchemeName
        '
        Me.dgvSchemeName.AllowUserToAddRows = False
        Me.dgvSchemeName.AllowUserToDeleteRows = False
        Me.dgvSchemeName.AllowUserToResizeColumns = False
        Me.dgvSchemeName.AllowUserToResizeRows = False
        Me.dgvSchemeName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSchemeName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SchemeName, Me.Total})
        Me.dgvSchemeName.Location = New System.Drawing.Point(199, 148)
        Me.dgvSchemeName.Name = "dgvSchemeName"
        Me.dgvSchemeName.ReadOnly = True
        Me.dgvSchemeName.RowHeadersVisible = False
        Me.dgvSchemeName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSchemeName.Size = New System.Drawing.Size(160, 156)
        Me.dgvSchemeName.TabIndex = 7
        '
        'SchemeName
        '
        Me.SchemeName.DataPropertyName = "SchemeName"
        Me.SchemeName.HeaderText = "Scheme"
        Me.SchemeName.Name = "SchemeName"
        Me.SchemeName.ReadOnly = True
        '
        'Total
        '
        Me.Total.DataPropertyName = "Total"
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        Me.Total.Width = 40
        '
        'cmdStageAll
        '
        Me.cmdStageAll.Location = New System.Drawing.Point(415, 304)
        Me.cmdStageAll.Name = "cmdStageAll"
        Me.cmdStageAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageAll.TabIndex = 8
        Me.cmdStageAll.Text = "All"
        Me.cmdStageAll.UseVisualStyleBackColor = True
        '
        'cmdStageClear
        '
        Me.cmdStageClear.Location = New System.Drawing.Point(477, 304)
        Me.cmdStageClear.Name = "cmdStageClear"
        Me.cmdStageClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageClear.TabIndex = 9
        Me.cmdStageClear.Text = "Clear"
        Me.cmdStageClear.UseVisualStyleBackColor = True
        '
        'cmdClientClear
        '
        Me.cmdClientClear.Location = New System.Drawing.Point(92, 304)
        Me.cmdClientClear.Name = "cmdClientClear"
        Me.cmdClientClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientClear.TabIndex = 13
        Me.cmdClientClear.Text = "Clear"
        Me.cmdClientClear.UseVisualStyleBackColor = True
        '
        'cmdClientAll
        '
        Me.cmdClientAll.Location = New System.Drawing.Point(30, 304)
        Me.cmdClientAll.Name = "cmdClientAll"
        Me.cmdClientAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientAll.TabIndex = 12
        Me.cmdClientAll.Text = "All"
        Me.cmdClientAll.UseVisualStyleBackColor = True
        '
        'cmdSchemeClear
        '
        Me.cmdSchemeClear.Location = New System.Drawing.Point(285, 304)
        Me.cmdSchemeClear.Name = "cmdSchemeClear"
        Me.cmdSchemeClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdSchemeClear.TabIndex = 15
        Me.cmdSchemeClear.Text = "Clear"
        Me.cmdSchemeClear.UseVisualStyleBackColor = True
        '
        'cmdSchemeAll
        '
        Me.cmdSchemeAll.Location = New System.Drawing.Point(223, 304)
        Me.cmdSchemeAll.Name = "cmdSchemeAll"
        Me.cmdSchemeAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdSchemeAll.TabIndex = 14
        Me.cmdSchemeAll.Text = "All"
        Me.cmdSchemeAll.UseVisualStyleBackColor = True
        '
        'cmdClearAll
        '
        Me.cmdClearAll.Location = New System.Drawing.Point(1065, 337)
        Me.cmdClearAll.Name = "cmdClearAll"
        Me.cmdClearAll.Size = New System.Drawing.Size(68, 20)
        Me.cmdClearAll.TabIndex = 25
        Me.cmdClearAll.Text = "Clear All"
        Me.cmdClearAll.UseVisualStyleBackColor = True
        '
        'lblLine1
        '
        Me.lblLine1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLine1.Location = New System.Drawing.Point(7, 329)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(1128, 1)
        Me.lblLine1.TabIndex = 26
        '
        'pnlAbsPerc
        '
        Me.pnlAbsPerc.Controls.Add(Me.radPerc)
        Me.pnlAbsPerc.Controls.Add(Me.radAbs)
        Me.pnlAbsPerc.Location = New System.Drawing.Point(958, 336)
        Me.pnlAbsPerc.Name = "pnlAbsPerc"
        Me.pnlAbsPerc.Size = New System.Drawing.Size(99, 25)
        Me.pnlAbsPerc.TabIndex = 29
        '
        'radPerc
        '
        Me.radPerc.AutoSize = True
        Me.radPerc.Location = New System.Drawing.Point(55, 4)
        Me.radPerc.Name = "radPerc"
        Me.radPerc.Size = New System.Drawing.Size(33, 17)
        Me.radPerc.TabIndex = 4
        Me.radPerc.TabStop = True
        Me.radPerc.Text = "%"
        Me.radPerc.UseVisualStyleBackColor = True
        '
        'radAbs
        '
        Me.radAbs.AutoSize = True
        Me.radAbs.Location = New System.Drawing.Point(6, 4)
        Me.radAbs.Name = "radAbs"
        Me.radAbs.Size = New System.Drawing.Size(43, 17)
        Me.radAbs.TabIndex = 3
        Me.radAbs.TabStop = True
        Me.radAbs.Text = "Abs"
        Me.radAbs.UseVisualStyleBackColor = True
        '
        'chkTopClients
        '
        Me.chkTopClients.AutoSize = True
        Me.chkTopClients.Location = New System.Drawing.Point(202, 341)
        Me.chkTopClients.Name = "chkTopClients"
        Me.chkTopClients.Size = New System.Drawing.Size(79, 17)
        Me.chkTopClients.TabIndex = 30
        Me.chkTopClients.Text = "Top Clients"
        Me.chkTopClients.UseVisualStyleBackColor = True
        '
        'cmdRefreshDB
        '
        Me.cmdRefreshDB.Location = New System.Drawing.Point(7, 338)
        Me.cmdRefreshDB.Name = "cmdRefreshDB"
        Me.cmdRefreshDB.Size = New System.Drawing.Size(75, 20)
        Me.cmdRefreshDB.TabIndex = 34
        Me.cmdRefreshDB.Text = "Refresh DB"
        Me.cmdRefreshDB.UseVisualStyleBackColor = True
        '
        'cmsSummary
        '
        Me.cmsSummary.Name = "ContextMenuStrip1"
        Me.cmsSummary.Size = New System.Drawing.Size(61, 4)
        '
        'cmdInYearClear
        '
        Me.cmdInYearClear.Location = New System.Drawing.Point(794, 304)
        Me.cmdInYearClear.Name = "cmdInYearClear"
        Me.cmdInYearClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdInYearClear.TabIndex = 37
        Me.cmdInYearClear.Text = "Clear"
        Me.cmdInYearClear.UseVisualStyleBackColor = True
        '
        'cmdInYearAll
        '
        Me.cmdInYearAll.Location = New System.Drawing.Point(732, 304)
        Me.cmdInYearAll.Name = "cmdInYearAll"
        Me.cmdInYearAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdInYearAll.TabIndex = 36
        Me.cmdInYearAll.Text = "All"
        Me.cmdInYearAll.UseVisualStyleBackColor = True
        '
        'dgvInYear
        '
        Me.dgvInYear.AllowUserToAddRows = False
        Me.dgvInYear.AllowUserToDeleteRows = False
        Me.dgvInYear.AllowUserToResizeColumns = False
        Me.dgvInYear.AllowUserToResizeRows = False
        Me.dgvInYear.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvInYear.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.InYear, Me.DataGridViewTextBoxColumn7})
        Me.dgvInYear.Location = New System.Drawing.Point(730, 239)
        Me.dgvInYear.Name = "dgvInYear"
        Me.dgvInYear.ReadOnly = True
        Me.dgvInYear.RowHeadersVisible = False
        Me.dgvInYear.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvInYear.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvInYear.Size = New System.Drawing.Size(115, 65)
        Me.dgvInYear.TabIndex = 35
        '
        'InYear
        '
        Me.InYear.DataPropertyName = "InYear"
        Me.InYear.HeaderText = "In year"
        Me.InYear.Name = "InYear"
        Me.InYear.ReadOnly = True
        Me.InYear.Width = 55
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 40
        '
        'bxtnCoveredAll
        '
        Me.bxtnCoveredAll.Location = New System.Drawing.Point(360, 160)
        Me.bxtnCoveredAll.Name = "bxtnCoveredAll"
        Me.bxtnCoveredAll.Size = New System.Drawing.Size(50, 20)
        Me.bxtnCoveredAll.TabIndex = 36
        Me.bxtnCoveredAll.Text = "All"
        Me.bxtnCoveredAll.UseVisualStyleBackColor = True
        '
        'dgvPostcodeArea
        '
        Me.dgvPostcodeArea.AllowUserToAddRows = False
        Me.dgvPostcodeArea.AllowUserToDeleteRows = False
        Me.dgvPostcodeArea.AllowUserToResizeColumns = False
        Me.dgvPostcodeArea.AllowUserToResizeRows = False
        Me.dgvPostcodeArea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPostcodeArea.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PostcodeArea, Me.DataGridViewTextBoxColumn9})
        Me.dgvPostcodeArea.Location = New System.Drawing.Point(583, 7)
        Me.dgvPostcodeArea.Name = "dgvPostcodeArea"
        Me.dgvPostcodeArea.ReadOnly = True
        Me.dgvPostcodeArea.RowHeadersVisible = False
        Me.dgvPostcodeArea.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvPostcodeArea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPostcodeArea.Size = New System.Drawing.Size(115, 297)
        Me.dgvPostcodeArea.TabIndex = 38
        '
        'PostcodeArea
        '
        Me.PostcodeArea.DataPropertyName = "PostcodeArea"
        Me.PostcodeArea.HeaderText = "Area"
        Me.PostcodeArea.Name = "PostcodeArea"
        Me.PostcodeArea.ReadOnly = True
        Me.PostcodeArea.Width = 55
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 40
        '
        'cmdPostcodeAreaClear
        '
        Me.cmdPostcodeAreaClear.Location = New System.Drawing.Point(647, 304)
        Me.cmdPostcodeAreaClear.Name = "cmdPostcodeAreaClear"
        Me.cmdPostcodeAreaClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaClear.TabIndex = 40
        Me.cmdPostcodeAreaClear.Text = "Clear"
        Me.cmdPostcodeAreaClear.UseVisualStyleBackColor = True
        '
        'cmdPostcodeAreaAll
        '
        Me.cmdPostcodeAreaAll.Location = New System.Drawing.Point(585, 304)
        Me.cmdPostcodeAreaAll.Name = "cmdPostcodeAreaAll"
        Me.cmdPostcodeAreaAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaAll.TabIndex = 39
        Me.cmdPostcodeAreaAll.Text = "All"
        Me.cmdPostcodeAreaAll.UseVisualStyleBackColor = True
        '
        'cmsList
        '
        Me.cmsList.Name = "ContextMenuStrip1"
        Me.cmsList.Size = New System.Drawing.Size(61, 4)
        '
        'cmsForm
        '
        Me.cmsForm.Name = "ContextMenuStrip1"
        Me.cmsForm.Size = New System.Drawing.Size(61, 4)
        '
        'lblSummary
        '
        Me.lblSummary.AutoSize = True
        Me.lblSummary.Location = New System.Drawing.Point(88, 342)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(39, 13)
        Me.lblSummary.TabIndex = 91
        Me.lblSummary.Text = "Label1"
        '
        'dgvDebtYear
        '
        Me.dgvDebtYear.AllowUserToAddRows = False
        Me.dgvDebtYear.AllowUserToDeleteRows = False
        Me.dgvDebtYear.AllowUserToResizeColumns = False
        Me.dgvDebtYear.AllowUserToResizeRows = False
        Me.dgvDebtYear.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDebtYear.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DebtYear, Me.DataGridViewTextBoxColumn12})
        Me.dgvDebtYear.Location = New System.Drawing.Point(730, 7)
        Me.dgvDebtYear.Name = "dgvDebtYear"
        Me.dgvDebtYear.ReadOnly = True
        Me.dgvDebtYear.RowHeadersVisible = False
        Me.dgvDebtYear.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvDebtYear.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDebtYear.Size = New System.Drawing.Size(115, 203)
        Me.dgvDebtYear.TabIndex = 98
        '
        'DebtYear
        '
        Me.DebtYear.DataPropertyName = "DebtYear"
        Me.DebtYear.HeaderText = "Year"
        Me.DebtYear.Name = "DebtYear"
        Me.DebtYear.ReadOnly = True
        Me.DebtYear.Width = 55
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 40
        '
        'cmdDebtYearClear
        '
        Me.cmdDebtYearClear.Location = New System.Drawing.Point(794, 210)
        Me.cmdDebtYearClear.Name = "cmdDebtYearClear"
        Me.cmdDebtYearClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdDebtYearClear.TabIndex = 100
        Me.cmdDebtYearClear.Text = "Clear"
        Me.cmdDebtYearClear.UseVisualStyleBackColor = True
        '
        'cmdDebtYearAll
        '
        Me.cmdDebtYearAll.Location = New System.Drawing.Point(732, 210)
        Me.cmdDebtYearAll.Name = "cmdDebtYearAll"
        Me.cmdDebtYearAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdDebtYearAll.TabIndex = 99
        Me.cmdDebtYearAll.Text = "All"
        Me.cmdDebtYearAll.UseVisualStyleBackColor = True
        '
        'dgvLinkedPIF
        '
        Me.dgvLinkedPIF.AllowUserToAddRows = False
        Me.dgvLinkedPIF.AllowUserToDeleteRows = False
        Me.dgvLinkedPIF.AllowUserToResizeColumns = False
        Me.dgvLinkedPIF.AllowUserToResizeRows = False
        Me.dgvLinkedPIF.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLinkedPIF.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LinkedPIF, Me.DataGridViewTextBoxColumn13})
        Me.dgvLinkedPIF.Location = New System.Drawing.Point(878, 239)
        Me.dgvLinkedPIF.Name = "dgvLinkedPIF"
        Me.dgvLinkedPIF.ReadOnly = True
        Me.dgvLinkedPIF.RowHeadersVisible = False
        Me.dgvLinkedPIF.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvLinkedPIF.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLinkedPIF.Size = New System.Drawing.Size(112, 65)
        Me.dgvLinkedPIF.TabIndex = 101
        '
        'LinkedPIF
        '
        Me.LinkedPIF.DataPropertyName = "LinkedPIF"
        Me.LinkedPIF.HeaderText = "Linked PIF"
        Me.LinkedPIF.Name = "LinkedPIF"
        Me.LinkedPIF.ReadOnly = True
        Me.LinkedPIF.Width = 70
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Width = 40
        '
        'cmdLinkedPIFClear
        '
        Me.cmdLinkedPIFClear.Location = New System.Drawing.Point(940, 304)
        Me.cmdLinkedPIFClear.Name = "cmdLinkedPIFClear"
        Me.cmdLinkedPIFClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedPIFClear.TabIndex = 103
        Me.cmdLinkedPIFClear.Text = "Clear"
        Me.cmdLinkedPIFClear.UseVisualStyleBackColor = True
        '
        'cmdLinkedPIFAll
        '
        Me.cmdLinkedPIFAll.Location = New System.Drawing.Point(878, 304)
        Me.cmdLinkedPIFAll.Name = "cmdLinkedPIFAll"
        Me.cmdLinkedPIFAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedPIFAll.TabIndex = 102
        Me.cmdLinkedPIFAll.Text = "All"
        Me.cmdLinkedPIFAll.UseVisualStyleBackColor = True
        '
        'RefreshCheckTimer
        '
        Me.RefreshCheckTimer.Interval = 30000
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(7, 7)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(160, 21)
        Me.cboCompany.TabIndex = 104
        '
        'pnlBalance
        '
        Me.pnlBalance.Controls.Add(Me.radHigh)
        Me.pnlBalance.Controls.Add(Me.radLow)
        Me.pnlBalance.Location = New System.Drawing.Point(314, 336)
        Me.pnlBalance.Name = "pnlBalance"
        Me.pnlBalance.Size = New System.Drawing.Size(197, 25)
        Me.pnlBalance.TabIndex = 105
        '
        'radHigh
        '
        Me.radHigh.AutoCheck = False
        Me.radHigh.AutoSize = True
        Me.radHigh.Location = New System.Drawing.Point(106, 4)
        Me.radHigh.Name = "radHigh"
        Me.radHigh.Size = New System.Drawing.Size(73, 17)
        Me.radHigh.TabIndex = 1
        Me.radHigh.TabStop = True
        Me.radHigh.Text = "> £10,000"
        Me.radHigh.UseVisualStyleBackColor = True
        '
        'radLow
        '
        Me.radLow.AutoCheck = False
        Me.radLow.AutoSize = True
        Me.radLow.Location = New System.Drawing.Point(6, 4)
        Me.radLow.Name = "radLow"
        Me.radLow.Size = New System.Drawing.Size(86, 17)
        Me.radLow.TabIndex = 0
        Me.radLow.TabStop = True
        Me.radLow.Text = "< £50 no link"
        Me.radLow.UseVisualStyleBackColor = True
        '
        'dgvLinkedTrace
        '
        Me.dgvLinkedTrace.AllowUserToAddRows = False
        Me.dgvLinkedTrace.AllowUserToDeleteRows = False
        Me.dgvLinkedTrace.AllowUserToResizeColumns = False
        Me.dgvLinkedTrace.AllowUserToResizeRows = False
        Me.dgvLinkedTrace.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLinkedTrace.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LinkedTrace, Me.DataGridViewTextBoxColumn2})
        Me.dgvLinkedTrace.Location = New System.Drawing.Point(878, 7)
        Me.dgvLinkedTrace.Name = "dgvLinkedTrace"
        Me.dgvLinkedTrace.ReadOnly = True
        Me.dgvLinkedTrace.RowHeadersVisible = False
        Me.dgvLinkedTrace.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvLinkedTrace.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLinkedTrace.Size = New System.Drawing.Size(112, 65)
        Me.dgvLinkedTrace.TabIndex = 106
        '
        'LinkedTrace
        '
        Me.LinkedTrace.DataPropertyName = "LinkedTrace"
        Me.LinkedTrace.HeaderText = "Linked Trc"
        Me.LinkedTrace.Name = "LinkedTrace"
        Me.LinkedTrace.ReadOnly = True
        Me.LinkedTrace.Width = 70
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 40
        '
        'cmdLinkedTraceClear
        '
        Me.cmdLinkedTraceClear.Location = New System.Drawing.Point(940, 72)
        Me.cmdLinkedTraceClear.Name = "cmdLinkedTraceClear"
        Me.cmdLinkedTraceClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedTraceClear.TabIndex = 108
        Me.cmdLinkedTraceClear.Text = "Clear"
        Me.cmdLinkedTraceClear.UseVisualStyleBackColor = True
        '
        'cmdLinkedTraceAll
        '
        Me.cmdLinkedTraceAll.Location = New System.Drawing.Point(878, 72)
        Me.cmdLinkedTraceAll.Name = "cmdLinkedTraceAll"
        Me.cmdLinkedTraceAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedTraceAll.TabIndex = 107
        Me.cmdLinkedTraceAll.Text = "All"
        Me.cmdLinkedTraceAll.UseVisualStyleBackColor = True
        '
        'dgvIsCompany
        '
        Me.dgvIsCompany.AllowUserToAddRows = False
        Me.dgvIsCompany.AllowUserToDeleteRows = False
        Me.dgvIsCompany.AllowUserToResizeColumns = False
        Me.dgvIsCompany.AllowUserToResizeRows = False
        Me.dgvIsCompany.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvIsCompany.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IsCompany, Me.DataGridViewTextBoxColumn3})
        Me.dgvIsCompany.Location = New System.Drawing.Point(878, 145)
        Me.dgvIsCompany.Name = "dgvIsCompany"
        Me.dgvIsCompany.ReadOnly = True
        Me.dgvIsCompany.RowHeadersVisible = False
        Me.dgvIsCompany.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvIsCompany.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvIsCompany.Size = New System.Drawing.Size(112, 65)
        Me.dgvIsCompany.TabIndex = 109
        '
        'IsCompany
        '
        Me.IsCompany.DataPropertyName = "IsCompany"
        Me.IsCompany.HeaderText = "Is Compa"
        Me.IsCompany.Name = "IsCompany"
        Me.IsCompany.ReadOnly = True
        Me.IsCompany.Width = 70
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 40
        '
        'cmdIsCompanyClear
        '
        Me.cmdIsCompanyClear.Location = New System.Drawing.Point(940, 210)
        Me.cmdIsCompanyClear.Name = "cmdIsCompanyClear"
        Me.cmdIsCompanyClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdIsCompanyClear.TabIndex = 111
        Me.cmdIsCompanyClear.Text = "Clear"
        Me.cmdIsCompanyClear.UseVisualStyleBackColor = True
        '
        'cmdIsCompanyAll
        '
        Me.cmdIsCompanyAll.Location = New System.Drawing.Point(878, 210)
        Me.cmdIsCompanyAll.Name = "cmdIsCompanyAll"
        Me.cmdIsCompanyAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdIsCompanyAll.TabIndex = 110
        Me.cmdIsCompanyAll.Text = "All"
        Me.cmdIsCompanyAll.UseVisualStyleBackColor = True
        '
        'dgvPDLCheck
        '
        Me.dgvPDLCheck.AllowUserToAddRows = False
        Me.dgvPDLCheck.AllowUserToDeleteRows = False
        Me.dgvPDLCheck.AllowUserToResizeColumns = False
        Me.dgvPDLCheck.AllowUserToResizeRows = False
        Me.dgvPDLCheck.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvPDLCheck.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PDLCheck, Me.DataGridViewTextBoxColumn4})
        Me.dgvPDLCheck.Location = New System.Drawing.Point(1021, 7)
        Me.dgvPDLCheck.Name = "dgvPDLCheck"
        Me.dgvPDLCheck.ReadOnly = True
        Me.dgvPDLCheck.RowHeadersVisible = False
        Me.dgvPDLCheck.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvPDLCheck.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPDLCheck.Size = New System.Drawing.Size(112, 65)
        Me.dgvPDLCheck.TabIndex = 112
        '
        'PDLCheck
        '
        Me.PDLCheck.DataPropertyName = "PDLCheck"
        Me.PDLCheck.HeaderText = "PDL Check"
        Me.PDLCheck.Name = "PDLCheck"
        Me.PDLCheck.ReadOnly = True
        Me.PDLCheck.Width = 70
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 40
        '
        'cmdPDLCheckClear
        '
        Me.cmdPDLCheckClear.Location = New System.Drawing.Point(1083, 72)
        Me.cmdPDLCheckClear.Name = "cmdPDLCheckClear"
        Me.cmdPDLCheckClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPDLCheckClear.TabIndex = 114
        Me.cmdPDLCheckClear.Text = "Clear"
        Me.cmdPDLCheckClear.UseVisualStyleBackColor = True
        '
        'cmdPDLCheckAll
        '
        Me.cmdPDLCheckAll.Location = New System.Drawing.Point(1021, 72)
        Me.cmdPDLCheckAll.Name = "cmdPDLCheckAll"
        Me.cmdPDLCheckAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPDLCheckAll.TabIndex = 113
        Me.cmdPDLCheckAll.Text = "All"
        Me.cmdPDLCheckAll.UseVisualStyleBackColor = True
        '
        'dgvPDLCheckBy
        '
        Me.dgvPDLCheckBy.AllowUserToAddRows = False
        Me.dgvPDLCheckBy.AllowUserToDeleteRows = False
        Me.dgvPDLCheckBy.AllowUserToResizeColumns = False
        Me.dgvPDLCheckBy.AllowUserToResizeRows = False
        Me.dgvPDLCheckBy.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPDLCheckBy.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PDLCheckBy, Me.DataGridViewTextBoxColumn1})
        Me.dgvPDLCheckBy.Location = New System.Drawing.Point(1021, 158)
        Me.dgvPDLCheckBy.Name = "dgvPDLCheckBy"
        Me.dgvPDLCheckBy.ReadOnly = True
        Me.dgvPDLCheckBy.RowHeadersVisible = False
        Me.dgvPDLCheckBy.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPDLCheckBy.Size = New System.Drawing.Size(114, 146)
        Me.dgvPDLCheckBy.TabIndex = 199
        '
        'cmdPDLCheckByClear
        '
        Me.cmdPDLCheckByClear.Location = New System.Drawing.Point(1083, 304)
        Me.cmdPDLCheckByClear.Name = "cmdPDLCheckByClear"
        Me.cmdPDLCheckByClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPDLCheckByClear.TabIndex = 198
        Me.cmdPDLCheckByClear.Text = "Clear"
        Me.cmdPDLCheckByClear.UseVisualStyleBackColor = True
        '
        'cmdPDLCheckByAll
        '
        Me.cmdPDLCheckByAll.Location = New System.Drawing.Point(1021, 306)
        Me.cmdPDLCheckByAll.Name = "cmdPDLCheckByAll"
        Me.cmdPDLCheckByAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPDLCheckByAll.TabIndex = 197
        Me.cmdPDLCheckByAll.Text = "All"
        Me.cmdPDLCheckByAll.UseVisualStyleBackColor = True
        '
        'PDLCheckBy
        '
        Me.PDLCheckBy.DataPropertyName = "PDLCheckBy"
        Me.PDLCheckBy.HeaderText = "Name"
        Me.PDLCheckBy.Name = "PDLCheckBy"
        Me.PDLCheckBy.ReadOnly = True
        Me.PDLCheckBy.Width = 70
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 40
        '
        'frmCaseSummary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange
        Me.ClientSize = New System.Drawing.Size(1141, 579)
        Me.Controls.Add(Me.dgvPDLCheckBy)
        Me.Controls.Add(Me.cmdPDLCheckByClear)
        Me.Controls.Add(Me.cmdPDLCheckByAll)
        Me.Controls.Add(Me.dgvPDLCheck)
        Me.Controls.Add(Me.cmdPDLCheckClear)
        Me.Controls.Add(Me.cmdPDLCheckAll)
        Me.Controls.Add(Me.dgvIsCompany)
        Me.Controls.Add(Me.cmdIsCompanyClear)
        Me.Controls.Add(Me.cmdIsCompanyAll)
        Me.Controls.Add(Me.dgvLinkedTrace)
        Me.Controls.Add(Me.cmdLinkedTraceClear)
        Me.Controls.Add(Me.cmdLinkedTraceAll)
        Me.Controls.Add(Me.pnlBalance)
        Me.Controls.Add(Me.cboCompany)
        Me.Controls.Add(Me.dgvDebtYear)
        Me.Controls.Add(Me.cmdDebtYearClear)
        Me.Controls.Add(Me.dgvLinkedPIF)
        Me.Controls.Add(Me.cmdDebtYearAll)
        Me.Controls.Add(Me.lblSummary)
        Me.Controls.Add(Me.cmdLinkedPIFClear)
        Me.Controls.Add(Me.cmdLinkedPIFAll)
        Me.Controls.Add(Me.dgvPostcodeArea)
        Me.Controls.Add(Me.cmdInYearClear)
        Me.Controls.Add(Me.dgvInYear)
        Me.Controls.Add(Me.cmdInYearAll)
        Me.Controls.Add(Me.cmdRefreshDB)
        Me.Controls.Add(Me.cmdPostcodeAreaClear)
        Me.Controls.Add(Me.cmdPostcodeAreaAll)
        Me.Controls.Add(Me.chkTopClients)
        Me.Controls.Add(Me.pnlAbsPerc)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cmdClearAll)
        Me.Controls.Add(Me.cmdSchemeClear)
        Me.Controls.Add(Me.cmdStageClear)
        Me.Controls.Add(Me.cmdStageAll)
        Me.Controls.Add(Me.cmdClientClear)
        Me.Controls.Add(Me.dgvSchemeName)
        Me.Controls.Add(Me.cmdClientAll)
        Me.Controls.Add(Me.cmdSchemeAll)
        Me.Controls.Add(Me.dgvClientName)
        Me.Controls.Add(Me.dgvStageName)
        Me.Controls.Add(Me.dgvSummary)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCaseSummary"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Previous day loaded"
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvStageName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSchemeName, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlAbsPerc.ResumeLayout(False)
        Me.pnlAbsPerc.PerformLayout()
        CType(Me.dgvInYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDebtYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLinkedPIF, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBalance.ResumeLayout(False)
        Me.pnlBalance.PerformLayout()
        CType(Me.dgvLinkedTrace, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvIsCompany, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPDLCheck, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPDLCheckBy, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvSummary As System.Windows.Forms.DataGridView
    Friend WithEvents dgvStageName As System.Windows.Forms.DataGridView
    Friend WithEvents dgvClientName As System.Windows.Forms.DataGridView
    Friend WithEvents dgvSchemeName As System.Windows.Forms.DataGridView
    Friend WithEvents ClientName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClientNameTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StageName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colStageNameTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdStageAll As System.Windows.Forms.Button
    Friend WithEvents cmdStageClear As System.Windows.Forms.Button
    Friend WithEvents cmdClientClear As System.Windows.Forms.Button
    Friend WithEvents cmdClientAll As System.Windows.Forms.Button
    Friend WithEvents cmdSchemeClear As System.Windows.Forms.Button
    Friend WithEvents cmdSchemeAll As System.Windows.Forms.Button
    Friend WithEvents cmdClearAll As System.Windows.Forms.Button
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents pnlAbsPerc As System.Windows.Forms.Panel
    Friend WithEvents radPerc As System.Windows.Forms.RadioButton
    Friend WithEvents radAbs As System.Windows.Forms.RadioButton
    Friend WithEvents chkTopClients As System.Windows.Forms.CheckBox
    Friend WithEvents cmdRefreshDB As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents cmsSummary As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmdInYearClear As System.Windows.Forms.Button
    Friend WithEvents cmdInYearAll As System.Windows.Forms.Button
    Friend WithEvents dgvInYear As System.Windows.Forms.DataGridView
    Friend WithEvents bxtnCoveredAll As System.Windows.Forms.Button
    Friend WithEvents SchemeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvPostcodeArea As System.Windows.Forms.DataGridView
    Friend WithEvents cmdPostcodeAreaClear As System.Windows.Forms.Button
    Friend WithEvents cmdPostcodeAreaAll As System.Windows.Forms.Button
    Friend WithEvents PostcodeArea As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmsList As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmsForm As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents dgvDebtYear As System.Windows.Forms.DataGridView
    Friend WithEvents cmdDebtYearClear As System.Windows.Forms.Button
    Friend WithEvents cmdDebtYearAll As System.Windows.Forms.Button
    Friend WithEvents DebtYear As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvLinkedPIF As System.Windows.Forms.DataGridView
    Friend WithEvents cmdLinkedPIFClear As System.Windows.Forms.Button
    Friend WithEvents cmdLinkedPIFAll As System.Windows.Forms.Button
    Friend WithEvents RefreshCheckTimer As System.Windows.Forms.Timer
    Friend WithEvents InYear As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LinkedPIF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents pnlBalance As System.Windows.Forms.Panel
    Friend WithEvents radHigh As System.Windows.Forms.RadioButton
    Friend WithEvents radLow As System.Windows.Forms.RadioButton
    Friend WithEvents dgvLinkedTrace As System.Windows.Forms.DataGridView
    Friend WithEvents cmdLinkedTraceClear As System.Windows.Forms.Button
    Friend WithEvents cmdLinkedTraceAll As System.Windows.Forms.Button
    Friend WithEvents LinkedTrace As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvIsCompany As System.Windows.Forms.DataGridView
    Friend WithEvents IsCompany As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdIsCompanyClear As System.Windows.Forms.Button
    Friend WithEvents cmdIsCompanyAll As System.Windows.Forms.Button
    Friend WithEvents dgvPDLCheck As System.Windows.Forms.DataGridView
    Friend WithEvents PDLCheck As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdPDLCheckClear As System.Windows.Forms.Button
    Friend WithEvents cmdPDLCheckAll As System.Windows.Forms.Button
    Friend WithEvents dgvPDLCheckBy As System.Windows.Forms.DataGridView
    Friend WithEvents cmdPDLCheckByClear As System.Windows.Forms.Button
    Friend WithEvents cmdPDLCheckByAll As System.Windows.Forms.Button
    Friend WithEvents PDLCheckBy As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn

End Class

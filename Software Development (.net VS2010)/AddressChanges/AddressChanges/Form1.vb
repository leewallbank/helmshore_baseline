﻿Imports CommonLibrary
Public Class Form1
    Dim clientID As Integer
    Dim folderName As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub pdfbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pdfbtn.Click
        clientID = 176
        folderName = "North Devon"
        run_report()
    End Sub
    Private Sub run_report()
        'get all address changes last week.
        Dim startDate As Date = DateAdd(DateInterval.Day, -Weekday(Now) - 6, Now)
        startDate = CDate(Format(startDate, "yyyy-MM-dd") & " 00:00:00")
        Dim endDate As Date = DateAdd(DateInterval.Day, 8, startDate)
        pdfbtn.Enabled = False
        exitbtn.Enabled = False
        knowsleybtn.Enabled = False
        Dim dt As New DataTable
        LoadDataTable("DebtRecovery", "SELECT D._rowid, D.client_ref " & _
                                                "FROM debtor D, clientScheme CS, note N, scheme S " & _
                                                "WHERE CS.clientID = " & clientID & _
                                                " AND CS._rowID = D.clientSchemeID" & _
                                                " AND CS.SchemeID = S._rowID" & _
                                                " AND not(S.work_type in (16,20))" & _
                                                " AND N.DebtorID = D._rowID" & _
                                                " AND D.status_open_closed = 'O'" & _
                                                " AND N.type = 'Address'" & _
                                                " AND N.text like 'changed%'" & _
                                                " AND N._createdDate >= '" & Format(startDate, "yyyy-MM-=dd") & "'" & _
                                                " AND N._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'", dt, False)
        Dim addressChanges As Integer = 0
        Dim dir_name As String = "H:RA2231\" & folderName
        Dim progressbar As New ProgressBar
        progressbar.Maximum = dt.Rows.Count
        For Each dtRow In dt.Rows
            progressbar.Value += 1
            Application.DoEvents()
            Dim debtorID As Integer = dtRow(0)
            Dim clientREf As String = dtRow(1)
            addressChanges += 1
            If addressChanges = 1 Then
                'create folder for pdfs
                'delete folder containing last reports
                Try
                    Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                    If System.IO.Directory.Exists(dir_name) = False Then
                        di = System.IO.Directory.CreateDirectory(dir_name)
                    Else
                        System.IO.Directory.Delete(dir_name, True)
                        di = System.IO.Directory.CreateDirectory(dir_name)
                    End If
                Catch ex As Exception
                    MsgBox("Unable to create reports folder")
                    Exit Sub
                    End
                End Try
            End If
            Dim filename As String = dir_name & "\" & clientREf & "-" & debtorID & ".pdf"
            Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
            Dim RA2231report = New RA2231
            Dim myArrayList1 As ArrayList = New ArrayList()
            myArrayList1.Add(startDate)
            SetCurrentValuesForParameterField1(RA2231report, myArrayList1)
            myArrayList1.Add(endDate)
            SetCurrentValuesForParameterField2(RA2231report, myArrayList1)
            myArrayList1.Add(debtorID)
            SetCurrentValuesForParameterField3(RA2231report, myArrayList1)
            'MsgBox("Ready to connect")
            myConnectionInfo.ServerName = "DebtRecovery"
            myConnectionInfo.DatabaseName = "DebtRecovery"
            myConnectionInfo.UserID = "vbnet"
            myConnectionInfo.Password = "tenbv"
            SetDBLogonForReport(myConnectionInfo, RA2231report)
            Try
                RA2231report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
            Catch ex As Exception
                MsgBox(ex.ToString)
            End Try

            RA2231report.Close()
        Next


        If addressChanges = 0 Then
            MsgBox("No address changes found")
        Else
            MsgBox("Address changes saved in folder 'H:RA2231\" & folderName)
        End If

        Me.Close()
    End Sub

   
    Private Sub knowsleybtn_Click(sender As System.Object, e As System.EventArgs) Handles knowsleybtn.Click
        clientID = 52
        folderName = "Knowsley"
        run_report()
    End Sub
End Class

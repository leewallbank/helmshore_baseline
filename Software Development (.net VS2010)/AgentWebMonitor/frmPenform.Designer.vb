<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmPenform
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPenform))
        Me.txtNumberFailed = New System.Windows.Forms.TextBox()
        Me.txtNumberWaiting = New System.Windows.Forms.TextBox()
        Me.txtNumberSent = New System.Windows.Forms.TextBox()
        Me.txtFailed = New System.Windows.Forms.TextBox()
        Me.txtWaiting = New System.Windows.Forms.TextBox()
        Me.txtSent = New System.Windows.Forms.TextBox()
        Me.chkHideOldest = New System.Windows.Forms.CheckBox()
        Me.lblCheckProcessServer = New System.Windows.Forms.Label()
        Me.lblSMSMessages = New System.Windows.Forms.Label()
        Me.lblLastUpdated = New System.Windows.Forms.Label()
        Me.lblOldestCase = New System.Windows.Forms.Label()
        Me.lblOutstandingAll = New System.Windows.Forms.Label()
        Me.lblAttentionHour = New System.Windows.Forms.Label()
        Me.lblOutstandingHour = New System.Windows.Forms.Label()
        Me.lblProcessedHour = New System.Windows.Forms.Label()
        Me.lblAttentionToday = New System.Windows.Forms.Label()
        Me.lblOutstandingToday = New System.Windows.Forms.Label()
        Me.lblProcessedToday = New System.Windows.Forms.Label()
        Me.tmrSwitchStats = New System.Windows.Forms.Timer(Me.components)
        Me.lblReceivedHour = New System.Windows.Forms.Label()
        Me.lblHour = New System.Windows.Forms.Label()
        Me.lblReceivedToday = New System.Windows.Forms.Label()
        Me.lblToday = New System.Windows.Forms.Label()
        Me.lblAttentionWeek = New System.Windows.Forms.Label()
        Me.lblOutstandingWeek = New System.Windows.Forms.Label()
        Me.lblProcessedWeek = New System.Windows.Forms.Label()
        Me.lblOutstanding = New System.Windows.Forms.Label()
        Me.lblProcessed = New System.Windows.Forms.Label()
        Me.lblReceived = New System.Windows.Forms.Label()
        Me.lblWeek = New System.Windows.Forms.Label()
        Me.lblReceivedWeek = New System.Windows.Forms.Label()
        Me.lblAttention = New System.Windows.Forms.Label()
        Me.PDFViewer = New AxAcroPDFLib.AxAcroPDF()
        Me.tmrNextPage = New System.Windows.Forms.Timer(Me.components)
        CType(Me.PDFViewer, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtNumberFailed
        '
        Me.txtNumberFailed.AcceptsReturn = True
        Me.txtNumberFailed.BackColor = System.Drawing.SystemColors.Window
        Me.txtNumberFailed.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNumberFailed.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumberFailed.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNumberFailed.Location = New System.Drawing.Point(787, 576)
        Me.txtNumberFailed.MaxLength = 0
        Me.txtNumberFailed.Name = "txtNumberFailed"
        Me.txtNumberFailed.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNumberFailed.Size = New System.Drawing.Size(153, 44)
        Me.txtNumberFailed.TabIndex = 60
        '
        'txtNumberWaiting
        '
        Me.txtNumberWaiting.AcceptsReturn = True
        Me.txtNumberWaiting.BackColor = System.Drawing.SystemColors.Window
        Me.txtNumberWaiting.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNumberWaiting.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumberWaiting.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNumberWaiting.Location = New System.Drawing.Point(491, 576)
        Me.txtNumberWaiting.MaxLength = 0
        Me.txtNumberWaiting.Name = "txtNumberWaiting"
        Me.txtNumberWaiting.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNumberWaiting.Size = New System.Drawing.Size(153, 44)
        Me.txtNumberWaiting.TabIndex = 59
        '
        'txtNumberSent
        '
        Me.txtNumberSent.AcceptsReturn = True
        Me.txtNumberSent.BackColor = System.Drawing.SystemColors.Window
        Me.txtNumberSent.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNumberSent.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNumberSent.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtNumberSent.Location = New System.Drawing.Point(179, 576)
        Me.txtNumberSent.MaxLength = 0
        Me.txtNumberSent.Name = "txtNumberSent"
        Me.txtNumberSent.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtNumberSent.Size = New System.Drawing.Size(153, 44)
        Me.txtNumberSent.TabIndex = 58
        '
        'txtFailed
        '
        Me.txtFailed.AcceptsReturn = True
        Me.txtFailed.BackColor = System.Drawing.SystemColors.Window
        Me.txtFailed.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtFailed.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtFailed.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtFailed.Location = New System.Drawing.Point(651, 576)
        Me.txtFailed.MaxLength = 0
        Me.txtFailed.Name = "txtFailed"
        Me.txtFailed.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtFailed.Size = New System.Drawing.Size(129, 44)
        Me.txtFailed.TabIndex = 57
        Me.txtFailed.Text = "Failed :"
        '
        'txtWaiting
        '
        Me.txtWaiting.AcceptsReturn = True
        Me.txtWaiting.BackColor = System.Drawing.SystemColors.Window
        Me.txtWaiting.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtWaiting.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtWaiting.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtWaiting.Location = New System.Drawing.Point(339, 576)
        Me.txtWaiting.MaxLength = 0
        Me.txtWaiting.Name = "txtWaiting"
        Me.txtWaiting.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtWaiting.Size = New System.Drawing.Size(145, 44)
        Me.txtWaiting.TabIndex = 56
        Me.txtWaiting.Text = "Waiting :"
        '
        'txtSent
        '
        Me.txtSent.AcceptsReturn = True
        Me.txtSent.BackColor = System.Drawing.SystemColors.Window
        Me.txtSent.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtSent.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSent.ForeColor = System.Drawing.SystemColors.WindowText
        Me.txtSent.Location = New System.Drawing.Point(75, 576)
        Me.txtSent.MaxLength = 0
        Me.txtSent.Name = "txtSent"
        Me.txtSent.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.txtSent.Size = New System.Drawing.Size(97, 44)
        Me.txtSent.TabIndex = 55
        Me.txtSent.Text = "Sent :"
        '
        'chkHideOldest
        '
        Me.chkHideOldest.BackColor = System.Drawing.SystemColors.Control
        Me.chkHideOldest.Checked = True
        Me.chkHideOldest.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkHideOldest.Cursor = System.Windows.Forms.Cursors.Default
        Me.chkHideOldest.ForeColor = System.Drawing.SystemColors.ControlText
        Me.chkHideOldest.Location = New System.Drawing.Point(30, 520)
        Me.chkHideOldest.Name = "chkHideOldest"
        Me.chkHideOldest.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.chkHideOldest.Size = New System.Drawing.Size(41, 37)
        Me.chkHideOldest.TabIndex = 53
        Me.chkHideOldest.UseVisualStyleBackColor = False
        Me.chkHideOldest.Visible = False
        '
        'lblCheckProcessServer
        '
        Me.lblCheckProcessServer.BackColor = System.Drawing.Color.Red
        Me.lblCheckProcessServer.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblCheckProcessServer.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblCheckProcessServer.Location = New System.Drawing.Point(75, 495)
        Me.lblCheckProcessServer.Name = "lblCheckProcessServer"
        Me.lblCheckProcessServer.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblCheckProcessServer.Size = New System.Drawing.Size(995, 33)
        Me.lblCheckProcessServer.TabIndex = 61
        Me.lblCheckProcessServer.Text = "Check Process Server - "
        Me.lblCheckProcessServer.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblCheckProcessServer.Visible = False
        '
        'lblSMSMessages
        '
        Me.lblSMSMessages.BackColor = System.Drawing.SystemColors.Control
        Me.lblSMSMessages.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblSMSMessages.Font = New System.Drawing.Font("Microsoft Sans Serif", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSMSMessages.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSMSMessages.Location = New System.Drawing.Point(77, 535)
        Me.lblSMSMessages.Name = "lblSMSMessages"
        Me.lblSMSMessages.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblSMSMessages.Size = New System.Drawing.Size(505, 37)
        Me.lblSMSMessages.TabIndex = 54
        Me.lblSMSMessages.Text = "Bailiff SMS Text Messages"
        '
        'lblLastUpdated
        '
        Me.lblLastUpdated.BackColor = System.Drawing.SystemColors.Control
        Me.lblLastUpdated.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblLastUpdated.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblLastUpdated.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblLastUpdated.Location = New System.Drawing.Point(75, 407)
        Me.lblLastUpdated.Name = "lblLastUpdated"
        Me.lblLastUpdated.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblLastUpdated.Size = New System.Drawing.Size(995, 33)
        Me.lblLastUpdated.TabIndex = 52
        '
        'lblOldestCase
        '
        Me.lblOldestCase.BackColor = System.Drawing.SystemColors.Control
        Me.lblOldestCase.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblOldestCase.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblOldestCase.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblOldestCase.Location = New System.Drawing.Point(75, 447)
        Me.lblOldestCase.Name = "lblOldestCase"
        Me.lblOldestCase.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblOldestCase.Size = New System.Drawing.Size(995, 41)
        Me.lblOldestCase.TabIndex = 51
        '
        'lblOutstandingAll
        '
        Me.lblOutstandingAll.BackColor = System.Drawing.SystemColors.Control
        Me.lblOutstandingAll.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblOutstandingAll.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblOutstandingAll.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOutstandingAll.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblOutstandingAll.Location = New System.Drawing.Point(75, 8)
        Me.lblOutstandingAll.Name = "lblOutstandingAll"
        Me.lblOutstandingAll.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblOutstandingAll.Size = New System.Drawing.Size(275, 73)
        Me.lblOutstandingAll.TabIndex = 50
        '
        'lblAttentionHour
        '
        Me.lblAttentionHour.BackColor = System.Drawing.SystemColors.Control
        Me.lblAttentionHour.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblAttentionHour.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAttentionHour.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAttentionHour.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAttentionHour.Location = New System.Drawing.Point(837, 328)
        Me.lblAttentionHour.Name = "lblAttentionHour"
        Me.lblAttentionHour.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAttentionHour.Size = New System.Drawing.Size(233, 73)
        Me.lblAttentionHour.TabIndex = 49
        '
        'lblOutstandingHour
        '
        Me.lblOutstandingHour.BackColor = System.Drawing.SystemColors.Control
        Me.lblOutstandingHour.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblOutstandingHour.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblOutstandingHour.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOutstandingHour.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblOutstandingHour.Location = New System.Drawing.Point(837, 248)
        Me.lblOutstandingHour.Name = "lblOutstandingHour"
        Me.lblOutstandingHour.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblOutstandingHour.Size = New System.Drawing.Size(233, 73)
        Me.lblOutstandingHour.TabIndex = 48
        '
        'lblProcessedHour
        '
        Me.lblProcessedHour.BackColor = System.Drawing.SystemColors.Control
        Me.lblProcessedHour.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblProcessedHour.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProcessedHour.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProcessedHour.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProcessedHour.Location = New System.Drawing.Point(837, 168)
        Me.lblProcessedHour.Name = "lblProcessedHour"
        Me.lblProcessedHour.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProcessedHour.Size = New System.Drawing.Size(233, 73)
        Me.lblProcessedHour.TabIndex = 47
        '
        'lblAttentionToday
        '
        Me.lblAttentionToday.BackColor = System.Drawing.SystemColors.Control
        Me.lblAttentionToday.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblAttentionToday.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAttentionToday.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAttentionToday.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAttentionToday.Location = New System.Drawing.Point(597, 328)
        Me.lblAttentionToday.Name = "lblAttentionToday"
        Me.lblAttentionToday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAttentionToday.Size = New System.Drawing.Size(233, 73)
        Me.lblAttentionToday.TabIndex = 44
        '
        'lblOutstandingToday
        '
        Me.lblOutstandingToday.BackColor = System.Drawing.SystemColors.Control
        Me.lblOutstandingToday.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblOutstandingToday.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblOutstandingToday.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOutstandingToday.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblOutstandingToday.Location = New System.Drawing.Point(597, 248)
        Me.lblOutstandingToday.Name = "lblOutstandingToday"
        Me.lblOutstandingToday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblOutstandingToday.Size = New System.Drawing.Size(233, 73)
        Me.lblOutstandingToday.TabIndex = 43
        '
        'lblProcessedToday
        '
        Me.lblProcessedToday.BackColor = System.Drawing.SystemColors.Control
        Me.lblProcessedToday.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblProcessedToday.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProcessedToday.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProcessedToday.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProcessedToday.Location = New System.Drawing.Point(597, 168)
        Me.lblProcessedToday.Name = "lblProcessedToday"
        Me.lblProcessedToday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProcessedToday.Size = New System.Drawing.Size(233, 73)
        Me.lblProcessedToday.TabIndex = 42
        '
        'tmrSwitchStats
        '
        Me.tmrSwitchStats.Enabled = True
        Me.tmrSwitchStats.Interval = 15000
        '
        'lblReceivedHour
        '
        Me.lblReceivedHour.BackColor = System.Drawing.SystemColors.Control
        Me.lblReceivedHour.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblReceivedHour.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReceivedHour.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceivedHour.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblReceivedHour.Location = New System.Drawing.Point(837, 88)
        Me.lblReceivedHour.Name = "lblReceivedHour"
        Me.lblReceivedHour.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReceivedHour.Size = New System.Drawing.Size(233, 73)
        Me.lblReceivedHour.TabIndex = 46
        '
        'lblHour
        '
        Me.lblHour.BackColor = System.Drawing.SystemColors.Control
        Me.lblHour.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblHour.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblHour.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHour.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblHour.Location = New System.Drawing.Point(837, 8)
        Me.lblHour.Name = "lblHour"
        Me.lblHour.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblHour.Size = New System.Drawing.Size(233, 73)
        Me.lblHour.TabIndex = 45
        Me.lblHour.Text = "Hour"
        '
        'lblReceivedToday
        '
        Me.lblReceivedToday.BackColor = System.Drawing.SystemColors.Control
        Me.lblReceivedToday.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblReceivedToday.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReceivedToday.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceivedToday.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblReceivedToday.Location = New System.Drawing.Point(597, 88)
        Me.lblReceivedToday.Name = "lblReceivedToday"
        Me.lblReceivedToday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReceivedToday.Size = New System.Drawing.Size(233, 73)
        Me.lblReceivedToday.TabIndex = 41
        '
        'lblToday
        '
        Me.lblToday.BackColor = System.Drawing.SystemColors.Control
        Me.lblToday.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblToday.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblToday.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblToday.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblToday.Location = New System.Drawing.Point(597, 8)
        Me.lblToday.Name = "lblToday"
        Me.lblToday.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblToday.Size = New System.Drawing.Size(233, 73)
        Me.lblToday.TabIndex = 40
        Me.lblToday.Text = "Day"
        '
        'lblAttentionWeek
        '
        Me.lblAttentionWeek.BackColor = System.Drawing.SystemColors.Control
        Me.lblAttentionWeek.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblAttentionWeek.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAttentionWeek.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAttentionWeek.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAttentionWeek.Location = New System.Drawing.Point(357, 328)
        Me.lblAttentionWeek.Name = "lblAttentionWeek"
        Me.lblAttentionWeek.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAttentionWeek.Size = New System.Drawing.Size(233, 73)
        Me.lblAttentionWeek.TabIndex = 39
        '
        'lblOutstandingWeek
        '
        Me.lblOutstandingWeek.BackColor = System.Drawing.SystemColors.Control
        Me.lblOutstandingWeek.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblOutstandingWeek.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblOutstandingWeek.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOutstandingWeek.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblOutstandingWeek.Location = New System.Drawing.Point(357, 248)
        Me.lblOutstandingWeek.Name = "lblOutstandingWeek"
        Me.lblOutstandingWeek.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblOutstandingWeek.Size = New System.Drawing.Size(233, 73)
        Me.lblOutstandingWeek.TabIndex = 38
        '
        'lblProcessedWeek
        '
        Me.lblProcessedWeek.BackColor = System.Drawing.SystemColors.Control
        Me.lblProcessedWeek.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblProcessedWeek.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProcessedWeek.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProcessedWeek.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProcessedWeek.Location = New System.Drawing.Point(357, 168)
        Me.lblProcessedWeek.Name = "lblProcessedWeek"
        Me.lblProcessedWeek.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProcessedWeek.Size = New System.Drawing.Size(233, 73)
        Me.lblProcessedWeek.TabIndex = 37
        '
        'lblOutstanding
        '
        Me.lblOutstanding.BackColor = System.Drawing.SystemColors.Control
        Me.lblOutstanding.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblOutstanding.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblOutstanding.Font = New System.Drawing.Font("Arial", 33.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOutstanding.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblOutstanding.Location = New System.Drawing.Point(75, 248)
        Me.lblOutstanding.Name = "lblOutstanding"
        Me.lblOutstanding.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblOutstanding.Size = New System.Drawing.Size(275, 73)
        Me.lblOutstanding.TabIndex = 33
        Me.lblOutstanding.Text = "OutStanding"
        '
        'lblProcessed
        '
        Me.lblProcessed.BackColor = System.Drawing.SystemColors.Control
        Me.lblProcessed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblProcessed.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblProcessed.Font = New System.Drawing.Font("Arial", 33.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProcessed.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblProcessed.Location = New System.Drawing.Point(75, 168)
        Me.lblProcessed.Name = "lblProcessed"
        Me.lblProcessed.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblProcessed.Size = New System.Drawing.Size(275, 73)
        Me.lblProcessed.TabIndex = 32
        Me.lblProcessed.Text = "Processed"
        '
        'lblReceived
        '
        Me.lblReceived.BackColor = System.Drawing.SystemColors.Control
        Me.lblReceived.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblReceived.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReceived.Font = New System.Drawing.Font("Arial", 33.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceived.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblReceived.Location = New System.Drawing.Point(75, 88)
        Me.lblReceived.Name = "lblReceived"
        Me.lblReceived.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReceived.Size = New System.Drawing.Size(275, 73)
        Me.lblReceived.TabIndex = 31
        Me.lblReceived.Text = "Received"
        '
        'lblWeek
        '
        Me.lblWeek.BackColor = System.Drawing.SystemColors.Control
        Me.lblWeek.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblWeek.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblWeek.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWeek.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblWeek.Location = New System.Drawing.Point(357, 8)
        Me.lblWeek.Name = "lblWeek"
        Me.lblWeek.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblWeek.Size = New System.Drawing.Size(233, 73)
        Me.lblWeek.TabIndex = 35
        Me.lblWeek.Text = "Week"
        '
        'lblReceivedWeek
        '
        Me.lblReceivedWeek.BackColor = System.Drawing.SystemColors.Control
        Me.lblReceivedWeek.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblReceivedWeek.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblReceivedWeek.Font = New System.Drawing.Font("Arial", 48.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceivedWeek.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblReceivedWeek.Location = New System.Drawing.Point(357, 88)
        Me.lblReceivedWeek.Name = "lblReceivedWeek"
        Me.lblReceivedWeek.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblReceivedWeek.Size = New System.Drawing.Size(233, 73)
        Me.lblReceivedWeek.TabIndex = 36
        '
        'lblAttention
        '
        Me.lblAttention.BackColor = System.Drawing.SystemColors.Control
        Me.lblAttention.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblAttention.Cursor = System.Windows.Forms.Cursors.Default
        Me.lblAttention.Font = New System.Drawing.Font("Arial", 33.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAttention.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblAttention.Location = New System.Drawing.Point(75, 328)
        Me.lblAttention.Name = "lblAttention"
        Me.lblAttention.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.lblAttention.Size = New System.Drawing.Size(275, 73)
        Me.lblAttention.TabIndex = 34
        Me.lblAttention.Text = "Attention"
        '
        'PDFViewer
        '
        Me.PDFViewer.Enabled = True
        Me.PDFViewer.Location = New System.Drawing.Point(6, 1)
        Me.PDFViewer.Name = "PDFViewer"
        Me.PDFViewer.OcxState = CType(resources.GetObject("PDFViewer.OcxState"), System.Windows.Forms.AxHost.State)
        Me.PDFViewer.Size = New System.Drawing.Size(1130, 619)
        Me.PDFViewer.TabIndex = 62
        Me.PDFViewer.Visible = False
        '
        'tmrNextPage
        '
        Me.tmrNextPage.Interval = 5000
        '
        'frmPenform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(1138, 627)
        Me.Controls.Add(Me.txtNumberFailed)
        Me.Controls.Add(Me.txtNumberWaiting)
        Me.Controls.Add(Me.txtNumberSent)
        Me.Controls.Add(Me.txtFailed)
        Me.Controls.Add(Me.txtWaiting)
        Me.Controls.Add(Me.txtSent)
        Me.Controls.Add(Me.chkHideOldest)
        Me.Controls.Add(Me.lblCheckProcessServer)
        Me.Controls.Add(Me.lblSMSMessages)
        Me.Controls.Add(Me.lblLastUpdated)
        Me.Controls.Add(Me.lblOldestCase)
        Me.Controls.Add(Me.lblOutstandingAll)
        Me.Controls.Add(Me.lblAttentionHour)
        Me.Controls.Add(Me.lblOutstandingHour)
        Me.Controls.Add(Me.lblProcessedHour)
        Me.Controls.Add(Me.lblAttentionToday)
        Me.Controls.Add(Me.lblOutstandingToday)
        Me.Controls.Add(Me.lblProcessedToday)
        Me.Controls.Add(Me.lblReceivedHour)
        Me.Controls.Add(Me.lblHour)
        Me.Controls.Add(Me.lblReceivedToday)
        Me.Controls.Add(Me.lblToday)
        Me.Controls.Add(Me.lblAttentionWeek)
        Me.Controls.Add(Me.lblOutstandingWeek)
        Me.Controls.Add(Me.lblProcessedWeek)
        Me.Controls.Add(Me.lblOutstanding)
        Me.Controls.Add(Me.lblProcessed)
        Me.Controls.Add(Me.lblReceived)
        Me.Controls.Add(Me.lblWeek)
        Me.Controls.Add(Me.lblReceivedWeek)
        Me.Controls.Add(Me.lblAttention)
        Me.Controls.Add(Me.PDFViewer)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Location = New System.Drawing.Point(4, 23)
        Me.Name = "frmPenform"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "OneStep Agent Web Monitor Vers. 1.11"
        CType(Me.PDFViewer, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Public WithEvents txtNumberFailed As System.Windows.Forms.TextBox
    Public WithEvents txtNumberWaiting As System.Windows.Forms.TextBox
    Public WithEvents txtNumberSent As System.Windows.Forms.TextBox
    Public WithEvents txtFailed As System.Windows.Forms.TextBox
    Public WithEvents txtWaiting As System.Windows.Forms.TextBox
    Public WithEvents txtSent As System.Windows.Forms.TextBox
    Public WithEvents chkHideOldest As System.Windows.Forms.CheckBox
    Public WithEvents lblCheckProcessServer As System.Windows.Forms.Label
    Public WithEvents lblSMSMessages As System.Windows.Forms.Label
    Public WithEvents lblLastUpdated As System.Windows.Forms.Label
    Public WithEvents lblOldestCase As System.Windows.Forms.Label
    Public WithEvents lblOutstandingAll As System.Windows.Forms.Label
    Public WithEvents lblAttentionHour As System.Windows.Forms.Label
    Public WithEvents lblOutstandingHour As System.Windows.Forms.Label
    Public WithEvents lblProcessedHour As System.Windows.Forms.Label
    Public WithEvents lblAttentionToday As System.Windows.Forms.Label
    Public WithEvents lblOutstandingToday As System.Windows.Forms.Label
    Public WithEvents lblProcessedToday As System.Windows.Forms.Label
    Public WithEvents tmrSwitchStats As System.Windows.Forms.Timer
    Public WithEvents lblReceivedHour As System.Windows.Forms.Label
    Public WithEvents lblHour As System.Windows.Forms.Label
    Public WithEvents lblReceivedToday As System.Windows.Forms.Label
    Public WithEvents lblToday As System.Windows.Forms.Label
    Public WithEvents lblAttentionWeek As System.Windows.Forms.Label
    Public WithEvents lblOutstandingWeek As System.Windows.Forms.Label
    Public WithEvents lblProcessedWeek As System.Windows.Forms.Label
    Public WithEvents lblOutstanding As System.Windows.Forms.Label
    Public WithEvents lblProcessed As System.Windows.Forms.Label
    Public WithEvents lblReceived As System.Windows.Forms.Label
    Public WithEvents lblWeek As System.Windows.Forms.Label
    Public WithEvents lblReceivedWeek As System.Windows.Forms.Label
    Public WithEvents lblAttention As System.Windows.Forms.Label
    Friend WithEvents PDFViewer As AxAcroPDFLib.AxAcroPDF
    Public WithEvents tmrNextPage As System.Windows.Forms.Timer
#End Region
End Class
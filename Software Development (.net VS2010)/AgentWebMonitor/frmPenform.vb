Imports CommonLibrary
Imports System.IO

Friend Class frmPenform
    Inherits System.Windows.Forms.Form

    Dim PDFFile As String = "H:\Requests\39042_AgentWebMonitor\Ops Announcement Screen Slides.pdf"
    Dim SlideDisplayTimeSeconds As Integer = 5

    Private Sub GetAgentWeb()
        Dim StartOfWeek As DateTime = DateTime.Today.AddDays(-Weekday(DateTime.Today, FirstDayOfWeek.System)) ' First Sunday of week
        Dim TotalReceivedToday As Integer, TotalReceivedHour As Integer, TotalOutstanding As Integer, TotalProcessed As Integer, TotalProcessedHour As Integer, TotalProcessedToday As Integer, TotalReceivedYesterday As Integer, Attention As Integer
        Dim AgentWeb As New DataTable

        Try
            lblCheckProcessServer.Visible = False

            LoadDataTable("opsWallboard", "SELECT p._createdDate, p.status, p.statusDate " & _
                                          "FROM PenForm AS p " & _
                                          "WHERE p._createdDate >= " & "'" & StartOfWeek.ToString("yyyy-MM-dd") & "'", AgentWeb, False)

            For Each PenForm As DataRow In AgentWeb.Rows

                ' Received yesterday
                If Weekday(PenForm("_createddate")) = Weekday(System.DateTime.FromOADate(Now.ToOADate - 1)) Then TotalReceivedYesterday += 1

                ' Received today and current hour
                If Weekday(PenForm("_createddate")) = Weekday(DateTime.Today) Then
                    If Hour(PenForm("_createddate")) = Hour(DateTime.Today) Then TotalReceivedHour += 1
                    TotalReceivedToday += 1
                End If

                If Trim(PenForm("status")) <> "U" And Trim(PenForm("status")) <> "D" And Trim(PenForm("status")) <> "DE" And Trim(PenForm("status")) <> "DU" Then
                    TotalOutstanding += 1
                Else
                    TotalProcessed += 1
                End If

                ' Processed in current day & hour
                If Not IsDBNull(PenForm("statusDate")) AndAlso Weekday(PenForm("statusDate")) = Weekday(DateTime.Today) Then
                    If Hour(PenForm("statusDate")) = Hour(DateTime.Today) Then
                        TotalProcessedHour += 1
                    End If
                    TotalProcessedToday += 1
                End If

                ' cases requiring attention
                If PenForm("status").ToString.IndexOf("?") > -1 Then Attention += 1
            Next PenForm

            lblReceivedWeek.Text = AgentWeb.Rows.Count.ToString
            lblReceivedToday.Text = TotalReceivedToday.ToString
            lblReceivedHour.Text = TotalReceivedHour.ToString

            lblOutstandingWeek.Text = TotalOutstanding.ToString
            lblOutstandingToday.Text = TotalOutstanding.ToString
            lblOutstandingHour.Text = TotalOutstanding.ToString

            lblAttentionWeek.Text = Attention.ToString
            lblAttentionToday.Text = Attention.ToString
            lblAttentionHour.Text = Attention.ToString

            lblProcessedWeek.Text = TotalProcessed.ToString
            lblProcessedToday.Text = TotalProcessedToday.ToString
            lblProcessedHour.Text = TotalProcessedHour.ToString

            Dim AgentWebPenForm() As Object = GetSQLResultsArray("OpsWallboard", "SELECT COUNT(*) " & _
                                                                                 "     , MIN(p._createdDate) " & _
                                                                                 "     , CAST(MIN(CASE WHEN p.status = 'N' THEN p._createdDate END) AS DATETIME) " & _
                                                                                 "FROM penform AS p " & _
                                                                                 "WHERE p.status NOT IN ('U','D','DE','DU')")

            lblOutstandingAll.Text = AgentWebPenForm(0).ToString

            lblOldestCase.Text = "Oldest Outstanding Case " & AgentWebPenForm(1)

            ' now put a warning if the oldest penform with a status of N is over 1 hour old
            If DateTime.Now.AddHours(-1) > AgentWebPenForm(2) Then
                lblCheckProcessServer.Visible = True
                lblCheckProcessServer.Text = "Check Process Server - Penform " & AgentWebPenForm(2)
            Else
                lblCheckProcessServer.Visible = False
            End If

            txtNumberSent.Text = GetSQLResults("OpsWallboard", "SELECT COUNT(*) FROM Message WHERE status = 'S' AND senderDate >= '" & Now.ToString("yyyy-MM-dd") & "'")

            txtNumberWaiting.Text = GetSQLResults("OpsWallboard", "SELECT COUNT(*) FROM Message WHERE status = 'W'")

            txtNumberFailed.Text = GetSQLResults("OpsWallboard", "SELECT COUNT(*) FROM Message WHERE status = 'F'")

            lblLastUpdated.Text = "Last Updated " & Today & " " & TimeOfDay

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub chkHideOldest_CheckStateChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles chkHideOldest.CheckStateChanged
        Try
            lblOldestCase.Visible = chkHideOldest.CheckState

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub tmrSwitchStats_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tmrSwitchStats.Tick
        Try
            If PDFViewer.Visible Then
                GetAgentWeb()
                PDFViewer.Visible = False
            Else
                'http://stackoverflow.com/questions/12750725/can-i-hide-the-adobe-floating-toolbar-when-showing-a-pdf-in-browser
                PDFViewer.src = PDFFile & "#toolbar=0&navpanes=0&scrollbar=0"
                PDFViewer.Visible = True
                PDFViewer.BringToFront()
                PDFViewer.gotoFirstPage()
            End If

            tmrNextPage.Enabled = PDFViewer.Visible

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub tmrNextPage_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles tmrNextPage.Tick
        Try
            PDFViewer.gotoNextPage()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmPenform_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Try
            tmrSwitchStats.Interval = GetNoOfPagesPDF(PDFFile) * SlideDisplayTimeSeconds * 1000
            tmrNextPage.Interval = SlideDisplayTimeSeconds * 1000

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmPenform_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        Try
            GetAgentWeb()

            PDFViewer.gotoFirstPage()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function GetNoOfPagesPDF(FileName As String) As Integer
        Try
            Dim result As Integer = 0
            Dim fs As FileStream = New FileStream(FileName, FileMode.Open, FileAccess.Read)
            Dim r As StreamReader = New StreamReader(fs)
            Dim pdfText As String = r.ReadToEnd()

            fs.Close()
            r.Close()

            Dim regx As System.Text.RegularExpressions.Regex = New System.Text.RegularExpressions.Regex("/Type\s*/Page[^s]")
            Dim matches As System.Text.RegularExpressions.MatchCollection = regx.Matches(pdfText)
            result = matches.Count
            Return result

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function
End Class
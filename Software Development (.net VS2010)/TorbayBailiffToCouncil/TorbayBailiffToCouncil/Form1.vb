﻿Imports System.Configuration
Imports System.Xml.Schema
Imports System.IO

Public Class Form1
    Dim xml_valid As Boolean = True
    Dim auditFile As String = ""
    Dim error_no As Integer = 0
    Dim case_no As Integer = 0
    Dim prod_run As Boolean = False
    Dim fileDate As Date = Now
    Dim street1, street2, street3, town, city, postcode As String
    Dim nullDate As Date = CDate("Jan 1, 1900")
    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        run_report()
    End Sub
    Private Sub run_report()
       
        ConnectDb2("DebtRecovery")
        'ConnectDb2Fees("StudentLoans")
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If
        Dim filename As String = "RossendalesToTorbay" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".xml"
        Dim activity_file As String = "\\ross-helm-fp001\Rossendales Shared\Torbay XML\" & filename
        auditFile = "\\ross-helm-fp001\Rossendales Shared\Torbay XML\Bailiff2Council_audit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        If Not prod_run Then
            MsgBox("Test run only")
            activity_file = "C:\AAtemp\" & filename
            auditFile = "C:\AAtemp\Torbay_Activity_audit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        End If

        Dim messageNo As Integer = 0
        Dim messageValue As Decimal = 0
        write_audit("report started at " & Now, False)
        Dim writer As New Xml.XmlTextWriter(activity_file, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)
        writer.Formatting = Xml.Formatting.Indented
        writer.WriteStartElement("message")
        writer.WriteStartElement("header")
        writer.WriteStartElement("partner")
        writer.WriteStartElement("partnerto")
        writer.WriteElementString("partnerid", "Torbay")
        writer.WriteElementString("partnerrole", "CREDITOR")
        writer.WriteEndElement()  'partnerto

        writer.WriteStartElement("partnerfrom")
        writer.WriteElementString("partnerid", 1)
        writer.WriteElementString("partnerrole", "BAILIFF")
        writer.WriteEndElement()  'partnerfrom

        writer.WriteEndElement()  'partner

        writer.WriteStartElement("batchdetails")

        writer.WriteElementString("batchno", Format(Now, "yyyyMMdd"))
        writer.WriteElementString("batchdate", Format(fileDate, "yyyy-MM-dd") & "T" & Format(fileDate, "HH:mm:ss"))
        writer.WriteElementString("messagetype", "UPDATE")
        writer.WriteEndElement()  'batchdetails
        writer.WriteEndElement()  'header
       

        Dim startDate As Date

        Dim endDate As Date
        If Weekday(Now) = 6 Then   'Friday
            endDate = Now
        ElseIf Weekday(Now) = 7 Then
            endDate = DateAdd(DateInterval.Day, -1, Now)
        Else
            endDate = DateAdd(DateInterval.Day, -Weekday(Now) - 1, Now)
        End If

        endDate = CDate(Format(endDate, "MMM d, yyyy" & " 00:00:00"))
        startDate = DateAdd(DateInterval.Day, -7, endDate)

        Dim RTDStartDate As Date = CDate("Jun 22, 2017")

        Dim clientID As Integer = 1417
        If Not prod_run Then
            'clientID = 24
            'startDate = CDate("2017-12-22")
            ' endDate = Now
            'RTDStartDate = CDate("mar 1, 2017")
        End If
        Dim clientCode As String = "Torbay"

        'first get PIF cases

        Dim debt_dt As New DataTable

        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref" & _
                      " from Debtor D, clientScheme CS, scheme S" & _
                      " where return_Date >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                          " and return_Date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                      " and D.clientschemeID = CS._rowID" & _
                      " and CS.schemeID = S._rowID" & _
                      " and work_type in (16,20)" & _
                      " and CS.clientID = " & clientID & _
                      " and status_open_closed = 'C'" & _
                      " and D.status = 'S'" & _
                      " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                      " order by D._rowid", debt_dt, False)

        For Each debtRow In debt_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "1000")
            'get last payment date
            Dim pay_dt As New DataTable

            LoadDataTable2("DebtRecovery", "SELECT P.status_date, P.split_debt, R.invoiceID from payment P, remit R " & _
                                               " WHERE DebtorID = " & debtorID & _
                                               " and P.status_remitID =R._rowID" & _
                                               " order by P.Date desc", pay_dt, False)
            Dim Statusdate As Date
            Dim payAmt As Decimal = 0
            Dim remitID As Integer = 0
            Try
                Statusdate = pay_dt.Rows(0).Item(0)
                payAmt = pay_dt.Rows(0).Item(1)
                remitID = pay_dt.Rows(0).Item(2)
            Catch ex As Exception
                Statusdate = nullDate
            End Try
            If Statusdate <> nullDate Then
                writer.WriteElementString("messagetime", Format(Statusdate, "yyyy-MM-dd") & "T" & Format(Statusdate, "HH:mm:ss"))
            End If
            writer.WriteElementString("messagetext", "Paid in full")
            writer.WriteElementString("messagevalue", payAmt)
            messageValue += payAmt
            writer.WriteElementString("remittanceadviceref", remitID)
            writer.WriteEndElement()  'themessage
        Next  'debtorID

        'get charge backs
        Dim debt2_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, P.status_date, P.amount, R.invoiceID" & _
                     " from Debtor D, clientScheme CS, scheme S, payment P, remit R" & _
                     " where P.status_Date >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and P.status_Date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and D.clientschemeID = CS._rowID" & _
                       " and P.status_remitID =R._rowID" & _
                     " and CS.schemeID = S._rowID" & _
                     " and work_type in (16,20)" & _
                     " and CS.clientID = " & clientID & _
                     " and P.debtorID = D._rowID" & _
                     " and P.status = 'R'" & _
                     " and P.amount < 0" & _
                     " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                     " order by D._rowid", debt2_dt, False)

        For Each debtRow In debt2_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim StatusDate As Date = debtRow(2)
            Dim payAmt As Decimal = debtRow(3)
            Dim remitID As Integer = debtRow(4)
            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "1050")

            writer.WriteElementString("messagetime", Format(StatusDate, "yyyy-MM-dd") & "T" & Format(StatusDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", "ChargeBack")
            writer.WriteElementString("messagevalue", payAmt)
            messageValue += payAmt
            writer.WriteElementString("remittanceadviceref", remitID)
            writer.WriteEndElement()  'themessage
        Next  'debtorID

        'No Trace
        Dim debt3_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, D.return_date" & _
                     " from Debtor D, clientScheme CS, scheme S, codereturns CR" & _
                     " where return_Date >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and return_Date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and D.clientschemeID = CS._rowID" & _
                     " and CS.schemeID = S._rowID" & _
                     " and work_type in (16,20)" & _
                     " and CS.clientID = " & clientID & _
                     " and D.return_codeID = CR._rowID" & _
                     " and D.status = 'C'" & _
                     " and D.return_codeID = 76" & _
                     " and D.status_open_closed = 'C'" & _
                     " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                     " order by D._rowid", debt3_dt, False)

        For Each debtRow In debt3_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim returnDate As Date = debtRow(2)

            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "2000")

            writer.WriteElementString("messagetime", Format(returnDate, "yyyy-MM-dd") & "T" & Format(returnDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", "No Trace")
            writer.WriteElementString("messagevalue", 0)
            writer.WriteElementString("remittanceadviceref", "")
            writer.WriteEndElement()  'themessage
        Next  'debtorID

        'part paid no more expected
        Dim debt4_dt As New DataTable

        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, P.status_Date, P.amount, R.invoiceID" & _
                      " from Debtor D, clientScheme CS, scheme S, payment P, remit R" & _
                      " where P.status_date >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                      " and P.status_date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                      " and D.clientschemeID = CS._rowID" & _
                       " and P.status_remitID =R._rowID" & _
                      " and P.debtorID = D._rowID " & _
                      " and CS.schemeID = S._rowID" & _
                      " and work_type in (16,20)" & _
                      " and CS.clientID = " & clientID & _
                      " and status_open_closed = 'C'" & _
                      " and D.status <> 'S'" & _
                      " and P.status = 'R'" & _
                      " and P.amount > 0" & _
                      " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                      " order by D._rowid", debt4_dt, False)

        For Each debtRow In debt4_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim statusDate As Date = debtRow(2)
            Dim payAmt As Decimal = debtRow(3)
            Dim remitID As Integer = debtRow(4)
            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "1100")

            writer.WriteElementString("messagetime", Format(statusDate, "yyyy-MM-dd") & "T" & Format(statusDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", "Part paid no more expected")
            writer.WriteElementString("messagevalue", payAmt)
            messageValue += payAmt
            writer.WriteElementString("remittanceadviceref", remitID)
            writer.WriteEndElement()  'themessage
        Next

        'part paid more expected
        Dim debt5_dt As New DataTable

        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, P.status_date, P.amount, R.invoiceID" & _
                      " from Debtor D, clientScheme CS, scheme S, payment P, remit R" & _
                      " where P.status_date >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                      " and P.status_date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                      " and D.clientschemeID = CS._rowID" & _
                       " and P.status_remitID =R._rowID" & _
                      " and P.debtorID = D._rowID " & _
                      " and CS.schemeID = S._rowID" & _
                      " and work_type in (16,20)" & _
                      " and CS.clientID = " & clientID & _
                      " and status_open_closed = 'O'" & _
                      " and P.status = 'R'" & _
                      " and P.amount > 0" & _
                      " and debt_balance > 0" & _
                      " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                      " order by D._rowid", debt5_dt, False)

        For Each debtRow In debt5_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim statusDate As Date = debtRow(2)
            Dim payAmt As Decimal = debtRow(3)
            Dim remitID As Integer = debtRow(4)
            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "1200")

            writer.WriteElementString("messagetime", Format(statusDate, "yyyy-MM-dd") & "T" & Format(statusDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", "Part paid more expected")
            writer.WriteElementString("messagevalue", payAmt)
            messageValue += payAmt
            writer.WriteElementString("remittanceadviceref", remitID)
            writer.WriteEndElement()  'themessage
        Next

        'No assets
        Dim debt6_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, D.return_date, CR.reason_short" & _
                     " from Debtor D, clientScheme CS, scheme S, codereturns CR" & _
                     " where return_Date >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and return_Date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and D.clientschemeID = CS._rowID" & _
                     " and CS.schemeID = S._rowID" & _
                     " and work_type in (16,20)" & _
                     " and CS.clientID = " & clientID & _
                     " and D.return_codeID = CR._rowID" & _
                     " and D.status = 'C'" & _
                     " and fee_category in (3)" & _
                     " and D.status_open_closed = 'C'" & _
                     " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                     " order by D._rowid", debt6_dt, False)

        For Each debtRow In debt6_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim returnDate As Date = debtRow(2)
            Dim returnReason As String = debtRow(3)
            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "1300")

            writer.WriteElementString("messagetime", Format(returnDate, "yyyy-MM-dd") & "T" & Format(returnDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", returnReason)
            writer.WriteElementString("messagevalue", 0)
            writer.WriteElementString("remittanceadviceref", "")
            writer.WriteEndElement()  'themessage
        Next  'debtorID

        'gone away
        Dim debt7_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, D.return_date, CR.reason_short" & _
                     " from Debtor D, clientScheme CS, scheme S, codereturns CR" & _
                     " where return_Date >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and return_Date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and D.clientschemeID = CS._rowID" & _
                     " and CS.schemeID = S._rowID" & _
                     " and work_type in (16,20)" & _
                     " and CS.clientID = " & clientID & _
                     " and D.return_codeID = CR._rowID" & _
                     " and D.status = 'C'" & _
                     " and fee_category in (1)" & _
                     " and D.status_open_closed = 'C'" & _
                     " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                     " order by D._rowid", debt7_dt, False)

        For Each debtRow In debt7_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim returnDate As Date = debtRow(2)
            Dim returnReason As String = debtRow(3)
            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "5100")

            writer.WriteElementString("messagetime", Format(returnDate, "yyyy-MM-dd") & "T" & Format(returnDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", returnReason)
            writer.WriteElementString("messagevalue", 0)
            writer.WriteElementString("remittanceadviceref", "")
            writer.WriteEndElement()  'themessage
        Next  'debtorID

        'arrangement in place
        Dim debt8_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, N._createdDate" & _
                     " from Debtor D, clientScheme CS, scheme S, note N" & _
                     " where N._createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and N._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and D.clientschemeID = CS._rowID" & _
                     " and CS.schemeID = S._rowID" & _
                     " and work_type in (16,20)" & _
                     " and CS.clientID = " & clientID & _
                     " and N.debtorID = D._rowID" & _
                     " and N.type in ('Arrangement','Payment plan')" & _
                     " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                     " order by D._rowid", debt8_dt, False)

        For Each debtRow In debt8_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim ArrangementDate As Date = debtRow(2)
            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "3000")

            writer.WriteElementString("messagetime", Format(ArrangementDate, "yyyy-MM-dd") & "T" & Format(ArrangementDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", "Arrangement")
            writer.WriteElementString("messagevalue", 0)
            writer.WriteElementString("remittanceadviceref", "")
            writer.WriteEndElement()  'themessage
        Next  'debtorID

        'bailiff fees
        Dim debt9_dt As New DataTable

        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, P.status_date, P.split_fees, P.split_van, P.split_other, R.invoiceID" & _
                      " from Debtor D, clientScheme CS, scheme S, payment P, remit R" & _
                      " where P.status_date >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                      " and P.status_date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                      " and D.clientschemeID = CS._rowID" & _
                        " and P.status_remitID =R._rowID" & _
                      " and P.debtorID = D._rowID " & _
                      " and CS.schemeID = S._rowID" & _
                      " and work_type in (16,20)" & _
                      " and CS.clientID = " & clientID & _
                      " and P.status = 'R'" & _
                      " and P.split_fees + P.split_van + P.split_other > 0" & _
                      " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                      " order by D._rowid", debt9_dt, False)

        For Each debtRow In debt9_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim statusDate As Date = debtRow(2)
            Dim CompFeeAmt As Decimal = debtRow(3)
            Dim EnfFeeAmt As Decimal = debtRow(4)
            Dim otherFeeAmt As Decimal = debtRow(5)
            Dim remitID As Integer = debtRow(6)
            If CompFeeAmt > 0 Then
                writer.WriteStartElement("themessage")
                writer.WriteElementString("pcnno", clientRef)
                writer.WriteElementString("messagecode", "3300")

                writer.WriteElementString("messagetime", Format(statusDate, "yyyy-MM-dd") & "T" & Format(statusDate, "HH:mm:ss"))

                writer.WriteElementString("messagetext", "Compliance Fee")
                writer.WriteElementString("messagevalue", CompFeeAmt)
                writer.WriteElementString("remittanceadviceref", remitID)
                messageValue += CompFeeAmt
                writer.WriteEndElement()  'themessage
            End If
            If EnfFeeAmt > 0 Then
                writer.WriteStartElement("themessage")
                writer.WriteElementString("pcnno", clientRef)
                writer.WriteElementString("messagecode", "3300")

                writer.WriteElementString("messagetime", Format(statusDate, "yyyy-MM-dd") & "T" & Format(statusDate, "HH:mm:ss"))

                writer.WriteElementString("messagetext", "Enforcement Fee")
                writer.WriteElementString("messagevalue", EnfFeeAmt)
                writer.WriteElementString("remittanceadviceref", remitID)
                messageValue += EnfFeeAmt
                writer.WriteEndElement()  'themessage
            End If
            If otherFeeAmt > 0 Then
                writer.WriteStartElement("themessage")
                writer.WriteElementString("pcnno", clientRef)
                writer.WriteElementString("messagecode", "3300")

                writer.WriteElementString("messagetime", Format(statusDate, "yyyy-MM-dd") & "T" & Format(statusDate, "HH:mm:ss"))

                writer.WriteElementString("messagetext", "Admin Fee")
                writer.WriteElementString("messagevalue", otherFeeAmt)
                writer.WriteElementString("remittanceadviceref", remitID)
                messageValue += otherFeeAmt
                writer.WriteEndElement()  'themessage
            End If
        Next

        'bailiff activity
        Dim debt10_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, D.laststagedate, ST.name" & _
                     " from Debtor D, clientScheme CS, scheme S, stage ST" & _
                     " where D.laststagedate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and D.laststagedate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and D.clientschemeID = CS._rowID" & _
                     " and CS.schemeID = S._rowID" & _
                     " and work_type in (16,20)" & _
                     " and CS.clientID = " & clientID & _
                     " and D.last_stageID = ST._rowID" & _
                     " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                     " order by D._rowid", debt10_dt, False)

        For Each debtRow In debt10_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim StageDate As Date = debtRow(2)
            Dim stageName As String = debtRow(3)
            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "3500")

            writer.WriteElementString("messagetime", Format(StageDate, "yyyy-MM-dd") & "T" & Format(StageDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", stageName & " Stage")
            writer.WriteElementString("messagevalue", 0)
            writer.WriteElementString("remittanceadviceref", "")
            writer.WriteEndElement()  'themessage
        Next  'debtorID

        'returned as requested
        Dim debt11_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, D.return_date, CR.reason_short" & _
                     " from Debtor D, clientScheme CS, scheme S, codereturns CR" & _
                     " where return_Date >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and return_Date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and D.clientschemeID = CS._rowID" & _
                     " and CS.schemeID = S._rowID" & _
                     " and work_type in (16,20)" & _
                     " and CS.clientID = " & clientID & _
                     " and D.return_codeID = CR._rowID" & _
                     " and D.status = 'C'" & _
                     " and fee_category in (2,4)" & _
                     " and not(D.return_codeID in (68,58,107,3,63,69,13,72))" & _
                     " and D.status_open_closed = 'C'" & _
                      " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                     " order by D._rowid", debt11_dt, False)

        For Each debtRow In debt11_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim returnDate As Date = debtRow(2)
            Dim returnReason As String = debtRow(3)
            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "4000")

            writer.WriteElementString("messagetime", Format(returnDate, "yyyy-MM-dd") & "T" & Format(returnDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", returnReason)
            writer.WriteElementString("messagevalue", 0)
            writer.WriteElementString("remittanceadviceref", "")
            writer.WriteEndElement()  'themessage
        Next  'debtorID


        'unenforceable
        Dim debt12_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, D.return_date, CR.reason_short" & _
                     " from Debtor D, clientScheme CS, scheme S, codereturns CR" & _
                     " where return_Date >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and return_Date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and D.clientschemeID = CS._rowID" & _
                     " and CS.schemeID = S._rowID" & _
                     " and work_type in (16,20)" & _
                     " and CS.clientID = " & clientID & _
                     " and D.return_codeID = CR._rowID" & _
                     " and D.status = 'C'" & _
                     " and D.return_codeID in (68,58,107,3,63,69,13,72)" & _
                     " and D.status_open_closed = 'C'" & _
                     " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                     " order by D._rowid", debt12_dt, False)

        For Each debtRow In debt12_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim returnDate As Date = debtRow(2)
            Dim returnReason As String = debtRow(3)
            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "5000")

            writer.WriteElementString("messagetime", Format(returnDate, "yyyy-MM-dd") & "T" & Format(returnDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", returnReason)
            writer.WriteElementString("messagevalue", 0)
            writer.WriteElementString("remittanceadviceref", "")
            writer.WriteEndElement()  'themessage
        Next  'debtorID

        'Sale or disposal
        Dim debt13_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, F._createdDate" & _
                     " from Debtor D, clientScheme CS, scheme S, fee F" & _
                     " where F._createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and F._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and D.clientschemeID = CS._rowID" & _
                     " and CS.schemeID = S._rowID" & _
                     " and work_type in (16,20)" & _
                     " and CS.clientID = " & clientID & _
                     " and D._rowID = F.DebtorID" & _
                    " and F.type = 'Sale or disposal'" & _
                    " and F.fee_amount > 0" & _
                   " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                     " order by D._rowid", debt13_dt, False)

        For Each debtRow In debt13_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim feeDate As Date = debtRow(2)

            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "3100")

            writer.WriteElementString("messagetime", Format(feeDate, "yyyy-MM-dd") & "T" & Format(feeDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", "Sale or disposal")
            writer.WriteElementString("messagevalue", 0)
            writer.WriteElementString("remittanceadviceref", "")
            writer.WriteEndElement()  'themessage
        Next  'debtorID

        'letter sent
        Dim debt14_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, N._createdDate,N.text" & _
                     " from Debtor D, clientScheme CS, scheme S, note N" & _
                     " where N._createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and N._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and D.clientschemeID = CS._rowID" & _
                     " and CS.schemeID = S._rowID" & _
                     " and work_type in (16,20)" & _
                     " and CS.clientID = " & clientID & _
                     " and N.debtorID = D._rowID" & _
                     " and N.type = 'Letter'" & _
                     " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                     " order by D._rowid", debt14_dt, False)

        For Each debtRow In debt14_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim LetterDate As Date = debtRow(2)
            Dim notetext As String = debtRow(3)
            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "7000")

            writer.WriteElementString("messagetime", Format(LetterDate, "yyyy-MM-dd") & "T" & Format(LetterDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", notetext)
            writer.WriteElementString("messagevalue", 0)
            writer.WriteElementString("remittanceadviceref", "")
            writer.WriteEndElement()  'themessage
        Next  'debtorID

        'visits
        Dim debt15_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, V.date_visited" & _
                     " from Debtor D, clientScheme CS, scheme S, visit V" & _
                     " where V.date_visited >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and V.date_visited < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and D.clientschemeID = CS._rowID" & _
                     " and CS.schemeID = S._rowID" & _
                     " and work_type in (16,20)" & _
                     " and CS.clientID = " & clientID & _
                     " and V.debtorID = D._rowID" & _
                     " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                     " order by D._rowid", debt15_dt, False)

        For Each debtRow In debt15_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim visitDate As Date = debtRow(2)

            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "7100")

            writer.WriteElementString("messagetime", Format(visitDate, "yyyy-MM-dd") & "T" & Format(visitDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", "Visit")
            writer.WriteElementString("messagevalue", 0)
            writer.WriteElementString("remittanceadviceref", "")
            writer.WriteEndElement()  'themessage
        Next  'debtorID

        'cases received
        Dim debt16_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, D._createdDate, D.client_batch" & _
                     " from Debtor D, clientScheme CS, scheme S" & _
                     " where D._createddate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and D._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and D.clientschemeID = CS._rowID" & _
                     " and CS.schemeID = S._rowID" & _
                     " and work_type in (16,20)" & _
                     " and CS.clientID = " & clientID & _
                      " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                     " order by D._rowid", debt16_dt, False)

        For Each debtRow In debt16_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim loadDate As Date = debtRow(2)
            Dim batchNo As String = ""
            Try
                batchNo = debtRow(3)
            Catch ex As Exception

            End Try

            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "9999")

            writer.WriteElementString("messagetime", Format(loadDate, "yyyy-MM-dd") & "T" & Format(loadDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", batchNo)
            writer.WriteElementString("messagevalue", 0)
            writer.WriteElementString("remittanceadviceref", "")
            writer.WriteEndElement()  'themessage
        Next  'debtorID

        'new addresses
        Dim debt17_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, N._createdDate" & _
                     " from Debtor D, clientScheme CS, scheme S, note N" & _
                     " where N._createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                         " and N._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and D.clientschemeID = CS._rowID" & _
                     " and CS.schemeID = S._rowID" & _
                     " and work_type in (16,20)" & _
                     " and CS.clientID = " & clientID & _
                     " and N.debtorID = D._rowID" & _
                     " and N.type = 'Address'" & _
                     " and N.text like 'changed from:%'" & _
                     " and D._createdDate >= '" & Format(RTDStartDate, "yyyy-MM-dd") & "'" & _
                     " order by D._rowid", debt17_dt, False)

        For Each debtRow In debt17_dt.Rows
            messageNo += 1
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim AddressChangeDate As Date = debtRow(2)
            writer.WriteStartElement("themessage")
            writer.WriteElementString("pcnno", clientRef)
            writer.WriteElementString("messagecode", "3000")

            writer.WriteElementString("messagetime", Format(AddressChangeDate, "yyyy-MM-dd") & "T" & Format(AddressChangeDate, "HH:mm:ss"))

            writer.WriteElementString("messagetext", "Address Change")
            writer.WriteElementString("messagevalue", 0)
            writer.WriteElementString("remittanceadviceref", "")
            writer.WriteEndElement()  'themessage
        Next  'debtorID


        writer.WriteStartElement("trailer")
        writer.WriteElementString("batchitems", messageNo)
        writer.WriteElementString("batchvalue", messageValue)
        writer.WriteEndElement()  'trailer
        writer.Close()
        'validate using xsd
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(activity_file)
        myDocument.Schemas.Add("", "\\ross-helm-fp001\Rossendales Shared\vb.net\Torbay XSD\BailiffToCouncil.xsd")
        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
        write_audit(error_no & " - errors in report finished at " & Now, False)
        'If error_no = 0 Then
        '    If prod_run Then
        '        'prod file name here

        '    End If

        'End If
        Me.Close()
    End Sub
    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_audit("Error: " & e.Message, True)
            Case XmlSeverityType.Warning
                write_audit("Warning " & e.Message, True)
        End Select
    End Sub
    Private Sub write_audit(ByVal audit_message As String, ByVal errorMessage As Boolean)
        If errorMessage Then
            error_no += 1
        End If
        audit_message = audit_message & vbNewLine
        My.Computer.FileSystem.WriteAllText(auditFile, audit_message, True)
    End Sub
    Private Sub get_address(ByVal addr As String, ByVal pcode As String)
        street1 = ""
        street2 = ""
        street3 = ""
        town = ""
        city = ""
        postcode = ""
        Try
            postcode = pcode
        Catch ex As Exception

        End Try
        Dim address = ""
        Try
            address = addr
        Catch ex As Exception
            Return
        End Try
        Dim addressline As String = ""
        For idx = 1 To address.Length
            If Mid(address, idx, 1) = Chr(10) Or Mid(address, idx, 1) = Chr(13) Then
                If street1 = "" Then
                    street1 = addressline
                ElseIf street2 = "" Then
                    street2 = addressline
                ElseIf town = "" Then
                    town = addressline
                ElseIf city = "" Then
                    city = addressline
                End If
                addressline = ""
            Else
                addressline &= Mid(address, idx, 1)
            End If
        Next
        'check for postcode and shuffle town/city
        If city = postcode Then
            city = ""
        ElseIf town = postcode Then
            town = ""
        ElseIf street2 = postcode Then
            street2 = ""
        End If
        If UCase(city) = "GB" Then
            city = ""
        End If
        If UCase(town) = "GB" Then
            town = ""
        End If
        If city = "" Then
            city = town
            town = street2
            street2 = ""
        End If
        If city = "" Then
            city = town
            town = street2
            street2 = ""
        End If
        If IsNumeric(Microsoft.VisualBasic.Left(town, 1)) Then
            If street2 = "" Then
                street2 = town
                town = ""
            Else
                street3 = town
                town = ""
            End If
        End If

    End Sub
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        run_report()
    End Sub
End Class

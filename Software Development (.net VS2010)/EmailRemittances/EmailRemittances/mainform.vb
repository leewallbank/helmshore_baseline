Imports CommonLibrary
Imports System.IO

Public Class mainform
    Public ascii As New System.Text.ASCIIEncoding()
    Dim log_user As String
    Dim cl_name As String = ""
    Dim next_cl_no, next_csid As Integer
    Dim sch_name As String = ""
    Dim subject As String = ""
    Dim fromaddress As String
    Dim next_toaddress As String
    Dim body As String
    Dim att1 As String = " "
    Dim att2 As String = " "
    Dim att3 As String = " "
    Dim att4 As String = " "
    Dim att5 As String = " "
    Dim att6 As String = " "
    Dim att7 As String = " "
    Dim att8 As String = " "
    Dim att9 As String = " "
    Dim error_found As Boolean = False
    Dim csid_array(100)
    Dim csid_idx As Integer = 0
    Dim startDate, endDate As Date
    Dim audit As String
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        prod_run = False
        Dim env_str As String = ""
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try

        If env_str = "Prod" Then prod_run = True
        If Not prod_run Then
            MsgBox("Warning - test run only")
        End If

        conn_open = False
        error_path = "h:\email client remits\"
        error_file = error_path & "error_log_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"
        log_path = "h:\email client remits\"
        log_file = error_path & "log_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"
        Dim oWSH = CreateObject("WScript.Shell")
        log_user = oWSH.ExpandEnvironmentStrings("%USERNAME%")
        startDate = DateAdd(DateInterval.Day, -Weekday(Now) - 5, Now)
        endDate = DateAdd(DateInterval.Day, 6, startDate)
        Label1.Text = "Remittances reports for " & Format(startDate, "dd.MM.yyyy") & " to " & Format(endDate, "dd.MM.yyyy")

        'JUST FOR COLLECT INITIALLY
        branch = 2


        If branch = 1 Then
            Label2.Text = "Bailiff"
        ElseIf branch = 2 Then
            Label2.Text = "Collect"
        ElseIf branch = 10 Then
            Label2.Text = "Marston"
        Else
            MsgBox("Unknown Branch")
            Me.Close()
        End If

    End Sub

    Private Sub allbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allbtn.Click
        allbtn.Enabled = False
        maintainbtn.Enabled = False
        somebtn.Enabled = False
        exitbtn.Enabled = False
        send_all = True
        If MsgBox("Sending emails to ALL clients", MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton2, "Send Emails") = MsgBoxResult.Ok Then
            Dim retn_no As Integer = prepare_emails()
            If retn_no = 0 Then
                MsgBox("ALL emails sent OK")
            Else
                If retn_no = 99 Then
                    MsgBox("No emails sent")
                Else
                    MsgBox("Emails sent but see error log at h:\email client remits")
                End If
            End If
        Else
            MsgBox("No emails sent")
        End If
        allbtn.Enabled = True
        somebtn.Enabled = True
        exitbtn.Enabled = True
        maintainbtn.Enabled = True
    End Sub

    Function prepare_emails()
        Dim toaddress As String = ""
        Dim ccaddress As String
        If branch = 10 Then
            ccaddress = log_user & "@marstongroup.co.uk"
            fromaddress = "clientreports@marstongroup.co.uk"
        Else
            ccaddress = log_user & "@rossendales.com"
            fromaddress = "crystal@rossendales.com"
        End If

        cc_all = InputBox("Send a CC for emails to ", "EMAIL to send CC to. Blank out for no CC", ccaddress)
        error_file = error_path & "error_log_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"

        file_path = "O:\DebtRecovery\Archives\"

        Dim idx As Integer = 0
        Dim final_warning_given As Boolean = False
        Dim csid As Integer
        Dim cl_rows As Integer
        Dim emaildt As New DataTable
        LoadDataTable("FeesSQL", "select er_csid, er_email, er_last_date from EmailRemit" & _
                         " order by er_csid", emaildt, False)
        cl_rows = emaildt.Rows.Count
        For Each row In emaildt.Rows
            ProgressBar1.Value = idx / cl_rows * 100
            Try
                toaddress = Trim(row(1))
            Catch ex As Exception
                toaddress = ""
            End Try
            Application.DoEvents()
            csid = row(0)

            If toaddress.Length < 5 Then
                idx += 1
                Continue For
            End If
            Dim last_date_str As String = ""
            Try
                last_date_str = row(2)
            Catch
                last_date_str = " "
            End Try
            If Not send_all Then
                If Microsoft.VisualBasic.Len(Trim(last_date_str)) > 0 Then
                    Dim last_date As Date = CDate(last_date_str)
                    If Format(last_date, "yyyy-MM-dd") >= Format(endDate, "yyyy-MM-dd") Then
                        idx += 1
                        Continue For
                    End If
                End If
            End If

            If Not final_warning_given Then
                final_warning_given = True
                If send_all Then
                    If MsgBox("Send ALL emails to clients", MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton2, "-----Last chance to stop emails-----") = MsgBoxResult.Cancel Then
                        Return 99
                    End If
                Else
                    If MsgBox("Send unsent emails to clients", MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton2, "-----Last chance to stop emails-----") = MsgBoxResult.Cancel Then
                        Return 99
                    End If
                End If
            End If
            send_email(csid, toaddress)
            If idx > 5 And Not prod_run Then
                Exit For
            End If
        Next

        ProgressBar1.Value = 0
        If error_found Then
            Return 10
        Else
            If final_warning_given Then
                Return 0
            Else
                Return 99
            End If
        End If
    End Function

    Sub send_email(ByVal csid As Integer, ByVal toaddress As String)

        'get remit nos for this clientscheme
        Dim remitdt As New DataTable
        LoadDataTable("DebtRecovery", "select _rowID, date from remit" & _
                                      " where clientschemeID = " & csid & _
                                      " and date >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                                      " and date <='" & Format(endDate, "yyyy-MM-dd") & "'", remitdt, False)

        For Each remitrow In remitdt.Rows
            att1 = " "
            att2 = " "
            att3 = " "
            att4 = " "
            att5 = " "
            att6 = " "
            att7 = " "
            att8 = " "
            att9 = " "
            Dim remitDate As Date = remitrow(1)
            Dim remitNo As Integer = remitrow(0)
            Dim month As String = Format(remitDate, "MM") & "_" & Format(remitDate, "MMM")
            Dim day As String = Format(remitDate, "dd") & "_" & Format(remitDate, "ddd")
            client_document = cl_name & "_" & sch_name & "_" & remitNo
            file_name = file_path & Format(remitDate, "yyyy") & "\" & month & "\" & day & "\Remittances\" & remitNo & "\"
            'get all documents in remit folder
            For Each foundFile As String In My.Computer.FileSystem.GetFiles(
               file_name,
                Microsoft.VisualBasic.FileIO.SearchOption.SearchAllSubDirectories, "*.*")
                If att1 = " " Then
                    att1 = foundFile
                ElseIf att2 = " " Then
                    att2 = foundFile
                ElseIf att3 = " " Then
                    att3 = foundFile
                ElseIf att4 = " " Then
                    att4 = foundFile
                ElseIf att5 = " " Then
                    att5 = foundFile
                ElseIf att6 = " " Then
                    att6 = foundFile
                ElseIf att7 = " " Then
                    att7 = foundFile
                ElseIf att8 = " " Then
                    att8 = foundFile
                ElseIf att9 = " " Then
                    att9 = foundFile
                End If
            Next
            If att1 = " " Then
                Continue For
            End If
            'get client name and scheme name
            Dim csidArray As Object
            csidArray = GetSQLResultsArray("DebtRecovery", "select C.name, S.name from clientScheme CS, client C, scheme S " & _
                                      " where CS._rowID = " & csid & _
                                      " and CS.clientID = C._rowID " & _
                                      " and CS.schemeID = S._rowID")
            cl_name = csidArray(0)
            sch_name = csidArray(1)
            subject = "Remittances for " & cl_name & "-" & sch_name & "-" & remitNo & "-" & Format(remitDate, "dd.MM.yyyy")
            If Not prod_run Then
                toaddress = "JBlundell@rossendales.com"
                'toaddress = "pete_glavey@hotmail.com"
                fromaddress = "crystal@rossendales.com"
            End If
            body = "Good Morning"
            If Format(Now, "HH") > 12 Then
                body = "Good afternoon"
            End If

            body &= vbNewLine & "Please find attached remittances produced from our Onestep system." & vbNewLine
            body &= "Client - " & cl_name & vbNewLine & "Scheme - " & sch_name & vbNewLine & "Remit No - " & remitNo & vbNewLine
            body &= "Date - " & Format(remitDate, "dd/MM/yyyy") & vbNewLine

            body &= "This is an automated email address do not reply." & vbNewLine
            body &= "However in the event of a query, please quote the filename/s to enable us to quickly retrieve the file for examination" & vbNewLine
            body &= " then please contact services@rossendales.com" & vbNewLine

            If branch = 1 Then
                body = body & vbNewLine & vbNewLine & "Regards" & vbNewLine
                body = body & "Client Liaison Team" & vbNewLine & _
                        "        Rossendales" & vbNewLine & _
                        "        PO Box 324" & vbNewLine & _
                        "        Rossendale" & vbNewLine & _
                        "        BB4 0GE   " & vbNewLine & _
                        "Tel: 01706 830323" & vbNewLine

            ElseIf branch = 2 Then
                body = body & vbNewLine & vbNewLine & "Regards" & vbNewLine
                body = body & "Rossendales Collect" & vbNewLine & _
                "        Rossendales" & vbNewLine & _
                "        PO Box 324" & vbNewLine & _
                "        Rossendale" & vbNewLine & _
                "        BB4 0GE   " & vbNewLine & _
                "Tel: 0844 701 3991"
            Else
                body = body & vbNewLine & vbNewLine & "Regards" & vbNewLine
                body = body & "Marston Group Limited" & vbNewLine & _
                           "        PO Box 323" & vbNewLine & _
                           "        Rossendale" & vbNewLine & _
                           "        BB4 0GD   " & vbNewLine & _
                           "Tel: 0845 076 6262"
            End If
            If email(csid, toaddress, fromaddress, subject, body, att1, att2, att3, att4, att5, att6, att7, att8, att9) = 0 Then
                Try
                    If prod_run Then
                        upd_txt = "update EmailRemit set er_last_date = '" & Format(Now, "yyyy-MM-dd") & "'" & _
                            " where er_csid = " & csid
                        update_sql(upd_txt)
                    End If
                Catch ex As Exception
                    error_message = "Last date not updated for client scheme " & _
                    cl_name & " " & sch_name & vbNewLine
                    write_error()
                    MsgBox("Last date not set for client scheme " & cl_name & " " & sch_name)
                    error_found = True
                End Try
            Else
                error_found = True
            End If
        Next
    End Sub

    Private Sub maintainbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles maintainbtn.Click
        allbtn.Enabled = False
        somebtn.Enabled = False
        exitbtn.Enabled = False
        maintainbtn.Enabled = False
        clientfrm.ShowDialog()
        allbtn.Enabled = True
        somebtn.Enabled = True
        exitbtn.Enabled = True
        maintainbtn.Enabled = True
    End Sub

    Private Sub somebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles somebtn.Click
        allbtn.Enabled = False
        somebtn.Enabled = False
        exitbtn.Enabled = False
        maintainbtn.Enabled = False
        send_all = False
        Dim retn_no As Integer = prepare_emails()
        If retn_no = 0 Then
            MsgBox("emails sent OK")
        Else
            If retn_no = 99 Then
                MsgBox("No emails sent")
            Else
                MsgBox("Emails sent but see error log at h:\email client remits")
            End If
        End If
        allbtn.Enabled = True
        somebtn.Enabled = True
        exitbtn.Enabled = True
        maintainbtn.Enabled = True
        Me.Close()
    End Sub


    Private Sub repbtn_Click(sender As System.Object, e As System.EventArgs) Handles repbtn.Click
        'write report for remits sent this week
        repbtn.Enabled = False
        exitbtn.Enabled = False
        maintainbtn.Enabled = False
        somebtn.Enabled = False
        allbtn.Enabled = False

        startDate = DateAdd(DateInterval.Day, -Weekday(Now) + 2, Now)
        endDate = DateAdd(DateInterval.Day, 6, startDate)
        Dim emaildt As New DataTable
        LoadDataTable("FeesSQL", "select count(distinct er_csid), er_last_date from EmailRemit" & _
                      " where er_last_date >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                      " and er_last_date <='" & Format(endDate, "yyyy-MM-dd") & "'" & _
                         " group by er_last_date", emaildt, False)
        Dim csid_rows As Integer = emaildt.Rows.Count
        csid_idx = 0
        For Each row In emaildt.Rows
            ProgressBar1.Value = (csid_idx / csid_rows) * 100
            Application.DoEvents()
            Dim emailCount As Integer = row(0)
            Dim sentDate As Date = row(1)
            audit &= "Date sent:" & Format(sentDate, "dd/MM/yyyy") & " number sent - " & emailcount & vbNewLine
            audit &= vbNewLine
        Next
        'write out file

        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "TXT files |*.txt"
            .DefaultExt = ".txt"
            .OverwritePrompt = True
            .FileName = "Email Remittances_" & Format(Now, "yyyy-MM-dd") & ".txt"
        End With
        If Not SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            MsgBox("report not saved ")
            Return
        End If

        Dim FileName As String = Path.GetFileName(SaveFileDialog1.FileName)
        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, audit, False, ascii)
        MsgBox("Report saved")
        Me.Close()
    End Sub
End Class

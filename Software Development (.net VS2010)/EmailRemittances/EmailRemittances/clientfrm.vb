Imports CommonLibrary
Public Class clientfrm


    Private Sub clientfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'populate datagrid
        DataGridView1.Rows.Clear()
        'get clients
        Dim cl_no As Integer
        Dim idx As Integer
        Dim client_name, scheme_name, email, cc1, cc2, last_date_str As String
        Dim csid_dt As New DataTable
        LoadDataTable("DebtRecovery", "select CS._rowID, C.name, S.name, C._rowID, S._rowID" & _
                            " from clientscheme CS, scheme S, client C, remit R" & _
                            " where CS.branchID = 2" & _
                            " and CS.clientID = C._rowID" & _
                            " and R.clientschemeID = CS._rowID" & _
                            " and not(CS.clientID in (1,2,24))" & _
                            " and CS.schemeID = S._rowID" & _
                            " group by 1" & _
                            " order by 2,3", csid_dt, False)
        idx = 0
        For Each row In csid_dt.Rows
            Try
                mainform.ProgressBar1.Value = idx / csid_dt.Rows.Count * 100
            Catch ex As Exception
                idx = 0
            End Try
            idx += 1
            Application.DoEvents()
            cl_no = row(3)
            client_name = row(1)
            'get csid
            Dim sch_no, csid, cc_no As Integer
            cc_no = 0
            csid = row(0)
            sch_no = row(4)
            scheme_name = row(2)
            'see if there is a last date on EmailRemit table for that csid

            Dim emailarray As Object = DBNull.Value

            Try
                emailarray = GetSQLResultsArray("FeesSQL", "SELECT er_last_date, er_email " & _
                                            "FROM EmailRemit  " & _
                                            "WHERE er_csid = " & csid)
                last_date_str = emailarray(0)
            Catch
                last_date_str = " "
            End Try
            If last_date_str > " " Then
                Try
                    last_date_str = Format(CDate(last_date_str), "dd/MM/yyyy")
                Catch
                    last_date_str = " "
                End Try
            End If

            'get from email address from table
            Try
                email = emailarray(1)
            Catch ex As Exception
                email = " "
            End Try
            'see if there are any ccs
            cc1 = " "
            cc2 = " "
            If email <> " " Then
                Dim emailccs As New DataTable
                LoadDataTable("FeesSQL", "select erc_cc from EmailRemitCC" & _
                                    " where erc_csid = " & csid, emailccs, False)

                Dim idx3 As Integer = 0
               
                For Each emailrow In emailccs.Rows
                    If idx3 = 0 Then
                        cc1 = emailrow(0)
                    ElseIf idx3 = 1 Then
                        cc2 = emailrow(0)
                    End If
                    cc_no += 1
                    idx3 += 1
                Next
            End If
           
            DataGridView1.Rows.Add(client_name, scheme_name, last_date_str, email, cc1, cc2, csid, cc_no, branch)
        Next
        mainform.ProgressBar1.Value = 0
    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Len(Trim(DataGridView1.Rows(e.RowIndex).Cells(3).Value)) = 0 Then
            MsgBox("Add main email address before any ccs")
            Exit Sub
        End If
        double_click = True
        csid_parm = DataGridView1.Rows(e.RowIndex).Cells(6).Value
        cl_name_parm = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        sch_name_parm = DataGridView1.Rows(e.RowIndex).Cells(1).Value
        ccform.ShowDialog()
        DataGridView1.Rows(e.RowIndex).Cells(7).Value = cc_no_parm
        Dim emailccs As New DataTable
        LoadDataTable("FeesSQL", "select erc_cc from EmailRemitCC" & _
                            " where erc_csid = " & csid_parm, emailccs, False)
        Dim row As DataRow
        Dim idx3 As Integer = 0
        Dim cc1 As String = " "
        Dim cc2 As String = " "
        For Each row In emailccs.Rows
            If idx3 = 0 Then
                cc1 = row(0)
            ElseIf idx3 = 1 Then
                cc2 = row(0)
            End If
            idx3 += 1
        Next
        DataGridView1.Rows(e.RowIndex).Cells(4).Value = cc1
        DataGridView1.Rows(e.RowIndex).Cells(5).Value = cc2
        double_click = False
    End Sub

    Private Sub DataGridView1_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DataGridView1.CellValidating
        If e.RowIndex < 0 Then
            Exit Sub
        End If

        If e.ColumnIndex = 3 Then  'email
            If Microsoft.VisualBasic.Len(Trim(e.FormattedValue.ToString)) = 0 _
            And DataGridView1.Rows(e.RowIndex).Cells(7).Value > 0 Then
                MsgBox("Remove ccs before removing main email address")
                e.Cancel = True
                Exit Sub
            End If
            If Microsoft.VisualBasic.Len(Trim(e.FormattedValue.ToString)) = 0 Then
                Exit Sub
            End If
            If InStr(e.FormattedValue.ToString, """") > 0 Then
                MsgBox("Email address must not contain quotes")
                e.Cancel = True
                Exit Sub
            End If
            If InStr(e.FormattedValue.ToString, ",") > 0 Then
                MsgBox("Email address must not contain a comma")
                e.Cancel = True
            End If
            If InStr(e.FormattedValue.ToString, "@") = 0 Then
                MsgBox("Email address must contain a @")
                e.Cancel = True
                Exit Sub
            End If
        End If

        If e.ColumnIndex = 2 Then
            Dim last_date_str As String = e.FormattedValue.ToString
            If Microsoft.VisualBasic.Len(Trim(last_date_str)) = 0 Then
                Exit Sub
            End If
            Try
                Dim last_date As Date = CDate(last_date_str)
            Catch
                MsgBox("Invalid date")
                e.Cancel = True
            End Try
        End If
        If e.ColumnIndex = 4 Or e.ColumnIndex = 5 Then
            orig_cc = Trim(DataGridView1.Rows(e.RowIndex).Cells(e.ColumnIndex).Value)
            If Microsoft.VisualBasic.Len(Trim(e.FormattedValue.ToString)) > 0 _
            And Microsoft.VisualBasic.Len(Trim(DataGridView1.Rows(e.RowIndex).Cells(3).Value)) = 0 Then
                MsgBox("Enter main email address before any ccs")
                e.Cancel = True
            End If
            If Microsoft.VisualBasic.Len(Trim(e.FormattedValue.ToString)) = 0 Then
                Exit Sub
            End If
            If InStr(e.FormattedValue.ToString, """") > 0 Then
                MsgBox("Email address must not contain quotes")
                e.Cancel = True
                Exit Sub
            End If
            If InStr(e.FormattedValue.ToString, ",") > 0 Then
                MsgBox("Email address must not contain a comma")
                e.Cancel = True
            End If
            If InStr(e.FormattedValue.ToString, "@") = 0 Then
                MsgBox("Email address must contain @")
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.RowIndex < 0 Or double_click Then
            Exit Sub
        End If
        Dim csid As Integer = DataGridView1.Rows(e.RowIndex).Cells(6).Value
        ' see if csid already exists
        Dim testcsid As Integer
        testcsid = GetSQLResults("FeesSQL", "Select er_csid from EmailRemit" & _
                                 " where er_csid = " & csid)
        If testcsid = 0 Then
            upd_txt = "insert into EmailRemit (er_csid) values ( " & _
                    csid & ")"
            update_sql(upd_txt)
        End If
        If e.ColumnIndex = 2 Then
            Dim last_date_str As String = DataGridView1.Rows(e.RowIndex).Cells(2).Value
            Try
                Dim last_date As Date = CDate(last_date_str)
            Catch
                last_date_str = Nothing
            End Try
            Try
                upd_txt = "update EmailRemit set er_last_date = '" & last_date_str & "'" & _
                    " where er_csid = " & csid
                update_sql(upd_txt)

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
        If e.ColumnIndex = 3 Then
            Dim email As String = DataGridView1.Rows(e.RowIndex).Cells(3).Value
            Try
                upd_txt = "update EmailRemit set er_email = '" & email & "'" & _
                   " where er_csid = " & csid
                update_sql(upd_txt)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

        End If
        If e.ColumnIndex = 4 Then
            Dim cc1 As String = DataGridView1.Rows(e.RowIndex).Cells(4).Value
            If Microsoft.VisualBasic.Len(orig_cc) > 0 Then
                If Microsoft.VisualBasic.Len(Trim(cc1)) = 0 Then
                    upd_txt = "delete from EmailRemitCC" & _
                   " where erc_csid = " & csid & _
                   " and erc_cc = '" & orig_cc & "'"
                    update_sql(upd_txt)

                    Dim emailarray As Object = DBNull.Value
                    Dim ccdt As New DataTable
                    LoadDataTable("FeesSQL", "SELECT erc_cc " & _
                                                    "FROM EmailRemitCC  " & _
                                                    "WHERE erc_csid = " & csid, ccdt, False)

                    Dim row As DataRow
                    Dim idx3 As Integer = 0
                    Dim cc2 As String = " "
                    For Each row In ccdt.Rows
                        If idx3 = 0 Then
                            cc1 = row(0)
                        ElseIf idx3 = 1 Then
                            cc2 = row(0)
                        End If
                        idx3 += 1
                    Next
                    DataGridView1.Rows(e.RowIndex).Cells(4).Value = cc1
                    DataGridView1.Rows(e.RowIndex).Cells(5).Value = cc2
                    DataGridView1.Rows(e.RowIndex).Cells(7).Value = idx3
                Else
                    If cc1 = orig_cc Then
                        Exit Sub
                    End If
                    Try
                        upd_txt = "update EmailRemitCC set erc_cc = '" & cc1 & "'" & _
                               " where erc_csid = " & csid & _
                               " and erc_cc = '" & orig_cc & "'"
                        update_sql(upd_txt)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try
                End If
            Else
                Try
                    upd_txt = "insert into EmailRemitCC (erc_csid, erc_cc) values(" & _
                      csid & ",'" & cc1 & "')"
                    update_sql(upd_txt)
                    DataGridView1.Rows(e.RowIndex).Cells(7).Value += 1
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            End If

        End If
        If e.ColumnIndex = 5 Then
            Dim cc2 As String = DataGridView1.Rows(e.RowIndex).Cells(5).Value
            If Microsoft.VisualBasic.Len(orig_cc) > 0 Then
                If Microsoft.VisualBasic.Len(Trim(cc2)) = 0 Then
                    upd_txt = "delete from EmailRemitCC" & _
                  " where erc_csid = " & csid & _
                  " and erc_cc = '" & orig_cc & "'"
                    update_sql(upd_txt)

                    DataGridView1.Rows(e.RowIndex).Cells(7).Value -= 1
                    Dim ccdt As New DataTable
                    LoadDataTable("FeesSQL", "SELECT erc_cc " & _
                                                    "FROM EmailRemitCC  " & _
                                                    "WHERE erc_csid = " & csid, ccdt, False)
                    Dim idx3 As Integer
                    Dim cc As String = " "
                    For Each row In ccdt.Rows
                       If idx3 = 1 Then
                            cc = row(0)
                        End If
                        idx3 += 1
                    Next
                    DataGridView1.Rows(e.RowIndex).Cells(5).Value = cc
                Else
                    If cc2 = orig_cc Then
                        Exit Sub
                    End If
                    Try
                        upd_txt = "update EmailRemitCC set erc_cc = '" & cc2 & "'" & _
                              " where erc_csid = " & csid & _
                              " and erc_cc = '" & orig_cc & "'"
                        update_sql(upd_txt)
                    Catch ex As Exception
                        MsgBox(ex.Message)
                    End Try

                End If
            Else
                Try
                    upd_txt = "insert into EmailRemitCC (erc_csid, erc_cc) values(" & _
                     csid & ",'" & cc2 & "')"
                    update_sql(upd_txt)
                    DataGridView1.Rows(e.RowIndex).Cells(7).Value += 1
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            End If
        End If

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class
Imports CommonLibrary
Public Class ccform

    Private Sub ccform_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'delete all ccs and then insert all
        ccDGV.EndEdit()
        upd_txt = "delete from EmailRemitCC where erc_csid = " & csid_parm
        update_sql(upd_txt)
        Dim datarow As DataGridViewRow
        Dim idx As Integer = 0
        cc_no_parm = 0
        For Each datarow In ccDGV.Rows
            Dim cc_cc As String
            Try
                cc_cc = ccDGV.Rows(idx).Cells(0).Value
            Catch ex As Exception
                idx += 1
                Continue For
            End Try
            If cc_cc = Nothing Then
                idx += 1
                Continue For
            End If
            upd_txt = "insert into EmailRemitCC (erc_csid, erc_cc) values (" & _
              csid_parm & ",'" & cc_cc & "')"
            update_sql(upd_txt)
            idx += 1
            cc_no_parm += 1
        Next
    End Sub

    Private Sub ccform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim emailccs As New DataTable
        LoadDataTable("FeesSQL", "select erc_cc from EmailRemitCC" & _
                            " where erc_csid = " & csid_parm, emailccs, False)
        'populate datagridview
        ccDGV.Rows.Clear()
        Dim datarow As DataRow
        For Each datarow In emailccs.Rows
            ccDGV.Rows.Add(datarow(0))
        Next
        cl_lbl.Text = cl_name_parm
        sch_lbl.Text = sch_name_parm
    End Sub

    Private Sub ccDataGridView_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles ccDGV.CellValidating
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Len(Trim(e.FormattedValue.ToString)) = 0 Then
            Exit Sub
        End If
        If InStr(e.FormattedValue.ToString, """") > 0 Then
            MsgBox("Email address must not contain quotes")
            e.Cancel = True
            Exit Sub
        End If
        If InStr(e.FormattedValue.ToString, ",") > 0 Then
            MsgBox("Email address must not contain a comma")
            e.Cancel = True
        End If
        If InStr(e.FormattedValue.ToString, "@") = 0 Then
            MsgBox("Email address must contain @")
            e.Cancel = True
        End If
    End Sub

End Class
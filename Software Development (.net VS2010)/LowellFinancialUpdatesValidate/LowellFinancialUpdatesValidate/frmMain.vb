﻿Imports System.IO
Imports CommonLibrary

Public Class frmMain
    Private Updates As New clsUpdatesData
    Const Separator As String = ","
    
    Private FilePath As String, FileName As String, FileExt As String, ReturnFileName As String

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click

        Dim FileDialog As New OpenFileDialog
        Dim ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String, ReturnLine As String, ConfirmationCode As String = "", ConfirmationReason As String = "", InvalidFile As String = ""
        Dim CaseDetails() As Object
        Dim InputLineArray() As String
        Dim LineNumber As Integer
        Dim ParseValueDate As Date
        Dim ParseValueDecimal As Decimal, TotalAdjustmentValue As Decimal = 0

        Try
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            FilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            lblReadingFile.Visible = True
            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)

            If Strings.Left(FileName, 19) <> "ROSSENDALES_OFA_ROF" Then InvalidFile = vbCrLf & "Invalid filename"

            Dim FileHeader() As String = FileContents(0).Split(",")
            If FileContents(1) <> "AccountNumber,ClientReferenceNumber,ClientName,Brand,ClientBalance,ClientBalanceDate,AdjustmentValue,AdjustmentDate,AdjustmentTypeCode,PaymentMethodCode,AdjustmentReference,CurrencyCode,Commission,CommissionRate,Fee,OFAID" Then InvalidFile &= vbCrLf & "Invalid header"

            For RowCount As Integer = 2 To UBound(FileContents)
                InputLineArray = FileContents(RowCount).Split(Separator)
                If Decimal.TryParse(InputLineArray(6), ParseValueDecimal) Then TotalAdjustmentValue += CDec(InputLineArray(6))
            Next RowCount

            If FileHeader(3) <> TotalAdjustmentValue Then InvalidFile &= vbCrLf & "Total value does not match header"
            If FileHeader(4) <> UBound(FileContents) - 1 Then InvalidFile &= vbCrLf & "Adjustment count does not match header"

            lblReadingFile.Visible = False

            'InvalidFile = "" ' used for testing row validation specifically invalid adjustment values.

            If InvalidFile <> "" Then
                ErrorLog = "File is invalid: " & InvalidFile
                MessageBox.Show(ErrorLog, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Else

                ProgressBar.Maximum = UBound(FileContents) + 8

                Updates.GetLastRunDetails()

                ReturnFileName = "CONFIRMATION_ROSSENDALES_OFA_ROF7369_" & DateTime.Today.ToString("ddMMyyyy") & (Updates.LastReturnDetails.FileSequence + 1).ToString & ".csv"
                If IO.File.Exists(FilePath & ReturnFileName) Then IO.File.Delete(FilePath & ReturnFileName)

                For Each InputLine As String In FileContents

                    LineNumber += 1

                    Application.DoEvents() ' without this line, the button disappears until processing is complete

                    InputLineArray = InputLine.Split(Separator)

                    ReturnLine = InputLine

                    If LineNumber = 2 Then ReturnLine &= Separator & "ConfirmationCode" & Separator & "ConfirmationReason"

                    If LineNumber > 2 Then
                        ConfirmationCode = ""
                        ConfirmationReason = ""
                        CaseDetails = Updates.GetCaseDetails(InputLineArray(0))

                        ' Perform fail checks first
                        If CaseDetails.Length = 0 Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "No current placement with the DCA receiving the transaction. "
                        End If

                        If InputLineArray(0).Length > 24 Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Account number length should not exceed 24 characters. "
                        End If

                        If InputLineArray(1).Length > 50 Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Client Reference Number length should not exceed 50 characters. "
                        End If

                        If InputLineArray(2) = "" Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Client name can not be blank. "
                        ElseIf InputLineArray(2).Length > 50 Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Client name length should not exceed 50 characters. "
                        End If

                        If InputLineArray(3) = "" Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Brand name can not be blank. "
                        ElseIf InputLineArray(3).Length > 50 Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Brand name can not be more than 50 characters. "
                        End If

                        If Not Decimal.TryParse(InputLineArray(4), ParseValueDecimal) Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Client balance amount is not a valid numeric value. "
                        End If

                        If Not Date.TryParse(InputLineArray(5), ParseValueDate) Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Invalid Client balance date format. "
                        ElseIf CDate(InputLineArray(5)) > Now Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Client balance date can not be greater than current date. "
                        End If

                        If Not Decimal.TryParse(InputLineArray(6), ParseValueDecimal) Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Transaction value is not a valid numeric value. "
                        ElseIf CDec(InputLineArray(6)) = 0 Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Transaction value should not be equal to zero. "
                        End If

                        If Not Date.TryParse(InputLineArray(7), ParseValueDate) Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Transaction date is not a valid date format. "
                        ElseIf CDate(InputLineArray(7)) > Now Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Transaction date can not be greater than current date. "
                        End If

                        If Not {"NPY", "PAY", "NPV", "PRV", "NBA", "PBA"}.Contains(InputLineArray(8)) Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Invalid adjustment type code. "
                        End If

                        If Not {"CC", "CHQ", "CSH", "DC", "DD", "STO", "DRS", "GIR", "PO", "DBC", "DSO", "DCH", "DCA", "CCR", "OTH"}.Contains(InputLineArray(9)) Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Invalid payment method code. "
                        End If

                        If InputLineArray(11).Length > 5 Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Currency code length should not be exceed more than 5. "
                        ElseIf InputLineArray(11) <> "GBP" Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Invalid Currency code. "
                        End If

                        If Not Decimal.TryParse(InputLineArray(12), ParseValueDecimal) Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Commission value is not a valid numeric value. "
                        End If

                        If Not Decimal.TryParse(InputLineArray(13), ParseValueDecimal) Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Commission rate is not a valid numeric value. "
                        End If

                        If InputLineArray(14) <> "" And Not Decimal.TryParse(InputLineArray(14), ParseValueDecimal) Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "Fee value is not a valid numeric value. "
                        End If

                        If InputLineArray(15) = "" Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "OFAID can not be blank. "
                        ElseIf InputLineArray(15).Length > 50 Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "OFAID length should be exceed more than 50. "
                        End If

                        ' check for acceptance with errors
                        If ConfirmationCode = "" Then

                            ' CaseDetails will be populated if ConfirmationCode is an empty string

                            If CDec(CaseDetails(1)) + CDec(InputLineArray(6)) < 0 Then ' + as adjustments are negative
                                ConfirmationCode = "ARWE"
                                ConfirmationReason = "Transaction Makes Balance Negative - Please cease sending payments " ' no need for & as reason will be empty if code is
                            End If

                            If CDec(CaseDetails(2)) <> InputLineArray(13) Then
                                ConfirmationCode = "ARWE"
                                ConfirmationReason &= "The DCA has a different commission rate to what is quoted"
                            End If

                            If CaseDetails(3) = "C" Then ' this section added TS 03/Sep/2015. Request ref 58382
                                ConfirmationCode = "FAIL"
                                ConfirmationReason &= "No current placement with the DCA receiving the transaction. "
                            End If

                        End If

                        ' else success
                        If ConfirmationCode = "" Then
                            ConfirmationCode = "ACRU"
                            ConfirmationReason = "Record accepted and loaded"
                        End If

                        ReturnLine &= Separator & ConfirmationCode & Separator & ConfirmationReason.TrimEnd

                    End If

                    AppendToFile(FilePath & ReturnFileName, ReturnLine & vbCrLf)

                    ProgressBar.Value += 1

                Next InputLine

                ProgressBar.Value += 1

                Updates.SetLastFileSequence(Updates.LastReturnDetails.FileSequence + 1, ReturnFileName, UBound(FileContents) + 1)

            End If

            AuditLog = "File processed: " & FilePath & FileName & FileExt & vbCrLf

            AuditLog &= "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= (UBound(FileContents) + 1).ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            WriteFile(FilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then WriteFile(FilePath & FileName & "_Error.txt", ErrorLog)

            If ExceptionLog <> "" Then WriteFile(FilePath & FileName & "_Exception.txt", ExceptionLog)

            MessageBox.Show("Preprocessing complete.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Try

            Using Writer As StreamWriter = New StreamWriter(FileName)
                Writer.Write(Content)
            End Using

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            Clipboard.SetText("notepad.exe " & FilePath & FileName & FileExt)
            If File.Exists(FilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", FilePath & FileName & FileExt)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        Try

            If File.Exists(FilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(FilePath & FileName & "_Audit.txt")
            If File.Exists(FilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(FilePath & FileName & "_Error.txt")
            If File.Exists(FilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(FilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(FilePath & ReturnFileName) Then System.Diagnostics.Process.Start(FilePath & ReturnFileName)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class


﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim OutputFile As String = ""
            Dim OutputFileWRP As String = ""
            Dim nonWRPCases As Integer = 0
            Dim WRPCases As Integer = 0
            Dim WRPValue As Decimal = 0
            Dim nonWRPValue As Decimal = 0
            Dim totValue As Decimal = 0
            Dim totCases As Integer = 0
            Dim InputLine As String
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents)
            Dim rowMax As Integer = UBound(FileContents)

            For rowIDX = 0 To rowMax
                InputLine = FileContents(rowIDX)
                Dim testAmt As Decimal
                Dim lineType As String = InputLine(0)
                If lineType = "H" Then
                    OutputFile = InputLine & vbNewLine
                    OutputFileWRP = InputLine & vbNewLine
                ElseIf lineType = "D" Then
                    Try
                        testAmt = Mid(InputLine, 356, 12)
                    Catch ex As Exception
                        testAmt = 0
                    End Try
                    'check column 335
                    If Mid(InputLine, 335, 1) = "C" Then
                        OutputFileWRP &= InputLine & vbNewLine
                        WRPCases += 1
                        WRPValue += testAmt
                    Else
                        OutputFile &= InputLine & vbNewLine
                        nonWRPCases += 1
                        nonWRPValue += testAmt
                    End If
                ElseIf lineType = "T" Then
                    Try
                        testAmt = Mid(InputLine, 12, 12)
                    Catch ex As Exception
                        testAmt = 0
                    End Try
                    totValue = testAmt
                    totCases = Mid(InputLine, 2, 3)
                Else
                    Continue For
                End If
            Next
            If WRPCases > 0 Then
                WriteFile(InputFilePath & FileName & "_WRP_4400.txt", OutputFileWRP)
                btnViewOutputFileHigh.Enabled = True
            End If
            If nonWRPCases > 0 Then
                WriteFile(InputFilePath & FileName & "_nonWRP_3990.txt", OutputFile)
                btnViewOutputFileLow.Enabled = True
            End If
            Dim statusMsg As String = ""
            If WRPCases + nonWRPCases <> totCases Then
                statusMsg = "Total cases do not match trailer" & vbNewLine
            End If
            If WRPValue + nonWRPValue <> totValue Then
                statusMsg &= "total Value does not match trailer"
            End If
            If statusMsg = "" Then
                statusMsg = "Totals match trailer"
            End If
            MsgBox("WRP Cases = " & WRPCases & " Value " & WRPValue & vbNewLine & _
                   "non WRP Cases = " & nonWRPCases & " Value " & nonWRPValue & vbNewLine & _
            statusMsg)

            btnViewInputFile.Enabled = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFileHigh.Click
        Try

            If File.Exists(InputFilePath & FileName & "_WRP_4400.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_WRP_4400.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewOutputFileLow.Click
        Try

            If File.Exists(InputFilePath & FileName & "_nonWRP_3990.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_nonWRP_3990.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.btnViewOutputFile = New System.Windows.Forms.Button()
        Me.btnViewInputFile = New System.Windows.Forms.Button()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.btnProcessFile = New System.Windows.Forms.Button()
        Me.btnViewOutputFil = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnViewOutputFile
        '
        Me.btnViewOutputFile.Enabled = False
        Me.btnViewOutputFile.Location = New System.Drawing.Point(68, 108)
        Me.btnViewOutputFile.Name = "btnViewOutputFile"
        Me.btnViewOutputFile.Size = New System.Drawing.Size(153, 42)
        Me.btnViewOutputFile.TabIndex = 8
        Me.btnViewOutputFile.Text = "View Output&File"
        Me.btnViewOutputFile.UseVisualStyleBackColor = True
        '
        'btnViewInputFile
        '
        Me.btnViewInputFile.Enabled = False
        Me.btnViewInputFile.Location = New System.Drawing.Point(68, 60)
        Me.btnViewInputFile.Name = "btnViewInputFile"
        Me.btnViewInputFile.Size = New System.Drawing.Size(153, 42)
        Me.btnViewInputFile.TabIndex = 7
        Me.btnViewInputFile.Text = "View &Input File"
        Me.btnViewInputFile.UseVisualStyleBackColor = True
        '
        'ProgressBar
        '
        Me.ProgressBar.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock
        Me.ProgressBar.Location = New System.Drawing.Point(12, 275)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(273, 22)
        Me.ProgressBar.TabIndex = 6
        '
        'btnProcessFile
        '
        Me.btnProcessFile.Location = New System.Drawing.Point(68, 12)
        Me.btnProcessFile.Name = "btnProcessFile"
        Me.btnProcessFile.Size = New System.Drawing.Size(153, 42)
        Me.btnProcessFile.TabIndex = 5
        Me.btnProcessFile.Text = "&Process File"
        Me.btnProcessFile.UseVisualStyleBackColor = True
        '
        'btnViewOutputFil
        '
        Me.btnViewOutputFil.Enabled = False
        Me.btnViewOutputFil.Location = New System.Drawing.Point(68, 108)
        Me.btnViewOutputFil.Name = "btnViewOutputFil"
        Me.btnViewOutputFil.Size = New System.Drawing.Size(153, 42)
        Me.btnViewOutputFil.TabIndex = 8
        Me.btnViewOutputFil.Text = "View &Output File"
        Me.btnViewOutputFil.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 309)
        Me.Controls.Add(Me.btnViewOutputFile)
        Me.Controls.Add(Me.btnViewInputFile)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.btnProcessFile)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Forest of Dean pre-processor"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnViewOutputFile As System.Windows.Forms.Button
    Friend WithEvents btnViewInputFile As System.Windows.Forms.Button
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents btnProcessFile As System.Windows.Forms.Button
    Friend WithEvents btnViewOutputFil As System.Windows.Forms.Button

End Class

﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String
    Dim addName(10) As String
    Dim addAddr(10) As String

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim OutputFile As String = "A|B|C|D|E|F|G|H|I|J|K|L|M|N|O|P|Q|R|S|T|U|V|W|X|Y|Z" & vbNewLine
            Dim totValue As Decimal = 0
            Dim totCases As Integer = 0

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents)
            Dim rowMax As Integer = UBound(FileContents)
            Dim yearPeriods As String = ""
            Dim notes As String = ""
            Dim balance As Decimal = 0
            Dim starttext As String = ""
            Dim endText As String = ""
            Dim record As String = ""
            For rowIDX = 0 To rowMax
                Dim recordType As String = Microsoft.VisualBasic.Left(FileContents(rowIDX), 1)
                Select Case recordType
                    Case "!"
                        If starttext = "" Then
                            starttext = Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                        Else
                            endText = Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                        End If
                    Case "0"
                        If totCases > 0 Then
                            OutputFile &= record & vbNewLine
                            record = ""
                        End If
                        'check for end of file
                        If rowIDX > rowMax - 3 Then
                            Continue For
                        End If
                        totCases += 1
                        'look for A
                        rowIDX += 1
                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "A" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for B

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "B" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for C

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "C" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for D

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "D" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for E

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "E" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for F

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "F" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for G

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "G" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for H

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "H" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for I

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "I" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for J

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "J" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for K

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "K" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for L

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "L" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for M

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "M" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for N

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "N" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for O

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "O" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for P

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "P" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for Q

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "Q" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for R

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "R" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                           
                        End If
                        record &= "|"
                        'look for S

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "S" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for T

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "T" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for U

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "U" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for V

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "V" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for W

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "W" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for X

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "X" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for Y

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "Y" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)

                            Dim testAmt As Decimal = 0
                            Try
                                testAmt = Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            Catch ex As Exception

                            End Try
                            totValue += testAmt
                            rowIDX += 1
                        End If
                        record &= "|"
                        'look for Z

                        If Microsoft.VisualBasic.Left(FileContents(rowIDX), 1) = "Z" Then
                            record &= Microsoft.VisualBasic.Right(FileContents(rowIDX), FileContents(rowIDX).Length - 2)
                            rowIDX += 1
                        End If
                        record &= "|"
                End Select
            Next
            btnViewInputFile.Enabled = True
            If totCases > 0 Then
                WriteFile(InputFilePath & FileName & "_preprocess.txt", OutputFile)
                btnViewOutputFile.Enabled = True
            End If


            MsgBox("Cases = " & totCases & " Value " & totValue & vbNewLine)
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_preprocess.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_preprocess.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & "_nonWRP_4200.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_nonWRP_4200.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

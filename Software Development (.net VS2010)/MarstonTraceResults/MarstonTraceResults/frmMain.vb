﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String, ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String
    Private ConnCode As New Dictionary(Of String, Integer)
    Private OutputFiles As New ArrayList()
    Private NewCases As New ArrayList()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim LineNumber As Integer
        Dim AddressOutputLine(55) As String, TraceResultsAddressFile As String = "", TraceResultsPhoneNumberFile As String = ""

        Try
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            Cursor = Cursors.WaitCursor

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            lblReadingFile.Visible = True

            Dim COAFileContents(,) As String = InputFromExcel(FileDialog.FileName, "COA")
            Dim LASFileContents(,) As String = InputFromExcel(FileDialog.FileName, "LAS")
            Dim NNDFileContents(,) As String = InputFromExcel(FileDialog.FileName, "NND")
            lblReadingFile.Visible = False
            ProgressBar.Maximum = UBound(COAFileContents) + UBound(LASFileContents) + UBound(NNDFileContents) + 3 ' audit file and two output files
            OutputFiles.Clear()

            For LineNumber = 1 To UBound(COAFileContents)
                ProgressBar.Value += 1
                Application.DoEvents() ' without this line, the button disappears until processing is complete
                Array.Clear(AddressOutputLine, 0, AddressOutputLine.Length)
                Dim InputPhoneNumber(2) As String

                AddressOutputLine(1) = COAFileContents(LineNumber, 0) ' Client ref
                AddressOutputLine(3) = "M" ' Match indicator
                AddressOutputLine(41) = ConcatFields({COAFileContents(LineNumber, 22), COAFileContents(LineNumber, 23), COAFileContents(LineNumber, 24), COAFileContents(LineNumber, 25), COAFileContents(LineNumber, 26)}, " ")
                AddressOutputLine(42) = ConcatFields({COAFileContents(LineNumber, 27), COAFileContents(LineNumber, 28)}, " ")
                AddressOutputLine(43) = COAFileContents(LineNumber, 29)
                AddressOutputLine(44) = COAFileContents(LineNumber, 30)
                AddressOutputLine(45) = COAFileContents(LineNumber, 31)

                If Not IsNothing(COAFileContents(LineNumber, 36)) AndAlso IsTelNumber(COAFileContents(LineNumber, 36)) Then InputPhoneNumber(0) = COAFileContents(LineNumber, 36)
                If Not IsNothing(COAFileContents(LineNumber, 37)) AndAlso IsTelNumber(COAFileContents(LineNumber, 37)) Then InputPhoneNumber(1) = COAFileContents(LineNumber, 37)
                If Not IsNothing(COAFileContents(LineNumber, 38)) AndAlso IsTelNumber(COAFileContents(LineNumber, 38)) Then InputPhoneNumber(2) = COAFileContents(LineNumber, 38)

                TraceResultsAddressFile &= Join(AddressOutputLine, vbTab) & vbCrLf

                ' ConcatFields ignores elements of nothing then split recreates the array with Distinct removing duplicates so {nothing, 1, nothing, 1, 2} becomes {1, 2}
                InputPhoneNumber = ConcatFields({InputPhoneNumber(0), InputPhoneNumber(1), InputPhoneNumber(2)}, "|").Split("|").Distinct.ToArray

                If InputPhoneNumber(0) <> "" Then TraceResultsPhoneNumberFile &= COAFileContents(LineNumber, 0) & "|" & _
                                                                                 InputPhoneNumber(0) & "|" & _
                                                                                 If(UBound(InputPhoneNumber) > 0, InputPhoneNumber(1), "") & vbCrLf

            Next LineNumber

            For LineNumber = 1 To UBound(LASFileContents)
                ProgressBar.Value += 1
                Application.DoEvents() ' without this line, the button disappears until processing is complete

                Dim InputPhoneNumber(2) As String

                If Not IsNothing(LASFileContents(LineNumber, 36)) AndAlso IsTelNumber(LASFileContents(LineNumber, 36)) Then InputPhoneNumber(0) = LASFileContents(LineNumber, 36)
                If Not IsNothing(LASFileContents(LineNumber, 37)) AndAlso IsTelNumber(LASFileContents(LineNumber, 37)) Then InputPhoneNumber(1) = LASFileContents(LineNumber, 37)
                If Not IsNothing(LASFileContents(LineNumber, 38)) AndAlso IsTelNumber(LASFileContents(LineNumber, 38)) Then InputPhoneNumber(2) = LASFileContents(LineNumber, 38)

                InputPhoneNumber = ConcatFields({InputPhoneNumber(0), InputPhoneNumber(1), InputPhoneNumber(2)}, "|").Split("|").Distinct.ToArray

                If InputPhoneNumber(0) <> "" Then TraceResultsPhoneNumberFile &= LASFileContents(LineNumber, 0) & "|" & _
                                                                                 InputPhoneNumber(0) & "|" & _
                                                                                 If(UBound(InputPhoneNumber) > 0, InputPhoneNumber(1), "") & vbCrLf
            Next LineNumber

            For LineNumber = 1 To UBound(NNDFileContents)
                ProgressBar.Value += 1
                Application.DoEvents() ' without this line, the button disappears until processing is complete

                Dim InputPhoneNumber(1) As String

                If Not IsNothing(NNDFileContents(LineNumber, 17)) AndAlso IsTelNumber(NNDFileContents(LineNumber, 17)) Then InputPhoneNumber(0) = NNDFileContents(LineNumber, 17)
                If Not IsNothing(NNDFileContents(LineNumber, 18)) AndAlso IsTelNumber(NNDFileContents(LineNumber, 18)) Then InputPhoneNumber(1) = NNDFileContents(LineNumber, 18)

                InputPhoneNumber = ConcatFields({InputPhoneNumber(0), InputPhoneNumber(1)}, "|").Split("|").Distinct.ToArray

                If InputPhoneNumber(0) <> "" Then TraceResultsPhoneNumberFile &= NNDFileContents(LineNumber, 0) & "|" & _
                                                                                 InputPhoneNumber(0) & "|" & _
                                                                                 If(UBound(InputPhoneNumber) > 0, InputPhoneNumber(1), "") & vbCrLf
            Next LineNumber

            ProgressBar.Value += 1
            AuditLog = "File processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= (UBound(COAFileContents) + UBound(LASFileContents) + UBound(NNDFileContents)).ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            ProgressBar.Value += 1

            If TraceResultsAddressFile <> "" Then
                TraceResultsAddressFile = "x	ClientId	x	M	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	x	BNumber	Street1	Locality	Town	Postcode	x	x	x	x	x	x	x	x	x	x" & vbCrLf & TraceResultsAddressFile
                WriteFile(InputFilePath & FileName & "_Addresses_PreProcessed.txt", TraceResultsAddressFile)
                OutputFiles.Add(InputFilePath & FileName & "_Addresses_PreProcessed.txt")
                AuditLog &= "Number of address trace results: " & (TraceResultsAddressFile.Split(vbCrLf).Length - 2).ToString & vbCrLf ' 
            End If

            If TraceResultsPhoneNumberFile <> "" Then
                WriteFile(InputFilePath & FileName & "_PhoneNumbers_PreProcessed.txt", TraceResultsPhoneNumberFile)
                OutputFiles.Add(InputFilePath & FileName & "_PhoneNumbers_PreProcessed.txt")
                AuditLog &= "Number of phone number trace results: " & (TraceResultsPhoneNumberFile.Split(vbCrLf).Length - 1).ToString & vbCrLf
            End If

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)
            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", ExceptionLog)

            Cursor = Cursors.Default

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If AuditLog <> "" And File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If ErrorLog <> "" And File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If ExceptionLog <> "" And File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub
End Class

﻿Imports System.Xml.Schema
Imports System.IO
Imports commonlibrary

Public Class UUXML
    Dim xml_valid As Boolean = True
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim new_file As String = ""
    Dim CSID, CSID1, CSID2, CSID1No, CSID2No As Integer
    Dim prod_run As Boolean = False
    Dim firstTrailer As Boolean = True
    Dim placements_processed As Integer

    Dim filename, auditFile, ExceptionLog, placementType, errorFile, errorFileName, MBCNotes As String
    Dim queryResponseFile As String = "DCA_code|Record_Type|Account_Number|Customer_Name|Query_Response" & vbNewLine
    Dim queryRequestFile As String = "DCA_code|Record_Type|Account_Number|Customer_Name|Query_Response" & vbNewLine
    Dim directPaymentFile As String = "DCA_code,Record_Type,Account_Number,Transaction_Value,Payment_Indicator,Filler, Placement_Indicator,Filler," & _
        "Debt_Status,Customer_Code,Customer_Name,Customer_address_line_1,Customer_Address_Line2,Customer_Address_Line_3,Customer_Address_Town," & _
        "Customer_Address_County,Customer_Postcode, Customer_Tel_Number,Court_Case_Number,Judgement_Date,Leaving_Date,DWP_Indicator,Charge_End_date," & _
        "File_Sequence" & vbNewLine
    Dim changesToCustomerFIle As String = "DCA_CODE,New_Charge_Type,Account_Number,Filler,FAStatus_Indicator,Filler,Customer_name,Customer_Address_Line_1,Customer_Address_Line_2," & _
        "Customer_Address_Line_3,CustomerAddress_Town,CustomerAddress_County,Customer_Postcode, Filler,Filler,Leaving_Date,Filler,Premise_Address_Line_1,Premise_Address_Line_2," & _
        "Premise_Address_Line_3,Premise_Address_Town,Premise_County,Premise_Postcode,Filler,Customer_status,Bankruptcy_flag,Deceased_flag,Tel_Number_1,Tel_Number_2," & _
        "Tel_Number_3,Tel_Number_4,Associated_customer_1,Associated_customer_2,Associated_customer_3,Associated_customer_4,FAO,Customer_DOB,Account_Type" & vbNewLine
    Dim financialAdjustmentsFile As String = directPaymentFile
    Dim additionalChargesFile As String = directPaymentFile
    Dim newCaseFile1 As String = "" 'DCA_code|Record_Type|Account_Number|Account_Balance|Fees_Balance|Costs_Balance|FAStatus_Indicator|Placement_Indicator|Previous_Recall|" & _
    '"Debt_Status|" & "Customer_Code|" & _
    '"Customer_Name|Customer_address|Filler|Court_Case_Number|Judgement_Date|Leaving_Date|DWP_Indicator|Premise_Address|Charge_End_Date|Customer_status|Customer_Type|" & _
    '"Tel_Number_1|Tel_Number_2|Tel_Number_3|Tel_Number_4|Associated_customer_1|Associated_customer_2|Associated_customer_3|Associated_customer_4|Commission_rate|" & _
    '"FAO|Date_Of_Birth|Last_Payment_Date|Last_Payment_Amount|Behavioural_Score|Charge_start_Date_|Latest_Bill_Date|Latest_Bill_Value|Date_of_oldest_Debt|Account_Type|" & _
    '"Current_Year_Debt|Previous_Year_Debt|Legacy_Debt|CustomerEmailAddress|Comments" & vbNewLine
    Dim newCaseFile2 As String = "" 'DCA_code|Record_Type|Account_Number|Account_Balance|Fees_Balance|Costs_Balance|FAStatus_Indicator|Placement_Indicator|Previous_Recall|" & _
    '"Debt_Status|" & "Customer_Code|" & _
    '"Customer_Name|Customer_address|Filler|Court_Case_Number|Judgement_Date|Leaving_Date|DWP_Indicator|Premise_Address|Charge_End_Date|Customer_status|Customer_Type|" & _
    '"Tel_Number_1|Tel_Number_2|Tel_Number_3|Tel_Number_4|Associated_customer_1|Associated_customer_2|Associated_customer_3|Associated_customer_4|Commission_rate|" & _
    '"FAO|Date_Of_Birth|Last_Payment_Date|Last_Payment_Amount|Behavioural_Score|Charge_start_Date_|Latest_Bill_Date|Latest_Bill_Value|Date_of_oldest_Debt|Account_Type|" & _
    '"Current_Year_Debt|Previous_Year_Debt|Legacy_Debt|CustomerEmailAddress|Comments" & vbNewLine
    Dim adhocFile As String = "Agency_Code,New_Charge_Type,ALTO_Account_Number,Customer_name,Customer_Address_Line_1,Customer_Address_Line_2,Customer_Address_Line_3," & _
        "Customer_Address_Town,Customer_Address_County,Customer_Postcode,Filler,Filler,Premise_Address_Line_1,Premise_Address_Line_2,Premise_Address_Line_3," & _
        "Premise_Address_Town,Premise_County,Premise_Postcode,Ad-Hoc_Value,Ad-Hoc_Date,Ad-Hoc_Text" & vbNewLine

    Dim informationStatusFile As String = "DCA_code,Record_Type,Account_Number,Customer_Name,Message_Status,Reason,Account_Balance" & vbNewLine

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If



    End Sub
    Private Sub validate_xml_file()
        'open file as text first to remove any £ signs
        Dim txt_doc As String
        txt_doc = My.Computer.FileSystem.ReadAllText(OpenFileDialog1.FileName)
        txt_doc = Replace(txt_doc, "£", "")
        My.Computer.FileSystem.WriteAllText(OpenFileDialog1.FileName, txt_doc, False)
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(OpenFileDialog1.FileName)
        Dim XSDFile As String = "R:\vb.net\UU XSD\TESTAgencyOutboundXSD.xsd"
        Try
            myDocument.Schemas.Add("", XSDFile)
        Catch ex As Exception
            errorFile = "File: " & OpenFileDialog1.FileName & "has failed validation with XSD" & vbNewLine
            errorFile &= "XSD file: " & XSDFile & vbNewLine
            errorFile &= "Reason: " & ex.ToString
            My.Computer.FileSystem.WriteAllText(errorFileName, errorFile, False)
            xml_valid = False
            Exit Sub
        End Try

        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
    End Sub
    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_error("Error: " & e.Message)
            Case XmlSeverityType.Warning
                write_error("Warning " & e.Message)
        End Select
    End Sub
    Private Sub write_error(ByVal error_message As String)
        Static Dim error_no As Integer
        error_no += 1
        error_message = error_message & vbNewLine
        If error_no = 1 Then
            My.Computer.FileSystem.WriteAllText(errorFileName, error_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(errorFileName, error_message, True)
        End If

    End Sub

    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub processbtn_Click(sender As System.Object, e As System.EventArgs) Handles processbtn.Click
        With OpenFileDialog1
            .Title = "Read XML file"
            .Filter = "XML file|*.xml"
            .FileName = ""
            .CheckFileExists = True
        End With
        Dim xml_dataSet As New DataSet()
        Dim doc As New Xml.XmlDocument()

        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("File not opened")
            Me.Close()
            Exit Sub
        End If
        Dim InputFilePath As String = Path.GetDirectoryName(OpenFileDialog1.FileName)
        InputFilePath &= "\"
        filename = Path.GetFileNameWithoutExtension(OpenFileDialog1.FileName)
        errorFileName = InputFilePath & "wagy023o_error.txt"
        validate_xml_file()
        If xml_valid = False Then
            MsgBox("XML file has failed validation - see error file")
            If MsgBox("Do you want to continue processing the file?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Me.Close()
                Exit Sub
            End If
        End If
        Dim nextFinancialYear As Date = CDate("Apr 1, " & Year(Now) + 2 & " 0:0:0")
        If Month(Now) < 4 Then
            nextFinancialYear = DateAdd(DateInterval.Year, -1, nextFinancialYear)
        End If
        filename = Path.GetFileNameWithoutExtension(OpenFileDialog1.FileName)
        'created directory for TO Client Support
        Dim toClientName As String = InputFilePath & "To Client Support"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(toClientName)
            If System.IO.Directory.Exists(toClientName) = False Then
                di = System.IO.Directory.CreateDirectory(toClientName)
            Else
                System.IO.Directory.Delete(toClientName, True)
                di = System.IO.Directory.CreateDirectory(toClientName)
            End If
        Catch ex As Exception
            MsgBox("Unable to create folder")
            End
        End Try

        Dim rdr_name As String = ""
        Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
        reader.Read()
        ProgressBar1.Value = 5
        Dim record_count As Integer = 0
        If prod_run = False Then
            MsgBox("TEST RUN ONLY")
        End If
        Dim newCases, FACases, addChargecases, dirPaycases, adHocCases, changeCases As Integer
        Dim totBalance, FAAdjustments, addChargeBalances, dirPayAmounts As Decimal
        Dim queryRequestFound As Boolean = False
        Dim queryResponseFound As Boolean = False
        Dim infoFound As Boolean = False
        Dim testDate As Date
        Dim comments As String
        Dim fileSequence As Integer
        Dim AccountNumber As String = ""
        Dim recordType As String = ""
        Dim customerName As String = ""
        Dim messageStatus As String = ""
        Dim reason As String = ""
        Dim filler As String = ""
        Dim CurrentYearDebt As Decimal
        Dim previousYearDebt As Decimal
        Dim LegacyDebt As Decimal
        Dim FAStatusIndicator As String = ""
        Dim placementIndicator As String = ""
        Dim previousRecall As String = ""
        Dim customerAddressLine1 As String = ""
        Dim customerAddressLine2 As String = ""
        Dim customerAddressLine3 As String = ""
        Dim customerAddressTown As String = ""
        Dim customerAddressCounty As String = ""
        Dim customerPostcode As String = ""
        Dim emailAddress As String = ""
        Dim LatestBillValue As Decimal
        Dim DateOfOldestDebt As String
        Dim premiseAddressLine1 As String = ""
        Dim premiseAddressLine2 As String = ""
        Dim premiseAddressLine3 As String = ""
        Dim premiseAddressTown As String = ""
        Dim premiseCounty As String = ""
        Dim premisePostcode As String = ""
        Dim adhocValue As String
        Dim adhocDate As String
        Dim adhoctext As String
        Dim BehaviouralScore As String
        Dim leavingDate As String = ""
        Dim BankruptcyFlag As String
        Dim DeceasedFlag As String
        Dim chargeStartDate As String = ""
        Dim chargeEndDate As String = ""
        Dim LastPaymentDate As String
        Dim LastPaymentAmount As Decimal
        Dim LatestBillDate As String
        Dim DWPIndicator As String = ""
        Dim accountType As String = ""
        Dim courtCaseNumber As String = ""
        Dim FAO As String = ""
        Dim DOB As String = ""
        Dim customerTelNumber1 As String = ""
        Dim customerTelNumber2 As String = ""
        Dim customerTelNumber3 As String = ""
        Dim customerTelNumber4 As String = ""
        Dim AssociatedCustomer1 As String = ""

        Dim PriorityCareRsn1 As String
        Dim PriorityCareSrv1 As String
        Dim PriorityCareRsn2 As String
        Dim PriorityCareSrv2 As String
        Dim PriorityCareRsn3 As String
        Dim PriorityCareSrv3 As String
        Dim PriorityCareRsn4 As String
        Dim PriorityCareSrv4 As String
        Dim PriorityCareRsn5 As String
        Dim PriorityCareSrv5 As String
        Dim PriorityCareRsn6 As String
        Dim PriorityCareSrv6 As String
        Dim PriorityCareRsn7 As String
        Dim PriorityCareSrv7 As String
        Dim PriorityCareRsn8 As String
        Dim PriorityCareSrv8 As String
        Dim PriorityCareRsn9 As String
        Dim PriorityCareSrv9 As String
        Dim PriorityCareRsn10 As String
        Dim PriorityCareSrv10 As String

        Dim PriorityCareAC1Rsn1 As String
        Dim PriorityCareAC1Srv1 As String
        Dim PriorityCareAC1Rsn2 As String
        Dim PriorityCareAC1Srv2 As String
        Dim PriorityCareAC1Rsn3 As String
        Dim PriorityCareAC1Srv3 As String
        Dim PriorityCareAC1Rsn4 As String
        Dim PriorityCareAC1Srv4 As String
        Dim PriorityCareAC1Rsn5 As String
        Dim PriorityCareAC1Srv5 As String
        Dim PriorityCareAC1Rsn6 As String
        Dim PriorityCareAC1Srv6 As String
        Dim PriorityCareAC1Rsn7 As String
        Dim PriorityCareAC1Srv7 As String
        Dim PriorityCareAC1Rsn8 As String
        Dim PriorityCareAC1Srv8 As String
        Dim PriorityCareAC1Rsn9 As String
        Dim PriorityCareAC1Srv9 As String
        Dim PriorityCareAC1Rsn10 As String
        Dim PriorityCareAC1Srv10 As String

        Dim PriorityCareAC2Rsn1 As String
        Dim PriorityCareAC2Srv1 As String
        Dim PriorityCareAC2Rsn2 As String
        Dim PriorityCareAC2Srv2 As String
        Dim PriorityCareAC2Rsn3 As String
        Dim PriorityCareAC2Srv3 As String
        Dim PriorityCareAC2Rsn4 As String
        Dim PriorityCareAC2Srv4 As String
        Dim PriorityCareAC2Rsn5 As String
        Dim PriorityCareAC2Srv5 As String
        Dim PriorityCareAC2Rsn6 As String
        Dim PriorityCareAC2Srv6 As String
        Dim PriorityCareAC2Rsn7 As String
        Dim PriorityCareAC2Srv7 As String
        Dim PriorityCareAC2Rsn8 As String
        Dim PriorityCareAC2Srv8 As String
        Dim PriorityCareAC2Rsn9 As String
        Dim PriorityCareAC2Srv9 As String
        Dim PriorityCareAC2Rsn10 As String
        Dim PriorityCareAC2Srv10 As String

        Dim PriorityCareAC3Rsn1 As String
        Dim PriorityCareAC3Srv1 As String
        Dim PriorityCareAC3Rsn2 As String
        Dim PriorityCareAC3Srv2 As String
        Dim PriorityCareAC3Rsn3 As String
        Dim PriorityCareAC3Srv3 As String
        Dim PriorityCareAC3Rsn4 As String
        Dim PriorityCareAC3Srv4 As String
        Dim PriorityCareAC3Rsn5 As String
        Dim PriorityCareAC3Srv5 As String
        Dim PriorityCareAC3Rsn6 As String
        Dim PriorityCareAC3Srv6 As String
        Dim PriorityCareAC3Rsn7 As String
        Dim PriorityCareAC3Srv7 As String
        Dim PriorityCareAC3Rsn8 As String
        Dim PriorityCareAC3Srv8 As String
        Dim PriorityCareAC3Rsn9 As String
        Dim PriorityCareAC3Srv9 As String
        Dim PriorityCareAC3Rsn10 As String
        Dim PriorityCareAC3Srv10 As String

        Dim PriorityCareAC4Rsn1 As String
        Dim PriorityCareAC4Srv1 As String
        Dim PriorityCareAC4Rsn2 As String
        Dim PriorityCareAC4Srv2 As String
        Dim PriorityCareAC4Rsn3 As String
        Dim PriorityCareAC4Srv3 As String
        Dim PriorityCareAC4Rsn4 As String
        Dim PriorityCareAC4Srv4 As String
        Dim PriorityCareAC4Rsn5 As String
        Dim PriorityCareAC4Srv5 As String
        Dim PriorityCareAC4Rsn6 As String
        Dim PriorityCareAC4Srv6 As String
        Dim PriorityCareAC4Rsn7 As String
        Dim PriorityCareAC4Srv7 As String
        Dim PriorityCareAC4Rsn8 As String
        Dim PriorityCareAC4Srv8 As String
        Dim PriorityCareAC4Rsn9 As String
        Dim PriorityCareAC4Srv9 As String
        Dim PriorityCareAC4Rsn10 As String
        Dim PriorityCareAC4Srv10 As String

        Dim AssociatedCustomer2 As String = ""
        Dim AssociatedCustomer3 As String = ""
        Dim AssociatedCustomer4 As String = ""
        Dim judgementDate As String = ""
        Dim queryRequestDesc As String = ""
        Dim queryResponseDesc As String = ""
        Dim dcaCode As String = ""
        Dim customerStatus As String = ""
        Dim customerType As String = ""
        Dim AccountBalance As Decimal = 0
        Dim transactionvalue As Decimal = 0
        Dim feesBalance As Decimal = 0
        Dim costsBalance As Decimal = 0
        Dim courtBalance As Decimal = 0
        While (reader.ReadState <> Xml.ReadState.EndOfFile)
            If reader.NodeType > 1 Then
                reader.Read()
                Continue While
            End If
            Try
                rdr_name = reader.Name
            Catch
                Continue While
            End Try

            'Note ReadElementContentAsString moves focus to next element so don't need read
            ProgressBar1.Value = record_count
            Application.DoEvents()

            Select Case rdr_name
                Case "DMtoAgencyDebtsUpdates"
                    reader.Read()
                Case "PlacementType"
                    placementType = reader.Item(0)
                    reader.Read()
                Case "Header"
                    reader.Read()
                    While reader.Name <> "Header"
                        rdr_name = reader.Name
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        rdr_name = reader.Name
                        Select Case rdr_name
                            Case "FileSequence"
                                fileSequence = reader.ReadElementContentAsString
                            Case "DCACode"
                                dcaCode = reader.ReadElementContentAsString
                                'get clientschemeID from onestep'
                                Dim CSID_dt As New DataTable
                                'get clientschemeID from onestep'
                                LoadDataTable("DebtRecovery", "SELECT CS._rowID, S.name from clientscheme CS, scheme S" & _
                                      " WHERE clientID = 1662" & _
                                      " and CS.SchemeID=S._rowID" & _
                                      " and defaultCourtCode = '" & dcaCode & "'", CSID_dt, False)
                                For Each csidrow In CSID_dt.Rows
                                    If csidrow(1) = "Early Out" Then
                                        CSID1 = csidrow(0)
                                        CSID2 = 0
                                    Else
                                        If InStr(csidrow(1), "1st") > 0 Then
                                            CSID1 = csidrow(0)
                                        End If
                                        If InStr(csidrow(1), "2nd") > 0 Then
                                            CSID2 = csidrow(0)
                                        End If
                                    End If
                                    
                                Next
                        End Select
                        reader.Read()
                    End While
                Case "Details"
                    reader.Read()
                    While reader.Name <> "Details"
                        rdr_name = reader.Name
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        rdr_name = reader.Name
                        Select Case rdr_name
                            Case "NewDebtsDetails"
                                reader.Read()
                                While reader.Name <> "NewDebtsDetails"
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    rdr_name = reader.Name
                                    Select Case rdr_name
                                        Case "NewDebtAccountDetails"
                                            AccountNumber = reader.Item(0)
                                            reader.Read()
                                            While reader.Name <> "NewDebtAccountDetails"
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                rdr_name = reader.Name
                                                Select Case rdr_name
                                                    Case "NewDebtDetails"
                                                        reader.Read()
                                                        While reader.Name <> "NewDebtDetails"
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            rdr_name = reader.Name
                                                            Select Case rdr_name
                                                                Case "NewDebtDetail"
                                                                    reader.Read()
                                                                    comments = ""
                                                                    dcaCode = ""
                                                                    recordType = ""
                                                                    AccountNumber = ""
                                                                    AccountBalance = 0
                                                                    feesBalance = 0
                                                                    costsBalance = 0
                                                                    courtBalance = 0
                                                                    customerName = ""
                                                                    transactionvalue = 0
                                                                    placementIndicator = ""
                                                                    previousRecall = ""
                                                                    customerAddressLine1 = ""
                                                                    customerAddressLine2 = ""
                                                                    customerAddressLine3 = ""
                                                                    customerAddressTown = ""
                                                                    customerAddressCounty = ""
                                                                    customerPostcode = ""
                                                                    emailAddress = ""
                                                                    customerTelNumber1 = ""
                                                                    customerTelNumber2 = ""
                                                                    customerTelNumber3 = ""
                                                                    customerTelNumber4 = ""
                                                                    courtCaseNumber = ""
                                                                    judgementDate = ""
                                                                    leavingDate = ""
                                                                    chargeEndDate = ""
                                                                    DWPIndicator = ""
                                                                    premiseAddressLine1 = ""
                                                                    premiseAddressLine2 = ""
                                                                    premiseAddressLine3 = ""
                                                                    premiseAddressTown = ""
                                                                    premiseCounty = ""
                                                                    premisePostcode = ""
                                                                    accountType = ""
                                                                    FAO = ""
                                                                    DOB = ""
                                                                    While reader.Name <> "NewDebtDetail"
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        rdr_name = reader.Name

                                                                        Select Case rdr_name
                                                                            Case "DCACode"
                                                                                dcaCode = reader.ReadElementContentAsString
                                                                            Case "RecordType"
                                                                                recordType = reader.ReadElementContentAsString
                                                                            Case "AccountNumber"
                                                                                AccountNumber = reader.ReadElementContentAsString
                                                                            Case "AccountBalance"
                                                                                AccountBalance = reader.ReadElementContentAsString
                                                                                comments &= ToNote(AccountBalance, rdr_name)(0)
                                                                            Case "FeesBalance"
                                                                                feesBalance = reader.ReadElementContentAsString
                                                                                comments &= ToNote(feesBalance, rdr_name)(0)
                                                                            Case "CostsBalance"
                                                                                costsBalance = reader.ReadElementContentAsString
                                                                                comments &= ToNote(costsBalance, rdr_name)(0)
                                                                            Case "CourtBalance"
                                                                                courtBalance = reader.ReadElementContentAsString
                                                                            Case "FAStatusIndicator"
                                                                                FAStatusIndicator = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(FAStatusIndicator, rdr_name)(0)
                                                                            Case "CustomerName"
                                                                                customerName = reader.ReadElementContentAsString
                                                                            Case "TransactionValue"
                                                                                transactionvalue = reader.ReadElementContentAsString
                                                                            Case "PlacementIndicator"
                                                                                placementIndicator = reader.ReadElementContentAsString
                                                                                comments &= ToNote(placementIndicator, rdr_name)(0)
                                                                                'set CSID
                                                                                Dim CSID_dt As New DataTable
                                                                                If placementIndicator = 1 Then
                                                                                    CSID = CSID1
                                                                                    CSID1No += 1
                                                                                ElseIf placementIndicator = 2 Then
                                                                                    CSID = CSID2
                                                                                    CSID2No += 1
                                                                                End If
                                                                                If CSID = 0 Then
                                                                                    MsgBox("Need to set up CSID for DCACode = " & dcaCode)
                                                                                    'Exit While
                                                                                End If
                                                                                placements_processed += 1
                                                                            Case "PreviousRecall"
                                                                                previousRecall = reader.ReadElementContentAsString
                                                                                comments &= ToNote(previousRecall, rdr_name)(0)
                                                                            Case "CustomerAddressLine1"
                                                                                customerAddressLine1 = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressLine2"
                                                                                customerAddressLine2 = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressLine3"
                                                                                customerAddressLine3 = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressTown"
                                                                                customerAddressTown = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressCounty"
                                                                                customerAddressCounty = reader.ReadElementContentAsString()
                                                                            Case "CustomerPostcode"
                                                                                customerPostcode = reader.ReadElementContentAsString()
                                                                            Case "CustomerEmailAddress"
                                                                                emailAddress = reader.ReadElementContentAsString()
                                                                            Case "TelNumber1"
                                                                                customerTelNumber1 = reader.ReadElementContentAsString()
                                                                            Case "TelNumber2"
                                                                                customerTelNumber2 = reader.ReadElementContentAsString()
                                                                            Case "TelNumber3"
                                                                                customerTelNumber3 = reader.ReadElementContentAsString()
                                                                            Case "TelNumber4"
                                                                                customerTelNumber4 = reader.ReadElementContentAsString()
                                                                            Case "CourtCaseNumber"
                                                                                courtCaseNumber = reader.ReadElementContentAsString()
                                                                            Case "JudgementDate"
                                                                                judgementDate = reader.ReadElementContentAsString()
                                                                            Case "LeavingDate"
                                                                                leavingDate = reader.ReadElementContentAsString()
                                                                                testDate = CDate(Mid(leavingDate, 1, 4) & "-" & _
                                                                                    Mid(leavingDate, 5, 2) & "-" & Mid(leavingDate, 7, 2))
                                                                                comments &= ToNote(Format(testDate, "dd/MMM/yyyy"), rdr_name)(0)
                                                                            Case "ChargeEndDate"
                                                                                chargeEndDate = reader.ReadElementContentAsString()
                                                                                testDate = CDate(Mid(chargeEndDate, 1, 4) & "-" & _
                                                                                    Mid(chargeEndDate, 5, 2) & "-" & Mid(chargeEndDate, 7, 2))
                                                                                comments &= ToNote(Format(testDate, "dd/MMM/yyyy"), rdr_name)(0)
                                                                            Case "ChargeStartDate"
                                                                                chargeStartDate = reader.ReadElementContentAsString()
                                                                                testDate = CDate(Mid(chargeStartDate, 1, 4) & "-" & _
                                                                                    Mid(chargeStartDate, 5, 2) & "-" & Mid(chargeStartDate, 7, 2))
                                                                                comments &= ToNote(Format(testDate, "dd/MMM/yyyy"), rdr_name)(0)
                                                                            Case "LatestBilldate"
                                                                                LatestBillDate = reader.ReadElementContentAsString()
                                                                                testDate = CDate(Mid(LatestBillDate, 1, 4) & "-" & _
                                                                                    Mid(LatestBillDate, 5, 2) & "-" & Mid(LatestBillDate, 7, 2))
                                                                                comments &= ToNote(Format(testDate, "dd/MMM/yyyy"), rdr_name)(0)
                                                                            Case "LatestBillValue"
                                                                                LatestBillValue = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(LatestBillValue, rdr_name)(0)
                                                                            Case "LastPaymentDate"
                                                                                LastPaymentDate = reader.ReadElementContentAsString()
                                                                                testDate = CDate(Mid(LastPaymentDate, 1, 4) & "-" & _
                                                                                    Mid(LastPaymentDate, 5, 2) & "-" & Mid(LastPaymentDate, 7, 2))
                                                                                comments &= ToNote(Format(testDate, "dd/MMM/yyyy"), rdr_name)(0)
                                                                            Case "DateOfOldestDebt"
                                                                                DateOfOldestDebt = reader.ReadElementContentAsString()
                                                                                testDate = CDate(Mid(DateOfOldestDebt, 1, 4) & "-" & _
                                                                                    Mid(DateOfOldestDebt, 5, 2) & "-" & Mid(DateOfOldestDebt, 7, 2))
                                                                                comments &= ToNote(Format(testDate, "dd/MMM/yyyy"), rdr_name)(0)
                                                                            Case " LastPaymentAmount"
                                                                                LastPaymentAmount = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(LastPaymentAmount, rdr_name)(0)
                                                                            Case "CustomerStatus"
                                                                                customerStatus = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(customerStatus, rdr_name)(0)
                                                                            Case "CustomerType"
                                                                                customerType = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(customerType, rdr_name)(0)
                                                                            Case "DWPIndicator"
                                                                                DWPIndicator = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(DWPIndicator, rdr_name)(0)
                                                                            Case "PremiseAddressLine1"
                                                                                premiseAddressLine1 = reader.ReadElementContentAsString()
                                                                            Case "PremiseAddressLine2"
                                                                                premiseAddressLine2 = reader.ReadElementContentAsString()
                                                                            Case "PremiseAddressLine3"
                                                                                premiseAddressLine3 = reader.ReadElementContentAsString()
                                                                            Case "PremiseAddressTown"
                                                                                premiseAddressTown = reader.ReadElementContentAsString()
                                                                            Case "PremiseCounty"
                                                                                premiseCounty = reader.ReadElementContentAsString()
                                                                            Case "PremisePostcode"
                                                                                premisePostcode = reader.ReadElementContentAsString()
                                                                            Case "FAO"
                                                                                FAO = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(FAO, rdr_name)(0)
                                                                            Case "AccountType"
                                                                                accountType = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(accountType, rdr_name)(0)
                                                                            Case "DateOfBirth"
                                                                                DOB = reader.ReadElementContentAsString()
                                                                            Case "CurrentYearDebt"
                                                                                CurrentYearDebt = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(CurrentYearDebt, rdr_name)(0)
                                                                            Case "PreviousYearDebt"
                                                                                previousYearDebt = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(previousYearDebt, rdr_name)(0)
                                                                            Case "LegacyDebt"
                                                                                LegacyDebt = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(LegacyDebt, rdr_name)(0)
                                                                            Case "PriorityCare"
                                                                                reader.Read()
                                                                                While reader.Name <> "PriorityCare"
                                                                                    If reader.NodeType > 1 Then
                                                                                        reader.Read()
                                                                                        Continue While
                                                                                    End If
                                                                                    rdr_name = reader.Name
                                                                                    Select Case rdr_name
                                                                                        Case "PriorityCareRsn1"
                                                                                            PriorityCareRsn1 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareRsn1, rdr_name)(0)
                                                                                        Case "PriorityCareSrv1"
                                                                                            PriorityCareSrv1 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareSrv1, rdr_name)(0)
                                                                                        Case "PriorityCareRsn2"
                                                                                            PriorityCareRsn2 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareRsn2, rdr_name)(0)
                                                                                        Case "PriorityCareSrv2"
                                                                                            PriorityCareSrv2 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareSrv2, rdr_name)(0)
                                                                                        Case "PriorityCareRsn3"
                                                                                            PriorityCareRsn3 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareRsn3, rdr_name)(0)
                                                                                        Case "PriorityCarSrv3"
                                                                                            PriorityCareSrv3 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareSrv3, rdr_name)(0)
                                                                                        Case "PriorityCareRsn4"
                                                                                            PriorityCareRsn4 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareRsn4, rdr_name)(0)
                                                                                        Case "PriorityCareSrv4"
                                                                                            PriorityCareSrv4 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareSrv4, rdr_name)(0)
                                                                                        Case "PriorityCareRsn5"
                                                                                            PriorityCareRsn5 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareRsn5, rdr_name)(0)
                                                                                        Case "PriorityCareSrv5"
                                                                                            PriorityCareSrv5 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareSrv5, rdr_name)(0)
                                                                                        Case "PriorityCareRsn6"
                                                                                            PriorityCareRsn6 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareRsn6, rdr_name)(0)
                                                                                        Case "PriorityCareSrv6"
                                                                                            PriorityCareSrv6 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareSrv6, rdr_name)(0)
                                                                                        Case "PriorityCareRsn7"
                                                                                            PriorityCareRsn7 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareRsn7, rdr_name)(0)
                                                                                        Case "PriorityCareSrv7"
                                                                                            PriorityCareSrv7 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareSrv7, rdr_name)(0)
                                                                                        Case "PriorityCareRsn8"
                                                                                            PriorityCareRsn8 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareRsn8, rdr_name)(0)
                                                                                        Case "PriorityCareSrv8"
                                                                                            PriorityCareSrv8 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareSrv8, rdr_name)(0)
                                                                                        Case "PriorityCareRsn9"
                                                                                            PriorityCareRsn9 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareRsn9, rdr_name)(0)
                                                                                        Case "PriorityCareSrv9"
                                                                                            PriorityCareSrv9 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareSrv9, rdr_name)(0)
                                                                                        Case "PriorityCareRsn10"
                                                                                            PriorityCareRsn10 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareRsn10, rdr_name)(0)
                                                                                        Case "PriorityCareSrv10"
                                                                                            PriorityCareSrv10 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareSrv10, rdr_name)(0)
                                                                                    End Select
                                                                                    reader.Read()
                                                                                End While
                                                                            Case "AssociatedCustomer1"
                                                                                AssociatedCustomer1 = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(AssociatedCustomer1, rdr_name)(0)
                                                                            Case "PriorityCareAC1"
                                                                                reader.Read()
                                                                                While reader.Name <> "PriorityCareAC1"
                                                                                    If reader.NodeType > 1 Then
                                                                                        reader.Read()
                                                                                        Continue While
                                                                                    End If
                                                                                    rdr_name = reader.Name
                                                                                    Select Case rdr_name
                                                                                        Case "PriorityCareAC1Rsn1"
                                                                                            PriorityCareAC1Rsn1 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Rsn1, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Srv1"
                                                                                            PriorityCareAC1Srv1 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Srv1, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Rsn2"
                                                                                            PriorityCareAC1Rsn2 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Rsn2, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Srv2"
                                                                                            PriorityCareAC1Srv2 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Srv2, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Rsn3"
                                                                                            PriorityCareAC1Rsn3 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Rsn3, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Srv3"
                                                                                            PriorityCareAC1Srv3 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Srv3, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Rsn4"
                                                                                            PriorityCareAC1Rsn4 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Rsn4, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Srv4"
                                                                                            PriorityCareAC1Srv4 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Srv4, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Rsn5"
                                                                                            PriorityCareAC1Rsn5 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Rsn5, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Srv5"
                                                                                            PriorityCareAC1Srv5 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Srv5, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Rsn6"
                                                                                            PriorityCareAC1Rsn6 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Rsn6, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Srv6"
                                                                                            PriorityCareAC1Srv6 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Srv6, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Rsn7"
                                                                                            PriorityCareAC1Rsn7 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Rsn7, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Srv7"
                                                                                            PriorityCareAC1Srv7 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Srv7, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Rsn8"
                                                                                            PriorityCareAC1Rsn8 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Rsn8, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Srv8"
                                                                                            PriorityCareAC1Srv8 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Srv8, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Rsn9"
                                                                                            PriorityCareAC1Rsn9 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Rsn9, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Srv9"
                                                                                            PriorityCareAC1Srv9 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Srv9, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Rsn10"
                                                                                            PriorityCareAC1Rsn10 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Rsn10, rdr_name)(0)
                                                                                        Case "PriorityCareAC1Srv10"
                                                                                            PriorityCareAC1Srv10 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC1Srv10, rdr_name)(0)
                                                                                    End Select
                                                                                    reader.Read()
                                                                                End While
                                                                            Case "AssociatedCustomer2"
                                                                                AssociatedCustomer2 = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(AssociatedCustomer2, rdr_name)(0)
                                                                            Case "PriorityCareAC2"
                                                                                reader.Read()
                                                                                While reader.Name <> "PriorityCareAC2"
                                                                                    If reader.NodeType > 1 Then
                                                                                        reader.Read()
                                                                                        Continue While
                                                                                    End If
                                                                                    rdr_name = reader.Name
                                                                                    Select Case rdr_name
                                                                                        Case "PriorityCareAC2Rsn1"
                                                                                            PriorityCareAC2Rsn1 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Rsn1, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Srv1"
                                                                                            PriorityCareAC2Srv1 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Srv1, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Rsn2"
                                                                                            PriorityCareAC2Rsn2 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Rsn2, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Srv2"
                                                                                            PriorityCareAC2Srv2 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Srv2, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Rsn3"
                                                                                            PriorityCareAC2Rsn3 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Rsn3, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Srv3"
                                                                                            PriorityCareAC2Srv3 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Srv3, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Rsn4"
                                                                                            PriorityCareAC2Rsn4 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Rsn4, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Srv4"
                                                                                            PriorityCareAC2Srv4 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Srv4, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Rsn5"
                                                                                            PriorityCareAC2Rsn5 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Rsn5, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Srv5"
                                                                                            PriorityCareAC2Srv5 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Srv5, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Rsn6"
                                                                                            PriorityCareAC2Rsn6 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Rsn6, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Srv6"
                                                                                            PriorityCareAC2Srv6 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Srv6, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Rsn7"
                                                                                            PriorityCareAC2Rsn7 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Rsn7, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Srv7"
                                                                                            PriorityCareAC2Srv7 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Srv7, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Rsn8"
                                                                                            PriorityCareAC2Rsn8 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Rsn8, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Srv8"
                                                                                            PriorityCareAC2Srv8 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Srv8, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Rsn9"
                                                                                            PriorityCareAC2Rsn9 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Rsn9, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Srv9"
                                                                                            PriorityCareAC2Srv9 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Srv9, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Rsn10"
                                                                                            PriorityCareAC2Rsn10 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Rsn10, rdr_name)(0)
                                                                                        Case "PriorityCareAC2Srv10"
                                                                                            PriorityCareAC2Srv10 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC2Srv10, rdr_name)(0)
                                                                                    End Select
                                                                                    reader.Read()
                                                                                End While
                                                                            Case "AssociatedCustomer3"
                                                                                AssociatedCustomer3 = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(AssociatedCustomer3, rdr_name)(0)
                                                                            Case "PriorityCareAC3"
                                                                                reader.Read()
                                                                                While reader.Name <> "PriorityCareAC3"
                                                                                    If reader.NodeType > 1 Then
                                                                                        reader.Read()
                                                                                        Continue While
                                                                                    End If
                                                                                    rdr_name = reader.Name
                                                                                    Select Case rdr_name
                                                                                        Case "PriorityCareAC3Rsn1"
                                                                                            PriorityCareAC3Rsn1 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Rsn1, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Srv1"
                                                                                            PriorityCareAC3Srv1 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Srv1, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Rsn2"
                                                                                            PriorityCareAC3Rsn2 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Rsn2, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Srv2"
                                                                                            PriorityCareAC3Srv2 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Srv2, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Rsn3"
                                                                                            PriorityCareAC3Rsn3 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Rsn3, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Srv3"
                                                                                            PriorityCareAC3Srv3 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Srv3, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Rsn4"
                                                                                            PriorityCareAC3Rsn4 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Rsn4, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Srv4"
                                                                                            PriorityCareAC3Srv4 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Srv4, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Rsn5"
                                                                                            PriorityCareAC3Rsn5 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Rsn5, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Srv5"
                                                                                            PriorityCareAC3Srv5 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Srv5, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Rsn6"
                                                                                            PriorityCareAC3Rsn6 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Rsn6, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Srv6"
                                                                                            PriorityCareAC3Srv6 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Srv6, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Rsn7"
                                                                                            PriorityCareAC3Rsn7 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Rsn7, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Srv7"
                                                                                            PriorityCareAC3Srv7 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Srv7, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Rsn8"
                                                                                            PriorityCareAC3Rsn8 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Rsn8, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Srv8"
                                                                                            PriorityCareAC3Srv8 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Srv8, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Rsn9"
                                                                                            PriorityCareAC3Rsn9 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Rsn9, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Srv9"
                                                                                            PriorityCareAC3Srv9 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Srv9, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Rsn10"
                                                                                            PriorityCareAC3Rsn10 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Rsn10, rdr_name)(0)
                                                                                        Case "PriorityCareAC3Srv10"
                                                                                            PriorityCareAC3Srv10 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC3Srv10, rdr_name)(0)
                                                                                    End Select
                                                                                    reader.Read()
                                                                                End While
                                                                            Case "AssociatedCustomer4"
                                                                                AssociatedCustomer4 = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(AssociatedCustomer4, rdr_name)(0)
                                                                            Case "PriorityCareAC4"
                                                                                reader.Read()
                                                                                While reader.Name <> "PriorityCareAC4"
                                                                                    If reader.NodeType > 1 Then
                                                                                        reader.Read()
                                                                                        Continue While
                                                                                    End If
                                                                                    rdr_name = reader.Name
                                                                                    Select Case rdr_name
                                                                                        Case "PriorityCareAC4Rsn1"
                                                                                            PriorityCareAC4Rsn1 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Rsn1, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Srv1"
                                                                                            PriorityCareAC4Srv1 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Srv1, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Rsn2"
                                                                                            PriorityCareAC4Rsn2 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Rsn2, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Srv2"
                                                                                            PriorityCareAC4Srv2 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Srv2, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Rsn3"
                                                                                            PriorityCareAC4Rsn3 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Rsn3, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Srv3"
                                                                                            PriorityCareAC4Srv3 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Srv3, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Rsn4"
                                                                                            PriorityCareAC4Rsn4 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Rsn4, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Srv4"
                                                                                            PriorityCareAC4Srv4 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Srv4, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Rsn5"
                                                                                            PriorityCareAC4Rsn5 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Rsn5, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Srv5"
                                                                                            PriorityCareAC4Srv5 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Srv5, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Rsn6"
                                                                                            PriorityCareAC4Rsn6 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Rsn6, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Srv6"
                                                                                            PriorityCareAC4Srv6 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Srv6, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Rsn7"
                                                                                            PriorityCareAC4Rsn7 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Rsn7, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Srv7"
                                                                                            PriorityCareAC4Srv7 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Srv7, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Rsn8"
                                                                                            PriorityCareAC4Rsn8 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Rsn8, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Srv8"
                                                                                            PriorityCareAC4Srv8 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Srv8, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Rsn9"
                                                                                            PriorityCareAC4Rsn9 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Rsn9, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Srv9"
                                                                                            PriorityCareAC4Srv9 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Srv9, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Rsn10"
                                                                                            PriorityCareAC4Rsn10 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Rsn10, rdr_name)(0)
                                                                                        Case "PriorityCareAC4Srv10"
                                                                                            PriorityCareAC4Srv10 = reader.ReadElementContentAsString
                                                                                            comments &= ToNote(PriorityCareAC4Srv10, rdr_name)(0)
                                                                                    End Select
                                                                                    reader.Read()
                                                                                End While
                                                                            Case "BehaviouralScore"
                                                                                BehaviouralScore = reader.ReadElementContentAsString()
                                                                                comments &= ToNote(BehaviouralScore, rdr_name)(0)
                                                                        End Select
                                                                        reader.Read()
                                                                    End While
                                                                    Select Case recordType
                                                                        Case "N"  'new case
                                                                            Dim customerAddress As String = Trim(customerAddressLine1)
                                                                            Try
                                                                                If customerAddress.Length > 0 Then
                                                                                    customerAddress &= ","
                                                                                End If
                                                                            Catch ex As Exception

                                                                            End Try
                                                                            Try
                                                                                If Trim(customerAddressLine2).Length > 0 Then
                                                                                    customerAddress &= Trim(customerAddressLine2) & ","
                                                                                End If
                                                                            Catch ex As Exception

                                                                            End Try
                                                                            Try
                                                                                If Trim(customerAddressLine3).Length > 0 Then
                                                                                    customerAddress &= Trim(customerAddressLine3) & ","
                                                                                End If
                                                                            Catch ex As Exception

                                                                            End Try
                                                                            Try
                                                                                If Trim(customerAddressTown).Length > 0 Then
                                                                                    customerAddress &= Trim(customerAddressTown) & ","
                                                                                End If
                                                                            Catch ex As Exception

                                                                            End Try
                                                                            Try
                                                                                If Trim(customerAddressCounty).Length > 0 Then
                                                                                    customerAddress &= Trim(customerAddressCounty) & ","
                                                                                End If
                                                                            Catch ex As Exception

                                                                            End Try
                                                                            If customerAddress.Length > 0 Then
                                                                                customerAddress = Microsoft.VisualBasic.Left(customerAddress, customerAddress.Length - 1) & ","
                                                                            End If
                                                                            If Trim(customerPostcode).Length > 0 Then
                                                                                customerAddress &= Trim(customerPostcode)
                                                                            End If
                                                                            Dim premiseAddress As String = Trim(premiseAddressLine1)
                                                                            Try
                                                                                If premiseAddress.Length > 0 Then
                                                                                    premiseAddress &= ","
                                                                                End If
                                                                            Catch ex As Exception

                                                                            End Try
                                                                            Try
                                                                                If Trim(premiseAddressLine2).Length > 0 Then
                                                                                    premiseAddress &= Trim(premiseAddressLine2) & ","
                                                                                End If
                                                                            Catch ex As Exception

                                                                            End Try
                                                                            Try
                                                                                If Trim(premiseAddressLine3).Length > 0 Then
                                                                                    premiseAddress &= Trim(premiseAddressLine3) & ","
                                                                                End If
                                                                            Catch ex As Exception

                                                                            End Try
                                                                            Try
                                                                                If Trim(premiseAddressTown).Length > 0 Then
                                                                                    premiseAddress &= Trim(premiseAddressTown) & ","
                                                                                End If
                                                                            Catch ex As Exception

                                                                            End Try
                                                                            Try
                                                                                If Trim(premiseCounty).Length > 0 Then
                                                                                    premiseAddress &= Trim(premiseCounty) & ","
                                                                                End If
                                                                            Catch ex As Exception

                                                                            End Try
                                                                            If premiseAddress.Length > 0 Then
                                                                                premiseAddress = Microsoft.VisualBasic.Left(premiseAddress, premiseAddress.Length - 1) & " "  'change last comma to a space
                                                                            End If
                                                                            If Trim(premisePostcode).Length > 0 Then
                                                                                premiseAddress &= Trim(premisePostcode)
                                                                            End If
                                                                            If CSID = CSID1 Then
                                                                                newCaseFile1 &= dcaCode & "|" & recordType & "|" & AccountNumber & "|" & AccountBalance & "|" & _
                                                                                                                                                              feesBalance & "|" & costsBalance & "|" & FAStatusIndicator & "|" & placementIndicator & "|" & _
                                                                                                                                                              previousRecall & "|" & filler & "|" & filler & "|" & customerName & "|" & customerAddress & "|" & _
                                                                                                                                                              filler & "|" & courtCaseNumber & "|" & _
                                                                                                                                                               judgementDate & "|" & leavingDate & "|" & DWPIndicator & "|" & premiseAddress & "|" & _
                                                                                                                                                           chargeEndDate & "|" & filler & "|" & filler & "|" & _
                                                                                                                                                             customerTelNumber1 & "|" & customerTelNumber2 & "|" & customerTelNumber3 & "|" & _
                                                                                                                                                             customerTelNumber4 & "|" & filler & "|" & filler & "|" & filler & "|" & filler & "|" & _
                                                                                                                                                              filler & "|" & FAO & "|" & DOB & "|" & filler & "|" & filler & "|" & filler & "|" & _
                                                                                                                                                              filler & "|" & filler & "|" & filler & "|" & filler & "|" & accountType & "|" & _
                                                                                                                                                              filler & "|" & filler & "|" & filler & "|" & emailAddress & "|" & comments & vbNewLine
                                                                            Else
                                                                                newCaseFile2 &= dcaCode & "|" & recordType & "|" & AccountNumber & "|" & AccountBalance & "|" & _
                                                                              feesBalance & "|" & costsBalance & "|" & FAStatusIndicator & "|" & placementIndicator & "|" & _
                                                                              previousRecall & "|" & filler & "|" & filler & "|" & customerName & "|" & customerAddress & "|" & _
                                                                              filler & "|" & courtCaseNumber & "|" & _
                                                                               judgementDate & "|" & leavingDate & "|" & DWPIndicator & "|" & premiseAddress & "|" & _
                                                                           chargeEndDate & "|" & filler & "|" & filler & "|" & _
                                                                             customerTelNumber1 & "|" & customerTelNumber2 & "|" & customerTelNumber3 & "|" & _
                                                                             customerTelNumber4 & "|" & filler & "|" & filler & "|" & filler & "|" & filler & "|" & _
                                                                              filler & "|" & FAO & "|" & DOB & "|" & filler & "|" & filler & "|" & filler & "|" & _
                                                                              filler & "|" & filler & "|" & filler & "|" & filler & "|" & accountType & "|" & _
                                                                              filler & "|" & filler & "|" & filler & "|" & emailAddress & "|" & comments & vbNewLine
                                                                            End If
                                                                           
                                                                            newCases += 1
                                                                            totBalance += AccountBalance
                                                                    End Select
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                            End Select
                                                            reader.Read()
                                                        End While
                                                    Case "QueriesHistory"
                                                        reader.Read()
                                                        While reader.Name <> "QueriesHistory"
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            rdr_name = reader.Name
                                                            Select Case rdr_name
                                                                Case "QueryHistory"
                                                                    reader.Read()
                                                                    While reader.Name <> "QueryHistory"
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        rdr_name = reader.Name
                                                                        Select Case rdr_name
                                                                            Case "QueryRequest"
                                                                                reader.Read()
                                                                                dcaCode = ""
                                                                                recordType = ""
                                                                                AccountNumber = ""
                                                                                customerName = ""
                                                                                queryRequestDesc = ""
                                                                                While reader.Name <> "QueryRequest"
                                                                                    If reader.NodeType > 1 Then
                                                                                        reader.Read()
                                                                                        Continue While
                                                                                    End If
                                                                                    rdr_name = reader.Name
                                                                                    Select Case rdr_name
                                                                                        Case "DCACode"
                                                                                            dcaCode = reader.ReadElementContentAsString
                                                                                        Case "RecordType"
                                                                                            recordType = reader.ReadElementContentAsString
                                                                                        Case "CustomerName"
                                                                                            customerName = reader.ReadElementContentAsString
                                                                                        Case "AccountNumber"
                                                                                            AccountNumber = reader.ReadElementContentAsString
                                                                                        Case "QueryRequestDesc"
                                                                                            queryRequestDesc = reader.ReadElementContentAsString
                                                                                    End Select
                                                                                    reader.Read()
                                                                                End While
                                                                                'save queryrequest in file
                                                                                queryRequestFile &= dcaCode & "|" & recordType & "|" & AccountNumber & "|" & _
                                                                                    customerName & "|" & queryRequestDesc & vbNewLine
                                                                                queryRequestFound = True
                                                                            Case "QueryResponse"
                                                                                dcaCode = ""
                                                                                recordType = ""
                                                                                AccountNumber = ""
                                                                                customerName = ""
                                                                                queryResponseDesc = ""
                                                                                reader.Read()
                                                                                While reader.Name <> "QueryResponse"
                                                                                    If reader.NodeType > 1 Then
                                                                                        reader.Read()
                                                                                        Continue While
                                                                                    End If
                                                                                    rdr_name = reader.Name
                                                                                    Select Case rdr_name
                                                                                        Case "DCACode"
                                                                                            dcaCode = reader.ReadElementContentAsString
                                                                                        Case "RecordType"
                                                                                            recordType = reader.ReadElementContentAsString
                                                                                        Case "AccountNumber"
                                                                                            AccountNumber = reader.ReadElementContentAsString
                                                                                        Case "CustomerName"
                                                                                            customerName = reader.ReadElementContentAsString
                                                                                        Case "QueryResponseDesc"
                                                                                            queryResponseDesc = reader.ReadElementContentAsString
                                                                                    End Select
                                                                                    reader.Read()
                                                                                End While
                                                                                'save queryresponse in file
                                                                                queryResponseFile &= dcaCode & "|" & recordType & "|" & AccountNumber & "|" & _
                                                                                    customerName & "|" & queryResponseDesc & vbNewLine
                                                                                queryResponseFound = True
                                                                            Case Else
                                                                                MsgBox("What is this tag?" & rdr_name)
                                                                        End Select
                                                                        reader.Read()
                                                                    End While
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                            End Select
                                                            reader.Read()
                                                        End While
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                End Select
                                                reader.Read()
                                            End While
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                    End Select
                                    reader.Read()

                                End While
                            Case "ExistingDebtsDetails"
                                reader.Read()
                                While reader.Name <> "ExistingDebtsDetails"
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    rdr_name = reader.Name
                                    Select Case rdr_name
                                        Case "ExistingDebtAccountDetails"
                                            reader.Read()
                                            While reader.Name <> "ExistingDebtAccountDetails"
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                rdr_name = reader.Name
                                                Select Case rdr_name
                                                    Case "FinancialTransactions"
                                                        reader.Read()
                                                        While reader.Name <> "FinancialTransactions"
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            rdr_name = reader.Name
                                                            Select Case rdr_name
                                                                Case "FinancialTransaction"
                                                                    reader.Read()
                                                                    dcaCode = ""
                                                                    recordType = ""
                                                                    AccountNumber = ""
                                                                    customerName = ""
                                                                    transactionvalue = 0
                                                                    placementIndicator = ""
                                                                    customerAddressLine1 = ""
                                                                    customerAddressLine2 = ""
                                                                    customerAddressLine3 = ""
                                                                    customerAddressTown = ""
                                                                    customerAddressCounty = ""
                                                                    customerPostcode = ""
                                                                    customerTelNumber1 = ""
                                                                    courtCaseNumber = ""
                                                                    judgementDate = ""
                                                                    leavingDate = ""
                                                                    chargeEndDate = ""
                                                                    DWPIndicator = ""
                                                                    While reader.Name <> "FinancialTransaction"
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        rdr_name = reader.Name
                                                                        Select Case rdr_name
                                                                            Case "DCACode"
                                                                                dcaCode = reader.ReadElementContentAsString
                                                                            Case "RecordType"
                                                                                recordType = reader.ReadElementContentAsString
                                                                            Case "AccountNumber"
                                                                                AccountNumber = reader.ReadElementContentAsString
                                                                            Case "CustomerName"
                                                                                customerName = reader.ReadElementContentAsString
                                                                            Case "TransactionValue"
                                                                                transactionvalue = reader.ReadElementContentAsString
                                                                            Case "PlacementIndicator"
                                                                                placementIndicator = reader.ReadElementContentAsString
                                                                            Case "CustomerAddressLine1"
                                                                                customerAddressLine1 = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressLine2"
                                                                                customerAddressLine2 = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressLine3"
                                                                                customerAddressLine3 = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressTown"
                                                                                customerAddressTown = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressCounty"
                                                                                customerAddressCounty = reader.ReadElementContentAsString()
                                                                            Case "CustomerPostcode"
                                                                                customerPostcode = reader.ReadElementContentAsString()
                                                                            Case "CustomerTelNumber"
                                                                                customerTelNumber1 = reader.ReadElementContentAsString()
                                                                            Case "CourtCaseNumber"
                                                                                courtCaseNumber = reader.ReadElementContentAsString()
                                                                            Case "JudgementDate"
                                                                                judgementDate = reader.ReadElementContentAsString()
                                                                            Case "LeavingDate"
                                                                                leavingDate = reader.ReadElementContentAsString()
                                                                            Case "ChargeEndDate"
                                                                                chargeEndDate = reader.ReadElementContentAsString()
                                                                            Case "DWPIndicator"
                                                                                DWPIndicator = reader.ReadElementContentAsString()
                                                                        End Select
                                                                        reader.Read()
                                                                    End While
                                                                    Select Case recordType
                                                                        Case "P"  'direct payment
                                                                            directPaymentFile &= dcaCode & "," & recordType & "," & AccountNumber & "," & transactionvalue & "," & _
                                                                               filler & "," & filler & "," & placementIndicator & "," & filler & "," & _
                                                                                filler & "," & filler & "," & customerName & "," & customerAddressLine1 & "," & _
                                                                                customerAddressLine2 & "," & customerAddressLine3 & "," & customerAddressTown & "," & _
                                                                                customerAddressCounty & "," & customerPostcode & "," & customerTelNumber1 & "," & _
                                                                                courtCaseNumber & "," & judgementDate & "," & leavingDate & "," & DWPIndicator & "," & _
                                                                                chargeEndDate & "," & fileSequence & vbNewLine
                                                                            dirPaycases += 1
                                                                            dirPayAmounts += transactionvalue
                                                                        Case "J"  'balance adjustment

                                                                            financialAdjustmentsFile &= dcaCode & "," & recordType & "," & AccountNumber & "," & transactionvalue & "," & _
                                                                               filler & "," & filler & "," & placementIndicator & "," & filler & "," & _
                                                                                filler & "," & filler & "," & customerName & "," & customerAddressLine1 & "," & _
                                                                                customerAddressLine2 & "," & customerAddressLine3 & "," & customerAddressTown & "," & _
                                                                                customerAddressCounty & "," & customerPostcode & "," & customerTelNumber1 & "," & _
                                                                                courtCaseNumber & "," & judgementDate & "," & leavingDate & "," & DWPIndicator & "," & _
                                                                                chargeEndDate & "," & fileSequence & vbNewLine
                                                                            FACases += 1
                                                                            FAAdjustments += transactionvalue
                                                                        Case "M"  'additional charges
                                                                            additionalChargesFile &= dcaCode & "," & recordType & "," & AccountNumber & "," & transactionvalue & "," & _
                                                                               filler & "," & filler & "," & placementIndicator & "," & filler & "," & _
                                                                                filler & "," & filler & "," & customerName & "," & customerAddressLine1 & "," & _
                                                                                customerAddressLine2 & "," & customerAddressLine3 & "," & customerAddressTown & "," & _
                                                                                customerAddressCounty & "," & customerPostcode & "," & customerTelNumber1 & "," & _
                                                                                courtCaseNumber & "," & judgementDate & "," & leavingDate & "," & DWPIndicator & "," & _
                                                                                chargeEndDate & "," & fileSequence & vbNewLine
                                                                            addChargecases += 1
                                                                            addChargeBalances += transactionvalue
                                                                            testDate = CDate(Microsoft.VisualBasic.Left(chargeEndDate, 4) & "," & _
                                                                                Mid(chargeEndDate, 5, 2) & "," & Microsoft.VisualBasic.Right(chargeEndDate, 2))
                                                                            If Format(testDate, "yyyy-MM-dd") = nextFinancialYear Then
                                                                                ExceptionLog &= "Account Number " & AccountNumber & " Main Billing Charge not loaded as it is in the next financial year " & vbCrLf
                                                                                MBCNotes &= getCaseID(AccountNumber) & "|Main Billing Charge of " & transactionvalue & " chargeable from " & Format(nextFinancialYear, "dd/MMM/yyyy") & vbNewLine
                                                                            End If
                                                                    End Select
                                                            End Select
                                                        End While
                                                    Case "ChangesToCustomerDetails"
                                                        reader.Read()
                                                        While reader.Name <> "ChangesToCustomerDetails"
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            rdr_name = reader.Name
                                                            dcaCode = ""
                                                            recordType = ""
                                                            AccountNumber = ""
                                                            AccountBalance = 0
                                                            feesBalance = 0
                                                            costsBalance = 0
                                                            courtBalance = 0
                                                            FAStatusIndicator = ""
                                                            customerName = ""
                                                            transactionvalue = 0
                                                            placementIndicator = ""
                                                            previousRecall = ""
                                                            customerAddressLine1 = ""
                                                            customerAddressLine2 = ""
                                                            customerAddressLine3 = ""
                                                            customerAddressTown = ""
                                                            customerAddressCounty = ""
                                                            customerPostcode = ""
                                                            customerTelNumber1 = ""
                                                            customerTelNumber2 = ""
                                                            customerTelNumber3 = ""
                                                            customerTelNumber4 = ""
                                                            courtCaseNumber = ""
                                                            judgementDate = ""
                                                            leavingDate = ""
                                                            chargeEndDate = ""
                                                            chargeStartDate = ""
                                                            LatestBillDate = ""
                                                            LatestBillValue = 0
                                                            LastPaymentDate = ""
                                                            DateOfOldestDebt = ""
                                                            LastPaymentAmount = 0
                                                            customerStatus = ""
                                                            customerType = ""
                                                            DWPIndicator = ""
                                                            premiseAddressLine1 = ""
                                                            premiseAddressLine2 = ""
                                                            premiseAddressLine3 = ""
                                                            premiseAddressTown = ""
                                                            premiseCounty = ""
                                                            premisePostcode = ""
                                                            FAO = ""
                                                            accountType = ""
                                                            DOB = ""
                                                            CurrentYearDebt = 0
                                                            previousYearDebt = 0
                                                            LegacyDebt = 0
                                                            AssociatedCustomer1 = ""
                                                            AssociatedCustomer2 = ""
                                                            AssociatedCustomer3 = ""
                                                            AssociatedCustomer4 = ""
                                                            BehaviouralScore = ""
                                                            BankruptcyFlag = ""
                                                            DeceasedFlag = ""
                                                            Select Case rdr_name
                                                                Case "ChangesToCustomerDetail"
                                                                    reader.Read()
                                                                    While reader.Name <> "ChangesToCustomerDetail"
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        rdr_name = reader.Name
                                                                        Select Case rdr_name
                                                                            Case "DCACode"
                                                                                dcaCode = reader.ReadElementContentAsString
                                                                            Case "RecordType"
                                                                                recordType = reader.ReadElementContentAsString
                                                                            Case "AccountNumber"
                                                                                AccountNumber = reader.ReadElementContentAsString
                                                                            Case "AccountBalance"
                                                                                AccountBalance = reader.ReadElementContentAsString
                                                                            Case "FeesBalance"
                                                                                feesBalance = reader.ReadElementContentAsString
                                                                            Case "CostsBalance"
                                                                                costsBalance = reader.ReadElementContentAsString
                                                                            Case "CourtBalance"
                                                                                courtBalance = reader.ReadElementContentAsString
                                                                            Case "FAStatusIndicator"
                                                                                FAStatusIndicator = reader.ReadElementContentAsString()
                                                                            Case "CustomerName"
                                                                                customerName = reader.ReadElementContentAsString
                                                                            Case "TransactionValue"
                                                                                transactionvalue = reader.ReadElementContentAsString
                                                                            Case "PlacementIndicator"
                                                                                placementIndicator = reader.ReadElementContentAsString
                                                                            Case "PreviousRecall"
                                                                                previousRecall = reader.ReadElementContentAsString
                                                                            Case "CustomerAddressLine1"
                                                                                customerAddressLine1 = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressLine2"
                                                                                customerAddressLine2 = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressLine3"
                                                                                customerAddressLine3 = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressTown"
                                                                                customerAddressTown = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressCounty"
                                                                                customerAddressCounty = reader.ReadElementContentAsString()
                                                                            Case "CustomerPostcode"
                                                                                customerPostcode = reader.ReadElementContentAsString()
                                                                            Case "TelNumber1"
                                                                                customerTelNumber1 = reader.ReadElementContentAsString()
                                                                            Case "TelNumber2"
                                                                                customerTelNumber2 = reader.ReadElementContentAsString()
                                                                            Case "TelNumber3"
                                                                                customerTelNumber3 = reader.ReadElementContentAsString()
                                                                            Case "TelNumber4"
                                                                                customerTelNumber4 = reader.ReadElementContentAsString()
                                                                            Case "CourtCaseNumber"
                                                                                courtCaseNumber = reader.ReadElementContentAsString()
                                                                            Case "JudgementDate"
                                                                                judgementDate = reader.ReadElementContentAsString()
                                                                            Case "LeavingDate"
                                                                                leavingDate = reader.ReadElementContentAsString()
                                                                            Case "ChargeEndDate"
                                                                                chargeEndDate = reader.ReadElementContentAsString()
                                                                            Case "ChargeStartDate"
                                                                                chargeStartDate = reader.ReadElementContentAsString()
                                                                            Case "LatestBilldate"
                                                                                LatestBillDate = reader.ReadElementContentAsString()
                                                                            Case "LatestBillValue"
                                                                                LatestBillValue = reader.ReadElementContentAsString()
                                                                            Case "LastPaymentDate"
                                                                                LastPaymentDate = reader.ReadElementContentAsString()
                                                                            Case "DateOfOldestDebt"
                                                                                DateOfOldestDebt = reader.ReadElementContentAsString()
                                                                            Case " LastPaymentAmount"
                                                                                LastPaymentAmount = reader.ReadElementContentAsString()
                                                                            Case "CustomerStatus"
                                                                                customerStatus = reader.ReadElementContentAsString()
                                                                            Case "CustomerType"
                                                                                customerType = reader.ReadElementContentAsString()
                                                                            Case "DWPIndicator"
                                                                                DWPIndicator = reader.ReadElementContentAsString()
                                                                            Case "PremiseAddressLine1"
                                                                                premiseAddressLine1 = reader.ReadElementContentAsString()
                                                                            Case "PremiseAddressLine2"
                                                                                premiseAddressLine2 = reader.ReadElementContentAsString()
                                                                            Case "PremiseAddressLine3"
                                                                                premiseAddressLine3 = reader.ReadElementContentAsString()
                                                                            Case "PremiseAddressTown"
                                                                                premiseAddressTown = reader.ReadElementContentAsString()
                                                                            Case "PremiseCounty"
                                                                                premiseCounty = reader.ReadElementContentAsString()
                                                                            Case "PremisePostcode"
                                                                                premisePostcode = reader.ReadElementContentAsString()
                                                                            Case "FAO"
                                                                                FAO = reader.ReadElementContentAsString()
                                                                            Case "AccountType"
                                                                                accountType = reader.ReadElementContentAsString()
                                                                            Case "DateOfBirth"
                                                                                DOB = reader.ReadElementContentAsString()
                                                                            Case "CurrentYearDebt"
                                                                                CurrentYearDebt = reader.ReadElementContentAsString()
                                                                            Case "PreviousYearDebt"
                                                                                previousYearDebt = reader.ReadElementContentAsString()
                                                                            Case "LegacyDebt"
                                                                                LegacyDebt = reader.ReadElementContentAsString()
                                                                            Case "AssociatedCustomer1"
                                                                                AssociatedCustomer1 = reader.ReadElementContentAsString()
                                                                            Case "AssociatedCustomer2"
                                                                                AssociatedCustomer2 = reader.ReadElementContentAsString()
                                                                            Case "AssociatedCustomer3"
                                                                                AssociatedCustomer3 = reader.ReadElementContentAsString()
                                                                            Case "AssociatedCustomer4"
                                                                                AssociatedCustomer4 = reader.ReadElementContentAsString()
                                                                            Case "BehaviouralScore"
                                                                                BehaviouralScore = reader.ReadElementContentAsString()
                                                                            Case "BankruptcyFlag"
                                                                                BankruptcyFlag = reader.ReadElementContentAsString()
                                                                            Case "DeceasedFlag"
                                                                                DeceasedFlag = reader.ReadElementContentAsString()
                                                                        End Select
                                                                        reader.Read()
                                                                    End While
                                                                    changesToCustomerFIle &= dcaCode & "," & recordType & "," & AccountNumber & "," & filler & "," & _
                                                                               FAStatusIndicator & "," & filler & "," & customerName & "," & customerAddressLine1 & "," & customerAddressLine2 & "," & _
                                                                               customerAddressLine3 & "," & customerAddressTown & "," & customerAddressCounty & "," & customerPostcode & "," & filler & "," & _
                                                                               filler & "," & leavingDate & "," & filler & "," & premiseAddressLine1 & "," & _
                                                                            premiseAddressLine2 & "," & premiseAddressLine3 & "," & premiseAddressTown & "," & _
                                                                              premiseCounty & "," & premisePostcode & "," & filler & "," & _
                                                                              customerStatus & "," & BankruptcyFlag & "," & DeceasedFlag & "," & customerTelNumber1 & "," & _
                                                                              customerTelNumber2 & "," & customerTelNumber3 & "," & customerTelNumber4 & "," & AssociatedCustomer1 & "," & _
                                                                               AssociatedCustomer2 & "," & AssociatedCustomer3 & "," & AssociatedCustomer4 & "," & FAO & "," & DOB & "," & _
                                                                               accountType & vbNewLine
                                                                    changeCases += 1
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                            End Select
                                                            reader.Read()
                                                        End While
                                                    Case "AdhocDetails"
                                                        reader.Read()
                                                        While reader.Name <> "AdhocDetails"
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            rdr_name = reader.Name
                                                            Select Case rdr_name
                                                                Case "AdhocDetail"
                                                                    reader.Read()
                                                                    dcaCode = ""
                                                                    recordType = ""
                                                                    AccountNumber = ""
                                                                    customerName = ""
                                                                    customerAddressLine1 = ""
                                                                    customerAddressLine2 = ""
                                                                    customerAddressLine3 = ""
                                                                    customerAddressTown = ""
                                                                    customerAddressCounty = ""
                                                                    customerPostcode = ""
                                                                    premiseAddressLine1 = ""
                                                                    premiseAddressLine2 = ""
                                                                    premiseAddressLine3 = ""
                                                                    premiseAddressTown = ""
                                                                    premiseCounty = ""
                                                                    premisePostcode = ""
                                                                    adhocValue = ""
                                                                    adhocDate = ""
                                                                    adhoctext = ""
                                                                    While reader.Name <> "AdhocDetail"
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        rdr_name = reader.Name
                                                                        Select Case rdr_name
                                                                            Case "DCACode"
                                                                                dcaCode = reader.ReadElementContentAsString
                                                                            Case "RecordType"
                                                                                recordType = reader.ReadElementContentAsString
                                                                            Case "AccountNumber"
                                                                                AccountNumber = reader.ReadElementContentAsString
                                                                            Case "CustomerName"
                                                                                customerName = reader.ReadElementContentAsString
                                                                            Case "CustomerAddressLine1"
                                                                                customerAddressLine1 = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressLine2"
                                                                                customerAddressLine2 = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressLine3"
                                                                                customerAddressLine3 = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressTown"
                                                                                customerAddressTown = reader.ReadElementContentAsString()
                                                                            Case "CustomerAddressCounty"
                                                                                customerAddressCounty = reader.ReadElementContentAsString()
                                                                            Case "CustomerPostcode"
                                                                                customerPostcode = reader.ReadElementContentAsString()
                                                                            Case "PremiseAddressLine1"
                                                                                premiseAddressLine1 = reader.ReadElementContentAsString()
                                                                            Case "PremiseAddressLine2"
                                                                                premiseAddressLine2 = reader.ReadElementContentAsString()
                                                                            Case "PremiseAddressLine3"
                                                                                premiseAddressLine3 = reader.ReadElementContentAsString()
                                                                            Case "PremiseAddressTown"
                                                                                premiseAddressTown = reader.ReadElementContentAsString()
                                                                            Case "PremiseCounty"
                                                                                premiseCounty = reader.ReadElementContentAsString()
                                                                            Case "PremisePostcode"
                                                                                premisePostcode = reader.ReadElementContentAsString()
                                                                            Case "AdHocValue"
                                                                                adhocValue = reader.ReadElementContentAsString()
                                                                            Case "AdHocDate"
                                                                                adhocDate = reader.ReadElementContentAsString()
                                                                            Case "AdHocText"
                                                                                adhoctext = reader.ReadElementContentAsString()
                                                                                adhoctext = Replace(adhoctext, ",", " ")
                                                                        End Select
                                                                        reader.Read()
                                                                    End While
                                                                    adhocFile &= dcaCode & "," & recordType & "," & AccountNumber & "," & customerName & "," & _
                                                                        customerAddressLine1 & "," & customerAddressLine2 & "," & customerAddressLine3 & "," & _
                                                                        customerAddressTown & "," & customerAddressCounty & "," & customerPostcode & "," & _
                                                                        filler & "," & filler & "," & _
                                                                        premiseAddressLine1 & "," & premiseAddressLine2 & "," & premiseAddressLine3 & "," & _
                                                                        premiseAddressTown & "," & premiseCounty & "," & premisePostcode & "," & _
                                                                        adhocValue & "," & adhocDate & "," & adhoctext & vbNewLine
                                                                    adHocCases += 1
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                            End Select
                                                            reader.Read()
                                                        End While
                                                    Case "QueriesResponse"
                                                        reader.Read()
                                                        While reader.Name <> "QueriesResponse"
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            rdr_name = reader.Name
                                                            Select Case rdr_name
                                                                Case "EDQueryResponse"
                                                                    reader.Read()
                                                                    While reader.Name <> "EDQueryResponse"
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        rdr_name = reader.Name
                                                                        Select Case rdr_name
                                                                            Case "RecordType"
                                                                                recordType = reader.ReadElementContentAsString

                                                                                'Case Else
                                                                                '  MsgBox("What is this tag?" & rdr_name)
                                                                        End Select
                                                                        reader.Read()
                                                                    End While
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                            End Select
                                                            reader.Read()
                                                        End While
                                                    Case "InformationStatusDetails"
                                                        reader.Read()
                                                        While reader.Name <> "InformationStatusDetails"
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            rdr_name = reader.Name
                                                            Select Case rdr_name
                                                                Case "InformationStatusDetail"
                                                                    reader.Read()
                                                                    dcaCode = ""
                                                                    recordType = ""
                                                                    AccountNumber = ""
                                                                    customerName = ""
                                                                    messageStatus = ""
                                                                    reason = ""
                                                                    AccountBalance = 0
                                                                    While reader.Name <> "InformationStatusDetail"
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        rdr_name = reader.Name
                                                                        Select Case rdr_name
                                                                            Case "DCACode"
                                                                                dcaCode = reader.ReadElementContentAsString
                                                                            Case "RecordType"
                                                                                recordType = reader.ReadElementContentAsString
                                                                            Case "AccountNumber"
                                                                                AccountNumber = reader.ReadElementContentAsString
                                                                            Case "CustomerName"
                                                                                customerName = reader.ReadElementContentAsString
                                                                            Case "MessageStatus"
                                                                                messageStatus = reader.ReadElementContentAsString
                                                                            Case "Reason"
                                                                                reason = reader.ReadElementContentAsString
                                                                            Case "AccountBalance"
                                                                                AccountBalance = reader.ReadElementContentAsString
                                                                        End Select
                                                                        reader.Read()
                                                                    End While
                                                                    'save information status in file
                                                                    informationStatusFile &= dcaCode & "," & recordType & "," & AccountNumber & "," & _
                                                                        customerName & "," & messageStatus & "," & reason & "," & AccountBalance & vbNewLine
                                                                    infoFound = True
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                            End Select
                                                            reader.Read()
                                                        End While
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                End Select
                                                reader.Read()
                                            End While
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                    End Select
                                    reader.Read()
                                End While
                            Case Else
                                MsgBox("What is this tag?" & rdr_name)
                                reader.Read()
                        End Select
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        reader.Read()
                    End While
                Case "Trailer"
                    reader.Read()
                    While reader.Name <> "Trailer"
                        rdr_name = reader.Name
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        reader.Read()
                    End While
                    If firstTrailer Then
                        auditFile = "                       wagy023o_Audit.txt" & vbNewLine
                        auditFile &= "File processes: " & OpenFileDialog1.FileName & vbNewLine
                        auditFile &= "File sequence number: " & fileSequence & vbNewLine & vbNewLine
                        auditFile &= "Errors found: 0" & vbNewLine
                        auditFile &= "Exceptions found: 0" & vbNewLine & vbNewLine
                    End If
                    firstTrailer = False
                    If CSID1No > 0 Then
                        filename = InputFilePath & CSID1 & "_wagy023o_NewDebt " & Format(Now, "dd.MM.yyyy") & " .txt"
                        My.Computer.FileSystem.WriteAllText(filename, newCaseFile1, False)
                        auditFile &= "Clientscheme - " & CSID1 & " Number of cases = " & CSID1No & vbNewLine
                        auditFile &= "Number of new cases: " & CSID1No & vbNewLine
                    End If
                    If CSID2No > 0 Then
                        filename = InputFilePath & CSID2 & "_wagy023o_NewDebt " & Format(Now, "dd.MM.yyyy") & " .txt"
                        My.Computer.FileSystem.WriteAllText(filename, newCaseFile2, False)
                        auditFile &= "Clientscheme - " & CSID2 & " Number of cases = " & CSID2No & vbNewLine & vbNewLine
                        auditFile &= "Number of new cases: " & CSID2No & vbNewLine
                    End If
                    newCaseFile1 = ""
                  
                    CSID1 = 0
                    CSID2 = 0
                    CSID1No = 0
                    CSID2No = 0
                Case Else
                    MsgBox("What is this tag?" & rdr_name)
                    reader.Read()
            End Select
        End While

        auditFile &= vbNewLine & "Total Number of new cases = " & newCases & vbNewLine
        auditFile &= "Total balance of new cases: " & totBalance & vbNewLine & vbNewLine
        'write out files
        If queryRequestFound Then
            filename = toClientName & "\wagy023o_QueryRequest " & Format(Now, "dd.MM.yyyy") & " .txt"
            My.Computer.FileSystem.WriteAllText(filename, queryRequestFile, False, ascii)
        End If

        If queryResponseFound Then
            filename = toClientName & "\wagy023o_QueryResponse " & Format(Now, "dd.MM.yyyy") & " .txt"
            My.Computer.FileSystem.WriteAllText(filename, queryResponseFile, False, ascii)
        End If
        If infoFound Then
            filename = toClientName & "\wagy023o_InformationStatus " & Format(Now, "dd.MM.yyyy") & " .xls"
            My.Computer.FileSystem.WriteAllText(filename, informationStatusFile, False)
        End If

        If dirPaycases > 0 Then
            filename = toClientName & "\wagy023o_DirectPayment " & Format(Now, "dd.MM.yyyy") & " .xls"
            My.Computer.FileSystem.WriteAllText(filename, directPaymentFile, False)
        End If

        If addChargecases > 0 Then
            filename = toClientName & "\wagy023o_AdditionalCharges " & Format(Now, "dd.MM.yyyy") & " .xls"
            My.Computer.FileSystem.WriteAllText(filename, additionalChargesFile, False)
        End If

        If adHocCases > 0 Then
            filename = toClientName & "\wagy023o_AdhocFile " & Format(Now, "dd.MM.yyyy") & " .xls"
            My.Computer.FileSystem.WriteAllText(filename, adhocFile, False)
        End If

        If FACases > 0 Then
            filename = toClientName & "\wagy023o_FinancialAdjustments " & Format(Now, "dd.MM.yyyy") & " .xls"
            My.Computer.FileSystem.WriteAllText(filename, financialAdjustmentsFile, False)
        End If

        If changeCases > 0 Then
            filename = toClientName & "\wagy023o_ChangesToCustomer " & Format(Now, "dd.MM.yyyy") & " .xls"
            My.Computer.FileSystem.WriteAllText(filename, changesToCustomerFIle, False)
        End If

        If ExceptionLog <> "" Then
            filename = toClientName & "\wagy023o_ExceptionLog " & Format(Now, "dd.MM.yyyy") & " .txt"
            My.Computer.FileSystem.WriteAllText(filename, ExceptionLog, False)
        End If

        If MBCNotes <> "" Then
            filename = toClientName & "\wagy023o_MBCNotes " & Format(Now, "dd.MM.yyyy") & " .txt"
            My.Computer.FileSystem.WriteAllText(filename, MBCNotes, False)
        End If

        'audit file
        auditFile &= placements_processed & " placements processed at " & Format(Now, "dd/MMM/yyyy hh:mm:ss") & vbNewLine & vbNewLine
        auditFile &= "Number of financial adjustments: " & FACases & vbNewLine
        auditFile &= "Total balance of financial adjustments: " & FAAdjustments & vbNewLine & vbNewLine

        auditFile &= "Number of additional charges: " & addChargecases & vbNewLine
        auditFile &= "Total balance of additional charges: " & addChargeBalances & vbNewLine & vbNewLine

        auditFile &= "Number of direct payments: " & dirPaycases & vbNewLine
        auditFile &= "Total balance of direct payments: " & dirPayAmounts & vbNewLine & vbNewLine

        auditFile &= "Number of changes to customer file: " & changeCases & vbNewLine & vbNewLine

        auditFile &= "Number of adhoc changes: " & adHocCases & vbNewLine


        filename = InputFilePath & "\wagy023o_Audit.txt"
        My.Computer.FileSystem.WriteAllText(filename, auditFile, False)

        MsgBox("Files saved")
        Me.Close()
    End Sub
    Private Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, Optional ByVal NoteLength As Integer = 250) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 2) ' + 2 is for :;
        Dim PreviousLength As Integer
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = (NoteTag & ":" & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)) & ";").PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function
    Private Function getCaseID(ByVal clientRef As String) As Integer
        Dim debtorID As Integer = 0
        debtorID = GetSQLResults("DebtRecovery", "SELECT D._rowID " & _
                                                   "FROM debtor D, clientscheme CS  " & _
                                                   "WHERE client_ref = '" & clientRef & "'" & _
                                                   " AND D.clientschemeID = CS._rowID " & _
                                                   " AND CS.clientID = 1662")

        Return debtorID
    End Function
End Class

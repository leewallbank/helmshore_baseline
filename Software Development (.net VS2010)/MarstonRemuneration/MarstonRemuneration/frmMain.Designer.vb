﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.btnSage = New System.Windows.Forms.Button()
        Me.lblProgress = New System.Windows.Forms.Label()
        Me.rtxtAudit = New System.Windows.Forms.RichTextBox()
        Me.prgMain = New System.Windows.Forms.ProgressBar()
        Me.btnCalculate = New System.Windows.Forms.Button()
        Me.btnImportEAs = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnSage
        '
        Me.btnSage.Location = New System.Drawing.Point(12, 109)
        Me.btnSage.Name = "btnSage"
        Me.btnSage.Size = New System.Drawing.Size(112, 30)
        Me.btnSage.TabIndex = 23
        Me.btnSage.Text = "Generate Sage"
        Me.btnSage.UseVisualStyleBackColor = True
        '
        'lblProgress
        '
        Me.lblProgress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblProgress.AutoSize = True
        Me.lblProgress.BackColor = System.Drawing.SystemColors.Control
        Me.lblProgress.Location = New System.Drawing.Point(245, 332)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(103, 13)
        Me.lblProgress.TabIndex = 22
        Me.lblProgress.Text = "Retrieving payments"
        Me.lblProgress.Visible = False
        '
        'rtxtAudit
        '
        Me.rtxtAudit.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtxtAudit.Location = New System.Drawing.Point(141, 12)
        Me.rtxtAudit.Name = "rtxtAudit"
        Me.rtxtAudit.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.rtxtAudit.Size = New System.Drawing.Size(440, 298)
        Me.rtxtAudit.TabIndex = 21
        Me.rtxtAudit.Text = ""
        '
        'prgMain
        '
        Me.prgMain.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.prgMain.Location = New System.Drawing.Point(12, 327)
        Me.prgMain.Name = "prgMain"
        Me.prgMain.Size = New System.Drawing.Size(569, 20)
        Me.prgMain.TabIndex = 20
        '
        'btnCalculate
        '
        Me.btnCalculate.Location = New System.Drawing.Point(12, 60)
        Me.btnCalculate.Name = "btnCalculate"
        Me.btnCalculate.Size = New System.Drawing.Size(112, 30)
        Me.btnCalculate.TabIndex = 19
        Me.btnCalculate.Text = "Calculate"
        Me.btnCalculate.UseVisualStyleBackColor = True
        '
        'btnImportEAs
        '
        Me.btnImportEAs.Location = New System.Drawing.Point(12, 12)
        Me.btnImportEAs.Name = "btnImportEAs"
        Me.btnImportEAs.Size = New System.Drawing.Size(112, 30)
        Me.btnImportEAs.TabIndex = 24
        Me.btnImportEAs.Text = "Import EAs"
        Me.btnImportEAs.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(593, 359)
        Me.Controls.Add(Me.btnImportEAs)
        Me.Controls.Add(Me.btnSage)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.rtxtAudit)
        Me.Controls.Add(Me.prgMain)
        Me.Controls.Add(Me.btnCalculate)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Marston Remuneration"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSage As System.Windows.Forms.Button
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents rtxtAudit As System.Windows.Forms.RichTextBox
    Friend WithEvents prgMain As System.Windows.Forms.ProgressBar
    Friend WithEvents btnCalculate As System.Windows.Forms.Button
    Friend WithEvents btnImportEAs As System.Windows.Forms.Button

End Class

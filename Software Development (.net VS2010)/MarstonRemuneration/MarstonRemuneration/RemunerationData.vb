﻿Imports CommonLibrary

Public Class RemunerationData
    Private RecoveredFeesTable As New DataTable
    Private SaleFeesTable As New DataTable

    Private Sage As New DataTable

    Public ReadOnly Property RecoveredFees() As DataTable
        Get
            RecoveredFees = RecoveredFeesTable
        End Get
    End Property

    Public ReadOnly Property SaleFees() As DataTable
        Get
            SaleFees = SaleFeesTable
        End Get
    End Property

    Public ReadOnly Property SageExtract() As DataTable
        Get
            SageExtract = Sage
        End Get
    End Property

    Public Function CommissionRate(ByVal EAID As Integer, ByVal WorkType As String) As Decimal
        Dim Sql As String

        CommissionRate = Nothing

        Try
            Sql = "EXEC dbo.GetCommRate " & EAID.ToString & ", '" & WorkType & "'"

            CommissionRate = GetSQLResults("MarstonRemuneration", Sql)

            CommissionRate = CommissionRate / 100

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Function VatRate(ByVal EffectiveDate As DateTime) As Decimal
        Dim Sql As String

        VatRate = Nothing

        Try
            Sql = "EXEC dbo.GetVATRate '" & EffectiveDate.ToString("dd/MMM/yyyy") & "'"

            VatRate = GetSQLResults("MarstonRemuneration", Sql)

            VatRate = VatRate / 100

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Sub GetRecoveredFees(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Dim Sql As String

        Try
            Sql = "SELECT cs.BranchID " & _
                  "     , cs.ClientID " & _
                  "     , s.work_type AS WorkTypeID " & _
                  "     , d.RemitID " & _
                  "     , s.work_type " & _
                  "     , b._rowID AS BailiffID " & _
                  "     , d.DebtorID " & _
                  "     , d.LinkID " & _
                  "     , d.PaymentID " & _
                  "     , d.split_fees+d.split_van AS FeesRemitted " & _
                  "     , v._rowID AS VisitID " & _
                  "     , v.date_allocated AS AllocationDate" & _
                  "     , v.date_visited AS VisitDate " & _
                  "     , f.date AS EnforcementFeeDate " & _
                  "     , b.name_fore " & _
                  "     , b.name_sur " & _
                  "     , b.vatReg " & _
                  "     , b.login_name AS LoginName " & _
                  "     , b.internalExternal " & _
                  "FROM ( " & _
                  "       SELECT d._rowID AS DebtorID " & _
                  "            , d.linkID " & _
                  "            , r._rowID AS RemitId " & _
                  "            , r.date AS RemittanceDate " & _
                  "            , d.ClientSchemeID " & _
                  "            , p._rowID AS PaymentID " & _
                  "            , p.split_fees " & _
                  "            , p.split_van " & _
                  "            , p.date AS PaymentDate " & _
                  "            , MAX(v._rowID) AS LastVisitID " & _
                  "       FROM debtor AS d " & _
                  "       INNER JOIN visit AS v ON d._rowID = v.debtorID " & _
                  "       INNER JOIN payment AS p ON d._rowid = p.debtorID " & _
                  "       INNER JOIN remit AS r ON p.status_remitID = r._rowid " & _
                  "       INNER JOIN bailiff AS b ON v.BailiffID = b._rowid " & _
                  "       WHERE p.status = 'R' " & _
                  "         AND DATE(r.date) BETWEEN '" & StartPeriod.ToString("yyyy-MM-dd") & "'" & _
                  "                              AND '" & EndPeriod.ToString("yyyy-MM-dd") & "'" & _
                  "         AND DATE(v.date_visited) <= p.date " & _
                  "         AND b.BranchID = 10 " & _
                  "         AND b.internalExternal = 'E' " & _
                  "         AND p.split_fees + p.split_van > 0 " & _
                  "       GROUP BY d._rowID " & _
                  "              , d.linkID " & _
                  "              , r._rowID " & _
                  "              , d.ClientSchemeID " & _
                  "              , p._RowID " & _
                  "              , p.split_fees " & _
                  "              , p.split_van " & _
                  "              , p.date " & _
                  "     ) AS d " & _
                  "INNER JOIN clientscheme AS cs ON d.ClientSchemeID = cs._rowid " & _
                  "INNER JOIN scheme       AS s  ON cs.schemeID      = s._rowID " & _
                  "INNER JOIN visit        AS v  ON d.debtorID       = v.DebtorID " & _
                  "                             AND d.LastVisitID    = v._rowID " & _
                  "INNER JOIN bailiff      AS b  ON v.BailiffID      = b._rowID " & _
                  "INNER JOIN fee          AS f  ON d.debtorID       = f.debtorID " & _
                  "WHERE (b.login_name LIKE 'fc%' OR b.login_name LIKE 'vb%') " & _
                  "  AND DATEDIFF(d.PaymentDate, v.date_visited) <= 120 " & _
                  "  AND v.BailiffID = ( SELECT v_s.BailiffID " & _
                  "                      FROM visit AS v_s USE INDEX (VisitDebtor) " & _
                  "                      INNER JOIN bailiff AS b_s ON v_s.BailiffID = b_s._rowID " & _
                  "                      WHERE v_s.debtorID = d.DebtorID " & _
                  "                        AND v_s.date_allocated IS NOT NULL " & _
                  "                        AND b_s.agent_type = 'B' " & _
                  "                        AND b_s.status = 'O' " & _
                  "                      ORDER BY v_s.date_allocated DESC, v_s._rowID DESC " & _
                  "                      LIMIT 1 " & _
                  "                    ) " & _
                  "  AND b.BranchID = 10 " & _
                  "  AND b.internalExternal = 'E' " & _
                  "	 AND f._rowID = ( SELECT MAX(f_s._rowID) " & _
                  "	                  FROM fee AS f_s " & _
                  "	                  WHERE f_s.debtorID = d.debtorID " & _
                  "	                    AND f_s.type = 'Enforcement' " & _
                  "	                )" & _
                  "  AND s.work_type <> 20 "

            LoadDataTable("DebtRecovery", Sql, RecoveredFeesTable, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function PreviouslyRecoveredFees(ByVal DebtorID As Integer, LinkID As Integer, RemitID As Integer, PaymentID As Integer, EnforcementFeeDate As DateTime) As Decimal
        Dim Sql As String

        PreviouslyRecoveredFees = Nothing

        Try
            Sql = "SELECT IFNULL(SUM(p_s.split_fees + p_s.split_van), 0) " & _
                  "FROM debtor AS d_s " & _
                  "INNER JOIN payment AS p_s ON d_s._rowID = p_s.DebtorID " & _
                  "WHERE p_s.status = 'R' "

            If LinkID > 0 Then
                Sql &= "  AND d_s.linkid = " & LinkID.ToString
            Else
                Sql &= "  AND d_s._rowID = " & DebtorID.ToString
            End If

            Sql &= "  AND p_s.status_remitID <= " & RemitID.ToString & _
                   "  AND p_s._rowid < " & PaymentID.ToString & _
                   "  AND p_s.split_fees + p_s.split_van > 0 " & _
                   "  AND EXISTS ( SELECT 1 " & _
                   "               FROM fee AS f_s " & _
                   "               WHERE f_s.DebtorID = d_s._rowID " & _
                   "                 AND f_s.date = '" & EnforcementFeeDate.ToString("yyyy-MM-dd HH:mm:ss") & "' " & _
                   "                 AND f_s.type = 'Enforcement' " & _
                   "             )"

            PreviouslyRecoveredFees = GetSQLResults("DebtRecovery", Sql)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Sub GetSaleFees(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Dim Sql As String

        Try
            Sql = "SELECT cs.BranchID " & _
                  "     , cs.ClientID " & _
                  "     , s.work_type AS WorkTypeID " & _
                  "     , d._rowid AS DebtorID " & _
                  "     , d.LinkID " & _
                  "     , v.BailiffID " & _
                  "     , v.date_allocated AS AllocationDate " & _
                  "     , v._rowID AS VisitID " & _
                  "     , v.date_visited AS VisitDate " & _
                  "     , f.date AS FeeDate " & _
                  "     , b.name_fore " & _
                  "     , b.name_sur " & _
                  "     , b.login_name AS LoginName" & _
                  "     , b.vatReg " & _
                  "     , b.internalExternal " & _
                  "     , ( SELECT COUNT(*) " & _
                  "         FROM debtor AS d_s" & _
                  "         INNER JOIN fee AS f_s ON d_s._rowID = f_s.debtorID " & _
                  "         WHERE d_s.LinkID = d.LinkID " & _
                  "           AND f_s.type = 'Sale or disposal' " & _
                  "           AND f_s.date = f.date " & _
                  "           AND d_s.status_open_closed <> 'C' " & _
                  "           AND d_s.status <> 'S' " & _
                  "       ) AS OpenLinkedCases " & _
                  "     , IFNULL(( SELECT SUM(p_s.split_debt) " & _
                  "                FROM debtor AS d_s " & _
                  "                INNER JOIN payment AS p_s ON d_s._rowID = p_s.debtorID " & _
                  "                WHERE (    (d.LinkID IS NOT NULL AND d_s.LinkID = d.LinkID) " & _
                  "                        OR (d.LinkID IS NULL     AND d_s._rowID = d._rowID) " & _
                  "                      ) " & _
                  "                  AND p_s.status_date >= f.date " & _
                  "                  AND p_s.status = 'R' " & _
                  "              ), 0) AS TotalDebt " & _
                  "     , ( SELECT v_s.BailiffID " & _
                  "         FROM visit AS v_s " & _
                  "         INNER JOIN bailiff AS b_s ON v_s.BailiffID = b_s._rowID " & _
                  "         WHERE v_s.debtorID = d._rowid " & _
                  "           AND v_s.date_allocated IS NOT NULL " & _
                  "           AND b_s.agent_type = 'B' " & _
                  "         ORDER BY v_s._rowID DESC " & _
                  "         LIMIT 1 " & _
                  "       ) AS LastAllocatedBailiff " & _
                  "FROM debtor AS d " & _
                  "INNER JOIN clientscheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "INNER JOIN scheme AS s ON cs.schemeID = s._rowID " & _
                  "INNER JOIN visit AS v ON d._rowid = v.debtorID " & _
                  "INNER JOIN bailiff AS b ON b._rowid = v.bailiffID " & _
                  "INNER JOIN fee AS f ON d._rowID = f.debtorID " & _
                  "WHERE cs.ClientID NOT IN (1,2,24) " & _
                  "  AND b.branchID = 10 " & _
                  "  AND b.status = 'O' " & _
                  "  AND b.internalExternal = 'E' " & _
                  "  AND b.typeSub <> 'ME' " & _
                  "  AND v._rowid = ( SELECT MAX(v_s._rowid) " & _
                  "                   FROM visit AS v_s " & _
                  "                   WHERE v_s.debtorID = d._rowid " & _
                  "                    AND v_s.date_visited IS NOT NULL " & _
                  "                 ) " & _
                  "  AND f.type = 'Sale or disposal' " & _
                  "  AND f.fee_amount > 0 " & _
                  "         AND DATE(d.return_date) BETWEEN '" & StartPeriod.ToString("yyyy-MM-dd") & "'" & _
                  "                              AND '" & EndPeriod.ToString("yyyy-MM-dd") & "'" & _
                  "  AND d.status_open_closed = 'C' " & _
                  "  AND d.status = 'S' "

            LoadDataTable("DebtRecovery", Sql, SaleFeesTable, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetSageExtract(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Dim Sql As String

        Try

            Sql = "EXEC dbo.GetSageExtract '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'"

            LoadDataTable("MarstonRemuneration", Sql, Sage, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub AddEAPayment(ByVal CaseID As String, _
                                ByVal LinkID As String, _
                                ByVal BailiffID As String, _
                                ByVal ClientID As String, _
                                ByVal WorkTypeID As String, _
                                ByVal VisitID As String, _
                                ByVal VisitDate As Date, _
                                ByVal AllocationDate As Date, _
                                ByVal EnforcementFeeDate As Nullable(Of Date), _
                                ByVal DebriefDate As Date, _
                                ByVal InvoiceNumber As String, _
                                ByVal InvoiceDate As Date, _
                                ByVal BasicPayment As Decimal, _
                                ByVal Vat As Decimal, _
                                ByVal StartPeriod As Date, _
                                ByVal EndPeriod As Date, _
                                ByVal BailiffSageAccount As String, _
                                ByVal InvoiceType As String, _
                                ByVal internalExternal As String)

        If LinkID = "" Then LinkID = "NULL"

        Dim Sql As String = "EXEC dbo.AddEAPayment " & CaseID & _
                                                   ", " & LinkID & _
                                                   ", " & BailiffID & _
                                                   ", " & ClientID & _
                                                   ", " & WorkTypeID & _
                                                   ", " & VisitID & _
                                                   ", '" & VisitDate.ToString("dd/MMM/yyyy") & "'" & _
                                                   ", '" & AllocationDate.ToString("dd/MMM/yyyy") & "'"

        If Not IsNothing(EnforcementFeeDate) Then
            Sql &= ",'" & CDate(EnforcementFeeDate).ToString("dd/MMM/yyyy") & "'"
        Else
            Sql &= ",NULL"
        End If

        Sql &= ", '" & DebriefDate.ToString("dd/MMM/yyyy") & "'" & _
               ", '" & InvoiceNumber & "'" & _
               ", '" & InvoiceDate.ToString("dd/MMM/yyyy") & "'" & _
               ", " & BasicPayment.ToString("0.00") & _
               ", " & Vat.ToString("0.00") & _
               ", '" & StartPeriod.ToString("dd/MMM/yyyy") & "'" & _
               ", '" & EndPeriod.ToString("dd/MMM/yyyy") & "'" & _
               ", '" & BailiffSageAccount & "'" & _
               ", '" & InvoiceType & "'" & _
               ", '" & internalExternal & "'"

        ExecStoredProc("MarstonRemuneration", Sql, 0)
    End Sub

    Public Sub UpdateInvoiceTotals(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Try
            ExecStoredProc("MarstonRemuneration", "EXEC dbo.UpdateInvoiceTotals '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'", 0)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub ImportEAs()
        Try
            ExecStoredProc("MarstonRemuneration", "EXEC stg.ImportEA")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function GetPayingCase(ByVal CaseID As String, LinkID As String, InvoiceType As String, FeeDate As DateTime) As String
        Dim Sql As String
        Dim PayingCase As String

        GetPayingCase = ""

        Try
            If LinkID = "" Then
                Sql = "SELECT p.CaseID " & _
                      "FROM dbo.EAPayment AS p " & _
                      "INNER JOIN dbo.EAInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                      "WHERE p.CaseID = " & CaseID & _
                      "  AND p.FeeDate = '" & FeeDate.ToString("dd/MMM/yyyy") & "'" & _
                      "  AND i.InvoiceType = '" & InvoiceType & "'"
            Else
                Sql = "SELECT p.CaseID " & _
                      "FROM dbo.EAPayment AS p " & _
                      "INNER JOIN dbo.EAInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                      "WHERE p.LinkID = " & LinkID & _
                      "  AND p.FeeDate = '" & FeeDate.ToString("dd/MMM/yyyy") & "'" & _
                      "  AND i.InvoiceType = '" & InvoiceType & "'"
            End If

            PayingCase = GetSQLResults("MarstonRemuneration", Sql)

            If IsDBNull(PayingCase) Then PayingCase = ""

            GetPayingCase = PayingCase

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function
End Class

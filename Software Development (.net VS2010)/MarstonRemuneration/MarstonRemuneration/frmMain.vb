﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO

Imports CommonLibrary

Public Class frmMain

    Private Remuneration As New RemunerationData
    Private AuditFile As String
    Private StartPeriod As Date, EndPeriod As Date, InvoiceDate As Date = Today
    Private Vat As Decimal = 0, VatRate As Decimal = 0, Payment As Decimal = 0, CommissionRate As Decimal = 0

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        EndPeriod = DateAdd(DateInterval.Day, -Now.DayOfWeek, Now.Date)
        StartPeriod = DateAdd(DateInterval.Day, -6, EndPeriod)
    End Sub

    Private Sub btnCalculate_Click(sender As Object, e As System.EventArgs) Handles btnCalculate.Click
        Try

            Dim FolderDialog As New FolderBrowserDialog

            If Not FolderDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            VatRate = Remuneration.VatRate(Today)

            AuditFile = FolderDialog.SelectedPath & "\Process_"

            rtxtAudit.Clear()

            CalculateRecoveredFees()

            CalculateSaleFees()

            Remuneration.UpdateInvoiceTotals(StartPeriod, EndPeriod)

            prgMain.Value = 0

            MsgBox("Calculation complete", MsgBoxStyle.OkOnly, Me.Text)

        Catch ex As Exception

        End Try
    End Sub
    Private Sub CalculateRecoveredFees()

        Dim CaseDetail As String = ""

        Try

            Dim pdfDoc As New Document()
            pdfDoc.Open()
            Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(AuditFile & "RecoveredFees_audit_" & InvoiceDate.ToString("yyyyMMdd") & ".pdf", FileMode.Append))
            Dim ev As New itsEvents
            pdfWrite.PageEvent = ev
            pdfDoc.Open()

            lblProgress.Visible = True
            Application.DoEvents()
            Remuneration.GetRecoveredFees(StartPeriod, EndPeriod)
            lblProgress.Visible = False

            prgMain.Maximum = Remuneration.RecoveredFees.Rows.Count

            For Each RecoveredFee As DataRow In Remuneration.RecoveredFees.Rows
                prgMain.Value += 1
                CaseDetail = ""
                Payment = 0
                Vat = 0
                Application.DoEvents()

                CaseDetail = "CaseID " & RecoveredFee("DebtorID") & vbCrLf
                CaseDetail &= "EA: " & RecoveredFee("name_sur") & ", " & RecoveredFee("name_fore") & "[" & RecoveredFee("BailiffID") & "]" & " visited: " & RecoveredFee("VisitDate") & vbCrLf

                Dim PreviouslyRecoveredFees As Decimal = Remuneration.PreviouslyRecoveredFees(RecoveredFee("DebtorID"), If(IsDBNull(RecoveredFee("LinkID")), -1, RecoveredFee("LinkID")), RecoveredFee("RemitID"), RecoveredFee("PaymentID"), RecoveredFee("EnforcementFeeDate"))

                CaseDetail &= "Recovered fees of " & RecoveredFee("FeesRemitted").ToString & " on paymentID " & RecoveredFee("PaymentID").ToString & ". Previously recovered fees " & PreviouslyRecoveredFees.ToString("0.00") & vbCrLf

                Dim Cap As Decimal

                Select Case RecoveredFee("work_type")

                    Case 2
                        Cap = 750
                        CaseDetail &= "CTax case. Cap of " & Cap.ToString("0.00") & vbCrLf
                        CommissionRate = Remuneration.CommissionRate(RecoveredFee("BailiffID"), "CTax")

                    Case 3
                        Cap = 1250
                        CaseDetail &= "NNDR case. Cap of " & Cap.ToString("0.00") & vbCrLf
                        CommissionRate = Remuneration.CommissionRate(RecoveredFee("BailiffID"), "NNDR")

                    Case Else
                        CaseDetail &= "Invalid work type" & vbCrLf
                        Cap = 0
                        CommissionRate = 0

                End Select

                If PreviouslyRecoveredFees < Cap Then
                    Dim CommissionableFee As Decimal = 0
                    If PreviouslyRecoveredFees + RecoveredFee("FeesRemitted") <= Cap Then
                        CommissionableFee = RecoveredFee("FeesRemitted")
                    Else
                        CommissionableFee = Cap - PreviouslyRecoveredFees
                    End If
                    CaseDetail &= "Commissionable amount " & CommissionableFee.ToString("0.00") & vbCrLf
                    Payment = CommissionableFee * CommissionRate
                Else
                    CaseDetail &= "Recovered fees exceed cap" & vbCrLf
                End If

                CaseDetail &= "Payment of " & Payment.ToString("0.00") & vbCrLf & vbCrLf

                If Payment > 0 Then

                    If Not IsDBNull(RecoveredFee("VatReg")) AndAlso RecoveredFee("VatReg").ToString.Length > 0 Then
                        Vat = Payment * VatRate
                    End If

                    Remuneration.AddEAPayment(RecoveredFee("DebtorId"), _
                                              If(IsDBNull(RecoveredFee("LinkID")), "", RecoveredFee("LinkID")), _
                                              RecoveredFee("BailiffId"), _
                                              RecoveredFee("ClientId"), _
                                              RecoveredFee("WorkTypeId"), _
                                              RecoveredFee("VisitID"), _
                                              RecoveredFee("VisitDate"), _
                                              If(IsDBNull(RecoveredFee("AllocationDate")), CDate("01-01-1900"), RecoveredFee("AllocationDate")), _
                                              RecoveredFee("EnforcementFeeDate"), _
                                              InvoiceDate, _
                                              "MAR" & InvoiceDate.ToString("yyyyMMdd") & RecoveredFee("BailiffId").ToString, _
                                              InvoiceDate, _
                                              Payment, _
                                              Vat, _
                                              StartPeriod, _
                                              EndPeriod, _
                                              If(IsDBNull(RecoveredFee("LoginName")), "", RecoveredFee("LoginName")), _
                                              "MAR", _
                                              RecoveredFee("internalExternal")
                                              )

                End If

                rtxtAudit.AppendText(CaseDetail)

                Dim Para As New Paragraph(CaseDetail)
                Para.Font.Size = 6
                pdfDoc.Add(Para)

            Next RecoveredFee

            pdfDoc.Close()

            prgMain.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Sub

    Private Sub CalculateSaleFees()

        Dim CaseDetail As String = "", PayingCase As String = ""

        Try

            Dim pdfDoc As New Document()
            pdfDoc.Open()
            Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(AuditFile & "SaleFees_audit_" & InvoiceDate.ToString("yyyyMMdd") & ".pdf", FileMode.Append))
            Dim ev As New itsEvents
            pdfWrite.PageEvent = ev
            pdfDoc.Open()

            lblProgress.Visible = True
            Application.DoEvents()
            Remuneration.GetSaleFees(StartPeriod, EndPeriod)
            lblProgress.Visible = False

            prgMain.Value = 1
            prgMain.Maximum = Remuneration.SaleFees.Rows.Count + 1

            For Each SaleFee As DataRow In Remuneration.SaleFees.Rows
                CaseDetail = ""
                Payment = 0
                Vat = 0
                PayingCase = ""
                Application.DoEvents()

                CaseDetail = "CaseID " & SaleFee("DebtorID") & vbCrLf
                CaseDetail &= "EA: " & SaleFee("name_sur") & ", " & SaleFee("name_fore") & "[" & SaleFee("BailiffID") & "]" & " visited: " & SaleFee("VisitDate") & vbCrLf

                PayingCase = Remuneration.GetPayingCase(SaleFee("DebtorID"), If(IsDBNull(SaleFee("linkID")), "", SaleFee("LinkID")), "SF", SaleFee("FeeDate"))

                If PayingCase = "" Then

                    If SaleFee("BailiffID") = SaleFee("LastAllocatedBailiff") Then

                        If SaleFee("OpenLinkedCases") = 0 Then
                            Payment = 20

                            If SaleFee("TotalDebt") > 1500 Then
                                Dim LargeBalanceBonus As Decimal
                                LargeBalanceBonus = Math.Floor((Math.Min(SaleFee("TotalDebt"), 26500) - 1500) / 100)
                                CaseDetail &= "Large balance bonus of " & LargeBalanceBonus.ToString("0.00").ToString & vbCrLf
                                Payment += LargeBalanceBonus
                            End If

                        Else

                            CaseDetail &= "Not all sale fee cases are closed." & vbCrLf

                        End If

                        CaseDetail &= "Payment of " & Payment.ToString("0.00") & vbCrLf & vbCrLf

                        If Payment > 0 Then

                            If Not IsDBNull(SaleFee("VatReg")) AndAlso SaleFee("VatReg").ToString.Length > 0 Then
                                Vat = Payment * VatRate
                            End If

                            Remuneration.AddEAPayment(SaleFee("DebtorId"), _
                                                      If(IsDBNull(SaleFee("LinkID")), "", SaleFee("LinkID")), _
                                                      SaleFee("BailiffId"), _
                                                      SaleFee("ClientId"), _
                                                      SaleFee("WorkTypeId"), _
                                                      SaleFee("VisitID"), _
                                                      SaleFee("VisitDate"), _
                                                      If(IsDBNull(SaleFee("AllocationDate")), CDate("01-01-1900"), SaleFee("AllocationDate")), _
                                                      SaleFee("FeeDate"), _
                                                      InvoiceDate, _
                                                      "SF" & InvoiceDate.ToString("yyyyMMdd") & SaleFee("BailiffId").ToString, _
                                                      InvoiceDate, _
                                                      Payment, _
                                                      Vat, _
                                                      StartPeriod, _
                                                      EndPeriod, _
                                                      If(IsDBNull(SaleFee("LoginName")), "", SaleFee("LoginName")), _
                                                      "SF", _
                                                      SaleFee("internalExternal")
                                                      )

                        End If

                    Else

                        CaseDetail &= "Case reallocated" & vbCrLf & vbCrLf

                    End If
                Else
                    CaseDetail &= "Case already paid on" & vbCrLf & vbCrLf
                End If

                rtxtAudit.AppendText(CaseDetail)

                Dim Para As New Paragraph(CaseDetail)
                Para.Font.Size = 6
                pdfDoc.Add(Para)

                prgMain.Value += 1

            Next SaleFee

            If Remuneration.SaleFees.Rows.Count > 0 Then pdfDoc.Close()

            prgMain.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Sub

    Private Sub frmMain_Scroll(sender As Object, e As System.Windows.Forms.ScrollEventArgs) Handles Me.Scroll
        lblProgress.BackColor = Color.Transparent
    End Sub

    Private Sub GenerateSageExtract(FileName As String)

        Remuneration.GetSageExtract(StartPeriod, EndPeriod)

        Using sw As New StreamWriter(FileName, False)

            For Each Row As DataRow In Remuneration.SageExtract.Rows

                Dim sb As New System.Text.StringBuilder
                For ColIdx As Integer = 0 To Remuneration.SageExtract.Columns.Count - 1

                    If ColIdx <> 2 And ColIdx <> 15 Then sb.Append("""")

                    If ColIdx <> 4 Then
                        sb.Append(Row.Item(ColIdx).ToString)
                    Else
                        sb.Append(CDate(Row.Item(ColIdx)).ToString("dd/MM/yyyy"))
                    End If

                    If ColIdx <> 2 And ColIdx <> 15 Then sb.Append("""")

                    If ColIdx < Remuneration.SageExtract.Columns.Count - 1 Then sb.Append(",")
                Next
                sw.WriteLine(sb.ToString)
            Next
        End Using
    End Sub

    Private Sub btnSage_Click(sender As Object, e As System.EventArgs) Handles btnSage.Click
        Try
            Dim FolderDialog As New FolderBrowserDialog

            If Not FolderDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            GenerateSageExtract(FolderDialog.SelectedPath & "\Marston_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv")

            MsgBox("Sage extract generated", MsgBoxStyle.OkOnly, Me.Text)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnImportEAs_Click(sender As Object, e As System.EventArgs) Handles btnImportEAs.Click
        Try

            Remuneration.ImportEAs()

            MessageBox.Show("EAs imported", "Marston Remuneration", MessageBoxButtons.OK, MessageBoxIcon.Information)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

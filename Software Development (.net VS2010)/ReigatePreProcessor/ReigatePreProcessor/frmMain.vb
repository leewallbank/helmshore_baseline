﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim OutputFile As String = "Client Reference|1st. Liable Name|Other Names|FwdAddress1|FwdAddress2|" & _
                "FwdAddress3|FwdAddress4|FwdAddress5|DebtAddress1|DebtAddress2|DebtAddress3|DebtAddress4|" & _
                "DebtAddress5|DebtAddress6|Debt|CourtCosts|LiabilityOrderAmount|CurrentBalance|FromDate|" & _
                "ToDate|WarrantDate|CourtName|CourtAddress|Tel1|tel2|Email" & vbNewLine
            Dim totValue As Decimal = 0
            Dim totCases As Integer = 0
            Dim clientREf, hearingDate, courtName, courtAddress As String
            Dim name1, otherNames, tel1, tel2, email As String
            Dim fromdate, todate As Date
            Dim debtAmt, costs, loAmt, CurrentBalance As Decimal
            Dim startIDX As Integer
            Dim InputLine As String
            Dim addr1, addr2, addr3, addr4, addr5 As String
            Dim daddr1, daddr2, daddr3, daddr4, daddr5, daddr6 As String
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents)
            Dim rowMax As Integer = UBound(FileContents)
            For rowIDX = 0 To rowMax
                InputLine = (FileContents(rowIDX))
                'get client ref
                startIDX = InStr(InputLine, "Account No:")
                While startIDX = 0
                    Continue For
                End While
                clientREf = Microsoft.VisualBasic.Right(InputLine, InputLine.Length - startIDX - 11)
                totCases += 1
                'now get Name/Correspondence

                startIDX = 0
                While startIDX = 0
                    rowIDX += 1
                    InputLine = (FileContents(rowIDX))
                    startIDX = InStr(InputLine, "Name/Correspondence")
                End While
                'ignore next line if dashes
                rowIDX += 1
                name1 = ""
                InputLine = (FileContents(rowIDX))
                If Mid(InputLine, 5, 5) = "-----" Then
                    rowIDX += 1
                End If
                InputLine = (FileContents(rowIDX))
                name1 = Trim(Microsoft.VisualBasic.Left(InputLine, 40))
                Try
                    fromdate = CDate(Mid(InputLine, 84, 12))
                Catch ex As Exception
                    fromdate = Nothing
                End Try
                debtAmt = Microsoft.VisualBasic.Right(InputLine, 14)
                'next line is addrline 1, debt addr line 1, to date and costs
                rowIDX += 1
                InputLine = (FileContents(rowIDX))
                addr1 = Trim(Microsoft.VisualBasic.Left(InputLine, 40))
                daddr1 = Trim(Mid(InputLine, 40, 42))
                Try
                    todate = CDate(Mid(InputLine, 84, 12))
                Catch ex As Exception
                    todate = Nothing
                End Try
                costs = Microsoft.VisualBasic.Right(InputLine, 14)
                'next line is addr line 2 and debt addr line2
                rowIDX += 1
                InputLine = (FileContents(rowIDX))
                addr2 = Trim(Microsoft.VisualBasic.Left(InputLine, 40))
                daddr2 = Trim(Mid(InputLine, 40, 42))
                'next line is addr line 3 and debt addr line3 and LO amt
                rowIDX += 1
                InputLine = (FileContents(rowIDX))
                addr3 = Trim(Microsoft.VisualBasic.Left(InputLine, 40))
                daddr3 = Trim(Mid(InputLine, 40, 42))
                loAmt = Microsoft.VisualBasic.Right(InputLine, 14)
                'next line is addr line 4 and debt addr line4
                rowIDX += 1
                InputLine = (FileContents(rowIDX))
                addr4 = Trim(Microsoft.VisualBasic.Left(InputLine, 40))
                daddr4 = Trim(Mid(InputLine, 40, 42))
                'next line is addr line 5 and debt addr line5 and Current balance
                rowIDX += 1
                InputLine = (FileContents(rowIDX))
                addr5 = Trim(Microsoft.VisualBasic.Left(InputLine, 40))
                daddr5 = Trim(Mid(InputLine, 40, 42))
                CurrentBalance = Microsoft.VisualBasic.Right(InputLine, 14)
                totValue += CurrentBalance
                'next line is addr line 6 and debt addr line6
                rowIDX += 1
                InputLine = (FileContents(rowIDX))
                addr5 &= " " & Trim(Microsoft.VisualBasic.Left(InputLine, 40))
                daddr6 = Trim(Mid(InputLine, 40, 42))
                'now look for phone numbers up to Hearing Date & Time  or Account No:
                startIDX = 0
                tel1 = ""
                tel2 = ""
                email = ""
                otherNames = ""
                'look for "Telephone Contact"
                While startIDX = 0
                    rowIDX += 1
                    InputLine = (FileContents(rowIDX))
                    Dim otherIDx As Integer = InStr(InputLine, "Other Names:")
                    If otherIDx > 0 Then
                        otherNames = Microsoft.VisualBasic.Right(InputLine, InputLine.Length - otherIDx - 12)
                    End If
                    Dim telIDX = InStr(InputLine, "Telephone Contact")
                    If telIDX > 0 Then
                        If tel1 = "" Then
                            tel1 = Trim(Microsoft.VisualBasic.Right(InputLine, InputLine.Length - telIDX - 17))
                        ElseIf tel2 = "" Then
                            tel2 = Trim(Microsoft.VisualBasic.Right(InputLine, InputLine.Length - telIDX - 17))
                        Else
                            MsgBox("3 telephone numbers - need to add notes")
                        End If
                    End If
                    'Home Telephone Number
                    telIDX = InStr(InputLine, "Home Telephone Number")
                    If telIDX > 0 Then
                        If tel1 = "" Then
                            tel1 = Trim(Microsoft.VisualBasic.Right(InputLine, InputLine.Length - telIDX - 21))
                        ElseIf tel2 = "" Then
                            tel2 = Trim(Microsoft.VisualBasic.Right(InputLine, InputLine.Length - telIDX - 21))
                        Else
                            MsgBox("3 telephone numbers - need to add notes")
                        End If
                    End If
                    'Contact Telephone 
                    telIDX = InStr(InputLine, "Contact Telephone")
                    If telIDX > 0 Then
                        If tel1 = "" Then
                            tel1 = Trim(Microsoft.VisualBasic.Right(InputLine, InputLine.Length - telIDX - 17))
                        ElseIf tel2 = "" Then
                            tel2 = Trim(Microsoft.VisualBasic.Right(InputLine, InputLine.Length - telIDX - 17))
                        Else
                            MsgBox("3 telephone numbers - need to add notes")
                        End If
                    End If

                    'email
                    Dim emailIDX As Integer = InStr(InputLine, "EMAIL ADDRESS")
                    If emailIDX > 0 Then
                        If email = "" Then
                            email = Trim(Microsoft.VisualBasic.Right(InputLine, InputLine.Length - emailIDX - 13))
                        Else
                            MsgBox("2 emails - need to add notes")
                        End If
                    End If
                    startIDX = InStr(InputLine, "Hearing Date & Time") + InStr(InputLine, "Account No:")
                    If rowIDX = rowMax Then
                        Exit While
                    End If
                End While
                courtAddress = ""
                courtName = ""
                If InStr(InputLine, "Hearing Date & Time") > 0 Then
                    'hearing date MAYBE on next line
                    hearingDate = Mid(InputLine, 32, 40)
                    'next line is court name
                    rowIDX += 1
                    InputLine = (FileContents(rowIDX))
                    courtName = Mid(InputLine, 32, 40)
                    'next line is courtaddress
                    rowIDX += 1
                    InputLine = (FileContents(rowIDX))
                    courtAddress = Mid(InputLine, 32, 40)
                Else
                    'need to put rowidx back 1 as already pointing at account no
                    rowIDX -= 1
                End If
                'write out output file
                OutputFile &= clientREf & "|" & name1 & "|" & otherNames & "|" & addr1 & "|" & addr2 & "|" & addr3 & "|" & _
                    addr4 & "|" & addr5 & "|" & daddr1 & "|" & daddr2 & "|" & daddr3 & "|" & daddr4 & "|" & _
                    daddr5 & "|" & daddr6 & "|" & debtAmt & "|" & Format(costs, "#") & "|" & loAmt & "|" & CurrentBalance & _
                    "|" & Format(fromdate, "dd-MM-yyyy") & "|" & Format(todate, "dd-MM-yyyy") & "|" & _
                hearingDate & "|" & courtName & "|" & _
                courtAddress & "|" & tel1 & "|" & tel2 & "|" & email & vbNewLine
            Next
            btnViewInputFile.Enabled = True
            If totCases > 0 Then
                WriteFile(InputFilePath & FileName & "_preprocess.txt", OutputFile)
                btnViewOutputFile.Enabled = True
            End If


            MsgBox("Cases = " & totCases & " Value " & totValue & vbNewLine)
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_preprocess.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_preprocess.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & "_nonWRP_4200.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_nonWRP_4200.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

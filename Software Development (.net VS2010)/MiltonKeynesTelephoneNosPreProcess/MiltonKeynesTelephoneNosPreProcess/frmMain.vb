﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String, ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String
    Private ConnCode As New Dictionary(Of String, Integer)
    Private OutputFiles As New ArrayList()
    Private NewCases As New ArrayList()
    Private TelNumData As New clsTelNumData

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim TelNumFile As String = "", TelNum As String, Trailer As String = ""
        Dim LineNumber As Integer, LineCount As Integer, TelNumCount As Integer, TrailerCount As Integer, CaseIDCount As Integer

        Try
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)

            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents) + 2 ' audit file and output file

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                Application.DoEvents() ' without this line, the button disappears until processing is complete

                If LineNumber < 5 Or LineNumber = UBound(FileContents) Then Continue For
                If LineNumber = UBound(FileContents) + 1 Then
                    Trailer = InputLine
                    Continue For
                End If

                LineCount += 1

                TelNum = InputLine.Substring(44, 14).Replace(" ", "")
                If TelNum <> "" Then
                    TelNumData.GetCaseID(InputLine.Substring(0, 12).Trim, InputLine.Substring(114, 10).Trim, dtpLoaded.Value)

                    If TelNumData.TelephoneNumbers.Rows.Count = 0 Then
                        ExceptionLog &= "No case ID found for account ref " & InputLine.Substring(0, 12).Trim & " at line number " & LineNumber.ToString & vbCrLf
                    ElseIf TelNumData.TelephoneNumbers.Rows.Count > 1 Then
                        ExceptionLog &= "Multiple case IDs found for account ref " & InputLine.Substring(0, 12).Trim & " at line number " & LineNumber.ToString & vbCrLf
                    End If

                    For Each CaseID As DataRow In TelNumData.TelephoneNumbers.Rows ' Should only every be one but just in case...
                        TelNumFile &= CaseID.Item(0) & "," & TelNum & vbCrLf
                        CaseIDCount += 1
                    Next CaseID
                    TelNumCount += 1

                End If
            Next InputLine

            If IsNumeric(Trailer.Substring(0, Trailer.IndexOf(" "))) Then
                TrailerCount = CInt(Trailer.Substring(0, Trailer.IndexOf(" ")))
                If TrailerCount <> LineCount Then ErrorLog &= "Incorrect trailer record count at line number " & LineNumber.ToString & ". Trailer: " & TrailerCount.ToString & " Actual: " & LineCount.ToString & vbCrLf
            Else
                ErrorLog &= "Cannot read trailer record count at line number " & LineNumber.ToString & vbCrLf
            End If

            ProgressBar.Value += 1
            AuditLog = "File processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            ProgressBar.Value += 1

            If TelNumFile <> "" Then
                WriteFile(InputFilePath & FileName & "_TelephoneNumbers.txt", TelNumFile)
                OutputFiles.Add(InputFilePath & FileName & "_TelephoneNumbers.txt")
                AuditLog &= "Number of telephone numbers: " & TelNumCount.ToString & vbCrLf
                AuditLog &= "Number of case IDs: " & CaseIDCount.ToString & vbCrLf
            End If

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)
            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", ExceptionLog)

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Clipboard.SetText("notepad.exe " & InputFilePath & FileName & FileExt)
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If AuditLog <> "" And File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If ErrorLog <> "" And File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If ExceptionLog <> "" And File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub
End Class

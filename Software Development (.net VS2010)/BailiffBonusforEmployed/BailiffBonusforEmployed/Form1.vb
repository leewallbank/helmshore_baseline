﻿Public Class Form1
    Dim upd_txt As String
    Dim periodstartDate As Date = CDate("Dec 1, 2014")
    Dim bailID As Integer
    Dim debtorArray(100) As case_str
    Dim arrayCount As Integer
    Dim bailTier As String
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("FeesSQL")
        ConnectDbremun("bailiffremuneration")

        'clear down tale for period start
        upd_txt = "delete from BailiffBonusPayments  " & _
         " where PaidDate >= '" & Format(periodstartDate, "yyyy-MM-dd HH:mm:ss") & "'"

        update_sql(upd_txt)
        Dim bail_dt As New DataTable

        Dim visitStartDate As Date = DateAdd(DateInterval.Day, -180, periodstartDate)

        '
        LoadDataTable2("DebtRecovery", "SELECT _rowID, internalExternal" & _
                                 " FROM Bailiff " & _
                                 " WHERE agent_type = 'B'" & _
                                 " and status = 'O'" & _
                                 " and add_mobile_name <> 'none'" & _
                                 " and typeSub = 'Employed'" & _
                                 " order by _rowID", bail_dt, False)

        For Each Bailrow In bail_dt.Rows
            bailID = Bailrow(0)
            bailTier = Bailrow(1)
            'get triggers
            'Dim remun_dt As New DataTable
            'Dim trigger As Integer = 0
            'LoadDataTable2("bailiffremuneration", "select Turnaround, Complaints, FieldAdministration, BWV, Visited" & _
            '               " from Bailiff" & _
            '               " where BailiffID = " & bailID, remun_dt, False)
            'For Each remunRow In remun_dt.Rows
            '    If remunRow(0) Then
            '        trigger += 1
            '    End If
            '    If remunRow(1) Then
            '        trigger += 1
            '    End If
            '    If remunRow(2) Then
            '        trigger += 1
            '    End If
            '    If remunRow(3) Then
            '        trigger += 1
            '    End If
            '    If remunRow(4) Then
            '        trigger += 1
            '    End If
            'Next
            unlinked_cases()
            linked_cases()
        Next
        Me.Close()
    End Sub
    Private Sub unlinked_cases()
        Dim debt_dt As New DataTable
        'first get unlinked cases
        'get successful cases where bailiff has visited
        LoadDataTable2("DebtRecovery", "select D._rowID, D.clientSchemeID, max(V.date_allocated), max(P.date), max(P._rowID), max(P.status_date), min(V.date_visited)" & _
                       " from Visit V, Debtor D, Payment P " & _
                       " where V.bailiffID = " & bailID & _
                       " and P.debtorID = D._rowID" & _
                       " and D.status = 'S'" & _
                       " and P.status = 'R'" & _
                       " and date_visited < P.date" & _
                       " and linkID is null " & _
                       " and V.debtorID = D._rowID" & _
                       " and P.status_date > '" & Format(periodstartDate, "yyyy-MM-dd") & "'" & _
                       " group by D._rowID, D.clientSchemeID" & _
                       " order by D._rowID", debt_dt, False)

        For Each debtRow In debt_dt.Rows
            Dim debtorID As Integer = debtRow(0)
            Dim CSID As Integer = debtRow(1)
            Dim clientID As Integer
            clientID = GetSQLResults2("DebtRecovery", "select clientID " & _
                                             " from clientScheme" & _
                                             " where _rowid = " & CSID)
            If clientID = 1 Or clientID = 2 Or clientID = 24 Then
                Continue For
            End If
            'check no waiting payment
            Dim waitingPayment As Integer
            waitingPayment = GetSQLResults2("DebtRecovery", "select count(*) from Payment" & _
                                          " where debtorID = " & debtorID & _
                                          " and status = 'W'")
            If waitingPayment > 0 Then
                Continue For
            End If

            'check last visit is by this bailiff
            Dim visit_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select B._rowID from Visit V, bailiff B" & _
                           " where debtorID = " & debtorID & _
                           " and B._rowid = bailiffID " & _
                           " and agent_type = 'B'" & _
                           " and add_mobile_name <> 'none'" & _
                           " order by date_allocated desc", visit_dt, False)
            Dim ignore_case As Boolean = False
            For Each visitRow In visit_dt.Rows
                If visitRow(0) <> bailID Then
                    ignore_case = True
                End If
                Exit For
            Next
            If ignore_case Then
                Continue For
            End If
            Dim fee_dt As New DataTable
            'check enforcement fee or sum fees > 42.50
            LoadDataTable2("DebtRecovery", "select fee_amount, type from Fee" & _
                           " where debtorID = " & debtorID & _
                           " and fee_amount > 0 " & _
                           " and fee_remit_col > 2", fee_dt, False)
            Dim EnfFeeFound As Boolean = False
            Dim feeAmt As Decimal = 0
            Dim enforcementFee As Decimal = 0
            For Each feeRow In fee_dt.Rows
                Dim feeType As String = feeRow(1)
                If feeType = "Enforcement" Then
                    EnfFeeFound = True
                    enforcementFee += feeRow(0)
                End If
                feeAmt += feeRow(0)
            Next
            If EnfFeeFound = False And feeAmt < 42.5 Then
                Continue For
            End If

            'case found
            Dim bonusPayment As Decimal = 0
            'save entry in table
            'calc days from allocation to last paid date
            Dim allocationDate As Date = debtRow(2)
            Dim lastPaidDate As Date = debtRow(3)
            Dim days As Integer = DateDiff(DateInterval.Day, allocationDate, lastPaidDate)
            If days > 180 Then
                Continue For
            End If
            'for 2nd tier must be <=120
            If bailTier = "E" Then
                If days > 120 Then
                    Continue For
                End If
            End If
            Dim payRowID As Long = debtRow(4)
            Dim statusDate As Date = debtRow(5)
            Dim firstVisitdate As Date = debtRow(6)
            upd_txt = "insert into BailiffBonusPayments (BailID, " & _
                                   "DebtorID,PeriodDate, days_to_allocate, LinkID, EnforcementFee, TotalFees, PaidDate, payRowID, StatusDate, FirstVisitDate, AllocatedDate)" & _
                                   "values (" & bailID & "," & debtorID & ",'" & Format(periodstartDate, "yyyy-MM-dd") & "'," & days & "," & _
                                   0 & "," & enforcementFee & "," & feeAmt & ",'" & lastPaidDate & "'," & payRowID & ",'" & statusDate & "','" & firstVisitdate & "','" & allocationDate & "')"
            update_sql(upd_txt)
        Next


    End Sub
    Private Sub linked_cases()
        Dim debt_dt As New DataTable
        'first get unlinked cases
        'get successful cases where bailiff has visited
        LoadDataTable2("DebtRecovery", "select D._rowID, D.clientSchemeID, max(V.date_allocated), max(P.date), linkID, D._createdDate, max(P._rowID), max(P.status_date), min(V.date_visited)" & _
                       " from Visit V, Debtor D, Payment P " & _
                       " where V.bailiffID = " & bailID & _
                       " and P.debtorID = D._rowID" & _
                       " and D.status = 'S'" & _
                       " and P.status = 'R'" & _
                       " and date_visited < P.date" & _
                       " and linkID is not null " & _
                       " and V.debtorID = D._rowID" & _
                       " and P.status_date > '" & Format(periodstartDate, "yyyy-MM-dd") & "'" & _
                       " group by D.linkID, D._createdDate, d._rowID" & _
                       " order by D.linkID, D._createdDate, D._rowID", debt_dt, False)
        Dim numRows As Integer = debt_dt.Rows.Count
        Dim ignoreLink As Boolean = False
        Dim lastLinkID As Integer = 0
        Dim lastLoadDate As Date = Nothing
        For Each debtRow In debt_dt.Rows
            Dim debtorID As Integer = debtRow(0)
            Dim linkID As Integer = debtRow(4)
            Dim debtorLoadDate As Date = debtRow(5)
            If ignoreLink And lastLinkID = linkID And Format(debtorLoadDate, "yyyy-MM-dd") = Format(lastLoadDate, "yyyy-MM-dd") Then
                Continue For
            End If
            If linkID <> lastLinkID Then
                ignoreLink = False
            End If
            lastLinkID = linkID
            lastLoadDate = debtRow(5)
            Dim CSID As Integer = debtRow(1)
            Dim clientID As Integer
            clientID = GetSQLResults2("DebtRecovery", "select clientID " & _
                                             " from clientScheme" & _
                                             " where _rowid = " & CSID)
            If clientID = 1 Or clientID = 2 Or clientID = 24 Then
                Continue For
            End If
            'check no waiting payment
            Dim waitingPayment As Integer
            waitingPayment = GetSQLResults2("DebtRecovery", "select count(*) from Payment" & _
                                          " where debtorID = " & debtorID & _
                                          " and status = 'W'")
            If waitingPayment > 0 Then
                Continue For
            End If

            'check last visit is by this bailiff
            Dim visit_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select B._rowID from Visit V, bailiff B" & _
                           " where debtorID = " & debtorID & _
                           " and B._rowid = bailiffID " & _
                           " and agent_type = 'B'" & _
                           " and add_mobile_name <> 'none'" & _
                           " order by date_allocated desc", visit_dt, False)
            Dim ignore_case As Boolean = False
            For Each visitRow In visit_dt.Rows
                If visitRow(0) <> bailID Then
                    ignore_case = True
                End If
                Exit For
            Next
            If ignore_case Then
                Continue For
            End If
            Dim fee_dt As New DataTable
            'check enforcement fee or sum fees > 42.50
            LoadDataTable2("DebtRecovery", "select fee_amount, type from Fee" & _
                           " where debtorID = " & debtorID & _
                           " and fee_amount > 0 " & _
                           " and fee_remit_col > 2", fee_dt, False)
            Dim EnfFeeFound As Boolean = False
            Dim feeAmt As Decimal = 0
            Dim enforcementFee As Decimal
            For Each feeRow In fee_dt.Rows
                Dim feeType As String = feeRow(1)
                If feeType = "Enforcement" Then
                    EnfFeeFound = True
                    enforcementFee += feeRow(0)
                End If
                feeAmt += feeRow(0)
            Next
            If EnfFeeFound = False And feeAmt < 42.5 Then
                Continue For
            End If

            'calc days from allocation to last paid date
            Dim allocationDate As Date = debtRow(2)
            Dim lastPaidDate As Date = debtRow(3)
            Dim days As Integer = DateDiff(DateInterval.Day, allocationDate, lastPaidDate)
            If days > 60 Then
                Continue For
            End If

            'lead case found for link - now check any other links for same loadDate
            'check all links are also paid in full within 120 days
            'only links loaded same day
            arrayCount = 0

            Dim debt2_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select _rowid, status, _createdDate from Debtor " & _
                           " where _rowid <> " & debtorID & _
                           " and linkID = " & linkID, debt2_dt, False)
            For Each debt2Row In debt2_dt.Rows
                Dim debtor2 As Integer = debt2Row(0)
                Dim debt2LoadDate As Date = debt2Row(2)
                If Format(debt2LoadDate, "yyyy-MM-dd") <> Format(debtorLoadDate, "yyyy-MM-dd") Then
                    Continue For
                End If
                If debt2Row(1) <> "S" Then
                    ignoreLink = True
                    Exit For
                End If
                'must be paid within 120 days of allocation date
                Dim debt3_dt As New DataTable
                LoadDataTable2("DebtRecovery", "select  max(V.date_allocated), max(P.date), max(P._rowID), max(P.status_date), min(V.date_visited)" & _
                      " from Visit V, Payment P " & _
                      " where V.bailiffID = " & bailID & _
                      " and V.DebtorID = " & debtor2 & _
                      " and P.debtorID = " & debtor2 & _
                      " and P.status = 'R'", debt3_dt, False)

                'calc days from allocation to last paid date
                Dim allocation2Date As Date
                Try
                    allocation2Date = debt3_dt.Rows(0).Item(0)
                Catch ex As Exception
                    ignoreLink = True
                    Exit For
                End Try

                Dim lastPaid2Date As Date = debt3_dt.Rows(0).Item(1)
                Dim days2 As Integer = DateDiff(DateInterval.Day, allocationDate, lastPaidDate)
                If days2 > 120 Then
                    ignoreLink = True
                    Exit For
                End If
                Dim fee2_dt As New DataTable
                LoadDataTable2("DebtRecovery", "select fee_amount, type from Fee" & _
                          " where debtorID = " & debtor2 & _
                          " and fee_amount > 0 " & _
                          " and fee_remit_col > 2", fee2_dt, False)
                Dim fee2Amt As Decimal = 0
                Dim enforcementFee2 As Decimal
                For Each feeRow In fee2_dt.Rows
                    Dim feeType As String = feeRow(1)
                    If feeType = "Enforcement" Then
                        enforcementFee2 += feeRow(0)
                    End If
                    fee2Amt += feeRow(0)
                Next


                'save case details
                arrayCount += 1
                debtorArray(arrayCount).debtorID = debtor2
                debtorArray(arrayCount).days = days2
                debtorArray(arrayCount).allocationDate = allocation2Date
                debtorArray(arrayCount).enforcementFee = enforcementFee2
                debtorArray(arrayCount).totalFees = fee2Amt
                Try
                    debtorArray(arrayCount).firstVisitDate = debt3_dt.Rows(0).Item(4)
                Catch ex As Exception
                    debtorArray(arrayCount).firstVisitDate = Nothing
                End Try

                debtorArray(arrayCount).payRowID = debt3_dt.Rows(0).Item(2)
                debtorArray(arrayCount).statusdate = debt3_dt.Rows(0).Item(3)
                debtorArray(arrayCount).lastPaidDate = lastPaid2Date

            Next
            If Not ignoreLink Then
                Try
                    Dim payRowID As Long = debtRow(6)
                    Dim statusDate As Date = debtRow(7)
                    Dim firstVisitDate As Date = debtRow(8)
                    upd_txt = "insert into BailiffBonusPayments (BailID, " & _
                                   "DebtorID,PeriodDate, days_to_allocate, LinkID, EnforcementFee, TotalFees, PaidDate, PayRowID, StatusDate, FirstVisitDate, AllocatedDate)" & _
                                   "values (" & bailID & "," & debtorID & ",'" & Format(periodstartDate, "yyyy-MM-dd") & "'," & days & "," & _
                                   linkID & "," & enforcementFee & "," & feeAmt & ",'" & lastPaidDate & "'," & payRowID & ",'" & statusDate & "','" & firstVisitDate & "','" & allocationDate & "')"
                    update_sql(upd_txt)
                    For debtorIDX = 1 To arrayCount
                        upd_txt = "insert into BailiffBonusPayments (BailID, " & _
                                   "DebtorID,PeriodDate, days_to_allocate, LinkID, EnforcementFee, TotalFees, PaidDate, PayRowID, StatusDate, FirstVisitDate, AllocatedDate)" & _
                   "values (" & bailID & "," & debtorArray(debtorIDX).debtorID & ",'" & Format(periodstartDate, "yyyy-MM-dd") & "'," & debtorArray(debtorIDX).days & "," & _
                   linkID & "," & debtorArray(debtorIDX).enforcementFee & "," & debtorArray(debtorIDX).totalFees & ",'" & debtorArray(debtorIDX).lastPaidDate & "'," & debtorArray(debtorIDX).payRowID & ",'" & debtorArray(debtorIDX).statusdate & "','" & debtorArray(debtorIDX).firstVisitDate & "','" & debtorArray(debtorIDX).allocationDate & "')"
                        update_sql(upd_txt)
                    Next
                    ignoreLink = True
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                
            End If
        Next
    End Sub
End Class

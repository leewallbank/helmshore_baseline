﻿Imports CommonLibrary

Public Class RemunerationData
    Private RecoveredFeesTable As New DataTable
    Private SaleFeesTable As New DataTable
    Private TMAPIF As New DataTable
    Private AllocationBatchDetails As New DataTable
    Private Sage As New DataTable

    Private TMAPIFRemitCountDT As New DataTable
    Private TMAPIFRemitCountDV As New DataView

    Public ReadOnly Property RecoveredFees() As DataTable
        Get
            RecoveredFees = RecoveredFeesTable
        End Get
    End Property

    Public ReadOnly Property SaleFees() As DataTable
        Get
            SaleFees = SaleFeesTable
        End Get
    End Property

    Public ReadOnly Property TMAPIFCases() As DataTable
        Get
            TMAPIFCases = TMAPIF
        End Get
    End Property


    Public ReadOnly Property TMAPIFRemitCount() As DataView
        Get
            TMAPIFRemitCount = TMAPIFRemitCountDV
        End Get
    End Property

    Public ReadOnly Property AllocationBatch() As DataTable
        Get
            AllocationBatch = AllocationBatchDetails
        End Get
    End Property

    Public ReadOnly Property SageExtract() As DataTable
        Get
            SageExtract = Sage
        End Get
    End Property

    Public Function VatRate(ByVal EffectiveDate As DateTime) As Decimal
        Dim Sql As String

        VatRate = Nothing

        Try
            Sql = "EXEC dbo.GetVATRate '" & EffectiveDate.ToString("dd/MMM/yyyy") & "'"

            VatRate = GetSQLResults("SwiftRemuneration", Sql)

            VatRate = VatRate / 100

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Sub GetRecoveredFees(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Dim Sql As String

        Try
            Sql = "SELECT cs.BranchID " & _
                  "     , cs.ClientID " & _
                  "     , s.work_type AS WorkTypeID " & _
                  "     , d.RemitID " & _
                  "     , s.work_type " & _
                  "     , b._rowID AS BailiffID " & _
                  "     , d.DebtorID " & _
                  "     , d.LinkID " & _
                  "     , d.PaymentID " & _
                  "     , d.split_fees+d.split_van AS FeesRemitted " & _
                  "     , v._rowID AS VisitID " & _
                  "     , v.date_allocated AS AllocationDate" & _
                  "     , v.date_visited AS VisitDate " & _
                  "     , f.date AS EnforcementFeeDate " & _
                  "     , b.name_fore " & _
                  "     , b.name_sur " & _
                  "     , b.vatReg " & _
                  "     , b.login_name AS LoginName " & _
                  "     , b.internalExternal " & _
                  "FROM ( " & _
                  "       SELECT d._rowID AS DebtorID " & _
                  "            , d.linkID " & _
                  "            , r._rowID AS RemitId " & _
                  "            , d.ClientSchemeID " & _
                  "            , p._rowID AS PaymentID " & _
                  "            , p.split_fees " & _
                  "            , p.split_van " & _
                  "            , MAX(v._rowID) AS LastVisitID " & _
                  "       FROM debtor AS d " & _
                  "       INNER JOIN visit AS v ON d._rowID = v.debtorID " & _
                  "       INNER JOIN payment AS p ON d._rowid = p.debtorID " & _
                  "       INNER JOIN remit AS r ON p.status_remitID = r._rowid " & _
                  "       INNER JOIN bailiff AS b ON v.BailiffID = b._rowid " & _
                  "       WHERE p.status = 'R' " & _
                  "         AND DATE(r.date) BETWEEN '" & StartPeriod.ToString("yyyy-MM-dd") & "'" & _
                  "                              AND '" & EndPeriod.ToString("yyyy-MM-dd") & "'" & _
                  "         AND DATE(v.date_visited) <= p.date " & _
                  "         AND b.BranchID = 24 " & _
                  "         AND b.internalExternal = 'E' " & _
                  "         AND p.split_fees + p.split_van > 0 " & _
                  "       GROUP BY d._rowID " & _
                  "              , d.linkID " & _
                  "              , r._rowID " & _
                  "              , d.ClientSchemeID " & _
                  "              , p._RowID " & _
                  "              , p.split_fees " & _
                  "              , p.split_van " & _
                  "     ) AS d " & _
                  "INNER JOIN clientscheme AS cs ON d.ClientSchemeID = cs._rowid " & _
                  "INNER JOIN scheme       AS s  ON cs.schemeID      = s._rowID " & _
                  "INNER JOIN visit        AS v  ON d.debtorID       = v.DebtorID " & _
                  "                             AND d.LastVisitID    = v._rowID " & _
                  "INNER JOIN bailiff      AS b  ON v.BailiffID      = b._rowID " & _
                  "INNER JOIN fee          AS f  ON d.debtorID       = f.debtorID " & _
                  "WHERE (b.login_name LIKE 'fc%' OR b.login_name LIKE 'vb%') " & _
                  "  AND v.BailiffID = ( SELECT v_s.BailiffID " & _
                  "                      FROM visit AS v_s USE INDEX (VisitDebtor) " & _
                  "                      INNER JOIN bailiff AS b_s ON v_s.BailiffID = b_s._rowID " & _
                  "                      WHERE v_s.debtorID = d.DebtorID " & _
                  "                        AND v_s.date_allocated IS NOT NULL " & _
                  "                        AND b_s.agent_type = 'B' " & _
                  "                        AND b_s.status = 'O' " & _
                  "                        AND (    b_s.typeSub IS NULL " & _
                  "                              OR b_s.typeSub <> 'Employed' " & _
                  "                            ) " & _
                  "                      ORDER BY v_s.date_allocated DESC, v_s._rowID DESC " & _
                  "                      LIMIT 1 " & _
                  "                    ) " & _
                  "  AND b.BranchID = 24 " & _
                  "  AND b.internalExternal = 'E' " & _
                  "	 AND f._rowID = ( SELECT MAX(f_s._rowID) " & _
                  "	                  FROM fee AS f_s " & _
                  "	                  WHERE f_s.debtorID = d.debtorID " & _
                  "	                    AND f_s.type = 'Enforcement' " & _
                  "	                )" & _
                  "  AND s.work_type <> 20 "

            LoadDataTable("DebtRecovery", Sql, RecoveredFeesTable, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function PreviouslyRecoveredFees(ByVal DebtorID As Integer, LinkID As Integer, RemitID As Integer, PaymentID As Integer, EnforcementFeeDate As DateTime) As Decimal
        Dim Sql As String

        PreviouslyRecoveredFees = Nothing

        Try
            Sql = "SELECT IFNULL(SUM(p_s.split_fees + p_s.split_van), 0) " & _
                  "FROM debtor AS d_s " & _
                  "INNER JOIN payment AS p_s ON d_s._rowID = p_s.DebtorID " & _
                  "WHERE p_s.status = 'R' "

            If LinkID > 0 Then
                Sql &= "  AND d_s.linkid = " & LinkID.ToString
            Else
                Sql &= "  AND d_s._rowID = " & DebtorID.ToString
            End If

            Sql &= "  AND p_s.status_remitID <= " & RemitID.ToString & _
                   "  AND p_s._rowid < " & PaymentID.ToString & _
                   "  AND p_s.split_fees + p_s.split_van > 0 " & _
                   "  AND EXISTS ( SELECT 1 " & _
                   "               FROM fee AS f_s " & _
                   "               WHERE f_s.DebtorID = d_s._rowID " & _
                   "                 AND f_s.date = '" & EnforcementFeeDate.ToString("yyyy-MM-dd HH:mm:ss") & "' " & _
                   "                 AND f_s.type = 'Enforcement' " & _
                   "             )"

            PreviouslyRecoveredFees = GetSQLResults("DebtRecovery", Sql)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Sub GetAllocationBatchDetailsTMAPIF(ByVal CaseID As String, ByVal LinkID As String, ByVal AllocationDate As Date, ByVal BailiffID As String, ByVal FirstAllocationVisitDate As Date, EnforcementFeeDate As Date)
        Dim Sql As String
        ' EnforcementFeeDate can be NULL in the base table but this Sub is only called where enforcement fees exist so no need to handle NULL
        Try

            Sql = "SELECT SUM(IFNULL(t.ClientDebtBalance,0)) AS TotalClientDebtBalance " & _
                  "     , COUNT(CASE WHEN t.status_open_closed = 'O' THEN 1 ELSE NULL END) AS OpenCases" & _
                  "     , COUNT(CASE WHEN t.status_open_closed = 'C' THEN 1 ELSE NULL END) AS ClosedCases " & _
                  "     , MAX(t.LastPaymentDate) AS LastBatchPaymentDate " & _
                  "     , MIN(t.LastPaymentDate) AS EarliestLastBatchPaymentDate " & _
                  "FROM ( SELECT d._rowid" & _
                  "            , IFNULL(SUM(p.split_debt), 0) AS ClientDebtBalance" & _
                  "            , MAX(p.date) AS LastPaymentDate" & _
                  "            , CASE WHEN DATE(d.return_date) > DATE_SUB(CURDATE(), INTERVAL DAYOFWEEK(CURDATE())-1 DAY) THEN 'O' ELSE d.status_open_closed END AS status_open_closed " & _
                  "       FROM debtor AS d" & _
                  "       LEFT JOIN payment AS p ON d._rowID = p.DebtorID" & _
                  "                             AND p.status = 'R'" & _
                  "                             AND p.status_date >= '" & FirstAllocationVisitDate.ToString("yyyy-MM-dd") & "'"

            If LinkID = "" Then
                Sql &= "       WHERE d._rowID = " & CaseID
            Else
                Sql &= "       WHERE d.LinkID = " & LinkID
            End If

            Sql &= "         AND EXISTS ( SELECT 1" & _
                   "                      FROM visit AS v_s" & _
                   "                      INNER JOIN bailiff AS b_s ON v_s.BailiffID = b_s._rowID" & _
                   "                      INNER JOIN debtor AS d_s ON v_s.DebtorID = d_s._rowID" & _
                   "                      INNER JOIN fee AS f_s ON f_s.DebtorID = d_s._rowID" & _
                   "                      WHERE v_s.Debtorid = d._rowID" & _
                   "                        AND v_s.date_allocated = '" & AllocationDate.ToString("yyyy-MM-dd") & "'" & _
                   "                        AND DATE(f_s.date) = '" & EnforcementFeeDate.ToString("yyyy-MM-dd") & "'" & _
                   "                        AND b_s._rowID = " & BailiffID & " " & _
                   "                        AND b_s.agent_type = 'B' " & _
                   "                        AND d_s.ClientSchemeID >= 3856 " & _
                   "                        AND f_s.type = 'Enforcement' " & _
                   "                    )" & _
                   "         AND d.ClientSchemeID >= 3856 " & _
                   "       GROUP BY d._rowid" & _
                   "              , d.status_open_closed" & _
                   "     ) t"

            LoadDataTable("DebtRecovery", Sql, AllocationBatchDetails, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function GetAllocationBatchPayingCaseTMAPIF(ByVal LinkID As String, ByVal EnforcementFeeDate As Date, ByVal BailiffID As String) As String
        Dim Sql As String
        Dim PayingCase As String

        GetAllocationBatchPayingCaseTMAPIF = ""

        Try

            Sql = "SELECT p.CaseID " & _
                  "FROM dbo.EAPayment AS p " & _
                  "INNER JOIN dbo.EAInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                  "WHERE p.LinkID = " & LinkID & _
                  "  AND p.EnforcementFeeDate = '" & EnforcementFeeDate.ToString("yyyy-MM-dd") & "'" & _
                  "  AND p.BailiffID = " & BailiffID & " " & _
                  "  AND i.InvoiceType = 'TMA'"

            PayingCase = GetSQLResults("SwiftRemuneration", Sql)

            If IsDBNull(PayingCase) Then PayingCase = ""

            GetAllocationBatchPayingCaseTMAPIF = PayingCase

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Function GetPayingCase(ByVal CaseID As String, InvoiceType As String) As String
        Dim Sql As String
        Dim PayingCase As String

        GetPayingCase = ""

        Try

            Sql = "SELECT p.CaseID " & _
                  "FROM dbo.EAPayment AS p " & _
                  "INNER JOIN dbo.EAInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                  "WHERE p.CaseID = " & CaseID & _
                  "  AND i.InvoiceType = '" & InvoiceType & "'"

            PayingCase = GetSQLResults("SwiftRemuneration", Sql)

            If IsDBNull(PayingCase) Then PayingCase = ""

            GetPayingCase = PayingCase

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Function GetPayingCaseSaleFee(ByVal CaseID As String, LinkID As String, FeeDate As DateTime) As String
        Dim Sql As String
        Dim PayingCase As String

        GetPayingCaseSaleFee = ""

        Try
            If LinkID = "" Then
                Sql = "SELECT p.CaseID " & _
                      "FROM dbo.EAPayment AS p " & _
                      "INNER JOIN dbo.EAInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                      "WHERE p.CaseID = " & CaseID & _
                      "  AND p.EnforcementFeeDate = '" & FeeDate.ToString("dd/MMM/yyyy") & "'" & _
                      "  AND i.InvoiceType = 'SF'"
            Else
                Sql = "SELECT p.CaseID " & _
                      "FROM dbo.EAPayment AS p " & _
                      "INNER JOIN dbo.EAInvoice AS i ON p.InvoiceNumber = i.InvoiceNumber " & _
                      "WHERE p.LinkID = " & LinkID & _
                      "  AND p.EnforcementFeeDate = '" & FeeDate.ToString("dd/MMM/yyyy") & "'" & _
                      "  AND i.InvoiceType = 'SF'"
            End If

            PayingCase = GetSQLResults("SwiftRemuneration", Sql)

            If IsDBNull(PayingCase) Then PayingCase = ""

            GetPayingCaseSaleFee = PayingCase

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Function

    Public Sub GetTMAPIFRemitCount()
        Dim Sql As String

        Try
            Sql = "EXEC dbo.GetTMAPIFRemitCOunt"

            LoadDataTable("SwiftRemuneration", Sql, TMAPIFRemitCountDT, False)

            TMAPIFRemitCountDV = New DataView(TMAPIFRemitCountDT)
            TMAPIFRemitCountDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub ImportTMAPIF(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Dim Sql As String

        Try
            Sql = "EXEC stg.ImportTMAPIF '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'"

            ExecStoredProc("SwiftRemuneration", Sql, 0)

            Sql = "EXEC dbo.GetTMAPIF"

            LoadDataTable("SwiftRemuneration", Sql, TMAPIF, False)

        Catch ex As Exception
            HandleException(ex)
        End Try

    End Sub

    Public Sub GetSaleFees(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Dim Sql As String

        Try
            Sql = "SELECT cs.BranchID " & _
                  "     , cs.ClientID " & _
                  "     , s.work_type AS WorkTypeID " & _
                  "     , d._rowid AS DebtorID " & _
                  "     , d.LinkID " & _
                  "     , v.BailiffID " & _
                  "     , v.date_allocated AS AllocationDate " & _
                  "     , v._rowID AS VisitID " & _
                  "     , v.date_visited AS VisitDate " & _
                  "     , f.date AS FeeDate " & _
                  "     , b.name_fore " & _
                  "     , b.name_sur " & _
                  "     , b.login_name AS LoginName" & _
                  "     , b.vatReg " & _
                  "     , b.internalExternal " & _
                  "     , ( SELECT COUNT(*) " & _
                  "         FROM debtor AS d_s" & _
                  "         INNER JOIN fee AS f_s ON d_s._rowID = f_s.debtorID " & _
                  "         WHERE d_s.LinkID = d.LinkID " & _
                  "           AND f_s.type = 'Sale or disposal' " & _
                  "           AND f_s.date = f.date " & _
                  "           AND d_s.status_open_closed <> 'C' " & _
                  "           AND d_s.status <> 'S' " & _
                  "       ) AS OpenLinkedCases " & _
                  "     , IFNULL(( SELECT SUM(d_s.debt_amount) " & _
                  "                FROM debtor AS d_s " & _
                  "                INNER JOIN fee AS f_s ON d_s._rowID = f_s.debtorID " & _
                  "                WHERE d_s.LinkID = d.LinkID " & _
                  "                  AND f_s.type = 'Sale or disposal' " & _
                  "                  AND f_s.date = f.date " & _
                  "              ), d.debt_amount) AS TotalDebt " & _
                  "FROM debtor AS d " & _
                  "INNER JOIN clientscheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "INNER JOIN scheme AS s ON cs.schemeID = s._rowID " & _
                  "INNER JOIN visit AS v ON d._rowid = v.debtorID " & _
                  "INNER JOIN bailiff AS b ON b._rowid = v.bailiffID " & _
                  "INNER JOIN fee AS f ON d._rowID = f.debtorID " & _
                  "WHERE cs.ClientID NOT IN (1,2,24) " & _
                  "  AND b.branchID = 24 " & _
                  "  AND b.status = 'O' " & _
                  "  AND b.internalExternal = 'E' " & _
                  "  AND b.typeSub <> 'Employed' " & _
                  "  AND v._rowid = ( SELECT MAX(v_s._rowid) " & _
                  "                   FROM visit AS v_s " & _
                  "                   WHERE v_s.debtorID = d._rowid " & _
                  "                    AND v_s.date_visited IS NOT NULL " & _
                  "                 ) " & _
                  "  AND f.type = 'Sale or disposal' " & _
                  "  AND f.fee_amount > 0 " & _
                  "         AND DATE(d.return_date) BETWEEN '" & StartPeriod.ToString("yyyy-MM-dd") & "'" & _
                  "                              AND '" & EndPeriod.ToString("yyyy-MM-dd") & "'" & _
                  "  AND d.status_open_closed = 'C' " & _
                  "  AND d.status = 'S' "

            LoadDataTable("DebtRecovery", Sql, SaleFeesTable, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetSageExtract(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Dim Sql As String

        Try

            Sql = "EXEC dbo.GetSageExtract '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'"

            LoadDataTable("SwiftRemuneration", Sql, Sage, False)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub AddEAPayment(ByVal CaseID As String, _
                                ByVal LinkID As String, _
                                ByVal BailiffID As String, _
                                ByVal ClientID As String, _
                                ByVal WorkTypeID As String, _
                                ByVal VisitID As String, _
                                ByVal VisitDate As Date, _
                                ByVal AllocationDate As Date, _
                                ByVal EnforcementFeeDate As Nullable(Of Date), _
                                ByVal DebriefDate As Date, _
                                ByVal InvoiceNumber As String, _
                                ByVal InvoiceDate As Date, _
                                ByVal BasicPayment As Decimal, _
                                ByVal Vat As Decimal, _
                                ByVal StartPeriod As Date, _
                                ByVal EndPeriod As Date, _
                                ByVal BailiffSageAccount As String, _
                                ByVal InvoiceType As String, _
                                ByVal internalExternal As String)

        If LinkID = "" Then LinkID = "NULL"

        Dim Sql As String = "EXEC dbo.AddEAPayment " & CaseID & _
                                                   ", " & LinkID & _
                                                   ", " & BailiffID & _
                                                   ", " & ClientID & _
                                                   ", " & WorkTypeID & _
                                                   ", " & VisitID & _
                                                   ", '" & VisitDate.ToString("dd/MMM/yyyy") & "'" & _
                                                   ", '" & AllocationDate.ToString("dd/MMM/yyyy") & "'"

        If Not IsNothing(EnforcementFeeDate) Then
            Sql &= ",'" & CDate(EnforcementFeeDate).ToString("dd/MMM/yyyy") & "'"
        Else
            Sql &= ",NULL"
        End If

        Sql &= ", '" & DebriefDate.ToString("dd/MMM/yyyy") & "'" & _
               ", '" & InvoiceNumber & "'" & _
               ", '" & InvoiceDate.ToString("dd/MMM/yyyy") & "'" & _
               ", " & BasicPayment.ToString("0.00") & _
               ", " & Vat.ToString("0.00") & _
               ", '" & StartPeriod.ToString("dd/MMM/yyyy") & "'" & _
               ", '" & EndPeriod.ToString("dd/MMM/yyyy") & "'" & _
               ", '" & BailiffSageAccount & "'" & _
               ", '" & InvoiceType & "'" & _
               ", '" & internalExternal & "'"

        ExecStoredProc("SwiftRemuneration", Sql, 0)
    End Sub

    Public Sub UpdateInvoiceTotals(ByVal StartPeriod As Date, ByVal EndPeriod As Date)
        Try
            ExecStoredProc("SwiftRemuneration", "EXEC dbo.UpdateInvoiceTotals '" & StartPeriod.ToString("dd/MMM/yyyy") & "', '" & EndPeriod.ToString("dd/MMM/yyyy") & "'", 0)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class

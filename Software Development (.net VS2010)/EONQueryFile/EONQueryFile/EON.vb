﻿Imports System.Xml.Schema
Imports System.IO
Imports commonlibrary

Public Class EON
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim new_file As String = ""
    Dim CSID As Integer
    Dim prod_run As Boolean = False
    Dim filename As String
    Dim lastQueryDate As Date
    Dim clientID As Integer = 1938

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try

        If env_str = "Prod" Then
            prod_run = True
        Else
            MsgBox("Test run only")
            'clientID = 24
        End If
        'get latest date from eon_query_file

        Try
            lastQueryDate = GetSQLResults("FeesSQL", "select eon_query_date from eon_query_file" & _
                                            " where eon_query_seqID = (select max(eon_query_seqID) from eon_query_file)")
        Catch ex As Exception
            lastQueryDate = CDate("Jan 1, 2017")
        End Try
        rundtpdate.Value = lastQueryDate
        rundtptime.Value = lastQueryDate
    End Sub

    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub processbtn_Click(sender As System.Object, e As System.EventArgs) Handles processbtn.Click
        Dim newQueryDate As Date = Now
        lastQueryDate = CDate(Format(rundtpdate.Value, "yyyy-MM-dd") & " " & Format(rundtptime.Value, "HH:mm:ss"))
        'get all cases with query within dates
        Dim debt_dt As New DataTable
        Dim recordCount As Integer
        Dim outFile As String = ""
        Dim totalClientBalance As Decimal = 0
        LoadDataTable("DebtRecovery", " select client_ref, offence_number, D.statusCode, D._rowID, CS._rowID, N.text, N._rowID, N._createdby " & _
                      " from debtor D, clientscheme CS, note N" & _
                      " where D.clientschemeID = CS._rowID" & _
                      " and CS.clientID = " & clientID & _
                      " and N.DebtorID = D._rowID" & _
                      " and N.type = 'Status changed'" & _
                      " and N.text like '%to EON%'" & _
                      " and N._createdDate > '" & Format(lastQueryDate, "yyyy-MM-dd HH:mm:ss") & "'" & _
                      " and N._createdDate < '" & Format(newQueryDate, "yyyy-MM-dd HH:mm:ss") & "'", debt_dt, False)
        For Each debtRow In debt_dt.Rows
            Dim debtorID As Integer = debtRow(3)
            Dim CSID As Integer = debtRow(4)
            Dim clientRef As String = debtRow(0)
            Dim offenceNumber As String = ""
            Try
                offenceNumber = debtRow(1)
            Catch ex As Exception

            End Try

            Dim statusCode As Integer = debtRow(2)
            'get description
            Dim codeDesc As String = GetSQLResults("DebtRecovery", "select desc_short from codestatus " & _
                                                 " where _rowID = " & statusCode)
            If Microsoft.VisualBasic.Left(codeDesc, 3) <> "EON" Then
                Continue For
            End If
            recordCount += 1
            'get client balance
            Dim clientbalance As Decimal
            clientbalance = GetSQLResults("DebtRecovery", "select sum(fee_amount-remited_fee) from fee" & _
                                          " where fee_remit_col=1" & _
                                        " and DebtorID = " & debtorID)
            totalClientBalance += clientbalance
            Dim noteText As String = debtRow(5)
            Dim startIDX As Integer = InStr(noteText, ":")
            If startIDX > 0 Then
                noteText = Trim(Microsoft.VisualBasic.Right(noteText, noteText.Length - startIDX))
            End If
            'T117365
            'if length > 250 see if there is another text line up to 5 mins later by same person
            If noteText.Length > 150 Then
                Dim noteRowID As Integer = debtRow(6)
                Dim note_dt As New DataTable
                LoadDataTable("DebtRecovery", " select text from note " & _
                              " where debtorID = " & debtorID & _
                              " and type = 'Status changed'" & _
                              " and _rowID > " & noteRowID & _
                              " and _rowID < " & noteRowID + 100 & _
                              " and text not like 'Status changed from%'" & _
                              " and _createdBy = '" & debtRow(7) & "'", note_dt, False)
                For Each noteRow In note_dt.Rows
                    noteText &= " " & noteRow(0)
                Next
            End If
            outFile &= offenceNumber & "," & clientRef & "," & clientbalance & "," & Format(Now, "dd/MM/yyyy") & "," & _
                Microsoft.VisualBasic.Right(codeDesc, 3) & "," & "N" & "," & Chr(34) & noteText & Chr(34) & vbNewLine
        Next

        'do header and trailer
        Dim fileName As String = "HOL-EON-ROS-" & Format(Now, "ddMMyyyy") & ".csv"
        Dim headerRecord As String = "HOL,EON," & Format(Now, "dd/MM/yyyy") & "," & "," & totalClientBalance & "," & recordCount & vbNewLine
        outFile = headerRecord & outFile

        Dim trailerRecord As String = "ROS"
        outFile &= trailerRecord

        'update stopper date
        If prod_run Then
            Dim upd_txt As String = "insert into eon_query_file (eon_query_date) values ('" & Format(newQueryDate, "yyyy-MM-dd HH:mm:ss") & "')"

            update_sql(upd_txt)
        End If

        'write out file
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "CSV files |*.csv"
            .DefaultExt = ".csv"
            .OverwritePrompt = True
            .FileName = fileName
        End With
        If Not SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            MsgBox("No file name selected for save ")
            Return
        End If
        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outFile, False, ascii)
        MsgBox("report saved")
        Me.Close()
    End Sub
    Private Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, Optional ByVal NoteLength As Integer = 250) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 2) ' + 2 is for :;
        Dim PreviousLength As Integer
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = (NoteTag & ":" & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)) & ";").PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function
    Private Function getCaseID(ByVal clientRef As String) As Integer
        Dim debtorID As Integer = 0
        debtorID = GetSQLResults("DebtRecovery", "SELECT D._rowID " & _
                                                   "FROM debtor D, clientscheme CS  " & _
                                                   "WHERE client_ref = '" & clientRef & "'" & _
                                                   " AND D.clientschemeID = CS._rowID " & _
                                                   " AND CS.clientID = 1662")

        Return debtorID
    End Function

    Private Sub ProgressBar1_Click(sender As System.Object, e As System.EventArgs) Handles ProgressBar1.Click

    End Sub

    Private Sub Label1_Click(sender As System.Object, e As System.EventArgs) Handles Label1.Click

    End Sub

    Private Sub rundtptime_ValueChanged(sender As System.Object, e As System.EventArgs) Handles rundtptime.ValueChanged

    End Sub

    Private Sub rundtpdate_ValueChanged(sender As System.Object, e As System.EventArgs) Handles rundtpdate.ValueChanged

    End Sub
End Class

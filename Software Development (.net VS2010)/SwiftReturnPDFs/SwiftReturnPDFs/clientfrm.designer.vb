<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class clientfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cl_dg = New System.Windows.Forms.DataGridView()
        Me.clientID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clientname = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.cl_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cl_dg
        '
        Me.cl_dg.AllowUserToAddRows = False
        Me.cl_dg.AllowUserToDeleteRows = False
        Me.cl_dg.AllowUserToOrderColumns = True
        Me.cl_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.cl_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clientID, Me.clientname, Me.clRef})
        Me.cl_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cl_dg.Location = New System.Drawing.Point(0, 0)
        Me.cl_dg.Name = "cl_dg"
        Me.cl_dg.ReadOnly = True
        Me.cl_dg.Size = New System.Drawing.Size(497, 384)
        Me.cl_dg.TabIndex = 0
        '
        'clientID
        '
        Me.clientID.HeaderText = "Client ID"
        Me.clientID.Name = "clientID"
        Me.clientID.ReadOnly = True
        '
        'clientname
        '
        Me.clientname.HeaderText = "Client Name"
        Me.clientname.Name = "clientname"
        Me.clientname.ReadOnly = True
        Me.clientname.Width = 250
        '
        'clRef
        '
        Me.clRef.HeaderText = "Ref"
        Me.clRef.Name = "clRef"
        Me.clRef.ReadOnly = True
        '
        'clientfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(497, 384)
        Me.Controls.Add(Me.cl_dg)
        Me.Name = "clientfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Double-click to select client"
        CType(Me.cl_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cl_dg As System.Windows.Forms.DataGridView
    Friend WithEvents clientID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clientname As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clRef As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

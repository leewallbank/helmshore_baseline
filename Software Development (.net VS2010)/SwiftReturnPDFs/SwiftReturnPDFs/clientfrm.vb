Public Class clientfrm

    Private Sub clientfrm_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.DoubleClick

    End Sub

    Private Sub clientfrm_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Enter

    End Sub

    Private Sub clientfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'get all Swift clients
        ConnectDb2("DebtRecovery")
        Dim cl_ds As New DataTable
        cl_dg.Rows.Clear()
        LoadDataTable2("DebtRecovery", "select distinct C._rowID, C.name, C.ref from Client C, clientscheme CS " & _
        " where CS.clientID = C._rowID " & _
        " and CS.branchID = 24" & _
        " order by C.name", cl_ds, False)
        Dim clRow As DataRow
        For Each clRow In cl_ds.Rows
            cl_dg.Rows.Add(clRow(0), clRow(1), clRow(2))
        Next
    End Sub

    Private Sub cl_dg_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles cl_dg.CellDoubleClick
        Me.Close()
    End Sub

    Private Sub cl_dg_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles cl_dg.CellEnter
        client_no = cl_dg.Rows(e.RowIndex).Cells(0).Value
        client_name = Replace(cl_dg.Rows(e.RowIndex).Cells(1).Value, "S:", "")
    End Sub
End Class
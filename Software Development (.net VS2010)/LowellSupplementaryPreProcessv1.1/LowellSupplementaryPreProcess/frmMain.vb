﻿Imports System.IO
Imports System.Collections
Imports CommonLibrary

Public Class frmMain
    Private LowellData As New clsLowellData
    Const Separator As String = "|"
    Private InputFilePath As String, FileName As String, FileExt As String
    Private OutputFiles As New ArrayList()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim LineNumber As Integer, CaseCount As Integer, FileNum As Integer = 1
            Dim OutputFile As String, OutputFilename As String
            Dim AuditLog As String, ErrorLog As String = "", InputLineArray() As String, DebtorID As String

            ' Column headers are not supplied. 10 per line below
            Dim ColumnHeader() As String = {"Account ID", "Router Account Number", "Primary Person Residential Status", "Primary Person Income", "Primary Person Employer Name", "Primary Person Occupation", "Secondary Person IVAFlag", "Secondary Person bankrupt Flag", "SecondaryDeceased Flag", "Secondary Person Residential Status", _
                                            "Secondary Person Income", "Secondary Person Employer Name", "Secondary Person Occupation", "Address4TypeCode", "Address4Line1", "Address4Line2", "Address4Line 3", "Address4City", "Address4County", "Address4Country", _
                                            "Address4PostCode", "ClientComments", "IGT Flag", "PostLitigationFlag", "PrePayFlag", "Account Score", "Previous Score Date", "Current Score Date", "Lowell Ownership Date", "Judgement Date", _
                                            "PrimaryPhone4", "PrimaryPhone5", "PrimaryPhone6", "dr-email2", "dr-email3", "NOK Forename", "NOK Surname", "PreferredAddress", "drHomeOwnerUpdateDate", "paidtodatevalue", _
                                            "TotalLetters", "TotalContact", "LastcontactDate", "Acct Turn STB", "Last know contact desc", "Valuation", "Secured balance", "Unsecured balance", "Tot equity", "Joint acc flag", _
                                            "Previous_Payer", "Last Client Payment Date", "Account Open Date", "Stat barred indicator", "Account due to be stat barred ", "Active credit accounts balance", "Active credit accounts older than 12 months balance", "Active mortgage accounts opened in last 12 months balance", "Active revolving credit accounts balance", "CurrDefOrigDefPct_ALL", _
                                            "YearsER", "Info1", "Info2", "Info3", "Info4", "Info5", "Info6", "Info7", "Info8", "Info9", _
                                            "Info10", "Info11", "Info12", "Info13", "Info14", "Info15", "Info16", "Info17", "Info18", "Info19", _
                                            "Info20", "Info21", "Info22", "Info23", "Info24", "Info25", "Info26", "Info27", "Info28", "Info29", _
                                            "Info30", "Info31", "Info32", "Info33", "Info34", "Info35", "Info36", "Info37", "Info38", "Info39", _
                                            "Info40", "Info41", "Info42", "Info43", "Info44", "Info45", "Info46", "Info47", "Info48", "Info49", _
                                            "Info50", "Info51", "Info52", "Info53", "Info54", "Info55", "Info56", "Info57", "Info58", "Info59", _
                                            "Info60", "Info61", "Info62", "Info63", "Info64", "Info65", "Info66", "Info67", "Info68", "Info69", _
                                            "Info70", "Info71", "Info72", "Info73", "Info74", "Info75", "Info76", "Info77", "Info78", "Info79", _
                                            "Info80", "CurrDefOrigDefPct_CC", "CurrValueDefaults_ALL", "MaxBalanceL3M", "MaxBalPctMaxBalL3M", "MaxCurrentBalance", "MaxStatus", "MaxStatusL3M", "MaxStatusL6M_Open4_12", "MonthsSinceLastDefault_ALL", _
                                            "NumActAccs_ALL", "NumActAccsUTDPosBal_ALL", "NumDefaultAccs_ALL", "NumSearchesL12M", "NumSettledAccs_ALL", "NumSettledMOAccs", "NumSettledNonMOAccs", "PctDefaultAccsPay_ALL", "PctDefaultAccsSettle_ALL", "PrimaryPersonIVAFlag", _
                                            "PrimaryPersonBankruptFlag", "PrimaryDeceased Flag ", "Previously Litigated Flag", "CCJ claim number"}


            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            For Each OldFile As String In Directory.GetFiles(InputFilePath, FileName & "_*_PreProcessed.txt")
                File.Delete(OldFile)
            Next OldFile

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

            lblReadingFile.Visible = True
            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents)

            'WriteFile(InputFilePath & FileName & "_PreProcessed.txt", "") ' overwrite the file if it exists
            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then File.Delete(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then File.Delete(InputFilePath & FileName & "_Error.txt")

            OutputFilename = InputFilePath & FileName & "_001_PreProcessed.txt"

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1

                If LineNumber = 1 Then ' validate header
                    If InputLine.Split(",")(3) <> UBound(FileContents) - 1 Then ErrorLog &= "Count total in the header (" & InputLine.Split(",")(3) & ") does not match file contents (" & (UBound(FileContents) - 1).ToString & ")."
                    Continue For
                End If

                If LineNumber = UBound(FileContents) + 1 Then Continue For ' skip footer line


                OutputFile = ""
                InputLineArray = Split(InputLine, ",", """")

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                DebtorID = LowellData.GetDebtorID(InputLineArray(0))

                If DebtorID <> "" Then

                    OutputFile &= DebtorID & "|"

                    For ColumnNumber As Integer = 1 To UBound(InputLineArray) ' skip first column
                        If InputLineArray(ColumnNumber) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(ColumnNumber).Replace("""", ""), ColumnHeader(ColumnNumber), ";"))
                    Next ColumnNumber

                    OutputFile &= vbCrLf
                    CaseCount += 1
                Else
                    ErrorLog &= "Cannot find unique DebtorID for client ref " & InputLineArray(0) & " at line number " & LineNumber.ToString & vbCrLf
                End If

                If CaseCount > CInt(txtMaxCasesInOutputFile.Text) Then
                    FileNum += 1
                    OutputFilename = InputFilePath & FileName & "_" & FileNum.ToString.PadLeft(3, "0") & "_PreProcessed.txt"
                    CaseCount = 1
                End If

                AppendToFile(OutputFilename, OutputFile)
                If Not OutputFiles.Contains(OutputFilename) Then OutputFiles.Add(OutputFilename)

            Next InputLine

            AuditLog = LogHeader & "Number of cases: " & (UBound(FileContents) - 1).ToString & vbCrLf

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            MsgBox("Pre-processing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            For Each Filename As String In OutputFiles
                If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
            Next Filename

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

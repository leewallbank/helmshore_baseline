﻿Imports CommonLibrary
Imports System.Configuration

Public Class clsLowellData
    Public Function GetDebtorID(ByVal ClientRef As String) As String
        Dim Sql As String
        Dim DebtorID As New DataTable
        GetDebtorID = ""

        Try

            Sql = "SELECT d._rowID " & _
                  "FROM debtor AS d " & _
                  "INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID " & _
                  "WHERE d.offenceLocation = '" & ClientRef & "' " & _
                  "  AND d.status_open_closed = 'O' " & _
                  "  AND cs.clientID = "

            If ConfigurationManager.AppSettings("Environment") = "Prod" Then
                Sql &= "1815"
            Else
                Sql &= "24"
            End If


            LoadDataTable("DebtRecovery", Sql, DebtorID, False)

            If DebtorID.Rows.Count = 1 Then GetDebtorID = DebtorID.Rows(0).Item(0).ToString ' check row count to catch duplicates or no match

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Function
End Class

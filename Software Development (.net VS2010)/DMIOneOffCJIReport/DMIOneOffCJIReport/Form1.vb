﻿'Imports CommonLibrary
Imports System.Configuration
Imports System.Xml.Schema
Imports System.IO

Public Class Form1
    Dim xml_valid As Boolean = True
    Dim auditFile As String = ""
    Dim error_no As Integer = 0
    Dim case_no As Integer = 0
    Dim prod_run As Boolean = False
    Dim fileDate As Date = Now
    Dim street1, street2, street3, town, city, postcode As String
    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        run_report()
    End Sub
    Private Sub run_report()

        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("StudentLoans")
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If
        Dim filename As String = "ADFRossendalesHMRC (IDS)" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".xml"
        Dim activity_file As String = "\\ross-helm-fp001\Rossendales Shared\TDX IDS Reports\" & filename
        auditFile = "\\ross-helm-fp001\Rossendales Shared\TDX IDS Reports\TDX_Activity_HMRC_IDSaudit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        If Not prod_run Then
            activity_file = "C:\AAtemp\" & filename
            auditFile = "C:\AAtemp\TDX_Activity_HMRC_IDSaudit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        End If
        write_audit("report started at " & Now, False)
        Dim writer As New Xml.XmlTextWriter(activity_file, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)
        writer.Formatting = Xml.Formatting.Indented
        writer.WriteStartElement("ActvtyData")
        writer.WriteAttributeString("schemaVersion", "1.0")
        writer.WriteAttributeString("xmlns", "ActivityData")
        writer.WriteStartElement("MsgHdr")
        writer.WriteAttributeString("xmlns", "")
        writer.WriteElementString("MsgTp", "ADF")
        writer.WriteElementString("FlNm", filename)
        writer.WriteElementString("FlDt", Format(fileDate, "yyyy-MM-dd") & "T" & Format(fileDate, "HH:mm:ss"))
        writer.WriteElementString("DCACd", "Rossendales")
        writer.WriteEndElement()  'MsgHdr
        writer.WriteStartElement("Accts")
        writer.WriteAttributeString("xmlns", "")

        Dim startDate As Date = DateAdd(DateInterval.Day, -Weekday(Now) - 6, Now)
        startDate = CDate(Format(startDate, "MMM d, yyyy" & " 00:00:00"))
        Dim endDate As Date = DateAdd(DateInterval.Day, 8, startDate)

        endDate = Now
        endDate = CDate(Format(endDate, "MMM d, yyyy" & " 00:00:00"))

         If Weekday(Now) = 7 Then 'Saturday
            endDate = CDate(Format(Now, "MMM") & " " & Format(Now, "dd") & ", " & Year(Now) & " 16:00:00")
        Else
            endDate = DateAdd(DateInterval.Day, -Weekday(Now), Now)
        End If


        endDate = CDate(Format(endDate, "MMM d, yyyy" & " 16:00:00"))

        startDate = DateAdd(DateInterval.Day, -7, endDate)
        Dim threedaysago As Date = DateAdd(DateInterval.Day, -3, startDate)
        ''if 2nd Jan 2016 go back a further 7 days
        'If Format(endDate, "yyyy-MM-dd") = Format(CDate("2016-01-02"), "yyyy-MM-dd") Then
        '    startDate = DateAdd(DateInterval.Day, -14, endDate)
        'End If
        If Not prod_run Then
            'clientID = 24
            startDate = CDate("Jan 1, 2017 16:00:00")
            endDate = CDate("Jul 1, 2017 16:00:00")
        End If
        Dim clientCode As String = "HMRC (IDS)"
        'get all CSIDs for HMRC DMI
        'Dim cs_dt As New DataTable
       
        ' LoadDataTable2("DebtRecovery", "select _rowid " & _
        '             " from clientScheme" & _
        '            " where branchID =26", cs_dt, False)
        'Dim CSIDCount As Integer = cs_dt.Rows.Count
        'For Each csRow In cs_dt.Rows
        'Dim CSID As Integer = csRow(0)
        'now get cases
        Dim debt_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, D.status, arrange_started, return_codeID, debt_balance, return_date, bail_current, bail_allocated," & _
                    "D.bailiffID, name_fore, name_sur, dateOfBirth, address, add_postcode, client_batch, D._createdDate, CS._rowID" & _
                   " status_open_closed, return_date" & _
                  " from Debtor D, clientScheme CS, note N" & _
                  " where N.DebtorID = D._rowID" & _
                  "    and N._createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                   " and D.clientschemeID = CS._rowID" & _
                  " and CS.branchID = 26" & _
                  " and N.type = 'Enf. Agent Note'" & _
                 " group by 1" & _
                  " order by D._rowid", debt_dt, False)
        Dim debtorRows As Integer = debt_dt.Rows.Count
        For Each debtRow In debt_dt.Rows
            Dim debtorID As Integer = debtRow(0)
            'T79496 ignore batch 4001 loaded 21.3.2016  
            Dim batch As String = ""
            Try
                batch = debtRow(15)
            Catch ex As Exception

            End Try
            Dim LoadDate As Date = debtRow(16)
            If batch = "4001" Then
                If Format(LoadDate, "yyyy-MM-dd") = CDate("2016-03-21") Then
                    Continue For
                End If
            End If
            Dim lastNoteRowid As Integer = 0
            Dim lastNoteType As String = ""
            Dim xmlWritten As Boolean = False
            Dim CSID As Integer = debtRow(17)
            Dim clientRef As String = debtRow(1)
            'T55090 check for phone number changes
            Dim phone_dt As New DataTable

            Dim caseStatus As String = debtRow(2)
            Dim arrangeStarted As Date
            Try
                arrangeStarted = debtRow(3)
            Catch ex As Exception
                arrangeStarted = Nothing
            End Try
            Dim totalBalance As Decimal = debtRow(5)

            Dim cancelledNote As String = ""
            'get all notes for last week
            Dim enfAgentNote(10) As String
            Dim enfAgentDate(10) As Date
            Dim enfAgentIDX As Integer = 0
            
            Dim note_dt As New DataTable
            LoadDataTable2("DebtRecovery", " select type, text, _createdDate, _createdBy, _rowid" & _
                          " from Note" & _
                          " where debtorID = " & debtorID & _
                          " and _createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                          " and _createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                           " and type = 'Enf. Agent Note'" & _
                          " order by _rowID", note_dt, False)

            For Each noteRow In note_dt.Rows
                Dim noteType As String = noteRow(0)
                Dim noteText As String = noteRow(1)
                Dim noteDate As Date = noteRow(2)
                Dim noteAgent As String = noteRow(3)
                Dim noteRowid As Integer = noteRow(4)
                Select Case noteType
                   
                    Case "Enf. Agent note"
                        'only enter field visits for certain clientschemes
                        If CSID <> 4823 And CSID <> 4972 And CSID <> 4999 And CSID <> 5000 And CSID <> 5010 And CSID <> 5011 And CSID <> 5012 And CSID <> 5013 Then
                            Continue For
                        End If
                        'build up all agent ref notes for checking at end
                        Dim agentNoteDate As Date = CDate(Format(noteDate, "yyyy-MM-dd" & " 00:00:00"))
                        If enfAgentIDX = 0 Then
                            enfAgentIDX = 1
                            enfAgentDate(1) = agentNoteDate
                            enfAgentNote(1) &= LCase(noteText)
                        Else
                            Dim datefound As Boolean = False
                            Dim agIDX As Integer = 0
                            For agIDX = 1 To enfAgentIDX
                                If enfAgentDate(agIDX) = agentNoteDate Then
                                    datefound = True
                                    Exit For
                                End If
                            Next
                            If datefound Then
                                enfAgentNote(agIDX) &= LCase(noteText)
                            Else
                                enfAgentIDX += 1
                                enfAgentDate(enfAgentIDX) = agentNoteDate
                                enfAgentNote(enfAgentIDX) = LCase(noteText)
                            End If
                        End If
                End Select
                lastNoteRowid = noteRowid
                lastNoteType = noteType
            Next
            'check if any enf agent notes
            If enfAgentIDX > 0 Then
                If Not xmlWritten Then
                    writer.WriteStartElement("Acct")
                    writer.WriteElementString("AcctID", clientRef)
                    writer.WriteElementString("ClntCd", clientCode)
                    writer.WriteStartElement("Actns")
                    xmlWritten = True
                End If
                'check each agent visits by date
                For agIDX = 1 To enfAgentIDX
                    writer.WriteStartElement("Actn")
                    writer.WriteElementString("ActnTS", Format(enfAgentDate(agIDX), "yyyy-MM-dd") & "T" & Format(enfAgentDate(agIDX), "HH:mm:ss"))
                    writer.WriteElementString("Tp", "FVM")

                    'get property type
                    Dim propertyType As String = ""
                    If InStr(enfAgentNote(agIDX), "semi-detached") > 0 Then
                        propertyType = "SED"
                    ElseIf InStr(enfAgentNote(agIDX), "detached") > 0 Then
                        propertyType = "DET"
                    ElseIf InStr(enfAgentNote(agIDX), "terraced") > 0 Then
                        propertyType = "TER"
                    ElseIf InStr(enfAgentNote(agIDX), "flat") > 0 Then
                        propertyType = "FLT"
                    ElseIf InStr(enfAgentNote(agIDX), "bungalow") > 0 Then
                        propertyType = "BUN"
                    End If
                   
                    'check if spoke to is found
                    Dim spokeTo As Integer = InStr(enfAgentNote(agIDX), "spoke to defaulter") + InStr(enfAgentNote(agIDX), "spoke to partner") &
                        +InStr(enfAgentNote(agIDX), "spoke to husband") + InStr(enfAgentNote(agIDX), "spoke to wife") + InStr(enfAgentNote(agIDX), "spoke to new occupier") &
                        +InStr(enfAgentNote(agIDX), "spoke to defaulter") + InStr(enfAgentNote(agIDX), "spoke to other")
                    'T112533 change cd to OutTp
                    If spokeTo > 0 Then
                        If InStr(enfAgentNote(agIDX), "spoke to nobody") > 0 Then
                            writer.WriteElementString("OutTp", "FVU")
                        Else
                            writer.WriteElementString("OutTp", "FVA")
                        End If
                    Else
                        writer.WriteElementString("OutTp", "FVD")
                    End If

                    If propertyType <> "" Then
                        
                        Dim pcode As String
                        Try
                            pcode = debtRow(14)
                        Catch ex As Exception
                            pcode = ""
                        End Try
                        Dim address As String
                        Try
                            address = debtRow(13)
                        Catch ex As Exception
                            address = ""
                        End Try
                        get_address(address, pcode)
                        writer.WriteStartElement("CtctMtd")
                        writer.WriteStartElement("Adr")
                        writer.WriteElementString("Strt1", street1)
                        If street2 <> "" Then
                            writer.WriteElementString("Strt2", street2)
                        End If
                        If street3 <> "" Then
                            writer.WriteElementString("Strt3", street3)
                        End If
                        'T66170 if town is blank use city
                        If town = "" Then
                            town = city
                            city = ""
                        End If
                        writer.WriteElementString("Twn", town)
                        If city <> "" Then
                            writer.WriteElementString("Cty", city)
                        End If
                        'If postcode <> "" Then
                        writer.WriteElementString("PstCd", postcode)
                        'End If
                        writer.WriteEndElement()   'adr
                        writer.WriteEndElement()  'CtctMtd
                        writer.WriteStartElement("FldVst")
                        writer.WriteElementString("PrptyType", propertyType)
                        writer.WriteEndElement()  'FldVst
                    End If
                   
                    writer.WriteEndElement()  'actn
                Next
            End If
            If xmlWritten Then
                case_no += 1
                writer.WriteEndElement()  'actns
                writer.WriteEndElement()  'acct
                'If case_no >= 10 Then
                '    Exit For
                'End If
            End If
        Next  'debtorID
        'Next  'CSID

        writer.Close()

        'validate using xsd
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(activity_file)
        myDocument.Schemas.Add("ActivityData", "\\ross-helm-fp001\Rossendales Shared\vb.net\TDX XSD\Activity_Data.xsd")
        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
        write_audit(error_no & " - errors in report finished at " & Now, False)
        If error_no = 0 Then
            If prod_run Then
                'copy file to home office crystal reports on R drive
                Dim newFilename As String = "\\ross-helm-fp001\Rossendales Shared\Home Office Crystal Reports\" & Path.GetFileName(activity_file)
                My.Computer.FileSystem.CopyFile(activity_file, newFilename)

            End If

        End If
        Me.Close()
    End Sub
    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_audit("Error: " & e.Message, True)
            Case XmlSeverityType.Warning
                write_audit("Warning " & e.Message, True)
        End Select
    End Sub
    Private Sub get_address(ByVal addr As String, ByVal pcode As String)
        street1 = ""
        street2 = ""
        street3 = ""
        town = ""
        city = ""
        postcode = ""
        Try
            postcode = pcode
        Catch ex As Exception

        End Try
        Dim address = ""
        Try
            address = addr
        Catch ex As Exception
            Return
        End Try
        Dim addressline As String = ""
        For idx = 1 To address.Length
            If Mid(address, idx, 1) = Chr(10) Or Mid(address, idx, 1) = Chr(13) Then
                If street1 = "" Then
                    street1 = addressline
                ElseIf street2 = "" Then
                    street2 = addressline
                ElseIf town = "" Then
                    town = addressline
                ElseIf city = "" Then
                    city = addressline
                End If
                addressline = ""
            Else
                addressline &= Mid(address, idx, 1)
            End If
        Next
        'check for postcode and shuffle town/city
        If city = postcode Then
            city = ""
        ElseIf town = postcode Then
            town = ""
        ElseIf street2 = postcode Then
            street2 = ""
        End If
        If UCase(city) = "GB" Then
            city = ""
        End If
        If UCase(town) = "GB" Then
            town = ""
        End If
        If city = "" Then
            city = town
            town = street2
            street2 = ""
        End If
        If city = "" Then
            city = town
            town = street2
            street2 = ""
        End If
        If IsNumeric(Microsoft.VisualBasic.Left(town, 1)) Then
            If street2 = "" Then
                street2 = town
                town = ""
            Else
                street3 = town
                town = ""
            End If
        End If
        If street1 = "" Then
            street1 = addressline
        End If
    End Sub
    Private Sub write_audit(ByVal audit_message As String, ByVal errorMessage As Boolean)
        If errorMessage Then
            error_no += 1
        End If
        audit_message = audit_message & vbNewLine
        My.Computer.FileSystem.WriteAllText(auditFile, audit_message, True)
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        run_report()
    End Sub
End Class

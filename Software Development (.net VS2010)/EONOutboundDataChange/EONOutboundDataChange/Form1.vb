﻿Imports CommonLibrary
Public Class Form1

    Dim ascii As New System.Text.ASCIIEncoding()
    Dim prod_run As Boolean = False
    Dim lastDataChangeDate As Date
    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try

        If env_str = "Prod" Then
            prod_run = True
        Else
            MsgBox("Test run only")
            'clientID = 24
        End If
        'get latest date from eon_query_file

        Try
            lastDataChangeDate = GetSQLResults("FeesSQL", "select eon_datachange_date from eon_datachange_file" & _
                                            " where eon_datachange_seqID = (select max(eon_datachange_seqID) from eon_datachange_file)")
        Catch ex As Exception
            lastDataChangeDate = CDate("Jan 1, 2017")
        End Try
        rundtpdate.Value = lastDataChangeDate
        rundtptime.Value = lastDataChangeDate
        
    End Sub

    Private Sub processbtn_Click(sender As System.Object, e As System.EventArgs) Handles processbtn.Click
        Dim newDataChangeDate As Date = Now
        lastDataChangeDate = CDate(Format(rundtpdate.Value, "yyyy-MM-dd") & " " & Format(rundtptime.Value, "HH:mm:ss"))
        'get address changes for date interval
        Dim debt_dt As New DataTable
        Dim clientID As Integer
        If prod_run Then
            clientID = 1938
        Else
            clientID = 1938
        End If
        Dim outFile As String = ""
        Dim volume As Integer = 0
        LoadDataTable("DebtRecovery", "select D._rowid, D.client_ref,D.address,D.add_postcode, offence_number" & _
                       " from Debtor D, note N, clientScheme CS" & _
                       " where N.debtorID = D._rowID" & _
                       " and D.clientSchemeID = CS._rowID" & _
                       " and CS.clientID =" & clientID & _
                       " and N.type = 'Address'" & _
                       " and N.text like 'changed from:%'" & _
                       " and N._createdDate >= '" & Format(lastDataChangeDate, "yyyy-MM-dd") & "'" & _
                       " and N._createdDate < ' " & Format(newDataChangeDate, "yyyy-MM-dd") & "'", debt_dt, False)
        For Each debtrow In debt_dt.Rows
            Dim debtorID As Integer = debtrow(0)
            Dim clientRef As String = debtrow(1)
            Dim address As String = debtrow(2)
            Dim postcode As String = debtrow(3)
            Dim offenceNo As String = debtrow(4)
            'remove postcode from address
            address = Replace(address, postcode, "")
            'split address into address lines

            Dim addressLine1 As String = ""
            Dim addressLine2 As String = ""
            Dim addressLine3 As String = ""
            Dim addressLine4 As String = ""
            Dim addressNo As Integer = 1
            Dim addressLine As String = ""
            For idx = 1 To address.Length
                If Mid(address, idx, 1) = "," Or Mid(address, idx, 1) = Chr(10) Or Mid(address, idx, 1) = Chr(13) Then
                    Select Case addressNo
                        Case 1
                            addressLine1 = addressLine
                            addressLine = ""
                        Case 2
                            addressLine2 = addressLine
                            addressLine = ""
                        Case 3
                            addressLine3 = addressLine
                            addressLine = ""
                        Case 4
                            addressLine4 = addressLine
                        Case Else
                            addressLine4 &= " " & addressLine
                    End Select
                    addressNo += 1
                Else
                    addressLine &= Mid(address, idx, 1)
                End If
            Next
            volume += 1
            outFile &= offenceNo & "," & clientRef & ",,,,CAD," & addressLine1 & "," & addressLine2 & "," & addressLine3 & "," & _
               addressLine4 & "," & postcode & vbNewLine
        Next

        'now get phone and name changes
        Dim phone_dt As New DataTable
        LoadDataTable("StudentLoans", "select BuildDate, add_phone, add_fax, empphone,empfax, name_title,name_fore, name_sur, debtorID" & _
                       " from rpt.EONDebtor" & _
                       " where BuildDate >= '" & lastDataChangeDate.ToString("yyyy-MMM-dd") & "'" & _
                       " and BuildDate < '" & newDataChangeDate.ToString("yyyy-MMM-dd") & "'", phone_dt, False)
        For Each phoneRow In phone_dt.Rows
            Dim addPhone As String = ""
            Try
                addPhone = phoneRow(1)
            Catch ex As Exception

            End Try
            Dim addFax As String = ""
            Try
                addFax = phoneRow(2)
            Catch ex As Exception

            End Try
            Dim empPhone As String = ""
            Try
                empPhone = phoneRow(3)
            Catch ex As Exception

            End Try
            Dim empFax As String = ""
            Try
                empFax = phoneRow(4)
            Catch ex As Exception

            End Try
            Dim nameTitle As String = ""
            Try
                nameTitle = phoneRow(5)
            Catch ex As Exception

            End Try
            Dim nameFore As String = ""
            Try
                nameFore = phoneRow(6)
            Catch ex As Exception

            End Try
            Dim namesur As String = ""
            Try
                namesur = phoneRow(7)
            Catch ex As Exception

            End Try
            Dim fullName As String = ""
            If nameTitle <> "" Then
                fullName = nameTitle & " "
            End If
            If nameFore <> "" Then
                fullName &= nameFore & " "
            End If
            If namesur <> "" Then
                fullName &= namesur
            End If

            Dim debtorID As Integer = phoneRow(8)
            Dim clientRef As String = GetSQLResults("DebtRecovery", "select client_ref from debtor where _rowID = " & debtorID)
            Dim offenceNumber As String = GetSQLResults("DebtRecovery", "select offence_number from debtor where _rowID = " & debtorID)
            'ignore phone change if name change
            If fullName <> "" Then
                volume += 1
                outFile &= offenceNumber & "," & clientRef & "," & fullName & ",,,,,,,," & vbNewLine
            End If
            Dim telType As String
            If addPhone.Length > 5 And fullName = "" Then
                volume += 1
                If Microsoft.VisualBasic.Left(addPhone, 2) = "07" Then
                    telType = "MOB"
                Else
                    telType = "HOME"
                End If
                outFile &= offenceNumber & "," & clientRef & ",," & telType & "," & addPhone & ",,,,,," & vbNewLine
            End If
            If addFax.Length > 5 And fullName = "" Then
                volume += 1
                If Microsoft.VisualBasic.Left(addFax, 2) = "07" Then
                    telType = "MOB"
                Else
                    telType = "HOME"
                End If
                outFile &= offenceNumber & "," & clientRef & ",," & telType & "," & addFax & ",,,,,," & vbNewLine
            End If
            If empPhone.Length > 5 And fullName = "" Then
                volume += 1
                If Microsoft.VisualBasic.Left(empPhone, 2) = "07" Then
                    telType = "MOB"
                Else
                    telType = "WORK"
                End If
                outFile &= offenceNumber & "," & clientRef & ",," & telType & "," & empPhone & ",,,,,," & vbNewLine
            End If
            If empFax.Length > 5 And fullName = "" Then
                volume += 1
                If Microsoft.VisualBasic.Left(empFax, 2) = "07" Then
                    telType = "MOB"
                Else
                    telType = "WORK"
                End If
                outFile &= offenceNumber & "," & clientRef & ",," & telType & "," & empFax & ",,,,,," & vbNewLine
            End If
        Next


        Dim filename As String = "CHA-EON-ROS-" & Format(Now, "ddMMyyyy") & ".csv"
        'add header and trailer
        Dim header As String = "CHA,EON," & Format(Now, "dd/MM/yyyy") & "," & filename & "," & volume & vbNewLine
        Dim trailer As String = "ROS" & vbNewLine
        outFile = header & outFile & trailer
        If prod_run Then
            'update datachange date
            Dim upd_txt As String = "insert into eon_datachange_file (eon_datachange_date) values ('" & Format(newDataChangeDate, "yyyy-MM-dd HH:mm:ss") & "')"

            update_sql(upd_txt)
        End If

        'write out file
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "CSV files |*.csv"
            .DefaultExt = ".csv"
            .OverwritePrompt = True
            .FileName = filename
        End With
        If Not SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            MsgBox("No file name selected for save ")
            Return
        End If
        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outFile, False, ascii)
        MsgBox("report saved")
        Me.Close()
    End Sub
End Class

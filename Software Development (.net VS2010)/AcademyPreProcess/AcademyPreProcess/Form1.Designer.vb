﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.clientbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.schemebtn = New System.Windows.Forms.Button()
        Me.processbtn = New System.Windows.Forms.Button()
        Me.cl_label = New System.Windows.Forms.Label()
        Me.sch_lbl = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.SuspendLayout()
        '
        'clientbtn
        '
        Me.clientbtn.Location = New System.Drawing.Point(83, 46)
        Me.clientbtn.Name = "clientbtn"
        Me.clientbtn.Size = New System.Drawing.Size(87, 23)
        Me.clientbtn.TabIndex = 0
        Me.clientbtn.Text = "Select Client"
        Me.clientbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(195, 218)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'schemebtn
        '
        Me.schemebtn.Location = New System.Drawing.Point(83, 102)
        Me.schemebtn.Name = "schemebtn"
        Me.schemebtn.Size = New System.Drawing.Size(87, 23)
        Me.schemebtn.TabIndex = 3
        Me.schemebtn.Text = "Select Scheme"
        Me.schemebtn.UseVisualStyleBackColor = True
        '
        'processbtn
        '
        Me.processbtn.Location = New System.Drawing.Point(83, 160)
        Me.processbtn.Name = "processbtn"
        Me.processbtn.Size = New System.Drawing.Size(87, 23)
        Me.processbtn.TabIndex = 4
        Me.processbtn.Text = "Process File"
        Me.processbtn.UseVisualStyleBackColor = True
        '
        'cl_label
        '
        Me.cl_label.AutoSize = True
        Me.cl_label.Location = New System.Drawing.Point(80, 30)
        Me.cl_label.Name = "cl_label"
        Me.cl_label.Size = New System.Drawing.Size(92, 13)
        Me.cl_label.TabIndex = 5
        Me.cl_label.Text = "No client selected"
        '
        'sch_lbl
        '
        Me.sch_lbl.AutoSize = True
        Me.sch_lbl.Location = New System.Drawing.Point(80, 86)
        Me.sch_lbl.Name = "sch_lbl"
        Me.sch_lbl.Size = New System.Drawing.Size(104, 13)
        Me.sch_lbl.TabIndex = 6
        Me.sch_lbl.Text = "No scheme selected"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.sch_lbl)
        Me.Controls.Add(Me.cl_label)
        Me.Controls.Add(Me.processbtn)
        Me.Controls.Add(Me.schemebtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.clientbtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Academy pre Process"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents clientbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents schemebtn As System.Windows.Forms.Button
    Friend WithEvents processbtn As System.Windows.Forms.Button
    Friend WithEvents cl_label As System.Windows.Forms.Label
    Friend WithEvents sch_lbl As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog

End Class

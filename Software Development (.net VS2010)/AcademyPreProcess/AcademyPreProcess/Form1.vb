﻿Public Class Form1
    Dim recycled_file As String
    Dim normal_file As String
    Dim filename As String
    Dim recycled_cases As Integer = 0
    Dim normal_cases As Integer = 0
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        selected_client_no = 0
        selected_client_name = ""
        selected_scheme_no = 0
        SELECTED_scheme_name = ""
    End Sub

    Private Sub schemebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles schemebtn.Click
        If selected_client_no = 0 Then
            MsgBox("Select Client first ")
            Exit Sub
        End If
        schemefrm.ShowDialog()
        sch_lbl.Text = selected_scheme_name
    End Sub

    Private Sub clientbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clientbtn.Click
        client_search = InputBox("Enter first part of client name", "Client Name")
        If client_search.Length = 0 Then
            Exit Sub
        End If
        clientfrm.ShowDialog()
        cl_label.Text = selected_client_name
    End Sub

    Private Sub processbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles processbtn.Click
        If selected_client_no = 0 Then
            MsgBox("Select Client first")
            Exit Sub
        End If
        If selected_scheme_no = 0 Then
            MsgBox("Select Scheme first")
            Exit Sub
        End If
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = False Then
            MsgBox("File NOT Opened")
            Exit Sub
        End If
        schemebtn.Enabled = False
        clientbtn.Enabled = False
        processbtn.Enabled = False
        exitbtn.Enabled = False
        'read file into array
        Dim file As String
        Dim lines As Integer = 0
        Dim line(0) As String
        Dim linetext As String = ""
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next
        For idx = 1 To lines - 1
            'ignore unless detail line
            If Mid(line(idx), 2, 1) <> "D" Then
                Continue For
            End If
            Dim cl_ref As String = Trim(Mid(line(idx), 4, 16))
            Dim lo_date As Date
            Try
                lo_date = Mid(line(idx), 392, 10)
            Catch ex As Exception

            End Try
            Dim debt_addr As String = Trim(Mid(line(idx), 412, 200))
            'see if case already exists on onestep
            param2 = "select _rowid, offence_details from Debtor where client_ref = '" & cl_ref & "'" &
                " and offence_court = '" & Format(lo_date, "yyyy-MM-dd") & "'" &
                " and clientSchemeID = " & selected_cs_id
            Dim debt_ds As DataSet = get_dataset("onestep", param2)
            If debt_ds.Tables(0).Rows.Count = 0 Then
                If normal_cases = 0 Then
                    normal_file = "header to be ignored" & vbNewLine
                End If
                normal_file = normal_file & Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 1) & vbNewLine
                normal_cases += 1
            Else
                'check debt address
                Dim onestep_debt_addr As String = debt_ds.Tables(0).Rows(0).Item(1)
                debt_addr = Replace(debt_addr, ",", " ")
                Dim adj_onestep_debt_addr As String = ""
                For idx2 = 1 To onestep_debt_addr.Length
                    If Mid(onestep_debt_addr, idx2, 1) = Chr(10) Or Mid(onestep_debt_addr, idx2, 1) = Chr(13) Then
                        adj_onestep_debt_addr = adj_onestep_debt_addr & " "
                    Else
                        adj_onestep_debt_addr = adj_onestep_debt_addr & Mid(onestep_debt_addr, idx2, 1)
                    End If
                Next
                If LCase(Microsoft.VisualBasic.Left(debt_addr, 15)) = LCase(Microsoft.VisualBasic.Left(adj_onestep_debt_addr, 15)) Then
                    If recycled_cases = 0 Then
                        recycled_file = "header to be ignored" & vbNewLine
                    End If
                    recycled_file = recycled_file & Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 1) & vbNewLine
                    recycled_cases += 1
                Else
                    If normal_cases = 0 Then
                        normal_file = "header to be ignored" & vbNewLine
                    End If
                    normal_file = normal_file & Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 1) & vbNewLine
                    normal_cases += 1
                End If
                
            End If
        Next

        MsgBox("Recycled cases = " & recycled_cases & vbNewLine &
               selected_scheme_name & " cases = " & normal_cases)

        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next

        If normal_cases > 0 Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_" & selected_scheme_name & "_preprocess.txt", normal_file, False)
        End If
        If recycled_cases > 0 Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_" & selected_scheme_name & "_recycled_preprocess.txt", recycled_file, False)
        End If
        Me.Close()
    End Sub
End Class

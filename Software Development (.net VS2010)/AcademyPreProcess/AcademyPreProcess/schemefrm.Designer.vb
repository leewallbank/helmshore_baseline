﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class schemefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.sch_dg = New System.Windows.Forms.DataGridView()
        Me.sch_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sch_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cs_id = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.sch_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'sch_dg
        '
        Me.sch_dg.AllowUserToAddRows = False
        Me.sch_dg.AllowUserToDeleteRows = False
        Me.sch_dg.AllowUserToOrderColumns = True
        Me.sch_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.sch_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.sch_name, Me.sch_id, Me.cs_id})
        Me.sch_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.sch_dg.Location = New System.Drawing.Point(0, 0)
        Me.sch_dg.Name = "sch_dg"
        Me.sch_dg.ReadOnly = True
        Me.sch_dg.Size = New System.Drawing.Size(475, 266)
        Me.sch_dg.TabIndex = 0
        '
        'sch_name
        '
        Me.sch_name.HeaderText = "Scheme name"
        Me.sch_name.Name = "sch_name"
        Me.sch_name.ReadOnly = True
        Me.sch_name.Width = 300
        '
        'sch_id
        '
        Me.sch_id.HeaderText = "SchemeID"
        Me.sch_id.Name = "sch_id"
        Me.sch_id.ReadOnly = True
        '
        'cs_id
        '
        Me.cs_id.HeaderText = "CSID"
        Me.cs_id.Name = "cs_id"
        Me.cs_id.ReadOnly = True
        Me.cs_id.Visible = False
        '
        'schemefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(475, 266)
        Me.Controls.Add(Me.sch_dg)
        Me.Name = "schemefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Double-click to select Scheme"
        CType(Me.sch_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents sch_dg As System.Windows.Forms.DataGridView
    Friend WithEvents sch_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sch_id As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cs_id As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

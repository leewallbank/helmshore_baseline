﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAllocationByRegionSummary
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.dgvEASummary = New System.Windows.Forms.DataGridView()
        Me.cmdClientClear = New System.Windows.Forms.Button()
        Me.cmdClientAll = New System.Windows.Forms.Button()
        Me.dgvClientName = New System.Windows.Forms.DataGridView()
        Me.ClientName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colClientNameTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvPostcodeArea = New System.Windows.Forms.DataGridView()
        Me.PostcodeArea = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdPostcodeAreaClear = New System.Windows.Forms.Button()
        Me.cmdPostcodeAreaAll = New System.Windows.Forms.Button()
        Me.dgvPostcodeDistrict = New System.Windows.Forms.DataGridView()
        Me.PostcodeDistrict = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdPostcodeDistrictClear = New System.Windows.Forms.Button()
        Me.cmdPostcodeDistrictAll = New System.Windows.Forms.Button()
        Me.dgvWorkType = New System.Windows.Forms.DataGridView()
        Me.WorkType = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdWorkTypeClear = New System.Windows.Forms.Button()
        Me.cmdWorkTypeAll = New System.Windows.Forms.Button()
        Me.cmdSchemeClear = New System.Windows.Forms.Button()
        Me.cmdStageComplianceClear = New System.Windows.Forms.Button()
        Me.cmdStageComplianceAll = New System.Windows.Forms.Button()
        Me.dgvSchemeName = New System.Windows.Forms.DataGridView()
        Me.SchemeName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Total = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdSchemeAll = New System.Windows.Forms.Button()
        Me.dgvStageNameCompliance = New System.Windows.Forms.DataGridView()
        Me.StageNameCompliance = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colStageNameComplianceTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvDebtYear = New System.Windows.Forms.DataGridView()
        Me.DebtYear = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdDebtYearClear = New System.Windows.Forms.Button()
        Me.dgvLinkedPIF = New System.Windows.Forms.DataGridView()
        Me.LinkedPIF = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdArrangementBrokenClear = New System.Windows.Forms.Button()
        Me.cmdDebtYearAll = New System.Windows.Forms.Button()
        Me.dgvArrangementBroken = New System.Windows.Forms.DataGridView()
        Me.ArrangementBroken = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdArrangementBrokenAll = New System.Windows.Forms.Button()
        Me.cmdLinkedPIFClear = New System.Windows.Forms.Button()
        Me.cmdLinkedPIFAll = New System.Windows.Forms.Button()
        Me.cmdInYearClear = New System.Windows.Forms.Button()
        Me.dgvInYear = New System.Windows.Forms.DataGridView()
        Me.InYear = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdInYearAll = New System.Windows.Forms.Button()
        Me.cmdPaymentClear = New System.Windows.Forms.Button()
        Me.cmdPaymentAll = New System.Windows.Forms.Button()
        Me.dgvAddConfirmed = New System.Windows.Forms.DataGridView()
        Me.AddConfirmed = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdCGAClear = New System.Windows.Forms.Button()
        Me.cmdCGAAll = New System.Windows.Forms.Button()
        Me.cmdAddConfirmedClear = New System.Windows.Forms.Button()
        Me.cmdAddConfirmedAll = New System.Windows.Forms.Button()
        Me.dgvPayment = New System.Windows.Forms.DataGridView()
        Me.Payment = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvCGA = New System.Windows.Forms.DataGridView()
        Me.CGA = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdNumberOfVisitsAll = New System.Windows.Forms.Button()
        Me.cmdNumberOfVisitsClear = New System.Windows.Forms.Button()
        Me.dgvNumberOfVisits = New System.Windows.Forms.DataGridView()
        Me.NumberOfVisits = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cboPeriodType = New System.Windows.Forms.ComboBox()
        Me.dgvSummary = New System.Windows.Forms.DataGridView()
        Me.lblSummary = New System.Windows.Forms.Label()
        Me.cmdClearAll = New System.Windows.Forms.Button()
        Me.cmsSummary = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cboAvailability = New System.Windows.Forms.ComboBox()
        Me.cboCompany = New System.Windows.Forms.ComboBox()
        Me.lblLine1 = New System.Windows.Forms.Label()
        Me.cmdStageEnforcementClear = New System.Windows.Forms.Button()
        Me.cmdStageEnforcementAll = New System.Windows.Forms.Button()
        Me.dgvStageNameEnforcement = New System.Windows.Forms.DataGridView()
        Me.StageNameEnforcement = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colStageNameEnforcementTotal = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdRegionClear = New System.Windows.Forms.Button()
        Me.cmsEASummary = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.dgvRegion = New System.Windows.Forms.DataGridView()
        Me.cmsForm = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.dgvAllocationsByUser = New System.Windows.Forms.DataGridView()
        Me.pnlBalance = New System.Windows.Forms.Panel()
        Me.radHigh = New System.Windows.Forms.RadioButton()
        Me.radLow = New System.Windows.Forms.RadioButton()
        Me.cmsAllocationsByUser = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.pnlAbsPerc = New System.Windows.Forms.Panel()
        Me.radPerc = New System.Windows.Forms.RadioButton()
        Me.radAbs = New System.Windows.Forms.RadioButton()
        Me.lblPeriodType = New System.Windows.Forms.Label()
        Me.cboDisplaySet = New System.Windows.Forms.ComboBox()
        Me.cmdRefreshDB = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.dgvLinkedArrangement = New System.Windows.Forms.DataGridView()
        Me.cmdLinkedArrangementClear = New System.Windows.Forms.Button()
        Me.cmdLinkedArrangementAll = New System.Windows.Forms.Button()
        Me.LinkedArrangement = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvEASummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPostcodeDistrict, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvWorkType, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSchemeName, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvStageNameCompliance, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDebtYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvLinkedPIF, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvArrangementBroken, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvInYear, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAddConfirmed, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvCGA, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvNumberOfVisits, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvStageNameEnforcement, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvRegion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAllocationsByUser, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlBalance.SuspendLayout()
        Me.pnlAbsPerc.SuspendLayout()
        CType(Me.dgvLinkedArrangement, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvEASummary
        '
        Me.dgvEASummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEASummary.Location = New System.Drawing.Point(173, 7)
        Me.dgvEASummary.MultiSelect = False
        Me.dgvEASummary.Name = "dgvEASummary"
        Me.dgvEASummary.ReadOnly = True
        Me.dgvEASummary.RowHeadersVisible = False
        Me.dgvEASummary.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEASummary.Size = New System.Drawing.Size(330, 509)
        Me.dgvEASummary.TabIndex = 0
        '
        'cmdClientClear
        '
        Me.cmdClientClear.Location = New System.Drawing.Point(761, 251)
        Me.cmdClientClear.Name = "cmdClientClear"
        Me.cmdClientClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientClear.TabIndex = 16
        Me.cmdClientClear.Text = "Clear"
        Me.cmdClientClear.UseVisualStyleBackColor = True
        '
        'cmdClientAll
        '
        Me.cmdClientAll.Location = New System.Drawing.Point(699, 251)
        Me.cmdClientAll.Name = "cmdClientAll"
        Me.cmdClientAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdClientAll.TabIndex = 15
        Me.cmdClientAll.Text = "All"
        Me.cmdClientAll.UseVisualStyleBackColor = True
        '
        'dgvClientName
        '
        Me.dgvClientName.AllowUserToAddRows = False
        Me.dgvClientName.AllowUserToDeleteRows = False
        Me.dgvClientName.AllowUserToResizeColumns = False
        Me.dgvClientName.AllowUserToResizeRows = False
        Me.dgvClientName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClientName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ClientName, Me.colClientNameTotal})
        Me.dgvClientName.Location = New System.Drawing.Point(676, 7)
        Me.dgvClientName.Name = "dgvClientName"
        Me.dgvClientName.ReadOnly = True
        Me.dgvClientName.RowHeadersVisible = False
        Me.dgvClientName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClientName.Size = New System.Drawing.Size(160, 244)
        Me.dgvClientName.TabIndex = 14
        '
        'ClientName
        '
        Me.ClientName.DataPropertyName = "ClientName"
        Me.ClientName.HeaderText = "Client"
        Me.ClientName.Name = "ClientName"
        Me.ClientName.ReadOnly = True
        '
        'colClientNameTotal
        '
        Me.colClientNameTotal.DataPropertyName = "Total"
        Me.colClientNameTotal.HeaderText = "Total"
        Me.colClientNameTotal.Name = "colClientNameTotal"
        Me.colClientNameTotal.ReadOnly = True
        Me.colClientNameTotal.Width = 40
        '
        'dgvPostcodeArea
        '
        Me.dgvPostcodeArea.AllowUserToAddRows = False
        Me.dgvPostcodeArea.AllowUserToDeleteRows = False
        Me.dgvPostcodeArea.AllowUserToResizeColumns = False
        Me.dgvPostcodeArea.AllowUserToResizeRows = False
        Me.dgvPostcodeArea.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPostcodeArea.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PostcodeArea, Me.DataGridViewTextBoxColumn9})
        Me.dgvPostcodeArea.Location = New System.Drawing.Point(676, 273)
        Me.dgvPostcodeArea.Name = "dgvPostcodeArea"
        Me.dgvPostcodeArea.ReadOnly = True
        Me.dgvPostcodeArea.RowHeadersVisible = False
        Me.dgvPostcodeArea.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPostcodeArea.Size = New System.Drawing.Size(160, 243)
        Me.dgvPostcodeArea.TabIndex = 41
        '
        'PostcodeArea
        '
        Me.PostcodeArea.DataPropertyName = "PostcodeArea"
        Me.PostcodeArea.HeaderText = "Area"
        Me.PostcodeArea.Name = "PostcodeArea"
        Me.PostcodeArea.ReadOnly = True
        Me.PostcodeArea.Width = 55
        '
        'DataGridViewTextBoxColumn9
        '
        Me.DataGridViewTextBoxColumn9.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn9.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn9.Name = "DataGridViewTextBoxColumn9"
        Me.DataGridViewTextBoxColumn9.ReadOnly = True
        Me.DataGridViewTextBoxColumn9.Width = 40
        '
        'cmdPostcodeAreaClear
        '
        Me.cmdPostcodeAreaClear.Location = New System.Drawing.Point(761, 516)
        Me.cmdPostcodeAreaClear.Name = "cmdPostcodeAreaClear"
        Me.cmdPostcodeAreaClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaClear.TabIndex = 43
        Me.cmdPostcodeAreaClear.Text = "Clear"
        Me.cmdPostcodeAreaClear.UseVisualStyleBackColor = True
        '
        'cmdPostcodeAreaAll
        '
        Me.cmdPostcodeAreaAll.Location = New System.Drawing.Point(699, 516)
        Me.cmdPostcodeAreaAll.Name = "cmdPostcodeAreaAll"
        Me.cmdPostcodeAreaAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeAreaAll.TabIndex = 42
        Me.cmdPostcodeAreaAll.Text = "All"
        Me.cmdPostcodeAreaAll.UseVisualStyleBackColor = True
        '
        'dgvPostcodeDistrict
        '
        Me.dgvPostcodeDistrict.AllowUserToAddRows = False
        Me.dgvPostcodeDistrict.AllowUserToDeleteRows = False
        Me.dgvPostcodeDistrict.AllowUserToResizeColumns = False
        Me.dgvPostcodeDistrict.AllowUserToResizeRows = False
        Me.dgvPostcodeDistrict.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPostcodeDistrict.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PostcodeDistrict, Me.DataGridViewTextBoxColumn2})
        Me.dgvPostcodeDistrict.Location = New System.Drawing.Point(843, 7)
        Me.dgvPostcodeDistrict.Name = "dgvPostcodeDistrict"
        Me.dgvPostcodeDistrict.ReadOnly = True
        Me.dgvPostcodeDistrict.RowHeadersVisible = False
        Me.dgvPostcodeDistrict.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPostcodeDistrict.Size = New System.Drawing.Size(155, 509)
        Me.dgvPostcodeDistrict.TabIndex = 44
        '
        'PostcodeDistrict
        '
        Me.PostcodeDistrict.DataPropertyName = "PostcodeDistrict"
        Me.PostcodeDistrict.HeaderText = "District"
        Me.PostcodeDistrict.Name = "PostcodeDistrict"
        Me.PostcodeDistrict.ReadOnly = True
        Me.PostcodeDistrict.Width = 55
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn2.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 40
        '
        'cmdPostcodeDistrictClear
        '
        Me.cmdPostcodeDistrictClear.Location = New System.Drawing.Point(927, 516)
        Me.cmdPostcodeDistrictClear.Name = "cmdPostcodeDistrictClear"
        Me.cmdPostcodeDistrictClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeDistrictClear.TabIndex = 46
        Me.cmdPostcodeDistrictClear.Text = "Clear"
        Me.cmdPostcodeDistrictClear.UseVisualStyleBackColor = True
        '
        'cmdPostcodeDistrictAll
        '
        Me.cmdPostcodeDistrictAll.Location = New System.Drawing.Point(865, 516)
        Me.cmdPostcodeDistrictAll.Name = "cmdPostcodeDistrictAll"
        Me.cmdPostcodeDistrictAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPostcodeDistrictAll.TabIndex = 45
        Me.cmdPostcodeDistrictAll.Text = "All"
        Me.cmdPostcodeDistrictAll.UseVisualStyleBackColor = True
        '
        'dgvWorkType
        '
        Me.dgvWorkType.AllowUserToAddRows = False
        Me.dgvWorkType.AllowUserToDeleteRows = False
        Me.dgvWorkType.AllowUserToResizeColumns = False
        Me.dgvWorkType.AllowUserToResizeRows = False
        Me.dgvWorkType.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvWorkType.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WorkType, Me.DataGridViewTextBoxColumn5})
        Me.dgvWorkType.Location = New System.Drawing.Point(1246, 428)
        Me.dgvWorkType.Name = "dgvWorkType"
        Me.dgvWorkType.ReadOnly = True
        Me.dgvWorkType.RowHeadersVisible = False
        Me.dgvWorkType.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvWorkType.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvWorkType.Size = New System.Drawing.Size(160, 88)
        Me.dgvWorkType.TabIndex = 53
        '
        'WorkType
        '
        Me.WorkType.DataPropertyName = "WorkType"
        Me.WorkType.HeaderText = "Work Type"
        Me.WorkType.Name = "WorkType"
        Me.WorkType.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn5.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        Me.DataGridViewTextBoxColumn5.Width = 40
        '
        'cmdWorkTypeClear
        '
        Me.cmdWorkTypeClear.Location = New System.Drawing.Point(1332, 516)
        Me.cmdWorkTypeClear.Name = "cmdWorkTypeClear"
        Me.cmdWorkTypeClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdWorkTypeClear.TabIndex = 55
        Me.cmdWorkTypeClear.Text = "Clear"
        Me.cmdWorkTypeClear.UseVisualStyleBackColor = True
        '
        'cmdWorkTypeAll
        '
        Me.cmdWorkTypeAll.Location = New System.Drawing.Point(1270, 516)
        Me.cmdWorkTypeAll.Name = "cmdWorkTypeAll"
        Me.cmdWorkTypeAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdWorkTypeAll.TabIndex = 54
        Me.cmdWorkTypeAll.Text = "All"
        Me.cmdWorkTypeAll.UseVisualStyleBackColor = True
        '
        'cmdSchemeClear
        '
        Me.cmdSchemeClear.Location = New System.Drawing.Point(1332, 399)
        Me.cmdSchemeClear.Name = "cmdSchemeClear"
        Me.cmdSchemeClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdSchemeClear.TabIndex = 52
        Me.cmdSchemeClear.Text = "Clear"
        Me.cmdSchemeClear.UseVisualStyleBackColor = True
        '
        'cmdStageComplianceClear
        '
        Me.cmdStageComplianceClear.Location = New System.Drawing.Point(594, 251)
        Me.cmdStageComplianceClear.Name = "cmdStageComplianceClear"
        Me.cmdStageComplianceClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageComplianceClear.TabIndex = 50
        Me.cmdStageComplianceClear.Text = "Clear"
        Me.cmdStageComplianceClear.UseVisualStyleBackColor = True
        '
        'cmdStageComplianceAll
        '
        Me.cmdStageComplianceAll.Location = New System.Drawing.Point(532, 251)
        Me.cmdStageComplianceAll.Name = "cmdStageComplianceAll"
        Me.cmdStageComplianceAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageComplianceAll.TabIndex = 49
        Me.cmdStageComplianceAll.Text = "All"
        Me.cmdStageComplianceAll.UseVisualStyleBackColor = True
        '
        'dgvSchemeName
        '
        Me.dgvSchemeName.AllowUserToAddRows = False
        Me.dgvSchemeName.AllowUserToDeleteRows = False
        Me.dgvSchemeName.AllowUserToResizeColumns = False
        Me.dgvSchemeName.AllowUserToResizeRows = False
        Me.dgvSchemeName.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSchemeName.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SchemeName, Me.Total})
        Me.dgvSchemeName.Location = New System.Drawing.Point(1246, 266)
        Me.dgvSchemeName.Name = "dgvSchemeName"
        Me.dgvSchemeName.ReadOnly = True
        Me.dgvSchemeName.RowHeadersVisible = False
        Me.dgvSchemeName.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSchemeName.Size = New System.Drawing.Size(160, 133)
        Me.dgvSchemeName.TabIndex = 48
        '
        'SchemeName
        '
        Me.SchemeName.DataPropertyName = "SchemeName"
        Me.SchemeName.HeaderText = "Scheme"
        Me.SchemeName.Name = "SchemeName"
        Me.SchemeName.ReadOnly = True
        '
        'Total
        '
        Me.Total.DataPropertyName = "Total"
        Me.Total.HeaderText = "Total"
        Me.Total.Name = "Total"
        Me.Total.ReadOnly = True
        Me.Total.Width = 40
        '
        'cmdSchemeAll
        '
        Me.cmdSchemeAll.Location = New System.Drawing.Point(1270, 399)
        Me.cmdSchemeAll.Name = "cmdSchemeAll"
        Me.cmdSchemeAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdSchemeAll.TabIndex = 51
        Me.cmdSchemeAll.Text = "All"
        Me.cmdSchemeAll.UseVisualStyleBackColor = True
        '
        'dgvStageNameCompliance
        '
        Me.dgvStageNameCompliance.AllowUserToAddRows = False
        Me.dgvStageNameCompliance.AllowUserToDeleteRows = False
        Me.dgvStageNameCompliance.AllowUserToResizeColumns = False
        Me.dgvStageNameCompliance.AllowUserToResizeRows = False
        Me.dgvStageNameCompliance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStageNameCompliance.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StageNameCompliance, Me.colStageNameComplianceTotal})
        Me.dgvStageNameCompliance.Location = New System.Drawing.Point(508, 7)
        Me.dgvStageNameCompliance.Name = "dgvStageNameCompliance"
        Me.dgvStageNameCompliance.ReadOnly = True
        Me.dgvStageNameCompliance.RowHeadersVisible = False
        Me.dgvStageNameCompliance.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvStageNameCompliance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvStageNameCompliance.Size = New System.Drawing.Size(160, 244)
        Me.dgvStageNameCompliance.TabIndex = 47
        '
        'StageNameCompliance
        '
        Me.StageNameCompliance.DataPropertyName = "StageName"
        Me.StageNameCompliance.HeaderText = "Stage"
        Me.StageNameCompliance.Name = "StageNameCompliance"
        Me.StageNameCompliance.ReadOnly = True
        Me.StageNameCompliance.Width = 90
        '
        'colStageNameComplianceTotal
        '
        Me.colStageNameComplianceTotal.DataPropertyName = "Total"
        Me.colStageNameComplianceTotal.HeaderText = "Total"
        Me.colStageNameComplianceTotal.Name = "colStageNameComplianceTotal"
        Me.colStageNameComplianceTotal.ReadOnly = True
        Me.colStageNameComplianceTotal.Width = 40
        '
        'dgvDebtYear
        '
        Me.dgvDebtYear.AllowUserToAddRows = False
        Me.dgvDebtYear.AllowUserToDeleteRows = False
        Me.dgvDebtYear.AllowUserToResizeColumns = False
        Me.dgvDebtYear.AllowUserToResizeRows = False
        Me.dgvDebtYear.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDebtYear.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DebtYear, Me.DataGridViewTextBoxColumn12})
        Me.dgvDebtYear.Location = New System.Drawing.Point(1005, 235)
        Me.dgvDebtYear.Name = "dgvDebtYear"
        Me.dgvDebtYear.ReadOnly = True
        Me.dgvDebtYear.RowHeadersVisible = False
        Me.dgvDebtYear.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvDebtYear.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDebtYear.Size = New System.Drawing.Size(115, 164)
        Me.dgvDebtYear.TabIndex = 119
        '
        'DebtYear
        '
        Me.DebtYear.DataPropertyName = "DebtYear"
        Me.DebtYear.HeaderText = "Year"
        Me.DebtYear.Name = "DebtYear"
        Me.DebtYear.ReadOnly = True
        Me.DebtYear.Width = 55
        '
        'DataGridViewTextBoxColumn12
        '
        Me.DataGridViewTextBoxColumn12.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn12.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn12.Name = "DataGridViewTextBoxColumn12"
        Me.DataGridViewTextBoxColumn12.ReadOnly = True
        Me.DataGridViewTextBoxColumn12.Width = 40
        '
        'cmdDebtYearClear
        '
        Me.cmdDebtYearClear.Location = New System.Drawing.Point(1069, 399)
        Me.cmdDebtYearClear.Name = "cmdDebtYearClear"
        Me.cmdDebtYearClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdDebtYearClear.TabIndex = 121
        Me.cmdDebtYearClear.Text = "Clear"
        Me.cmdDebtYearClear.UseVisualStyleBackColor = True
        '
        'dgvLinkedPIF
        '
        Me.dgvLinkedPIF.AllowUserToAddRows = False
        Me.dgvLinkedPIF.AllowUserToDeleteRows = False
        Me.dgvLinkedPIF.AllowUserToResizeColumns = False
        Me.dgvLinkedPIF.AllowUserToResizeRows = False
        Me.dgvLinkedPIF.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLinkedPIF.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LinkedPIF, Me.DataGridViewTextBoxColumn13})
        Me.dgvLinkedPIF.Location = New System.Drawing.Point(1005, 451)
        Me.dgvLinkedPIF.Name = "dgvLinkedPIF"
        Me.dgvLinkedPIF.ReadOnly = True
        Me.dgvLinkedPIF.RowHeadersVisible = False
        Me.dgvLinkedPIF.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvLinkedPIF.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLinkedPIF.Size = New System.Drawing.Size(115, 65)
        Me.dgvLinkedPIF.TabIndex = 122
        '
        'LinkedPIF
        '
        Me.LinkedPIF.DataPropertyName = "LinkedPIF"
        Me.LinkedPIF.HeaderText = "Linked PIF"
        Me.LinkedPIF.Name = "LinkedPIF"
        Me.LinkedPIF.ReadOnly = True
        Me.LinkedPIF.Width = 70
        '
        'DataGridViewTextBoxColumn13
        '
        Me.DataGridViewTextBoxColumn13.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn13.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn13.Name = "DataGridViewTextBoxColumn13"
        Me.DataGridViewTextBoxColumn13.ReadOnly = True
        Me.DataGridViewTextBoxColumn13.Width = 40
        '
        'cmdArrangementBrokenClear
        '
        Me.cmdArrangementBrokenClear.Location = New System.Drawing.Point(1189, 516)
        Me.cmdArrangementBrokenClear.Name = "cmdArrangementBrokenClear"
        Me.cmdArrangementBrokenClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdArrangementBrokenClear.TabIndex = 118
        Me.cmdArrangementBrokenClear.Text = "Clear"
        Me.cmdArrangementBrokenClear.UseVisualStyleBackColor = True
        '
        'cmdDebtYearAll
        '
        Me.cmdDebtYearAll.Location = New System.Drawing.Point(1007, 399)
        Me.cmdDebtYearAll.Name = "cmdDebtYearAll"
        Me.cmdDebtYearAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdDebtYearAll.TabIndex = 120
        Me.cmdDebtYearAll.Text = "All"
        Me.cmdDebtYearAll.UseVisualStyleBackColor = True
        '
        'dgvArrangementBroken
        '
        Me.dgvArrangementBroken.AllowUserToAddRows = False
        Me.dgvArrangementBroken.AllowUserToDeleteRows = False
        Me.dgvArrangementBroken.AllowUserToResizeColumns = False
        Me.dgvArrangementBroken.AllowUserToResizeRows = False
        Me.dgvArrangementBroken.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvArrangementBroken.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ArrangementBroken, Me.DataGridViewTextBoxColumn10})
        Me.dgvArrangementBroken.Location = New System.Drawing.Point(1127, 451)
        Me.dgvArrangementBroken.Name = "dgvArrangementBroken"
        Me.dgvArrangementBroken.ReadOnly = True
        Me.dgvArrangementBroken.RowHeadersVisible = False
        Me.dgvArrangementBroken.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvArrangementBroken.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvArrangementBroken.Size = New System.Drawing.Size(112, 65)
        Me.dgvArrangementBroken.TabIndex = 116
        '
        'ArrangementBroken
        '
        Me.ArrangementBroken.DataPropertyName = "ArrangementBroken"
        Me.ArrangementBroken.HeaderText = "Broken"
        Me.ArrangementBroken.Name = "ArrangementBroken"
        Me.ArrangementBroken.ReadOnly = True
        Me.ArrangementBroken.Width = 70
        '
        'DataGridViewTextBoxColumn10
        '
        Me.DataGridViewTextBoxColumn10.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn10.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn10.Name = "DataGridViewTextBoxColumn10"
        Me.DataGridViewTextBoxColumn10.ReadOnly = True
        Me.DataGridViewTextBoxColumn10.Width = 40
        '
        'cmdArrangementBrokenAll
        '
        Me.cmdArrangementBrokenAll.Location = New System.Drawing.Point(1127, 516)
        Me.cmdArrangementBrokenAll.Name = "cmdArrangementBrokenAll"
        Me.cmdArrangementBrokenAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdArrangementBrokenAll.TabIndex = 117
        Me.cmdArrangementBrokenAll.Text = "All"
        Me.cmdArrangementBrokenAll.UseVisualStyleBackColor = True
        '
        'cmdLinkedPIFClear
        '
        Me.cmdLinkedPIFClear.Location = New System.Drawing.Point(1069, 516)
        Me.cmdLinkedPIFClear.Name = "cmdLinkedPIFClear"
        Me.cmdLinkedPIFClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedPIFClear.TabIndex = 124
        Me.cmdLinkedPIFClear.Text = "Clear"
        Me.cmdLinkedPIFClear.UseVisualStyleBackColor = True
        '
        'cmdLinkedPIFAll
        '
        Me.cmdLinkedPIFAll.Location = New System.Drawing.Point(1007, 516)
        Me.cmdLinkedPIFAll.Name = "cmdLinkedPIFAll"
        Me.cmdLinkedPIFAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedPIFAll.TabIndex = 123
        Me.cmdLinkedPIFAll.Text = "All"
        Me.cmdLinkedPIFAll.UseVisualStyleBackColor = True
        '
        'cmdInYearClear
        '
        Me.cmdInYearClear.Location = New System.Drawing.Point(1189, 399)
        Me.cmdInYearClear.Name = "cmdInYearClear"
        Me.cmdInYearClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdInYearClear.TabIndex = 115
        Me.cmdInYearClear.Text = "Clear"
        Me.cmdInYearClear.UseVisualStyleBackColor = True
        '
        'dgvInYear
        '
        Me.dgvInYear.AllowUserToAddRows = False
        Me.dgvInYear.AllowUserToDeleteRows = False
        Me.dgvInYear.AllowUserToResizeColumns = False
        Me.dgvInYear.AllowUserToResizeRows = False
        Me.dgvInYear.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvInYear.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.InYear, Me.DataGridViewTextBoxColumn7})
        Me.dgvInYear.Location = New System.Drawing.Point(1127, 334)
        Me.dgvInYear.Name = "dgvInYear"
        Me.dgvInYear.ReadOnly = True
        Me.dgvInYear.RowHeadersVisible = False
        Me.dgvInYear.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvInYear.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvInYear.Size = New System.Drawing.Size(112, 65)
        Me.dgvInYear.TabIndex = 113
        '
        'InYear
        '
        Me.InYear.DataPropertyName = "InYear"
        Me.InYear.HeaderText = "In year"
        Me.InYear.Name = "InYear"
        Me.InYear.ReadOnly = True
        Me.InYear.Width = 55
        '
        'DataGridViewTextBoxColumn7
        '
        Me.DataGridViewTextBoxColumn7.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn7.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn7.Name = "DataGridViewTextBoxColumn7"
        Me.DataGridViewTextBoxColumn7.ReadOnly = True
        Me.DataGridViewTextBoxColumn7.Width = 40
        '
        'cmdInYearAll
        '
        Me.cmdInYearAll.Location = New System.Drawing.Point(1127, 399)
        Me.cmdInYearAll.Name = "cmdInYearAll"
        Me.cmdInYearAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdInYearAll.TabIndex = 114
        Me.cmdInYearAll.Text = "All"
        Me.cmdInYearAll.UseVisualStyleBackColor = True
        '
        'cmdPaymentClear
        '
        Me.cmdPaymentClear.Location = New System.Drawing.Point(1188, 300)
        Me.cmdPaymentClear.Name = "cmdPaymentClear"
        Me.cmdPaymentClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdPaymentClear.TabIndex = 109
        Me.cmdPaymentClear.Text = "Clear"
        Me.cmdPaymentClear.UseVisualStyleBackColor = True
        '
        'cmdPaymentAll
        '
        Me.cmdPaymentAll.Location = New System.Drawing.Point(1126, 300)
        Me.cmdPaymentAll.Name = "cmdPaymentAll"
        Me.cmdPaymentAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdPaymentAll.TabIndex = 108
        Me.cmdPaymentAll.Text = "All"
        Me.cmdPaymentAll.UseVisualStyleBackColor = True
        '
        'dgvAddConfirmed
        '
        Me.dgvAddConfirmed.AllowUserToAddRows = False
        Me.dgvAddConfirmed.AllowUserToDeleteRows = False
        Me.dgvAddConfirmed.AllowUserToResizeColumns = False
        Me.dgvAddConfirmed.AllowUserToResizeRows = False
        Me.dgvAddConfirmed.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAddConfirmed.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.AddConfirmed, Me.DataGridViewTextBoxColumn3})
        Me.dgvAddConfirmed.Location = New System.Drawing.Point(1127, 116)
        Me.dgvAddConfirmed.Name = "dgvAddConfirmed"
        Me.dgvAddConfirmed.ReadOnly = True
        Me.dgvAddConfirmed.RowHeadersVisible = False
        Me.dgvAddConfirmed.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvAddConfirmed.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAddConfirmed.Size = New System.Drawing.Size(112, 65)
        Me.dgvAddConfirmed.TabIndex = 110
        '
        'AddConfirmed
        '
        Me.AddConfirmed.DataPropertyName = "AddConfirmed"
        Me.AddConfirmed.HeaderText = "Confirmed"
        Me.AddConfirmed.Name = "AddConfirmed"
        Me.AddConfirmed.ReadOnly = True
        Me.AddConfirmed.Width = 70
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn3.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 40
        '
        'cmdCGAClear
        '
        Me.cmdCGAClear.Location = New System.Drawing.Point(1189, 72)
        Me.cmdCGAClear.Name = "cmdCGAClear"
        Me.cmdCGAClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdCGAClear.TabIndex = 107
        Me.cmdCGAClear.Text = "Clear"
        Me.cmdCGAClear.UseVisualStyleBackColor = True
        '
        'cmdCGAAll
        '
        Me.cmdCGAAll.Location = New System.Drawing.Point(1127, 72)
        Me.cmdCGAAll.Name = "cmdCGAAll"
        Me.cmdCGAAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdCGAAll.TabIndex = 106
        Me.cmdCGAAll.Text = "All"
        Me.cmdCGAAll.UseVisualStyleBackColor = True
        '
        'cmdAddConfirmedClear
        '
        Me.cmdAddConfirmedClear.Location = New System.Drawing.Point(1189, 181)
        Me.cmdAddConfirmedClear.Name = "cmdAddConfirmedClear"
        Me.cmdAddConfirmedClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdAddConfirmedClear.TabIndex = 112
        Me.cmdAddConfirmedClear.Text = "Clear"
        Me.cmdAddConfirmedClear.UseVisualStyleBackColor = True
        '
        'cmdAddConfirmedAll
        '
        Me.cmdAddConfirmedAll.Location = New System.Drawing.Point(1127, 181)
        Me.cmdAddConfirmedAll.Name = "cmdAddConfirmedAll"
        Me.cmdAddConfirmedAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdAddConfirmedAll.TabIndex = 111
        Me.cmdAddConfirmedAll.Text = "All"
        Me.cmdAddConfirmedAll.UseVisualStyleBackColor = True
        '
        'dgvPayment
        '
        Me.dgvPayment.AllowUserToAddRows = False
        Me.dgvPayment.AllowUserToDeleteRows = False
        Me.dgvPayment.AllowUserToResizeColumns = False
        Me.dgvPayment.AllowUserToResizeRows = False
        Me.dgvPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPayment.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Payment, Me.DataGridViewTextBoxColumn1})
        Me.dgvPayment.Location = New System.Drawing.Point(1126, 235)
        Me.dgvPayment.Name = "dgvPayment"
        Me.dgvPayment.ReadOnly = True
        Me.dgvPayment.RowHeadersVisible = False
        Me.dgvPayment.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvPayment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPayment.Size = New System.Drawing.Size(112, 65)
        Me.dgvPayment.TabIndex = 104
        '
        'Payment
        '
        Me.Payment.DataPropertyName = "Payment"
        Me.Payment.HeaderText = "Payment"
        Me.Payment.Name = "Payment"
        Me.Payment.ReadOnly = True
        Me.Payment.Width = 70
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn1.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 40
        '
        'dgvCGA
        '
        Me.dgvCGA.AllowUserToAddRows = False
        Me.dgvCGA.AllowUserToDeleteRows = False
        Me.dgvCGA.AllowUserToResizeColumns = False
        Me.dgvCGA.AllowUserToResizeRows = False
        Me.dgvCGA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCGA.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CGA, Me.DataGridViewTextBoxColumn4})
        Me.dgvCGA.Location = New System.Drawing.Point(1127, 7)
        Me.dgvCGA.Name = "dgvCGA"
        Me.dgvCGA.ReadOnly = True
        Me.dgvCGA.RowHeadersVisible = False
        Me.dgvCGA.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvCGA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCGA.Size = New System.Drawing.Size(112, 65)
        Me.dgvCGA.TabIndex = 105
        '
        'CGA
        '
        Me.CGA.DataPropertyName = "CGA"
        Me.CGA.HeaderText = "CGA"
        Me.CGA.Name = "CGA"
        Me.CGA.ReadOnly = True
        Me.CGA.Width = 70
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn4.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        Me.DataGridViewTextBoxColumn4.Width = 40
        '
        'cmdNumberOfVisitsAll
        '
        Me.cmdNumberOfVisitsAll.Location = New System.Drawing.Point(1007, 181)
        Me.cmdNumberOfVisitsAll.Name = "cmdNumberOfVisitsAll"
        Me.cmdNumberOfVisitsAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdNumberOfVisitsAll.TabIndex = 126
        Me.cmdNumberOfVisitsAll.Text = "All"
        Me.cmdNumberOfVisitsAll.UseVisualStyleBackColor = True
        '
        'cmdNumberOfVisitsClear
        '
        Me.cmdNumberOfVisitsClear.Location = New System.Drawing.Point(1069, 181)
        Me.cmdNumberOfVisitsClear.Name = "cmdNumberOfVisitsClear"
        Me.cmdNumberOfVisitsClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdNumberOfVisitsClear.TabIndex = 127
        Me.cmdNumberOfVisitsClear.Text = "Clear"
        Me.cmdNumberOfVisitsClear.UseVisualStyleBackColor = True
        '
        'dgvNumberOfVisits
        '
        Me.dgvNumberOfVisits.AllowUserToAddRows = False
        Me.dgvNumberOfVisits.AllowUserToDeleteRows = False
        Me.dgvNumberOfVisits.AllowUserToResizeColumns = False
        Me.dgvNumberOfVisits.AllowUserToResizeRows = False
        Me.dgvNumberOfVisits.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvNumberOfVisits.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumberOfVisits, Me.DataGridViewTextBoxColumn6})
        Me.dgvNumberOfVisits.Location = New System.Drawing.Point(1005, 7)
        Me.dgvNumberOfVisits.Name = "dgvNumberOfVisits"
        Me.dgvNumberOfVisits.ReadOnly = True
        Me.dgvNumberOfVisits.RowHeadersVisible = False
        Me.dgvNumberOfVisits.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvNumberOfVisits.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvNumberOfVisits.Size = New System.Drawing.Size(115, 174)
        Me.dgvNumberOfVisits.TabIndex = 125
        '
        'NumberOfVisits
        '
        Me.NumberOfVisits.DataPropertyName = "NumberOfVisits"
        Me.NumberOfVisits.HeaderText = "# Visits"
        Me.NumberOfVisits.Name = "NumberOfVisits"
        Me.NumberOfVisits.ReadOnly = True
        Me.NumberOfVisits.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.NumberOfVisits.Width = 70
        '
        'DataGridViewTextBoxColumn6
        '
        Me.DataGridViewTextBoxColumn6.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn6.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn6.Name = "DataGridViewTextBoxColumn6"
        Me.DataGridViewTextBoxColumn6.ReadOnly = True
        Me.DataGridViewTextBoxColumn6.Width = 40
        '
        'cboPeriodType
        '
        Me.cboPeriodType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPeriodType.FormattingEnabled = True
        Me.cboPeriodType.Location = New System.Drawing.Point(953, 559)
        Me.cboPeriodType.Name = "cboPeriodType"
        Me.cboPeriodType.Size = New System.Drawing.Size(110, 21)
        Me.cboPeriodType.TabIndex = 128
        '
        'dgvSummary
        '
        Me.dgvSummary.ColumnHeadersHeight = 21
        Me.dgvSummary.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvSummary.Location = New System.Drawing.Point(319, 587)
        Me.dgvSummary.Name = "dgvSummary"
        Me.dgvSummary.ReadOnly = True
        Me.dgvSummary.RowHeadersVisible = False
        Me.dgvSummary.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvSummary.Size = New System.Drawing.Size(770, 42)
        Me.dgvSummary.TabIndex = 129
        '
        'lblSummary
        '
        Me.lblSummary.AutoSize = True
        Me.lblSummary.Location = New System.Drawing.Point(321, 563)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(39, 13)
        Me.lblSummary.TabIndex = 130
        Me.lblSummary.Text = "Label1"
        '
        'cmdClearAll
        '
        Me.cmdClearAll.Location = New System.Drawing.Point(1337, 559)
        Me.cmdClearAll.Name = "cmdClearAll"
        Me.cmdClearAll.Size = New System.Drawing.Size(68, 20)
        Me.cmdClearAll.TabIndex = 131
        Me.cmdClearAll.Text = "Clear All"
        Me.cmdClearAll.UseVisualStyleBackColor = True
        '
        'cmsSummary
        '
        Me.cmsSummary.Name = "ContextMenuStrip1"
        Me.cmsSummary.Size = New System.Drawing.Size(61, 4)
        '
        'cboAvailability
        '
        Me.cboAvailability.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAvailability.FormattingEnabled = True
        Me.cboAvailability.Items.AddRange(New Object() {"Available now", "Within 2 days", "Within 5 days", "Within 10 days", ">10 days", "All"})
        Me.cboAvailability.Location = New System.Drawing.Point(1134, 559)
        Me.cboAvailability.Name = "cboAvailability"
        Me.cboAvailability.Size = New System.Drawing.Size(95, 21)
        Me.cboAvailability.TabIndex = 133
        '
        'cboCompany
        '
        Me.cboCompany.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCompany.FormattingEnabled = True
        Me.cboCompany.Location = New System.Drawing.Point(6, 7)
        Me.cboCompany.Name = "cboCompany"
        Me.cboCompany.Size = New System.Drawing.Size(160, 21)
        Me.cboCompany.TabIndex = 162
        '
        'lblLine1
        '
        Me.lblLine1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lblLine1.Location = New System.Drawing.Point(6, 551)
        Me.lblLine1.Name = "lblLine1"
        Me.lblLine1.Size = New System.Drawing.Size(1402, 1)
        Me.lblLine1.TabIndex = 163
        '
        'cmdStageEnforcementClear
        '
        Me.cmdStageEnforcementClear.Location = New System.Drawing.Point(594, 384)
        Me.cmdStageEnforcementClear.Name = "cmdStageEnforcementClear"
        Me.cmdStageEnforcementClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageEnforcementClear.TabIndex = 166
        Me.cmdStageEnforcementClear.Text = "Clear"
        Me.cmdStageEnforcementClear.UseVisualStyleBackColor = True
        '
        'cmdStageEnforcementAll
        '
        Me.cmdStageEnforcementAll.Location = New System.Drawing.Point(532, 384)
        Me.cmdStageEnforcementAll.Name = "cmdStageEnforcementAll"
        Me.cmdStageEnforcementAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdStageEnforcementAll.TabIndex = 165
        Me.cmdStageEnforcementAll.Text = "All"
        Me.cmdStageEnforcementAll.UseVisualStyleBackColor = True
        '
        'dgvStageNameEnforcement
        '
        Me.dgvStageNameEnforcement.AllowUserToAddRows = False
        Me.dgvStageNameEnforcement.AllowUserToDeleteRows = False
        Me.dgvStageNameEnforcement.AllowUserToResizeColumns = False
        Me.dgvStageNameEnforcement.AllowUserToResizeRows = False
        Me.dgvStageNameEnforcement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStageNameEnforcement.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.StageNameEnforcement, Me.colStageNameEnforcementTotal})
        Me.dgvStageNameEnforcement.Location = New System.Drawing.Point(508, 273)
        Me.dgvStageNameEnforcement.Name = "dgvStageNameEnforcement"
        Me.dgvStageNameEnforcement.ReadOnly = True
        Me.dgvStageNameEnforcement.RowHeadersVisible = False
        Me.dgvStageNameEnforcement.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvStageNameEnforcement.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvStageNameEnforcement.Size = New System.Drawing.Size(160, 111)
        Me.dgvStageNameEnforcement.TabIndex = 164
        '
        'StageNameEnforcement
        '
        Me.StageNameEnforcement.DataPropertyName = "StageName"
        Me.StageNameEnforcement.HeaderText = "Stage"
        Me.StageNameEnforcement.Name = "StageNameEnforcement"
        Me.StageNameEnforcement.ReadOnly = True
        Me.StageNameEnforcement.Width = 90
        '
        'colStageNameEnforcementTotal
        '
        Me.colStageNameEnforcementTotal.DataPropertyName = "Total"
        Me.colStageNameEnforcementTotal.HeaderText = "Total"
        Me.colStageNameEnforcementTotal.Name = "colStageNameEnforcementTotal"
        Me.colStageNameEnforcementTotal.ReadOnly = True
        Me.colStageNameEnforcementTotal.Width = 40
        '
        'cmdRegionClear
        '
        Me.cmdRegionClear.Location = New System.Drawing.Point(59, 401)
        Me.cmdRegionClear.Name = "cmdRegionClear"
        Me.cmdRegionClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdRegionClear.TabIndex = 168
        Me.cmdRegionClear.Text = "Clear"
        Me.cmdRegionClear.UseVisualStyleBackColor = True
        '
        'cmsEASummary
        '
        Me.cmsEASummary.Name = "cmsEASumm"
        Me.cmsEASummary.Size = New System.Drawing.Size(61, 4)
        '
        'dgvRegion
        '
        Me.dgvRegion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvRegion.ColumnHeadersVisible = False
        Me.dgvRegion.Location = New System.Drawing.Point(6, 50)
        Me.dgvRegion.Name = "dgvRegion"
        Me.dgvRegion.ReadOnly = True
        Me.dgvRegion.RowHeadersVisible = False
        Me.dgvRegion.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvRegion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRegion.Size = New System.Drawing.Size(160, 351)
        Me.dgvRegion.TabIndex = 169
        '
        'cmsForm
        '
        Me.cmsForm.Name = "ContextMenuStrip1"
        Me.cmsForm.Size = New System.Drawing.Size(61, 4)
        '
        'dgvAllocationsByUser
        '
        Me.dgvAllocationsByUser.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAllocationsByUser.Location = New System.Drawing.Point(1246, 8)
        Me.dgvAllocationsByUser.MultiSelect = False
        Me.dgvAllocationsByUser.Name = "dgvAllocationsByUser"
        Me.dgvAllocationsByUser.ReadOnly = True
        Me.dgvAllocationsByUser.RowHeadersVisible = False
        Me.dgvAllocationsByUser.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvAllocationsByUser.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAllocationsByUser.Size = New System.Drawing.Size(160, 219)
        Me.dgvAllocationsByUser.TabIndex = 170
        '
        'pnlBalance
        '
        Me.pnlBalance.Controls.Add(Me.radHigh)
        Me.pnlBalance.Controls.Add(Me.radLow)
        Me.pnlBalance.Location = New System.Drawing.Point(508, 559)
        Me.pnlBalance.Name = "pnlBalance"
        Me.pnlBalance.Size = New System.Drawing.Size(197, 25)
        Me.pnlBalance.TabIndex = 171
        '
        'radHigh
        '
        Me.radHigh.AutoCheck = False
        Me.radHigh.AutoSize = True
        Me.radHigh.Location = New System.Drawing.Point(106, 4)
        Me.radHigh.Name = "radHigh"
        Me.radHigh.Size = New System.Drawing.Size(73, 17)
        Me.radHigh.TabIndex = 1
        Me.radHigh.TabStop = True
        Me.radHigh.Text = "> £10,000"
        Me.radHigh.UseVisualStyleBackColor = True
        '
        'radLow
        '
        Me.radLow.AutoCheck = False
        Me.radLow.AutoSize = True
        Me.radLow.Location = New System.Drawing.Point(6, 4)
        Me.radLow.Name = "radLow"
        Me.radLow.Size = New System.Drawing.Size(86, 17)
        Me.radLow.TabIndex = 0
        Me.radLow.TabStop = True
        Me.radLow.Text = "< £50 no link"
        Me.radLow.UseVisualStyleBackColor = True
        '
        'cmsAllocationsByUser
        '
        Me.cmsAllocationsByUser.Name = "cmsAllocationsByUser"
        Me.cmsAllocationsByUser.Size = New System.Drawing.Size(61, 4)
        '
        'pnlAbsPerc
        '
        Me.pnlAbsPerc.Controls.Add(Me.radPerc)
        Me.pnlAbsPerc.Controls.Add(Me.radAbs)
        Me.pnlAbsPerc.Location = New System.Drawing.Point(1241, 559)
        Me.pnlAbsPerc.Name = "pnlAbsPerc"
        Me.pnlAbsPerc.Size = New System.Drawing.Size(99, 25)
        Me.pnlAbsPerc.TabIndex = 172
        '
        'radPerc
        '
        Me.radPerc.AutoSize = True
        Me.radPerc.Location = New System.Drawing.Point(55, 4)
        Me.radPerc.Name = "radPerc"
        Me.radPerc.Size = New System.Drawing.Size(33, 17)
        Me.radPerc.TabIndex = 4
        Me.radPerc.TabStop = True
        Me.radPerc.Text = "%"
        Me.radPerc.UseVisualStyleBackColor = True
        '
        'radAbs
        '
        Me.radAbs.AutoSize = True
        Me.radAbs.Location = New System.Drawing.Point(6, 4)
        Me.radAbs.Name = "radAbs"
        Me.radAbs.Size = New System.Drawing.Size(43, 17)
        Me.radAbs.TabIndex = 3
        Me.radAbs.TabStop = True
        Me.radAbs.Text = "Abs"
        Me.radAbs.UseVisualStyleBackColor = True
        '
        'lblPeriodType
        '
        Me.lblPeriodType.Location = New System.Drawing.Point(874, 563)
        Me.lblPeriodType.Name = "lblPeriodType"
        Me.lblPeriodType.Size = New System.Drawing.Size(73, 16)
        Me.lblPeriodType.TabIndex = 173
        Me.lblPeriodType.Text = "based on:"
        Me.lblPeriodType.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'cboDisplaySet
        '
        Me.cboDisplaySet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboDisplaySet.FormattingEnabled = True
        Me.cboDisplaySet.Items.AddRange(New Object() {"Months", "Periods"})
        Me.cboDisplaySet.Location = New System.Drawing.Point(832, 559)
        Me.cboDisplaySet.Name = "cboDisplaySet"
        Me.cboDisplaySet.Size = New System.Drawing.Size(59, 21)
        Me.cboDisplaySet.TabIndex = 174
        '
        'cmdRefreshDB
        '
        Me.cmdRefreshDB.Location = New System.Drawing.Point(6, 560)
        Me.cmdRefreshDB.Name = "cmdRefreshDB"
        Me.cmdRefreshDB.Size = New System.Drawing.Size(75, 20)
        Me.cmdRefreshDB.TabIndex = 175
        Me.cmdRefreshDB.Text = "Refresh DB"
        Me.cmdRefreshDB.UseVisualStyleBackColor = True
        '
        'dgvLinkedArrangement
        '
        Me.dgvLinkedArrangement.AllowUserToAddRows = False
        Me.dgvLinkedArrangement.AllowUserToDeleteRows = False
        Me.dgvLinkedArrangement.AllowUserToResizeColumns = False
        Me.dgvLinkedArrangement.AllowUserToResizeRows = False
        Me.dgvLinkedArrangement.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.dgvLinkedArrangement.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LinkedArrangement, Me.DataGridViewTextBoxColumn11})
        Me.dgvLinkedArrangement.Location = New System.Drawing.Point(529, 451)
        Me.dgvLinkedArrangement.Name = "dgvLinkedArrangement"
        Me.dgvLinkedArrangement.ReadOnly = True
        Me.dgvLinkedArrangement.RowHeadersVisible = False
        Me.dgvLinkedArrangement.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvLinkedArrangement.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvLinkedArrangement.Size = New System.Drawing.Size(115, 65)
        Me.dgvLinkedArrangement.TabIndex = 176
        '
        'cmdLinkedArrangementClear
        '
        Me.cmdLinkedArrangementClear.Location = New System.Drawing.Point(593, 516)
        Me.cmdLinkedArrangementClear.Name = "cmdLinkedArrangementClear"
        Me.cmdLinkedArrangementClear.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedArrangementClear.TabIndex = 178
        Me.cmdLinkedArrangementClear.Text = "Clear"
        Me.cmdLinkedArrangementClear.UseVisualStyleBackColor = True
        '
        'cmdLinkedArrangementAll
        '
        Me.cmdLinkedArrangementAll.Location = New System.Drawing.Point(531, 516)
        Me.cmdLinkedArrangementAll.Name = "cmdLinkedArrangementAll"
        Me.cmdLinkedArrangementAll.Size = New System.Drawing.Size(50, 20)
        Me.cmdLinkedArrangementAll.TabIndex = 177
        Me.cmdLinkedArrangementAll.Text = "All"
        Me.cmdLinkedArrangementAll.UseVisualStyleBackColor = True
        '
        'LinkedArrangement
        '
        Me.LinkedArrangement.DataPropertyName = "LinkedArrangement"
        Me.LinkedArrangement.HeaderText = "Linked Arr"
        Me.LinkedArrangement.Name = "LinkedArrangement"
        Me.LinkedArrangement.ReadOnly = True
        Me.LinkedArrangement.Width = 70
        '
        'DataGridViewTextBoxColumn11
        '
        Me.DataGridViewTextBoxColumn11.DataPropertyName = "Total"
        Me.DataGridViewTextBoxColumn11.HeaderText = "Total"
        Me.DataGridViewTextBoxColumn11.Name = "DataGridViewTextBoxColumn11"
        Me.DataGridViewTextBoxColumn11.ReadOnly = True
        Me.DataGridViewTextBoxColumn11.Width = 40
        '
        'frmAllocationByRegionSummary
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1414, 636)
        Me.Controls.Add(Me.dgvLinkedArrangement)
        Me.Controls.Add(Me.cmdLinkedArrangementClear)
        Me.Controls.Add(Me.cmdLinkedArrangementAll)
        Me.Controls.Add(Me.cmdRefreshDB)
        Me.Controls.Add(Me.cboDisplaySet)
        Me.Controls.Add(Me.lblPeriodType)
        Me.Controls.Add(Me.pnlAbsPerc)
        Me.Controls.Add(Me.pnlBalance)
        Me.Controls.Add(Me.dgvAllocationsByUser)
        Me.Controls.Add(Me.dgvRegion)
        Me.Controls.Add(Me.cmdRegionClear)
        Me.Controls.Add(Me.cmdStageEnforcementClear)
        Me.Controls.Add(Me.cmdStageEnforcementAll)
        Me.Controls.Add(Me.dgvStageNameEnforcement)
        Me.Controls.Add(Me.lblLine1)
        Me.Controls.Add(Me.cboCompany)
        Me.Controls.Add(Me.cboAvailability)
        Me.Controls.Add(Me.cmdClearAll)
        Me.Controls.Add(Me.lblSummary)
        Me.Controls.Add(Me.dgvSummary)
        Me.Controls.Add(Me.cboPeriodType)
        Me.Controls.Add(Me.cmdNumberOfVisitsAll)
        Me.Controls.Add(Me.cmdNumberOfVisitsClear)
        Me.Controls.Add(Me.dgvNumberOfVisits)
        Me.Controls.Add(Me.dgvDebtYear)
        Me.Controls.Add(Me.cmdDebtYearClear)
        Me.Controls.Add(Me.dgvLinkedPIF)
        Me.Controls.Add(Me.cmdArrangementBrokenClear)
        Me.Controls.Add(Me.cmdDebtYearAll)
        Me.Controls.Add(Me.dgvArrangementBroken)
        Me.Controls.Add(Me.cmdArrangementBrokenAll)
        Me.Controls.Add(Me.cmdLinkedPIFClear)
        Me.Controls.Add(Me.cmdLinkedPIFAll)
        Me.Controls.Add(Me.cmdInYearClear)
        Me.Controls.Add(Me.dgvInYear)
        Me.Controls.Add(Me.cmdInYearAll)
        Me.Controls.Add(Me.cmdPaymentClear)
        Me.Controls.Add(Me.cmdPaymentAll)
        Me.Controls.Add(Me.dgvAddConfirmed)
        Me.Controls.Add(Me.cmdCGAClear)
        Me.Controls.Add(Me.cmdCGAAll)
        Me.Controls.Add(Me.cmdAddConfirmedClear)
        Me.Controls.Add(Me.cmdAddConfirmedAll)
        Me.Controls.Add(Me.dgvPayment)
        Me.Controls.Add(Me.dgvCGA)
        Me.Controls.Add(Me.dgvWorkType)
        Me.Controls.Add(Me.cmdWorkTypeClear)
        Me.Controls.Add(Me.cmdWorkTypeAll)
        Me.Controls.Add(Me.cmdSchemeClear)
        Me.Controls.Add(Me.cmdStageComplianceClear)
        Me.Controls.Add(Me.cmdStageComplianceAll)
        Me.Controls.Add(Me.dgvSchemeName)
        Me.Controls.Add(Me.cmdSchemeAll)
        Me.Controls.Add(Me.dgvStageNameCompliance)
        Me.Controls.Add(Me.dgvPostcodeDistrict)
        Me.Controls.Add(Me.cmdPostcodeDistrictClear)
        Me.Controls.Add(Me.cmdPostcodeDistrictAll)
        Me.Controls.Add(Me.dgvPostcodeArea)
        Me.Controls.Add(Me.cmdPostcodeAreaClear)
        Me.Controls.Add(Me.cmdPostcodeAreaAll)
        Me.Controls.Add(Me.cmdClientClear)
        Me.Controls.Add(Me.cmdClientAll)
        Me.Controls.Add(Me.dgvClientName)
        Me.Controls.Add(Me.dgvEASummary)
        Me.Name = "frmAllocationByRegionSummary"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Allocation By Region"
        CType(Me.dgvEASummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvClientName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPostcodeArea, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPostcodeDistrict, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvWorkType, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSchemeName, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvStageNameCompliance, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDebtYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvLinkedPIF, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvArrangementBroken, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvInYear, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAddConfirmed, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvPayment, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvCGA, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvNumberOfVisits, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvSummary, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvStageNameEnforcement, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvRegion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAllocationsByUser, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlBalance.ResumeLayout(False)
        Me.pnlBalance.PerformLayout()
        Me.pnlAbsPerc.ResumeLayout(False)
        Me.pnlAbsPerc.PerformLayout()
        CType(Me.dgvLinkedArrangement, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout

End Sub
    Friend WithEvents dgvEASummary As System.Windows.Forms.DataGridView
    Friend WithEvents cmdClientClear As System.Windows.Forms.Button
    Friend WithEvents cmdClientAll As System.Windows.Forms.Button
    Friend WithEvents dgvClientName As System.Windows.Forms.DataGridView
    Friend WithEvents ClientName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClientNameTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvPostcodeArea As System.Windows.Forms.DataGridView
    Friend WithEvents PostcodeArea As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdPostcodeAreaClear As System.Windows.Forms.Button
    Friend WithEvents cmdPostcodeAreaAll As System.Windows.Forms.Button
    Friend WithEvents dgvPostcodeDistrict As System.Windows.Forms.DataGridView
    Friend WithEvents PostcodeDistrict As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdPostcodeDistrictClear As System.Windows.Forms.Button
    Friend WithEvents cmdPostcodeDistrictAll As System.Windows.Forms.Button
    Friend WithEvents dgvWorkType As System.Windows.Forms.DataGridView
    Friend WithEvents WorkType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdWorkTypeClear As System.Windows.Forms.Button
    Friend WithEvents cmdWorkTypeAll As System.Windows.Forms.Button
    Friend WithEvents cmdSchemeClear As System.Windows.Forms.Button
    Friend WithEvents cmdStageComplianceClear As System.Windows.Forms.Button
    Friend WithEvents cmdStageComplianceAll As System.Windows.Forms.Button
    Friend WithEvents dgvSchemeName As System.Windows.Forms.DataGridView
    Friend WithEvents SchemeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Total As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdSchemeAll As System.Windows.Forms.Button
    Friend WithEvents dgvStageNameCompliance As System.Windows.Forms.DataGridView
    Friend WithEvents dgvDebtYear As System.Windows.Forms.DataGridView
    Friend WithEvents DebtYear As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn12 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdDebtYearClear As System.Windows.Forms.Button
    Friend WithEvents dgvLinkedPIF As System.Windows.Forms.DataGridView
    Friend WithEvents LinkedPIF As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn13 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdArrangementBrokenClear As System.Windows.Forms.Button
    Friend WithEvents cmdDebtYearAll As System.Windows.Forms.Button
    Friend WithEvents dgvArrangementBroken As System.Windows.Forms.DataGridView
    Friend WithEvents ArrangementBroken As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn10 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdArrangementBrokenAll As System.Windows.Forms.Button
    Friend WithEvents cmdLinkedPIFClear As System.Windows.Forms.Button
    Friend WithEvents cmdLinkedPIFAll As System.Windows.Forms.Button
    Friend WithEvents cmdInYearClear As System.Windows.Forms.Button
    Friend WithEvents dgvInYear As System.Windows.Forms.DataGridView
    Friend WithEvents InYear As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdInYearAll As System.Windows.Forms.Button
    Friend WithEvents cmdPaymentClear As System.Windows.Forms.Button
    Friend WithEvents cmdPaymentAll As System.Windows.Forms.Button
    Friend WithEvents dgvAddConfirmed As System.Windows.Forms.DataGridView
    Friend WithEvents AddConfirmed As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdCGAClear As System.Windows.Forms.Button
    Friend WithEvents cmdCGAAll As System.Windows.Forms.Button
    Friend WithEvents cmdAddConfirmedClear As System.Windows.Forms.Button
    Friend WithEvents cmdAddConfirmedAll As System.Windows.Forms.Button
    Friend WithEvents dgvPayment As System.Windows.Forms.DataGridView
    Friend WithEvents Payment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dgvCGA As System.Windows.Forms.DataGridView
    Friend WithEvents CGA As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdNumberOfVisitsAll As System.Windows.Forms.Button
    Friend WithEvents cmdNumberOfVisitsClear As System.Windows.Forms.Button
    Friend WithEvents dgvNumberOfVisits As System.Windows.Forms.DataGridView
    Friend WithEvents NumberOfVisits As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cboPeriodType As System.Windows.Forms.ComboBox
    Friend WithEvents dgvSummary As System.Windows.Forms.DataGridView
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents cmdClearAll As System.Windows.Forms.Button
    Friend WithEvents cmsSummary As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cboAvailability As System.Windows.Forms.ComboBox
    Friend WithEvents cboCompany As System.Windows.Forms.ComboBox
    Friend WithEvents lblLine1 As System.Windows.Forms.Label
    Friend WithEvents cmdStageEnforcementClear As System.Windows.Forms.Button
    Friend WithEvents cmdStageEnforcementAll As System.Windows.Forms.Button
    Friend WithEvents dgvStageNameEnforcement As System.Windows.Forms.DataGridView
    Friend WithEvents StageNameEnforcement As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colStageNameEnforcementTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents StageNameCompliance As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colStageNameComplianceTotal As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdRegionClear As System.Windows.Forms.Button
    Friend WithEvents cmsEASummary As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents dgvRegion As System.Windows.Forms.DataGridView
    Friend WithEvents cmsForm As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents dgvAllocationsByUser As System.Windows.Forms.DataGridView
    Friend WithEvents pnlBalance As System.Windows.Forms.Panel
    Friend WithEvents radHigh As System.Windows.Forms.RadioButton
    Friend WithEvents radLow As System.Windows.Forms.RadioButton
    Friend WithEvents cmsAllocationsByUser As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents pnlAbsPerc As System.Windows.Forms.Panel
    Friend WithEvents radPerc As System.Windows.Forms.RadioButton
    Friend WithEvents radAbs As System.Windows.Forms.RadioButton
    Friend WithEvents lblPeriodType As System.Windows.Forms.Label
    Friend WithEvents cboDisplaySet As System.Windows.Forms.ComboBox
    Friend WithEvents cmdRefreshDB As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents dgvLinkedArrangement As System.Windows.Forms.DataGridView
    Friend WithEvents cmdLinkedArrangementClear As System.Windows.Forms.Button
    Friend WithEvents cmdLinkedArrangementAll As System.Windows.Forms.Button
    Friend WithEvents LinkedArrangement As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn11 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

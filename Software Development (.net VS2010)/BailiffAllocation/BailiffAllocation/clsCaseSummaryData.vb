﻿Imports CommonLibrary

Public Class clsCaseSummaryData

    Private LastLoadDT As New DataTable
    Private SummaryDT As New DataTable
    Private WarningSummaryDT As New DataTable
    Private ClientAllocationLevelDT As New DataTable
    Private UnderAllocatedClientDT As New DataTable
    Private DetailDT As New DataTable
    Private PeriodTypeDT As New DataTable
    Private CompanyDT As New DataTable
    Private PDLAttributesDT As New DataTable

    Private LastLoadDV As DataView
    Private SummaryDV As DataView
    Private WarningSummaryDV As DataView
    Private ClientAllocationLevelDV As DataView
    Private UnderAllocatedClientDV As DataView
    Private DetailDV As DataView
    Private PeriodTypeDV As DataView
    Private CompanyDV As DataView
    Private MapCaseDV As DataView

    Public Sub New()
        Try

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPeriodTypes 'C'", PeriodTypeDT)
            PeriodTypeDV = New DataView(PeriodTypeDT)
            PeriodTypeDV.AllowDelete = False
            PeriodTypeDV.AllowNew = False

            ' UserCompanyID added TS 31/Mar/2016. Request ref 73695
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetCompanies 'C', " & UserCompanyID.ToString, CompanyDT)
            CompanyDV = New DataView(CompanyDT)
            CompanyDV.AllowDelete = False
            CompanyDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#Region " Public Properties"

    Public ReadOnly Property LastLoadDataView() As DataView
        Get
            LastLoadDataView = LastLoadDV
        End Get
    End Property

    Public ReadOnly Property SummaryDataView() As DataView
        Get
            SummaryDataView = SummaryDV
        End Get
    End Property

    Public ReadOnly Property WarningSummaryDataView() As DataView
        Get
            WarningSummaryDataView = WarningSummaryDV
        End Get
    End Property
    Public ReadOnly Property DetailDataView() As DataView
        Get
            DetailDataView = DetailDV
        End Get
    End Property

    Public ReadOnly Property CompanyDataView() As DataView
        Get
            CompanyDataView = CompanyDV
        End Get
    End Property

    Public ReadOnly Property PeriodTypeDataView() As DataView
        Get
            PeriodTypeDataView = PeriodTypeDV
        End Get
    End Property

    Public ReadOnly Property ClientAllocationLevelDataView() As DataView
        Get
            ClientAllocationLevelDataView = ClientAllocationLevelDV
        End Get
    End Property

    Public ReadOnly Property UnderAllocatedClientDataView() As DataView
        Get
            UnderAllocatedClientDataView = UnderAllocatedClientDV
        End Get
    End Property

    Public ReadOnly Property PDLAttributes() As DataTable
        Get
            PDLAttributes = PDLAttributesDT
        End Get
    End Property

#End Region

#Region "Public Methods"

    Public Sub GetSummary(ByVal ParamList As String, ByVal Abs As Boolean)
        Try
            Dim SummaryParamList As String = "", WarningSummaryParamList As String = ""

            SummaryDV = Nothing ' Needed to clear column list otherwise new columns eg clientname are added at the end
            WarningSummaryDV = Nothing

            If Abs Then SummaryParamList = ParamList & ",'A'" Else SummaryParamList = ParamList & ",'P'"

            'SummaryParamList &= "," & DisplaySet
            WarningSummaryParamList = ParamList '& "," & DisplaySet

            SummaryDT.Clear()
            SummaryDT.Reset()

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetSummary " & SummaryParamList, SummaryDT)
            SummaryDV = New DataView(SummaryDT)
            SummaryDV.AllowDelete = False
            SummaryDV.AllowNew = False

            WarningSummaryDT.Clear()
            WarningSummaryDT.Reset()

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetWarningSummary " & WarningSummaryParamList, WarningSummaryDT)
            WarningSummaryDV = New DataView(WarningSummaryDT)
            WarningSummaryDV.AllowDelete = False
            WarningSummaryDV.AllowNew = False

            GetClientAllocationLevels()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetClientAllocationLevels()
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetClientAllocation", ClientAllocationLevelDT)
            ClientAllocationLevelDV = New DataView(ClientAllocationLevelDT)
            ClientAllocationLevelDV.AllowDelete = False
            ClientAllocationLevelDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetUnderAllocatedClients()
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetUnderAllocatedClients", UnderAllocatedClientDT)
            UnderAllocatedClientDV = New DataView(UnderAllocatedClientDT)
            UnderAllocatedClientDV.AllowDelete = False
            UnderAllocatedClientDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetCases(ByVal ParamList As String, ByVal Period As String)
        Try
            ParamList += "," & Period.ToString

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetCases " & ParamList, DetailDT, True)
            DetailDV = New DataView(DetailDT.DefaultView.ToTable(True))
            DetailDV.AllowDelete = False
            DetailDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetBailiffCases(ByVal ParamList As String, ByVal Period As String)
        Try
            ParamList += "," & Period.ToString

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffCases " & ParamList, DetailDT, True)
            DetailDV = New DataView(DetailDT.DefaultView.ToTable(True))
            DetailDV.AllowDelete = False
            DetailDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetPostEnforcementCases(ByVal ParamList As String, ByVal Period As String)
        Try
            ParamList += "," & Period.ToString

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementCases " & ParamList, DetailDT, True)
            DetailDV = New DataView(DetailDT.DefaultView.ToTable(True))
            DetailDV.AllowDelete = False
            DetailDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetEAUpdateCases(ByVal ParamList As String, ByVal Period As String)
        Try
            ParamList += "," & Period.ToString

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateCases " & ParamList, DetailDT, True)
            DetailDV = New DataView(DetailDT.DefaultView.ToTable(True))
            DetailDV.AllowDelete = False
            DetailDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetAllocationByRegionCases(ByVal ParamList As String, ByVal Period As String)
        Try
            ParamList += "," & Period.ToString

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetAllocationByRegionCases " & ParamList, DetailDT, True)
            DetailDV = New DataView(DetailDT.DefaultView.ToTable(True))
            DetailDV.AllowDelete = False
            DetailDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetCasesByBailiff(ByVal BailiffID As Integer)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetCasesByBailiff " & BailiffID.ToString, DetailDT, True)
            DetailDV = New DataView(DetailDT)
            DetailDV.AllowDelete = False
            DetailDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetAllocationByUserCases(ByVal CompanyID As Integer, ByVal Username As String)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetAllocationByUserCases " & CompanyID.ToString & ", " & Username, DetailDT, True)
            DetailDV = New DataView(DetailDT)
            DetailDV.AllowDelete = False
            DetailDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetAllocationByUserBreakdownCases(ByVal CompanyID As Integer, ByVal Username As String, ByVal PostcodeArea As String, ByVal AllocationDate As Date)
        Try

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetAllocationByUserBreakdownCases " & CompanyID.ToString & ", '" & Username & "', '" & PostcodeArea & "', '" & AllocationDate.ToString("dd/MMM/yyyy") & "'", DetailDT, True)
            DetailDV = New DataView(DetailDT)
            DetailDV.AllowDelete = False
            DetailDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetLastLoad()
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetLastLoad", LastLoadDT)
            LastLoadDV = New DataView(LastLoadDT)
            LastLoadDV.AllowDelete = False
            LastLoadDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub RefreshDatabase()
        Try
            ExecStoredProc("BailiffAllocation", "EXEC dbo.BuildBailiffAllocation 'I'", 1800)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub SetClientAllocation(ByVal ClientName As String, ByVal AllocationDays As String, ByVal AllocationQuantity As String)
        Try
            ExecStoredProc("BailiffAllocation", "EXEC dbo.SetClientAllocation '" & ClientName & "'," & AllocationDays & "," & AllocationQuantity, 600)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetPDLAttributes(DebtorID As String)

        PDLAttributesDT.Clear()
        PDLAttributesDT.Reset()

        LoadDataTable("BailiffAllocation", "EXEC dbo.GetPDLAttributes " & DebtorID, PDLAttributesDT)

    End Sub

#End Region

End Class


﻿Public Class Form1
    Dim branch As Integer
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dtp.Value = DateAdd(DateInterval.Day, -1, Now)
    End Sub
    Private Sub get_visits()
        Dim outFile As String = "E/I,EAName,FirstNote,LastNote,LatestNoteToday,AddressesVisitedOn " & Format(dtp.Value, "dd/MM/yyyy") & vbNewLine

        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "CSV files |*.csv"
            .DefaultExt = ".csv"
            .OverwritePrompt = True
            .FileName = "EAVisits_" & Format(dtp.Value, "yyyy-MM-dd") & ".csv"
        End With
        If Not SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            MsgBox("No file name selected for save ")
            Return
        End If

        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outFile, False)

        exitbtn.Enabled = False
        br1btn.Enabled = False
        br10btn.Enabled = False
        ConnectDb2("DebtRecovery")

        'get all open EAs
        Dim EA_dt As New DataTable
        Dim firstNote, lastNote, latestNote As Date
        Dim lowDate As Date = CDate("Jan 1, 1900")
        Dim highDate As Date = CDate("Jan 1, 2100")
        Dim startDate As Date = dtp.Value
        Dim endDate As Date = DateAdd(DateInterval.Day, 1, startDate)
        endDate = CDate(Format(endDate, "yyyy-MM-dd") & " 00:00:00")
        startDate = CDate(Format(startDate, "yyyy-MM-dd") & " 00:00:00")
        LoadDataTable2("DebtRecovery", "SELECT _rowID, name_sur, name_fore,hasPen,login_name,branchID,internalExternal,typesub from Bailiff " & _
                                                " where status = 'O'" & _
                                                " and agent_type = 'B'" & _
                                                " and branchID = " & branch & _
                                                " and not(_rowid in (21,43))" & _
                                                " order by branchID,internalExternal desc, name_sur", EA_dt, False)
        ProgressBar1.Maximum = EA_dt.Rows.Count
        For Each EARow In EA_dt.Rows
            Dim typeSub As String = ""
            Try
                typeSub = EARow(7)
            Catch ex As Exception

            End Try
            If typeSub = "Employed" Then
                Continue For
            End If
            ProgressBar1.Value += 1
            Application.DoEvents()
            firstNote = highDate
            lastNote = lowDate
            latestNote = lowDate
            Dim EAName = EARow(1)
            Try
                EAName &= " " & EARow(2)
            Catch ex As Exception

            End Try
            Dim callsOnSelectedDate As Integer = 0
            Dim visit_dt As New DataTable
            Dim lastlinkID As Integer = 0
            'get all visits made on selected date
            LoadDataTable2("DebtRecovery", "SELECT date_visited, DebtorID, linkID from Visit V, debtor D " & _
                                               " where V.BailiffID = " & EARow(0) & _
                                               " and V.DebtorID = D._rowID" & _
                                               " and date_visited >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                                               " and date_visited < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                                               " order by linkID, DebtorID", visit_dt, False)
            'T67695 count addresses not visits
            For Each visitRow In visit_dt.Rows
                Dim linkID As Integer
                Dim debtorID As Integer = visitRow(1)
                Try
                    linkID = visitRow(2)
                Catch ex As Exception
                    linkID = 0
                End Try
                'If visitRow(1) <> lastDebtorID Then
                '    callsOnSelectedDate += 1
                'End If
                If linkID <> lastlinkID Or linkID = 0 Then
                    callsOnSelectedDate += 1
                End If
                lastlinkID = linkID
            Next
            'get all notes made by EA on selected date
            Dim note_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select _createdDate,debtorID from Note" & _
                           " where _createdBy = '" & EARow(4) & "'" & _
                     " and _createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                     " order by debtorID", note_dt, False)
            For Each noteRow In note_dt.Rows
                If noteRow(0) < endDate Then
                    If noteRow(0) < firstNote Then
                        firstNote = noteRow(0)
                    End If
                    If noteRow(0) > lastNote Then
                        lastNote = noteRow(0)
                    End If
                   
                ElseIf noteRow(0) > latestNote Then
                    latestNote = noteRow(0)
                End If
            Next
            'get all penforms from yesterday
            Dim penform_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select _createdDate from penform" & _
                           " where bailiffID = " & EARow(0) & _
                           " and _createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'", penform_dt, False)
            For Each penformRow In penform_dt.Rows
                If penformRow(0) < endDate Then
                    If penformRow(0) < firstNote Then
                        firstNote = penformRow(0)
                    End If
                    If penformRow(0) > lastNote Then
                        lastNote = penformRow(0)
                    End If
                ElseIf penformRow(0) > latestNote Then
                    latestNote = penformRow(0)
                End If

            Next
            outFile = EARow(6) & "," & EAName & ","
            If firstNote = highDate Then
                outFile &= ","
            Else
                outFile &= firstNote & ","
            End If
            If lastNote = lowDate Then
                outFile &= ","
            Else
                outFile &= lastNote & ","
            End If
            If latestNote = lowDate Then
                outFile &= ","
            Else
                outFile &= latestNote & ","
            End If
            outFile &= callsOnSelectedDate & vbNewLine
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outFile, True)
            outFile = ""
        Next
        MsgBox("File saved")
        Me.Close()
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub br10btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles br10btn.Click
        branch = 10
        get_visits()
    End Sub

    Private Sub allbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles br1btn.Click
        branch = 1
        Get_visits()
    End Sub
End Class

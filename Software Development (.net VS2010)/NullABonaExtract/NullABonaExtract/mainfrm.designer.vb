<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.retnbtn = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.date_picker = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.clntbtn = New System.Windows.Forms.Button()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.cl_lbl = New System.Windows.Forms.Label()
        Me.nobatchrbtn = New System.Windows.Forms.RadioButton()
        Me.batchrbtn = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GroupBox1.SuspendLayout
        Me.SuspendLayout
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(218, 409)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 4
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = true
        '
        'retnbtn
        '
        Me.retnbtn.Location = New System.Drawing.Point(95, 322)
        Me.retnbtn.Name = "retnbtn"
        Me.retnbtn.Size = New System.Drawing.Size(125, 23)
        Me.retnbtn.TabIndex = 3
        Me.retnbtn.Text = "Split Returns"
        Me.retnbtn.UseVisualStyleBackColor = true
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(10, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = " "
        '
        'date_picker
        '
        Me.date_picker.Location = New System.Drawing.Point(110, 149)
        Me.date_picker.Name = "date_picker"
        Me.date_picker.Size = New System.Drawing.Size(125, 20)
        Me.date_picker.TabIndex = 2
        Me.date_picker.Value = New Date(2010, 5, 17, 0, 0, 0, 0)
        '
        'Label2
        '
        Me.Label2.AutoSize = true
        Me.Label2.Location = New System.Drawing.Point(134, 133)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Select remit date"
        '
        'clntbtn
        '
        Me.clntbtn.Location = New System.Drawing.Point(110, 55)
        Me.clntbtn.Name = "clntbtn"
        Me.clntbtn.Size = New System.Drawing.Size(125, 23)
        Me.clntbtn.TabIndex = 0
        Me.clntbtn.Text = "Select Client"
        Me.clntbtn.UseVisualStyleBackColor = true
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(15, 409)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 10
        '
        'cl_lbl
        '
        Me.cl_lbl.AutoSize = true
        Me.cl_lbl.Location = New System.Drawing.Point(125, 39)
        Me.cl_lbl.Name = "cl_lbl"
        Me.cl_lbl.Size = New System.Drawing.Size(95, 13)
        Me.cl_lbl.TabIndex = 11
        Me.cl_lbl.Text = " No selected client"
        '
        'nobatchrbtn
        '
        Me.nobatchrbtn.AutoSize = true
        Me.nobatchrbtn.Checked = true
        Me.nobatchrbtn.Location = New System.Drawing.Point(17, 28)
        Me.nobatchrbtn.Name = "nobatchrbtn"
        Me.nobatchrbtn.Size = New System.Drawing.Size(81, 17)
        Me.nobatchrbtn.TabIndex = 12
        Me.nobatchrbtn.TabStop = true
        Me.nobatchrbtn.Text = "No Batches"
        Me.nobatchrbtn.UseVisualStyleBackColor = true
        '
        'batchrbtn
        '
        Me.batchrbtn.AutoSize = true
        Me.batchrbtn.Location = New System.Drawing.Point(17, 61)
        Me.batchrbtn.Name = "batchrbtn"
        Me.batchrbtn.Size = New System.Drawing.Size(91, 17)
        Me.batchrbtn.TabIndex = 13
        Me.batchrbtn.Text = "Batches of 50"
        Me.batchrbtn.UseVisualStyleBackColor = true
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.nobatchrbtn)
        Me.GroupBox1.Controls.Add(Me.batchrbtn)
        Me.GroupBox1.Location = New System.Drawing.Point(93, 192)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 100)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = false
        Me.GroupBox1.Text = "Batch Option"
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(371, 464)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cl_lbl)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.clntbtn)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.date_picker)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.retnbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Nulla Bona Extract"
        Me.GroupBox1.ResumeLayout(false)
        Me.GroupBox1.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents retnbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents date_picker As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents clntbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents cl_lbl As System.Windows.Forms.Label
    Friend WithEvents nobatchrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents batchrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox

End Class

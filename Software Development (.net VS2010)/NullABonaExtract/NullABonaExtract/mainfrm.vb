Public Class mainfrm
    Dim first_letter As String
    Dim files_found As Boolean
    Dim NBfiles As Integer
    Dim Retnfiles As Integer
    Dim NBBatchNo As Integer
    Dim RetnBatchNo As Integer
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim days As Integer = Weekday(Now)
        date_picker.Value = DateAdd(DateInterval.Day, -days, Now)
        selected_scheme_name = ""
        selected_cl_name = ""
    End Sub

    Private Sub retnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles retnbtn.Click
        If selected_cl_name = "" Then
            MsgBox("Select client before doing the splits")
            Exit Sub
        End If
        disable_buttons()
        ProgressBar1.Value = 5
        retn_month = Format(date_picker.Value, "MM_MMM")
        retn_day = Format(date_picker.Value, "dd_ddd")

        'get all branch 1 clentschemes for client
        param2 = "select _rowid from clientScheme where clientID = " & selected_cl_id & _
            " and branchID = '1'"
        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        files_found = False
        Dim cs_rows As Integer = no_of_rows
        For idx = 0 To cs_rows - 1
            'get remittance number
            param2 = "select _rowid from Remit where clientschemeID = " & cs_ds.Tables(0).Rows(idx).Item(0) & _
            " and date = '" & Format(date_picker.Value, "yyyy-MM-dd") & "'"
            Dim remit_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim remit_no As Integer = remit_dataset.Tables(0).Rows(0).Item(0)
            process_remit(remit_no)
        Next
        If files_found Then
            MsgBox("All reports copied to directories")
        Else
            MsgBox("No reports were found")
        End If
        Me.Close()
    End Sub
    Public Sub process_remit(ByVal remit_no As Integer)

        file_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                                   retn_day & "\Remittances\" & Format(remit_no, "#") & "\"
        Dim file_name As String
        'create directories for returns
        If nobatchrbtn.Checked Then
            Dim dir_name As String = file_path & "Nulla Bona - " & remit_no
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
            dir_name = file_path & "Returns - " & remit_no
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
        End If
        Dim debtor As Integer

        Dim return_file_path As String = file_path & "Returns\"
        NBfiles = 0
        Retnfiles = 0
        NBBatchNo = 1
        RetnBatchNo = 1
        Try
            For Each foundFile As String In My.Computer.FileSystem.GetFiles _
       (return_file_path, FileIO.SearchOption.SearchTopLevelOnly, "*.pdf")
                'get debtor number
                files_found = True
                Try
                    ProgressBar1.Value += 5
                Catch ex As Exception
                    ProgressBar1.Value = 5
                End Try
                Dim dashIDX As Integer
                For dashIDX = foundFile.Length To 1 Step -1
                    If Mid(foundFile, dashIDX, 1) = "-" Then
                        Exit For
                    End If
                Next
                Try
                    debtor = Mid(foundFile, dashIDX + 1, foundFile.Length - dashIDX - 4)
                Catch ex As Exception
                    Continue For
                End Try
                If debtor < 0 Then
                    Continue For
                End If
                'instead of using return code - check last cancelled note type for nulla bona

                'get return code for this debtor
                'param2 = "select return_codeID, client_ref, status_open_closed from Debtor " & _
                '" where _rowid = " & debtor
                'Dim debtor_dataset As DataSet = get_dataset(param1, param2)
                'If no_of_rows = 0 Then
                '    MsgBox("Unable to find case number" & debtor)
                '    Exit Sub
                'End If
                'Dim status_open_closed As String = debtor_dataset.Tables(0).Rows(0).Item(2)
                'If status_open_closed = "O" Then
                '    Continue For
                'End If
                'Try
                '    retn_codeid = debtor_dataset.Tables(0).Rows(0).Item(0)
                'Catch ex As Exception
                '    retn_codeid = 0
                'End Try
                Dim idx As Integer
                For idx = foundFile.Length To 1 Step -1
                    If Mid(foundFile, idx, 1) = "\" Then
                        Exit For
                    End If
                Next
                Dim retn_group As String = "Returns - " & remit_no
                param2 = "select text from Note where debtorID = " & debtor & _
                    " and type = 'Cancelled' order by _rowid desc"
                Dim note_ds As DataSet = get_dataset("onestep", param2)
                Dim NullaBona As Boolean = False
                If InStr(LCase(note_ds.Tables(0).Rows(0).Item(0)), "nulla") > 0 Then
                    retn_group = "Nulla Bona - " & remit_no
                    NBfiles += 1
                    NullaBona = True
                Else
                    Retnfiles += 1
                End If
                'get retn_group for non-zero retn_codeid
                Dim pdf_name As String

                pdf_name = Microsoft.VisualBasic.Right(foundFile, foundFile.Length - idx)
                'If retn_codeid > 0 Then
                '    param2 = "select fee_category from CodeReturns" & _
                '    " where _rowid = " & retn_codeid
                '    Dim cr_dataset As DataSet = get_dataset(param1, param2)
                '    If no_of_rows = 1 Then
                '        If cr_dataset.Tables(0).Rows(0).Item(0) = 3 Then
                '            retn_group = "Nulla Bona - " & remit_no
                '        End If
                '    End If

                'End If
                'client_ref = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))

                'add batch number if required
                file_name = ""
                If nobatchrbtn.Checked Then
                    file_name = file_path & retn_group & "\" & pdf_name
                Else
                    If NullaBona Then
                        file_name = file_path & retn_group & "_" & NBBatchNo & "\" & pdf_name
                        If NBfiles = 50 Then
                            NBfiles = 0
                            NBBatchNo += 1
                        End If
                    Else
                        file_name = file_path & retn_group & "_" & RetnBatchNo & "\" & pdf_name
                        If Retnfiles = 50 Then
                            Retnfiles = 0
                            RetnBatchNo += 1
                        End If
                    End If

                End If
                My.Computer.FileSystem.CopyFile(foundFile, file_name)
            Next
        Catch ex As Exception

        End Try


    End Sub

    Private Sub clntbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clntbtn.Click
        clntfrm.ShowDialog()
        cl_lbl.Text = selected_cl_name
    End Sub
    Private Sub disable_buttons()
        exitbtn.Enabled = False
        clntbtn.Enabled = False
        retnbtn.Enabled = False
    End Sub
    Private Sub enable_buttons()
        exitbtn.Enabled = True
        clntbtn.Enabled = True
        retnbtn.Enabled = True
    End Sub
End Class

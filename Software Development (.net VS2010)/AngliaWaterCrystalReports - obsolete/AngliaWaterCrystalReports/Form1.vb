Imports System.IO
Public Class Form1
    Dim allReports As Boolean
    Dim filename As String = ""
    Dim filepath As String = ""
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub RD260btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RD260btn.Click
        disable_buttons()
        run_RD483()
    End Sub
    Private Sub run_RD483()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD483report = New RD483
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD483report, myArrayList1)
        'myArrayList1.Add(2560)  'vacated
        'SetCurrentValuesForParameterField2(RD260Preport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD483report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD483report, myConnectionInfo)
        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = "RD483 Anglian Water Combined Invoice.pdf"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                RD483report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
                RD483report.close()
                'now write out occupied scheme
                filepath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                filepath &= "\"
            Else
                MsgBox("Report not saved")
            End If
        End If
        'filename = filepath & "RD260 Occupied Anglia Water Invoice.pdf"
        'myArrayList1.Add(start_dtp.Value)
        'SetCurrentValuesForParameterField1(RD260Preport, myArrayList1)
        'myArrayList1.Add(2562)  'Occupied
        'SetCurrentValuesForParameterField2(RD260Preport, myArrayList1)
        'myArrayList1.Add(end_dtp.Value)
        'SetCurrentValuesForParameterField3(RD260Preport, myArrayList1)
        'myConnectionInfo.ServerName = "DebtRecovery"
        'myConnectionInfo.DatabaseName = "DebtRecovery"
        'myConnectionInfo.UserID = "vbnet"
        'myConnectionInfo.Password = "tenbv"
        'SetDBLogonForReport(myConnectionInfo, RD260Preport, myConnectionInfo)
        'RD260Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        'RD260Preport.close()

        'Dim RD260NPreport = New RD260NP
        'filename = filepath & "RD260NP Anglia Water Invoice.pdf"
        'myArrayList1.Add(start_dtp.Value)
        'SetCurrentValuesForParameterField1(RD260NPreport, myArrayList1)
        'myArrayList1.Add(end_dtp.Value)
        'SetCurrentValuesForParameterField3(RD260NPreport, myArrayList1)
        'myConnectionInfo.ServerName = "DebtRecovery"
        'myConnectionInfo.DatabaseName = "DebtRecovery"
        'myConnectionInfo.UserID = "vbnet"
        'myConnectionInfo.Password = "tenbv"
        'SetDBLogonForReport(myConnectionInfo, RD260NPreport, myConnectionInfo)
        'RD260NPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        'RD260NPreport.close()

        Dim RD260C3Preport = New RD260C3P
        filename = filepath & "RD260C3P Anglia Water Credit Note.pdf"
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD260C3Preport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD260C3Preport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD260C3Preport, myConnectionInfo)
        RD260C3Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD260C3Preport.close()


        'Dim RD260NDPreport = New RD260NDP
        'filename = filepath & "RD260NDP Anglia Water Direct Invoice.pdf"
        'myArrayList1.Add(start_dtp.Value)
        'SetCurrentValuesForParameterField1(RD260NDPreport, myArrayList1)
        'myArrayList1.Add(end_dtp.Value)
        'SetCurrentValuesForParameterField3(RD260NDPreport, myArrayList1)
        'myConnectionInfo.ServerName = "DebtRecovery"
        'myConnectionInfo.DatabaseName = "DebtRecovery"
        'myConnectionInfo.UserID = "vbnet"
        'myConnectionInfo.Password = "tenbv"
        'SetDBLogonForReport(myConnectionInfo, RD260NDPreport, myConnectionInfo)
        'RD260NDPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        'RD260NDPreport.close()
        If Not allReports Then
            MsgBox("Completed")
            Me.Close()
        End If
    End Sub
    Private Sub run_RD260C2P()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD260C2Preport = New RD260C2P
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD260C2Preport, myArrayList1)
        myArrayList1.Add(2560)  'vacated
        SetCurrentValuesForParameterField2(RD260C2Preport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD260C2Preport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD260C2Preport, myConnectionInfo)
        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = "RD260C2P Vacated Anglia Water Credit Note.pdf"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                filepath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                filepath &= "\"
            Else
                MsgBox("Report not saved")
            End If
        Else
            filename = filepath & "RD260C2P Vacated Anglia Water Credit Note.pdf"
        End If
        RD260C2Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD260C2Preport.close()
        'now write out occupied scheme
        filename = filepath & "RD260C2P Occupied Anglia Water Credit Note.pdf"
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD260C2Preport, myArrayList1)
        myArrayList1.Add(2562)  'Occupied
        SetCurrentValuesForParameterField2(RD260C2Preport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD260C2Preport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD260C2Preport, myConnectionInfo)
        RD260C2Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD260C2Preport.close()
        If Not allReports Then
            MsgBox("Completed")
            Me.Close()
        End If
    End Sub
    Private Sub disable_buttons()
        exitbtn.Enabled = False
        RD260btn.Enabled = False
        rd262btn.Enabled = False
        allbtn.Enabled = False
        rd260c2btn.Enabled = False
        rd244btn.Enabled = False
    End Sub

    Private Sub rd262btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd262btn.Click
        If MsgBox("Have Shelley or David Dicks run the process remitted payments since remit?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            MsgBox("Report not run")
            Exit Sub
        End If
        disable_buttons()
        run_RD262P()

    End Sub
    Private Sub run_RD262P()
        'report now all payments for remit date
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD262Preport = New RD262P
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD262Preport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD262Preport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        myConnectionInfo2.ServerName = "RossFeeTable"
        myConnectionInfo2.DatabaseName = "FeesSQL"
        myConnectionInfo2.UserID = "sa"
        myConnectionInfo2.Password = "sa"
        SetDBLogonForReport(myConnectionInfo, RD262Preport, myConnectionInfo2)
        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "XLS files |*.xls"
                .DefaultExt = ".xls"
                .OverwritePrompt = True
                .FileName = "RD262 Anglia Water Payment file.xls"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                filepath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                filepath &= "\"
            Else
                MsgBox("Report not saved")
            End If
        Else
            filename = filepath & "RD262 Anglia Water Payment file.xls"
        End If
        RD262Preport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, filename)
        RD262Preport.close()

        MsgBox("Completed")

        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        allReports = False
    End Sub

    Private Sub rd260c2btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd260c2btn.Click
        disable_buttons()
        run_RD260C2P()
    End Sub

    Private Sub allbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allbtn.Click
        If MsgBox("Have Shelley or David Dicks run the process remitted payments since remit?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
            MsgBox("Report not run")
            Exit Sub
        End If
        allReports = True
        disable_buttons()
        run_RD483()
        run_RD260C2P()
        run_RD262P()
        run_rd244()
    End Sub

    Private Sub rd244btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rd244btn.Click
        disable_buttons()
        run_RD244()
    End Sub
    Private Sub run_RD244()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD244P2report = New RD244P2
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD244P2report, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField3(RD244P2report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"

        SetDBLogonForReport(myConnectionInfo, RD244P2report, myConnectionInfo2)
        Dim crystalFilename As String = "H:\temp\RD244P2\" & "RD262 Vacated Anglia Water Payment file.xls"
        'create directories for returns report
        Dim dir_name As String = "H:\temp\RD244P2"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create folder")
            End
        End Try
        RD244P2report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, crystalFilename)
        RD244P2report.close()


        'now read in out write out as pipe separated
        Dim excel_file_contents(,) As String = InputFromExcel(crystalFilename)

        'first write out heading
        Dim returnFile As String
        returnFile = excel_file_contents(0, 0) & "|" & excel_file_contents(0, 1) & vbNewLine
        Dim maxRow As Integer = UBound(excel_file_contents)
        Dim rowIDx As Integer
        For rowIDx = 1 To maxRow
            Dim colIDX As Integer
            For colIDX = 0 To 27
                If colIDX = 3 Then
                    'date might be zero
                    Dim testDate As Date
                    Dim testDateStr As String = ""
                    Try
                        testDate = excel_file_contents(rowIDx, colIDX)
                        If Format(testDate, "yyyy") < 1900 Then
                            testDateStr = ""
                        Else
                            testDateStr = Format(testDate, "dd/MM/yyyy")
                        End If
                    Catch ex As Exception

                    End Try
                    returnFile &= testDateStr & "|"
                ElseIf colIDX = 26 Then
                    'set value to include decimals
                    Dim testAmt As Decimal = 0
                    Try
                        testAmt = excel_file_contents(rowIDx, colIDX)
                    Catch ex As Exception

                    End Try
                    returnFile &= Format(testAmt, "f") & "|"
                Else
                    returnFile &= excel_file_contents(rowIDx, colIDX) & "|"
                End If

            Next
            returnFile &= excel_file_contents(rowIDx, colIDX) & vbNewLine
        Next



        If filepath = "" Then
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "CSV files |*.csv"
                .DefaultExt = ".csv"
                .OverwritePrompt = True
                .FileName = "RD244 Anglia Water Returns.csv"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                filename = SaveFileDialog1.FileName
                filepath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                filepath &= "\"
            Else
                MsgBox("Report not saved")
            End If
        Else
            filename = filepath & "RD244 Anglia Water Returns.csv"
        End If
        My.Computer.FileSystem.WriteAllText(filename, returnFile, False, System.Text.Encoding.ASCII)
        MsgBox("Completed")

        Me.Close()
    End Sub
End Class

Public Class mainfrm
    Dim first_letter As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim days As Integer = Weekday(Now)
        date_picker.Value = DateAdd(DateInterval.Day, -days, Now)
    End Sub


    Private Sub retnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles retnbtn.Click
        retn_month = Format(date_picker.Value, "MM_MMM")
        retn_day = Format(date_picker.Value, "dd_ddd")
        retnbtn.Enabled = False
        selected_cl_no = 590 'LBH
        Dim files_found As Boolean = False
        'get all client schemes for client
        param2 = "select _rowid from clientScheme where clientID = " & selected_cl_no &
            " and branchID = 1"
        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        Dim cs_rows As Integer = no_of_rows - 1
        Dim cs_idx As Integer
        For cs_idx = 0 To cs_rows
            Try
                ProgressBar1.Value = (cs_idx / cs_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            'get remittance number for client schemes
            Dim csid As Integer = cs_ds.Tables(0).Rows(cs_idx).Item(0)
            param2 = "select _rowid from Remit where clientschemeID = " & csid & _
                 " and date = '" & Format(date_picker.Value, "yyyy-MM-dd") & "'"
            Dim remit_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim remit_no As Integer = remit_dataset.Tables(0).Rows(0).Item(0)
            file_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                                   retn_day & "\Remittances\" & Format(remit_no, "#") & "\Returns\"

            Dim file_name, client_ref As String
            'create directories for returns
            Dim dir_name As String = file_path & "Fees part paid"
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
            dir_name = file_path & "No fees paid"
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
            dir_name = file_path & "All fees paid"
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
            Dim debtor As Integer

            For Each foundFile As String In My.Computer.FileSystem.GetFiles _
                (file_path, FileIO.SearchOption.SearchAllSubDirectories, "*.pdf")
                'get debtor number
                files_found = True
                Dim txt As String = Microsoft.VisualBasic.Right(foundFile, 11)
                txt = Microsoft.VisualBasic.Left(txt, txt.Length - 4)
                Try
                    debtor = txt
                Catch ex As Exception
                    MsgBox("unable to read debtor number in file " & foundFile)
                End Try
                'get return code for this debtor
                param2 = "select client_ref, status_open_closed from Debtor " & _
                " where _rowid = " & debtor
                Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
                If no_of_rows = 0 Then
                    MsgBox("Unable to find case number" & debtor)
                    Exit Sub
                End If
                Dim status_open_closed As String = debtor_dataset.Tables(0).Rows(0).Item(1)
                If status_open_closed = "O" Then
                    Continue For
                End If

                'any fees oustanding?
                param2 = "select sum(fee_amount),sum(remited_fee) from Fee where debtorID = " & debtor &
                    " and fee_remit_col > 2"
                Dim fee_ds As DataSet = get_dataset("onestep", param2)
                Dim fee_amount As Decimal
                Try
                    fee_amount = fee_ds.Tables(0).Rows(0).Item(0)
                Catch ex As Exception
                    fee_amount = 0
                End Try
                Dim fee_remitted As Decimal
                Try
                    fee_remitted = fee_ds.Tables(0).Rows(0).Item(1)
                Catch ex As Exception
                    fee_remitted = 0
                End Try

                'get retn_group 
                client_ref = Trim(debtor_dataset.Tables(0).Rows(0).Item(0))
                Dim retn_group As String = ""
                If fee_remitted = 0 Then
                    retn_group = "No fees paid"
                ElseIf fee_amount > fee_remitted Then
                    retn_group = "Fees part paid"
                Else
                    retn_group = "All fees paid"
                End If

                file_name = file_path & retn_group & "\" & client_ref & "-" & debtor & ".pdf"
                My.Computer.FileSystem.CopyFile(foundFile, file_name, True)
            Next
        Next
        If files_found Then
            MsgBox("All reports moved to directories")
        Else
            MsgBox("No reports were found")
        End If
        Me.Close()
    End Sub

End Class

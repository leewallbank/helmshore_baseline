﻿Public Class Form1
    Dim outfile As String = "Batch,Supplier,Trace date,Successful cases,Links,Cases with a visit," &
        "Gone Away,Returned Other reason,Paid to client,fees paid,Waiting paid" & vbNewLine
    Dim batch_number As Integer
    Dim suppl_no As Integer
    Dim succ_cases As Integer = 0
    Dim link_cases As Integer = 0
    Dim visit_cases As Integer = 0
    Dim gone_aways As Integer = 0
    Dim waiting As Decimal = 0
    Dim paid_to_client As Decimal = 0
    Dim fees_paid As Decimal = 0
    Dim other As Integer = 0
    Dim last_batch_no As Integer = 0
    Dim last_suppl_no As Integer = 0
    Dim early_date As Date = CDate("jan 1, 1900")
    Dim last_trace_date As Date = early_date
    Dim trace_date As Date
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        Me.TriBureauxTraceResultsTableAdapter.Fill(Me.FeesSQLDataSet.TriBureauxTraceResults)
        Dim case_rows As Integer = Me.FeesSQLDataSet.TriBureauxTraceResults.Rows.Count - 1
        Dim case_idx As Integer
        runbtn.Enabled = False
        exitbtn.Enabled = False
        For case_idx = 0 To case_rows
            Try
                ProgressBar1.Value = (case_idx / case_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            Dim debtorID As Integer = Me.FeesSQLDataSet.TriBureauxTraceResults.Rows(case_idx).Item(1)
            batch_number = Me.FeesSQLDataSet.TriBureauxTraceResults.Rows(case_idx).Item(2)
            suppl_no = Me.FeesSQLDataSet.TriBureauxTraceResults.Rows(case_idx).Item(3)
            trace_date = Me.FeesSQLDataSet.TriBureauxTraceResults.Rows(case_idx).Item(7)
            If (suppl_no <> last_suppl_no And last_suppl_no > 0) Or
                (Format(trace_date, "yyyy-MM-dd") <> Format(last_trace_date, "yyyy-MM-dd") And
                Format(last_trace_date, "yyyy-MM-dd") <> Format(early_date, "yyyy-MM-dd")) Or
                (batch_number <> last_batch_no And last_batch_no > 0) Then
                end_of_batch()
            End If

            last_batch_no = batch_number
            last_suppl_no = suppl_no
            last_trace_date = trace_date
            succ_cases += 1
            'see if debtorID has had visit since trace
            Dim visit_found As Boolean = False
            param2 = "select date_visited from Visit where debtorID = " & debtorID &
                         " and date_visited is not null" &
                         " order by date_visited desc"
            Dim visit_ds As DataSet = get_dataset("onestep", param2)
            If no_of_rows > 0 Then
                If Format(visit_ds.Tables(0).Rows(0).Item(0), "yyyy-MM-dd") > Format(trace_date, "yyyy-MM-dd") Then
                    visit_cases += 1
                    visit_found = True
                End If
            End If

            'get payments
            param2 = "select status, amount, split_debt, split_costs, split_fees, split_van, split_other, date from payment where debtorID = " & debtorID &
                " and (status = 'W' or status = 'R') and date > '" & Format(trace_date, "yyy-MM-dd") & "'"
            Dim pay_ds As DataSet = get_dataset("onestep", param2)
            Dim pay_rows As Integer = no_of_rows - 1
            Dim pay_idx As Integer
            For pay_idx = 0 To pay_rows
                Dim pay_status As String = pay_ds.Tables(0).Rows(pay_idx).Item(0)
                If pay_status = "W" Then
                    waiting += pay_ds.Tables(0).Rows(pay_idx).Item(1)
                Else
                    paid_to_client = paid_to_client + pay_ds.Tables(0).Rows(pay_idx).Item(2) _
                        + pay_ds.Tables(0).Rows(pay_idx).Item(3)
                    fees_paid = fees_paid + pay_ds.Tables(0).Rows(pay_idx).Item(4) + _
                        pay_ds.Tables(0).Rows(pay_idx).Item(5) _
                        + pay_ds.Tables(0).Rows(pay_idx).Item(6)
                End If
            Next
            param2 = "select linkID, return_date, status, return_codeID from Debtor where _rowid = " & debtorID

            Dim debt_ds As DataSet = get_dataset("onestep", param2)
           
            Dim status As String = debt_ds.Tables(0).Rows(0).Item(2)
            Dim ga_found As Boolean = False
            Dim other_found As Boolean = False
            If status = "C" Then
                Dim return_date As Date
                Try
                    return_date = debt_ds.Tables(0).Rows(0).Item(1)
                Catch ex As Exception
                    return_date = Nothing
                End Try
                If return_date <> Nothing Then
                    If Format(return_date, "yyyy-MM-dd") > Format(trace_date, "yyyy-MM-dd") Then
                        Dim retn_code As Integer = debt_ds.Tables(0).Rows(0).Item(3)
                        If retn_code = 33 Then
                            gone_aways += 1
                            ga_found = True
                        Else
                            param2 = "select fee_category from CodeReturns where _rowid = " & retn_code
                            Dim retn_ds As DataSet = get_dataset("onestep", param2)
                            If retn_ds.Tables(0).Rows(0).Item(0) = 1 Then
                                gone_aways += 1
                                ga_found = True
                            Else
                                other += 1
                                other_found = True
                            End If
                        End If
                    End If
                End If
            End If
            Dim linkID As Integer
            Try
                linkID = debt_ds.Tables(0).Rows(0).Item(0)
            Catch ex As Exception
                linkID = 0
            End Try
            If linkID > 0 Then
                param2 = "select _rowid, return_date, status, return_codeID from Debtor where linkID = " & linkID &
                    " and _rowid<> " & debtorID
                Dim link_ds As DataSet = get_dataset("onestep", param2)
                link_cases += no_of_rows
                Dim link_rows As Integer = no_of_rows - 1
                Dim link_idx As Integer
                Dim link_oth_found As Boolean = False
                Dim link_ga_found As Boolean = False
                For link_idx = 0 To link_rows
                    Dim link_debtorID As Integer = link_ds.Tables(0).Rows(link_idx).Item(0)
                    param2 = "select status, amount, split_debt, split_costs, split_fees, split_van, split_other from payment where debtorID = " & link_debtorID &
               " and (status = 'W' or status = 'R') and date > '" & Format(trace_date, "yyy-MM-dd") & "'"
                    Dim pay2_ds As DataSet = get_dataset("onestep", param2)
                    Dim pay2_rows As Integer = no_of_rows - 1
                    Dim pay2_idx As Integer
                    For pay2_idx = 0 To pay2_rows
                        Dim pay2_status As String = pay2_ds.Tables(0).Rows(pay2_idx).Item(0)
                        If pay2_status = "W" Then
                            waiting += pay2_ds.Tables(0).Rows(pay2_idx).Item(1)
                        Else
                            paid_to_client = paid_to_client + pay2_ds.Tables(0).Rows(pay2_idx).Item(2) _
                                + pay2_ds.Tables(0).Rows(pay2_idx).Item(3)
                            fees_paid = fees_paid + pay2_ds.Tables(0).Rows(pay2_idx).Item(4) _
                                + pay2_ds.Tables(0).Rows(pay2_idx).Item(5) _
                                + pay2_ds.Tables(0).Rows(pay2_idx).Item(6)
                        End If
                    Next
                    If Not visit_found Then
                        param2 = "select date_visited from Visit where debtorID = " & link_debtorID &
                     " and date_visited is not null" &
                     " order by date_visited desc"
                        Dim visit2_ds As DataSet = get_dataset("onestep", param2)
                        If no_of_rows > 0 Then
                            If Format(visit2_ds.Tables(0).Rows(0).Item(0), "yyyy-MM-dd") > Format(trace_date, "yyyy-MM-dd") Then
                                visit_cases += 1
                                visit_found = True
                            End If
                        End If
                    End If
                    status = link_ds.Tables(0).Rows(link_idx).Item(2)
                    If status = "C" And (Not ga_found Or Not other_found) Then
                        Dim return_date As Date
                        Try
                            return_date = link_ds.Tables(0).Rows(link_idx).Item(1)
                        Catch ex As Exception
                            return_date = Nothing
                        End Try
                        If return_date <> Nothing Then
                            If Format(return_date, "yyyy-MM-dd") > Format(trace_date, "yyyy-MM-dd") Then
                                Dim retn_code As Integer = link_ds.Tables(0).Rows(link_idx).Item(3)
                                If retn_code = 33 Then
                                    link_ga_found = True
                                Else
                                    param2 = "select fee_category from CodeReturns where _rowid = " & retn_code
                                    Dim retn_ds As DataSet = get_dataset("onestep", param2)
                                    If retn_ds.Tables(0).Rows(0).Item(0) = 1 Then
                                        link_ga_found = True
                                    Else
                                        link_oth_found = True
                                    End If
                                End If
                            End If
                        End If
                    End If
                Next
                If Not ga_found And link_ga_found Then
                    gone_aways += 1
                End If
                If Not other_found And link_oth_found Then
                    other += 1
                End If
            End If
        Next
        'do last batch
        end_of_batch()

        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "CSV files |*.csv"
            .DefaultExt = ".csv"
            .OverwritePrompt = True
            .FileName = "trace_results.csv"
        End With

        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
        End If
        MsgBox("File saved")
        Me.Close()
    End Sub

    Private Sub end_of_batch()
        Dim suppl_name As String = "???"
        Select Case last_suppl_no
            Case 1
                suppl_name = "Call credit"
            Case 2
                suppl_name = "Experian"
            Case 3
                suppl_name = "Equifax"

        End Select
        outfile = outfile & last_batch_no & "," & suppl_name & "," & Format(last_trace_date, "dd/MM/yyyy") &
            "," & succ_cases & "," & link_cases & "," & visit_cases & "," & gone_aways &
            "," & other & "," & paid_to_client & "," & fees_paid & "," & waiting & vbNewLine
        succ_cases = 0
        link_cases = 0
        visit_cases = 0
        gone_aways = 0
        other = 0
        paid_to_client = 0
        fees_paid = 0
        waiting = 0
    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load



    End Sub
End Class

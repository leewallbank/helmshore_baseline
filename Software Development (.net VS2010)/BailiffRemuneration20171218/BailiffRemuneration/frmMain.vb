﻿Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports System.IO

Imports CommonLibrary

Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class frmMain
    Private Remuneration As New clsRemunerationData

    Private LargeBalanceThreshold As Decimal = 1500
    Private LargeBalanceMaximum As Decimal = 26500
    Private LargeBalanceIncrement As Decimal = 100

    Private RecoveredComplianceFeeFirstEnforcementThreshold As Integer = 10

    'Private FirstEnforcementAmount1Cutoff As Integer = 14
    Private FirstEnforcementAmount1Cutoff As Integer = 31 ' TS 10/Dec/2014. Request ref 36738
    Private FirstEnforcementAmount2Cutoff As Integer = 180
    Private SecondEnforcementAmount1Cutoff As Integer = 70 ' TS 30/Oct/2014 was 60 request ref 34265
    Private SecondEnforcementAmount1LinkedCutoff As Integer = 130 ' added request ref 34265
    Private SecondEnforcementAmount2Cutoff As Integer = 180

    Private Vat As Decimal = 0, VatRate As Decimal = 0, Payment As Decimal = 0

    Private AuditFile As String

    Private InvoiceDate As Date = Today

    Private Sub frmMain_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        dtpCalcMonth.Value = dtpCalcMonth.Value.AddDays(-dtpCalcMonth.Value.Day + 1).AddMonths(-1)
    End Sub

    Private Sub btnCalculate_Click(sender As Object, e As System.EventArgs) Handles btnCalculate.Click
        Try

            Dim FolderDialog As New FolderBrowserDialog

            If Not FolderDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            VatRate = Remuneration.VatRate(Today)

            AuditFile = FolderDialog.SelectedPath & "\Process_"

            rtxtAudit.Clear()

            'Remuneration.ImportBailiff()

            CalculateFirstVisit()
            'CalculateRecoveredComplianceFee()
            'CalculatePIFPayments()
            CalculateCGA()

            CalculateMHRVisit()
            CalculateCollectPayment()

            'CalculateCGAPIF() ' added TS 14/Jul/2017. Request ref 112264

            'CalculateSaleFee() ' added TS 10/Aug/2017. Request ref 114817

            Remuneration.UpdateInvoiceTotals(dtpCalcMonth.Value, dtpCalcMonth.Value.AddMonths(1).AddDays(-1))

            prgMain.Value = 0

            MsgBox("Calculation complete", MsgBoxStyle.OkOnly, Me.Text)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub CalculateFirstVisit()
        ' WorkingAwayUplift added TS 19/Mar/2015 request ref 22167
        Try

            Dim CaseDetail As String = "", PayingCase As String

            lblProgress.Text = "Importing visits"
            lblProgress.Visible = True
            Application.DoEvents()
            Remuneration.ImportVisit(dtpCalcMonth.Value, dtpCalcMonth.Value.AddMonths(1).AddDays(-1))
            lblProgress.Visible = False

            Dim pdfDoc As New Document()
            pdfDoc.Open()
            Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(AuditFile & "FirstVisit_audit_" & InvoiceDate.ToString("yyyyMMdd") & ".pdf", FileMode.Append))
            Dim ev As New itsEvents
            pdfWrite.PageEvent = ev
            pdfDoc.Open()

            prgMain.Value = 1
            prgMain.Maximum = Remuneration.VisitCases.Rows.Count + 1

            For Each VisitCase As DataRow In Remuneration.VisitCases.Rows

                Payment = 0
                Vat = 0
                PayingCase = ""

                Application.DoEvents()

                CaseDetail = "CASE - START PROCESSING CaseId " & VisitCase("CaseID") & vbCrLf
                CaseDetail &= "Bailiff: " & VisitCase("Surname") & ", " & VisitCase("Forename") & "[" & VisitCase("BailiffID") & "]" & " visited: " & VisitCase("VisitDate") & vbCrLf

                'If VisitCase("VisitID") = VisitCase("FirstVisitID") Then commented out TS 10/Dec/2014. Request ref 36738
                If CDate(VisitCase("VisitDate")).ToString("d") = CDate(VisitCase("FirstVisitDate")).ToString("d") Then ' added TS 10/Dec/2014. Request ref 36738

                    'If Not IsDBNull(VisitCase("LinkID")) Then PayingCase = Remuneration.GetPayingCase(VisitCase("CaseID"), "FV") ' commented out TS 02/Sep/2014. Request ref 29547
                    'PayingCase = Remuneration.GetPayingCase(VisitCase("CaseID"), "FV") ' added TS 02/Sep/2014. Request ref 29547 Comment out 30/Oct/2014
                    'PayingCase = Remuneration.GetPayingCaseFV(VisitCase("CaseID"), VisitCase("AllocationDate")) ' added TS 30/Oct/2014. Commented out TS 04/Nov/2014. Request ref 34525
                    PayingCase = Remuneration.GetPayingCaseFV(VisitCase("CaseID"), dtpCalcMonth.Value, VisitCase("BailiffID")) ' added TS 30/Oct/2014. Request ref 34525. BailiffID added TS 26/Sep/2016. Request ref 91792

                    If PayingCase = "" Then ' this check added TS 10/Jun/2014 23531

                        If IsDBNull(VisitCase("LinkID")) Then
                            PayingCase = "" ' not strictly required
                        Else
                            PayingCase = Remuneration.GetAllocationBatchPayingCaseFV(VisitCase("linkID"), VisitCase("VisitDate"), VisitCase("BailiffID")) ' added TS 10/Dec/2014 request ref 36738
                        End If

                        If PayingCase = "" Then ' this section added TS 10/Dec/2014 request ref 36738
                            CaseDetail &= "First visit adding enforcement fee. Payment = " & VisitCase("FirstVisitPayment") & vbCrLf

                            Payment = VisitCase("FirstVisitPayment")
                        Else
                            CaseDetail &= "First visit adding enforcement fee. Case " & PayingCase & " already paid full amount. Payment = " & VisitCase("FirstVisitSubsequentPayment") & vbCrLf

                            Payment = VisitCase("FirstVisitSubsequentPayment")
                        End If

                        If Not IsDBNull(VisitCase("VatReg")) AndAlso VisitCase("VatReg").ToString.Length > 0 Then Vat = Payment * VatRate

                        Remuneration.AddBailiffPayment(VisitCase("CaseId"), _
                                                               If(IsDBNull(VisitCase("LinkID")), "", VisitCase("LinkID")), _
                                                                VisitCase("BailiffId"), _
                                                                VisitCase("ClientId"), _
                                                                VisitCase("WorkTypeId"), _
                                                                VisitCase("VisitID"), _
                                                                VisitCase("VisitDate"), _
                                                                If(IsDBNull(VisitCase("AllocationDate")), CDate("01-01-1900"), VisitCase("AllocationDate")), _
                                                                Nothing, _
                                                                InvoiceDate, _
                                                                "FV" & InvoiceDate.ToString("yyyyMMdd") & VisitCase("BailiffId").ToString, _
                                                                InvoiceDate, _
                                                                Payment, _
                                                                0, _
                                                                0, _
                                                                0, _
                                                                0, _
                                                                Vat, _
                                                                dtpCalcMonth.Value, _
                                                                dtpCalcMonth.Value.AddMonths(1).AddDays(-1), _
                                                                If(IsDBNull(VisitCase("LoginName")), "", VisitCase("LoginName")), _
                                                                "FV")
                    Else
                        CaseDetail &= "Case already paid on" & vbCrLf
                    End If
                Else
                    'If VisitCase("CaseID") <> VisitCase("FirstCaseID") Then commented out TS 10/Dec/2014. Request ref 36738
                    'CaseDetail &= "Linked case " & VisitCase("FirstCaseID") & " visited by bailiff: " & VisitCase("FirstSurname") & ", " & VisitCase("FirstForename") & "[" & VisitCase("FirstBailiffID") & "]" & " visited: " & VisitCase("FirstVisitDate") & vbCrLf commented out TS 10/Dec/2014. Request ref 36738
                    'Else commented out TS 10/Dec/2014. Request ref 36738
                    CaseDetail &= "Case visited previously by bailiff: " & VisitCase("FirstSurname") & ", " & VisitCase("FirstForename") & "[" & VisitCase("FirstBailiffID") & "]" & " visited: " & VisitCase("FirstVisitDate") & vbCrLf
                    'End If commented out TS 10/Dec/2014. Request ref 36738

                End If

                CaseDetail &= "CASE - END PROCESSING " & VisitCase("CaseID") & vbCrLf & vbCrLf
                rtxtAudit.AppendText(CaseDetail)

                Dim Para As New Paragraph(CaseDetail)
                Para.Font.Size = 6
                pdfDoc.Add(Para)

                prgMain.Value += 1
            Next VisitCase

            If Remuneration.VisitCases.Rows.Count > 0 Then pdfDoc.Close()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub CalculateRecoveredComplianceFee()
        ' WorkingAwayUplift added TS 19/Mar/2015 request ref 22167
        Try

            Dim CaseDetail As String = "", PayingCases As String = "", LinkID As Integer = -1, AllocationDate As String = "", PayingCase As String
            Dim PaymentTotal As Decimal = 0
            Dim OpenBatchCases As Integer

            lblProgress.Text = "Importing recovered compliance fee"
            lblProgress.Visible = True
            Application.DoEvents()
            Remuneration.ImportRecoveredComplianceFee(dtpCalcMonth.Value, dtpCalcMonth.Value.AddMonths(1).AddDays(-1))
            lblProgress.Visible = False

            Dim pdfDoc As New Document()
            pdfDoc.Open()
            Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(AuditFile & "RecoveredComplianceFee_audit_" & InvoiceDate.ToString("yyyyMMdd") & ".pdf", FileMode.Append))
            Dim ev As New itsEvents
            pdfWrite.PageEvent = ev
            pdfDoc.Open()

            prgMain.Value = 1
            prgMain.Maximum = Remuneration.RecoveredComplianceFeeCases.Rows.Count + 1

            For Each RecoveredComplianceFeeCase As DataRow In Remuneration.RecoveredComplianceFeeCases.Rows

                Payment = 0
                Vat = 0
                OpenBatchCases = 0

                Application.DoEvents()

                CaseDetail = "CASE - START PROCESSING CaseId " & RecoveredComplianceFeeCase("CaseID") & vbCrLf
                CaseDetail &= "Bailiff: " & RecoveredComplianceFeeCase("Surname") & ", " & RecoveredComplianceFeeCase("Forename") & "[" & RecoveredComplianceFeeCase("BailiffID") & "]" & " visited: " & RecoveredComplianceFeeCase("VisitDate") & vbCrLf
                'CaseDetail &= "Compliance fee balance outstanding at visit: " & RecoveredComplianceFeeCase("BalanceOutstandingAtVisit") & vbCrLf' commented TS 03/Dec/2014. Request ref 35859

                PayingCase = Remuneration.GetPayingCaseCOM(RecoveredComplianceFeeCase("CaseID"), RecoveredComplianceFeeCase("InternalExternal")) ' added TS 29/Sep/2014. Request ref 31146. InternalExternal added TS 11/Dec/2014. Request ref 37573

                If PayingCase = "" Then ' this check added TS 29/Sep/2014. Request ref 31146

                    If RecoveredComplianceFeeCase("FeeDate") < RecoveredComplianceFeeCase("VisitDate") Then ' added TS 12/Jun/2014 23530

                        ' Reset running totals here. Do so if LinkID is NULL, LinkID changes or LinkID stays the same but allocation date changes
                        If IsDBNull(RecoveredComplianceFeeCase("LinkID")) OrElse LinkID <> RecoveredComplianceFeeCase("LinkID") OrElse (LinkID = RecoveredComplianceFeeCase("LinkID") And AllocationDate <> RecoveredComplianceFeeCase("AllocationDate")) Then
                            PaymentTotal = 0
                            PayingCases = ""
                            LinkID = -1
                            AllocationDate = ""
                        End If

                        Select Case RecoveredComplianceFeeCase("InternalExternal")

                            Case "I"

                                'If RecoveredComplianceFeeCase("BailiffID") = RecoveredComplianceFeeCase("AllocatedBailiffID") Then

                                CaseDetail &= "Compliance fee balance outstanding at allocation: " & RecoveredComplianceFeeCase("BalanceOutstandingAtAllocation") & vbCrLf ' added TS 03/Dec/2014. Request ref 35859

                                ' The following code commented out TS 10/Dec/2014. Request ref 36738

                                'If IsDBNull(RecoveredComplianceFeeCase("LinkID")) Then
                                '    Payment = RecoveredComplianceFeeCase("BalanceOutstandingAtVisit") * 0.1
                                '    CaseDetail &= "Payment amount = " & Payment.ToString("0.00") & vbCrLf
                                'Else

                                '    PaymentTotal = Remuneration.GetAllocationBatchTotalPaymentCOM(RecoveredComplianceFeeCase("CaseID"), RecoveredComplianceFeeCase("linkID"), RecoveredComplianceFeeCase("AllocationDate"), RecoveredComplianceFeeCase("BailiffID")) ' added TS 27/Oct/2014 request ref 33864

                                '    Payment = RecoveredComplianceFeeCase("BalanceOutstandingAtVisit") * 0.1

                                '    If Payment + PaymentTotal > RecoveredComplianceFeeFirstEnforcementThreshold Then
                                '        Payment = RecoveredComplianceFeeFirstEnforcementThreshold - PaymentTotal
                                '        CaseDetail &= "Payment amount = " & Payment.ToString("0.00") & " - capped as already paid on " & PayingCases & vbCrLf
                                '    Else
                                '        CaseDetail &= "Payment amount = " & Payment.ToString("0.00") & vbCrLf
                                '    End If

                                '    If PayingCases <> "" Then PayingCases &= ", "
                                '    PayingCases &= RecoveredComplianceFeeCase("CaseID")

                                '    'PaymentTotal += Payment commented out TS 27/Oct/2014 33864
                                'End If
                                ''Else
                                ''CaseDetail &= "Case is no longer allocated to bailiff" & vbCrLf
                                ''End If

                                'End of code commented out TS 10/Dec/2014 and the following added

                                'Payment = 4 Commented out TS 20/Jan/2015. Request ref 37806
                                Payment = RecoveredComplianceFeeCase("CompliancePayment") ' added TS 20/Jan/2015. Request ref 37806
                                CaseDetail &= "Payment amount = " & Payment.ToString("0.00") & vbCrLf

                            Case "E"

                                If RecoveredComplianceFeeCase("BailiffID") = RecoveredComplianceFeeCase("AllocatedBailiffID") Then

                                    CaseDetail &= "Compliance fee balance outstanding at allocation: " & RecoveredComplianceFeeCase("BalanceOutstandingAtAllocation") & vbCrLf ' added TS 03/Dec/2014. Request ref 35859

                                    If RecoveredComplianceFeeCase("BalanceOutstandingAtAllocation") = RecoveredComplianceFeeCase("ComplianceFeeAmount") Then

                                        If Not IsDBNull(RecoveredComplianceFeeCase("LinkID")) Then ' this section added TS 25/Nov/2014. Request ref 35859
                                            Remuneration.GetAllocationBatchDetailsCOM(RecoveredComplianceFeeCase("CaseID"), RecoveredComplianceFeeCase("LinkID"), RecoveredComplianceFeeCase("AllocationDate"), RecoveredComplianceFeeCase("BailiffID"), dtpCalcMonth.Value.AddMonths(1).AddDays(-1))
                                            OpenBatchCases = Remuneration.AllocationBatch.Rows(0)("OpenCases")
                                        Else
                                            OpenBatchCases = 0
                                        End If

                                        If OpenBatchCases = 0 Then

                                            If IsDBNull(RecoveredComplianceFeeCase("LinkID")) Then
                                                CaseDetail &= "No payment for first fully outstanding compliance fee recovered" & vbCrLf
                                            Else
                                                If PaymentTotal = 0 Then
                                                    CaseDetail &= "No payment for first fully outstanding compliance fee recovered" & vbCrLf
                                                    PaymentTotal += 15 ' 15 is irrelevant really, just so that next time the Else is invoked
                                                Else

                                                    'Payment = 15 commented out TS 28/Nov/2014. Request ref 32642
                                                    'Payment = RecoveredComplianceFeeCase("BalanceOutstandingAtAllocation") * 0.25 'added out TS 28/Nov/2014. Request ref 32642. Commented out TS 20/Jan/2015. Request ref 37806
                                                    Payment = RecoveredComplianceFeeCase("BalanceOutstandingAtAllocation") * RecoveredComplianceFeeCase("CompliancePayment") 'added out TS 20/Jan/2015. Request ref 37806
                                                    CaseDetail &= "Payment amount = " & Payment.ToString("0.00") & vbCrLf

                                                End If
                                            End If

                                        Else
                                            CaseDetail &= "Allocation batch has open cases" & vbCrLf
                                        End If
                                    Else
                                        CaseDetail &= "No payment as fee balance not equal to fee amount of £" & RecoveredComplianceFeeCase("ComplianceFeeAmount") & " at visit" & vbCrLf
                                    End If
                                Else
                                    CaseDetail &= "Case is no longer allocated to bailiff" & vbCrLf
                                End If

                        End Select

                        If IsDBNull(RecoveredComplianceFeeCase("LinkID")) Then
                            LinkID = -1
                        Else
                            LinkID = RecoveredComplianceFeeCase("LinkID")
                        End If
                        AllocationDate = RecoveredComplianceFeeCase("AllocationDate")

                        If Payment > 0 Then

                            If Not IsDBNull(RecoveredComplianceFeeCase("VatReg")) AndAlso RecoveredComplianceFeeCase("VatReg").ToString.Length > 0 Then Vat = Payment * VatRate

                            Remuneration.AddBailiffPayment(RecoveredComplianceFeeCase("CaseId"), _
                                                           If(IsDBNull(RecoveredComplianceFeeCase("LinkID")), "", RecoveredComplianceFeeCase("LinkID")), _
                                                            RecoveredComplianceFeeCase("BailiffId"), _
                                                            RecoveredComplianceFeeCase("ClientId"), _
                                                            RecoveredComplianceFeeCase("WorkTypeId"), _
                                                            RecoveredComplianceFeeCase("VisitID"), _
                                                            RecoveredComplianceFeeCase("VisitDate"), _
                                                            If(IsDBNull(RecoveredComplianceFeeCase("AllocationDate")), CDate("01-01-1900"), RecoveredComplianceFeeCase("AllocationDate")), _
                                                            Nothing, _
                                                            InvoiceDate, _
                                                            "COM" & InvoiceDate.ToString("yyyyMMdd") & RecoveredComplianceFeeCase("BailiffId").ToString, _
                                                            InvoiceDate, _
                                                            Payment, _
                                                            0, _
                                                            0, _
                                                            0, _
                                                            0, _
                                                            Vat, _
                                                            dtpCalcMonth.Value, _
                                                            dtpCalcMonth.Value.AddMonths(1).AddDays(-1), _
                                                            If(IsDBNull(RecoveredComplianceFeeCase("LoginName")), "", RecoveredComplianceFeeCase("LoginName")), _
                                                            "COM")

                        End If
                    Else
                        CaseDetail &= "Fee added after visit on " & RecoveredComplianceFeeCase("FeeDate") & vbCrLf
                    End If
                Else
                    CaseDetail &= "Case already paid on" & vbCrLf
                End If

                CaseDetail &= "CASE - END PROCESSING " & RecoveredComplianceFeeCase("CaseID") & vbCrLf & vbCrLf
                rtxtAudit.AppendText(CaseDetail)

                Dim Para As New Paragraph(CaseDetail)
                Para.Font.Size = 6
                pdfDoc.Add(Para)

                prgMain.Value += 1
            Next RecoveredComplianceFeeCase

            If Remuneration.RecoveredComplianceFeeCases.Rows.Count > 0 Then pdfDoc.Close()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub CalculatePIFPayments()
        ' WorkingAwayUplift added TS 19/Mar/2015 request ref 22167
        Try
            Dim LastRemitID As Integer, DaysBetweenAllocationAndPayment As Integer, OpenBatchCases As Integer, DaysBetweenVisitAndPayment As Integer
            Dim LargeBalancePayment As Decimal = 0, Quality As Decimal = 0, Productivity As Decimal = 0, TotalClientDebtBalance As Decimal = 0, WorkingAwayUplift As Decimal ' WorkingAwayUplift added TS 23/Mar/2015. Request ref 22167
            Dim LastBatchPaymentDate As Date
            Dim CaseDetail As String = "", PayingCase As String

            ' get the PIF cases for the selected month
            lblProgress.Text = "Importing PIF cases"
            lblProgress.Visible = True
            Application.DoEvents()
            Remuneration.ImportPIF(dtpCalcMonth.Value, dtpCalcMonth.Value.AddMonths(1).AddDays(-1))
            Remuneration.GetPIFRemitCount()
            'Remuneration.GetPIFBailiffCount()
            lblProgress.Visible = False

            Dim pdfDoc As New Document()
            pdfDoc.Open()
            Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(AuditFile & "PIF_audit_" & InvoiceDate.ToString("yyyyMMdd") & ".pdf", FileMode.Append))
            Dim ev As New itsEvents
            pdfWrite.PageEvent = ev
            pdfDoc.Open()

            prgMain.Value = 1
            prgMain.Maximum = Remuneration.PIFCases.Rows.Count + 1

            For Each PIFCase As DataRow In Remuneration.PIFCases.Rows

                CaseDetail = ""
                Payment = 0
                LargeBalancePayment = 0
                Quality = 0
                Productivity = 0
                Vat = 0
                PayingCase = ""
                OpenBatchCases = 0
                TotalClientDebtBalance = 0
                WorkingAwayUplift = 0 ' added TS 23/Mar/2015. Request ref 22167
                LastBatchPaymentDate = Nothing

                Application.DoEvents()

                ' add the remit header if the remit id changes
                If PIFCase("RemitID") <> LastRemitID Then
                    Remuneration.PIFRemitCount.RowFilter = "RemitID = " & PIFCase("RemitID")
                    CaseDetail = "Remit " & PIFCase("RemitID") & " - " & Remuneration.PIFRemitCount.ToTable.Rows(0).Item(1) & " case" & IIf(Remuneration.PIFRemitCount.ToTable.Rows(0).Item(1) > 1, "s", "") & vbCrLf & vbCrLf
                    LastRemitID = PIFCase("RemitID").ToString
                End If

                ' add the case details to the audit
                CaseDetail &= "CASE - START PROCESSING CaseId " & PIFCase("CaseID") & vbCrLf

                If Not IsDBNull(PIFCase("BailiffID")) Then
                    CaseDetail &= "Bailiff: " & PIFCase("Surname") & ", " & PIFCase("Forename") & "[" & PIFCase("BailiffID") & "]" & vbCrLf
                    CaseDetail &= "Bailifftype = " & IIf(PIFCase("SubType").ToString.ToUpper = "EMPLOYED", "Empl.", "S/Empl.") & vbCrLf
                Else
                    CaseDetail &= "No bailiff visit" & vbCrLf
                End If

                If Not IsDBNull(PIFCase("VisitDate")) Then CaseDetail &= "Last visit before payment on " & PIFCase("VisitDate") & vbCrLf
                CaseDetail &= "Last payment date on case " & PIFCase("LastPaymentDate") & vbCrLf
                CaseDetail &= "Case return date = " & PIFCase("ReturnDate") & vbCrLf

                PayingCase = Remuneration.GetPayingCase(PIFCase("CaseID"), "PIF") ' added TS 24/Oct/2014. Request ref 33821

                If PayingCase = "" Then ' this check added TS 24/Oct/2014. Request ref 33821
                    PayingCase = "" ' added TS 24/Oct/2014. Request ref 33821

                    If Not IsDBNull(PIFCase("AllocationDate")) Then DaysBetweenAllocationAndPayment = DateDiff("d", CDate(PIFCase("AllocationDate")).ToShortDateString, CDate(PIFCase("LastPaymentDate")).ToShortDateString)
                    If Not IsDBNull(PIFCase("VisitDate")) Then DaysBetweenVisitAndPayment = DateDiff("d", CDate(PIFCase("VisitDate")).ToShortDateString, CDate(PIFCase("LastPaymentDate")).ToShortDateString) ' uncommented TS 24/Feb/2015. Request ref 41977

                    ' now pay the bailiff if the criteria are met
                    'If Not IsDBNull(PIFCase("VisitStageSort")) Then ' These two lines commented out TS 10/Jun/2014 23529
                    '    If {500, 550}.Contains(PIFCase("VisitStageSort")) Then

                    If Not IsDBNull(PIFCase("InternalExternal")) Then
                        Select Case PIFCase("InternalExternal") ' added TS 10/Jun/2014 23529
                            Case "I"
                                If PIFCase("BailiffID") = PIFCase("AllocatedBailiffID") Then

                                    ' mod TS 10/Dec/2014 changed all references to visit date to allocation date. Request ref 36738
                                    'CaseDetail &= "No. of days from allocation to last payment date = " & DaysBetweenAllocationAndPayment.ToString & vbCrLf ' commented TS 24/Feb/2015. Request ref 41977
                                    CaseDetail &= "No. of days from last visit to last payment date = " & DaysBetweenVisitAndPayment.ToString & vbCrLf ' added TS 24/Feb/2015. Request ref 41977

                                    If PIFCase("EnforcementFees") > 0 Then
                                        ' Get details of case in the same allocation batch
                                        ' EnforcementFeeDate can be NULL but not when EnforcementFees are > 0, so no need to handle NULL. Added TS 23/Jul/2014 26481
                                        Remuneration.GetAllocationBatchDetailsPIF(PIFCase("CaseID"), If(IsDBNull(PIFCase("linkID")), "", PIFCase("LinkID")), PIFCase("AllocationDate"), PIFCase("BailiffID"), PIFCase("FirstAllocationVisitDate"), PIFCase("EnforcementFeeDate"), dtpCalcMonth.Value.AddMonths(1).AddDays(-1))

                                        If Not IsDBNull(PIFCase("LinkID")) Then
                                            ' PayingCase = Remuneration.GetAllocationBatchPayingCase(PIFCase("linkID"), PIFCase("AllocationDate"), PIFCase("BailiffID"), "PIF") commented out TS 14/Aug/2014 request ref 28078
                                            PayingCase = Remuneration.GetAllocationBatchPayingCasePIF(PIFCase("linkID"), PIFCase("EnforcementFeeDate"), PIFCase("BailiffID")) ' added TS 14/Aug/2014 request ref 28078
                                            TotalClientDebtBalance = Remuneration.AllocationBatch.Rows(0)("TotalClientDebtBalance")
                                            LastBatchPaymentDate = Remuneration.AllocationBatch.Rows(0)("LastBatchPaymentDate")
                                            OpenBatchCases = Remuneration.AllocationBatch.Rows(0)("OpenCases")
                                        Else
                                            TotalClientDebtBalance = Remuneration.AllocationBatch.Rows(0)("TotalClientDebtBalance")
                                            LastBatchPaymentDate = Remuneration.AllocationBatch.Rows(0)("LastBatchPaymentDate")
                                            OpenBatchCases = 0
                                        End If

                                        If OpenBatchCases = 0 Then

                                            If PIFCase("LastPaymentDate") = LastBatchPaymentDate Then

                                                If PayingCase = "" Then

                                                    'Select Case DaysBetweenAllocationAndPayment ' commented TS 24/Feb/2015. Request ref 41977
                                                    Select Case DaysBetweenVisitAndPayment ' added TS 24/Feb/2015. Request ref 41977
                                                        Case Is <= FirstEnforcementAmount1Cutoff
                                                            Payment = PIFCase("FirstAmount1")
                                                            Quality = (If(IsDBNull(PIFCase("visited")), 0, CInt(PIFCase("Visited"))) + CInt(PIFCase("BWV")) + CInt(PIFCase("Complaints")) + CInt(PIFCase("FieldAdministration")) + CInt(PIFCase("Turnaround"))) * -PIFCase("QualityBonus") ' minus needed as true = -1

                                                            ' the following section added TS 19/Mar/2015. Request ref 22167
                                                            If PIFCase("WorkingAwayUplift") > 0 Then
                                                                ' Add Endate expiry check
                                                                If IsDBNull(PIFCase("UpliftEndDate")) Then
                                                                    WorkingAwayUplift = PIFCase("WorkingAwayUplift")
                                                                Else
                                                                    If PIFCase("UpliftEndDate") >= PIFCase("LastPaymentDate") Then
                                                                        WorkingAwayUplift = PIFCase("WorkingAwayUplift")
                                                                    Else
                                                                        CaseDetail &= "Working away uplift of " & CDec(PIFCase("WorkingAwayUplift")).ToString("0.00") & " expired on " & PIFCase("UpliftEndDate") & vbCrLf
                                                                    End If
                                                                End If

                                                            End If
                                                            ' end of 22167 section

                                                        Case Is <= FirstEnforcementAmount2Cutoff
                                                            Payment = PIFCase("FirstAmount2")
                                                            Quality = (If(IsDBNull(PIFCase("visited")), 0, CInt(PIFCase("Visited"))) + CInt(PIFCase("BWV")) + CInt(PIFCase("Complaints")) + CInt(PIFCase("FieldAdministration")) + CInt(PIFCase("Turnaround"))) * -PIFCase("QualityBonus") ' minus needed as true = -1
                                                        Case Else
                                                            Payment = PIFCase("FirstAmount3")
                                                    End Select

                                                    If TotalClientDebtBalance > LargeBalanceThreshold Then LargeBalancePayment = Math.Floor((Math.Min(TotalClientDebtBalance, LargeBalanceMaximum) - LargeBalanceThreshold) / LargeBalanceIncrement) * PIFCase("LargeBalanceBonus")

                                                Else
                                                    CaseDetail &= "Allocation batch already paid on " & PayingCase & vbCrLf
                                                End If

                                            Else
                                                CaseDetail &= "Last payment on case is not the last payment on allocation batch" & vbCrLf
                                            End If

                                        Else
                                            CaseDetail &= "Allocation batch has open cases" & vbCrLf
                                        End If

                                    Else
                                        CaseDetail &= "No enforcement fees on case" & vbCrLf
                                    End If
                                Else
                                    CaseDetail &= "Case is no longer allocated to bailiff" & vbCrLf
                                End If

                            Case "E"

                                If PIFCase("BailiffID") = PIFCase("AllocatedBailiffID") Then
                                    If PIFCase("EnforcementFees") > 0 Then

                                        ' Get details of case in the same allocation batch
                                        ' EnforcementFeeDate can be NULL but not when EnforcementFees are > 0, so no need to handle NULL. Added TS 23/Jul/2014 26481
                                        Remuneration.GetAllocationBatchDetailsPIF(PIFCase("CaseID"), If(IsDBNull(PIFCase("linkID")), "", PIFCase("LinkID")), PIFCase("AllocationDate"), PIFCase("BailiffID"), PIFCase("FirstAllocationVisitDate"), PIFCase("EnforcementFeeDate"), dtpCalcMonth.Value.AddMonths(1).AddDays(-1))

                                        If Not IsDBNull(PIFCase("LinkID")) Then
                                            ' PayingCase = Remuneration.GetAllocationBatchPayingCase(PIFCase("linkID"), PIFCase("AllocationDate"), PIFCase("BailiffID"), "PIF") commented out TS 14/Aug/2014 request ref 28078
                                            PayingCase = Remuneration.GetAllocationBatchPayingCasePIF(PIFCase("linkID"), PIFCase("EnforcementFeeDate"), PIFCase("BailiffID")) ' added TS 14/Aug/2014 request ref 28078
                                            TotalClientDebtBalance = Remuneration.AllocationBatch.Rows(0)("TotalClientDebtBalance")
                                            LastBatchPaymentDate = Remuneration.AllocationBatch.Rows(0)("LastBatchPaymentDate")
                                            OpenBatchCases = Remuneration.AllocationBatch.Rows(0)("OpenCases")
                                        Else
                                            TotalClientDebtBalance = Remuneration.AllocationBatch.Rows(0)("TotalClientDebtBalance")
                                            LastBatchPaymentDate = Remuneration.AllocationBatch.Rows(0)("LastBatchPaymentDate")
                                            OpenBatchCases = 0
                                        End If

                                        If OpenBatchCases = 0 Then

                                            If PIFCase("LastPaymentDate") = LastBatchPaymentDate Then

                                                If PayingCase = "" Then

                                                    ' The following section commented out TS 10/Jun/2014 23529
                                                    'Select Case PIFCase("InternalExternal")
                                                    '    Case "I"
                                                    '        CaseDetail &= "No. of days from visit to case return date = " & DaysBetweenVisitAndPayment.ToString & vbCrLf

                                                    '        Select Case DaysBetweenVisitAndPayment
                                                    '            Case Is <= SecondEnforcementAmount1Cutoff
                                                    '                Payment = PIFCase("SecondAmount1")

                                                    '            Case Is <= SecondEnforcementAmount2Cutoff
                                                    '                Payment = PIFCase("SecondAmount2")

                                                    '            Case Else
                                                    '                Payment = PIFCase("SecondAmount3")
                                                    '        End Select

                                                    '    Case "E"
                                                    CaseDetail &= "No. of days from allocation to case return date = " & DaysBetweenAllocationAndPayment.ToString & vbCrLf

                                                    If TotalClientDebtBalance > LargeBalanceThreshold Then LargeBalancePayment = Math.Floor((Math.Min(TotalClientDebtBalance, LargeBalanceMaximum) - LargeBalanceThreshold) / LargeBalanceIncrement) * PIFCase("LargeBalanceBonus")

                                                    If Remuneration.AllocationBatch.Rows(0)("ClosedCases") = 1 Then ' request ref 34265

                                                        Select Case DaysBetweenAllocationAndPayment
                                                            Case Is <= SecondEnforcementAmount1Cutoff
                                                                Payment = PIFCase("SecondAmount1")

                                                                ' commented out TS 25/11/2015. Request ref 65201
                                                                'Quality = (If(IsDBNull(PIFCase("Visited")), 0, CInt(PIFCase("Visited"))) + CInt(PIFCase("BWV")) + CInt(PIFCase("Complaints")) + CInt(PIFCase("FieldAdministration")) + CInt(PIFCase("Turnaround"))) * -PIFCase("QualityBonus") ' minus needed as true = -1
                                                                ' added TS 25/11/2015. Request ref 65201
                                                                Quality = ((CInt(PIFCase("BWV")) + CInt(PIFCase("Complaints")) + CInt(PIFCase("FieldAdministration")) + CInt(PIFCase("Turnaround"))) * -PIFCase("QualityBonus")) + PIFCase("Batch") ' minus needed as true = -1

                                                                Productivity = PIFCase("Productivity")

                                                                ' the following section added TS 19/Mar/2015. Request ref 22167
                                                                If PIFCase("WorkingAwayUplift") > 0 Then
                                                                    ' Add Endate expiry check
                                                                    If IsDBNull(PIFCase("UpliftEndDate")) Then
                                                                        WorkingAwayUplift = PIFCase("WorkingAwayUplift")
                                                                    Else
                                                                        If PIFCase("UpliftEndDate") >= PIFCase("LastPaymentDate") Then
                                                                            WorkingAwayUplift = PIFCase("WorkingAwayUplift")
                                                                        Else
                                                                            CaseDetail &= "Working away uplift of " & CDec(PIFCase("WorkingAwayUplift")).ToString("0.00") & " expired on " & PIFCase("UpliftEndDate") & vbCrLf
                                                                        End If
                                                                    End If

                                                                End If
                                                                ' end of 22167 section

                                                            Case Is <= SecondEnforcementAmount2Cutoff
                                                                Payment = PIFCase("SecondAmount2")

                                                            Case Else
                                                                Payment = PIFCase("SecondAmount3")

                                                        End Select

                                                    Else ' the following section added TS 30/Oct/2014 request ref 34265
                                                        ' the following commented out TS 15/Jun/2017. Request ref 106437
                                                        'Select Case DaysBetweenAllocationAndPayment
                                                        '    Case Is <= SecondEnforcementAmount1LinkedCutoff

                                                        '        If DateDiff("d", CDate(PIFCase("AllocationDate")).ToShortDateString, CDate(Remuneration.AllocationBatch.Rows(0)("EarliestLastBatchPaymentDate")).ToShortDateString) < SecondEnforcementAmount1Cutoff Then

                                                        '            Payment = PIFCase("SecondAmount1")

                                                        '            ' commented out TS 25/11/2015. Request ref 65201
                                                        '            'Quality = (If(IsDBNull(PIFCase("Visited")), 0, CInt(PIFCase("Visited"))) + CInt(PIFCase("BWV")) + CInt(PIFCase("Complaints")) + CInt(PIFCase("FieldAdministration")) + CInt(PIFCase("Turnaround"))) * -PIFCase("QualityBonus") ' minus needed as true = -1
                                                        '            ' added TS 25/11/2015. Request ref 65201
                                                        '            Quality = ((CInt(PIFCase("BWV")) + CInt(PIFCase("Complaints")) + CInt(PIFCase("FieldAdministration")) + CInt(PIFCase("Turnaround"))) * -PIFCase("QualityBonus")) + PIFCase("Batch") ' minus needed as true = -1

                                                        '            Productivity = PIFCase("Productivity")

                                                        '            ' the following section added TS 19/Mar/2015. Request ref 22167
                                                        '            If PIFCase("WorkingAwayUplift") > 0 Then
                                                        '                ' Add Endate expiry check
                                                        '                If IsDBNull(PIFCase("UpliftEndDate")) Then
                                                        '                    WorkingAwayUplift = PIFCase("WorkingAwayUplift")
                                                        '                Else
                                                        '                    If PIFCase("UpliftEndDate") >= PIFCase("LastPaymentDate") Then
                                                        '                        WorkingAwayUplift = PIFCase("WorkingAwayUplift")
                                                        '                    Else
                                                        '                        CaseDetail &= "Working away uplift of " & CDec(PIFCase("WorkingAwayUplift")).ToString("0.00") & " expired on " & PIFCase("UpliftEndDate") & vbCrLf
                                                        '                    End If
                                                        '                End If

                                                        '            End If
                                                        '            ' end of 22167 section

                                                        '            CaseDetail &= "Revised PIF logic triggered" & vbCrLf

                                                        '        Else
                                                        '            Payment = PIFCase("SecondAmount2")

                                                        '            Quality = 0

                                                        '            Productivity = 0

                                                        '        End If
                                                        '    Case Is <= SecondEnforcementAmount2Cutoff
                                                        '        Payment = PIFCase("SecondAmount2")

                                                        '    Case Else
                                                        '        Payment = PIFCase("SecondAmount3")

                                                        'End Select
                                                        ' the following added TS 15/Jun/2017. Request ref 106437

                                                        If DaysBetweenAllocationAndPayment / Remuneration.AllocationBatch.Rows(0)("ClosedCases") <= SecondEnforcementAmount1Cutoff Then
                                                            DaysBetweenAllocationAndPayment = SecondEnforcementAmount1Cutoff
                                                            CaseDetail &= "Revised PIF logic triggered" & vbCrLf
                                                        End If

                                                        Select Case DaysBetweenAllocationAndPayment
                                                            Case Is <= SecondEnforcementAmount1Cutoff
                                                                Payment = PIFCase("SecondAmount1")

                                                                Quality = ((CInt(PIFCase("BWV")) + CInt(PIFCase("Complaints")) + CInt(PIFCase("FieldAdministration")) + CInt(PIFCase("Turnaround"))) * -PIFCase("QualityBonus")) + PIFCase("Batch") ' minus needed as true = -1

                                                                Productivity = PIFCase("Productivity")

                                                                If PIFCase("WorkingAwayUplift") > 0 Then
                                                                    ' Add Endate expiry check
                                                                    If IsDBNull(PIFCase("UpliftEndDate")) Then
                                                                        WorkingAwayUplift = PIFCase("WorkingAwayUplift")
                                                                    Else
                                                                        If PIFCase("UpliftEndDate") >= PIFCase("LastPaymentDate") Then
                                                                            WorkingAwayUplift = PIFCase("WorkingAwayUplift")
                                                                        Else
                                                                            CaseDetail &= "Working away uplift of " & CDec(PIFCase("WorkingAwayUplift")).ToString("0.00") & " expired on " & PIFCase("UpliftEndDate") & vbCrLf
                                                                        End If
                                                                    End If

                                                                End If

                                                            Case Is <= SecondEnforcementAmount2Cutoff
                                                                Payment = PIFCase("SecondAmount2")

                                                            Case Else
                                                                Payment = PIFCase("SecondAmount3")

                                                        End Select
                                                    End If '  end of request ref 106437

                                                    'End Select

                                                Else
                                                    CaseDetail &= "Allocation batch already paid on " & PayingCase & vbCrLf
                                                End If

                                            Else
                                                CaseDetail &= "Last payment on case is not the last payment on allocation batch" & vbCrLf
                                            End If

                                        Else
                                            CaseDetail &= "Allocation batch has open cases" & vbCrLf
                                        End If

                                    Else
                                        CaseDetail &= "No enforcement fees on case" & vbCrLf
                                    End If
                                Else
                                    CaseDetail &= "Case is no longer allocated to bailiff" & vbCrLf
                                End If
                        End Select

                        If Payment > 0 Then
                            CaseDetail &= "Payment amount = " & Payment.ToString("0.00") & vbCrLf

                            ' WorkingAwayUplift added TS 14/Apr/2015. Request ref 47167
                            If Not IsDBNull(PIFCase("VatReg")) AndAlso PIFCase("VatReg").ToString.Length > 0 Then Vat = (Payment + LargeBalancePayment + Quality + Productivity + WorkingAwayUplift) * VatRate

                            Remuneration.AddBailiffPayment(PIFCase("CaseId"), _
                                                               If(IsDBNull(PIFCase("LinkID")), "", PIFCase("LinkID")), _
                                                               PIFCase("BailiffId"), _
                                                               PIFCase("ClientId"), _
                                                               PIFCase("WorkTypeId"), _
                                                               PIFCase("VisitID"), _
                                                               PIFCase("VisitDate"), _
                                                               If(IsDBNull(PIFCase("AllocationDate")), CDate("01-01-1900"), PIFCase("AllocationDate")), _
                                                               PIFCase("EnforcementFeeDate"), _
                                                               InvoiceDate, _
                                                               "PIF" & InvoiceDate.ToString("yyyyMMdd") & PIFCase("BailiffId").ToString, _
                                                               InvoiceDate, _
                                                               Payment, _
                                                               LargeBalancePayment, _
                                                               Quality, _
                                                               Productivity, _
                                                               WorkingAwayUplift, _
                                                               Vat, _
                                                               dtpCalcMonth.Value, _
                                                               dtpCalcMonth.Value.AddMonths(1).AddDays(-1), _
                                                               If(IsDBNull(PIFCase("LoginName")), "", PIFCase("LoginName")), _
                                                               "PIF")
                        End If

                        If LargeBalancePayment > 0 Then CaseDetail &= "Large balance payment of " & LargeBalancePayment.ToString("0.00") & vbCrLf
                        If Quality > 0 Then CaseDetail &= Quality.ToString("0.00") & " quality triggers" & vbCrLf
                        If Productivity > 0 Then CaseDetail &= Productivity.ToString("0.00") & " productivity bonus" & vbCrLf
                        If WorkingAwayUplift > 0 Then CaseDetail &= "Working away uplift = " & WorkingAwayUplift.ToString("0.00") & vbCrLf ' added TS 19/Mar/2015. Request ref 22167

                    Else
                        'CaseDetail &= "Visit not made at a valid stage" & vbCrLf & vbCrLf commented out TS 10/Jun/2014 23529
                        CaseDetail &= "No bailiff visit" & vbCrLf & vbCrLf ' added TS 10/Jun/2014 23529
                    End If

                Else
                    CaseDetail &= "Case already paid on" & vbCrLf
                End If

                CaseDetail &= "CASE - END PROCESSING " & PIFCase("CaseID") & vbCrLf & vbCrLf
                rtxtAudit.AppendText(CaseDetail)

                Dim Para As New Paragraph(CaseDetail)
                Para.Font.Size = 6
                pdfDoc.Add(Para)

                prgMain.Value += 1

            Next PIFCase

            If Remuneration.PIFCases.Rows.Count > 0 Then pdfDoc.Close()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub CalculateCGA()
        ' WorkingAwayUplift added TS 19/Mar/2015 request ref 22167
        Try
            ' Dim DaysBetweenVisitAndPayment As Integer
            Dim CaseDetail As String = "", PayingCase As String
            Dim CGACount As Integer

            lblProgress.Text = "Importing CGA cases"
            lblProgress.Visible = True
            Application.DoEvents()
            Remuneration.ImportCGA(dtpCalcMonth.Value, dtpCalcMonth.Value.AddMonths(1).AddDays(-1))
            lblProgress.Visible = False

            Dim pdfDoc As New Document()
            pdfDoc.Open()
            Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(AuditFile & "CGA_audit_" & InvoiceDate.ToString("yyyyMMdd") & ".pdf", FileMode.Append))
            Dim ev As New itsEvents
            pdfWrite.PageEvent = ev
            pdfDoc.Open()

            prgMain.Value = 1
            prgMain.Maximum = Remuneration.CGACases.Rows.Count + 1

            For Each CGACase As DataRow In Remuneration.CGACases.Rows

                Payment = 0
                Vat = 0
                PayingCase = ""
                CGACount = 0

                Application.DoEvents()

                CaseDetail = "CASE - START PROCESSING CaseId " & CGACase("CaseID") & vbCrLf
                CaseDetail &= "Bailiff: " & CGACase("Surname") & ", " & CGACase("Forename") & "[" & CGACase("BailiffID") & "]" & " added CGA: " & CGACase("VisitDate") & vbCrLf

                'If (CGACase("status_open_closed") <> "C" And CGACase("status") <> "S") OrElse (CGACase("status_open_closed") = "C" And CGACase("status") = "S" And (CDate(CGACase("VisitDate")) - CDate(CGACase("return_date")).Date).TotalDays > FirstEnforcementAmount1Cutoff) Then ' changed Now.Date to CGACase("VisitDate") TS 12/Jun/2014 24123. Commented out TS 06/Oct/2014 Request ref 32313
                'If CGACase("status") <> "S" OrElse (CGACase("status") = "S" And (CDate(CGACase("LastPaymentDate")) - CDate(CGACase("VisitDate")).Date).TotalDays > FirstEnforcementAmount1Cutoff) Then ' changed Now.Date to CGACase("VisitDate") TS 12/Jun/2014 24123 Commented out TS 10/Dec/2014. Request ref 37489

                'If Not IsDBNull(CGACase("LinkID")) Then commented out TS 13/Aug/2014 request ref 31130
                'PayingCase = Remuneration.GetAllocationBatchPayingCase(CGACase("linkID"), CGACase("VisitDate"), CGACase("BailiffID"), "CGA") commented out TS 13/Aug/2014 request ref 25433
                PayingCase = Remuneration.GetAllocationBatchPayingCaseCGA(CGACase("CaseID"), If(IsDBNull(CGACase("linkID")), "", CGACase("linkID")), CGACase("VisitDate"), CGACase("BailiffID")) ' added TS 13/Aug/2014 request ref 25433
                'End If commented out TS 13/Aug/2014 request ref 31130

                If PayingCase = "" Then

                    CGACount = Remuneration.GetCaseCGACount(CGACase("CaseID"))

                    If CGACount = 1 Then

                        CaseDetail &= "Payment of " & CGACase("CGAPayment") & vbCrLf
                        Payment = CGACase("CGAPayment")

                        If Not IsDBNull(CGACase("VatReg")) AndAlso CGACase("VatReg").ToString.Length > 0 Then Vat = Payment * VatRate

                        Remuneration.AddBailiffPayment(CGACase("CaseId"), _
                                                       If(IsDBNull(CGACase("LinkID")), "", CGACase("LinkID")), _
                                                        CGACase("BailiffId"), _
                                                        CGACase("ClientId"), _
                                                        CGACase("WorkTypeId"), _
                                                        CGACase("VisitID"), _
                                                        CGACase("VisitDate"), _
                                                        If(IsDBNull(CGACase("AllocationDate")), CDate("01-01-1900"), CGACase("AllocationDate")), _
                                                        Nothing, _
                                                        InvoiceDate, _
                                                        "CGA" & InvoiceDate.ToString("yyyyMMdd") & CGACase("BailiffId").ToString, _
                                                        InvoiceDate, _
                                                        Payment, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        Vat, _
                                                        dtpCalcMonth.Value, _
                                                        dtpCalcMonth.Value.AddMonths(1).AddDays(-1), _
                                                        If(IsDBNull(CGACase("LoginName")), "", CGACase("LoginName")), _
                                                        "CGA")
                    Else
                        CaseDetail &= "Case has " & CGACount.ToString & "s CGAs" & vbCrLf
                    End If
                Else
                    CaseDetail &= "CGA already paid on " & PayingCase & vbCrLf ' Allocation batch changed to CGA. TS 13/Aug/2014 request ref 25433
                End If

                'Else Commented out TS 10/Dec/2014. Request ref 37489
                'CaseDetail &= "No CGA payment: Last payment received on " & CGACase("LastPaymentDate") & vbCrLf Commented out TS 10/Dec/2014. Request ref 37489
                'End If Commented out TS 10/Dec/2014. Request ref 37489

                CaseDetail &= "CASE - END PROCESSING " & CGACase("CaseID") & vbCrLf & vbCrLf
                rtxtAudit.AppendText(CaseDetail)

                Dim Para As New Paragraph(CaseDetail)
                Para.Font.Size = 6
                pdfDoc.Add(Para)

                prgMain.Value += 1
            Next CGACase

            If Remuneration.CGACases.Rows.Count > 0 Then pdfDoc.Close()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub CalculateMHRVisit()
        Try
            Dim CaseDetail As String = ""

            lblProgress.Text = "Importing MHRVisit cases"
            lblProgress.Visible = True
            Application.DoEvents()
            Remuneration.ImportMHRVisit(dtpCalcMonth.Value, dtpCalcMonth.Value.AddMonths(1).AddDays(-1))
            lblProgress.Visible = False

            Dim pdfDoc As New Document()
            pdfDoc.Open()
            Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(AuditFile & "MHRVisit_audit_" & InvoiceDate.ToString("yyyyMMdd") & ".pdf", FileMode.Append))
            Dim ev As New itsEvents
            pdfWrite.PageEvent = ev
            pdfDoc.Open()

            prgMain.Value = 1
            prgMain.Maximum = Remuneration.MHRVisitCases.Rows.Count + 1

            For Each MHRVisitCase As DataRow In Remuneration.MHRVisitCases.Rows

                Payment = 0
                Vat = 0

                Application.DoEvents()

                CaseDetail = "CASE - START PROCESSING CaseId " & MHRVisitCase("CaseID") & vbCrLf
                CaseDetail &= "Bailiff: " & MHRVisitCase("Surname") & ", " & MHRVisitCase("Forename") & "[" & MHRVisitCase("BailiffID") & "]" & " visited: " & MHRVisitCase("VisitDate") & vbCrLf

                Select Case MHRVisitCase("return_CodeID")
                    Case 146
                        Payment = MHRVisitCase("HolidayHomeDeclarationPayment")
                    Case 147
                        Payment = MHRVisitCase("SoleMainResidencePayment")
                End Select

                CaseDetail &= "Case returned on " & MHRVisitCase("ReturnDate") & " with return code " & MHRVisitCase("return_CodeID").ToString & " Payment of " & Payment.ToString("0.00") & vbCrLf

                If Not IsDBNull(MHRVisitCase("VatReg")) AndAlso MHRVisitCase("VatReg").ToString.Length > 0 Then Vat = Payment * VatRate

                Remuneration.AddBailiffPayment(MHRVisitCase("CaseId"), _
                                               If(IsDBNull(MHRVisitCase("LinkID")), "", MHRVisitCase("LinkID")), _
                                                MHRVisitCase("BailiffId"), _
                                                MHRVisitCase("ClientId"), _
                                                MHRVisitCase("WorkTypeId"), _
                                                MHRVisitCase("VisitID"), _
                                                MHRVisitCase("VisitDate"), _
                                                If(IsDBNull(MHRVisitCase("AllocationDate")), CDate("01-01-1900"), MHRVisitCase("AllocationDate")), _
                                                Nothing, _
                                                InvoiceDate, _
                                                "MHR" & InvoiceDate.ToString("yyyyMMdd") & MHRVisitCase("BailiffId").ToString, _
                                                InvoiceDate, _
                                                Payment, _
                                                0, _
                                                0, _
                                                0, _
                                                0, _
                                                Vat, _
                                                dtpCalcMonth.Value, _
                                                dtpCalcMonth.Value.AddMonths(1).AddDays(-1), _
                                                If(IsDBNull(MHRVisitCase("LoginName")), "", MHRVisitCase("LoginName")), _
                                                "MHR")


                CaseDetail &= "CASE - END PROCESSING " & MHRVisitCase("CaseID") & vbCrLf & vbCrLf
                rtxtAudit.AppendText(CaseDetail)

                Dim Para As New Paragraph(CaseDetail)
                Para.Font.Size = 6
                pdfDoc.Add(Para)

                prgMain.Value += 1
            Next MHRVisitCase

            If Remuneration.MHRVisitCases.Rows.Count > 0 Then pdfDoc.Close()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub CalculateCollectPayment()
        Try
            Dim CaseDetail As String = ""

            lblProgress.Text = "Importing Collect payment cases"
            lblProgress.Visible = True
            Application.DoEvents()
            Remuneration.ImportCollectPayment(dtpCalcMonth.Value, dtpCalcMonth.Value.AddMonths(1).AddDays(-1))
            lblProgress.Visible = False

            Dim pdfDoc As New Document()
            pdfDoc.Open()
            Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(AuditFile & "CollectPayment_audit_" & InvoiceDate.ToString("yyyyMMdd") & ".pdf", FileMode.Append))
            Dim ev As New itsEvents
            pdfWrite.PageEvent = ev
            pdfDoc.Open()

            prgMain.Value = 1
            prgMain.Maximum = Remuneration.CollectPaymentCases.Rows.Count + 1

            For Each CollectPaymentCase As DataRow In Remuneration.CollectPaymentCases.Rows

                Payment = 0
                Vat = 0

                Application.DoEvents()

                CaseDetail = "CASE - START PROCESSING CaseId " & CollectPaymentCase("CaseID") & vbCrLf
                CaseDetail &= "Bailiff: " & CollectPaymentCase("Surname") & ", " & CollectPaymentCase("Forename") & "[" & CollectPaymentCase("BailiffID") & "]" & " visited: " & CollectPaymentCase("VisitDate") & vbCrLf

                Payment = CollectPaymentCase("PaymentAmount") * CollectPaymentCase("CollectPaymentCommission")

                CaseDetail &= "Payment from debtor of " & CollectPaymentCase("PaymentAmount") & " remitted on " & CollectPaymentCase("PaymentRemitDate") & ". Payment to EA of " & Payment.ToString("0.00") & vbCrLf

                ' this section added TS 29/Dec/2016. Request ref 96257
                If Payment > CollectPaymentCase("CollectPaymentCap") Then
                    Payment = CollectPaymentCase("CollectPaymentCap")
                    CaseDetail &= "Payment capped at " & CollectPaymentCase("CollectPaymentCap").ToString("0.00") & vbCrLf
                End If
                ' end of 96257 section

                If Not IsDBNull(CollectPaymentCase("VatReg")) AndAlso CollectPaymentCase("VatReg").ToString.Length > 0 Then Vat = Payment * VatRate

                Remuneration.AddBailiffPayment(CollectPaymentCase("CaseId"), _
                                               If(IsDBNull(CollectPaymentCase("LinkID")), "", CollectPaymentCase("LinkID")), _
                                                CollectPaymentCase("BailiffId"), _
                                                CollectPaymentCase("ClientId"), _
                                                CollectPaymentCase("WorkTypeId"), _
                                                CollectPaymentCase("VisitID"), _
                                                CollectPaymentCase("VisitDate"), _
                                                If(IsDBNull(CollectPaymentCase("AllocationDate")), CDate("01-01-1900"), CollectPaymentCase("AllocationDate")), _
                                                Nothing, _
                                                InvoiceDate, _
                                                "COL" & InvoiceDate.ToString("yyyyMMdd") & CollectPaymentCase("BailiffId").ToString, _
                                                InvoiceDate, _
                                                Payment, _
                                                0, _
                                                0, _
                                                0, _
                                                0, _
                                                Vat, _
                                                dtpCalcMonth.Value, _
                                                dtpCalcMonth.Value.AddMonths(1).AddDays(-1), _
                                                If(IsDBNull(CollectPaymentCase("LoginName")), "", CollectPaymentCase("LoginName")), _
                                                "COL")


                CaseDetail &= "CASE - END PROCESSING " & CollectPaymentCase("CaseID") & vbCrLf & vbCrLf
                rtxtAudit.AppendText(CaseDetail)

                Dim Para As New Paragraph(CaseDetail)
                Para.Font.Size = 6
                pdfDoc.Add(Para)

                prgMain.Value += 1
            Next CollectPaymentCase

            If Remuneration.CollectPaymentCases.Rows.Count > 0 Then pdfDoc.Close()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub CalculateCGAPIF() ' Added TS 07/Jul/2017. Request ref 112264
        Try

            Dim CaseDetail As String = "", PayingCase As String
            Dim CGACount As Integer
            Dim OpenBatchCases As Integer

            lblProgress.Text = "Importing CGA PIF cases"
            lblProgress.Visible = True
            Application.DoEvents()
            Remuneration.ImportCGAPIF(dtpCalcMonth.Value, dtpCalcMonth.Value.AddMonths(1).AddDays(-1))
            lblProgress.Visible = False

            Dim pdfDoc As New Document()
            pdfDoc.Open()
            Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(AuditFile & "CGAPIF_audit_" & InvoiceDate.ToString("yyyyMMdd") & ".pdf", FileMode.Append))
            Dim ev As New itsEvents
            pdfWrite.PageEvent = ev
            pdfDoc.Open()

            prgMain.Value = 1
            prgMain.Maximum = Remuneration.CGAPIFCases.Rows.Count + 1

            For Each CGAPIFCase As DataRow In Remuneration.CGAPIFCases.Rows

                Payment = 0
                Vat = 0
                PayingCase = ""
                CGACount = 0
                OpenBatchCases = 0

                Application.DoEvents()

                CaseDetail = "CASE - START PROCESSING CaseId " & CGAPIFCase("CaseID") & vbCrLf
                CaseDetail &= "Bailiff: " & CGAPIFCase("Surname") & ", " & CGAPIFCase("Forename") & "[" & CGAPIFCase("BailiffID") & "]" & " added CGA: " & CGAPIFCase("VisitDate") & vbCrLf

                PayingCase = Remuneration.GetAllocationBatchPayingCaseCGAPIF(CGAPIFCase("CaseID"), If(IsDBNull(CGAPIFCase("linkID")), "", CGAPIFCase("linkID")), CGAPIFCase("VisitDate"), CGAPIFCase("BailiffID"))
                Remuneration.GetAllocationBatchDetailsPIF(CGAPIFCase("CaseID"), If(IsDBNull(CGAPIFCase("linkID")), "", CGAPIFCase("LinkID")), CGAPIFCase("AllocationDate"), CGAPIFCase("BailiffID"), CGAPIFCase("FirstAllocationVisitDate"), CGAPIFCase("EnforcementFeeDate"), dtpCalcMonth.Value.AddMonths(1).AddDays(-1))

                If Not IsDBNull(CGAPIFCase("LinkID")) Then
                    OpenBatchCases = Remuneration.AllocationBatch.Rows(0)("OpenCases")
                Else
                    OpenBatchCases = 0
                End If

                If OpenBatchCases = 0 Then

                    If PayingCase = "" Then

                        CGACount = Remuneration.GetCaseCGACount(CGAPIFCase("CaseID"))

                        If CGACount = 1 Then

                            CaseDetail &= "Payment of 5" & vbCrLf
                            Payment = 5

                            If Not IsDBNull(CGAPIFCase("VatReg")) AndAlso CGAPIFCase("VatReg").ToString.Length > 0 Then Vat = Payment * VatRate

                            Remuneration.AddBailiffPayment(CGAPIFCase("CaseId"), _
                                                           If(IsDBNull(CGAPIFCase("LinkID")), "", CGAPIFCase("LinkID")), _
                                                            CGAPIFCase("BailiffId"), _
                                                            CGAPIFCase("ClientId"), _
                                                            CGAPIFCase("WorkTypeId"), _
                                                            CGAPIFCase("VisitID"), _
                                                            CGAPIFCase("VisitDate"), _
                                                            If(IsDBNull(CGAPIFCase("AllocationDate")), CDate("01-01-1900"), CGAPIFCase("AllocationDate")), _
                                                            Nothing, _
                                                            InvoiceDate, _
                                                            "CGAPIF" & InvoiceDate.ToString("yyyyMMdd") & CGAPIFCase("BailiffId").ToString, _
                                                            InvoiceDate, _
                                                            Payment, _
                                                            0, _
                                                            0, _
                                                            0, _
                                                            0, _
                                                            Vat, _
                                                            dtpCalcMonth.Value, _
                                                            dtpCalcMonth.Value.AddMonths(1).AddDays(-1), _
                                                            If(IsDBNull(CGAPIFCase("LoginName")), "", CGAPIFCase("LoginName")), _
                                                            "CGAPIF")
                        Else
                            CaseDetail &= "Case has " & CGACount.ToString & "s CGAs" & vbCrLf
                        End If
                    Else
                        CaseDetail &= "CGA already paid on " & PayingCase & vbCrLf
                    End If
                Else
                    CaseDetail &= "Allocation batch has open cases" & vbCrLf
                End If

                CaseDetail &= "CASE - END PROCESSING " & CGAPIFCase("CaseID") & vbCrLf & vbCrLf
                rtxtAudit.AppendText(CaseDetail)

                Dim Para As New Paragraph(CaseDetail)
                Para.Font.Size = 6
                pdfDoc.Add(Para)

                prgMain.Value += 1
            Next CGAPIFCase

            If Remuneration.CGAPIFCases.Rows.Count > 0 Then pdfDoc.Close()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub CalculateSaleFee() ' Added TS 09/Aug/2017. Request ref 114817
        Try
            Dim LastRemitID As Integer, OpenBatchCases As Integer
            Dim LargeBalancePayment As Decimal = 0, TotalClientDebtBalance As Decimal = 0
            Dim LastBatchPaymentDate As Date
            Dim CaseDetail As String = "", PayingCase As String

            ' get the PIF cases for the selected month
            lblProgress.Text = "Importing PIF cases"
            lblProgress.Visible = True
            Application.DoEvents()
            Remuneration.ImportSaleFee(dtpCalcMonth.Value, dtpCalcMonth.Value.AddMonths(1).AddDays(-1))
            Remuneration.GetPIFRemitCount()
            lblProgress.Visible = False

            Dim pdfDoc As New Document()
            pdfDoc.Open()
            Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(AuditFile & "SaleFee_audit_" & InvoiceDate.ToString("yyyyMMdd") & ".pdf", FileMode.Append))
            Dim ev As New itsEvents
            pdfWrite.PageEvent = ev
            pdfDoc.Open()

            prgMain.Value = 1
            prgMain.Maximum = Remuneration.PIFCases.Rows.Count + 1

            For Each PIFCase As DataRow In Remuneration.PIFCases.Rows

                CaseDetail = ""
                Payment = 0
                LargeBalancePayment = 0
                Vat = 0
                PayingCase = ""
                OpenBatchCases = 0
                TotalClientDebtBalance = 0
                LastBatchPaymentDate = Nothing

                Application.DoEvents()

                ' add the remit header if the remit id changes
                If PIFCase("RemitID") <> LastRemitID Then
                    Remuneration.PIFRemitCount.RowFilter = "RemitID = " & PIFCase("RemitID")
                    CaseDetail = "Remit " & PIFCase("RemitID") & " - " & Remuneration.PIFRemitCount.ToTable.Rows(0).Item(1) & " case" & IIf(Remuneration.PIFRemitCount.ToTable.Rows(0).Item(1) > 1, "s", "") & vbCrLf & vbCrLf
                    LastRemitID = PIFCase("RemitID").ToString
                End If

                ' add the case details to the audit
                CaseDetail &= "CASE - START PROCESSING CaseId " & PIFCase("CaseID") & vbCrLf

                If Not IsDBNull(PIFCase("BailiffID")) Then
                    CaseDetail &= "Bailiff: " & PIFCase("Surname") & ", " & PIFCase("Forename") & "[" & PIFCase("BailiffID") & "]" & vbCrLf
                    CaseDetail &= "Bailifftype = " & IIf(PIFCase("SubType").ToString.ToUpper = "EMPLOYED", "Empl.", "S/Empl.") & vbCrLf
                Else
                    CaseDetail &= "No bailiff visit" & vbCrLf
                End If

                If Not IsDBNull(PIFCase("VisitDate")) Then CaseDetail &= "Last visit before payment on " & PIFCase("VisitDate") & vbCrLf
                CaseDetail &= "Last payment date on case " & PIFCase("LastPaymentDate") & vbCrLf
                CaseDetail &= "Case return date = " & PIFCase("ReturnDate") & vbCrLf

                PayingCase = Remuneration.GetPayingCase(PIFCase("CaseID"), "SF")

                If PayingCase = "" Then ' this check added TS 24/Oct/2014. Request ref 33821
                    PayingCase = "" ' added TS 24/Oct/2014. Request ref 33821

                    If Not IsDBNull(PIFCase("InternalExternal")) Then

                        If Not IsDBNull(PIFCase("SaleFeeDate")) Then ' addd TS 29/Nov/2017

                            If PIFCase("BailiffID") = PIFCase("AllocatedBailiffID") Then

                                If PIFCase("EnforcementFees") > 0 Then
                                    ' Get details of case in the same allocation batch
                                    ' EnforcementFeeDate can be NULL but not when EnforcementFees are > 0, so no need to handle NULL. Added TS 23/Jul/2014 26481
                                    Remuneration.GetAllocationBatchDetailsPIF(PIFCase("CaseID"), If(IsDBNull(PIFCase("linkID")), "", PIFCase("LinkID")), PIFCase("AllocationDate"), PIFCase("BailiffID"), PIFCase("FirstAllocationVisitDate"), PIFCase("EnforcementFeeDate"), dtpCalcMonth.Value.AddMonths(1).AddDays(-1))

                                    If Not IsDBNull(PIFCase("LinkID")) Then
                                        PayingCase = Remuneration.GetAllocationBatchPayingCaseSaleFee(PIFCase("linkID"), PIFCase("SaleFeeDate"), PIFCase("BailiffID")) ' added TS 14/Aug/2014 request ref 28078 EnforcementFeeDate changed to SaleFeeDate. TS 27/Nov/2017
                                        TotalClientDebtBalance = Remuneration.AllocationBatch.Rows(0)("TotalClientDebtBalance")
                                        LastBatchPaymentDate = Remuneration.AllocationBatch.Rows(0)("LastBatchPaymentDate")
                                        OpenBatchCases = Remuneration.AllocationBatch.Rows(0)("OpenCases")
                                    Else
                                        TotalClientDebtBalance = Remuneration.AllocationBatch.Rows(0)("TotalClientDebtBalance")
                                        LastBatchPaymentDate = Remuneration.AllocationBatch.Rows(0)("LastBatchPaymentDate")
                                        OpenBatchCases = 0
                                    End If

                                    If OpenBatchCases = 0 Then

                                        If PIFCase("LastPaymentDate") = LastBatchPaymentDate Then

                                            If PayingCase = "" Then

                                                If Remuneration.GetSaleFeeCount(PIFCase("CaseID")) > 0 Then

                                                    'Payment = 38.5
                                                    Payment = 20 ' TS 29/Sep/2017

                                                    'If TotalClientDebtBalance > LargeBalanceThreshold Then LargeBalancePayment = Math.Round((TotalClientDebtBalance - LargeBalanceThreshold) * 0.01, 2)
                                                    If TotalClientDebtBalance > LargeBalanceThreshold Then LargeBalancePayment = Math.Floor((Math.Min(TotalClientDebtBalance, LargeBalanceMaximum) - LargeBalanceThreshold) / LargeBalanceIncrement) * 0.01 ' changed TS 27/Nov/2017

                                                Else

                                                    CaseDetail &= "No sale fee on case" & vbCrLf

                                                End If

                                            Else
                                                CaseDetail &= "Allocation batch already paid on " & PayingCase & vbCrLf
                                            End If

                                        Else
                                            CaseDetail &= "Last payment on case is not the last payment on allocation batch" & vbCrLf
                                        End If

                                    Else
                                        CaseDetail &= "Allocation batch has open cases" & vbCrLf
                                    End If

                                Else
                                    CaseDetail &= "No enforcement fees on case" & vbCrLf
                                End If
                            Else
                                CaseDetail &= "Case is no longer allocated to bailiff" & vbCrLf
                            End If

                            If Payment > 0 Then
                                CaseDetail &= "Payment amount = " & Payment.ToString("0.00") & vbCrLf

                                If Not IsDBNull(PIFCase("VatReg")) AndAlso PIFCase("VatReg").ToString.Length > 0 Then Vat = (Payment + LargeBalancePayment) * VatRate

                                Remuneration.AddBailiffPayment(PIFCase("CaseId"), _
                                                                   If(IsDBNull(PIFCase("LinkID")), "", PIFCase("LinkID")), _
                                                                   PIFCase("BailiffId"), _
                                                                   PIFCase("ClientId"), _
                                                                   PIFCase("WorkTypeId"), _
                                                                   PIFCase("VisitID"), _
                                                                   PIFCase("VisitDate"), _
                                                                   If(IsDBNull(PIFCase("AllocationDate")), CDate("01-01-1900"), PIFCase("AllocationDate")), _
                                                                   PIFCase("SaleFeeDate"), _
                                                                   InvoiceDate, _
                                                                   "SF" & InvoiceDate.ToString("yyyyMMdd") & PIFCase("BailiffId").ToString, _
                                                                   InvoiceDate, _
                                                                   Payment, _
                                                                   LargeBalancePayment, _
                                                                   0, _
                                                                   0, _
                                                                   0, _
                                                                   Vat, _
                                                                   dtpCalcMonth.Value, _
                                                                   dtpCalcMonth.Value.AddMonths(1).AddDays(-1), _
                                                                   If(IsDBNull(PIFCase("LoginName")), "", PIFCase("LoginName")), _
                                                                   "SF")        ' EnforcementFeeDate changed to SaleFeeDate TS 27/Nov/2017
                            End If

                            If LargeBalancePayment > 0 Then CaseDetail &= "Large balance payment of " & LargeBalancePayment.ToString("0.00") & vbCrLf
                        Else ' addd TS 29/Nov/2017

                            CaseDetail &= "No sale fee on case" & vbCrLf ' addd TS 29/Nov/2017

                        End If ' addd TS 29/Nov/2017

                    Else
                        'CaseDetail &= "Visit not made at a valid stage" & vbCrLf & vbCrLf commented out TS 10/Jun/2014 23529
                        CaseDetail &= "No bailiff visit" & vbCrLf & vbCrLf ' added TS 10/Jun/2014 23529
                    End If

                    Else
                        CaseDetail &= "Case already paid on" & vbCrLf
                    End If

                CaseDetail &= "CASE - END PROCESSING " & PIFCase("CaseID") & vbCrLf & vbCrLf
                rtxtAudit.AppendText(CaseDetail)

                Dim Para As New Paragraph(CaseDetail)
                Para.Font.Size = 6
                pdfDoc.Add(Para)

                prgMain.Value += 1

            Next PIFCase

            If Remuneration.PIFCases.Rows.Count > 0 Then pdfDoc.Close()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    'Private Sub CalculateArrangement()

    '    Dim CaseDetail As String = ""
    '    Dim DaysBetweenAllocationAndPayment As Integer = 0

    '    lblProgress.Text = "Importing arrangements"
    '    lblProgress.Visible = True
    '    Application.DoEvents()
    '    Remuneration.ImportArrangement(dtpCalcMonth.Value, dtpCalcMonth.Value.AddMonths(1).AddDays(-1))
    '    lblProgress.Visible = False

    '    Dim pdfDoc As New Document()
    '    pdfDoc.Open()
    '    Dim pdfWrite As PdfWriter = PdfWriter.GetInstance(pdfDoc, New FileStream(AuditFile & "Arrangement_audit_" & InvoiceDate.ToString("yyyyMMdd") & ".pdf", FileMode.Append))
    '    Dim ev As New itsEvents
    '    pdfWrite.PageEvent = ev
    '    pdfDoc.Open()

    '    prgMain.Value = 0
    '    prgMain.Maximum = Remuneration.ArrangementCases.Rows.Count

    '    For Each ArrangementCase As DataRow In Remuneration.ArrangementCases.Rows

    '        Payment = 0
    '        Vat = 0

    '        Application.DoEvents()

    '        CaseDetail = "CASE - START PROCESSING CaseId " & ArrangementCase("CaseID") & vbCrLf
    '        CaseDetail &= "Bailiff: " & ArrangementCase("Surname") & ", " & ArrangementCase("Forename") & "[" & ArrangementCase("BailiffID") & "]" & vbCrLf
    '        CaseDetail &= "Arrangement set on " & CDate(ArrangementCase("ArrangementDate")).ToShortDateString & vbCrLf

    '        If IsDBNull(ArrangementCase("BrokenDate")) OrElse ArrangementCase("ArrangementDate") > ArrangementCase("BrokenDate") Then
    '            If ArrangementCase("BailiffID") = ArrangementCase("LastBailiffID") Then
    '                If ArrangementCase("EnforcementFees") > 0 Then
    '                    If Not IsDBNull(ArrangementCase("VisitStageSort")) AndAlso ArrangementCase("VisitStageSort") = "550" Then
    '                        DaysBetweenAllocationAndPayment = DateDiff("d", CDate(ArrangementCase("AllocationDate")).ToShortDateString, CDate(ArrangementCase("LastPayment")).ToShortDateString)
    '                        CaseDetail &= "No. of days from allocation to case return date = " & DaysBetweenAllocationAndPayment.ToString & vbCrLf

    '                        Select Case DaysBetweenAllocationAndPayment
    '                            Case Is <= SecondEnforcementAmount1Cutoff
    '                                CaseDetail &= "Covered by PIF rule" & vbCrLf
    '                            Case Is <= SecondEnforcementAmount2Cutoff
    '                                CaseDetail &= "Payment of " & CDec(ArrangementCase("Amount1")).ToString("0.00") & vbCrLf
    '                                Payment = ArrangementCase("Amount1")
    '                            Case Else
    '                                CaseDetail &= "Payment of " & ArrangementCase("ArrangementPayment").ToString("0.00") & vbCrLf
    '                                Payment = ArrangementCase("ArrangementPayment")
    '                        End Select

    '                        If Not IsDBNull(ArrangementCase("VatReg")) AndAlso ArrangementCase("VatReg").ToString.Length > 0 Then Vat = Payment * VatRate

    '                        If Payment > 0 Then Remuneration.AddBailiffPayment(ArrangementCase("CaseId"), _
    '                                                                            ArrangementCase("BailiffId"), _
    '                                                                            ArrangementCase("ClientId"), _
    '                                                                            ArrangementCase("WorkTypeId"), _
    '                                                                            ArrangementCase("VisitID"), _
    '                                                                            ArrangementCase("VisitDate"), _
    '                                                                            If(IsDBNull(ArrangementCase("AllocationDate")), CDate("01-01-1900"), ArrangementCase("AllocationDate")), _
    '                                                                            InvoiceDate, _
    '                                                                            "ARR" & InvoiceDate.ToString("yyyyMMdd") & ArrangementCase("BailiffId").ToString, _
    '                                                                            InvoiceDate, _
    '                                                                            Payment, _
    '                                                                            0, _
    '                                                                            0, _
    '                                                                            0, _
    '                                                                            Vat, _
    '                                                                            dtpCalcMonth.Value, _
    '                                                                            dtpCalcMonth.Value.AddMonths(1).AddDays(-1), _
    '                                                                            If(IsDBNull(ArrangementCase("LoginName")), "", ArrangementCase("LoginName")), _
    '                                                                            "Arrangement")
    '                    Else
    '                        CaseDetail &= "Visit not made at a valid stage" & vbCrLf & vbCrLf
    '                    End If
    '                Else
    '                    CaseDetail &= "No enforcement fees on case" & vbCrLf
    '                End If
    '            Else
    '                CaseDetail &= "Different bailiff allocated since arrangement set - " & ArrangementCase("LastSurname") & ", " & ArrangementCase("LastForename") & "[" & ArrangementCase("LastBailiffID") & "]" & vbCrLf
    '                End If
    '        Else
    '            CaseDetail &= "Arrangement broken on " & CDate(ArrangementCase("BrokenDate")).ToShortDateString & vbCrLf
    '        End If


    '        CaseDetail &= "CASE - END PROCESSING " & ArrangementCase("CaseID") & vbCrLf & vbCrLf
    '            rtxtAudit.AppendText(CaseDetail)

    '            Dim Para As New Paragraph(CaseDetail)
    '            Para.Font.Size = 6
    '            pdfDoc.Add(Para)

    '            prgMain.Value += 1
    '    Next ArrangementCase

    '    pdfDoc.Close()
    'End Sub

    'Private Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.Click

    '    Dim cryRpt As New ReportDocument

    '    'http://stackoverflow.com/questions/14335966/how-to-programatically-export-crystal-report-as-pdf-in-vb-net

    '    'http://www.vbforums.com/showthread.php?261568-Crystal-Report-RecordSelectionFormula

    '    cryRpt.Load("H:\BailiffRemuneration\BailiffPayments\BailiffInvoices.rpt")
    '    cryRpt.SetDatabaseLogon("crystal", "latsyrc", "onestepdb-rep", "DebtRecovery")



    '    Try
    '        Dim CrExportOptions As ExportOptions
    '        Dim CrDiskFileDestinationOptions As New DiskFileDestinationOptions()
    '        Dim CrFormatTypeOptions As New PdfRtfWordFormatOptions()
    '        CrDiskFileDestinationOptions.DiskFileName = "C:\crystalExport.pdf"
    '        CrExportOptions = cryRpt.ExportOptions
    '        With CrExportOptions
    '            .ExportDestinationType = ExportDestinationType.DiskFile
    '            .ExportFormatType = ExportFormatType.PortableDocFormat
    '            .DestinationOptions = CrDiskFileDestinationOptions
    '            .FormatOptions = CrFormatTypeOptions
    '        End With

    '        cryRpt.Export()


    '    Catch ex As Exception
    '        MsgBox(ex.ToString)
    '    End Try
    'End Sub


    Private Sub btnImportTriggers_Click(sender As Object, e As System.EventArgs) Handles btnImportTriggers.Click
        Try
            If Not MsgBox("This will  overwrite existing values. Proceed?", MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton2, Me.Text) = MsgBoxResult.Ok Then Return

            Dim FileDialog As New OpenFileDialog
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            Dim FileContents(,) As String

            For Each Worksheet As String In {"FC Quarterly", "Van Quarterly"}
                FileContents = InputFromExcel(FileDialog.FileName, Worksheet)
                prgMain.Minimum = 8
                prgMain.Maximum = UBound(FileContents)
                prgMain.Value = prgMain.Minimum

                For RowCount As Integer = 8 To UBound(FileContents)
                    prgMain.Value = RowCount
                    Application.DoEvents()

                    If String.IsNullOrEmpty(FileContents(RowCount, 0)) Then Continue For

                    If Not IsNumeric(FileContents(RowCount, 0)) Then
                        MsgBox("Invalid bailiffID on line " & (RowCount + 1).ToString, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, Me.Text)
                        Continue For
                    End If

                    If Not {"Y", "N"}.Contains(FileContents(RowCount, 5)) Then
                        MsgBox("Invalid turnaround flag on line " & (RowCount + 1).ToString, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, Me.Text)
                        Continue For
                    End If

                    If Not {"Y", "N"}.Contains(FileContents(RowCount, 10)) Then
                        MsgBox("Invalid complaints flag on line " & (RowCount + 1).ToString, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, Me.Text)
                        Continue For
                    End If

                    If Not {"Y", "N"}.Contains(FileContents(RowCount, 15)) Then
                        MsgBox("Invalid field admin flag on line " & (RowCount + 1).ToString, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, Me.Text)
                        Continue For
                    End If

                    If Not {"Y", "N"}.Contains(FileContents(RowCount, 20)) Then
                        MsgBox("Invalid BWV flag on line " & (RowCount + 1).ToString, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, Me.Text)
                        Continue For
                    End If

                    If Worksheet = "Van Quarterly" AndAlso Not {"Y", "N"}.Contains(FileContents(RowCount, 25)) Then
                        MsgBox("Invalid visited flag on line " & (RowCount + 1).ToString, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, Me.Text)
                        Continue For
                    End If

                    Remuneration.UpdateBailiffPerformanceTriggers(FileContents(RowCount, 0), FileContents(RowCount, 5), FileContents(RowCount, 10), FileContents(RowCount, 15), FileContents(RowCount, 20), If(Worksheet = "Van Quarterly", FileContents(RowCount, 25), ""), If(Worksheet = "Van Quarterly", FileContents(RowCount, 28), 0))

                Next RowCount
            Next Worksheet

            prgMain.Value = prgMain.Minimum

            MsgBox("Import complete", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, Me.Text)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnGenerateInvoices_Click(sender As Object, e As System.EventArgs) Handles btnGenerateInvoices.Click
        Try

            Dim FolderDialog As New FolderBrowserDialog
            If Not FolderDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            If System.IO.File.Exists(Path.Combine(Application.StartupPath, "BailiffInvoices.rpt")) = True Then

                lblProgress.Visible = True

                For Each InvoiceType As String In {"PIF", "COM", "FV", "CGA"}

                    lblProgress.Text = "Generating " & InvoiceType & " invoice"

                    Application.DoEvents()

                    Dim customerReport As New ReportDocument()

                    Dim reportPath As String = Path.Combine(Application.StartupPath, "BailiffInvoices.rpt")
                    With customerReport
                        .Load(reportPath)
                        .SetDatabaseLogon("crystal", "latsyrc", "onestepdb-rep", "DebtRecovery")
                        .DataDefinition.RecordSelectionFormula = "{BailiffInvoice.InvoiceType} = '" & InvoiceType & "' AND {BailiffInvoice.StartPeriod} = DATE(" & dtpCalcMonth.Value.ToString("yyyy,MM,dd") & ") AND {BailiffInvoice.EndPeriod} = DATE(" & dtpCalcMonth.Value.AddMonths(1).AddDays(-1).ToString("yyyy,MM,dd") & ")"
                        .Subreports(0).DataDefinition.RecordSelectionFormula = "{BailiffInvoice.InvoiceType} = '" & InvoiceType & "' AND {BailiffInvoice.StartPeriod} = DATE(" & dtpCalcMonth.Value.ToString("yyyy,MM,dd") & ") AND {BailiffInvoice.EndPeriod} = DATE(" & dtpCalcMonth.Value.AddMonths(1).AddDays(-1).ToString("yyyy,MM,dd") & ")"
                        .ExportToDisk(ExportFormatType.PortableDocFormat, Path.Combine(FolderDialog.SelectedPath, InvoiceType & "_BailiffInvoices" & InvoiceDate.ToString("yyyyMMdd") & ".pdf"))
                    End With

                Next InvoiceType

                lblProgress.Visible = False

                MsgBox("Invoices generated", MsgBoxStyle.OkOnly, Me.Text)

            Else
                MessageBox.Show("Invoice report was not found in the following folder: " & vbCrLf & Application.StartupPath)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub GenerateSageExtract(FileName As String, InvoiceType As String, BranchID As Integer)

        Remuneration.GetSageExtract(dtpCalcMonth.Value, dtpCalcMonth.Value.AddMonths(1).AddDays(-1), InvoiceType, BranchID)

        Using sw As New StreamWriter(FileName, False)

            For Each Row As DataRow In Remuneration.SageExtract.Rows

                Dim sb As New System.Text.StringBuilder
                For ColIdx As Integer = 0 To Remuneration.SageExtract.Columns.Count - 1

                    If ColIdx <> 2 And ColIdx <> 15 Then sb.Append("""")

                    If ColIdx <> 4 Then
                        sb.Append(Row.Item(ColIdx).ToString)
                    Else
                        sb.Append(CDate(Row.Item(ColIdx)).ToString("dd/MM/yyyy"))
                    End If

                    If ColIdx <> 2 And ColIdx <> 15 Then sb.Append("""")

                    If ColIdx < Remuneration.SageExtract.Columns.Count - 1 Then sb.Append(",")
                Next
                sw.WriteLine(sb.ToString)
            Next
        End Using
    End Sub

    Private Sub btnSage_Click(sender As Object, e As System.EventArgs) Handles btnSage.Click
        Try
            Dim FolderDialog As New FolderBrowserDialog

            If Not FolderDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            GenerateSageExtract(FolderDialog.SelectedPath & "\Rossendales_FirstVisit_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "FV", 1)
            GenerateSageExtract(FolderDialog.SelectedPath & "\Rossendales_RecoveredComplianceFee_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "COM", 1)
            GenerateSageExtract(FolderDialog.SelectedPath & "\Rossendales_PIF_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "PIF", 1)
            GenerateSageExtract(FolderDialog.SelectedPath & "\Rossendales_CGA_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "CGA", 1)
            'The following added TS 17/Jun/2016
            GenerateSageExtract(FolderDialog.SelectedPath & "\Rossendales_MHRVisit_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "MHR", 1)
            GenerateSageExtract(FolderDialog.SelectedPath & "\Rossendales_CollectPayment_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "COL", 1)

            ' The following added TS 14/Jul/2017
            'GenerateSageExtract(FolderDialog.SelectedPath & "\Rossendales_CGAPIF_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "CGAPIF")

            ' The following added TS 16/Aug/2017
            GenerateSageExtract(FolderDialog.SelectedPath & "\Rossendales_SF_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "SF", 1)
            GenerateSageExtract(FolderDialog.SelectedPath & "\Marston_FirstVisit_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "FV", 10)
            GenerateSageExtract(FolderDialog.SelectedPath & "\Marston_RecoveredComplianceFee_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "COM", 10)
            GenerateSageExtract(FolderDialog.SelectedPath & "\Marston_PIF_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "PIF", 10)
            GenerateSageExtract(FolderDialog.SelectedPath & "\Marston_CGA_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "CGA", 10)

            ' The following added TS 01/Nov/2017
            GenerateSageExtract(FolderDialog.SelectedPath & "\Swift_FirstVisit_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "FV", 24)
            GenerateSageExtract(FolderDialog.SelectedPath & "\Swift_RecoveredComplianceFee_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "COM", 24)
            GenerateSageExtract(FolderDialog.SelectedPath & "\Swift_PIF_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "PIF", 24)
            GenerateSageExtract(FolderDialog.SelectedPath & "\Swift_CGA_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "CGA", 24)

            ' The following added TS 05/Dec/2017
            GenerateSageExtract(FolderDialog.SelectedPath & "\Marston_SF_SageX3_" & InvoiceDate.ToString("yyyyMMdd") & ".csv", "SF", 10)

            MsgBox("Sage extracts generated", MsgBoxStyle.OkOnly, Me.Text)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

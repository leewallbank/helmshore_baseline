﻿Imports System.Xml.Schema
Imports System.IO
Imports commonlibrary

Public Class EON
    Dim xml_valid As Boolean = True
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim new_file As String = ""
    Dim CSID As Integer
    Dim prod_run As Boolean = False
    Dim filename, errorFileName, errorfile As String

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If
    End Sub

    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub processbtn_Click(sender As System.Object, e As System.EventArgs) Handles processbtn.Click
        With OpenFileDialog1
            .Title = "Read XML file"
            .Filter = "XML file|*.xml"
            .FileName = ""
            .CheckFileExists = True
        End With
        Dim xml_dataSet As New DataSet()
        Dim doc As New Xml.XmlDocument()

        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("File not opened")
            Me.Close()
            Exit Sub
        End If
        Dim InputFilePath As String = Path.GetDirectoryName(OpenFileDialog1.FileName)
        InputFilePath &= "\"
        filename = Path.GetFileNameWithoutExtension(OpenFileDialog1.FileName)
        errorFileName = InputFilePath & filename & "_EON_QUERY_error.txt"
        validate_xml_file()
        If xml_valid = False Then
            MsgBox("XML file has failed validation - see error file")
            If MsgBox("Do you want to continue processing the file?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Me.Close()
                Exit Sub
            End If
        End If
        Dim queryFile As String = "Client Ref,Hold_Code,Notes" & vbNewLine
        Dim fieldValue As String
        Dim rdr_name As String = ""
        Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
        reader.Read()
        ProgressBar1.Value = 5
        Dim record_count As Integer = 0
        Dim clientID As Integer = 1938
        If prod_run = False Then
            MsgBox("TEST RUN ONLY")
            clientID = 24
        End If
        Dim errorFound As Boolean = False

        While (reader.ReadState <> Xml.ReadState.EndOfFile)
            If reader.NodeType > 1 Then
                reader.Read()
                Continue While
            End If
            Try
                rdr_name = reader.Name
            Catch
                Continue While
            End Try

            'Note ReadElementContentAsString moves focus to next element so don't need read
            ProgressBar1.Value = record_count
            Application.DoEvents()

            Select Case rdr_name
                Case "HOLD"
                    reader.Read()
                    While reader.Name <> "HOLD"
                        rdr_name = reader.Name
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        rdr_name = reader.Name
                        Select Case rdr_name
                            Case "DEBT"
                                Dim clientRef As String = ""
                                Dim holdCode As String = ""
                                Dim notes As String = ""
                                reader.Read()
                                While reader.Name <> "DEBT"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    rdr_name = reader.Name
                                    Select Case rdr_name
                                        Case "CLIENT_REFERENCE"
                                            fieldValue = reader.ReadElementContentAsString
                                            clientRef = fieldValue
                                        Case "HOLD_CODE"
                                            fieldValue = reader.ReadElementContentAsString
                                            holdCode = fieldValue
                                        Case "NOTES"
                                            fieldValue = reader.ReadElementContentAsString
                                            notes = fieldValue
                                        Case Else
                                            reader.Read()
                                    End Select
                                End While
                                queryFile &= clientRef & "," & holdCode & "," & notes & vbNewLine
                            Case Else
                                'MsgBox("What is this tag?" & rdr_name)
                                reader.Read()
                        End Select
                    End While
            End Select
        End While

        filename = InputFilePath & "Query_" & Format(Now, "ddMMyyyy") & ".txt"
        My.Computer.FileSystem.WriteAllText(filename, queryFile, False)
        MsgBox("Query report saved")

        Me.Close()
    End Sub
    Private Sub validate_xml_file()
        'open file as text first to remove any £ signs
        Dim txt_doc As String
        txt_doc = My.Computer.FileSystem.ReadAllText(OpenFileDialog1.FileName)
        txt_doc = Replace(txt_doc, "£", "")
        My.Computer.FileSystem.WriteAllText(OpenFileDialog1.FileName, txt_doc, False)
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(OpenFileDialog1.FileName)
        Dim XSDFile As String = "R:\vb.net\EON XSD\EON QUERY.xsd"
        Try
            myDocument.Schemas.Add("", XSDFile)
        Catch ex As Exception
            errorfile = "File: " & OpenFileDialog1.FileName & "has failed validation with XSD" & vbNewLine
            errorfile &= "XSD file: " & XSDFile & vbNewLine
            errorfile &= "Reason: " & ex.ToString
            My.Computer.FileSystem.WriteAllText(errorFileName, errorfile, False)
            xml_valid = False
            Exit Sub
        End Try

        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
    End Sub
    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_error("Error: " & e.Message)
            Case XmlSeverityType.Warning
                write_error("Warning " & e.Message)
        End Select
    End Sub
    Private Sub write_error(ByVal error_message As String)
        Static Dim error_no As Integer
        error_no += 1
        error_message = error_message & vbNewLine
        If error_no = 1 Then
            My.Computer.FileSystem.WriteAllText(errorFileName, error_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(errorFileName, error_message, True)
        End If

    End Sub
    Private Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, Optional ByVal NoteLength As Integer = 250) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 2) ' + 2 is for :;
        Dim PreviousLength As Integer
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = (NoteTag & ":" & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)) & ";").PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function
    Private Function getCaseID(ByVal clientRef As String) As Integer
        Dim debtorID As Integer = 0
        debtorID = GetSQLResults("DebtRecovery", "SELECT D._rowID " & _
                                                   "FROM debtor D, clientscheme CS  " & _
                                                   "WHERE client_ref = '" & clientRef & "'" & _
                                                   " AND D.clientschemeID = CS._rowID " & _
                                                   " AND CS.clientID = 1662")

        Return debtorID
    End Function
End Class

﻿Imports System.IO
Imports System.Text
Imports CommonLibrary

Public Class frmMain

    Private AddressData As New clsAddressData
    Private SourceRow As Integer, SourceCol As Integer
    Private LoadedFile As String = Nothing
    Private SystemColumns As String() = {"Exclude", "Scheme name", "ProductCode", "DebtorID", "NoteID", "DebtorContactID"}

    Public Sub New()
        Try
            ' This call is required by the designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call.

            If Now.DayOfWeek = DayOfWeek.Monday Then
                dtpModifiedDateFrom.Value = DateAdd(DateInterval.Day, -2, Now)
                dtpModifiedDateTo.Value = DateAdd(DateInterval.Day, -1, Now)
            Else
                dtpModifiedDateFrom.Value = DateAdd(DateInterval.Day, -1, Now)
                dtpModifiedDateTo.Value = dtpModifiedDateFrom.Value
            End If

            cmsDGV.Items.Add("Cut")
            cmsDGV.Items.Add("Copy")
            cmsDGV.Items.Add("Paste")

            cboTarget.Text = "SLC"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCreateFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCreateFile.Click
        Try
            If cboTarget.Text = "SLC" Then CreateSLCReturnFile()
            If cboTarget.Text = "CCI" Then CreateCCIFile()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub CreateSLCReturnFile()
        ' A fixed width file per scheme
        Try
            If Not DataValid() Then
                MsgBox("Invalid data.", vbOKOnly + vbCritical, Me.Text)
                Return
            End If

            Dim Line As String = "", Filename As String = "", FileDate As String, DataItem As String
            Dim FolderBrowserDialog As New FolderBrowserDialog
            Dim FileCount As Integer = 0

            FileDate = "_" & DateTime.Today.ToString("ddMMyyyy") & ""

            If FolderBrowserDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then

                dgvMain.EndEdit()
                dgvMain.Sort(dgvMain.Columns("ProductCode"), System.ComponentModel.ListSortDirection.Ascending)

                For Each DataRow As DataGridViewRow In dgvMain.Rows
                    If DataRow.Cells("Exclude").Value = True Then Continue For ' added TS 21/Mar/2013 Portal task ref 14709

                    If Filename <> "" And Filename <> DataRow.Cells("ProductCode").Value Then
                        WriteFile(FolderBrowserDialog.SelectedPath & "\" & Filename & FileDate & ".txt", Line)
                        FileCount += 1
                        Line = ""
                    End If

                    If Line <> "" Then Line &= (vbCrLf) ' Do this at the start of the loop as the last line must not have a CrLf after it

                    For Each Cell As DataGridViewCell In DataRow.Cells
                        If Not SystemColumns.Contains(AddressData.OutputDataView.Table.Columns(Cell.ColumnIndex).ColumnName) Then ' Changed to SystemColumns added TS 21/Mar/2013 Portal task ref 14709
                            If Not IsDBNull(Cell.Value) Then
                                DataItem = UCase(Cell.Value)
                            Else
                                DataItem = ""
                            End If

                            ' Take spaces out of phone numbers
                            If {"CustomerHomePhone", "CustomerMobilePhone", "CustomerWorkPhone"}.Contains(AddressData.OutputDataView.Table.Columns(Cell.ColumnIndex).ColumnName) Then DataItem = DataItem.ToString.Replace(" ", "")

                            Line &= DataItem.ToString.PadRight(AddressData.OutputDataView.Table.Columns(Cell.ColumnIndex).ExtendedProperties("MaxLength"))
                        End If
                    Next Cell

                    Filename = DataRow.Cells("ProductCode").Value

                Next DataRow

                If Line <> "" Then
                    If LoadedFile Is Nothing Then
                        WriteFile(FolderBrowserDialog.SelectedPath & "\" & Filename & FileDate & ".txt", Line)
                    Else
                        WriteFile(FolderBrowserDialog.SelectedPath & "\" & LoadedFile, Line)
                    End If

                    FileCount += 1
                End If

                AddressData.LogTraceCases()

                MsgBox(FileCount.ToString & " file" & If(FileCount = 1, "", "s") & " saved", vbOKOnly + vbInformation, Me.Text)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub CreateCCIFile()
        ' This is slightly different to the SLC file as there is to be one file, the filename is different, debtorid is to be included and the file is pipe separated rather than fixed width
        Try

            If Not DataValid() Then
                MsgBox("Invalid data.", vbOKOnly + vbCritical, Me.Text)
                Return
            End If

            Dim WarningCases As New ArrayList
            Dim Line As String = "", Filename As String, Filedate As String, ProductCode As String = "", DataItem As String
            Dim FolderBrowserDialog As New FolderBrowserDialog
            Dim FileCount As Integer = 0

            Filename = "RossendalesContactDetails_"
            Filedate = "_" & DateTime.Today.ToString("yyyyMMdd") & ".txt"

            If FolderBrowserDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then

                For Each DataRow As DataGridViewRow In dgvMain.Rows
                    If ProductCode <> "" And ProductCode <> DataRow.Cells("ProductCode").Value Then
                        WriteFile(FolderBrowserDialog.SelectedPath & "\" & Filename & ProductCode & Filedate, Line)
                        FileCount += 1
                        Line = ""
                    End If

                    If Line <> "" Then Line &= (vbCrLf) ' Do this at the start of the loop as the last line must not have a CrLf after it

                    For Each Cell As DataGridViewCell In DataRow.Cells
                        If Not SystemColumns.Contains(AddressData.OutputDataView.Table.Columns(Cell.ColumnIndex).ColumnName) Then ' Changed to SystemColumns TS 21/Mar/2013 Portal task ref 14709
                            If Not IsDBNull(Cell.Value) Then
                                DataItem = UCase(Cell.Value)
                            Else
                                DataItem = ""
                            End If

                            ' Take spaces out of phone numbers
                            If {"CustomerHomePhone", "CustomerMobilePhone", "CustomerWorkPhone"}.Contains(AddressData.OutputDataView.Table.Columns(Cell.ColumnIndex).ColumnName) Then DataItem = DataItem.ToString.Replace(" ", "")

                            Line &= DataItem

                            If Cell.ColumnIndex < DataRow.Cells.Count - 1 Then Line &= "|" ' If the column is not the last add a pipe

                        End If
                    Next Cell

                    ProductCode = DataRow.Cells("ProductCode").Value

                    If AddressData.OutputExists(DataRow.Cells("DebtorID").Value, DataRow.Cells("NoteID").Value & "," & DataRow.Cells("DebtorContactID").Value) Then
                        If Not WarningCases.Contains(DataRow.Cells("DebtorID").Value) Then WarningCases.Add(DataRow.Cells("DebtorID").Value) ' Check so we don't load duplicates
                    End If

                    AddressData.LogOutput(DataRow.Cells("DebtorID").Value, DataRow.Cells("NoteID").Value & "," & DataRow.Cells("DebtorContactID").Value)

                Next DataRow

                If Line <> "" Then
                    WriteFile(FolderBrowserDialog.SelectedPath & "\" & Filename & ProductCode & Filedate, Line)
                    FileCount += 1
                End If

                MsgBox(FileCount.ToString & " file" & If(FileCount = 1, "", "s") & " saved", vbOKOnly + vbInformation, Me.Text)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    'Private Sub WriteFile(ByVal FileName As String, ByVal Line As String)
    '    Using Writer As StreamWriter = New StreamWriter(FileName)
    '        Writer.Write(Line)
    '    End Using
    'End Sub

    Private Sub dgvMain_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMain.CellValueChanged
        Try
            If Not IsDBNull(dgvMain(e.ColumnIndex, e.RowIndex).Value) And dgvMain.Columns(e.ColumnIndex).Name <> "Exclude" Then ' column name check added TS 21/Mar/2013 Portal task ref 14709
                dgvMain(e.ColumnIndex, e.RowIndex).Value = LTrim(dgvMain(e.ColumnIndex, e.RowIndex).Value)
                'If dgvMain.Columns(e.ColumnIndex).Name = "Success Code" And dgvMain(e.ColumnIndex, e.RowIndex).Value = "NEG" Then
                '    dgvMain("ClosureCode", e.RowIndex).Value = "G/A"
                'Else
                '    dgvMain("ClosureCode", e.RowIndex).Value = ""
                'End If
            End If
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMain_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvMain.MouseDown
        Try
            Dim p As Point = dgvMain.PointToClient(Control.MousePosition)
            Dim hit As DataGridView.HitTestInfo = dgvMain.HitTest(p.X, p.Y)

            SourceRow = hit.RowIndex
            SourceCol = hit.ColumnIndex

            If hit.Type = DataGridViewHitTestType.Cell And e.Button = MouseButtons.Left Then
                dgvMain.BeginEdit(False)
                If Not dgvMain(hit.ColumnIndex, hit.RowIndex).IsInEditMode Then
                    dgvMain.DoDragDrop(dgvMain(hit.ColumnIndex, hit.RowIndex).Value, DragDropEffects.Copy Or DragDropEffects.Move)
                End If
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMain_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvMain.DragEnter
        Try
            If (e.Data.GetDataPresent(DataFormats.Text)) Then
                e.Effect = DragDropEffects.Copy
            Else
                e.Effect = DragDropEffects.None
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMain_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvMain.DragDrop
        Try
            Dim p As System.Drawing.Point = dgvMain.PointToClient(Control.MousePosition)
            Dim hit As DataGridView.HitTestInfo = dgvMain.HitTest(p.X, p.Y)

            If hit.Type = DataGridViewHitTestType.Cell AndAlso SourceRow = hit.RowIndex AndAlso Not dgvMain(hit.ColumnIndex, hit.RowIndex).ReadOnly Then ' Check rows as we do not want data moving from one case to another
                dgvMain(SourceCol, SourceRow).Value = ""
                ' if drop position is in the left hand half of the cell, insert data before the existing contents, else insert after
                If p.X < dgvMain.GetCellDisplayRectangle(hit.ColumnIndex, hit.RowIndex, False).Left + (dgvMain.Columns(hit.ColumnIndex).Width / 2) Then
                    dgvMain(hit.ColumnIndex, hit.RowIndex).Value = e.Data.GetData(DataFormats.Text).ToString & dgvMain(hit.ColumnIndex, hit.RowIndex).Value
                Else
                    dgvMain(hit.ColumnIndex, hit.RowIndex).Value &= e.Data.GetData(DataFormats.Text).ToString
                End If
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        Try
            If DataValid() Then MsgBox("Data is valid.", vbOKOnly + vbInformation, Me.Text)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function DataValid() As Boolean
        Dim ColLoopCount As Integer, RowLoopCount As Integer, MaxSize As Integer

        DataValid = True

        Try
            dgvMain.EndEdit()

            For Each DataColumn As DataGridViewColumn In dgvMain.Columns ' Changed from DataGridViewTextBoxColumn TS 21/Mar/2013 Portal task ref 14709
                If Not SystemColumns.Contains(DataColumn.Name) Then  ' Changed to SystemColumns TS 21/Mar/2013 Portal task ref 14709
                    ColLoopCount = DataColumn.Index

                    MaxSize = AddressData.OutputDataView.Table.Columns(ColLoopCount).ExtendedProperties("MaxLength")
                    For RowLoopCount = 0 To dgvMain.RowCount - 1
                        With dgvMain(ColLoopCount, RowLoopCount)
                            Select Case dgvMain.Columns(ColLoopCount).Name
                                Case "Scheme name"
                                    If .Value = AddressData.ErrFileName Then
                                        .Style.BackColor = Color.Red
                                        DataValid = False
                                    Else
                                        .Style.BackColor = Color.White
                                    End If
                                Case "DOB"
                                    Dim DateTest As DateTime
                                    If .Value <> "" And (.Value.ToString.Length > MaxSize Or Date.TryParseExact(.Value, "ddMMyyyy", Globalization.CultureInfo.CurrentCulture, Globalization.DateTimeStyles.None, DateTest) = False) Then
                                        .Style.BackColor = Color.Red
                                        DataValid = False
                                    Else
                                        .Style.BackColor = Color.White
                                    End If
                                    'Case "CustomerHomePhone"
                                    '    If Not IsDBNull(.Value) AndAlso .Value.ToString.Length > 1 AndAlso Strings.Left(.Value, 2) = "07" Then
                                    '        .Style.BackColor = Color.Red
                                    '        DataValid = False
                                    '    Else
                                    '        .Style.BackColor = Color.White
                                    '    End If
                                Case Else ' The majority of fields are validate by length only
                                    If .Value.ToString.Length > MaxSize Then
                                        .Style.BackColor = Color.Red
                                        DataValid = False
                                    Else
                                        .Style.BackColor = Color.White
                                    End If
                            End Select
                        End With
                    Next RowLoopCount
                End If ' Column not in system columns
            Next DataColumn

            If Not DataValid Then MsgBox("Invalid data.", vbOKOnly + vbCritical, Me.Text)

        Catch ex As Exception
            HandleException(ex)
        End Try

        Return DataValid
    End Function

    Private Sub frmMain_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            '    DataValid()
            RefreshData()
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dtpModifiedDate_CloseUp(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpModifiedDateFrom.CloseUp
        RefreshData()
    End Sub

    Private Sub cmdRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        RefreshData()
    End Sub

    Private Sub RefreshData()
        Try
            Me.Cursor = Cursors.WaitCursor

            LoadedFile = Nothing

            AddressData.GetData(dtpModifiedDateFrom.Value, dtpModifiedDateTo.Value, If(cboTarget.Text = "CCI", True, False))

            With dgvMain

                .DataSource = AddressData.OutputDataView

                For Each DataColumn As DataGridViewColumn In .Columns ' Changed from DataGridViewTextBoxColumn TS 21/Mar/2013 Portal task ref 14709
                    DataColumn.SortMode = DataGridViewColumnSortMode.NotSortable
                Next DataColumn

                .Columns("Exclude").Width = 50 ' added TS 21/Mar/2013 Portal task ref 14709
                .Columns("Scheme name").ReadOnly = True
                .Columns("NoteID").Visible = False
                .Columns("DebtorID").ReadOnly = True
                .Columns("ProductCode").Visible = False
                .Columns("FirstLoanPack").ReadOnly = True
                .Columns("PayInBookReference").ReadOnly = True
                .Columns("CustomerFullName").ReadOnly = True
                .Columns("DOB").ReadOnly = True
                .Columns("SuccessCode").ReadOnly = True
                .Columns("DebtorContactID").Visible = False
              
            End With

            DataValid()

            Me.Cursor = Cursors.Default
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsDGV_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsDGV.ItemClicked
        Try
            Dim ClickCell As DataGridViewCell = dgvMain.SelectedCells(0)
            Select Case e.ClickedItem.Text
                Case "Cut"
                    If Not ClickCell.ReadOnly Then
                        Clipboard.SetText(ClickCell.Value)
                        ClickCell.Value = ""
                    End If
                Case "Copy"
                    Clipboard.SetText(ClickCell.Value, TextDataFormat.Text)
                Case "Paste"
                    If Not ClickCell.ReadOnly Then ClickCell.Value = Clipboard.GetText(TextDataFormat.Text)
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If dgvMain.SelectedCells.Count = 0 Then Return

        Try
            Dim ClickCell As DataGridViewCell = dgvMain.SelectedCells(0)
            If e.Control Then
                Select Case e.KeyCode
                    Case Keys.C
                        Clipboard.SetText(ClickCell.Value, TextDataFormat.Text)
                    Case Keys.V
                        If Not ClickCell.ReadOnly Then ClickCell.Value = Clipboard.GetText(TextDataFormat.Text)
                    Case Keys.X
                        If Not ClickCell.ReadOnly Then
                            Clipboard.SetText(ClickCell.Value)
                            ClickCell.Value = ""
                        End If
                End Select
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMain_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvMain.MouseUp
        Try
            If e.Button = MouseButtons.Right Then cmsDGV.Show(sender, e.Location)
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLoadFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLoadFile.Click
        Dim FileDialog As New OpenFileDialog

        Try
            FileDialog.ShowDialog()
            AddressData.LoadFromFile(FileDialog.FileName)

            LoadedFile = Path.GetFileName(FileDialog.FileName)

            DataValid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    'Private Sub cboTarget_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboTarget.SelectedValueChanged
    '    RefreshData()
    'End Sub
End Class
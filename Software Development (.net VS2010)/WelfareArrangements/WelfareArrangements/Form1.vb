Public Class Form1
    Dim upd_txt As String
    Dim start_debtor As Integer
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        
        Try
            'delete rows from table
            upd_txt = "delete from WelfareArrangements " & _
                " where w_debtorID >0"
            update_sql(upd_txt)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        start_debtor = 0
        process_debtors()
    End Sub
    Private Sub process_debtors()

        Dim start_date As Date = CDate("April 1 2014 00:00:00")
        Dim debtor_dt As New DataTable
        LoadDataTable2("DebtRecovery", "SELECT  D._rowid, debt_amount, debt_costs, status, debt_fees from Debtor D, stage S" & _
        "  where D._rowid > " & start_debtor & _
        " and status_open_closed = 'O'" & _
        " and S.name = 'Welfare Arrangement'" & _
        " and D.last_stageID = S._rowID " & _
        " order by D._rowid", debtor_dt, False)

        Dim debtorID As Integer
        For Each debtRow In debtor_dt.Rows
            Application.DoEvents()
            debtorID = debtRow(0)
            Dim created_date As Date
            'get welfare arrangement start date
            created_date = GetSQLResults2("DebtRecovery", "select max(_createdDate) " & _
                                " from note " & _
                                " where DebtorID = " & debtorID & _
                                " and text like  '%to Welfare%'" & _
                                " and type = 'Moved'")

            Dim tot_debt_amt As Decimal = debtRow(1) + debtRow(2) + debtRow(4)

            'has this case got a visit?
            Dim visits As Integer
            visits = GetSQLResults2("DebtRecovery", " select count(_rowid) from Visit where debtorID = " & debtorID & _
            " and date_visited is not null")
            Dim visit_found As Boolean = False
            If visits > 0 Then
                visit_found = True
            End If

            'get payments before allocation
            Dim payment_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select amount, status, split_debt, split_costs, split_fees, split_other, split_van from Payment" & _
                        " where debtorID = " & debtorID & " and date <= '" & Format(created_date, "yyyy-MM-dd") & "'" & _
                        " and status='R'", payment_dt, False)

            Dim paid2client_before As Decimal = 0
            For Each payRow In payment_dt.Rows
                paid2client_before = paid2client_before + payRow(2) + payRow(3) + payRow(4) + payRow(5) + payRow(6)
            Next
            Dim w_no_payments As Integer = 0
            'get payments since allocation 
            Dim payments2_dt As New DataTable
            LoadDataTable2("DebtRecovery", "select amount, status, split_debt, split_costs, split_fees, split_van, date from Payment" & _
            " where debtorID = " & debtorID & _
            " and (status='R' or status='W')", payments2_dt, False)

            Dim to_client As Decimal = 0
            Dim to_fees As Decimal = 0
            Dim waiting As Decimal = 0
            For Each pay2Row In payments2_dt.Rows
                Dim pay_date As Date = pay2Row(6)
                If pay_date < created_date Then
                    Continue For
                End If
                If pay2Row(1) = "R" Then
                    to_client = to_client + pay2Row(2) + pay2Row(3)
                    to_fees = to_fees + pay2Row(4) + pay2Row(5)
                Else
                    waiting += pay2Row(0)
                End If
                w_no_payments += 1
            Next

            
            Dim debt_amt2 As Decimal = (tot_debt_amt - paid2client_before)
            Dim pif As String = "N"
            Dim status As String = debtRow(3)
            If (status = "S" Or status = "F") And to_client > 0 Then
                pif = "Y"
            End If
            Dim visited As String = "N"
            If visit_found Then
                visited = "Y"
            End If
            Try
                upd_txt = "Insert into WelfareArrangements values (" & _
                    debtorID & ",'" & created_date & "'," & to_client & "," & to_fees & "," & waiting & "," & _
                    debt_amt2 & ",'" & visited & "','" & pif & "'," & w_no_payments & ")"
                update_sql(upd_txt)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
        Next
        
        Me.Close()
    End Sub



    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("FeesSQL")
        Try
            'delete rows from table
            upd_txt = "delete from WelfareArrangements " & _
                " where w_debtorID >0"
            update_sql(upd_txt)
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        start_debtor = 0
        process_debtors()
        Me.Close()
    End Sub

    Private Sub restartbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        start_debtor = InputBox("Enter start debtor ID", "Start debtor ID")
        start_debtor -= 1
        process_debtors()
    End Sub
End Class

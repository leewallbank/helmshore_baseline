Public Class mainfrm
    Dim first_letter As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim days As Integer = Weekday(Now)
        date_picker.Value = DateAdd(DateInterval.Day, -days, Now)
        selected_cl_name = ""
    End Sub

    Private Sub retnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles retnbtn.Click
        disable_buttons()
        ProgressBar1.Value = 5
        retn_month = Format(date_picker.Value, "MM_MMM")
        retn_day = Format(date_picker.Value, "dd_ddd")
        Dim files_found As Boolean = False
        Dim returnCases1 As String = ""
        Dim returncases2 As String = ""
        Dim returncases3 As String = ""
        'get all csids for client 1779
        param2 = "select _rowid from clientScheme where clientID = 1779"
        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        Dim cs_rows As Integer = no_of_rows - 1
        Dim idx As Integer
        files_found = False
        For idx = 0 To cs_rows
            selected_csid = cs_ds.Tables(0).Rows(idx).Item(0)
            'get remittance number
            param2 = "select _rowid from Remit where clientschemeID = " & selected_csid & _
            " and date = '" & Format(date_picker.Value, "yyyy-MM-dd") & "'"
            Dim remit_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim remit_no As Integer = remit_dataset.Tables(0).Rows(0).Item(0)
            file_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                                       retn_day & "\Remittances\" & Format(remit_no, "#") & "\"
            Dim file_name, client_ref As String
            'create directories for returns
            Dim dir_name As String = file_path & "Returns 1"
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
            dir_name = file_path & "Returns 2"
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create folder")
                End
            End Try
            Dim debtor, retn_codeid As Integer
            Dim returnFileName As String = file_path & "ReturnedCases.txt"
            'also split ReturnedCases.txt into 2/4 and 6
            Dim FileContents() As String = Nothing
            Dim returnFileFound As Boolean = True
            Try
                FileContents = System.IO.File.ReadAllLines(returnFileName)
            Catch ex As Exception
                returnFileFound = False
            End Try
            Dim count1 As Integer = 0
            Dim count2 As Integer = 0
            Dim value1 As Decimal = 0
            Dim value2 As Decimal = 0
            If returnFileFound Then
                For Each InputLine As String In FileContents
                    'first char is H,D or T for header, detail and trailer
                    Select Case Microsoft.VisualBasic.Left(InputLine, 1)
                        Case "H"
                            returnCases1 = InputLine & vbNewLine
                            returncases2 = InputLine & vbNewLine
                            returncases3 = InputLine & vbNewLine
                        Case "D"
                            Dim linevalue As Decimal = 0
                            Dim valString As String = Mid(InputLine, 99, 11)
                            linevalue = Val(valString)
                            If Mid(InputLine, 98, 1) = "-" Then
                                linevalue = linevalue * -1
                            End If
                            If Mid(InputLine, 44, 2) = "05" Or Mid(InputLine, 44, 2) = "06" Or Mid(InputLine, 44, 2) = "18" Then
                                returnCases1 &= InputLine & vbNewLine
                                count1 += 1
                                value1 += linevalue
                            Else
                                returncases2 &= InputLine & vbNewLine
                                count2 += 1
                                value2 += linevalue
                                'Else
                                '    MsgBox("Invalid return code in Returnedcases.txt for clientschemeID = " & selected_csid)
                                '    Exit For
                            End If
                        Case "T"
                            'write out trailers
                            Dim sign As String = "+"
                            If value1 < 0 Then
                                sign = "-"
                            End If
                            Dim trailer1 As String = "T " & Format(count1, "00000000") & " " & sign & Format(value1, "00000000.00")
                            sign = "+"
                            If value2 < 0 Then
                                sign = "-"
                            End If
                            Dim trailer2 As String = "T " & Format(count2, "00000000") & " " & sign & Format(value2, "00000000.00")
                            sign = "+"
                           
                            returnCases1 &= trailer1 & vbNewLine
                            returncases2 &= trailer2 & vbNewLine
                    End Select
                Next
                'write out return files to approriate folder
                If returnCases1 <> "" Then
                    Dim newFileName As String = file_path & "Returns 1\ReturnedCases1.txt"
                    My.Computer.FileSystem.WriteAllText(newFileName, returnCases1, False)
                End If
                If returncases2 <> "" Then
                    Dim newFileName As String = file_path & "Returns 2\ReturnedCases2.txt"
                    My.Computer.FileSystem.WriteAllText(newFileName, returncases2, False)
                End If
            End If

            file_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                                       retn_day & "\Remittances\" & Format(remit_no, "#") & "\Returns\"
            Try
                For Each foundFile As String In My.Computer.FileSystem.GetFiles _
           (file_path, FileIO.SearchOption.SearchTopLevelOnly, "*.pdf")
                    'get debtor number
                    files_found = True
                    Try
                        ProgressBar1.Value += 5
                    Catch ex As Exception
                        ProgressBar1.Value = 5
                    End Try

                    Try
                        debtor = Mid(foundFile, foundFile.Length - 11, 8)
                    Catch ex As Exception
                        Continue For
                    End Try
                    If debtor < 0 Then
                        debtor = debtor * -1
                    End If
                    'get return code for this debtor
                    param2 = "select return_codeID, client_ref, status_open_closed from Debtor " & _
                    " where _rowid = " & debtor
                    Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
                    If no_of_rows = 0 Then
                        MsgBox("Unable to find case number" & debtor)
                        Exit Sub
                    End If
                    Dim status_open_closed As String = debtor_dataset.Tables(0).Rows(0).Item(2)
                    If status_open_closed = "O" Then
                        Continue For
                    End If
                    Try
                        retn_codeid = debtor_dataset.Tables(0).Rows(0).Item(0)
                    Catch ex As Exception
                        retn_codeid = 0
                    End Try
                    'get override return code
                    Dim retn_group As String = ""
                    If retn_codeid > 0 Then
                        param2 = "select clientreturnnumber from clientSchemeReturn" & _
                        " where returnID = " & retn_codeid & _
                        " and clientSchemeID = " & selected_csid
                        Dim cr_dataset As DataSet = get_dataset("onestep", param2)
                        If no_of_rows = 1 Then
                            If cr_dataset.Tables(0).Rows(0).Item(0) = "05" Or
                                 cr_dataset.Tables(0).Rows(0).Item(0) = "06" Or cr_dataset.Tables(0).Rows(0).Item(0) = "18" Then
                                retn_group = "Returns 1"
                            Else
                                retn_group = "Returns 2"
                            End If
                        End If
                    End If
                    If retn_group <> "" Then
                        client_ref = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
                        file_name = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                                          retn_day & "\Remittances\" & Format(remit_no, "#") & "\" & retn_group & "\" & client_ref & "-" & debtor & ".pdf"
                        Try
                            My.Computer.FileSystem.CopyFile(foundFile, file_name)
                        Catch ex As Exception

                        End Try
                    End If
                Next
            Catch ex As Exception

            End Try
           
        Next
      
        If files_found Then
            MsgBox("All reports copied to directories")
        Else
            MsgBox("No reports were found")
        End If
        Me.Close()
    End Sub

   
    Private Sub disable_buttons()
        exitbtn.Enabled = False
        retnbtn.Enabled = False
    End Sub
    Private Sub enable_buttons()
        exitbtn.Enabled = True
        retnbtn.Enabled = True
    End Sub
End Class

﻿Imports CommonLibrary
Imports System.Configuration

Module modCommon

    Public DataColumnsList() As String = {"Total", "Period1", "Period2", "Period3", "Period4", "Period5", "Period6", "Month1", "Month2", "Month3", "Month4", "Month5", "Month6", "Month7", "Month8", "Month9", "Month10", "Month11", "Month12", "YearPlus"} 'Used for validation within events
    Public RefreshScroll As Boolean = True ' needed as chkTopClients will change the number of rows in dgvClientName

    Sub Main()
        Application.EnableVisualStyles()

        Select Case GetStartScreen()
            Case "C"
                frmCaseSummary.ShowDialog()
            Case "B"
                frmBailiffSummary.ShowDialog()
        End Select
    End Sub

    Public Sub PreRefresh(ByRef Control As Control)
        Control.Cursor = Cursors.WaitCursor
        Control.SuspendLayout()
    End Sub

    Public Sub PostRefresh(ByRef Control As Control)
        Control.ResumeLayout()
        Control.Cursor = Cursors.Default
    End Sub

    Public Sub FormatGridColumns(ByRef DataGridView As DataGridView, Optional ByVal StageList As String() = Nothing)
        If IsNothing(StageList) Then StageList = {}

        Dim PeriodNames As String() = GetPeriodNames(String.Join(vbTab, StageList))

        For Each Column As DataGridViewColumn In DataGridView.Columns
            Select Case Column.Name
                Case "StageName"
                    Column.Width = 140
                    Column.HeaderText = "Stage"
                Case "ClientName"
                    Column.Width = 170
                    Column.HeaderText = "Client"
                Case "WorkType"
                    Column.Width = 90
                    Column.HeaderText = "Work type"
                Case "SchemeName"
                    Column.Width = 100
                    Column.HeaderText = "Scheme"
                Case "Allocated"
                    Column.Width = 55
                Case "Levied"
                    Column.Width = 55
                Case "VanFeesApplied"
                    Column.Width = 80
                    Column.HeaderText = "Van Fees"
                Case "Payment"
                    Column.Width = 60
                Case "Visited"
                    Column.Width = 55
                Case "AddConfirmed"
                    Column.Width = 60
                    Column.HeaderText = "Confirmed"
                Case "ArrangementBroken"
                    Column.Width = 60
                    Column.HeaderText = "Broken"
                Case "Covered"
                    Column.Width = 90
                Case "BailiffName"
                    Column.Width = 120
                    Column.HeaderText = "Bailiff"
                Case "PostcodeArea"
                    If DataGridView.Name = "dgvSummary" Then
                        Column.Width = 55
                        Column.HeaderText = "Area"
                    Else
                        Column.Visible = False
                    End If
                Case "Total"
                    Column.Width = 55
                Case "Period1"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(0)
                Case "Period2"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(1)
                Case "Period3"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(2)
                Case "Period4"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(3)
                Case "Period5"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(4)
                Case "Period6"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(5)
                Case "Month1"
                    Column.Width = 55
                Case "Month2"
                    Column.Width = 55
                Case "Month3"
                    Column.Width = 55
                Case "Month4"
                    Column.Width = 55
                Case "Month5"
                    Column.Width = 55
                Case "Month6"
                    Column.Width = 55
                Case "Month7"
                    Column.Width = 55
                Case "Month8"
                    Column.Width = 55
                Case "Month9"
                    Column.Width = 55
                Case "Month10"
                    Column.Width = 55
                Case "Month11"
                    Column.Width = 55
                Case "Month12"
                    Column.Width = 55
                Case "YearPlus"
                    Column.Width = 55
                Case "DebtorID"
                    Column.Width = 80
                    Column.HeaderText = "Case ID"
                Case "LinkID"
                    Column.Width = 70
                    Column.HeaderText = "Link ID"
                Case "Warning"
                    Column.Width = 55
                Case "add_postcode"
                    Column.Width = 70
                    Column.HeaderText = "Postcode"
                Case "CreatedDate"
                    Column.Width = 105
                    Column.HeaderText = "Created date"
                Case "debt_balance"
                    Column.Width = 60
                    Column.HeaderText = "Balance"
                Case "FeesOutstanding"
                    Column.Width = 70
                    Column.HeaderText = "Os fees"
                Case "LastAction"
                    Column.Width = 105
                    Column.HeaderText = "Last action"
                Case "BailiffID"
                    Column.Visible = False
                Case "name_fore"
                    Column.Width = 70
                    Column.HeaderText = "Forename"
                Case "name_sur"
                    Column.Width = 70
                    Column.HeaderText = "Surname"
                Case "BailiffType"
                    Column.Width = 60
                    Column.HeaderText = "Type"
                Case "CasesAllocated"
                    Column.Width = 60
                    Column.HeaderText = "Allocated"
                Case "MaxCases"
                    Column.Width = 50
                    Column.HeaderText = "Max"
                Case "LastAllocation"
                    Column.HeaderText = "Last Allocation"
                Case "BailiffAllocatedDate"
                    Column.Width = 70
                    Column.HeaderText = "Allocated"
                Case "LODate"
                    Column.Width = 70
                    Column.HeaderText = "LO date"
                Case "ResidencyScore"
                    Column.Width = 80
                    Column.HeaderText = "Res score"
                Case "add_phone" ' added TS 06/May/2013
                    Column.Width = 80
                    Column.HeaderText = "Telephone 1"
                Case "add_fax" ' added TS 06/May/2013
                    Column.Width = 80
                    Column.HeaderText = "Telephone 2"
                Case "add_os_easting"
                    Column.Visible = False
                Case "add_os_northing"
                    Column.Visible = False
                Case "LinkedCase"
                    Column.Visible = False
                Case "FirstTwoVisitsPAWithinTwoDays" ' added TS 24/May/2013
                    Column.Width = 80
                    Column.HeaderText = "Visit 2 < 2"
                Case "NumberOfVisits" ' added TS 13/Sep/2013
                    Column.Width = 70
                    Column.HeaderText = "# Visits"
                Case "NumberOfVisitsPA" ' added TS 24/May/2013
                    Column.Width = 80
                    Column.HeaderText = "# Visits"
                Case "AllVisitsPAWithinTwentyDays" ' added TS 24/May/2013
                    Column.Width = 85
                    Column.HeaderText = "Visits < 20"
            End Select

        Next Column
    End Sub

    Public Function FormOpen(ByVal Formname As String) As Boolean
        FormOpen = False

        For Each frm As Form In Application.OpenForms
            If Formname = frm.Name Then FormOpen = True
        Next

    End Function

    Private Function GetStartScreen() As String
        GetStartScreen = GetSQLResults("BailiffAllocation", "EXEC dbo.GetStartScreen")
    End Function

    Private Function GetPeriodNames(ByVal StageList As String) As String()
        GetPeriodNames = GetSQLResults("BailiffAllocation", "EXEC dbo.GetPeriodNames '" & StageList & "'").Split(",")
    End Function

    Public Sub SetFormIcon(ByRef Sender As Form)
        Select Case ConfigurationManager.AppSettings("Company")
            Case "Rossendales"
                Sender.Icon = My.Resources.Rossendales
            Case "Marston"
                Sender.Icon = My.Resources.Marston
        End Select

    End Sub
End Module

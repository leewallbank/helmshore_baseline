﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMap
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMap))
        Me.cmdPlotRoute = New System.Windows.Forms.Button()
        Me.lstSelected = New System.Windows.Forms.ListBox()
        Me.chkSelect = New System.Windows.Forms.CheckBox()
        Me.lstLegend = New System.Windows.Forms.ListView()
        Me.imgPushpin = New System.Windows.Forms.ImageList(Me.components)
        Me.lblSummary = New System.Windows.Forms.Label()
        Me.cmsSelected = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.chkShowBailiffs = New System.Windows.Forms.CheckBox()
        Me.mapMain = New AxMapPoint.AxMappointControl()
        CType(Me.mapMain,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'cmdPlotRoute
        '
        Me.cmdPlotRoute.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.cmdPlotRoute.Location = New System.Drawing.Point(657, 69)
        Me.cmdPlotRoute.Name = "cmdPlotRoute"
        Me.cmdPlotRoute.Size = New System.Drawing.Size(99, 25)
        Me.cmdPlotRoute.TabIndex = 1
        Me.cmdPlotRoute.Text = "Plot Route"
        Me.cmdPlotRoute.UseVisualStyleBackColor = true
        '
        'lstSelected
        '
        Me.lstSelected.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left),System.Windows.Forms.AnchorStyles)
        Me.lstSelected.FormattingEnabled = true
        Me.lstSelected.Location = New System.Drawing.Point(6, 115)
        Me.lstSelected.Name = "lstSelected"
        Me.lstSelected.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.lstSelected.Size = New System.Drawing.Size(99, 251)
        Me.lstSelected.TabIndex = 5
        '
        'chkSelect
        '
        Me.chkSelect.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.chkSelect.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkSelect.Location = New System.Drawing.Point(657, 11)
        Me.chkSelect.Name = "chkSelect"
        Me.chkSelect.Size = New System.Drawing.Size(99, 25)
        Me.chkSelect.TabIndex = 8
        Me.chkSelect.Text = "Select"
        Me.chkSelect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkSelect.UseVisualStyleBackColor = true
        '
        'lstLegend
        '
        Me.lstLegend.Alignment = System.Windows.Forms.ListViewAlignment.[Default]
        Me.lstLegend.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.lstLegend.CheckBoxes = true
        Me.lstLegend.FullRowSelect = true
        Me.lstLegend.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.lstLegend.Location = New System.Drawing.Point(6, 12)
        Me.lstLegend.MultiSelect = false
        Me.lstLegend.Name = "lstLegend"
        Me.lstLegend.ShowGroups = false
        Me.lstLegend.Size = New System.Drawing.Size(645, 80)
        Me.lstLegend.TabIndex = 9
        Me.lstLegend.UseCompatibleStateImageBehavior = false
        '
        'imgPushpin
        '
        Me.imgPushpin.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit
        Me.imgPushpin.ImageSize = New System.Drawing.Size(16, 16)
        Me.imgPushpin.TransparentColor = System.Drawing.Color.Lime
        '
        'lblSummary
        '
        Me.lblSummary.AutoSize = true
        Me.lblSummary.Location = New System.Drawing.Point(3, 98)
        Me.lblSummary.Name = "lblSummary"
        Me.lblSummary.Size = New System.Drawing.Size(39, 13)
        Me.lblSummary.TabIndex = 10
        Me.lblSummary.Text = "Label1"
        '
        'cmsSelected
        '
        Me.cmsSelected.Name = "ContextMenuStrip1"
        Me.cmsSelected.Size = New System.Drawing.Size(61, 4)
        '
        'chkShowBailiffs
        '
        Me.chkShowBailiffs.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.chkShowBailiffs.Appearance = System.Windows.Forms.Appearance.Button
        Me.chkShowBailiffs.Location = New System.Drawing.Point(657, 40)
        Me.chkShowBailiffs.Name = "chkShowBailiffs"
        Me.chkShowBailiffs.Size = New System.Drawing.Size(99, 25)
        Me.chkShowBailiffs.TabIndex = 11
        Me.chkShowBailiffs.Text = "Show Bailiffs"
        Me.chkShowBailiffs.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.chkShowBailiffs.UseVisualStyleBackColor = true
        '
        'mapMain
        '
        Me.mapMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom)  _
            Or System.Windows.Forms.AnchorStyles.Left)  _
            Or System.Windows.Forms.AnchorStyles.Right),System.Windows.Forms.AnchorStyles)
        Me.mapMain.Enabled = true
        Me.mapMain.Location = New System.Drawing.Point(111, 98)
        Me.mapMain.Name = "mapMain"
        Me.mapMain.OcxState = CType(resources.GetObject("mapMain.OcxState"),System.Windows.Forms.AxHost.State)
        Me.mapMain.Size = New System.Drawing.Size(645, 268)
        Me.mapMain.TabIndex = 12
        '
        'frmMap
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(761, 370)
        Me.Controls.Add(Me.chkShowBailiffs)
        Me.Controls.Add(Me.mapMain)
        Me.Controls.Add(Me.lblSummary)
        Me.Controls.Add(Me.lstLegend)
        Me.Controls.Add(Me.lstSelected)
        Me.Controls.Add(Me.chkSelect)
        Me.Controls.Add(Me.cmdPlotRoute)
        Me.Icon = CType(resources.GetObject("$this.Icon"),System.Drawing.Icon)
        Me.Name = "frmMap"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Map"
        CType(Me.mapMain,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents cmdPlotRoute As System.Windows.Forms.Button
    Friend WithEvents lstSelected As System.Windows.Forms.ListBox
    Friend WithEvents chkSelect As System.Windows.Forms.CheckBox
    Friend WithEvents lstLegend As System.Windows.Forms.ListView
    Friend WithEvents imgPushpin As System.Windows.Forms.ImageList
    Friend WithEvents lblSummary As System.Windows.Forms.Label
    Friend WithEvents cmsSelected As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents chkShowBailiffs As System.Windows.Forms.CheckBox
    Friend WithEvents mapMain As AxMapPoint.AxMappointControl

End Class

﻿Public Class clsGridState

    Private ItemsCollection() As String
    Private ScrollPosition As Integer
    Private SortedColumn As Integer = -1
    Private SortOrder As SortOrder
    Private DataGridView As DataGridView

    Public Sub New(ByRef dgv As DataGridView)
        DataGridView = dgv
    End Sub
#Region "Public properties"

#End Region

#Region "Public methods"

    Public Sub GetState()

        ItemsCollection = Nothing

        For Each dr As DataGridViewRow In DataGridView.SelectedRows
            If IsNothing(ItemsCollection) Then ReDim ItemsCollection(0) Else ReDim Preserve ItemsCollection(UBound(ItemsCollection) + 1)
            ItemsCollection(UBound(ItemsCollection)) = dr.Cells(0).Value
        Next dr

        ScrollPosition = DataGridView.FirstDisplayedScrollingRowIndex

    End Sub

    Private Sub GetSort() ' Not called from anything external
        If IsNothing(DataGridView.SortedColumn) Then
            SortedColumn = -1
            SortOrder = Nothing
        Else
            SortedColumn = DataGridView.SortedColumn.Index
            SortOrder = DataGridView.SortOrder
        End If
    End Sub

    Public Sub SetSort()
        If SortedColumn > -1 Then
            If SortOrder.Ascending Then
                DataGridView.Sort(DataGridView.Columns(SortedColumn), System.ComponentModel.ListSortDirection.Ascending)
            ElseIf SortOrder.Descending Then
                DataGridView.Sort(DataGridView.Columns(SortedColumn), System.ComponentModel.ListSortDirection.Descending)
            End If
        End If
    End Sub

    Public Sub SetState()

        GetSort()

        DataGridView.ClearSelection()

        If Not IsNothing(ItemsCollection) Then
            For Each dr As DataGridViewRow In DataGridView.Rows
                If ItemsCollection.Contains(dr.Cells(0).Value) Then dr.Selected = True
            Next dr
        End If

        If Not {"dgvClientName", "dgvBailiffName"}.Contains(DataGridView.Name) Or RefreshScroll = True Then DataGridView.FirstDisplayedScrollingRowIndex = ScrollPosition ' without this the mouseup event selects the row under the pointer with the scroll bar at the top - the default after datasource is changed

    End Sub

#End Region

End Class

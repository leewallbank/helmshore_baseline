﻿Public Class diaCaseDetail
    Private SummaryData As New clsCaseSummaryData
    Private CurrentParamList, CurrentPeriod As String
    Private SendingForm As Form

    Private ColSort As String

    Public ReadOnly Property CaseCount As Integer ' Added TS 17/Sep/2013 Portal task ref 16716
        Get
            CaseCount = 0
            If Not SummaryData.DetailDataView Is Nothing Then CaseCount = SummaryData.DetailDataView.Count
        End Get
    End Property


    Public Sub New(ByRef sender As Form)

        SendingForm = sender

        InitializeComponent()

        SetFormIcon(Me)

        cmsDetail.Items.Add("Show Bailiffs")
        cmsDetail.Items.Add("Copy all case IDs")
        cmsDetail.Items.Add("Copy selected case IDs")
        cmsDetail.Items.Add("Copy")
        cmsDetail.Items.Add("Select All")

    End Sub

    Private Sub cmdCopyAllDebtorID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCopyAllDebtorID.Click
        CopyAllDebtorIDs()
    End Sub

    Private Sub CopyAllDebtorIDs()
        PreRefresh(Me)

        ' Store the current selected cells
        Dim SelectedCells As DataGridViewSelectedCellCollection = dgvDetail.SelectedCells
        dgvDetail.ClearSelection()

        'Change sort mode
        For Each Column As DataGridViewColumn In dgvDetail.Columns
            Column.SortMode = DataGridViewColumnSortMode.NotSortable
        Next Column

        dgvDetail.SelectionMode = DataGridViewSelectionMode.FullColumnSelect
        dgvDetail.Columns("DebtorID").Selected = True

        dgvDetail.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText ' OneStep will not accept column headers
        Clipboard.SetDataObject(dgvDetail.GetClipboardContent())

        dgvDetail.SelectionMode = DataGridViewSelectionMode.CellSelect

        ' Change sortmode back
        For Each Column As DataGridViewColumn In dgvDetail.Columns
            Column.SortMode = DataGridViewColumnSortMode.Automatic
        Next Column

        ' Reapply selected cells
        dgvDetail.ClearSelection()
        For Each Cell As DataGridViewCell In SelectedCells
            dgvDetail.Rows(Cell.RowIndex).Cells(Cell.ColumnIndex).Selected = True
        Next Cell

        PostRefresh(Me)
    End Sub

    Private Sub CopySelectedCasesDebtorIDs()
        PreRefresh(Me)

        ' Store the current selected cells
        Dim SelectedCells As DataGridViewSelectedCellCollection = dgvDetail.SelectedCells
        dgvDetail.ClearSelection()

        'Change sort mode
        For Each Column As DataGridViewColumn In dgvDetail.Columns
            Column.SortMode = DataGridViewColumnSortMode.NotSortable
        Next Column

        For Each Cell As DataGridViewCell In SelectedCells
            dgvDetail.Rows(Cell.RowIndex).Cells("DebtorID").Selected = True
        Next (Cell)

        dgvDetail.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableWithoutHeaderText ' OneStep will not accept column headers
        Clipboard.SetDataObject(dgvDetail.GetClipboardContent())

        dgvDetail.SelectionMode = DataGridViewSelectionMode.CellSelect

        ' Change sortmode back
        For Each Column As DataGridViewColumn In dgvDetail.Columns
            Column.SortMode = DataGridViewColumnSortMode.Automatic
        Next Column

        ' Reapply selected cells
        dgvDetail.ClearSelection()
        For Each Cell As DataGridViewCell In SelectedCells
            dgvDetail.Rows(Cell.RowIndex).Cells(Cell.ColumnIndex).Selected = True
        Next Cell

        PostRefresh(Me)
    End Sub

    Public Sub AddCases(ByVal ParamList As String, ByVal Period As String)

        CurrentParamList = ParamList
        CurrentPeriod = Period

        PreRefresh(Me)

        Select Case SendingForm.Name
            Case "frmCaseSummary"
                SummaryData.GetCases(ParamList, Period)
            Case "frmBailiffSummary"
                SummaryData.GetBailiffCases(ParamList, Period)
                chkInclLinked.Visible = False
        End Select

        dgvDetail.DataSource = SummaryData.DetailDataView

        FormatGridColumns(dgvDetail)

        PostRefresh(Me)

    End Sub

    Public Sub AddCasesByBailiff(ByVal BailiffID)
        PreRefresh(Me)

        SummaryData.GetCasesByBailiff(BailiffID)
        dgvDetail.DataSource = SummaryData.DetailDataView

        FormatGridColumns(dgvDetail)

        PostRefresh(Me)

        chkInclLinked.Visible = False
    End Sub

    Private Sub dgvDetail_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvDetail.MouseDown
        Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

        If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvDetail.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
            dgvDetail.Item(hti.ColumnIndex, hti.RowIndex).Selected = True
        ElseIf hti.Type = DataGridViewHitTestType.ColumnHeader Then
            ColSort = SummaryData.DetailDataView.Sort
        End If

    End Sub

    Private Sub dgvDetail_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvDetail.MouseUp
        Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

        If e.Button = MouseButtons.Right Then cmsDetail.Show(dgvDetail, e.Location)

        If hti.Type = DataGridViewHitTestType.ColumnHeader Then
            If ColSort <> "" Then SummaryData.DetailDataView.Sort += "," & ColSort
        End If
    End Sub

    Private Sub dgvDetail_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles dgvDetail.Paint
        ' Highlight any linked cases in yellow
        For Each Row As DataGridViewRow In dgvDetail.Rows
            If Row.Cells("LinkedCase").Value = 0 Or Not chkInclLinked.Checked Then
                Row.DefaultCellStyle.BackColor = Color.White
            ElseIf Row.Cells("LinkedCase").Value = 2 Then
                Row.DefaultCellStyle.BackColor = Color.Yellow
            ElseIf Row.Cells("LinkedCase").Value = 1 And chkInclLinked.Checked Then
                Row.DefaultCellStyle.BackColor = Color.LightGreen
            Else
                Row.DefaultCellStyle.BackColor = Color.Red ' Should never hit this
            End If

            If Row.Cells("Warning").Value = "Y" Then Row.Cells("Warning").Style.BackColor = Color.Red
        Next Row
    End Sub

    Private Sub dgvDetail_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDetail.SelectionChanged
        Dim SelectedRows() As Integer = Nothing
        Dim TotalValue As Decimal = 0

        lblSummary.Text = ""

        ' This looks more complex than it needs to be but we need to process unique rows
        For Each Cell As DataGridViewCell In dgvDetail.SelectedCells
            If Not IsNothing(SelectedRows) Then
                If Not SelectedRows.Contains(Cell.RowIndex) Then
                    ReDim Preserve SelectedRows(UBound(SelectedRows) + 1)
                    SelectedRows(UBound(SelectedRows)) = Cell.RowIndex
                    TotalValue += dgvDetail.Item("debt_balance", Cell.RowIndex).Value
                End If
            Else
                ReDim SelectedRows(0)
                SelectedRows(0) = Cell.RowIndex
                TotalValue += dgvDetail.Item("debt_balance", Cell.RowIndex).Value
            End If
        Next Cell

        If Not IsNothing(SelectedRows) Then
            lblSummary.Text = (UBound(SelectedRows) + 1).ToString & " case" & If(UBound(SelectedRows) > 0, "s", "") & " selected. Total balance: £" & TotalValue.ToString
        End If

    End Sub

    Private Sub diaDetail_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        lblSummary.Text = ""
        If Not IsNothing(SummaryData.DetailDataView) Then SummaryData.DetailDataView.RowFilter = "LinkedCase <= 1"
        dgvDetail.ClearSelection()
    End Sub

    Private Sub cmsDetail_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsDetail.ItemClicked
        Select Case e.ClickedItem.Text
            Case "Copy all case IDs"
                CopyAllDebtorIDs()
            Case "Copy selected case IDs"
                CopySelectedCasesDebtorIDs()
            Case "Copy"
                dgvDetail.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                Clipboard.SetDataObject(dgvDetail.GetClipboardContent())
            Case "Show Bailiffs"

                Dim PostcodeArea() As String = Nothing

                PreRefresh(Me)

                For Each Cell As DataGridViewCell In dgvDetail.SelectedCells
                    If dgvDetail.Rows(Cell.RowIndex).Cells("PostcodeArea").Value <> "" Then
                        If IsNothing(PostcodeArea) Then ReDim PostcodeArea(0) Else ReDim Preserve PostcodeArea(UBound(PostcodeArea) + 1)
                        PostcodeArea(UBound(PostcodeArea)) = dgvDetail.Rows(Cell.RowIndex).Cells("PostcodeArea").Value
                    End If
                Next Cell

                If Not IsNothing(PostcodeArea) Then ' if no postcodes
                    ' Remove duplicate postcode areas
                    PostcodeArea = PostcodeArea.Distinct().ToArray()

                    Dim Bailiff As New diaBailiff
                    Bailiff.AddCases(PostcodeArea)
                    PostRefresh(Me)
                    Bailiff.ShowDialog()
                    Bailiff = Nothing
                Else
                    PostRefresh(Me)
                End If

            Case "Select All"
                dgvDetail.SelectAll()
        End Select
    End Sub

    Private Sub chkInclLinked_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkInclLinked.CheckedChanged

        If Not chkInclLinked.Checked Then
            SummaryData.DetailDataView.RowFilter = "LinkedCase <= 1"
        Else
            SummaryData.DetailDataView.RowFilter = ""
        End If

        dgvDetail.ClearSelection()

    End Sub
End Class
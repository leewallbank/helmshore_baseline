﻿Option Explicit On

Imports MapPoint

Public Class frmMap

    Private mpGUI As MapPointGUI
    Private SendingDGV As DataGridView
    Private CaseData As New clsCaseSummaryData
    Private BailiffData As New clsBailiffData
    Private ImagePath As String = "R:\Allocation Diary\BailiffAllocation\Pushpin Images\"
    Private Images() As String = {"001BlackPushpin.bmp", "002RedPushpin.bmp", "003YellowPushpin.bmp", "004DarkBluePusphin.bmp", "005BluePushpin.bmp", "006MaroonPushpin.bmp", "007GreenPushpin.bmp", "008PurplePushpin.bmp", "009BlackTack.bmp", "010RedTack.bmp", "011YellowTack.bmp", "012DarkBlueTack.bmp", "013BlueTack.bmp", "014MaroonTack.bmp", "015GreenTack.bmp", "016PurpleTack.bmp", "017SmallBlackCircle.bmp", "018SmallRedCircle.bmp", "019SmallYellowCircle.bmp", "020SmallWhiteCircle.bmp", "021SmallBlueCircle.bmp", "022SmallTurquoiseCircle.bmp", "023SmallGreenCircle.bmp", "024SmallPurpleCircle.bmp", "025BlackCircle.bmp", "026RedCircle.bmp", "027YellowCircle.bmp", "028WhiteCircle.bmp", "029BlueCircle.bmp", "030TurquoiseCircle.bmp", "031GreenCircle.bmp", "032PurpleCircle.bmp", "033SmallBlackSquare.bmp", "034SmallRedSquare.bmp", "035SmallYellowSquare.bmp""036SmallWhiteSquare.bmp", "037SmallBlueSquare.bmp", "038SmallTurquoiseSquare.bmp", "039SmallGreenSquare.bmp", "040SmallPurpleSquare.bmp", "041BlackSquare.bmp", "042RedSquare.bmp", "043YellowSquare.bmp", "044WhiteSquare.bmp", "045BlueSquare.mp", "046TurquoiseSquare.bmp", "047GreenSquare.bmp", "048PurpleSquare.bmp", "049SmallBlackTriangle.bmp", "050SmallRedTriangle.bmp", "051SmallYellowTriangle.bmp", "052SmallWhiteTriangle.bmp", "053SmallBlueTriangle.bmp", "054SmallTurquoiseTriangle.bmp", "055SmallGreenTriangle.bmp", "056SmallPurpleTriangle.bmp", "057BlackTriangle.bmp", "058RedTriangle.bmp", "059YellowTriangle.bmp", "060WhiteTriangle.bmp", "061BlueTriangle.bmp", "062TurquoiseTriangle.bmp", "063GreenTriangle.bmp", "064PurpleTriangle.bmp"}
    Private MapDrag As Boolean = False
    Private MouseDownButton As Integer
    Private BailiffsAdded As Boolean = False

    Public Const WM_CLOSE As Short = &H10S

    Public Sub New(ByRef sender As DataGridView)

        SendingDGV = sender

        ' This call is required by the designer.
        InitializeComponent()

        SetFormIcon(Me)

        mapMain.NewMap(GeoMapRegion.geoMapEurope)
        mapMain.ActiveMap.MapStyle = MapPoint.GeoMapStyle.geoMapStyleRoad
        mapMain.Units = MapPoint.GeoUnits.geoMiles

        lblSummary.Text = "0 cases"

        lstLegend.SmallImageList = imgPushpin
        lstLegend.LargeImageList = imgPushpin
        lstLegend.View = View.List

        cmsSelected.Items.Add("Copy")
        cmsSelected.Items.Add("Copy All")

    End Sub

    Private Sub Form1_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        mapMain.ActiveMap.Saved = True
    End Sub

    Private Sub PopulateLegend()
        Dim LoopCount As Integer = 0

        lstLegend.Items.Clear()

        RemoveHandler lstLegend.ItemChecked, AddressOf lstLegend_ItemChecked

        For Each DataSet As DataSet In mapMain.ActiveMap.DataSets
            If DataSet.Name <> "My pushpins" Then
                If Not {"First call", "Van"}.Contains(DataSet.Name) Or chkShowBailiffs.Checked Then
                    Dim NewItem As New ListViewItem
                    NewItem = lstLegend.Items.Add(DataSet.Name, LoopCount.ToString)
                    NewItem.Checked = True
                End If
            End If
            LoopCount += 1
        Next DataSet

        AddHandler lstLegend.ItemChecked, AddressOf lstLegend_ItemChecked
    End Sub

    Private Sub cmdPlotRoute_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Waypoints As MapPoint.Waypoints

        mapMain.ActiveMap.ActiveRoute.Clear()
        Waypoints = mapMain.ActiveMap.ActiveRoute.Waypoints

        For Each Item As String In lstSelected.Items
            Waypoints.Add(mapMain.ActiveMap.FindPushpin(Item))
        Next Item

        If Waypoints.Count > 1 Then
            mapMain.ActiveMap.ActiveRoute.Waypoints.Optimize()
            mapMain.ActiveMap.ActiveRoute.Calculate()
        End If
    End Sub

    Private Sub mapMain_BeforeClick(sender As Object, e As AxMapPoint._IMappointCtrlEvents_BeforeClickEvent) Handles mapMain.BeforeClick

        If e.button = MapPoint.GeoMouseButtonConstants.geoLeftButton Then
            For Each objResult As Object In mapMain.ActiveMap.ObjectsFromPoint(e.x, e.y)
                Dim Test As Pushpin = TryCast(objResult, Pushpin)
                If Not IsNothing(Test) Then
                    e.cancel = True
                End If
            Next objResult
        End If
    End Sub

    Private Sub mapMain_MouseMoveEvent(sender As Object, e As AxMapPoint._IMappointCtrlEvents_MouseMoveEvent) Handles mapMain.MouseMoveEvent
        If e.button = MapPoint.GeoMouseButtonConstants.geoLeftButton Then
            MapDrag = True
        Else
            MapDrag = False
        End If
    End Sub

    Private Sub MapMain_MouseUpEvent(sender As Object, e As AxMapPoint._IMappointCtrlEvents_MouseUpEvent) Handles mapMain.MouseUpEvent

        If chkSelect.Checked Then
            ListSelection()
            chkSelect.Checked = False
        ElseIf MapDrag = False Then

            Dim SelectedItems As New ArrayList()

            If My.Computer.Keyboard.CtrlKeyDown = False Then
                ClearHighlights()
            Else
                SelectedItems.AddRange(lstSelected.Items)
            End If

            lstSelected.Items.Clear()
            lblSummary.Text = ""
            For Each objResult As Object In mapMain.ActiveMap.ObjectsFromPoint(e.x, e.y)
                Dim Test As Pushpin = TryCast(objResult, Pushpin)
                If Not IsNothing(Test) Then
                    If Not {"First call", "Van"}.Contains(Test.Parent.Name) Then
                        objResult.highlight = True
                        SelectedItems.Add(objResult.name)
                    Else
                        If objResult.balloonstate = MapPoint.GeoBalloonState.geoDisplayName Then
                            objResult.balloonstate = MapPoint.GeoBalloonState.geoDisplayNone
                        Else
                            objResult.balloonstate = MapPoint.GeoBalloonState.geoDisplayName
                        End If
                    End If
                End If
            Next objResult

            If Not IsNothing(SelectedItems) Then
                ClearShapes()
                mapMain.ActiveMap.ActiveRoute.Clear()
                SelectedItems.Sort()
                lstSelected.Items.AddRange(SelectedItems.ToArray)
                lblSummary.Text = lstSelected.Items.Count.ToString & " case" & If(lstSelected.Items.Count <> 1, "s", "")
            End If
        End If

    End Sub

    Private Sub mapMain_SelectionChange(pNewSelection As Object, pOldSelection As Object) Handles mapMain.SelectionChange

        ' This stops anyone moving pushpins
        'http://www.mp2kmag.com/a97--pushpins.immoveable.mappoint.html

        Dim sp As MapPoint.Shape
        Dim loc As MapPoint.Location

        If TypeOf (pNewSelection) Is MapPoint.Pushpin Then
            '   DirectCast(e.pNewSelection, MapPoint.Pushpin).Highlight = True
            '           DirectCast(e.pNewSelection, MapPoint.Pushpin).BalloonState = GeoBalloonState.geoDisplayBalloon
            loc = mapMain.ActiveMap.GetLocation(80, 0) 'get a location over at the arctic ocean
            sp = mapMain.ActiveMap.Shapes.AddShape(MapPoint.GeoAutoShapeType.geoShapeRectangle, loc, 1, 1)
            sp.Select()
            sp.Delete()
        End If

    End Sub

    Public Sub AddCases(ByVal ParamList As String, ByVal Period As String)
        CaseData.GetCases(ParamList, Period)
    End Sub

    Public Sub GetCasesForMap()

        CaseData.GetCasesForMap(SendingDGV)

        Dim Setnames As DataTable = CaseData.MapCaseDataView.ToTable(True, "Dataset")
        Dim InvalidCases As New ArrayList
        Dim DataSetExists As Boolean

        With mapMain.ActiveMap

            For Each dr As DataRow In Setnames.Rows
                DataSetExists = False
                For Each ds As MapPoint.DataSet In mapMain.ActiveMap.DataSets
                    If ds.Name = dr.Item("Dataset") Then DataSetExists = True
                Next ds
                If Not DataSetExists Then mapMain.ActiveMap.DataSets.AddPushpinSet(dr.Item("Dataset"))
            Next dr

            For Each dr As DataRow In CaseData.MapCaseDataView.Table.Rows
                Dim NewPushpin As Pushpin = Nothing
                Dim NewLocation As Location = Nothing

                If Not IsDBNull(dr.Item("Easting")) AndAlso dr.Item("Easting").ToString.Length = 6 AndAlso Not IsDBNull(dr.Item("Northing")) AndAlso dr.Item("Northing").ToString.Length = 6 Then ' try eastings and northings
                    NewPushpin = .AddPushpin(.LocationFromOSGridReference(dr.Item("Easting") & dr.Item("Northing")), dr.Item("DebtorID"))
                Else ' try postcode
                    Dim Results As FindResults = .FindAddressResults(, , , , dr.Item("Postcode"))
                    If Results.Count = 0 Then
                        If Not InvalidCases.Contains(dr.Item("DebtorID")) Then InvalidCases.Add(dr.Item("DebtorID")) ' Check so we don't load duplicates
                    Else
                        NewPushpin = .AddPushpin(Results(1), dr.Item("DebtorID"))
                    End If
                End If

                If Not IsNothing(NewPushpin) Then
                    NewPushpin.MoveTo(.DataSets(dr.Item("Dataset")))
                    If Not IsDBNull(dr.Item("BailiffName")) Then NewPushpin.Note = "Bailiff: " & dr.Item("BailiffName")
                End If

            Next dr

            SetSymbols()

            If .DataSets.Count > 0 Then .DataSets.ZoomTo()

            If InvalidCases.Count > 0 Then MsgBox("The following case" & If(InvalidCases.Count = 1, "", "s") & " cannot be loaded:" & vbCrLf & String.Join(vbCrLf, InvalidCases.ToArray))
        End With

        PopulateLegend()
    End Sub

    Private Sub SetSymbols()
        Dim Symbol As MapPoint.Symbol
        Dim Symbols As MapPoint.Symbols

        Symbols = mapMain.ActiveMap.Symbols

        For ImageCount As Integer = 0 To mapMain.ActiveMap.DataSets.Count - 1
            Symbol = Symbols.Add(ImagePath & Images(ImageCount))
            mapMain.ActiveMap.DataSets(ImageCount + 1).Symbol = Symbol.ID
            imgPushpin.Images.Add(ImageCount.ToString, Image.FromFile(ImagePath & Images(ImageCount)))
        Next ImageCount

    End Sub

    Private Sub chkSelect_CheckChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSelect.CheckedChanged
        If chkSelect.Checked Then
            ClearHighlights()
            lstSelected.Items.Clear()
            lblSummary.Text = ""

            ClearShapes()
            mapMain.ActiveMap.ActiveRoute.Clear()

            mpGUI = New MapPointGUI(Me.mapMain)
            mpGUI.BeginScribbleTool()
            mapMain.ActiveMap.Shapes(1).Line.Weight = 0.5 ' Set line to 1pt
        End If

    End Sub

    Private Sub ListSelection()
        Dim Records As MapPoint.Recordset
        Dim Shape As MapPoint.Shape
        Dim SelectedItems As New ArrayList()

        If mapMain.ActiveMap.Shapes.Count > 0 Then

            lstSelected.Items.Clear()
            Shape = mapMain.ActiveMap.Shapes(1)

            For Each DataSet As MapPoint.DataSet In mapMain.ActiveMap.DataSets
                If DataSet.PushpinsVisible Then
                    Records = DataSet.QueryShape(Shape)
                    Records.MoveFirst()
                    Do While Not Records.EOF
                        SelectedItems.Add(Records.Pushpin.Name)
                        Records.MoveNext()
                    Loop
                End If
            Next DataSet

            SelectedItems.Sort()
            lstSelected.Items.AddRange(SelectedItems.ToArray)

            lblSummary.Text = SelectedItems.Count.ToString & " case" & If(SelectedItems.Count <> 1, "s", "")
            'Else
            '    MsgBox("Please select cases")
        End If

    End Sub

    Private Sub lstLegend_ItemChecked(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckedEventArgs) Handles lstLegend.ItemChecked
        mapMain.ActiveMap.DataSets(e.Item.Text).PushpinsVisible = e.Item.Checked
        ListSelection()
    End Sub

    Private Sub ClearHighlights()
        Dim Records As MapPoint.Recordset
        For Each DataSet As MapPoint.DataSet In mapMain.ActiveMap.DataSets
            Records = DataSet.QueryAllRecords
            Records.MoveFirst()
            Do While Not Records.EOF
                Records.Pushpin.Highlight = False
                Records.MoveNext()
            Loop
        Next DataSet
    End Sub

    Private Sub ClearShapes()
        Do Until mapMain.ActiveMap.Shapes.Count = 0 ' Delete all shapes. There should only be one.
            mapMain.ActiveMap.Shapes(1).Delete()
        Loop
    End Sub

    Private Sub cmsSelected_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsSelected.ItemClicked
        Select Case e.ClickedItem.Text
            Case "Copy"
                CopyListItems(lstSelected.SelectedItems)
            Case "Copy All"
                For LoopCount As Integer = 0 To lstSelected.Items.Count - 1
                    lstSelected.SetSelected(LoopCount, True)
                Next LoopCount
                CopyListItems(lstSelected.Items)
        End Select
    End Sub

    Private Sub CopyListItems(ByRef List As Object)
        Dim CopyItems As String = ""
        Clipboard.Clear()
        For Each Item As String In List
            CopyItems &= Item & vbCrLf
        Next Item
        Clipboard.SetText(CopyItems)
    End Sub

    Private Sub lstSelected_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles lstSelected.MouseUp
        If e.Button = MouseButtons.Right Then cmsSelected.Show(sender, e.Location)
    End Sub

    Private Sub chkShowBailiffs_CheckChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowBailiffs.CheckedChanged
        Dim NewPushpin As Pushpin = Nothing
        Dim Symbol As MapPoint.Symbol
        Dim Symbols As MapPoint.Symbols = mapMain.ActiveMap.Symbols

        PreRefresh(Me)

        If chkShowBailiffs.Checked Then

            If Not BailiffsAdded Then

                With mapMain.ActiveMap
                    BailiffData.GetBailiffs({""})
                    mapMain.ActiveMap.DataSets.AddPushpinSet("First call")
                    Symbol = Symbols.Add(ImagePath & "083RedCar.bmp")
                    mapMain.ActiveMap.DataSets("First call").Symbol = Symbol.ID
                    imgPushpin.Images.Add(imgPushpin.Images.Count.ToString, Image.FromFile(ImagePath & "083RedCar.bmp"))

                    mapMain.ActiveMap.DataSets.AddPushpinSet("Van")
                    Symbol = Symbols.Add(ImagePath & "087BlueTruck.bmp")
                    mapMain.ActiveMap.DataSets("Van").Symbol = Symbol.ID
                    imgPushpin.Images.Add(imgPushpin.Images.Count.ToString, Image.FromFile(ImagePath & "087BlueTruck.bmp"))

                    For Each dr As DataRow In BailiffData.BailiffDataView.Table.Rows

                        If {"First call", "Van"}.Contains(dr.Item("BailiffType")) Then
                            If Not IsDBNull(dr.Item("add_os_easting")) AndAlso dr.Item("add_os_easting").ToString.Length = 6 AndAlso Not IsDBNull(dr.Item("add_os_northing")) AndAlso dr.Item("add_os_northing").ToString.Length = 6 Then ' try eastings and northings
                                NewPushpin = .AddPushpin(.LocationFromOSGridReference(dr.Item("add_os_easting") & dr.Item("add_os_northing")), dr.Item("name_fore") & " " & dr.Item("name_sur"))
                            Else ' try postcode
                                Dim Results As FindResults = Nothing
                                If Not IsDBNull(dr.Item("add_postcode")) Then Results = .FindAddressResults(, , , , dr.Item("add_postcode"))
                                If Not IsNothing(Results) AndAlso Results.Count > 0 Then
                                    NewPushpin = .AddPushpin(Results(1), dr.Item("name_fore") & " " & dr.Item("name_sur"))
                                    Results = Nothing
                                End If
                            End If
                        End If

                        If Not IsNothing(NewPushpin) Then NewPushpin.MoveTo(mapMain.ActiveMap.DataSets(dr.Item("BailiffType")))
                        NewPushpin = Nothing
                    Next dr
                End With

                BailiffsAdded = True
            Else
                mapMain.ActiveMap.DataSets("First call").PushpinsVisible = True
                mapMain.ActiveMap.DataSets("Van").PushpinsVisible = True
            End If
        Else
            mapMain.ActiveMap.DataSets("First call").PushpinsVisible = False
            mapMain.ActiveMap.DataSets("Van").PushpinsVisible = False

        End If
        PopulateLegend()

        PostRefresh(Me)
    End Sub

    'Private Sub AddLocation(ByVal CaseID As String, ByVal DataSet As String, ByVal Easting As String, ByVal Northing As String)
    '    Dim NewPushpin As Pushpin
    '    NewPushpin = mapMain.ActiveMap.AddPushpin(mapMain.ActiveMap.LocationFromOSGridReference(Easting & Northing), CaseID)
    '    NewPushpin.MoveTo(mapMain.ActiveMap.DataSets(DataSet))
    'End Sub

    'Private Sub AddPostcode(ByVal CaseID As String, ByVal DataSet As String, ByVal Postcode As String)
    '    Dim NewPushpin As Pushpin
    '    NewPushpin = mapMain.ActiveMap.AddPushpin(mapMain.ActiveMap.FindAddressResults(, , , , Postcode)(1), CaseID)
    '    NewPushpin.MoveTo(mapMain.ActiveMap.DataSets(DataSet))
    'End Sub

    
    
 
End Class

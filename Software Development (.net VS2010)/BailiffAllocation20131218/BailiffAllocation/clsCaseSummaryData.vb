﻿Imports CommonLibrary

Public Class clsCaseSummaryData

    Private LastLoadDT As New DataTable
    Private SummaryDT As New DataTable
    Private WarningSummaryDT As New DataTable
    Private ClientAllocationLevelDT As New DataTable
    Private UnderAllocatedClientDT As New DataTable
    Private DetailDT As New DataTable
    Private PeriodTypeDT As New DataTable
    Private ConsortiumDT As New DataTable
    Private MapCaseDT As New DataTable

    Private LastLoadDV As DataView
    Private SummaryDV As DataView
    Private WarningSummaryDV As DataView
    Private ClientAllocationLevelDV As DataView
    Private UnderAllocatedClientDV As DataView
    Private DetailDV As DataView

    Private PeriodTypeDV As DataView

    Private ConsortiumDV As DataView
    Private MapCaseDV As DataView

    Public Sub New()

        MapCaseDT.Columns.Add("DebtorID")
        MapCaseDT.Columns.Add("DataSet")
        MapCaseDT.Columns.Add("Easting")
        MapCaseDT.Columns.Add("Northing")
        MapCaseDT.Columns.Add("Postcode")
        MapCaseDT.Columns.Add("BailiffName")

        LoadDataTable("BailiffAllocation", "EXEC dbo.GetPeriodTypes 'C'", PeriodTypeDT)
        PeriodTypeDV = New DataView(PeriodTypeDT)
        PeriodTypeDV.AllowDelete = False
        PeriodTypeDV.AllowNew = False
    End Sub

    Protected Overrides Sub Finalize()
        If Not IsNothing(LastLoadDV) Then LastLoadDV.Dispose()

        If Not IsNothing(SummaryDV) Then SummaryDV.Dispose()
        If Not IsNothing(WarningSummaryDV) Then WarningSummaryDV.Dispose()
        If Not IsNothing(ClientAllocationLevelDV) Then ClientAllocationLevelDV.Dispose()
        If Not IsNothing(UnderAllocatedClientDV) Then UnderAllocatedClientDV.Dispose()
        If Not IsNothing(DetailDV) Then DetailDV.Dispose()
        If Not IsNothing(PeriodTypeDV) Then PeriodTypeDV.Dispose()

        If Not IsNothing(ConsortiumDV) Then ConsortiumDV.Dispose()
        If Not IsNothing(MapCaseDV) Then MapCaseDV.Dispose()

        MyBase.Finalize()

    End Sub

#Region " Public Properties"

    Public ReadOnly Property LastLoadDataView() As DataView
        Get
            LastLoadDataView = LastLoadDV
        End Get
    End Property

    Public ReadOnly Property SummaryDataView() As DataView
        Get
            SummaryDataView = SummaryDV
        End Get
    End Property

    Public ReadOnly Property WarningSummaryDataView() As DataView
        Get
            WarningSummaryDataView = WarningSummaryDV
        End Get
    End Property
    Public ReadOnly Property DetailDataView() As DataView
        Get
            DetailDataView = DetailDV
        End Get
    End Property

    Public ReadOnly Property PeriodTypeDataView() As DataView
        Get
            PeriodTypeDataView = PeriodTypeDV
        End Get
    End Property

    Public ReadOnly Property ClientAllocationLevelDataView() As DataView
        Get
            ClientAllocationLevelDataView = ClientAllocationLevelDV
        End Get
    End Property

    Public ReadOnly Property UnderAllocatedClientDataView() As DataView
        Get
            UnderAllocatedClientDataView = UnderAllocatedClientDV
        End Get
    End Property

    Public ReadOnly Property ConsortiumDataView() As DataView
        Get
            ConsortiumDataView = ConsortiumDV
        End Get
    End Property

    Public ReadOnly Property MapCaseDataView() As DataView
        Get
            MapCaseDataView = MapCaseDV
        End Get
    End Property

#End Region

#Region "Public Methods"

    Public Sub GetSummary(ByVal ParamList As String, ByVal Abs As Boolean, ByVal DisplaySet As String)
        Dim SummaryParamList As String = "", WarningSummaryParamList As String

        SummaryDV = Nothing ' Needed to clear column list otherwise new columns eg clientname are added at the end
        WarningSummaryDV = Nothing

        If Abs Then SummaryParamList = ParamList & ",'A'" Else SummaryParamList = ParamList & ",'P'"

        SummaryParamList &= "," & DisplaySet
        WarningSummaryParamList = ParamList & "," & DisplaySet

        SummaryDT.Clear()
        SummaryDT.Reset()

        LoadDataTable("BailiffAllocation", "EXEC dbo.GetSummary " & SummaryParamList, SummaryDT)
        SummaryDV = New DataView(SummaryDT)
        SummaryDV.AllowDelete = False
        SummaryDV.AllowNew = False

        WarningSummaryDT.Clear()
        WarningSummaryDT.Reset()

        LoadDataTable("BailiffAllocation", "EXEC dbo.GetWarningSummary " & WarningSummaryParamList, WarningSummaryDT)
        WarningSummaryDV = New DataView(WarningSummaryDT)
        WarningSummaryDV.AllowDelete = False
        WarningSummaryDV.AllowNew = False

        GetClientAllocationLevels()

    End Sub

    Public Sub GetClientAllocationLevels()
        LoadDataTable("BailiffAllocation", "EXEC dbo.GetClientAllocation", ClientAllocationLevelDT)
        ClientAllocationLevelDV = New DataView(ClientAllocationLevelDT)
        ClientAllocationLevelDV.AllowDelete = False
        ClientAllocationLevelDV.AllowNew = False
    End Sub

    Public Sub GetUnderAllocatedClients()
        LoadDataTable("BailiffAllocation", "EXEC dbo.GetUnderAllocatedClients", UnderAllocatedClientDT)
        UnderAllocatedClientDV = New DataView(UnderAllocatedClientDT)
        UnderAllocatedClientDV.AllowDelete = False
        UnderAllocatedClientDV.AllowNew = False
    End Sub

    Public Sub GetCases(ByVal ParamList As String, ByVal Period As String)
        ParamList += "," & Period.ToString

        LoadDataTable("BailiffAllocation", "EXEC dbo.GetCases " & ParamList, DetailDT, True)
        DetailDV = New DataView(DetailDT.DefaultView.ToTable(True))
        DetailDV.AllowDelete = False
        DetailDV.AllowNew = False
    End Sub

    Public Sub GetBailiffCases(ByVal ParamList As String, ByVal Period As String)
        ParamList += "," & Period.ToString

        LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffCases " & ParamList, DetailDT, True)
        DetailDV = New DataView(DetailDT.DefaultView.ToTable(True))
        DetailDV.AllowDelete = False
        DetailDV.AllowNew = False
    End Sub

    Public Sub GetCasesByBailiff(ByVal BailiffID As Integer)
        LoadDataTable("BailiffAllocation", "EXEC dbo.GetCasesByBailiff " & BailiffID.ToString, DetailDT, True)
        DetailDV = New DataView(DetailDT)
        DetailDV.AllowDelete = False
        DetailDV.AllowNew = False
    End Sub

    Public Sub GetConsortiums()
        LoadDataTable("BailiffAllocation", "EXEC dbo.GetConsortiums", ConsortiumDT)
        ConsortiumDV = New DataView(ConsortiumDT)
        ConsortiumDV.AllowDelete = False
        ConsortiumDV.AllowNew = False
    End Sub

    Public Sub GetCasesForMap(ByRef Source As DataGridView)
        Dim DataSetName As String
        MapCaseDT.Clear()

        For Each Row As DataRow In DetailDT.Rows
            DataSetName = ""

            If Not IsNothing(Source.Columns("StageName")) Then DataSetName &= Row.Item("StageName")

            If Not IsNothing(Source.Columns("ClientName")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                DataSetName &= Row.Item("ClientName")
            End If

            If Not IsNothing(Source.Columns("WorkType")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                DataSetName &= Row.Item("WorkType")
            End If

            If Not IsNothing(Source.Columns("Allocated")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                If Row.Item("Allocated") = "Y" Then
                    DataSetName &= "allocated"
                Else
                    DataSetName &= "unallocated"
                End If
            End If

            If Not IsNothing(Source.Columns("SchemeName")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                DataSetName &= Row.Item("SchemeName")
            End If

            If Not IsNothing(Source.Columns("Payment")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                If Row.Item("Payment") = "Y" Then
                    DataSetName &= "payment made"
                Else
                    DataSetName &= "no payment"
                End If
            End If

            If Not IsNothing(Source.Columns("Visited")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                If Row.Item("Visited") = "Y" Then
                    DataSetName &= "visited"
                Else
                    DataSetName &= "no visits"
                End If
            End If

            If Not IsNothing(Source.Columns("Covered")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                DataSetName &= Row.Item("Covered")
            End If

            If Not IsNothing(Source.Columns("Levied")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                If Row.Item("Levied") = "Y" Then
                    DataSetName &= "levied"
                Else
                    DataSetName &= "not levied"
                End If
            End If

            If Not IsNothing(Source.Columns("VanFeesApplied")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                If Row.Item("VanFeesApplied") = "Y" Then
                    DataSetName &= "Van fees applied"
                Else
                    DataSetName &= "Van fees not applied"
                End If
            End If

            If Not IsNothing(Source.Columns("Broken")) Then
                If DataSetName <> "" Then DataSetName &= ", "
                If Row.Item("Broken") = "Y" Then
                    DataSetName &= "broken"
                Else
                    DataSetName &= "not broken"
                End If
            End If

            If DataSetName = "" Then
                DataSetName = "All cases"
            Else
                DataSetName = StrConv(DataSetName, VbStrConv.ProperCase)
            End If


            Dim NewRow As DataRow = MapCaseDT.Rows.Add
            NewRow.Item("DebtorID") = Row.Item("DebtorID")
            NewRow.Item("Dataset") = DataSetName
            NewRow.Item("Easting") = Row.Item("add_os_easting")
            NewRow.Item("Northing") = Row.Item("add_os_northing")
            NewRow.Item("Postcode") = Row.Item("add_postcode")
            NewRow.Item("BailiffName") = Row.Item("BailiffName")
        Next Row

        MapCaseDV = New DataView(MapCaseDT)
        MapCaseDV.AllowDelete = False
        MapCaseDV.AllowNew = False
    End Sub

    Public Sub GetLastLoad()
        LoadDataTable("BailiffAllocation", "EXEC dbo.GetLastLoad", LastLoadDT)
        LastLoadDV = New DataView(LastLoadDT)
        LastLoadDV.AllowDelete = False
        LastLoadDV.AllowNew = False
    End Sub

    Public Sub RefreshDatabase()

        ExecStoredProc("BailiffAllocation", "EXEC dbo.BuildBailiffAllocation 'I'", 1200)

    End Sub

    Public Sub SetClientAllocation(ByVal ClientName As String, ByVal AllocationDays As String, ByVal AllocationQuantity As String)

        ExecStoredProc("BailiffAllocation", "EXEC dbo.SetClientAllocation '" & ClientName & "'," & AllocationDays & "," & AllocationQuantity, 600)

    End Sub
#End Region

End Class


﻿Imports System.IO
Imports System.Collections
Imports CommonLibrary

Public Class frmMain
    Private LowellData As New clsLowellData
    Const Separator As String = "|"
    Private InputFilePath As String, FileName As String, FileExt As String
    Private OutputFiles As New ArrayList()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        'Try
        Dim FileDialog As New OpenFileDialog
        Dim LineNumber As Integer, CaseCount As Integer, FileNum As Integer = 1
        Dim OutputLine As String, OutputFilename As String
        Dim AuditLog As String, ErrorLog As String = "", InputLineArray() As String, DebtorID As String

        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)

        For Each OldFile As String In Directory.GetFiles(InputFilePath, FileName & "_*_PreProcessed.txt")
            File.Delete(OldFile)
        Next OldFile

        Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

        lblReadingFile.Visible = True
        Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
        lblReadingFile.Visible = False

        ProgressBar.Maximum = UBound(FileContents)

        If File.Exists(InputFilePath & FileName & "_Audit.txt") Then File.Delete(InputFilePath & FileName & "_Audit.txt")
        If File.Exists(InputFilePath & FileName & "_Error.txt") Then File.Delete(InputFilePath & FileName & "_Error.txt")

        OutputFilename = InputFilePath & FileName & "_001_PreProcessed.txt"

        For Each InputLine As String In FileContents
            ProgressBar.Value = LineNumber
            LineNumber += 1

            If LineNumber = 1 Then ' validate header
                If InputLine.Split(",")(4) <> UBound(FileContents) - 1 Then ErrorLog &= "Count total in the header (" & InputLine.Split(",")(4) & ") does not match file contents (" & (UBound(FileContents) - 1).ToString & ")." & vbCrLf
                Continue For
            End If

            If LineNumber = 2 Then Continue For ' skip second header line

            OutputLine = ""
            'InputLineArray = Split(InputLine, ",", """") ' Commented out TS 26/Oct/2015. Request ref 64407
            InputLineArray = InputLine.Split(",") ' Added TS 26/Oct/2015. Request ref 64407

            Application.DoEvents() ' without this line, the button disappears until processing is complete

            DebtorID = LowellData.GetDebtorID(InputLineArray(3))

            If DebtorID <> "" Then

                OutputLine &= DebtorID & "|"

                OutputLine &= String.Join("", ToNote(InputLineArray(0), "Created On Clients System", ";"))
                OutputLine &= String.Join("", ToNote(InputLineArray(1), "Last Updated On Clients System", ";"))
                OutputLine &= String.Join("", ToNote(InputLineArray(2), "Client Code", ";"))
                OutputLine &= String.Join("", ToNote(InputLineArray(3), "Debt Code", ";"))
                OutputLine &= String.Join("", ToNote(InputLineArray(5), "Agreement Number", ";"))
                OutputLine &= String.Join("", ToNote(InputLineArray(6), "CRA Reference", ";"))
                OutputLine &= String.Join("", ToNote(InputLineArray(7), "Original Open Date", ";"))
                OutputLine &= String.Join("", ToNote(InputLineArray(8), "Original Loan Agreement Amount", ";"))
                OutputLine &= String.Join("", ToNote(InputLineArray(9), "Regular Instalment Amount", ";"))
                OutputLine &= String.Join("", ToNote(InputLineArray(10), "Agreement Term", ";"))
                OutputLine &= String.Join("", ToNote(InputLineArray(11), "Total Charges For Credit", ";"))
                OutputLine &= String.Join("", ToNote(InputLineArray(12), "Original Balance", ";"))

                OutputLine &= vbCrLf
                CaseCount += 1
            Else
                ErrorLog &= "Cannot find unique DebtorID for client ref " & InputLineArray(0) & " at line number " & LineNumber.ToString & vbCrLf
            End If

            If CaseCount > CInt(txtMaxCasesInOutputFile.Text) Then
                FileNum += 1
                OutputFilename = InputFilePath & FileName & "_" & FileNum.ToString.PadLeft(3, "0") & "_PreProcessed.txt"
                CaseCount = 1
            End If

            AppendToFile(OutputFilename, OutputLine)
            If Not OutputFiles.Contains(OutputFilename) Then OutputFiles.Add(OutputFilename)

        Next InputLine

        AuditLog = LogHeader & "Number of cases: " & (UBound(FileContents) - 1).ToString & vbCrLf

        WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

        If ErrorLog <> "" Then
            WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
            MessageBox.Show("Errors found.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        End If

        MessageBox.Show("Pre-processing complete.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

        btnViewInputFile.Enabled = True
        btnViewOutputFile.Enabled = True
        btnViewLogFiles.Enabled = True

        ProgressBar.Value = 0

        'Catch ex As Exception
        '    HandleException(ex)
        'End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            For Each Filename As String In OutputFiles
                If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
            Next Filename

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String, ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String
    Private ConnCode As New Dictionary(Of String, Integer)
    Private OutputFiles As New ArrayList()
    Private NewCases As New ArrayList()
    Private ConnID As String() = {"3390"}

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        ConnCode.Add("3390", 0)

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim LineNumber As Integer, TotalNewCount As Integer
        Dim ConnKey As String = "", NewDebtNotes As String = "", NewDebtFile(0) As String, NewDebtLine(18) As String, DebtorID As String = ""
        Dim NewDebtSumm(0, 1) As Decimal, TotalNewDebt As Decimal

        Try
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            Cursor = Cursors.WaitCursor

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)

            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            lblReadingFile.Visible = True

            Dim FileContents(,) As String = InputFromExcel(FileDialog.FileName)
            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents) + 2 ' audit file and output file

            For LineNumber = 1 To UBound(FileContents)
                ProgressBar.Value = LineNumber

                Application.DoEvents() ' without this line, the button disappears until processing is complete
                Array.Clear(NewDebtLine, 0, NewDebtLine.Length)
                NewDebtNotes = ""
                ConnKey = "3390"

                NewDebtLine(0) = FileContents(LineNumber, 0) ' ClaimantName
                NewDebtLine(1) = FileContents(LineNumber, 1) ' prevReference
                NewDebtLine(2) = FileContents(LineNumber, 2) ' offenceLocation
                NewDebtLine(3) = FileContents(LineNumber, 3) ' ClientRef
                If FileContents(LineNumber, 4) <> "" Then NewDebtNotes = Join(ToNote(FileContents(LineNumber, 4), "Contact Name", ";"), "")
                NewDebtLine(4) = FileContents(LineNumber, 5) ' offenceReg
                NewDebtLine(5) = FileContents(LineNumber, 6) ' debtAmount
                If FileContents(LineNumber, 6) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 6), "First pay coll ID", ";"), "")
                If FileContents(LineNumber, 7) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 7), "First pay coll Nm", ";"), "")
                NewDebtLine(6) = FileContents(LineNumber, 9) ' offenceNumber
                NewDebtLine(7) = FileContents(LineNumber, 10) ' nameTitle
                NewDebtLine(8) = (FileContents(LineNumber, 11) & " " & FileContents(LineNumber, 12)).Trim ' nameFore
                NewDebtLine(9) = FileContents(LineNumber, 13) ' nameSur
                NewDebtLine(10) = ConcatAddressFields({FileContents(LineNumber, 14), FileContents(LineNumber, 15), FileContents(LineNumber, 16), FileContents(LineNumber, 17), FileContents(LineNumber, 18), FileContents(LineNumber, 19)}, " ") ' Address
                NewDebtLine(11) = FileContents(LineNumber, 20) ' addPostcode
                NewDebtLine(12) = FileContents(LineNumber, 21) ' addPhone
                NewDebtLine(13) = FileContents(LineNumber, 22) ' addFax
                NewDebtLine(14) = FileContents(LineNumber, 23) ' EmpPhone
                NewDebtLine(15) = FileContents(LineNumber, 24) ' EmpFax
                NewDebtLine(16) = FileContents(LineNumber, 25) ' addEmail
                NewDebtLine(17) = FileContents(LineNumber, 26) ' dateOfBirth
                If FileContents(LineNumber, 27) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 27), "Lenders ref", ";"), "")
                If FileContents(LineNumber, 28) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 28), "Loan open date", ";"), "")
                If FileContents(LineNumber, 29) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 29), "Orig due date", ";"), "")
                If FileContents(LineNumber, 30) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 30), "Charge off date", ";"), "")
                If FileContents(LineNumber, 31) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 31), "Orig loan princ amount", ";"), "")
                If FileContents(LineNumber, 32) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 32), "Loan fixed interest", ";"), "")
                If FileContents(LineNumber, 33) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 33), "Additional refinance fees", ";"), "")
                If FileContents(LineNumber, 34) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 34), "Failed direct debit fees", ";"), "")
                If FileContents(LineNumber, 35) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 35), "Amount paid", ";"), "")
                If FileContents(LineNumber, 36) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 36), "Bank", ";"), "")
                If FileContents(LineNumber, 37) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 37), "Sort code", ";"), "")
                If FileContents(LineNumber, 38) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 38), "Bank ac no", ";"), "")
                If FileContents(LineNumber, 39) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 39), "No of completed prior loans", ";"), "")
                If FileContents(LineNumber, 40) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 40), "1st visit to lenders website", ";"), "")
                If FileContents(LineNumber, 41) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 41), "Employer", ";"), "")
                If FileContents(LineNumber, 42) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 42), "Work department", ";"), "")
                If FileContents(LineNumber, 43) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 43), "Supervisors name", ";"), "")
                If FileContents(LineNumber, 44) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 44), "Monthly net salary", ";"), "")
                If FileContents(LineNumber, 45) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 45), "Salary frequency", ";"), "")
                NewDebtLine(18) = FileContents(LineNumber, 46) ' empNI
                If FileContents(LineNumber, 47) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 47), "Drivers licence", ";"), "")
                If FileContents(LineNumber, 48) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 48), "Bad address home", ";"), "")
                If FileContents(LineNumber, 49) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 49), "Own or rent", ";"), "")
                If FileContents(LineNumber, 50) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 50), "Last payment date", ";"), "")
                If FileContents(LineNumber, 51) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 51), "Last payment date", ";"), "")
                If FileContents(LineNumber, 52) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 52), "Employer phone", ";"), "")
                If FileContents(LineNumber, 53) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 53), "Employer post code", ";"), "")
                If FileContents(LineNumber, 54) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 54), "Check date", ";"), "")
                If FileContents(LineNumber, 55) <> "" Then NewDebtNotes &= Join(ToNote(FileContents(LineNumber, 55), "Card details", ";"), "")

                'NewDebtFile(ConnCode(ConnKey)) &= Join(NewDebtLine, "|") & "|" & NewDebtNotes & vbCrLf
                DebtorID = GetDebtorID(FileContents(LineNumber, 3)) 'temp
                If DebtorID = "" Then DebtorID = "Error-" & FileContents(LineNumber, 3) ' temp
                'NewDebtFile(ConnCode(ConnKey)) &= DebtorID & "," & NewDebtNotes & vbCrLf 'temp
                NewDebtFile(ConnCode(ConnKey)) &= DebtorID & "," & FileContents(LineNumber, 1) & vbCrLf 'temp

                NewDebtSumm(ConnCode(ConnKey), 0) += 1 ' Number of debt records
                NewDebtSumm(ConnCode(ConnKey), 1) += FileContents(LineNumber, 6) ' Value of debt records

            Next LineNumber

            ProgressBar.Value += 1
            AuditLog = "File processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            For NewDebtFileCount As Integer = 0 To UBound(NewDebtFile)
                ProgressBar.Value += 1
                If NewDebtFile(NewDebtFileCount) <> "" Then
                    WriteFile(InputFilePath & FileName & "_NewDebt.txt", NewDebtFile(NewDebtFileCount))
                    OutputFiles.Add(InputFilePath & FileName & "_NewDebt.txt")
                    AuditLog &= "Clientscheme " & ConnID(NewDebtFileCount) & vbCrLf
                    AuditLog &= "Number of new cases: " & NewDebtSumm(NewDebtFileCount, 0).ToString & vbCrLf
                    AuditLog &= "Total balance of new cases: " & NewDebtSumm(NewDebtFileCount, 1).ToString & vbCrLf & vbCrLf
                    TotalNewCount += NewDebtSumm(NewDebtFileCount, 0)
                    TotalNewDebt += NewDebtSumm(NewDebtFileCount, 1)
                End If
            Next NewDebtFileCount

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)
            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", ExceptionLog)

            Cursor = Cursors.Default

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If AuditLog <> "" And File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If ErrorLog <> "" And File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If ExceptionLog <> "" And File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub

    Public Function GetDebtorID(ByVal ClientRef As String) As String  ' temp

        GetDebtorID = GetSQLResults("DebtRecovery", "SELECT d._rowID " & _
                                                    "FROM debtor AS d " & _
                                                    "INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID " & _
                                                    "WHERE d.Client_Ref = '" & ClientRef & "' " & _
                                                    "  AND cs.clientID = 1792")
    End Function
End Class

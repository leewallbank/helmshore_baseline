﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization
Imports System.Net.Mail

Public Class frmMain
    Const Separator As String = ","
    Private ConnCode As New Dictionary(Of String, Integer)
    Private CustomerType As New Dictionary(Of String, String)
    Private PersonType As New Dictionary(Of String, String)
    Private Nationality As New Dictionary(Of String, String)
    Private IDType As New Dictionary(Of String, String)
    Private PersonAddressType As New Dictionary(Of String, String)
    Private ConnID As String() = {"4529", "4623", "4624", "4625", "4626", "4627", "4628", "4629", "4630", "4631", _
                                  "4632", "4639", "4640", "4641", "4642", "4643", "4644", "4645", "4646", "4647", _
                                  "4648", "4649", "4650", "4651", "4652", "4653", "4729", "4730", "4731", "4732", _
                                  "4733", "4734", "4735", "4740"}
    Private ConnIndex As Integer
    Private InputFilePath As String, FileName As String, FileExt As String, ReturnFileName As String
    Private OutputFiles As New ArrayList()
    Private NewCases As New ArrayList()

    Private PreProcessData As New clsLowellPreProcessData

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        ConnCode.Add("PL1|Financial Services|VH", 1)
        ConnCode.Add("PL2|Financial Services|VH", 2)
        ConnCode.Add("PL3|Financial Services|VH", 3)
        ConnCode.Add("PL4|Financial Services|VH", 4)
        ConnCode.Add("PL2|Financial Services|M", 5)
        ConnCode.Add("PL3|Financial Services|M", 6)
        ConnCode.Add("PL4|Financial Services|M", 7)
        ConnCode.Add("PL4|Financial Services|L", 8)
        ConnCode.Add("PL1|Communications|VH", 9)
        ConnCode.Add("PL1|Mail Order|VH", 10)
        ConnCode.Add("PL1|Financial Services|H", 11)
        ConnCode.Add("PL1|Financial Services|M", 12)
        ConnCode.Add("PL1|Communications|H", 13)
        ConnCode.Add("PL1|Communications|M", 14)
        ConnCode.Add("PL4|Financial Services|H", 15)
        ConnCode.Add("PL4|Communications|VH", 16)
        ConnCode.Add("PL4|Communications|H", 17)
        ConnCode.Add("PL4|Communications|M", 18)
        ConnCode.Add("PL2|Financial Services|H", 19)
        ConnCode.Add("PL2|Communications|H", 20)
        ConnCode.Add("PL2|Communications|M", 21)
        ConnCode.Add("PL3|Financial Services|H", 22)
        ConnCode.Add("PL3|Communications|H", 23)
        ConnCode.Add("PL3|Communications|M", 24)
        ConnCode.Add("PL1|Mail Order|M", 25)
        ConnCode.Add("PL2|Communications|VH", 26)
        ConnCode.Add("PL3|Communications|VH", 27)
        ConnCode.Add("PL4|Communications|L", 28)
        ConnCode.Add("PL1|Financial Services|L", 29)
        ConnCode.Add("PL3|Financial Services|L", 30)
        ConnCode.Add("PL3|Communications|L", 31)
        ConnCode.Add("PL1|Communications|L", 32)
        ConnCode.Add("PL2|Financial Services|L", 33) ' Added TS 22/Oct/2015

        'ConnCode.Add("PFS1", 1)
        'ConnCode.Add("SFS1", 2)
        'ConnCode.Add("TFS1", 3)
        'ConnCode.Add("QFS1", 4)
        'ConnCode.Add("SFS3", 5)
        'ConnCode.Add("TFS3", 6)
        'ConnCode.Add("QFS3", 7)
        'ConnCode.Add("QFS4", 8)
        'ConnCode.Add("PTE1", 9)
        'ConnCode.Add("PMO1", 10)
        'ConnCode.Add("PFS2", 11)
        'ConnCode.Add("PFS3", 12)
        'ConnCode.Add("PTE2", 13)
        'ConnCode.Add("PTE3", 14)
        'ConnCode.Add("QFS2", 15)
        'ConnCode.Add("QTE1", 16)
        'ConnCode.Add("QTE2", 17)
        'ConnCode.Add("QTE3", 18)
        'ConnCode.Add("SFS2", 19)
        'ConnCode.Add("STE2", 20)
        'ConnCode.Add("STE3", 21)
        'ConnCode.Add("TFS2", 22)
        'ConnCode.Add("TTE2", 23)
        'ConnCode.Add("TTE3", 24)
        'ConnCode.Add("PMO3", 25)
        'ConnCode.Add("PL1", 25)

        CustomerType.Add("COR", "Corporate")
        CustomerType.Add("IND", "Individual")
        CustomerType.Add("JSH", "Joint Account Holder")
        CustomerType.Add("RES", "Residential")
        CustomerType.Add("SME", "Small Business")

        PersonAddressType.Add("BAD", "Business Address")
        PersonAddressType.Add("CAD", "Current Address")
        PersonAddressType.Add("EAD", "Employers Address")
        PersonAddressType.Add("INT", "International Address")
        PersonAddressType.Add("PAD", "Previous Address")
        PersonAddressType.Add("SAD", "Service Address")

        Nationality.Add("GB", "UK")
        Nationality.Add("BE", "Belgium")
        Nationality.Add("FI", "Finland")
        Nationality.Add("FR", "France")
        Nationality.Add("DE", "Germany")
        Nationality.Add("GR", "Greece")

        IDType.Add("BC", "Birth Certificate")
        IDType.Add("BILL", "Bill (e.g.: utility)")
        IDType.Add("NIN", "National Insurance Number")
        IDType.Add("OTH", "Other")
        IDType.Add("PAS", "Passport")
        IDType.Add("STA", "Statement (e.g.: Bank/Credit Card)")
        IDType.Add("UKN", "Unknown")


        PersonType.Add("AUT", "Authorised User")
        PersonType.Add("GUA", "Guarantor")
        PersonType.Add("JAH", "Joint Account Holder")
        PersonType.Add("PAH", "Primary Account Holder")
        PersonType.Add("SAH", "Secondary Account Holder")

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String, InputLineArray() As String, ConnKey As String, DebtNotes As String, RejectionFile As String = Nothing, ReturnLine As String, ConfirmationCode As String = "", ConfirmationReason As String = ""
        Dim NewDebtFile(UBound(ConnID)) As String
        Dim NewDebtSumm(UBound(ConnID), 1) As Decimal, TotalNewDebt As Decimal, ParseValueDecimal As Decimal
        Dim LineNumber As Integer, CaseCount As Integer

        Try
            FileDialog.Filter = "Lowell assignment files|*.csv|All files (*.*)|*.*"
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            For Each OldFile As String In Directory.GetFiles(InputFilePath, "*" & FileName & "_*.txt")
                File.Delete(OldFile)
            Next OldFile

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents) + (UBound(NewDebtFile) + 1) + 1 ' +1 for audit log

            PreProcessData.GetLastRunDetails()

            ReturnFileName = "CONFIRMATION_ROSSENDALES_ONBB_RON7373_" & DateTime.Today.ToString("ddMMyyyy") & (PreProcessData.LastReturnDetails.FileSequence + 1).ToString & ".csv"
            If IO.File.Exists(InputFilePath & ReturnFileName) Then IO.File.Delete(InputFilePath & ReturnFileName)

            For RowCount As Integer = 2 To UBound(FileContents)
                InputLineArray = FileContents(RowCount).Split(Separator)
                If Decimal.TryParse(InputLineArray(4), ParseValueDecimal) Then TotalNewDebt += CDec(InputLineArray(4))
            Next RowCount

            ' validate headers
            CaseCount = UBound(FileContents) - 1
            If FileContents(0).Split(",")(3) <> TotalNewDebt Then ErrorLog &= "Total value does not match header" & vbCrLf
            If FileContents(0).Split(",")(4) <> CaseCount Then ErrorLog &= "Adjustment count does not match header" & vbCrLf
            If FileContents(1) <> "AccountNumber,ClientReferenceNumber,CurrencyCode,ClientName,ClientBalance,ClientBalanceDate,Brand,CountryOfOrigin,AssignmentTypeCode,TotalAmountAssigned,SegmentName,PrimaryCustomerTypeCode,PersonTypeCode,PrimarySalutation,PrimaryFirstName,PrimaryLastName,PrimaryDateOfBirth,PrimaryDebtorNationality,PrimaryIDTypeCode1,PrimaryIDDetail1,PrimaryEmail,OriginalClientName,PrimaryBankAccount,PrimaryPhoneTypeCode1,PrimaryTelephoneNo1,PrimaryPhoneTypeCode2,PrimaryTelephoneNo2,PrimaryPhoneTypeCode3,PrimaryTelephoneNo3,SecondaryPersonTypeCode,SecondarySalutation,SecondaryFirstName,SecondaryLastName,SecondaryDateOfBirth,SecondaryDebtorNationality,SecondaryIDTypeCode1,SecondaryIDDetail1,SecondaryEmail,SecondarySpecialNeeds,SecondaryBankAccount,SecondaryPhoneTypeCode1,SecondaryTelephoneNo1,SecondaryPhoneTypeCode2,SecondaryTelephoneNo2,SecondaryPhoneTypeCode3,SecondaryTelephoneNo3,PersonAddressTypeCode1,Address1Line1,Address1Line2,Address1Line3,Address1Line4,Address1Line5,Address1Country,Address1PostCode,PersonAddressTypeCode2,Address2Line1,Address2Line2,Address2Line3,Address2Line4,Address2Line5,Address2Country,Address2PostCode,PersonAddressTypeCode3,Address3Line1,Address3Line2,Address3Line3,Address3Line4,Address3Line5,Address3Country,Address3PostCode,LastPaymentAmount,LastPaymentDate,NumberOfPaymentsInColls,ValueOfPaymentsInColls,PaymentMethod,DefaultDate,DefaultAmount,TerminationReason,CoreDebtAmount,ChargesToDate,InterestToDate,PrimaryProductNumber,PrimaryProductDescription,PrimaryProductValue,PrimaryProductOpenDate,PrimaryProductCloseDate,Reading1,Reading2,Reading3,AssociatedProduct1ProductNumber,AssociatedProduct1ProductDescription,AssociatedProduct1ProductValue,AssociatedProduct1ProductOpenDate,AssociatedProduct1ProductCloseDate,AssociatedProduct1Reading1,AssociatedProduct1Reading2,AssociatedProduct1Reading3,AssociatedProduct2ProductNumber,AssociatedProduct2ProductDescription,AssociatedProduct2ProductValue,AssociatedProduct2ProductOpenDate,AssociatedProduct2ProductCloseDate,AssociatedProduct2Reading1,AssociatedProduct2Reading2,AssociatedProduct2Reading3,AssociatedProduct3ProductNumber,AssociatedProduct3ProductDescription,AssociatedProduct3ProductValue,AssociatedProduct3ProductOpenDate,AssociatedProduct3ProductCloseDate,AssociatedProduct3Reading1,AssociatedProduct3Reading2,AssociatedProduct3Reading3,LitigationStatus,ContractAvailable,Region,Fee_Perc,FlatFeeAmount,VulnerableFlag,ClientComments,IGTFlag,DisconnectionFlag,PostLitigationFlag,AccountScore,ONBID" Then ErrorLog &= "Unexpected column headers" & vbCrLf

            If ErrorLog = "" Then

                For Each InputLine As String In FileContents
                    ProgressBar.Value = LineNumber
                    LineNumber += 1
                    ConnIndex = -1

                    Application.DoEvents() ' without this line, the button disappears until processing is complete

                    InputLineArray = InputLine.Split(",")

                    ReturnLine = InputLine

                    If LineNumber = 2 Then ReturnLine &= ",ConfirmationCode,ConfirmationReason"

                    If LineNumber > 2 Then
                        ConfirmationCode = ""
                        ConfirmationReason = ""

                        ' Perform fail checks first' These are in the same order as the specification verson 0.4
                        If PreProcessData.GetCaseCount(InputLineArray(0)) > 0 Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "No current placement with the DCA receiving the transaction. "
                        End If

                        If InputLineArray(1) = "" Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "The ClientReferenceNumber has not been supplied. "
                        End If

                        If Not {"PL1", "PL2", "PL3", "PL4", ""}.Contains(InputLineArray(8)) Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "The AssignmentTypeCode has been supplied incorrectly (Placement Number). "
                        End If

                        If InputLineArray(3) = "" Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "The ClientName has not been supplied. "
                        End If

                        Decimal.TryParse(InputLineArray(4), ParseValueDecimal)
                        If ParseValueDecimal < 10 Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "The Balance of the account is too low (Below £10). "
                        End If

                        If InputLineArray(6) = "" Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "The Brand has not been supplied. "
                        End If

                        If InputLineArray(10) = "" Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "The Segment has not been provided. "
                        End If

                        If InputLineArray(15) = "" Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "The Customers Last Name has not been provided. "
                        End If

                        If InputLineArray(47) = "" Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "The Customers 1st Address line has now been provided. "
                        End If

                        If InputLineArray(123) = "" Then
                            ConfirmationCode = "FAIL"
                            ConfirmationReason &= "The Account Score has not been provided. "
                        End If

                        ' else success
                        If ConfirmationCode = "" Then
                            ConfirmationCode = "ACRU"
                            ConfirmationReason = "The account has been accepted and loaded by the DCA"
                        End If

                        ReturnLine &= Separator & ConfirmationCode & Separator & ConfirmationReason.TrimEnd

                    End If

                    AppendToFile(InputFilePath & ReturnFileName, ReturnLine & vbCrLf)

                    ' The case only goes into the output file if it has passed the above validation
                    If ConfirmationCode = "ACRU" Then

                        ConnKey = InputLineArray(8) & "|" & InputLineArray(10) & "|" & InputLineArray(123)
                        'If ConnCode.ContainsKey(ConnKey) Then

                        If ConnKey.IndexOf("Standard Utilities") > -1 Then
                            ConnIndex = 0
                        Else
                            If ConnCode.ContainsKey(ConnKey) Then ConnIndex = ConnCode(ConnKey)
                        End If

                        If ConnIndex <> -1 Then

                            ' all occurrences of ConnCode(ConnKey) changed to ConnIndex after this point. TS 22/May/2015. Request ref 50525

                            If InputLineArray(8) = "" Then InputLineArray(8) = "PL1"

                            'If InputLineArray(19) = "" OrElse CurrentAge(DateTime.Parse(InputLineArray(17))) < 70 Then commented out TS 07/Jul/2016. Request ref 88680
                            'If InputLineArray(37) = "" OrElse CurrentAge(DateTime.Parse(InputLineArray(33))) < 70 Then commented out TS 07/Jul/2016. Request ref 88680
                            If InputLineArray(16) = "" OrElse CurrentAge(DateTime.Parse(InputLineArray(16))) < 70 Then
                                If InputLineArray(33) = "" OrElse CurrentAge(DateTime.Parse(InputLineArray(33))) < 70 Then

                                    NewDebtFile(ConnIndex) = Join(InputLineArray, Separator) & _
                                                                        Separator

                                    DebtNotes = ToNote(InputLineArray(4), "Outstanding balance as per Clients system", ";")(0) & _
                                                ToNote(InputLineArray(6), "Client Brand", ";")(0) & _
                                                ToNote(InputLineArray(7), "Debt country of origin", ";")(0) & _
                                                ToNote(InputLineArray(8), "Assignment Type Code", ";")(0) & _
                                                ToNote(InputLineArray(9), "Total Amount Assigned", ";")(0) & _
                                                ToNote(InputLineArray(10), "Segment Name", ";")(0)

                                    If CustomerType.ContainsKey(InputLineArray(11)) Then
                                        DebtNotes &= ToNote(CustomerType(InputLineArray(11)), "Small Business or Residential Customer", ";")(0)
                                    Else
                                        DebtNotes &= ToNote("??", "Small Business or Residential Customer", ";")(0)
                                        ErrorLog &= "Unexpected PrimaryCustomerTypeCode of " & InputLineArray(11) & " found at line number " & LineNumber.ToString & vbCrLf
                                    End If

                                    DebtNotes &= ToNote(InputLineArray(12), "Person Type Code", ";")(0)

                                    If PersonType.ContainsKey(InputLineArray(12)) Then
                                        DebtNotes &= ToNote(PersonType(InputLineArray(12)), "Primary Person", ";")(0)
                                    Else
                                        DebtNotes &= ToNote("??", "Primary Person", ";")(0)
                                        ErrorLog &= "Unexpected PersonTypeCode of " & InputLineArray(12) & " found at line number " & LineNumber.ToString & vbCrLf
                                    End If

                                    If Nationality.ContainsKey(InputLineArray(17)) Then
                                        DebtNotes &= ToNote(Nationality(InputLineArray(17)), "Main Debtor Nationality", ";")(0)
                                    Else
                                        DebtNotes &= ToNote(InputLineArray(17), "Main Debtor Nationality", ";")(0)
                                    End If

                                    DebtNotes &= ToNote(InputLineArray(18), "PrimaryIDTypeCode", ";")(0)

                                    If IDType.ContainsKey(InputLineArray(18)) Then
                                        DebtNotes &= ToNote(IDType(InputLineArray(18)), "PrimaryIDTypeDescription", ";")(0)
                                    Else
                                        DebtNotes &= ToNote(InputLineArray(18), "PrimaryIDTypeDescription", ";")(0)
                                    End If

                                    DebtNotes &= ToNote(InputLineArray(19), "Primary ID Detail1", ";")(0) & _
                                                 ToNote(InputLineArray(21), "Original Client Name", ";")(0) & _
                                                 ToNote(InputLineArray(22), "Debtor 1 Bank Account Number", ";")(0) & _
                                                 ToNote(InputLineArray(32), "Name 2 TDX unique identifier", ";")(0) & _
                                                 ToNote(InputLineArray(29), "SecondaryPersonTypeCode", ";")(0)

                                    If PersonType.ContainsKey(InputLineArray(29)) Then
                                        DebtNotes &= ToNote(PersonType(InputLineArray(29)), "Name 2 type description", ";")(0)
                                    ElseIf Not String.IsNullOrEmpty(InputLineArray(29)) Then
                                        DebtNotes &= ToNote("??", "Name 2 type description", ";")(0)
                                        ErrorLog &= "Unexpected SecondaryPersonTypeCode of " & InputLineArray(29) & " found at line number " & LineNumber.ToString & vbCrLf
                                    End If

                                    DebtNotes &= ToNote(InputLineArray(33), "Name 2 DOB", ";")(0)

                                    If Nationality.ContainsKey(InputLineArray(34)) Then
                                        DebtNotes &= ToNote(Nationality(InputLineArray(34)), "Debtor 2 Nationality", ";")(0)
                                    Else
                                        DebtNotes &= ToNote(InputLineArray(34), "Debtor 2 Nationality", ";")(0)
                                    End If

                                    DebtNotes &= ToNote(InputLineArray(35), "Debtor 2 ID type", ";")(0)

                                    If IDType.ContainsKey(InputLineArray(35)) Then
                                        DebtNotes &= ToNote(IDType(InputLineArray(35)), "SecondaryIDTypeDescription", ";")(0)
                                    Else
                                        DebtNotes &= ToNote(InputLineArray(35), "SecondaryIDTypeDescription", ";")(0)
                                    End If

                                    DebtNotes &= ToNote(InputLineArray(36), "Debtor 2 ID Detail 1", ";")(0) & _
                                                 ToNote(InputLineArray(37), "Debtor 2 Email", ";")(0) & _
                                                 ToNote(InputLineArray(38), "Debtor 2 special needs", ";")(0) & _
                                                 ToNote(InputLineArray(39), "Debtor 2 Bank Account Number", ";")(0) & _
                                                 ToNote(InputLineArray(40), "SecondaryPhoneTypeCode1", ";")(0) & _
                                                 ToNote(InputLineArray(41), "Debtor 2 Home Tel Number", ";")(0) & _
                                                 ToNote(InputLineArray(42), "SecondaryPhoneTypeCode2", ";")(0) & _
                                                 ToNote(InputLineArray(43), "Debtor 2 Work Tel Number", ";")(0) & _
                                                 ToNote(InputLineArray(44), "SecondaryPhoneTypeCode3", ";")(0) & _
                                                 ToNote(InputLineArray(45), "SecondaryTelephoneNo3", ";")(0) & _
                                                 ToNote(InputLineArray(46), "PersonAddressTypeCode1", ";")(0)

                                    If PersonAddressType.ContainsKey(InputLineArray(46)) Then
                                        DebtNotes &= ToNote(PersonAddressType(InputLineArray(46)), "PersonAddressType1", ";")(0)
                                    Else
                                        DebtNotes &= ToNote(InputLineArray(46), "PersonAddressType1", ";")(0)
                                    End If

                                    DebtNotes &= ToNote(InputLineArray(54), "PersonAddressTypeCode2", ";")(0)

                                    If PersonAddressType.ContainsKey(InputLineArray(54)) Then
                                        DebtNotes &= ToNote(PersonAddressType(InputLineArray(54)), "PersonAddressType2", ";")(0)
                                    Else
                                        DebtNotes &= ToNote(InputLineArray(54), "PersonAddressType2", ";")(0)
                                    End If

                                    DebtNotes &= ToNote(InputLineArray(62), "PersonAddressTypeCode3", ";")(0)

                                    If PersonAddressType.ContainsKey(InputLineArray(62)) Then
                                        DebtNotes &= ToNote(PersonAddressType(InputLineArray(62)), "PersonAddressType3", ";")(0)
                                    Else
                                        DebtNotes &= ToNote(InputLineArray(62), "PersonAddressType3", ";")(0)
                                    End If

                                    DebtNotes &= ToNote(ConcatFields({InputLineArray(63), InputLineArray(64), InputLineArray(65), InputLineArray(66), InputLineArray(67), InputLineArray(68), InputLineArray(69)}, ",") _
                                                        , "Previous Address", ";")(0)

                                    DebtNotes &= ToNote(InputLineArray(70), "Last Payment Amount", ";")(0) & _
                                                 ToNote(InputLineArray(71), "Last Payment Date", ";")(0) & _
                                                 ToNote(InputLineArray(72), "Total number of payments made by the debtor to date", ";")(0) & _
                                                 ToNote(InputLineArray(73), "Value of payments made to date by debtor", ";")(0) & _
                                                 ToNote(InputLineArray(74), "Method in which the debtor usually pays", ";")(0) & _
                                                 ToNote(InputLineArray(75), "Default date", ";")(0) & _
                                                 ToNote(InputLineArray(76), "Default amount", ";")(0) & _
                                                 ToNote(InputLineArray(77), "Termination reason", ";")(0) & _
                                                 ToNote(InputLineArray(78), "Amount owing to date not including fees", ";")(0) & _
                                                 ToNote(InputLineArray(79), "Fees charged to date", ";")(0)

                                    DebtNotes &= ToNote(InputLineArray(80), "Interest to date", ";")(0) & _
                                                 ToNote(InputLineArray(81), "PrimaryProductNumber", ";")(0) & _
                                                 ToNote(InputLineArray(82), "PrimaryProductDescription", ";")(0) & _
                                                 ToNote(InputLineArray(83), "PrimaryProductValue", ";")(0) & _
                                                 ToNote(InputLineArray(84), "PrimaryProductOpenDate", ";")(0) & _
                                                 ToNote(InputLineArray(85), "PrimaryProductCloseDate", ";")(0) & _
                                                 ToNote(InputLineArray(86), "Reading1", ";")(0) & _
                                                 ToNote(InputLineArray(87), "Reading2", ";")(0) & _
                                                 ToNote(InputLineArray(88), "Reading3", ";")(0) & _
                                                 ToNote(InputLineArray(89), "AssociatedProduct1ProductNumber", ";")(0)

                                    DebtNotes &= ToNote(InputLineArray(90), "AssociatedProduct1ProductDescription", ";")(0) & _
                                                 ToNote(InputLineArray(91), "AssociatedProduct1ProductValue", ";")(0) & _
                                                 ToNote(InputLineArray(92), "AssociatedProduct1ProductOpenDate", ";")(0) & _
                                                 ToNote(InputLineArray(93), "AssociatedProduct1ProductCloseDate", ";")(0) & _
                                                 ToNote(InputLineArray(94), "AssociatedProduct1Reading1", ";")(0) & _
                                                 ToNote(InputLineArray(95), "AssociatedProduct1Reading2", ";")(0) & _
                                                 ToNote(InputLineArray(96), "AssociatedProduct1Reading3", ";")(0) & _
                                                 ToNote(InputLineArray(97), "AssociatedProduct2ProductNumber", ";")(0) & _
                                                 ToNote(InputLineArray(98), "AssociatedProduct2ProductDescription", ";")(0) & _
                                                 ToNote(InputLineArray(99), "AssociatedProduct2ProductValue", ";")(0)

                                    DebtNotes &= ToNote(InputLineArray(100), "AssociatedProduct2OpenDate", ";")(0) & _
                                                 ToNote(InputLineArray(101), "AssociatedProduct2CloseDate", ";")(0) & _
                                                 ToNote(InputLineArray(102), "AssociatedProduct2Reading1", ";")(0) & _
                                                 ToNote(InputLineArray(103), "AssociatedProduct2Reading2", ";")(0) & _
                                                 ToNote(InputLineArray(104), "AssociatedProduct2Reading3", ";")(0) & _
                                                 ToNote(InputLineArray(105), "AssociatedProduct3ProductNumber", ";")(0) & _
                                                 ToNote(InputLineArray(106), "AssociatedProduct3ProductDescription", ";")(0) & _
                                                 ToNote(InputLineArray(107), "AssociatedProduct3ProductValue", ";")(0) & _
                                                 ToNote(InputLineArray(108), "AssociatedProduct3OpenDate", ";")(0) & _
                                                 ToNote(InputLineArray(109), "AssociatedProduct3CloseDate", ";")(0)

                                    DebtNotes &= ToNote(InputLineArray(110), "AssociatedProduct3Reading1", ";")(0) & _
                                                 ToNote(InputLineArray(111), "AssociatedProduct3Reading2", ";")(0) & _
                                                 ToNote(InputLineArray(112), "AssociatedProduct3Reading3", ";")(0) & _
                                                 ToNote(InputLineArray(113), "Litigation status", ";")(0) & _
                                                 ToNote(InputLineArray(114), "Contract available", ";")(0) & _
                                                 ToNote(InputLineArray(115), "Region", ";")(0) & _
                                                 ToNote(InputLineArray(116), "Fee%", ";")(0) & _
                                                 ToNote(InputLineArray(117), "Flat fee amount", ";")(0) & _
                                                 ToNote(InputLineArray(118), "Vulnerable", ";")(0)

                                    DebtNotes &= String.Join("", ToNote(InputLineArray(119), "Client Comments", ";")) & _
                                                 ToNote(InputLineArray(120), "IGT Flag", ";")(0) & _
                                                 ToNote(InputLineArray(121), "Disconnection flag", ";")(0) & _
                                                 ToNote(InputLineArray(122), "Post litigation flag", ";")(0) & _
                                                 ToNote(InputLineArray(121), "Lowell Account Score", ";")(0) & vbCrLf

                                    NewDebtFile(ConnIndex) &= DebtNotes

                                    NewDebtSumm(ConnIndex, 0) += 1
                                    NewDebtSumm(ConnIndex, 1) += InputLineArray(4)

                                    AppendToFile(InputFilePath & ConnID(ConnIndex) & "_" & FileName & "_PreProcessed.txt", NewDebtFile(ConnIndex))

                                Else
                                    RejectionFile &= InputLineArray(0) & "," & InputLineArray(1) & vbCrLf
                                    ErrorLog &= "Debtor 2 aged 70 or more at line number " & LineNumber.ToString & ". Case not loaded." & vbCrLf
                                End If

                            Else
                                RejectionFile &= InputLineArray(0) & "," & InputLineArray(1) & vbCrLf
                                ErrorLog &= "Debtor 1 aged 70 or more at line number " & LineNumber.ToString & ". Case not loaded." & vbCrLf
                            End If

                        Else
                            ErrorLog &= "Unable to identify client scheme at line number " & LineNumber.ToString & vbCrLf

                        End If

                    End If

                Next InputLine

                ProgressBar.Value += 1

                AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
                AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
                AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

                Dim TotalNewDebtOutput As Decimal
                Dim CaseCountOutput As Integer

                For NewDebtFileCount As Integer = 0 To UBound(NewDebtFile)
                    ProgressBar.Value += 1
                    If NewDebtFile(NewDebtFileCount) <> "" Then
                        OutputFiles.Add(InputFilePath & ConnID(NewDebtFileCount) & "_" & FileName & "_PreProcessed.txt")
                        AuditLog &= "Clientscheme: " & ConnID(NewDebtFileCount) & " - " & PreProcessData.GetConnIDName(ConnID(NewDebtFileCount)) & vbCrLf
                        AuditLog &= "Number of new cases: " & NewDebtSumm(NewDebtFileCount, 0).ToString & vbCrLf
                        AuditLog &= "Total balance of new cases: " & NewDebtSumm(NewDebtFileCount, 1).ToString & vbCrLf & vbCrLf

                        CaseCountOutput += NewDebtSumm(NewDebtFileCount, 0)
                        TotalNewDebtOutput += NewDebtSumm(NewDebtFileCount, 1)
                    End If
                Next NewDebtFileCount

                WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

                If CaseCount <> CaseCountOutput Or TotalNewDebt <> TotalNewDebtOutput Then MessageBox.Show("Output files do not validate with the original.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error) ' Added TS 07/Jul/2016

            Else

            End If

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
                MessageBox.Show("Errors found.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", LogHeader & ExceptionLog)

            If Not RejectionFile Is Nothing Then
                MsgBox("Cases have been rejected due to debtor age.", vbCritical, Me.Text)
                WriteFile(InputFilePath & FileName & "_Rejections.csv", "TDX ID, Client ref" & vbCrLf & RejectionFile)
            End If

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub

    Private Function CurrentAge(ByVal DOB As Date) As Integer
        ' http://stackoverflow.com/questions/16874911/compute-age-from-given-birthdate
        Dim Age As Integer = Today.Year - DOB.Year
        If (DOB > Today.AddYears(-Age)) Then Age -= 1

        Return Age
    End Function
End Class

﻿Public Class Form1
    Dim outfile As String = "Client,Scheme, Debtor,LinkID,visit date" & vbNewLine
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub createbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles createbtn.Click
        run_process()
        MsgBox("Completed")
        Me.Close()
    End Sub
    Private Sub run_process()
        'first clear table
        Me.Visit_made_but_no_visit_feeTableAdapter.DeleteQuery()

        param2 = "select clientID, schemeID, branchID, _rowID from clientScheme where (branchID = 1 or branchID = 10) " & _
            " and clientID <> 1 and clientID <> 2 and clientID <>24 " & _
            " and _rowid > 2" & _
            " order by _rowid"

        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        Dim csRows As Integer = no_of_rows - 1
        ProgressBar1.Value = 5
        For CSidx = 0 To csRows

            Try
                ProgressBar1.Value = (CSidx / csRows) * 100
            Catch ex As Exception

            End Try
            param2 = "select name, work_type from Scheme where _rowid = " & cs_ds.Tables(0).Rows(CSidx).Item(1)
            Dim sch_ds As DataSet = get_dataset("onestep", param2)
            If InStr(sch_ds.Tables(0).Rows(0).Item(0), "(NC)") > 0 Then
                Continue For
            End If
            If sch_ds.Tables(0).Rows(0).Item(1) = 12 Then
                Continue For
            End If
            Application.DoEvents()
            Dim clientSchemeID As Integer = cs_ds.Tables(0).Rows(CSidx).Item(3)
            param2 = "select _rowid, clientSchemeID, linkID, last_stageID from Debtor where status_open_closed = 'O'" &
                " and clientSchemeID = " & clientSchemeID & _
                "   order by linkID"
            Dim debt_ds As DataSet = get_dataset("onestep", param2)
            Dim debtRows As Integer = no_of_rows - 1
            Dim last_linkID As Integer = 0
            Dim visit_fee_missing As Boolean = False
            For debtidx = 0 To debtRows
                Dim debtorID As Integer = debt_ds.Tables(0).Rows(debtidx).Item(0)
                Dim stageID As Integer = debt_ds.Tables(0).Rows(debtidx).Item(3)
                Dim branchID As Integer = cs_ds.Tables(0).Rows(CSidx).Item(2)
                param2 = "select name from Stage where _rowid = " & stageID
                Dim stage_ds As DataSet = get_dataset("onestep", param2)
                If stage_ds.Tables(0).Rows(0).Item(0) <> "Van Attendance" And
                    stage_ds.Tables(0).Rows(0).Item(0) <> "FurtherVanAttendance" Then
                    If branchID <> 10 Then
                        Continue For
                    Else
                        If stage_ds.Tables(0).Rows(0).Item(0) <> "Officer Attendance" And
                            stage_ds.Tables(0).Rows(0).Item(0) <> "Enforcement" And
                        stage_ds.Tables(0).Rows(0).Item(0) <> "2nd Enforcement" Then
                            Continue For
                        End If
                    End If
                End If

                Dim linkId As Integer
                Try
                    linkId = debt_ds.Tables(0).Rows(debtidx).Item(2)
                Catch ex As Exception
                    linkId = 0
                End Try
                If linkId > 0 And linkId = last_linkID And visit_fee_missing Then
                    Continue For
                End If
                If linkId > 0 And linkId <> last_linkID Then
                    last_linkID = linkId
                    visit_fee_missing = False
                End If

                'Dim scheme_name As String = sch_ds.Tables(0).Rows(0).Item(0)
                'see if case has been levied
                param2 = "select count(*) from Fee where debtorID = " & debtorID &
                    " and fee_amount > 0 and type like '%evy%'"
                Dim levy_cnt_ds As DataSet = get_dataset("onestep", param2)
                If linkId = 0 And levy_cnt_ds.Tables(0).Rows(0).Item(0) > 0 Then
                    Continue For
                End If

                'see how many visit fees are on the case already
                param2 = "select count(*) from Fee where debtorID = " & debtorID &
                    " and fee_amount > 0 and type like '%isit%'"
                Dim visit_cnt_ds As DataSet = get_dataset("onestep", param2)

                If linkId = 0 And visit_cnt_ds.Tables(0).Rows(0).Item(0) > 1 Then
                    Continue For
                End If
                Dim room_for_visit_fee_on_lead As Boolean = False
                If visit_cnt_ds.Tables(0).Rows(0).Item(0) < 2 And levy_cnt_ds.Tables(0).Rows(0).Item(0) = 0 Then
                    room_for_visit_fee_on_lead = True
                End If

                Dim room_for_visit_fee_on_link As Boolean = False
                'param2 = "select name from Client where _rowid = " & cs_ds.Tables(0).Rows(CSidx).Item(0)
                'Dim cl_ds As DataSet = get_dataset("onestep", param2)
                'Dim client_name As String = cl_ds.Tables(0).Rows(0).Item(0)

                'get all visits 
                param2 = "select date_visited, date_allocated, bailiffID from Visit where debtorID = " & debtorID &
                    " and date_visited is not null order by date_visited"
                Dim visit_ds As DataSet = get_dataset("onestep", param2)
                Dim visit_rows As Integer = no_of_rows - 1
                Dim last_visit_date As Date = Nothing
                Dim visit_fee_found As Boolean = False
                Dim visit_date, allocated_date As Date
                Dim allocated_date_found As Boolean
                If visit_rows < 0 Then
                    Continue For
                End If

                Dim bailiff_count(9999) As Integer
                For idx2 = 0 To visit_rows
                    visit_date = visit_ds.Tables(0).Rows(idx2).Item(0)
                    Try
                        allocated_date = visit_ds.Tables(0).Rows(idx2).Item(1)
                    Catch ex As Exception
                        allocated_date = Nothing
                    End Try


                    If last_visit_date <> Nothing Then
                        If Format(last_visit_date, "yyyy-MM-dd") = Format(visit_date, "yyyy-MM-dd") Then
                            Continue For
                        End If
                    End If
                    bailiff_count(visit_ds.Tables(0).Rows(idx2).Item(2)) += 1
                    If bailiff_count(visit_ds.Tables(0).Rows(idx2).Item(2)) > 2 Then
                        Continue For
                    End If
                    visit_fee_found = False
                    last_visit_date = visit_date
                    'see if visit fee exists for this date
                    param2 = "select type, date from Fee where debtorID = " & debtorID &
                        " and fee_amount > 0 and type like '%isit%'"
                    Dim fee_ds As DataSet = get_dataset("onestep", param2)
                    Dim fee_rows As Integer = no_of_rows - 1

                    For idx3 = 0 To fee_rows
                        Dim fee_date As Date = fee_ds.Tables(0).Rows(idx3).Item(1)
                        If Format(fee_date, "yyyy-MM-dd") <> Format(visit_date, "yyyy-MM-dd") Then
                            Continue For
                        End If
                        visit_fee_found = True
                        Exit For
                    Next
                    If visit_fee_found Then
                        Continue For
                    End If

                    If linkId = 0 Then
                        Exit For
                    End If

                    'see if visit fee exists in a linked case   - change taken out 24.12.2012 OR levy OR waiting fee OR van fee
                    '" and fee_amount > 0 and (type like '%isit%' or type like '%evy%' or type like '%aiting%' or type like '%ttendance%')"
                    param2 = " select _rowid from Debtor where linkID = " & linkId &
                        " and _rowid <> " & debtorID
                    Dim d2_ds As DataSet = get_dataset("onestep", param2)
                    Dim d2_rows As Integer = no_of_rows - 1

                    For d2_idx = 0 To d2_rows
                        param2 = "select type, date from Fee where debtorID = " & d2_ds.Tables(0).Rows(d2_idx).Item(0) &
                       " and fee_amount > 0 and type like '%isit%'"
                        Dim fee2_ds As DataSet = get_dataset("onestep", param2)
                        Dim fee2_rows As Integer = no_of_rows - 1
                        For idx4 = 0 To fee2_rows
                            visit_fee_found = False
                            Dim fee_date As Date = fee2_ds.Tables(0).Rows(idx4).Item(1)
                            If Format(fee_date, "yyyy-MM-dd") <> Format(visit_date, "yyyy-MM-dd") Then
                                Continue For
                            End If
                            visit_fee_found = True
                            Exit For
                        Next
                        If visit_fee_found Then
                            Exit For
                        End If
                        'see how many visit fees are on the case already
                        param2 = "select count(*) from Fee where debtorID = " & d2_ds.Tables(0).Rows(d2_idx).Item(0) &
                            " and fee_amount > 0 and type like '%isit%'"
                        Dim visit2_cnt_ds As DataSet = get_dataset("onestep", param2)
                        If visit2_cnt_ds.Tables(0).Rows(0).Item(0) > 1 Then
                            Continue For
                        End If
                        'case has a levy on?
                        param2 = "select count(*) from Fee where debtorID = " & d2_ds.Tables(0).Rows(d2_idx).Item(0) &
                            " and fee_amount > 0 and type like '%evy%'"
                        Dim levy2_cnt_ds As DataSet = get_dataset("onestep", param2)
                        If levy2_cnt_ds.Tables(0).Rows(0).Item(0) > 0 Then
                            Continue For
                        End If
                        room_for_visit_fee_on_link = True
                        'check if allocation date on visit record is same
                        allocated_date_found = False
                        param2 = "select date_allocated from Visit where debtorID = " & d2_ds.Tables(0).Rows(d2_idx).Item(0)
                        Dim visit2_ds As DataSet = get_dataset("onestep", param2)
                        Dim visit2_rows As Integer = no_of_rows - 1
                        For v_idx = 0 To visit2_rows
                            Try
                                If Format(visit2_ds.Tables(0).Rows(v_idx).Item(0), "yyyy-MM-dd") = Format(visit_date, "yyyy-MM-dd") Then
                                    allocated_date_found = True
                                    Exit For
                                End If
                            Catch ex As Exception

                            End Try
                        Next
                        If allocated_date_found Then
                            Exit For
                        End If
                    Next
                    If visit_fee_found Then
                        Continue For
                    End If
                    If Not visit_fee_found And allocated_date_found Then
                        Continue For
                    End If
                    If linkId = 0 Then
                        If room_for_visit_fee_on_lead And Not visit_fee_found Then
                            'outfile = outfile & client_name & "," & scheme_name & "," & debtorID & "," & linkId & "," & Format(visit_date, "dd/MM/yyyy") & "," & vbNewLine
                            'save in table
                            Me.Visit_made_but_no_visit_feeTableAdapter.InsertQuery(debtorID, visit_date)
                        End If
                    ElseIf Not visit_fee_found And _
                        (room_for_visit_fee_on_lead Or (room_for_visit_fee_on_link And allocated_date_found)) Then
                        ' outfile = outfile & client_name & "," & scheme_name & "," & debtorID & "," & linkId & "," & Format(visit_date, "dd/MM/yyyy") & "," & vbNewLine
                        Me.Visit_made_but_no_visit_feeTableAdapter.InsertQuery(debtorID, visit_date)
                        visit_fee_missing = True
                    End If
                    If visit_fee_missing Then
                        Exit For
                    End If
                Next
            Next
        Next
    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        run_process()
        Me.Close()
    End Sub
End Class

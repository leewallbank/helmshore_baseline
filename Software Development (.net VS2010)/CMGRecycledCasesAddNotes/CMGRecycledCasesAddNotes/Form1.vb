﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration
Public Class Form1
    Private InputFilePath As String, FileName As String, FileExt As String, outputFile As String
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click
        Dim FileDialog As New OpenFileDialog
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        readbtn.Enabled = False
        exitbtn.enabled = False
        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)
        Dim excel_file_contents(,) As String = InputFromExcel(FileDialog.FileName)
        Dim maxRows As Integer = UBound(excel_file_contents)

        For Excelrow = 0 To maxRows
            Try
                ProgressBar1.Value = (Excelrow / maxRows) * 100
            Catch ex As Exception

            End Try
            Dim debtorID As Integer
            Try
                debtorID = excel_file_contents(Excelrow, 0)
            Catch ex As Exception
                For colIDx = 0 To 19
                    outputFile &= excel_file_contents(Excelrow, colIDx) & "|"
                Next
                outputFile &= "Notes" & vbNewLine
                Continue For
            End Try
            'get client notes
            Dim note_dt As New DataTable
            LoadDataTable("DebtRecovery", "SELECT text " & _
                                           "FROM note " & _
                                           "WHERE DebtorID = " & debtorID & _
                                           " AND type = 'Client note'", note_dt, False)
            Dim noteText As String = ""
            Dim noteRow As DataRow

            For Each noteRow In note_dt.Rows
                noteText &= noteRow(0).PadRight(250, " ")
            Next
            'save new file
            For colIDx = 0 To 19
                outputFile &= excel_file_contents(Excelrow, colIDx) & "|"
            Next
            'add new notes
            outputFile &= noteText & vbNewLine
        Next
        WriteFile(InputFilePath & FileName & "_preprocess.txt", outputFile)
        MsgBox("File saved")
        Me.Close()
    End Sub
End Class

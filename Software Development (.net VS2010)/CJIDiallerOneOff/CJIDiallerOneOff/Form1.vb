﻿Imports CommonLibrary
Imports System.Configuration
Imports System.Xml.Schema
Imports System.IO

Public Class Form1
    Dim xml_valid As Boolean = True
    Dim auditFile As String = ""
    Dim error_no As Integer = 0
    Dim case_no As Integer = 0
    Dim prod_run As Boolean = False
    Dim fileDate As Date = Now
    Dim street1, street2, street3, town, city, postcode As String
    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        run_report()
    End Sub
    Private Sub run_report()

        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("StudentLoans")
        'when adding student loans update prod config file as well.
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If
        Dim filename As String = "ADFRossendales_LAA (IDS)_" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".xml"
        'T109307
        filename = "ADFRossendalesLAA" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".xml"
        Dim activity_file As String = "\\ross-helm-fp001\Rossendales Shared\TDX LAA CJI\" & filename
        auditFile = "\\ross-helm-fp001\Rossendales Shared\TDX LAA CJI\TDX_Activity_audit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        If Not prod_run Then
            activity_file = "C:\AAtemp\" & filename
            auditFile = "C:\AAtemp\TDXLAA_Activity_audit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        End If
        write_audit("report started at " & Now, False)
        Dim startDate As Date
        Dim endDate As Date
        If Weekday(Now) = 7 Then 'Saturday
            endDate = CDate(Format(Now, "MMM") & " " & Format(Now, "dd") & ", " & Year(Now) & " 16:00:00")
        Else
            endDate = DateAdd(DateInterval.Day, -Weekday(Now), Now)
        End If




        startDate = DateAdd(DateInterval.Day, -7, endDate)
        Dim clientCode As String = "LAA (IDS)"
        Dim branchID As Integer = 29
        If Not prod_run Then
            endDate = CDate("June 26, 2017")
            endDate = CDate(Format(endDate, "MMM d, yyyy" & " 16:00:00"))
            startDate = CDate("May 4, 2016")
        End If
        Dim writer As New Xml.XmlTextWriter(activity_file, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)
        writer.Formatting = Xml.Formatting.Indented
        writer.WriteStartElement("ActvtyData")
        writer.WriteAttributeString("schemaVersion", "1.0")
        writer.WriteAttributeString("xmlns", "ActivityData")
        writer.WriteStartElement("MsgHdr")
        writer.WriteAttributeString("xmlns", "")
        writer.WriteElementString("MsgTp", "ADF")
        writer.WriteElementString("FlNm", filename)
        writer.WriteElementString("FlDt", Format(fileDate, "yyyy-MM-dd") & "T" & Format(fileDate, "HH:mm:ss"))
        'If Not prod_run Then
        '    writer.WriteElementString("DCACd", "CJITest Agency")
        'Else
        '   
        'End If
        writer.WriteElementString("DCACd", "Rossendales")

        writer.WriteEndElement()  'MsgHdr
        writer.WriteStartElement("Accts")
        writer.WriteAttributeString("xmlns", "")
       

        'END DATE CHANGED
        'endDate = DateAdd(DateInterval.Day, 1, Now)

        ''if 2nd Jan 2016 go back a further 7 days
        'If Format(endDate, "yyyy-MM-dd") = Format(CDate("2016-01-02"), "yyyy-MM-dd") Then
        '    startDate = DateAdd(DateInterval.Day, -14, endDate)
        'End If

       
        'get all CSIDs for ETC HMRC
        'Dim cs_dt As New DataTable
        'LoadDataTable2("DebtRecovery", "select _rowid " & _
        '              " from clientScheme" & _
        '              " where clientID = " & clientID, cs_dt, False)
        'For Each csRow In cs_dt.Rows
        'Dim CSID As Integer = csRow(0)
        'now get cases
        Dim debt_dt As New DataTable
        LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, D.status, arrange_started, return_codeID, debt_balance, return_date, bail_current, bail_allocated," & _
                       "D.bailiffID, name_fore, name_sur, dateOfBirth, address, add_postcode" & _
                        " status_open_closed, return_date" & _
                        " from Debtor D, clientScheme CS, note N" & _
                        " where N.DebtorID = D._rowID" & _
                        "    and N._createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                        "  and N._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                       " and D.clientschemeID = CS._rowID" & _
                        " and CS.branchID = " & branchID & _
                        " and N.type = 'Phone'" & _
                        " and N._createdby = 'dialer'" & _
                       " group by 1" & _
                       " order by D._rowid", debt_dt, False)
        Dim debtRows As Integer = debt_dt.Rows.Count
        For Each debtRow In debt_dt.Rows
            Dim lastNoteRowid As Integer = 0
            Dim lastNoteType As String = ""
            Dim xmlWritten As Boolean = False
            Dim debtorID As Integer = debtRow(0)
            Dim clientRef As String = debtRow(1)
            Dim caseStatus As String = debtRow(2)
            Dim arrangeStarted As Date
            Try
                arrangeStarted = debtRow(3)
            Catch ex As Exception
                arrangeStarted = Nothing
            End Try
            Dim totalBalance As Decimal = debtRow(5)

            Dim cancelledNote As String = ""
            'get all notes for last week
            Dim note_dt As New DataTable
            LoadDataTable2("DebtRecovery", " select type, text, _createdDate, _createdBy, _rowid" & _
                          " from Note" & _
                          " where debtorID = " & debtorID & _
                          " and _createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                          " and _createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                           " and type = 'Phone'" & _
                          " and _createdby = 'dialer'" & _
                          " order by _createdDate", note_dt, False)

            For Each noteRow In note_dt.Rows
                Dim noteType As String = noteRow(0)
                Dim noteText As String = noteRow(1)
                Dim noteDate As Date = noteRow(2)
                Dim noteAgent As String = noteRow(3)
                Dim noteRowid As Integer = noteRow(4)
                Select Case noteType
                   
                    Case "Phone"
                        'ignore phone as notes have phone call details - otherwise may be duplicated
                        'T97571 include dispute or query if not allocated to 3793
                       
                            'T115175 include phone type if from dialer
                            If noteAgent = "dialer" Then
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "PCM")
                                Dim outcome As String = "NOA"
                                If InStr(noteText, "No Answer") = 0 Then
                                    If InStr(noteText, "Number Unobtainable") + InStr(noteText, "Unobtainable Number") > 0 Then
                                        outcome = "BDN"
                                    ElseIf InStr(noteText, "Ans") + InStr(noteText, "Failed") + InStr(noteText, "Third Party Call") +
                                        InStr(noteText, "Wrong Number") > 0 Then
                                        outcome = "PCA"
                                    End If
                                End If

                                writer.WriteElementString("OutTp", outcome)
                                writer.WriteElementString("Notes", noteText)
                                writer.WriteEndElement()  'actn
                            End If

                End Select
                lastNoteRowid = noteRowid
                lastNoteType = noteType
            Next

           
            If xmlWritten Then
                case_no += 1
                writer.WriteEndElement()  'actns
                writer.WriteEndElement()  'acct
                'If case_no >= 10 Then
                '    Exit For
                'End If
            End If
        Next  'debtorID
        'Next  'CSID

        writer.Close()

        'validate using xsd
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(activity_file)
        myDocument.Schemas.Add("ActivityData", "\\ross-helm-fp001\Rossendales Shared\vb.net\TDX XSD\Activity_Data.xsd")
        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
        write_audit(error_no & " - errors in report finished at " & Now, False)
        If error_no = 0 Then
            If prod_run Then
                'copy file to home office crystal reports on R drive
                Dim newFilename As String = "\\ross-helm-fp001\Rossendales Shared\Home Office Crystal Reports\" & Path.GetFileName(activity_file)
                My.Computer.FileSystem.CopyFile(activity_file, newFilename)

            End If
        End If
        Me.Close()
    End Sub
    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_audit("Error: " & e.Message, True)
            Case XmlSeverityType.Warning
                write_audit("Warning " & e.Message, True)
        End Select
    End Sub
    Private Sub write_audit(ByVal audit_message As String, ByVal errorMessage As Boolean)
        If errorMessage Then
            error_no += 1
        End If
        audit_message = audit_message & vbNewLine
        My.Computer.FileSystem.WriteAllText(auditFile, audit_message, True)
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        run_report()
    End Sub
    Private Sub get_address(ByVal addr As String, ByVal pcode As String)
        street1 = ""
        street2 = ""
        street3 = ""
        town = ""
        city = ""
        postcode = ""
        Try
            postcode = pcode
        Catch ex As Exception

        End Try
        Dim address = ""
        Try
            address = addr
        Catch ex As Exception
            Return
        End Try
        Dim addressline As String = ""
        For idx = 1 To address.Length
            If Mid(address, idx, 1) = Chr(10) Or Mid(address, idx, 1) = Chr(13) Then
                If street1 = "" Then
                    street1 = addressline
                ElseIf street2 = "" Then
                    street2 = addressline
                ElseIf town = "" Then
                    town = addressline
                ElseIf city = "" Then
                    city = addressline
                End If
                addressline = ""
            Else
                addressline &= Mid(address, idx, 1)
            End If
        Next
        'check for postcode and shuffle town/city
        If city = postcode Then
            city = ""
        ElseIf town = postcode Then
            town = ""
        ElseIf street2 = postcode Then
            street2 = ""
        End If
        If UCase(city) = "GB" Then
            city = ""
        End If
        If UCase(town) = "GB" Then
            town = ""
        End If
        If city = "" Then
            city = town
            town = street2
            street2 = ""
        End If
        If city = "" Then
            city = town
            town = street2
            street2 = ""
        End If
        If IsNumeric(Microsoft.VisualBasic.Left(town, 1)) Then
            If street2 = "" Then
                street2 = town
                town = ""
            Else
                street3 = town
                town = ""
            End If
        End If

    End Sub
End Class

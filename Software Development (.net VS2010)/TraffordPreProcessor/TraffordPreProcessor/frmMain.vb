﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim OutputFile As String = ""
            Dim totValue As Decimal = 0
            Dim totCases As Integer = 0
            Dim comments As String = ""
            Dim InputLineArray() As String
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents)
            Dim rowMax As Integer = UBound(FileContents)
            For rowIDX = 0 To rowMax
                InputLineArray = FileContents(rowIDX).Split("|")
                'all columns up to AV are copied to output, then debtor address

                Dim ColMax As Integer = UBound(InputLineArray)
                If ColMax < 10 Then
                    Continue For
                End If
                totCases += 1
                totValue += InputLineArray(33)
                comments = ""
                For colIDX = 0 To 47
                    OutputFile &= InputLineArray(colIDX) & "|"
                Next
                For colIDX = 84 To 89
                    OutputFile &= InputLineArray(colIDX) & "|"
                Next
                Dim name As String = ""
                Dim address As String = ""
                'look for 2nd address in cols 49-53
                For colidx = 48 To 53
                    address &= InputLineArray(colidx) & " "
                Next
                address = Trim(address)
                If address.Length > 0 Then
                    comments &= "Address2:" & address & ";"
                End If
                'see if there is a name in cols 54-57
               
                For colidx = 54 To 57
                    name &= InputLineArray(colidx) & " "
                Next
                name = Trim(name)
                If name.Length > 0 Then
                    comments &= "Name3:" & name & ";"
                    'now check address
                    For colidx = 58 To 63
                        address = InputLineArray(colidx) & " "
                    Next
                    address = Trim(address)
                    If address.Length > 0 Then
                        comments &= "Address3:" & address & ";"
                    End If
                End If
                'see if there is a name in cols 64-67
                name = ""
                address = ""
                For colidx = 64 To 67
                    name &= InputLineArray(colidx) & " "
                Next
                name = Trim(name)
                If name.Length > 0 Then
                    comments &= "Name4:" & name & ";"
                    'now check address
                    For colidx = 68 To 73
                        address = InputLineArray(colidx) & " "
                    Next
                    address = Trim(address)
                    If address.Length > 0 Then
                        comments &= "Address4:" & address & ";"
                    End If
                End If
                'see if there is a name in cols 74-77
                name = ""
                address = ""
                For colidx = 74 To 77
                    name &= InputLineArray(colidx) & " "
                Next
                name = Trim(name)
                If name.Length > 0 Then
                    comments &= "Name5:" & name & ";"
                    'now check address
                    For colidx = 78 To 83
                        address = InputLineArray(colidx) & " "
                    Next
                    address = Trim(address)
                    If address.Length > 0 Then
                        comments &= "Address5:" & address & ";"
                    End If
                End If

                OutputFile &= comments & vbNewLine
            Next
            btnViewInputFile.Enabled = True
            If totCases > 0 Then
                WriteFile(InputFilePath & FileName & "_preprocess.txt", OutputFile)
                btnViewOutputFile.Enabled = True
            End If


            MsgBox("Cases = " & totCases & " Value " & totValue & vbNewLine)
        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_preprocess.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_preprocess.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & "_nonWRP_4200.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_nonWRP_4200.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

﻿Imports CommonLibrary

Public Class clsListData

    Private StageNameDT As New DataTable
    Private ClientNameDT As New DataTable
    Private WorkTypeDT As New DataTable
    Private SchemeNameDT As New DataTable
    Private AllocatedDT As New DataTable
    Private CGADT As New DataTable
    Private EnforcementFeesAppliedDT As New DataTable
    Private PaymentDT As New DataTable
    Private NumberOfVisitsDT As New DataTable
    Private AddConfirmedDT As New DataTable
    Private InYearDT As New DataTable
    Private PostcodeAreaDT As New DataTable
    Private ArrangementBrokenDT As New DataTable
    Private DebtYearDT As New DataTable
    Private LinkedPIFDT As New DataTable ' added TS 214/Oct/2013. Portal task ref 17216
    Private PhoneNumberDT As New DataTable
    Private PaymentStageDT As New DataTable
    Private EASpokeToDefaulterDT As New DataTable
    Private ResidencyScoreDT As New DataTable
    Private ArrangementBrokenStageDT As New DataTable
    Private NumberOfDiallingAttemptsDT As New DataTable
    Private LinkedPaymentDT As New DataTable
    Private ConsortiumDT As New DataTable
    Private PostcodeAreaRegionDT As New DataTable

    Private BailiffNameDT As New DataTable
    Private BailiffTypeDT As New DataTable
    Private StatusNameDT As New DataTable
    Private VisitedDT As New DataTable
    Private FirstTwoVisitsPAWithinTwoDaysDT As New DataTable
    Private NumberOfVisitsPADT As New DataTable
    Private AllVisitsPAWithinTwentyDaysDT As New DataTable
    Private NumberOfVisitsAddressDT As New DataTable
    Private OutOfHoursVisitDT As New DataTable
    Private HoursBetweenVisitsDT As New DataTable
    Private AddressChangedDT As New DataTable
    Private LastVisitBailiffTypeDT As New DataTable

    Private VisitedNotLiveDT As New DataTable
    Private ArrangementMadeDT As New DataTable
    Private CollectedDT As New DataTable
    Private NOIDT As New DataTable
    Private AuditCheckDT As New DataTable
    Private GoneAwayDT As New DataTable
    Private VisitAfterPIFDT As New DataTable
    Private AuditCheckByDT As New DataTable
    Private LastVisitBailiffNameDT As New DataTable

    Private StageNameDV As DataView
    Private ClientNameDV As DataView
    Private WorkTypeDV As DataView
    Private SchemeNameDV As DataView
    Private AllocatedDV As DataView
    Private CGADV As DataView
    Private EnforcementFeesAppliedDV As DataView
    Private PaymentDV As DataView
    Private NumberOfVisitsDV As DataView
    Private AddConfirmedDV As DataView
    Private InYearDV As DataView
    Private PostcodeAreaDV As DataView
    Private ArrangementBrokenDV As DataView
    Private DebtYearDV As DataView
    Private LinkedPIFDV As New DataView
    Private PhoneNumberDV As New DataView
    Private PaymentStageDV As New DataView
    Private EASpokeToDefaulterDV As New DataView
    Private ResidencyScoreDV As New DataView
    Private ArrangementBrokenStageDV As DataView
    Private NumberOfDiallingAttemptsDV As DataView
    Private LinkedPaymentDV As New DataView
    Private ConsortiumDV As DataView
    Private PostcodeAreaRegionDV As New DataView

    Private BailiffNameDV As New DataView
    Private BailiffTypeDV As New DataView
    Private StatusNameDV As New DataView
    Private VisitedDV As New DataView
    Private FirstTwoVisitsPAWithinTwoDaysDV As New DataView
    Private NumberOfVisitsPADV As New DataView
    Private AllVisitsPAWithinTwentyDaysDV As New DataView
    Private NumberOfVisitsAddressDV As New DataView
    Private OutOfHoursVisitDV As New DataView
    Private HoursBetweenVisitsDV As New DataView
    Private AddressChangedDV As New DataView
    Private LastVisitBailiffTypeDV As New DataView

    Private VisitedNotLiveDV As New DataView
    Private ArrangementMadeDV As New DataView
    Private CollectedDV As New DataView
    Private NOIDV As New DataView
    Private AuditCheckDV As New DataView
    Private GoneAwayDV As New DataView
    Private VisitAfterPIFDV As New DataView
    Private AuditCheckByDV As New DataView
    Private LastVisitBailiffNameDV As New DataView


#Region "Public properties"

    Public ReadOnly Property StageName() As DataView
        Get
            StageName = StageNameDV
        End Get
    End Property

    Public ReadOnly Property ClientName() As DataView
        Get
            ClientName = ClientNameDV
        End Get
    End Property

    Public ReadOnly Property WorkType() As DataView
        Get
            WorkType = WorkTypeDV
        End Get
    End Property

    Public ReadOnly Property SchemeName() As DataView
        Get
            SchemeName = SchemeNameDV
        End Get
    End Property

    Public ReadOnly Property Allocated() As DataView
        Get
            Allocated = AllocatedDV
        End Get
    End Property

    Public ReadOnly Property CGA() As DataView
        Get
            CGA = CGADV
        End Get
    End Property

    Public ReadOnly Property EnforcementFeesApplied() As DataView
        Get
            EnforcementFeesApplied = EnforcementFeesAppliedDV
        End Get
    End Property

    Public ReadOnly Property Payment() As DataView
        Get
            Payment = PaymentDV
        End Get
    End Property

    Public ReadOnly Property NumberOfVisits() As DataView
        Get
            NumberOfVisits = NumberOfVisitsDV
        End Get
    End Property

    Public ReadOnly Property AddConfirmed() As DataView
        Get
            AddConfirmed = AddConfirmedDV
        End Get
    End Property

    Public ReadOnly Property InYear() As DataView
        Get
            InYear = InYearDV
        End Get
    End Property

    Public ReadOnly Property PostcodeArea() As DataView
        Get
            PostcodeArea = PostcodeAreaDV
        End Get
    End Property

    Public ReadOnly Property ArrangementBroken() As DataView
        Get
            ArrangementBroken = ArrangementBrokenDV
        End Get
    End Property

    Public ReadOnly Property DebtYear() As DataView
        Get
            DebtYear = DebtYearDV
        End Get
    End Property

    Public ReadOnly Property LinkedPIF() As DataView
        Get
            LinkedPIF = LinkedPIFDV
        End Get
    End Property

    Public ReadOnly Property PhoneNumber() As DataView
        Get
            PhoneNumber = PhoneNumberDV
        End Get
    End Property

    Public ReadOnly Property PaymentStage() As DataView
        Get
            PaymentStage = PaymentStageDV
        End Get
    End Property

    Public ReadOnly Property EASpokeToDefaulter() As DataView
        Get
            EASpokeToDefaulter = EASpokeToDefaulterDV
        End Get
    End Property

    Public ReadOnly Property ResidencyScore() As DataView
        Get
            ResidencyScore = ResidencyScoreDV
        End Get
    End Property

    Public ReadOnly Property ArrangementBrokenStage() As DataView
        Get
            ArrangementBrokenStage = ArrangementBrokenStageDV
        End Get
    End Property

    Public ReadOnly Property NumberOfDiallingAttempts() As DataView
        Get
            NumberOfDiallingAttempts = NumberOfDiallingAttemptsDV
        End Get
    End Property

    Public ReadOnly Property LinkedPayment() As DataView
        Get
            LinkedPayment = LinkedPaymentDV
        End Get
    End Property


    Public ReadOnly Property LastVisitBailiffName() As DataView
        Get
            LastVisitBailiffName = LastVisitBailiffNameDV
        End Get
    End Property

    Public ReadOnly Property ConsortiumDataView() As DataView
        Get
            ConsortiumDataView = ConsortiumDV
        End Get
    End Property

    Public ReadOnly Property PostcodeAreaRegionDataView() As DataView
        Get
            PostcodeAreaRegionDataView = PostcodeAreaRegionDV
        End Get
    End Property

    ' Bailiff screen specific lists
    Public ReadOnly Property BailiffName() As DataView
        Get
            BailiffName = BailiffNameDV
        End Get
    End Property

    Public ReadOnly Property BailiffType() As DataView
        Get
            BailiffType = BailiffTypeDV
        End Get
    End Property

    Public ReadOnly Property StatusName() As DataView
        Get
            StatusName = StatusNameDV
        End Get
    End Property

    Public ReadOnly Property Visited() As DataView
        Get
            Visited = VisitedDV
        End Get
    End Property

    Public ReadOnly Property FirstTwoVisitsPAWithinTwoDays() As DataView
        Get
            FirstTwoVisitsPAWithinTwoDays = FirstTwoVisitsPAWithinTwoDaysDV
        End Get
    End Property

    Public ReadOnly Property NumberOfVisitsPA() As DataView
        Get
            NumberOfVisitsPA = NumberOfVisitsPADV
        End Get
    End Property

    Public ReadOnly Property AllVisitsPAWithinTwentyDays() As DataView
        Get
            AllVisitsPAWithinTwentyDays = AllVisitsPAWithinTwentyDaysDV
        End Get
    End Property

    Public ReadOnly Property xIsRefreshRunning() As Boolean
        Get
            xIsRefreshRunning = GetSQLResults("BailiffAllocation", "EXEC dbo.IsRefreshRunning")
        End Get
    End Property

    ' Post Enforcement specific

    Public ReadOnly Property NumberOfVisitsAddress() As DataView
        Get
            NumberOfVisitsAddress = NumberOfVisitsAddressDV
        End Get
    End Property

    Public ReadOnly Property OutOfHoursVisit() As DataView
        Get
            OutOfHoursVisit = OutOfHoursVisitDV
        End Get
    End Property

    Public ReadOnly Property HoursBetweenVisits() As DataView
        Get
            HoursBetweenVisits = HoursBetweenVisitsDV
        End Get
    End Property

    Public ReadOnly Property AddressChanged() As DataView
        Get
            AddressChanged = AddressChangedDV
        End Get
    End Property

    Public ReadOnly Property LastVisitBailiffType() As DataView
        Get
            LastVisitBailiffType = LastVisitBailiffTypeDV
        End Get
    End Property

    ' EA Update specific

    Public ReadOnly Property VisitedNotLive() As DataView
        Get
            VisitedNotLive = VisitedNotLiveDV
        End Get
    End Property

    Public ReadOnly Property ArrangementMade() As DataView
        Get
            ArrangementMade = ArrangementMadeDV
        End Get
    End Property

    Public ReadOnly Property Collected() As DataView
        Get
            Collected = CollectedDV
        End Get
    End Property

    Public ReadOnly Property NOI() As DataView
        Get
            NOI = NOIDV
        End Get
    End Property

    Public ReadOnly Property AuditCheck() As DataView
        Get
            AuditCheck = AuditCheckDV
        End Get
    End Property

    Public ReadOnly Property GoneAway() As DataView
        Get
            GoneAway = GoneAwayDV
        End Get
    End Property

    Public ReadOnly Property VisitAfterPIF() As DataView
        Get
            VisitAfterPIF = VisitAfterPIFDV
        End Get
    End Property


    Public ReadOnly Property AuditCheckBy() As DataView
        Get
            AuditCheckBy = AuditCheckByDV
        End Get
    End Property

#End Region

#Region "Public methods"

    Public Sub GetCaseList(ByVal ParamList As String)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'StageName'", StageNameDT)
            StageNameDV = New DataView(StageNameDT)
            StageNameDV.AllowDelete = False
            StageNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'ClientName'", ClientNameDT)
            ClientNameDV = New DataView(ClientNameDT)
            ClientNameDV.AllowDelete = False
            ClientNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'WorkType'", WorkTypeDT)
            WorkTypeDV = New DataView(WorkTypeDT)
            WorkTypeDV.AllowDelete = False
            WorkTypeDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'SchemeName'", SchemeNameDT)
            SchemeNameDV = New DataView(SchemeNameDT)
            SchemeNameDV.AllowDelete = False
            SchemeNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'Allocated'", AllocatedDT)
            AllocatedDV = New DataView(AllocatedDT)
            AllocatedDV.AllowDelete = False
            AllocatedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'CGA'", CGADT)
            CGADV = New DataView(CGADT)
            CGADV.AllowDelete = False
            CGADV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'EnforcementFeesApplied'", EnforcementFeesAppliedDT)
            EnforcementFeesAppliedDV = New DataView(EnforcementFeesAppliedDT)
            EnforcementFeesAppliedDV.AllowDelete = False
            EnforcementFeesAppliedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'Payment'", PaymentDT)
            PaymentDV = New DataView(PaymentDT)
            PaymentDV.AllowDelete = False
            PaymentDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'NumberOfVisits'", NumberOfVisitsDT)
            NumberOfVisitsDV = New DataView(NumberOfVisitsDT)
            NumberOfVisitsDV.AllowDelete = False
            NumberOfVisitsDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'AddConfirmed'", AddConfirmedDT)
            AddConfirmedDV = New DataView(AddConfirmedDT)
            AddConfirmedDV.AllowDelete = False
            AddConfirmedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'InYear'", InYearDT)
            InYearDV = New DataView(InYearDT)
            InYearDV.AllowDelete = False
            InYearDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'PostcodeArea'", PostcodeAreaDT)
            PostcodeAreaDV = New DataView(PostcodeAreaDT)
            PostcodeAreaDV.AllowDelete = False
            PostcodeAreaDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'ArrangementBroken'", ArrangementBrokenDT)
            ArrangementBrokenDV = New DataView(ArrangementBrokenDT)
            ArrangementBrokenDV.AllowDelete = False
            ArrangementBrokenDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'DebtYear'", DebtYearDT)
            DebtYearDV = New DataView(DebtYearDT)
            DebtYearDV.AllowDelete = False
            DebtYearDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'LinkedPIF'", LinkedPIFDT)
            LinkedPIFDV = New DataView(LinkedPIFDT)
            LinkedPIFDV.AllowDelete = False
            LinkedPIFDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetBailiffList(ByVal ParamList As String)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'BailiffName'", BailiffNameDT)
            BailiffNameDV = New DataView(BailiffNameDT)
            BailiffNameDV.AllowDelete = False
            BailiffNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'BailiffType'", BailiffTypeDT)
            BailiffTypeDV = New DataView(BailiffTypeDT)
            BailiffTypeDV.AllowDelete = False
            BailiffTypeDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'ClientName'", ClientNameDT)
            ClientNameDV = New DataView(ClientNameDT)
            ClientNameDV.AllowDelete = False
            ClientNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'WorkType'", WorkTypeDT)
            WorkTypeDV = New DataView(WorkTypeDT)
            WorkTypeDV.AllowDelete = False
            WorkTypeDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'SchemeName'", SchemeNameDT)
            SchemeNameDV = New DataView(SchemeNameDT)
            SchemeNameDV.AllowDelete = False
            SchemeNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'StatusName'", StatusNameDT)
            StatusNameDV = New DataView(StatusNameDT)
            StatusNameDV.AllowDelete = False
            StatusNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'CGA'", CGADT)
            CGADV = New DataView(CGADT)
            CGADV.AllowDelete = False
            CGADV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'Payment'", PaymentDT)
            PaymentDV = New DataView(PaymentDT)
            PaymentDV.AllowDelete = False
            PaymentDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'Visited'", VisitedDT)
            VisitedDV = New DataView(VisitedDT)
            VisitedDV.AllowDelete = False
            VisitedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'AddConfirmed'", AddConfirmedDT)
            AddConfirmedDV = New DataView(AddConfirmedDT)
            AddConfirmedDV.AllowDelete = False
            AddConfirmedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'EnforcementFeesApplied'", EnforcementFeesAppliedDT)
            EnforcementFeesAppliedDV = New DataView(EnforcementFeesAppliedDT)
            EnforcementFeesAppliedDV.AllowDelete = False
            EnforcementFeesAppliedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'PostcodeArea'", PostcodeAreaDT)
            PostcodeAreaDV = New DataView(PostcodeAreaDT)
            PostcodeAreaDV.AllowDelete = False
            PostcodeAreaDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'FirstTwoVisitsPAWithinTwoDays'", FirstTwoVisitsPAWithinTwoDaysDT)
            FirstTwoVisitsPAWithinTwoDaysDV = New DataView(FirstTwoVisitsPAWithinTwoDaysDT)
            FirstTwoVisitsPAWithinTwoDaysDV.AllowDelete = False
            FirstTwoVisitsPAWithinTwoDaysDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'NumberOfVisitsPA'", NumberOfVisitsPADT)
            NumberOfVisitsPADV = New DataView(NumberOfVisitsPADT)
            NumberOfVisitsPADV.AllowDelete = False
            NumberOfVisitsPADV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'AllVisitsPAWithinTwentyDays'", AllVisitsPAWithinTwentyDaysDT)
            AllVisitsPAWithinTwentyDaysDV = New DataView(AllVisitsPAWithinTwentyDaysDT)
            AllVisitsPAWithinTwentyDaysDV.AllowDelete = False
            AllVisitsPAWithinTwentyDaysDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetPostEnforcementList(ByVal ParamList As String)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'StageName'", StageNameDT)
            StageNameDV = New DataView(StageNameDT)
            StageNameDV.AllowDelete = False
            StageNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'ClientName'", ClientNameDT)
            ClientNameDV = New DataView(ClientNameDT)
            ClientNameDV.AllowDelete = False
            ClientNameDV.AllowNew = False

            'LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'WorkType'", WorkTypeDT)
            'WorkTypeDV = New DataView(WorkTypeDT)
            'WorkTypeDV.AllowDelete = False
            'WorkTypeDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'SchemeName'", SchemeNameDT)
            SchemeNameDV = New DataView(SchemeNameDT)
            SchemeNameDV.AllowDelete = False
            SchemeNameDV.AllowNew = False

            'LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'PhoneNumber'", PhoneNumberDT)
            'PhoneNumberDV = New DataView(PhoneNumberDT)
            'PhoneNumberDV.AllowDelete = False
            'PhoneNumberDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'CGA'", CGADT)
            CGADV = New DataView(CGADT)
            CGADV.AllowDelete = False
            CGADV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'ResidencyScore'", ResidencyScoreDT)
            ResidencyScoreDV = New DataView(ResidencyScoreDT)
            ResidencyScoreDV.AllowDelete = False
            ResidencyScoreDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'PaymentStage'", PaymentStageDT)
            PaymentStageDV = New DataView(PaymentStageDT)
            PaymentStageDV.AllowDelete = False
            PaymentStageDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'NumberOfVisitsAddress'", NumberOfVisitsAddressDT)
            NumberOfVisitsAddressDV = New DataView(NumberOfVisitsAddressDT)
            NumberOfVisitsAddressDV.AllowDelete = False
            NumberOfVisitsAddressDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'AddConfirmed'", AddConfirmedDT)
            AddConfirmedDV = New DataView(AddConfirmedDT)
            AddConfirmedDV.AllowDelete = False
            AddConfirmedDV.AllowNew = False

            'LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'EASpokeToDefaulter'", EASpokeToDefaulterDT)
            'EASpokeToDefaulterDV = New DataView(EASpokeToDefaulterDT)
            'EASpokeToDefaulterDV.AllowDelete = False
            'EASpokeToDefaulterDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'PostcodeArea'", PostcodeAreaDT)
            PostcodeAreaDV = New DataView(PostcodeAreaDT)
            PostcodeAreaDV.AllowDelete = False
            PostcodeAreaDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'ArrangementBrokenStage'", ArrangementBrokenStageDT)
            ArrangementBrokenStageDV = New DataView(ArrangementBrokenStageDT)
            ArrangementBrokenStageDV.AllowDelete = False
            ArrangementBrokenStageDV.AllowNew = False

            'LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'NumberOfDiallingAttempts'", NumberOfDiallingAttemptsDT)
            'NumberOfDiallingAttemptsDV = New DataView(NumberOfDiallingAttemptsDT)
            'NumberOfDiallingAttemptsDV.AllowDelete = False
            'NumberOfDiallingAttemptsDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'LinkedPIF'", LinkedPIFDT)
            LinkedPIFDV = New DataView(LinkedPIFDT)
            LinkedPIFDV.AllowDelete = False
            LinkedPIFDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'DebtYear'", DebtYearDT)
            DebtYearDV = New DataView(DebtYearDT)
            DebtYearDV.AllowDelete = False
            DebtYearDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'InYear'", InYearDT)
            InYearDV = New DataView(InYearDT)
            InYearDV.AllowDelete = False
            InYearDV.AllowNew = False

            'LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'LinkedPayment'", LinkedPaymentDT)
            'LinkedPaymentDV = New DataView(LinkedPaymentDT)
            'LinkedPaymentDV.AllowDelete = False
            'LinkedPaymentDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'LastVisitBailiffName'", LastVisitBailiffNameDT)
            LastVisitBailiffNameDV = New DataView(LastVisitBailiffNameDT)
            LastVisitBailiffNameDV.AllowDelete = False
            LastVisitBailiffNameDV.AllowNew = False

            'LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'Visited'", VisitedDT)
            'VisitedDV = New DataView(VisitedDT)
            'VisitedDV.AllowDelete = False
            'VisitedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'OutOfHoursVisit'", OutOfHoursVisitDT)
            OutOfHoursVisitDV = New DataView(OutOfHoursVisitDT)
            OutOfHoursVisitDV.AllowDelete = False
            OutOfHoursVisitDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'HoursBetweenVisits'", HoursBetweenVisitsDT)
            HoursBetweenVisitsDV = New DataView(HoursBetweenVisitsDT)
            HoursBetweenVisitsDV.AllowDelete = False
            HoursBetweenVisitsDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'AddressChanged'", AddressChangedDT)
            AddressChangedDV = New DataView(AddressChangedDT)
            AddressChangedDV.AllowDelete = False
            AddressChangedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'LastVisitBailiffType'", LastVisitBailiffTypeDT)
            LastVisitBailiffTypeDV = New DataView(LastVisitBailiffTypeDT)
            LastVisitBailiffTypeDV.AllowDelete = False
            LastVisitBailiffTypeDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetEAUpdateList(ByVal ParamList As String)
        Try

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'EAName'", BailiffNameDT)
            BailiffNameDV = New DataView(BailiffNameDT)
            BailiffNameDV.AllowDelete = False
            BailiffNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'CGA'", CGADT)
            CGADV = New DataView(CGADT)
            CGADV.AllowDelete = False
            CGADV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'VisitedNotLive'", VisitedNotLiveDT)
            VisitedNotLiveDV = New DataView(VisitedNotLiveDT)
            VisitedNotLiveDV.AllowDelete = False
            VisitedNotLiveDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'ArrangementMade'", ArrangementMadeDT)
            ArrangementMadeDV = New DataView(ArrangementMadeDT)
            ArrangementMadeDV.AllowDelete = False
            ArrangementMadeDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'Collected'", CollectedDT)
            CollectedDV = New DataView(CollectedDT)
            CollectedDV.AllowDelete = False
            CollectedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'NOI'", NOIDT)
            NOIDV = New DataView(NOIDT)
            NOIDV.AllowDelete = False
            NOIDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'Visited'", VisitedDT)
            VisitedDV = New DataView(VisitedDT)
            VisitedDV.AllowDelete = False
            VisitedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'WorkType'", WorkTypeDT)
            WorkTypeDV = New DataView(WorkTypeDT)
            WorkTypeDV.AllowDelete = False
            WorkTypeDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'AuditCheck'", AuditCheckDT)
            AuditCheckDV = New DataView(AuditCheckDT)
            AuditCheckDV.AllowDelete = False
            AuditCheckDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'GoneAway'", GoneAwayDT)
            GoneAwayDV = New DataView(GoneAwayDT)
            GoneAwayDV.AllowDelete = False
            GoneAwayDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'VisitAfterPIF'", VisitAfterPIFDT)
            VisitAfterPIFDV = New DataView(VisitAfterPIFDT)
            VisitAfterPIFDV.AllowDelete = False
            VisitAfterPIFDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'AuditCheckBy'", AuditCheckByDT)
            AuditCheckByDV = New DataView(AuditCheckByDT)
            AuditCheckByDV.AllowDelete = False
            AuditCheckByDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'ClientName'", ClientNameDT)
            ClientNameDV = New DataView(ClientNameDT)
            ClientNameDV.AllowDelete = False
            ClientNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'NumberOfVisitsPA'", NumberOfVisitsPADT)
            NumberOfVisitsPADV = New DataView(NumberOfVisitsPADT)
            NumberOfVisitsPADV.AllowDelete = False
            NumberOfVisitsPADV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'PostcodeArea'", PostcodeAreaDT)
            PostcodeAreaDV = New DataView(PostcodeAreaDT)
            PostcodeAreaDV.AllowDelete = False
            PostcodeAreaDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetClientList(ByVal ParamList As String)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'ClientName'", ClientNameDT)
            ClientNameDV = New DataView(ClientNameDT)
            ClientNameDV.AllowDelete = False
            ClientNameDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetBailiffNameList(ByVal ParamList As String)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'BailiffName'", BailiffNameDT)
            BailiffNameDV = New DataView(BailiffNameDT)
            BailiffNameDV.AllowDelete = False
            BailiffNameDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetLastVisitBailiffNameList(ByVal ParamList As String)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostEnforcementList " & ParamList & ",'LastVisitBailiffName'", LastVisitBailiffNameDT)
            LastVisitBailiffNameDV = New DataView(LastVisitBailiffNameDT)
            LastVisitBailiffNameDV.AllowDelete = False
            LastVisitBailiffNameDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetEANameList(ByVal ParamList As String)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetEAUpdateList " & ParamList & ",'EAName'", BailiffNameDT)
            BailiffNameDV = New DataView(BailiffNameDT)
            BailiffNameDV.AllowDelete = False
            BailiffNameDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetConsortiums(CompanyID As Integer)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetConsortiums " & CompanyID.ToString & ",'C'", ConsortiumDT)
            ConsortiumDV = New DataView(ConsortiumDT)
            ConsortiumDV.AllowDelete = False
            ConsortiumDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetPostcodeAreaRegions()
        ' Added TS 09/Feb/2015. Request ref 38930
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetPostcodeAreas", PostcodeAreaRegionDT)
            PostcodeAreaRegionDV = New DataView(PostcodeAreaRegionDT)
            PostcodeAreaRegionDV.AllowDelete = False
            PostcodeAreaRegionDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

    'Protected Overrides Sub Finalize()

    '    If Not IsNothing(StageNameDV) Then StageNameDV.Dispose()
    '    If Not IsNothing(ClientNameDV) Then ClientNameDV.Dispose()
    '    If Not IsNothing(WorkTypeDV) Then WorkTypeDV.Dispose()
    '    If Not IsNothing(SchemeNameDV) Then SchemeNameDV.Dispose()
    '    If Not IsNothing(AllocatedDV) Then AllocatedDV.Dispose()
    '    If Not IsNothing(LeviedDV) Then LeviedDV.Dispose()
    '    If Not IsNothing(EnforcementFeesAppliedDV) Then EnforcementFeesAppliedDV.Dispose()
    '    If Not IsNothing(PaymentDV) Then PaymentDV.Dispose()
    '    If Not IsNothing(VisitedDV) Then VisitedDV.Dispose()
    '    If Not IsNothing(AddConfirmedDV) Then AddConfirmedDV.Dispose()
    '    If Not IsNothing(CoveredDV) Then CoveredDV.Dispose()
    '    If Not IsNothing(PostcodeAreaDV) Then PostcodeAreaDV.Dispose()
    '    If Not IsNothing(ArrangementBrokenDV) Then ArrangementBrokenDV.Dispose()
    '    If Not IsNothing(DebtYearDV) Then DebtYearDV.Dispose()
    '    If Not IsNothing(LinkedPIFDV) Then LinkedPIFDV.Dispose()

    '    MyBase.Finalize()
    'End Sub

End Class

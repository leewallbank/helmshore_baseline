<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.clientlbl = New System.Windows.Forms.Label()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.clientbtn = New System.Windows.Forms.Button()
        Me.cl_tbox = New System.Windows.Forms.TextBox()
        Me.msglbl = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(361, 205)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 3
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'clientlbl
        '
        Me.clientlbl.AutoSize = True
        Me.clientlbl.Location = New System.Drawing.Point(134, 93)
        Me.clientlbl.Name = "clientlbl"
        Me.clientlbl.Size = New System.Drawing.Size(124, 13)
        Me.clientlbl.TabIndex = 2
        Me.clientlbl.Text = "Enter start of client name"
        '
        'clientbtn
        '
        Me.clientbtn.Location = New System.Drawing.Point(137, 160)
        Me.clientbtn.Name = "clientbtn"
        Me.clientbtn.Size = New System.Drawing.Size(106, 23)
        Me.clientbtn.TabIndex = 1
        Me.clientbtn.Text = "Select Client"
        Me.clientbtn.UseVisualStyleBackColor = True
        '
        'cl_tbox
        '
        Me.cl_tbox.Location = New System.Drawing.Point(137, 109)
        Me.cl_tbox.Name = "cl_tbox"
        Me.cl_tbox.Size = New System.Drawing.Size(100, 20)
        Me.cl_tbox.TabIndex = 0
        '
        'msglbl
        '
        Me.msglbl.AutoSize = True
        Me.msglbl.Location = New System.Drawing.Point(36, 259)
        Me.msglbl.Name = "msglbl"
        Me.msglbl.Size = New System.Drawing.Size(139, 13)
        Me.msglbl.TabIndex = 4
        Me.msglbl.Text = "Running report - please wait"
        Me.msglbl.Visible = False
        '
        'Form1
        '
        Me.AcceptButton = Me.clientbtn
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(448, 303)
        Me.Controls.Add(Me.msglbl)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.clientbtn)
        Me.Controls.Add(Me.cl_tbox)
        Me.Controls.Add(Me.clientlbl)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Run RD452P report"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents clientlbl As System.Windows.Forms.Label
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents clientbtn As System.Windows.Forms.Button
    Friend WithEvents cl_tbox As System.Windows.Forms.TextBox
    Friend WithEvents msglbl As System.Windows.Forms.Label

End Class

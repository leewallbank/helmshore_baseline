Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub schbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        
    End Sub

    Private Sub clbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clientbtn.Click
        clientfrm.ShowDialog()
        schemefrm.ShowDialog()
        schemefrm.sch_dg.EndEdit()
        clientbtn.Enabled = False
        disable_btns()
        If MsgBox("Run report for " & clName & " scheme " & schName, MsgBoxStyle.YesNo, "Run report") = MsgBoxResult.No Then
            MsgBox("Report not run")
        Else
            msglbl.Visible = True
            run_RD452P2()
        End If

        Me.Close()
    End Sub
    Private Sub disable_btns()
        clientbtn.Enabled = False
        exitbtn.Enabled = False
    End Sub

    Private Sub run_RD452P2()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD452P2report = New RD452P2
        Dim myArrayList1 As ArrayList = New ArrayList()

        myArrayList1.Add(parmCSID)
        SetCurrentValuesForParameterField1(RD452P2report, myArrayList1)
        Dim myArrayList2 As ArrayList = New ArrayList()

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD452P2report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RD452P2 Batch Tracking.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RD452P2report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RD452P2report.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
   

End Class

Public Class schemefrm

    Private Sub schemefrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'get all schemes for client

        sch_dg.Rows.Clear()
        Dim sch_ds As New DataTable
        LoadDataTable2("DebtRecovery", "select S._rowid, S.name, CS._rowID from Scheme S, clientScheme CS " & _
                   " where S._rowid = CS.schemeID " & _
                   " and CS.clientID = " & CLID, sch_ds, False)
        Dim schRow As DataRow
        For Each schRow In sch_ds.Rows
            sch_dg.Rows.Add(schRow(1), schRow(0), schRow(2))
        Next
    End Sub


    Private Sub sch_dg_CellDoubleClick(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles sch_dg.CellDoubleClick
        Me.Close()
    End Sub

    Private Sub sch_dg_CellEnter(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles sch_dg.CellEnter
        parmCSID = sch_dg.Rows(e.RowIndex).Cells(2).Value
        schName = sch_dg.Rows(e.RowIndex).Cells(0).Value
    End Sub

    Private Sub sch_dg_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles sch_dg.CellContentClick

    End Sub
End Class
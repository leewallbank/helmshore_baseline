﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.createbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.remit_dtp = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.sch_tbox = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.schbtn = New System.Windows.Forms.Button()
        Me.allbtn = New System.Windows.Forms.Button()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.remitbtn = New System.Windows.Forms.Button()
        Me.testbtn = New System.Windows.Forms.Button()
        Me.StudentLoansDataSet = New StudentLoansReturnFile.StudentLoansDataSet()
        Me.ReturnedTraceTableAdapter = New StudentLoansReturnFile.StudentLoansDataSetTableAdapters.ReturnedTraceTableAdapter()
        Me.TableAdapterManager = New StudentLoansReturnFile.StudentLoansDataSetTableAdapters.TableAdapterManager()
        CType(Me.StudentLoansDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'createbtn
        '
        Me.createbtn.Location = New System.Drawing.Point(174, 239)
        Me.createbtn.Name = "createbtn"
        Me.createbtn.Size = New System.Drawing.Size(107, 23)
        Me.createbtn.TabIndex = 5
        Me.createbtn.Text = "Create return file"
        Me.createbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(359, 322)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 6
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'remit_dtp
        '
        Me.remit_dtp.Location = New System.Drawing.Point(174, 187)
        Me.remit_dtp.Name = "remit_dtp"
        Me.remit_dtp.Size = New System.Drawing.Size(118, 20)
        Me.remit_dtp.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(208, 171)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Remit date:"
        '
        'sch_tbox
        '
        Me.sch_tbox.Location = New System.Drawing.Point(161, 130)
        Me.sch_tbox.Name = "sch_tbox"
        Me.sch_tbox.ReadOnly = True
        Me.sch_tbox.Size = New System.Drawing.Size(173, 20)
        Me.sch_tbox.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(220, 114)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Scheme:"
        '
        'schbtn
        '
        Me.schbtn.Location = New System.Drawing.Point(185, 39)
        Me.schbtn.Name = "schbtn"
        Me.schbtn.Size = New System.Drawing.Size(107, 23)
        Me.schbtn.TabIndex = 1
        Me.schbtn.Text = "Select Scheme"
        Me.schbtn.UseVisualStyleBackColor = True
        '
        'allbtn
        '
        Me.allbtn.Location = New System.Drawing.Point(33, 39)
        Me.allbtn.Name = "allbtn"
        Me.allbtn.Size = New System.Drawing.Size(133, 23)
        Me.allbtn.TabIndex = 0
        Me.allbtn.Text = "ALL PROD Schemes"
        Me.allbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 322)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 6
        '
        'remitbtn
        '
        Me.remitbtn.Location = New System.Drawing.Point(320, 39)
        Me.remitbtn.Name = "remitbtn"
        Me.remitbtn.Size = New System.Drawing.Size(114, 23)
        Me.remitbtn.TabIndex = 2
        Me.remitbtn.Text = "Enter remit No"
        Me.remitbtn.UseVisualStyleBackColor = True
        '
        'testbtn
        '
        Me.testbtn.Enabled = False
        Me.testbtn.Location = New System.Drawing.Point(33, 82)
        Me.testbtn.Name = "testbtn"
        Me.testbtn.Size = New System.Drawing.Size(133, 23)
        Me.testbtn.TabIndex = 7
        Me.testbtn.Text = "ALL TEST Schemes"
        Me.testbtn.UseVisualStyleBackColor = True
        '
        'StudentLoansDataSet
        '
        Me.StudentLoansDataSet.DataSetName = "StudentLoansDataSet"
        Me.StudentLoansDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReturnedTraceTableAdapter
        '
        Me.ReturnedTraceTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.ReturnedTraceTableAdapter = Me.ReturnedTraceTableAdapter
        Me.TableAdapterManager.UpdateOrder = StudentLoansReturnFile.StudentLoansDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(517, 370)
        Me.Controls.Add(Me.testbtn)
        Me.Controls.Add(Me.remitbtn)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.allbtn)
        Me.Controls.Add(Me.schbtn)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.sch_tbox)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.remit_dtp)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.createbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Student Loans Return File"
        CType(Me.StudentLoansDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents createbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents remit_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents sch_tbox As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents schbtn As System.Windows.Forms.Button
    Friend WithEvents allbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents remitbtn As System.Windows.Forms.Button
    Friend WithEvents testbtn As System.Windows.Forms.Button
    Friend WithEvents StudentLoansDataSet As StudentLoansReturnFile.StudentLoansDataSet
    Friend WithEvents ReturnedTraceTableAdapter As StudentLoansReturnFile.StudentLoansDataSetTableAdapters.ReturnedTraceTableAdapter
    Friend WithEvents TableAdapterManager As StudentLoansReturnFile.StudentLoansDataSetTableAdapters.TableAdapterManager

End Class

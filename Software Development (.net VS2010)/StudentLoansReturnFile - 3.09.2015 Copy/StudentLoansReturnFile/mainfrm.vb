﻿Public Class mainfrm
    Dim return_file, trace_return_file As String
    Public ascii As New System.Text.ASCIIEncoding()
    Dim remit_date As Date
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub createbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles createbtn.Click
        If selected_csid = 0 And selected_remit_no = 0 Then
            MsgBox("select all schemes or particular scheme or remit no")
            Exit Sub
        End If
        allbtn.Enabled = False
        schbtn.Enabled = False
        createbtn.Enabled = False
        exitbtn.Enabled = False
        remitbtn.Enabled = False

        'use selected_csid if >0 otherwise get all 
        Dim filepath As String = ""
        remit_date = remit_dtp.Value
        If selected_remit_no > 0 Then
            param2 = "select clientSchemeID from Remit where _rowid = " & selected_remit_no
            Dim remit_ds As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to find remit No  = " & selected_remit_no)
                Exit Sub
            End If
            selected_csid = remit_ds.Tables(0).Rows(0).Item(0)
            param2 = "select clientID from clientScheme where _rowid = " & selected_csid
            Dim cs2_ds As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to access clientscheme for csid = " & selected_csid)
                Exit Sub
            End If
            selected_cl_ID = cs2_ds.Tables(0).Rows(0).Item(0)
        End If
        param2 = "select _rowid, schemeID, defaultCourtCode from clientScheme where clientID = " & selected_cl_ID
        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        Dim cs_rows As Integer = no_of_rows - 1
        Dim remit_found As Boolean = False
        For cs_idx = 0 To cs_rows
            Try
                ProgressBar1.Value = (cs_idx / cs_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()

            Dim payinbook_ref As String = ""
            'if test selected just include csids from 3249 to 3271

            Dim csID As Integer = cs_ds.Tables(0).Rows(cs_idx).Item(0)
            If selected_csid > 0 Then
                If selected_csid <> csID Then
                    Continue For
                End If
            End If
            If selected_cl_ID = 24 Then
                If csID < 3249 Or csID > 3271 Then
                    Continue For
                End If
            End If
            Dim sch_id As Integer = cs_ds.Tables(0).Rows(cs_idx).Item(1)
            Dim default_court_code As String = ""
            Try
                default_court_code = cs_ds.Tables(0).Rows(cs_idx).Item(2)
            Catch ex As Exception
                MsgBox("No default Court Code has been set up for clientschemeID = " & csID)
                default_court_code = "Unknown"
            End Try
            Dim success_code As String = ""
            'if this is a trace and collect client - need to report on corresponding scheme from mapping table
            'also need to check if a successful trace has been made on the case to set success code to NEG or space

            'CHANGED No longer need to use cross reference
            'still need to know whether a trace scheme though
            param2 = "select name from Scheme where _rowid = " & sch_id
            Dim sch_ds As DataSet = get_dataset("onestep", param2)

            'Me.ClientSchemeMappingTableAdapter.FillBy(Me.StudentLoansDataSet.ClientSchemeMapping, csID)
            Dim tc_client As Boolean = False
            Dim RGO_client As Boolean = False
            Dim arrears_client As Boolean = False
            If csID >= 3174 And csID <= 3183 Then
                'If Me.StudentLoansDataSet.ClientSchemeMapping.Rows.Count > 0 Then
                tc_client = True
            End If
            If InStr(sch_ds.Tables(0).Rows(0).Item(0), "RGO") > 0 Then
                RGO_client = True
            End If
            If csID = 3161 Or csID = 3162 Or csID = 3163 Or csID = 3164 Or csID = 3165 Or csID = 3166 Or csID = 3171 _
                Or csID = 3172 Or csID = 3174 Or csID = 3175 Or csID = 3176 Or csID = 3179 Or csID = 3180 Or csID = 3181 Then
                arrears_client = True
            End If

            'Dim map_csid As Integer = Me.StudentLoansDataSet.ClientSchemeMapping.Rows(0).Item(2)
            'param2 = "select defaultCourtCode from clientScheme where _rowid = " & map_csid
            'Dim cs2_ds As DataSet = get_dataset("onestep", param2)
            'If no_of_rows > 0 Then
            '    Try
            '        default_court_code = cs2_ds.Tables(0).Rows(0).Item(0)
            '    Catch ex As Exception
            '        MsgBox("No default Court Code has been set up for clientschemeID = " & csID)
            '        default_court_code = "Unknown"
            '    End Try
            'End If
            'End If


            ' If csID <> 4591 Then
            ' Continue For
            'End If



            'get all remits for this csID and remit date
            param2 = "select _rowid from Remit where clientSchemeID = " & csID &
                " and date = '" & Format(remit_date, "yyyy-MM-dd") & "'"

            Dim remit_ds As DataSet = get_dataset("onestep", param2)
            Dim remit_rows As Integer = no_of_rows - 1
            For remit_idx = 0 To remit_rows
                Dim remitID As Integer = remit_ds.Tables(0).Rows(remit_idx).Item(0)
                If selected_remit_no > 0 Then
                    If selected_remit_no <> remitID Then
                        Continue For
                    End If
                End If
                param2 = "select _rowid, client_ref, prevReference,name_title, name_fore, name_sur, dateOfBirth, " &
                    " return_codeID, status, offence_number, debt_amount" &
                    " from Debtor where return_remitID = " & remitID
                Dim debt_ds As DataSet = get_dataset("onestep", param2)
                Dim debtor_rows As Integer = no_of_rows - 1
                For idx = 0 To debtor_rows
                    Dim debtorID As Integer = debt_ds.Tables(0).Rows(idx).Item(0)
                    Dim cl_ref As String = debt_ds.Tables(0).Rows(idx).Item(1)
                    Dim reported_trace_case As Boolean = False
                    Dim prev_ref As String
                    Try
                        prev_ref = debt_ds.Tables(0).Rows(idx).Item(2)
                    Catch ex As Exception
                        prev_ref = ""
                    End Try

                    Dim cust_name, title, fore_name, surname As String
                    Try
                        title = Trim(debt_ds.Tables(0).Rows(idx).Item(3))
                    Catch ex As Exception
                        title = ""
                    End Try
                    cust_name = title
                    If cust_name.Length > 0 Then
                        cust_name = cust_name & " "
                    End If
                    Try
                        fore_name = debt_ds.Tables(0).Rows(idx).Item(4)
                        cust_name = cust_name & Trim(fore_name) & " "
                    Catch ex As Exception
                        fore_name = ""
                    End Try
                    Try
                        surname = debt_ds.Tables(0).Rows(idx).Item(5)
                        cust_name = cust_name & Trim(surname)
                    Catch ex As Exception
                        surname = ""
                    End Try

                    Dim dob As Date
                    Try
                        dob = debt_ds.Tables(0).Rows(idx).Item(6)
                    Catch ex As Exception
                        dob = Nothing
                    End Try
                    Dim dob_str As String
                    If dob = Nothing Then
                        dob_str = ""
                    Else
                        dob_str = Format(dob, "ddMMyyyy")
                    End If
                    Dim address As String = ""



                    'get payments in this remit
                    param2 = "select split_debt, split_costs, amount_sourceID from Payment where status_remitID = " & remitID &
                        " and status = 'R' and debtorID = " & debtorID

                    Dim pay_ds As DataSet = get_dataset("onestep", param2)
                    Dim paid_to_client As Decimal = 0
                    For idx2 = 0 To no_of_rows - 1
                        'ignore direct payments
                        Dim sourceID As Integer = pay_ds.Tables(0).Rows(idx2).Item(2)
                        param2 = "select direct from PaySource where _rowid = " & sourceID
                        Dim ps_ds As DataSet = get_dataset("onestep", param2)
                        If ps_ds.Tables(0).Rows(0).Item(0) <> "Y" Then
                            paid_to_client = paid_to_client + pay_ds.Tables(0).Rows(idx2).Item(0) + pay_ds.Tables(0).Rows(idx2).Item(1)
                        End If
                    Next
                    paid_to_client = paid_to_client * 100 'amount in pence
                    Dim paid_str As String
                    paid_str = CInt(paid_to_client)
                    If paid_str = 0 Then
                        paid_str = ""
                    End If
                    Dim status As String = debt_ds.Tables(0).Rows(idx).Item(8)
                    Dim closure_code As String = ""
                    If status = "S" Then
                        If arrears_client Then
                            'get total balance from notes
                            param2 = "select text from Note where debtorID = " & debtorID &
                                " order by _createdDate desc"
                            Dim note_ds As DataSet = get_dataset("onestep", param2)
                            Dim tot_bal As Decimal = Nothing
                            For note_idx = 0 To no_of_rows - 1
                                Dim start_idx As Integer = InStr(note_ds.Tables(0).Rows(note_idx).Item(0), "Total Balance:")
                                If start_idx = 0 Then
                                    Continue For
                                End If
                                Dim end_idx As Integer = InStr(note_ds.Tables(0).Rows(note_idx).Item(0), ";")
                                Try
                                    tot_bal = Mid(note_ds.Tables(0).Rows(note_idx).Item(0), start_idx + 14, end_idx - start_idx - 14)
                                Catch ex As Exception
                                    MsgBox("No total balance on case " & debtorID)
                                    closure_code = "PIF"
                                End Try
                                If tot_bal <> Nothing Then
                                    Dim debt_amt As Decimal = debt_ds.Tables(0).Rows(idx).Item(10)
                                    If debt_amt >= tot_bal Then
                                        closure_code = "PIF"
                                    Else
                                        closure_code = "RAB"
                                    End If
                                    Exit For
                                End If
                            Next
                        Else
                            closure_code = "PIF"
                        End If
                    Else
                        'ignore cases re-opened and put into trace
                        If status = "T" Then
                            Continue For
                        End If
                        Dim return_code As Integer
                        Try
                            return_code = debt_ds.Tables(0).Rows(idx).Item(7)
                        Catch ex As Exception
                            return_code = 0
                        End Try
                        If return_code > 0 Then
                            param2 = "select clientReturnCode from clientSchemeReturn " &
                                " where clientSchemeID = " & csID &
                                " and returnID = " & return_code
                            Dim ret_ds As DataSet = get_dataset("onestep", param2)
                            Try
                                closure_code = ret_ds.Tables(0).Rows(0).Item(0)
                            Catch ex As Exception
                                closure_code = ""
                            End Try
                            If closure_code = "G/A" Then
                                closure_code = "EXH"
                            End If
                        End If
                    End If


                    ' If closure_code <> "EXH" Then
                    'Continue For
                    'End If



                    If RGO_client Then
                        Try
                            payinbook_ref = debt_ds.Tables(0).Rows(idx).Item(9)
                        Catch ex As Exception
                            MsgBox("Case " & debtorID & " has no offence number!")
                            payinbook_ref = ""
                        End Try

                    Else
                        payinbook_ref = ""
                    End If
                    success_code = ""
                    If tc_client Then
                        'check for address change
                        If tc_client And closure_code <> "CLI" Then
                            param2 = "select type from Note where debtorID = " & debtorID &
                                " and type = 'Address'"
                            Dim note_ds As DataSet = get_dataset("onestep", param2)

                            If note_ds.Tables(0).Rows.Count = 0 Then
                                success_code = "NEG"
                            End If
                        End If
                        If Microsoft.VisualBasic.Left(default_court_code, 1) = "T" Then
                            Me.ReturnedTraceTableAdapter.FillBy(Me.StudentLoansDataSet.ReturnedTrace, debtorID)
                            If Me.StudentLoansDataSet.ReturnedTrace.Rows.Count > 0 Then
                                reported_trace_case = True
                                success_code = ""
                                'if entered by this program this week then ignore
                                If Me.StudentLoansDataSet.ReturnedTrace.Rows(0).Item(2) = "ROSSENDALES\StudentLoansReturnFile" Then
                                    If DateDiff(DateInterval.Day, Me.StudentLoansDataSet.ReturnedTrace.Rows(0).Item(1), Now) < 5 Then
                                        reported_trace_case = False
                                        If closure_code = "EXH" Then
                                            success_code = "NEG"
                                        End If
                                    End If
                                End If
                            Else
                                'if G/A insert row
                                '20.3.2013 save a record for all return codes
                                success_code = ""
                                If closure_code = "EXH" Then
                                    success_code = "NEG"
                                End If
                                Try
                                    Me.ReturnedTraceTableAdapter.InsertQuery(debtorID, Now, "ROSSENDALES\StudentLoansReturnFile")
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If

                    If reported_trace_case Then
                        trace_return_file = trace_return_file & cl_ref.PadRight(11) & payinbook_ref.PadRight(18) & cust_name.PadRight(100) &
                                            dob_str.PadRight(8) & success_code.PadRight(10) & address.PadRight(501) & paid_str.PadLeft(10) &
                                            closure_code.PadRight(3) & vbNewLine
                    Else
                        'return_file = return_file & payinbook_ref.PadRight(18) & cust_name.PadRight(100) &
                        'dob_str.PadRight(8) & success_code.PadRight(10) & address.PadRight(501) & paid_str.PadLeft(10) &
                        'closure_code.PadRight(3) & vbNewLine
                        return_file = return_file & cl_ref.PadRight(11) & payinbook_ref.PadRight(18) & cust_name.PadRight(100) &
                        dob_str.PadRight(8) & success_code.PadRight(10) & address.PadRight(501) & paid_str.PadLeft(10) &
                        closure_code.PadRight(3) & vbNewLine
                    End If
                Next
                'get payments in this remit for cases not returned
                Dim last_debtorID As Integer = 0
                Dim debt_ds2 As DataSet = Nothing
                Dim cl_ref2 As String = ""
                Dim cust_name2 As String = ""
                Dim title2 As String = ""
                Dim fore_name2 As String = ""
                Dim surname2 As String = ""
                Dim dob_str2 As String = ""
                Dim address2 As String = ""
                Dim paid_to_client2 As Decimal = 0
                Dim reported_trace_case2 As Boolean = False
                Dim ignore_debtor As Boolean = False
                param2 = "select split_debt, split_costs, debtorID, amount_sourceID from Payment where status_remitID = " & remitID &
                    " and status = 'R' order by debtorID"
                Dim pay_ds2 As DataSet = get_dataset("onestep", param2)
                Dim pay_rows As Integer = no_of_rows - 1
                For pay_idx As Integer = 0 To pay_rows
                    'ignore direct payments
                    Dim sourceID As Integer = pay_ds2.Tables(0).Rows(pay_idx).Item(3)
                    param2 = "select direct from paySource where _rowid = " & sourceID
                    Dim ps_ds As DataSet = get_dataset("onestep", param2)
                    If ps_ds.Tables(0).Rows(0).Item(0) = "Y" Then
                        Continue For
                    End If

                    Dim debtorID As Integer = pay_ds2.Tables(0).Rows(pay_idx).Item(2)
                    If debtorID <> last_debtorID Then
                        'different debtorID
                        If paid_to_client2 > 0 Then
                            paid_to_client2 = paid_to_client2 * 100 'amount in pence
                            Dim paid_str As String
                            paid_str = CInt(paid_to_client2)
                            If paid_str = 0 Then
                                paid_str = ""
                            End If
                            Dim status As String = debt_ds2.Tables(0).Rows(0).Item(8)
                            Dim closure_code As String = ""
                            'closure code is blank for cases not returned
                            'If status = "S" Then
                            '    If arrears_client Then
                            '        'get total balance from notes
                            '        param2 = "select text from Note where debtorID = " & last_debtorID &
                            '            " order by _createdDate desc"
                            '        Dim note_ds As DataSet = get_dataset("onestep", param2)
                            '        Dim tot_bal As Decimal = Nothing
                            '        For note_idx = 0 To no_of_rows - 1
                            '            Dim start_idx As Integer = InStr(note_ds.Tables(0).Rows(note_idx).Item(0), "Total Balance:")
                            '            If start_idx = 0 Then
                            '                Continue For
                            '            End If
                            '            Dim end_idx As Integer = InStr(note_ds.Tables(0).Rows(note_idx).Item(0), ";")
                            '            Try
                            '                tot_bal = Mid(note_ds.Tables(0).Rows(note_idx).Item(0), start_idx + 14, end_idx - start_idx - 14)
                            '            Catch ex As Exception
                            '                MsgBox("No total balance on case " & last_debtorID)
                            '                closure_code = "PIF"
                            '            End Try
                            '            If tot_bal <> Nothing Then
                            '                Dim debt_amt As Decimal = debt_ds2.Tables(0).Rows(0).Item(11)
                            '                If debt_amt >= tot_bal Then
                            '                    closure_code = "PIF"
                            '                Else
                            '                    closure_code = "RAB"
                            '                End If
                            '                Exit For
                            '            End If
                            '        Next
                            '    Else
                            '        closure_code = "PIF"
                            '    End If
                            'Else
                            '    Dim return_code As Integer
                            '    Try
                            '        return_code = debt_ds2.Tables(0).Rows(0).Item(7)
                            '    Catch ex As Exception
                            '        return_code = 0
                            '    End Try
                            '    If return_code > 0 Then
                            '        param2 = "select clientReturnCode from clientSchemeReturn " &
                            '            " where clientSchemeID = " & csID &
                            '            " and returnID = " & return_code
                            '        Dim ret_ds As DataSet = get_dataset("onestep", param2)
                            '        Try
                            '            closure_code = ret_ds.Tables(0).Rows(0).Item(0)
                            '        Catch ex As Exception
                            '            closure_code = ""
                            '        End Try
                            '        If closure_code = "G/A" Then
                            '            closure_code = "EXH"
                            '        End If
                            '    End If
                            'End If

                            If RGO_client Then
                                Try
                                    payinbook_ref = debt_ds2.Tables(0).Rows(0).Item(10)
                                Catch ex As Exception
                                    payinbook_ref = ""
                                End Try

                            Else
                                payinbook_ref = ""
                            End If
                            success_code = ""
                            If tc_client Then
                                'check for address change
                                If tc_client And closure_code <> "CLI" Then
                                    param2 = "select type from Note where debtorID = " & last_debtorID &
                                        " and type = 'Address'"
                                    Dim note_ds As DataSet = get_dataset("onestep", param2)

                                    If note_ds.Tables(0).Rows.Count = 0 Then
                                        success_code = "NEG"
                                    End If
                                End If
                                If Microsoft.VisualBasic.Left(default_court_code, 1) = "T" Then
                                    Me.ReturnedTraceTableAdapter.FillBy(Me.StudentLoansDataSet.ReturnedTrace, last_debtorID)
                                    If Me.StudentLoansDataSet.ReturnedTrace.Rows.Count > 0 Then
                                        reported_trace_case2 = True
                                        success_code = ""
                                        'if entered by this program this week then ignore
                                        If Me.StudentLoansDataSet.ReturnedTrace.Rows(0).Item(2) = "ROSSENDALES\StudentLoansReturnFile" Then
                                            If DateDiff(DateInterval.Day, Me.StudentLoansDataSet.ReturnedTrace.Rows(0).Item(1), Now) < 5 Then
                                                reported_trace_case2 = False
                                                If closure_code = "EXH" Then
                                                    success_code = "NEG"
                                                End If
                                            End If
                                        End If
                                    Else
                                        'if G/A insert row
                                        '20.3.2013 save record for all return codes
                                        success_code = ""
                                        If closure_code = "EXH" Then
                                            success_code = "NEG"
                                        End If
                                        Try
                                            Me.ReturnedTraceTableAdapter.InsertQuery(last_debtorID, Now, "ROSSENDALES\StudentLoansReturnFile")
                                        Catch ex As Exception
                                            MsgBox(ex.Message)
                                        End Try
                                    End If
                                End If
                            End If
                            If reported_trace_case2 Then
                                trace_return_file = trace_return_file & cl_ref2.PadRight(11) & payinbook_ref.PadRight(18) & cust_name2.PadRight(100) &
                          dob_str2.PadRight(8) & address2.PadRight(511) & paid_str.PadLeft(10) &
                          closure_code.PadRight(3) & vbNewLine
                            Else
                                return_file = return_file & cl_ref2.PadRight(11) & payinbook_ref.PadRight(18) & cust_name2.PadRight(100) &
                          dob_str2.PadRight(8) & address2.PadRight(511) & paid_str.PadLeft(10) &
                          closure_code.PadRight(3) & vbNewLine
                            End If
                        End If

                        last_debtorID = debtorID
                        paid_to_client2 = 0
                        reported_trace_case2 = False
                        ignore_debtor = False
                        'process new debtorID
                        param2 = "select _rowid, client_ref, prevReference,name_title, name_fore, name_sur, dateOfBirth, " &
                  " return_codeID, status, return_remitID, offence_number, debt_amount" &
                  " from Debtor where _rowID = " & debtorID
                        debt_ds2 = get_dataset("onestep", param2)
                        Dim retn_remitID As Integer
                        Try
                            retn_remitID = debt_ds2.Tables(0).Rows(0).Item(9)
                        Catch ex As Exception
                            retn_remitID = 0
                        End Try
                        If retn_remitID = remitID Then 'case not returned this week
                            ignore_debtor = True
                            Continue For
                        End If
                        cl_ref2 = debt_ds2.Tables(0).Rows(0).Item(1)
                        Dim prev_ref As String
                        Try
                            prev_ref = debt_ds2.Tables(0).Rows(0).Item(2)
                        Catch ex As Exception
                            prev_ref = ""
                        End Try


                        Try
                            title2 = Trim(debt_ds2.Tables(0).Rows(0).Item(3))
                        Catch ex As Exception
                            title2 = ""
                        End Try
                        cust_name2 = title2
                        If cust_name2.Length > 0 Then
                            cust_name2 = cust_name2 & " "
                        End If
                        Try
                            fore_name2 = debt_ds2.Tables(0).Rows(0).Item(4)
                            cust_name2 = cust_name2 & Trim(fore_name2) & " "
                        Catch ex As Exception
                            fore_name2 = ""
                        End Try
                        Try
                            surname2 = debt_ds2.Tables(0).Rows(0).Item(5)
                            cust_name2 = cust_name2 & Trim(surname2)
                        Catch ex As Exception
                            surname2 = ""
                        End Try

                        Dim dob As Date
                        Try
                            dob = debt_ds2.Tables(0).Rows(0).Item(6)
                        Catch ex As Exception
                            dob = Nothing
                        End Try

                        If dob = Nothing Then
                            dob_str2 = ""
                        Else
                            dob_str2 = Format(dob, "ddMMyyyy")
                        End If
                        paid_to_client2 = 0
                        paid_to_client2 = paid_to_client2 + pay_ds2.Tables(0).Rows(pay_idx).Item(0) + pay_ds2.Tables(0).Rows(pay_idx).Item(1)
                    Else
                        'same debtorID
                        If ignore_debtor Then
                            Continue For
                        End If
                        paid_to_client2 = paid_to_client2 + pay_ds2.Tables(0).Rows(pay_idx).Item(0) + pay_ds2.Tables(0).Rows(pay_idx).Item(1)
                    End If
                Next
                'process last debtorID
                If paid_to_client2 > 0 Then
                    paid_to_client2 = paid_to_client2 * 100 'amount in pence
                    Dim paid_str As String
                    paid_str = CInt(paid_to_client2)
                    If paid_str = 0 Then
                        paid_str = ""
                    End If
                    Dim status As String = debt_ds2.Tables(0).Rows(0).Item(8)
                    Dim closure_code As String = ""
                    'closure code is blank for cases not returned
                    'If status = "S" Then
                    '    If arrears_client Then
                    '        'get total balance from notes
                    '        param2 = "select text from Note where debtorID = " & last_debtorID &
                    '            " order by _createdDate desc"
                    '        Dim note_ds As DataSet = get_dataset("onestep", param2)
                    '        Dim tot_bal As Decimal = Nothing
                    '        For note_idx = 0 To no_of_rows - 1
                    '            Dim start_idx As Integer = InStr(note_ds.Tables(0).Rows(note_idx).Item(0), "Total Balance:")
                    '            If start_idx = 0 Then
                    '                Continue For
                    '            End If
                    '            Dim end_idx As Integer = InStr(note_ds.Tables(0).Rows(note_idx).Item(0), ";")
                    '            Try
                    '                tot_bal = Mid(note_ds.Tables(0).Rows(note_idx).Item(0), start_idx + 14, end_idx - start_idx - 14)
                    '            Catch ex As Exception
                    '                MsgBox("No total balance on case " & last_debtorID)
                    '                closure_code = "PIF"
                    '            End Try
                    '            If tot_bal <> Nothing Then
                    '                Dim debt_amt As Decimal = debt_ds2.Tables(0).Rows(0).Item(11)
                    '                If debt_amt >= tot_bal Then
                    '                    closure_code = "PIF"
                    '                Else
                    '                    closure_code = "RAB"
                    '                End If
                    '                Exit For
                    '            End If
                    '        Next
                    '    Else
                    '        closure_code = "PIF"
                    '    End If
                    'Else
                    '    Dim return_code As Integer
                    '    Try
                    '        return_code = debt_ds2.Tables(0).Rows(0).Item(7)
                    '    Catch ex As Exception
                    '        return_code = 0
                    '    End Try
                    '    If return_code > 0 Then
                    '        param2 = "select clientReturnCode from clientSchemeReturn " &
                    '            " where clientSchemeID = " & csID &
                    '            " and returnID = " & return_code
                    '        Dim ret_ds As DataSet = get_dataset("onestep", param2)
                    '        Try
                    '            closure_code = ret_ds.Tables(0).Rows(0).Item(0)
                    '        Catch ex As Exception
                    '            closure_code = ""
                    '        End Try
                    '        '20.3.2013 treat gone away as collection exhausted
                    '        If closure_code = "G/A" Then
                    '            closure_code = "EXH"
                    '        End If
                    '    End If
                    'End If
                    If RGO_client Then
                        Try
                            payinbook_ref = debt_ds2.Tables(0).Rows(0).Item(10)
                        Catch ex As Exception
                            payinbook_ref = ""
                        End Try
                    Else
                        payinbook_ref = ""
                    End If
                    success_code = ""
                    If tc_client Then
                        'check for address change
                        If tc_client And closure_code <> "CLI" Then
                            param2 = "select type from Note where debtorID = " & last_debtorID &
                                " and type = 'Address'"
                            Dim note_ds As DataSet = get_dataset("onestep", param2)

                            If note_ds.Tables(0).Rows.Count = 0 Then
                                success_code = "NEG"
                            End If
                        End If
                        If Microsoft.VisualBasic.Left(default_court_code, 1) = "T" Then
                            Me.ReturnedTraceTableAdapter.FillBy(Me.StudentLoansDataSet.ReturnedTrace, last_debtorID)
                            If Me.StudentLoansDataSet.ReturnedTrace.Rows.Count > 0 Then
                                reported_trace_case2 = True
                                success_code = ""
                                'if entered by this program this week then ignore
                                If Me.StudentLoansDataSet.ReturnedTrace.Rows(0).Item(2) = "ROSSENDALES\StudentLoansReturnFile" Then
                                    If DateDiff(DateInterval.Day, Me.StudentLoansDataSet.ReturnedTrace.Rows(0).Item(1), Now) < 5 Then
                                        reported_trace_case2 = False
                                        If closure_code = "EXH" Then
                                            success_code = "NEG"
                                        End If
                                    End If
                                End If
                            Else
                                'if G/A insert row
                                '20.3.2013 save record for all return codes
                                success_code = ""
                                If closure_code = "EXH" Then
                                    success_code = "NEG"
                                End If
                                Try
                                    Me.ReturnedTraceTableAdapter.InsertQuery(last_debtorID, Now, "ROSSENDALES\StudentLoansReturnFile")
                                Catch ex As Exception
                                    MsgBox(ex.Message)
                                End Try
                            End If
                        End If
                    End If
                    If reported_trace_case2 Then
                        trace_return_file = trace_return_file & cl_ref2.PadRight(11) & payinbook_ref.PadRight(18) & cust_name2.PadRight(100) &
                  dob_str2.PadRight(8) & address2.PadRight(511) & paid_str.PadLeft(10) &
                  closure_code.PadRight(3) & vbNewLine
                    Else
                        return_file = return_file & cl_ref2.PadRight(11) & payinbook_ref.PadRight(18) & cust_name2.PadRight(100) &
                  dob_str2.PadRight(8) & address2.PadRight(511) & paid_str.PadLeft(10) &
                  closure_code.PadRight(3) & vbNewLine
                    End If
                End If
            Next
            If Len(return_file) > 0 Then
                'remove last ODOA from file
                Try
                    return_file = Microsoft.VisualBasic.Left(return_file, return_file.Length - 2)
                Catch ex As Exception

                End Try
            End If

            If Len(return_file) > 0 Then
                If filepath = "" Then
                    With SaveFileDialog1
                        .Title = "Save file"
                        .Filter = "TXT files |*.txt"
                        .DefaultExt = ".txt"
                        .OverwritePrompt = True
                        .InitialDirectory = "H:"
                        .FileName = default_court_code & "_" & Format(Now, "ddMMyyyy") & ".txt"
                    End With

                    If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, return_file, False, ascii)
                    End If
                    Dim idx2 As Integer
                    For idx2 = SaveFileDialog1.FileName.Length To 1 Step -1
                        If Mid(SaveFileDialog1.FileName, idx2, 1) = "\" Then
                            Exit For
                        End If
                    Next
                    filepath = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, idx2)
                Else
                    SaveFileDialog1.FileName = filepath & default_court_code & "_" & Format(Now, "ddMMyyyy") & ".txt"
                    My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, return_file, False, ascii)
                End If
            End If

            If Len(trace_return_file) > 0 Then
                'remove last ODOA from file
                Try
                    trace_return_file = Microsoft.VisualBasic.Left(trace_return_file, trace_return_file.Length - 2)
                Catch ex As Exception

                End Try
            End If
            If Len(trace_return_file) > 0 Then
                If Microsoft.VisualBasic.Left(default_court_code, 1) = "T" Then
                    default_court_code = Microsoft.VisualBasic.Right(default_court_code, default_court_code.Length - 1)
                End If

                If filepath = "" Then
                    With SaveFileDialog1
                        .Title = "Save file"
                        .Filter = "TXT files |*.txt"
                        .DefaultExt = ".txt"
                        .OverwritePrompt = True
                        .InitialDirectory = "H:"
                        .FileName = default_court_code & "_" & Format(Now, "ddMMyyyy") & ".txt"
                    End With

                    If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, trace_return_file, False, ascii)
                    End If
                    Dim idx2 As Integer
                    For idx2 = SaveFileDialog1.FileName.Length To 1 Step -1
                        If Mid(SaveFileDialog1.FileName, idx2, 1) = "\" Then
                            Exit For
                        End If
                    Next
                    filepath = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, idx2)
                Else
                    SaveFileDialog1.FileName = filepath & default_court_code & "_" & Format(Now, "ddMMyyyy") & ".txt"
                    My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, trace_return_file, False, ascii)
                End If
            End If
            return_file = ""
            trace_return_file = ""
            remit_found = True
        Next
        If Not remit_found Then
            MsgBox("No remit found")
        Else
            MsgBox("File saved")
        End If

        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        Dim days_ago As Integer = Weekday(Now)
        remit_date = DateAdd(DateInterval.Day, -days_ago, Now)
        remit_dtp.Value = remit_date
    End Sub

    Private Sub schbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles schbtn.Click
        selected_csid = 0
        selected_cl_ID = 1572
        schemefrm.ShowDialog()
        sch_tbox.Text = selected_scheme
    End Sub

    Private Sub allbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allbtn.Click
        sch_tbox.Text = "ALL Schemes"
        selected_csid = -1
        selected_cl_ID = 1572
    End Sub

    Private Sub testbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles remitbtn.Click
        'selected_csid = 0
        'selected_cl_ID = 24
        'schemefrm.ShowDialog()
        'sch_tbox.Text = selected_scheme
        Try
            selected_remit_no = InputBox("Enter Remit No")
        Catch ex As Exception
            MsgBox("Invalid remit no")
            Exit Sub
        End Try
        sch_tbox.Text = "Remit No " & selected_remit_no

    End Sub

   
    Private Sub testbtn_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles testbtn.Click
        sch_tbox.Text = "ALL TEST Schemes"
        selected_csid = -1
        selected_cl_ID = 24
    End Sub

End Class

﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration

Public Class frmMain
    Private InputFilePath As String, FileName As String, FileExt As String
    Dim outfile_oxford As String
    Dim outline As String = ""
    Dim client_found As Integer = 0
    Dim statute_barred_case As Boolean = False
    Dim lodate As Date
    Dim clref As String = ""
    Dim off_number, batch_no, off_location As String
    Dim off_court As Date
    Dim title, forename, surname As String
    Dim dob As Date
    Dim addr1, addr2, addr3, addr4, pcode As String
    Dim d_addr1, d_addr2, d_addr3, d_addr4, d_addr5, d_pcode As String
    Dim phone, emp_phone, fax As String
    Dim notes As String
    Dim joint_name As String
    Dim joint_address As String
    Dim prev_addr As String
    Dim statute_barred As String
    Dim statute_barred_date As Date
    Dim ignore_row As Boolean
    Dim CaseBalance As Decimal
    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim colval As String = ""
        Dim spaces As Integer = 250
        Try
            Dim FileDialog As New OpenFileDialog
            Dim LineNumber As Integer = 0
            Dim AuditLog As String = "", ErrorLog As String = "", InputLineArray() As String
            Dim TotalBalance As Decimal = 0

            Dim Cases As Integer = 0
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            lblReadingFile.Visible = True

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            lblReadingFile.Visible = False
            ProgressBar.Maximum = UBound(FileContents)
            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                Application.DoEvents()
                LineNumber += 1
                Dim columnidx As Integer
                Dim columnNumber As Integer
                InputLineArray = Split(InputLine, ",", """")
                notes = ""
                For columnidx = 0 To 24
                    Try
                        colval = Trim(InputLineArray(columnidx))
                    Catch ex As Exception
                        MsgBox("error on line " & LineNumber & " column " & columnidx)
                        Me.Close()
                    End Try

                    If colval = "" And columnidx = 0 Then
                        Exit For
                    End If
                    columnNumber = columnidx + 1
                    If columnNumber = 1 Then
                        Cases += 1
                        TotalBalance += InputLineArray(16)
                    End If
                    outfile_oxford &= colval & "|"
                    If columnNumber >= 3 And columnNumber <= 5 Then
                        If colval <> "" Then
                            If notes = "" Then
                                notes = "Other names:"
                            End If
                            notes &= colval & ";"
                        End If
                    End If
                Next

                Try
                    outfile_oxford &= outline & "|" & notes & vbNewLine

                    outline = ""
                    notes = ""
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

            Next InputLine

            AuditLog &= "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
            AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

            If Cases > 0 Then
                AuditLog &= "Number of cases: " & Cases & vbCrLf
                AuditLog &= "Total balance of First cases: " & TotalBalance & vbCrLf
                WriteFile(InputFilePath & FileName & "_preprocess.txt", outfile_oxford)
            End If

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)

            MsgBox("Pre-processing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
    Private Sub write_file(ByVal limit As Integer, ByVal filename As String, ByVal outfile As String)
        Dim LineNumber As Integer, FileCount As Integer = 1, RowCount As Integer = 0
        Dim OutputFile As String = ""
        Dim FileContents() As String = outfile.Split(vbNewLine)

        If UBound(FileContents) = 0 Then
            Return
        End If
        For Each InputLine As String In FileContents
            OutputFile &= InputLine
            LineNumber += 1
            RowCount += 1

            If RowCount > limit Then ' Not equal so as not to include the header

                WriteFile(filename & FileCount.ToString & ".txt", OutputFile)
                FileCount += 1

                OutputFile = FileContents(0)  ' Add header again
                RowCount = 1
            End If

        Next InputLine

        WriteFile(filename & FileCount.ToString & ".txt", OutputFile)

    End Sub
    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed_wrp.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

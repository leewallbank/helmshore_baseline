﻿Imports CommonLibrary

Public Class clsTDXHMRCData

    Private DebtorDT As New DataTable

    Public ReadOnly Property Debtor() As DataTable
        Get
            Debtor = DebtorDT
        End Get
    End Property

    Public Function GetDebtorID(ByVal ClientRef As String, ByVal ClientID As String) As Object()

        GetDebtorID = GetSQLResultsArray("DebtRecovery", "SELECT d._rowID " & _
                                                        "      , d.ClientSchemeID " & _
                                                    "FROM debtor AS d " & _
                                                    "INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID " & _
                                                    "WHERE d.client_ref = '" & ClientRef & "' " & _
                                                    "  AND cs.clientID = " & ClientID & " " & _
                                                    "ORDER BY d._rowID DESC " & _
                                                    "LIMIT 1")

        '"  AND d.status_open_closed = 'O' " & _
    End Function

    Public Function GetCaseBalance(ByVal DebtorID As String) As Decimal

        GetCaseBalance = GetSQLResults("DebtRecovery", "SELECT d.debt_amount " & _
                                                       "FROM debtor AS d " & _
                                                       "WHERE d._rowID = " & DebtorID)
    End Function

    Public Sub GetHHBDDebtorID(ByVal ClientRef As String, ByVal ClientID As String)

        LoadDataTable("DebtRecovery", "SELECT d._rowID " & _
                                                    "FROM debtor AS d " & _
                                                    "WHERE d.client_ref = '" & ClientRef & "' " & _
                                                    "  AND d.clientSchemeID = 4814 " & _
                                                    "ORDER BY d._rowID DESC " & _
                                                    "LIMIT 2", DebtorDT)

    End Sub
End Class
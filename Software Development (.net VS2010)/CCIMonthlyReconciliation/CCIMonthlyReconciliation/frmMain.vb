﻿Imports System.IO
Imports System.Text

Public Class frmMain

    Private ReconciliationData As New clsReconciliationData

    Private Sub btnOpenFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOpenFile.Click
        Dim FileDialog As New OpenFileDialog
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        Dim ExcelApp As Object, Workbook As Object, Worksheet As Object = Nothing
        Dim FilePath As String, FileName As String, ErrLog As String = "", InputLineArray() As String, DebtRecovery() As Object
        Dim LineNumber As Integer, ErrorCount As Integer
        Dim WorkbookPath As String = ""
        Dim CatchException As Boolean = False
        Dim Red As Object = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red)

        ExcelApp = CreateObject("Excel.Application")
        Workbook = ExcelApp.Workbooks.Add()
        Try
            For Each Sheet As Object In Workbook.Worksheets
                If Sheet.Index > 1 Then Sheet.Delete()
            Next Sheet

            FilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName, Encoding.ASCII)
            ProgressBar.Maximum = UBound(FileContents)

            Worksheet = Workbook.Sheets(1)
            Worksheet.Name = "Reconciliation errors"
            Worksheet.Cells(1, 1) = FileDialog.FileName
            Worksheet.Cells(2, 1) = "Reconciled at " & DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss")

            For Each InputLine As String In FileContents
                Application.DoEvents()
                ProgressBar.Value = LineNumber
                LineNumber += 1

                If LineNumber > 1 Then
                    InputLineArray = InputLine.Split("|")

                    DebtRecovery = ReconciliationData.GetCaseDetail(InputLineArray(0))

                    If InputLineArray(1) <> DebtRecovery(0) Or Replace(InputLineArray(2), Chr(63), "") <> DebtRecovery(1) Then
                        If ErrorCount = 0 Then
                            Worksheet.Cells(3, 1) = "DebtorID"
                            Worksheet.Cells(3, 2) = "DR status"
                            Worksheet.Cells(3, 3) = "CCI status"
                            Worksheet.Cells(3, 4) = "DR balance"
                            Worksheet.Cells(3, 5) = "CCI balance"
                            Worksheet.Range(Worksheet.Cells(3, 1), Worksheet.Cells(3, 5)).Font.Bold = True
                        End If

                        ErrorCount += 1
                        Worksheet.Cells(ErrorCount + 3, 1) = InputLineArray(0)
                        Worksheet.Cells(ErrorCount + 3, 2) = DebtRecovery(0)
                        Worksheet.Cells(ErrorCount + 3, 3) = InputLineArray(1)
                        Worksheet.Cells(ErrorCount + 3, 4) = DebtRecovery(1)
                        Worksheet.Cells(ErrorCount + 3, 5) = Replace(InputLineArray(2), Chr(63), "")
                        If InputLineArray(1) <> DebtRecovery(0) Then
                            Worksheet.Cells(ErrorCount + 3, 2).Font.Bold = True
                            Worksheet.Cells(ErrorCount + 3, 2).Font.Color = Red
                            Worksheet.Cells(ErrorCount + 3, 3).Font.Bold = True
                            Worksheet.Cells(ErrorCount + 3, 3).Font.Color = Red
                        End If
                        If Replace(InputLineArray(2), Chr(63), "") <> DebtRecovery(1) Then
                            Worksheet.Cells(ErrorCount + 3, 4).Font.Bold = True
                            Worksheet.Cells(ErrorCount + 3, 4).Font.Color = Red
                            Worksheet.Cells(ErrorCount + 3, 5).Font.Bold = True
                            Worksheet.Cells(ErrorCount + 3, 5).Font.Color = Red
                        End If
                    End If
                End If

            Next InputLine

            Worksheet.Cells(2, 5) = LineNumber.ToString
            Worksheet.Cells(2, 6) = "cases"
            Worksheet.Cells(2, 7) = ErrorCount.ToString
            Worksheet.Cells(2, 8) = "errors"

            ExcelApp.DisplayAlerts = False

            WorkbookPath = FilePath & "\CCI_Reconciliation_Errors_" & DateTime.Today.ToString("yyyyMMdd") & ".xls"
            Workbook.SaveAs(WorkbookPath, 39) ' 39  Is Excel version 8. A bit old but more likely to be compatible

        Catch ex As Exception
            MsgBox(ex.ToString)
        Finally
            CloseComObject(ExcelApp.ActiveWorkbook)
            ExcelApp.ActiveWorkbook.Close()
            ExcelApp.Quit()
            CloseComObject(Workbook)
            Workbook = Nothing
            CloseComObject(ExcelApp)
            ExcelApp = Nothing

            Me.Cursor = Cursors.Default

            If Not CatchException And File.Exists(WorkbookPath) Then System.Diagnostics.Process.Start(WorkbookPath)
        End Try
    End Sub

    Private Sub CloseComObject(ByVal o As Object)
        Try
            While (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0)
            End While
        Catch
        Finally
            o = Nothing
        End Try
    End Sub
End Class

﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization
Imports System.Net.Mail

Public Class frmMain
    Const Separator As String = "|"
    Const HHBDSeparator As String = "^"
    Private ConnCode As New Dictionary(Of String, Integer)
    Private CustomerType As New Dictionary(Of String, String)
    Private PersonType As New Dictionary(Of String, String)
    Private Nationality As New Dictionary(Of String, String)
    Private IDType As New Dictionary(Of String, String)
    Private PersonAddressType As New Dictionary(Of String, String)
    'Private ConnIDList As String() = {"3549", "3552", "3553", "3554", "3541", "3550", "3551", "3543", "3556", "3542", "3555", "4471", "4437", "4667", "4723", "4724", "4725", "4726", "4766"} ' commented out TS 12/Apr/2016. Request ref 77466
    'Private ConnIDList As String() = {"4810", "4811", "4812", "4813", "4814", "4822", "4823"} ' Added TS 12/Apr/2016. Request ref 77466
    'Private ConnIDList As String() = {"4810", "4811", "4812", "4813", "4814", "4822", "4823", "4972"} ' Added TS 13/Oct/2016. Request ref 94579
    'Private ConnIDList As String() = {"4810", "4811", "4812", "4813", "4814", "4822", "4823", "4972", "4982", "4984", "4983"} ' Added TS 30/Nov/2016. Request ref 98745
    Private ConnIDList As String() = {"4810", "4811", "4812", "4813", "4814", "4822", "4823", "4972", "4982", "4984", "4983", "5004"} ' Added TS 11/Jan/2017. Request ref 102125
    Private InputFilePath As String, FileName As String, FileExt As String
    Private OutputFiles As New ArrayList()
    Private NewCases As New ArrayList()

    Private PreProcessData As New clsPreProcessData

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        ' The following commented out TS 12/Apr/2016. Request ref 77466
        'ConnCode.Add("3549", 0)
        'ConnCode.Add("3552", 1)
        'ConnCode.Add("3553", 2)
        'ConnCode.Add("3554", 3)
        'ConnCode.Add("3541", 4)
        'ConnCode.Add("3550", 5)
        'ConnCode.Add("3551", 6)
        'ConnCode.Add("3543", 7)
        'ConnCode.Add("3556", 8)
        'ConnCode.Add("3542", 9)
        'ConnCode.Add("3555", 10)
        'ConnCode.Add("4471", 11)
        'ConnCode.Add("4437", 12)
        'ConnCode.Add("4667", 13)
        'ConnCode.Add("4723", 14)
        'ConnCode.Add("4724", 15)
        'ConnCode.Add("4725", 16)
        'ConnCode.Add("4726", 17)
        'ConnCode.Add("4766", 18)
        ' end of 77466 commenting out.

        ' Added TS 12/Apr/2016. Request ref 77466
        ConnCode.Add("4810", 0)
        ConnCode.Add("4811", 1)
        ConnCode.Add("4812", 2)
        ConnCode.Add("4813", 3)
        ConnCode.Add("4814", 4)
        ConnCode.Add("4822", 5)
        ConnCode.Add("4823", 6)
        ConnCode.Add("4972", 7)
        ' end of new 77466 section

        ' Added TS 30/Nov/2016. Request ref 98745
        ConnCode.Add("4982", 8)
        ConnCode.Add("4984", 9)
        ConnCode.Add("4983", 10)
        ' end 98745 section

        'Added TS 11/Jan/2017. Request ref 102125
        ConnCode.Add("5004", 11)
        ' end of 102125 section

        CustomerType.Add("RES", "Residential")
        CustomerType.Add("SME", "Small Medium Enterprise")
        CustomerType.Add("COR", "SmallBusiness")
        CustomerType.Add("SB", "SmallBusiness")
        CustomerType.Add("IND", "SmallBusiness")

        PersonType.Add("PAH", "Primary Account Holder")
        PersonType.Add("SAH", "Secondary Account Holder")
        PersonType.Add("JAH", "Joint Account Holder")
        PersonType.Add("GUA", "Guarantor")
        PersonType.Add("AUT", "Authorised User")
        PersonType.Add("SME", "Small to Medium Enterprise")
        PersonType.Add("PRN", "Primary Recovery Name")
        PersonType.Add("SRN", "Secondary Recovery Name")
        PersonType.Add("ILW", "Illegal Worker")

        Nationality.Add("GB", "United Kingdom")
        Nationality.Add("BE", "Belgium")
        Nationality.Add("FI", "Finland")
        Nationality.Add("FR", "France")
        Nationality.Add("DE", "Germany")
        Nationality.Add("GR", "Greece")

        IDType.Add("BC", "Birth Certificate")
        IDType.Add("BILL", "Bill (e.g.: utility)")
        IDType.Add("CIF", "CompanyRegistration")
        IDType.Add("DVLC", "Drivers Licence")
        IDType.Add("NIE", "Resident Card")
        IDType.Add("NIF", "Spanish National ID Card")
        IDType.Add("NIN", "National Insurance Number")
        IDType.Add("SSN", "Social Security Number")
        IDType.Add("OTH", "Other")
        IDType.Add("PAS", "Passport")
        IDType.Add("STA", "Statement (e.g.: Bank/Credit Card)")
        IDType.Add("UKN", "Unknown")

        PersonAddressType.Add("BAD", "Business Address")
        PersonAddressType.Add("CAD", "Current Address")
        PersonAddressType.Add("EAD", "Employers Address")
        PersonAddressType.Add("INT", "International Address")
        PersonAddressType.Add("PAD", "Previous Address")
        PersonAddressType.Add("PAF", "Post Office Address File formatted address")
        PersonAddressType.Add("PRA", "Primary Recovery Address")
        PersonAddressType.Add("SAD", "Service Address")
        PersonAddressType.Add("UKN", "Unknown")
        PersonAddressType.Add("SRA", "Secondary Recovery Address")
        PersonAddressType.Add("UNC", "Unconfirmed")
        PersonAddressType.Add("COM", "Comaker address")
        PersonAddressType.Add("DCO", "Debt Counselling")

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String, InputLineArray() As String, ConnID As String = "", DebtNotes As String
        Dim OldFileLoadReason As String = "" ' added TS 24/May/2016. Request ref 82342
        Dim PrimaryOutputLineArray(123) As String, SecondaryOutputLineArray(123) As String
        Dim NewDebtFile(ConnCode.Count - 1) As String
        Dim NewDebtSumm(ConnCode.Count, 2) As Decimal, TotalNewDebt As Decimal
        Dim LineNumber As Integer, LoopCount As Integer

        Try
            FileDialog.Filter = "TDX ETC HHBD assignment files|*.csv|All files (*.*)|*.*"
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            ' Validate filename. Added TS 27/Apr/2016. Request ref 80637
            Dim FileDateStamp As Date
            If DateTime.TryParseExact(Strings.Right(FileName, 8), "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None, FileDateStamp) Then
                If DateTime.Today.Subtract(FileDateStamp).Days > 1 Then
                    If MessageBox.Show("File is more than one day old. Proceed?", Me.Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
                        Return
                    Else ' added TS 24/May/2016. Request ref 82342
                        OldFileLoadReason = InputBox("Please enter the reason for loading an old file.", Me.Text) ' added TS 24/May/2016. Request ref 82342
                        If OldFileLoadReason = "" Then Return ' added TS 24/May/2016. Request ref 82342
                    End If
                End If
            Else
                MessageBox.Show("Cannot parse filename", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If
            ' End of 80637 section.

            For Each OldFile As String In Directory.GetFiles(InputFilePath, "*" & FileName & "_*.txt")
                File.Delete(OldFile)
            Next OldFile

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents) + (UBound(NewDebtFile) + 1) + 1 ' +1 for audit log

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1

                If LineNumber = 1 Then ' validate header
                    If InputLine.Split(",")(4) <> UBound(FileContents) - 1 Then ErrorLog &= "Count total in the header (" & InputLine.Split(",")(4) & ") does not match file contents (" & (UBound(FileContents) - 1).ToString & ")." & vbCrLf
                    Continue For
                End If

                If LineNumber = UBound(FileContents) + 1 Then Continue For ' skip footer line

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                InputLine = InputLine.Replace("|", "") ' added TS 11/Jan/2016 as a precaution. We have never received pipes in this file but if we did it would thouw out the filter file. Request ref 70534
                'InputLineArray = InputLine.Split(",")
                InputLineArray = Split(InputLine, ",", """")

                If UBound(InputLineArray) <> 118 Then ErrorLog &= "Unexpected line length found at line number " & LineNumber.ToString & vbCrLf ' Added TS Request ref 71045

                ' Identify the client scheme
                Dim ClientBrand = InputLineArray(7)
                Dim SegmentName = InputLineArray(12)
                Dim PrimaryProductDescription = InputLineArray(86)

                ' The following commented out TS 12/Apr/2016. Request ref 77466
                'Select Case ClientBrand
                '    Case "Self Assessment"
                '        Select Case SegmentName
                '            Case "SAPC"
                '                ConnID = "3549"
                '            Case "PRSAP1"
                '                ConnID = "3552"
                '            Case "PRSAP2"
                '                ConnID = "3553"
                '            Case "PRSAP3"
                '                ConnID = "3554"
                '            Case "SALC"
                '                ConnID = "3541"
                '            Case "PRSAL1"
                '                ConnID = "3550"
                '            Case "PRSAL2"
                '                ConnID = "3551"
                '            Case Else
                '                ConnID = "-1"
                '        End Select
                '    Case "VAT"
                '        Select Case SegmentName
                '            Case "VATC"
                '                ConnID = "3543"
                '            Case "PSVAT"
                '                ConnID = "3556"
                '            Case Else
                '                ConnID = "-1"
                '        End Select
                '    Case "PAYE"
                '        Select Case SegmentName
                '            Case "PAYC"
                '                ConnID = "3542"
                '            Case "PSPAY"
                '                ConnID = "3555"
                '            Case Else
                '                ConnID = "-1"
                '        End Select
                '    Case "CorporationTax"
                '        Select Case SegmentName
                '            Case "CTC"
                '                ConnID = "-1"
                '            Case Else
                '                ConnID = "-1"
                '        End Select
                '    Case "Extending Tax Credits"
                '        ' the following commented out TS 15/Oct/2015. Request ref 62703
                '        'Select Case PrimaryProductDescription
                '        '    Case "Single Tax Credits"
                '        '        ConnID = "4471"
                '        '    Case "Dual Tax Credits"
                '        '        ConnID = "4437"
                '        '    Case "Household Breakdown"
                '        '        ConnID = "4667"
                '        '    Case Else
                '        '        ConnID = "-1"
                '        'End Select
                '        ' the following added TS 15/Oct/2015. Request ref 62703
                '        Select Case SegmentName
                '            Case "PCON"
                '                ConnID = "4723"
                '            Case "PETCA"
                '                ConnID = "4724"
                '            Case "PETCB"
                '                ConnID = "4725"
                '            Case "PETCT"
                '                ConnID = "4726"
                '            Case "SEG6"
                '                ConnID = "4667"
                '                ' The following added TS 14/Jan/2016. Request ref 71045
                '            Case "FCON"
                '                ConnID = "4723"
                '            Case "FETCA"
                '                ConnID = "4724"
                '            Case "FETCB"
                '                ConnID = "4725"
                '            Case "FETCT"
                '                ConnID = "4726"
                '            Case "FSEG6"
                '                ConnID = "4667"
                '                ' The following added TS 25/Jan/2016. Request ref 71945
                '            Case "CB"
                '                ConnID = "4766"
                '            Case "CM"
                '                ConnID = "4766"
                '            Case Else
                '                ConnID = "-1"
                '        End Select
                '    Case Else
                '        ConnID = "-1"
                'End Select
                ' end of 77466 commenting out.

                ' Added TS 12/Apr/2016. Request ref 77466
                Select Case SegmentName
                    Case "FCON"
                        ConnID = "4810"
                    Case "FTCA"
                        ConnID = "4811"
                    Case "FTC"
                        ConnID = "4812"
                    Case "FTCT"
                        ConnID = "4813"
                    Case "FCONH"
                        ConnID = "4814"
                    Case "FTCH"
                        ConnID = "4814"
                    Case "PTCU"
                        ConnID = "4822"
                        'Case "TCPC" commented out TS. Request ref 94579
                        '    ConnID = "4823"
                        ' The following added TS 07/Oct/2016. Request ref 92089. Amended 13/Oct/2016. Request ref 94579
                    Case "TCPC1"
                        ConnID = "4823"
                    Case "TCPC2"
                        ConnID = "4972"
                        ' end of 94579 section
                        ' The following added TS 30/Nov/2016. Request ref 98745
                    Case "PRTIQ1"
                        ConnID = "4982"
                    Case "PRTIQ2"
                        ConnID = "4984"
                    Case "PRTIQC"
                        ConnID = "4983"
                        ' end of 98745 section
                        ' The following added TS 11/Jan/2017. Request ref 102125
                    Case "STC"
                        ConnID = "5004"
                        ' end of 102125 section
                    Case Else
                        ConnID = "-1"
                End Select
                ' end of new 77466 section

                ' remove double quotes from addresses
                For LoopCount = 51 To 73
                    If {58, 66}.Contains(LoopCount) Then Continue For
                    InputLineArray(LoopCount) = InputLineArray(LoopCount).Replace("""", "")
                Next LoopCount

                If ConnCode.ContainsKey(ConnID) Then

                    If ConnID <> "4814" Then ' changed from 4667. TS 12/Apr/2016. Request ref 77466

                        DebtNotes = ToNote(InputLineArray(0), "AccountID", ";")(0) & _
                               ToNote(InputLineArray(5), "Outstanding balance as per Clients system", ";")(0) & _
                               ToNote(InputLineArray(7), "Client Brand", ";")(0) & _
                               ToNote(InputLineArray(8), "Debt country of origin", ";")(0) & _
                               ToNote(InputLineArray(9), "Assignment ID", ";")(0) & _
                               ToNote(InputLineArray(10), "Assignment Type Code", ";")(0) & _
                               ToNote(InputLineArray(11), "Total Amount Assigned", ";")(0) & _
                               ToNote(InputLineArray(12), "Segment Name", ";")(0)

                        If CustomerType.ContainsKey(InputLineArray(13)) Then
                            DebtNotes &= ToNote(CustomerType(InputLineArray(13)), "Small Business or Residential Customer", ";")(0)
                        Else
                            DebtNotes &= ToNote("??", "Small Business or Residential Customer", ";")(0)
                            ErrorLog &= "Unexpected PrimaryCustomerTypeCode of " & InputLineArray(13) & " found at line number " & LineNumber.ToString & vbCrLf
                        End If

                        DebtNotes &= ToNote(InputLineArray(14), "TDX unique identifier", ";")(0) & _
                                     ToNote(InputLineArray(15), "Person Type Code", ";")(0)

                        If PersonType.ContainsKey(InputLineArray(15)) Then
                            DebtNotes &= ToNote(PersonType(InputLineArray(15)), "Primary Person", ";")(0)
                        Else
                            DebtNotes &= ToNote("??", "Primary Person", ";")(0)
                            ErrorLog &= "Unexpected PersonTypeCode of " & InputLineArray(15) & " found at line number " & LineNumber.ToString & vbCrLf
                        End If

                        If Nationality.ContainsKey(InputLineArray(20)) Then
                            DebtNotes &= ToNote(Nationality(InputLineArray(20)), "Main Debtor Nationality", ";")(0)
                        Else
                            DebtNotes &= ToNote(InputLineArray(20), "Main Debtor Nationality", ";")(0)
                        End If

                        DebtNotes &= ToNote(InputLineArray(21), "PrimaryIDTypeCode", ";")(0)

                        If IDType.ContainsKey(InputLineArray(21)) Then
                            DebtNotes &= ToNote(IDType(InputLineArray(21)), "PrimaryIDTypeDescription", ";")(0)
                        Else
                            DebtNotes &= ToNote(InputLineArray(21), "PrimaryIDTypeDescription", ";")(0)
                        End If

                        DebtNotes &= ToNote(InputLineArray(22), "Primary ID Detail 1", ";")(0) & _
                                     ToNote(InputLineArray(24), "Debtor 1 special needs", ";")(0) & _
                                     ToNote(InputLineArray(25), "Debtor 1 Bank Account Number", ";")(0) & _
                                     ToNote(InputLineArray(32), "Name 2 TDX unique identifier", ";")(0)

                        If PersonType.ContainsKey(InputLineArray(33)) Then
                            DebtNotes &= ToNote(PersonType(InputLineArray(33)), "Name 2 type description", ";")(0)
                        ElseIf Not String.IsNullOrEmpty(InputLineArray(33)) Then
                            DebtNotes &= ToNote("??", "Name 2 type description", ";")(0)
                            ErrorLog &= "Unexpected SecondaryPersonTypeCode of " & InputLineArray(33) & " found at line number " & LineNumber.ToString & vbCrLf
                        End If

                        DebtNotes &= ToNote(InputLineArray(37), "Name 2 DOB", ";")(0)

                        If Nationality.ContainsKey(InputLineArray(38)) Then
                            DebtNotes &= ToNote(Nationality(InputLineArray(38)), "Debtor 2 Nationality", ";")(0)
                        Else
                            DebtNotes &= ToNote(InputLineArray(38), "Debtor 2 Nationality", ";")(0)
                        End If

                        DebtNotes &= ToNote(InputLineArray(39), "Debtor 2 ID type", ";")(0)

                        If IDType.ContainsKey(InputLineArray(39)) Then
                            DebtNotes &= ToNote(IDType(InputLineArray(39)), "SecondaryIDTypeDescription", ";")(0)
                        Else
                            DebtNotes &= ToNote(InputLineArray(39), "SecondaryIDTypeDescription", ";")(0)
                        End If

                        DebtNotes &= ToNote(InputLineArray(40), "Debtor 2 ID Detail 1", ";")(0) & _
                                     ToNote(InputLineArray(41), "Debtor 2 Email", ";")(0) & _
                                     ToNote(InputLineArray(42), "Debtor 2 special needs", ";")(0) & _
                                     ToNote(InputLineArray(43), "Debtor 2 Bank Account Number", ";")(0) & _
                                     ToNote(InputLineArray(44), "SecondaryPhoneTypeCode1", ";")(0) & _
                                     ToNote(InputLineArray(45), "Debtor 2 Home Tel Number", ";")(0) & _
                                     ToNote(InputLineArray(46), "SecondaryPhoneTypeCode2", ";")(0) & _
                                     ToNote(InputLineArray(47), "Debtor 2 Work Tel Number", ";")(0) & _
                                     ToNote(InputLineArray(48), "SecondaryPhoneTypeCode3", ";")(0) & _
                                     ToNote(InputLineArray(49), "SecondaryTelephoneNo3", ";")(0) & _
                                     ToNote(InputLineArray(50), "PersonAddressTypeCode1", ";")(0)

                        If SegmentName <> "TCPC2" Then ' added TS 17/Oct/2016. Request ref 94747

                            If InputLineArray(50) <> "CAD" Then ErrorLog &= "CAD not found in PersonAddressTypeCode1 (col51) at line number " & LineNumber.ToString & vbCrLf ' Added TS 11/Jan/2016. Request ref 70543

                            If PersonAddressType.ContainsKey(InputLineArray(50)) Then
                                DebtNotes &= ToNote(PersonAddressType(InputLineArray(50)), "PersonAddressType1", ";")(0)
                            Else
                                DebtNotes &= ToNote(InputLineArray(50), "PersonAddressType1", ";")(0)
                            End If

                            DebtNotes &= ToNote(InputLineArray(58), "PersonAddressTypeCode2", ";")(0)

                            If PersonAddressType.ContainsKey(InputLineArray(58)) Then
                                DebtNotes &= ToNote(PersonAddressType(InputLineArray(58)), "PersonAddressType2", ";")(0)
                            Else
                                DebtNotes &= ToNote(InputLineArray(58), "PersonAddressType2", ";")(0)
                            End If

                            DebtNotes &= ToNote(InputLineArray(66), "PersonAddressTypeCode3", ";")(0)

                            If PersonAddressType.ContainsKey(InputLineArray(66)) Then
                                DebtNotes &= ToNote(PersonAddressType(InputLineArray(66)), "PersonAddressType3", ";")(0)
                            Else
                                DebtNotes &= ToNote(InputLineArray(66), "PersonAddressType3", ";")(0)
                            End If

                            DebtNotes &= ToNote(ConcatFields({InputLineArray(67), InputLineArray(68), InputLineArray(69), InputLineArray(70), InputLineArray(71), InputLineArray(72), InputLineArray(73)}, ",") _
                                                , "Previous Address", ";")(0)

                        Else ' the following added TS 17/Oct/2016. Request ref 94747


                            If PersonAddressType.ContainsKey(InputLineArray(50)) Then
                                DebtNotes &= ToNote(PersonAddressType(InputLineArray(50)), "PersonAddressType1", ";")(0)
                            Else
                                DebtNotes &= ToNote(InputLineArray(50), "PersonAddressType1", ";")(0)
                            End If

                            DebtNotes &= ToNote(InputLineArray(58), "PersonAddressTypeCode2", ";")(0)

                            If PersonAddressType.ContainsKey(InputLineArray(58)) Then
                                DebtNotes &= ToNote(PersonAddressType(InputLineArray(58)), "PersonAddressType2", ";")(0)
                            Else
                                DebtNotes &= ToNote(InputLineArray(58), "PersonAddressType2", ";")(0)
                            End If

                            DebtNotes &= ToNote(ConcatFields({InputLineArray(51), InputLineArray(52), InputLineArray(53), InputLineArray(54), InputLineArray(55), InputLineArray(56), InputLineArray(57)}, ",") _
                                               , "Person Address 1", ";")(0)

                            DebtNotes &= ToNote(ConcatFields({InputLineArray(59), InputLineArray(60), InputLineArray(61), InputLineArray(62), InputLineArray(63), InputLineArray(64), InputLineArray(65)}, ",") _
                                               , "Person Address 2", ";")(0)

                            If InputLineArray(58) = "UNC" Then
                                ' Move all address details to the first section
                                InputLineArray(50) = InputLineArray(58)
                                InputLineArray(51) = InputLineArray(59)
                                InputLineArray(52) = InputLineArray(60)
                                InputLineArray(53) = InputLineArray(61)
                                InputLineArray(54) = InputLineArray(62)
                                InputLineArray(55) = InputLineArray(63)
                                InputLineArray(56) = InputLineArray(64)
                                InputLineArray(57) = InputLineArray(65)
                            End If

                            DebtNotes &= ToNote(InputLineArray(66), "PersonAddressTypeCode3", ";")(0)

                            If PersonAddressType.ContainsKey(InputLineArray(66)) Then
                                DebtNotes &= ToNote(PersonAddressType(InputLineArray(66)), "PersonAddressType3", ";")(0)
                            Else
                                DebtNotes &= ToNote(InputLineArray(66), "PersonAddressType3", ";")(0)
                            End If

                            DebtNotes &= ToNote(ConcatFields({InputLineArray(67), InputLineArray(68), InputLineArray(69), InputLineArray(70), InputLineArray(71), InputLineArray(72), InputLineArray(73)}, ",") _
                                                , "Person Address 3", ";")(0)

                            If InputLineArray(66) = "UNC" Then
                                ' Move all address details to the first section
                                InputLineArray(50) = InputLineArray(66)
                                InputLineArray(51) = InputLineArray(67)
                                InputLineArray(52) = InputLineArray(68)
                                InputLineArray(53) = InputLineArray(69)
                                InputLineArray(54) = InputLineArray(70)
                                InputLineArray(55) = InputLineArray(71)
                                InputLineArray(56) = InputLineArray(72)
                                InputLineArray(57) = InputLineArray(73)
                            End If

                            If InputLineArray(50) <> "UNC" Then ErrorLog &= "UNC not found in PersonAddressTypeCode1 (col51) at line number " & LineNumber.ToString & vbCrLf

                        End If ' added TS 17/Oct/2016. Request ref 94747

                        DebtNotes &= ToNote(InputLineArray(74), "Last Payment Amount", ";")(0) & _
                                     ToNote(InputLineArray(75), "Last Payment Date", ";")(0) & _
                                     ToNote(InputLineArray(76), "Total number of payments made by the debtor to date", ";")(0) & _
                                     ToNote(InputLineArray(77), "Value of payments made to date by debtor", ";")(0) & _
                                     ToNote(InputLineArray(78), "Method in which the debtor usually pays", ";")(0) & _
                                     ToNote(InputLineArray(79), "Default date", ";")(0) & _
                                     ToNote(InputLineArray(80), "Default amount", ";")(0) & _
                                     ToNote(InputLineArray(81), "Termination reason", ";")(0) & _
                                     ToNote(InputLineArray(82), "Amount owing to date not including fees", ";")(0) & _
                                     ToNote(InputLineArray(83), "Fees charged to date", ";")(0)

                        DebtNotes &= ToNote(InputLineArray(84), "Interest to date", ";")(0) & _
                                     ToNote(InputLineArray(85), "PrimaryProductNumber", ";")(0) & _
                                     ToNote(InputLineArray(86), "PrimaryProductDescription", ";")(0) & _
                                     ToNote(InputLineArray(87), "PrimaryProductValue", ";")(0) & _
                                     ToNote(InputLineArray(88), "PrimaryProductOpenDate", ";")(0) & _
                                     ToNote(InputLineArray(89), "PrimaryProductCloseDate", ";")(0) & _
                                     ToNote(InputLineArray(90), "Product1AccountNumber", ";")(0) & _
                                     ToNote(InputLineArray(91), "Product1Description", ";")(0) & _
                                     ToNote(InputLineArray(92), "Product1Value", ";")(0) & _
                                     ToNote(InputLineArray(93), "Product1OpenDate", ";")(0)

                        DebtNotes &= ToNote(InputLineArray(94), "Product1CloseDate", ";")(0) & _
                                     ToNote(InputLineArray(95), "Product2AccountNumber", ";")(0) & _
                                     ToNote(InputLineArray(96), "Product2Description", ";")(0) & _
                                     ToNote(InputLineArray(97), "Product2AccountValue", ";")(0) & _
                                     ToNote(InputLineArray(98), "Product2OpenDate", ";")(0) & _
                                     ToNote(InputLineArray(99), "Product2CloseDate", ";")(0) & _
                                     ToNote(InputLineArray(100), "Product3AccountNumber", ";")(0) & _
                                     ToNote(InputLineArray(101), "Product3Description", ";")(0) & _
                                     ToNote(InputLineArray(102), "Product3Value", ";")(0) & _
                                     ToNote(InputLineArray(103), "Product3OpenDate", ";")(0)

                        DebtNotes &= ToNote(InputLineArray(104), "Product3CloseDate", ";")(0) & _
                                     ToNote(InputLineArray(105), "PropensityScore", ";")(0) & _
                                     ToNote(InputLineArray(106), "LitigationStatus", ";")(0) & _
                                     ToNote(InputLineArray(107), "ContractAvailable", ";")(0) & _
                                     ToNote(InputLineArray(108), "Region", ";")(0) & _
                                     ToNote(InputLineArray(109), "NumberOfDocuments", ";")(0) & _
                                     ToNote(InputLineArray(110), "OriginalPrincipleBalance", ";")(0) & _
                                     ToNote(InputLineArray(111), "ClientDebtorType", ";")(0) & _
                                     ToNote(InputLineArray(112), "AccountStatus", ";")(0) & _
                                     ToNote(InputLineArray(113), "ServiceRestrictionDate", ";")(0)

                        DebtNotes &= ToNote(InputLineArray(114), "NumberOfServices", ";")(0) & _
                                     ToNote(InputLineArray(115), "SAPAccountNumber", ";")(0) & _
                                     ToNote(InputLineArray(116), "TypeOfPortfolio", ";")(0) & _
                                     ToNote(InputLineArray(117), "SpareField1", ";")(0) & _
                                     ToNote(InputLineArray(118), "SpareField2", ";")(0) ' & _
                        'ToNote(InputLineArray(119), "SpareField3", ";")(0)

                        NewDebtFile(ConnCode(ConnID)) = Join(InputLineArray, Separator) & Separator

                        NewDebtFile(ConnCode(ConnID)) &= DebtNotes & vbCrLf

                        NewDebtSumm(ConnCode(ConnID), 0) += 1
                        NewDebtSumm(ConnCode(ConnID), 1) += InputLineArray(11)
                        TotalNewDebt += InputLineArray(11)
                        AppendToFile(InputFilePath & ConnID & "_" & FileName & "_PreProcessed.txt", NewDebtFile(ConnCode(ConnID)))


                    Else
                        ' Start to build up the two output files
                        ReDim PrimaryOutputLineArray(123)
                        ReDim SecondaryOutputLineArray(123)

                        For LoopCount = 0 To 10
                            PrimaryOutputLineArray(LoopCount) = InputLineArray(LoopCount)
                            SecondaryOutputLineArray(LoopCount) = InputLineArray(LoopCount)
                        Next LoopCount

                        PrimaryOutputLineArray(11) = Math.Ceiling(Math.Round(InputLineArray(11) * 100) / 2) / 100
                        SecondaryOutputLineArray(11) = InputLineArray(11) - PrimaryOutputLineArray(11)

                        PrimaryOutputLineArray(12) = InputLineArray(12)
                        SecondaryOutputLineArray(12) = InputLineArray(12)
                        PrimaryOutputLineArray(13) = InputLineArray(13)
                        SecondaryOutputLineArray(13) = InputLineArray(13)

                        For LoopCount = 14 To 31
                            PrimaryOutputLineArray(LoopCount) = InputLineArray(LoopCount)
                            SecondaryOutputLineArray(LoopCount) = InputLineArray(LoopCount + 18)
                        Next LoopCount

                        If InputLineArray(50) = "CAD" Then
                            ' Move all adress details to the first section
                            PrimaryOutputLineArray(50) = InputLineArray(50)
                            PrimaryOutputLineArray(51) = InputLineArray(51)
                            PrimaryOutputLineArray(52) = InputLineArray(52)
                            PrimaryOutputLineArray(53) = InputLineArray(53)
                            PrimaryOutputLineArray(54) = InputLineArray(54)
                            PrimaryOutputLineArray(55) = InputLineArray(55)
                            PrimaryOutputLineArray(56) = InputLineArray(56)
                            PrimaryOutputLineArray(57) = InputLineArray(57)
                        End If

                        If InputLineArray(58) = "CAD" Then
                            ' Move all adress details to the first section
                            PrimaryOutputLineArray(50) = InputLineArray(58)
                            PrimaryOutputLineArray(51) = InputLineArray(59)
                            PrimaryOutputLineArray(52) = InputLineArray(60)
                            PrimaryOutputLineArray(53) = InputLineArray(61)
                            PrimaryOutputLineArray(54) = InputLineArray(62)
                            PrimaryOutputLineArray(55) = InputLineArray(63)
                            PrimaryOutputLineArray(56) = InputLineArray(64)
                            PrimaryOutputLineArray(57) = InputLineArray(65)
                        End If

                        If InputLineArray(66) = "CAD" Then
                            ' Move all adress details to the first section
                            PrimaryOutputLineArray(50) = InputLineArray(66)
                            PrimaryOutputLineArray(51) = InputLineArray(67)
                            PrimaryOutputLineArray(52) = InputLineArray(68)
                            PrimaryOutputLineArray(53) = InputLineArray(69)
                            PrimaryOutputLineArray(54) = InputLineArray(70)
                            PrimaryOutputLineArray(55) = InputLineArray(71)
                            PrimaryOutputLineArray(56) = InputLineArray(72)
                            PrimaryOutputLineArray(57) = InputLineArray(73)
                        End If

                        If InputLineArray(50) = "SRA" Then
                            ' Move all adress details to the first section
                            SecondaryOutputLineArray(50) = InputLineArray(50)
                            SecondaryOutputLineArray(51) = InputLineArray(51)
                            SecondaryOutputLineArray(52) = InputLineArray(52)
                            SecondaryOutputLineArray(53) = InputLineArray(53)
                            SecondaryOutputLineArray(54) = InputLineArray(54)
                            SecondaryOutputLineArray(55) = InputLineArray(55)
                            SecondaryOutputLineArray(56) = InputLineArray(56)
                            SecondaryOutputLineArray(57) = InputLineArray(57)
                        End If

                        If InputLineArray(58) = "SRA" Then
                            ' Move all adress details to the first section
                            SecondaryOutputLineArray(50) = InputLineArray(58)
                            SecondaryOutputLineArray(51) = InputLineArray(59)
                            SecondaryOutputLineArray(52) = InputLineArray(60)
                            SecondaryOutputLineArray(53) = InputLineArray(61)
                            SecondaryOutputLineArray(54) = InputLineArray(62)
                            SecondaryOutputLineArray(55) = InputLineArray(63)
                            SecondaryOutputLineArray(56) = InputLineArray(64)
                            SecondaryOutputLineArray(57) = InputLineArray(65)
                        End If

                        If InputLineArray(66) = "SRA" Then
                            ' Move all adress details to the first section
                            SecondaryOutputLineArray(50) = InputLineArray(66)
                            SecondaryOutputLineArray(51) = InputLineArray(67)
                            SecondaryOutputLineArray(52) = InputLineArray(68)
                            SecondaryOutputLineArray(53) = InputLineArray(69)
                            SecondaryOutputLineArray(54) = InputLineArray(70)
                            SecondaryOutputLineArray(55) = InputLineArray(71)
                            SecondaryOutputLineArray(56) = InputLineArray(72)
                            SecondaryOutputLineArray(57) = InputLineArray(73)
                        End If

                        For LoopCount = 74 To 118
                            PrimaryOutputLineArray(LoopCount) = InputLineArray(LoopCount)
                            SecondaryOutputLineArray(LoopCount) = InputLineArray(LoopCount)
                        Next LoopCount

                        PrimaryOutputLineArray(120) = InputLineArray(11)
                        SecondaryOutputLineArray(120) = InputLineArray(11)
                        PrimaryOutputLineArray(121) = "Primary"
                        SecondaryOutputLineArray(121) = "Secondary"
                        PrimaryOutputLineArray(122) = InputLineArray(1)
                        SecondaryOutputLineArray(122) = InputLineArray(1)
                        PrimaryOutputLineArray(123) = PrimaryOutputLineArray(22)
                        SecondaryOutputLineArray(123) = SecondaryOutputLineArray(22)

                        ' now start building notes
                        DebtNotes = ToNote(InputLineArray(0), "AccountID", ";")(0) & _
                                    ToNote(InputLineArray(5), "Outstanding balance as per Clients system", ";")(0) & _
                                    ToNote(InputLineArray(7), "Client Brand", ";")(0) & _
                                    ToNote(InputLineArray(8), "Debt country of origin", ";")(0) & _
                                    ToNote(InputLineArray(9), "Assignment ID", ";")(0) & _
                                    ToNote(InputLineArray(10), "Assignment Type Code", ";")(0)
                        'DebtNotes &= ToNote(InputLineArray(11), "Total Amount Assigned", ";")(0) ' moved to later so that DebtNotes can be reused instead of duplicating nearly of all it for each debtor.
                        DebtNotes &= ToNote(InputLineArray(12), "Segment Name", ";")(0)

                        If CustomerType.ContainsKey(InputLineArray(13)) Then
                            DebtNotes &= ToNote(CustomerType(InputLineArray(13)), "Small Business or Residential Customer", ";")(0)
                        Else
                            DebtNotes &= ToNote("??", "Small Business or Residential Customer", ";")(0)
                            ErrorLog &= "Unexpected PrimaryCustomerTypeCode of " & InputLineArray(13) & " found at line number " & LineNumber.ToString & vbCrLf
                        End If

                        DebtNotes &= ToNote(InputLineArray(14), "TDX unique identifier", ";")(0) & _
                                     ToNote(InputLineArray(15), "Person Type Code", ";")(0)

                        If PersonType.ContainsKey(InputLineArray(15)) Then
                            DebtNotes &= ToNote(PersonType(InputLineArray(15)), "Primary Person", ";")(0)
                        Else
                            DebtNotes &= ToNote("??", "Primary Person", ";")(0)
                            ErrorLog &= "Unexpected PersonTypeCode of " & InputLineArray(15) & " found at line number " & LineNumber.ToString & vbCrLf
                        End If

                        If Nationality.ContainsKey(InputLineArray(20)) Then
                            DebtNotes &= ToNote(Nationality(InputLineArray(20)), "Main Debtor Nationality", ";")(0)
                        Else
                            DebtNotes &= ToNote(InputLineArray(20), "Main Debtor Nationality", ";")(0)
                        End If

                        DebtNotes &= ToNote(InputLineArray(21), "PrimaryIDTypeCode", ";")(0)

                        If IDType.ContainsKey(InputLineArray(21)) Then
                            DebtNotes &= ToNote(IDType(InputLineArray(21)), "PrimaryIDTypeDescription", ";")(0)
                        Else
                            DebtNotes &= ToNote(InputLineArray(21), "PrimaryIDTypeDescription", ";")(0)
                        End If

                        DebtNotes &= ToNote(InputLineArray(22), "Primary ID Detail1", ";")(0) & _
                                     ToNote(InputLineArray(24), "Debtor 1 special needs", ";")(0) & _
                                     ToNote(InputLineArray(25), "Debtor 1 Bank Account Number", ";")(0) & _
                                     ToNote(InputLineArray(32), "Name 2 TDX unique identifier", ";")(0)

                        If PersonType.ContainsKey(InputLineArray(33)) Then
                            DebtNotes &= ToNote(PersonType(InputLineArray(33)), "Name 2 type description", ";")(0)
                        ElseIf Not String.IsNullOrEmpty(InputLineArray(33)) Then
                            DebtNotes &= ToNote("??", "Name 2 type description", ";")(0)
                            ErrorLog &= "Unexpected SecondaryPersonTypeCode of " & InputLineArray(33) & " found at line number " & LineNumber.ToString & vbCrLf
                        End If

                        DebtNotes &= ToNote(InputLineArray(37), "Name 2 DOB", ";")(0)

                        If Nationality.ContainsKey(InputLineArray(38)) Then
                            DebtNotes &= ToNote(Nationality(InputLineArray(38)), "Debtor 2 Nationality", ";")(0)
                        Else
                            DebtNotes &= ToNote(InputLineArray(38), "Debtor 2 Nationality", ";")(0)
                        End If

                        DebtNotes &= ToNote(InputLineArray(39), "Debtor 2 ID type", ";")(0)

                        If IDType.ContainsKey(InputLineArray(39)) Then
                            DebtNotes &= ToNote(IDType(InputLineArray(39)), "SecondaryIDTypeDescription", ";")(0)
                        Else
                            DebtNotes &= ToNote(InputLineArray(39), "SecondaryIDTypeDescription", ";")(0)
                        End If

                        DebtNotes &= ToNote(InputLineArray(40), "Debtor 2 ID Detail 1", ";")(0) & _
                                     ToNote(InputLineArray(41), "Debtor 2 Email", ";")(0) & _
                                     ToNote(InputLineArray(42), "Debtor 2 special needs", ";")(0) & _
                                     ToNote(InputLineArray(43), "Debtor 2 Bank Account Number", ";")(0) & _
                                     ToNote(InputLineArray(44), "SecondaryPhoneTypeCode1", ";")(0) & _
                                     ToNote(InputLineArray(45), "Debtor 2 Home Tel Number", ";")(0) & _
                                     ToNote(InputLineArray(46), "SecondaryPhoneTypeCode2", ";")(0) & _
                                     ToNote(InputLineArray(47), "Debtor 2 Work Tel Number", ";")(0) & _
                                     ToNote(InputLineArray(48), "SecondaryPhoneTypeCode3", ";")(0) & _
                                     ToNote(InputLineArray(49), "SecondaryTelephoneNo3", ";")(0) & _
                                     ToNote(InputLineArray(50), "PersonAddressTypeCode1", ";")(0)

                        If PersonAddressType.ContainsKey(InputLineArray(50)) Then
                            DebtNotes &= ToNote(PersonAddressType(InputLineArray(50)), "PersonAddressType1", ";")(0)
                        Else
                            DebtNotes &= ToNote(InputLineArray(50), "PersonAddressType1", ";")(0)
                        End If

                        DebtNotes &= ToNote(ConcatFields({InputLineArray(51), InputLineArray(52), InputLineArray(53), InputLineArray(54), InputLineArray(55), InputLineArray(56), InputLineArray(57)}, ",") _
                                           , "Person Address 1", ";")(0)

                        DebtNotes &= ToNote(InputLineArray(58), "PersonAddressTypeCode2", ";")(0)


                        If PersonAddressType.ContainsKey(InputLineArray(58)) Then
                            DebtNotes &= ToNote(PersonAddressType(InputLineArray(58)), "PersonAddressType2", ";")(0)
                        Else
                            DebtNotes &= ToNote(InputLineArray(58), "PersonAddressType2", ";")(0)
                        End If

                        DebtNotes &= ToNote(ConcatFields({InputLineArray(59), InputLineArray(60), InputLineArray(61), InputLineArray(62), InputLineArray(63), InputLineArray(64), InputLineArray(65)}, ",") _
                                           , "Person Address 2", ";")(0)

                        DebtNotes &= ToNote(InputLineArray(66), "PersonAddressTypeCode3", ";")(0)

                        If PersonAddressType.ContainsKey(InputLineArray(66)) Then
                            DebtNotes &= ToNote(PersonAddressType(InputLineArray(66)), "PersonAddressType3", ";")(0)
                        Else
                            DebtNotes &= ToNote(InputLineArray(66), "PersonAddressType3", ";")(0)
                        End If

                        DebtNotes &= ToNote(ConcatFields({InputLineArray(67), InputLineArray(68), InputLineArray(69), InputLineArray(70), InputLineArray(71), InputLineArray(72), InputLineArray(73)}, ",") _
                                            , "Person Address 3", ";")(0)

                        DebtNotes &= ToNote(InputLineArray(74), "Last Payment Amount", ";")(0) & _
                                     ToNote(InputLineArray(75), "Last Payment Date", ";")(0) & _
                                     ToNote(InputLineArray(76), "Total number of payments made by the debtor to date", ";")(0) & _
                                     ToNote(InputLineArray(77), "Value of payments made to date by debtor", ";")(0) & _
                                     ToNote(InputLineArray(78), "Method in which the debtor usually pays", ";")(0) & _
                                     ToNote(InputLineArray(79), "Default date", ";")(0) & _
                                     ToNote(InputLineArray(80), "Default amount", ";")(0) & _
                                     ToNote(InputLineArray(81), "Termination reason", ";")(0) & _
                                     ToNote(InputLineArray(82), "Amount owing to date not including fees", ";")(0) & _
                                     ToNote(InputLineArray(83), "Fees charged to date", ";")(0)

                        DebtNotes &= ToNote(InputLineArray(84), "Interest to date", ";")(0) & _
                                     ToNote(InputLineArray(85), "PrimaryProductNumber", ";")(0) & _
                                     ToNote(InputLineArray(86), "PrimaryProductDescription", ";")(0) & _
                                     ToNote(InputLineArray(87), "PrimaryProductValue", ";")(0) & _
                                     ToNote(InputLineArray(88), "PrimaryProductOpenDate", ";")(0) & _
                                     ToNote(InputLineArray(89), "PrimaryProductCloseDate", ";")(0) & _
                                     ToNote(InputLineArray(90), "Product1AccountNumber", ";")(0) & _
                                     ToNote(InputLineArray(91), "Product1Description", ";")(0) & _
                                     ToNote(InputLineArray(92), "Product1Value", ";")(0) & _
                                     ToNote(InputLineArray(93), "Product1OpenDate", ";")(0)

                        DebtNotes &= ToNote(InputLineArray(94), "Product1CloseDate", ";")(0) & _
                                     ToNote(InputLineArray(95), "Product2AccountNumber", ";")(0) & _
                                     ToNote(InputLineArray(96), "Product2Description", ";")(0) & _
                                     ToNote(InputLineArray(97), "Product2AccountValue", ";")(0) & _
                                     ToNote(InputLineArray(98), "Product2OpenDate", ";")(0) & _
                                     ToNote(InputLineArray(99), "Product2CloseDate", ";")(0) & _
                                     ToNote(InputLineArray(100), "Product3AccountNumber", ";")(0) & _
                                     ToNote(InputLineArray(101), "Product3Description", ";")(0) & _
                                     ToNote(InputLineArray(102), "Product3Value", ";")(0) & _
                                     ToNote(InputLineArray(103), "Product3OpenDate", ";")(0)

                        DebtNotes &= ToNote(InputLineArray(104), "Product3CloseDate", ";")(0) & _
                                     ToNote(InputLineArray(105), "PropensityScore", ";")(0) & _
                                     ToNote(InputLineArray(106), "LitigationStatus", ";")(0) & _
                                     ToNote(InputLineArray(107), "ContractAvailable", ";")(0) & _
                                     ToNote(InputLineArray(108), "Region", ";")(0) & _
                                     ToNote(InputLineArray(109), "NumberOfDocuments", ";")(0) & _
                                     ToNote(InputLineArray(110), "OriginalPrincipleBalance", ";")(0) & _
                                     ToNote(InputLineArray(111), "ClientDebtorType", ";")(0) & _
                                     ToNote(InputLineArray(112), "AccountStatus", ";")(0) & _
                                     ToNote(InputLineArray(113), "ServiceRestrictionDate", ";")(0)

                        DebtNotes &= ToNote(InputLineArray(114), "NumberOfServices", ";")(0) & _
                                     ToNote(InputLineArray(115), "SAPAccountNumber", ";")(0) & _
                                     ToNote(InputLineArray(116), "TypeOfPortfolio", ";")(0) & _
                                     ToNote(InputLineArray(117), "SpareField1", ";")(0) & _
                                     ToNote(InputLineArray(118), "SpareField2", ";")(0) '& _
                        'ToNote(InputLineArray(119), "SpareField3", ";")(0)

                        NewDebtSumm(ConnCode(ConnID), 0) += 1

                        'Output the first debtor
                        NewDebtFile(ConnCode(ConnID)) = Join(PrimaryOutputLineArray, HHBDSeparator) & HHBDSeparator
                        NewDebtFile(ConnCode(ConnID)) &= DebtNotes & ToNote(PrimaryOutputLineArray(11), "Total Amount Assigned", ";")(0) & vbCrLf
                        NewDebtSumm(ConnCode(ConnID), 1) += PrimaryOutputLineArray(11)
                        NewDebtSumm(ConnCode(ConnID), 2) += 1
                        TotalNewDebt += PrimaryOutputLineArray(11)

                        AppendToFile(InputFilePath & ConnID & "_" & FileName & "_PreProcessed.txt", NewDebtFile(ConnCode(ConnID)))

                        'then the second
                        NewDebtFile(ConnCode(ConnID)) = Join(SecondaryOutputLineArray, HHBDSeparator) & HHBDSeparator
                        NewDebtFile(ConnCode(ConnID)) &= DebtNotes & ToNote(SecondaryOutputLineArray(11), "Total Amount Assigned", ";")(0) & vbCrLf
                        NewDebtSumm(ConnCode(ConnID), 1) += SecondaryOutputLineArray(11)
                        NewDebtSumm(ConnCode(ConnID), 2) += 1
                        TotalNewDebt += SecondaryOutputLineArray(11)

                        AppendToFile(InputFilePath & ConnID & "_" & FileName & "_PreProcessed.txt", NewDebtFile(ConnCode(ConnID)))

                    End If
                Else
                    ErrorLog &= "Unable to identify client scheme at line number " & LineNumber.ToString & vbCrLf
                End If

            Next InputLine

            If FileContents(0).Split(",")(3) <> TotalNewDebt Then ErrorLog &= "Debt total in the header (" & FileContents(0).Split(",")(3) & ") does not match file contents (" & TotalNewDebt.ToString & ")." & vbCrLf

            ProgressBar.Value += 1

            AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            If OldFileLoadReason <> "" Then AuditLog &= "File was generated before yesterday. Reason for loading: " & OldFileLoadReason & vbCrLf & vbCrLf ' added TS 24/May/2016. Request ref 82342

            For NewDebtFileCount As Integer = 0 To UBound(NewDebtFile)
                ProgressBar.Value += 1
                If NewDebtFile(NewDebtFileCount) <> "" Then
                    OutputFiles.Add(InputFilePath & ConnIDList(NewDebtFileCount) & "_" & FileName & "_PreProcessed.txt")
                    AuditLog &= "Clientscheme: " & ConnIDList(NewDebtFileCount) & " - " & PreProcessData.GetConnIDName(ConnIDList(NewDebtFileCount)) & vbCrLf
                    AuditLog &= "Number of new cases: " & NewDebtSumm(NewDebtFileCount, 0).ToString & vbCrLf
                    If NewDebtSumm(NewDebtFileCount, 2) > 0 Then AuditLog &= "Number of split new cases: " & NewDebtSumm(NewDebtFileCount, 2).ToString & vbCrLf
                    AuditLog &= "Total balance of new cases: " & NewDebtSumm(NewDebtFileCount, 1).ToString & vbCrLf & vbCrLf
                End If
            Next NewDebtFileCount

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)
            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", LogHeader & ExceptionLog)

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub


    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub

    Public Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, ByVal Terminator As String, Optional ByVal NoteLength As Integer = 249) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 1 + Terminator.Length) ' + 1 is for :
        Dim PreviousLength As Integer, CharPos As Integer, EndPos As Integer
        Dim NotePadded As Boolean
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            If UBound(Note) > 0 Then
                For NoteCount = 0 To UBound(Note)
                    NotePadded = False
                    EndPos = Math.Min(NoteLength * (NoteCount + 1), FreeText.Length)
                    CharPos = EndPos
                    Do
                        If FreeText.Substring(CharPos - 1, 1) = " " Then
                            FreeText = FreeText.Insert(CharPos, Space(NoteLength * (NoteCount + 1) - CharPos))
                            NotePadded = True
                        End If
                        CharPos += -1
                    Loop Until CharPos = NoteLength * NoteCount Or NotePadded

                Next NoteCount
            End If

            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = NoteTag & ": " & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)).Trim ' this and the line below could be done in one line but becomes difficult to read
                Note(NoteCount) = (Note(NoteCount) & Terminator).PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function

End Class


﻿Public Class Form1
    Dim start_date As Date
    Dim end_date As Date
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        start_date = Now
        start_date = DateAdd(DateInterval.Month, -1, start_date)
        start_date = CDate(Format(start_date, "yyyy") & " " & Format(start_date, "MM") & " 1")
        end_date = DateAdd(DateInterval.Month, 1, start_date)
        run_dtp.Value = start_date

        run_stats()
    End Sub

    Private Sub delbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles delbtn.Click
        If MsgBox("Delete rows for selected month?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            Me.Allocation_statisticsTableAdapter.DeleteQuery1(start_date, end_date)
            MsgBox("Rows deleted")
        Else
            MsgBox("No rows deleted")
        End If

    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click

    End Sub
    Private Sub run_stats()
        Dim csID As Integer


        param2 = "select _createdDate, text, debtorID from Note where _createddate >= '" & Format(start_date, "yyyy-MM-dd") & "'" &
            " and _createdDate < '" & Format(end_date, "yyyy-MM-dd") &
               "' and type = 'Stage'"
        Dim note_ds As DataSet = get_dataset("onestep", param2)
        Dim note_rows As Integer = no_of_rows - 1
        For idx = 0 To note_rows
            Try
                ProgressBar1.Value = (idx / note_rows) * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            Dim debtorID As Integer = note_ds.Tables(0).Rows(idx).Item(2)
            param2 = "select clientschemeID from Debtor where _rowid = " & debtorID
            Dim debt_ds As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            csID = debt_ds.Tables(0).Rows(0).Item(0)
            param2 = "select branchID from clientScheme where _rowid = " & csID
            Dim cs_ds As DataSet = get_dataset("onestep", param2)
            If cs_ds.Tables(0).Rows(0).Item(0) <> 1 Then
                Continue For
            End If

            Dim note_text As String = note_ds.Tables(0).Rows(idx).Item(1)

            Dim alloc_type As String
            If InStr(note_text, "F C") > 0 Then
                alloc_type = "FC"
            ElseIf InStr(note_text, "FurtherVanAttendance") > 0 Then
                alloc_type = "FVAN"
            ElseIf InStr(note_text, "Van Attendance") > 0 Then
                alloc_type = "VAN"
            Else
                Continue For
            End If
            Dim note_created_date As Date = note_ds.Tables(0).Rows(idx).Item(0)

            'can't use visit table - maybe no call record put on if no visit.
            'param2 = "select date_allocated, stageName, bailiffID from Visit where debtorID = " & debtorID &
            '    " and date_allocated >= '" & Format(note_created_date, "yyyy-MM-dd") & "'" &
            '    " order by date_allocated"
            'Dim visit_ds As DataSet = get_dataset("onestep", param2)
            'Dim visit_rows As Integer = no_of_rows - 1

            param2 = "select type, text, _createdDate from Note where debtorID = " & debtorID &
                " and _createdDate > '" & Format(note_created_date, "yyyy-MM-dd") & "'" &
                " order by _createdDate"
            Dim note2_ds As DataSet = get_dataset("onestep", param2)
            Dim note2_rows As Integer = no_of_rows - 1
            Dim fc_allocated As Integer = 0
            Dim van_allocated As Integer = 0
            Dim fvan_allocated As Integer = 0
            Dim fc_waiting As Integer = 1
            Dim van_waiting As Integer = 1
            Dim fvan_waiting As Integer = 1
            Dim date_allocated As Date = ("jan 1 1900")
            Dim alloc_stage_name As String = ""
            For idx3 = 0 To note2_rows
                If note2_ds.Tables(0).Rows(idx3).Item(0) = "Stage" Then
                    alloc_stage_name = note2_ds.Tables(0).Rows(idx3).Item(1)
                    Continue For
                End If
                If InStr(note2_ds.Tables(0).Rows(idx3).Item(1), "Bailiff") > 0 And
                    InStr(note2_ds.Tables(0).Rows(idx3).Item(1), "allocated on") > 0 Then
                    If alloc_type = "FC" And InStr(alloc_stage_name, "F C") > 0 Then
                        date_allocated = note2_ds.Tables(0).Rows(idx3).Item(2)
                        fc_allocated = 1
                        If Format(note_created_date, "MM yyyy") = Format(date_allocated, "MM yyyy") Then
                            fc_waiting = 0
                        End If
                    ElseIf alloc_type = "FVAN" And InStr(alloc_stage_name, "Further") > 0 Then
                        date_allocated = note2_ds.Tables(0).Rows(idx3).Item(2)
                        fvan_allocated = 1
                        If Format(note_created_date, "MM yyyy") = Format(date_allocated, "MM yyyy") Then
                            fvan_waiting = 0
                        End If
                    ElseIf alloc_type = "VAN" And InStr(alloc_stage_name, "Van") > 0 Then
                        date_allocated = note2_ds.Tables(0).Rows(idx3).Item(2)
                        van_allocated = 1
                        If Format(note_created_date, "MM yyyy") = Format(date_allocated, "MM yyyy") Then
                            van_waiting = 0
                        End If
                    End If
                    Exit For
                End If

            Next
            Select Case alloc_type
                Case "FC"
                    insert_row(alloc_type, debtorID, date_allocated, fc_waiting, fc_allocated, note_created_date)
                Case "VAN"
                    insert_row(alloc_type, debtorID, date_allocated, van_waiting, van_allocated, note_created_date)
                Case "FVAN"
                    insert_row(alloc_type, debtorID, date_allocated, fvan_waiting, fvan_allocated, note_created_date)
            End Select

        Next

        ' MsgBox("Completed")
        Me.Close()
    End Sub
    Private Sub insert_row(ByVal alloc_type As String, ByVal debtorid As Integer, ByVal stage_date As Date, ByVal waiting As Integer, ByVal allocated As Integer, ByVal alloc_stage_date As Date)
        'update/insert row
        Try
            Me.Allocation_statisticsTableAdapter.InsertQuery1(debtorid, Format(stage_date, "yyyy-MM-dd"), alloc_type, waiting, allocated, alloc_stage_date)
        Catch ex As Exception
            MsgBox(ex.Message)

        End Try
    End Sub

    Private Sub run_dtp_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles run_dtp.ValueChanged
        start_date = run_dtp.Value
        start_date = CDate(Format(start_date, "yyyy") & " " & Format(start_date, "MM") & " 1")
        end_date = DateAdd(DateInterval.Month, 1, start_date)
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.delbtn = New System.Windows.Forms.Button()
        Me.runbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.FeesSQLDataSet = New AllocationStatistics.FeesSQLDataSet()
        Me.Allocation_statisticsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Allocation_statisticsTableAdapter = New AllocationStatistics.FeesSQLDataSetTableAdapters.Allocation_statisticsTableAdapter()
        Me.TableAdapterManager = New AllocationStatistics.FeesSQLDataSetTableAdapters.TableAdapterManager()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.run_dtp = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Allocation_statisticsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'delbtn
        '
        Me.delbtn.Location = New System.Drawing.Point(86, 147)
        Me.delbtn.Name = "delbtn"
        Me.delbtn.Size = New System.Drawing.Size(121, 23)
        Me.delbtn.TabIndex = 0
        Me.delbtn.Text = "Delete rows for month"
        Me.delbtn.UseVisualStyleBackColor = True
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(86, 200)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(114, 23)
        Me.runbtn.TabIndex = 1
        Me.runbtn.Text = "run stats for month"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(236, 286)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Allocation_statisticsBindingSource
        '
        Me.Allocation_statisticsBindingSource.DataMember = "Allocation_statistics"
        Me.Allocation_statisticsBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'Allocation_statisticsTableAdapter
        '
        Me.Allocation_statisticsTableAdapter.ClearBeforeFill = True
        '
        'TableAdapterManager
        '
        Me.TableAdapterManager.Allocation_statisticsTableAdapter = Me.Allocation_statisticsTableAdapter
        Me.TableAdapterManager.BackupDataSetBeforeUpdate = False
        Me.TableAdapterManager.UpdateOrder = AllocationStatistics.FeesSQLDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(22, 274)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 3
        '
        'run_dtp
        '
        Me.run_dtp.Location = New System.Drawing.Point(86, 86)
        Me.run_dtp.Name = "run_dtp"
        Me.run_dtp.Size = New System.Drawing.Size(121, 20)
        Me.run_dtp.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(105, 70)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(78, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Run for Month:"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(335, 339)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.run_dtp)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.runbtn)
        Me.Controls.Add(Me.delbtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Allocation Statistics"
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Allocation_statisticsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents delbtn As System.Windows.Forms.Button
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents FeesSQLDataSet As AllocationStatistics.FeesSQLDataSet
    Friend WithEvents Allocation_statisticsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Allocation_statisticsTableAdapter As AllocationStatistics.FeesSQLDataSetTableAdapters.Allocation_statisticsTableAdapter
    Friend WithEvents TableAdapterManager As AllocationStatistics.FeesSQLDataSetTableAdapters.TableAdapterManager
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents run_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class

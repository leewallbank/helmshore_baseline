﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try

            Dim FileDialog As New OpenFileDialog
            Dim OutputFileLow As String = "" '"Name|Name2|Ref|Curr Add1|Curr Add2|Curr Add3|,Curr Pcode|Debt Addr1|Debt Addr2|" & _
            '"Debt Addr3|DebtAddr4|Debt Pcode|LO Date|LO Balance|Balance Referred|Mags Court|LO Order ID|Tel 1|" & _
            '"Tel 2|Tel 3|Email" & vbNewLine
            Dim OutputFileHigh As String = OutputFileLow
            Dim lowCases As Integer = 0
            Dim highCases As Integer = 0

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim excel_file_contents(,) As String = InputFromExcel(FileDialog.FileName)
            ProgressBar.Maximum = UBound(excel_file_contents)
            Dim rowMax As Integer = UBound(excel_file_contents)

            For rowIDX = 0 To rowMax
                If rowIDX = 0 Then
                    For colIDX = 0 To 22
                        OutputFileLow &= excel_file_contents(rowIDX, colIDX) & "|"
                        OutputFileHigh &= excel_file_contents(rowIDX, colIDX) & "|"
                    Next
                    OutputFileLow &= vbNewLine
                    OutputFileHigh &= vbNewLine
                Else

                    Dim balance As Decimal
                    Try
                        balance = excel_file_contents(rowIDX, 15)
                    Catch ex As Exception
                        Continue For
                    End Try

                    For colIDX = 0 To 22
                        If balance <= 160 Then
                            OutputFileLow &= excel_file_contents(rowIDX, colIDX) & "|"
                        Else
                            OutputFileHigh &= excel_file_contents(rowIDX, colIDX) & "|"
                        End If
                    Next
                    If balance <= 160 Then
                        OutputFileLow &= vbNewLine
                        lowCases += 1
                    Else
                        OutputFileHigh &= vbNewLine
                        highCases += 1
                    End If
                End If
            Next
            If highCases > 0 Then
                WriteFile(InputFilePath & FileName & "_HighBalance_4406.txt", OutputFileHigh)
                btnViewOutputFileHigh.Enabled = True
            End If
            If lowCases > 0 Then
                WriteFile(InputFilePath & FileName & "_LowBalance_4401.txt", OutputFileLow)
                btnViewOutputFileLow.Enabled = True
            End If
            MsgBox("High Balance Cases = " & highCases & vbNewLine & _
                   "Low  Balance Cases = " & lowCases)
            btnViewInputFile.Enabled = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFileHigh.Click
        Try

            If File.Exists(InputFilePath & FileName & "_HighBalance_4406.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_HighBalance_4406.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnViewOutputFileLow.Click
        Try

            If File.Exists(InputFilePath & FileName & "_LowBalance_4401.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_LowBalance_4401.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

Imports System.Collections
Public Class mainfrm

    Dim log_user As String
    Dim subject As String = ""
    Dim fromaddress As String
    Dim toaddress As String = ""
    Dim cc1 As String = ""
    Dim cc2 As String = ""
    Dim cc3 As String = ""
    Dim cc4 As String = ""
    Dim body As String
    Dim error_found As Boolean = False
    'Private RA1071report As RA1071
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        disable_buttons()
        Dim today_30 As Date = DateAdd(DateInterval.Day, -30, Now)
        Dim changes_found As Boolean = False
        param1 = "onestep"
        param2 = "select _rowid, remitBacsAccountNumber, remitBacsSortCode, remit_proportional, remit_prosplit, remitdirectstoclient from ClientScheme"
        Dim cs_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("No client schemes found")
            Exit Sub
        End If
        Dim idx As Integer
        Dim csid_rows As Integer = no_of_rows - 1
        Dim bank_acc, sort_code As String
        For idx = 0 To csid_rows
            ProgressBar1.Value = (idx / csid_rows) * 100
            Dim csid As Integer = cs_dataset.Tables(0).Rows(idx).Item(0)
            Try
                bank_acc = cs_dataset.Tables(0).Rows(idx).Item(1)
            Catch ex As Exception
                bank_acc = ""
            End Try
            Try
                sort_code = cs_dataset.Tables(0).Rows(idx).Item(2)
            Catch ex As Exception
                sort_code = ""
            End Try

            Dim proportional As String = ""
            Try
                proportional = cs_dataset.Tables(0).Rows(idx).Item(3)
            Catch ex As Exception

            End Try

            Dim prosplit As Decimal = 0
            Try
                prosplit = cs_dataset.Tables(0).Rows(idx).Item(4)
            Catch ex As Exception

            End Try

            Dim directstoclient As String = ""
            Try
                directstoclient = cs_dataset.Tables(0).Rows(idx).Item(5)
            Catch ex As Exception

            End Try

            'see if csid exists on bank account table
            Me.BankAccountsTableAdapter.FillBy(Me.FeesSQLDataSet.BankAccounts, csid)

            If Me.FeesSQLDataSet.BankAccounts.Rows.Count = 0 Then
                'insert row 
                Try
                    Me.BankAccountsTableAdapter.InsertQuery(csid, bank_acc, sort_code, "", "", today_30, proportional, prosplit, directstoclient)
                Catch ex As Exception
                    MsgBox("Unable to insert into bank account table for csid = " & csid)
                    Exit Sub
                End Try
            Else

                ''update new fields
                'Me.BankAccountsTableAdapter.UpdateQuery1(proportional, prosplit, directstoclient, csid)

                'Continue For
                'look for changes
                Dim onestep_prosplit As Decimal = 0
                Try
                    onestep_prosplit = Me.FeesSQLDataSet.BankAccounts.Rows(0).Item(7)
                Catch ex As Exception

                End Try
                Dim onestep_proportional As String = ""
                Try
                    onestep_proportional = Me.FeesSQLDataSet.BankAccounts.Rows(0).Item(6)
                Catch ex As Exception

                End Try
                Dim onestep_directstoclient As String = ""
                Try
                    onestep_directstoclient = Me.FeesSQLDataSet.BankAccounts.Rows(0).Item(8)
                Catch ex As Exception

                End Try
                If bank_acc <> Me.FeesSQLDataSet.BankAccounts.Rows(0).Item(1) Or _
                sort_code <> Me.FeesSQLDataSet.BankAccounts.Rows(0).Item(2) Or _
                proportional <> onestep_proportional Or _
                prosplit <> onestep_prosplit Or _
                directstoclient <> onestep_directstoclient Then
                    changes_found = True
                    Dim prev_bank_acc As String = Me.FeesSQLDataSet.BankAccounts.Rows(0).Item(1)
                    Dim prev_sort_code As String = Me.FeesSQLDataSet.BankAccounts.Rows(0).Item(2)
                    Dim prev_proportional As String = onestep_proportional
                    Dim prev_prosplit As Decimal = onestep_prosplit
                    Dim prev_directstoclient As String = onestep_directstoclient
                    Try
                        Me.BankAccountsTableAdapter.UpdateQuery(bank_acc, sort_code, prev_bank_acc, prev_sort_code, Now, proportional, prosplit, directstoclient, prev_proportional, prev_prosplit, prev_directstoclient, csid)
                    Catch ex As Exception
                        MsgBox("Unable to update bank account table for csid = " & csid)
                        Exit Sub
                    End Try

                End If
            End If
        Next
        fromaddress = "crystal@rossendales.com"

        If CheckedListBox1.CheckedItems.Count = 0 Then
            MsgBox("Warning - only going to yourself")
            toaddress = user_name & "@rossendales.com"
        End If
        Try
            toaddress = CheckedListBox1.CheckedItems.Item(0).ToString & "@rossendales.com"
        Catch ex As Exception

        End Try

        Try
            cc2 = CheckedListBox1.CheckedItems.Item(1).ToString & "@rossendales.com"
        Catch ex As Exception

        End Try
        Try
            cc3 = CheckedListBox1.CheckedItems.Item(2).ToString & "@rossendales.com"
        Catch ex As Exception

        End Try

        Try
            cc4 = CheckedListBox1.CheckedItems.Item(3).ToString & "@rossendales.com"
        Catch ex As Exception

        End Try

        cc1 = user_name & "@rossendales.com"
        subject = "RA1071 bank account changes"
        body = "Please find attached report of bank account changes"
        send_email()

        Me.Close()
    End Sub
    Sub send_email()
        'run crystal report for client
        Try
            Dim RA1071Report = New RA1071
            Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
            myConnectionInfo.ServerName = "DebtRecovery"
            myConnectionInfo.DatabaseName = "DebtRecovery"
            myConnectionInfo.UserID = "crystal"
            myConnectionInfo.Password = "latsyrc"
            SetDBLogonForReport(myConnectionInfo, RA1071Report)
            Dim myConnectionInfo2 As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
            myConnectionInfo2.ServerName = "RossFeeTable"
            myConnectionInfo2.DatabaseName = "FeesSQL"
            myConnectionInfo2.UserID = "sa"
            myConnectionInfo2.Password = "sa"
            SetDBLogonForReport2(myConnectionInfo2, RA1071Report)
            file_name = "H:\RA1071.pdf"
            Try
                RA1071Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, file_name)
            Catch ex As Exception
                MsgBox(ex.Message)
                Exit Sub
            End Try
            RA1071Report.Close()
            subject = "Bank Account changes"
            If email(toaddress, fromaddress, subject, body, file_name, cc1, cc2, cc3, cc4) = 0 Then
                MsgBox("Email sent")
                Me.Close()
            Else
                MsgBox("See error log at H:\Ra1071\error_log")
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
    End Sub
    Sub disable_buttons()
        exitbtn.Enabled = False
        runbtn.Enabled = False
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        CheckedListBox1.SetItemChecked(0, True)
        CheckedListBox1.SetItemChecked(1, True)
        CheckedListBox1.SetItemChecked(2, True)

        user_name = My.User.Name
        Dim idx As Integer = InStr(user_name, "\")
        user_name = Microsoft.VisualBasic.Right(user_name, user_name.Length - idx)
        Label2.Text = " and   " & user_name
        error_path = "H:\RA1071 \"
        error_file = error_path & "error_log_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"
        log_path = "H:\RA1071 \"
        log_file = error_path & "log_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"
    End Sub

   
End Class

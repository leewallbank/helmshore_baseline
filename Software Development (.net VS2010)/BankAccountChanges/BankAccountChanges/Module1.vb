Module Module1
    Public file_name, user_name, log_message, error_message, error_path, log_path, log_file, error_file As String
    Public Sub SetDBLogonForReport(ByVal myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo, ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            If myTable.Name <> "BankAccounts" Then
                Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTable.ApplyLogOnInfo(myTableLogonInfo)
            End If
        Next
    End Sub
    Public Sub SetDBLogonForReport2(ByVal myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo, ByVal myReportDocument As CrystalDecisions.CrystalReports.Engine.ReportDocument)
        Dim myTables As CrystalDecisions.CrystalReports.Engine.Tables = myReportDocument.Database.Tables
        For Each myTable As CrystalDecisions.CrystalReports.Engine.Table In myTables
            If myTable.Name = "BankAccounts" Then
                Dim myTableLogonInfo As CrystalDecisions.Shared.TableLogOnInfo = myTable.LogOnInfo
                myTableLogonInfo.ConnectionInfo = myConnectionInfo
                myTableLogonInfo.TableName = myTable.Name
                myTable.ApplyLogOnInfo(myTableLogonInfo)
            End If
        Next
    End Sub
    
    Sub write_error()
        Dim dir_info As IO.DirectoryInfo
        Try
            If IO.Directory.Exists(error_path) = False Then
                dir_info = IO.Directory.CreateDirectory(error_path)
            End If
            'save document
        Catch ex As Exception
            MsgBox("Unable to save error file")
        End Try
        My.Computer.FileSystem.WriteAllText(error_file, error_message, True)

    End Sub
    Sub write_log()
        Dim dir_info As IO.DirectoryInfo
        Try
            If IO.Directory.Exists(log_path) = False Then
                dir_info = IO.Directory.CreateDirectory(log_path)
            End If
            'save document
        Catch ex As Exception
            MsgBox("Unable to save log file")
        End Try
        My.Computer.FileSystem.WriteAllText(log_file, log_message, True)

    End Sub
End Module

﻿Imports System.Drawing.Printing
Imports System.IO
Imports CommonLibrary
Public Class Form1
    Private InputFilePath As String, FileName As String, FileExt As String
    'Dim objUDC As UDC.IUDC
    'Dim itfPrinter As UDC.IUDCPrinter
    Dim defaultprinter As String
    'Dim itfProfile As UDC.IProfile
    Dim debtorID As Integer
    Dim notesfile As String = ""
    Dim ProfilePath As String
    Dim adCmdText As ADODB.CommandTypeEnum

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub convertbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles convertbtn.Click
        Dim file_name As String
        Dim FileDialog As New OpenFileDialog
        Dim auditFile As String = ""
        Dim user As String = My.User.Name

        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        convertbtn.Enabled = False
        auditFile = Now & vbTab & user & vbTab & "Starting processing" & vbNewLine
        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        Dim new_filename As String = ""
        FileExt = Path.GetExtension(FileDialog.FileName)
        'create directories for log and pdfs/TIFFs
        Dim dir_name As String = InputFilePath & "LogFile"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create LogFile folder")
            Exit Sub
            End
        End Try
        dir_name = InputFilePath & "NotesFile"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create Notesfile folder")
            Exit Sub
            End
        End Try
        dir_name = InputFilePath & "ReRefPDF"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create ReRefFile folder")
            Exit Sub
            End
        End Try
        dir_name = InputFilePath & "TIFF"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create TIFF folder")
            Exit Sub
            End
        End Try
        'read results file and create note file
        'copy pdf files to ReRefPDF folder and rename to debtorID
        Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
        ProgressBar1.Maximum = UBound(FileContents)
        For lineIDX = 1 To UBound(FileContents)
            Try
                ProgressBar1.Value = lineIDX
            Catch ex As Exception

            End Try
            Application.DoEvents()
            Dim lineArray() As String = FileContents(lineIDX).Split(vbTab)
            Dim title As String = lineArray(0)
            debtorID = lineArray(1)
            Dim tenure As String = lineArray(2)
            Dim proprietor As String = lineArray(3)
            'remove quotes and commas 
            proprietor = Replace(proprietor, ",", " ")
            proprietor = Replace(proprietor, Chr(34), "")
            Dim address As String = lineArray(4)
            address = Replace(address, ",", " ")
            address = Replace(address, Chr(34), "")
            'write out notes file
            If title.Length > 0 Then
                notesfile &= debtorID & "," & "TitleNo:" & title & vbNewLine
            End If
            If tenure.Length > 0 Then
                notesfile &= debtorID & "," & "Tenure:" & tenure & vbNewLine
            End If
            If proprietor.Length > 0 Then
                notesfile &= debtorID & "," & "Proprietor:" & proprietor & vbNewLine
            End If
            If address.Length > 0 Then
                notesfile &= debtorID & "," & "Charge:" & address & vbNewLine
            End If

            For Each foundFile As String In My.Computer.FileSystem.GetFiles _
            (InputFilePath, FileIO.SearchOption.SearchTopLevelOnly, title & ".pdf")
                'copy file and rename
                Dim foundFileName As String = Path.GetFileNameWithoutExtension(foundFile)
                Dim newFilename As String = InputFilePath & "ReRefPDF\" & debtorID & "-doc" & foundFileName & ".pdf"
                My.Computer.FileSystem.CopyFile(foundFile, newFilename, True)
            Next
        Next
        auditFile &= Now & vbTab & user & vbTab & "Note file created" & vbNewLine
        

        'write notes file
        WriteFile(InputFilePath & "NotesFile/NoteFile.txt", notesfile)
        Dim PDFConvert As Object
        PDFConvert = CreateObject("PDFConverter.PDFConverterX")
        Dim no_of_files As Integer = 0
        Dim last_file_name As String = ""
        Dim pbar As Integer = 0
        Try

            For Each foundFile As String In My.Computer.FileSystem.GetFiles _
         (InputFilePath & "ReRefPDF", FileIO.SearchOption.SearchTopLevelOnly, "*.pdf")
                pbar += 1
                Try
                    ProgressBar1.Value = pbar
                Catch ex As Exception
                    pbar = 0
                End Try
                Application.DoEvents()
                'convert pdf to tiff
                'get debtor from file name
                Dim foundFileName As String = Path.GetFileNameWithoutExtension(foundFile)
                Dim dashIDX As Integer
                For dashIDX = foundFileName.Length To 1 Step -1
                    If Mid(foundFileName, dashIDX, 1) = "-" Then
                        Exit For
                    End If
                Next
                Try
                    debtorID = Microsoft.VisualBasic.Left(foundFileName, dashIDX - 1)
                Catch ex As Exception
                    MsgBox("Unable to get debtorID for file " & foundFile)
                    debtorID = 0
                End Try
               
                no_of_files += 1

                new_filename = InputFilePath & "TIFF\" & foundFileName & ".tif"
                PDFConvert.Convert(foundFile, new_filename, "-c TIF -s -t'[Name].page#.tiff' -tc LZW")
                last_file_name = foundFile
                auditFile &= Now & vbTab & user & vbTab & "converting to tiff " & vbTab & foundFile & new_filename & vbNewLine
            Next
        Catch ex As Exception

        End Try

        'wait until last tiff file has been created
        'when last tiff file is there reset default printer
        If no_of_files = 0 Then
            MsgBox("No files found")
        Else
            Dim last_tif_found As Boolean = False
            MsgBox("Waiting for tiff background tasks to catch up")
            Try
                While last_tif_found = False
                    Try
                        My.Computer.FileSystem.ReadAllText(last_file_name)
                        last_tif_found = True
                    Catch ex As Exception
                        System.Threading.Thread.Sleep(10000)
                    End Try
                End While
            Catch ex As Exception

            End Try
        End If

        'now read all tif files and check page number is in name
        If no_of_files > 0 Then
            '    file_name = InputFilePath & "ReRefPDF\"
            '    Try
            '        For Each foundFile As String In My.Computer.FileSystem.GetFiles _
            '     (file_name, FileIO.SearchOption.SearchTopLevelOnly, "*.tif")
            '            pbar += 1
            '            Try
            '                ProgressBar1.Value = pbar
            '            Catch ex As Exception
            '                pbar = 0
            '            End Try
            '            Application.DoEvents()
            '            Dim tif_image As Image
            '            tif_image = Image.FromFile(foundFile)
            '            auditFile &= Now & vbTab & user & vbTab & "renaming" & vbTab & foundFile & vbNewLine
            '            Dim page_no As Integer = tif_image.GetFrameCount(Imaging.FrameDimension.Page)
            '            For idx = 0 To page_no - 1
            '                'Dim tif_page As Image
            '                tif_image.SelectActiveFrame(Imaging.FrameDimension.Page, idx)
            '                Dim tif_page_file_name As String = Replace(foundFile, ".tif", "-doc-Page00" & idx + 1 & ".tif")
            '                'tif_page_file_name = Replace(tif_page_file_name, "ReRefPDF", "TIF")
            '                tif_image.Save(tif_page_file_name, Imaging.ImageFormat.Tiff)
            '            Next
            '        Next
            '    Catch ex As Exception
            '        MsgBox(ex.Message)
            '    End Try
            '    'copy tif files to TIFF folder
            file_name = InputFilePath & "TIFF\"
            Try
                For Each foundFile As String In My.Computer.FileSystem.GetFiles _
             (file_name, FileIO.SearchOption.SearchTopLevelOnly, "*.tif")
                    Dim newFoundFile As String = Path.GetFileName(foundFile)
                    If InStr(foundFile, "page") = 0 Then
                        newFoundFile = Replace(newFoundFile, ".tif", ".001.tif")
                        My.Computer.FileSystem.RenameFile(foundFile, newFoundFile)
                    Else
                        newFoundFile = Replace(newFoundFile, "page", "00")
                        My.Computer.FileSystem.RenameFile(foundFile, newFoundFile)
                    End If

                Next

            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
        auditFile &= Now & vbTab & user & vbTab & "Completed" & vbNewLine
        WriteFile(InputFilePath & "LogFile/AuditFile.txt", auditFile)
        MsgBox("Completed")
        Me.Close()
    End Sub

End Class

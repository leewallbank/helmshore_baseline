﻿Imports CommonLibrary
Imports System.IO
Imports System.Collections

Public Class frmMain
    Private UnitedUtilitiesData As New clsUnitedUtilitiesData
    Private QueryHistoryHeader As String() = {"DCA_CODE", "Record_Type", "Account_Number", "Dummy_Balance", "Filler", "Record_Title", "Query_sequence_number", "Type", "Query/Response"}

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim FileContents As String(,), Notes As String()
        Dim FileValid As Boolean = True

        Dim OutputFile As String = ""

        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        FileContents = InputFromSeparatedFile(FileDialog.FileName, "|")

        If UBound(FileContents, 2) <> UBound(QueryHistoryHeader) Then FileValid = False

        For ColCount As Integer = 0 To UBound(FileContents, 2)
            If QueryHistoryHeader(ColCount) <> FileContents(0, ColCount) Then FileValid = False
        Next ColCount

        If Not FileValid Then
            MsgBox("Incorrect file layout.", vbCritical + vbOKOnly, Me.Text)
            Return
        End If

        ProgressBar.Maximum = UBound(FileContents, 1)

        For RowCount As Integer = 1 To UBound(FileContents, 1)
            ProgressBar.Value = RowCount
            Notes = ToNote(FileContents(RowCount, 8), FileContents(RowCount, 7), "")

            For NoteCount As Integer = 0 To UBound(Notes)
                OutputFile &= UnitedUtilitiesData.GetCaseID(FileContents(RowCount, 2)) & "|" & Notes(NoteCount) & vbCrLf
            Next NoteCount

        Next RowCount

        WriteFile(Path.GetDirectoryName(FileDialog.FileName) & "\QueryHistoryNotes" & DateTime.Today.ToString("yyyyMMdd") & ".txt", OutputFile)

        MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

        ProgressBar.Value = 0

    End Sub


End Class

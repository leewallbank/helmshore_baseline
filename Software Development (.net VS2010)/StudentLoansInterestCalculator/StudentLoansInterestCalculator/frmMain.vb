﻿Imports System.IO

Public Class frmMain
    Private InterestData As New clsInterestData

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        dtpCalcMonth.Value = dtpCalcMonth.Value.AddDays(-dtpCalcMonth.Value.Day + 1).AddMonths(-1)
    End Sub

    Private Sub cmdCalculate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCalculate.Click
        Dim FolderBrowserDialog As New FolderBrowserDialog

        FolderBrowserDialog.Description = "Select where the output spreadsheet will be saved."
        FolderBrowserDialog.ShowNewFolderButton = True
        FolderBrowserDialog.SelectedPath = "R:\"

        If FolderBrowserDialog.ShowDialog() <> Windows.Forms.DialogResult.OK Then Return

        Dim LoopCount As Integer, DebtorCount As Integer, CCIDebtorCount = 0, DaysInYear As Integer
        Dim DailyRate As Decimal, MonthlyInterest As Decimal, CCIMonthlyInterest As Decimal = 0
        Dim ExcelApp As Object, Workbook As Object, SummaryWorksheet As Object, BalanceWorksheet As Object
        Dim DailyBalance(DateTime.DaysInMonth(dtpCalcMonth.Value.Year, dtpCalcMonth.Value.Month) - 1) As Decimal
        Dim WorkbookPath As String = "", NotesFile As String = "", FeesFile As String = ""
        Dim CCICasesExist As Boolean = False, CatchException As Boolean = False
        Dim CCIFileContents As String = ""

        ExcelApp = CreateObject("Excel.Application")
        Workbook = ExcelApp.Workbooks.Add()
        Try
            ' MOD TS 03/Nov/2015. Request ref 65239
            'For Each Sheet As Object In Workbook.Worksheets
            '    If Sheet.Index > 2 Then Sheet.Delete()
            'Next Sheet

            Workbook.Worksheets.Add()

            ' End of request ref 65239

            SummaryWorksheet = Workbook.Sheets(1)
            SummaryWorksheet.Name = "Interest to apply"
            SummaryWorksheet.Cells(1, 1) = "ClientSchemeID"
            SummaryWorksheet.Cells(1, 2) = "DebtorID"
            SummaryWorksheet.Cells(1, 3) = "Interest"
            SummaryWorksheet.Range(SummaryWorksheet.Cells(1, 1), SummaryWorksheet.Cells(1, 3)).Font.Bold = True

            BalanceWorksheet = Workbook.Sheets(2)
            BalanceWorksheet.Name = "Balance Breakdown"
            BalanceWorksheet.Cells(1, 1) = "ClientSchemeID"
            BalanceWorksheet.Cells(1, 2) = "DebtorID"
            BalanceWorksheet.Cells(1, 3) = "Annual int"
            BalanceWorksheet.Cells(1, 4) = "Daily int"
            BalanceWorksheet.Range(BalanceWorksheet.Cells(1, 1), BalanceWorksheet.Cells(1, UBound(DailyBalance) + 5)).Font.Bold = True

            For LoopCount = 1 To UBound(DailyBalance) + 1
                Select Case LoopCount
                    Case 1
                        BalanceWorksheet.Cells(1, LoopCount + 4) = LoopCount.ToString + "st"
                    Case 2
                        BalanceWorksheet.Cells(1, LoopCount + 4) = LoopCount.ToString + "nd"
                    Case 3
                        BalanceWorksheet.Cells(1, LoopCount + 4) = LoopCount.ToString + "rd"
                    Case 21
                        BalanceWorksheet.Cells(1, LoopCount + 4) = LoopCount.ToString + "st"
                    Case 22
                        BalanceWorksheet.Cells(1, LoopCount + 4) = LoopCount.ToString + "nd"
                    Case 23
                        BalanceWorksheet.Cells(1, LoopCount + 4) = LoopCount.ToString + "rd"
                    Case 31
                        BalanceWorksheet.Cells(1, LoopCount + 4) = LoopCount.ToString + "st"
                    Case Else
                        BalanceWorksheet.Cells(1, LoopCount + 4) = LoopCount.ToString + "th"
                End Select
            Next LoopCount

            Me.Cursor = Cursors.WaitCursor

            txtOutput.Text = ""

            ' Calculate the number of days in the year. This is not the calendar year but the year the interest rate applies to
            If dtpCalcMonth.Value.Month < 9 Then
                DaysInYear = DateDiff(DateInterval.Day, CDate("1/Sep/" & dtpCalcMonth.Value.Year - 1), CDate("31/Aug/" & dtpCalcMonth.Value.Year)) + 1
            Else
                DaysInYear = DateDiff(DateInterval.Day, CDate("1/Sep/" & dtpCalcMonth.Value.Year), CDate("31/Aug/" & dtpCalcMonth.Value.Year + 1)) + 1
            End If

            ' Get data from DebtRecovery
            InterestData.GetDebtors(dtpCalcMonth.Value)
            InterestData.GetPayments(dtpCalcMonth.Value)
            InterestData.GetFees(dtpCalcMonth.Value)

            ' Check for CCI cases
            InterestData.DebtorDataView.RowFilter = "ClientSchemeID IN (3167, 3178)"
            If InterestData.DebtorDataView.Count > 0 Then
                CCIFileContents = "H|Rossendales|StudentLoansInterestCalculator_v1.0|" & DateTime.Today.ToString("dd/MM/yyyy") & vbCrLf
                CCICasesExist = True
            End If

            ' Loop through Debtors
            For Each Debtor As DataRow In InterestData.DebtorDataView.Table.Rows
                DebtorCount += 1

                ' Calculate the daily rate. The SLC spec requires rounding to 9 places at each point in the calculation
                DailyRate = Math.Round(Math.Pow(1 + (Debtor("Interest") / 100), Math.Round(1 / DaysInYear, 9)) - 1, 9)

                ' Populate the daily balance outstanding with the balance brought forward
                For LoopCount = 0 To UBound(DailyBalance)
                    DailyBalance(LoopCount) = Debtor("Balance")
                Next LoopCount

                ' Now reduce the balance by any payments
                InterestData.PaymentDataView.RowFilter = "DebtorID = " & Debtor("DebtorID")

                ' Loop through payments
                For Each Payment As DataRow In InterestData.PaymentDataView.ToTable.Rows
                    ' Apply payments to all dates in the month from the payment date onward
                    For LoopCount = CDate(Payment("Date")).Day - 1 To UBound(DailyBalance)
                        DailyBalance(LoopCount) -= Payment("Amount")
                    Next LoopCount
                Next Payment

                ' Now increase the balance by any fees. This is needed as payments include card fees which are not subject to interest
                InterestData.FeeDataView.RowFilter = "DebtorID = " & Debtor("DebtorID")

                ' Loop through fees
                For Each Fee As DataRow In InterestData.FeeDataView.ToTable.Rows
                    ' Apply fee to all dates in the month from the fee date onward
                    For LoopCount = CDate(Fee("Date")).Day - 1 To UBound(DailyBalance)
                        DailyBalance(LoopCount) += Fee("Amount")
                    Next LoopCount
                Next Fee

                ' Add up all the interest due
                MonthlyInterest = 0
                For LoopCount = 0 To UBound(DailyBalance)
                    BalanceWorksheet.Cells(DebtorCount + 1, LoopCount + 5) = DailyBalance(LoopCount)
                    MonthlyInterest += DailyBalance(LoopCount) * DailyRate
                Next LoopCount

                NotesFile &= Debtor("DebtorID").ToString & "," & "Monthly Interest: " & Math.Round(MonthlyInterest, 2).ToString("F2") & ";" & vbCrLf
                NotesFile &= Debtor("DebtorID").ToString & "," & "Total Balance: " & Math.Round(MonthlyInterest + DailyBalance(UBound(DailyBalance)), 2).ToString("F2") & ";" & vbCrLf

                If Not {3161, 3162, 3163, 3164, 3165, 3166}.Contains(Debtor("ClientSchemeID")) Then FeesFile &= Debtor("DebtorID").ToString & "," & Math.Round(MonthlyInterest, 2).ToString("F2") & vbCrLf

                txtOutput.AppendText(Debtor("DebtorID") & vbTab & " Balance brought foward £" & Debtor("Balance") & vbTab & " Interest £" & Math.Round(MonthlyInterest, 2) & vbCrLf)
                SummaryWorksheet.Cells(DebtorCount + 1, 1) = Debtor("ClientSchemeID")
                SummaryWorksheet.Cells(DebtorCount + 1, 2) = Debtor("DebtorID")
                SummaryWorksheet.Cells(DebtorCount + 1, 3) = Math.Round(MonthlyInterest, 2)
                SummaryWorksheet.Cells(DebtorCount + 1, 3).numberformat = "£#,###.00"
                BalanceWorksheet.Cells(DebtorCount + 1, 1) = Debtor("ClientSchemeID")
                BalanceWorksheet.Cells(DebtorCount + 1, 2) = Debtor("DebtorID")
                BalanceWorksheet.Cells(DebtorCount + 1, 3) = Debtor("Interest")
                BalanceWorksheet.Cells(DebtorCount + 1, 4) = DailyRate

                If {3167, 3178}.Contains(Debtor("ClientSchemeID")) Then
                    CCIFileContents &= "D|" & Debtor("DebtorID") & "|" & dtpCalcMonth.Value.AddMonths(1).ToString("dd/MM/yyyy") & "|" & Math.Round(MonthlyInterest, 2).ToString & vbCrLf
                    CCIDebtorCount += 1
                    CCIMonthlyInterest += Math.Round(MonthlyInterest, 2)
                End If

                Windows.Forms.Application.DoEvents()

            Next Debtor

            If CCICasesExist Then
                CCIFileContents &= "T|" & CCIDebtorCount.ToString & "|" & CCIMonthlyInterest.ToString
                Using Writer As StreamWriter = New StreamWriter(FolderBrowserDialog.SelectedPath & "\RossendalesMonthlyInterest_" & dtpCalcMonth.Value.ToString("MMMM_yyyy") & ".txt")
                    Writer.Write(CCIFileContents)
                End Using
            End If

            ExcelApp.DisplayAlerts = False

            WorkbookPath = FolderBrowserDialog.SelectedPath & "\SLC_Interest_" & DateTime.Today.ToString("yyyyMMdd") & ".xls"
            Workbook.SaveAs(WorkbookPath, 39) ' 39  Is Excel version 8. A bit old but more likely to be compatible

            If NotesFile <> "" Then
                WriteFile(FolderBrowserDialog.SelectedPath & "\SLC_NotesFile.csv", NotesFile)
                System.Diagnostics.Process.Start(FolderBrowserDialog.SelectedPath & "\SLC_NotesFile.csv")
            End If

            If FeesFile <> "" Then
                WriteFile(FolderBrowserDialog.SelectedPath & "\SLC_FeesFile.csv", FeesFile)
                System.Diagnostics.Process.Start(FolderBrowserDialog.SelectedPath & "\SLC_FeesFile.csv")
            End If

        Catch ex As Exception
            CatchException = True
            MsgBox(ex.ToString)

        Finally
            CloseComObject(ExcelApp.ActiveWorkbook)
            ExcelApp.ActiveWorkbook.Close()
            ExcelApp.Quit()
            CloseComObject(Workbook)
            Workbook = Nothing
            CloseComObject(ExcelApp)
            ExcelApp = Nothing

            Me.Cursor = Cursors.Default

            If Not CatchException Then
                If File.Exists(WorkbookPath) Then System.Diagnostics.Process.Start(WorkbookPath)
                If NotesFile <> "" Then System.Diagnostics.Process.Start(FolderBrowserDialog.SelectedPath & "\SLC_NotesFile.csv")
                If FeesFile <> "" Then System.Diagnostics.Process.Start(FolderBrowserDialog.SelectedPath & "\SLC_FeesFile.csv")
            End If
        End Try
    End Sub

    Private Sub dtpCalcMonth_CloseUp(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpCalcMonth.CloseUp
        dtpCalcMonth.Value = dtpCalcMonth.Value.AddDays(-dtpCalcMonth.Value.Day + 1)
    End Sub

    Private Sub CloseComObject(ByVal o As Object)
        Try
            While (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0)
            End While
        Catch
        Finally
            o = Nothing
        End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.cmdCalculate = New System.Windows.Forms.Button()
        Me.lblCalcMonth = New System.Windows.Forms.Label()
        Me.dtpCalcMonth = New System.Windows.Forms.DateTimePicker()
        Me.txtOutput = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'cmdCalculate
        '
        Me.cmdCalculate.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.cmdCalculate.Location = New System.Drawing.Point(495, 329)
        Me.cmdCalculate.Name = "cmdCalculate"
        Me.cmdCalculate.Size = New System.Drawing.Size(101, 30)
        Me.cmdCalculate.TabIndex = 8
        Me.cmdCalculate.Text = "Calculate"
        Me.cmdCalculate.UseVisualStyleBackColor = True
        '
        'lblCalcMonth
        '
        Me.lblCalcMonth.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.lblCalcMonth.AutoSize = True
        Me.lblCalcMonth.Location = New System.Drawing.Point(188, 338)
        Me.lblCalcMonth.Name = "lblCalcMonth"
        Me.lblCalcMonth.Size = New System.Drawing.Size(98, 13)
        Me.lblCalcMonth.TabIndex = 9
        Me.lblCalcMonth.Text = "Month to calculate:"
        '
        'dtpCalcMonth
        '
        Me.dtpCalcMonth.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.dtpCalcMonth.CustomFormat = "MMMM yyyy"
        Me.dtpCalcMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpCalcMonth.Location = New System.Drawing.Point(302, 334)
        Me.dtpCalcMonth.Name = "dtpCalcMonth"
        Me.dtpCalcMonth.Size = New System.Drawing.Size(126, 20)
        Me.dtpCalcMonth.TabIndex = 7
        '
        'txtOutput
        '
        Me.txtOutput.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOutput.HideSelection = False
        Me.txtOutput.Location = New System.Drawing.Point(50, 35)
        Me.txtOutput.Multiline = True
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtOutput.Size = New System.Drawing.Size(789, 277)
        Me.txtOutput.TabIndex = 10
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(900, 381)
        Me.Controls.Add(Me.txtOutput)
        Me.Controls.Add(Me.cmdCalculate)
        Me.Controls.Add(Me.lblCalcMonth)
        Me.Controls.Add(Me.dtpCalcMonth)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Student Loans Interest Calculator"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdCalculate As System.Windows.Forms.Button
    Friend WithEvents lblCalcMonth As System.Windows.Forms.Label
    Friend WithEvents dtpCalcMonth As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtOutput As System.Windows.Forms.TextBox

End Class

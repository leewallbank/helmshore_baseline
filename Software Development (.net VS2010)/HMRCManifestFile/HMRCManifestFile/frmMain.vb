﻿Imports CommonLibrary
Imports System.IO



Public Class frmMain

    Private Const Separator As String = "|"
    Private Const DCAID As String = "7"
    Private Const SourceIndicator As String = "D"
    Private Const LineTerminator As String = vbLf

    Private Sub btnGenerateManifestFile_Click(sender As Object, e As System.EventArgs) Handles btnGenerateManifestFile.Click
        Try
            Dim ManifestLine As String = ""
            Dim FolderDialog As New FolderBrowserDialog
            FolderDialog.Description = "Select folder"
            FolderDialog.ShowNewFolderButton = False
            If Not FolderDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then Return

            Dim ManifestFileName As String = FolderDialog.SelectedPath & "\DCA.MANIFEST.D." & DCAID.PadLeft(5, "0") & "." & Date.Now.ToString("yyyyMMdd") & ".dat"

            If File.Exists(ManifestFileName) Then File.Delete(ManifestFileName)

            For Each File As IO.FileInfo In New IO.DirectoryInfo(FolderDialog.SelectedPath).GetFiles("DCA.*.D." & DCAID.PadLeft(5, "0") & ".*.dat")

                Dim lines As New List(Of String)
                Using sr As New StreamReader(File.FullName)
                    While Not sr.EndOfStream
                        lines.Add(sr.ReadLine.Replace(vbCrLf, vbLf))
                    End While
                End Using

                Using sw As New StreamWriter(File.FullName)
                    For Each line As String In lines
                        sw.Write(line & vbLf)
                    Next
                End Using


                ManifestLine = String.Join(Separator, {File.Name _
                                                      , IO.File.ReadAllBytes(File.FullName).Length.ToString _
                                                      , IO.File.ReadAllLines(File.FullName).Length.ToString _
                                                      , DCAID _
                                                      , "D" _
                                                      , "" _
                                                      , "" _
                                                      , Date.Now.ToString("yyyyMMdd")
                                                      } _
                                          ) & LineTerminator

                AppendToFile(ManifestFileName, ManifestLine)

            Next File

            Dim OutputFilename As String = IO.Directory.GetParent(FolderDialog.SelectedPath).FullName & "\DCA.FILE_SET.D." & DCAID.PadLeft(5, "0") & "." & DateTime.Today.ToString("yyyyMMdd") & ".zip"

            CreateZipFile(OutputFilename, FolderDialog.SelectedPath & "\")

            MsgBox("Manifest file created", MsgBoxStyle.OkOnly + MsgBoxStyle.Information, Me.Text)

            Process.Start("explorer.exe", "/select," & OutputFilename)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.btnGenerateManifestFile = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnGenerateManifestFile
        '
        Me.btnGenerateManifestFile.Location = New System.Drawing.Point(70, 76)
        Me.btnGenerateManifestFile.Name = "btnGenerateManifestFile"
        Me.btnGenerateManifestFile.Size = New System.Drawing.Size(153, 42)
        Me.btnGenerateManifestFile.TabIndex = 0
        Me.btnGenerateManifestFile.Text = "Generate Manifest"
        Me.btnGenerateManifestFile.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.btnGenerateManifestFile)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "HMRC Manifest file"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnGenerateManifestFile As System.Windows.Forms.Button

End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.runbtn = New System.Windows.Forms.Button
        Me.rundtp = New System.Windows.Forms.DateTimePicker
        Me.exitbtn = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.allrbtn = New System.Windows.Forms.RadioButton
        Me.daterbtn = New System.Windows.Forms.RadioButton
        Me.bail_cbox = New System.Windows.Forms.ComboBox
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.SuspendLayout()
        '
        'runbtn
        '
        Me.runbtn.Location = New System.Drawing.Point(88, 212)
        Me.runbtn.Name = "runbtn"
        Me.runbtn.Size = New System.Drawing.Size(75, 23)
        Me.runbtn.TabIndex = 0
        Me.runbtn.Text = "Run report"
        Me.runbtn.UseVisualStyleBackColor = True
        '
        'rundtp
        '
        Me.rundtp.Location = New System.Drawing.Point(156, 137)
        Me.rundtp.Name = "rundtp"
        Me.rundtp.Size = New System.Drawing.Size(125, 20)
        Me.rundtp.TabIndex = 2
        Me.rundtp.Visible = False
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(182, 261)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 3
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(98, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Select Bailiff"
        '
        'allrbtn
        '
        Me.allrbtn.AutoSize = True
        Me.allrbtn.Checked = True
        Me.allrbtn.Location = New System.Drawing.Point(47, 114)
        Me.allrbtn.Name = "allrbtn"
        Me.allrbtn.Size = New System.Drawing.Size(67, 17)
        Me.allrbtn.TabIndex = 6
        Me.allrbtn.TabStop = True
        Me.allrbtn.Text = "All Dates"
        Me.allrbtn.UseVisualStyleBackColor = True
        '
        'daterbtn
        '
        Me.daterbtn.AutoSize = True
        Me.daterbtn.Location = New System.Drawing.Point(47, 137)
        Me.daterbtn.Name = "daterbtn"
        Me.daterbtn.Size = New System.Drawing.Size(81, 17)
        Me.daterbtn.TabIndex = 7
        Me.daterbtn.Text = "Select Date"
        Me.daterbtn.UseVisualStyleBackColor = True
        '
        'bail_cbox
        '
        Me.bail_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.bail_cbox.FormattingEnabled = True
        Me.bail_cbox.Location = New System.Drawing.Point(63, 55)
        Me.bail_cbox.Name = "bail_cbox"
        Me.bail_cbox.Size = New System.Drawing.Size(156, 21)
        Me.bail_cbox.TabIndex = 1
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(293, 323)
        Me.Controls.Add(Me.daterbtn)
        Me.Controls.Add(Me.allrbtn)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.rundtp)
        Me.Controls.Add(Me.bail_cbox)
        Me.Controls.Add(Me.runbtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Allocations Summary report"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents runbtn As System.Windows.Forms.Button
    Friend WithEvents rundtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents allrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents daterbtn As System.Windows.Forms.RadioButton
    Friend WithEvents bail_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog

End Class

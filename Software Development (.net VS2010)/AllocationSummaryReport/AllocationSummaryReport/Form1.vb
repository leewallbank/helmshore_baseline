Public Class Form1
    Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
    Dim filename As String = ""

    Dim bailiffID As Integer
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub daterbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles daterbtn.CheckedChanged
        rundtp.Visible = daterbtn.Checked
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        param2 = "select _rowid, name_sur, name_fore from Bailiff " & _
        " where status = 'O' and agent_type = 'B'" & _
        " and add_mobile_name <> 'none' " & _
        " order by name_sur, name_fore"
        Dim bail_ds As DataSet = get_dataset("onestep", param2)
        Dim row As DataRow
        Dim bailiff As String = ""
        For Each row In bail_ds.Tables(0).Rows

            Try
                bailiff = Trim(row(1)) & ", "
            Catch ex As Exception

            End Try
            Try
                bailiff &= Trim(row(2)) & " "
            Catch ex As Exception

            End Try
            bailiff &= ":" & row(0)
            bail_cbox.Items.Add(bailiff)
        Next
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        Dim colon As Integer = InStr(bail_cbox.Text, ":")

        Try
            bailiffID = Microsoft.VisualBasic.Right(bail_cbox.Text, bail_cbox.Text.Length - colon)
        Catch ex As Exception
            MsgBox("Unable to find bailiff number in " & bail_cbox.Text)
        End Try

        exitbtn.Enabled = False
        runbtn.Enabled = False

        If allrbtn.Checked Then
            run_RA2106A()
        Else
            run_RA2106()
        End If
    End Sub
    Private Sub run_RA2106()


        Dim RA2106report = New RA2106
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(bailiffID)
        SetCurrentValuesForParameterField1(RA2106report, myArrayList1)
        myArrayList1.Add(rundtp.Value)
        SetCurrentValuesForParameterField2(RA2106report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2106report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RA2106 Bailiff Allocation Summary.pdf"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = SaveFileDialog1.FileName
        Else
            MsgBox("Report not saved")
            Me.Close()
            Exit Sub
        End If


        Application.DoEvents()
        RA2106report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        MsgBox("Report saved")
        RA2106report.close()
        Me.Close()
    End Sub
    Private Sub run_RA2106A()
        Dim RA2106report = New RA2106A
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(bailiffID)
        SetCurrentValuesForParameterField1(RA2106report, myArrayList1)
       
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2106report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RA2106 Bailiff Allocation Summary.pdf"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            filename = SaveFileDialog1.FileName
        Else
            MsgBox("Report not saved")
            Me.Close()
            Exit Sub
        End If

        Application.DoEvents()
        'MsgBox("filename = " & filename)
        RA2106report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        MsgBox("Report saved")
        RA2106report.close()
        Me.Close()

    End Sub
End Class

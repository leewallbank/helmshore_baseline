﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration

Public Class frmMain
    Private InputFilePath As String, inFileName As String, busFile As String, TMAFile As String, FileExt As String
    Private auditlog As String, busFileName As String, TMAFileName As String

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & inFileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & inFileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(busFileName) Then System.Diagnostics.Process.Start("wordpad.exe", """" & busFileName & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & inFileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & inFileName & "_Audit.txt")
            If File.Exists(InputFilePath & inFileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & inFileName & "_Error.txt")
            If File.Exists(InputFilePath & inFileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & inFileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        inFileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)
        Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
        Dim lineCount As Integer
        Dim busCount As Integer
        Dim TMACount As Integer
        For Each InputLine As String In FileContents
            lineCount += 1
            If lineCount = 1 Then
                busFile = InputLine & vbNewLine
                TMAFile = InputLine & vbNewLine
            Else
                Dim testChars As String = Microsoft.VisualBasic.Left(InputLine, 3)
                If Microsoft.VisualBasic.Left(testChars, 1) = Chr(34) Then
                    testChars = Mid(InputLine, 2, 3)
                End If

                If testChars = "NG9" Then
                    busFile &= InputLine & vbNewLine
                    busCount += 1
                Else
                    TMAFile &= InputLine & vbNewLine
                    TMACount += 1
                End If
            End If
        Next
        Dim filename As String = InputFilePath & inFileName & "_bus.txt"
        WriteFile(filename, busFile)
        busFileName = filename
        filename = InputFilePath & inFileName & "_TMA.txt"
        WriteFile(filename, TMAFile)

        AuditLog = "File pre-processed: " & InputFilePath & filename & FileExt & vbCrLf
        AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
        AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

        AuditLog &= "Number of Buslane cases: " & busCount & vbCrLf
        AuditLog &= "Number of TMA cases: " & TMACount

        WriteFile(InputFilePath & inFileName & "_Audit.txt", auditlog)
        btnViewInputFile.Enabled = True
        btnViewOutputFile.Enabled = True
        btnViewLogFile.Enabled = True
    End Sub
End Class

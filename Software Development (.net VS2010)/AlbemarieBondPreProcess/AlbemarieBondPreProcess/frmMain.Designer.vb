﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnViewOutputFile = New System.Windows.Forms.Button()
        Me.btnViewInputFile = New System.Windows.Forms.Button()
        Me.btnProcessFile = New System.Windows.Forms.Button()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.btnViewOutputFil = New System.Windows.Forms.Button()
        Me.btnViewLogFile = New System.Windows.Forms.Button()
        Me.lblReadingFile = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'btnViewOutputFile
        '
        Me.btnViewOutputFile.Enabled = False
        Me.btnViewOutputFile.Location = New System.Drawing.Point(68, 108)
        Me.btnViewOutputFile.Name = "btnViewOutputFile"
        Me.btnViewOutputFile.Size = New System.Drawing.Size(153, 42)
        Me.btnViewOutputFile.TabIndex = 13
        Me.btnViewOutputFile.Text = "View &Output File"
        Me.btnViewOutputFile.UseVisualStyleBackColor = True
        '
        'btnViewInputFile
        '
        Me.btnViewInputFile.Enabled = False
        Me.btnViewInputFile.Location = New System.Drawing.Point(68, 60)
        Me.btnViewInputFile.Name = "btnViewInputFile"
        Me.btnViewInputFile.Size = New System.Drawing.Size(153, 42)
        Me.btnViewInputFile.TabIndex = 11
        Me.btnViewInputFile.Text = "View &Input File"
        Me.btnViewInputFile.UseVisualStyleBackColor = True
        '
        'btnProcessFile
        '
        Me.btnProcessFile.Location = New System.Drawing.Point(68, 12)
        Me.btnProcessFile.Name = "btnProcessFile"
        Me.btnProcessFile.Size = New System.Drawing.Size(153, 42)
        Me.btnProcessFile.TabIndex = 9
        Me.btnProcessFile.Text = "&Process File"
        Me.btnProcessFile.UseVisualStyleBackColor = True
        '
        'ProgressBar
        '
        Me.ProgressBar.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock
        Me.ProgressBar.Location = New System.Drawing.Point(10, 232)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(273, 22)
        Me.ProgressBar.TabIndex = 10
        '
        'btnViewOutputFil
        '
        Me.btnViewOutputFil.Enabled = False
        Me.btnViewOutputFil.Location = New System.Drawing.Point(68, 108)
        Me.btnViewOutputFil.Name = "btnViewOutputFil"
        Me.btnViewOutputFil.Size = New System.Drawing.Size(153, 42)
        Me.btnViewOutputFil.TabIndex = 12
        Me.btnViewOutputFil.Text = "View &Output File"
        Me.btnViewOutputFil.UseVisualStyleBackColor = True
        '
        'btnViewLogFile
        '
        Me.btnViewLogFile.Enabled = False
        Me.btnViewLogFile.Location = New System.Drawing.Point(68, 156)
        Me.btnViewLogFile.Name = "btnViewLogFile"
        Me.btnViewLogFile.Size = New System.Drawing.Size(153, 42)
        Me.btnViewLogFile.TabIndex = 14
        Me.btnViewLogFile.Text = "View &Log File"
        Me.btnViewLogFile.UseVisualStyleBackColor = True
        '
        'lblReadingFile
        '
        Me.lblReadingFile.AutoSize = True
        Me.lblReadingFile.BackColor = System.Drawing.Color.White
        Me.lblReadingFile.Location = New System.Drawing.Point(110, 237)
        Me.lblReadingFile.Name = "lblReadingFile"
        Me.lblReadingFile.Size = New System.Drawing.Size(72, 13)
        Me.lblReadingFile.TabIndex = 16
        Me.lblReadingFile.Text = "Reading file..."
        Me.lblReadingFile.Visible = False
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.lblReadingFile)
        Me.Controls.Add(Me.btnViewLogFile)
        Me.Controls.Add(Me.btnViewOutputFile)
        Me.Controls.Add(Me.btnViewInputFile)
        Me.Controls.Add(Me.btnProcessFile)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.btnViewOutputFil)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Albemarie & Bond Pre-processor"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnViewOutputFile As System.Windows.Forms.Button
    Friend WithEvents btnViewInputFile As System.Windows.Forms.Button
    Friend WithEvents btnProcessFile As System.Windows.Forms.Button
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents btnViewOutputFil As System.Windows.Forms.Button
    Friend WithEvents btnViewLogFile As System.Windows.Forms.Button
    Friend WithEvents lblReadingFile As System.Windows.Forms.Label

End Class

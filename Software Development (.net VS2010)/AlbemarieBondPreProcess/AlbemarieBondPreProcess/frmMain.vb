﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try

            Dim FileDialog As New OpenFileDialog
            Dim LineNumber As Integer
            Dim OutputFile As String = "offenceDate|nameTitle|clientRef|offenceLocation|nameFore|nameSur|address|addPhone|empPhone|empName|debtAmount|offenceFrom|offenceCourt|offenceValue|offenceNumber|addFax|dateOfBirth|empFax|notes" & vbCrLf
            Dim AuditLog As String
            Dim TotalBalance As Decimal = 0

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            lblReadingFile.Visible = True
            Dim FileContents(,) As String = InputFromExcel(FileDialog.FileName)
            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents)

            For LineNumber = 1 To UBound(FileContents)
                ProgressBar.Value = LineNumber

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                OutputFile &= FileContents(LineNumber, 0) & "|" ' offenceDate
                OutputFile &= FileContents(LineNumber, 1) & "|" ' nameTitle
                OutputFile &= FileContents(LineNumber, 2) & "|" ' clientRef
                OutputFile &= FileContents(LineNumber, 3).Replace("£", "") & "|" ' offenceLocation
                OutputFile &= FileContents(LineNumber, 4) & "|" ' nameFore
                OutputFile &= FileContents(LineNumber, 5) & "|" ' nameSure
                OutputFile &= ConcatAddressFields({LTrim(FileContents(LineNumber, 6) & " " & FileContents(LineNumber, 7)), FileContents(LineNumber, 8), FileContents(LineNumber, 9), FileContents(LineNumber, 10)}) & "|" ' address
                OutputFile &= FileContents(LineNumber, 11) & "|" ' addPhone
                OutputFile &= FileContents(LineNumber, 12) & "|" ' empPhone
                OutputFile &= FileContents(LineNumber, 13) & "|" ' empName
                OutputFile &= FileContents(LineNumber, 14) & "|" ' debtAmount
                OutputFile &= FileContents(LineNumber, 15) & "|" ' offenceFrom
                OutputFile &= FileContents(LineNumber, 16) & "|" ' offenceCourt
                OutputFile &= FileContents(LineNumber, 17) & "|" ' offenceValue
                OutputFile &= If(FileContents(LineNumber, 18) <> "", "APR:" & FileContents(LineNumber, 18), "") & "|" ' offenceNumber
                OutputFile &= FileContents(LineNumber, 29) & "|" ' addFax
                OutputFile &= FileContents(LineNumber, 31) & "|" ' DateOfBirth
                OutputFile &= FileContents(LineNumber, 35) & "|" ' empFax

                If FileContents(LineNumber, 19) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 19), "Fees", ";"))
                If FileContents(LineNumber, 20) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 20), "Paid Month 9", ";"))
                If FileContents(LineNumber, 21) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 21), "Paid Month 8", ";"))
                If FileContents(LineNumber, 22) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 22), "Paid Month 7", ";"))
                If FileContents(LineNumber, 23) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 23), "Paid Month 6", ";"))
                If FileContents(LineNumber, 24) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 24), "Paid Month 5", ";"))
                If FileContents(LineNumber, 25) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 25), "Paid Month 4", ";"))
                If FileContents(LineNumber, 26) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 26), "Paid Month 3", ";"))
                If FileContents(LineNumber, 27) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 27), "Paid Month 2", ";"))
                If FileContents(LineNumber, 28) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 28), "Paid Month 1", ";"))
                If FileContents(LineNumber, 30) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 30), "Review Date", ";"))
                If FileContents(LineNumber, 32) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 32), "Salary Frequency", ";"))
                If FileContents(LineNumber, 33) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 33), "Salary £", ";"))
                If FileContents(LineNumber, 34) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 34), "Best Time To Call", ";"))
                If FileContents(LineNumber, 36) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 36), "Loan Notes", ";"))
                If FileContents(LineNumber, 37) <> "" Then OutputFile &= String.Join("", ToNote(FileContents(LineNumber, 37), "Residential Status", ";"))

                OutputFile &= vbCrLf

                TotalBalance += FileContents(LineNumber, 14)

            Next LineNumber

            WriteFile(InputFilePath & FileName & "_PreProcessed.txt", OutputFile)

            AuditLog = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
            AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

            AuditLog &= "Number of new cases: " & UBound(FileContents).ToString & vbCrLf
            AuditLog &= "Total balance of new cases: " & TotalBalance.ToString

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            MsgBox("Pre-processing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class

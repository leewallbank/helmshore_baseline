<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class retnfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.retn_code = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.retn_group = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ReturnCodesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FeesSQLDataSet = New Returnedcases.FeesSQLDataSet
        Me.ReturnCodesTableAdapter = New Returnedcases.FeesSQLDataSetTableAdapters.ReturnCodesTableAdapter
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ReturnCodesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.retn_code, Me.retn_group})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(314, 495)
        Me.DataGridView1.TabIndex = 0
        '
        'retn_code
        '
        Me.retn_code.HeaderText = "Retn Code"
        Me.retn_code.Name = "retn_code"
        Me.retn_code.ReadOnly = True
        Me.retn_code.Width = 75
        '
        'retn_group
        '
        Me.retn_group.HeaderText = "Retn Group"
        Me.retn_group.Name = "retn_group"
        Me.retn_group.Width = 150
        '
        'ReturnCodesBindingSource
        '
        Me.ReturnCodesBindingSource.DataMember = "ReturnCodes"
        Me.ReturnCodesBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ReturnCodesTableAdapter
        '
        Me.ReturnCodesTableAdapter.ClearBeforeFill = True
        '
        'retnfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(314, 495)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "retnfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "NB, AB, AT,CL, PA,OT"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ReturnCodesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents FeesSQLDataSet As Returnedcases.FeesSQLDataSet
    Friend WithEvents ReturnCodesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ReturnCodesTableAdapter As Returnedcases.FeesSQLDataSetTableAdapters.ReturnCodesTableAdapter
    Friend WithEvents retn_code As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents retn_group As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration

Public Class frmMain
    Private InputFilePath As String, FileName As String, FileExt As String
    Private outFileName As String
    Dim recordType As String = ""
    Dim FileDialog As New OpenFileDialog
    Dim LineNumber As Integer = 0
    Dim lineCount As Integer = 0
    Dim phone1Count As Integer = 0
    Dim phone2Count As Integer = 0
    Dim noteCount As Integer = 0
    Dim OutputFilePhone1 As String = ""
    Dim OutputFilePhone2 As String = ""
    Dim OutputFileNotes As String = ""
    Dim AuditLog As String, ErrorLog As String = ""
    Dim phoneIDX As Integer = 0
    Dim phoneArray(5000) As phone_str
    Dim phoneNumberArray(5000) As String

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click


        Try
            process_file()

            lblReadingFile.Visible = True
            MsgBox("Pre-processing complete.", vbInformation + vbOKOnly, Me.Text)
            btnViewInputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
    Private Sub process_file()
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        Dim excel_file_contents(,) As String = InputFromExcel(FileDialog.FileName)
        Dim upperBound As Integer = UBound(excel_file_contents)
        ProgressBar.Maximum = upperBound
        Dim clientRef As String
        Dim debtorID As Integer
        For lineIdx As Integer = 0 To upperBound
            lineCount += 1
            ProgressBar.Value = lineIdx
            Application.DoEvents()
            Dim phoneNumber As String = ""

            phoneNumber = Trim(excel_file_contents(lineIdx, 6))
            phoneNumber = Replace(phoneNumber, " ", "")
            If phoneNumber = Nothing Then
                Continue For
            End If
            If IsNumeric(phoneNumber) Then
                If phoneNumber = 0 Then
                    Continue For
                End If
            Else
                If phoneNumber.Length < 11 Then
                    Continue For
                End If
            End If
            clientRef = excel_file_contents(lineIdx, 4)
            If clientRef = Nothing Then
                Continue For
            End If
            'get debtorID numbers
            Dim debtor_dt As New DataTable
            LoadDataTable("DebtRecovery", "SELECT _rowid, _createdDate " & _
                                           "FROM debtor " & _
                                           "WHERE client_ref = '" & clientRef & "'" & _
                                           " And clientschemeID = 3722", debtor_dt, False)
            Dim debtorRow As DataRow

            For Each debtorRow In debtor_dt.Rows
                Dim createdDate As Date = debtorRow.Item(1)
                If Format(createdDate, "yyyy-MM-dd") <> Format(load_dtp.Value, "yyyy-MM-dd") Then
                    Continue For
                End If
                debtorID = debtorRow.Item(0)
                Try
                    phoneArray(phoneIDX).debtorID = debtorID
                Catch ex As Exception
                    ReDim Preserve phoneArray(phoneIDX + 10)
                End Try
                phoneNumberArray(phoneIDX) = debtorID

                phoneArray(phoneIDX).phoneType = excel_file_contents(lineIdx, 7)
                phoneArray(phoneIDX).phoneNumber = excel_file_contents(lineIdx, 6)
                phoneIDX += 1

            Next
        Next
        Array.Sort(phoneNumberArray, phoneArray, 0, phoneIDX)
        Dim idx As Integer
        Dim lastDebtorID As Integer = 0
        Dim clientPhone As Integer = 0
        Dim debtorIDX As Integer = 0
        Dim debtorArray(500) As phone_str2
        Dim debtorNumberArray(500) As String
        Dim lastPhoneNo As String = ""
        Dim idx2 As Integer
        For idx = 0 To phoneIDX - 1
            debtorID = phoneArray(idx).debtorID
            If debtorID = lastDebtorID Then
                debtorNumberArray(debtorIDX) = phoneArray(idx).phoneNumber
                debtorArray(debtorIDX).phoneNumber = phoneArray(idx).phoneNumber
                debtorArray(debtorIDX).phoneType = phoneArray(idx).phoneType
                debtorIDX += 1
            Else
                If lastDebtorID = 0 Then
                    debtorNumberArray(debtorIDX) = phoneArray(idx).phoneNumber
                    debtorArray(debtorIDX).phoneNumber = phoneArray(idx).phoneNumber
                    debtorArray(debtorIDX).phoneType = phoneArray(idx).phoneType
                    debtorIDX += 1
                    lastDebtorID = debtorID
                Else
                    'sort debtor array by phone number
                    Array.Sort(debtorNumberArray, debtorArray, 0, debtorIDX)
                    lastPhoneNo = ""
                    clientPhone = 0
                    For idx2 = 0 To debtorIDX - 1
                        Dim phoneNumber As String = debtorArray(idx2).phoneNumber
                        If phoneNumber = lastPhoneNo Then
                            Continue For
                        End If
                        lastPhoneNo = phoneNumber
                        If clientPhone = 0 Then
                            OutputFilePhone1 &= lastDebtorID & "," & phoneNumber & vbNewLine
                            clientPhone = 1
                            phone1Count += 1
                        ElseIf clientPhone = 1 Then
                            OutputFilePhone2 &= lastDebtorID & "," & phoneNumber & vbNewLine
                            phone2Count += 1
                            clientPhone = 2
                        Else
                            OutputFileNotes &= lastDebtorID & "," & debtorArray(idx2).phoneType & ":" & phoneNumber & ";" & vbNewLine
                            noteCount += 1
                        End If
                    Next
                    lastDebtorID = debtorID
                    debtorIDX = 0
                    debtorNumberArray(debtorIDX) = phoneArray(idx).phoneNumber
                    debtorArray(debtorIDX).phoneNumber = phoneArray(idx).phoneNumber
                    debtorArray(debtorIDX).phoneType = phoneArray(idx).phoneType
                    debtorIDX += 1
                End If
            End If
        Next
        'do last case
        Array.Sort(debtorNumberArray, debtorArray, 0, debtorIDX)
        lastPhoneNo = ""

        clientPhone = 0
        For idx2 = 0 To debtorIDX - 1
            Dim phoneNumber As String = debtorArray(idx2).phoneNumber
            If phoneNumber = lastPhoneNo Then
                Continue For
            End If
            lastPhoneNo = phoneNumber
            If clientPhone = 0 Then
                OutputFilePhone1 &= lastDebtorID & "," & phoneNumber & vbNewLine
                clientPhone = 1
                phone1Count += 1
            ElseIf clientPhone = 1 Then
                OutputFilePhone2 &= lastDebtorID & "," & phoneNumber & vbNewLine
                phone2Count += 1
                clientPhone = 2
            Else
                OutputFileNotes &= lastDebtorID & "," & debtorArray(idx2).phoneType & ":" & phoneNumber & ";" & vbNewLine
                noteCount += 1
            End If
        Next
        If phone1Count > 0 Then
            outFileName = "Blackburn_phone1_" & Format(Now, "yyyyMMdd")
            outFileName = InputFilePath & outFileName & ".csv"
            WriteFile(outFileName, OutputFilePhone1)
        End If
        If phone2Count > 0 Then
            outFileName = "Blackburn_phone2_" & Format(Now, "yyyyMMdd")
            outFileName = InputFilePath & outFileName & ".csv"
            WriteFile(outFileName, OutputFilePhone2)
        End If
        If noteCount > 0 Then
            outFileName = "Blackburn_phoneNotes_" & Format(Now, "yyyyMMdd")
            outFileName = InputFilePath & outFileName & ".csv"
            WriteFile(outFileName, OutputFileNotes)
        End If

        outFileName = FileName & "_Audit.txt"
        outFileName = InputFilePath & outFileName
        AuditLog = "File pre-processed: " & FileDialog.FileName & vbCrLf
        AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
        AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf
        AuditLog &= "Number of rows: " & lineCount & vbCrLf
        AuditLog &= "Number of phone 1 rows: " & phone1Count & vbCrLf
        AuditLog &= "Number of phone 2 rows: " & phone2Count & vbCrLf
        AuditLog &= "Number of phone notes: " & noteCount & vbCrLf
        WriteFile(outFileName, AuditLog)

        If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
    End Sub


    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(FileDialog.FileName) Then System.Diagnostics.Process.Start("excel.exe", """" & FileDialog.FileName & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(outFileName) Then System.Diagnostics.Process.Start(outFileName)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try
            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        load_dtp.Value = DateAdd(DateInterval.Day, -1, Now)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed_wrp.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFil_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub clobtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    End Sub
End Class

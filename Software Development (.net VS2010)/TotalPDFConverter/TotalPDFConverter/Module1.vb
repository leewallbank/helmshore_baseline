﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data

Module Module1
    Public sqlCon2 As New SqlConnection
    'Sub update_sql(ByVal upd_txt As String)
    '    Try
    '        If sqlCon2.State = ConnectionState.Closed Then
    '            Connect_sqlDb()
    '        End If
    '        Dim sqlCMD As SqlCommand

    '        'Define attachment to database table specifics
    '        sqlCMD = New SqlCommand
    '        With sqlCMD
    '            .Connection = sqlCon2
    '            .CommandText = upd_txt
    '            .ExecuteNonQuery()
    '        End With

    '        'Clean up the connection
    '        sqlCMD = Nothing

    '    Catch ex As Exception
    '        MsgBox(ex.Message)
    '    End Try
    'End Sub
    Public Sub Connect_sqlDb()
        Try
            If Not IsNothing(sqlCon2) Then
                'This is only necessary following an exception...
                If sqlCon2.State = ConnectionState.Open Then sqlCon2.Close()
            End If
            sqlCon2.ConnectionString = ""
            sqlCon2.ConnectionString = ConfigurationManager.ConnectionStrings("PDF2TIFF").ConnectionString
            sqlCon2.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings("PDF2TIFF").ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings("PDF2TIFF").ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub
End Module

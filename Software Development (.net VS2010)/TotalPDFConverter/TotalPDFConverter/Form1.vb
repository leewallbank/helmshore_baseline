﻿Imports System.IO
Imports CommonLibrary
Imports System.Data

Imports System.Data.SqlClient

Public Class Form1
    Dim start_remit_no As Integer
    Dim end_remit_no As Integer
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles convertbtn.Click
        Try
            Dim file_name As String
            ConnectDb2("DebtRecovery")
            'create directories for returns
            Dim dir_name As String = "O:\DebtRecovery\Archives\Remittances\TIF2\"
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create tif2 folder")
                Exit Sub
                End
            End Try
            dir_name = "O:\DebtRecovery\Archives\Remittances\TIF2 do not use\"
            Try
                Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
                If System.IO.Directory.Exists(dir_name) = False Then
                    di = System.IO.Directory.CreateDirectory(dir_name)
                Else
                    System.IO.Directory.Delete(dir_name, True)
                    di = System.IO.Directory.CreateDirectory(dir_name)
                End If
            Catch ex As Exception
                MsgBox("Unable to create tif2 do not use folder")
                Exit Sub
                End
            End Try

            start_remit_no = start_remit_tbox.Text
            'get last remit no
            convertbtn.Enabled = False
            start_remit_tbox.Enabled = False

            Try
                end_remit_no = GetSQLResults2("DebtRecovery", "select max(_rowid) from Remit")
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try

            Dim unknownDebtorID As Integer = 0
            Dim remit_no As Integer
            Dim parm_yr As String = ""
            Dim parm_mth As String = ""
            Dim parm_day As String = ""

            'Dim inputfilepath As String
            'Dim infileName As String
            'Dim outfileName As String
            Dim pbar As Integer = 0
            Dim fileFound As Boolean = False
           
            For remit_no = start_remit_no To end_remit_no
                pbar += 1
                Try
                    ProgressBar1.Value = pbar
                Catch ex As Exception
                    pbar = 0
                End Try
                Application.DoEvents()
                'get clientschemeid of case
                Dim Array As Object() = Nothing
                Try
                    Array = GetSQLResultsArray2("DebtRecovery", "select clientSchemeID, _rowid from Debtor" & _
                                                                      " where return_remitID = " & remit_no)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                Dim csid As Integer
                Dim debtor As Integer
                Try
                    csid = Array(0)
                    debtor = Array(1)
                Catch ex As Exception
                    Continue For
                End Try

                'check work-type (12 for CMEC case)
                Dim schID As Integer
                Try
                    schID = GetSQLResults2("DebtRecovery", "select schemeID from clientScheme where _rowid = " & csid)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

                Dim workType As Integer
                Try
                    workType = GetSQLResults2("DebtRecovery", "select work_type from Scheme where _rowid = " & schID)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                If workType <> 12 Then
                    Continue For
                End If

                'get remit date
                Dim remitDate As Date
                Try
                    remitDate = GetSQLResults2("DebtRecovery", "select date from Remit where _rowid = " & remit_no)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try

                parm_yr = Format(remitDate, "yyyy") & "\"
                parm_mth = Format(remitDate, "MM") & "_" & Format(remitDate, "MMM") & "\"
                parm_day = Format(remitDate, "dd") & "_" & Format(remitDate, "ddd")
                file_name = "O:\DebtRecovery\Archives\" & parm_yr & parm_mth & parm_day & "\Remittances\" & remit_no & "\Returns"
                Try
                    For Each foundFile As String In My.Computer.FileSystem.GetFiles _
                 (file_name, FileIO.SearchOption.SearchTopLevelOnly, "*.pdf")
                        'get debtor from file name
                        Dim fileIDX As Integer
                        For fileIDX = foundFile.Length To 1 Step -1
                            If Mid(foundFile, fileIDX, 1) = "-" Then
                                Exit For
                            End If
                        Next
                        Try
                            debtor = Mid(foundFile, fileIDX + 1, foundFile.Length - fileIDX - 4)
                        Catch ex As Exception
                            debtor = unknownDebtorID
                            unknownDebtorID += 1
                        End Try
                        new_filename = "O:\DebtRecovery\Archives\Remittances\TIF2 do not use\" & debtor & ".pdf"
                        My.Computer.FileSystem.CopyFile(foundFile, new_filename, True)
                        fileFound = True
                    Next
                Catch ex As Exception

                End Try
            Next
            Dim errorFound As Boolean = False
            Dim PDFConvert As Object
            PDFConvert = CreateObject("PDFConverter.PDFConverterX")
            PDFConvert.LogFile = "H:pdfconvert.log"
            infilename = "O:\DebtRecovery\Archives\Remittances\TIF2 do not use\"
            Dim exitLoop As Boolean = False
            If fileFound = False Then
                exitLoop = True
            End If
            While exitLoop = False
                errorFound = False
                For Each foundFile As String In My.Computer.FileSystem.GetFiles _
             (infilename, FileIO.SearchOption.SearchTopLevelOnly, "*.pdf")
                    inputfilepath = Path.GetDirectoryName(foundFile)
                    inputfilepath &= "\"
                    outfileName = inputfilepath & Path.GetFileNameWithoutExtension(foundFile) & ".tiff"
                    outfileName = Replace(outfileName, " do not use", "")
                    ' PDFConvert.Convert(foundFile, outfileName, "-c TIF  -s -t'[Name].page#.tiff' -tc LZW ")
                    PDFConvert.Convert(foundFile, outfileName, "-c TIF  -s -t'[Name].page#.tiff'")
                    pbar += 1
                    Try
                        ProgressBar1.Value = pbar
                    Catch ex As Exception
                        pbar = 0
                    End Try
                    Application.DoEvents()
                    'look for tiff page 1
                    Dim searchName As String = outfileName
                    If System.IO.File.Exists(searchName) Then
                        errorFound = True
                    Else
                        'delete pdf file
                        Dim PDFName As String = inputfilepath & Path.GetFileName(foundFile)
                        System.IO.File.Delete(PDFName)
                    End If
                Next

                If errorFound Then
                    If MsgBox("Errors -found - Continue with a further pass?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                        exitLoop = True
                    End If
                Else
                    exitLoop = True
                End If
            End While
            PDFConvert = Nothing
            'now rename files 
            infilename = "O:\DebtRecovery\Archives\Remittances\TIF2\"
            If Not fileFound Then
                MsgBox("No pdfs found")
            Else
                For Each foundFile As String In My.Computer.FileSystem.GetFiles _
        (infilename, FileIO.SearchOption.SearchTopLevelOnly, "*.tiff")
                    inputfilepath = Path.GetDirectoryName(foundFile)
                    inputfilepath &= "\"
                    outfileName = inputfilepath & Path.GetFileName(foundFile)
                    Dim tif_new_file_name As String = Replace(outfileName, ".page", "-doc-page")
                    Try
                        Rename(foundFile, tif_new_file_name)
                    Catch ex As Exception
                        MsgBox("Unable to rename file " & foundFile)
                        Me.Close()
                        Exit Sub
                    End Try
                    pbar += 1
                    Try
                        ProgressBar1.Value = pbar
                    Catch ex As Exception
                        pbar = 0
                    End Try
                    Application.DoEvents()
                Next
                'update table with latest remit_no
                Dim upd_txt As String = "update LastRemitPdf2Tiff set last_remit_no = " & end_remit_no

                update_sql(upd_txt)
                MsgBox("Files converted")
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
       
        Me.Close()
    End Sub
    
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            start_remit_no = GetSQLResults("FeesSQL", "select last_remit_no from LastRemitPdf2Tiff")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        start_remit_tbox.Text = start_remit_no
    End Sub
End Class

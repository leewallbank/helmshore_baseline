﻿Imports iTextSharp.text.pdf
Imports iTextSharp.text

Public Class itsEvents
    Inherits PdfPageEventHelper
    Public Overrides Sub OnEndPage(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document)
        Dim ch As New Chunk(New String(" "c, 120) & "Page " & writer.PageNumber)
        ch.Font.Size = 8

        document.Add(ch)

    End Sub
End Class
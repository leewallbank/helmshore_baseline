Public Class mainform
    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline, caption As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim propaddr As String = ""
        Dim curraddr As String = ""
        Dim name As String = ""
        Dim name2 As String = ""
        Dim ta_name As String = ""
        'Dim lonum As String = ""
        Dim lostring As String = ""
        Dim idx, idx2, idx3 As Integer
        Dim lines As Integer = 0
        Dim debt_amt, war_amt As Decimal
        Dim lodate, fromdate, todate As Date
        Dim clref As String = ""
        Dim addrline As String = ""
        Dim amtstring As String = ""
        Dim assoc_line1 As String = ""
        Dim assoc_line2 As String = ""
        Dim comments As String = Nothing

        Dim tot_debt As Decimal = 0
        Dim rep_tot_debt
        Dim line_length As Integer

        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'read split names into array
        read_names()
        If name_count = 0 Then
            MsgBox("Can't read split names database")
            Exit Sub
        End If
        Dim check_only As Boolean = False
        'check if just total check is required
        If MsgBox("Just do totals check?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
            check_only = True
        End If

        'write out headings
        outline = "Client ref|Name|DebtAddress|Name2|Current Address|LO Date|Warrant Amt|From date|To date|Debt Amount|Comments|TA name|" & vbNewLine
        outfile = outline

        'look for Assoc line 
        tot_cases = 0
        For idx = 1 To lines - 1
            If Mid(line(idx), 2, 11) = "Total Cases" Then
                rep_tot_cases = Mid(line(idx), 40, 10)
            End If
            If Mid(line(idx), 2, 12) = "Total Amount" Then
                rep_tot_debt = Mid(line(idx), 40, 20)
                If tot_cases <> rep_tot_cases Then
                    MsgBox("Cases read = " & tot_cases & " but report has " & rep_tot_cases)
                ElseIf tot_debt <> rep_tot_debt Then
                    MsgBox("Total Debt read = " & tot_debt & " but report has " & rep_tot_debt)
                Else
                    MsgBox("Totals agree - Cases = " & tot_cases & "  Debt = " & tot_debt)
                End If
                Exit For
            End If
            caption = Mid(line(idx), 4, 6)
            If caption = "Assoc:" Then
                tot_cases += 1
                'look back to get client ref line
                debt_amt = 0
                war_amt = 0
                assoc_line1 = ""
                clref = " "
                comments = ""
                ta_name = ""
                For idx2 = idx - 1 To 1 Step -1
                    If IsNumeric(Microsoft.VisualBasic.Left(line(idx2), 4)) Then
                        clref = Mid(line(idx2), 2, 10)
                        'T63116 make length 15 instead of 10
                        amtstring = Mid(line(idx2), 121, 15)
                        If Not IsNumeric(amtstring) Then
                            errorfile = errorfile & "Line  " & idx2 & " - Debt amount not numeric - " _
                                     & amtstring & vbNewLine
                        Else
                            debt_amt = amtstring
                            tot_debt += debt_amt
                        End If
                        'T63116 make length 15 instead of 10
                        amtstring = Mid(line(idx2), 89, 15)
                        If Not IsNumeric(amtstring) Then
                            errorfile = errorfile & "Line  " & idx2 & " - Warrant amount not numeric - " _
                                     & amtstring & vbNewLine
                        Else
                            war_amt = amtstring

                        End If
                        Exit For
                    End If
                Next
                'display details of assoc line for amending
                text_line = Mid(line(idx), 11, 120)
                'check there are spaces between split names
                If Not check_only Then
                    case_no += 1
                    check_names()
                    line_length = Microsoft.VisualBasic.Len(text_line)
                    'add a pipe before T/A
                    For idx2 = 1 To line_length
                        If Mid(text_line, idx2, 3) = "T/A" Then
                            Dim temp As String
                            temp = Microsoft.VisualBasic.Left(text_line, idx2 - 1) & "|" & Microsoft.VisualBasic.Right(text_line, line_length - idx2 + 1)
                            text_line = temp
                            line_length += 1
                            Exit For
                        End If
                    Next

                    textfrm.ShowDialog()
                End If

                assoc_line1 = Trim(text_line)
                line_length = Microsoft.VisualBasic.Len(assoc_line1)
                'split into name and address

                For idx2 = 1 To line_length
                    If Mid(assoc_line1, idx2, 1) = "|" Then
                        name = Microsoft.VisualBasic.Left(assoc_line1, idx2 - 1)
                        'look for another pipe for ta name split
                        curraddr = Microsoft.VisualBasic.Right(assoc_line1, line_length - idx2)
                        line_length = Microsoft.VisualBasic.Len(curraddr)
                        For idx3 = 1 To line_length
                            If Mid(curraddr, idx3, 1) = "|" Then
                                ta_name = Mid(curraddr, 4, idx3 - 4)
                                curraddr = Microsoft.VisualBasic.Right(curraddr, line_length - idx3)
                            End If
                        Next
                        Exit For
                    End If
                Next
                
                'get another assoc or prop/liab/court line
                assoc_line2 = ""
                For idx2 = idx + 1 To lines - 1
                    caption = Mid(line(idx2), 4, 6)
                    If caption = "Assoc:" Then
                        text_line = Mid(line(idx2), 11, 120)
                        'check there are spaces between split names
                        If Not check_only Then
                            check_names()
                            textfrm.ShowDialog()
                        End If
                        line_length = Microsoft.VisualBasic.Len(text_line)
                        For idx3 = 1 To line_length
                            If Mid(text_line, idx3, 1) = "|" Then
                                name2 = Microsoft.VisualBasic.Left(text_line, idx3 - 1)
                                comments = "2nd name address - " & Microsoft.VisualBasic.Right(text_line, line_length - idx3)
                                Exit For
                            End If
                        Next

                    End If
                    If caption = "Prop.:" Then
                        If IsNumeric(Mid(line(idx2), 11, 4)) Then
                            propaddr = Trim(Mid(line(idx2), 23, 120))
                        Else
                            propaddr = Trim(Mid(line(idx2), 11, 120))
                        End If
                    End If
                    If caption = "Liab.:" Then
                        lostring = Mid(line(idx2), 11, 8)
                        '23.10.2014 check 2 digits after 2nd /
                        Dim slashIDX As Integer
                        For slashIDX = lostring.Length To 1 Step -1
                            If Mid(lostring, slashIDX, 1) = "/" Then
                                Exit For
                            End If
                        Next
                        If Not IsDate(lostring) Or lostring.Length - slashIDX < 2 Then
                            lostring = Mid(line(idx2), 12, 8)
                            If Not IsDate(lostring) Then
                                errorfile = errorfile & "Line  " & idx2 & " - LO From Date not valid - " _
                                                             & lostring & vbNewLine
                            Else
                                fromdate = lostring
                            End If
                        Else
                            fromdate = lostring
                        End If
                        lostring = Mid(line(idx2), 20, 8)
                        If Not IsDate(lostring) Then
                            lostring = Mid(line(idx2), 21, 8)
                            If Not IsDate(lostring) Then
                                errorfile = errorfile & "Line  " & idx2 & " - LO To Date not valid - " _
                                                             & lostring & vbNewLine
                            Else
                                todate = lostring
                            End If
                        Else
                            todate = lostring
                        End If
                        'look for second to date
                        lostring = Mid(line(idx2), 38, 8)
                        '23.10.2014 start from -
                        Dim hyphen As Integer = InStr(lostring, "-")
                        lostring = Mid(line(idx2), 38 + hyphen, 8)
                        If IsDate(lostring) Then
                            If lostring > todate Then
                                todate = lostring
                            End If
                        End If
                    End If
                    If caption = "Court:" Then
                        lostring = Mid(line(idx2), 11, 8)
                        If Not IsDate(lostring) Then
                            errorfile = errorfile & "Line  " & idx2 & " - LO Date not valid - " _
                                                         & lostring & vbNewLine
                        Else
                            lodate = lostring
                        End If
                        idx = idx2
                        Exit For
                    End If

                Next
                'validate case details
                If clref = Nothing Then
                    errorfile = errorfile & "Line  " & idx2 & " - No client reference" & vbNewLine
                End If

                'save case in outline
                outfile = outfile & clref & "|" & name & "|" & propaddr & "|" & name2 _
                 & "|" & curraddr & "|" & lodate & "|" & war_amt & _
                "|" & fromdate & "|" & todate & "|" & debt_amt & "|" & comments & "|" & ta_name & vbNewLine
                name = ""
                name2 = ""
                propaddr = ""
                curraddr = ""
                lodate = Nothing
                debt_amt = Nothing
                war_amt = Nothing
                comments = Nothing
                fromdate = Nothing
                todate = Nothing
            End If
        Next
        viewbtn.Enabled = True
        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
        End If
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
    

    Private Sub checkbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

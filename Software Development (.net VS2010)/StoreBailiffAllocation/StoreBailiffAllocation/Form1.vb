﻿Imports CommonLibrary
'Imports System.Configuration
'Imports System.IO
Public Class MainForm
    Dim bail_dt As New DataTable
    Dim null_date As Date = CDate("Jan 1, 1900")
    Dim today As Integer = Now.DayOfWeek
    Dim lastThursday As Date = DateAdd(DateInterval.Day, -Weekday(Now) - 2, Now)
    Dim lastSaturday As Date = DateAdd(DateInterval.Day, 2, lastThursday)
    Dim todaysDate As Date = CDate(Format(Now, "MMM dd, yyyy ") & "12:00:00 AM")
    Dim yesterday As Date = DateAdd(DateInterval.Day, -1, todaysDate)
    Dim FourDaysAgo As Date = DateAdd(DateInterval.Day, -4, todaysDate)
    Dim tenDaysInFuture As Date = DateAdd(DateInterval.Day, 10, todaysDate)
    Dim errorFile As String = ""

    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim bailArray, clientArray As Object()
        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("FeesSQL")

        'Fees(table)
        LoadDataTable2("FeesSQL", "SELECT bail_ID, bail_debtorID, bail_seq_no " & _
                                              "FROM BailiffAllocationTable " & _
                                              "WHERE bail_de_allocation_found_date = '" & null_date & "'", bail_dt, False)


        Dim row_count As Integer
        Dim no_of_rows As Integer = bail_dt.Rows.Count
        For Each row In bail_dt.Rows
            row_count += 1
            Dim bailID As Integer = row.Item(0)
            Dim debtorID As Integer = row.item(1)
            Dim seqNo As Integer = row.item(2)
            'check if still allocated
            Dim currentBailID As Integer
            bailArray = Nothing
            Try
                bailArray = GetSQLResultsArray2("DebtRecoveryLocal", "SELECT bailiffID " & _
                                               "FROM debtor " & _
                                               "WHERE _rowid= " & debtorID & _
                                               " AND bail_current = 'Y'")
                currentBailID = bailArray(0)
            Catch ex As Exception
                currentBailID = 0
            End Try
            Dim upd_txt As String
            If currentBailID = bailID Then  'still allocated
                Continue For
            ElseIf currentBailID > 0 Then
                'allocated to another bailiff so update
                'update allocation found date
                upd_txt = "update BailiffAllocationTable set bail_de_allocation_found_date = '" & _
                    Format(Now, "yyyy-MM-dd") & "'" & _
                    " WHERE bail_seq_no = " & seqNo
                update_sql(upd_txt)
                Continue For
            End If

            'May be de-allocated due to remit - only update tues/weds/thurs/Fri
            'But not Tuesday if Monday was bank holiday
            'and not Friday if it's a bank holiday 
            'CAN NOW UPDATE EVERY DAY AS DO NOT DE-ALLOCATE FOR REMIT NOW
            'If today >= 2 And today <= 5 Then
            '    Dim updateRequired As Boolean = True
            '    If today = 2 Then 'Tuesday
            '        'was Monday a bank holiday?
            '        Dim testDate As String = Nothing
            '        Try
            '            testDate = GetSQLResults2("FeesSQL", "SELECT BankHolidayDate " & _
            '                                          "FROM BankHolidayDates " & _
            '                                          "WHERE BankHolidayDate= '" & yesterday & "'")
            '        Catch ex As Exception
            '            testDate = Nothing
            '        End Try
            '        If testDate <> Nothing Then
            '            updateRequired = False
            '        End If
            '    End If
            '    If today = 5 Then  '/friday
            '        'Is today a bank holiday?
            '        Dim testDate As String = Nothing
            '        Try
            '            testDate = GetSQLResults2("FeesSQL", "SELECT BankHolidayDate " & _
            '                                          "FROM BankHolidayDates " & _
            '                                          "WHERE BankHolidayDate= '" & todaysDate & "'")
            '        Catch ex As Exception
            '            testDate = Nothing
            '        End Try
            '        If testDate <> Nothing Then
            '            updateRequired = False
            '        End If
            '    End If
            'update allocation found date
            ' If updateRequired Then
            upd_txt = "update BailiffAllocationTable set bail_de_allocation_found_date = '" & _
                Format(Now, "yyyy-MM-dd") & "'" & _
                " WHERE bail_seq_no = " & seqNo
            update_sql(upd_txt)
            'End If
            'End If
        Next

        Dim debt_dt As New DataTable

        'get all allocated cases
        Dim start_date As Date = Now
        start_date = DateAdd("d", 1 - Weekday(start_date), start_date)
        LoadDataTable2("DebtRecoveryLocal", "SELECT _rowid, clientSchemeID, bailiffID, last_stageID, bail_allocated " & _
                                               "FROM debtor " & _
                                               "WHERE status_open_closed = 'O' " & _
                                               " AND bail_allocated >= '" & Format(FourDaysAgo, "yyyy-MM-dd") & "'" & _
                                               " AND _rowid > 99 AND bail_current = 'Y'", debt_dt, False)

        For Each row In debt_dt.Rows
            Dim bailID As Integer
            Try
                bailID = row.Item(2)
            Catch ex As Exception

            End Try

            'ignore certain bailiffIDS
            If bailID = 0 Or bailID = 2477 Or bailID = 2593 Or bailID = 2691 Or bailID = 2831 Or bailID = 3349 _
                Or bailID = 37 Or bailID = 2245 Or bailID = 2930 Or bailID = 2942 Or bailID = 3311 Or bailID = 3342 Then
                Continue For
            End If
            'check bailiff is a bailiff
            bailArray = GetSQLResultsArray2("DebtRecoveryLocal", "SELECT agent_type, name_sur " & _
                                                    "FROM bailiff " & _
                                                    "WHERE _rowid= " & bailID)
            Dim agentType As String = bailArray(0)
            'include admin 3954 shakespeare martineau for RD516
            If agentType <> "B" And bailID <> 3954 Then
                Continue For
            End If


            Dim bailName As String = Microsoft.VisualBasic.Left(bailArray(1), 7)
            If bailName = "Account" Or bailName = "Stacked" Or bailName = "Overplu" Or bailName = "Return " Then
                Continue For
            End If
            Dim csID As Integer = row.item(1)
            clientArray = GetSQLResultsArray2("DebtRecoveryLocal", "SELECT clientID " & _
                                                    "FROM clientScheme " & _
                                                    "WHERE _rowid= " & csID)
            Dim clientID As Integer = clientArray(0)
            If clientID = 1 Or clientID = 2 Or clientID = 24 Then
                Continue For
            End If
            Dim debtorID As Integer = row.item(0)
            Dim bail_allocated As Date
            Try
                bail_allocated = row.item(4)
            Catch ex As Exception
                Continue For
            End Try

            'check if already on table
            Dim fees_dt As New DataTable
            LoadDataTable2("FeesSQL", "SELECT bail_allocation_date, bail_de_alloc_for_remit, bail_seq_no " & _
                                                   "FROM BailiffAllocationTable " & _
                                                   "WHERE bail_debtorID = " & debtorID & _
                                                   " AND bail_de_allocation_found_date = '" & null_date & "'" & _
                                                    " AND bail_ID = " & bailID, fees_dt, False)
            Dim row_found As Boolean = False
            If fees_dt.Rows.Count > 0 Then
                row_found = True
            End If
            If Not row_found Then
                Dim stageID As Integer = row.item(3)
                Dim stagename As String
                stagename = GetSQLResults2("DebtRecoveryLocal", "SELECT name " & _
                                                       "FROM Stage " & _
                                                       "WHERE _rowid= " & stageID)
                Dim upd_txt As String = "insert into BailiffAllocationTable (bail_ID, bail_debtorID, " & _
                    "bail_allocation_date, bail_de_alloc_for_remit, bail_stage_name, bail_de_allocation_found_date)" & _
                    "values (" & bailID & "," & debtorID & ",'" & Format(bail_allocated, "yyyy-MM-dd") & "'," & 0 & ",'" &
                    stagename & "','" & Format(null_date, "yyyy-MM-dd") & "')"
                'allow future dat up 10 10 days
                If bail_allocated > tenDaysInFuture Then
                    'MsgBox("future bailiff allocated for case " & debtorID)
                    errorFile &= "future bailiff allocated for case " & debtorID & vbNewLine
                End If
                update_sql(upd_txt)
            End If
        Next
        'Check cases marked as found in last few days to see if re-allocated
        Dim fees2_dt As New DataTable
        Dim three_days_ago As Date = DateAdd(DateInterval.Day, -3, todaysDate)
        LoadDataTable2("FeesSQL", "SELECT bail_allocation_date, bail_debtorID, bail_ID, bail_seq_no " & _
                                               "FROM BailiffAllocationTable " & _
                                               "WHERE bail_de_allocation_found_date >= '" & Format(three_days_ago, "yyyy-MM-dd") & "'" & _
                                               " AND bail_de_allocation_found_date < '" & Format(yesterday, "yyyy-MM-dd") & "'", fees2_dt, False)


        For Each row In fees2_dt.Rows
            Dim allocationdate As Date = row(0)
            Dim bailDebtorID As Integer = row(1)
            Dim bailID As Integer = row(2)
            Dim debt2_dt As New DataTable
            'check if still allocated with same allocation date and same bailiffID
            LoadDataTable2("DebtRecoveryLocal", "SELECT bailiffID, bail_allocated " & _
                                                "FROM debtor " & _
                                                "WHERE status_open_closed = 'O' " & _
                                                " AND _rowid  = " & bailDebtorID & _
                                                " and bail_current = 'Y'", debt2_dt, False)
            For Each debtrow In debt2_dt.Rows
                Dim debtBailiffiD As Integer = debtrow(0)
                Dim debtAllocationdate As Date = debtrow(1)
                If debtBailiffiD = bailID And
                    debtAllocationdate = allocationdate Then
                    'still allocated so update found date to null
                    Dim bailSeqNo As Integer = row(3)
                    Dim upd_txt As String = "update BailiffAllocationTable set bail_de_allocation_found_date = '" & _
                   null_date & "'" & _
                   " WHERE bail_seq_no = " & bailSeqNo
                    update_sql(upd_txt)
                End If
            Next
        Next
        'MsgBox("done - remove later")
        'If errorFile <> "" Then
        '    WriteFile("\\ross-helm-fp001\Rossendales Shared\FutureAllocationdates\StoreBailiffError" & Format(Now, "yyyy-MM-dd") & ".txt", errorFile)
        'End If
        Me.Close()
    End Sub
End Class

Public Class mainform



    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim propaddr As String = ""
        Dim curraddr As String = ""
        Dim name As String = ""
        Dim idx, idx2 As Integer
        Dim lines As Integer = 0
        Dim debt_amt As Decimal
        Dim offence_court As Date
        Dim dob As Date
        Dim offence_reg As Date
        Dim offence_from As Date
        Dim offence_date As Date
        Dim clref As String = ""
        Dim title As String = ""
        Dim forename As String = ""
        Dim offence_number As Decimal
        Dim surname As String = ""
        Dim addr1 As String = ""
        Dim addr2 As String = ""
        Dim addr3 As String = ""
        Dim addr4 As String = ""
        Dim pcode As String = ""
        Dim phone As String = ""
        Dim phone2 As String = ""
        Dim email As String = ""
        Dim debt_addr1 As String = ""
        Dim debt_addr2 As String = ""
        Dim debt_addr3 As String = ""
        Dim debt_addr4 As String = ""
        Dim debt_addr5 As String = ""
        Dim name2 As String = ""
        Dim latepaymentCharge As Decimal = 0
        Dim code As String = ""
        Dim sep4482 As String = "|"
        Dim sep4481 As String = "*"
        Dim sep4483 As String = "`"
        Dim sep4486 As String = "~"
        Dim sep4487 As String = "#"
        Dim sep4484 As String = "%"
        Dim sep4496 As String = "<"
        Dim sep4485 As String = ">"
        Dim sep4491 As String = "$"
        Dim sep4489 As String = "?"
        Dim sep4492 As String = "^"
        Dim sep4490 As String = "!"
        Dim sep4480 As String = "+"
        Dim sep4479 As String = "{"
        Dim sep4493 As String = "}"
        Dim sep4495 As String = "["
        Dim sep4494 As String = "]"

        Dim cnt4479 As Integer = 0
        Dim cnt4480 As Integer = 0
        Dim cnt4481 As Integer = 0
        Dim cnt4482 As Integer = 0
        Dim cnt4483 As Integer = 0
        Dim cnt4484 As Integer = 0
        Dim cnt4485 As Integer = 0
        Dim cnt4486 As Integer = 0
        Dim cnt4487 As Integer = 0
        Dim cnt4489 As Integer = 0
        Dim cnt4490 As Integer = 0
        Dim cnt4491 As Integer = 0
        Dim cnt4492 As Integer = 0
        Dim cnt4493 As Integer = 0
        Dim cnt4494 As Integer = 0
        Dim cnt4495 As Integer = 0
        Dim cnt4496 As Integer = 0

        Dim bal4479 As Decimal = 0
        Dim bal4480 As Decimal = 0
        Dim bal4481 As Decimal = 0
        Dim bal4482 As Decimal = 0
        Dim bal4483 As Decimal = 0
        Dim bal4484 As Decimal = 0
        Dim bal4485 As Decimal = 0
        Dim bal4486 As Decimal = 0
        Dim bal4487 As Decimal = 0
        Dim bal4489 As Decimal = 0
        Dim bal4490 As Decimal = 0
        Dim bal4491 As Decimal = 0
        Dim bal4492 As Decimal = 0
        Dim bal4493 As Decimal = 0
        Dim bal4494 As Decimal = 0
        Dim bal4495 As Decimal = 0
        Dim bal4496 As Decimal = 0
        Dim newCases As Integer = 0
        Dim withdrawnCases As Integer = 0
        Dim updateCases As Integer = 0
        Dim queryCases As Integer = 0
        Dim reissueCases As Decimal = 0
        Dim continueCases As Decimal = 0
        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)
        auditFile = "File pre-processed: " & filename & vbNewLine & _
        "Date pre-processed: " & Now & vbNewLine & _
        "By: " & My.User.Name & vbNewLine & vbNewLine


        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "ClientRef|OffenceNumber|DebtAmount|title|forename|surname|Name2|curr_addr1" & _
        "|curr_addr2|curr_addr3|curr_addr4|curr_postcode|Phone|Phone2|Email|debt_addr1|" & _
        "debt_addr2|debt_addr3|debt_addr4|debt_addr5|OffenceCourt|OffenceReg|OffenceFrom|OffenceDate|" & _
        "DOB|LatePaymentCharge|comments" & vbNewLine
        outfile_new = outline
        outfile_upd = outline
        outfile_withdraw = outline
        outfile_query = outline
        outfile_reissue = outline
        outfile_continue = outline
        outfile_4482 = outline
        outfile_4479 = Replace(outline, "|", sep4479)
        outfile_4480 = Replace(outline, "|", sep4480)
        outfile_4481 = Replace(outline, "|", sep4481)
        outfile_4483 = Replace(outline, "|", sep4483)
        outfile_4484 = Replace(outline, "|", sep4484)
        outfile_4485 = Replace(outline, "|", sep4485)
        outfile_4486 = Replace(outline, "|", sep4486)
        outfile_4487 = Replace(outline, "|", sep4487)
        outfile_4489 = Replace(outline, "|", sep4489)
        outfile_4490 = Replace(outline, "|", sep4490)
        outfile_4491 = Replace(outline, "|", sep4491)
        outfile_4492 = Replace(outline, "|", sep4492)
        outfile_4493 = Replace(outline, "|", sep4493)
        outfile_4494 = Replace(outline, "|", sep4494)
        outfile_4495 = Replace(outline, "|", sep4495)
        outfile_4496 = Replace(outline, "|", sep4496)
        Dim vacated As Boolean
        Dim CSID As Integer = 0
        Dim outfile_upd2 As String = "Balance/Payment,DebtorID,ClientREf,New Debt, OnestepClientBalance" & vbNewLine
        'process cases delimited by a pipe
        For idx = 0 To lines - 1
            ProgressBar1.Value = (idx / lines) * 100
            Dim amtstring As String
            Dim lostring, field As String
            Dim line_length As Integer = line(idx).Length
            Dim start_field_idx As Integer = 1
            Dim end_field_idx As Integer = 0
            Dim field_no As Integer = 0
            Dim row_count As Integer = 0
            Dim calc_row_count As Integer = 0
            Dim requestType As Integer = 0
            For idx2 = 1 To line_length
                'first line is date and row-count
                'ignore any row of length < 10
                If line(idx).Length < 10 Then
                    Continue For
                End If
                If idx = 0 Then
                    Try
                        row_count = Microsoft.VisualBasic.Right(line(0), line(0).Length - 9)
                    Catch ex As Exception
                        errorfile = errorfile & "Line  " & idx & " - row-count is not numeric" & vbNewLine
                    End Try
                    Exit For
                End If

                If Mid(line(idx), idx2, 1) = "|" Or idx2 = line_length Then
                    field_no += 1
                    end_field_idx = idx2
                    field = Mid(line(idx), start_field_idx, end_field_idx - start_field_idx)
                    If Microsoft.VisualBasic.Left(field, 1) = Chr(10) Then
                        field = Microsoft.VisualBasic.Right(field, field.Length - 1)
                    End If
                    field = Trim(field)
                    field = remove_chars(field)
                    Select Case field_no
                        Case 1
                            calc_row_count += 1
                            code = field
                            If code <> "N" And code <> "W" And code <> "U" And code <> "C" And code <> "Q" And code <> "R" Then
                                errorfile = errorfile & "Line  " & idx & " - code is not N, U, W, C, Q or R - " _
                                                                         & code & vbNewLine
                            End If
                        Case 2
                            amtstring = field
                            If Not IsNumeric(amtstring) Then
                                errorfile = errorfile & "Line  " & idx & " - offence number not numeric - " _
                                         & amtstring & vbNewLine
                            Else
                                offence_number = amtstring
                            End If
                        Case 3
                            clref = field
                        Case 4
                            amtstring = field
                            If Not IsNumeric(amtstring) Then
                                errorfile = errorfile & "Line  " & idx & " - debt not numeric - " _
                                         & amtstring & vbNewLine
                            Else
                                debt_amt = amtstring

                            End If
                        Case 5
                            If field.Length > 0 Then
                                comments = "BPCourtFlag:" & field & ";"
                                space_comments()
                            End If
                        Case 6
                            If field.Length > 0 Then
                                comments = comments & "CAConfirmedHomeownerDetails:" & field & ";"
                                space_comments()
                            End If
                        Case 7
                            If field.Length > 0 Then
                                comments = comments & "PRMoveIn:" & field & ";"
                                space_comments()
                            End If
                        Case 8
                            If field.Length > 0 Then
                                comments = comments & "PRMoveOut:" & field & ";"
                                space_comments()
                            End If
                        Case 9
                            title = field
                        Case 10
                            forename = field
                        Case 11
                            surname = field
                        Case 12
                            name2 = field
                        Case 13
                            addr1 = field
                        Case 14
                            addr2 = field
                        Case 15
                            addr3 = field
                        Case 16
                            addr4 = field
                        Case 17
                            pcode = field
                        Case 18
                            phone = field
                        Case 19
                            phone2 = field
                        Case 20
                            email = field
                        Case 21
                            debt_addr1 = field
                        Case 22
                            debt_addr2 = field
                        Case 23
                            debt_addr3 = field
                        Case 24
                            debt_addr4 = field
                        Case 25
                            debt_addr5 = field
                        Case 26
                            lostring = field
                            If lostring.Length > 0 Then
                                If Not IsDate(lostring) Then
                                    errorfile = errorfile & "Line  " & idx & " - offence from Date not valid - " _
                                                                 & lostring & vbNewLine
                                Else
                                    offence_from = lostring
                                End If
                            End If
                        Case 27
                            Try
                                requestType = field
                            Catch ex As Exception
                                field = ""
                            End Try
                            If field.Length > 0 Then
                                comments = comments & "CADCARequestType:" & field & ";"
                                space_comments()
                            End If
                        Case 28
                            vacated = False
                            If field.Length > 0 Then
                                comments = comments & "CAVacated:" & field & ";"
                                space_comments()
                                If field = "Y" Then
                                    vacated = True
                                End If
                            End If
                        Case 29
                            If field.Length > 0 Then
                                comments = comments & "CAInvoiceDate:" & field & ";"
                                space_comments()
                            End If
                        Case 30
                            If field.Length > 0 Then
                                comments = comments & "CALastBillAmount:" & field & ";"
                                space_comments()
                            End If
                        Case 31
                            If field.Length > 0 Then
                                comments = comments & "CALastBFAmount:" & field & ";"
                                space_comments()
                            End If
                        Case 32
                            If field.Length > 0 Then
                                comments = comments & "CALastBillVATAmount:" & field & ";"
                                space_comments()
                            End If
                        Case 33
                            lostring = field
                            If lostring.Length > 0 Then
                                If Not IsDate(lostring) Then
                                    errorfile = errorfile & "Line  " & idx & " - offence date Date not valid - " _
                                                                 & lostring & vbNewLine
                                Else
                                    offence_date = lostring
                                End If
                            End If
                        Case 34
                            If field.Length > 0 Then
                                comments = comments & "CTClaimNo:" & field & ";"
                                space_comments()
                            End If
                        Case 35
                            lostring = field
                            If lostring.Length > 0 Then
                                If Not IsDate(lostring) Then
                                    errorfile = errorfile & "Line  " & idx & " - offence court Date not valid - " _
                                                                 & lostring & vbNewLine
                                Else
                                    offence_court = lostring
                                End If
                            End If
                        Case 36
                            lostring = field
                            If lostring.Length > 0 Then
                                If Not IsDate(lostring) Then
                                    errorfile = errorfile & "Line  " & idx & " - offence reg Date not valid - " _
                                                                 & lostring & vbNewLine
                                Else
                                    offence_reg = lostring
                                End If
                            End If
                        Case 37
                            If field.Length > 0 Then
                                comments = comments & "CLaimPeriodStart:" & field & ";"
                                space_comments()
                            End If

                        Case 38
                            If field.Length > 0 Then
                                comments = comments & "CLaimPeriodEnd:" & field & ";"
                                space_comments()
                            End If

                        Case 39
                            If field.Length > 0 Then
                                comments = comments & "CATracedAddress:" & field & ";"
                                space_comments()
                            End If
                        Case 40
                            If field.Length > 0 Then
                                If Not IsDate(field) Then
                                    errorfile = errorfile & "Line  " & idx & " - Date of Birth not valid - " _
                                                                 & field & vbNewLine
                                Else
                                    dob = CDate(field)
                                End If
                            End If
                        Case 41
                            If field.Length > 0 Then
                                comments = comments & "NumberOfFailedArrangements:" & field & ";"
                                space_comments()
                            End If
                        Case 42
                            If field.Length > 0 Then
                                comments = comments & "CAISDefaultIssued:" & field & ";"
                                space_comments()
                            End If
                        Case 43
                            If field.Length > 0 Then
                                comments = comments & "FiledVisitCorrectCustomerContact:" & field & ";"
                                space_comments()
                            End If
                        Case 44
                            If field.Length > 0 Then
                                comments = comments & "MeasuredorUnmeasured:" & field & ";"
                                space_comments()
                            End If
                        Case 45
                            If field.Length > 0 Then
                                comments = comments & "MeterReading:" & field & ";"
                                space_comments()
                            End If
                        Case 46
                            If field.Length > 0 Then
                                comments = comments & "TypeofMeterReading:" & field & ";"
                                space_comments()
                            End If
                        Case 47
                            If field.Length > 0 Then
                                If Not IsDate(field) Then
                                    errorfile = errorfile & "Line  " & idx & " - Date of Meter reading not valid - " _
                                              & field & vbNewLine
                                Else
                                    comments = comments & "DateofMeterReading:" & Format(CDate(field), "dd/MM/yyyy") & ";"
                                    space_comments()
                                End If
                            End If
                        Case 48
                            CSID = 0
                            If field.Length > 0 Then
                                'comments = comments & "LandlordIndicator:" & field & ";"
                                comments = comments & "AccountIndicator:"
                                Select Case field
                                    Case "Y"
                                        comments = comments & "Domestic Landlord;"
                                    Case "N"
                                        comments = comments & "Domestic;"
                                    Case "L"
                                        comments = comments & "Commercial Landlord;"
                                    Case "C"
                                        comments = comments & "Commercial;"
                                    Case Else
                                        comments = comments & "Unknown;"
                                End Select
                                space_comments()
                                'set CSID
                                If code = "N" Then
                                    Select Case requestType
                                        Case 1
                                            Select Case field
                                                Case "Y"
                                                    If vacated Then
                                                        If debt_amt < 100 Then
                                                            CSID = 4489
                                                        Else
                                                            CSID = 4481
                                                        End If
                                                    Else
                                                        If debt_amt < 100 Then
                                                            CSID = 4491
                                                        Else
                                                            CSID = 4482
                                                        End If
                                                    End If
                                                Case "N"
                                                    If vacated Then
                                                        If debt_amt < 100 Then
                                                            CSID = 4489
                                                        Else
                                                            CSID = 4481
                                                        End If
                                                    Else
                                                        If debt_amt < 100 Then
                                                            CSID = 4491
                                                        Else
                                                            CSID = 4482
                                                        End If
                                                    End If
                                                Case "L"
                                                    If vacated Then
                                                        If debt_amt < 100 Then
                                                            CSID = 4490
                                                        End If
                                                    Else
                                                        If debt_amt < 100 Then
                                                            CSID = 4492
                                                        End If
                                                    End If
                                                Case "C"
                                                    If vacated Then
                                                        If debt_amt < 100 Then
                                                            CSID = 4490
                                                        End If
                                                    Else
                                                        If debt_amt < 100 Then
                                                            CSID = 4492
                                                        End If
                                                    End If
                                            End Select
                                        Case 2
                                            If field = "Y" Or field = "N" Then
                                                CSID = 4483
                                            Else
                                                If field = "L" Or field = "C" Then
                                                    CSID = 4484
                                                End If
                                            End If
                                        Case 3
                                            Select Case field
                                                Case "Y"
                                                    If vacated Then
                                                        CSID = 4487
                                                    Else
                                                        CSID = 4486
                                                    End If
                                                Case "N"
                                                    If vacated Then
                                                        CSID = 4487
                                                    Else
                                                        CSID = 4486
                                                    End If
                                                Case "L"
                                                    If vacated Then
                                                        CSID = 4485
                                                    Else
                                                        CSID = 4496
                                                    End If
                                                Case "C"
                                                    If vacated Then
                                                        CSID = 4485
                                                    Else
                                                        CSID = 4496
                                                    End If
                                            End Select
                                        Case 4
                                            If field = "Y" Or field = "N" Then
                                                CSID = 4480
                                            Else
                                                If field = "L" Or field = "C" Then
                                                    CSID = 4479
                                                End If
                                            End If
                                        Case 5
                                            If field = "Y" Or field = "N" Then
                                                CSID = 4493
                                            End If
                                        Case 7
                                            If field = "Y" Or field = "N" Then
                                                CSID = 4495
                                            Else
                                                If field = "L" Or field = "C" Then
                                                    CSID = 4494
                                                End If
                                            End If
                                    End Select
                                End If
                            End If
                        Case 49
                            If field.Length > 0 Then
                                comments = comments & "WithdrawalReason:" & field & ";"
                                space_comments()
                            End If
                        Case 50
                            If field.Length > 0 Then
                                comments = comments & "PaymentsSinceReferral:" & field & ";"
                                space_comments()
                            End If
                            If code = "U" And debt_amt <> Nothing Then
                                'get debt amount from onestep
                                '18.11.2014 use debt balance
                                param2 = "select D._rowid,CS.clientID " & _
                                " from Debtor D, clientScheme CS" & _
                                " where client_ref = '" & clref & "'" & _
                                " and status_open_closed = 'O'" & _
                                " and CS._rowID = D.clientschemeID " & _
                                " and CS.clientID = 1245"
                                Dim debt_ds As DataSet = get_dataset("onestep", param2)
                                Dim debtIDX As Integer
                                For debtIDX = 0 To debt_ds.Tables(0).Rows.Count - 1
                                    Dim debtorID As Integer = debt_ds.Tables(0).Rows(debtIDX).Item(0)
                                    Dim client_debtOS As Decimal = 0
                                    param2 = "select fee_amount, remited_fee from Fee " & _
                                    " where debtorID = " & debtorID & _
                                    " and fee_remit_col < 3"
                                    Dim fee_ds As DataSet = get_dataset("onestep", param2)
                                    Dim feeRow As DataRow
                                    For Each feeRow In fee_ds.Tables(0).Rows
                                        client_debtOS = client_debtOS + feeRow(0) - feeRow(1)
                                    Next
                                    If debt_amt <> client_debtOS Then
                                        Dim balORPay As String = "Balance"
                                        If field.Length > 0 Then
                                            Dim testAmt As Decimal
                                            Try
                                                testAmt = field
                                                If field > 0 Then
                                                    balORPay = "Payment"
                                                End If
                                            Catch ex As Exception

                                            End Try
                                        End If
                                        outfile_upd2 &= balORPay & "," & debtorID & "," & clref & "," & debt_amt & "," & client_debtOS & vbNewLine
                                    End If
                                Next
                            End If
                        Case 51
                            '18.11.2014 changed from Y/N to amount
                            'If field.Length > 0 Then
                            '    comments = comments & "AdminFeeLetterSent:" & field & ";"
                            '    space_comments()
                            'End If
                            If field.Length > 0 Then
                                comments = comments & "LatePaymentCharge:" & field & ";"
                                space_comments()
                                latepaymentCharge = field
                            End If

                        Case 52
                            If field.Length > 0 Then
                                comments = comments & "DCA.DCAID:" & field & ";"
                                space_comments()
                            End If
                    End Select
                    start_field_idx = idx2 + 1
                End If
            Next
            If idx = 0 Then
                Continue For
            End If
            If line(idx).Length < 10 Then
                Continue For
            End If
            'validate case details
            If clref = Nothing Then
                errorfile = errorfile & "Line  " & idx & " - No client reference" & vbNewLine
            End If
            Dim sep As String = "|"
            '17.12.2014 do not include late payment charge in balances
            If code = "N" Then
                Select Case CSID
                    Case 4482
                        sep = sep4482
                        cnt4482 += 1
                        bal4482 += debt_amt ' + latepaymentCharge
                    Case 4481
                        sep = sep4481
                        cnt4481 += 1
                        bal4481 += debt_amt '+ latepaymentCharge
                    Case 4483
                        sep = sep4483
                        cnt4483 += 1
                        bal4483 += debt_amt '+ latepaymentCharge
                    Case 4486
                        sep = sep4486
                        cnt4486 += 1
                        bal4486 += debt_amt '+ latepaymentCharge
                    Case 4487
                        sep = sep4487
                        cnt4487 += 1
                        bal4487 += debt_amt '+ latepaymentCharge
                    Case 4484
                        sep = sep4484
                        cnt4484 += 1
                        bal4484 += debt_amt '+ latepaymentCharge
                    Case 4496
                        sep = sep4496
                        cnt4496 += 1
                        bal4496 += debt_amt '+ latepaymentCharge
                    Case 4485
                        sep = sep4485
                        cnt4485 += 1
                        bal4485 += debt_amt '+ latepaymentCharge
                    Case 4491
                        sep = sep4491
                        cnt4491 += 1
                        bal4491 += debt_amt '+ latepaymentCharge
                    Case 4489
                        sep = sep4489
                        cnt4489 += 1
                        bal4489 += debt_amt '+ latepaymentCharge
                    Case 4492
                        sep = sep4492
                        cnt4492 += 1
                        bal4492 += debt_amt '+ latepaymentCharge
                    Case 4490
                        sep = sep4490
                        cnt4490 += 1
                        bal4490 += debt_amt '+ latepaymentCharge
                    Case 4480
                        sep = sep4480
                        cnt4480 += 1
                        bal4480 += debt_amt '+ latepaymentCharge
                    Case 4479
                        sep = sep4479
                        cnt4479 += 1
                        bal4479 += debt_amt '+ latepaymentCharge
                    Case 4493
                        sep = sep4493
                        cnt4493 += 1
                        bal4493 += debt_amt '+ latepaymentCharge
                    Case 4495
                        sep = sep4495
                        cnt4495 += 1
                        bal4495 += debt_amt '+ latepaymentCharge
                    Case 4494
                        sep = sep4494
                        cnt4494 += 1
                        bal4494 += debt_amt '+ latepaymentCharge
                End Select
            End If

            'save case in outline
            Dim outrec As String = clref & sep & offence_number & sep & debt_amt & sep & _
                title & sep & forename & sep & surname & sep & name2 & sep & addr1 & sep & _
                addr2 & sep & addr3 & sep & addr4 & sep & pcode & sep & phone & sep & _
                phone2 & sep & email & sep & debt_addr1 & sep & debt_addr2 & sep & debt_addr3 & sep & _
                debt_addr4 & sep & debt_addr5 & sep

            If offence_court = Nothing Then
                outrec = outrec & sep
            Else
                outrec = outrec & offence_court & sep
            End If
            If offence_reg = Nothing Then
                outrec = outrec & sep
            Else
                outrec = outrec & offence_reg & sep
            End If
            If offence_from = Nothing Then
                outrec = outrec & sep
            Else
                outrec = outrec & offence_from & sep
            End If
            If offence_date = Nothing Then
                outrec = outrec & sep
            Else
                outrec = outrec & offence_date & sep
            End If
            If dob = Nothing Then
                outrec = outrec & sep
            Else
                outrec = outrec & Format(dob, "dd/MM/yyyy") & sep
            End If
            outrec &= latepaymentCharge & sep
            Dim comments2 As String = ""
            Dim comments3 As String = comments
            'If comments.Length <= 250 Then
            outrec = outrec & comments
            'Else
            '    While comments3.Length > 250
            '        Dim len As Integer = Microsoft.VisualBasic.Len(comments3)
            '        Dim idx3 As Integer
            '        For idx3 = 250 To 1 Step -1
            '            If Mid(comments3, idx3, 1) = ";" Then
            '                Exit For
            '            End If
            '        Next
            '        comments2 = comments2 & Microsoft.VisualBasic.Left(comments3, idx3)
            '        For idx3 = idx3 To 250
            '            comments2 = comments2 & " "
            '        Next
            '        comments3 = Microsoft.VisualBasic.Right(comments3, len - idx3)
            '    End While
            '    outrec = outrec & comments2 & comments3
            'End If

            Select Case code
                Case "N"
                    newCases += 1
                    Select Case CSID
                        Case 0
                            MsgBox("No CSID found for clref = " & clref & " on line" & idx + 1)
                        Case 4482
                            outfile_4482 &= outrec & vbNewLine
                        Case 4481
                            outfile_4481 &= outrec & vbNewLine
                        Case 4483
                            outfile_4483 &= outrec & vbNewLine
                        Case 4486
                            outfile_4486 &= outrec & vbNewLine
                        Case 4487
                            outfile_4487 &= outrec & vbNewLine
                        Case 4484
                            outfile_4484 &= outrec & vbNewLine
                        Case 4496
                            outfile_4496 &= outrec & vbNewLine
                        Case 4485
                            outfile_4485 &= outrec & vbNewLine
                        Case 4491
                            outfile_4491 &= outrec & vbNewLine
                        Case 4489
                            outfile_4489 &= outrec & vbNewLine
                        Case 4492
                            outfile_4492 &= outrec & vbNewLine
                        Case 4490
                            outfile_4490 &= outrec & vbNewLine
                        Case 4480
                            outfile_4480 &= outrec & vbNewLine
                        Case 4479
                            outfile_4479 &= outrec & vbNewLine
                        Case 4493
                            outfile_4493 &= outrec & vbNewLine
                        Case 4495
                            outfile_4495 &= outrec & vbNewLine
                        Case 4494
                            outfile_4494 &= outrec & vbNewLine
                    End Select
                Case "U"
                    outfile_upd &= outrec & vbNewLine
                    updateCases += 1
                Case "W"
                    outfile_withdraw &= outrec & vbNewLine
                    withdrawnCases += 1
                Case "Q"
                    outfile_query &= outrec & vbNewLine
                    queryCases += 1
                Case "R"
                    outfile_reissue &= outrec & vbNewLine
                    reissueCases += 1
                Case "C"
                    outfile_continue &= outrec & vbNewLine
                    continueCases += 1
            End Select


            'clear fields
            name2 = ""
            title = ""
            forename = ""
            surname = ""
            addr1 = ""
            addr2 = ""
            addr3 = ""
            addr4 = ""
            phone = ""
            phone2 = ""
            email = ""
            pcode = ""
            debt_addr1 = ""
            debt_addr2 = ""
            debt_addr3 = ""
            debt_addr4 = ""
            debt_addr5 = ""
            comments = ""
            offence_court = Nothing
            offence_from = Nothing
            offence_date = Nothing
            clref = ""
            debt_amt = Nothing
            offence_number = Nothing
            offence_reg = Nothing
            dob = Nothing
            latepaymentCharge = 0
        Next

        viewbtn.Enabled = True
        Dim filename_prefix As String = ""
        Dim idx4 As Integer
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_update.txt", outfile_upd, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_update2.csv", outfile_upd2, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_withdraw.txt", outfile_withdraw, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_query.txt", outfile_query, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_reissue.txt", outfile_reissue, False)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_continue.txt", outfile_continue, False)
        Dim msgline As String = ""
        If cnt4479 > 0 Then
            msgline &= "CSID 4479 cases = " & cnt4479 & vbNewLine
            auditFile &= "Clientscheme: 4479" & vbNewLine & _
            "Number of new cases: " & cnt4479 & vbNewLine & _
            "Total balance or new cases: " & bal4479 & vbNewLine & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4479.txt", outfile_4479, False)
        End If

        If cnt4480 > 0 Then
            auditFile &= "Clientscheme: 4480" & vbNewLine & _
                        "Number of new cases: " & cnt4480 & vbNewLine & _
                        "Total balance or new cases: " & bal4480 & vbNewLine & vbNewLine
            msgline &= "CSID 4480 cases = " & cnt4480 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4480.txt", outfile_4480, False)
        End If
        If cnt4481 > 0 Then
            auditFile &= "Clientscheme: 4481" & vbNewLine & _
                        "Number of new cases: " & cnt4481 & vbNewLine & _
                        "Total balance or new cases: " & bal4481 & vbNewLine & vbNewLine
            msgline &= "CSID 4481 cases = " & cnt4481 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4481.txt", outfile_4481, False)
        End If
        If cnt4482 > 0 Then
            auditFile &= "Clientscheme: 4482" & vbNewLine & _
                        "Number of new cases: " & cnt4482 & vbNewLine & _
                        "Total balance or new cases: " & bal4482 & vbNewLine & vbNewLine
            msgline &= "CSID 4482 cases = " & cnt4482 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4482.txt", outfile_4482, False)
        End If
        If cnt4483 > 0 Then
            auditFile &= "Clientscheme: 4483" & vbNewLine & _
                        "Number of new cases: " & cnt4483 & vbNewLine & _
                        "Total balance or new cases: " & bal4483 & vbNewLine & vbNewLine
            msgline &= "CSID 4483 cases = " & cnt4483 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4483.txt", outfile_4483, False)
        End If
        If cnt4484 > 0 Then
            auditFile &= "Clientscheme: 4484" & vbNewLine & _
                        "Number of new cases: " & cnt4484 & vbNewLine & _
                        "Total balance or new cases: " & bal4484 & vbNewLine & vbNewLine
            msgline &= "CSID 4484 cases = " & cnt4484 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4484.txt", outfile_4484, False)
        End If
        If cnt4485 > 0 Then
            auditFile &= "Clientscheme: 4485" & vbNewLine & _
                        "Number of new cases: " & cnt4485 & vbNewLine & _
                        "Total balance or new cases: " & bal4485 & vbNewLine & vbNewLine
            msgline &= "CSID 4485 cases = " & cnt4485 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4485.txt", outfile_4485, False)
        End If
        If cnt4486 > 0 Then
            auditFile &= "Clientscheme: 4486" & vbNewLine & _
                        "Number of new cases: " & cnt4486 & vbNewLine & _
                        "Total balance or new cases: " & bal4486 & vbNewLine & vbNewLine
            msgline &= "CSID 4486 cases = " & cnt4486 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4486.txt", outfile_4486, False)
        End If
        If cnt4487 > 0 Then
            auditFile &= "Clientscheme: 4487" & vbNewLine & _
                        "Number of new cases: " & cnt4487 & vbNewLine & _
                        "Total balance or new cases: " & bal4487 & vbNewLine & vbNewLine
            msgline &= "CSID 4487 cases = " & cnt4487 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4487.txt", outfile_4487, False)
        End If
        If cnt4489 > 0 Then
            auditFile &= "Clientscheme: 4489" & vbNewLine & _
                        "Number of new cases: " & cnt4489 & vbNewLine & _
                        "Total balance or new cases: " & bal4489 & vbNewLine & vbNewLine
            msgline &= "CSID 4489 cases = " & cnt4489 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4489.txt", outfile_4489, False)
        End If
        If cnt4490 > 0 Then
            auditFile &= "Clientscheme: 4490" & vbNewLine & _
                        "Number of new cases: " & cnt4490 & vbNewLine & _
                        "Total balance or new cases: " & bal4490 & vbNewLine & vbNewLine
            msgline &= "CSID 4490 cases = " & cnt4490 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4490.txt", outfile_4490, False)
        End If
        If cnt4491 > 0 Then
            auditFile &= "Clientscheme: 4491" & vbNewLine & _
                        "Number of new cases: " & cnt4491 & vbNewLine & _
                        "Total balance or new cases: " & bal4491 & vbNewLine & vbNewLine
            msgline &= "CSID 4491 cases = " & cnt4491 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4491.txt", outfile_4491, False)
        End If
        If cnt4492 > 0 Then
            auditFile &= "Clientscheme: 4492" & vbNewLine & _
                        "Number of new cases: " & cnt4492 & vbNewLine & _
                        "Total balance or new cases: " & bal4492 & vbNewLine & vbNewLine
            msgline &= "CSID 4492 cases = " & cnt4492 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4492.txt", outfile_4492, False)
        End If
        If cnt4493 > 0 Then
            auditFile &= "Clientscheme: 4493" & vbNewLine & _
                        "Number of new cases: " & cnt4493 & vbNewLine & _
                        "Total balance or new cases: " & bal4493 & vbNewLine & vbNewLine
            msgline &= "CSID 4493 cases = " & cnt4493 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4493.txt", outfile_4493, False)
        End If
        If cnt4494 > 0 Then
            auditFile &= "Clientscheme: 4494" & vbNewLine & _
                        "Number of new cases: " & cnt4494 & vbNewLine & _
                        "Total balance or new cases: " & bal4494 & vbNewLine & vbNewLine
            msgline &= "CSID 4494 cases = " & cnt4494 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4494.txt", outfile_4494, False)
        End If
        If cnt4495 > 0 Then
            auditFile &= "Clientscheme: 4495" & vbNewLine & _
                        "Number of new cases: " & cnt4495 & vbNewLine & _
                        "Total balance or new cases: " & bal4495 & vbNewLine & vbNewLine
            msgline &= "CSID 4495 cases = " & cnt4495 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4495.txt", outfile_4495, False)
        End If
        If cnt4496 > 0 Then
            auditFile &= "Clientscheme: 4496" & vbNewLine & _
                        "Number of new cases: " & cnt4496 & vbNewLine & _
                        "Total balance or new cases: " & bal4496 & vbNewLine & vbNewLine
            msgline &= "CSID 4496 cases = " & cnt4496 & vbNewLine
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess_4496.txt", outfile_4496, False)
        End If
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_audit.txt", auditFile, False)
        msgline &= "Total cases with CSID = " & cnt4479 + cnt4480 + cnt4481 + _
        cnt4482 + cnt4483 + cnt4484 + cnt4485 + cnt4486 + cnt4487 + cnt4489 + _
        cnt4490 + cnt4491 + cnt4492 + cnt4493 + cnt4494 + cnt4495 + cnt4496 & vbNewLine

        msgline &= "New cases = " & newCases & vbNewLine & vbNewLine
       
        msgline &= "Withdrawn cases = " & withdrawnCases & vbNewLine
        auditFile &= "Withdrawn caes: " & withdrawnCases & vbNewLine
        msgline &= "Update cases = " & updateCases & vbNewLine
        auditFile &= "Update cases: " & updateCases & vbNewLine
        msgline &= "Query cases = " & queryCases & vbNewLine
        auditFile &= "Query cases: " & queryCases & vbNewLine
        msgline &= "Reissue cases = " & reissueCases & vbNewLine
        auditFile &= "Reissue cases: " & reissueCases & vbNewLine
        msgline &= "Continue cases = " & continueCases & vbNewLine
        auditFile &= "Continue cases: " & continueCases & vbNewLine
        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText("error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
            MsgBox(msgline)
            Me.Close()
        End If
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
    Private Function remove_chars(ByVal field As String) As String
        field = Replace(field, "&", " ")
        field = Replace(field, "^", " ")
        field = Replace(field, "+", " ")
        field = Trim(field)
        Return field

    End Function
    Private Sub space_comments()
        Dim spaces As Integer
        Try

            spaces = (Int((comments.Length / 250)) + 1) * 250
            comments = comments & Space(spaces - comments.Length)
        Catch ex As Exception
            MsgBox("error in spacing comments")
        End Try

    End Sub
End Class

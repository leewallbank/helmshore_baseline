Public Class mainform

    Dim ascii As New System.Text.ASCIIEncoding()

    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline, check_line As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim debt_addr As String = ""
        Dim curraddr As String = ""
        Dim name As String = ""
        Dim ta_name As String = ""
        Dim summons_num As String = ""
        Dim lostring As String = ""
        Dim idx As Integer
        Dim lines As Integer = 0
        Dim debt_amt, war_amt As Decimal
        Dim lodate, fromdate, todate As Date
        Dim clref As String = ""
        Dim addrline As String = ""
        Dim amtstring As String = ""
        Dim tot_cases As Integer = 0
        Dim tot_debt As Decimal = 0
        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "Client ref|Name|TA Name|DebtAddress|Current Address|Summons No|LO Date|Warrant Amt|From date|To date|Debt Amount" & vbNewLine
        outfile = outline
        'look for BAILXX
        Dim start_idx As Integer
        For idx = 0 To lines - 1
            check_line = remove_chars(line(idx))
            If check_line.Length = 0 Then
                Continue For
            End If
            If InStr(check_line, "BID7DL") > 0 Then
                'start of new case
                clref = Nothing
                name = ""
                ta_name = ""
                debt_addr = ""
                curraddr = ""
                summons_num = ""
                lodate = Nothing
                debt_amt = Nothing
                war_amt = Nothing
                fromdate = Nothing
                todate = Nothing
                Continue For
            End If

            start_idx = InStr(check_line, "Account No:")
            If start_idx > 0 Then
                clref = Trim(Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 11))
                tot_cases += 1
                Continue For
            End If

            start_idx = InStr(check_line, "Case No:")
            If start_idx > 0 Then
                summons_num = Trim(Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 8))
                Continue For
            End If

            start_idx = InStr(check_line, "Re:")
            If start_idx > 0 Then
                name = Trim(Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 3))
                start_idx = InStr(name, "T/A")
                If start_idx > 0 Then
                    ta_name = Trim(Microsoft.VisualBasic.Right(name, name.Length - start_idx - 3))
                    name = Trim(Microsoft.VisualBasic.Left(name, start_idx))
                End If
                'next line might be T/A or start of adddress
                idx += 1
                check_line = remove_chars(line(idx))
                start_idx = InStr(check_line, "T/A")
                If start_idx > 0 Then
                    ta_name = Trim(Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 3))
                    idx += 1
                    check_line = remove_chars(line(idx))
                End If
                'look for current address lines - stop when "A liability" is found
                start_idx = 0
                While start_idx = 0
                    If check_line.Length > 1 Then
                        If curraddr <> Nothing Then
                            curraddr = curraddr & ","
                        End If
                        curraddr = curraddr & check_line
                    End If
                    idx += 1
                    check_line = remove_chars(line(idx))
                    start_idx = InStr(LCase(check_line), "a liability")
                End While
            End If

            start_idx = InStr(check_line, "COURT on")
            If start_idx > 0 Then
                lostring = Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 8)
                Try
                    lodate = CDate(lostring)
                Catch ex As Exception
                    errorfile = errorfile & "Line  " & idx & " - LO date is invalid" & vbNewLine
                End Try

            End If

            check_line = remove_chars(line(idx))
            start_idx = InStr(check_line, "currently outstanding")
            If start_idx > 0 Then
                'get debt amount
                Try
                    debt_amt = Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 40)
                    tot_debt += debt_amt
                Catch ex As Exception
                    errorfile = errorfile & "Line  " & idx & " - Debt amount is invalid" & vbNewLine
                End Try
            End If

            start_idx = InStr(check_line, "Re Property:")
            If start_idx > 0 Then
                'get debt address lines - stop when For The Period 
                idx += 1
                check_line = remove_chars(line(idx))
                start_idx = 0
                While start_idx = 0
                    If check_line.Length > 1 Then
                        If debt_addr <> Nothing Then
                            debt_addr = debt_addr & ","
                        End If
                        debt_addr = debt_addr & check_line
                    End If
                    idx += 1
                    check_line = remove_chars(line(idx))
                    start_idx = InStr(check_line, "For The Period ")
                End While
                'look for period from and to dates - stop when found or current balance found
                idx += 1
                check_line = remove_chars(line(idx))
                start_idx = 0
                While start_idx = 0
                    idx += 1
                    check_line = remove_chars(line(idx))
                    start_idx = InStr(check_line, "to")
                End While
                lostring = Trim(Microsoft.VisualBasic.Left(check_line, start_idx - 1))
                Try
                    fromdate = CDate(lostring)
                Catch ex As Exception
                    errorfile = errorfile & "Line  " & idx & " - Offence from date is invalid" & vbNewLine
                    Continue For
                End Try
                lostring = Trim(Mid(check_line, start_idx + 2, 12))
                Try
                    todate = CDate(lostring)
                Catch ex As Exception
                    errorfile = errorfile & "Line  " & idx & " - Offence to date is invalid" & vbNewLine
                    Continue For
                End Try
                Try
                    war_amt = Microsoft.VisualBasic.Right(check_line, check_line.Length - start_idx - 20)
                Catch ex As Exception

                End Try
                'validate case details
                If clref = Nothing Or Microsoft.VisualBasic.Left(clref, 3) = "   " Then
                    errorfile = errorfile & "Line  " & idx & " - No client reference" & vbNewLine
                    Continue For
                End If

                'save case in outline
                outfile = outfile & clref & "|" & name & "|" & ta_name & "|" & debt_addr & "|" _
                 & curraddr & "|" & summons_num & "|" & lodate & "|" & war_amt & _
                "|" & fromdate & "|" & todate & "|" & debt_amt & vbNewLine
            End If
        Next

        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False, ascii)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            MsgBox("Total cases " & tot_cases & vbNewLine & _
                   "Total debt  " & tot_debt)
            Me.Close()
        End If
    End Sub

    Private Function remove_chars(ByVal line As String) As String
        Dim idx As Integer
        Dim new_line As String = ""
        For idx = 1 To line.Length
            If Mid(line, idx, 1) = Chr(10) Or Mid(line, idx, 1) = Chr(13) Then
                new_line = new_line & " "
            Else
                new_line = new_line & Mid(line, idx, 1)
            End If
        Next
        Return Trim((new_line))

    End Function

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
End Class

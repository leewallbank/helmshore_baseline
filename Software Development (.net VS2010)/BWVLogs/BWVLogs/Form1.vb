﻿Imports System.IO
Imports CommonLibrary
Imports System.Net

Public Class Form1
    Dim filePath, filepath2 As String
    Dim filename As String = ""
    Dim documentname As String = ""
    Dim foldername As String = ""
    Dim folderpath As String
    Dim startDate, endDate, fileDate, forty_days_ago As Date
    Dim infoReader As System.IO.FileInfo
    Dim fileSize As Long
    Dim fileDateStr As String
    Dim futureEndDate As Date = CDate("Jan 1, 2100 00:00:00")
    Dim upd_txt As String

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'futureEndDate = CDate("Mar 1, 2014 00:00:00")
        'startDate = CDate("Mar 1, 2014 00:00:00")
        'upd_txt = "insert into CameraBailiffXREF (cam_camera_no, cam_bailiffID, cam_start_date, cam_end_date) " & _
        '                                " values ('dev00c0d4205416',2618,'" & startDate & "','" & futureEndDate & "')"
        'upd_txt = "update CameraBailiffXref set cam_start_date = ' " & futureEndDate & "'" & _
        '                      " where cam_seq_no = 19"
        'upd_txt = "delete from CameraLogFile where log_bailiffID = 22"
        'update_sql(upd_txt)
        'upd_txt = "delete from CameraBailiffXREF where cam_seq_no = 22"
        'update_sql(upd_txt)
        'upd_txt = "update CameraBailiffXref set cam_start_date = ' " & futureEndDate & "'" & _
        '                     " where cam_seq_no = 691"
        'update_sql(upd_txt)


        upd_txt = "delete from CameraLogFile where log_bailiffID>=0"
        update_sql(upd_txt)


        forty_days_ago = DateAdd(DateInterval.Day, -40, Now)
        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("BodyCamera")

        filePath = "\\192.0.30.40\ftp-rossendales\footage\archive\"
        'get all folders starting with dev
        For Each foundFolder As String In My.Computer.FileSystem.GetDirectories _
               (filePath, FileIO.SearchOption.SearchTopLevelOnly, "dev*")
            folderpath = foundFolder
            Dim cameraNumber As String = Path.GetFileName(folderpath)

            If cameraNumber = "dev00c0d4205063" Then 'ignore as Marston bailiff
                Continue For
            End If

            If cameraNumber = "dev00c0d4205418" Then 'ignore as Marston TMA Agent
                Continue For
            End If

            'TEST()
            'If cameraNumber <> "dev00c0d4211144" Then 'test
            '    Continue For
            'End If

            filepath2 = folderpath & "\archive\"
            Dim firstDocument As Boolean = True
            Dim bailiffID As Integer
            Try

                For Each foundfile As String In My.Computer.FileSystem.GetFiles _
                    (filepath2, FileIO.SearchOption.SearchTopLevelOnly, "*.pss")
                    documentname = Path.GetFileName(foundfile)
                    fileDateStr = Mid(documentname, 5, 8)
                    Dim month As String = MonthName(Mid(fileDateStr, 5, 2), False)
                    fileDate = CDate(Microsoft.VisualBasic.Right(fileDateStr, 2) & "/" & month & _
                        "/" & Microsoft.VisualBasic.Left(fileDateStr, 4) & " " & Mid(documentname, 14, 2) & ":" & Mid(documentname, 16, 2) & ":" & Mid(documentname, 18, 2))
                    'only need to look at filedate from last 14 days
                    If Format(fileDate, "yyyy-MM-dd") < Format(forty_days_ago, "yyyy-MM-dd") Then
                        Continue For
                    End If
                    infoReader = My.Computer.FileSystem.GetFileInfo(foundfile)
                    Try
                        fileSize = infoReader.Length
                    Catch ex As Exception
                        Continue For
                    End Try
                    fileSize = infoReader.Length
                    ' Dim fileTime As Integer = Format(infoReader.CreationTime, "HH")
                    Dim filetime As Integer
                    Try
                        filetime = Mid(documentname, 14, 2)
                    Catch ex As Exception
                        filetime = 12
                    End Try
                    If filetime < 6 Then
                        fileDate = DateAdd("d", -1, fileDate)
                    End If
                    If firstDocument Then
                        firstDocument = False
                        Dim camBailiffID, camSeqNo As Integer
                        'check bailiffID on onestep
                        Try
                            bailiffID = GetSQLResults2("DebtRecovery", "SELECT _rowID " & _
                                                              "FROM Bailiff  " & _
                                                              "WHERE add_fax = '" & cameraNumber & "'" & _
                                                              " and (status = 'O' or _rowID = 2714)")
                        Catch ex As Exception
                            bailiffID = 0
                        End Try
                        'check open entry on xref table for camera
                        Dim xref2_dt As New DataTable
                        LoadDataTable("BodyCamera", "SELECT cam_seq_no, cam_bailiffID, cam_start_date " & _
                                                        " FROM CameraBailiffXref " & _
                                                        " WHERE cam_camera_no = '" & cameraNumber & "'" & _
                                                        " and cam_end_date = '" & futureEndDate & "'", xref2_dt, False)
                        If xref2_dt.Rows.Count = 0 Then
                            'No open entry on XREF
                            'look for any closed entry 
                            Dim xref3_dt As New DataTable
                            LoadDataTable("BodyCamera", "SELECT cam_seq_no, cam_bailiffID, cam_start_date, cam_end_date " & _
                                                            " FROM CameraBailiffXref " & _
                                                            " WHERE cam_camera_no = '" & cameraNumber & "'", xref3_dt, False)
                            If xref3_dt.Rows.Count = 0 Then
                                'no closed entry on XREF either!
                                If bailiffID = 0 Then
                                    ' MsgBox("Camera " & cameraNumber & " is not on XREF or onestep")
                                Else
                                    'no entries at all for camera so add one
                                    startDate = Now
                                    upd_txt = "insert into CameraBailiffXREF (cam_camera_no, cam_bailiffID, cam_start_date, cam_end_date) " & _
                                        " values ('" & cameraNumber & "'," & bailiffID & ",'" & Format(startDate, "yyyy-MM-dd") & "','" & Format(futureEndDate, "yyyy-MM-dd") & "')"
                                    update_sql(upd_txt)
                                End If
                            ElseIf xref3_dt.Rows.Count = 1 Then
                                'one closed entry found on xref
                                camBailiffID = xref3_dt.Rows(0).Item(1)
                                camSeqNo = xref3_dt.Rows(0).Item(0)
                                If camBailiffID = bailiffID Then
                                    'same bailiffs so can re-open XREF entry
                                    upd_txt = "update CameraBailiffXref set cam_end_date = '" & Format(futureEndDate, "yyyy-MM-dd") & "'" & _
                                   " where cam_seq_no = " & camSeqNo
                                    update_sql(upd_txt)
                                Else
                                    'one closed entry - different bailiff
                                    If bailiffID > 0 Then
                                        'can start new entry in XREF
                                        startDate = Now
                                        upd_txt = "insert into CameraBailiffXREF (cam_camera_no, cam_bailiffID, cam_start_date, cam_end_date) " & _
                                            " values ('" & cameraNumber & "'," & bailiffID & ",'" & Format(startDate, "yyyy-MM-dd") & "','" & Format(futureEndDate, "yyyy-MM-dd") & "')"
                                        update_sql(upd_txt)
                                    Else
                                        'no entry on onestep
                                        endDate = xref3_dt.Rows(0).Item(3)
                                        If endDate < fileDate Then
                                            'bailiff had this camera before file date
                                            'MsgBox("camera " & cameraNumber & " at date " & fileDate & " - unknown bailiff")
                                        End If
                                    End If
                                End If
                            Else
                                'multiple closed entries
                                'see if one of them was active at filedate
                                For Each row In xref3_dt.Rows
                                    startDate = row(2)
                                    endDate = row(3)
                                    If fileDate >= startDate And
                                        fileDate <= endDate Then
                                        'OK found bailiff
                                        bailiffID = row(1)
                                    Else
                                        'baliff not found
                                        'MsgBox("camera " & cameraNumber & " at date " & fileDate & " - unknown bailiff")
                                    End If
                                Next
                            End If
                        ElseIf xref2_dt.Rows.Count > 1 Then
                            'MsgBox("Camera " & cameraNumber & " has 2 open entries in XREF")
                        Else
                            'one open entry found
                            camBailiffID = xref2_dt.Rows(0).Item(1)
                            If camBailiffID <> bailiffID Then
                                camSeqNo = xref2_dt.Rows(0).Item(0)
                                'camera number must have gone to new person 
                                'close existing entrry
                                upd_txt = "update CameraBailiffXref set cam_end_date = '" & Format(Now, "yyyy-MM-dd") & "'" & _
                                    " where cam_seq_no = " & camSeqNo
                                update_sql(upd_txt)
                                'add new one for new bailiff
                                startDate = Now
                                upd_txt = "insert into CameraBailiffXREF (cam_camera_no, cam_bailiffID, cam_start_date,cam_end_date) " & _
                                    " values ('" & cameraNumber & "'," & bailiffID & ",'" & Format(startDate, "yyyy-MM-dd") & "','" & Format(futureEndDate, "yyyy-MM-dd") & "')"
                                update_sql(upd_txt)
                            End If
                        End If
                    End If
                    If bailiffID = 0 Then
                        Exit For
                    End If
                    'If bailiffID <> 1361 Then
                    '    Exit For
                    'End If
                    'add details to LOG table if not already there
                    Dim testSeqNo As Integer
                    Try
                        testSeqNo = GetSQLResults("BodyCamera", "SELECT log_seq_no " & _
                                                      "FROM CameraLogFile  " & _
                                                      "WHERE log_camera_no = '" & cameraNumber & "'" & _
                                                      " AND log_file_name = '" & documentname & "'")
                    Catch ex As Exception
                        testSeqNo = 0
                    End Try
                    If testSeqNo = 0 Then
                        upd_txt = "insert into CameraLogFile (log_camera_no, log_bailiffID, log_file_name, log_file_size, log_file_path, log_file_date) " & _
                            " values ('" & cameraNumber & "'," & bailiffID & ",'" & documentname & "'," & fileSize & ",'" & _
                            foundfile & "','" & Format(fileDate, "yyyy-MM-dd HH:mm:ss") & "')"
                        update_sql(upd_txt)
                    End If
                Next
            Catch ex2 As Exception
                ConnectDb2Fees("BodyCamera")
            End Try
        Next
        'MsgBox("finished")
        Me.Close()




    End Sub
End Class

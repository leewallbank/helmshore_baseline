﻿Imports System.IO

Public Class clsAddressData
    Public AddressDS As New DataSet
    Private MasterList As New DataTable, TempTable As New DataTable, MergedSource As DataTable
    Private OutputDV As DataView
    Private ReturnedTraceDV As DataView
    Public ErrFileName As String = "##Error##"

    Public Sub New()
        Try
            AddressDS.Tables.Add("AddressSource")
            AddressDS.Tables.Add("ContactSource")
            AddressDS.Tables.Add("AddressOutput")
            AddressDS.Tables.Add("OutputTrace")
            AddressDS.Tables.Add("ReturnedTrace")

            ' The following column definitions are also used to pad fields in the output file, so this section must match the return file spec from student loans
            ' These definitions are reference throughout the app
            With AddressDS.Tables("AddressOutput").Columns

                .Add("Exclude", Type.GetType("System.Boolean")) ' Added TS 21/Mar/2013 Portal task ref 14709
                AddressDS.Tables("AddressOutput").Columns("Exclude").DefaultValue = False ' Added TS 21/Mar/2013 Portal task ref 14709

                .Add("Scheme name", Type.GetType("System.String"))

                .Add("NoteID", Type.GetType("System.String"))

                .Add("DebtorID", Type.GetType("System.String"))
                .Item("DebtorID").ExtendedProperties.Add("MaxLength", 7)

                .Add("ProductCode", Type.GetType("System.String"))

                .Add("FirstLoanPack", Type.GetType("System.String"))
                .Item("FirstLoanPack").ExtendedProperties.Add("MaxLength", 11)

                .Add("PayInBookReference", Type.GetType("System.String"))
                .Item("PayInBookReference").ExtendedProperties.Add("MaxLength", 18)

                .Add("CustomerFullName", Type.GetType("System.String"))
                .Item("CustomerFullName").ExtendedProperties.Add("MaxLength", 100)

                .Add("DOB", Type.GetType("System.String"))
                .Item("DOB").ExtendedProperties.Add("MaxLength", 8)

                .Add("SuccessCode", Type.GetType("System.String"))
                .Item("SuccessCode").ExtendedProperties.Add("MaxLength", 10)

                .Add("SubBuildingName", Type.GetType("System.String"))
                .Item("SubBuildingName").ExtendedProperties.Add("MaxLength", 60)

                .Add("BuildingName", Type.GetType("System.String"))
                .Item("BuildingName").ExtendedProperties.Add("MaxLength", 60)

                .Add("BuildingNumber", Type.GetType("System.String"))
                .Item("BuildingNumber").ExtendedProperties.Add("MaxLength", 10)

                .Add("Street", Type.GetType("System.String"))
                .Item("Street").ExtendedProperties.Add("MaxLength", 60)

                .Add("Locality", Type.GetType("System.String"))
                .Item("Locality").ExtendedProperties.Add("MaxLength", 60)

                .Add("Town", Type.GetType("System.String"))
                .Item("Town").ExtendedProperties.Add("MaxLength", 60)

                .Add("County", Type.GetType("System.String"))
                .Item("County").ExtendedProperties.Add("MaxLength", 60)

                .Add("PostCode", Type.GetType("System.String"))
                .Item("PostCode").ExtendedProperties.Add("MaxLength", 8)

                .Add("DebtorContactID", Type.GetType("System.String"))

                .Add("CustomerHomePhone", Type.GetType("System.String"))
                .Item("CustomerHomePhone").ExtendedProperties.Add("MaxLength", 16)

                .Add("CustomerMobilePhone", Type.GetType("System.String"))
                .Item("CustomerMobilePhone").ExtendedProperties.Add("MaxLength", 16)

                .Add("CustomerWorkPhone", Type.GetType("System.String"))
                .Item("CustomerWorkPhone").ExtendedProperties.Add("MaxLength", 16)

                .Add("CustomerEmail", Type.GetType("System.String"))
                .Item("CustomerEmail").ExtendedProperties.Add("MaxLength", 75)

                .Add("CollectionAmount", Type.GetType("System.String"))
                .Item("CollectionAmount").ExtendedProperties.Add("MaxLength", 10)

                .Add("ClosureCode", Type.GetType("System.String"))
                .Item("ClosureCode").ExtendedProperties.Add("MaxLength", 3)

            End With

            TempTable.Columns.Add("client_ref", Type.GetType("System.String"))

            AddressDS.Tables("OutputTrace").Columns.Add("DebtorID", Type.GetType("System.String"))

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        Try
            If Not IsNothing(OutputDV) Then OutputDV.Dispose()

            If Not IsNothing(AddressDS) Then AddressDS.Dispose() : AddressDS = Nothing
            MyBase.Finalize()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public ReadOnly Property OutputDataView() As DataView
        Get
            OutputDataView = OutputDV
        End Get
    End Property

    Public Sub GetData(ByVal ModDateFrom As Date, ByVal ModDateTo As Date, ByVal OverseasOnly As Boolean)
        GetUpdates(ModDateFrom, ModDateTo, OverseasOnly)
        MergeUpdates()
        GenerateOutput()
    End Sub

    Private Sub GetUpdates(ByVal ModDateFrom As Date, ByVal ModDateTo As Date, ByVal OverseasOnly As Boolean)
        Dim Sql As String
        ' TS 18/03/2015 Excluded conn ID 4567. Request ref 44983

        Try
            ' Load address changes
            'Sql = "SELECT cs._rowid AS ClientSchemeID " & _ commented out TS 19/Feb/2013
            '      "     , CASE WHEN nt.DebtorID IS NOT NULL THEN 'T' ELSE '' END AS TraceIndicator " & _
            '      "     , cs.defaultCourtCode AS ProductCode " & _
            '      "     , s.name AS SchemeName " & _
            '      "     , CAST(n._RowID AS CHAR) AS NoteID " & _
            '      "     , d._rowID AS DebtorID " & _
            '      "     , d.client_ref " & _
            '      "     , IFNULL(d.offence_number,'') AS offence_number " & _
            '      "     , IFNULL(CAST(d.name_title AS CHAR),'') AS name_title " & _
            '      "     , IFNULL(d.name_fore,'') AS name_fore " & _
            '      "     , IFNULL(d.name_sur,'') AS name_sur " & _
            '      "     , d.dateOfBirth " & _
            '      "     , IFNULL(d.address,'')  AS address " & _
            '      "     , '' AS DebtorContactID " & _
            '      "     , '' AS add_phone " & _
            '      "     , '' AS add_fax " & _
            '      "     , '' AS empphone " & _
            '      "     , '' AS addemail " & _
            '      "FROM debtor AS d " & _
            '      "INNER JOIN note AS n ON d._rowID = n.DebtorID " & _
            '      "INNER JOIN clientscheme AS cs ON d.clientschemeID = cs._RowID " & _
            '      "INNER JOIN scheme AS s ON cs.schemeID = s._RowID " & _
            '      "INNER JOIN ( SELECT n_s.DebtorID " & _
            '      "                  , MAX(n_s._rowID) AS LastNoteID " & _
            '      "             FROM debtor AS d_s " & _
            '      "             INNER JOIN note AS n_s ON d_s._rowID = n_s.DebtorID " & _
            '      "             INNER JOIN clientscheme AS cs_s ON d_s.clientschemeID = cs_s._RowID " & _
            '      "             WHERE n_s.type = 'Address' " & _
            '      "               AND n_s.text LIKE 'changed from:%'" & _
            '      "               AND cs_s.clientID = 1572 " & _
            '      "             GROUP BY n_s.DebtorID " & _
            '      "           ) AS n_max ON n.DebtorID = n_max.DebtorID " & _
            '      "                     AND n._rowID   = n_max.LastNoteID " & _
            '      "LEFT JOIN note AS nt ON n.debtorID           = nt.debtorID " & _
            '      "                    AND 'Off trace'          = nt.type " & _
            '      "                    AND DATE(n._createdDate) = DATE(nt._createdDate)" & _
            '      "LEFT JOIN ( SELECT p_s.DebtorID " & _
            '      "            FROM Payment AS p_s " & _
            '      "            INNER JOIN ClientScheme AS cs_s ON p_s.ClientSchemeID = cs_s._rowID " & _
            '      "            WHERE cs_s.clientID = 1572 " & _
            '      "              AND DATE(p_s.date) BETWEEN '" & ModDateFrom.AddDays(-1).ToString("yyyy-MM-dd") & "' " & _
            '      "                                     AND '" & ModDateTo.AddDays(-1).ToString("yyyy-MM-dd") & "' " & _
            '      "            GROUP BY p_s.DebtorID " & _
            '      "          ) AS p ON d._rowID = p.DebtorID " & _
            '      "WHERE n.type = 'Address' " & _
            '      "  AND n.text LIKE 'changed from:%'" & _
            '      "  AND cs.clientID = 1572 " & _
            '      "  AND (    ( nt.DebtorID IS NULL AND DATE() BETWEEN '" & ModDateFrom.ToString("yyyy-MM-dd") & "' " & _
            '      "                                                              AND '" & ModDateTo.ToString("yyyy-MM-dd") & "' " & _
            '      "           ) " & _
            '      "        OR ( nt.DebtorID IS NOT NULL AND ( DATE(n._createdDate) BETWEEN '" & ModDateFrom.AddDays(-14).ToString("yyyy-MM-dd") & "' " & _
            '      "                                                                    AND '" & ModDateTo.AddDays(-14).ToString("yyyy-MM-dd") & "' " & _
            '      "                                           OR " & _
            '      "                                           ( p.DebtorID IS NOT NULL " & _
            '      "                                             AND DATE(n._createdDate) BETWEEN '" & ModDateFrom.ToString("yyyy-MM-dd") & "' " & _
            '      "                                                                        AND '" & ModDateTo.ToString("yyyy-MM-dd") & "' " & _
            '      "                                           ) " & _
            '      "                                         ) " & _
            '      "           )" & _
            '      "      )"

            'Sql = "SELECT cs._rowid AS ClientSchemeID " & _
            '      "     , CASE WHEN nt.DebtorID IS NOT NULL THEN 'T' ELSE '' END AS TraceIndicator " & _
            '      "     , cs.defaultCourtCode AS ProductCode " & _
            '      "     , s.name AS SchemeName " & _
            '      "     , CAST(n._RowID AS CHAR) AS NoteID " & _
            '      "     , d._rowID AS DebtorID " & _
            '      "     , d.client_ref " & _
            '      "     , IFNULL(d.offence_number,'') AS offence_number " & _
            '      "     , IFNULL(CAST(d.name_title AS CHAR),'') AS name_title " & _
            '      "     , IFNULL(d.name_fore,'') AS name_fore " & _
            '      "     , IFNULL(d.name_sur,'') AS name_sur " & _
            '      "     , d.dateOfBirth " & _
            '      "     , IFNULL(d.address,'')  AS address " & _
            '      "     , '' AS DebtorContactID " & _
            '      "     , '' AS add_phone " & _
            '      "     , '' AS add_fax " & _
            '      "     , '' AS empphone " & _
            '      "     , '' AS addemail " & _
            '      "FROM debtor AS d " & _
            '      "INNER JOIN note AS n ON d._rowID = n.DebtorID " & _
            '      "INNER JOIN clientscheme AS cs ON d.clientschemeID = cs._RowID " & _
            '      "INNER JOIN scheme AS s ON cs.schemeID = s._RowID " & _
            '      "INNER JOIN ( SELECT n_s.DebtorID " & _
            '      "                  , MAX(n_s._rowID) AS LastNoteID " & _
            '      "             FROM debtor AS d_s " & _
            '      "             INNER JOIN note AS n_s ON d_s._rowID = n_s.DebtorID " & _
            '      "             INNER JOIN clientscheme AS cs_s ON d_s.clientschemeID = cs_s._RowID " & _
            '      "             WHERE n_s.type = 'Address' " & _
            '      "               AND n_s.text LIKE 'changed from:%'" & _
            '      "               AND cs_s.clientID = 1572 " & _
            '      "             GROUP BY n_s.DebtorID " & _
            '      "           ) AS n_max ON n.DebtorID = n_max.DebtorID " & _
            '      "                     AND n._rowID   = n_max.LastNoteID " & _
            '      "LEFT JOIN note AS nt ON n.debtorID           = nt.debtorID " & _
            '      "                    AND 'Off trace'          = nt.type " & _
            '      "                    AND DATE(n._createdDate) = DATE(nt._createdDate)" & _
            '      "WHERE n.type = 'Address' " & _
            '      "  AND n.text LIKE 'changed from:%'" & _
            '      "  AND cs.clientID = 1572 " & _
            '      "  AND (    ( nt.DebtorID IS     NULL AND DATE(n._createdDate) BETWEEN '" & ModDateFrom.ToString("yyyy-MM-dd") & "' " & _
            '      "                                                                  AND '" & ModDateTo.ToString("yyyy-MM-dd") & "' " & _
            '      "           ) " & _
            '      "        OR ( nt.DebtorID IS NOT NULL AND DATE(n._createdDate) BETWEEN '" & ModDateFrom.AddDays(-14).ToString("yyyy-MM-dd") & "' " & _
            '      "                                                                  AND '" & ModDateTo.AddDays(-14).ToString("yyyy-MM-dd") & "' " & _
            '      "           )" & _
            '      "      )"

            Sql = "SELECT cs._rowid AS ClientSchemeID " & _
                  "     , CASE WHEN cs._rowID IN (3174,3175,3176,3177,3178,3179,3180,3181,3182,3183) THEN 'T' ELSE '' END AS TraceIndicator " & _
                  "     , cs.defaultCourtCode AS ProductCode " & _
                  "     , s.name AS SchemeName " & _
                  "     , CAST(n._RowID AS CHAR) AS NoteID " & _
                  "     , d._rowID AS DebtorID " & _
                  "     , d.client_ref " & _
                  "     , IFNULL(d.offence_number,'') AS offence_number " & _
                  "     , IFNULL(CAST(d.name_title AS CHAR),'') AS name_title " & _
                  "     , IFNULL(d.name_fore,'') AS name_fore " & _
                  "     , IFNULL(d.name_sur,'') AS name_sur " & _
                  "     , d.dateOfBirth " & _
                  "     , IFNULL(d.address,'')  AS address " & _
                  "     , '' AS DebtorContactID " & _
                  "     , '' AS add_phone " & _
                  "     , '' AS add_fax " & _
                  "     , '' AS empphone " & _
                  "     , '' AS addemail " & _
                  "FROM debtor AS d " & _
                  "INNER JOIN note AS n ON d._rowID = n.DebtorID " & _
                  "INNER JOIN clientscheme AS cs ON d.clientschemeID = cs._RowID " & _
                  "INNER JOIN scheme AS s ON cs.schemeID = s._RowID " & _
                  "INNER JOIN ( SELECT n_s.DebtorID " & _
                  "                  , MAX(n_s._rowID) AS LastNoteID " & _
                  "             FROM debtor AS d_s " & _
                  "             INNER JOIN note AS n_s ON d_s._rowID = n_s.DebtorID " & _
                  "             INNER JOIN clientscheme AS cs_s ON d_s.clientschemeID = cs_s._RowID " & _
                  "             WHERE n_s.type = 'Address' " & _
                  "               AND n_s.text LIKE 'changed from:%'" & _
                  "               AND cs_s.clientID = 1572 " & _
                  "             GROUP BY n_s.DebtorID " & _
                  "           ) AS n_max ON n.DebtorID = n_max.DebtorID " & _
                  "                     AND n._rowID   = n_max.LastNoteID " & _
                  "LEFT JOIN note AS nt ON n.debtorID           = nt.debtorID " & _
                  "                    AND 'Off trace'          = nt.type " & _
                  "                    AND DATE(n._createdDate) = DATE(nt._createdDate)" & _
                  "WHERE n.type = 'Address' " & _
                  "  AND n.text LIKE 'changed from:%'" & _
                  "  AND cs.clientID = 1572 " & _
                  "  AND cs._rowID <> 4567 " & _
                  "  AND (    ( nt.DebtorID IS     NULL AND DATE(n._createdDate) BETWEEN '" & ModDateFrom.ToString("yyyy-MM-dd") & "' " & _
                  "                                                                  AND '" & ModDateTo.ToString("yyyy-MM-dd") & "' " & _
                  "           ) " & _
                  "        OR ( nt.DebtorID IS NOT NULL AND DATE(n._createdDate) BETWEEN '" & ModDateFrom.AddDays(-14).ToString("yyyy-MM-dd") & "' " & _
                  "                                                                  AND '" & ModDateTo.AddDays(-14).ToString("yyyy-MM-dd") & "' " & _
                  "           )" & _
                  "      )"

            If OverseasOnly Then Sql &= "AND cs._rowid IN (3162, 3163, 3164, 3167, 3168, 3174, 3175, 3176, 3177, 3178)"

            LoadDataTable("DebtRecovery", Sql, AddressDS, "AddressSource", False)

            ' Load contact detail changes
            ' TraceIndicator logic added TS 04/Feb/2013
            Sql = "SELECT sd.ClientSchemeID " & _
                  "     , CASE WHEN sd.ClientSchemeID IN (3174,3175,3176,3177,3178,3179,3180,3181,3182,3183) THEN 'T' ELSE '' END AS TraceIndicator " & _
                  "     , sd.FileName AS ProductCode " & _
                  "     , sd.SchemeName " & _
                  "     , '' AS NoteID " & _
                  "     , sd.DebtorID " & _
                  "     , sd.client_ref " & _
                  "     , sd.offence_number " & _
                  "     , sd.name_title " & _
                  "     , sd.name_fore " & _
                  "     , sd.name_sur " & _
                  "     , sd.dateOfBirth " & _
                  "     , ''  AS address " & _
                  "     , d.DebtorContactID " & _
                  "     , ISNULL(d.add_phone,'') AS add_phone " & _
                  "     , ISNULL(d.add_fax,'') AS add_fax " & _
                  "     , ISNULL(d.empphone,'') AS empphone " & _
                  "     , ISNULL(d.addemail,'') AS addemail " & _
                  "FROM rpt.DebtorContact AS d " & _
                  "INNER JOIN stg.Debtor AS sd ON d.DebtorID = sd.DebtorID " & _
                  "  AND (    (     sd.ClientSchemeID NOT IN (3174,3175,3176,3177,3178,3179,3180,3181,3182,3183) " & _
                  "             AND CAST(FLOOR(CAST(d.BuildDate AS FLOAT)) AS DATETIME) BETWEEN '" & ModDateFrom.ToString("yyyy-MM-dd") & "' " & _
                  "                                                                         AND '" & ModDateTo.ToString("yyyy-MM-dd") & "' " & _
                  "           )" & _
                  "       OR (     sd.ClientSchemeID IN (3174,3175,3176,3177,3178,3179,3180,3181,3182,3183) " & _
                  "            AND CAST(FLOOR(CAST(d.BuildDate AS FLOAT)) AS DATETIME) BETWEEN '" & ModDateFrom.AddDays(-14).ToString("yyyy-MM-dd") & "' " & _
                  "                                                                     AND '" & ModDateTo.AddDays(-14).ToString("yyyy-MM-dd") & "' " & _
                  "          ) " & _
                  "      )"

            If OverseasOnly Then Sql &= "AND sd.ClientSchemeID IN (3162, 3163, 3164, 3167, 3168, 3174, 3175, 3176, 3177, 3178)"

            Sql &= "WHERE sd.ClientSchemeID <> 4567"

            LoadDataTable("StudentLoans", Sql, AddressDS, "ContactSource", False)

            ' Load returned traces
            Sql = "SELECT rt.DebtorID " & _
                  "FROM dbo.ReturnedTrace AS rt"

            LoadDataTable("StudentLoans", Sql, AddressDS, "ReturnedTrace", False)

            ReturnedTraceDV = New DataView(AddressDS.Tables("ReturnedTrace"))
            ReturnedTraceDV.AllowNew = False

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub MergeUpdates()
        Try
            Dim AddressDV As DataView = New DataView(AddressDS.Tables("AddressSource"))
            Dim ContactDV As DataView = New DataView(AddressDS.Tables("ContactSource"))

            MergedSource = AddressDS.Tables("AddressSource").Clone() ' Clone the record structure of one of the source files for the merge output
            TempTable.Clear()

            ' Append both sets of headers to temp table
            For Each dr As DataRow In AddressDS.Tables("AddressSource").Rows
                TempTable.Rows.Add(dr("client_ref"))
            Next dr

            For Each dr As DataRow In AddressDS.Tables("ContactSource").Rows
                TempTable.Rows.Add(dr("client_ref"))
            Next dr

            ' Get distinct line headers
            MasterList = TempTable.DefaultView.ToTable(True)

            ' Build the merged table
            For Each dr As DataRow In MasterList.Rows
                AddressDV.RowFilter = ("client_ref = '" & dr("client_ref") & "'")
                ContactDV.RowFilter = ("client_ref = '" & dr("client_ref") & "'")

                If AddressDV.Count = 0 And ContactDV.Count > 0 Then
                    MergedSource.ImportRow(ContactDV.Item(0).Row)
                ElseIf ContactDV.Count = 0 And AddressDV.Count > 0 Then
                    MergedSource.ImportRow(AddressDV.Item(0).Row)
                Else ' Assume data in both as the client ref must have been in at least one of the source tables
                    MergedSource.Rows.Add({AddressDV.Item(0).Item(0), _
                                           AddressDV.Item(0).Item(1), _
                                           AddressDV.Item(0).Item(2), _
                                           AddressDV.Item(0).Item(3), _
                                           AddressDV.Item(0).Item(4), _
                                           AddressDV.Item(0).Item(5), _
                                           AddressDV.Item(0).Item(6), _
                                           AddressDV.Item(0).Item(7), _
                                           AddressDV.Item(0).Item(8), _
                                           AddressDV.Item(0).Item(9), _
                                           AddressDV.Item(0).Item(10), _
                                           AddressDV.Item(0).Item(11), _
                                           AddressDV.Item(0).Item(12), _
                                           ContactDV.Item(0).Item(13), _
                                           ContactDV.Item(0).Item(14), _
                                           ContactDV.Item(0).Item(15), _
                                           ContactDV.Item(0).Item(16), _
                                           ContactDV.Item(0).Item(17)})
                End If

            Next dr

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub GenerateOutput()

        Dim OutputDataRow As DataRow
        Dim FullName As String, AddressLine() As String, AddressWord() As String

        Try
            ClearDataTable(AddressDS, "AddressOutput")

            With AddressDS.Tables("AddressOutput")
                For Each SourceDataRow As DataRow In MergedSource.Rows
                    OutputDataRow = .Rows.Add

                    ' The following section performs the mapping from DebtRecovery fields to the output file
                    OutputDataRow("Scheme name") = SourceDataRow("SchemeName")

                    OutputDataRow("NoteID") = SourceDataRow("NoteID")

                    OutputDataRow("DebtorID") = SourceDataRow("DebtorID")

                    ReturnedTraceDV.RowFilter = "DebtorID = " & SourceDataRow("DebtorID")

                    OutputDataRow("ProductCode") = If(ReturnedTraceDV.Count = 0, SourceDataRow("TraceIndicator"), "") & SourceDataRow("ProductCode").ToString.Substring(0, 1).Replace("T", "") & SourceDataRow("ProductCode").ToString.Substring(1) ' only prefix the product code with the T indicator if we haven't before

                    If SourceDataRow("TraceIndicator") = "T" Then AddressDS.Tables("OutputTrace").Rows.Add(SourceDataRow("DebtorID"))

                    OutputDataRow("FirstLoanPack") = SourceDataRow("client_ref")

                    OutputDataRow("PayInBookReference") = SourceDataRow("offence_number")

                    FullName = SourceDataRow("name_title")
                    If SourceDataRow("name_title") <> "" Then FullName &= " "
                    FullName &= SourceDataRow("name_fore")
                    If SourceDataRow("name_fore") <> "" Then FullName &= " "
                    FullName &= SourceDataRow("name_sur")

                    OutputDataRow("CustomerFullName") = FullName

                    If IsDBNull(SourceDataRow("dateOfBirth")) Then
                        OutputDataRow("DOB") = ""
                    Else
                        OutputDataRow("DOB") = CDate(SourceDataRow("dateOfBirth")).ToString("ddMMyyy")
                    End If

                    OutputDataRow("SuccessCode") = "NEW"

                    ' Create an array of address lines. Note DebtRecovery sometimes just uses a line feed for new line with no carriage return
                    If SourceDataRow("Address") <> "" Then
                        AddressLine = Replace(LTrim(SourceDataRow("Address")), vbCr, "").ToString.TrimEnd(vbLf).Split(vbLf)
                    Else
                        AddressLine = Nothing
                    End If

                    ' DebtRecovery stored the address in one field. The following logic is to break it down into multiple
                    If Not IsNothing(AddressLine) Then

                        AddressWord = AddressLine(0).ToString.Split(" ") ' Split first line into words to get house number and flat details

                        ' If the second word is a number assume a flat or appartment or if addressline contains the word flat assume a flat
                        If (UBound(AddressWord) > 1 AndAlso System.Text.RegularExpressions.Regex.Match(AddressWord(1), "\d").Success) Or AddressWord(0).ToUpper.Contains("FLAT") Then
                            OutputDataRow("SubBuildingName") = AddressWord(0)

                            If UBound(AddressWord) > 0 Then
                                OutputDataRow("SubBuildingName") &= " " & AddressWord(1) ' Append the first two words to SubBuildingName
                            End If

                            ' If the flat and its number are the only line contents, assume the next line is the road and rebuild the array
                            If UBound(AddressWord) < 2 Then
                                OutputDataRow("Street") = AddressLine(1)
                                Dim objArrayList As New ArrayList(AddressLine)
                                objArrayList.RemoveAt(1)
                                AddressLine = objArrayList.ToArray(GetType(String))
                            Else
                                For WordLoop As Integer = 2 To UBound(AddressWord) ' 
                                    OutputDataRow("Street") &= " " & AddressWord(WordLoop) ' Append the rest to Street
                                Next WordLoop
                            End If

                            If Not IsDBNull(OutputDataRow("Street")) Then OutputDataRow("Street") = LTrim(OutputDataRow("Street"))

                            If System.Text.RegularExpressions.Regex.Match(OutputDataRow("Street"), "\d").Success Then
                                ' If the first word is a number assume a normal house number
                                If OutputDataRow("Street").IndexOf(" ") > 0 Then ' added TS 15/May/2014 Request ref 22188
                                    OutputDataRow("BuildingNumber") = Left(OutputDataRow("Street"), OutputDataRow("Street").IndexOf(" "))
                                    OutputDataRow("Street") = Right(OutputDataRow("Street"), OutputDataRow("Street").Length - OutputDataRow("Street").IndexOf(" ") - 1)
                                End If
                            End If

                            'ElseIf IsNumeric(AddressWord(0).Replace(",", "|")) Then 'System.Text.RegularExpressions.Regex.Match(AddressWord(0), "\d").Success Then
                            '    ' If the first word is a number assume a normal house number
                            '    OutputDataRow("BuildingNumber") = Left(AddressLine(0), AddressLine(0).IndexOf(" "))
                            '    OutputDataRow("Street") = Right(AddressLine(0), AddressLine(0).Length - AddressLine(0).IndexOf(" ") - 1)
                        ElseIf System.Text.RegularExpressions.Regex.Match(AddressWord(0), "\d").Success Then
                            ' If the first word is a number assume a normal house number
                            If AddressLine(0).IndexOf(" ") > 0 Then ' Added TS 08/Nov/2013 Portal task ref 17938
                                ' Original logic
                                OutputDataRow("BuildingNumber") = Left(AddressLine(0), AddressLine(0).IndexOf(" "))
                                OutputDataRow("Street") = Right(AddressLine(0), AddressLine(0).Length - AddressLine(0).IndexOf(" ") - 1)
                            Else
                                OutputDataRow("BuildingNumber") = AddressLine(0)
                            End If
                        Else

                            ' Nothing captured above
                            OutputDataRow("Street") = AddressLine(0)
                        End If

                        ' put the lines in the address into the different fields. The logic used differs based on the number of lines in the address
                        Select Case UBound(AddressLine)
                            Case 0
                                ' no need to do anything as the single line will go into street and postcode
                            Case 1
                                ' no need to do anything as the first line will go into street and the last into postcode
                            Case 2
                                OutputDataRow("Town") = AddressLine(1) ' MOD TS 30/Oct/2012 was county
                            Case 3
                                OutputDataRow("Town") = AddressLine(1)
                                OutputDataRow("County") = AddressLine(2)
                            Case 4
                                OutputDataRow("Locality") = AddressLine(1)
                                OutputDataRow("Town") = AddressLine(2)
                                OutputDataRow("County") = AddressLine(3)
                            Case Else
                                For LineCount As Integer = 1 To UBound(AddressLine) - 3
                                    If Not IsDBNull(OutputDataRow("Locality")) Then OutputDataRow("Locality") += " "
                                    OutputDataRow("Locality") += AddressLine(LineCount) ' Append the rest to Locality
                                Next LineCount

                                OutputDataRow("Town") = AddressLine(UBound(AddressLine) - 2)
                                OutputDataRow("County") = AddressLine(UBound(AddressLine) - 1)
                        End Select

                        OutputDataRow("PostCode") = Trim(AddressLine(UBound(AddressLine)))

                    End If

                    OutputDataRow("DebtorContactID") = SourceDataRow("DebtorContactID")

                    OutputDataRow("CustomerHomePhone") = Replace(SourceDataRow("add_phone"), " ", "")

                    OutputDataRow("CustomerMobilePhone") = Replace(SourceDataRow("add_fax"), " ", "")

                    OutputDataRow("CustomerWorkPhone") = Replace(SourceDataRow("empphone"), " ", "")

                    OutputDataRow("CustomerEmail") = SourceDataRow("addemail")

                    OutputDataRow("CollectionAmount") = ""

                    OutputDataRow("ClosureCode") = ""

                Next SourceDataRow
            End With

            OutputDV = New DataView(AddressDS.Tables("AddressOutput"))
            OutputDV.AllowDelete = False
            OutputDV.AllowNew = False

        Catch ex As Exception
            AddressDS.Tables("AddressOutput").Clear()
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub LoadFromFile(ByVal Filename As String)
        Dim Line As String
        Try
            ' This is just in case we need to edit a file that already exists
            Dim objReader As New System.IO.StreamReader(Filename)

            ClearDataTable(AddressDS, "AddressOutput")

            Do While objReader.Peek() <> -1
                Line = objReader.ReadLine()
                ' RTrim as the datafiles are right padded
                AddressDS.Tables("AddressOutput").Rows.Add({"", "", "", Path.GetFileName(Filename).ToString.Substring(0, Path.GetFileName(Filename).IndexOf("_")) _
                                                          , RTrim(Line.Substring(0, 11)) _
                                                          , RTrim(Line.Substring(11, 18)) _
                                                          , RTrim(Line.Substring(29, 100)) _
                                                          , RTrim(Line.Substring(129, 8)) _
                                                          , RTrim(Line.Substring(137, 10)) _
                                                          , RTrim(Line.Substring(147, 60)) _
                                                          , RTrim(Line.Substring(207, 60)) _
                                                          , RTrim(Line.Substring(267, 10)) _
                                                          , RTrim(Line.Substring(277, 60)) _
                                                          , RTrim(Line.Substring(337, 60)) _
                                                          , RTrim(Line.Substring(397, 60)) _
                                                          , RTrim(Line.Substring(457, 60)) _
                                                          , RTrim(Line.Substring(517, 8)) _
                                                          , "" _
                                                          , RTrim(Line.Substring(525, 16)) _
                                                          , RTrim(Line.Substring(541, 16)) _
                                                          , RTrim(Line.Substring(557, 16)) _
                                                          , RTrim(Line.Substring(573, 75)) _
                                                          , RTrim(Line.Substring(648, 10)) _
                                                          , RTrim(Line.Substring(658, 3))})
            Loop

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub LogOutput(ByVal DebtorID As String, ByVal NoteID As String)
        Dim Sql As String

        Try
            Sql = "EXEC dbo.LogCCIOutput " & DebtorID & ", 'A', '" & NoteID & "'"

            ExecStoredProc("StudentLoans", Sql)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub LogTraceCases()
        Dim Sql As String

        Try
            For Each SourceDataRow As DataRow In AddressDS.Tables("OutputTrace").Rows

                Sql = "EXEC dbo.SetReturnedTrace " & SourceDataRow("DebtorID")

                ExecStoredProc("StudentLoans", Sql)

            Next
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Function OutputExists(ByVal DebtorID As String, ByVal NoteID As String) As Boolean
        OutputExists = GetProcResults("EXEC dbo.CCIOutputExists " & DebtorID & ", 'A', '" & NoteID & "'")
    End Function
End Class

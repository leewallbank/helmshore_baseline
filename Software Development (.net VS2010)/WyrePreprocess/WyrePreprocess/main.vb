Public Class mainform
    Public name1, name2, name3, name4, curr_addr, debt_addr, outfile, clref, comment As String
    Public debt_amt As Decimal
    Public lodate As Date
    Public rowidx As Integer
    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        reformbtn.Enabled = False
        errbtn.Enabled = False
        ProgressBar1.Value = 0
        TextBox1.Text = "Opening file"
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "XLS files|*.xls;*.xlsx"
            .FileName = ""
            .CheckFileExists = True
        End With
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MessageBox.Show("No file Selected")
            Exit Sub
        End If
        load_vals(OpenFileDialog1.FileName)
        If finalrow > 0 Then
            reformbtn.Enabled = True
        End If
        TextBox1.Text = "File opened"
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        errorfile = Nothing
        ProgressBar1.Value = 0
        'write out headings
        outfile = "Client Ref|Name1|Name2|ClientNotes|CurrAddr|DebtAddr|LO date|Debt Amount" & vbNewLine
        'read array to get name in column 1
        Dim rowidx As Integer
        For rowidx = 2 To finalrow
            Try
                ProgressBar1.Value = rowidx / finalrow * 100
            Catch ex As Exception

            End Try
            Application.DoEvents()
            case_found(rowidx)
        Next

        Dim namelength = Len(OpenFileDialog1.FileName)
        Dim filename_prefix As String = ""
        filename_prefix = Microsoft.VisualBasic.Left(OpenFileDialog1.FileName, namelength - 4)
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
            MsgBox("Preprocess file saved")
            Me.Close()
        End If
    End Sub

    Public Sub case_found(ByVal rowidx)
        Dim lostring As String
        lodate = Nothing
        debt_amt = Nothing
        curr_addr = Nothing
        debt_addr = Nothing
        name1 = Nothing
        name2 = Nothing
        name3 = Nothing
        name4 = Nothing
        comment = Nothing

        clref = vals(rowidx, 1)

        name1 = Trim(vals(rowidx, 2))
        name2 = Trim(vals(rowidx, 3))
        name3 = Trim(vals(rowidx, 4))
        name4 = Trim(vals(rowidx, 5))
        curr_addr = Trim(vals(rowidx, 6))
        debt_addr = Trim(vals(rowidx, 7))

        Try
            debt_amt = vals(rowidx, 9)
        Catch ex As Exception
            Exit Sub
        End Try

        If name3.Length > 0 Then
            comment = comment & "Name3:" & name3 & ";"
        End If

        If name4.Length > 0 Then
            comment = comment & "Name4:" & name4 & ";"
        End If
        lostring = vals(rowidx, 8)

        Try
            lodate = CDate(lostring)
        Catch ex As Exception
            errorfile = errorfile & "Line  " & rowidx & " - Invalid LO date" & vbNewLine
            Exit Sub
        End Try

        If clref = Nothing Then
            errorfile = errorfile & "Line  " & rowidx & " - No client reference" & vbNewLine
            Exit Sub
        End If

        If debt_amt = Nothing Then
            errorfile = errorfile & "Line  " & rowidx - 1 & " - INVALID DEBT AMOUNT" & vbNewLine
            Exit Sub
        End If

        If debt_amt <= 0 Then
            errorfile = errorfile & "Line  " & rowidx - 1 & " - INVALID DEBT AMOUNT" & vbNewLine
            Exit Sub
        End If
        'save case in outline
        outfile = outfile & clref & "|" & name1 & "|" & name2 & "|" & comment _
         & "|" & curr_addr & "|" & debt_addr & "|" & lodate & "|" & debt_amt & _
          vbNewLine


    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub


    Private Sub checkbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
End Class

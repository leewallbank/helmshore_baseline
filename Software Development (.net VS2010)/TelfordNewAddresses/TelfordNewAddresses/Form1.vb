﻿Public Class Form1
    'table for keeping record of telephone numbers reported on
    Dim telArray(10000, 3) As String
    Dim telMax As Integer = 0
    Dim outfile As String = ""
    Dim rowCount As Integer = 0
    Dim ascii As New System.Text.ASCIIEncoding()
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ConnectDb2("DebtRecovery")

        'get address changes for last week
        Dim startdate As Date = DateAdd(DateInterval.Day, -Weekday(Now) - 6, Now)
        startdate = CDate(Format(startdate, "MMM dd, yyyy" & " 00:00:00"))
        Dim endDate As Date = DateAdd(DateInterval.Day, 7, startdate)
        Dim debt_dt As New DataTable
        '1876
        'T85117 ignore cancelled, fully paid and successful cases
        LoadDataTable2("DebtRecovery", "select D._rowid, D.client_ref,name_title, name_fore, name_sur, N.text,N._createdDate,D.address,D.add_postcode," & _
                       "D.status,D.arrange_amount,arrange_interval,debt_balance " &
                       " from Debtor D, note N, clientScheme CS" & _
                       " where N.debtorID = D._rowID" & _
                       " and not (D.status in ('C', 'F','S'))" & _
                       " and D.clientSchemeID = CS._rowID" & _
                       " and CS.clientID = 1150" & _
                       " and N.type = 'Address'" & _
                       " and N.text like 'changed from:%'" & _
                       " and N._createdDate >= '" & Format(startdate, "yyyy-MM-dd") & "'" & _
                       " and N._createdDate < ' " & Format(endDate, "yyyy-MM-dd") & "'", debt_dt, False)
        For Each debtrow In debt_dt.Rows
            Dim debtorID As Integer = debtrow(0)
            Dim clientRef As String = Microsoft.VisualBasic.Left(debtrow(1), 13)
            Dim fullName As String = ""
            Try
                fullName = debtrow(2) & " "
            Catch ex As Exception

            End Try
            Try
                fullName &= debtrow(3) & " "
            Catch ex As Exception

            End Try
            Try
                fullName &= debtrow(4)
            Catch ex As Exception

            End Try
            outfile &= clientRef & "|" & fullName & "|"
            Dim address As String = debtrow(5)
            Dim addressArray() As String = address.Split(",")
            Dim postCode As String = ""
            Dim addLine(10) As String
            For addressidx = 0 To 10
                addLine(addressidx) = ""
            Next
            For addressIDX = 0 To UBound(addressArray)
                Dim addrLine As String = Trim(Replace(addressArray(addressIDX), "changed from:", ""))
                addLine(addressIDX) = addrLine
            Next
            'set postcode to last address line
            For addressidx = 10 To 0 Step -1
                If addLine(addressidx) <> "" Then
                    postCode = addLine(addressidx)
                    addLine(addressidx) = ""
                    Exit For
                End If
            Next
            'set last address line if more than 4
            For addressidx = 5 To 10
                If addLine(addressidx) <> "" Then
                    addLine(4) &= " " & addLine(addressidx)
                End If
            Next

            Dim notedate As Date = debtrow(6)
            outfile &= addLine(0) & "|" & addLine(1) & "|" & addLine(2) & "|" & addLine(3) & "|" & addLine(4) & "|" & postCode & "|" & Format(notedate, "dd/MM/yyyy") & "|"
            outfile &= fullName & "|"
            Dim newPostCode As String = ""
            Try
                newPostCode = debtrow(8)
            Catch ex As Exception

            End Try
            Dim newAddress As String = debtrow(7)
            'separate out into address lines
            Dim newAddLines(7) As String
            For addressidx = 0 To 7
                newAddLines(addressidx) = ""
            Next
            Dim addLineIDX As Integer = 0
            Dim addressline As String = ""
            For addressidx = 1 To newAddress.Length
                If Mid(newAddress, addressidx, 1) = Chr(10) Or Mid(newAddress, addressidx, 1) = Chr(13) Then
                    newAddLines(addLineIDX) = addressline
                    addLineIDX += 1
                    addressline = ""
                Else
                    addressline &= Mid(newAddress, addressidx, 1)
                End If
            Next
            'save last line
            newAddLines(addLineIDX) = addressline
            'if any address line is same as postcode - blank it out
            For addressidx = 0 To 7
                If newAddLines(addressidx) = newPostCode Then
                    newAddLines(addressidx) = ""
                End If
            Next
            outfile &= newAddLines(0) & "|" & newAddLines(1) & "|" & newAddLines(2) & "|" & newAddLines(3) & "|" & newAddLines(4) & "|"
            outfile &= newPostCode & "|"
            Dim frequency As String = ""
            If debtrow(9) = "A" And debtrow(10) <> 0 Then
                outfile &= debtrow(10) & "|"

                Select Case debtrow(11)
                    Case 7 : frequency = "Weekly"
                    Case 14 : frequency = "Fortnightly"
                    Case 30 : frequency = "Monthly"
                    Case 31 : frequency = "Monthly"
                    Case Else : frequency = "Periodic"
                End Select
                outfile &= frequency & "|"
            Else
                outfile &= "|" & "|"
            End If

            'dob and homeowner status
            outfile &= "|" & "|"

            outfile &= "|" & "|"

            'balance and emp status
            outfile &= debtrow(12) & "|" & "|"

            outfile &= vbNewLine
            rowCount += 1
        Next

       
        'outfile = Format(Now, "dd/MM/yyyy") & "|" & Format(rowCount, "#") & vbNewLine & outfile
        ' Dim filename As String = "C:\AATemp\Rossendales_aa_Update_" & Format(Now, "yyyyMMddHHmmss") & ".csv"
        Dim filename As String = "\\ross-helm-fp001\Rossendales Shared\Telford Reports\Rossendales_aa_Update_" & Format(Now, "yyyyMMddHHmmss") & ".txt"
        My.Computer.FileSystem.WriteAllText(filename, outfile, False, ascii)
        Me.Close()
    End Sub
    Private Function check_tel(ByVal debtorID As Integer, ByVal telno As String) As Boolean
        'see if already reported on
        Dim telFound As Boolean = False
        For telIDX = 1 To telMax
            If telArray(telIDX, 1) = debtorID And
                telArray(telIDX, 2) = telNo Then
                telFound = True
                Exit For
            End If
        Next

        Return (telFound)
    End Function
    Private Function check_tel_type(ByVal debtorID As Integer, ByVal phonetype As String) As String
        'see if phone type available to report on
        Dim phoneTypeFound As Boolean = False
        For telIDX = 1 To telMax
            If telArray(telIDX, 1) = debtorID And
               telArray(telIDX, 3) = phonetype Then
                phoneTypeFound = True
            End If
        Next
        Return (phoneTypeFound)
    End Function
    Private Sub write_out_tel_change(ByVal debtorID As Integer, ByVal telNo As String, ByVal phonetype As String)
        Dim debt_dt As New DataTable
        LoadDataTable2("DebtRecovery", "select _rowid, client_ref,name_title, name_fore, name_sur,debt_balance " &
                       " from Debtor" & _
                       " where _rowID = " & debtorID, debt_dt, False)
        For Each debtRow In debt_dt.Rows
            Dim clientRef As String = Microsoft.VisualBasic.Left(debtRow(1), 13)
            Dim fullName As String = ""
            Try
                fullName = debtRow(2) & " "
            Catch ex As Exception

            End Try
            Try
                fullName &= debtRow(3) & " "
            Catch ex As Exception

            End Try
            Try
                fullName &= debtRow(4)
            Catch ex As Exception

            End Try
            outfile &= clientRef & "|" & fullName & "|"
            'all columns blank until phone 
            outfile &= "||||||||||||||||||"
            
            outfile &= telNo & "|" & phonetype & "|"
            'balance and emp status
            outfile &= Format(debtRow(5), "##.##") & "|" & "|" & vbNewLine
            rowCount += 1
        Next
    End Sub
End Class

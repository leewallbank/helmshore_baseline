﻿Imports System.Xml.Schema
Imports System.IO
Imports commonlibrary

Public Class EON
    Dim xml_valid As Boolean = True
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim new_file As String = ""
    Dim prod_run As Boolean = False
    Dim total_volume, total_transaction_vol As Integer
   
    Dim total_value, total_transaction_value As Decimal
    Dim ackFileName As String
    Dim filename, auditFile, errorfile, errorFileName, acknowledgementFile As String

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        processXMLbtn.Enabled = False
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If



    End Sub
    Private Sub validate_xml_file()
        'open file as text first to remove any £ signs
        Dim txt_doc As String
        txt_doc = My.Computer.FileSystem.ReadAllText(OpenFileDialog1.FileName)
        txt_doc = Replace(txt_doc, "£", "")
        My.Computer.FileSystem.WriteAllText(OpenFileDialog1.FileName, txt_doc, False)
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(OpenFileDialog1.FileName)
        Dim XSDFile As String = "R:\vb.net\EON XSD\EON ADJ.xsd"
        Try
            myDocument.Schemas.Add("", XSDFile)
        Catch ex As Exception
            errorfile = "File: " & OpenFileDialog1.FileName & "has failed validation with XSD" & vbNewLine
            errorfile &= "XSD file: " & XSDFile & vbNewLine
            errorfile &= "Reason: " & ex.ToString
            My.Computer.FileSystem.WriteAllText(errorFileName, errorfile, False)
            xml_valid = False
            Exit Sub
        End Try

        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
    End Sub
    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_error("Error: " & e.Message)
            Case XmlSeverityType.Warning
                write_error("Warning " & e.Message)
        End Select
    End Sub
    Private Sub write_error(ByVal error_message As String)
        Static Dim error_no As Integer
        error_no += 1
        error_message = error_message & vbNewLine
        If error_no = 1 Then
            My.Computer.FileSystem.WriteAllText(errorFileName, error_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(errorFileName, error_message, True)
        End If

    End Sub

    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub processbtn_Click(sender As System.Object, e As System.EventArgs) Handles processXMLbtn.Click
        With OpenFileDialog1
            .Title = "Read XML file"
            .Filter = "XML file|*.xml"
            .FileName = ""
            .CheckFileExists = True
        End With
        Dim xml_dataSet As New DataSet()
        Dim doc As New Xml.XmlDocument()

        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("File not opened")
            Me.Close()
            Exit Sub
        End If
        Dim InputFilePath As String = Path.GetDirectoryName(OpenFileDialog1.FileName)
        InputFilePath &= "\"
        filename = Path.GetFileNameWithoutExtension(OpenFileDialog1.FileName)
        errorFileName = InputFilePath & filename & "_EONADJ_error.txt"
        validate_xml_file()
        If xml_valid = False Then
            MsgBox("XML file has failed validation - see error file")
            If MsgBox("Do you want to continue processing the file?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Me.Close()
                Exit Sub
            End If
        End If
        Dim fieldValue As String
        Dim comments As String = ""
        Dim rdr_name As String = ""
        errorfile &= vbNewLine & "ClientRef,Value, Adj Type,Reason" & vbNewLine

        Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
        reader.Read()
        ProgressBar1.Value = 5
        Dim record_count As Integer = 0
        If prod_run = False Then
            MsgBox("TEST RUN ONLY")
        End If
        Dim validationErrorFound As Boolean = False
     
        Dim clientRef As String = ""
        Dim lastClientRef As String = ""
        Dim transactionValue As Decimal = 0
        Dim adjustmentTypeCode As String = ""
        Dim dateCreated As Date
        While (reader.ReadState <> Xml.ReadState.EndOfFile)
            If reader.NodeType > 1 Then
                reader.Read()
                Continue While
            End If
            Try
                rdr_name = reader.Name
            Catch
                Continue While
            End Try

            'Note ReadElementContentAsString moves focus to next element so don't need read
            ProgressBar1.Value = record_count
            Application.DoEvents()

            Select Case rdr_name
                Case "BALANCE_ADJUSTMENT"
                    reader.Read()
                    While reader.Name <> "BALANCE_ADJUSTMENT"
                        rdr_name = reader.Name
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        rdr_name = reader.Name
                        Select Case rdr_name
                            Case "FILE"
                                reader.Read()
                                While reader.Name <> "FILE"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    rdr_name = reader.Name
                                    Select Case rdr_name
                                        Case "VOLUME"
                                            fieldValue = reader.ReadElementContentAsString
                                            auditFile &= "Volume:" & fieldValue & vbNewLine
                                            total_volume = fieldValue
                                        Case "VALUE"
                                            fieldValue = reader.ReadElementContentAsString
                                            auditFile &= "Value:" & fieldValue & vbNewLine
                                            total_value = fieldValue
                                        Case "DATE_CREATED"
                                            fieldValue = reader.ReadElementContentAsString
                                            dateCreated = fieldValue
                                    End Select
                                    reader.Read()
                                End While
                            Case "SENDER"
                                reader.Read()
                                While reader.Name <> "SENDER"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    rdr_name = reader.Name
                                    Select Case rdr_name
                                        Case "MEMBER_CODE"
                                            fieldValue = reader.ReadElementContentAsString
                                            auditFile &= "MEMBER Code:" & fieldValue & vbNewLine
                                    End Select
                                    reader.Read()
                                End While
                            Case "DEBT"
                                reader.Read()
                                While reader.Name <> "DEBT"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    rdr_name = reader.Name
                                    Select Case rdr_name
                                        Case "CLIENT_REFERENCE"
                                            fieldValue = reader.ReadElementContentAsString
                                            clientRef = fieldValue
                                            If clientRef <> lastClientRef Then
                                                total_transaction_vol += 1
                                            End If
                                            lastClientRef = clientRef
                                        Case "TRANSACTION_VALUE"
                                            fieldValue = reader.ReadElementContentAsString
                                            transactionValue = fieldValue
                                            total_transaction_value += transactionValue

                                        Case "ADJUSTMENT_TYPE_CODE"
                                            fieldValue = reader.ReadElementContentAsString
                                            adjustmentTypeCode = fieldValue
                                    End Select
                                    reader.Read()
                                End While
                                'check adjustment is on onestep
                                Select Case adjustmentTypeCode
                                    Case "PAY"
                                        Dim paymentCount As Integer = GetSQLResults("DebtRecovery", "select count(*) from payment P, clientScheme CS," & _
                                                                                  " debtor D, paysource PS " & _
                                                                                  " where D.clientschemeID = CS._rowID " & _
                                                                                  " and P.debtorID = D._rowID" & _
                                                                                  " and P.amount_sourceID = PS._rowID" & _
                                                                                  " and CS.clientID = 1938" & _
                                                                                  " and D.client_ref = '" & clientRef & "'" & _
                                                                                 " and  PS.direct = 'Y'" & _
                                                                                 " and P._createdDate >= '" & Format(dateCreated, "yyyy-MM-dd") & "'")
                                        If paymentCount = 0 Then
                                            'write out error file
                                            validationErrorFound = True
                                            errorfile &= clientRef & "," & transactionValue & "," & adjustmentTypeCode & "," & "Direct Payment not found" & vbNewLine
                                        End If
                                    Case "PBA"
                                        Dim noteCount As Integer = GetSQLResults("DebtRecovery", "select count(*) from note N, clientScheme CS," & _
                                                                                  " debtor D" & _
                                                                                  " where D.clientschemeID = CS._rowID " & _
                                                                                  " and N.debtorID = D._rowID" & _
                                                                                  " and N.type = 'Debt Changed'" & _
                                                                                  " and CS.clientID = 1938" & _
                                                                                  " and D.client_ref = '" & clientRef & "'" & _
                                                                                 " and  N.text like 'Debt Increased by " & transactionValue & "%'" & _
                                                                                 " and N._createdDate >= '" & Format(dateCreated, "yyyy-MM-dd") & "'")
                                        If noteCount = 0 Then
                                            validationErrorFound = True
                                            errorfile &= clientRef & "," & transactionValue & "," & adjustmentTypeCode & "," & "Debt Change increase not found" & vbNewLine
                                        End If
                                    Case "NBA"
                                        Dim adjValue As Decimal = transactionValue * -1
                                        Dim noteCount As Integer = GetSQLResults("DebtRecovery", "select count(*) from note N, clientScheme CS," & _
                                                                                  " debtor D" & _
                                                                                  " where D.clientschemeID = CS._rowID " & _
                                                                                  " and N.debtorID = D._rowID" & _
                                                                                  " and N.type = 'Debt Changed'" & _
                                                                                  " and CS.clientID = 1938" & _
                                                                                  " and D.client_ref = '" & clientRef & "'" & _
                                                                                 " and  N.text like 'Debt Reduced by " & adjValue & "%'" & _
                                                                                 " and N._createdDate >= '" & Format(dateCreated, "yyyy-MM-dd") & "'")
                                        If noteCount = 0 Then
                                            validationErrorFound = True
                                            errorfile &= clientRef & "," & transactionValue & "," & adjustmentTypeCode & "," & "Debt Change decrease not found" & vbNewLine
                                        End If
                                    Case "FEE"
                                        Dim adjValue As Decimal = transactionValue * -1
                                        Dim note_dt As New DataTable
                                        LoadDataTable("DebtRecovery", "select N.text from note N, clientScheme CS, debtor D" & _
                                                                      " where D.clientschemeID = CS._rowID " & _
                                                                     " and N.debtorID = D._rowID " & _
                                                                     " and N.type = 'Figures changed'" & _
                                                                     " and CS.clientID = 1938" & _
                                                                     " and D.client_ref = '" & clientRef & "'" & _
                                                                     " and  N.text like 'Costs amount was%'" & _
                                                                     " and N._createdDate >= '" & Format(dateCreated, "yyyy-MM-dd") & "'", note_dt, False)
                                        For Each noteRow In note_dt.Rows
                                            Dim noteText As String = noteRow(0)
                                            'get original amount
                                            Dim startIDX As Integer = InStr(noteText, "was")
                                            Dim dotIDX As Integer = InStr(noteText, ".")
                                            Dim origAmt As Decimal
                                            Try
                                                origAmt = Mid(noteText, startIDX + 3, dotIDX + 2 - startIDX - 2)
                                            Catch ex As Exception
                                                validationErrorFound = True
                                            End Try
                                            'now get new amount
                                            Dim newAmt As Decimal
                                            noteText = Microsoft.VisualBasic.Right(noteText, noteText.Length - dotIDX - 2)
                                            noteText = Replace(noteText, "now", "")
                                            dotIDX = InStr(noteText, ".")
                                            Try
                                                newAmt = Microsoft.VisualBasic.Left(noteText, dotIDX + 2)
                                            Catch ex As Exception
                                                validationErrorFound = True
                                            End Try
                                            If Not validationErrorFound Then
                                                If newAmt - origAmt <> transactionValue Then
                                                    validationErrorFound = True
                                                End If
                                            End If
                                            If validationErrorFound Then
                                                errorfile &= clientRef & "," & transactionValue & "," & adjustmentTypeCode & "," & "Figures changed not found" & vbNewLine
                                            End If
                                        Next
                                    Case "OTH"
                                        Dim adjValue As Decimal = transactionValue
                                        Dim noteCount As Integer = GetSQLResults("DebtRecovery", "select count(*) from note N, clientScheme CS," & _
                                                                                  " debtor D" & _
                                                                                  " where D.clientschemeID = CS._rowID " & _
                                                                                  " and N.debtorID = D._rowID" & _
                                                                                  " and N.type = 'Debt Changed'" & _
                                                                                  " and CS.clientID = 1938" & _
                                                                                  " and D.client_ref = '" & clientRef & "'" & _
                                                                                 " and  N.text like 'Debt Reduced by " & adjValue & "%'" & _
                                                                                 " and N._createdDate >= '" & Format(dateCreated, "yyyy-MM-dd") & "'")
                                        If noteCount = 0 Then
                                            validationErrorFound = True
                                            errorfile &= clientRef & "," & transactionValue & "," & adjustmentTypeCode & "," & "Debt Change decrease not found" & vbNewLine
                                        End If
                                    Case Else
                                        MsgBox("What is this tag?" & adjustmentTypeCode)
                                        reader.Read()
                                End Select
                            Case Else
                                MsgBox("What is this tag?" & rdr_name)
                                reader.Read()
                        End Select
                        reader.Read()
                    End While
                Case Else
                    MsgBox("What is this tag?" & rdr_name)
                    reader.Read()
            End Select
        End While

        If total_transaction_value <> total_value Then
            errorfile &= ",,,Total transaction value <> sum of transaction values" & vbNewLine
            validationErrorFound = True
        Else
            If total_transaction_vol <> total_volume Then
                errorfile &= ",,,Total transaction volume <> number of transactions" & vbNewLine
                validationErrorFound = True
            End If
        End If

        'audit file
        If validationErrorFound Then
            My.Computer.FileSystem.WriteAllText(errorFileName, errorfile, False, ascii)
            MsgBox("Discrepancy found - see error report")
        Else
            'Acknowledgement file
            'read in file and update value and volume on row 2, cols 2 and 3
            Dim InputLineArray() As String
            Dim FileContents() As String = System.IO.File.ReadAllLines(ackFileName)
            'now rename original file
            Try
                My.Computer.FileSystem.RenameFile(ackFileName, "original_acknowledgement.csv")
            Catch ex As Exception

            End Try
            Dim lineNumber As Integer
            acknowledgementFile = ""
            'T108167 add timestamp to ack file name
            'look for last hyphen
            Dim idx As Integer
            For idx = ackFileName.Length To 1 Step -1
                If Mid(ackFileName, idx, 1) = "-" Then
                    Exit For
                End If
            Next
            ackFileName = Microsoft.VisualBasic.Left(ackFileName, idx)
            Dim runtime As Date = Now
            ackFileName &= Format(runtime, "ddMMyyyyHHmmss") & ".csv"
            For Each InputLine As String In FileContents
                InputLineArray = InputLine.Split(",")
                lineNumber += 1
                If lineNumber = 2 Then
                    InputLineArray(2) = total_value
                    InputLineArray(3) = total_volume
                End If
                'write out new acknowledgement file
                Select Case lineNumber
                    Case 1
                        For colIDX = 0 To 2
                            If colIDX < 2 Then
                                acknowledgementFile &= InputLineArray(colIDX) & ","
                            Else
                                acknowledgementFile &= Format(runtime, "yyyy-MM-dd HH:mm:ss") & ","
                            End If
                        Next
                        acknowledgementFile &= Path.GetFileName(ackFileName) & vbNewLine
                    Case 2
                        For colIDX = 0 To 2
                            acknowledgementFile &= InputLineArray(colIDX) & ","
                        Next
                        acknowledgementFile &= InputLineArray(3) & vbNewLine
                    Case 3
                        acknowledgementFile &= "ROS" & vbNewLine
                End Select

            Next
            My.Computer.FileSystem.WriteAllText(ackFileName, acknowledgementFile, False, ascii)
            MsgBox("No discrepancy - acknowledgement saved")
        End If
        Me.Close()
    End Sub
    Private Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, Optional ByVal NoteLength As Integer = 250) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 2) ' + 2 is for :;
        Dim PreviousLength As Integer
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = (NoteTag & ":" & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)) & ";").PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function
    Private Function getCaseID(ByVal clientRef As String) As Integer
        Dim debtorID As Integer = 0
        debtorID = GetSQLResults("DebtRecovery", "SELECT D._rowID " & _
                                                   "FROM debtor D, clientscheme CS  " & _
                                                   "WHERE client_ref = '" & clientRef & "'" & _
                                                   " AND D.clientschemeID = CS._rowID " & _
                                                   " AND CS.clientID = 1662")

        Return debtorID
    End Function

    Private Sub ackbtn_Click(sender As System.Object, e As System.EventArgs) Handles ackbtn.Click
        With OpenFileDialog1
            .Title = "Read CSV file"
            .Filter = "CSV file|*.csv"
            .FileName = ""
            .CheckFileExists = True
        End With
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("File not opened")
            Me.Close()
            Exit Sub
        End If
        ackFileName = OpenFileDialog1.FileName
        processXMLbtn.Enabled = True
    End Sub
End Class

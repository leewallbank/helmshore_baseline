﻿Imports System.IO

Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        runbtn.Enabled = False
        exitbtn.Enabled = False
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD253CPreport = New RD253CP
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD253CPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD253CPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253CPreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RD253C TDX Student Loans non-direct invoice.pdf"
        End With

        If SaveFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("Reports not run")
            Exit Sub
        End If
        Dim pathname As String = Path.GetDirectoryName(SaveFileDialog1.FileName) & "\"
        RD253CPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, SaveFileDialog1.FileName)
        RD253CPreport.Close()

        Dim RD253CDPreport = New RD253CDP
        'Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD253CDPreport, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD253CDPreport, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253CDPreport)
        Dim filename As String = pathname & "RD253CD TDX Student Loans direct invoice.pdf"
        RD253CDPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD253CDPreport.Close()
        MsgBox("Reports saved")
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        start_dtp.Value = DateAdd(DateInterval.Day, -4 - Weekday(Now), Now)
        end_dtp.Value = DateAdd(DateInterval.Day, -Weekday(Now) + 2, Now)
    End Sub
End Class

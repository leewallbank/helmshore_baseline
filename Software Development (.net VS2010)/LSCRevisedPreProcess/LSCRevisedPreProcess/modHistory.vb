﻿Module modHistory

    ' 15.04.2013 JEB app config Test/prod added and splashfrm
    ' 22.04.2013 JEB take out carriage control characters from property third partyequity other description
    ' 29.07.2012 JEB Task 16336 New Motor Vehicle Ownership fields
    ' 23.09.2013 JEB TAsk 17235 ignore update flag - treat as new if not on onestep
    ' 11.11.2013 JEB Task 17949 remove letters from changes file
    ' 21.08.2014 JEB Task 28841 check for blank MAATID before loading     
    '20.11.2014  JEB Task 35972 ensure files are written to correct directory
End Module

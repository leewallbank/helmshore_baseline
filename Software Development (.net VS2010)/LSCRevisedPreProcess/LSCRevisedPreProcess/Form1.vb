Imports System.Xml.Schema
Imports System.Configuration
Imports System.IO


Public Class Form1

    Dim invalid_date_file As String = "MAATID|Date field|Date value" & vbNewLine
    Dim exceptions_file As String
    Dim invalid_date_found As Boolean = False
    Dim xml_valid As Boolean = True
    Dim new_file As String = ""
    'Dim outcome_file As String
    Dim input_file, input_file2 As String
    Dim write_audit As Boolean = False
    Dim audit_file, no_changes_file As String
    Dim record As String = ""
    Dim filename, filename_id, header_id As String
    Dim date_error_file As String = ""
    Dim first_error As Boolean = True
    Dim first_change As Boolean = True
    Dim on_onestep, equity_check As Boolean
    Dim contrib_id As Double
    Dim flag_id As String
    Dim no_changes As Integer = 0
    Dim changes As Integer = 0
    Dim spaces As Integer = 250
    Dim test_date As Date
    Dim appeal_case As Boolean
    Dim applicant_id, summons_no, maat_id, first_name, surname, dob, invalid_dob, ni_no, outcome, outcome2, appeal_type, rep_status As String
    Dim mthly_contrib_amt_str, upfront_contrib_amt_str, income_contrib_cap_str, income_uplift_applied As String
    Dim case_type, in_court_custody, debt_addr1, debt_addr2, debt_addr3, debt_addr4, debt_postcode As String
    Dim postal_addr1, postal_addr2, postal_addr3, postal_addr4, postal_postcode, landline, mobile, email As String
    Dim equity_amt, eq_appl_equity_amt, eq_partner_equity_amt, cap_amt, asset_no, inc_ev_no As Integer
    Dim ci_code, ci_desc, case_type_code, rep_status_desc, appeal_type_desc As String
    Dim passport_result_code, passport_result_desc, passport_reason_code, passport_reason_desc As String
    Dim passport_date_completed As Date
    Dim motor_vehicle_ownership As String
    Dim registrations(100) As String
    Dim registration_no As Integer
    Dim has_partner, partner_first_name, partner_last_name, partner_nino, partner_emp_status, partner_emp_code As String
    Dim contrary_interest, disability_declaration, disability_description, disability_code, nfa, si, hardship_result As String
    Dim assessment_reason, assessment_code, eq_bedroom_count, undeclared_property, no_capital_declared, imprisoned, sentence_date_str As String
    Dim eq_declared_value, eq_declared_mortgage, eq_verified_mortgage, eq_declared_equity, eq_verified_market_value, total_capital_assets As Decimal
    Dim all_evidence_date, uplift_applied_date, uplift_removed_date, sentence_date As Date
    Dim effective_date, rep_status_date, committal_date, hardship_review_date, assessment_date, partner_dob, outcome_date As Date
    Dim sol_acc_code, sol_name As String
    Dim valid_recs As Integer = 0
    Dim new_appeal_cases As Integer = 0
    Dim rej_recs As Integer = 0
    Dim no_of_errors As Integer = 0
    Dim appeal_valid_recs As Integer = 0
    Dim appeal_rej_recs As Integer = 0
    Dim appeal_no_of_errors As Integer = 0
    Dim contrib_recs As Integer
    Dim equity_recs As Integer
    Dim eq_percent_owned_partner, eq_percent_owned_appl As Integer
    Dim cap_percent_owned_partner, cap_percent_owned_appl As Integer
    Dim asset_table(100, 7)
    Dim inc_evidence_table(50, 4) As String
    Dim letter_table(30) As letter_str
    Dim rep_order_withdrawal_date As Date
    Dim future_effective_date As Boolean
    Dim earlier_contrib_found As Boolean
    Dim mthly_contrib_amt, upfront_contrib_amt, income_contrib_cap As Decimal
    Dim comments2 As String
    Dim equity_amt_verified, cap_asset_type, cap_amt_verified, comments, new_comments, eq_residential_code, eq_residential_desc As String
    Dim allowable_cap_threshold, effective_date_str, pref_pay_method, pref_pay_code, pref_pay_day, eq_prop_type_code, eq_prop_type_desc As String
    Dim bank_acc_name, bank_acc_no, bank_sort_code, emp_status, emp_code, rep_order_withdrawal_date_str, hardship_appl_recvd As String
    Dim offence_type_code, offence_type_desc, mags_court_desc, mags_court_code, sufficient_declared_equity, sufficient_verified_equity, sufficient_capital_and_equity As String
    Dim equity_addr1, equity_addr2, equity_addr3, equity_postcode, equity_city, equity_country, eq_tenant_in_place As String
    Dim eq_third_party_name, eq_third_party_code, eq_third_party_desc, eq_third_party_other_desc, equity_verified_by, prop_equity_verified_by As String
    Dim eq_third_party_created_date, eq_third_party_deleted_date, equity_declared_date, equity_verified_date As Date
    Dim cap_residential_code, cap_residential_desc, cap_third_party_name, cap_third_party_code, cap_third_party_desc, cap_third_party_other_desc As String
    Dim cap_bedroom_count, cap_prop_type_code, cap_prop_type_desc, cap_tenant_in_place As String
    Dim cap_third_party_created_date, cap_third_party_deleted_date, cap_declared_date, cap_verified_date As Date
    Dim cap_addr1, cap_addr2, cap_addr3, cap_postcode, cap_city, cap_country, cap_verified_by As String
    Dim cap_appl_equity_amt, cap_partner_equity_amt As Integer
    Dim cap_declared_value, cap_declared_mortgage, cap_verified_mortgage, cap_declared_equity, cap_verified_market_value As Decimal
    Dim equity_props(100) As property_str
    Dim cap_props(100) As property_str
    Dim equity_tps(100, 100) As third_party_str
    Dim cap_tps(100, 100) As third_party_str
    Dim equity_prop_no As Integer = 0
    Dim cap_prop_no As Integer = 0
    Dim equity_tp_no As Integer = 0
    Dim cap_tp_no As Integer = 0
    Dim letter_no As Integer = 0
    Dim change_found As Boolean
    Private Sub readbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles readbtn.Click
        conn_open = False
        readbtn.Enabled = False
        exitbtn.Enabled = False
        Dim env_str As String = ""
        prod_run = False
        Try
            'Dim conf_str As String = ConfigurationManager.ConnectionStrings("PRODdatabase").ConnectionString
            'Dim conf_str As String = ConfigurationManager.ConnectionStrings("Feesdatabase").ConnectionString
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If
        'task 28841 check for any blank MAATIDS
        'first get all CSIDs for client=909
        Dim TestCSID As Integer = 909
        If Not prod_run Then
            TestCSID = 24
        End If
        param2 = "select _rowID from clientScheme " & _
            " where clientID = " & TestCSID
        Dim cs_ds As DataSet = get_dataset("onestep", param2)
        Dim CSrow As DataRow
        Dim blankCases As Integer = 0
        For Each CSrow In cs_ds.Tables(0).Rows
            Dim CSID As Integer = CSrow(0)
            'now check any open cases with blank MAATID
            param2 = "select _rowID from Debtor " & _
                " where clientschemeID = " & CSID & _
                " and status_open_closed = 'O' " & _
                " and trim(client_ref) = ''"
            Dim debt_ds As DataSet = get_dataset("onestep", param2)
            If no_of_rows > 0 Then
                'write out report
                Dim blankIDX As Integer
                Dim blankMAATFile As String = "DebtorID" & vbNewLine
                For blankIDX = 0 To no_of_rows - 1
                    blankMAATFile &= debt_ds.Tables(0).Rows(blankIDX).Item(0) & vbNewLine
                    blankCases += 1
                Next
                Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
                new_file = "H:blank_MAATIDs.txt"
                My.Computer.FileSystem.WriteAllText(new_file, blankMAATFile, False)

            End If
        Next
        If blankCases > 0 Then
            MsgBox("There are " & blankCases & " cases with blank MAAT IDS - see blank_MAATID report on H drive")
            If MsgBox("DO you want to continue?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Me.Close()
                Exit Sub
            End If
        End If
        Try
            With OpenFileDialog1
                .Title = "Read XML file"
                .Filter = "XML file|*.xml"
                .FileName = ""
                .CheckFileExists = True
            End With
            Dim xml_dataSet As New DataSet()
            Dim doc As New Xml.XmlDocument()

            Dim contrib_file As String = "Applicant ID" & "|" & "SummonsNum" & "|" & "MAAT ID" & _
                        "|" & "First name" & "|" & "Surname" & "|" & "Date of birth" & "|" & "NI No" & _
                        "|" & "Monthly Amount" & "|" & "Upfront Amt" & "|" & "Income contribution cap" & "|" & _
                        "Debt addr1" & "|" & "Debt addr2" & "|" & "Debt addr3" & "|" & "Debt addr4" & _
                        "|" & "Debt postcode" & "|" & "Curr addr1" & "|" & "Curr addr2" & _
                        "|" & "Curr addr3" & "|" & "Curr addr4" & "|" & "Curr postcode" & "|" & _
                        "Landline" & "|" & "Mobile" & "|" & "Email" & "|" & _
                         "Effective date" & "|" & "Outcome|SODate|Comments" & vbNewLine
            Dim appeal_file As String = contrib_file
            Dim equity_file As String = contrib_file


            no_changes_file = "Maat ID" & vbNewLine
            date_error_file = "DebtorID|Date Field|Invalid date" & vbNewLine
            input_file = contrib_file
            input_file2 = contrib_file


            Dim idx As Integer = 0

            If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                validate_xml_file()
                If xml_valid = False Then
                    MsgBox("XML file has failed validation - see error file")
                    If MsgBox("Do you want to continue processing the file?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                        Me.Close()
                        Exit Sub
                    End If
                End If
                Try
                    filename = OpenFileDialog1.FileName
                    Dim ln2 As Integer = Microsoft.VisualBasic.Len(filename)

                    new_file = Microsoft.VisualBasic.Left(filename, ln2 - 4) & "_error.txt"
                    My.Computer.FileSystem.WriteAllText(new_file, "Error messages " & Now, False)

                    new_file = Microsoft.VisualBasic.Left(filename, ln2 - 4) & "_changes.txt"
                    Dim change_message As String = "Debtor" & vbTab & "Maat ID" & vbTab & "Field" & _
                    vbTab & "Onestep Value" & vbTab & "New value" & vbNewLine
                    My.Computer.FileSystem.WriteAllText(new_file, change_message, False)
                    write_eff_date("debtor|effective_date" & vbNewLine)
                    reset_fields()
                    Dim rdr_name As String = ""
                    Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
                    reader.Read()
                    ProgressBar1.Value = 5
                    Dim record_count As Integer = 0

                    While (reader.ReadState <> Xml.ReadState.EndOfFile)
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        Try
                            rdr_name = reader.Name
                        Catch
                            Continue While
                        End Try

                        'Note ReadElementContentAsString moves focus to next element so don't need read
                        ProgressBar1.Value = record_count
                        Application.DoEvents()
                        Select Case rdr_name
                            Case "header"
                                header_id = reader.Item(0)
                                'get last header id

                                If prod_run = False Then
                                    MsgBox("TEST RUN ONLY - header and MAAT ID")
                                Else
                                    Me.LSC_preprocess_last_Header_IDTableAdapter.Fill(Me.FeesSQLDataSet1.LSC_preprocess_last_Header_ID)
                                    If Me.FeesSQLDataSet1.LSC_preprocess_last_Header_ID.Rows.Count = 0 Then
                                        Try
                                            Me.LSC_preprocess_last_Header_IDTableAdapter.InsertQuery(header_id, Now, log_user)
                                        Catch ex As Exception
                                            MsgBox(ex.Message)
                                        End Try
                                    Else
                                        Dim last_header_id As Integer = Me.FeesSQLDataSet1.LSC_preprocess_last_Header_ID.Rows(0).Item(0)
                                        Dim header_id_num As Integer = header_id
                                        If header_id_num < last_header_id Then
                                            MsgBox("Header ID on file is before last Header ID" & vbNewLine & _
                                            "This file Header ID is " & header_id_num & vbNewLine & _
                                            "Previous Header ID was " & last_header_id)
                                            If MsgBox("Do you want to carry on", MsgBoxStyle.YesNo, "Carry on regardless") = MsgBoxResult.No Then
                                                Me.Close()
                                                Exit Sub
                                            End If
                                        Else
                                            If header_id_num <> last_header_id Then
                                                Try
                                                    Me.LSC_preprocess_last_Header_IDTableAdapter.InsertQuery(header_id, Now, log_user)
                                                Catch ex As Exception
                                                    MsgBox(ex.Message)
                                                End Try
                                            End If
                                        End If
                                    End If
                                End If
                                reader.Read()
                            Case "filename"
                                filename_id = reader.ReadElementContentAsString
                            Case "CONTRIBUTIONS"
                                record_count += 1
                                If record_count > 100 Then
                                    record_count = 0
                                End If
                                If idx > 0 Then
                                    end_of_record()
                                    Try
                                        input_file = input_file & record & vbNewLine
                                    Catch ex As Exception
                                        input_file2 = input_file
                                        input_file = record & vbNewLine
                                    End Try

                                    If validate_record() = False Then
                                        'rej_recs += 1 ' TS 13/Feb/2013
                                        If appeal_case Then ' TS 13/Feb/2013
                                            appeal_rej_recs += 1 ' TS 13/Feb/2013
                                        Else ' TS 13/Feb/2013
                                            rej_recs += 1 ' TS 13/Feb/2013
                                        End If ' TS 13/Feb/2013
                                        audit_file = audit_file & maat_id & "|rejected" & vbNewLine
                                    Else
                                        If Not appeal_case Then
                                            valid_recs += 1
                                        Else  '23.10.2014 include even if on onestep already
                                            appeal_valid_recs += 1
                                        End If
                                        If on_onestep = False Then
                                            If appeal_case Then
                                                audit_file = audit_file & maat_id & "|appeal" & vbNewLine
                                                appeal_file = appeal_file & record & vbNewLine
                                                new_appeal_cases += 1
                                            ElseIf equity_check = True Then
                                                equity_recs += 1
                                                audit_file = audit_file & maat_id & "|equity" & vbNewLine
                                                equity_file = equity_file & record & vbNewLine
                                            Else
                                                contrib_recs += 1
                                                audit_file = audit_file & maat_id & "|contrib" & vbNewLine
                                                contrib_file = contrib_file & record & vbNewLine
                                            End If
                                        End If
                                    End If
                                    reset_fields()
                                End If
                                idx += 1
                                contrib_id = reader.Item(0)
                                flag_id = ""
                                flag_id = reader.Item(1)
                                reader.Read()
                            Case "maat_id"
                                appeal_case = False
                                maat_id = reader.ReadElementContentAsString
                                asset_no = 0
                                inc_ev_no = 0
                                cap_prop_no = 0
                                equity_prop_no = 0
                                cap_tp_no = 0
                                equity_tp_no = 0
                                letter_no = 0
                                registration_no = 0
                                'get last contrib id
                                If prod_run Then
                                    Dim last_contrib_id As Integer = 0
                                    Me.LSC_preprocess_last_IDTableAdapter1.FillBy(Me.FeesSQLDataSet1.LSC_preprocess_last_ID, maat_id)
                                    If Me.FeesSQLDataSet1.LSC_preprocess_last_ID.Rows.Count > 0 Then
                                        last_contrib_id = Me.FeesSQLDataSet1.LSC_preprocess_last_ID.Rows(0).Item(1)
                                    End If
                                    earlier_contrib_found = False
                                    If contrib_id < last_contrib_id And last_contrib_id > 0 Then
                                        earlier_contrib_found = True
                                    End If
                                    If earlier_contrib_found = False Then
                                        Try
                                            Me.LSC_preprocess_last_IDTableAdapter1.InsertQuery(maat_id, contrib_id, Now, log_user)
                                        Catch ex As Exception
                                            Try
                                                Me.LSC_preprocess_last_IDTableAdapter1.DeleteQuery(maat_id)
                                                Me.LSC_preprocess_last_IDTableAdapter1.InsertQuery(maat_id, contrib_id, Now, log_user)
                                            Catch ex2 As Exception
                                                MsgBox(ex2.Message)
                                            End Try
                                        End Try
                                    End If
                                End If

                            Case "applicant"
                                applicant_id = reader.Item(0)
                                reader.Read()
                            Case "firstName"
                                first_name = reader.ReadElementContentAsString
                            Case "lastName"
                                surname = reader.ReadElementContentAsString
                            Case "dob"
                                dob = reader.ReadElementContentAsString
                                Try
                                    Dim dob_date As Date = dob
                                    dob = Format(dob_date, "dd-MM-yyyy")
                                Catch ex As Exception
                                    invalid_dob = dob
                                    dob = Nothing
                                End Try
                            Case "ni_number"
                                ni_no = reader.ReadElementContentAsString
                            Case "landline"
                                landline = reader.ReadElementContentAsString
                            Case "mobile"
                                mobile = reader.ReadElementContentAsString
                            Case "email"
                                email = reader.ReadElementContentAsString
                            Case "noFixedAbode"
                                nfa = reader.ReadElementContentAsString
                                new_comments = "No Fixed Abode:" & nfa & ";"
                                remove_nonascii_chars(new_comments)
                                comments &= new_comments
                                space_comments()
                            Case "specialInvestigation"
                                si = reader.ReadElementContentAsString
                                new_comments = "Special Investigation:" & si & ";"
                                remove_nonascii_chars(new_comments)
                                comments &= new_comments
                                space_comments()
                            Case "homeAddress"
                                reader.Read()
                                While reader.Name <> "homeAddress"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "line1"
                                            debt_addr1 = reader.ReadElementContentAsString
                                        Case "line2"
                                            debt_addr2 = reader.ReadElementContentAsString
                                        Case "line3"
                                            debt_addr3 = reader.ReadElementContentAsString
                                        Case "city"
                                            debt_addr4 = reader.ReadElementContentAsString
                                        Case "postcode"
                                            debt_postcode = reader.ReadElementContentAsString
                                        Case "country"
                                            Dim country As String = reader.ReadElementContentAsString
                                            If country <> "GB" Then
                                                debt_addr4 = debt_addr4 & " " & Trim(country)
                                            End If
                                        Case "detail"
                                            reader.Read()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "postalAddress"
                                reader.Read()
                                While reader.Name <> "postalAddress"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "line1"
                                            postal_addr1 = reader.ReadElementContentAsString
                                        Case "line2"
                                            postal_addr2 = reader.ReadElementContentAsString
                                        Case "line3"
                                            postal_addr3 = reader.ReadElementContentAsString
                                        Case "city"
                                            postal_addr4 = reader.ReadElementContentAsString
                                        Case "postcode"
                                            postal_postcode = reader.ReadElementContentAsString
                                        Case "country"
                                            Dim country As String = reader.ReadElementContentAsString
                                            If country <> "GB" Then
                                                postal_addr4 = postal_addr4 & " " & Trim(country)
                                            End If
                                        Case "detail"
                                            reader.Read()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "offenceType"
                                reader.Read()
                                While reader.Name <> "offenceType"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "description"
                                            offence_type_desc = reader.ReadElementContentAsString
                                            remove_strange_chrs(offence_type_desc)
                                            comments = comments & "Offence Type Desc:" & offence_type_desc & ";"
                                            space_comments()
                                        Case "code"
                                            offence_type_code = reader.ReadElementContentAsString
                                            new_comments = "Offence Type Code:" & offence_type_code & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "magsCourt"
                                reader.Read()
                                While reader.Name <> "magsCourt"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "description"
                                            mags_court_desc = reader.ReadElementContentAsString
                                            new_comments = "Mags Court Desc:" & mags_court_desc & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "court"
                                            mags_court_code = reader.ReadElementContentAsString
                                            new_comments = "Mags Court Code:" & mags_court_code & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "employmentStatus"
                                reader.Read()
                                While reader.Name <> "employmentStatus"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "description"
                                            emp_status = reader.ReadElementContentAsString
                                            new_comments = "Emp status:" & emp_status & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "code"
                                            emp_code = reader.ReadElementContentAsString
                                            new_comments = "Emp Code:" & emp_code & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "partner"
                                reader.Read()
                                While reader.Name <> "partner"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "hasPartner"
                                            has_partner = reader.ReadElementContentAsString
                                            new_comments = "Has Partner:" & has_partner & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "contraryInterest"
                                            contrary_interest = reader.ReadElementContentAsString
                                            new_comments = "Contrary Interest:" & contrary_interest & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "ciDetails"
                                            reader.Read()
                                            While reader.Name <> "ciDetails"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "code"
                                                        ci_code = reader.ReadElementContentAsString
                                                        new_comments = "Contrary Interest Code:" & ci_code & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "description"
                                                        ci_desc = reader.ReadElementContentAsString
                                                        new_comments = "Contrary Interest Description:" & ci_desc & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case Else
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "partnerDetails"
                                reader.Read()
                                While reader.Name <> "partnerDetails"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "firstName"
                                            partner_first_name = reader.ReadElementContentAsString
                                            new_comments = "Partner First Name:" & partner_first_name & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "lastName"
                                            partner_last_name = reader.ReadElementContentAsString
                                            new_comments = "Partner Last Name:" & partner_last_name & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "dob"
                                            partner_dob = reader.ReadElementContentAsString
                                            new_comments = "Partner DOB:" & Format(partner_dob, "dd/MM/yyyy") & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "niNumber"
                                            partner_nino = reader.ReadElementContentAsString
                                            new_comments = "Partner NINO:" & partner_nino & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "employmentStatus"
                                            reader.Read()
                                            While reader.Name <> "employmentStatus"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "description"
                                                        partner_emp_status = reader.ReadElementContentAsString
                                                        new_comments = "Emp Status Partner:" & partner_emp_status & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "code"
                                                        partner_emp_code = reader.ReadElementContentAsString
                                                        new_comments = comments & "Emp Code Partner:" & partner_emp_code & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case Else
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                                'Case "PassportType"
                            Case "passported"
                                reader.Read()
                                While reader.Name <> "passported" And reader.Name <> "equity"
                                    ' While reader.Name <> "PassportType"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "result"
                                            reader.Read()
                                            While reader.Name <> "result"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "code"
                                                        passport_result_code = reader.ReadElementContentAsString
                                                        new_comments = "Passport Result Code:" & passport_result_code & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "description"
                                                        passport_result_desc = reader.ReadElementContentAsString
                                                        new_comments = "Passport Result Desc:" & passport_result_desc & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case "date_completed"
                                            passport_date_completed = reader.ReadElementContentAsString
                                            new_comments = "Passport Date Completed:" & Format(passport_date_completed, "dd/MM/yyyy") & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "reason"
                                            reader.Read()
                                            While reader.Name <> "reason"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "code"
                                                        passport_reason_code = reader.ReadElementContentAsString
                                                        new_comments = "Passport Reason Code:" & passport_reason_code & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "description"
                                                        passport_reason_desc = reader.ReadElementContentAsString
                                                        new_comments = "Passport Reason Desc:" & passport_reason_desc & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "disabilitySummary"
                                reader.Read()
                                While reader.Name <> "disabilitySummary"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "declaration"
                                            disability_declaration = reader.ReadElementContentAsString
                                            new_comments = "Disability Declaration:" & disability_declaration & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "disability"
                                            reader.Read()
                                            While reader.Name <> "disability"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "description"
                                                        disability_description = reader.ReadElementContentAsString
                                                        new_comments = "Disability Description:" & disability_description & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "code"
                                                        disability_code = reader.ReadElementContentAsString
                                                        new_comments = "Disability Code:" & disability_code & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case "disabilities"
                                            reader.Read()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "caseType"
                                reader.Read()
                                While reader.Name <> "caseType"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "description"
                                            case_type = Trim(reader.ReadElementContentAsString)
                                            new_comments = "Case type:" & case_type & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                            'If UCase(case_type) = "APPEAL CC" Then ' Changed from appeal TS 05/Feb/2013
                                            '    appeal_case = True
                                            'End If
                                        Case "code"
                                            case_type_code = reader.ReadElementContentAsString
                                            new_comments = "Case Type Code:" & case_type_code & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                            If UCase(case_type_code) = "APPEAL CC" Then ' Changed from appeal TS 05/Feb/2013
                                                appeal_case = True
                                            End If
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "repStatus"
                                reader.Read()
                                While reader.Name <> "repStatus"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "status"
                                            rep_status = reader.ReadElementContentAsString
                                            new_comments = new_comments & "Rep Status:" & rep_status & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "description"
                                            rep_status_desc = reader.ReadElementContentAsString
                                            new_comments = "Rep Status Description:" & rep_status_desc & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "repStatusDate"
                                rep_status_date = reader.ReadElementContentAsString
                                new_comments = "Rep Status Date:" & Format(rep_status_date, "dd/MM/yyyy") & ";"
                                remove_nonascii_chars(new_comments)
                                comments &= new_comments
                                space_comments()
                            Case "appealType"
                                ' This section uncommented TS 04/02/2013
                                reader.Read()
                                While reader.Name <> "appealType"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "code"
                                            appeal_type = reader.ReadElementContentAsString
                                            new_comments = "Appeal Type Code:" & appeal_type & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "description"
                                            appeal_type_desc = reader.ReadElementContentAsString
                                            new_comments = "Appeal Type Description:" & appeal_type_desc & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "arrestSummonsNumber"
                                summons_no = reader.ReadElementContentAsString
                            Case "inCourtCustody"
                                in_court_custody = reader.ReadElementContentAsString
                                If in_court_custody.Length > 0 Then
                                    If LCase(Microsoft.VisualBasic.Left(in_court_custody, 1)) = "n" Then
                                        in_court_custody = "No"
                                    End If
                                    If LCase(Microsoft.VisualBasic.Left(in_court_custody, 1)) = "y" Then
                                        in_court_custody = "Yes"
                                    End If
                                    comments = comments & "In Court Custody:" & in_court_custody & ";"
                                    space_comments()
                                End If
                            Case "imprisoned"
                                imprisoned = reader.ReadElementContentAsString
                                If imprisoned.Length > 0 Then
                                    If LCase(Microsoft.VisualBasic.Left(imprisoned, 1)) = "n" Then
                                        imprisoned = "No"
                                    End If
                                    If LCase(Microsoft.VisualBasic.Left(imprisoned, 1)) = "y" Then
                                        imprisoned = "Yes"
                                    End If
                                    comments = comments & "InPrison:" & imprisoned & ";"
                                    space_comments()
                                End If
                            Case "sentenceDate"
                                sentence_date_str = reader.ReadElementContentAsString
                                Try
                                    sentence_date = CDate(sentence_date_str)
                                    comments = comments & "SentenceOrderDate:" & Format(sentence_date, "dd/MM/yyyy") & ";"
                                    space_comments()
                                Catch ex As Exception
                                    sentence_date = Nothing
                                End Try
                            Case "committalDate"
                                committal_date = reader.ReadElementContentAsString
                                comments = comments & "Committal Date:" & Format(committal_date, "dd/MM/yyyy") & ";"
                                space_comments()
                            Case "solicitor"
                                reader.Read()
                                While reader.Name <> "solicitor"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "accountCode"
                                            sol_acc_code = reader.ReadElementContentAsString
                                            new_comments = "Solicitor account code:" & sol_acc_code & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "name"
                                            sol_name = reader.ReadElementContentAsString
                                            new_comments = "Solicitor name:" & sol_name & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "sufficientEquity"
                                MsgBox("Contact Jeff - sufficientEquity - found on file")
                            Case "sufficientCapital"
                                MsgBox("Contact Jeff - sufficientCapital - found on file")
                            Case "ccHardshipReceived"
                                MsgBox("Contact Jeff - ccHardshipReceived - found on file")
                            Case "ccHardship"
                                reader.Read()
                                While reader.Name <> "ccHardship"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "reviewDate"
                                            hardship_review_date = reader.ReadElementContentAsString
                                            comments = comments & "Hardship Review Date:" & Format(hardship_review_date, "dd/MM/yyyy") & ";"
                                            space_comments()
                                        Case "reviewResult"
                                            hardship_result = reader.ReadElementContentAsString
                                            new_comments = "Hardship Review Result:" & hardship_result & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "effectiveDate"
                                effective_date_str = reader.ReadElementContentAsString
                                Try
                                    effective_date = effective_date_str
                                Catch ex As Exception
                                    effective_date = Nothing
                                End Try
                                'if effective date in the future - add to notes 
                                If effective_date <> Nothing Then
                                    If effective_date > Now Then
                                        future_effective_date = True
                                        comments = comments & "Effective Date:" & Format(effective_date, "dd/MM/yyyy") & ";"
                                        space_comments()
                                    End If
                                End If
                            Case "monthlyContribution"
                                mthly_contrib_amt_str = reader.ReadElementContentAsString
                                mthly_contrib_amt = mthly_contrib_amt_str      ' pounds not pence/ 100
                            Case "upfrontContribution"
                                upfront_contrib_amt_str = reader.ReadElementContentAsString
                                upfront_contrib_amt = upfront_contrib_amt_str     'pounds not pence/ 100
                            Case "incomeContributionCap"
                                income_contrib_cap_str = reader.ReadElementContentAsString
                                income_contrib_cap = income_contrib_cap_str      'pounds not pence/ 100
                            Case "assessmentReason"
                                reader.Read()
                                While reader.Name <> "assessmentReason"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "description"
                                            assessment_reason = reader.ReadElementContentAsString
                                            new_comments = "Assessment Reason:" & assessment_reason & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "code"
                                            assessment_code = reader.ReadElementContentAsString
                                            new_comments = "Assessment Code:" & assessment_code & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "assessmentDate"
                                assessment_date = reader.ReadElementContentAsString
                                comments = comments & "Assessment Date:" & Format(assessment_date, "dd/MM/yyyy") & ";"
                                space_comments()
                            Case "sufficientDeclaredEquity"
                                sufficient_declared_equity = reader.ReadElementContentAsString
                                new_comments = "Sufficient Declared Equity:" & sufficient_declared_equity & ";"
                                remove_nonascii_chars(new_comments)
                                comments &= new_comments
                                space_comments()
                            Case "sufficientVerifiedEquity"
                                sufficient_verified_equity = reader.ReadElementContentAsString
                                new_comments = "Sufficient Verified Equity:" & sufficient_verified_equity & ";"
                                remove_nonascii_chars(new_comments)
                                comments &= new_comments
                                space_comments()
                            Case "sufficientCapitalandEquity"
                                sufficient_capital_and_equity = reader.ReadElementContentAsString
                                new_comments = "Sufficient Capital and Equity:" & sufficient_capital_and_equity & ";"
                                remove_nonascii_chars(new_comments)
                                comments &= new_comments
                                space_comments()
                            Case "equity"
                                reader.Read()
                                While reader.Name <> "equity" And reader.Name <> "capitalSummary"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "undeclaredProperty"
                                            undeclared_property = reader.ReadElementContentAsString
                                            new_comments = "Undeclared Property:" & undeclared_property & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "equityVerified"
                                            equity_amt_verified = LCase(reader.ReadElementContentAsString)
                                            If Microsoft.VisualBasic.Left(equity_amt_verified, 1) = "n" Then
                                                equity_amt_verified = "No"
                                            End If
                                            If Microsoft.VisualBasic.Left(equity_amt_verified, 1) = "y" Then
                                                equity_amt_verified = "Yes"
                                            End If
                                            comments = comments & "Equity Amt Verified:" & equity_amt_verified & ";"
                                            space_comments()
                                        Case "equityVerifiedDate"
                                            equity_verified_date = reader.ReadElementContentAsString
                                            comments = comments & "Equity Verified Date:" & equity_verified_date & ";"
                                            space_comments()
                                        Case "equityVerifiedBy"
                                            equity_verified_by = reader.ReadElementContentAsString
                                            new_comments = "Equity Verified By:" & equity_verified_by & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "propertyDescriptor"
                                            reader.Read()
                                            equity_prop_no += 1
                                            Try
                                                equity_props(equity_prop_no) = Nothing
                                            Catch ex As Exception
                                                ReDim Preserve equity_props(equity_prop_no + 10)
                                                equity_props(equity_prop_no) = Nothing
                                            End Try
                                            equity_props(equity_prop_no).partner_equity_amt = -999
                                            equity_props(equity_prop_no).appl_equity_amt = -999
                                            equity_props(equity_prop_no).declared_mortgage = -999
                                            equity_props(equity_prop_no).declared_value = -999
                                            equity_props(equity_prop_no).declared_equity = -999
                                            equity_props(equity_prop_no).verified_value = -999
                                            equity_props(equity_prop_no).verified_mortgage = -999
                                            While reader.Name <> "propertyDescriptor"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "residentialStatus"
                                                        reader.Read()
                                                        While reader.Name <> "residentialStatus"
                                                            rdr_name = reader.Name
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            Select Case rdr_name
                                                                Case "code"
                                                                    eq_residential_code = reader.ReadElementContentAsString
                                                                    equity_props(equity_prop_no).residential_code = eq_residential_code
                                                                    new_comments = "Prop " & equity_prop_no & " Residential Code Equity:" & Trim(eq_residential_code) & ";"
                                                                    remove_nonascii_chars(new_comments)
                                                                    comments &= new_comments
                                                                    space_comments()
                                                                Case "description"
                                                                    eq_residential_desc = reader.ReadElementContentAsString
                                                                    equity_props(equity_prop_no).residential_desc = eq_residential_desc
                                                                    new_comments = "Prop " & equity_prop_no & " Residential Description Equity:" & Trim(eq_residential_desc) & ";"
                                                                    remove_nonascii_chars(new_comments)
                                                                    comments &= new_comments
                                                                    space_comments()
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                    reader.Read()
                                                            End Select
                                                        End While
                                                    Case "propertyType"
                                                        reader.Read()
                                                        While reader.Name <> "propertyType"
                                                            rdr_name = reader.Name
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            Select Case rdr_name
                                                                Case "code"
                                                                    eq_prop_type_code = reader.ReadElementContentAsString
                                                                    equity_props(equity_prop_no).prop_type_code = eq_prop_type_code
                                                                    new_comments = "Prop " & equity_prop_no & " Property Type Code Equity:" & Trim(eq_prop_type_code) & ";"
                                                                    remove_nonascii_chars(new_comments)
                                                                    comments &= new_comments
                                                                    space_comments()
                                                                Case "description"
                                                                    eq_prop_type_desc = reader.ReadElementContentAsString
                                                                    equity_props(equity_prop_no).prop_type_desc = eq_prop_type_desc
                                                                    new_comments = "Prop " & equity_prop_no & " Property Type Description Equity:" & Trim(eq_prop_type_desc) & ";"
                                                                    remove_nonascii_chars(new_comments)
                                                                    comments &= new_comments
                                                                    space_comments()
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                    reader.Read()
                                                            End Select
                                                        End While
                                                    Case "thirdPartyList"
                                                        reader.Read()
                                                        While reader.Name <> "thirdPartyList"
                                                            rdr_name = reader.Name
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            Select Case rdr_name
                                                                Case "thirdParty"
                                                                    reader.Read()
                                                                    equity_tp_no += 1
                                                                    Try
                                                                        equity_tps(equity_prop_no, equity_tp_no).created_date = Nothing
                                                                        equity_tps(equity_prop_no, equity_tp_no).deleted_date = Nothing
                                                                        equity_tps(equity_prop_no, equity_tp_no).name = Nothing
                                                                        equity_tps(equity_prop_no, equity_tp_no).other_desc = Nothing
                                                                    Catch ex As Exception
                                                                        MsgBox("Increase size of equity_tps table")
                                                                    End Try

                                                                    While reader.Name <> "thirdParty"
                                                                        rdr_name = reader.Name
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        Select Case rdr_name
                                                                            Case "name"
                                                                                eq_third_party_name = reader.ReadElementContentAsString
                                                                                equity_tps(equity_prop_no, equity_tp_no).name = eq_third_party_name
                                                                                new_comments = "Prop " & equity_prop_no & " TP " & equity_tp_no & " name Equity:" & eq_third_party_name & ";"
                                                                                remove_nonascii_chars(new_comments)
                                                                                comments &= new_comments
                                                                                space_comments()
                                                                            Case "otherDescription"
                                                                                eq_third_party_other_desc = reader.ReadElementContentAsString
                                                                                '22.4.2013 take out carriage return characters
                                                                                remove_chrs(eq_third_party_other_desc)
                                                                                equity_tps(equity_prop_no, equity_tp_no).other_desc = eq_third_party_other_desc
                                                                                If eq_third_party_other_desc.Length > 0 Then
                                                                                    new_comments = "Prop " & equity_prop_no & " TP " & equity_tp_no & " Other Desc Equity:" & eq_third_party_other_desc & ";"
                                                                                    remove_nonascii_chars(new_comments)
                                                                                    comments &= new_comments
                                                                                    space_comments()
                                                                                End If
                                                                            Case "dateCreated"
                                                                                eq_third_party_created_date = reader.ReadElementContentAsString
                                                                                equity_tps(equity_prop_no, equity_tp_no).created_date = eq_third_party_created_date
                                                                                comments = comments & "Prop " & equity_prop_no & " TP " & equity_tp_no & " Created Date Equity:" & eq_third_party_created_date & ";"
                                                                                space_comments()
                                                                            Case "dateDeleted"
                                                                                eq_third_party_deleted_date = reader.ReadElementContentAsString
                                                                                equity_tps(equity_prop_no, equity_tp_no).deleted_date = eq_third_party_deleted_date
                                                                                comments = comments & "Prop " & equity_prop_no & " TP " & equity_tp_no & " Deleted Date Equity:" & eq_third_party_deleted_date & ";"
                                                                                space_comments()
                                                                            Case "relationship"
                                                                                reader.Read()
                                                                                While reader.Name <> "relationship"
                                                                                    rdr_name = reader.Name
                                                                                    If reader.NodeType > 1 Then
                                                                                        reader.Read()
                                                                                        Continue While
                                                                                    End If
                                                                                    Select Case rdr_name
                                                                                        Case "code"
                                                                                            eq_third_party_code = reader.ReadElementContentAsString
                                                                                            equity_tps(equity_prop_no, equity_tp_no).rel_code = eq_third_party_code
                                                                                            new_comments = "Prop " & equity_prop_no & " TP " & equity_tp_no & " Relationship Code Equity:" & eq_third_party_code & ";"
                                                                                            remove_nonascii_chars(new_comments)
                                                                                            comments &= new_comments
                                                                                            space_comments()
                                                                                        Case "description"
                                                                                            eq_third_party_desc = reader.ReadElementContentAsString
                                                                                            equity_tps(equity_prop_no, equity_tp_no).rel_desc = eq_third_party_desc
                                                                                            new_comments = "Prop " & equity_prop_no & " TP " & equity_tp_no & " Relationship Desc Equity:" & eq_third_party_desc & ";"
                                                                                            remove_nonascii_chars(new_comments)
                                                                                            comments &= new_comments
                                                                                            space_comments()
                                                                                        Case Else
                                                                                            MsgBox("What is this tag?" & rdr_name)
                                                                                            reader.Read()
                                                                                    End Select
                                                                                End While
                                                                            Case Else
                                                                                MsgBox("What is this tag?" & rdr_name)
                                                                                reader.Read()
                                                                        End Select
                                                                    End While
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                    reader.Read()
                                                            End Select
                                                        End While
                                                    Case "bedRoomCount"
                                                        eq_bedroom_count = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).bedroom_count = eq_bedroom_count
                                                        new_comments = "Prop " & equity_prop_no & " Bedroom Count Equity:" & eq_bedroom_count & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "line1"
                                                        equity_addr1 = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).addr_line1 = equity_addr1
                                                        If equity_addr1.Length > 0 Then
                                                            new_comments = "Prop " & equity_prop_no & " Address Line1 Equity:" & equity_addr1 & ";"
                                                            remove_nonascii_chars(new_comments)
                                                            comments &= new_comments
                                                            space_comments()
                                                        End If
                                                    Case "line2"
                                                        equity_addr2 = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).addr_line2 = equity_addr2
                                                        If equity_addr2.Length > 0 Then
                                                            new_comments = "Prop " & equity_prop_no & " Address Line2 Equity:" & equity_addr2 & ";"
                                                            remove_nonascii_chars(new_comments)
                                                            comments &= new_comments
                                                            space_comments()
                                                        End If
                                                    Case "line3"
                                                        equity_addr3 = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).addr_line3 = equity_addr3
                                                        If equity_addr3.Length > 0 Then
                                                            new_comments = "Prop " & equity_prop_no & " Address Line3 Equity:" & equity_addr3 & ";"
                                                            remove_nonascii_chars(new_comments)
                                                            comments &= new_comments
                                                            space_comments()
                                                        End If
                                                    Case "city"
                                                        equity_city = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).addr_city = equity_city
                                                        If equity_city.Length > 0 Then
                                                            new_comments = "Prop " & equity_prop_no & " Address city Equity:" & equity_city & ";"
                                                            remove_nonascii_chars(new_comments)
                                                            comments &= new_comments
                                                            space_comments()
                                                        End If
                                                    Case "postcode"
                                                        equity_postcode = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).addr_pcode = equity_postcode
                                                        If equity_postcode.Length > 0 Then
                                                            new_comments = "Prop " & equity_prop_no & " Address Postcode Equity:" & equity_postcode & ";"
                                                            remove_nonascii_chars(new_comments)
                                                            comments &= new_comments
                                                            space_comments()
                                                        End If
                                                    Case "country"
                                                        equity_country = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).addr_country = equity_country
                                                        If equity_country.Length > 0 Then
                                                            new_comments = "Prop " & equity_prop_no & " Address Country Equity:" & equity_country & ";"
                                                            remove_nonascii_chars(new_comments)
                                                            comments &= new_comments
                                                            space_comments()
                                                        End If
                                                    Case "percentageApplicantOwned"
                                                        eq_percent_owned_appl = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).percent_appl = eq_percent_owned_appl
                                                        new_comments = "Prop " & equity_prop_no & " Percent Owned Applicant Equity:" & eq_percent_owned_appl & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "percentagePartnerOwned"
                                                        eq_percent_owned_partner = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).percent_partner = eq_percent_owned_partner
                                                        new_comments = "Prop " & equity_prop_no & " Percent Owned Partner Equity:" & eq_percent_owned_partner & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "applicantEquityAmount"
                                                        eq_appl_equity_amt = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).appl_equity_amt = eq_appl_equity_amt
                                                        new_comments = "Prop " & equity_prop_no & " Applicant Equity Amount Equity:" & eq_appl_equity_amt & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "partnerEquityAmount"
                                                        eq_partner_equity_amt = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).partner_equity_amt = eq_partner_equity_amt
                                                        new_comments = "Prop " & equity_prop_no & " Partner Equity Amount Equity:" & eq_partner_equity_amt & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "declaredMortgage"
                                                        eq_declared_mortgage = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).declared_mortgage = eq_declared_mortgage
                                                        new_comments = "Prop " & equity_prop_no & " Declared Mortgage Equity:" & eq_declared_mortgage & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "declaredValue"
                                                        eq_declared_value = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).declared_value = eq_declared_value
                                                        new_comments = "Prop " & equity_prop_no & " Declared Value Equity:" & eq_declared_value & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "declaredEquity"
                                                        eq_declared_equity = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).declared_equity = eq_declared_equity
                                                        new_comments = "Prop " & equity_prop_no & " Declared Equity Equity:" & eq_declared_equity & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "declaredDate"
                                                        equity_declared_date = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).declared_date = equity_declared_date
                                                        new_comments = "Prop " & equity_prop_no & " Declared Date Equity:" & equity_declared_date & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "verifiedValue"
                                                        eq_verified_market_value = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).verified_value = eq_verified_market_value
                                                        new_comments = "Prop " & equity_prop_no & " Verified Value Equity:" & eq_verified_market_value & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "verifiedMortgage"
                                                        eq_verified_mortgage = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).verified_mortgage = eq_verified_mortgage
                                                        comments = comments & "Prop " & equity_prop_no & " Verified Mortgage Equity:" & eq_verified_mortgage & ";"
                                                        space_comments()
                                                    Case "verifiedDate"
                                                        equity_props(equity_prop_no).verified_date = reader.ReadElementContentAsString
                                                        new_comments = "Prop " & equity_prop_no & " Verified Date Equity:" & equity_props(equity_prop_no).verified_date & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "verifiedBy"
                                                        prop_equity_verified_by = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).verified_by = prop_equity_verified_by
                                                        new_comments = "Prop " & equity_prop_no & " Verified By Equity:" & prop_equity_verified_by & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "tenantInPlace"
                                                        eq_tenant_in_place = reader.ReadElementContentAsString
                                                        equity_props(equity_prop_no).tenant_in_place = eq_tenant_in_place
                                                        new_comments = "Prop " & equity_prop_no & " Tenant in Place Equity:" & eq_tenant_in_place & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "address"
                                                        reader.Read()
                                                    Case "detail"
                                                        reader.Read()
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "capitalSummary"
                                reader.Read()
                                While reader.Name <> "capitalSummary"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "motorVehicleOwnership"
                                            reader.Read()
                                            While reader.Name <> "motorVehicleOwnership"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "Owner"
                                                        motor_vehicle_ownership = reader.ReadElementContentAsString
                                                        new_comments = "Motor Vehicle Ownership:" & motor_vehicle_ownership & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "RegistrationList"
                                                        reader.Read()
                                                        While reader.Name <> "RegistrationList"
                                                            rdr_name = reader.Name
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            Select Case rdr_name
                                                                Case "Registration"
                                                                    registration_no += 1
                                                                    registrations(registration_no) = reader.ReadElementContentAsString
                                                                    new_comments = "Registration " & registration_no & ":" & registrations(registration_no) & ";"
                                                                    remove_nonascii_chars(new_comments)
                                                                    comments &= new_comments
                                                                    space_comments()
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                    reader.Read()
                                                            End Select
                                                        End While
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case "noCapitalDeclared"
                                            no_capital_declared = reader.ReadElementContentAsString
                                            new_comments = "No Capital Declared:" & no_capital_declared & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "additionalProperties"
                                            new_comments = "Additional Properties:" & reader.ReadElementContentAsString & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "undeclaredProperties"
                                            new_comments = "Undeclared Properties:" & reader.ReadElementContentAsString & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "totalCapitalAssets"
                                            total_capital_assets = reader.ReadElementContentAsString
                                            new_comments = "Total Capital Assets:" & total_capital_assets & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "allEvidenceDate"
                                            all_evidence_date = reader.ReadElementContentAsString
                                            comments = comments & "All Evidence Date:" & Format(all_evidence_date, "dd/MM/yyyy") & ";"
                                            space_comments()
                                        Case "capAllowanceWithheld"
                                            new_comments = "Cap Allowance Withheld:" & reader.ReadElementContentAsString & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "capAllowanceRestore"
                                            new_comments = "Cap Allowance Restore:" & reader.ReadElementContentAsString & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "assetList"
                                            reader.Read()
                                            While reader.Name <> "assetList"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "code"
                                                        asset_no += 1
                                                        Try
                                                            asset_table(asset_no, 7) = reader.ReadElementContentAsString
                                                        Catch ex As Exception
                                                            MsgBox("asset table dimension requires increasing")
                                                            End
                                                        End Try
                                                        asset_table(asset_no, 1) = ""
                                                        asset_table(asset_no, 2) = ""
                                                        asset_table(asset_no, 3) = ""
                                                        asset_table(asset_no, 4) = ""
                                                        asset_table(asset_no, 5) = ""
                                                        asset_table(asset_no, 6) = ""
                                                    Case "description"
                                                        Dim temp As String = reader.ReadElementContentAsString
                                                        remove_chrs(temp)
                                                        asset_table(asset_no, 1) = temp
                                                    Case "amount"
                                                        asset_table(asset_no, 2) = reader.ReadElementContentAsString
                                                    Case "verified"
                                                        cap_amt_verified = reader.ReadElementContentAsString
                                                        remove_chrs(cap_amt_verified)
                                                        asset_table(asset_no, 3) = cap_amt_verified
                                                    Case "dateVerified"
                                                        test_date = reader.ReadElementContentAsString
                                                        asset_table(asset_no, 4) = Format(test_date, "dd/MM/yyyy")
                                                    Case "evidenceReceivedDate"
                                                        test_date = reader.ReadElementContentAsString
                                                        asset_table(asset_no, 5) = Format(test_date, "dd/MM/yyyy")
                                                    Case "otherDescription"
                                                        Dim temp As String = reader.ReadElementContentAsString
                                                        remove_chrs(temp)
                                                        asset_table(asset_no, 6) = temp
                                                    Case "asset"
                                                        reader.Read()
                                                    Case "type"
                                                        reader.Read()
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case "propertyList"
                                            reader.Read()
                                            While reader.Name <> "propertyList"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                Select Case rdr_name
                                                    Case "property"
                                                        cap_prop_no += 1
                                                        Try
                                                            cap_props(cap_prop_no) = Nothing
                                                        Catch ex As Exception
                                                            ReDim Preserve cap_props(cap_prop_no + 10)
                                                            cap_props(cap_prop_no) = Nothing
                                                        End Try

                                                        cap_props(cap_prop_no).partner_equity_amt = -999
                                                        cap_props(cap_prop_no).appl_equity_amt = -999
                                                        cap_props(cap_prop_no).declared_mortgage = -999
                                                        cap_props(cap_prop_no).declared_value = -999
                                                        cap_props(cap_prop_no).declared_equity = -999
                                                        cap_props(cap_prop_no).verified_value = -999
                                                        cap_props(cap_prop_no).verified_mortgage = -999
                                                        reader.Read()
                                                    Case "residentialStatus"
                                                        reader.Read()
                                                        While reader.Name <> "residentialStatus"
                                                            rdr_name = reader.Name
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            Select Case rdr_name
                                                                Case "code"
                                                                    cap_residential_code = reader.ReadElementContentAsString
                                                                    cap_props(cap_prop_no).residential_code = cap_residential_code
                                                                    new_comments = "Prop " & cap_prop_no & " & Residential Code Capital:" & Trim(cap_residential_code) & ";"
                                                                    remove_nonascii_chars(new_comments)
                                                                    comments &= new_comments
                                                                    space_comments()
                                                                Case "description"
                                                                    cap_residential_desc = reader.ReadElementContentAsString
                                                                    cap_props(cap_prop_no).residential_desc = cap_residential_desc
                                                                    new_comments = "Prop " & cap_prop_no & " Residential Description Capital:" & Trim(cap_residential_desc) & ";"
                                                                    remove_nonascii_chars(new_comments)
                                                                    comments &= new_comments
                                                                    space_comments()
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                    reader.Read()
                                                            End Select
                                                        End While
                                                    Case "propertyType"
                                                        reader.Read()
                                                        While reader.Name <> "propertyType"
                                                            rdr_name = reader.Name
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            Select Case rdr_name
                                                                Case "code"
                                                                    cap_prop_type_code = reader.ReadElementContentAsString
                                                                    cap_props(cap_prop_no).prop_type_code = cap_prop_type_code
                                                                    new_comments = "Prop " & cap_prop_no & " Property Type Code Capital:" & Trim(cap_prop_type_code) & ";"
                                                                    remove_nonascii_chars(new_comments)
                                                                    comments &= new_comments
                                                                    space_comments()
                                                                Case "description"
                                                                    cap_prop_type_desc = reader.ReadElementContentAsString
                                                                    cap_props(cap_prop_no).prop_type_desc = cap_prop_type_desc
                                                                    new_comments = "Prop " & cap_prop_no & " Property Type Description Capital:" & Trim(cap_prop_type_desc) & ";"
                                                                    remove_nonascii_chars(new_comments)
                                                                    comments &= new_comments
                                                                    space_comments()
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                    reader.Read()
                                                            End Select
                                                        End While
                                                    Case "thirdPartyList"
                                                        reader.Read()
                                                        While reader.Name <> "thirdPartyList"
                                                            rdr_name = reader.Name
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            Select Case rdr_name
                                                                Case "thirdParty"
                                                                    reader.Read()
                                                                    cap_tp_no += 1
                                                                    Try
                                                                        cap_tps(cap_prop_no, cap_tp_no) = Nothing
                                                                    Catch ex As Exception
                                                                        MsgBox("Increase size of cap_tps array")
                                                                    End Try

                                                                    While reader.Name <> "thirdParty"
                                                                        rdr_name = reader.Name
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        Select Case rdr_name
                                                                            Case "name"
                                                                                cap_third_party_name = reader.ReadElementContentAsString
                                                                                cap_tps(cap_prop_no, cap_tp_no).name = cap_third_party_name
                                                                                new_comments = "Prop " & cap_prop_no & " TP " & cap_tp_no & " name Capital:" & cap_third_party_name & ";"
                                                                                remove_nonascii_chars(new_comments)
                                                                                comments &= new_comments
                                                                                space_comments()
                                                                            Case "otherDescription"
                                                                                cap_third_party_other_desc = reader.ReadElementContentAsString
                                                                                cap_tps(cap_prop_no, cap_tp_no).other_desc = cap_third_party_other_desc
                                                                                If cap_third_party_other_desc.Length > 0 Then
                                                                                    new_comments = "Prop " & cap_prop_no & " TP " & cap_tp_no & " Other Desc Capital:" & cap_third_party_other_desc & ";"
                                                                                    remove_nonascii_chars(new_comments)
                                                                                    comments &= new_comments
                                                                                    space_comments()
                                                                                End If
                                                                            Case "dateCreated"
                                                                                cap_third_party_created_date = reader.ReadElementContentAsString
                                                                                cap_tps(cap_prop_no, cap_tp_no).created_date = cap_third_party_created_date
                                                                                comments = comments & "Prop " & cap_prop_no & " TP " & cap_tp_no & " Created Date Capital:" & cap_third_party_created_date & ";"
                                                                                space_comments()
                                                                            Case "dateDeleted"
                                                                                cap_third_party_deleted_date = reader.ReadElementContentAsString
                                                                                cap_tps(cap_prop_no, cap_tp_no).deleted_date = cap_third_party_deleted_date
                                                                                comments = comments & "Prop " & cap_prop_no & " TP " & cap_tp_no & " Deleted Date Capital:" & cap_third_party_deleted_date & ";"
                                                                                space_comments()
                                                                            Case "relationship"
                                                                                reader.Read()
                                                                                While reader.Name <> "relationship"
                                                                                    rdr_name = reader.Name
                                                                                    If reader.NodeType > 1 Then
                                                                                        reader.Read()
                                                                                        Continue While
                                                                                    End If
                                                                                    Select Case rdr_name
                                                                                        Case "code"
                                                                                            cap_third_party_code = reader.ReadElementContentAsString
                                                                                            cap_tps(cap_prop_no, cap_tp_no).rel_code = cap_third_party_code
                                                                                            new_comments = "Prop " & cap_prop_no & " TP " & cap_tp_no & " Relationship Code Capital:" & cap_third_party_code & ";"
                                                                                            remove_nonascii_chars(new_comments)
                                                                                            comments &= new_comments
                                                                                            space_comments()
                                                                                        Case "description"
                                                                                            cap_third_party_desc = reader.ReadElementContentAsString
                                                                                            cap_tps(cap_prop_no, cap_tp_no).rel_desc = cap_third_party_desc
                                                                                            new_comments = "Prop " & cap_prop_no & " TP " & cap_tp_no & " Relationship Desc Capital:" & cap_third_party_desc & ";"
                                                                                            remove_nonascii_chars(new_comments)
                                                                                            comments &= new_comments
                                                                                            space_comments()
                                                                                        Case Else
                                                                                            reader.Read()
                                                                                    End Select
                                                                                End While
                                                                            Case Else
                                                                                MsgBox("What is this tag?" & rdr_name)
                                                                                reader.Read()
                                                                        End Select
                                                                    End While
                                                                Case Else
                                                                    MsgBox("What is this tag?" & rdr_name)
                                                                    reader.Read()
                                                            End Select
                                                        End While
                                                    Case "bedRoomCount"
                                                        cap_bedroom_count = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).bedroom_count = cap_bedroom_count
                                                        new_comments = "Prop " & cap_prop_no & " Bedroom Count Capital:" & cap_bedroom_count & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "line1"
                                                        cap_addr1 = reader.ReadElementContentAsString
                                                        If cap_addr1.Length > 0 Then
                                                            cap_props(cap_prop_no).addr_line1 = cap_addr1
                                                            new_comments = "Prop " & cap_prop_no & " Address Line1 Capital:" & cap_addr1 & ";"
                                                            remove_nonascii_chars(new_comments)
                                                            comments &= new_comments
                                                            space_comments()
                                                        End If
                                                    Case "line2"
                                                        cap_addr2 = reader.ReadElementContentAsString
                                                        If cap_addr2.Length > 0 Then
                                                            cap_props(cap_prop_no).addr_line2 = cap_addr2
                                                            new_comments = "Prop " & cap_prop_no & " Address Line2 Capital:" & cap_addr2 & ";"
                                                            remove_nonascii_chars(new_comments)
                                                            comments &= new_comments
                                                            space_comments()
                                                        End If
                                                    Case "line3"
                                                        cap_addr3 = reader.ReadElementContentAsString
                                                        If cap_addr3.Length > 0 Then
                                                            cap_props(cap_prop_no).addr_line3 = cap_addr3
                                                            comments = comments & "Prop " & cap_prop_no & " Address Line3 Capital:" & cap_addr3 & ";"
                                                            space_comments()
                                                        End If
                                                    Case "city"
                                                        cap_city = reader.ReadElementContentAsString
                                                        If cap_city.Length > 0 Then
                                                            cap_props(cap_prop_no).addr_city = cap_city
                                                            new_comments = "Prop " & cap_prop_no & " Address city Capital:" & cap_city & ";"
                                                            remove_nonascii_chars(new_comments)
                                                            comments &= new_comments
                                                            space_comments()
                                                        End If
                                                    Case "postcode"
                                                        cap_postcode = reader.ReadElementContentAsString
                                                        If cap_postcode.Length > 0 Then
                                                            cap_props(cap_prop_no).addr_pcode = cap_postcode
                                                            new_comments = "Prop " & cap_prop_no & " Address Postcode Capital:" & cap_postcode & ";"
                                                            remove_nonascii_chars(new_comments)
                                                            comments &= new_comments
                                                            space_comments()
                                                        End If
                                                    Case "country"
                                                        cap_country = reader.ReadElementContentAsString
                                                        If cap_country.Length > 0 Then
                                                            cap_props(cap_prop_no).addr_country = cap_country
                                                            new_comments = "Prop " & cap_prop_no & " Address Country Capital:" & cap_country & ";"
                                                            remove_nonascii_chars(new_comments)
                                                            comments &= new_comments
                                                            space_comments()
                                                        End If
                                                    Case "percentageApplicantOwned"
                                                        cap_percent_owned_appl = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).percent_appl = cap_percent_owned_appl
                                                        new_comments = "Prop " & cap_prop_no & " Percent Owned Applicant Capital:" & cap_percent_owned_appl & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "percentagePartnerOwned"
                                                        cap_percent_owned_partner = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).percent_partner = cap_percent_owned_partner
                                                        new_comments = "Prop " & cap_prop_no & " Percent Owned Partner Capital:" & cap_percent_owned_partner & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "applicantEquityAmount"
                                                        cap_appl_equity_amt = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).appl_equity_amt = cap_appl_equity_amt
                                                        comments = comments & "Prop " & cap_prop_no & " Applicant Equity Amount Capital:" & cap_appl_equity_amt & ";"
                                                        space_comments()
                                                    Case "partnerEquityAmount"
                                                        cap_partner_equity_amt = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).partner_equity_amt = cap_partner_equity_amt
                                                        new_comments = "Prop " & cap_prop_no & " Partner Equity Amount Capital:" & cap_partner_equity_amt & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "declaredMortgage"
                                                        cap_declared_mortgage = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).declared_mortgage = cap_declared_mortgage
                                                        new_comments = "Prop " & cap_prop_no & " Declared Mortgage Capital:" & cap_declared_mortgage & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "declaredValue"
                                                        cap_declared_value = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).declared_value = cap_declared_value
                                                        new_comments = "Prop " & cap_prop_no & " Declared Value Capital:" & cap_declared_value & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "declaredEquity"
                                                        cap_declared_equity = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).declared_equity = cap_declared_equity
                                                        new_comments = "Prop " & cap_prop_no & " Declared Equity Capital:" & cap_declared_equity & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "declaredDate"
                                                        cap_declared_date = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).declared_date = cap_declared_date
                                                        comments = comments & "Prop " & cap_prop_no & " Declared Date Capital:" & cap_declared_date & ";"
                                                        space_comments()
                                                    Case "verifiedValue"
                                                        cap_verified_market_value = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).verified_value = cap_verified_market_value
                                                        new_comments = "Prop " & cap_prop_no & " Verified Value Capital:" & cap_verified_market_value & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "verifiedMortgage"
                                                        cap_verified_mortgage = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).verified_mortgage = cap_verified_mortgage
                                                        new_comments = "Prop " & cap_prop_no & " Verified Mortgage Capital:" & cap_verified_mortgage & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "verifiedDate"
                                                        cap_verified_date = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).verified_date = cap_verified_date
                                                        comments = comments & "Prop " & cap_prop_no & " Verified Date Capital:" & cap_verified_date & ";"
                                                        space_comments()
                                                    Case "verifiedBy"
                                                        cap_verified_by = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).verified_by = cap_verified_by
                                                        new_comments = "Prop " & cap_prop_no & " Verified By Capital:" & cap_verified_by & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "tenantInPlace"
                                                        cap_tenant_in_place = reader.ReadElementContentAsString
                                                        cap_props(cap_prop_no).tenant_in_place = cap_tenant_in_place
                                                        new_comments = "Prop " & cap_prop_no & " Tenant in Place Capital:" & cap_tenant_in_place & ";"
                                                        remove_nonascii_chars(new_comments)
                                                        comments &= new_comments
                                                        space_comments()
                                                    Case "address"
                                                        reader.Read()
                                                    Case "detail"
                                                        reader.Read()
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "repOrderWithdrawalDate"
                                rep_order_withdrawal_date_str = reader.ReadElementContentAsString
                                Try
                                    rep_order_withdrawal_date = rep_order_withdrawal_date_str
                                Catch ex As Exception
                                    rep_order_withdrawal_date_str = ""
                                End Try
                                If IsDate(rep_order_withdrawal_date) Then
                                    comments = comments & "Rep-Order-Withdrawn-Date:" & _
                                    Format(rep_order_withdrawal_date, "dd/MM/yyyy") & ";"
                                    space_comments()
                                End If
                            Case "incomeEvidenceList"
                                reader.Read()
                                Dim new_comment2 As String = ""
                                While reader.Name <> "incomeEvidenceList"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        If reader.Name = "incomeEvidence" And reader.NodeType = Xml.XmlNodeType.EndElement Then
                                            If new_comment2.Length <= 250 Then
                                                new_comments = new_comment2
                                                remove_nonascii_chars(new_comments)
                                                comments &= new_comments
                                                space_comments()
                                            Else
                                                get_comment_break(new_comment2)
                                            End If
                                            new_comment2 = ""
                                        End If
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "evidence"
                                            inc_ev_no += 1
                                            Try
                                                inc_evidence_table(inc_ev_no, 2) = ""
                                            Catch ex As Exception
                                                MsgBox("Increase array size on inc_evidence_table")
                                            End Try
                                            inc_evidence_table(inc_ev_no, 1) = reader.ReadElementContentAsString

                                            inc_evidence_table(inc_ev_no, 3) = ""
                                            inc_evidence_table(inc_ev_no, 4) = ""
                                            new_comment2 = new_comment2 & "Inc Evidence:" & inc_evidence_table(inc_ev_no, 1) & ";"
                                        Case "mandatory"
                                            inc_evidence_table(inc_ev_no, 2) = reader.ReadElementContentAsString
                                            new_comment2 = new_comment2 & "Inc Evidence Mandatory:" & inc_evidence_table(inc_ev_no, 2) & ";"
                                        Case "otherText"
                                            Try
                                                inc_evidence_table(inc_ev_no, 3) = reader.ReadElementContentAsString
                                                Replace(inc_evidence_table(inc_ev_no, 3), "�", "")
                                                remove_chrs(inc_evidence_table(inc_ev_no, 3))
                                                new_comment2 = new_comment2 & "Inc Evidence Other Text:" & inc_evidence_table(inc_ev_no, 3) & ";"
                                            Catch ex As Exception
                                                MsgBox(ex.Message)
                                            End Try
                                        Case "dateReceived"
                                            test_date = reader.ReadElementContentAsString
                                            inc_evidence_table(inc_ev_no, 4) = Format(test_date, "dd/MM/yyyy")
                                            new_comment2 = new_comment2 & "Inc Evidence Date Received:" & Format(test_date, "dd/MM/yyyy") & ";"
                                        Case "incomeEvidence"
                                            reader.Read()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "incomeUpliftApplied"
                                income_uplift_applied = reader.ReadElementContentAsString
                                new_comments = "Inc uplift applied:" & income_uplift_applied & ";"
                                remove_nonascii_chars(new_comments)
                                comments &= new_comments
                                space_comments()
                            Case "upliftApplied"
                                MsgBox("Contact Jeff - uplift applied found")
                            Case "upliftAppliedDate"
                                uplift_applied_date = reader.ReadElementContentAsString
                                comments = comments & "uplift applied date:" & Format(uplift_applied_date, "dd/MM/yyyy") & ";"
                                space_comments()
                            Case "upliftRemovedDate"
                                uplift_removed_date = reader.ReadElementContentAsString
                                comments = comments & "uplift removed date:" & Format(uplift_removed_date, "dd/MM/yyyy") & ";"
                                space_comments()
                            Case "ccOutcomes"
                                reader.Read()
                                While reader.Name <> "ccOutcomes" And reader.Name <> "CONTRIBUTIONS" And reader.Name <> "correspondence"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        If reader.Name = "ccOutcome" And reader.NodeType = Xml.XmlNodeType.EndElement Then
                                            space_comments()
                                        End If
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "code"
                                            If outcome = "" Then
                                                outcome = reader.ReadElementContentAsString
                                                comments = comments & "OUTCOME:" & outcome & ";"
                                            Else
                                                outcome2 = reader.ReadElementContentAsString
                                                comments = comments & "OUTCOME:" & outcome2 & ";"
                                            End If
                                        Case "date"
                                            Try
                                                test_date = reader.ReadElementContentAsString
                                            Catch ex As Exception
                                                reader.Read()
                                                Continue While
                                            End Try
                                            outcome_date = test_date
                                            comments = comments & "Outcome Date:" & Format(test_date, "dd/MM/yyyy") & ";"
                                        Case "ccOutcome"
                                            reader.Read()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                                'Case "FINAL_DEFENCE_COST"
                                '    final_defence_cost = reader.ReadElementContentAsString

                                'Case "ALLOWABLE_CAPITAL_THRESHOLD"
                                '    allowable_cap_threshold = reader.ReadElementContentAsString
                                '    remove_chrs(allowable_cap_threshold)
                            Case "preferredPaymentMethod"
                                reader.Read()
                                While reader.Name <> "preferredPaymentMethod"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "description"
                                            pref_pay_method = reader.ReadElementContentAsString
                                            new_comments = "Preferred Payment Method:" & pref_pay_method & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case "code"
                                            pref_pay_code = reader.ReadElementContentAsString
                                            new_comments = "Preferred Payment Code:" & pref_pay_code & ";"
                                            remove_nonascii_chars(new_comments)
                                            comments &= new_comments
                                            space_comments()
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                            Case "correspondence"
                                reader.Read()
                                While reader.Name <> "correspondence" And reader.Name <> "CONTRIBUTIONS" ' Contributions check added TS 08/Feb/2013
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    Select Case rdr_name
                                        Case "letter"
                                            reader.Read()
                                            rdr_name = reader.Name
                                            letter_no += 1
                                            If letter_no > 1 Then
                                                space_comments()
                                            End If
                                            Try
                                                letter_table(letter_no).ref = Nothing
                                                letter_table(letter_no).type = Nothing
                                                letter_table(letter_no).created = Nothing
                                                letter_table(letter_no).printed = Nothing
                                                letter_table(letter_no).id = Nothing
                                            Catch ex As Exception
                                                ReDim Preserve letter_table(letter_no + 10)
                                            End Try
                                            While rdr_name <> "letter" And rdr_name <> "correspondence"
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    rdr_name = reader.Name
                                                    Continue While
                                                End If
                                                rdr_name = reader.Name
                                                Select Case rdr_name
                                                    Case "id"
                                                        letter_table(letter_no).id = reader.ReadElementContentAsString
                                                        comments = comments & "Letter id:" & letter_table(letter_no).id & ";"
                                                    Case "Ref"
                                                        letter_table(letter_no).ref = reader.ReadElementContentAsString
                                                        comments = comments & "Letter ref:" & letter_table(letter_no).ref & ";"
                                                    Case "type"
                                                        letter_table(letter_no).type = reader.ReadElementContentAsString
                                                        comments = comments & "Letter type:" & letter_table(letter_no).type & ";"
                                                    Case "created"
                                                        letter_table(letter_no).created = reader.ReadElementContentAsString
                                                        comments = comments & "Letter created:" & letter_table(letter_no).created & ";"
                                                    Case "printed"
                                                        Try
                                                            letter_table(letter_no).printed = reader.ReadElementContentAsString
                                                            comments = comments & "Letter printed:" & letter_table(letter_no).printed & ";"
                                                        Catch ex As Exception
                                                            reader.Read()
                                                        End Try
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                        reader.Read()
                                                End Select
                                            End While
                                        Case Else
                                            MsgBox("What is this tag?" & rdr_name)
                                            reader.Read()
                                    End Select
                                End While
                                If letter_no > 1 Then
                                    space_comments()
                                End If
                            Case "preferredPaymentDay"
                                pref_pay_day = reader.ReadElementContentAsString
                                new_comments = "Preferred Payment Day:" & pref_pay_day & ";"
                                remove_nonascii_chars(new_comments)
                                comments &= new_comments
                                space_comments()
                            Case "accountName"
                                bank_acc_name = reader.ReadElementContentAsString
                                If bank_acc_name.Length > 0 Then
                                    new_comments = "Bank Acc Name:" & bank_acc_name & ";"
                                    remove_nonascii_chars(new_comments)
                                    comments &= new_comments
                                    space_comments()
                                End If
                            Case "accountNo"
                                bank_acc_no = reader.ReadElementContentAsString
                                If bank_acc_no.Length > 0 Then
                                    new_comments = "Bank Acc No:" & bank_acc_no & ";"
                                    remove_nonascii_chars(new_comments)
                                    comments &= new_comments
                                    space_comments()
                                End If
                            Case "sortCode"
                                bank_sort_code = reader.ReadElementContentAsString
                                If bank_sort_code.Length > 0 Then
                                    new_comments = "Bank Sort Code:" & bank_sort_code & ";"
                                    remove_nonascii_chars(new_comments)
                                    comments &= new_comments
                                    space_comments()
                                End If

                            Case Else
                                If maat_id.Length > 1 And _
                                rdr_name <> "application" And _
                                rdr_name <> "assessment" And _
                                rdr_name <> "bankDetails" Then
                                    MsgBox("what is this? " & rdr_name)
                                End If

                                reader.Read()
                        End Select
                    End While
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
            Else
                MsgBox("XML file not processed")
                Exit Sub
            End If
            'write file including last applicant
            end_of_record()
            input_file = input_file & record & vbNewLine
            If validate_record() = False Then
                rej_recs += 1
                audit_file = audit_file & maat_id & "|rejected" & vbNewLine
            Else
                If Not appeal_case Then
                    valid_recs += 1
                ElseIf Not on_onestep Then
                    appeal_valid_recs += 1
                End If
                If on_onestep = False Then
                    If appeal_case Then
                        audit_file = audit_file & maat_id & "|appeal" & vbNewLine
                        appeal_file = appeal_file & record & vbNewLine
                        new_appeal_cases += 1
                    ElseIf equity_check = True Then
                        equity_recs += 1
                        audit_file = audit_file & maat_id & "|equity" & vbNewLine
                        equity_file = equity_file & record & vbNewLine
                    Else
                        contrib_recs += 1
                        audit_file = audit_file & maat_id & "|contrib" & vbNewLine
                        contrib_file = contrib_file & record & vbNewLine
                    End If
                End If
            End If
            Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
            '20.11.2014 ensure correct directory used
            Dim InputFilePath As String = Path.GetDirectoryName(filename)
            InputFilePath &= "\"
            Dim infilename As String = Path.GetFileNameWithoutExtension(filename)
            'new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_appeal_preprocess.txt"
            new_file = InputFilePath & infilename & "_appeal_preprocess.txt"
            My.Computer.FileSystem.WriteAllText(new_file, appeal_file, False)
            'new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_equity_preprocess.txt"
            new_file = InputFilePath & infilename & "_equity_preprocess.txt"
            My.Computer.FileSystem.WriteAllText(new_file, equity_file, False)
            'new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_input.txt"
            new_file = InputFilePath & infilename & "_input.txt"
            My.Computer.FileSystem.WriteAllText(new_file, input_file, False)
            If write_audit Then
                'new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_audit.txt"
                new_file = InputFilePath & infilename & "_audit.txt"
                My.Computer.FileSystem.WriteAllText(new_file, audit_file, False)
            End If
            'new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_contrib_preprocess.txt"
            new_file = InputFilePath & infilename & "_contrib_preprocess.txt"
            My.Computer.FileSystem.WriteAllText(new_file, contrib_file, False)
            'new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_no_changes.txt"
            new_file = InputFilePath & infilename & "_no_changes.txt"
            My.Computer.FileSystem.WriteAllText(new_file, no_changes_file, False)

            'write acknowledgement file in xml
            If valid_recs + rej_recs > 0 Then
                Dim ack_file As String = InputFilePath & "CONTRIBUTIONS_FILE_ACK_" & Format(Now, "yyyyMMddHHmm") & ".xml"
                Dim writer As New Xml.XmlTextWriter(ack_file, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)
                writer.Formatting = Xml.Formatting.Indented
                'acknowledgement file includes all records not rejected
                Dim tot_valid_recs As Integer = valid_recs
                'Write_ack(writer, filename_id, header_id, tot_valid_recs, rej_recs, no_of_errors)

                Write_ack(writer, InputFilePath & filename_id, header_id, tot_valid_recs, rej_recs, no_of_errors)

                writer.Close()
            End If

            'write acknowledgement file in xml
            If appeal_valid_recs + appeal_rej_recs > 0 Then
                'T66113 add inputfile path
                Dim appeal_ack_file As String = InputFilePath & "APPEALS_FILE_ACK_" & Format(Now, "yyyyMMddHHmm") & ".xml"
                Dim appeal_writer As New Xml.XmlTextWriter(appeal_ack_file, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)
                appeal_writer.Formatting = Xml.Formatting.Indented
                'acknowledgement file includes all records not rejected
                Dim tot_appeal_valid_recs As Integer = appeal_valid_recs
                'Write_appeal_ack(appeal_writer, filename_id, header_id, tot_appeal_valid_recs, appeal_rej_recs, appeal_no_of_errors)
                Write_appeal_ack(appeal_writer, InputFilePath & filename_id, header_id, tot_appeal_valid_recs, appeal_rej_recs, appeal_no_of_errors)
                appeal_writer.Close()
            End If

            If invalid_date_found Then
                MsgBox("Invalid date found - Inform LSC team report is available")
                'new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_invalid_dates.txt"
                new_file = InputFilePath & infilename & "_invalid_dates.txt"
                My.Computer.FileSystem.WriteAllText(new_file, invalid_date_file, False)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
        ProgressBar1.Value = 100
        MsgBox("Cases with no changes = " & no_changes & vbNewLine & "Changes = " & changes)
        MsgBox("Appeal cases = " & new_appeal_cases & vbNewLine & _
               "Contrib recs = " & contrib_recs & vbNewLine & _
               "Equity recs = " & equity_recs)
        Me.Close()
    End Sub
    Private Sub remove_nonascii_chars(ByRef new_comments As String)
        Dim comm_idx As Integer
        Dim ret_comments As String = ""
        For comm_idx = 1 To new_comments.Length
            If AscW(Mid(new_comments, comm_idx, 1)) > 127 Then
                ret_comments &= " "
            Else
                ret_comments &= Mid(new_comments, comm_idx, 1)
            End If
        Next
        new_comments = ret_comments
    End Sub
    Private Sub space_comments()
        'Try
        '    'Dim len As Integer = comments.Length
        '    'comments = comments & Space(spaces - comments.Length)
        '    'spaces += 250
        'Catch ex As Exception
        '    MsgBox("Out of comment space")
        'End Try
        Dim spaces As Integer
        'remove any non ascii character from comments

        Try

            spaces = (Int((comments.Length / 250)) + 1) * 250
            comments = comments & Space(spaces - comments.Length)
        Catch ex As Exception
            MsgBox("error in spacing comments")
        End Try

    End Sub
    Private Sub save_error(ByVal error_text As String)
        If error_text = "invalid maat id" Then
            If appeal_case Then
                appeal_no_of_errors += 1
                appeal_error_table(appeal_no_of_errors).contrib_id = contrib_id ' TS 13/Feb/2013
                appeal_error_table(appeal_no_of_errors).applicant_id = applicant_id ' TS 13/Feb/2013
                appeal_error_table(appeal_no_of_errors).maat_id = Nothing ' TS 13/Feb/2013
                appeal_error_table(appeal_no_of_errors).error_text = error_text ' TS 13/Feb/2013
            Else
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = Nothing
                error_table(no_of_errors).error_text = error_text
            End If
        Else
            If appeal_case Then
                appeal_no_of_errors += 1
                appeal_error_table(appeal_no_of_errors).contrib_id = contrib_id ' TS 13/Feb/2013
                appeal_error_table(appeal_no_of_errors).applicant_id = applicant_id ' TS 13/Feb/2013
                appeal_error_table(appeal_no_of_errors).maat_id = maat_id ' TS 13/Feb/2013
                appeal_error_table(appeal_no_of_errors).error_text = error_text ' TS 13/Feb/2013
            Else
                no_of_errors += 1
                error_table(no_of_errors).contrib_id = contrib_id
                error_table(no_of_errors).applicant_id = applicant_id
                error_table(no_of_errors).maat_id = maat_id
                error_table(no_of_errors).error_text = error_text
            End If
        End If


    End Sub
    Private Function validate_record() As Boolean
        equity_check = False
        Dim orig_no_of_errors As Integer = no_of_errors
        Dim orig_appeal_no_of_errors As Integer = appeal_no_of_errors
        If maat_id = "" Then
            save_error("invalid maat id")
        End If

        If sentence_date <> Nothing Then
            If sentence_date < CDate("2001,1,1") Then
                save_error("invalid sentence order date")
            End If
        End If

        If earlier_contrib_found = True Then
            save_error("MAAT ID contrib ID is less than previous")
        End If

        If outcome <> "" Then
            If outcome <> "CONVICTED" And outcome <> "PART CONVICTED" And outcome <> "APPEAL" And outcome <> "ABANDONED" _
                    And outcome <> "AQUITTED" And outcome <> "DISMISSED" And outcome <> "DISMISSED" _
                    And outcome <> "PART SUCCESS" And outcome <> "UNSUCCESSFUL" And outcome <> "SUCCESSFUL" Then
                save_error("invalid crown court outcome")
            End If
        End If

        'If appeal_type <> "" Then
        '    If appeal_type <> "ASE" And appeal_type <> "ACV" And appeal_type <> "ACS" Then
        '        save_error("invalid appeal type")
        '    End If
        'End If

        If rep_status <> "" Then
            If rep_status <> "CURR" And rep_status <> "ERR" And rep_status <> "SUSP" _
            And rep_status <> "FI" And rep_status <> "NOT SENT FOR TRIAL" Then
                save_error("invalid rep type")
            End If
        End If


        If on_onestep Then
            'take this out until further notice
            '    If flag_id = "new" Then
            '        save_error("not a new case")
            '        Return (False)
            '    End If
            Return (True)
        End If

        '        If flag_id <> "new" Then commented out TS 18/Feb/2013
        '23.09.2013 ignore new flag
        'If flag_id <> "new" And Not appeal_case Then
        '    save_error("case does not exist")
        '    Return (False)
        'End If

        If first_name = "" Then
            save_error("invalid first name")
        End If

        If surname = "" Then
            save_error("invalid surname")
        End If

        If Not appeal_case Then ' Added TS 13/Feb/2013
            If mthly_contrib_amt_str = "" Then
                save_error("invalid monthly contribution")
            ElseIf mthly_contrib_amt = 0 Then
                If outcome = "CONVICTED" Or outcome = "PART CONVICTED" Then
                    equity_check = True
                ElseIf outcome <> "APPEAL" And UCase(Microsoft.VisualBasic.Left(case_type, 6)) <> "APPEAL" Then
                    save_error("invalid monthly contribution")
                End If
            End If
        Else ' Added TS 13/Feb/2013
            'If UCase(flag_id) = "NEW" Then ' Commented out TS 18/Feb/2013
            If upfront_contrib_amt_str = "" Or upfront_contrib_amt_str = "0" Then
                save_error("invalid upfront contribution")
            End If
            'End If ' Commented out TS 18/Feb/2013
        End If

        If case_type = "" Then
            save_error("invalid case type")
        End If

        If in_court_custody <> "Yes" And in_court_custody <> "No" Then
            save_error("invalid in court custody")
        End If

        If emp_status = "" Then
            save_error("invalid employment status")
        End If

        If effective_date = Nothing And future_effective_date = False Then
            save_error("invalid effective date")
        End If

        If income_contrib_cap_str = "" Then
            save_error("invalid income contribution cap")
        End If

        'If income_uplift_applied <> "Yes" And income_uplift_applied <> "No" Then
        '    no_of_errors += 1
        '    error_table(no_of_errors).contrib_id = contrib_id
        '    error_table(no_of_errors).applicant_id = applicant_id
        '    error_table(no_of_errors).maat_id = maat_id
        '    error_table(no_of_errors).error_text = "invalid income uplift applied"
        'End If

        If equity_amt_verified <> "" Then
            If equity_amt_verified <> "Yes" And equity_amt_verified <> "No" Then
                save_error("invalid equity amount verified")
            End If
        End If

        If cap_amt_verified <> "" Then
            If LCase(cap_amt_verified) <> "yes" And cap_amt_verified <> "no" Then
                save_error("invalid capital amount verified")
            End If
        End If

        If orig_no_of_errors = no_of_errors And orig_appeal_no_of_errors = appeal_no_of_errors Then
            'audit_file = audit_file & maat_id & "|no errors" & vbNewLine
            Return (True)
        Else
            equity_check = False
            'audit_file = audit_file & maat_id & "|errors" & vbNewLine
            If no_of_errors = UBound(error_table) Then
                ReDim Preserve error_table(no_of_errors + 10)
            End If
            If appeal_no_of_errors = UBound(appeal_error_table) Then
                ReDim Preserve appeal_error_table(appeal_no_of_errors + 10)
            End If
            Return (False)
        End If

    End Function

    Private Sub Write_ack(ByVal writer As Xml.XmlWriter, ByVal filename_id As String, _
    ByVal filename_id_tag As String, ByVal valid_recs As Integer, ByVal rej_recs As Integer, _
    ByVal no_of_errors As Integer)
        writer.WriteStartElement("CONTRIBUTIONS_FILE_ACK")
        writer.WriteStartElement("FILENAME")
        writer.WriteAttributeString("ID", filename_id_tag)
        writer.WriteString(filename_id)
        writer.WriteEndElement()
        writer.WriteElementString("DATE_LOADED", Format(Now, "dd-MM-yyyy"))
        writer.WriteElementString("NO_OF_RECORDS_LOADED", valid_recs)
        writer.WriteElementString("NO_OF_RECORDS_REJECTED", rej_recs)

        Dim idx As Integer
        Dim contrib_id As Double = 0
        Dim last_contrib_id As Double = 0
        Dim applicant_id As Double = 0
        Dim maat_id As Double = 0
        Dim error_text As String = ""
        For idx = 1 To no_of_errors
            If idx = 1 Then
                writer.WriteStartElement("REJECTED_RECORD_LIST")
            End If
            contrib_id = error_table(idx).contrib_id
            If contrib_id <> last_contrib_id Then
                last_contrib_id = contrib_id
                writer.WriteStartElement("CONTRIBUTIONS")
                writer.WriteAttributeString("ID", contrib_id)
                applicant_id = error_table(idx).applicant_id
                maat_id = error_table(idx).maat_id
                writer.WriteElementString("APPLICANT_ID", applicant_id)
                writer.WriteElementString("MAAT_ID", maat_id)
            End If

            error_text = error_table(idx).error_text
            writer.WriteElementString("ERROR_TEXT", error_text)

            If idx = no_of_errors Then
                writer.WriteEndElement()
                writer.WriteEndElement()
            ElseIf contrib_id <> error_table(idx + 1).contrib_id Then
                writer.WriteEndElement()
            End If
        Next
        writer.WriteEndElement()
    End Sub
    Private Sub Write_appeal_ack(ByVal writer As Xml.XmlWriter, ByVal filename_id As String, _
    ByVal filename_id_tag As String, ByVal valid_recs As Integer, ByVal rej_recs As Integer, _
    ByVal no_of_errors As Integer)
        writer.WriteStartElement("CONTRIBUTIONS_FILE_ACK")
        writer.WriteStartElement("FILENAME")
        writer.WriteAttributeString("ID", filename_id_tag)
        writer.WriteString(filename_id)
        writer.WriteEndElement()
        writer.WriteElementString("DATE_LOADED", Format(Now, "dd-MM-yyyy"))
        writer.WriteElementString("NO_OF_RECORDS_LOADED", valid_recs)
        writer.WriteElementString("NO_OF_RECORDS_REJECTED", rej_recs)

        Dim idx As Integer
        Dim contrib_id As Double = 0
        Dim last_contrib_id As Double = 0
        Dim applicant_id As Double = 0
        Dim maat_id As Double = 0
        Dim error_text As String = ""
        For idx = 1 To no_of_errors
            If idx = 1 Then
                writer.WriteStartElement("REJECTED_RECORD_LIST")
            End If
            contrib_id = appeal_error_table(idx).contrib_id
            If contrib_id <> last_contrib_id Then
                last_contrib_id = contrib_id
                writer.WriteStartElement("CONTRIBUTIONS")
                writer.WriteAttributeString("ID", contrib_id)
                applicant_id = appeal_error_table(idx).applicant_id
                maat_id = appeal_error_table(idx).maat_id
                writer.WriteElementString("APPLICANT_ID", applicant_id)
                writer.WriteElementString("MAAT_ID", maat_id)
            End If

            error_text = appeal_error_table(idx).error_text
            writer.WriteElementString("ERROR_TEXT", error_text)

            If idx = no_of_errors Then
                writer.WriteEndElement()
                writer.WriteEndElement()
            ElseIf contrib_id <> appeal_error_table(idx + 1).contrib_id Then
                writer.WriteEndElement()
            End If
        Next
        writer.WriteEndElement()
    End Sub
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub end_of_record()

        Try
            If applicant_id = "" Then
                write_error("No applicant found")
                Exit Sub
            End If
            If maat_id = "" Then
                write_error("No maat found")
                Exit Sub
            End If

            If allowable_cap_threshold <> "" Then
                comments = comments & "Allowable cap threshold:" & allowable_cap_threshold & ";"
                space_comments()
            End If

            'If outcome <> "" Then
            '    comments = comments & "Outcome:" & outcome & ";"
            '    space_comments()
            'End If

            Dim idx3 As Integer
            Try
                For idx3 = 1 To asset_no
                    Dim orig_comment_length As Integer = comments.Length
                    comments = comments & "Cap asset type:" & asset_table(idx3, 1) & ";"
                    comments = comments & "Cap amt:" & asset_table(idx3, 2) & ";"
                    comments = comments & "Cap amt verified:" & asset_table(idx3, 3) & ";"
                    If asset_table(idx3, 4) <> "" Then
                        comments = comments & "Cap amt Date Verified:" & asset_table(idx3, 4) & ";"
                    End If
                    If asset_table(idx3, 5) <> "" Then
                        comments = comments & "Cap amt Evidence Received Date:" & asset_table(idx3, 5) & ";"
                    End If
                    If asset_table(idx3, 7) <> "" Then
                        comments &= "Cap asset code:" & asset_table(idx3, 7) & ";"
                    End If
                    If asset_table(idx3, 6) <> "" Then
                        Dim temp_comments As String = comments & "Cap amt description:" & asset_table(idx3, 6) & ";"
                        If temp_comments.Length - orig_comment_length > 250 Then
                            'truncate length to 250 
                            Dim desc_reqd_length As Integer = 250 - (comments.Length - orig_comment_length) - 22
                            asset_table(idx3, 6) = Microsoft.VisualBasic.Left(asset_table(idx3, 6), desc_reqd_length)
                            comments &= "Cap amt description:" & asset_table(idx3, 6) & ";"
                        Else
                            comments = temp_comments
                        End If
                    End If
                    space_comments()
                Next
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try



            If landline = "" Then
                landline = mobile
                mobile = ""
            End If
            'use final costs if appeal type and mthly amt=0
            'If outcome = "APPEAL" Or UCase(Microsoft.VisualBasic.Left(case_type, 6)) = "APPEAL" _
            'And mthly_contrib_amt = 0 And upfront_contrib_amt = 0 Then
            '    upfront_contrib_amt = final_defence_cost
            '    comments = comments & "Final Defence Costs:" & Format(final_defence_cost, "F") & ";"
            '    space_comments()
            'End If
            record = applicant_id & "|" & summons_no & "|" & maat_id & "|" & first_name & "|" & _
                                        surname & "|" & dob & "|" & ni_no & "|" & mthly_contrib_amt & "|" & upfront_contrib_amt & "|" & _
                                        income_contrib_cap & "|" & debt_addr1 & "|" & debt_addr2 & "|" & debt_addr3 & "|" & _
                                        debt_addr4 & "|" & debt_postcode & "|" & postal_addr1 & "|" & postal_addr2 & "|" & _
                                        postal_addr3 & "|" & postal_addr4 & "|" & postal_postcode & "|" & landline & "|" & mobile & "|" & email & _
                                        "|"
            If future_effective_date Then
                record = record & ""
            Else
                record = record & Format(effective_date, "dd-MM-yyyy")
            End If
            'add outcome and SO date
            record &= "|" & outcome & "|"
            If sentence_date <> Nothing Then
                record &= Format(sentence_date, "dd-MM-yyyy")
            End If
            'check if maat-id already exists on onestep
            param1 = "onestep"
            param2 = "select  clientschemeID, _rowid from Debtor" & _
            " where  client_ref = '" & maat_id & "'"
            Dim debtor1_dataset As DataSet = get_dataset(param1, param2)
            Dim debtor As Integer = 0
            If no_of_rows > 0 Then
                Dim csid_no As Integer = no_of_rows - 1
                Dim idx As Integer
                For idx = 0 To csid_no
                    Dim csid As Integer = debtor1_dataset.Tables(0).Rows(idx).Item(0)
                    param2 = "select clientID from ClientScheme where _rowid = " & csid
                    Dim csid_datatset As DataSet = get_dataset(param1, param2)
                    If no_of_rows <> 1 Then
                        MsgBox("Unable to read clientscheme for csid = " & csid)
                        Exit Sub
                    End If
                    Dim clientID As Integer = 24
                    If prod_run Then
                        clientID = 909
                    End If
                    'use <> 24 for test client;  909 for production client
                    If csid_datatset.Tables(0).Rows(0).Item(0) <> clientID Then
                        Continue For
                    Else
                        debtor = debtor1_dataset.Tables(0).Rows(idx).Item(1)
                        Exit For
                    End If
                Next
            End If

            If no_of_rows = 0 Or debtor = 0 Then
                on_onestep = False
                'audit_file = audit_file & maat_id & "|not on onestep" & vbNewLine
                Dim comments2 As String = ""
                Dim comments3 As String = comments
                'If comments.Length <= 250 Then
                record = record & "|" & comments
                'Else
                '    While comments3.Length > 250
                '        Dim len As Integer = Microsoft.VisualBasic.Len(comments3)
                '        Dim idx As Integer
                '        For idx = 250 To 1 Step -1
                '            If Mid(comments3, idx, 1) = ";" Then
                '                Exit For
                '            End If
                '        Next
                '        comments2 = comments2 & Microsoft.VisualBasic.Left(comments3, idx)
                '        Dim idx2 As Integer
                '        For idx2 = idx To 250
                '            comments2 = comments2 & " "
                '        Next
                '        comments3 = Microsoft.VisualBasic.Right(comments3, len - idx)
                '    End While
                'record = record & "|" & comments2 & comments3
                'end If
                'check if any dates are valid but out of range
                Dim test_date As Date
                If IsDate(dob) Then
                    test_date = CDate(dob)
                    If Not date_range_valid(test_date) Then
                        invalid_date_file = invalid_date_file & maat_id & "|Date of Birth|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                    End If
                End If
                If partner_dob <> Nothing Then
                    test_date = partner_dob
                    If Not date_range_valid(test_date) Then
                        invalid_date_file = invalid_date_file & maat_id & "|Partner Date of Birth|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                    End If
                End If
                If rep_status_date <> Nothing Then
                    test_date = rep_status_date
                    If Not date_range_valid(test_date) Then
                        invalid_date_file = invalid_date_file & maat_id & "|Rep Status Date|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                    End If
                End If
                If sentence_date <> Nothing Then
                    test_date = sentence_date
                    If Not date_range_valid(test_date) Then
                        invalid_date_file = invalid_date_file & maat_id & "|Sentence date|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                    End If
                End If
                If committal_date <> Nothing Then
                    test_date = committal_date
                    If Not date_range_valid(test_date) Then
                        invalid_date_file = invalid_date_file & maat_id & "|Committal date|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                    End If
                End If
                If hardship_review_date <> Nothing Then
                    test_date = hardship_review_date
                    If Not date_range_valid(test_date) Then
                        invalid_date_file = invalid_date_file & maat_id & "|Hardship review date|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                    End If
                End If
                If effective_date <> Nothing Then
                    test_date = effective_date
                    If Not date_range_valid(test_date) Then
                        invalid_date_file = invalid_date_file & maat_id & "|Effective date|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                    End If
                End If
                If assessment_date <> Nothing Then
                    test_date = assessment_date
                    If Not date_range_valid(test_date) Then
                        invalid_date_file = invalid_date_file & maat_id & "|Assessment date|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                    End If
                End If
                If all_evidence_date <> Nothing Then
                    test_date = all_evidence_date
                    If Not date_range_valid(test_date) Then
                        invalid_date_file = invalid_date_file & maat_id & "|All Evidence date|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                    End If
                End If
                If rep_order_withdrawal_date <> Nothing Then
                    test_date = rep_order_withdrawal_date
                    If Not date_range_valid(test_date) Then
                        invalid_date_file = invalid_date_file & maat_id & "|Rep Order withdrawal date|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                    End If
                End If
                If uplift_applied_date <> Nothing Then
                    test_date = uplift_applied_date
                    If Not date_range_valid(test_date) Then
                        invalid_date_file = invalid_date_file & maat_id & "|Uplift Applied date|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                    End If
                End If
                If uplift_removed_date <> Nothing Then
                    test_date = uplift_removed_date
                    If Not date_range_valid(test_date) Then
                        invalid_date_file = invalid_date_file & maat_id & "|Uplift Removed date|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                    End If
                End If
                If outcome_date <> Nothing Then
                    test_date = outcome_date
                    If Not date_range_valid(test_date) Then
                        invalid_date_file = invalid_date_file & maat_id & "|Outcome date|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                    End If
                End If
                'now check asset table dates
                Dim test_idx As Integer
                For test_idx = 1 To asset_no
                    If asset_table(test_idx, 4) <> "" Then
                        test_date = CDate(asset_table(test_idx, 4))
                        If Not date_range_valid(test_date) Then
                            invalid_date_file = invalid_date_file & maat_id & "|Asset date verified|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                        End If
                    End If
                    If asset_table(test_idx, 5) <> "" Then
                        test_date = CDate(asset_table(test_idx, 5))
                        If Not date_range_valid(test_date) Then
                            invalid_date_file = invalid_date_file & maat_id & "|Asset evidence received date|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                        End If
                    End If
                Next
                'now check inc evidence table
                For test_idx = 1 To inc_ev_no
                    If inc_evidence_table(test_idx, 4) <> "" Then
                        test_date = CDate(inc_evidence_table(test_idx, 4))
                        If Not date_range_valid(test_date) Then
                            invalid_date_file = invalid_date_file & maat_id & "|Income Evidence date received|" & Format(test_date, "dd/MM/yyyy") & vbNewLine
                        End If
                    End If
                Next
                Exit Sub
            End If

            'case exists on onestep so look for changes
            'audit_file = audit_file & maat_id & "|on onestep" & vbNewLine
            change_found = False
            on_onestep = True
            param2 = "select _rowid, offence_number, offence_court, debt_amount, debt_costs, " & _
           " offenceValue, empNI, add_phone, add_fax, addEmail, prevReference, add_postcode, " & _
           " address, clientschemeID from Debtor" & _
           " where _rowid = " & debtor
            Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to read debtorID = " & debtor)
                Exit Sub
            End If

            'see if prevref has changed
            Dim orig_prevref As String
            Try
                orig_prevref = debtor_dataset.Tables(0).Rows(0).Item(10)
            Catch
                orig_prevref = ""
            End Try
            If applicant_id <> orig_prevref Then
                write_change(debtor & "|" & maat_id & "|" & "Prev Reference(Applicant)" & "|" & _
                                orig_prevref & "|" & applicant_id & vbNewLine)
                change_found = True
            End If

            'see if postcode has changed
            Dim orig_postcode As String
            Try
                orig_postcode = Trim(debtor_dataset.Tables(0).Rows(0).Item(11))
            Catch
                orig_postcode = ""
            End Try
            If debt_postcode.Length > 0 Then
                If Trim(LCase(Microsoft.VisualBasic.Left(debt_postcode, 3)) <> LCase(Microsoft.VisualBasic.Left(orig_postcode, 3))) Then
                    Dim orig_home_addr As String = ""
                    Try
                        orig_home_addr = Trim(debtor_dataset.Tables(0).Rows(0).Item(12))
                    Catch ex As Exception

                    End Try
                    Dim addr As String = ""
                    Dim idx As Integer
                    Dim origAddressFound As Boolean = False
                    Try
                        If orig_home_addr.Length > 0 Then
                            origAddressFound = True
                        End If
                    Catch ex As Exception

                    End Try
                    If origAddressFound Then
                        For idx = 1 To Microsoft.VisualBasic.Len(orig_home_addr)
                            If Mid(orig_home_addr, idx, 1) <> Chr(10) And _
                            Mid(orig_home_addr, idx, 1) <> Chr(13) Then
                                addr = addr & Mid(orig_home_addr, idx, 1)
                            Else
                                addr = addr & " "
                            End If
                        Next
                    End If

                    write_change(debtor & "|" & maat_id & "|" & "Current Address" & "|" & _
                                    addr & "|" & debt_addr1 & " " & debt_addr2 & " " & debt_addr3 & " " & _
                                    debt_addr4 & " " & debt_postcode & vbNewLine)
                    change_found = True
                End If
            End If

            'see if summons number has changed
            Dim orig_summons_no As String
            Try
                orig_summons_no = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
            Catch
                orig_summons_no = ""
            End Try
            If Trim(LCase(summons_no)) <> LCase(orig_summons_no) Then
                write_change(debtor & "|" & maat_id & "|" & "Summons number" & "|" & _
                orig_summons_no & "|" & summons_no & vbNewLine)
                change_found = True
            End If

            'get notes into dataset
            param2 = "select text, _createdDate from Note where debtorID = " & debtor & _
                            " and type = 'Client note' order by _rowid desc"
            Dim note_dataset As DataSet = get_dataset(param1, param2)
            Dim note_text As String = ""
            Dim no_of_notes As String = no_of_rows

            'see if effective date has changed
            Dim orig_effective_date As Date
            Dim orig_effective_date_found As Boolean = True
            Try
                orig_effective_date = debtor_dataset.Tables(0).Rows(0).Item(2)
            Catch ex As Exception
                orig_effective_date_found = False
            End Try
            If effective_date <> Nothing Then
                If orig_effective_date_found Then
                    If effective_date <> orig_effective_date Then
                        Dim os_date As Date = orig_effective_date
                        write_change(debtor & "|" & maat_id & "|" & "Effective date(LO date)" & _
                        "|" & Format(os_date, "dd-MM-yyyy") & "|" & Format(effective_date, "dd-MM-yyyy") & vbNewLine)
                        change_found = True
                    End If
                Else
                    If future_effective_date Then
                        Dim eff_text As String = ""
                        For idx3 = 0 To no_of_rows - 1
                            eff_text = note_dataset.Tables(0).Rows(idx3).Item(0)
                            Dim start_idx As Integer = InStr(eff_text, "Effective Date:")
                            If start_idx > 0 Then
                                eff_text = Mid(eff_text, start_idx + 15, 10)
                                Exit For
                            End If
                        Next
                        If eff_text.Length > 0 Then
                            If effective_date <> CDate(eff_text) Then
                                write_change(debtor & "|" & maat_id & "|" & "Effective date(LO date)" & _
                                                       "|" & CDate(eff_text) & "|" & Format(effective_date, "dd-MM-yyyy") & vbNewLine)
                                write_eff_date(debtor & "|" & effective_date_str & vbNewLine)
                                change_found = True
                            End If
                        End If
                    Else
                        If effective_date <> Nothing Then
                            write_change(debtor & "|" & maat_id & "|" & "Effective date(LO date)" & _
                                                "|" & " " & "|" & Format(effective_date, "dd-MM-yyyy") & vbNewLine)
                            change_found = True
                        End If
                    End If
                End If
            End If
            'see if debt amt has changed
            Dim test_amt As Decimal
            If upfront_contrib_amt_str = "" Then
                test_amt = 0
            Else
                test_amt = upfront_contrib_amt_str   ' pounds not pence/ 100
            End If
            Dim debt_amt As Decimal = debtor_dataset.Tables(0).Rows(0).Item(3)
            If test_amt <> debt_amt Then
                'no change if final costs = costs on onestep
                'If debt_amt <> final_defence_cost Then
                write_change(debtor & "|" & maat_id & "|" & "Debt amount" & "|" & _
                                Format(debt_amt, "#.##") & "|" & Format(test_amt, "#.##") & vbNewLine)
                change_found = True
                ' End If
            End If

            'see if debt costs has changed
            Dim letter_idx As Integer
            If mthly_contrib_amt_str = "" Then
                test_amt = 0
            Else
                test_amt = mthly_contrib_amt_str    'pounds not pence / 100
            End If
            Dim debt_costs As Decimal = debtor_dataset.Tables(0).Rows(0).Item(4)
            If test_amt <> debt_costs Then
                write_change(debtor & "|" & maat_id & "|" & "Debt Costs" & "|" & _
                                Format(debt_costs, "#.##") & "|" & test_amt & vbNewLine)
                change_found = True
                'change 19.2.2013 add letters to change file
                'remove 11.11.2013
                'For letter_idx = 1 To letter_no
                '    If letter_table(letter_idx).printed = Nothing Then
                '        Continue For
                '    End If
                '    Dim letter_text As String = "Letter Ref:" & letter_table(letter_idx).ref & ";"
                '    If letter_table(letter_idx).id <> Nothing Then
                '        letter_text = letter_text & "Letter Id:" & letter_table(letter_idx).id & ";"
                '    End If
                '    If letter_table(letter_idx).type <> Nothing Then
                '        letter_text = letter_text & "Letter Type:" & letter_table(letter_idx).type & ";"
                '    End If
                '    If letter_table(letter_idx).created <> Nothing Then
                '        letter_text = letter_text & "Letter Created:" & letter_table(letter_idx).created & ";"
                '    End If
                '    If letter_table(letter_idx).printed <> Nothing Then
                '        letter_text = letter_text & "Letter Printed:" & letter_table(letter_idx).printed & ";"
                '    End If
                '    'remove last ; before writing note
                '    letter_text = Microsoft.VisualBasic.Left(letter_text, letter_text.Length - 1)
                '    write_change(debtor & "|" & maat_id & "|" & "Letters" & "|" & _
                '                 letter_text & vbNewLine)
                'Next
            End If

            'see if offence value has changed
            If income_contrib_cap_str = "" Then
                test_amt = 0
            Else
                test_amt = income_contrib_cap_str     'pounds not pence / 100
            End If
            Dim offence_value As Decimal = debtor_dataset.Tables(0).Rows(0).Item(5)
            If test_amt <> 0 And test_amt <> offence_value Then
                write_change(debtor & "|" & maat_id & "|" & "Offence value" & "|" & _
                Format(offence_value, "#.##") & "|" & Format(test_amt, "#.##") & vbNewLine)
                change_found = True
            End If

            'see if NIno has changed
            Dim orig_ni_no As String
            Try
                orig_ni_no = debtor_dataset.Tables(0).Rows(0).Item(6)
            Catch
                orig_ni_no = ""
            End Try
            If ni_no > "" Then
                If LCase(ni_no) <> LCase(orig_ni_no) Then
                    write_change(debtor & "|" & maat_id & "|" & "NI No" & "|" & orig_ni_no & "|" & ni_no & vbNewLine)
                    change_found = True
                End If
            End If

            'check if phone number has changed
            Dim orig_phone_no As String
            Try
                orig_phone_no = Trim(debtor_dataset.Tables(0).Rows(0).Item(7))
            Catch
                orig_phone_no = ""
            End Try

            Dim orig_fax_no As String
            Try
                orig_fax_no = Trim(debtor_dataset.Tables(0).Rows(0).Item(8))
            Catch
                orig_fax_no = ""
            End Try
            If landline <> "" Then
                If landline <> orig_phone_no And _
                landline <> orig_fax_no Then
                    write_change(debtor & "|" & maat_id & "|" & "Phone No" & "|" & orig_phone_no & "|" & landline & vbNewLine)
                    change_found = True
                End If
            End If
            If mobile <> "" Then
                If mobile <> orig_phone_no And _
                mobile <> orig_fax_no Then
                    write_change(debtor & "|" & maat_id & "|" & "Mobile No" & "|" & orig_fax_no & "|" & mobile & vbNewLine)
                    change_found = True
                End If
            End If

            'check if email has changed
            Dim orig_email As String
            Try
                orig_email = debtor_dataset.Tables(0).Rows(0).Item(9)
            Catch
                orig_email = ""
            End Try
            If email <> orig_email Then
                write_change(debtor & "|" & maat_id & "|" & "Email" & "|" & orig_email & "|" & email & vbNewLine)
                change_found = True
            End If
            Dim idx4 As Integer

            'see if outcomes have changed
            If outcome <> "" Then
                Dim note_outcome As String = ""
                Dim note2_outcome As String = ""
                Dim first_outcome_found As Boolean = False
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "outcome:")
                    If start_idx > 0 Then
                        start_idx += 8
                    End If
                    If start_idx > 0 Then
                        If first_outcome_found = False Then
                            For idx4 = start_idx To note_text.Length
                                If Mid(note_text, idx4, 1) = ";" Then
                                    note_outcome = Mid(note_text, start_idx, idx4 - start_idx)
                                    Exit For
                                End If
                            Next
                            'continue if there is a second outcome
                            If outcome2 = "" Then
                                Exit For
                            End If
                            first_outcome_found = True
                        Else
                            For idx4 = start_idx To note_text.Length
                                If Mid(note_text, idx4, 1) = ";" Then
                                    note2_outcome = Mid(note_text, start_idx, idx4 - start_idx)
                                    Exit For
                                End If
                            Next
                            Exit For
                        End If
                    End If
                Next
                If LCase(outcome) <> note_outcome And LCase(outcome) <> note2_outcome Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Outcome" & "|" & "" & "|" & outcome & vbNewLine)
                    write_note(debtor & "|" & "OUTCOME:" & outcome)
                End If
                If outcome2 <> "" Then
                    If LCase(outcome2) <> note_outcome And LCase(outcome2) <> note2_outcome Then
                        change_found = True
                        write_outcome(debtor & "|" & maat_id & "|" & "Outcome" & "|" & "" & "|" & outcome2 & vbNewLine)
                        write_note(debtor & "|" & "OUTCOME:" & outcome2)
                    End If
                End If
            End If


            'if case-type is appeal and clientscheme <> 2109(appeal scheme) then write to outcome file
            If UCase(Microsoft.VisualBasic.Left(case_type, 6)) = "APPEAL" And _
            debtor_dataset.Tables(0).Rows(0).Item(13) <> 2109 Then
                change_found = True
                write_outcome(debtor & "|" & maat_id & "|" & "Case type Appeal" & "|" & " " & "|" & case_type & vbNewLine)
            End If

            'see if bank account no has changed
            Dim bank_note As String = ""
            If bank_acc_no.Length > 0 Then
                Dim note_acc_no = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "bank acc no:")
                    If start_idx > 0 Then
                        start_idx += 12
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_acc_no = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_acc_no = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If
                    End If
                    If note_acc_no <> "" Then
                        Exit For
                    End If
                Next
                If bank_acc_no <> note_acc_no Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Bank Acc No" & "|" & note_acc_no & "|" & bank_acc_no & vbNewLine)
                    bank_note = debtor & "|" & "BANK DETAILS;Bank Acc No:" & bank_acc_no & ";"
                End If
            End If

            'see if bank sort code has changed
            If bank_sort_code.Length > 0 Then
                Dim note_sort_code = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "bank sortcode:")
                    If start_idx = 0 Then
                        start_idx = InStr(note_text, "bank sort code:") + 1
                    End If
                    If start_idx > 1 Then
                        start_idx += 14
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_sort_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_sort_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If
                    End If
                    If note_sort_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(bank_sort_code) <> LCase(note_sort_code) Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Bank Sortcode" & "|" & note_sort_code & "|" & bank_sort_code & vbNewLine)
                    If bank_note = "" Then
                        bank_note = debtor & "|" & "BANK DETAILS;Bank Acc No:" & bank_acc_no & ";"
                    End If
                    bank_note = bank_note & "Bank Sortcode:" & bank_sort_code & ";"
                ElseIf bank_note <> "" Then
                    bank_note = bank_note & "Bank Sortcode:" & bank_sort_code & ";"
                End If
            ElseIf bank_note <> "" Then
                bank_note = bank_note & "Bank Sortcode:" & bank_sort_code & ";"
            End If

            'see if bank acc name has changed
            If bank_acc_name.Length > 0 Then
                Dim note_acc_name As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "bank acc name:")
                    If start_idx > 0 Then
                        start_idx += 14
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_acc_name = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_acc_name = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If
                    End If
                    If note_acc_name <> "" Then
                        Exit For
                    End If
                Next
                If LCase(bank_acc_name) <> LCase(note_acc_name) Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Bank Acc Name" & "|" & note_acc_name & "|" & bank_acc_name & vbNewLine)
                    If bank_note = "" Then
                        If bank_note = "" Then
                            bank_note = debtor & "|" & "BANK DETAILS;Bank Acc No:" & _
                            bank_acc_no & ";Bank Sortcode:" & bank_sort_code & ";"
                        End If
                    End If
                    bank_note = bank_note & "Bank Acc Name:" & bank_acc_name
                    write_note(bank_note)
                ElseIf bank_note <> "" Then
                    bank_note = bank_note & "Bank Acc Name:" & bank_acc_name
                    write_note(bank_note)
                End If
            ElseIf bank_note <> "" Then
                bank_note = bank_note & "Bank Acc Name:" & bank_acc_name
                write_note(bank_note)
            End If

            'see if Rep-Order Withdrawal Date has changed
            If rep_order_withdrawal_date <> Nothing Then
                Dim note_date As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "rep-order-withdrawn-date:")
                    If start_idx > 0 Then
                        start_idx += 25
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_date = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_date = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If
                    End If
                    If note_date <> "" Then
                        Exit For
                    End If
                Next
                If note_date = "" Then
                    change_found = True
                    write_outcome(debtor & "|" & maat_id & "|" & "Rep-order-Withdrawn-Date" & "||" & _
                    Format(rep_order_withdrawal_date, "dd.MMM.yyyy") & "|" & vbNewLine)
                    write_note(debtor & "|" & "REP-ORDER-WITHDRAWN-DATE:" & Format(rep_order_withdrawal_date, "dd.MMM.yyyy"))
                Else
                    If rep_order_withdrawal_date <> CDate(note_date) Then
                        change_found = True
                        write_outcome(debtor & "|" & maat_id & "|" & "Rep-order-Withdrawn-Date" & "||" & _
                        Format(rep_order_withdrawal_date, "dd.MMM.yyyy") & "|" & vbNewLine)
                        write_note(debtor & "|" & "REP-ORDER-WITHDRAWN-DATE:" & Format(rep_order_withdrawal_date, "dd.MMM.yyyy"))
                    End If
                End If
            End If

            'see if final costs entered
            'If final_defence_cost <> Nothing Then
            '    If debt_amt <> final_defence_cost And _
            '        debt_costs <> final_defence_cost Then
            '        write_change(debtor & "|" & maat_id & "|" & "Final Costs" & "|" & " " & "|" & _
            '        Format(final_defence_cost, "F") & vbNewLine)
            '        write_note(debtor & "|" & "FINAL-DEFENCE-COSTS:" & Format(final_defence_cost, "F"))
            '        change_found = True
            '    End If
            'End If

            'see if inc-uplift-applied has changed
            Dim note_inc_uplift_applied As String = ""
            For idx3 = 0 To no_of_notes - 1
                note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                Dim start_idx As Integer = InStr(note_text, "income-uplift-applied:")
                If start_idx > 0 Then
                    start_idx += 22
                Else
                    start_idx = InStr(LCase(note_text), "inc uplift applied:")
                    If start_idx > 0 Then
                        start_idx += 19
                    End If
                End If
                If start_idx > 0 Then
                    note_inc_uplift_applied = UCase(Mid(note_text, start_idx, 1))
                    If note_inc_uplift_applied = "Y" Then
                        note_inc_uplift_applied = "Yes"
                    Else
                        note_inc_uplift_applied = "No"
                    End If
                End If
                If note_inc_uplift_applied <> "" Then
                    Exit For
                End If
            Next
            If note_inc_uplift_applied = "" Then
                note_inc_uplift_applied = "No"
            End If
            If income_uplift_applied <> "" And income_uplift_applied <> note_inc_uplift_applied Then
                write_change(debtor & "|" & maat_id & "|" & "Income Uplift Applied" & "|" & _
                income_uplift_applied & "|" & note_inc_uplift_applied & vbNewLine)
                write_note(debtor & "|" & "INCOME-UPLIFT-APPLIED:" & income_uplift_applied)
                change_found = True
            End If

            'see if equity amount verified has changed
            Dim note_equity_amt_verified As String = ""
            For idx3 = 0 To no_of_notes - 1
                note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                Dim start_idx As Integer = InStr(note_text, "equity amt verified:")
                If start_idx > 0 Then
                    start_idx += 20
                End If
                If start_idx > 0 Then
                    note_equity_amt_verified = UCase(Mid(note_text, start_idx, 1))
                    If note_equity_amt_verified = "Y" Then
                        note_equity_amt_verified = "Yes"
                    Else
                        note_equity_amt_verified = "No"
                    End If
                End If
                If note_equity_amt_verified <> "" Then
                    Exit For
                End If
            Next
            If note_equity_amt_verified = "" Then
                note_equity_amt_verified = "No"
            End If
            If equity_amt_verified = "" Then
                equity_amt_verified = "No"
            End If
            If equity_amt_verified <> note_equity_amt_verified Then
                write_change(debtor & "|" & maat_id & "|" & "Equity Amt Verified" & "|" & _
                equity_amt_verified & "|" & note_equity_amt_verified & vbNewLine)
                write_note(debtor & "|" & "Equity Amt Verified:" & equity_amt_verified)
                change_found = True
            End If

            'see if equity amount has changed
            'Dim note_equity_amt As Decimal = -9999
            'For idx3 = 0 To no_of_notes - 1
            '    note_text = note_dataset.Tables(0).Rows(idx3).Item(0)
            '    Dim start_idx As Integer = InStr(note_text, "Equity Amt:")
            '    If start_idx > 0 Then
            '        start_idx += 11
            '    End If
            '    If start_idx > 0 Then
            '        For idx4 = start_idx To note_text.Length
            '            If Mid(note_text, idx4, 1) = ";" Then
            '                note_equity_amt = Mid(note_text, start_idx, idx4 - start_idx)
            '                Exit For
            '            End If
            '        Next
            '        If idx4 > note_text.Length Then
            '            note_equity_amt = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '        End If
            '    End If
            '    If note_equity_amt <> -9999 Then
            '        Exit For
            '    End If
            'Next
            'If equity_amt <> note_equity_amt And equity_amt <> -9999 Then
            '    If note_equity_amt <> -9999 Then
            '        write_change(debtor & "|" & maat_id & "|" & "Equity Amt" & "|" & _
            '                        equity_amt & "|" & note_equity_amt & vbNewLine)
            '        write_note(debtor & "|" & "Equity Amt:" & equity_amt)
            '        change_found = True
            '    End If
            'End If

            'see if applicant equity amount has changed
            'Dim note_appl_equity_amt As Decimal = -9999
            'For idx3 = 0 To no_of_notes - 1
            '    note_text = note_dataset.Tables(0).Rows(idx3).Item(0)
            '    Dim start_idx As Integer = InStr(note_text, "Applicant Equity Amount:")
            '    If start_idx > 0 Then
            '        start_idx += 24
            '    End If
            '    If start_idx > 0 Then
            '        For idx4 = start_idx To note_text.Length
            '            If Mid(note_text, idx4, 1) = ";" Then
            '                note_appl_equity_amt = Mid(note_text, start_idx, idx4 - start_idx)
            '                Exit For
            '            End If
            '        Next
            '        If idx4 > note_text.Length Then
            '            note_appl_equity_amt = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '        End If
            '    End If
            '    If note_appl_equity_amt <> -9999 Then
            '        Exit For
            '    End If
            'Next
            'If eq_appl_equity_amt <> note_appl_equity_amt And eq_appl_equity_amt <> -9999 Then
            '    If note_appl_equity_amt <> -9999 Then
            '        write_change(debtor & "|" & maat_id & "|" & "Applicant Equity Amount" & "|" & _
            '                        eq_appl_equity_amt & "|" & note_appl_equity_amt & vbNewLine)
            '        write_note(debtor & "|" & "Applicant Equity Amount:" & eq_appl_equity_amt)
            '        change_found = True
            '    End If
            'End If

            'see if partner equity amount has changed
            'Dim note_part_equity_amt As Decimal = -9999
            'For idx3 = 0 To no_of_notes - 1
            '    note_text = note_dataset.Tables(0).Rows(idx3).Item(0)
            '    Dim start_idx As Integer = InStr(note_text, "Partner Equity Amount:")
            '    If start_idx > 0 Then
            '        start_idx += 22
            '    End If
            '    If start_idx > 0 Then
            '        For idx4 = start_idx To note_text.Length
            '            If Mid(note_text, idx4, 1) = ";" Then
            '                note_part_equity_amt = Mid(note_text, start_idx, idx4 - start_idx)
            '                Exit For
            '            End If
            '        Next
            '        If idx4 > note_text.Length Then
            '            note_part_equity_amt = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '        End If
            '    End If
            '    If note_part_equity_amt <> -9999 Then
            '        Exit For
            '    End If
            'Next
            'If eq_partner_equity_amt <> note_part_equity_amt And eq_partner_equity_amt <> -9999 Then
            '    If note_part_equity_amt <> -9999 Then
            '        write_change(debtor & "|" & maat_id & "|" & "Partner Equity Amount" & "|" & _
            '                        eq_partner_equity_amt & "|" & note_part_equity_amt & vbNewLine)
            '        write_note(debtor & "|" & "Partner Equity Amount:" & eq_partner_equity_amt)
            '        change_found = True
            '    End If
            'End If

            'see if Has Partner has changed
            If has_partner <> "" Then
                Dim note_has_partner As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "has partner:")
                    If start_idx > 0 Then
                        start_idx += 12
                    End If
                    If start_idx > 0 Then
                        note_has_partner = Mid(note_text, start_idx, 1)
                        If note_has_partner = "y" Then
                            note_has_partner = "yes"
                        Else
                            note_has_partner = "no"
                        End If
                    End If
                    If note_has_partner <> "" Then
                        Exit For
                    End If
                Next

                If has_partner <> note_has_partner Then
                    write_note(debtor & "|" & "Has Partner:" & has_partner)
                    change_found = True
                End If
            End If

            'see if ci_code has changed
            If ci_code <> "" Then
                Dim note_ci_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "contrary interest code:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_ci_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_ci_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_ci_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(ci_code) <> note_ci_code Then
                    write_note(debtor & "|" & "Contrary Interest Code:" & ci_code)
                    change_found = True
                End If
            End If

            'see if ci_desc has changed
            If ci_desc <> "" Then
                Dim note_ci_desc As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "contrary interest description:")
                    If start_idx > 0 Then
                        start_idx += 30
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_ci_desc = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_ci_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_ci_desc <> "" Then
                        Exit For
                    End If
                Next
                If LCase(ci_desc) <> note_ci_desc Then
                    write_note(debtor & "|" & "Contrary Interest Description:" & ci_desc)
                    change_found = True
                End If
            End If

            'see if equity declared value has changed
            'If eq_declared_value <> Nothing Then
            '    Dim note_declared_value As Decimal = Nothing
            '    For idx3 = 0 To no_of_notes - 1
            '        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            '        Dim start_idx As Integer = InStr(note_text, "declared value equity:")
            '        If start_idx > 0 Then
            '            start_idx += 22
            '        End If
            '        If start_idx > 0 Then
            '            For idx4 = start_idx To note_text.Length
            '                If Mid(note_text, idx4, 1) = ";" Then
            '                    note_declared_value = Mid(note_text, start_idx, idx4 - start_idx)
            '                    Exit For
            '                End If
            '            Next
            '            If idx4 > note_text.Length Then
            '                note_declared_value = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '            End If

            '        End If
            '        If note_declared_value <> Nothing Then
            '            Exit For
            '        End If
            '    Next
            '    If eq_declared_value <> note_declared_value Then
            '        write_note(debtor & "|" & "Declared Value Equity:" & eq_declared_value)
            '        change_found = True
            '    End If
            'End If

            ''see if capital declared value has changed
            'If cap_declared_value <> Nothing Then
            '    Dim note_declared_value As Decimal = Nothing
            '    For idx3 = 0 To no_of_notes - 1
            '        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            '        Dim start_idx As Integer = InStr(note_text, "declared value capital:")
            '        If start_idx > 0 Then
            '            start_idx += 24
            '        End If
            '        If start_idx > 0 Then
            '            For idx4 = start_idx To note_text.Length
            '                If Mid(note_text, idx4, 1) = ";" Then
            '                    note_declared_value = Mid(note_text, start_idx, idx4 - start_idx)
            '                    Exit For
            '                End If
            '            Next
            '            If idx4 > note_text.Length Then
            '                note_declared_value = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '            End If

            '        End If
            '        If note_declared_value <> Nothing Then
            '            Exit For
            '        End If
            '    Next
            '    If cap_declared_value <> note_declared_value Then
            '        write_note(debtor & "|" & "Declared Value Capital:" & eq_declared_value)
            '        change_found = True
            '    End If
            'End If

            'see if no fixed abode has changed
            Dim note_nfa As String = ""
            If nfa <> "" Then
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "no fixed abode:")
                    If start_idx > 0 Then
                        start_idx += 15
                    End If
                    If start_idx > 0 Then
                        note_nfa = Mid(note_text, start_idx, 1)
                        If note_nfa = "y" Then
                            note_nfa = "yes"
                        Else
                            note_nfa = "no"
                        End If
                    End If
                    If note_nfa <> "" Then
                        Exit For
                    End If
                Next
                If nfa <> note_nfa Then
                    write_note(debtor & "|" & "No Fixed Abode:" & nfa)
                    change_found = True
                End If
            End If

            'see if special investigation has changed
            If si <> "" Then
                Dim note_si As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "special investigation:")
                    If start_idx > 0 Then
                        start_idx += 22
                    End If
                    If start_idx > 0 Then
                        note_si = Mid(note_text, start_idx, 1)
                        If note_si = "y" Then
                            note_si = "yes"
                        Else
                            note_si = "no"
                        End If
                    End If
                    If note_si <> "" Then
                        Exit For
                    End If
                Next
                If si <> note_si Then
                    write_note(debtor & "|" & "Special Investigation:" & si)
                    change_found = True
                End If
            End If

            'see if emp status has changed
            If emp_status <> "" Then
                Dim note_emp_status As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "emp status:")
                    If start_idx > 0 Then
                        start_idx += 11
                    End If
                    If start_idx > 0 And InStr(note_text, "partner emp status:") = 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_emp_status = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_emp_status = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_emp_status <> "" Then
                        Exit For
                    End If
                Next
                If LCase(emp_status) <> note_emp_status Then
                    write_note(debtor & "|" & "Emp Status:" & emp_status)
                    change_found = True
                End If
            End If

            'see if emp code has changed
            If emp_code <> "" Then
                Dim note_emp_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "emp code:")
                    If start_idx > 0 Then
                        start_idx += 9
                    End If
                    If start_idx > 0 And InStr(note_text, "partner emp code") = 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_emp_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_emp_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_emp_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(emp_code) <> note_emp_code Then
                    write_note(debtor & "|" & "Emp Code:" & emp_code)
                    change_found = True
                End If
            End If

            'see if Disability Declaration has changed
            If disability_declaration <> "" Then
                Dim note_disability_declaration As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "disability declaration:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_disability_declaration = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_disability_declaration = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_disability_declaration <> "" Then
                        Exit For
                    End If
                Next
                If LCase(disability_declaration) <> note_disability_declaration Then
                    write_note(debtor & "|" & "Disability Declaration:" & disability_declaration)
                    change_found = True
                End If
            End If

            'see if Disability Description has changed
            If disability_description <> "" Then
                Dim note_disability_description As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "disability description:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_disability_description = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                    End If
                    If note_disability_description <> "" Then
                        If LCase(disability_description) = note_disability_description Then
                            Exit For
                        Else
                            note_text = Microsoft.VisualBasic.Right(note_text, note_text.Length - start_idx)
                            start_idx = InStr(note_text, "disability description:")
                            If start_idx > 0 Then
                                start_idx += 23
                            End If
                            If start_idx > 0 Then
                                For idx4 = start_idx To note_text.Length
                                    If Mid(note_text, idx4, 1) = ";" Then
                                        note_disability_description = Mid(note_text, start_idx, idx4 - start_idx)
                                        Exit For
                                    End If
                                Next
                            End If
                            Exit For
                        End If
                    End If
                Next
                If LCase(disability_description) <> note_disability_description Then
                    write_note(debtor & "|" & "Disability Description:" & disability_description)
                    change_found = True
                End If
            End If

            'see if case-type has changed
            If case_type <> "" Then
                Dim note_case_type As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "case type:")
                    If start_idx > 0 Then
                        start_idx += 10
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_case_type = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_case_type = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_case_type <> "" Then
                        Exit For
                    End If
                Next
                If LCase(case_type) <> note_case_type Then
                    write_note(debtor & "|" & "Case type:" & case_type)
                    change_found = True
                End If
            End If

            'see if case-type code has changed
            If case_type_code <> "" Then
                Dim note_case_type_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "case type code:")
                    If start_idx > 0 Then
                        start_idx += 15
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_case_type_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_case_type_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_case_type_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(case_type_code) <> note_case_type_code Then
                    write_note(debtor & "|" & "Case type Code:" & case_type_code)
                    change_found = True
                End If
            End If
            'see if Rep Status has changed
            If rep_status <> "" Then
                Dim note_rep_status As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "rep status:")
                    If start_idx > 0 Then
                        start_idx += 11
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_rep_status = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_rep_status = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_rep_status <> "" Then
                        Exit For
                    End If
                Next
                If note_rep_status = "" Then
                    write_note(debtor & "|" & "Rep Status:" & rep_status)
                    change_found = True
                Else
                    If LCase(rep_status) <> note_rep_status Then
                        write_note(debtor & "|" & "Rep Status:" & rep_status)
                        change_found = True
                    End If
                End If
            End If

            'see if Rep Status Description has changed
            If rep_status_desc <> "" Then
                Dim note_rep_status_desc As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "rep status description:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_rep_status_desc = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_rep_status_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_rep_status_desc <> "" Then
                        Exit For
                    End If
                Next
                If note_rep_status_desc = "" Then
                    write_note(debtor & "|" & "Rep Status Description:" & rep_status_desc)
                    change_found = True
                Else
                    If LCase(rep_status_desc) <> note_rep_status_desc Then
                        write_note(debtor & "|" & "Rep Status Description:" & rep_status_desc)
                        change_found = True
                    End If
                End If
            End If

            'see if Rep Status Date has changed
            If rep_status_date <> Nothing Then
                Dim note_rep_status_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "rep status date:")
                    If start_idx > 0 Then
                        start_idx += 16
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_rep_status_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_rep_status_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_rep_status_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_rep_status_date_str)
                    If rep_status_date <> test_date Then
                        write_note(debtor & "|" & "Rep Status Date:" & Format(rep_status_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Rep Status Date:" & Format(rep_status_date, "dd/MM/yyyy"))
                End Try
            End If

            'see if in court custody has changed
            If in_court_custody <> "" Then
                Dim note_in_court_custody As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "in court custody:")
                    If start_idx > 0 Then
                        start_idx += 17
                    End If
                    If start_idx > 0 Then
                        note_in_court_custody = Mid(note_text, start_idx, 1)
                        If note_in_court_custody = "y" Then
                            note_in_court_custody = "yes"
                        Else
                            note_in_court_custody = "no"
                        End If
                    End If
                    If note_in_court_custody <> "" Then
                        Exit For
                    End If
                Next
                If LCase(in_court_custody) <> note_in_court_custody Then
                    write_note(debtor & "|" & "In Court Custody:" & in_court_custody)
                    change_found = True
                End If
            End If

            'see if imprisoned has changed
            If imprisoned <> "" Then
                Dim note_imprisoned As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "inprison:")
                    If start_idx > 0 Then
                        start_idx += 9
                    End If
                    If start_idx > 0 Then
                        note_imprisoned = Mid(note_text, start_idx, 1)
                        If note_imprisoned = "y" Then
                            note_imprisoned = "yes"
                        Else
                            note_imprisoned = "no"
                        End If
                    End If
                    If note_imprisoned <> "" Then
                        Exit For
                    End If
                Next
                If LCase(imprisoned) <> note_imprisoned Then
                    write_note(debtor & "|" & "InPrison:" & imprisoned)
                    change_found = True
                End If
            End If

            'see if sentence date has changed
            If sentence_date <> Nothing Then
                Dim note_sentence_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "sentenceorderdate:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_sentence_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_sentence_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_sentence_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_sentence_date_str)
                    If sentence_date <> test_date Then
                        write_note(debtor & "|" & "SentenceOrderDate:" & Format(sentence_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "SentenceOrderDate:" & Format(sentence_date, "dd/MM/yyyy"))
                End Try
            End If

            'see if committal Date has changed
            If committal_date <> Nothing Then
                Dim note_committal_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "committal date:")
                    If start_idx > 0 Then
                        start_idx += 15
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_committal_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_committal_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_committal_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_committal_date_str)
                    If committal_date <> test_date Then
                        write_note(debtor & "|" & "Committal Date:" & Format(committal_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Committal Date:" & Format(committal_date, "dd/MM/yyyy"))
                End Try
            End If

            'see if hardship review Date has changed
            If hardship_review_date <> Nothing Then
                Dim note_hardship_review_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "hardship review date:")
                    If start_idx > 0 Then
                        start_idx += 21
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_hardship_review_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_hardship_review_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_hardship_review_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_hardship_review_date_str)
                    If hardship_review_date <> test_date Then
                        write_note(debtor & "|" & "Hardship Review Date:" & Format(hardship_review_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Hardship Review Date:" & Format(hardship_review_date, "dd/MM/yyyy"))
                End Try
            End If


            'see if Hardship Result has changed
            If hardship_result <> "" Then
                Dim note_hardship_result As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "hardship review result:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_hardship_result = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_hardship_result = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_hardship_result <> "" Then
                        Exit For
                    End If
                Next
                If LCase(hardship_result) <> note_hardship_result Then
                    write_note(debtor & "|" & "Hardship Review Result:" & hardship_result)
                    change_found = True
                End If
            End If

            'see if assessment reason has changed
            If assessment_reason <> "" Then
                Dim note_assessment_reason As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "assessment reason:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_assessment_reason = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_assessment_reason = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_assessment_reason <> "" Then
                        Exit For
                    End If
                Next
                If LCase(assessment_reason) <> note_assessment_reason Then
                    write_note(debtor & "|" & "Assessment Reason:" & assessment_reason)
                    change_found = True
                End If
            End If

            'see if assessment code has changed
            If assessment_code <> "" Then
                Dim note_assessment_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "assessment code:")
                    If start_idx > 0 Then
                        start_idx += 16
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_assessment_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_assessment_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_assessment_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(assessment_code) <> note_assessment_code Then
                    write_note(debtor & "|" & "Assessment Code:" & assessment_code)
                    change_found = True
                End If
            End If
            'see if assessment Date has changed
            If assessment_date <> Nothing Then
                Dim note_assessment_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "assessment date:")
                    If start_idx > 0 Then
                        start_idx += 16
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_assessment_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_assessment_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_assessment_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_assessment_date_str)
                    If assessment_date <> test_date Then
                        write_note(debtor & "|" & "Assessment Date:" & Format(assessment_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Assessment Date:" & Format(assessment_date, "dd/MM/yyyy"))
                End Try
            End If


            ''see if equity bedroom count has changed
            'If eq_bedroom_count <> "" Then
            '    Dim note_bedroom_count As String = ""
            '    For idx3 = 0 To no_of_notes - 1
            '        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            '        Dim start_idx As Integer = InStr(note_text, "bedroom count equity:")
            '        If start_idx > 0 Then
            '            start_idx += 21
            '        End If
            '        If start_idx > 0 Then
            '            For idx4 = start_idx To note_text.Length
            '                If Mid(note_text, idx4, 1) = ";" Then
            '                    note_bedroom_count = Mid(note_text, start_idx, idx4 - start_idx)
            '                    Exit For
            '                End If
            '            Next
            '            If idx4 > note_text.Length Then
            '                note_bedroom_count = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '            End If

            '        End If
            '        If note_bedroom_count <> "" Then
            '            Exit For
            '        End If
            '    Next
            '    If LCase(eq_bedroom_count) <> note_bedroom_count Then
            '        write_note(debtor & "|" & "Bedroom Count Equity:" & eq_bedroom_count)
            '        change_found = True
            '    End If
            'End If

            'see if undeclared property has changed
            If undeclared_property <> "" Then
                Dim note_undeclared_property As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "undeclared property:")
                    If start_idx > 0 Then
                        start_idx += 20
                    End If
                    If start_idx > 0 Then
                        note_undeclared_property = Mid(note_text, start_idx, 1)
                        If note_undeclared_property = "y" Then
                            note_undeclared_property = "yes"
                        Else
                            note_undeclared_property = "no"
                        End If
                    End If
                    If note_undeclared_property <> "" Then
                        Exit For
                    End If
                Next
                If LCase(undeclared_property) <> note_undeclared_property Then
                    write_note(debtor & "|" & "Undeclared Property:" & undeclared_property)
                    change_found = True
                End If
            End If

            'see if pcent owned applicant has changed
            'If eq_percent_owned_appl <> -999 Then
            '    Dim note_percent_owned_appl As String = ""
            '    For idx3 = 0 To no_of_notes - 1
            '        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            '        Dim start_idx As Integer = InStr(note_text, "percent owned applicant:")
            '        If start_idx > 0 Then
            '            start_idx += 24
            '        End If
            '        If start_idx > 0 Then
            '            For idx4 = start_idx To note_text.Length
            '                If Mid(note_text, idx4, 1) = ";" Then
            '                    note_percent_owned_appl = Mid(note_text, start_idx, idx4 - start_idx)
            '                    Exit For
            '                End If
            '            Next
            '            If idx4 > note_text.Length Then
            '                note_percent_owned_appl = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '            End If

            '        End If
            '        If note_percent_owned_appl <> "" Then
            '            Exit For
            '        End If
            '    Next
            '    Dim test_number As Decimal
            '    Try
            '        test_number = note_percent_owned_appl
            '        If eq_percent_owned_appl <> test_number Then
            '            write_note(debtor & "|" & "Percent Owned Applicant:" & eq_percent_owned_appl)
            '            change_found = True
            '        End If
            '    Catch ex As Exception
            '        write_note(debtor & "|" & "Percent Owned Applicant:" & eq_percent_owned_appl)
            '        change_found = True
            '    End Try

            'End If

            'see if pcent owned partner has changed
            'If eq_percent_owned_partner <> -999 Then
            '    Dim note_percent_owned_partner As String = ""
            '    For idx3 = 0 To no_of_notes - 1
            '        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            '        Dim start_idx As Integer = InStr(note_text, "percent owned partner:")
            '        If start_idx > 0 Then
            '            start_idx += 22
            '        End If
            '        If start_idx > 0 Then
            '            For idx4 = start_idx To note_text.Length
            '                If Mid(note_text, idx4, 1) = ";" Then
            '                    note_percent_owned_partner = Mid(note_text, start_idx, idx4 - start_idx)
            '                    Exit For
            '                End If
            '            Next
            '            If idx4 > note_text.Length Then
            '                note_percent_owned_partner = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '            End If

            '        End If
            '        If note_percent_owned_partner <> "" Then
            '            Exit For
            '        End If
            '    Next
            '    Dim test_number As Decimal
            '    Try
            '        test_number = note_percent_owned_partner
            '        If eq_percent_owned_partner <> test_number Then
            '            write_note(debtor & "|" & "Percent Owned Partner:" & eq_percent_owned_partner)
            '            change_found = True
            '        End If
            '    Catch ex As Exception
            '        write_note(debtor & "|" & "Percent Owned Partner:" & eq_percent_owned_partner)
            '        change_found = True
            '    End Try
            'End If

            'see if declared mortgage has changed
            'If eq_declared_mortgage <> -99999 Then
            '    Dim note_declared_mortgage As String = ""
            '    For idx3 = 0 To no_of_notes - 1
            '        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            '        Dim start_idx As Integer = InStr(note_text, "declared mortgage:")
            '        If start_idx > 0 Then
            '            start_idx += 18
            '        End If
            '        If start_idx > 0 Then
            '            For idx4 = start_idx To note_text.Length
            '                If Mid(note_text, idx4, 1) = ";" Then
            '                    note_declared_mortgage = Mid(note_text, start_idx, idx4 - start_idx)
            '                    Exit For
            '                End If
            '            Next
            '            If idx4 > note_text.Length Then
            '                note_declared_mortgage = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '            End If

            '        End If
            '        If note_declared_mortgage <> "" Then
            '            Exit For
            '        End If
            '    Next
            '    Dim test_number As Decimal
            '    Try
            '        test_number = note_declared_mortgage
            '        If eq_declared_mortgage <> test_number Then
            '            write_note(debtor & "|" & "Declared Mortgage:" & eq_declared_mortgage)
            '            change_found = True
            '        End If
            '    Catch ex As Exception
            '        write_note(debtor & "|" & "Declared Mortgage:" & eq_declared_mortgage)
            '        change_found = True
            '    End Try
            'End If

            'see if verified mortgage has changed
            'If eq_verified_mortgage <> -99999 Then
            '    Dim note_verified_mortgage As String = ""
            '    For idx3 = 0 To no_of_notes - 1
            '        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            '        Dim start_idx As Integer = InStr(note_text, "verified mortgage:")
            '        If start_idx > 0 Then
            '            start_idx += 18
            '        End If
            '        If start_idx > 0 Then
            '            For idx4 = start_idx To note_text.Length
            '                If Mid(note_text, idx4, 1) = ";" Then
            '                    note_verified_mortgage = Mid(note_text, start_idx, idx4 - start_idx)
            '                    Exit For
            '                End If
            '            Next
            '            If idx4 > note_text.Length Then
            '                note_verified_mortgage = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '            End If

            '        End If
            '        If note_verified_mortgage <> "" Then
            '            Exit For
            '        End If
            '    Next
            '    Dim test_number As Decimal
            '    Try
            '        test_number = note_verified_mortgage
            '        If eq_verified_mortgage <> test_number Then
            '            write_note(debtor & "|" & "Verified Mortgage:" & eq_verified_mortgage)
            '            change_found = True
            '        End If
            '    Catch ex As Exception
            '        write_note(debtor & "|" & "Verified Mortgage:" & eq_verified_mortgage)
            '        change_found = True
            '    End Try
            'End If

            ''see if verified market value has changed
            'If eq_verified_market_value <> -99999 Then
            '    Dim note_verified_market_value As String = ""
            '    For idx3 = 0 To no_of_notes - 1
            '        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            '        Dim start_idx As Integer = InStr(note_text, "verified market value:")
            '        If start_idx > 0 Then
            '            start_idx += 22
            '        End If
            '        If start_idx > 0 Then
            '            For idx4 = start_idx To note_text.Length
            '                If Mid(note_text, idx4, 1) = ";" Then
            '                    note_verified_market_value = Mid(note_text, start_idx, idx4 - start_idx)
            '                    Exit For
            '                End If
            '            Next
            '            If idx4 > note_text.Length Then
            '                note_verified_market_value = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '            End If

            '        End If
            '        If note_verified_market_value <> "" Then
            '            Exit For
            '        End If
            '    Next
            '    Dim test_number As Decimal
            '    Try
            '        test_number = note_verified_market_value
            '        If eq_verified_market_value <> test_number Then
            '            write_note(debtor & "|" & "Verified Market Value:" & eq_verified_market_value)
            '            change_found = True
            '        End If
            '    Catch ex As Exception
            '        write_note(debtor & "|" & "Verified Market Value:" & eq_verified_market_value)
            '        change_found = True
            '    End Try
            'End If

            'see if no capital declared has changed
            If no_capital_declared <> "" Then
                Dim note_no_capital_declared As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "no capital declared:")
                    If start_idx > 0 Then
                        start_idx += 20
                    End If
                    If start_idx > 0 Then
                        note_no_capital_declared = Mid(note_text, start_idx, 1)
                        If note_no_capital_declared = "y" Then
                            note_no_capital_declared = "yes"
                        Else
                            note_no_capital_declared = "no"
                        End If
                    End If
                    If note_no_capital_declared <> "" Then
                        Exit For
                    End If
                Next
                If LCase(no_capital_declared) <> note_no_capital_declared Then
                    write_note(debtor & "|" & "No Capital Declared:" & no_capital_declared)
                    change_found = True
                End If
            End If

            'see if contrary interest has changed
            If contrary_interest <> "" Then
                Dim note_contrary_interest As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "contrary interest:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        note_contrary_interest = Mid(note_text, start_idx, 1)
                        If note_contrary_interest = "y" Then
                            note_contrary_interest = "yes"
                        Else
                            note_contrary_interest = "no"
                        End If
                    End If
                    If note_contrary_interest <> "" Then
                        Exit For
                    End If
                Next
                If LCase(contrary_interest) <> note_contrary_interest Then
                    write_note(debtor & "|" & "Contrary Interest:" & contrary_interest)
                    change_found = True
                End If
            End If

            'see if partner first name has changed
            If partner_first_name <> "" Then
                Dim note_partner_first_name As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "partner first name:")
                    If start_idx > 0 Then
                        start_idx += 19
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_partner_first_name = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_partner_first_name = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_partner_first_name <> "" Then
                        Exit For
                    End If
                Next
                If LCase(partner_first_name) <> note_partner_first_name Then
                    write_note(debtor & "|" & "Partner First Name:" & partner_first_name)
                    change_found = True
                End If
            End If

            'see if partner last name has changed
            If partner_last_name <> "" Then
                Dim note_partner_last_name As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "partner last name:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_partner_last_name = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_partner_last_name = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_partner_last_name <> "" Then
                        Exit For
                    End If
                Next
                If LCase(partner_last_name) <> note_partner_last_name Then
                    write_note(debtor & "|" & "Partner Last Name:" & partner_last_name)
                    change_found = True
                End If
            End If

            'see if partner dob has changed
            If partner_dob <> Nothing Then
                Dim note_partner_dob_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "partner dob:")
                    If start_idx > 0 Then
                        start_idx += 12
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_partner_dob_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_partner_dob_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_partner_dob_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_partner_dob_str)
                    If test_date <> partner_dob Then
                        write_note(debtor & "|" & "Partner DOB:" & Format(partner_dob, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Partner DOB:" & Format(partner_dob, "dd/MM/yyyy"))
                    change_found = True
                End Try

            End If

            'see if partner nino has changed
            If partner_nino <> "" Then
                Dim note_partner_nino As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "partner nino:")
                    If start_idx > 0 Then
                        start_idx += 13
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_partner_nino = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_partner_nino = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_partner_nino <> "" Then
                        Exit For
                    End If
                Next
                If LCase(partner_nino) <> note_partner_nino Then
                    write_note(debtor & "|" & "Partner NINO:" & partner_nino)
                    change_found = True
                End If
            End If

            'see if partner emp status has changed
            If partner_emp_status <> "" Then
                Dim note_partner_emp_status As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "emp status partner:")
                    If start_idx > 0 Then
                        start_idx += 19
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_partner_emp_status = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_partner_emp_status = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_partner_emp_status <> "" Then
                        Exit For
                    End If
                Next
                If LCase(partner_emp_status) <> note_partner_emp_status Then
                    write_note(debtor & "|" & "Emp Status Partner:" & partner_emp_status)
                    change_found = True
                End If
            End If

            'see if partner emp code has changed
            If partner_emp_code <> "" Then
                Dim note_partner_emp_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "emp code partner:")
                    If start_idx > 0 Then
                        start_idx += 17
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_partner_emp_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_partner_emp_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_partner_emp_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(partner_emp_code) <> note_partner_emp_code Then
                    write_note(debtor & "|" & "Emp Code Partner:" & partner_emp_code)
                    change_found = True
                End If
            End If

            'see if residential code has changed
            'If eq_residential_code <> "" Then
            '    Dim note_residential_code As String = ""
            '    For idx3 = 0 To no_of_notes - 1
            '        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            '        Dim start_idx As Integer = InStr(note_text, "residential code:")
            '        If start_idx > 0 Then
            '            start_idx += 17
            '        End If
            '        If start_idx > 0 Then
            '            For idx4 = start_idx To note_text.Length
            '                If Mid(note_text, idx4, 1) = ";" Then
            '                    note_residential_code = Mid(note_text, start_idx, idx4 - start_idx)
            '                    Exit For
            '                End If
            '            Next
            '            If idx4 > note_text.Length Then
            '                note_residential_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '            End If

            '        End If
            '        If note_residential_code <> "" Then
            '            Exit For
            '        End If
            '    Next
            '    If LCase(eq_residential_code) <> note_residential_code Then
            '        write_note(debtor & "|" & "Residential Code:" & eq_residential_code)
            '        change_found = True
            '    End If
            'End If

            ''see if residential description has changed
            'If eq_residential_desc <> "" Then
            '    Dim note_residential_desc As String = ""
            '    For idx3 = 0 To no_of_notes - 1
            '        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            '        Dim start_idx As Integer = InStr(note_text, "residential description:")
            '        If start_idx > 0 Then
            '            start_idx += 24
            '        End If
            '        If start_idx > 0 Then
            '            For idx4 = start_idx To note_text.Length
            '                If Mid(note_text, idx4, 1) = ";" Then
            '                    note_residential_desc = Mid(note_text, start_idx, idx4 - start_idx)
            '                    Exit For
            '                End If
            '            Next
            '            If idx4 > note_text.Length Then
            '                note_residential_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '            End If

            '        End If
            '        If note_residential_desc <> "" Then
            '            Exit For
            '        End If
            '    Next
            '    If LCase(eq_residential_desc) <> note_residential_desc Then
            '        write_note(debtor & "|" & "Residential Description:" & eq_residential_desc)
            '        change_found = True
            '    End If
            'End If

            ''see if property_type_code has changed
            'If eq_prop_type_code <> "" Then
            '    Dim note_prop_type_code As String = ""
            '    For idx3 = 0 To no_of_notes - 1
            '        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            '        Dim start_idx As Integer = InStr(note_text, "property type code:")
            '        If start_idx > 0 Then
            '            start_idx += 19
            '        End If
            '        If start_idx > 0 Then
            '            For idx4 = start_idx To note_text.Length
            '                If Mid(note_text, idx4, 1) = ";" Then
            '                    note_prop_type_code = Mid(note_text, start_idx, idx4 - start_idx)
            '                    Exit For
            '                End If
            '            Next
            '            If idx4 > note_text.Length Then
            '                note_prop_type_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '            End If

            '        End If
            '        If note_prop_type_code <> "" Then
            '            Exit For
            '        End If
            '    Next
            '    If LCase(eq_prop_type_code) <> note_prop_type_code Then
            '        write_note(debtor & "|" & "Property Type Code:" & eq_prop_type_code)
            '        change_found = True
            '    End If
            'End If

            ''see if property_type_description has changed
            'If eq_prop_type_desc <> "" Then
            '    Dim note_prop_type_desc As String = ""
            '    For idx3 = 0 To no_of_notes - 1
            '        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            '        Dim start_idx As Integer = InStr(note_text, "property type description:")
            '        If start_idx > 0 Then
            '            start_idx += 26
            '        End If
            '        If start_idx > 0 Then
            '            For idx4 = start_idx To note_text.Length
            '                If Mid(note_text, idx4, 1) = ";" Then
            '                    note_prop_type_desc = Mid(note_text, start_idx, idx4 - start_idx)
            '                    Exit For
            '                End If
            '            Next
            '            If idx4 > note_text.Length Then
            '                note_prop_type_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
            '            End If

            '        End If
            '        If note_prop_type_desc <> "" Then
            '            Exit For
            '        End If
            '    Next
            '    If LCase(eq_prop_type_desc) <> note_prop_type_desc Then
            '        write_note(debtor & "|" & "Property Type Description:" & eq_prop_type_desc)
            '        change_found = True
            '    End If
            'End If

            'see if all evidence date has changed
            If all_evidence_date <> Nothing Then
                Dim note_all_evidence_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "all evidence date:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_all_evidence_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_all_evidence_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_all_evidence_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_all_evidence_date_str)
                    If all_evidence_date <> note_all_evidence_date_str Then
                        write_note(debtor & "|" & "All Evidence Date:" & Format(all_evidence_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "All Evidence Date:" & Format(all_evidence_date, "dd/MM/yyyy"))
                    change_found = True
                End Try

            End If

            'see if preferred payment day has changed
            If pref_pay_day <> "" Then
                Dim note_pref_pay_day As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "preferred payment day:")
                    If start_idx > 0 Then
                        start_idx += 22
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_pref_pay_day = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_pref_pay_day = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_pref_pay_day <> "" Then
                        Exit For
                    End If
                Next
                If LCase(pref_pay_day) <> note_pref_pay_day Then
                    write_note(debtor & "|" & "Preferred Payment Day:" & pref_pay_day)
                    change_found = True
                End If
            End If

            'see if preferred payment method has changed
            If pref_pay_method <> "" Then
                Dim note_pref_pay_method As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "preferred payment method:")
                    If start_idx > 0 Then
                        start_idx += 25
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_pref_pay_method = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_pref_pay_method = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_pref_pay_method <> "" Then
                        Exit For
                    End If
                Next
                If LCase(pref_pay_method) <> note_pref_pay_method Then
                    write_note(debtor & "|" & "Preferred Payment Method:" & pref_pay_method)
                    change_found = True
                End If
            End If

            'see if preferred payment code has changed
            If pref_pay_code <> "" Then
                Dim note_pref_pay_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "preferred payment code:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_pref_pay_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_pref_pay_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_pref_pay_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(pref_pay_code) <> note_pref_pay_code Then
                    write_note(debtor & "|" & "Preferred Payment Code:" & pref_pay_code)
                    change_found = True
                End If
            End If

            'see if income evidence has changed
            '25.2.2013 JB need to check individual fields within income evidence 
            Dim inc_evidence_change_found As Boolean
            Dim inc_idx As Integer
            Dim note_inc_evidence As String = ""
            For inc_idx = 1 To inc_ev_no
                inc_evidence_change_found = True
                Dim note_date As Date
                If inc_idx = 1 Then
                    note_date = Nothing
                End If
                Dim start_idx As Integer
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    start_idx = InStr(note_text, "inc evidence:")
                    If start_idx > 0 Then
                        start_idx += 13
                    End If
                    If start_idx > 0 Then
                        If note_date = Nothing Then
                            note_date = note_dataset.Tables(0).Rows(idx3).Item(1)
                        Else
                            If Format(note_date, "yyyy-MM-dd") <> Format(note_dataset.Tables(0).Rows(idx3).Item(1), "yyyy-MM-dd") Then
                                Exit For
                            End If
                        End If
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_inc_evidence = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_inc_evidence = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_inc_evidence = "" Then
                        Continue For
                    End If
                    If LCase(inc_evidence_table(inc_idx, 1)) <> note_inc_evidence Then
                        Continue For
                    End If
                    'see if other fields are the same
                    If inc_evidence_table(inc_idx, 2) <> Nothing Then
                        Dim search_text As String = "inc evidence mandatory:" & inc_evidence_table(inc_idx, 2) & ";"
                        If InStr(note_text, LCase(search_text)) = 0 Then
                            Continue For
                        End If
                    End If
                    If inc_evidence_table(inc_idx, 3) <> Nothing Then
                        Dim search_text As String = "inc evidence other text:" & inc_evidence_table(inc_idx, 3) & ";"
                        If InStr(note_text, LCase(search_text)) = 0 Then
                            Continue For
                        End If
                    End If
                    If inc_evidence_table(inc_idx, 4) <> Nothing Then
                        Dim search_text As String = "inc evidence date received:" & inc_evidence_table(inc_idx, 4) & ";"
                        If InStr(note_text, search_text) = 0 Then
                            Continue For
                        End If
                    End If
                    inc_evidence_change_found = False
                    Exit For
                Next
                'end of notes
                If inc_evidence_change_found Then
                    Exit For
                End If
            Next
            If inc_evidence_change_found Then
                For inc_idx = 1 To inc_ev_no
                    Dim inc_text As String = "Inc Evidence:" & inc_evidence_table(inc_idx, 1) & ";"
                    If inc_evidence_table(inc_idx, 2) <> Nothing Then
                        inc_text = inc_text & "Inc Evidence Mandatory:" & inc_evidence_table(inc_idx, 2) & ";"
                    End If
                    If inc_evidence_table(inc_idx, 3) <> Nothing Then
                        inc_text = inc_text & "Inc Evidence Other Text:" & inc_evidence_table(inc_idx, 3) & ";"
                    End If
                    If inc_evidence_table(inc_idx, 4) <> Nothing Then
                        inc_text = inc_text & "Inc Evidence Date Received:" & inc_evidence_table(inc_idx, 4) & ";"
                    End If
                    'remove last ; before writing note
                    inc_text = Microsoft.VisualBasic.Left(inc_text, inc_text.Length - 1)
                    Dim inc_text2 As String = ""
                    If inc_text.Length > 225 Then
                        'split into 2 notes
                        'get last ; at length < 225
                        Dim split_idx As Integer
                        For split_idx = 225 To 1 Step -1
                            If Mid(inc_text(split_idx), 1) = ";" Then
                                Exit For
                            End If
                        Next
                        inc_text2 = inc_text
                        inc_text = Microsoft.VisualBasic.Left(inc_text, split_idx)
                        inc_text2 = Microsoft.VisualBasic.Right(inc_text2, inc_text2.Length - split_idx - 1)
                    End If
                    write_note(debtor & "|" & inc_text)
                    If inc_text2.Length > 0 Then
                        write_note(debtor & "|" & inc_text2)
                    End If
                Next
                change_found = True
            End If

            'If inc_ev_no > 0 Then
            '    Dim inc_idx As Integer
            '    For inc_idx = 1 To inc_ev_no
            '        'for each field in the structure, get the latest entry in notes
            '        Dim tag, last_note As String
            '        If inc_evidence_table(inc_idx, 1) <> "" Then
            '            tag = "Inc Evidence:"
            '            last_note = get_latest_note(tag, note_dataset, no_of_notes)
            '            If LCase(inc_evidence_table(inc_idx, 1)) <> last_note Then
            '                write_inc_evidence_changes(debtor)
            '                Continue For
            '            End If
            '        End If
            '    Next


            '    Dim note_evidence(30) As String
            '    Dim note_inc_ev_no As Integer = 0

            '    For idx3 = 0 To no_of_notes - 1
            '        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            '        Dim start_idx As Integer = InStr(note_text, "inc evidence:")
            '        If start_idx > 0 Then
            '            start_idx += 13
            '        End If
            '        If start_idx > 0 Then
            '            note_inc_ev_no += 1
            '            For idx4 = start_idx To note_text.Length
            '                If Mid(note_text, idx4, 1) = ";" Then
            '                    note_evidence(note_inc_ev_no) = Mid(note_text, start_idx, idx4 - start_idx)
            '                    Exit For
            '                End If
            '            Next
            '            note_text = Microsoft.VisualBasic.Right(note_text, note_text.Length - start_idx)
            '            start_idx = InStr(note_text, "inc evidence:")
            '            While start_idx <> 0
            '                start_idx += 13
            '                note_inc_ev_no += 1
            '                For idx4 = start_idx To note_text.Length
            '                    If Mid(note_text, idx4, 1) = ";" Then
            '                        note_evidence(note_inc_ev_no) = Mid(note_text, start_idx, idx4 - start_idx)
            '                        Exit For
            '                    End If
            '                Next
            '                note_text = Microsoft.VisualBasic.Right(note_text, note_text.Length - start_idx)
            '                start_idx = InStr(note_text, "inc evidence:")
            '            End While
            '        End If
            '    Next
            '    For idx3 = 1 To inc_ev_no
            '        Dim inc_ev_found As Boolean = False
            '        For idx4 = 1 To note_inc_ev_no
            '            If LCase(inc_evidence_table(idx3, 1)) = LCase(note_evidence(idx4)) Then
            '                inc_ev_found = True
            '                Exit For
            '            End If
            '        Next
            '        If inc_ev_found = False Then
            '            Dim note_txt As String = debtor & "|" & "Inc Evidence:" & inc_evidence_table(idx3, 1)
            '            If inc_evidence_table(idx3, 2) <> "" Then
            '                note_txt = note_txt & ";" & "Inc Evidence Mandatory:" & inc_evidence_table(idx3, 2)
            '            End If
            '            If inc_evidence_table(idx3, 3) <> "" Then
            '                note_txt = note_txt & ";" & "Inc Evidence Other Text:" & inc_evidence_table(idx3, 3)
            '            End If
            '            If inc_evidence_table(idx3, 4) <> "" Then
            '                test_date = inc_evidence_table(idx3, 4)
            '                note_txt = note_txt & ";" & "Inc Evidence Date Received:" & Format(test_date, "dd/MM/yyyy")
            '            End If
            '            write_note(note_txt)
            '            change_found = True
            '        End If
            '    Next
            'End If

            'see if assets have changed
            Dim asset_change_found As Boolean
            If asset_no > 0 Then
                Dim note_asset_type As String = ""
                Dim asset_idx As Integer
                For asset_idx = 1 To asset_no
                    asset_change_found = True
                    Dim note_date As Date
                    If asset_idx = 1 Then
                        note_date = Nothing
                    End If
                    Dim start_idx As Integer
                    For idx3 = 0 To no_of_notes - 1
                        note_asset_type = ""
                        note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                        start_idx = InStr(note_text, "cap asset type:")
                        If start_idx > 0 Then
                            start_idx += 15
                        End If
                        If start_idx > 0 Then
                            If note_date = Nothing Then
                                note_date = note_dataset.Tables(0).Rows(idx3).Item(1)
                            Else
                                If Format(note_date, "yyyy-MM-dd") <> Format(note_dataset.Tables(0).Rows(idx3).Item(1), "yyyy-MM-dd") Then
                                    Exit For
                                End If
                            End If
                            For idx4 = start_idx To note_text.Length
                                If Mid(note_text, idx4, 1) = ";" Then
                                    note_asset_type = Mid(note_text, start_idx, idx4 - start_idx)
                                    Exit For
                                End If
                            Next
                            If idx4 > note_text.Length Then
                                note_asset_type = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                            End If

                        End If
                        If note_asset_type = "" Then
                            Continue For
                        End If
                        If LCase(asset_table(asset_idx, 1)) <> note_asset_type Then
                            Continue For
                        End If
                        'see if other fields are the same
                        If asset_table(asset_idx, 2) <> Nothing Then
                            Dim search_text As String = "cap amt:" & asset_table(asset_idx, 2) & ";"
                            If InStr(note_text, search_text) = 0 Then
                                Continue For
                            End If
                        End If
                        If asset_table(asset_idx, 3) <> Nothing Then
                            Dim search_text As String = "cap amt verified:" & asset_table(asset_idx, 3) & ";"
                            If InStr(note_text, LCase(search_text)) = 0 Then
                                Continue For
                            End If
                        End If
                        If asset_table(asset_idx, 4) <> Nothing Then
                            Dim search_text As String = "cap amt date verified:" & asset_table(asset_idx, 4) & ";"
                            If InStr(note_text, LCase(search_text)) = 0 Then
                                Continue For
                            End If
                        End If
                        If asset_table(asset_idx, 5) <> Nothing Then
                            Dim search_text As String = "cap amt evidence received date:" & asset_table(asset_idx, 5) & ";"
                            If InStr(note_text, LCase(search_text)) = 0 Then
                                Continue For
                            End If
                        End If
                        If asset_table(asset_idx, 6) <> Nothing Then
                            Dim search_text As String = "cap amt description:" & asset_table(asset_idx, 6) & ";"
                            'If InStr(note_text, LCase(search_text)) = 0 Then
                            'need to get what is on notes now.
                            Dim temp_note_text As String = note_text
                            Dim cap_idx As Integer = InStr(temp_note_text, "cap amt description:")
                            If cap_idx > 0 Then
                                temp_note_text = Microsoft.VisualBasic.Right(temp_note_text, temp_note_text.Length - cap_idx + 1)
                                cap_idx = InStr(temp_note_text, ";")
                                If cap_idx > 0 Then
                                    temp_note_text = Microsoft.VisualBasic.Left(temp_note_text, cap_idx - 1)
                                End If
                            End If
                            search_text = Microsoft.VisualBasic.Left(search_text, temp_note_text.Length)
                            If InStr(LCase(search_text), temp_note_text) = 0 Then
                                Continue For
                            End If
                        End If
                        If asset_table(asset_idx, 7) <> Nothing Then
                            Dim search_text As String = "cap asset code:" & asset_table(asset_idx, 7) & ";"
                            If InStr(note_text, LCase(search_text)) = 0 Then
                                Continue For
                            End If
                        End If
                        asset_change_found = False
                        Exit For
                    Next
                    'end of notes
                    If asset_change_found Then
                        Exit For
                    End If
                Next
                If asset_change_found Then
                    '26.2.2013 JB need to report all cap assets if a change is found
                    For idx3 = 1 To asset_no
                        Dim asset_note_txt As String = debtor & "|" & "Cap asset type:" & asset_table(idx3, 1) & ";"
                        If asset_table(idx3, 2) <> "" Then
                            asset_note_txt = asset_note_txt & "Cap amt:" & asset_table(idx3, 2) & ";"
                        End If
                        If asset_table(idx3, 3) <> "" Then
                            asset_note_txt = asset_note_txt & "Cap amt verified:" & asset_table(idx3, 3) & ";"
                        End If
                        If asset_table(idx3, 4) <> "" Then
                            test_date = asset_table(idx3, 4)
                            asset_note_txt = asset_note_txt & "Cap amt Date Verified:" & Format(test_date, "dd/MM/yyyy") & ";"
                        End If
                        If asset_table(idx3, 5) <> "" Then
                            test_date = asset_table(idx3, 5)
                            asset_note_txt = asset_note_txt & "Cap amt Evidence Received Date:" & Format(test_date, "dd/MM/yyyy") & ";"
                        End If
                        If asset_table(idx3, 7) <> "" Then
                            asset_note_txt = asset_note_txt & "Cap asset code:" & asset_table(idx3, 7) & ";"
                        End If
                        If asset_table(idx3, 6) <> "" Then
                            Dim temp_note_txt As String = asset_note_txt & "Cap amt description:" & asset_table(idx3, 6) & ";"
                            If temp_note_txt.Length >= 220 Then
                                'truncate description
                                asset_note_txt = Microsoft.VisualBasic.Left(temp_note_txt, 219)
                            Else
                                asset_note_txt = temp_note_txt
                            End If
                        End If
                        If asset_note_txt.Length < 220 Then
                            write_note(asset_note_txt)
                        Else
                            Dim no_of_220s As Integer = Int(asset_note_txt.Length / 220) + 1
                            Dim new_note_txt As String = ""
                            Dim init_length As Integer = asset_note_txt.Length
                            Dim note_idx, note_idx2 As Integer
                            For note_idx = 1 To no_of_220s
                                For note_idx2 = 220 To 1 Step -1
                                    If Mid(asset_note_txt, note_idx2, 1) = ";" Then
                                        Exit For
                                    End If
                                Next
                                new_note_txt = Microsoft.VisualBasic.Left(asset_note_txt, note_idx2)
                                write_note(new_note_txt)
                                asset_note_txt = debtor & "|" & Microsoft.VisualBasic.Right(asset_note_txt, asset_note_txt.Length - note_idx2)
                                If asset_note_txt.Length <= 220 Then
                                    write_note(asset_note_txt)
                                    Exit For
                                End If
                            Next
                        End If

                    Next
                    change_found = True
                End If
            End If

            'see if uplift applied date has changed
            If uplift_applied_date <> Nothing Then
                Dim note_uplift_applied_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "uplift applied date:")
                    If start_idx > 0 Then
                        start_idx += 20
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_uplift_applied_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_uplift_applied_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_uplift_applied_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_uplift_applied_date_str)
                    If test_date <> uplift_applied_date Then
                        write_note(debtor & "|" & "uplift applied date:" & Format(uplift_applied_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "uplift applied date:" & Format(uplift_applied_date, "dd/MM/yyyy"))
                    change_found = True
                End Try
            End If

            'see if uplift removed date has changed
            If uplift_removed_date <> Nothing Then
                Dim note_uplift_removed_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "uplift removed date:")
                    If start_idx > 0 Then
                        start_idx += 20
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_uplift_removed_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_uplift_removed_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_uplift_removed_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_uplift_removed_date_str)
                    If test_date <> uplift_removed_date Then
                        write_note(debtor & "|" & "uplift removed date:" & Format(uplift_removed_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "uplift removed date:" & Format(uplift_removed_date, "dd/MM/yyyy"))
                    change_found = True
                End Try
            End If

            'new changes from here 21.8.2012

            'see if sufficient declared equity
            If sufficient_declared_equity <> "" Then
                Dim note_sufficient_declared_equity As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "sufficient declared equity:")
                    If start_idx > 0 Then
                        start_idx += 27
                    End If
                    If start_idx > 0 Then
                        note_sufficient_declared_equity = Mid(note_text, start_idx, 1)
                        If note_sufficient_declared_equity = "y" Then
                            note_sufficient_declared_equity = "yes"
                        Else
                            note_sufficient_declared_equity = "no"
                        End If
                    End If
                    If note_sufficient_declared_equity <> "" Then
                        Exit For
                    End If
                Next
                If LCase(sufficient_declared_equity) <> note_sufficient_declared_equity Then
                    write_note(debtor & "|" & "Sufficient Declared Equity:" & sufficient_declared_equity)
                    change_found = True
                End If
            End If

            'see if sufficient verified equity
            If sufficient_verified_equity <> "" Then
                Dim note_sufficient_verified_equity As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "sufficient verified equity:")
                    If start_idx > 0 Then
                        start_idx += 27
                    End If
                    If start_idx > 0 Then
                        note_sufficient_verified_equity = Mid(note_text, start_idx, 1)
                        If note_sufficient_verified_equity = "y" Then
                            note_sufficient_verified_equity = "yes"
                        Else
                            note_sufficient_verified_equity = "no"
                        End If
                    End If
                    If note_sufficient_verified_equity <> "" Then
                        Exit For
                    End If
                Next
                If LCase(sufficient_verified_equity) <> note_sufficient_verified_equity Then
                    write_note(debtor & "|" & "Sufficient Verified Equity:" & sufficient_verified_equity)
                    change_found = True
                End If
            End If

            'see if sufficient capital and equity has changed
            If sufficient_capital_and_equity <> "" Then
                Dim note_sufficient_capital_and_equity As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "sufficient capital and equity:")
                    If start_idx > 0 Then
                        start_idx += 30
                    End If
                    If start_idx > 0 Then
                        note_sufficient_capital_and_equity = Mid(note_text, start_idx, 1)
                        If note_sufficient_capital_and_equity = "y" Then
                            note_sufficient_capital_and_equity = "yes"
                        Else
                            note_sufficient_capital_and_equity = "no"
                        End If
                    End If
                    If note_sufficient_capital_and_equity <> "" Then
                        Exit For
                    End If
                Next
                If LCase(sufficient_capital_and_equity) <> note_sufficient_capital_and_equity Then
                    write_note(debtor & "|" & "Sufficient Capital and Equity:" & sufficient_capital_and_equity)
                    change_found = True
                End If
            End If

            'see if total capital assets has changed
            Dim note_tot_cap_assets As Decimal = -99999
            For idx3 = 0 To no_of_notes - 1
                note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                Dim start_idx As Integer = InStr(note_text, "total capital assets:")
                If start_idx > 0 Then
                    start_idx += 21
                End If
                If start_idx > 0 Then
                    For idx4 = start_idx To note_text.Length
                        If Mid(note_text, idx4, 1) = ";" Then
                            note_tot_cap_assets = Mid(note_text, start_idx, idx4 - start_idx)
                            Exit For
                        End If
                    Next
                    If idx4 > note_text.Length Then
                        note_tot_cap_assets = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                    End If
                End If
                If note_tot_cap_assets <> -99999 Then
                    Exit For
                End If
            Next
            If total_capital_assets <> note_tot_cap_assets And total_capital_assets <> -99999 Then
                If note_tot_cap_assets <> -99999 Then
                    write_note(debtor & "|" & "Total Capital Assets:" & total_capital_assets)
                    change_found = True
                End If
            End If

            'see if offence type desc has changed
            If offence_type_desc <> "" Then
                Dim note_offence_type_desc As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "offence type desc:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_offence_type_desc = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_offence_type_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_offence_type_desc <> "" Then
                        Exit For
                    End If
                Next
                If LCase(offence_type_desc) <> note_offence_type_desc Then
                    write_note(debtor & "|" & "Offence Type Desc:" & offence_type_desc)
                    change_found = True
                End If
            End If

            'see if offence type code has changed
            If offence_type_code <> "" Then
                Dim note_offence_type_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "offence type code:")
                    If start_idx > 0 Then
                        start_idx += 18
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_offence_type_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_offence_type_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_offence_type_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(offence_type_code) <> note_offence_type_code Then
                    write_note(debtor & "|" & "Offence Type Code:" & offence_type_code)
                    change_found = True
                End If
            End If

            'see if mags court code has changed
            If mags_court_code <> "" Then
                Dim note_mags_court_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "mags court code:")
                    If start_idx > 0 Then
                        start_idx += 16
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_mags_court_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_mags_court_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_mags_court_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(mags_court_code) <> note_mags_court_code Then
                    write_note(debtor & "|" & "Mags Court Code:" & mags_court_code)
                    change_found = True
                End If
            End If

            'see if mags court desc has changed
            If mags_court_desc <> "" Then
                Dim note_mags_court_desc As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "mags court desc:")
                    If start_idx > 0 Then
                        start_idx += 16
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_mags_court_desc = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_mags_court_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_mags_court_desc <> "" Then
                        Exit For
                    End If
                Next
                If LCase(mags_court_desc) <> note_mags_court_desc Then
                    write_note(debtor & "|" & "Mags Court Desc:" & mags_court_desc)
                    change_found = True
                End If
            End If

            'see if equity verified date has changed
            If equity_verified_date <> Nothing Then
                Dim note_equity_verified_date_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "equity verified date:")
                    If start_idx > 0 Then
                        start_idx += 21
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_equity_verified_date_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_equity_verified_date_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_equity_verified_date_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_equity_verified_date_str)
                    If equity_verified_date <> test_date Then
                        write_note(debtor & "|" & "Equity Verified Date:" & Format(equity_verified_date, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Equity Verified Date:" & Format(equity_verified_date, "dd/MM/yyyy"))
                End Try
            End If

            'see if equity verified by has changed
            If equity_verified_by <> "" Then
                Dim note_equity_verified_by As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "equity verified by:")
                    If start_idx > 0 Then
                        start_idx += 19
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_equity_verified_by = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_equity_verified_by = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_equity_verified_by <> "" Then
                        Exit For
                    End If
                Next
                If LCase(equity_verified_by) <> note_equity_verified_by Then
                    write_note(debtor & "|" & "Equity Verified By:" & equity_verified_by)
                    change_found = True
                End If
            End If

            'see if any of the equity properties has changed - if so write them all to changes
            Dim prop_idx As Integer

            For prop_idx = 1 To equity_prop_no
                'for each field in the structure, get the lastest entry in notes
                Dim tag, last_note As String
                If equity_props(prop_idx).addr_city <> "" Then
                    tag = "prop " & prop_idx & " address city equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).addr_city) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).addr_country <> "" Then
                    tag = "prop " & prop_idx & " address country equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).addr_country) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).addr_line1 <> "" Then
                    tag = "prop " & prop_idx & " address line1 equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).addr_line1) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).addr_line2 <> "" Then
                    tag = "prop " & prop_idx & " address line2 equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).addr_line2) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).addr_line3 <> "" Then
                    tag = "prop " & prop_idx & " address line3 equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).addr_line3) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).addr_pcode <> "" Then
                    tag = "prop " & prop_idx & " address postcode equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).addr_pcode) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).appl_equity_amt <> -999 Then
                    tag = "prop " & prop_idx & " applicant equity amount equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).appl_equity_amt) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).bedroom_count <> "" Then
                    tag = "prop " & prop_idx & " bedroom count equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).bedroom_count) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).declared_date <> Nothing Then
                    tag = "prop " & prop_idx & " declared date equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).declared_date) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).declared_equity <> -999 Then
                    tag = "prop " & prop_idx & " declared equity equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).declared_equity) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).declared_mortgage <> -999 Then
                    tag = "prop " & prop_idx & " declared mortgage equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).declared_mortgage) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).declared_value <> -999 Then
                    tag = "prop " & prop_idx & " declared value equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).declared_value) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).partner_equity_amt <> -999 Then
                    tag = "prop " & prop_idx & " partner equity amount equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).partner_equity_amt) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).percent_appl <> Nothing Then
                    tag = "prop " & prop_idx & " percent owned applicant equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).percent_appl) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).percent_partner <> Nothing Then
                    tag = "prop " & prop_idx & " percent owned partner equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).percent_partner) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).prop_type_code <> "" Then
                    tag = "prop " & prop_idx & " property type code equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).prop_type_code) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).prop_type_desc <> "" Then
                    tag = "prop " & prop_idx & " property type description equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).prop_type_desc) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If

                If equity_props(prop_idx).residential_code <> "" Then
                    tag = "prop " & prop_idx & " residential code equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).residential_code) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).residential_desc <> "" Then
                    tag = "prop " & prop_idx & " residential description equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).residential_desc) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).tenant_in_place <> "" Then
                    tag = "prop " & prop_idx & " tenant in place equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).tenant_in_place) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).verified_by <> "" Then
                    tag = "prop " & prop_idx & " verified by equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).verified_by) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).verified_date <> Nothing Then
                    tag = "prop " & prop_idx & " verified date equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).verified_date) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).verified_mortgage <> -999 Then
                    tag = "prop " & prop_idx & " verified mortgage equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).verified_mortgage) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).verified_value <> -999 Then
                    tag = "prop " & prop_idx & " verified value equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).verified_value) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If equity_props(prop_idx).verified_by <> "" Then
                    tag = "prop " & prop_idx & " verified by equity:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(equity_props(prop_idx).verified_by) <> last_note Then
                        write_equity_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
            Next


            'see if any of the capital properties has changed - if so write them all to changes
            Dim cap_prop_change As Boolean = False
            For prop_idx = 1 To cap_prop_no
                'for each field in the structure, get the lastest entry in notes
                Dim tag, last_note As String
                If cap_props(prop_idx).addr_city <> "" Then
                    tag = "prop " & prop_idx & " address city capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).addr_city) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).addr_country <> "" Then
                    tag = "prop " & prop_idx & " address country capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).addr_country) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).addr_line1 <> "" Then
                    tag = "prop " & prop_idx & " address line1 capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).addr_line1) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).addr_line2 <> "" Then
                    tag = "prop " & prop_idx & " address line2 capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).addr_line2) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).addr_line3 <> "" Then
                    tag = "prop " & prop_idx & " address line3 capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).addr_line3) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).addr_pcode <> "" Then
                    tag = "prop " & prop_idx & " address postcode capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).addr_pcode) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).appl_equity_amt <> -999 Then
                    tag = "prop " & prop_idx & " applicant equity amount capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).appl_equity_amt) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).bedroom_count <> "" Then
                    tag = "prop " & prop_idx & " bedroom count capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).bedroom_count) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).declared_date <> Nothing Then
                    tag = "prop " & prop_idx & " declared date capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).declared_date) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).declared_equity <> -999 Then
                    tag = "prop " & prop_idx & " declared equity capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).declared_equity) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).declared_mortgage <> -999 Then
                    tag = "prop " & prop_idx & " declared mortgage capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).declared_mortgage) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).declared_value <> -999 Then
                    tag = "prop " & prop_idx & " declared value capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).declared_value) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).partner_equity_amt <> -999 Then
                    tag = "prop " & prop_idx & " partner equity amount capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).partner_equity_amt) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).percent_appl <> Nothing Then
                    tag = "prop " & prop_idx & " percent owned applicant capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).percent_appl) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).percent_partner <> Nothing Then
                    tag = "prop " & prop_idx & " percent owned partner capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).percent_partner) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).prop_type_code <> "" Then
                    tag = "prop " & prop_idx & " property type code capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).prop_type_code) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).prop_type_desc <> "" Then
                    tag = "prop " & prop_idx & " property type description capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).prop_type_desc) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If

                If cap_props(prop_idx).residential_code <> "" Then
                    tag = "prop " & prop_idx & " residential code capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).residential_code) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).residential_desc <> "" Then
                    tag = "prop " & prop_idx & " residential description capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).residential_desc) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).tenant_in_place <> "" Then
                    tag = "prop " & prop_idx & " tenant in place capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).tenant_in_place) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).verified_by <> "" Then
                    tag = "prop " & prop_idx & " verified by capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).verified_by) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).verified_date <> Nothing Then
                    tag = "prop " & prop_idx & " verified date capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).verified_date) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).verified_mortgage <> -999 Then
                    tag = "prop " & prop_idx & " verified mortgage capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).verified_mortgage) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).verified_value <> -999 Then
                    tag = "prop " & prop_idx & " verified value capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).verified_value) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
                If cap_props(prop_idx).verified_by <> "" Then
                    tag = "prop " & prop_idx & " verified by capital:"
                    last_note = get_latest_note(tag, note_dataset, no_of_notes)
                    If LCase(cap_props(prop_idx).verified_by) <> last_note Then
                        write_cap_changes(debtor, prop_idx)
                        Continue For
                    End If
                End If
            Next

            'see if any third party details have changed on equity
            Dim tp_idx As Integer
            For prop_idx = 1 To equity_prop_no
                For tp_idx = 1 To equity_tp_no
                    'for each field in the structure, get the latest entry in notes
                    Dim tag, last_note As String
                    If equity_tps(prop_idx, tp_idx).name <> "" Then
                        tag = "prop " & prop_idx & " tp " & tp_idx & " name equity:"
                        last_note = get_latest_note(tag, note_dataset, no_of_notes)
                        If LCase(equity_tps(prop_idx, tp_idx).name) <> last_note Then
                            write_tp_equity_changes(debtor, prop_idx, tp_idx)
                            Continue For
                        End If
                    End If
                    If equity_tps(prop_idx, tp_idx).other_desc <> "" Then
                        tag = "prop " & prop_idx & " tp " & tp_idx & " other desc equity:"
                        last_note = get_latest_note(tag, note_dataset, no_of_notes)
                        If LCase(equity_tps(prop_idx, tp_idx).other_desc) <> last_note Then
                            write_tp_equity_changes(debtor, prop_idx, tp_idx)
                            Continue For
                        End If
                    End If
                    If equity_tps(prop_idx, tp_idx).rel_code <> "" Then
                        tag = "prop " & prop_idx & " tp " & tp_idx & " relationship code equity:"
                        last_note = get_latest_note(tag, note_dataset, no_of_notes)
                        If LCase(equity_tps(prop_idx, tp_idx).rel_code) <> last_note Then
                            write_tp_equity_changes(debtor, prop_idx, tp_idx)
                            Continue For
                        End If
                    End If
                    If equity_tps(prop_idx, tp_idx).rel_desc <> "" Then
                        tag = "prop " & prop_idx & " tp " & tp_idx & " relationship desc equity:"
                        last_note = get_latest_note(tag, note_dataset, no_of_notes)
                        If LCase(equity_tps(prop_idx, tp_idx).rel_desc) <> last_note Then
                            write_tp_equity_changes(debtor, prop_idx, tp_idx)
                            Continue For
                        End If
                    End If
                    If equity_tps(prop_idx, tp_idx).created_date <> Nothing Then
                        tag = "prop " & prop_idx & " tp " & tp_idx & " created date equity:"
                        last_note = get_latest_note(tag, note_dataset, no_of_notes)
                        If LCase(equity_tps(prop_idx, tp_idx).created_date) <> last_note Then
                            write_tp_equity_changes(debtor, prop_idx, tp_idx)
                            Continue For
                        End If
                    End If
                    If equity_tps(prop_idx, tp_idx).deleted_date <> Nothing Then
                        tag = "prop " & prop_idx & " tp " & tp_idx & " deleted date equity:"
                        last_note = get_latest_note(tag, note_dataset, no_of_notes)
                        If LCase(equity_tps(prop_idx, tp_idx).deleted_date) <> last_note Then
                            write_tp_equity_changes(debtor, prop_idx, tp_idx)
                            Continue For
                        End If
                    End If
                Next
            Next

            'see if any third party details have changed on capital
            For prop_idx = 1 To cap_prop_no
                For tp_idx = 1 To cap_tp_no
                    'for each field in the structure, get the lastest entry in notes
                    Dim tag, last_note As String
                    If cap_tps(prop_idx, tp_idx).name <> "" Then
                        tag = "prop " & prop_idx & " tp " & tp_idx & " name capital:"
                        last_note = get_latest_note(tag, note_dataset, no_of_notes)
                        If LCase(cap_tps(prop_idx, tp_idx).name) <> last_note Then
                            write_tp_capital_changes(debtor, prop_idx, tp_idx)
                            Continue For
                        End If
                    End If
                    If cap_tps(prop_idx, tp_idx).other_desc <> "" Then
                        tag = "prop " & prop_idx & " tp " & tp_idx & " other desc capital:"
                        last_note = get_latest_note(tag, note_dataset, no_of_notes)
                        If LCase(cap_tps(prop_idx, tp_idx).other_desc) <> last_note Then
                            write_tp_capital_changes(debtor, prop_idx, tp_idx)
                            Continue For
                        End If
                    End If
                    If cap_tps(prop_idx, tp_idx).rel_code <> "" Then
                        tag = "prop " & prop_idx & " tp " & tp_idx & " relationship code capital:"
                        last_note = get_latest_note(tag, note_dataset, no_of_notes)
                        If LCase(cap_tps(prop_idx, tp_idx).rel_code) <> last_note Then
                            write_tp_capital_changes(debtor, prop_idx, tp_idx)
                            Continue For
                        End If
                    End If
                    If cap_tps(prop_idx, tp_idx).rel_desc <> "" Then
                        tag = "prop " & prop_idx & " tp " & tp_idx & " relationship desc capital:"
                        last_note = get_latest_note(tag, note_dataset, no_of_notes)
                        If LCase(cap_tps(prop_idx, tp_idx).rel_desc) <> last_note Then
                            write_tp_capital_changes(debtor, prop_idx, tp_idx)
                            Continue For
                        End If
                    End If
                    If cap_tps(prop_idx, tp_idx).created_date <> Nothing Then
                        tag = "prop " & prop_idx & " tp " & tp_idx & " created date capital:"
                        last_note = get_latest_note(tag, note_dataset, no_of_notes)
                        If LCase(cap_tps(prop_idx, tp_idx).created_date) <> last_note Then
                            write_tp_capital_changes(debtor, prop_idx, tp_idx)
                            Continue For
                        End If
                    End If
                    If cap_tps(prop_idx, tp_idx).deleted_date <> Nothing Then
                        tag = "prop " & prop_idx & " tp " & tp_idx & " deleted date capital:"
                        last_note = get_latest_note(tag, note_dataset, no_of_notes)
                        If LCase(cap_tps(prop_idx, tp_idx).deleted_date) <> last_note Then
                            write_tp_capital_changes(debtor, prop_idx, tp_idx)
                            Continue For
                        End If
                    End If
                Next
            Next
            'see if solicitor account code has changed
            If sol_acc_code <> "" Then
                Dim note_sol_acc_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "solicitor account code:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_sol_acc_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_sol_acc_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_sol_acc_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(sol_acc_code) <> note_sol_acc_code Then
                    write_note(debtor & "|" & "Solicitor account code:" & sol_acc_code)
                    change_found = True
                End If
            End If

            'see if solicitor name has changed
            If sol_name <> "" Then
                Dim note_sol_name As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "solicitor name:")
                    If start_idx > 0 Then
                        start_idx += 15
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_sol_name = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_sol_name = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_sol_name <> "" Then
                        Exit For
                    End If
                Next
                If LCase(sol_name) <> note_sol_name Then
                    write_note(debtor & "|" & "Solicitor name:" & sol_name)
                    change_found = True
                End If
            End If

            'see if appeal type has changed
            If appeal_type <> "" Then
                Dim note_appeal_type As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "appeal type code:")
                    If start_idx > 0 Then
                        start_idx += 17
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_appeal_type = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_appeal_type = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_appeal_type <> "" Then
                        Exit For
                    End If
                Next
                If LCase(appeal_type) <> note_appeal_type Then
                    write_note(debtor & "|" & "Appeal Type Code:" & appeal_type)
                    change_found = True
                End If
            End If
            'see if appeal type desc has changed
            If appeal_type_desc <> "" Then
                Dim note_appeal_type_desc As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "appeal type decription:")
                    If start_idx > 0 Then
                        start_idx += 23
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_appeal_type_desc = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_appeal_type_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_appeal_type_desc <> "" Then
                        Exit For
                    End If
                Next
                If LCase(appeal_type_desc) <> note_appeal_type_desc Then
                    write_note(debtor & "|" & "Appeal Type Description:" & appeal_type_desc)
                    change_found = True
                End If
            End If

            'See If any letters have changed - only pick letters from first date found
            '19.3.2012 use id to find letter
            'then need to check for changes

            Dim letter_change_found As Boolean = False
            Dim letter_type_found As Boolean
            For letter_idx = 1 To letter_no
                letter_type_found = False
                Dim note_date As Date
                If letter_idx = 1 Then
                    note_date = Nothing
                End If
                Dim start_idx As Integer
                Dim note_letter_id As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    start_idx = InStr(note_text, "letter id:")
                    If start_idx > 0 Then
                        start_idx += 10
                    End If
                    If start_idx > 0 Then
                        If note_date = Nothing Then
                            note_date = note_dataset.Tables(0).Rows(idx3).Item(1)
                        Else
                            If Format(note_date, "yyyy-MM-dd") <> Format(note_dataset.Tables(0).Rows(idx3).Item(1), "yyyy-MM-dd") Then
                                letter_change_found = True
                                Exit For
                            End If
                        End If
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_letter_id = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_letter_id = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_letter_id = "" Then
                        Continue For
                    End If
                    If letter_table(letter_idx).id <> note_letter_id Then
                        Continue For
                    End If
                    'see if other fields are the same
                    If letter_table(letter_idx).type <> Nothing Then
                        Dim search_text As String = "letter type:" & letter_table(letter_idx).type & ";"
                        If InStr(note_text, LCase(search_text)) = 0 Then
                            Exit For
                        End If
                    End If
                    If letter_table(letter_idx).ref <> Nothing Then
                        Dim search_text As String = "letter ref:" & letter_table(letter_idx).ref & ";"
                        If InStr(note_text, LCase(search_text)) = 0 Then
                            Exit For
                        End If
                    End If
                    If letter_table(letter_idx).created <> Nothing Then
                        Dim search_text As String = "letter created:" & letter_table(letter_idx).created & ";"
                        If InStr(note_text, search_text) = 0 Then
                            Exit For
                        End If
                    End If
                    If letter_table(letter_idx).printed <> Nothing Then
                        Dim search_text As String = "letter printed:" & letter_table(letter_idx).printed & ";"
                        If InStr(note_text, search_text) = 0 Then
                            Exit For
                        End If
                    End If
                    letter_type_found = True
                    Exit For
                Next
                'end of notes
                If letter_change_found Then
                    Exit For
                End If
                If letter_type_found = False Then
                    letter_change_found = True
                    Exit For
                End If
            Next
            If letter_change_found Then
                For letter_idx = 1 To letter_no
                    Dim letter_text As String = "Letter Ref:" & letter_table(letter_idx).ref & ";"
                    If letter_table(letter_idx).type <> Nothing Then
                        letter_text = letter_text & "Letter Type:" & letter_table(letter_idx).type & ";"
                    End If
                    If letter_table(letter_idx).created <> Nothing Then
                        letter_text = letter_text & "Letter Created:" & letter_table(letter_idx).created & ";"
                    End If
                    If letter_table(letter_idx).printed <> Nothing Then
                        letter_text = letter_text & "Letter Printed:" & letter_table(letter_idx).printed & ";"
                    End If
                    If letter_table(letter_idx).id <> Nothing Then
                        letter_text = letter_text & "Letter Id:" & letter_table(letter_idx).id & ";"
                    End If
                    'remove last ; before writing note
                    letter_text = Microsoft.VisualBasic.Left(letter_text, letter_text.Length - 1)
                    write_note(debtor & "|" & letter_text)
                Next
                change_found = True
            End If

            'see if passport result code has changed
            If passport_result_code <> "" Then
                Dim note_passport_result_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "passport result code:")
                    If start_idx > 0 Then
                        start_idx += 21
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_passport_result_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_passport_result_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_passport_result_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(passport_result_code) <> note_passport_result_code Then
                    write_note(debtor & "|" & "Passport Result Code:" & passport_result_code)
                    change_found = True
                End If
            End If
            'see if passport result desc has changed
            If passport_result_desc <> "" Then
                Dim note_passport_result_desc As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "passport result desc:")
                    If start_idx > 0 Then
                        start_idx += 21
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_passport_result_desc = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_passport_result_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_passport_result_desc <> "" Then
                        Exit For
                    End If
                Next
                If LCase(passport_result_desc) <> note_passport_result_desc Then
                    write_note(debtor & "|" & "Passport Result Desc:" & passport_result_desc)
                    change_found = True
                End If
            End If

            'see if passport reason code has changed
            If passport_reason_code <> "" Then
                Dim note_passport_reason_code As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "passport reason code:")
                    If start_idx > 0 Then
                        start_idx += 21
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_passport_reason_code = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_passport_reason_code = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_passport_reason_code <> "" Then
                        Exit For
                    End If
                Next
                If LCase(passport_reason_code) <> note_passport_reason_code Then
                    write_note(debtor & "|" & "Passport Reason Code:" & passport_reason_code)
                    change_found = True
                End If
            End If
            'see if passport reason desc has changed
            If passport_reason_desc <> "" Then
                Dim note_passport_reason_desc As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "passport reason desc:")
                    If start_idx > 0 Then
                        start_idx += 21
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_passport_reason_desc = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_passport_reason_desc = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_passport_reason_desc <> "" Then
                        Exit For
                    End If
                Next
                If LCase(passport_reason_desc) <> note_passport_reason_desc Then
                    write_note(debtor & "|" & "Passport Reason Desc:" & passport_reason_desc)
                    change_found = True
                End If
            End If
            'see if passport date completed has changed
            If passport_date_completed <> Nothing Then
                Dim note_passport_date_completed_str As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "passport date completed:")
                    If start_idx > 0 Then
                        start_idx += 24
                    End If
                    If start_idx > 0 Then
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_passport_date_completed_str = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_passport_date_completed_str = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_passport_date_completed_str <> "" Then
                        Exit For
                    End If
                Next
                Try
                    test_date = CDate(note_passport_date_completed_str)
                    If passport_date_completed <> test_date Then
                        write_note(debtor & "|" & "Passport Date Completed:" & Format(passport_date_completed, "dd/MM/yyyy"))
                        change_found = True
                    End If
                Catch ex As Exception
                    write_note(debtor & "|" & "Passport Date Completed:" & Format(passport_date_completed, "dd/MM/yyyy"))
                End Try
            End If

            'see if vehicle ownership has changed

            If motor_vehicle_ownership <> "" Then
                Dim note_vehicle_ownership As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    Dim start_idx As Integer = InStr(note_text, "motor vehicle ownership:")
                    If start_idx > 0 Then
                        start_idx += 24
                    End If
                    If start_idx > 0 Then
                        note_vehicle_ownership = Mid(note_text, start_idx, 1)
                        If note_vehicle_ownership = "y" Then
                            note_vehicle_ownership = "yes"
                        Else
                            note_vehicle_ownership = "no"
                        End If
                    End If
                    If note_vehicle_ownership <> "" Then
                        Exit For
                    End If
                Next
                If LCase(motor_vehicle_ownership) <> note_vehicle_ownership Then
                    write_note(debtor & "|" & "Motor Vehicle Ownership:" & motor_vehicle_ownership)
                    change_found = True
                End If
            End If


            'See If any registrations have changed - only pick letters from first date found
            Dim registration_found As Boolean = True
            Dim registration_idx As Integer
            For registration_idx = 1 To registration_no
                registration_found = False
                Dim note_date As Date
                If registration_idx = 1 Then
                    note_date = Nothing
                End If
                Dim start_idx As Integer
                Dim note_registration_no As String = ""
                For idx3 = 0 To no_of_notes - 1
                    note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
                    start_idx = InStr(note_text, "registration " & registration_idx & ":")
                    If start_idx > 0 Then
                        start_idx += 15
                    End If
                    If start_idx > 0 Then
                        If note_date = Nothing Then
                            note_date = note_dataset.Tables(0).Rows(idx3).Item(1)
                        Else
                            If Format(note_date, "yyyy-MM-dd") <> Format(note_dataset.Tables(0).Rows(idx3).Item(1), "yyyy-MM-dd") Then
                                Exit For
                            End If
                        End If
                        For idx4 = start_idx To note_text.Length
                            If Mid(note_text, idx4, 1) = ";" Then
                                note_registration_no = Mid(note_text, start_idx, idx4 - start_idx)
                                Exit For
                            End If
                        Next
                        If idx4 > note_text.Length Then
                            note_registration_no = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                        End If

                    End If
                    If note_registration_no = "" Then
                        Continue For
                    End If
                    If LCase(registrations(registration_idx)) = note_registration_no Then
                        registration_found = True
                    End If
                Next
                'end of notes
                If Not registration_found Then
                    Exit For
                End If
            Next
            If Not registration_found Then
                For registration_idx = 1 To registration_no
                    Dim registration_text As String = ""
                    If registrations(registration_idx) <> Nothing Then
                        registration_text = registration_text & "Registration " & registration_idx & ":" & registrations(registration_idx) & ";"
                    End If
                    'remove last ; before writing note
                    registration_text = Microsoft.VisualBasic.Left(registration_text, registration_text.Length - 1)
                    write_note(debtor & "|" & registration_text)
                Next
                change_found = True
            End If
            'ANY CHANGES FOUND?
            If Not change_found Then
                'audit_file = audit_file & maat_id & "|no changes found" & vbNewLine
                no_changes_file = no_changes_file & maat_id & vbNewLine
                no_changes += 1

            Else
                'audit_file = audit_file & maat_id & "|changes found" & vbNewLine
                changes += 1
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub
    Private Sub write_cap_changes(ByVal debtor As Integer, ByVal prop_idx As Integer)

        If cap_props(prop_idx).addr_city <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Address City Capital:" & cap_props(prop_idx).addr_city)
        End If
        If cap_props(prop_idx).addr_country <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Address Country Capital:" & cap_props(prop_idx).addr_country)
        End If
        If cap_props(prop_idx).addr_line1 <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Address Line1 Capital:" & cap_props(prop_idx).addr_line1)
        End If
        If cap_props(prop_idx).addr_line2 <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Address Line2 Capital:" & cap_props(prop_idx).addr_line2)
        End If
        If cap_props(prop_idx).addr_line3 <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Address Line3 Capital:" & cap_props(prop_idx).addr_line3)
        End If
        If cap_props(prop_idx).addr_pcode <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Address Postcode Capital:" & cap_props(prop_idx).addr_pcode)
        End If
        If cap_props(prop_idx).appl_equity_amt <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Applicant Equity Amount Capital:" & cap_props(prop_idx).appl_equity_amt)
        End If
        If cap_props(prop_idx).bedroom_count <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Bedroom Count Capital:" & cap_props(prop_idx).bedroom_count)
        End If
        If cap_props(prop_idx).declared_date <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Declared Date Capital:" & cap_props(prop_idx).declared_date)
        End If
        If cap_props(prop_idx).declared_equity <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Declared Equity Capital:" & cap_props(prop_idx).declared_equity)
        End If
        If cap_props(prop_idx).declared_mortgage <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Declared Mortgage Capital:" & cap_props(prop_idx).declared_mortgage)
        End If
        If cap_props(prop_idx).declared_value <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Declared Value Capital:" & cap_props(prop_idx).declared_value)
        End If
        If cap_props(prop_idx).partner_equity_amt <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Partner Equity Amount Capital:" & cap_props(prop_idx).partner_equity_amt)
        End If
        If cap_props(prop_idx).percent_appl <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Percent Owned Applicant Capital:" & cap_props(prop_idx).percent_appl)
        End If
        If cap_props(prop_idx).percent_partner <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Percent Owned Partner Capital:" & cap_props(prop_idx).percent_partner)
        End If
        If cap_props(prop_idx).prop_type_code <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Property Type Code Capital:" & cap_props(prop_idx).prop_type_code)
        End If
        If cap_props(prop_idx).prop_type_desc <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Property Type Description Capital:" & cap_props(prop_idx).prop_type_desc)
        End If
        If cap_props(prop_idx).residential_code <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Residential Code Capital:" & cap_props(prop_idx).residential_code)
        End If
        If cap_props(prop_idx).residential_desc <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Residential Description Capital:" & cap_props(prop_idx).residential_desc)
        End If
        If cap_props(prop_idx).tenant_in_place <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Tenant in Place Capital:" & cap_props(prop_idx).tenant_in_place)
        End If
        If cap_props(prop_idx).verified_value <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Verified Value Capital:" & cap_props(prop_idx).verified_value)
        End If
        If cap_props(prop_idx).verified_by <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Verified By Capital:" & cap_props(prop_idx).verified_by)
        End If
        If cap_props(prop_idx).verified_date <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Verified Date Capital:" & cap_props(prop_idx).verified_date)
        End If
        If cap_props(prop_idx).verified_mortgage <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Verified Mortgage Capital:" & cap_props(prop_idx).verified_mortgage)
        End If

        change_found = True
    End Sub
    Private Sub write_equity_changes(ByVal debtor As Integer, ByVal prop_idx As Integer)

        If equity_props(prop_idx).addr_city <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Address City Equity:" & equity_props(prop_idx).addr_city)
        End If
        If equity_props(prop_idx).addr_country <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Address Country Equity:" & equity_props(prop_idx).addr_country)
        End If
        If equity_props(prop_idx).addr_line1 <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Address Line1 Equity:" & equity_props(prop_idx).addr_line1)
        End If
        If equity_props(prop_idx).addr_line2 <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Address Line2 Equity:" & equity_props(prop_idx).addr_line2)
        End If
        If equity_props(prop_idx).addr_line3 <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Address Line3 Equity:" & equity_props(prop_idx).addr_line3)
        End If
        If equity_props(prop_idx).addr_pcode <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Address Postcode Equity:" & equity_props(prop_idx).addr_pcode)
        End If
        If equity_props(prop_idx).appl_equity_amt <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Applicant Equity Amount Equity:" & equity_props(prop_idx).appl_equity_amt)
        End If
        If equity_props(prop_idx).bedroom_count <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Bedroom Count Equity:" & equity_props(prop_idx).bedroom_count)
        End If
        If equity_props(prop_idx).declared_date <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Declared Date Equity:" & equity_props(prop_idx).declared_date)
        End If
        If equity_props(prop_idx).declared_equity <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Declared Equity Equity:" & equity_props(prop_idx).declared_equity)
        End If
        If equity_props(prop_idx).declared_mortgage <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Declared Mortgage Equity:" & equity_props(prop_idx).declared_mortgage)
        End If
        If equity_props(prop_idx).declared_value <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Declared Value Equity:" & equity_props(prop_idx).declared_value)
        End If
        If equity_props(prop_idx).partner_equity_amt <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Partner Equity Amount Equity:" & equity_props(prop_idx).partner_equity_amt)
        End If
        If equity_props(prop_idx).percent_appl <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Percent Owned Applicant Equity:" & equity_props(prop_idx).percent_appl)
        End If
        If equity_props(prop_idx).percent_partner <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Percent Owned Partner Equity:" & equity_props(prop_idx).percent_partner)
        End If
        If equity_props(prop_idx).prop_type_code <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Property Type Code Equity:" & equity_props(prop_idx).prop_type_code)
        End If
        If equity_props(prop_idx).prop_type_desc <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Property Type Description Equity:" & equity_props(prop_idx).prop_type_desc)
        End If
        If equity_props(prop_idx).residential_code <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Residential Code Equity:" & equity_props(prop_idx).residential_code)
        End If
        If equity_props(prop_idx).residential_desc <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Residential Description Equity:" & equity_props(prop_idx).residential_desc)
        End If
        If equity_props(prop_idx).tenant_in_place <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Tenant in Place Equity:" & equity_props(prop_idx).tenant_in_place)
        End If
        If equity_props(prop_idx).verified_value <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Verified Value Equity:" & equity_props(prop_idx).verified_value)
        End If
        If equity_props(prop_idx).verified_by <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Verified By Equity:" & equity_props(prop_idx).verified_by)
        End If
        If equity_props(prop_idx).verified_date <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Verified Date Equity:" & equity_props(prop_idx).verified_date)
        End If
        If equity_props(prop_idx).verified_mortgage <> -999 Then
            write_note(debtor & "|" & "Prop " & prop_idx & " Verified Mortgage Equity:" & equity_props(prop_idx).verified_mortgage)
        End If

        change_found = True
    End Sub
    Private Sub write_tp_equity_changes(ByVal debtor As Integer, ByVal prop_idx As Integer, ByVal tp_idx As Integer)

        If equity_tps(prop_idx, tp_idx).name <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " tp " & tp_idx & " name Equity:" & equity_tps(prop_idx, tp_idx).name)
        End If
        If equity_tps(prop_idx, tp_idx).rel_code <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " tp " & tp_idx & " Relationship Code Equity:" & equity_tps(prop_idx, tp_idx).rel_code)
        End If
        If equity_tps(prop_idx, tp_idx).rel_desc <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " tp " & tp_idx & " Relationship Desc Equity:" & equity_tps(prop_idx, tp_idx).rel_desc)
        End If
        If equity_tps(prop_idx, tp_idx).other_desc <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " tp " & tp_idx & " Other Desc Equity:" & equity_tps(prop_idx, tp_idx).other_desc)
        End If
        If equity_tps(prop_idx, tp_idx).created_date <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " tp " & tp_idx & " Created Date Equity:" & equity_tps(prop_idx, tp_idx).created_date)
        End If
        If equity_tps(prop_idx, tp_idx).deleted_date <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " tp " & tp_idx & " Deleted Date Equity:" & equity_tps(prop_idx, tp_idx).deleted_date)
        End If
        change_found = True
    End Sub
    Private Sub write_tp_capital_changes(ByVal debtor As Integer, ByVal prop_idx As Integer, ByVal tp_idx As Integer)

        If cap_tps(prop_idx, tp_idx).name <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " tp " & tp_idx & " name Capital:" & cap_tps(prop_idx, tp_idx).name)
        End If
        If cap_tps(prop_idx, tp_idx).rel_code <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " tp " & tp_idx & " Relationship Code Capital:" & cap_tps(prop_idx, tp_idx).rel_code)
        End If
        If cap_tps(prop_idx, tp_idx).rel_desc <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " tp " & tp_idx & " Relationship Desc Capital:" & cap_tps(prop_idx, tp_idx).rel_desc)
        End If
        If cap_tps(prop_idx, tp_idx).other_desc <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " tp " & tp_idx & " Other Desc Capital:" & cap_tps(prop_idx, tp_idx).other_desc)
        End If
        If cap_tps(prop_idx, tp_idx).created_date <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " tp " & tp_idx & " Created Date Capital:" & cap_tps(prop_idx, tp_idx).created_date)
        End If
        If cap_tps(prop_idx, tp_idx).deleted_date <> Nothing Then
            write_note(debtor & "|" & "Prop " & prop_idx & " tp " & tp_idx & " Deleted Date Capital:" & cap_tps(prop_idx, tp_idx).deleted_date)
        End If
        change_found = True
    End Sub

    Private Function date_range_valid(ByVal dte As Date) As Boolean
        Static low_date As Date = "jan 1 1900"
        Static high_date As Date = "jan 1 2050"
        If dte < low_date Or dte > high_date Then
            invalid_date_found = True
            Return (False)
        Else
            Return (True)
        End If
    End Function
    Private Function get_latest_note(ByVal tag As String, ByVal note_dataset As DataSet, ByVal no_of_notes As Integer) As String
        Dim return_text As String = ""
        Dim note_text As String = ""
        Dim idx3, idx4 As Integer
        For idx3 = 0 To no_of_notes - 1
            note_text = LCase(note_dataset.Tables(0).Rows(idx3).Item(0))
            Dim start_idx As Integer = InStr(note_text, tag)
            If start_idx > 0 Then
                start_idx += tag.Length
            End If
            If start_idx > 0 Then
                For idx4 = start_idx To note_text.Length
                    If Mid(note_text, idx4, 1) = ";" Then
                        note_text = Mid(note_text, start_idx, idx4 - start_idx)
                        Exit For
                    End If
                Next
                If idx4 > note_text.Length Then
                    return_text = Microsoft.VisualBasic.Right(note_text, idx4 - start_idx)
                End If

            End If
            If return_text <> "" Then
                Exit For
            End If
        Next
        Return return_text
    End Function
    Private Sub write_error(ByVal error_message As String)
        Static Dim error_no As Integer
        error_no += 1
        Dim ln As Integer = Microsoft.VisualBasic.Len(OpenFileDialog1.FileName)
        new_file = Microsoft.VisualBasic.Left(OpenFileDialog1.FileName, ln - 4) & "_error.txt"
        error_message = error_message & vbNewLine
        If error_no = 1 Then
            My.Computer.FileSystem.WriteAllText(new_file, error_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(new_file, error_message, True)
        End If

    End Sub
    Private Sub write_eff_date(ByVal change_message As String)
        Static Dim date_cnt As Integer
        date_cnt += 1
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_eff_date.txt"
        If date_cnt = 1 Then
            My.Computer.FileSystem.WriteAllText(new_file, change_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
        End If
    End Sub
    Private Sub write_change(ByVal change_message As String)
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_changes.txt"
        My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
    End Sub
    Private Sub write_outcome(ByVal change_message As String)
        'Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        'new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_outcome.txt"
        'My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
    End Sub
    Private Sub write_note(ByVal change_message As String)
        Static Dim note_count As Integer
        If change_message.Length = 0 Then
            Exit Sub
        End If
        If Microsoft.VisualBasic.Right(change_message, 1) <> ";" Then
            change_message = change_message & ";"
        End If
        change_message = change_message & "from feed on " & Format(Now, "dd.MM.yyyy") & vbNewLine
        Dim ln As Integer = Microsoft.VisualBasic.Len(filename)
        new_file = Microsoft.VisualBasic.Left(filename, ln - 4) & "_note.txt"
        note_count += 1
        If note_count = 1 Then
            My.Computer.FileSystem.WriteAllText(new_file, change_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(new_file, change_message, True)
        End If
    End Sub
    Public Sub remove_chrs(ByRef in_field As String)
        Dim idx2 As Integer
        Dim out_field As String = ""
        For idx2 = 1 To Microsoft.VisualBasic.Len(in_field)
            If Mid(in_field, idx2, 1) <> Chr(9) And _
              Mid(in_field, idx2, 1) <> Chr(10) And _
            Mid(in_field, idx2, 1) <> Chr(13) Then
                out_field = out_field & Mid(in_field, idx2, 1)
            End If
        Next
        in_field = out_field
    End Sub
    Private Sub remove_strange_chrs(ByRef in_field As String)
        Dim idx2 As Integer
        Dim out_field As String = ""
        For idx2 = 1 To Microsoft.VisualBasic.Len(in_field)
            Dim test As Char = Mid(in_field, idx2, 1)
            If Not Char.IsSymbol(test) Then
                out_field = out_field & Mid(in_field, idx2, 1)
            End If
        Next
        in_field = out_field
    End Sub
    Private Sub reset_fields()
        applicant_id = ""
        summons_no = ""
        maat_id = ""
        first_name = ""
        surname = ""
        dob = ""
        invalid_dob = ""
        ni_no = ""
        mthly_contrib_amt_str = ""
        mthly_contrib_amt = Nothing
        upfront_contrib_amt_str = ""
        upfront_contrib_amt = Nothing
        'final_defence_cost = Nothing
        income_contrib_cap_str = ""
        income_uplift_applied = ""
        case_type = ""
        in_court_custody = ""
        imprisoned = ""
        sentence_date = Nothing
        debt_addr1 = ""
        debt_addr2 = ""
        debt_addr3 = ""
        debt_addr4 = ""
        debt_postcode = ""
        postal_addr1 = ""
        postal_addr2 = ""
        postal_addr3 = ""
        postal_addr4 = ""
        postal_postcode = ""
        landline = ""
        mobile = ""
        email = ""
        equity_amt = -9999
        eq_partner_equity_amt = -9999
        eq_appl_equity_amt = -9999
        equity_amt_verified = ""
        cap_asset_type = ""
        cap_amt = 0
        cap_amt_verified = ""
        allowable_cap_threshold = ""
        effective_date_str = ""
        effective_date = Nothing
        future_effective_date = False
        pref_pay_method = ""
        pref_pay_code = ""
        pref_pay_day = ""
        bank_acc_name = ""
        bank_acc_no = ""
        bank_sort_code = ""
        emp_status = ""
        emp_code = ""
        rep_order_withdrawal_date_str = ""
        rep_order_withdrawal_date = Nothing
        hardship_appl_recvd = ""
        comments = ""
        outcome = ""
        outcome2 = ""
        rep_status = ""
        appeal_type = ""
        eq_residential_code = ""
        eq_residential_desc = ""
        eq_prop_type_code = ""
        eq_prop_type_desc = ""
        eq_percent_owned_appl = -999
        eq_percent_owned_partner = -999
        has_partner = ""
        partner_first_name = ""
        partner_last_name = ""
        partner_dob = Nothing
        partner_nino = ""
        partner_emp_status = ""
        partner_emp_code = ""
        contrary_interest = ""
        disability_declaration = ""
        disability_description = ""
        eq_declared_value = Nothing
        rep_status_date = Nothing
        committal_date = Nothing
        hardship_review_date = Nothing
        hardship_result = ""
        si = ""
        nfa = ""
        assessment_reason = ""
        assessment_code = ""
        assessment_date = Nothing
        eq_bedroom_count = ""
        undeclared_property = ""
        eq_declared_mortgage = -99999
        eq_verified_mortgage = -99999
        eq_verified_market_value = -99999
        no_capital_declared = ""
        all_evidence_date = Nothing
        uplift_applied_date = Nothing
        uplift_removed_date = Nothing
        ci_code = ""
        ci_desc = ""
        case_type_code = ""
        rep_status_desc = ""
        outcome_date = Nothing
        sufficient_declared_equity = ""
        sufficient_capital_and_equity = ""
        sufficient_verified_equity = ""
        total_capital_assets = -99999
        offence_type_code = ""
        offence_type_desc = ""
        mags_court_code = ""
        mags_court_desc = ""
        equity_verified_date = Nothing
        prop_equity_verified_by = ""
        equity_verified_by = ""
        sol_acc_code = ""
        sol_name = ""
        passport_date_completed = Nothing
        passport_reason_code = ""
        passport_reason_desc = ""
        passport_result_code = ""
        passport_result_desc = ""
        motor_vehicle_ownership = ""
    End Sub
    Private Sub get_comment_break(ByVal new_comment As String)
        Dim idx, idx2 As Integer
        'break comments into 250 chunks broken at ;
        Dim no_of_250s As Integer = Int(new_comment.Length / 250) + 1
        Dim init_length As Integer = new_comment.Length
        For idx = 1 To no_of_250s
            For idx2 = 250 To 1 Step -1
                If Mid(new_comment, idx2, 1) = ";" Then
                    Exit For
                End If
            Next
            comments = comments & Microsoft.VisualBasic.Left(new_comment, idx2)
            space_comments()
            new_comment = Microsoft.VisualBasic.Right(new_comment, new_comment.Length - idx2)
            If new_comment.Length <= 250 Then
                comments = comments & new_comment
                space_comments()
                Exit For
            End If
        Next
    End Sub

    Private Sub validate_xml_file()
        'open file as text first to remove any � signs
        Dim txt_doc As String
        txt_doc = My.Computer.FileSystem.ReadAllText(OpenFileDialog1.FileName)
        txt_doc = Replace(txt_doc, "�", "")
        My.Computer.FileSystem.WriteAllText(OpenFileDialog1.FileName, txt_doc, False)
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(OpenFileDialog1.FileName)
        'MsgBox("Using new XSD file")
        myDocument.Schemas.Add("", "R:\vb.net\LSC XSD\contribution_file.xsd")
        'myDocument.Schemas.Add("", "R:\vb.net\LSC XSD\contribution_file_new.xsd")
        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
    End Sub

    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_error("Error: " & e.Message)
            Case XmlSeverityType.Warning
                write_error("Warning " & e.Message)
        End Select
    End Sub


    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        log_user = My.User.Name
        Dim slash_idx As Integer = InStr(log_user, "\")
        If slash_idx > 0 Then
            log_user = Microsoft.VisualBasic.Right(log_user, log_user.Length - slash_idx)
        End If
    End Sub

End Class

Module Module1
    Public prod_run As Boolean
    Public Structure error_str
        Dim contrib_id As Double
        Dim applicant_id As Double
        Dim maat_id As Double
        Dim error_text As String
    End Structure
    Public Structure third_party_str
        Dim name As String
        Dim rel_code As String
        Dim rel_desc As String
        Dim other_desc As String
        Dim created_date As Date
        Dim deleted_date As Date
    End Structure
    Public Structure letter_str
        Dim ref As String
        Dim type As String
        Dim created As Date
        Dim printed As Date
        Dim id As String
    End Structure
    Public Structure property_str
        Dim residential_code As String
        Dim residential_desc As String
        Dim prop_type_code As String
        Dim prop_type_desc As String
        Dim addr_line1 As String
        Dim addr_line2 As String
        Dim addr_line3 As String
        Dim addr_city As String
        Dim addr_pcode As String
        Dim addr_country As String
        Dim declared_value As Decimal
        Dim declared_mortgage As Decimal
        Dim declared_equity As Decimal
        Dim declared_date As Date
        Dim verified_value As Decimal
        Dim verified_mortgage As Decimal
        Dim verified_by As String
        Dim verified_date As Date
        Dim appl_equity_amt As Decimal
        Dim partner_equity_amt As Decimal
        Dim percent_appl As String
        Dim percent_partner As String
        Dim bedroom_count As String
        Dim tenant_in_place As String
    End Structure

    Public error_table(5000) As error_str
    Public appeal_error_table(5000) As error_str
    Public conn_open As Boolean
    Public log_user As String
End Module

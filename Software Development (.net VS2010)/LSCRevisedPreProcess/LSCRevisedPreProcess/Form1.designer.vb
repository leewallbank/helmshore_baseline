<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.readbtn = New System.Windows.Forms.Button
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.exitbtn = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.FeesSQLDataSet1 = New LSCRevisedPreProcess.FeesSQLDataSet
        Me.LSC_preprocess_last_IDTableAdapter1 = New LSCRevisedPreProcess.FeesSQLDataSetTableAdapters.LSC_preprocess_last_IDTableAdapter
        Me.LSC_preprocess_last_Header_IDTableAdapter = New LSCRevisedPreProcess.FeesSQLDataSetTableAdapters.LSC_preprocess_last_Header_IDTableAdapter
        CType(Me.FeesSQLDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'readbtn
        '
        Me.readbtn.Location = New System.Drawing.Point(62, 61)
        Me.readbtn.Name = "readbtn"
        Me.readbtn.Size = New System.Drawing.Size(107, 23)
        Me.readbtn.TabIndex = 0
        Me.readbtn.Text = "Read XML file"
        Me.readbtn.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(190, 212)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 212)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 2
        '
        'FeesSQLDataSet1
        '
        Me.FeesSQLDataSet1.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'LSC_preprocess_last_IDTableAdapter1
        '
        Me.LSC_preprocess_last_IDTableAdapter1.ClearBeforeFill = True
        '
        'LSC_preprocess_last_Header_IDTableAdapter
        '
        Me.LSC_preprocess_last_Header_IDTableAdapter.ClearBeforeFill = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(320, 266)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.readbtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "LSC Revised preprocess"
        CType(Me.FeesSQLDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents readbtn As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents FeesSQLDataSet As LSCRevisedPreProcess.FeesSQLDataSet
    Friend WithEvents LSC_preprocess_last_IDBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents LSC_preprocess_last_IDTableAdapter As LSCRevisedPreProcess.FeesSQLDataSetTableAdapters.LSC_preprocess_last_IDTableAdapter
    Friend WithEvents FeesSQLDataSet1 As LSCRevisedPreProcess.FeesSQLDataSet
    Friend WithEvents LSC_preprocess_last_IDTableAdapter1 As LSCRevisedPreProcess.FeesSQLDataSetTableAdapters.LSC_preprocess_last_IDTableAdapter
    Friend WithEvents LSC_preprocess_last_Header_IDTableAdapter As LSCRevisedPreProcess.FeesSQLDataSetTableAdapters.LSC_preprocess_last_Header_IDTableAdapter

End Class

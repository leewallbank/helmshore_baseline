﻿Imports System.Windows.Forms.DataVisualization.Charting

Public Class frmMain
    Dim Activity As New clsOperationsData
    Dim PenOutstanding As Pen
    Dim ReCalcArrow As Boolean = False

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

    End Sub

    Private Sub frmMain_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Me.Paint
        DrawOutstandingArrow()
    End Sub


    Private Sub frmMain_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        Dim Horiz As Integer, Vert As Integer

        Try
            Horiz = Me.Width / 2
            Vert = (Me.Height - 30) / 2

            chtActivity.Width = Horiz
            chtActivity.Height = Vert
            chtActivity.Left = 0
            chtActivity.Top = 0

            chtPenFormBalance.Width = Horiz
            chtPenFormBalance.Height = Vert
            chtPenFormBalance.Left = Horiz
            chtPenFormBalance.Top = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub


    Private Sub frmMain_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            chtActivity.Titles.Add("Pen forms actioned")

            chtPenFormBalance.Titles.Add("Pen form volumes")

            dtpDate.Value = Date.Now

            RefreshChart()
            RefreshTotals()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub tmrRefreshCharts_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmrRefreshCharts.Tick
        RefreshChart()
    End Sub

    Private Sub tmrRefreshTotals_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmrRefreshTotals.Tick
        RefreshTotals()
    End Sub

    Private Sub RefreshChart()
        Try
            Me.Cursor = Cursors.WaitCursor

            Activity.GetAgentActivity(dtpDate.Value.ToString("yyyy-MM-dd"))

            chtActivity.Series.Clear()
            chtActivity.DataBindCrossTable(Activity.AgentActivityDataView, "statusBy", "ActionTime", "Total", "")

            For Each Series As Series In chtActivity.Series
                Series.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
                Series.XValueType = DataVisualization.Charting.ChartValueType.Time
                Series.BorderWidth = 3
            Next Series

            Activity.GetPenFormBalance(dtpDate.Value.ToString("yyyy-MM-dd"))

            chtPenFormBalance.Series.Clear()
            chtPenFormBalance.DataBindTable(Activity.PenFormBalanceDataView, "ActionTime")

            For Each Series As Series In chtPenFormBalance.Series
                Series.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
                Series.XValueType = DataVisualization.Charting.ChartValueType.Time
                Series.BorderWidth = 3
            Next Series

            chtPenFormBalance.ChartAreas(0).AxisX.IntervalAutoMode = IntervalAutoMode.VariableCount
            chtPenFormBalance.Series("Received").Color = Color.LawnGreen
            chtPenFormBalance.Series("Processed").Color = Color.Blue

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub RefreshTotals()
        Try
            Me.Cursor = Cursors.WaitCursor

            Activity.GetPenFormStatus()

            ReCalcArrow = True
            DrawOutstandingArrow()

            With Activity.PenFormStatusDataView.Table.Rows(0)
                lblOutstandingCount.Text = .Item("Outstanding").ToString
                lblReceivedTodayCount.Text = .Item("ReceivedToday").ToString
                lblUpdatedTodayCount.Text = .Item("UpdatedToday").ToString
                lblFailedCount.Text = .Item("Failed").ToString
                lblWaitingCount.Text = .Item("Waiting").ToString
                lblLastValidatedDate.Text = .Item("LastValidated").ToString
            End With

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dtpDate_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpDate.ValueChanged
        RefreshChart()
    End Sub

    Private Sub DrawOutstandingArrow()
        Dim Graph As Graphics = pnlOutstanding.CreateGraphics()

        Try
            If ReCalcArrow Then

                If Not IsNothing(PenOutstanding) Then
                    PenOutstanding.Color = Me.BackColor
                    Graph.DrawLine(PenOutstanding, 155, 40, 155, 100)
                Else
                    PenOutstanding = New Pen(Me.BackColor, 5)
                End If

                If CInt(lblOutstandingCount.Text) < Activity.PenFormStatusDataView.Table.Rows(0).Item("Outstanding") Then
                    PenOutstanding.Color = Color.Red
                    PenOutstanding.StartCap = Drawing2D.LineCap.ArrowAnchor
                    PenOutstanding.EndCap = Drawing2D.LineCap.NoAnchor
                Else
                    PenOutstanding.Color = Color.GreenYellow
                    PenOutstanding.StartCap = Drawing2D.LineCap.NoAnchor
                    PenOutstanding.EndCap = Drawing2D.LineCap.ArrowAnchor
                End If
            Else
                If IsNothing(PenOutstanding) Then PenOutstanding = New Pen(Me.BackColor, 5)
            End If

            Graph.DrawLine(PenOutstanding, 155, 40, 155, 100)
            ReCalcArrow = False

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub


End Class

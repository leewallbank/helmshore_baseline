﻿Public Class clsOperationsData
    Private ActivityDS As New DataSet
    Private AgentActivityDV As DataView
    Private PenFormBalanceDV As DataView
    Private PenFormStatusDV As DataView

    Public Sub New()
        ActivityDS.Tables.Add("AgentActivity")
        ActivityDS.Tables.Add("PenFormBalance")
        ActivityDS.Tables.Add("PenFormStatus")
    End Sub

    Protected Overrides Sub Finalize()
        If Not IsNothing(AgentActivityDV) Then AgentActivityDV.Dispose()

        If Not IsNothing(ActivityDS) Then ActivityDS.Dispose() : ActivityDS = Nothing
        MyBase.Finalize()
    End Sub

    Public ReadOnly Property AgentActivityDataView() As DataView
        Get
            AgentActivityDataView = AgentActivityDV
        End Get
    End Property

    Public ReadOnly Property PenFormBalanceDataView() As DataView
        Get
            PenFormBalanceDataView = PenFormBalanceDV
        End Get
    End Property

    Public ReadOnly Property PenFormStatusDataView() As DataView
        Get
            PenFormStatusDataView = PenFormStatusDV
        End Get
    End Property

    Public Sub GetAgentActivity(ByVal ForDate As String)
        Dim Sql As String

        Try

            Sql = "SELECT Times.ActionTime" & _
              "     , Activity.statusBY" & _
              "     , SUM(Activity.Total) AS Total " & _
              "FROM ( SELECT DISTINCT FROM_UNIXTIME(300*FLOOR(UNIX_TIMESTAMP(statusDate)/300))  AS ActionTime" & _
              "       FROM PenForm" & _
              "       WHERE status IN ('U','D')" & _
              "         AND DATE(statusDate) = '" & ForDate & "'" & _
              "         AND statusBy <> 'SVR-PROC'" & _
              "     ) AS Times " & _
              "INNER JOIN ( SELECT statusBy" & _
              "                  , FROM_UNIXTIME(300*FLOOR(UNIX_TIMESTAMP(statusDate)/300))  AS ActionTime" & _
              "                  , COUNT(*)  AS Total" & _
              "             FROM PenForm" & _
              "             WHERE status IN ('U','D')" & _
              "               AND DATE(statusDate) = '" & ForDate & "'" & _
              "               AND statusBy <> 'SVR-PROC'" & _
              "             GROUP BY statusby,FROM_UNIXTIME(300*FLOOR(UNIX_TIMESTAMP(statusDate)/300)) " & _
              "           ) AS Activity ON Activity.ActionTime <= Times.ActionTime " & _
              "GROUP BY Times.ActionTime" & _
              "     , Activity.statusBY"

            LoadDataTable(Sql, ActivityDS, "AgentActivity", False)

            AgentActivityDV = New DataView(ActivityDS.Tables("AgentActivity"))
            AgentActivityDV.AllowDelete = False
            AgentActivityDV.AllowNew = False

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub GetPenFormBalance(ByVal ForDate As String)
        Dim Sql As String

        Try

            Sql = "SELECT t.ActionTime" & _
                  "     , SUM(t.Received) AS Received" & _
                  "     , SUM(t.Processed) AS Processed " & _
                  "FROM (" & _
                  "        SELECT FROM_UNIXTIME(900*FLOOR(UNIX_TIMESTAMP(_createdDate)/900)) AS ActionTime" & _
                  "             , COUNT(*) AS Received" & _
                  "             , NULL AS Processed" & _
                  "        FROM PenForm" & _
                  "        WHERE DATE(_createdDate) = '" & ForDate & "'" & _
                  "        GROUP BY FROM_UNIXTIME(900*FLOOR(UNIX_TIMESTAMP(_createdDate)/900)) " & _
                  "        UNION ALL" & _
                  "        SELECT FROM_UNIXTIME(900*FLOOR(UNIX_TIMESTAMP(statusDate)/900)) AS ActionTime" & _
                  "             , NULL AS Received" & _
                  "             , COUNT(*) AS Processsed" & _
                  "        FROM PenForm" & _
                  "        WHERE DATE(statusDate) = '" & ForDate & "'" & _
                  "        GROUP BY FROM_UNIXTIME(900*FLOOR(UNIX_TIMESTAMP(statusDate)/900))" & _
                  "     ) AS t " & _
                  "GROUP BY t.ActionTime"

            LoadDataTable(Sql, ActivityDS, "PenFormBalance", False)

            PenFormBalanceDV = New DataView(ActivityDS.Tables("PenFormBalance"))
            PenFormBalanceDV.AllowDelete = False
            PenFormBalanceDV.AllowNew = False

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub GetPenFormStatus()
        Dim Sql As String

        Try

            Sql = "SELECT o.Outstanding" & _
                  "     , r.ReceivedToday" & _
                  "     , u.UpdatedToday" & _
                  "     , IFNULL(m.Failed,0) AS Failed" & _
                  "     , IFNULL(m.Waiting,0) AS Waiting" & _
                  "     , v.LastValidated" & _
                  "     , 0 AS AlwaysReturnOneRow " & _
                  "FROM (" & _
                  "        SELECT SUM(CASE WHEN status = 'F' THEN 1 ELSE 0 END) AS Failed" & _
                  "             , SUM(CASE WHEN status = 'W' THEN 1 ELSE 0 END) AS Waiting" & _
                  "        FROM Message" & _
                  "        WHERE status in ('W','F') " & _
                  "     ) AS m " & _
                  "CROSS JOIN (" & _
                  "        SELECT COUNT(*) AS Outstanding " & _
                  "        FROM PenForm AS p" & _
                  "        WHERE p.status NOT IN ('U','D','DE','DU')" & _
                  "           ) AS o " & _
                  "CROSS JOIN (" & _
                  "        SELECT COUNT(*) AS ReceivedToday" & _
                  "        FROM PenForm AS p" & _
                  "        WHERE DATE(p._CreatedDate) = CURDATE()" & _
                  "           ) AS r " & _
                  "CROSS JOIN (" & _
                  "        SELECT COUNT(*) AS UpdatedToday" & _
                  "        FROM PenForm AS p" & _
                  "        WHERE DATE(p.statusdate) = CURDATE()" & _
                  "           ) AS u " & _
                  "CROSS JOIN (" & _
                  "        SELECT MIN(_CreatedDate) AS LastValidated" & _
                  "        FROM PenForm AS p" & _
                  "        WHERE status = 'N'" & _
                  "           ) AS v"

            LoadDataTable(Sql, ActivityDS, "PenFormStatus", False)

            PenFormStatusDV = New DataView(ActivityDS.Tables("PenFormStatus"))
            PenFormStatusDV.AllowDelete = False
            PenFormStatusDV.AllowNew = False

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
End Class

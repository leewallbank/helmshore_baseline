﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ChartArea1 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend1 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Dim ChartArea2 As System.Windows.Forms.DataVisualization.Charting.ChartArea = New System.Windows.Forms.DataVisualization.Charting.ChartArea()
        Dim Legend2 As System.Windows.Forms.DataVisualization.Charting.Legend = New System.Windows.Forms.DataVisualization.Charting.Legend()
        Me.chtActivity = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.tmrRefreshCharts = New System.Windows.Forms.Timer(Me.components)
        Me.dtpDate = New System.Windows.Forms.DateTimePicker()
        Me.chtPenFormBalance = New System.Windows.Forms.DataVisualization.Charting.Chart()
        Me.pnlFailed = New System.Windows.Forms.Panel()
        Me.lblFailedCount = New System.Windows.Forms.Label()
        Me.lblFailed = New System.Windows.Forms.Label()
        Me.pnlWaiting = New System.Windows.Forms.Panel()
        Me.lblWaitingCount = New System.Windows.Forms.Label()
        Me.lblWaiting = New System.Windows.Forms.Label()
        Me.pnlOutstanding = New System.Windows.Forms.Panel()
        Me.lblOutstandingCount = New System.Windows.Forms.Label()
        Me.lblOutstanding = New System.Windows.Forms.Label()
        Me.pnlLastValidated = New System.Windows.Forms.Panel()
        Me.lblLastValidatedDate = New System.Windows.Forms.Label()
        Me.lblLastValidated = New System.Windows.Forms.Label()
        Me.pnlReceivedToday = New System.Windows.Forms.Panel()
        Me.lblReceivedTodayCount = New System.Windows.Forms.Label()
        Me.lblReceivedToday = New System.Windows.Forms.Label()
        Me.pnlUpdatedToday = New System.Windows.Forms.Panel()
        Me.lblUpdatedTodayCount = New System.Windows.Forms.Label()
        Me.lblUpdatedToday = New System.Windows.Forms.Label()
        Me.tmrRefreshTotals = New System.Windows.Forms.Timer(Me.components)
        CType(Me.chtActivity, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.chtPenFormBalance, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlFailed.SuspendLayout()
        Me.pnlWaiting.SuspendLayout()
        Me.pnlOutstanding.SuspendLayout()
        Me.pnlLastValidated.SuspendLayout()
        Me.pnlReceivedToday.SuspendLayout()
        Me.pnlUpdatedToday.SuspendLayout()
        Me.SuspendLayout()
        '
        'chtActivity
        '
        Me.chtActivity.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.chtActivity.BackColor = System.Drawing.Color.Transparent
        ChartArea1.BackColor = System.Drawing.Color.Transparent
        ChartArea1.Name = "ChartArea1"
        Me.chtActivity.ChartAreas.Add(ChartArea1)
        Legend1.BackColor = System.Drawing.Color.Transparent
        Legend1.Name = "Legend1"
        Me.chtActivity.Legends.Add(Legend1)
        Me.chtActivity.Location = New System.Drawing.Point(0, 0)
        Me.chtActivity.Name = "chtActivity"
        Me.chtActivity.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright
        Me.chtActivity.Size = New System.Drawing.Size(500, 400)
        Me.chtActivity.TabIndex = 0
        Me.chtActivity.Text = "Chart1"
        '
        'tmrRefreshCharts
        '
        Me.tmrRefreshCharts.Enabled = True
        Me.tmrRefreshCharts.Interval = 300000
        '
        'dtpDate
        '
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpDate.Location = New System.Drawing.Point(1, 2)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(101, 20)
        Me.dtpDate.TabIndex = 1
        '
        'chtPenFormBalance
        '
        Me.chtPenFormBalance.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.chtPenFormBalance.BackColor = System.Drawing.Color.Transparent
        ChartArea2.BackColor = System.Drawing.Color.Transparent
        ChartArea2.Name = "ChartArea1"
        Me.chtPenFormBalance.ChartAreas.Add(ChartArea2)
        Legend2.BackColor = System.Drawing.Color.Transparent
        Legend2.Name = "Legend1"
        Me.chtPenFormBalance.Legends.Add(Legend2)
        Me.chtPenFormBalance.Location = New System.Drawing.Point(500, 0)
        Me.chtPenFormBalance.Name = "chtPenFormBalance"
        Me.chtPenFormBalance.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Bright
        Me.chtPenFormBalance.Size = New System.Drawing.Size(500, 400)
        Me.chtPenFormBalance.TabIndex = 2
        Me.chtPenFormBalance.Text = "Chart1"
        '
        'pnlFailed
        '
        Me.pnlFailed.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.pnlFailed.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlFailed.Controls.Add(Me.lblFailedCount)
        Me.pnlFailed.Controls.Add(Me.lblFailed)
        Me.pnlFailed.Location = New System.Drawing.Point(500, 420)
        Me.pnlFailed.Name = "pnlFailed"
        Me.pnlFailed.Size = New System.Drawing.Size(150, 128)
        Me.pnlFailed.TabIndex = 4
        '
        'lblFailedCount
        '
        Me.lblFailedCount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFailedCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFailedCount.Location = New System.Drawing.Point(0, 39)
        Me.lblFailedCount.Name = "lblFailedCount"
        Me.lblFailedCount.Size = New System.Drawing.Size(145, 76)
        Me.lblFailedCount.TabIndex = 5
        Me.lblFailedCount.Text = "99"
        Me.lblFailedCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFailed
        '
        Me.lblFailed.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblFailed.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFailed.ForeColor = System.Drawing.Color.Red
        Me.lblFailed.Location = New System.Drawing.Point(0, 9)
        Me.lblFailed.Name = "lblFailed"
        Me.lblFailed.Size = New System.Drawing.Size(145, 20)
        Me.lblFailed.TabIndex = 4
        Me.lblFailed.Text = "Failed messages"
        Me.lblFailed.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'pnlWaiting
        '
        Me.pnlWaiting.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.pnlWaiting.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlWaiting.Controls.Add(Me.lblWaitingCount)
        Me.pnlWaiting.Controls.Add(Me.lblWaiting)
        Me.pnlWaiting.Location = New System.Drawing.Point(660, 420)
        Me.pnlWaiting.Name = "pnlWaiting"
        Me.pnlWaiting.Size = New System.Drawing.Size(150, 128)
        Me.pnlWaiting.TabIndex = 5
        '
        'lblWaitingCount
        '
        Me.lblWaitingCount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWaitingCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWaitingCount.Location = New System.Drawing.Point(0, 39)
        Me.lblWaitingCount.Name = "lblWaitingCount"
        Me.lblWaitingCount.Size = New System.Drawing.Size(145, 76)
        Me.lblWaitingCount.TabIndex = 5
        Me.lblWaitingCount.Text = "99"
        Me.lblWaitingCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblWaiting
        '
        Me.lblWaiting.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWaiting.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWaiting.ForeColor = System.Drawing.Color.Red
        Me.lblWaiting.Location = New System.Drawing.Point(0, 9)
        Me.lblWaiting.Name = "lblWaiting"
        Me.lblWaiting.Size = New System.Drawing.Size(145, 20)
        Me.lblWaiting.TabIndex = 4
        Me.lblWaiting.Text = "Waiting messages"
        Me.lblWaiting.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'pnlOutstanding
        '
        Me.pnlOutstanding.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.pnlOutstanding.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlOutstanding.Controls.Add(Me.lblOutstandingCount)
        Me.pnlOutstanding.Controls.Add(Me.lblOutstanding)
        Me.pnlOutstanding.Location = New System.Drawing.Point(20, 420)
        Me.pnlOutstanding.Name = "pnlOutstanding"
        Me.pnlOutstanding.Size = New System.Drawing.Size(150, 128)
        Me.pnlOutstanding.TabIndex = 6
        '
        'lblOutstandingCount
        '
        Me.lblOutstandingCount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblOutstandingCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOutstandingCount.Location = New System.Drawing.Point(0, 39)
        Me.lblOutstandingCount.Name = "lblOutstandingCount"
        Me.lblOutstandingCount.Size = New System.Drawing.Size(145, 76)
        Me.lblOutstandingCount.TabIndex = 5
        Me.lblOutstandingCount.Text = "99"
        Me.lblOutstandingCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblOutstanding
        '
        Me.lblOutstanding.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblOutstanding.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblOutstanding.ForeColor = System.Drawing.Color.Red
        Me.lblOutstanding.Location = New System.Drawing.Point(0, 9)
        Me.lblOutstanding.Name = "lblOutstanding"
        Me.lblOutstanding.Size = New System.Drawing.Size(145, 20)
        Me.lblOutstanding.TabIndex = 4
        Me.lblOutstanding.Text = "Outstanding"
        Me.lblOutstanding.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'pnlLastValidated
        '
        Me.pnlLastValidated.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.pnlLastValidated.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlLastValidated.Controls.Add(Me.lblLastValidatedDate)
        Me.pnlLastValidated.Controls.Add(Me.lblLastValidated)
        Me.pnlLastValidated.Location = New System.Drawing.Point(820, 420)
        Me.pnlLastValidated.Name = "pnlLastValidated"
        Me.pnlLastValidated.Size = New System.Drawing.Size(150, 128)
        Me.pnlLastValidated.TabIndex = 7
        '
        'lblLastValidatedDate
        '
        Me.lblLastValidatedDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastValidatedDate.Location = New System.Drawing.Point(0, 39)
        Me.lblLastValidatedDate.Name = "lblLastValidatedDate"
        Me.lblLastValidatedDate.Size = New System.Drawing.Size(141, 76)
        Me.lblLastValidatedDate.TabIndex = 5
        Me.lblLastValidatedDate.Text = "16/08/2012 10:58:10"
        Me.lblLastValidatedDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLastValidated
        '
        Me.lblLastValidated.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastValidated.ForeColor = System.Drawing.Color.Red
        Me.lblLastValidated.Location = New System.Drawing.Point(0, 9)
        Me.lblLastValidated.Name = "lblLastValidated"
        Me.lblLastValidated.Size = New System.Drawing.Size(141, 20)
        Me.lblLastValidated.TabIndex = 4
        Me.lblLastValidated.Text = "Last validated"
        Me.lblLastValidated.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'pnlReceivedToday
        '
        Me.pnlReceivedToday.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.pnlReceivedToday.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlReceivedToday.Controls.Add(Me.lblReceivedTodayCount)
        Me.pnlReceivedToday.Controls.Add(Me.lblReceivedToday)
        Me.pnlReceivedToday.Location = New System.Drawing.Point(180, 420)
        Me.pnlReceivedToday.MaximumSize = New System.Drawing.Size(170, 128)
        Me.pnlReceivedToday.Name = "pnlReceivedToday"
        Me.pnlReceivedToday.Size = New System.Drawing.Size(150, 128)
        Me.pnlReceivedToday.TabIndex = 8
        '
        'lblReceivedTodayCount
        '
        Me.lblReceivedTodayCount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblReceivedTodayCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceivedTodayCount.Location = New System.Drawing.Point(0, 39)
        Me.lblReceivedTodayCount.Name = "lblReceivedTodayCount"
        Me.lblReceivedTodayCount.Size = New System.Drawing.Size(145, 76)
        Me.lblReceivedTodayCount.TabIndex = 5
        Me.lblReceivedTodayCount.Text = "99"
        Me.lblReceivedTodayCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblReceivedToday
        '
        Me.lblReceivedToday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblReceivedToday.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblReceivedToday.ForeColor = System.Drawing.Color.Red
        Me.lblReceivedToday.Location = New System.Drawing.Point(0, 9)
        Me.lblReceivedToday.Name = "lblReceivedToday"
        Me.lblReceivedToday.Size = New System.Drawing.Size(145, 20)
        Me.lblReceivedToday.TabIndex = 4
        Me.lblReceivedToday.Text = "Received today"
        Me.lblReceivedToday.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'pnlUpdatedToday
        '
        Me.pnlUpdatedToday.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.pnlUpdatedToday.AutoSize = True
        Me.pnlUpdatedToday.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.pnlUpdatedToday.Controls.Add(Me.lblUpdatedTodayCount)
        Me.pnlUpdatedToday.Controls.Add(Me.lblUpdatedToday)
        Me.pnlUpdatedToday.Location = New System.Drawing.Point(340, 420)
        Me.pnlUpdatedToday.MaximumSize = New System.Drawing.Size(170, 128)
        Me.pnlUpdatedToday.Name = "pnlUpdatedToday"
        Me.pnlUpdatedToday.Size = New System.Drawing.Size(150, 128)
        Me.pnlUpdatedToday.TabIndex = 9
        '
        'lblUpdatedTodayCount
        '
        Me.lblUpdatedTodayCount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblUpdatedTodayCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUpdatedTodayCount.Location = New System.Drawing.Point(0, 39)
        Me.lblUpdatedTodayCount.Name = "lblUpdatedTodayCount"
        Me.lblUpdatedTodayCount.Size = New System.Drawing.Size(125, 76)
        Me.lblUpdatedTodayCount.TabIndex = 5
        Me.lblUpdatedTodayCount.Text = "99"
        Me.lblUpdatedTodayCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblUpdatedToday
        '
        Me.lblUpdatedToday.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblUpdatedToday.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUpdatedToday.ForeColor = System.Drawing.Color.Red
        Me.lblUpdatedToday.Location = New System.Drawing.Point(0, 9)
        Me.lblUpdatedToday.Name = "lblUpdatedToday"
        Me.lblUpdatedToday.Size = New System.Drawing.Size(125, 20)
        Me.lblUpdatedToday.TabIndex = 4
        Me.lblUpdatedToday.Text = "Updated today"
        Me.lblUpdatedToday.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'tmrRefreshTotals
        '
        Me.tmrRefreshTotals.Enabled = True
        Me.tmrRefreshTotals.Interval = 60000
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(992, 576)
        Me.Controls.Add(Me.pnlUpdatedToday)
        Me.Controls.Add(Me.pnlReceivedToday)
        Me.Controls.Add(Me.pnlLastValidated)
        Me.Controls.Add(Me.pnlOutstanding)
        Me.Controls.Add(Me.pnlWaiting)
        Me.Controls.Add(Me.pnlFailed)
        Me.Controls.Add(Me.chtPenFormBalance)
        Me.Controls.Add(Me.dtpDate)
        Me.Controls.Add(Me.chtActivity)
        Me.Name = "frmMain"
        Me.Text = "Operations Wallboard"
        CType(Me.chtActivity, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.chtPenFormBalance, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlFailed.ResumeLayout(False)
        Me.pnlWaiting.ResumeLayout(False)
        Me.pnlOutstanding.ResumeLayout(False)
        Me.pnlLastValidated.ResumeLayout(False)
        Me.pnlReceivedToday.ResumeLayout(False)
        Me.pnlUpdatedToday.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chtActivity As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents tmrRefreshCharts As System.Windows.Forms.Timer
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chtPenFormBalance As System.Windows.Forms.DataVisualization.Charting.Chart
    Friend WithEvents pnlFailed As System.Windows.Forms.Panel
    Friend WithEvents lblFailedCount As System.Windows.Forms.Label
    Friend WithEvents lblFailed As System.Windows.Forms.Label
    Friend WithEvents pnlWaiting As System.Windows.Forms.Panel
    Friend WithEvents lblWaitingCount As System.Windows.Forms.Label
    Friend WithEvents lblWaiting As System.Windows.Forms.Label
    Friend WithEvents pnlOutstanding As System.Windows.Forms.Panel
    Friend WithEvents lblOutstandingCount As System.Windows.Forms.Label
    Friend WithEvents lblOutstanding As System.Windows.Forms.Label
    Friend WithEvents pnlLastValidated As System.Windows.Forms.Panel
    Friend WithEvents lblLastValidatedDate As System.Windows.Forms.Label
    Friend WithEvents lblLastValidated As System.Windows.Forms.Label
    Friend WithEvents pnlReceivedToday As System.Windows.Forms.Panel
    Friend WithEvents lblReceivedTodayCount As System.Windows.Forms.Label
    Friend WithEvents lblReceivedToday As System.Windows.Forms.Label
    Friend WithEvents pnlUpdatedToday As System.Windows.Forms.Panel
    Friend WithEvents lblUpdatedTodayCount As System.Windows.Forms.Label
    Friend WithEvents lblUpdatedToday As System.Windows.Forms.Label
    Friend WithEvents tmrRefreshTotals As System.Windows.Forms.Timer

End Class

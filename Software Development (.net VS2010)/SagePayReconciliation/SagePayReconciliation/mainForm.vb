﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration

Public Class mainForm
    Dim InputLineArray() As String
    Dim OutputFile As String = "Match, DebtorID, Paid date,CreatedBy, Batch, Amount, Reference,BankAuthCode,CardHolder" & vbNewLine
    Private PaymentData As New clsPaymentData
    Private InputFilePath As String, FileName As String, FileExt As String
    Private Sub mainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ReconciliationDTP.Value = DateAdd(DateInterval.Day, -1, Now)
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        'read in csv file
        Try


            Dim FileDialog As New OpenFileDialog
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
            runbtn.Enabled = False
            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)
            Dim RowNumber As Integer = 0
            'Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            Dim Filecontents(,) As String = InputFromExcel(FileDialog.FileName)

            ConnectDb2("DebtRecoveryLocal")
            ProgressBar1.Maximum = UBound(Filecontents)
            For RowNumber = 0 To UBound(Filecontents)
                ProgressBar1.Value = RowNumber
                Application.DoEvents()
                'InputLineArray = CommonLibrary.modCommon.Split(InputLine, ",", """")
                'If UBound(InputLineArray) < 12 Then Continue For
                If RowNumber = 0 Then Continue For 'ignore headings
                If RowNumber = 175 Then
                    RowNumber = 175
                End If
                Dim PaymentReference As String = Filecontents(RowNumber, 13)

                Dim PaymentAmountstr As String = Filecontents(RowNumber, 7)
                'remove quotes
                Dim PaymentAmount As String = ""
                For char_idx = 1 To PaymentAmountstr.Length
                    Dim chr As Char = Mid(PaymentAmountstr, char_idx, 1)
                    If Char.IsNumber(chr) Or chr = "." Then
                        PaymentAmount &= chr
                    End If
                Next
                Dim CardHolder As String = ""
                Dim BankAuthCode As String = Filecontents(RowNumber, 14)
                Try
                    CardHolder = Filecontents(RowNumber, 1)
                Catch ex As Exception

                End Try

                'remove ODOA from name 
                Dim Temp As String = ""
                If CardHolder <> Nothing Then
                    For char_idx = 1 To CardHolder.Length
                        If Mid(CardHolder, char_idx, 1) <> Chr(10) And _
                        Mid(CardHolder, char_idx, 1) <> Chr(13) Then
                            Temp &= Mid(CardHolder, char_idx, 1)
                        End If
                    Next
                End If

                CardHolder = Temp
                Dim debtorID As Integer = 0
                'look on onestep for this payment
                Dim DebtorArray As Object() = PaymentData.GetDebtorArray(PaymentReference, PaymentAmount)
                Dim CreatedBy As String = ""
                Dim BatchID As String = ""
                Dim Paiddate As Date = Nothing
                Dim Matched As String = "X"
                If UBound(DebtorArray) > 0 Then
                    debtorID = DebtorArray(0)
                    CreatedBy = DebtorArray(1)
                    BatchID = DebtorArray(2)
                    Paiddate = DebtorArray(3)
                    If Format(Paiddate, "yyyy-MM-dd") = Format(ReconciliationDTP.Value, "yyyy-MM-dd") Then
                        Matched = "M"
                    Else
                        Matched = "D"
                    End If
                End If

                OutputFile &= Matched & "," & debtorID & ","
                If Paiddate <> Nothing Then
                    OutputFile &= Format(Paiddate, "yyyy-MM-dd")
                End If
                OutputFile &= "," & CreatedBy & "," & BatchID & "," & PaymentAmount & "," &
                    PaymentReference & "," & BankAuthCode & "," & CardHolder & vbNewLine
            Next
            WriteFile(InputFilePath & FileName & "_Reconciliation.csv", OutputFile)
            MsgBox("report created")

        Catch ex As Exception
            HandleException(ex)
        End Try
        Me.Close()
    End Sub
    Public Sub ConnectDb2(ByVal ConnStr As String)
        Try
            If Not IsNothing(DBCon) Then
                'This is only necessary following an exception...
                If DBCon.State = ConnectionState.Open Then DBCon.Close()
            End If

            DBCon.ConnectionString = ConfigurationManager.ConnectionStrings(ConnStr).ConnectionString
            DBCon.Open()

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.btnProcessFile = New System.Windows.Forms.Button()
        Me.btnViewOutputFil = New System.Windows.Forms.Button()
        Me.GBrbtn = New System.Windows.Forms.RadioButton()
        Me.batchbtn = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.tracerbtn = New System.Windows.Forms.RadioButton()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ProgressBar
        '
        Me.ProgressBar.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock
        Me.ProgressBar.Location = New System.Drawing.Point(12, 275)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(273, 22)
        Me.ProgressBar.TabIndex = 6
        '
        'btnProcessFile
        '
        Me.btnProcessFile.Location = New System.Drawing.Point(61, 205)
        Me.btnProcessFile.Name = "btnProcessFile"
        Me.btnProcessFile.Size = New System.Drawing.Size(153, 42)
        Me.btnProcessFile.TabIndex = 5
        Me.btnProcessFile.Text = "&Process File"
        Me.btnProcessFile.UseVisualStyleBackColor = True
        '
        'btnViewOutputFil
        '
        Me.btnViewOutputFil.Enabled = False
        Me.btnViewOutputFil.Location = New System.Drawing.Point(68, 108)
        Me.btnViewOutputFil.Name = "btnViewOutputFil"
        Me.btnViewOutputFil.Size = New System.Drawing.Size(153, 42)
        Me.btnViewOutputFil.TabIndex = 8
        Me.btnViewOutputFil.Text = "View &Output File"
        Me.btnViewOutputFil.UseVisualStyleBackColor = True
        '
        'GBrbtn
        '
        Me.GBrbtn.AutoSize = True
        Me.GBrbtn.Checked = True
        Me.GBrbtn.Location = New System.Drawing.Point(26, 33)
        Me.GBrbtn.Name = "GBrbtn"
        Me.GBrbtn.Size = New System.Drawing.Size(80, 17)
        Me.GBrbtn.TabIndex = 7
        Me.GBrbtn.TabStop = True
        Me.GBrbtn.Text = "GB Monthly"
        Me.GBrbtn.UseVisualStyleBackColor = True
        '
        'batchbtn
        '
        Me.batchbtn.AutoSize = True
        Me.batchbtn.Location = New System.Drawing.Point(26, 67)
        Me.batchbtn.Name = "batchbtn"
        Me.batchbtn.Size = New System.Drawing.Size(53, 17)
        Me.batchbtn.TabIndex = 8
        Me.batchbtn.Text = "Batch"
        Me.batchbtn.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tracerbtn)
        Me.GroupBox1.Controls.Add(Me.batchbtn)
        Me.GroupBox1.Controls.Add(Me.GBrbtn)
        Me.GroupBox1.Location = New System.Drawing.Point(35, 28)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 144)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Tele Append"
        '
        'tracerbtn
        '
        Me.tracerbtn.AutoSize = True
        Me.tracerbtn.Location = New System.Drawing.Point(26, 104)
        Me.tracerbtn.Name = "tracerbtn"
        Me.tracerbtn.Size = New System.Drawing.Size(53, 17)
        Me.tracerbtn.TabIndex = 9
        Me.tracerbtn.Text = "Trace"
        Me.tracerbtn.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 309)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.btnProcessFile)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Arrow Load Phone Numbers"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents btnProcessFile As System.Windows.Forms.Button
    Friend WithEvents btnViewOutputFil As System.Windows.Forms.Button
    Friend WithEvents GBrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents batchbtn As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tracerbtn As System.Windows.Forms.RadioButton

End Class

Public Class mainform
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim debt_addr(5) As String
    Dim curr_addr(5) As String
    Dim orig_amt As String = ""
    Dim inv_amt As String = ""
    Dim debtor_name As String = ""
    Dim cl_ref As String = ""
    Dim offence_number As String = ""
    Dim phone As String = ""
    Dim phone2 As String = ""
    Dim inv_number As String = ""
    Dim period_of_ovp As String = ""
    Dim os_bal As String = ""
    Dim paid As String = ""
    Dim comments As String = ""
    Dim from_date As String = ""
    Dim to_date As String = ""


    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim file, outline As String
        Dim linetext As String = ""
        Dim line(0) As String

        Dim idx, idx2 As Integer
        Dim lines As Integer = 0
        Dim txt_start, start_idx As Integer
        openbtn.Enabled = False
        reformbtn.Enabled = False
        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)
        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "Debtors Name" & vbTab & "DOB" & vbTab & "Additional Debtors Name" & vbTab & _
        "Client Ref" & vbTab & "Current Addr1" & vbTab & "Current Addr2" & vbTab & _
        "Current Addr3" & vbTab & "Current Addr4" & vbTab & "Current Addr pcode" & vbTab & _
        "Debt Addr1" & vbTab & "Debt Addr2" & vbTab & "Debt Addr3" & vbTab & "Debt Addr4" & vbTab & _
        "Debt Addr pcode" & vbTab & "From Date" & vbTab & "ToDate" & vbTab & "Comments" & vbTab & _
        "Telephone Number" & vbTab & "Additional Telephone Number" & vbTab & "Original Debt" & vbTab & _
        "Current Debt" & vbTab & "Offence number" & vbNewLine
        outfile = outline
        'look for FINAL DEMAND
        For idx = 0 To lines - 1
            If InStr(line(idx), "FINAL DEMAND") > 0 Then
                reset_fields()
            Else
                Continue For
            End If
            'get current address

            start_idx = idx + 2
            For idx2 = idx + 2 To idx + 200
                If InStr(line(idx2), "Date") > 0 Then
                    Continue For
                End If
                If InStr(line(idx2), "Dear") > 0 Then
                    Exit For
                End If
                Dim addr As String = Trim(Microsoft.VisualBasic.Left(line(idx2), 40))
                If addr.Length > 1 Then
                    addr = remove_chars(addr)
                    curr_addr(idx2 - start_idx) = addr
                End If

                txt_start = InStr(line(idx2), "Number:")
                If txt_start > 0 Then
                    Dim phone_length As String = line(idx2).Length - txt_start - 6
                    If phone_length > 3 Then
                        phone = Microsoft.VisualBasic.Right(line(idx2), phone_length)
                    End If
                End If
            Next

            'find claim
            For idx2 = idx2 + 1 To idx2 + 200
                If InStr(line(idx2), "Claim") > 0 Then
                    Exit For
                End If
            Next
            idx2 += 2
            offence_number = Trim(Mid(line(idx2), 2, 12))
            'debtor_name = Trim(Mid(line(idx2), 11, 20))
            'debtor_name = remove_chars(debtor_name)
            start_idx = idx2
            For idx2 = idx2 To idx2 + 200
                If InStr(line(idx2), "Invoice Number") > 0 Then
                    Exit For
                End If
                Dim addr As String
                Try
                    addr = Microsoft.VisualBasic.Right(line(idx2), line(idx2).Length - 40)
                Catch ex As Exception
                    addr = ""
                End Try

                If addr.Length > 2 Then
                    addr = remove_chars(addr)
                    debt_addr(idx2 - start_idx + 1) = addr
                End If

            Next
            cl_ref = Trim(Mid(line(idx2), 40, 30))
            idx2 += 2
            inv_amt = Trim(Mid(line(idx2), 40, 30))
            idx2 += 2
            period_of_ovp = Trim(Microsoft.VisualBasic.Right(line(idx2), line(idx2).Length - 40))
            Try
                from_date = CDate(Microsoft.VisualBasic.Left(period_of_ovp, 12))
            Catch ex As Exception
                from_date = ""
            End Try
            Dim start_hyphen As Integer = InStr(period_of_ovp, "-")
            If start_hyphen > 0 Then
                Try
                    to_date = CDate(Microsoft.VisualBasic.Right(period_of_ovp, period_of_ovp.Length - start_hyphen))
                Catch ex As Exception
                    to_date = ""
                End Try
            End If
           
            'comments = comments & "PeriodOfOverpayment:" & period_of_ovp & ";"
            idx2 += 2
            paid = Trim(Microsoft.VisualBasic.Right(line(idx2), line(idx2).Length - 40))
            idx2 += 2
            os_bal = Trim(Microsoft.VisualBasic.Right(line(idx2), line(idx2).Length - 40))
            For idx2 = idx2 To lines - 1
                If InStr(line(idx2), "REVENUES") > 0 Then
                    Exit For
                End If
                If InStr(line(idx2), "Telephone") > 0 Then
                    Dim phone_idx As Integer = InStr(line(idx2), ":")
                    phone = Trim(Mid(line(idx2), phone_idx + 1, 20))
                End If
            Next
            idx2 += 1
            'get full name and phone number
            start_idx = InStr(LCase(line(idx2)), "full name")
            debtor_name = Trim(Mid(line(idx2), start_idx + 11, 40))
            debtor_name = remove_chars(debtor_name)
            Dim start_idx2 As Integer
            start_idx2 = InStr(LCase(line(idx2)), "tel")
            If start_idx2 > 0 Then
                phone2 = Trim(Mid(line(idx2), start_idx2 + 4, start_idx - start_idx2 - 4))
                If phone2.Length > 3 And phone.Length < 4 Then
                    phone = phone2
                    phone2 = ""
                End If
            End If

            'validate case details
            If cl_ref = Nothing Then
                errorfile = errorfile & "Line  " & idx2 & " - No client reference" & vbNewLine
            End If

            'move pcode to element 5
            If debt_addr(5).Length = 0 Then
                debt_addr(5) = debt_addr(4)
                debt_addr(4) = ""
            End If
            If debt_addr(5).Length = 0 Then
                debt_addr(5) = debt_addr(3)
                debt_addr(3) = ""
            End If
            If curr_addr(5).Length = 0 Then
                curr_addr(5) = curr_addr(4)
                curr_addr(4) = ""
            End If
            If curr_addr(5).Length = 0 Then
                curr_addr(5) = curr_addr(3)
                curr_addr(3) = ""
            End If
            'save case in outline

            outfile = outfile & debtor_name & vbTab & "" & vbTab & "" & vbTab & cl_ref & vbTab & _
            curr_addr(1) & vbTab & curr_addr(2) & vbTab & curr_addr(3) & vbTab & curr_addr(4) & vbTab & _
            curr_addr(5) & vbTab & _
            debt_addr(1) & vbTab & debt_addr(2) & vbTab & debt_addr(3) & vbTab & debt_addr(4) & vbTab & _
            debt_addr(5) & vbTab & from_date & vbTab & to_date & vbTab & comments & vbTab & phone & vbTab & phone2 & vbTab & _
            orig_amt & vbTab & os_bal & vbTab & offence_number & vbNewLine
            idx = idx2 - 1
        Next

        viewbtn.Enabled = True
        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False, ascii)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            MsgBox("Preprocess file saved")
            Me.Close()
        End If
    End Sub
    Private Sub reset_fields()
        Dim idx As Integer
        For idx = 1 To 5
            debt_addr(idx) = ""
            curr_addr(idx) = ""
        Next
        debtor_name = ""
        orig_amt = ""
        phone = ""
        phone2 = ""
        inv_number = ""
        period_of_ovp = ""
        os_bal = ""
        inv_amt = ""
        cl_ref = ""
        offence_number = ""
        paid = ""
        comments = ""
        from_date = ""
        to_date = ""
    End Sub
    Private Function remove_chars(ByVal txt As String) As String
        Dim new_txt As String = ""
        Dim idx As Integer
        For idx = 1 To txt.Length
            If Mid(txt, idx, 1) <> Chr(10) And Mid(txt, idx, 1) <> Chr(13) And Mid(txt, idx, 1) <> Chr(12) Then
                new_txt = new_txt & Mid(txt, idx, 1)
            Else
                new_txt = new_txt & " "
            End If
        Next
        Return (Trim(new_txt))
    End Function

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
End Class

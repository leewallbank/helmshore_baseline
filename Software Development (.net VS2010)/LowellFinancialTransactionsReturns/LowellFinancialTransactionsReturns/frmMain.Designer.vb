﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.dtpDateTo = New System.Windows.Forms.DateTimePicker()
        Me.lblDateFrom = New System.Windows.Forms.Label()
        Me.dtpDateFrom = New System.Windows.Forms.DateTimePicker()
        Me.btnCreateReturns = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.lblAccessing = New System.Windows.Forms.Label()
        Me.btnViewOutputFile = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'dtpDateTo
        '
        Me.dtpDateTo.Location = New System.Drawing.Point(124, 67)
        Me.dtpDateTo.Name = "dtpDateTo"
        Me.dtpDateTo.Size = New System.Drawing.Size(126, 20)
        Me.dtpDateTo.TabIndex = 29
        '
        'lblDateFrom
        '
        Me.lblDateFrom.AutoSize = True
        Me.lblDateFrom.Location = New System.Drawing.Point(43, 45)
        Me.lblDateFrom.Name = "lblDateFrom"
        Me.lblDateFrom.Size = New System.Drawing.Size(75, 13)
        Me.lblDateFrom.TabIndex = 28
        Me.lblDateFrom.Text = "Remitted from:"
        Me.lblDateFrom.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpDateFrom
        '
        Me.dtpDateFrom.Location = New System.Drawing.Point(124, 41)
        Me.dtpDateFrom.Name = "dtpDateFrom"
        Me.dtpDateFrom.Size = New System.Drawing.Size(126, 20)
        Me.dtpDateFrom.TabIndex = 27
        '
        'btnCreateReturns
        '
        Me.btnCreateReturns.Location = New System.Drawing.Point(70, 111)
        Me.btnCreateReturns.Name = "btnCreateReturns"
        Me.btnCreateReturns.Size = New System.Drawing.Size(153, 42)
        Me.btnCreateReturns.TabIndex = 25
        Me.btnCreateReturns.Text = "Create &Return File"
        Me.btnCreateReturns.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(99, 71)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(19, 13)
        Me.Label1.TabIndex = 30
        Me.Label1.Text = "to:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ProgressBar
        '
        Me.ProgressBar.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock
        Me.ProgressBar.Location = New System.Drawing.Point(10, 232)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(273, 22)
        Me.ProgressBar.TabIndex = 31
        '
        'lblAccessing
        '
        Me.lblAccessing.AutoSize = True
        Me.lblAccessing.BackColor = System.Drawing.Color.White
        Me.lblAccessing.Location = New System.Drawing.Point(118, 236)
        Me.lblAccessing.Name = "lblAccessing"
        Me.lblAccessing.Size = New System.Drawing.Size(56, 13)
        Me.lblAccessing.TabIndex = 32
        Me.lblAccessing.Text = "Accessing"
        Me.lblAccessing.Visible = False
        '
        'btnViewOutputFile
        '
        Me.btnViewOutputFile.Enabled = False
        Me.btnViewOutputFile.Location = New System.Drawing.Point(70, 171)
        Me.btnViewOutputFile.Name = "btnViewOutputFile"
        Me.btnViewOutputFile.Size = New System.Drawing.Size(153, 42)
        Me.btnViewOutputFile.TabIndex = 33
        Me.btnViewOutputFile.Text = "View &Output File"
        Me.btnViewOutputFile.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.btnViewOutputFile)
        Me.Controls.Add(Me.lblAccessing)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtpDateTo)
        Me.Controls.Add(Me.lblDateFrom)
        Me.Controls.Add(Me.dtpDateFrom)
        Me.Controls.Add(Me.btnCreateReturns)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Lowell Financial Transactions Returns"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtpDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblDateFrom As System.Windows.Forms.Label
    Friend WithEvents dtpDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnCreateReturns As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents lblAccessing As System.Windows.Forms.Label
    Friend WithEvents btnViewOutputFile As System.Windows.Forms.Button

End Class

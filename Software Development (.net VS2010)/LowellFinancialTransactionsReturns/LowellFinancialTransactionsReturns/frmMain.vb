﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private Returns As New clsReturnsData
    Dim FilePath As String
    Dim ReturnFileName As String

    Private Sub frmMain_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        Try
            Application.DoEvents()

            Returns.GetLastRunDetails()

            dtpDateFrom.Value = Returns.LastReturnDetails.DateTo.AddDays(1)
            dtpDateTo.Value = Today.AddDays(-1)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnCreateReturns_Click(sender As Object, e As System.EventArgs) Handles btnCreateReturns.Click

        Const Separator As String = ","
        Const LineTerminator As String = vbCrLf

        Dim ReturnHeader() As String = {"AccountNumber", "ClientReferenceNumber", "ClientName", "Brand", "DCABalance", "DCABalanceDate", "TransactionValue", "TransactionDate", "AdjustmentTypeCode", "PaymentMethodCode", "TransactionReference", "CurrencyCode", "Commission", "CommissionRate,Fee"}
        Dim ReturnLine(UBound(ReturnHeader)) As String

        Try
            Dim FolderBrowserDialog As New FolderBrowserDialog

            If FolderBrowserDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                FilePath = FolderBrowserDialog.SelectedPath & "\"
            Else
                Return
            End If

            Me.Cursor = Cursors.WaitCursor

            lblAccessing.Visible = True

            Application.DoEvents()

            Returns.GetPayments(dtpDateFrom.Value, dtpDateTo.Value)
            Returns.GetLastRunDetails()

            lblAccessing.Visible = False

            ProgressBar.Maximum = Returns.Payments.Rows.Count

            ReturnFileName = "ROSSENDALES_IFT_RIF7358_" & DateTime.Today.ToString("ddMMyyyy") & (Returns.LastReturnDetails.FileSequence + 1).ToString & ".csv"

            ' the following line should never delete anything as the file number should increment, but this is included here just in case the Sub never completes
            If IO.File.Exists(FilePath & ReturnFileName) Then IO.File.Delete(FilePath & ReturnFileName)

            ' the file format is to have the total in the header rather than a footer
            AppendToFile(FilePath & ReturnFileName, "Transaction" & Separator & DateTime.Today.ToString("dd/MM/yyyy") & Separator & ReturnFileName & Separator & Returns.Payments.Compute("SUM(TransactionValue)", "").ToString & Separator & Returns.Payments.Rows.Count.ToString & LineTerminator)
            AppendToFile(FilePath & ReturnFileName, String.Join(Separator, ReturnHeader) & LineTerminator)

            For Each Payment As DataRow In Returns.Payments.Rows

                ' clear the array
                ReDim ReturnLine(14)

                ReturnLine(0) = Payment.Item("AccountNumber")
                ReturnLine(1) = Payment.Item("ClientReferenceNumber")
                ReturnLine(2) = Payment.Item("ClientName")
                ReturnLine(3) = Payment.Item("Brand")
                ReturnLine(4) = Decimal.Round(CDec(Payment.Item("DCABalance")), 2)
                ReturnLine(5) = Now.ToString("dd/MM/yyyy")
                ReturnLine(6) = Decimal.Round(CDec(Payment.Item("TransactionValue")), 2)
                ReturnLine(7) = CDate(Payment.Item("TransactionDate")).ToString("dd/MM/yyyy")

                Select Case Math.Sign(Payment.Item("TransactionValue"))
                    Case 1
                        If Payment.Item("CommissionRate") > 0 Then
                            ReturnLine(8) = "PAY"
                        Else
                            ReturnLine(8) = "NPY"
                        End If
                    Case -1
                        If Payment.Item("CommissionRate") > 0 Then
                            ReturnLine(8) = "PRV"
                        Else
                            ReturnLine(8) = "NPV"
                        End If
                End Select

                Select Case Payment.Item("PaymentMethodCode")
                    Case 3 ', 55 Commented out TS 26/Nov/2015. Request ref 67507
                        ReturnLine(9) = "CC"
                    Case 1
                        ReturnLine(9) = "CHQ"
                    Case 2
                        ReturnLine(9) = "CSH"
                    Case 13
                        ReturnLine(9) = "DBC"
                    Case 7, 58
                        ReturnLine(9) = "DC"
                    Case 60
                        ReturnLine(9) = "DD"
                    Case 6
                        ReturnLine(9) = "PO"
                    Case 55
                        ReturnLine(9) = "STO"
                    Case Else
                        ReturnLine(9) = "OTH"
                End Select

                ReturnLine(10) = Payment.Item("TransactionReference")
                ReturnLine(11) = Payment.Item("CurrencyCode")
                ReturnLine(12) = Decimal.Round(CDec(Payment.Item("Commission")), 2, MidpointRounding.AwayFromZero) ' MidpointRounding added TS 22/Mar/2016. Request ref 77416
                ReturnLine(13) = Decimal.Round(CDec(Payment.Item("CommissionRate")), 2)
                ReturnLine(14) = Decimal.Round(CDec(Payment.Item("Fee")), 2)

                AppendToFile(FilePath & ReturnFileName, String.Join(Separator, ReturnLine) & LineTerminator)

                ProgressBar.Value += 1

            Next Payment

            ' this is need to increment the file sequence number
            Returns.SetLastFileSequence(Returns.LastReturnDetails.FileSequence + 1, ReturnFileName, Returns.Payments.Rows.Count, dtpDateFrom.Value, dtpDateTo.Value)

            Me.Cursor = Cursors.Default
            btnViewOutputFile.Enabled = True

            MessageBox.Show("File created.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(FilePath & ReturnFileName) Then System.Diagnostics.Process.Start("notepad.exe", FilePath & ReturnFileName)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class

﻿Imports CommonLibrary

Public Class clsReturnsData
    Private Payment As New DataTable, LastReturnTable As New DataTable
    Private PaymentDV As DataView

    Public ReadOnly Property Payments() As DataTable
        Get
            Payments = Payment
        End Get
    End Property

    Public ReadOnly Property LastReturnDetails() As LastReturnDetails  ' added TS 12/Mar/2015. Request ref 44285
        Get
            LastReturnDetails = New LastReturnDetails
            LastReturnDetails.FileSequence = LastReturnTable.Rows(0).Item("FileSequence")
            LastReturnDetails.DateTo = LastReturnTable.Rows(0).Item("DateTo")
        End Get
    End Property

    Public Sub GetPayments(ByVal DateFrom As Date, ByVal DateTo As Date)
        Dim Sql As String
        ' p.status_date change to p.date. Request ref 54384

        Try
            ' Get updated cases
            Sql = "SELECT d.client_ref AS AccountNumber " & _
                  "     , d.offencelocation AS ClientReferenceNumber " & _
                  "     , d.offenceCourtName AS ClientName " & _
                  "     , IFNULL(( SELECT REPLACE(REPLACE(n.text, 'Client Brand:',''),';','') " & _
                  "         FROM note AS n " & _
                  "            WHERE n.DebtorID = d._rowID " & _
                  "           AND n.text LIKE 'Client Brand:%' " & _
                  "       ), '') AS Brand " & _
                  "     , d.debt_amount - ( SELECT SUM(p_s.split_debt + p_s.split_costs) " & _
                  "                         FROM payment AS p_s" & _
                  "                         WHERE p_s.debtorID = d._rowID ) AS DCABalance " & _
                  "     , p.split_debt + p.split_costs AS TransactionValue " & _
                  "     , p.date AS TransactionDate " & _
                  "     , p.amount_typeID AS PaymentMethodCode " & _
                  "     , p._rowID AS TransactionReference " & _
                  "     , 'GBP' AS CurrencyCode " & _
                  "     , (p.split_debt + p.split_costs) * (p.commClientRate / 100) AS Commission " & _
                  "     , p.commClientRate AS CommissionRate " & _
                  "     , 0.00 AS Fee " & _
                  "FROM payment AS p " & _
                  "INNER JOIN paysource AS s ON p.amount_sourceID = s._rowID " & _
                  "INNER JOIN debtor AS d ON p.debtorID = d._rowID " & _
                  "INNER JOIN clientScheme AS cs ON p.clientschemeID = cs._rowID " & _
                  "WHERE cs.ClientID = 1815 " & _
                  "  AND s.direct = 'N' " & _
                  "  AND p.status = 'R' " & _
                  "  AND DATE(p.status_date) BETWEEN '" & DateFrom.ToString("yyyy-MM-dd") & "' AND '" & DateTo.ToString("yyyy-MM-dd") & "' " & _
                  "  AND p.split_debt + p.split_costs <> 0"

            LoadDataTable("DebtRecovery", Sql, Payment, False)

            PaymentDV = New DataView(Payment)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetLastRunDetails()
        Dim Sql As String

        Try
            Sql = "SELECT ( SELECT TOP 1 DateTo " & _
                  "         FROM dbo.LowellInbound " & _
                  "         WHERE FileType = 'IFT' " & _
                  "         ORDER BY RowID DESC " & _
                  "       ) AS DateTo " & _
                  "     , ( SELECT COUNT(*) " & _
                  "         FROM dbo.LowellInbound " & _
                  "         WHERE CreatedDate > DATEADD(Day, DATEDIFF(DAY, 0, GETDATE()), 0) " & _
                  "           AND FileType = 'IFT' " & _
                  "       ) AS FileSequence"

            LoadDataTable("FeesSQL", Sql, LastReturnTable)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub SetLastFileSequence(ByVal FileSequence As String, FileName As String, ByVal CasesLoaded As Integer, ByVal DateFrom As Date, ByVal DateTo As Date)
        Try

            ExecStoredProc("FeesSQL", " EXEC dbo.SetLowellInbound 'IFT'," & FileSequence & ", '" & FileName & "', " & CasesLoaded.ToString & ", '" & DateFrom.ToString("dd/MMM/yyyy") & "', '" & DateTo.ToString("dd/MMM/yyyy") & "'")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

Public Class LastReturnDetails
    Public Property FileSequence As Integer
    Public Property DateTo As Date
End Class

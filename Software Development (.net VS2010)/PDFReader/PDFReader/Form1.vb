﻿Imports iTextSharp.text.pdf

Public Class Form1

    Public Shared Function GetTextFromPDF(ByVal PdfFileName As String) As String
        Dim oReader As New iTextSharp.text.pdf.PdfReader(PdfFileName)

        Dim sOut = ""

        For i = 1 To oReader.NumberOfPages
            Dim its As New iTextSharp.text.pdf.parser.SimpleTextExtractionStrategy

            sOut &= iTextSharp.text.pdf.parser.PdfTextExtractor.GetTextFromPage(oReader, i, its)
        Next

        Return sOut
    End Function

    Private Sub btnReadPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReadPDF.Click

        txtPDFText.Lines = GetTextFromPDF("h:\ProposedBailiffReform.pdf").Split(vbLf)

    End Sub
End Class
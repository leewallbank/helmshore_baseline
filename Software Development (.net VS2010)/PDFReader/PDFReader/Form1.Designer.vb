﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnReadPDF = New System.Windows.Forms.Button()
        Me.txtPDFText = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btnReadPDF
        '
        Me.btnReadPDF.Location = New System.Drawing.Point(494, 537)
        Me.btnReadPDF.Name = "btnReadPDF"
        Me.btnReadPDF.Size = New System.Drawing.Size(153, 29)
        Me.btnReadPDF.TabIndex = 0
        Me.btnReadPDF.Text = "Read PDF"
        Me.btnReadPDF.UseVisualStyleBackColor = True
        '
        'txtPDFText
        '
        Me.txtPDFText.Location = New System.Drawing.Point(29, 24)
        Me.txtPDFText.Multiline = True
        Me.txtPDFText.Name = "txtPDFText"
        Me.txtPDFText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtPDFText.Size = New System.Drawing.Size(600, 507)
        Me.txtPDFText.TabIndex = 1
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(659, 578)
        Me.Controls.Add(Me.txtPDFText)
        Me.Controls.Add(Me.btnReadPDF)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnReadPDF As System.Windows.Forms.Button
    Friend WithEvents txtPDFText As System.Windows.Forms.TextBox

End Class

﻿Imports System.IO
Imports System.Collections
Imports CommonLibrary
Imports System.Globalization

Public Class frmMain

    Private ConnID As String() = {"3769", "3771", "3689", "3779", "3768", "3778", "3770", "3777", "3780", "3775", "3781"}
    Private InputFilePath As String, FileName As String, FileExt As String, ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String
    Private ConnCode As New Dictionary(Of String, Integer)
    Private OutputFiles As New ArrayList()
    Private NewCases As New ArrayList()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        ConnCode.Add("Cash On Go - Debt Assigned to Motormile Finance UK Ltd", 0)
        ConnCode.Add("Cheque Centres Limited", 1)
        ConnCode.Add("Mr Lender - Debt Assigned to Motormile Finance Uk Ltd", 2)
        ConnCode.Add("National Cash Advance Ltd - debt assigned to Motormile Finance UK Ltd", 3)
        ConnCode.Add("Quick Quid Payday Loans", 4)
        ConnCode.Add("Quick Quid pay day loan - assigned to Motormile Finance UK Limited", 5)
        ConnCode.Add("Quick Quid pay day loan - assigned to Motormile Finance UK Ltd", 5)
        ConnCode.Add("Quick Quid pay day loan assigned to Motormile Finance UK Ltd", 5)
        ConnCode.Add("Quick Quid payday loan - assigned to MMF", 5)
        ConnCode.Add("Quick Quid payday loan assigned to Motormile Finance UK Ltd", 5)
        ConnCode.Add("Quid24 Ltd", 6)
        ConnCode.Add("The Loan Store Ltd - debt assigned to Motormile Finance UK Limited", 7)
        ConnCode.Add("Think Finance (UK) Ltd t/a 1 MONTH LOAN (debt assigned to Motormile Finance UK Ltd)", 8)
        ConnCode.Add("Uncle Buck Payday Loans - Debt Assigned to Motormile Finance UK Ltd", 9)
        ConnCode.Add("Wonga Payday Loan  (debt assigned to Motormile Finance UK Limited)", 10)
        ConnCode.Add("WageDayAdvance.co.uk (Debt Assigned to Motormile Finance UK Limited)", 10) ' check this
        ConnCode.Add("WageDayAdvance - Debt Assigned to Motormile Finance UK Ltd", 10) ' check this
        ConnCode.Add("WageDayAdvance.co.uk (debt assigned to Motormile Finance UK Ltd)", 10) ' check this
        ConnCode.Add("WageDayAdvance.co.uk - debt assigned to Motormile Finance UK Ltd", 10)
        ConnCode.Add("WageDayAdvance.co.uk Payday Loan - debt assigned to Motormil;e Finance UK Limited", 10)
        ConnCode.Add("WagedayAdvance.co.uk payday loan (debt assigned to Motormile Finance UK Ltd)", 10)
        ConnCode.Add("Wagedayadvance.co.uk (debt assigned to motormile finance uk ltd)", 10)

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim InputLineArray() As String, ConnKey As String = "", NewDebtNotes As String = "", NewDebtFile As String = "", NewDebtLine(22) As String, PrevConnKey As String = ""
        Dim NewDebtSumm(10, 1) As Decimal, TotalNewDebt As Decimal, TrailerNewDebt As Decimal
        Dim LineNumber As Integer, ContactNumber As Integer, ContactLandCount As Integer, ContactMobCount As Integer, ContactEmailCount As Integer, ContactOtherCount As Integer, TotalNewCount As Integer, TrailerNewCount As Integer, InvoiceCount As Integer, CaseStartLine As Integer

        Try
            FileDialog.Filter = "Motormile placement files|*.PLC|All files (*.*)|*.*"
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)

            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            For Each OldFile As String In Directory.GetFiles(InputFilePath, "*" & FileName & "_*.txt")
                File.Delete(OldFile)
            Next OldFile

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents) + 2 + UBound(NewDebtSumm, 1)

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                Application.DoEvents() ' without this line, the button disappears until processing is complete

                InputLineArray = InputLine.Split(vbTab)

                Select Case InputLineArray(0) ' The following are in the order they are expected in the file. 

                    Case "01" ' Header

                        If InputLineArray(8) <> FileExt.Replace(".", "") Then ErrorLog &= "Mismatched file extension at line number " & LineNumber.ToString & ". Header: " & InputLineArray(7) & " Actual: " & FileExt.Replace(".", "") & vbCrLf

                    Case "03" ' Account Record

                        ConnKey = InputLineArray(10)
                        If PrevConnKey <> "" Then ' Write out the previous debt
                            NewDebtLine(21) = InvoiceCount.ToString '20
                            NewDebtFile = Join(NewDebtLine, "|") & "|" & NewDebtNotes & vbCrLf
                            AppendToFile(InputFilePath & ConnID(ConnCode(PrevConnKey)) & "_" & FileName & "_PreProcessed.txt", NewDebtFile)
                        End If

                        PrevConnKey = ConnKey
                        ContactNumber = 0 ' reset this
                        InvoiceCount = 0
                        CaseStartLine = LineNumber
                        NewDebtFile = ""
                        Array.Clear(NewDebtLine, 0, NewDebtLine.Length)  ' Clear the line array
                        NewDebtNotes = ""

                        If ConnCode.ContainsKey(ConnKey) Then

                            NewDebtLine(22) = InputLineArray(6) ' clientBatch 21
                            If InputLineArray(7) <> "" Then NewDebtNotes = Join(ToNote(InputLineArray(7), "Client Code", ";"), "")
                            NewDebtLine(0) = InputLineArray(8) ' Claimant name
                            If InputLineArray(8) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(8), "Client Name", ";"), "")
                            If InputLineArray(9) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(9), "Sub Client Code", ";"), "")
                            NewDebtLine(1) = InputLineArray(10) ' Sub Claimant name
                            If InputLineArray(10) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(10), "Sub Client Name", ";"), "")
                            If InputLineArray(11) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(11), "Account Description", ";"), "")
                            NewDebtLine(2) = InputLineArray(12) ' offenceFrom 1
                            NewDebtLine(3) = InputLineArray(13) ' debtOriginal 2
                            NewDebtLine(4) = InputLineArray(14) ' debtAmount 3
                            If InputLineArray(15) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(15), "Last Payment Date", ";"), "")
                            If InputLineArray(16) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(16), "Last Payment Amount", ";"), "")
                            If InputLineArray(17) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(17), "Last Payment Type", ";"), "")
                            If InputLineArray(18) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(18), "Days No Commission", ";"), "")

                            NewDebtSumm(ConnCode(PrevConnKey), 0) += 1 ' Number of debt records
                            NewDebtSumm(ConnCode(PrevConnKey), 1) += InputLineArray(14) ' Value of debt records
                        Else
                            PrevConnKey = ""
                            ErrorLog &= "Unknown sub client name of '" & ConnKey & "'at line number " & LineNumber.ToString & ". Line cannot be processed." & vbCrLf
                        End If

                    Case "09" ' Invoice Record

                        If InputLineArray(7) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(7), "Invoice ID", ";"), "")
                        If InputLineArray(8) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(8), "Invoice Original Amount", ";"), "")
                        If InputLineArray(9) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(9), "Invoice Balance", ";"), "")
                        If InputLineArray(10) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(10), "Invoice Status", ";"), "")
                        If InputLineArray(11) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(11), "Invoice Reason", ";"), "")
                        If InputLineArray(12) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(12), "Status Change Date", ";"), "")

                        InvoiceCount += 1

                    Case "04", "11" ' Contact Record 
                        ' 11 added TS 20/02/2014 Request ref 16450
                        ' Reset contact method type counters
                        ContactLandCount = 0
                        ContactMobCount = 0
                        ContactEmailCount = 0
                        ContactOtherCount = 0
                        ContactNumber += 1

                        Dim PreviousFlag As String = "" ' added TS 20/02/2014 Request ref 16450
                        If InputLineArray(0) = "11" Then PreviousFlag = "Previous "

                        NewDebtLine(5) = InputLineArray(1) ' offenceCourt 4
                        NewDebtLine(6) = InputLineArray(3) ' clientRef 5
                        NewDebtLine(7) = InputLineArray(4) ' prevReference 6
                        NewDebtLine(8) = InputLineArray(7) ' offenceNumber 7
                        NewDebtLine(9) = InputLineArray(8) ' offenceLocation 8
                        If ContactNumber = 1 Then ' First contact
                            NewDebtLine(10) = InputLineArray(9) ' Title 9
                            NewDebtLine(11) = InputLineArray(10) ' Forename 10
                            If InputLineArray(11) <> "" Then NewDebtLine(11) &= " " & InputLineArray(11) ' any initials 10
                            NewDebtLine(12) = InputLineArray(12) ' Surname 12
                            NewDebtLine(13) = InputLineArray(13) ' DOB 13
                            NewDebtLine(14) = ConcatFields({InputLineArray(14), InputLineArray(15), InputLineArray(16), InputLineArray(17), InputLineArray(18), InputLineArray(19), InputLineArray(20)}, ",") ' Address 13
                            If InputLineArray(21) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(21), PreviousFlag & "Contact " & ContactNumber.ToString & " contact type code", ";"), "") '
                            NewDebtLine(15) = If(InputLineArray(22) = "FALSE", "N", "Y") ' Address confirmed 14
                        Else
                            ' add to notes
                            If InputLineArray(9) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(9), PreviousFlag & "Contact " & ContactNumber.ToString & " title", ";"), "")
                            If InputLineArray(10) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(10), PreviousFlag & "Contact " & ContactNumber.ToString & " forename", ";"), "")
                            If InputLineArray(11) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(11), PreviousFlag & "Contact " & ContactNumber.ToString & " initial", ";"), "")
                            If InputLineArray(12) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(12), PreviousFlag & "Contact " & ContactNumber.ToString & " surname", ";"), "")
                            If InputLineArray(13) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(13), PreviousFlag & "Contact " & ContactNumber.ToString & " DOB", ";"), "")
                            If InputLineArray(14) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(14), PreviousFlag & "Contact " & ContactNumber.ToString & " address line 1", ";"), "")
                            If InputLineArray(15) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(15), PreviousFlag & "Contact " & ContactNumber.ToString & " address line 2", ";"), "")
                            If InputLineArray(16) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(16), PreviousFlag & "Contact " & ContactNumber.ToString & " address line 3", ";"), "")
                            If InputLineArray(17) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(17), PreviousFlag & "Contact " & ContactNumber.ToString & " address line 4", ";"), "")
                            If InputLineArray(18) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(18), PreviousFlag & "Contact " & ContactNumber.ToString & " address line 5", ";"), "")
                            If InputLineArray(19) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(19), PreviousFlag & "Contact " & ContactNumber.ToString & " address line 6", ";"), "")
                            If InputLineArray(20) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(20), PreviousFlag & "Contact " & ContactNumber.ToString & " post code", ";"), "")
                            If InputLineArray(21) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(21), PreviousFlag & "Contact " & ContactNumber.ToString & " contact type code", ";"), "")
                            If InputLineArray(22) <> "" Then NewDebtNotes &= Join(ToNote(If(InputLineArray(22) = "FALSE", "N", "Y"), PreviousFlag & "Contact " & ContactNumber.ToString & " add confirmed", ";"), "")
                            If InputLineArray(23) <> "" Then NewDebtNotes &= Join(ToNote(If(InputLineArray(23) = "FALSE", "N", "Y"), PreviousFlag & "Contact " & ContactNumber.ToString & " correspondence address", ";"), "") ' added TS 20/02/2014 Request ref 16450
                        End If

                    Case "08" ' Contact Method Record
                        Dim ContactMethodType As String
                        Dim ContactMethodCount As Integer

                        Select Case InputLineArray(10)
                            Case "LND"
                                ContactLandCount += 1
                                ContactMethodType = "Landline"
                                ContactMethodCount = ContactLandCount
                            Case "MOB"
                                ContactMobCount += 1
                                ContactMethodType = "Mobile"
                                ContactMethodCount = ContactMobCount
                            Case "EML"
                                ContactEmailCount += 1
                                ContactMethodType = "Email"
                                ContactMethodCount = ContactEmailCount
                            Case Else
                                ContactOtherCount += 1
                                ContactMethodType = "Other"
                                ContactMethodCount = ContactOtherCount
                        End Select

                        ' Phone and email first
                        If ContactNumber = 1 And InputLineArray(9) <> "" Then
                            If {"LND", "MOB"}.Contains(InputLineArray(10)) Then
                                If IsNothing(NewDebtLine(17)) Then ' addPhone 16
                                    NewDebtLine(17) = InputLineArray(9) ' 16
                                ElseIf IsNothing(NewDebtLine(18)) Then ' addFax 17
                                    NewDebtLine(18) = InputLineArray(9) ' 17
                                ElseIf IsNothing(NewDebtLine(19)) Then ' empPhone 18
                                    NewDebtLine(19) = InputLineArray(9) ' 18
                                ElseIf IsNothing(NewDebtLine(20)) Then ' empFax 20
                                    NewDebtLine(20) = InputLineArray(9) ' 20
                                Else ' note
                                    NewDebtNotes &= Join(ToNote(InputLineArray(9), "Contact " & ContactNumber.ToString & " " & ContactMethodType & " " & ContactMethodCount.ToString, ";"), "")
                                End If
                            ElseIf InputLineArray(10) = "EML" Then
                                If IsNothing(NewDebtLine(16)) Then '15
                                    NewDebtLine(16) = InputLineArray(9) ' addEmail 15
                                Else
                                    NewDebtNotes &= Join(ToNote(InputLineArray(9), "Contact " & ContactNumber.ToString & " " & ContactMethodType & " " & ContactMethodCount.ToString, ";"), "")
                                End If
                            Else ' Should never hit this point but...
                                NewDebtNotes &= Join(ToNote(InputLineArray(9), "Contact " & ContactNumber.ToString & " " & ContactMethodType & " " & ContactMethodCount.ToString, ";"), "")
                            End If
                        Else
                            ' notes for Contact 2 onwards phone and email
                            If InputLineArray(9) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(9), "Contact " & ContactNumber.ToString & " " & ContactMethodType & " " & ContactMethodCount.ToString, ";"), "")
                        End If

                        ' All other contact details 
                        If InputLineArray(11) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(11), "Contact " & ContactNumber.ToString & " " & ContactMethodType & " " & ContactMethodCount.ToString & " " & "Contact Method Confirmed", ";"), "")
                        If InputLineArray(12) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(12), "Contact " & ContactNumber.ToString & " " & ContactMethodType & " " & ContactMethodCount.ToString & " " & "Contact Method Agreed", ";"), "")
                        If InputLineArray(13) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(13), "Contact " & ContactNumber.ToString & " " & ContactMethodType & " " & ContactMethodCount.ToString & " " & "Do Not Disturb", ";"), "")
                        If InputLineArray(14) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(14), "Contact " & ContactNumber.ToString & " " & ContactMethodType & " " & ContactMethodCount.ToString & " " & "Contact Method Note", ";"), "")

                    Case "07" ' Extra Details Record

                        If InputLineArray(7) <> "" And InputLineArray(8) <> "" Then NewDebtNotes &= Join(ToNote(InputLineArray(8), InputLineArray(7), ";"), "") ' Any extra details

                    Case "02" ' Trailer

                        TrailerNewCount = CInt(InputLineArray(7))
                        TrailerNewDebt = CDec(InputLineArray(8))
                        NewDebtLine(21) = InvoiceCount.ToString ' 20
                        NewDebtFile &= Join(NewDebtLine, "|") & "|" & NewDebtNotes & vbCrLf ' Write out the last debt
                        AppendToFile(InputFilePath & ConnID(ConnCode(ConnKey)) & "_" & FileName & "_PreProcessed.txt", NewDebtFile)
                    Case Else

                        ErrorLog &= "Unknown record type of '" & InputLineArray(0) & "'at line number " & LineNumber.ToString & ". Line cannot be processed." & vbCrLf

                End Select

            Next InputLine

            ProgressBar.Value += 1
            AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            For NewDebtFileCount As Integer = 0 To UBound(NewDebtSumm, 1)
                ProgressBar.Value += 1
                If NewDebtSumm(NewDebtFileCount, 0) <> 0 Then
                    OutputFiles.Add(InputFilePath & ConnID(NewDebtFileCount) & "_" & FileName & "_PreProcessed.txt")
                    AuditLog &= "Clientscheme: " & ConnID(NewDebtFileCount) & vbCrLf
                    AuditLog &= "Number of new cases: " & NewDebtSumm(NewDebtFileCount, 0).ToString & vbCrLf
                    AuditLog &= "Total balance of new cases: " & NewDebtSumm(NewDebtFileCount, 1).ToString("0.00") & vbCrLf & vbCrLf
                    TotalNewCount += NewDebtSumm(NewDebtFileCount, 0)
                    TotalNewDebt += NewDebtSumm(NewDebtFileCount, 1)
                End If
            Next NewDebtFileCount

            If UBound(FileContents) + 1 <> TrailerNewCount Then ErrorLog &= "Incorrect trailer data record count at line number " & LineNumber.ToString & ". Trailer: " & TrailerNewCount.ToString & " Actual: " & (UBound(FileContents) + 1).ToString & vbCrLf
            If TrailerNewDebt <> Decimal.Round(TotalNewDebt, 2) Then ErrorLog &= "Incorrect trailer total balance at line number " & LineNumber.ToString & ". Trailer: " & TrailerNewDebt.ToString & " Detail: " & TotalNewDebt.ToString & vbCrLf

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)
            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", LogHeader & ExceptionLog)

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("notepad.exe", InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If AuditLog <> "" And File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If ErrorLog <> "" And File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If ExceptionLog <> "" And File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub
End Class

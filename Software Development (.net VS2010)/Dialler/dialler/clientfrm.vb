Public Class clientfrm

    Private Sub clientfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        param1 = "onestep"
        If collect_cases Then
            param2 = "select distinct C._rowid, C.name from Client C, clientscheme CS " & _
        " where C._rowid <> 1 and C._rowid <> 2 and C._rowid <> 24 " & _
        " and CS.ClientID = C._rowID" & _
        " and CS.branchID = 2 order by name"
        Else
            param2 = "select distinct C.name from Client C, clientscheme CS " & _
        " where C._rowid <> 1 and C._rowid <> 2 and C._rowid <> 24 " & _
        " and CS.ClientID = C._rowID" & _
        " and CS.BranchID in (1,10,24) order by name"
        End If
        
        Dim cl_dataset As DataSet = get_dataset(param1, param2)
        Dim idx As Integer
        Dim lastClientname As String = ""
        DataGridView1.Rows.Clear()
        For idx = 0 To no_of_rows - 1
            Dim cl_no As String
            Dim cl_name As String = ""
            Dim shortClientName As String
            If collect_cases Then
                cl_no = cl_dataset.Tables(0).Rows(idx).Item(0)
                cl_name = Trim(cl_dataset.Tables(0).Rows(idx).Item(1))
                shortClientName = ""
            Else
                cl_name = Trim(cl_dataset.Tables(0).Rows(idx).Item(0))
                shortclientname = cl_name
                shortClientName = Trim(Replace(shortClientName, "Client:", ""))
                shortClientName = Replace(shortClientName, "London Borough", "")
                shortClientName = Replace(shortClientName, "S:", "")
                shortClientName = Trim(shortClientName)
                If shortClientName.Length > 10 Then
                    shortClientName = Microsoft.VisualBasic.Left(shortClientName, 10)
                End If
                If shortClientName = lastClientname Then
                    Continue For
                End If
                cl_no = 9999
                lastClientname = shortClientName
            End If
            DataGridView1.Rows.Add(cl_no, shortClientName, cl_name)
        Next

    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        Me.Close()
    End Sub

    Private Sub DataGridView1_RowEnter1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter
        selected_client = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        selected_client_name = DataGridView1.Rows(e.RowIndex).Cells(2).Value
        selected_short_client_name = DataGridView1.Rows(e.RowIndex).Cells(1).Value
    End Sub
End Class
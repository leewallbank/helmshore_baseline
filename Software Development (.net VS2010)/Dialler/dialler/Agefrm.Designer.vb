<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Agefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.age_gbox = New System.Windows.Forms.GroupBox()
        Me.age8rbtn = New System.Windows.Forms.RadioButton()
        Me.age7rbtn = New System.Windows.Forms.RadioButton()
        Me.age6rbtn = New System.Windows.Forms.RadioButton()
        Me.age5rbtn = New System.Windows.Forms.RadioButton()
        Me.age4rbtn = New System.Windows.Forms.RadioButton()
        Me.age3rbtn = New System.Windows.Forms.RadioButton()
        Me.age2rbtn = New System.Windows.Forms.RadioButton()
        Me.age1rbtn = New System.Windows.Forms.RadioButton()
        Me.createbtn = New System.Windows.Forms.Button()
        Me.cancelbtn = New System.Windows.Forms.Button()
        Me.age_gbox.SuspendLayout()
        Me.SuspendLayout()
        '
        'age_gbox
        '
        Me.age_gbox.Controls.Add(Me.age8rbtn)
        Me.age_gbox.Controls.Add(Me.age7rbtn)
        Me.age_gbox.Controls.Add(Me.age6rbtn)
        Me.age_gbox.Controls.Add(Me.age5rbtn)
        Me.age_gbox.Controls.Add(Me.age4rbtn)
        Me.age_gbox.Controls.Add(Me.age3rbtn)
        Me.age_gbox.Controls.Add(Me.age2rbtn)
        Me.age_gbox.Controls.Add(Me.age1rbtn)
        Me.age_gbox.Location = New System.Drawing.Point(50, 33)
        Me.age_gbox.Name = "age_gbox"
        Me.age_gbox.Size = New System.Drawing.Size(200, 212)
        Me.age_gbox.TabIndex = 0
        Me.age_gbox.TabStop = False
        Me.age_gbox.Text = "Select Case Age Range"
        '
        'age8rbtn
        '
        Me.age8rbtn.AutoSize = True
        Me.age8rbtn.Location = New System.Drawing.Point(19, 192)
        Me.age8rbtn.Name = "age8rbtn"
        Me.age8rbtn.Size = New System.Drawing.Size(74, 17)
        Me.age8rbtn.TabIndex = 7
        Me.age8rbtn.Text = "202+ days"
        Me.age8rbtn.UseVisualStyleBackColor = True
        '
        'age7rbtn
        '
        Me.age7rbtn.AutoSize = True
        Me.age7rbtn.Location = New System.Drawing.Point(19, 169)
        Me.age7rbtn.Name = "age7rbtn"
        Me.age7rbtn.Size = New System.Drawing.Size(89, 17)
        Me.age7rbtn.TabIndex = 6
        Me.age7rbtn.Text = "173-201 days"
        Me.age7rbtn.UseVisualStyleBackColor = True
        '
        'age6rbtn
        '
        Me.age6rbtn.AutoSize = True
        Me.age6rbtn.Location = New System.Drawing.Point(19, 146)
        Me.age6rbtn.Name = "age6rbtn"
        Me.age6rbtn.Size = New System.Drawing.Size(89, 17)
        Me.age6rbtn.TabIndex = 5
        Me.age6rbtn.Text = "144-172 days"
        Me.age6rbtn.UseVisualStyleBackColor = True
        '
        'age5rbtn
        '
        Me.age5rbtn.AutoSize = True
        Me.age5rbtn.Location = New System.Drawing.Point(19, 123)
        Me.age5rbtn.Name = "age5rbtn"
        Me.age5rbtn.Size = New System.Drawing.Size(89, 17)
        Me.age5rbtn.TabIndex = 4
        Me.age5rbtn.Text = "115-143 days"
        Me.age5rbtn.UseVisualStyleBackColor = True
        '
        'age4rbtn
        '
        Me.age4rbtn.AutoSize = True
        Me.age4rbtn.Location = New System.Drawing.Point(19, 100)
        Me.age4rbtn.Name = "age4rbtn"
        Me.age4rbtn.Size = New System.Drawing.Size(83, 17)
        Me.age4rbtn.TabIndex = 3
        Me.age4rbtn.Text = "86-114 days"
        Me.age4rbtn.UseVisualStyleBackColor = True
        '
        'age3rbtn
        '
        Me.age3rbtn.AutoSize = True
        Me.age3rbtn.Location = New System.Drawing.Point(19, 77)
        Me.age3rbtn.Name = "age3rbtn"
        Me.age3rbtn.Size = New System.Drawing.Size(77, 17)
        Me.age3rbtn.TabIndex = 2
        Me.age3rbtn.Text = "58-85 days"
        Me.age3rbtn.UseVisualStyleBackColor = True
        '
        'age2rbtn
        '
        Me.age2rbtn.AutoSize = True
        Me.age2rbtn.Location = New System.Drawing.Point(19, 54)
        Me.age2rbtn.Name = "age2rbtn"
        Me.age2rbtn.Size = New System.Drawing.Size(77, 17)
        Me.age2rbtn.TabIndex = 1
        Me.age2rbtn.Text = "29-57 days"
        Me.age2rbtn.UseVisualStyleBackColor = True
        '
        'age1rbtn
        '
        Me.age1rbtn.AutoSize = True
        Me.age1rbtn.Checked = True
        Me.age1rbtn.Location = New System.Drawing.Point(19, 31)
        Me.age1rbtn.Name = "age1rbtn"
        Me.age1rbtn.Size = New System.Drawing.Size(71, 17)
        Me.age1rbtn.TabIndex = 0
        Me.age1rbtn.TabStop = True
        Me.age1rbtn.Text = "3-28 days"
        Me.age1rbtn.UseVisualStyleBackColor = True
        '
        'createbtn
        '
        Me.createbtn.Location = New System.Drawing.Point(165, 280)
        Me.createbtn.Name = "createbtn"
        Me.createbtn.Size = New System.Drawing.Size(95, 23)
        Me.createbtn.TabIndex = 2
        Me.createbtn.Text = "Create Report"
        Me.createbtn.UseVisualStyleBackColor = True
        '
        'cancelbtn
        '
        Me.cancelbtn.Location = New System.Drawing.Point(21, 280)
        Me.cancelbtn.Name = "cancelbtn"
        Me.cancelbtn.Size = New System.Drawing.Size(75, 23)
        Me.cancelbtn.TabIndex = 1
        Me.cancelbtn.Text = "Cancel"
        Me.cancelbtn.UseVisualStyleBackColor = True
        '
        'Agefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 356)
        Me.Controls.Add(Me.cancelbtn)
        Me.Controls.Add(Me.createbtn)
        Me.Controls.Add(Me.age_gbox)
        Me.Name = "Agefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Select Age range"
        Me.age_gbox.ResumeLayout(False)
        Me.age_gbox.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents age_gbox As System.Windows.Forms.GroupBox
    Friend WithEvents age5rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents age4rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents age3rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents age2rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents age1rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents createbtn As System.Windows.Forms.Button
    Friend WithEvents cancelbtn As System.Windows.Forms.Button
    Friend WithEvents age8rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents age7rbtn As System.Windows.Forms.RadioButton
    Friend WithEvents age6rbtn As System.Windows.Forms.RadioButton
End Class

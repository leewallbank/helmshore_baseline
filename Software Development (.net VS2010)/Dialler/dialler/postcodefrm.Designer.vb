<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class postcodefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.OoacodeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OoanameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.OutofareaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FeesSQLDataSet = New dialler.FeesSQLDataSet
        Me.Out_of_areaTableAdapter = New dialler.FeesSQLDataSetTableAdapters.Out_of_areaTableAdapter
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OutofareaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.OoacodeDataGridViewTextBoxColumn, Me.OoanameDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.OutofareaBindingSource
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(253, 692)
        Me.DataGridView1.TabIndex = 0
        '
        'OoacodeDataGridViewTextBoxColumn
        '
        Me.OoacodeDataGridViewTextBoxColumn.DataPropertyName = "ooa_code"
        Me.OoacodeDataGridViewTextBoxColumn.HeaderText = "ooa_code"
        Me.OoacodeDataGridViewTextBoxColumn.Name = "OoacodeDataGridViewTextBoxColumn"
        Me.OoacodeDataGridViewTextBoxColumn.ReadOnly = True
        Me.OoacodeDataGridViewTextBoxColumn.Visible = False
        '
        'OoanameDataGridViewTextBoxColumn
        '
        Me.OoanameDataGridViewTextBoxColumn.DataPropertyName = "ooa_name"
        Me.OoanameDataGridViewTextBoxColumn.HeaderText = "Postcode Sector"
        Me.OoanameDataGridViewTextBoxColumn.Name = "OoanameDataGridViewTextBoxColumn"
        '
        'OutofareaBindingSource
        '
        Me.OutofareaBindingSource.DataMember = "Out_of_area"
        Me.OutofareaBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Out_of_areaTableAdapter
        '
        Me.Out_of_areaTableAdapter.ClearBeforeFill = True
        '
        'postcodefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(253, 692)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "postcodefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "postcodefrm"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OutofareaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents FeesSQLDataSet As dialler.FeesSQLDataSet
    Friend WithEvents OutofareaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Out_of_areaTableAdapter As dialler.FeesSQLDataSetTableAdapters.Out_of_areaTableAdapter
    Friend WithEvents OoacodeDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OoanameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

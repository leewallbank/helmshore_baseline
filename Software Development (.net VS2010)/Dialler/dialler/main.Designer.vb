<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainform
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim eonSetLetter As System.Windows.Forms.Button
        Me.prioritybtn = New System.Windows.Forms.Button()
        Me.top20btn = New System.Windows.Forms.Button()
        Me.paymentsbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.clntbtn = New System.Windows.Forms.Button()
        Me.arrbtn = New System.Windows.Forms.Button()
        Me.agentbtn = New System.Windows.Forms.Button()
        Me.ooabtn = New System.Windows.Forms.Button()
        Me.otherbtn = New System.Windows.Forms.Button()
        Me.returnbtn = New System.Windows.Forms.Button()
        Me.cmec_ooabtn = New System.Windows.Forms.Button()
        Me.cmecbtn = New System.Windows.Forms.Button()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.bail_retnbtn = New System.Windows.Forms.Button()
        Me.hmrcbtn = New System.Windows.Forms.Button()
        Me.feesbtn = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lsc_camp1btn = New System.Windows.Forms.Button()
        Me.lsc_camp2btn = New System.Windows.Forms.Button()
        Me.lsc_defaultedbtn = New System.Windows.Forms.Button()
        Me.lsc_fdcbtn = New System.Windows.Forms.Button()
        Me.lsc_crystalbtn = New System.Windows.Forms.Button()
        Me.rtn_not_cmecbtn = New System.Windows.Forms.Button()
        Me.cco_btn = New System.Windows.Forms.Button()
        Me.arrow_sch_grpbtn = New System.Windows.Forms.Button()
        Me.arrow_sch_cbox = New System.Windows.Forms.ComboBox()
        Me.sch_grplbl = New System.Windows.Forms.Label()
        Me.cpw_schbtn = New System.Windows.Forms.Button()
        Me.cpw_schlbl = New System.Windows.Forms.Label()
        Me.cpw_sch_cbox = New System.Windows.Forms.ComboBox()
        Me.outcallbtn = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.vulnerbtn = New System.Windows.Forms.Button()
        Me.UUBtn = New System.Windows.Forms.Button()
        Me.UUCbox = New System.Windows.Forms.ComboBox()
        Me.UUlbl = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tma_rbtn = New System.Windows.Forms.Button()
        Me.hbop_mbtn = New System.Windows.Forms.Button()
        Me.tmabtn = New System.Windows.Forms.Button()
        Me.hmrcagebtn = New System.Windows.Forms.Button()
        Me.precom1btn = New System.Windows.Forms.Button()
        Me.precom2btn = New System.Windows.Forms.Button()
        Me.fvanbtn = New System.Windows.Forms.Button()
        Me.rtdfvanbtn = New System.Windows.Forms.Button()
        Me.fsbtn = New System.Windows.Forms.Button()
        Me.dvlabtn = New System.Windows.Forms.Button()
        Me.arrow_by_agebtn = New System.Windows.Forms.Button()
        Me.lowellbtn = New System.Windows.Forms.Button()
        Me.dvlabrokenbtn = New System.Windows.Forms.Button()
        Me.slbtn = New System.Windows.Forms.Button()
        Me.etcbtn = New System.Windows.Forms.Button()
        Me.awbtn = New System.Windows.Forms.Button()
        Me.etc_brokenbtn = New System.Windows.Forms.Button()
        Me.listbtn = New System.Windows.Forms.Button()
        Me.segmentbtn = New System.Windows.Forms.Button()
        Me.appealbtn = New System.Windows.Forms.Button()
        Me.compbtn7 = New System.Windows.Forms.Button()
        Me.remindbtn = New System.Windows.Forms.Button()
        Me.remind23btn = New System.Windows.Forms.Button()
        Me.precomL1btn = New System.Windows.Forms.Button()
        Me.retnbtn = New System.Windows.Forms.Button()
        Me.hmrc_batchbtn = New System.Windows.Forms.Button()
        Me.finrembtn = New System.Windows.Forms.Button()
        Me.finenfbtn = New System.Windows.Forms.Button()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.br1_postenfbtn = New System.Windows.Forms.Button()
        Me.br1_enfbtn = New System.Windows.Forms.Button()
        Me.br1_compbtn = New System.Windows.Forms.Button()
        Me.RNNDRBtn = New System.Windows.Forms.Button()
        Me.exceptionbtn = New System.Windows.Forms.Button()
        Me.stagecbox = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.furtherenfbtn = New System.Windows.Forms.Button()
        Me.precommL2btn = New System.Windows.Forms.Button()
        Me.enfletterbtn = New System.Windows.Forms.Button()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.mar_postenfbtn = New System.Windows.Forms.Button()
        Me.mar_enfbtn = New System.Windows.Forms.Button()
        Me.mar_compbtn = New System.Windows.Forms.Button()
        Me.MNNdrBtn = New System.Windows.Forms.Button()
        Me.Mexceptionbtn = New System.Windows.Forms.Button()
        Me.Mstagecbox = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Mfurtherenfbtn = New System.Windows.Forms.Button()
        Me.Mretnbtn = New System.Windows.Forms.Button()
        Me.MprecommL2 = New System.Windows.Forms.Button()
        Me.MpcommL1btn = New System.Windows.Forms.Button()
        Me.Mfinenfbtn = New System.Windows.Forms.Button()
        Me.MfinRemindbtn = New System.Windows.Forms.Button()
        Me.MenfLetterbtn = New System.Windows.Forms.Button()
        Me.MfinalWbtn = New System.Windows.Forms.Button()
        Me.Mremindbtn = New System.Windows.Forms.Button()
        Me.Mcompbtn = New System.Windows.Forms.Button()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.hmrcsw2btn = New System.Windows.Forms.Button()
        Me.hmrcsw1btn = New System.Windows.Forms.Button()
        Me.hmrcl4btn = New System.Windows.Forms.Button()
        Me.hmrcl3btn = New System.Windows.Forms.Button()
        Me.hmrcl2btn = New System.Windows.Forms.Button()
        Me.hmrcintrobtn = New System.Windows.Forms.Button()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.slcs3btn = New System.Windows.Forms.Button()
        Me.slcs2letterbtn = New System.Windows.Forms.Button()
        Me.noasentbtn = New System.Windows.Forms.Button()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.laa3btn = New System.Windows.Forms.Button()
        Me.laa2btn = New System.Windows.Forms.Button()
        Me.laal1btn = New System.Windows.Forms.Button()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.lowell90btn = New System.Windows.Forms.Button()
        Me.lowells3btn = New System.Windows.Forms.Button()
        Me.lowells2btn = New System.Windows.Forms.Button()
        Me.lowells1lbtn = New System.Windows.Forms.Button()
        Me.UU = New System.Windows.Forms.TabPage()
        Me.uusl4btn = New System.Windows.Forms.Button()
        Me.uusl3btn = New System.Windows.Forms.Button()
        Me.uusl2btn = New System.Windows.Forms.Button()
        Me.uusl1btn = New System.Windows.Forms.Button()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.southendbtn = New System.Windows.Forms.Button()
        Me.ret_pendingbtn = New System.Windows.Forms.Button()
        Me.allsdbtn = New System.Windows.Forms.Button()
        Me.allftabtn = New System.Windows.Forms.Button()
        Me.allhbopbtn = New System.Windows.Forms.Button()
        Me.adhoc90btn = New System.Windows.Forms.Button()
        Me.adhocbabtn = New System.Windows.Forms.Button()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.brokenbtn = New System.Windows.Forms.Button()
        Me.softtracebtn = New System.Windows.Forms.Button()
        Me.outboundbtn = New System.Windows.Forms.Button()
        Me.hello8plusbtn = New System.Windows.Forms.Button()
        Me.hello4to8btn = New System.Windows.Forms.Button()
        Me.TabPage11 = New System.Windows.Forms.TabPage()
        Me.eonS2 = New System.Windows.Forms.Button()
        Me.EONS1B = New System.Windows.Forms.Button()
        Me.eonS1A = New System.Windows.Forms.Button()
        Me.TabPage12 = New System.Windows.Forms.TabPage()
        Me.sw_postenfbtn = New System.Windows.Forms.Button()
        Me.sw_enfbtn = New System.Windows.Forms.Button()
        Me.sw_compbtn = New System.Windows.Forms.Button()
        Me.SNNDRbtn = New System.Windows.Forms.Button()
        Me.swrem2btn = New System.Windows.Forms.Button()
        Me.swrunexcbtn = New System.Windows.Forms.Button()
        Me.swexcombo = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.swfenfbtn = New System.Windows.Forms.Button()
        Me.swpc2btn = New System.Windows.Forms.Button()
        Me.swenfbtn = New System.Windows.Forms.Button()
        Me.swretbtn = New System.Windows.Forms.Button()
        Me.swfebtn = New System.Windows.Forms.Button()
        Me.swpc1btn = New System.Windows.Forms.Button()
        Me.swcompbtn = New System.Windows.Forms.Button()
        Me.swrem1btn = New System.Windows.Forms.Button()
        Me.swfrbtn = New System.Windows.Forms.Button()
        Me.lowellnotvulbtn = New System.Windows.Forms.Button()
        Me.TabControl2 = New System.Windows.Forms.TabControl()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.SLNotvul = New System.Windows.Forms.Button()
        Me.LAANotvul = New System.Windows.Forms.Button()
        Me.HMRCnotvul = New System.Windows.Forms.Button()
        Me.adhocClientbtn = New System.Windows.Forms.Button()
        eonSetLetter = New System.Windows.Forms.Button()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.TabPage8.SuspendLayout()
        Me.UU.SuspendLayout()
        Me.TabPage9.SuspendLayout()
        Me.TabPage10.SuspendLayout()
        Me.TabPage11.SuspendLayout()
        Me.TabPage12.SuspendLayout()
        Me.TabControl2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        Me.SuspendLayout()
        '
        'eonSetLetter
        '
        eonSetLetter.Location = New System.Drawing.Point(162, 133)
        eonSetLetter.Name = "eonSetLetter"
        eonSetLetter.Size = New System.Drawing.Size(134, 23)
        eonSetLetter.TabIndex = 46
        eonSetLetter.Text = "Settlement Letter 6+ days"
        eonSetLetter.UseVisualStyleBackColor = True
        AddHandler eonSetLetter.Click, AddressOf Me.eonSetLetter_Click
        '
        'prioritybtn
        '
        Me.prioritybtn.Location = New System.Drawing.Point(37, 52)
        Me.prioritybtn.Name = "prioritybtn"
        Me.prioritybtn.Size = New System.Drawing.Size(86, 23)
        Me.prioritybtn.TabIndex = 0
        Me.prioritybtn.Text = "Priority clients"
        Me.prioritybtn.UseVisualStyleBackColor = True
        '
        'top20btn
        '
        Me.top20btn.Location = New System.Drawing.Point(162, 52)
        Me.top20btn.Name = "top20btn"
        Me.top20btn.Size = New System.Drawing.Size(99, 23)
        Me.top20btn.TabIndex = 1
        Me.top20btn.Text = "Top 20 clients"
        Me.top20btn.UseVisualStyleBackColor = True
        '
        'paymentsbtn
        '
        Me.paymentsbtn.Location = New System.Drawing.Point(286, 96)
        Me.paymentsbtn.Name = "paymentsbtn"
        Me.paymentsbtn.Size = New System.Drawing.Size(110, 23)
        Me.paymentsbtn.TabIndex = 7
        Me.paymentsbtn.Text = "Payment or Broken"
        Me.paymentsbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(969, 656)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 54
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'clntbtn
        '
        Me.clntbtn.Location = New System.Drawing.Point(37, 96)
        Me.clntbtn.Name = "clntbtn"
        Me.clntbtn.Size = New System.Drawing.Size(95, 23)
        Me.clntbtn.TabIndex = 5
        Me.clntbtn.Text = "Choose Client"
        Me.clntbtn.UseVisualStyleBackColor = True
        '
        'arrbtn
        '
        Me.arrbtn.Location = New System.Drawing.Point(68, 249)
        Me.arrbtn.Name = "arrbtn"
        Me.arrbtn.Size = New System.Drawing.Size(137, 23)
        Me.arrbtn.TabIndex = 8
        Me.arrbtn.Text = "Arrangements > 3 months old"
        Me.arrbtn.UseVisualStyleBackColor = True
        '
        'agentbtn
        '
        Me.agentbtn.Location = New System.Drawing.Point(433, 52)
        Me.agentbtn.Name = "agentbtn"
        Me.agentbtn.Size = New System.Drawing.Size(109, 23)
        Me.agentbtn.TabIndex = 3
        Me.agentbtn.Text = "Select Agent"
        Me.agentbtn.UseVisualStyleBackColor = True
        '
        'ooabtn
        '
        Me.ooabtn.Location = New System.Drawing.Point(12, 616)
        Me.ooabtn.Name = "ooabtn"
        Me.ooabtn.Size = New System.Drawing.Size(126, 23)
        Me.ooabtn.TabIndex = 53
        Me.ooabtn.Text = "Out of Area"
        Me.ooabtn.UseVisualStyleBackColor = True
        '
        'otherbtn
        '
        Me.otherbtn.Location = New System.Drawing.Point(286, 52)
        Me.otherbtn.Name = "otherbtn"
        Me.otherbtn.Size = New System.Drawing.Size(86, 23)
        Me.otherbtn.TabIndex = 2
        Me.otherbtn.Text = "Other clients"
        Me.otherbtn.UseVisualStyleBackColor = True
        '
        'returnbtn
        '
        Me.returnbtn.Location = New System.Drawing.Point(162, 96)
        Me.returnbtn.Name = "returnbtn"
        Me.returnbtn.Size = New System.Drawing.Size(99, 43)
        Me.returnbtn.TabIndex = 6
        Me.returnbtn.Text = "Return Stage (incHMRC)"
        Me.returnbtn.UseVisualStyleBackColor = True
        '
        'cmec_ooabtn
        '
        Me.cmec_ooabtn.Location = New System.Drawing.Point(320, 479)
        Me.cmec_ooabtn.Name = "cmec_ooabtn"
        Me.cmec_ooabtn.Size = New System.Drawing.Size(148, 23)
        Me.cmec_ooabtn.TabIndex = 40
        Me.cmec_ooabtn.Text = "CSA/CMEC/CMECS OOA"
        Me.cmec_ooabtn.UseVisualStyleBackColor = True
        '
        'cmecbtn
        '
        Me.cmecbtn.Location = New System.Drawing.Point(162, 479)
        Me.cmecbtn.Name = "cmecbtn"
        Me.cmecbtn.Size = New System.Drawing.Size(137, 23)
        Me.cmecbtn.TabIndex = 39
        Me.cmecbtn.Text = "CSA/CMEC/CMECS"
        Me.cmecbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(173, 638)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(287, 23)
        Me.ProgressBar1.TabIndex = 36
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(34, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "COLLECT"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(34, 449)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 13)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "ENFORCEMENT"
        '
        'bail_retnbtn
        '
        Me.bail_retnbtn.Location = New System.Drawing.Point(162, 575)
        Me.bail_retnbtn.Name = "bail_retnbtn"
        Me.bail_retnbtn.Size = New System.Drawing.Size(122, 36)
        Me.bail_retnbtn.TabIndex = 48
        Me.bail_retnbtn.Text = "Return Stage         (Not CMEC)"
        Me.bail_retnbtn.UseVisualStyleBackColor = True
        '
        'hmrcbtn
        '
        Me.hmrcbtn.Location = New System.Drawing.Point(37, 149)
        Me.hmrcbtn.Name = "hmrcbtn"
        Me.hmrcbtn.Size = New System.Drawing.Size(95, 39)
        Me.hmrcbtn.TabIndex = 14
        Me.hmrcbtn.Text = "Indesser Broken"
        Me.hmrcbtn.UseVisualStyleBackColor = True
        '
        'feesbtn
        '
        Me.feesbtn.Location = New System.Drawing.Point(12, 479)
        Me.feesbtn.Name = "feesbtn"
        Me.feesbtn.Size = New System.Drawing.Size(126, 23)
        Me.feesbtn.TabIndex = 38
        Me.feesbtn.Text = "Fees only Branch 1"
        Me.feesbtn.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(864, 36)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(93, 13)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "LAA CAMPAIGNS"
        '
        'lsc_camp1btn
        '
        Me.lsc_camp1btn.Location = New System.Drawing.Point(845, 105)
        Me.lsc_camp1btn.Name = "lsc_camp1btn"
        Me.lsc_camp1btn.Size = New System.Drawing.Size(143, 23)
        Me.lsc_camp1btn.TabIndex = 10
        Me.lsc_camp1btn.Text = "Payment due 5-7 days"
        Me.lsc_camp1btn.UseVisualStyleBackColor = True
        '
        'lsc_camp2btn
        '
        Me.lsc_camp2btn.Location = New System.Drawing.Point(845, 64)
        Me.lsc_camp2btn.Name = "lsc_camp2btn"
        Me.lsc_camp2btn.Size = New System.Drawing.Size(143, 23)
        Me.lsc_camp2btn.TabIndex = 9
        Me.lsc_camp2btn.Text = "Payment due today"
        Me.lsc_camp2btn.UseVisualStyleBackColor = True
        Me.lsc_camp2btn.Visible = False
        '
        'lsc_defaultedbtn
        '
        Me.lsc_defaultedbtn.Location = New System.Drawing.Point(845, 149)
        Me.lsc_defaultedbtn.Name = "lsc_defaultedbtn"
        Me.lsc_defaultedbtn.Size = New System.Drawing.Size(143, 23)
        Me.lsc_defaultedbtn.TabIndex = 11
        Me.lsc_defaultedbtn.Text = "Pre-Trial Defaulted"
        Me.lsc_defaultedbtn.UseVisualStyleBackColor = True
        '
        'lsc_fdcbtn
        '
        Me.lsc_fdcbtn.Location = New System.Drawing.Point(845, 196)
        Me.lsc_fdcbtn.Name = "lsc_fdcbtn"
        Me.lsc_fdcbtn.Size = New System.Drawing.Size(143, 23)
        Me.lsc_fdcbtn.TabIndex = 12
        Me.lsc_fdcbtn.Text = "FDC Default"
        Me.lsc_fdcbtn.UseVisualStyleBackColor = True
        '
        'lsc_crystalbtn
        '
        Me.lsc_crystalbtn.Location = New System.Drawing.Point(845, 239)
        Me.lsc_crystalbtn.Name = "lsc_crystalbtn"
        Me.lsc_crystalbtn.Size = New System.Drawing.Size(143, 23)
        Me.lsc_crystalbtn.TabIndex = 13
        Me.lsc_crystalbtn.Text = "Crystallised cases"
        Me.lsc_crystalbtn.UseVisualStyleBackColor = True
        '
        'rtn_not_cmecbtn
        '
        Me.rtn_not_cmecbtn.Location = New System.Drawing.Point(320, 575)
        Me.rtn_not_cmecbtn.Name = "rtn_not_cmecbtn"
        Me.rtn_not_cmecbtn.Size = New System.Drawing.Size(122, 23)
        Me.rtn_not_cmecbtn.TabIndex = 49
        Me.rtn_not_cmecbtn.Text = "Return Stage (CMEC)"
        Me.rtn_not_cmecbtn.UseVisualStyleBackColor = True
        '
        'cco_btn
        '
        Me.cco_btn.Location = New System.Drawing.Point(845, 278)
        Me.cco_btn.Name = "cco_btn"
        Me.cco_btn.Size = New System.Drawing.Size(143, 23)
        Me.cco_btn.TabIndex = 27
        Me.cco_btn.Text = "CCO due"
        Me.cco_btn.UseVisualStyleBackColor = True
        '
        'arrow_sch_grpbtn
        '
        Me.arrow_sch_grpbtn.Location = New System.Drawing.Point(11, 58)
        Me.arrow_sch_grpbtn.Name = "arrow_sch_grpbtn"
        Me.arrow_sch_grpbtn.Size = New System.Drawing.Size(109, 39)
        Me.arrow_sch_grpbtn.TabIndex = 15
        Me.arrow_sch_grpbtn.Text = "Arrow Scheme Groups"
        Me.arrow_sch_grpbtn.UseVisualStyleBackColor = True
        '
        'arrow_sch_cbox
        '
        Me.arrow_sch_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.arrow_sch_cbox.Enabled = False
        Me.arrow_sch_cbox.FormattingEnabled = True
        Me.arrow_sch_cbox.Items.AddRange(New Object() {"Barclaycard", "FS", "MBNA", "Mid Val", "Mixed", "PC", "Shop", "Telco", "Terbo"})
        Me.arrow_sch_cbox.Location = New System.Drawing.Point(108, 104)
        Me.arrow_sch_cbox.Name = "arrow_sch_cbox"
        Me.arrow_sch_cbox.Size = New System.Drawing.Size(137, 21)
        Me.arrow_sch_cbox.TabIndex = 16
        '
        'sch_grplbl
        '
        Me.sch_grplbl.AutoSize = True
        Me.sch_grplbl.Location = New System.Drawing.Point(126, 79)
        Me.sch_grplbl.Name = "sch_grplbl"
        Me.sch_grplbl.Size = New System.Drawing.Size(107, 13)
        Me.sch_grplbl.TabIndex = 34
        Me.sch_grplbl.Text = "Select scheme group"
        Me.sch_grplbl.Visible = False
        '
        'cpw_schbtn
        '
        Me.cpw_schbtn.Location = New System.Drawing.Point(35, 141)
        Me.cpw_schbtn.Name = "cpw_schbtn"
        Me.cpw_schbtn.Size = New System.Drawing.Size(124, 39)
        Me.cpw_schbtn.TabIndex = 17
        Me.cpw_schbtn.Text = "CPW Scheme Groups"
        Me.cpw_schbtn.UseVisualStyleBackColor = True
        '
        'cpw_schlbl
        '
        Me.cpw_schlbl.AutoSize = True
        Me.cpw_schlbl.Location = New System.Drawing.Point(65, 192)
        Me.cpw_schlbl.Name = "cpw_schlbl"
        Me.cpw_schlbl.Size = New System.Drawing.Size(107, 13)
        Me.cpw_schlbl.TabIndex = 36
        Me.cpw_schlbl.Text = "Select scheme group"
        Me.cpw_schlbl.Visible = False
        '
        'cpw_sch_cbox
        '
        Me.cpw_sch_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cpw_sch_cbox.Enabled = False
        Me.cpw_sch_cbox.FormattingEnabled = True
        Me.cpw_sch_cbox.Items.AddRange(New Object() {"CPW", "Mixed", "Telco", "Terbo", "PC"})
        Me.cpw_sch_cbox.Location = New System.Drawing.Point(68, 208)
        Me.cpw_sch_cbox.Name = "cpw_sch_cbox"
        Me.cpw_sch_cbox.Size = New System.Drawing.Size(137, 21)
        Me.cpw_sch_cbox.TabIndex = 12
        '
        'outcallbtn
        '
        Me.outcallbtn.Location = New System.Drawing.Point(876, 387)
        Me.outcallbtn.Name = "outcallbtn"
        Me.outcallbtn.Size = New System.Drawing.Size(143, 23)
        Me.outcallbtn.TabIndex = 37
        Me.outcallbtn.Text = "Cases to Outcall"
        Me.outcallbtn.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(913, 364)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(75, 13)
        Me.Label5.TabIndex = 37
        Me.Label5.Text = "HIGH COURT"
        '
        'vulnerbtn
        '
        Me.vulnerbtn.Location = New System.Drawing.Point(161, 210)
        Me.vulnerbtn.Name = "vulnerbtn"
        Me.vulnerbtn.Size = New System.Drawing.Size(100, 45)
        Me.vulnerbtn.TabIndex = 22
        Me.vulnerbtn.Text = "Vulnerable Collect/HMRC"
        Me.vulnerbtn.UseVisualStyleBackColor = True
        '
        'UUBtn
        '
        Me.UUBtn.Location = New System.Drawing.Point(162, 149)
        Me.UUBtn.Name = "UUBtn"
        Me.UUBtn.Size = New System.Drawing.Size(99, 45)
        Me.UUBtn.TabIndex = 20
        Me.UUBtn.Text = "United Utilities"
        Me.UUBtn.UseVisualStyleBackColor = True
        '
        'UUCbox
        '
        Me.UUCbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.UUCbox.FormattingEnabled = True
        Me.UUCbox.Items.AddRange(New Object() {"Domestic", "Commercial", "Other"})
        Me.UUCbox.Location = New System.Drawing.Point(275, 173)
        Me.UUCbox.Name = "UUCbox"
        Me.UUCbox.Size = New System.Drawing.Size(121, 21)
        Me.UUCbox.TabIndex = 40
        Me.UUCbox.Visible = False
        '
        'UUlbl
        '
        Me.UUlbl.AutoSize = True
        Me.UUlbl.Location = New System.Drawing.Point(289, 154)
        Me.UUlbl.Name = "UUlbl"
        Me.UUlbl.Size = New System.Drawing.Size(107, 13)
        Me.UUlbl.TabIndex = 41
        Me.UUlbl.Text = "Select scheme group"
        Me.UUlbl.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(35, 423)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(179, 13)
        Me.Label6.TabIndex = 42
        Me.Label6.Text = "TRANSFERRED FROM MARSTON"
        '
        'tma_rbtn
        '
        Me.tma_rbtn.Location = New System.Drawing.Point(275, 418)
        Me.tma_rbtn.Name = "tma_rbtn"
        Me.tma_rbtn.Size = New System.Drawing.Size(75, 23)
        Me.tma_rbtn.TabIndex = 35
        Me.tma_rbtn.Text = "TMA"
        Me.tma_rbtn.UseVisualStyleBackColor = True
        '
        'hbop_mbtn
        '
        Me.hbop_mbtn.Location = New System.Drawing.Point(381, 418)
        Me.hbop_mbtn.Name = "hbop_mbtn"
        Me.hbop_mbtn.Size = New System.Drawing.Size(87, 23)
        Me.hbop_mbtn.TabIndex = 36
        Me.hbop_mbtn.Text = "HBOP - M"
        Me.hbop_mbtn.UseVisualStyleBackColor = True
        '
        'tmabtn
        '
        Me.tmabtn.Location = New System.Drawing.Point(432, 149)
        Me.tmabtn.Name = "tmabtn"
        Me.tmabtn.Size = New System.Drawing.Size(110, 45)
        Me.tmabtn.TabIndex = 25
        Me.tmabtn.Text = "TMA Schemes"
        Me.tmabtn.UseVisualStyleBackColor = True
        '
        'hmrcagebtn
        '
        Me.hmrcagebtn.Location = New System.Drawing.Point(37, 210)
        Me.hmrcagebtn.Name = "hmrcagebtn"
        Me.hmrcagebtn.Size = New System.Drawing.Size(95, 44)
        Me.hmrcagebtn.TabIndex = 18
        Me.hmrcagebtn.Text = "HMRC by Case Age"
        Me.hmrcagebtn.UseVisualStyleBackColor = True
        '
        'precom1btn
        '
        Me.precom1btn.Location = New System.Drawing.Point(12, 523)
        Me.precom1btn.Name = "precom1btn"
        Me.precom1btn.Size = New System.Drawing.Size(126, 23)
        Me.precom1btn.TabIndex = 42
        Me.precom1btn.Text = "Pre Committal Letter 1"
        Me.precom1btn.UseVisualStyleBackColor = True
        '
        'precom2btn
        '
        Me.precom2btn.Location = New System.Drawing.Point(162, 523)
        Me.precom2btn.Name = "precom2btn"
        Me.precom2btn.Size = New System.Drawing.Size(126, 23)
        Me.precom2btn.TabIndex = 43
        Me.precom2btn.Text = "Pre Committal Letter 2"
        Me.precom2btn.UseVisualStyleBackColor = True
        '
        'fvanbtn
        '
        Me.fvanbtn.Location = New System.Drawing.Point(320, 523)
        Me.fvanbtn.Name = "fvanbtn"
        Me.fvanbtn.Size = New System.Drawing.Size(140, 23)
        Me.fvanbtn.TabIndex = 44
        Me.fvanbtn.Text = "Further Van (excl RTD)"
        Me.fvanbtn.UseVisualStyleBackColor = True
        '
        'rtdfvanbtn
        '
        Me.rtdfvanbtn.Location = New System.Drawing.Point(12, 575)
        Me.rtdfvanbtn.Name = "rtdfvanbtn"
        Me.rtdfvanbtn.Size = New System.Drawing.Size(126, 23)
        Me.rtdfvanbtn.TabIndex = 47
        Me.rtdfvanbtn.Text = "Further Van (RTD)"
        Me.rtdfvanbtn.UseVisualStyleBackColor = True
        '
        'fsbtn
        '
        Me.fsbtn.Location = New System.Drawing.Point(73, 192)
        Me.fsbtn.Name = "fsbtn"
        Me.fsbtn.Size = New System.Drawing.Size(86, 44)
        Me.fsbtn.TabIndex = 23
        Me.fsbtn.Text = "Financial Services"
        Me.fsbtn.UseVisualStyleBackColor = True
        '
        'dvlabtn
        '
        Me.dvlabtn.Location = New System.Drawing.Point(73, 17)
        Me.dvlabtn.Name = "dvlabtn"
        Me.dvlabtn.Size = New System.Drawing.Size(86, 44)
        Me.dvlabtn.TabIndex = 21
        Me.dvlabtn.Text = "DVLA by Case Age"
        Me.dvlabtn.UseVisualStyleBackColor = True
        '
        'arrow_by_agebtn
        '
        Me.arrow_by_agebtn.Location = New System.Drawing.Point(68, 9)
        Me.arrow_by_agebtn.Name = "arrow_by_agebtn"
        Me.arrow_by_agebtn.Size = New System.Drawing.Size(109, 39)
        Me.arrow_by_agebtn.TabIndex = 19
        Me.arrow_by_agebtn.Text = "Arrow by Case Age"
        Me.arrow_by_agebtn.UseVisualStyleBackColor = True
        '
        'lowellbtn
        '
        Me.lowellbtn.Location = New System.Drawing.Point(38, 288)
        Me.lowellbtn.Name = "lowellbtn"
        Me.lowellbtn.Size = New System.Drawing.Size(94, 44)
        Me.lowellbtn.TabIndex = 24
        Me.lowellbtn.Text = "Lowell by Case Age"
        Me.lowellbtn.UseVisualStyleBackColor = True
        '
        'dvlabrokenbtn
        '
        Me.dvlabrokenbtn.Location = New System.Drawing.Point(73, 81)
        Me.dvlabrokenbtn.Name = "dvlabrokenbtn"
        Me.dvlabrokenbtn.Size = New System.Drawing.Size(86, 44)
        Me.dvlabrokenbtn.TabIndex = 28
        Me.dvlabrokenbtn.Text = "DVLA Broken Arrangements"
        Me.dvlabrokenbtn.UseVisualStyleBackColor = True
        '
        'slbtn
        '
        Me.slbtn.Location = New System.Drawing.Point(38, 348)
        Me.slbtn.Name = "slbtn"
        Me.slbtn.Size = New System.Drawing.Size(86, 44)
        Me.slbtn.TabIndex = 29
        Me.slbtn.Text = "Student Loans"
        Me.slbtn.UseVisualStyleBackColor = True
        '
        'etcbtn
        '
        Me.etcbtn.Location = New System.Drawing.Point(73, 138)
        Me.etcbtn.Name = "etcbtn"
        Me.etcbtn.Size = New System.Drawing.Size(86, 44)
        Me.etcbtn.TabIndex = 30
        Me.etcbtn.Text = "HMRC ETC"
        Me.etcbtn.UseVisualStyleBackColor = True
        '
        'awbtn
        '
        Me.awbtn.Location = New System.Drawing.Point(432, 211)
        Me.awbtn.Name = "awbtn"
        Me.awbtn.Size = New System.Drawing.Size(86, 44)
        Me.awbtn.TabIndex = 33
        Me.awbtn.Text = "Anglian Water by Age"
        Me.awbtn.UseVisualStyleBackColor = True
        '
        'etc_brokenbtn
        '
        Me.etc_brokenbtn.Location = New System.Drawing.Point(275, 288)
        Me.etc_brokenbtn.Name = "etc_brokenbtn"
        Me.etc_brokenbtn.Size = New System.Drawing.Size(108, 44)
        Me.etc_brokenbtn.TabIndex = 32
        Me.etc_brokenbtn.Text = "HMRC ETC Broken Arrangements"
        Me.etc_brokenbtn.UseVisualStyleBackColor = True
        '
        'listbtn
        '
        Me.listbtn.Location = New System.Drawing.Point(433, 90)
        Me.listbtn.Name = "listbtn"
        Me.listbtn.Size = New System.Drawing.Size(109, 34)
        Me.listbtn.TabIndex = 4
        Me.listbtn.Text = "Select by Onestep ListID"
        Me.listbtn.UseVisualStyleBackColor = True
        '
        'segmentbtn
        '
        Me.segmentbtn.Location = New System.Drawing.Point(161, 288)
        Me.segmentbtn.Name = "segmentbtn"
        Me.segmentbtn.Size = New System.Drawing.Size(86, 44)
        Me.segmentbtn.TabIndex = 31
        Me.segmentbtn.Text = "HMRC ETC by Segment"
        Me.segmentbtn.UseVisualStyleBackColor = True
        '
        'appealbtn
        '
        Me.appealbtn.Location = New System.Drawing.Point(845, 317)
        Me.appealbtn.Name = "appealbtn"
        Me.appealbtn.Size = New System.Drawing.Size(143, 23)
        Me.appealbtn.TabIndex = 34
        Me.appealbtn.Text = "Appeals"
        Me.appealbtn.UseVisualStyleBackColor = True
        '
        'compbtn7
        '
        Me.compbtn7.Location = New System.Drawing.Point(6, 13)
        Me.compbtn7.Name = "compbtn7"
        Me.compbtn7.Size = New System.Drawing.Size(134, 23)
        Me.compbtn7.TabIndex = 41
        Me.compbtn7.Text = "Compliance Letter"
        Me.compbtn7.UseVisualStyleBackColor = True
        '
        'remindbtn
        '
        Me.remindbtn.Location = New System.Drawing.Point(146, 13)
        Me.remindbtn.Name = "remindbtn"
        Me.remindbtn.Size = New System.Drawing.Size(134, 23)
        Me.remindbtn.TabIndex = 45
        Me.remindbtn.Text = "Reminder Letter"
        Me.remindbtn.UseVisualStyleBackColor = True
        '
        'remind23btn
        '
        Me.remind23btn.Location = New System.Drawing.Point(6, 48)
        Me.remind23btn.Name = "remind23btn"
        Me.remind23btn.Size = New System.Drawing.Size(134, 23)
        Me.remind23btn.TabIndex = 46
        Me.remind23btn.Text = "Final Warning Letter"
        Me.remind23btn.UseVisualStyleBackColor = True
        '
        'precomL1btn
        '
        Me.precomL1btn.Location = New System.Drawing.Point(146, 126)
        Me.precomL1btn.Name = "precomL1btn"
        Me.precomL1btn.Size = New System.Drawing.Size(134, 23)
        Me.precomL1btn.TabIndex = 51
        Me.precomL1btn.Text = "Pre comm Letter 1"
        Me.precomL1btn.UseVisualStyleBackColor = True
        '
        'retnbtn
        '
        Me.retnbtn.Location = New System.Drawing.Point(146, 164)
        Me.retnbtn.Name = "retnbtn"
        Me.retnbtn.Size = New System.Drawing.Size(134, 23)
        Me.retnbtn.TabIndex = 52
        Me.retnbtn.Text = "Returns"
        Me.retnbtn.UseVisualStyleBackColor = True
        '
        'hmrc_batchbtn
        '
        Me.hmrc_batchbtn.Location = New System.Drawing.Point(432, 278)
        Me.hmrc_batchbtn.Name = "hmrc_batchbtn"
        Me.hmrc_batchbtn.Size = New System.Drawing.Size(86, 44)
        Me.hmrc_batchbtn.TabIndex = 26
        Me.hmrc_batchbtn.Text = "HMRC by Batch"
        Me.hmrc_batchbtn.UseVisualStyleBackColor = True
        '
        'finrembtn
        '
        Me.finrembtn.Location = New System.Drawing.Point(6, 91)
        Me.finrembtn.Name = "finrembtn"
        Me.finrembtn.Size = New System.Drawing.Size(134, 23)
        Me.finrembtn.TabIndex = 55
        Me.finrembtn.Text = "Final Reminder"
        Me.finrembtn.UseVisualStyleBackColor = True
        '
        'finenfbtn
        '
        Me.finenfbtn.Location = New System.Drawing.Point(6, 126)
        Me.finenfbtn.Name = "finenfbtn"
        Me.finenfbtn.Size = New System.Drawing.Size(134, 23)
        Me.finenfbtn.TabIndex = 56
        Me.finenfbtn.Text = "Final Enforcement"
        Me.finenfbtn.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage5)
        Me.TabControl1.Controls.Add(Me.TabPage6)
        Me.TabControl1.Controls.Add(Me.TabPage7)
        Me.TabControl1.Controls.Add(Me.TabPage8)
        Me.TabControl1.Controls.Add(Me.UU)
        Me.TabControl1.Controls.Add(Me.TabPage9)
        Me.TabControl1.Controls.Add(Me.TabPage10)
        Me.TabControl1.Controls.Add(Me.TabPage11)
        Me.TabControl1.Controls.Add(Me.TabPage12)
        Me.TabControl1.Location = New System.Drawing.Point(474, 428)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(574, 221)
        Me.TabControl1.TabIndex = 58
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.br1_postenfbtn)
        Me.TabPage1.Controls.Add(Me.br1_enfbtn)
        Me.TabPage1.Controls.Add(Me.br1_compbtn)
        Me.TabPage1.Controls.Add(Me.RNNDRBtn)
        Me.TabPage1.Controls.Add(Me.exceptionbtn)
        Me.TabPage1.Controls.Add(Me.stagecbox)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.furtherenfbtn)
        Me.TabPage1.Controls.Add(Me.precommL2btn)
        Me.TabPage1.Controls.Add(Me.enfletterbtn)
        Me.TabPage1.Controls.Add(Me.retnbtn)
        Me.TabPage1.Controls.Add(Me.finenfbtn)
        Me.TabPage1.Controls.Add(Me.precomL1btn)
        Me.TabPage1.Controls.Add(Me.compbtn7)
        Me.TabPage1.Controls.Add(Me.remindbtn)
        Me.TabPage1.Controls.Add(Me.remind23btn)
        Me.TabPage1.Controls.Add(Me.finrembtn)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(566, 195)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Branch 1"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'br1_postenfbtn
        '
        Me.br1_postenfbtn.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.br1_postenfbtn.Location = New System.Drawing.Point(426, 142)
        Me.br1_postenfbtn.Name = "br1_postenfbtn"
        Me.br1_postenfbtn.Size = New System.Drawing.Size(134, 45)
        Me.br1_postenfbtn.TabIndex = 88
        Me.br1_postenfbtn.Text = "Post Enforcement Stages"
        Me.br1_postenfbtn.UseVisualStyleBackColor = True
        '
        'br1_enfbtn
        '
        Me.br1_enfbtn.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.br1_enfbtn.Location = New System.Drawing.Point(429, 102)
        Me.br1_enfbtn.Name = "br1_enfbtn"
        Me.br1_enfbtn.Size = New System.Drawing.Size(134, 23)
        Me.br1_enfbtn.TabIndex = 87
        Me.br1_enfbtn.Text = "Enforcement Stages"
        Me.br1_enfbtn.UseVisualStyleBackColor = True
        '
        'br1_compbtn
        '
        Me.br1_compbtn.Location = New System.Drawing.Point(426, 53)
        Me.br1_compbtn.Name = "br1_compbtn"
        Me.br1_compbtn.Size = New System.Drawing.Size(134, 23)
        Me.br1_compbtn.TabIndex = 86
        Me.br1_compbtn.Text = "Compliance Stages"
        Me.br1_compbtn.UseVisualStyleBackColor = True
        '
        'RNNDRBtn
        '
        Me.RNNDRBtn.Location = New System.Drawing.Point(286, 13)
        Me.RNNDRBtn.Name = "RNNDRBtn"
        Me.RNNDRBtn.Size = New System.Drawing.Size(134, 23)
        Me.RNNDRBtn.TabIndex = 73
        Me.RNNDRBtn.Text = "NNDR"
        Me.RNNDRBtn.UseVisualStyleBackColor = True
        '
        'exceptionbtn
        '
        Me.exceptionbtn.Location = New System.Drawing.Point(286, 102)
        Me.exceptionbtn.Name = "exceptionbtn"
        Me.exceptionbtn.Size = New System.Drawing.Size(134, 23)
        Me.exceptionbtn.TabIndex = 68
        Me.exceptionbtn.Text = "Run Exception Report"
        Me.exceptionbtn.UseVisualStyleBackColor = True
        '
        'stagecbox
        '
        Me.stagecbox.FormattingEnabled = True
        Me.stagecbox.Items.AddRange(New Object() {"Compliance Letter", "Reminder Letter", "Final Warning Letter", "Enforcement Letter", "Final Reminder", "Further Enforcement", "Final Enforcement", "Pre Com 1", "Pre Com 2", "Returns"})
        Me.stagecbox.Location = New System.Drawing.Point(286, 75)
        Me.stagecbox.Name = "stagecbox"
        Me.stagecbox.Size = New System.Drawing.Size(121, 21)
        Me.stagecbox.TabIndex = 67
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(303, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 13)
        Me.Label4.TabIndex = 66
        Me.Label4.Text = "Choose Stage"
        '
        'furtherenfbtn
        '
        Me.furtherenfbtn.Location = New System.Drawing.Point(146, 91)
        Me.furtherenfbtn.Name = "furtherenfbtn"
        Me.furtherenfbtn.Size = New System.Drawing.Size(134, 23)
        Me.furtherenfbtn.TabIndex = 65
        Me.furtherenfbtn.Text = "Further Enforcement"
        Me.furtherenfbtn.UseVisualStyleBackColor = True
        '
        'precommL2btn
        '
        Me.precommL2btn.Location = New System.Drawing.Point(6, 166)
        Me.precommL2btn.Name = "precommL2btn"
        Me.precommL2btn.Size = New System.Drawing.Size(134, 23)
        Me.precommL2btn.TabIndex = 58
        Me.precommL2btn.Text = "Pre comm Letter 2"
        Me.precommL2btn.UseVisualStyleBackColor = True
        '
        'enfletterbtn
        '
        Me.enfletterbtn.Location = New System.Drawing.Point(146, 48)
        Me.enfletterbtn.Name = "enfletterbtn"
        Me.enfletterbtn.Size = New System.Drawing.Size(134, 23)
        Me.enfletterbtn.TabIndex = 57
        Me.enfletterbtn.Text = "Enforcement Letter"
        Me.enfletterbtn.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.mar_postenfbtn)
        Me.TabPage2.Controls.Add(Me.mar_enfbtn)
        Me.TabPage2.Controls.Add(Me.mar_compbtn)
        Me.TabPage2.Controls.Add(Me.MNNdrBtn)
        Me.TabPage2.Controls.Add(Me.Mexceptionbtn)
        Me.TabPage2.Controls.Add(Me.Mstagecbox)
        Me.TabPage2.Controls.Add(Me.Label7)
        Me.TabPage2.Controls.Add(Me.Mfurtherenfbtn)
        Me.TabPage2.Controls.Add(Me.Mretnbtn)
        Me.TabPage2.Controls.Add(Me.MprecommL2)
        Me.TabPage2.Controls.Add(Me.MpcommL1btn)
        Me.TabPage2.Controls.Add(Me.Mfinenfbtn)
        Me.TabPage2.Controls.Add(Me.MfinRemindbtn)
        Me.TabPage2.Controls.Add(Me.MenfLetterbtn)
        Me.TabPage2.Controls.Add(Me.MfinalWbtn)
        Me.TabPage2.Controls.Add(Me.Mremindbtn)
        Me.TabPage2.Controls.Add(Me.Mcompbtn)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(566, 195)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Marston"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'mar_postenfbtn
        '
        Me.mar_postenfbtn.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.mar_postenfbtn.Location = New System.Drawing.Point(426, 121)
        Me.mar_postenfbtn.Name = "mar_postenfbtn"
        Me.mar_postenfbtn.Size = New System.Drawing.Size(134, 45)
        Me.mar_postenfbtn.TabIndex = 87
        Me.mar_postenfbtn.Text = "Post Enforcement Stages"
        Me.mar_postenfbtn.UseVisualStyleBackColor = True
        '
        'mar_enfbtn
        '
        Me.mar_enfbtn.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.mar_enfbtn.Location = New System.Drawing.Point(432, 89)
        Me.mar_enfbtn.Name = "mar_enfbtn"
        Me.mar_enfbtn.Size = New System.Drawing.Size(134, 23)
        Me.mar_enfbtn.TabIndex = 86
        Me.mar_enfbtn.Text = "Enforcement Stages"
        Me.mar_enfbtn.UseVisualStyleBackColor = True
        '
        'mar_compbtn
        '
        Me.mar_compbtn.Location = New System.Drawing.Point(432, 47)
        Me.mar_compbtn.Name = "mar_compbtn"
        Me.mar_compbtn.Size = New System.Drawing.Size(134, 23)
        Me.mar_compbtn.TabIndex = 85
        Me.mar_compbtn.Text = "Compliance Stages"
        Me.mar_compbtn.UseVisualStyleBackColor = True
        '
        'MNNdrBtn
        '
        Me.MNNdrBtn.Location = New System.Drawing.Point(283, 3)
        Me.MNNdrBtn.Name = "MNNdrBtn"
        Me.MNNdrBtn.Size = New System.Drawing.Size(134, 23)
        Me.MNNdrBtn.TabIndex = 72
        Me.MNNdrBtn.Text = "NNDR"
        Me.MNNdrBtn.UseVisualStyleBackColor = True
        '
        'Mexceptionbtn
        '
        Me.Mexceptionbtn.Location = New System.Drawing.Point(283, 89)
        Me.Mexceptionbtn.Name = "Mexceptionbtn"
        Me.Mexceptionbtn.Size = New System.Drawing.Size(134, 23)
        Me.Mexceptionbtn.TabIndex = 71
        Me.Mexceptionbtn.Text = "Run Exception Report"
        Me.Mexceptionbtn.UseVisualStyleBackColor = True
        '
        'Mstagecbox
        '
        Me.Mstagecbox.FormattingEnabled = True
        Me.Mstagecbox.Items.AddRange(New Object() {"Compliance Letter", "Reminder Letter", "Final Warning Letter", "Enforcement Letter", "Final Reminder", "Further Enforcement", "Final Enforcement", "Pre Com 1", "Pre Com 2", "Returns"})
        Me.Mstagecbox.Location = New System.Drawing.Point(283, 52)
        Me.Mstagecbox.Name = "Mstagecbox"
        Me.Mstagecbox.Size = New System.Drawing.Size(121, 21)
        Me.Mstagecbox.TabIndex = 70
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(302, 36)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(74, 13)
        Me.Label7.TabIndex = 69
        Me.Label7.Text = "Choose Stage"
        '
        'Mfurtherenfbtn
        '
        Me.Mfurtherenfbtn.Location = New System.Drawing.Point(143, 89)
        Me.Mfurtherenfbtn.Name = "Mfurtherenfbtn"
        Me.Mfurtherenfbtn.Size = New System.Drawing.Size(134, 23)
        Me.Mfurtherenfbtn.TabIndex = 66
        Me.Mfurtherenfbtn.Text = "Further Enforcement"
        Me.Mfurtherenfbtn.UseVisualStyleBackColor = True
        '
        'Mretnbtn
        '
        Me.Mretnbtn.Location = New System.Drawing.Point(145, 163)
        Me.Mretnbtn.Name = "Mretnbtn"
        Me.Mretnbtn.Size = New System.Drawing.Size(134, 23)
        Me.Mretnbtn.TabIndex = 65
        Me.Mretnbtn.Text = "Returns"
        Me.Mretnbtn.UseVisualStyleBackColor = True
        '
        'MprecommL2
        '
        Me.MprecommL2.Location = New System.Drawing.Point(3, 166)
        Me.MprecommL2.Name = "MprecommL2"
        Me.MprecommL2.Size = New System.Drawing.Size(134, 23)
        Me.MprecommL2.TabIndex = 62
        Me.MprecommL2.Text = "Pre comm Letter 2"
        Me.MprecommL2.UseVisualStyleBackColor = True
        '
        'MpcommL1btn
        '
        Me.MpcommL1btn.Location = New System.Drawing.Point(145, 126)
        Me.MpcommL1btn.Name = "MpcommL1btn"
        Me.MpcommL1btn.Size = New System.Drawing.Size(134, 23)
        Me.MpcommL1btn.TabIndex = 61
        Me.MpcommL1btn.Text = "Pre comm Letter 1"
        Me.MpcommL1btn.UseVisualStyleBackColor = True
        '
        'Mfinenfbtn
        '
        Me.Mfinenfbtn.Location = New System.Drawing.Point(3, 126)
        Me.Mfinenfbtn.Name = "Mfinenfbtn"
        Me.Mfinenfbtn.Size = New System.Drawing.Size(134, 23)
        Me.Mfinenfbtn.TabIndex = 60
        Me.Mfinenfbtn.Text = "Final Enforcement"
        Me.Mfinenfbtn.UseVisualStyleBackColor = True
        '
        'MfinRemindbtn
        '
        Me.MfinRemindbtn.Location = New System.Drawing.Point(3, 89)
        Me.MfinRemindbtn.Name = "MfinRemindbtn"
        Me.MfinRemindbtn.Size = New System.Drawing.Size(134, 23)
        Me.MfinRemindbtn.TabIndex = 59
        Me.MfinRemindbtn.Text = "Final Reminder"
        Me.MfinRemindbtn.UseVisualStyleBackColor = True
        '
        'MenfLetterbtn
        '
        Me.MenfLetterbtn.Location = New System.Drawing.Point(143, 47)
        Me.MenfLetterbtn.Name = "MenfLetterbtn"
        Me.MenfLetterbtn.Size = New System.Drawing.Size(134, 23)
        Me.MenfLetterbtn.TabIndex = 58
        Me.MenfLetterbtn.Text = "Enforcement Letter"
        Me.MenfLetterbtn.UseVisualStyleBackColor = True
        '
        'MfinalWbtn
        '
        Me.MfinalWbtn.Location = New System.Drawing.Point(3, 47)
        Me.MfinalWbtn.Name = "MfinalWbtn"
        Me.MfinalWbtn.Size = New System.Drawing.Size(134, 23)
        Me.MfinalWbtn.TabIndex = 47
        Me.MfinalWbtn.Text = "Final Warning Letter"
        Me.MfinalWbtn.UseVisualStyleBackColor = True
        '
        'Mremindbtn
        '
        Me.Mremindbtn.Location = New System.Drawing.Point(143, 3)
        Me.Mremindbtn.Name = "Mremindbtn"
        Me.Mremindbtn.Size = New System.Drawing.Size(134, 23)
        Me.Mremindbtn.TabIndex = 46
        Me.Mremindbtn.Text = "Reminder Letter"
        Me.Mremindbtn.UseVisualStyleBackColor = True
        '
        'Mcompbtn
        '
        Me.Mcompbtn.Location = New System.Drawing.Point(3, 3)
        Me.Mcompbtn.Name = "Mcompbtn"
        Me.Mcompbtn.Size = New System.Drawing.Size(134, 23)
        Me.Mcompbtn.TabIndex = 42
        Me.Mcompbtn.Text = "Compliance Letter"
        Me.Mcompbtn.UseVisualStyleBackColor = True
        '
        'TabPage5
        '
        Me.TabPage5.Controls.Add(Me.hmrcsw2btn)
        Me.TabPage5.Controls.Add(Me.hmrcsw1btn)
        Me.TabPage5.Controls.Add(Me.hmrcl4btn)
        Me.TabPage5.Controls.Add(Me.hmrcl3btn)
        Me.TabPage5.Controls.Add(Me.hmrcl2btn)
        Me.TabPage5.Controls.Add(Me.hmrcintrobtn)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(566, 195)
        Me.TabPage5.TabIndex = 2
        Me.TabPage5.Text = "HMRC"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'hmrcsw2btn
        '
        Me.hmrcsw2btn.Location = New System.Drawing.Point(168, 143)
        Me.hmrcsw2btn.Name = "hmrcsw2btn"
        Me.hmrcsw2btn.Size = New System.Drawing.Size(134, 23)
        Me.hmrcsw2btn.TabIndex = 48
        Me.hmrcsw2btn.Text = "2nd sweep"
        Me.hmrcsw2btn.UseVisualStyleBackColor = True
        '
        'hmrcsw1btn
        '
        Me.hmrcsw1btn.Location = New System.Drawing.Point(6, 143)
        Me.hmrcsw1btn.Name = "hmrcsw1btn"
        Me.hmrcsw1btn.Size = New System.Drawing.Size(134, 23)
        Me.hmrcsw1btn.TabIndex = 47
        Me.hmrcsw1btn.Text = "1st sweep"
        Me.hmrcsw1btn.UseVisualStyleBackColor = True
        '
        'hmrcl4btn
        '
        Me.hmrcl4btn.Location = New System.Drawing.Point(88, 93)
        Me.hmrcl4btn.Name = "hmrcl4btn"
        Me.hmrcl4btn.Size = New System.Drawing.Size(134, 23)
        Me.hmrcl4btn.TabIndex = 46
        Me.hmrcl4btn.Text = "Letter 4"
        Me.hmrcl4btn.UseVisualStyleBackColor = True
        '
        'hmrcl3btn
        '
        Me.hmrcl3btn.Location = New System.Drawing.Point(168, 49)
        Me.hmrcl3btn.Name = "hmrcl3btn"
        Me.hmrcl3btn.Size = New System.Drawing.Size(134, 23)
        Me.hmrcl3btn.TabIndex = 45
        Me.hmrcl3btn.Text = "Letter 3"
        Me.hmrcl3btn.UseVisualStyleBackColor = True
        '
        'hmrcl2btn
        '
        Me.hmrcl2btn.Location = New System.Drawing.Point(6, 49)
        Me.hmrcl2btn.Name = "hmrcl2btn"
        Me.hmrcl2btn.Size = New System.Drawing.Size(134, 23)
        Me.hmrcl2btn.TabIndex = 44
        Me.hmrcl2btn.Text = "Letter 2"
        Me.hmrcl2btn.UseVisualStyleBackColor = True
        '
        'hmrcintrobtn
        '
        Me.hmrcintrobtn.Location = New System.Drawing.Point(88, 6)
        Me.hmrcintrobtn.Name = "hmrcintrobtn"
        Me.hmrcintrobtn.Size = New System.Drawing.Size(134, 23)
        Me.hmrcintrobtn.TabIndex = 43
        Me.hmrcintrobtn.Text = "Introduction Letter"
        Me.hmrcintrobtn.UseVisualStyleBackColor = True
        '
        'TabPage6
        '
        Me.TabPage6.Controls.Add(Me.slcs3btn)
        Me.TabPage6.Controls.Add(Me.slcs2letterbtn)
        Me.TabPage6.Controls.Add(Me.noasentbtn)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(566, 195)
        Me.TabPage6.TabIndex = 3
        Me.TabPage6.Text = "SLC IDS"
        Me.TabPage6.UseVisualStyleBackColor = True
        '
        'slcs3btn
        '
        Me.slcs3btn.Location = New System.Drawing.Point(77, 110)
        Me.slcs3btn.Name = "slcs3btn"
        Me.slcs3btn.Size = New System.Drawing.Size(134, 23)
        Me.slcs3btn.TabIndex = 46
        Me.slcs3btn.Text = "Stage 3 letter"
        Me.slcs3btn.UseVisualStyleBackColor = True
        '
        'slcs2letterbtn
        '
        Me.slcs2letterbtn.Location = New System.Drawing.Point(77, 67)
        Me.slcs2letterbtn.Name = "slcs2letterbtn"
        Me.slcs2letterbtn.Size = New System.Drawing.Size(134, 23)
        Me.slcs2letterbtn.TabIndex = 45
        Me.slcs2letterbtn.Text = "Stage 2 letter"
        Me.slcs2letterbtn.UseVisualStyleBackColor = True
        '
        'noasentbtn
        '
        Me.noasentbtn.Location = New System.Drawing.Point(77, 23)
        Me.noasentbtn.Name = "noasentbtn"
        Me.noasentbtn.Size = New System.Drawing.Size(134, 23)
        Me.noasentbtn.TabIndex = 44
        Me.noasentbtn.Text = "NOA Sent"
        Me.noasentbtn.UseVisualStyleBackColor = True
        '
        'TabPage7
        '
        Me.TabPage7.Controls.Add(Me.laa3btn)
        Me.TabPage7.Controls.Add(Me.laa2btn)
        Me.TabPage7.Controls.Add(Me.laal1btn)
        Me.TabPage7.Location = New System.Drawing.Point(4, 22)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage7.Size = New System.Drawing.Size(566, 195)
        Me.TabPage7.TabIndex = 4
        Me.TabPage7.Text = "LAA IDS"
        Me.TabPage7.UseVisualStyleBackColor = True
        '
        'laa3btn
        '
        Me.laa3btn.Location = New System.Drawing.Point(87, 132)
        Me.laa3btn.Name = "laa3btn"
        Me.laa3btn.Size = New System.Drawing.Size(134, 23)
        Me.laa3btn.TabIndex = 48
        Me.laa3btn.Text = "DMI Letter 3"
        Me.laa3btn.UseVisualStyleBackColor = True
        '
        'laa2btn
        '
        Me.laa2btn.Location = New System.Drawing.Point(87, 85)
        Me.laa2btn.Name = "laa2btn"
        Me.laa2btn.Size = New System.Drawing.Size(134, 23)
        Me.laa2btn.TabIndex = 47
        Me.laa2btn.Text = "DMI Letter 2"
        Me.laa2btn.UseVisualStyleBackColor = True
        '
        'laal1btn
        '
        Me.laal1btn.Location = New System.Drawing.Point(87, 34)
        Me.laal1btn.Name = "laal1btn"
        Me.laal1btn.Size = New System.Drawing.Size(134, 23)
        Me.laal1btn.TabIndex = 46
        Me.laal1btn.Text = "DMI Letter 1 "
        Me.laal1btn.UseVisualStyleBackColor = True
        '
        'TabPage8
        '
        Me.TabPage8.Controls.Add(Me.lowell90btn)
        Me.TabPage8.Controls.Add(Me.lowells3btn)
        Me.TabPage8.Controls.Add(Me.lowells2btn)
        Me.TabPage8.Controls.Add(Me.lowells1lbtn)
        Me.TabPage8.Location = New System.Drawing.Point(4, 22)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage8.Size = New System.Drawing.Size(566, 195)
        Me.TabPage8.TabIndex = 5
        Me.TabPage8.Text = "Lowell"
        Me.TabPage8.UseVisualStyleBackColor = True
        '
        'lowell90btn
        '
        Me.lowell90btn.Location = New System.Drawing.Point(87, 151)
        Me.lowell90btn.Name = "lowell90btn"
        Me.lowell90btn.Size = New System.Drawing.Size(134, 23)
        Me.lowell90btn.TabIndex = 50
        Me.lowell90btn.Text = "90 days Review"
        Me.lowell90btn.UseVisualStyleBackColor = True
        '
        'lowells3btn
        '
        Me.lowells3btn.Location = New System.Drawing.Point(87, 109)
        Me.lowells3btn.Name = "lowells3btn"
        Me.lowells3btn.Size = New System.Drawing.Size(134, 23)
        Me.lowells3btn.TabIndex = 49
        Me.lowells3btn.Text = "Stage Letter 3 Sent"
        Me.lowells3btn.UseVisualStyleBackColor = True
        '
        'lowells2btn
        '
        Me.lowells2btn.Location = New System.Drawing.Point(87, 67)
        Me.lowells2btn.Name = "lowells2btn"
        Me.lowells2btn.Size = New System.Drawing.Size(134, 23)
        Me.lowells2btn.TabIndex = 48
        Me.lowells2btn.Text = "Stage Letter 2 Sent"
        Me.lowells2btn.UseVisualStyleBackColor = True
        '
        'lowells1lbtn
        '
        Me.lowells1lbtn.Location = New System.Drawing.Point(87, 23)
        Me.lowells1lbtn.Name = "lowells1lbtn"
        Me.lowells1lbtn.Size = New System.Drawing.Size(134, 23)
        Me.lowells1lbtn.TabIndex = 47
        Me.lowells1lbtn.Text = "Stage Letter 1 Sent"
        Me.lowells1lbtn.UseVisualStyleBackColor = True
        '
        'UU
        '
        Me.UU.Controls.Add(Me.uusl4btn)
        Me.UU.Controls.Add(Me.uusl3btn)
        Me.UU.Controls.Add(Me.uusl2btn)
        Me.UU.Controls.Add(Me.uusl1btn)
        Me.UU.Location = New System.Drawing.Point(4, 22)
        Me.UU.Name = "UU"
        Me.UU.Padding = New System.Windows.Forms.Padding(3)
        Me.UU.Size = New System.Drawing.Size(566, 195)
        Me.UU.TabIndex = 6
        Me.UU.Text = "UU"
        Me.UU.UseVisualStyleBackColor = True
        '
        'uusl4btn
        '
        Me.uusl4btn.Location = New System.Drawing.Point(91, 149)
        Me.uusl4btn.Name = "uusl4btn"
        Me.uusl4btn.Size = New System.Drawing.Size(134, 23)
        Me.uusl4btn.TabIndex = 50
        Me.uusl4btn.Text = "Stage Letter 4"
        Me.uusl4btn.UseVisualStyleBackColor = True
        '
        'uusl3btn
        '
        Me.uusl3btn.Location = New System.Drawing.Point(91, 107)
        Me.uusl3btn.Name = "uusl3btn"
        Me.uusl3btn.Size = New System.Drawing.Size(134, 23)
        Me.uusl3btn.TabIndex = 49
        Me.uusl3btn.Text = "Stage Letter 3"
        Me.uusl3btn.UseVisualStyleBackColor = True
        '
        'uusl2btn
        '
        Me.uusl2btn.Location = New System.Drawing.Point(91, 67)
        Me.uusl2btn.Name = "uusl2btn"
        Me.uusl2btn.Size = New System.Drawing.Size(134, 23)
        Me.uusl2btn.TabIndex = 48
        Me.uusl2btn.Text = "Stage Letter 2"
        Me.uusl2btn.UseVisualStyleBackColor = True
        '
        'uusl1btn
        '
        Me.uusl1btn.Location = New System.Drawing.Point(91, 23)
        Me.uusl1btn.Name = "uusl1btn"
        Me.uusl1btn.Size = New System.Drawing.Size(134, 23)
        Me.uusl1btn.TabIndex = 47
        Me.uusl1btn.Text = "Stage Letter 1 "
        Me.uusl1btn.UseVisualStyleBackColor = True
        '
        'TabPage9
        '
        Me.TabPage9.Controls.Add(Me.adhocClientbtn)
        Me.TabPage9.Controls.Add(Me.southendbtn)
        Me.TabPage9.Controls.Add(Me.ret_pendingbtn)
        Me.TabPage9.Controls.Add(Me.allsdbtn)
        Me.TabPage9.Controls.Add(Me.allftabtn)
        Me.TabPage9.Controls.Add(Me.allhbopbtn)
        Me.TabPage9.Controls.Add(Me.adhoc90btn)
        Me.TabPage9.Controls.Add(Me.adhocbabtn)
        Me.TabPage9.Location = New System.Drawing.Point(4, 22)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage9.Size = New System.Drawing.Size(566, 195)
        Me.TabPage9.TabIndex = 7
        Me.TabPage9.Text = "Adhoc"
        Me.TabPage9.UseVisualStyleBackColor = True
        '
        'southendbtn
        '
        Me.southendbtn.Location = New System.Drawing.Point(372, 78)
        Me.southendbtn.Name = "southendbtn"
        Me.southendbtn.Size = New System.Drawing.Size(129, 23)
        Me.southendbtn.TabIndex = 54
        Me.southendbtn.Text = "Southend"
        Me.southendbtn.UseVisualStyleBackColor = True
        '
        'ret_pendingbtn
        '
        Me.ret_pendingbtn.Location = New System.Drawing.Point(372, 23)
        Me.ret_pendingbtn.Name = "ret_pendingbtn"
        Me.ret_pendingbtn.Size = New System.Drawing.Size(129, 23)
        Me.ret_pendingbtn.TabIndex = 53
        Me.ret_pendingbtn.Text = "Return Pending 1/2 10"
        Me.ret_pendingbtn.UseVisualStyleBackColor = True
        '
        'allsdbtn
        '
        Me.allsdbtn.Location = New System.Drawing.Point(244, 133)
        Me.allsdbtn.Name = "allsdbtn"
        Me.allsdbtn.Size = New System.Drawing.Size(100, 23)
        Me.allsdbtn.TabIndex = 52
        Me.allsdbtn.Text = "ALL Sundry Debt"
        Me.allsdbtn.UseVisualStyleBackColor = True
        '
        'allftabtn
        '
        Me.allftabtn.Location = New System.Drawing.Point(141, 133)
        Me.allftabtn.Name = "allftabtn"
        Me.allftabtn.Size = New System.Drawing.Size(75, 23)
        Me.allftabtn.TabIndex = 51
        Me.allftabtn.Text = "ALL FTA"
        Me.allftabtn.UseVisualStyleBackColor = True
        '
        'allhbopbtn
        '
        Me.allhbopbtn.Location = New System.Drawing.Point(29, 133)
        Me.allhbopbtn.Name = "allhbopbtn"
        Me.allhbopbtn.Size = New System.Drawing.Size(75, 23)
        Me.allhbopbtn.TabIndex = 50
        Me.allhbopbtn.Text = "ALL HBOP"
        Me.allhbopbtn.UseVisualStyleBackColor = True
        '
        'adhoc90btn
        '
        Me.adhoc90btn.Location = New System.Drawing.Point(80, 78)
        Me.adhoc90btn.Name = "adhoc90btn"
        Me.adhoc90btn.Size = New System.Drawing.Size(220, 23)
        Me.adhoc90btn.TabIndex = 49
        Me.adhoc90btn.Text = "HMRC/SLC IDS/LAA IDS 90 day review"
        Me.adhoc90btn.UseVisualStyleBackColor = True
        '
        'adhocbabtn
        '
        Me.adhocbabtn.Location = New System.Drawing.Point(69, 23)
        Me.adhocbabtn.Name = "adhocbabtn"
        Me.adhocbabtn.Size = New System.Drawing.Size(250, 23)
        Me.adhocbabtn.TabIndex = 48
        Me.adhocbabtn.Text = "HMRC/SLC IDS/LAA IDS Broken Arrangements"
        Me.adhocbabtn.UseVisualStyleBackColor = True
        '
        'TabPage10
        '
        Me.TabPage10.Controls.Add(Me.brokenbtn)
        Me.TabPage10.Controls.Add(Me.softtracebtn)
        Me.TabPage10.Controls.Add(Me.outboundbtn)
        Me.TabPage10.Controls.Add(Me.hello8plusbtn)
        Me.TabPage10.Controls.Add(Me.hello4to8btn)
        Me.TabPage10.Location = New System.Drawing.Point(4, 22)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage10.Size = New System.Drawing.Size(566, 195)
        Me.TabPage10.TabIndex = 8
        Me.TabPage10.Text = "Personal Calls"
        Me.TabPage10.UseVisualStyleBackColor = True
        '
        'brokenbtn
        '
        Me.brokenbtn.Location = New System.Drawing.Point(234, 120)
        Me.brokenbtn.Name = "brokenbtn"
        Me.brokenbtn.Size = New System.Drawing.Size(134, 23)
        Me.brokenbtn.TabIndex = 46
        Me.brokenbtn.Text = "Broken Arrangements"
        Me.brokenbtn.UseVisualStyleBackColor = True
        '
        'softtracebtn
        '
        Me.softtracebtn.Location = New System.Drawing.Point(64, 120)
        Me.softtracebtn.Name = "softtracebtn"
        Me.softtracebtn.Size = New System.Drawing.Size(134, 23)
        Me.softtracebtn.TabIndex = 45
        Me.softtracebtn.Text = "Soft traceLetter 4-8 days"
        Me.softtracebtn.UseVisualStyleBackColor = True
        '
        'outboundbtn
        '
        Me.outboundbtn.Location = New System.Drawing.Point(138, 68)
        Me.outboundbtn.Name = "outboundbtn"
        Me.outboundbtn.Size = New System.Drawing.Size(151, 23)
        Me.outboundbtn.TabIndex = 44
        Me.outboundbtn.Text = "Outbound Contact Strategy"
        Me.outboundbtn.UseVisualStyleBackColor = True
        '
        'hello8plusbtn
        '
        Me.hello8plusbtn.Location = New System.Drawing.Point(234, 24)
        Me.hello8plusbtn.Name = "hello8plusbtn"
        Me.hello8plusbtn.Size = New System.Drawing.Size(134, 23)
        Me.hello8plusbtn.TabIndex = 43
        Me.hello8plusbtn.Text = "Hello Letter 8+ days"
        Me.hello8plusbtn.UseVisualStyleBackColor = True
        '
        'hello4to8btn
        '
        Me.hello4to8btn.Location = New System.Drawing.Point(64, 24)
        Me.hello4to8btn.Name = "hello4to8btn"
        Me.hello4to8btn.Size = New System.Drawing.Size(134, 23)
        Me.hello4to8btn.TabIndex = 42
        Me.hello4to8btn.Text = "Hello Letter 4-8 days"
        Me.hello4to8btn.UseVisualStyleBackColor = True
        '
        'TabPage11
        '
        Me.TabPage11.Controls.Add(eonSetLetter)
        Me.TabPage11.Controls.Add(Me.eonS2)
        Me.TabPage11.Controls.Add(Me.EONS1B)
        Me.TabPage11.Controls.Add(Me.eonS1A)
        Me.TabPage11.Location = New System.Drawing.Point(4, 22)
        Me.TabPage11.Name = "TabPage11"
        Me.TabPage11.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage11.Size = New System.Drawing.Size(566, 195)
        Me.TabPage11.TabIndex = 9
        Me.TabPage11.Text = "EON"
        Me.TabPage11.UseVisualStyleBackColor = True
        '
        'eonS2
        '
        Me.eonS2.Location = New System.Drawing.Point(162, 83)
        Me.eonS2.Name = "eonS2"
        Me.eonS2.Size = New System.Drawing.Size(134, 23)
        Me.eonS2.TabIndex = 45
        Me.eonS2.Text = "Stage 2 Letter 3+ days"
        Me.eonS2.UseVisualStyleBackColor = True
        '
        'EONS1B
        '
        Me.EONS1B.Location = New System.Drawing.Point(254, 38)
        Me.EONS1B.Name = "EONS1B"
        Me.EONS1B.Size = New System.Drawing.Size(134, 23)
        Me.EONS1B.TabIndex = 44
        Me.EONS1B.Text = "Stage 1 Letter 7-11 days"
        Me.EONS1B.UseVisualStyleBackColor = True
        '
        'eonS1A
        '
        Me.eonS1A.Location = New System.Drawing.Point(68, 38)
        Me.eonS1A.Name = "eonS1A"
        Me.eonS1A.Size = New System.Drawing.Size(134, 23)
        Me.eonS1A.TabIndex = 43
        Me.eonS1A.Text = "Stage 1 Letter 3-6 days"
        Me.eonS1A.UseVisualStyleBackColor = True
        '
        'TabPage12
        '
        Me.TabPage12.Controls.Add(Me.sw_postenfbtn)
        Me.TabPage12.Controls.Add(Me.sw_enfbtn)
        Me.TabPage12.Controls.Add(Me.sw_compbtn)
        Me.TabPage12.Controls.Add(Me.SNNDRbtn)
        Me.TabPage12.Controls.Add(Me.swrem2btn)
        Me.TabPage12.Controls.Add(Me.swrunexcbtn)
        Me.TabPage12.Controls.Add(Me.swexcombo)
        Me.TabPage12.Controls.Add(Me.Label8)
        Me.TabPage12.Controls.Add(Me.swfenfbtn)
        Me.TabPage12.Controls.Add(Me.swpc2btn)
        Me.TabPage12.Controls.Add(Me.swenfbtn)
        Me.TabPage12.Controls.Add(Me.swretbtn)
        Me.TabPage12.Controls.Add(Me.swfebtn)
        Me.TabPage12.Controls.Add(Me.swpc1btn)
        Me.TabPage12.Controls.Add(Me.swcompbtn)
        Me.TabPage12.Controls.Add(Me.swrem1btn)
        Me.TabPage12.Controls.Add(Me.swfrbtn)
        Me.TabPage12.Location = New System.Drawing.Point(4, 22)
        Me.TabPage12.Name = "TabPage12"
        Me.TabPage12.Size = New System.Drawing.Size(566, 195)
        Me.TabPage12.TabIndex = 10
        Me.TabPage12.Text = "Swift"
        Me.TabPage12.UseVisualStyleBackColor = True
        '
        'sw_postenfbtn
        '
        Me.sw_postenfbtn.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.sw_postenfbtn.Location = New System.Drawing.Point(425, 125)
        Me.sw_postenfbtn.Name = "sw_postenfbtn"
        Me.sw_postenfbtn.Size = New System.Drawing.Size(134, 45)
        Me.sw_postenfbtn.TabIndex = 86
        Me.sw_postenfbtn.Text = "Post Enforcement Stages"
        Me.sw_postenfbtn.UseVisualStyleBackColor = True
        '
        'sw_enfbtn
        '
        Me.sw_enfbtn.Cursor = System.Windows.Forms.Cursors.SizeNWSE
        Me.sw_enfbtn.Location = New System.Drawing.Point(425, 87)
        Me.sw_enfbtn.Name = "sw_enfbtn"
        Me.sw_enfbtn.Size = New System.Drawing.Size(134, 23)
        Me.sw_enfbtn.TabIndex = 85
        Me.sw_enfbtn.Text = "Enforcement Stages"
        Me.sw_enfbtn.UseVisualStyleBackColor = True
        '
        'sw_compbtn
        '
        Me.sw_compbtn.Location = New System.Drawing.Point(423, 44)
        Me.sw_compbtn.Name = "sw_compbtn"
        Me.sw_compbtn.Size = New System.Drawing.Size(134, 23)
        Me.sw_compbtn.TabIndex = 84
        Me.sw_compbtn.Text = "Compliance Stages"
        Me.sw_compbtn.UseVisualStyleBackColor = True
        '
        'SNNDRbtn
        '
        Me.SNNDRbtn.Location = New System.Drawing.Point(285, 9)
        Me.SNNDRbtn.Name = "SNNDRbtn"
        Me.SNNDRbtn.Size = New System.Drawing.Size(134, 23)
        Me.SNNDRbtn.TabIndex = 83
        Me.SNNDRbtn.Text = "NNDR"
        Me.SNNDRbtn.UseVisualStyleBackColor = True
        '
        'swrem2btn
        '
        Me.swrem2btn.Location = New System.Drawing.Point(3, 44)
        Me.swrem2btn.Name = "swrem2btn"
        Me.swrem2btn.Size = New System.Drawing.Size(134, 23)
        Me.swrem2btn.TabIndex = 82
        Me.swrem2btn.Text = "Reminder Letter 2"
        Me.swrem2btn.UseVisualStyleBackColor = True
        '
        'swrunexcbtn
        '
        Me.swrunexcbtn.Location = New System.Drawing.Point(285, 87)
        Me.swrunexcbtn.Name = "swrunexcbtn"
        Me.swrunexcbtn.Size = New System.Drawing.Size(134, 23)
        Me.swrunexcbtn.TabIndex = 81
        Me.swrunexcbtn.Text = "Run Exception Report"
        Me.swrunexcbtn.UseVisualStyleBackColor = True
        '
        'swexcombo
        '
        Me.swexcombo.FormattingEnabled = True
        Me.swexcombo.Items.AddRange(New Object() {"Compliance Letter", "Reminder Letter 1", "Reminder Letter 2", "Final Warning Letter", "Enforcement letter", "Final Reminder", "Further Enforcement", "Final Enforcement", "Pre Com 1", "Pre Com 2", "Returns"})
        Me.swexcombo.Location = New System.Drawing.Point(285, 55)
        Me.swexcombo.Name = "swexcombo"
        Me.swexcombo.Size = New System.Drawing.Size(121, 21)
        Me.swexcombo.TabIndex = 80
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(329, 39)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(74, 13)
        Me.Label8.TabIndex = 79
        Me.Label8.Text = "Choose Stage"
        '
        'swfenfbtn
        '
        Me.swfenfbtn.Location = New System.Drawing.Point(143, 87)
        Me.swfenfbtn.Name = "swfenfbtn"
        Me.swfenfbtn.Size = New System.Drawing.Size(134, 23)
        Me.swfenfbtn.TabIndex = 78
        Me.swfenfbtn.Text = "Further Enforcement"
        Me.swfenfbtn.UseVisualStyleBackColor = True
        '
        'swpc2btn
        '
        Me.swpc2btn.Location = New System.Drawing.Point(3, 162)
        Me.swpc2btn.Name = "swpc2btn"
        Me.swpc2btn.Size = New System.Drawing.Size(134, 23)
        Me.swpc2btn.TabIndex = 77
        Me.swpc2btn.Text = "Pre comm Letter 2"
        Me.swpc2btn.UseVisualStyleBackColor = True
        '
        'swenfbtn
        '
        Me.swenfbtn.Location = New System.Drawing.Point(143, 44)
        Me.swenfbtn.Name = "swenfbtn"
        Me.swenfbtn.Size = New System.Drawing.Size(134, 23)
        Me.swenfbtn.TabIndex = 76
        Me.swenfbtn.Text = "Enforcement Letter"
        Me.swenfbtn.UseVisualStyleBackColor = True
        '
        'swretbtn
        '
        Me.swretbtn.Location = New System.Drawing.Point(143, 160)
        Me.swretbtn.Name = "swretbtn"
        Me.swretbtn.Size = New System.Drawing.Size(134, 23)
        Me.swretbtn.TabIndex = 73
        Me.swretbtn.Text = "Returns"
        Me.swretbtn.UseVisualStyleBackColor = True
        '
        'swfebtn
        '
        Me.swfebtn.Location = New System.Drawing.Point(3, 122)
        Me.swfebtn.Name = "swfebtn"
        Me.swfebtn.Size = New System.Drawing.Size(134, 23)
        Me.swfebtn.TabIndex = 75
        Me.swfebtn.Text = "Final Enforcement"
        Me.swfebtn.UseVisualStyleBackColor = True
        '
        'swpc1btn
        '
        Me.swpc1btn.Location = New System.Drawing.Point(143, 122)
        Me.swpc1btn.Name = "swpc1btn"
        Me.swpc1btn.Size = New System.Drawing.Size(134, 23)
        Me.swpc1btn.TabIndex = 72
        Me.swpc1btn.Text = "Pre comm Letter 1"
        Me.swpc1btn.UseVisualStyleBackColor = True
        '
        'swcompbtn
        '
        Me.swcompbtn.Location = New System.Drawing.Point(3, 9)
        Me.swcompbtn.Name = "swcompbtn"
        Me.swcompbtn.Size = New System.Drawing.Size(134, 23)
        Me.swcompbtn.TabIndex = 69
        Me.swcompbtn.Text = "Compliance Letter"
        Me.swcompbtn.UseVisualStyleBackColor = True
        '
        'swrem1btn
        '
        Me.swrem1btn.Location = New System.Drawing.Point(143, 9)
        Me.swrem1btn.Name = "swrem1btn"
        Me.swrem1btn.Size = New System.Drawing.Size(134, 23)
        Me.swrem1btn.TabIndex = 70
        Me.swrem1btn.Text = "Reminder Letter 1"
        Me.swrem1btn.UseVisualStyleBackColor = True
        '
        'swfrbtn
        '
        Me.swfrbtn.Location = New System.Drawing.Point(3, 87)
        Me.swfrbtn.Name = "swfrbtn"
        Me.swfrbtn.Size = New System.Drawing.Size(134, 23)
        Me.swfrbtn.TabIndex = 74
        Me.swfrbtn.Text = "Final Reminder"
        Me.swfrbtn.UseVisualStyleBackColor = True
        '
        'lowellnotvulbtn
        '
        Me.lowellnotvulbtn.Location = New System.Drawing.Point(275, 210)
        Me.lowellnotvulbtn.Name = "lowellnotvulbtn"
        Me.lowellnotvulbtn.Size = New System.Drawing.Size(109, 60)
        Me.lowellnotvulbtn.TabIndex = 60
        Me.lowellnotvulbtn.Text = "Lowell NOT Vulnerable NOT dialled"
        Me.lowellnotvulbtn.UseVisualStyleBackColor = True
        '
        'TabControl2
        '
        Me.TabControl2.Controls.Add(Me.TabPage3)
        Me.TabControl2.Controls.Add(Me.TabPage4)
        Me.TabControl2.Location = New System.Drawing.Point(568, 105)
        Me.TabControl2.Name = "TabControl2"
        Me.TabControl2.SelectedIndex = 0
        Me.TabControl2.Size = New System.Drawing.Size(271, 321)
        Me.TabControl2.TabIndex = 61
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.arrow_by_agebtn)
        Me.TabPage3.Controls.Add(Me.arrow_sch_grpbtn)
        Me.TabPage3.Controls.Add(Me.sch_grplbl)
        Me.TabPage3.Controls.Add(Me.arrow_sch_cbox)
        Me.TabPage3.Controls.Add(Me.arrbtn)
        Me.TabPage3.Controls.Add(Me.cpw_schbtn)
        Me.TabPage3.Controls.Add(Me.cpw_schlbl)
        Me.TabPage3.Controls.Add(Me.cpw_sch_cbox)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(263, 295)
        Me.TabPage3.TabIndex = 0
        Me.TabPage3.Text = "No Longer Used 1"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.dvlabtn)
        Me.TabPage4.Controls.Add(Me.dvlabrokenbtn)
        Me.TabPage4.Controls.Add(Me.etcbtn)
        Me.TabPage4.Controls.Add(Me.fsbtn)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(263, 295)
        Me.TabPage4.TabIndex = 1
        Me.TabPage4.Text = "No Longer Used 2"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'SLNotvul
        '
        Me.SLNotvul.Location = New System.Drawing.Point(161, 348)
        Me.SLNotvul.Name = "SLNotvul"
        Me.SLNotvul.Size = New System.Drawing.Size(109, 60)
        Me.SLNotvul.TabIndex = 62
        Me.SLNotvul.Text = "Student Loans IDS NOT Vulnerable NOT dialled"
        Me.SLNotvul.UseVisualStyleBackColor = True
        '
        'LAANotvul
        '
        Me.LAANotvul.Location = New System.Drawing.Point(286, 348)
        Me.LAANotvul.Name = "LAANotvul"
        Me.LAANotvul.Size = New System.Drawing.Size(109, 60)
        Me.LAANotvul.TabIndex = 63
        Me.LAANotvul.Text = "DMI LAA IDS NOT Vulnerable NOT dialled"
        Me.LAANotvul.UseVisualStyleBackColor = True
        '
        'HMRCnotvul
        '
        Me.HMRCnotvul.Location = New System.Drawing.Point(420, 348)
        Me.HMRCnotvul.Name = "HMRCnotvul"
        Me.HMRCnotvul.Size = New System.Drawing.Size(109, 60)
        Me.HMRCnotvul.TabIndex = 64
        Me.HMRCnotvul.Text = "HMRC DMI NOT Vulnerable NOT dialled"
        Me.HMRCnotvul.UseVisualStyleBackColor = True
        '
        'adhocClientbtn
        '
        Me.adhocClientbtn.Location = New System.Drawing.Point(372, 133)
        Me.adhocClientbtn.Name = "adhocClientbtn"
        Me.adhocClientbtn.Size = New System.Drawing.Size(129, 23)
        Me.adhocClientbtn.TabIndex = 55
        Me.adhocClientbtn.Text = "Choose Client"
        Me.adhocClientbtn.UseVisualStyleBackColor = True
        '
        'mainform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1060, 691)
        Me.Controls.Add(Me.HMRCnotvul)
        Me.Controls.Add(Me.LAANotvul)
        Me.Controls.Add(Me.SLNotvul)
        Me.Controls.Add(Me.TabControl2)
        Me.Controls.Add(Me.lowellnotvulbtn)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.hmrc_batchbtn)
        Me.Controls.Add(Me.appealbtn)
        Me.Controls.Add(Me.segmentbtn)
        Me.Controls.Add(Me.listbtn)
        Me.Controls.Add(Me.etc_brokenbtn)
        Me.Controls.Add(Me.awbtn)
        Me.Controls.Add(Me.slbtn)
        Me.Controls.Add(Me.lowellbtn)
        Me.Controls.Add(Me.rtdfvanbtn)
        Me.Controls.Add(Me.fvanbtn)
        Me.Controls.Add(Me.precom2btn)
        Me.Controls.Add(Me.precom1btn)
        Me.Controls.Add(Me.hmrcagebtn)
        Me.Controls.Add(Me.tmabtn)
        Me.Controls.Add(Me.hbop_mbtn)
        Me.Controls.Add(Me.tma_rbtn)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.UUlbl)
        Me.Controls.Add(Me.UUCbox)
        Me.Controls.Add(Me.UUBtn)
        Me.Controls.Add(Me.vulnerbtn)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.outcallbtn)
        Me.Controls.Add(Me.cco_btn)
        Me.Controls.Add(Me.rtn_not_cmecbtn)
        Me.Controls.Add(Me.lsc_crystalbtn)
        Me.Controls.Add(Me.lsc_fdcbtn)
        Me.Controls.Add(Me.lsc_defaultedbtn)
        Me.Controls.Add(Me.lsc_camp2btn)
        Me.Controls.Add(Me.lsc_camp1btn)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.feesbtn)
        Me.Controls.Add(Me.hmrcbtn)
        Me.Controls.Add(Me.bail_retnbtn)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.cmecbtn)
        Me.Controls.Add(Me.cmec_ooabtn)
        Me.Controls.Add(Me.returnbtn)
        Me.Controls.Add(Me.otherbtn)
        Me.Controls.Add(Me.ooabtn)
        Me.Controls.Add(Me.agentbtn)
        Me.Controls.Add(Me.clntbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.paymentsbtn)
        Me.Controls.Add(Me.top20btn)
        Me.Controls.Add(Me.prioritybtn)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "mainform"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Phone Dialler"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage8.ResumeLayout(False)
        Me.UU.ResumeLayout(False)
        Me.TabPage9.ResumeLayout(False)
        Me.TabPage10.ResumeLayout(False)
        Me.TabPage11.ResumeLayout(False)
        Me.TabPage12.ResumeLayout(False)
        Me.TabPage12.PerformLayout()
        Me.TabControl2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents prioritybtn As System.Windows.Forms.Button
    Friend WithEvents top20btn As System.Windows.Forms.Button
    Friend WithEvents paymentsbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents clntbtn As System.Windows.Forms.Button
    Friend WithEvents arrbtn As System.Windows.Forms.Button
    Friend WithEvents agentbtn As System.Windows.Forms.Button
    Friend WithEvents ooabtn As System.Windows.Forms.Button
    Friend WithEvents otherbtn As System.Windows.Forms.Button
    Friend WithEvents returnbtn As System.Windows.Forms.Button
    Friend WithEvents cmec_ooabtn As System.Windows.Forms.Button
    Friend WithEvents cmecbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents bail_retnbtn As System.Windows.Forms.Button
    Friend WithEvents hmrcbtn As System.Windows.Forms.Button
    Friend WithEvents feesbtn As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lsc_camp1btn As System.Windows.Forms.Button
    Friend WithEvents lsc_camp2btn As System.Windows.Forms.Button
    Friend WithEvents lsc_defaultedbtn As System.Windows.Forms.Button
    Friend WithEvents lsc_fdcbtn As System.Windows.Forms.Button
    Friend WithEvents lsc_crystalbtn As System.Windows.Forms.Button
    Friend WithEvents rtn_not_cmecbtn As System.Windows.Forms.Button
    Friend WithEvents cco_btn As System.Windows.Forms.Button
    Friend WithEvents arrow_sch_grpbtn As System.Windows.Forms.Button
    Friend WithEvents arrow_sch_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents sch_grplbl As System.Windows.Forms.Label
    Friend WithEvents cpw_schbtn As System.Windows.Forms.Button
    Friend WithEvents cpw_schlbl As System.Windows.Forms.Label
    Friend WithEvents cpw_sch_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents outcallbtn As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents vulnerbtn As System.Windows.Forms.Button
    Friend WithEvents UUBtn As System.Windows.Forms.Button
    Friend WithEvents UUCbox As System.Windows.Forms.ComboBox
    Friend WithEvents UUlbl As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tma_rbtn As System.Windows.Forms.Button
    Friend WithEvents hbop_mbtn As System.Windows.Forms.Button
    Friend WithEvents tmabtn As System.Windows.Forms.Button
    Friend WithEvents hmrcagebtn As System.Windows.Forms.Button
    Friend WithEvents precom1btn As System.Windows.Forms.Button
    Friend WithEvents precom2btn As System.Windows.Forms.Button
    Friend WithEvents fvanbtn As System.Windows.Forms.Button
    Friend WithEvents rtdfvanbtn As System.Windows.Forms.Button
    Friend WithEvents fsbtn As System.Windows.Forms.Button
    Friend WithEvents dvlabtn As System.Windows.Forms.Button
    Friend WithEvents arrow_by_agebtn As System.Windows.Forms.Button
    Friend WithEvents lowellbtn As System.Windows.Forms.Button
    Friend WithEvents dvlabrokenbtn As System.Windows.Forms.Button
    Friend WithEvents slbtn As System.Windows.Forms.Button
    Friend WithEvents etcbtn As System.Windows.Forms.Button
    Friend WithEvents awbtn As System.Windows.Forms.Button
    Friend WithEvents etc_brokenbtn As System.Windows.Forms.Button
    Friend WithEvents listbtn As System.Windows.Forms.Button
    Friend WithEvents segmentbtn As System.Windows.Forms.Button
    Friend WithEvents appealbtn As System.Windows.Forms.Button
    Friend WithEvents compbtn7 As System.Windows.Forms.Button
    Friend WithEvents remindbtn As System.Windows.Forms.Button
    Friend WithEvents remind23btn As System.Windows.Forms.Button
    Friend WithEvents precomL1btn As System.Windows.Forms.Button
    Friend WithEvents retnbtn As System.Windows.Forms.Button
    Friend WithEvents hmrc_batchbtn As System.Windows.Forms.Button
    Friend WithEvents finrembtn As System.Windows.Forms.Button
    Friend WithEvents finenfbtn As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents MfinalWbtn As System.Windows.Forms.Button
    Friend WithEvents Mremindbtn As System.Windows.Forms.Button
    Friend WithEvents Mcompbtn As System.Windows.Forms.Button
    Friend WithEvents lowellnotvulbtn As System.Windows.Forms.Button
    Friend WithEvents TabControl2 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents SLNotvul As System.Windows.Forms.Button
    Friend WithEvents LAANotvul As System.Windows.Forms.Button
    Friend WithEvents HMRCnotvul As System.Windows.Forms.Button
    Friend WithEvents enfletterbtn As System.Windows.Forms.Button
    Friend WithEvents precommL2btn As System.Windows.Forms.Button
    Friend WithEvents MenfLetterbtn As System.Windows.Forms.Button
    Friend WithEvents MfinRemindbtn As System.Windows.Forms.Button
    Friend WithEvents Mfinenfbtn As System.Windows.Forms.Button
    Friend WithEvents MpcommL1btn As System.Windows.Forms.Button
    Friend WithEvents MprecommL2 As System.Windows.Forms.Button
    Friend WithEvents Mretnbtn As System.Windows.Forms.Button
    Friend WithEvents furtherenfbtn As System.Windows.Forms.Button
    Friend WithEvents Mfurtherenfbtn As System.Windows.Forms.Button
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents hmrcintrobtn As System.Windows.Forms.Button
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents noasentbtn As System.Windows.Forms.Button
    Friend WithEvents slcs2letterbtn As System.Windows.Forms.Button
    Friend WithEvents slcs3btn As System.Windows.Forms.Button
    Friend WithEvents hmrcl2btn As System.Windows.Forms.Button
    Friend WithEvents hmrcl3btn As System.Windows.Forms.Button
    Friend WithEvents hmrcl4btn As System.Windows.Forms.Button
    Friend WithEvents hmrcsw1btn As System.Windows.Forms.Button
    Friend WithEvents hmrcsw2btn As System.Windows.Forms.Button
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents laal1btn As System.Windows.Forms.Button
    Friend WithEvents laa2btn As System.Windows.Forms.Button
    Friend WithEvents laa3btn As System.Windows.Forms.Button
    Friend WithEvents TabPage8 As System.Windows.Forms.TabPage
    Friend WithEvents lowells1lbtn As System.Windows.Forms.Button
    Friend WithEvents lowells2btn As System.Windows.Forms.Button
    Friend WithEvents lowells3btn As System.Windows.Forms.Button
    Friend WithEvents lowell90btn As System.Windows.Forms.Button
    Friend WithEvents UU As System.Windows.Forms.TabPage
    Friend WithEvents TabPage9 As System.Windows.Forms.TabPage
    Friend WithEvents adhocbabtn As System.Windows.Forms.Button
    Friend WithEvents adhoc90btn As System.Windows.Forms.Button
    Friend WithEvents uusl1btn As System.Windows.Forms.Button
    Friend WithEvents uusl2btn As System.Windows.Forms.Button
    Friend WithEvents uusl3btn As System.Windows.Forms.Button
    Friend WithEvents uusl4btn As System.Windows.Forms.Button
    Friend WithEvents TabPage10 As System.Windows.Forms.TabPage
    Friend WithEvents hello4to8btn As System.Windows.Forms.Button
    Friend WithEvents hello8plusbtn As System.Windows.Forms.Button
    Friend WithEvents outboundbtn As System.Windows.Forms.Button
    Friend WithEvents softtracebtn As System.Windows.Forms.Button
    Friend WithEvents brokenbtn As System.Windows.Forms.Button
    Friend WithEvents TabPage11 As System.Windows.Forms.TabPage
    Friend WithEvents eonS1A As System.Windows.Forms.Button
    Friend WithEvents EONS1B As System.Windows.Forms.Button
    Friend WithEvents eonS2 As System.Windows.Forms.Button
    Friend WithEvents allsdbtn As System.Windows.Forms.Button
    Friend WithEvents allftabtn As System.Windows.Forms.Button
    Friend WithEvents allhbopbtn As System.Windows.Forms.Button
    Friend WithEvents exceptionbtn As System.Windows.Forms.Button
    Friend WithEvents stagecbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Mexceptionbtn As System.Windows.Forms.Button
    Friend WithEvents Mstagecbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TabPage12 As System.Windows.Forms.TabPage
    Friend WithEvents swrunexcbtn As System.Windows.Forms.Button
    Friend WithEvents swexcombo As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents swfenfbtn As System.Windows.Forms.Button
    Friend WithEvents swpc2btn As System.Windows.Forms.Button
    Friend WithEvents swenfbtn As System.Windows.Forms.Button
    Friend WithEvents swretbtn As System.Windows.Forms.Button
    Friend WithEvents swfebtn As System.Windows.Forms.Button
    Friend WithEvents swpc1btn As System.Windows.Forms.Button
    Friend WithEvents swcompbtn As System.Windows.Forms.Button
    Friend WithEvents swrem1btn As System.Windows.Forms.Button
    Friend WithEvents swfrbtn As System.Windows.Forms.Button
    Friend WithEvents swrem2btn As System.Windows.Forms.Button
    Friend WithEvents RNNDRBtn As System.Windows.Forms.Button
    Friend WithEvents MNNdrBtn As System.Windows.Forms.Button
    Friend WithEvents SNNDRbtn As System.Windows.Forms.Button
    Friend WithEvents ret_pendingbtn As System.Windows.Forms.Button
    Friend WithEvents southendbtn As System.Windows.Forms.Button
    Friend WithEvents sw_postenfbtn As System.Windows.Forms.Button
    Friend WithEvents sw_enfbtn As System.Windows.Forms.Button
    Friend WithEvents sw_compbtn As System.Windows.Forms.Button
    Friend WithEvents mar_postenfbtn As System.Windows.Forms.Button
    Friend WithEvents mar_enfbtn As System.Windows.Forms.Button
    Friend WithEvents mar_compbtn As System.Windows.Forms.Button
    Friend WithEvents br1_compbtn As System.Windows.Forms.Button
    Friend WithEvents br1_postenfbtn As System.Windows.Forms.Button
    Friend WithEvents br1_enfbtn As System.Windows.Forms.Button
    Friend WithEvents adhocClientbtn As System.Windows.Forms.Button

End Class

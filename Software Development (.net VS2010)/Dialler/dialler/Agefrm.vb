Public Class Agefrm

    Private Sub cancelbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cancelbtn.Click
        cancelled = True
        Me.Close()
    End Sub

    Private Sub createbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles createbtn.Click
        Me.Close()
    End Sub

    Private Sub Agefrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        age1rbtn.Checked = True
        If DVLA_case Then
            age1rbtn.Text = "3-28 days"
        Else
            age1rbtn.Text = "5-28 days"
        End If
    End Sub
End Class
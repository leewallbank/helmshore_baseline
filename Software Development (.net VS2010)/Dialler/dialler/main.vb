Public Class mainform
    '9.12.2010 lsc client 1045 excluded from other clients
    '20.12.2010 when client selected allow scheme selection
    '15.02.2011 ignore new cases until 5th day
    '16.06.2011 emp phone numbers to be added - revamp of code
    '28.06.2011 select cases from 6 days old - remove letter check

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub prioritybtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles prioritybtn.Click
        mode = "P"
        collect_stage.ShowDialog()
    End Sub

    Private Sub top20bth_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles top20btn.Click
        mode = "T"
        collect_stage.ShowDialog()
    End Sub

    Private Sub maintainbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        maintainfrm.Show()
    End Sub

    Private Sub mainform_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        os_con.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DVLA_case = False
        Dim env_str As String = ""
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
            If env_str = "Prod" Then
                prod_run = True
            End If
        Catch ex As Exception
            prod_run = False
        End Try

        smsfrm.ShowDialog()
        'get client names and schemes
        selected_stage_rows = 0
        stage_form = False
        'param1 = "onestep"
        'param2 = "ClientScheme"
        'param3 = "01_rowid 2clientID 3schemeID 4branchID"
        'param4 = "order by _rowid"
        'ret_code = get_table(param1, param2, param3, param4)

        'If ret_code = 1 Then
        '    Return
        'End If

        'ReDim cs_table(last_rowid, 3)
        'cs_table = table_array
        'param2 = "Client"
        'param3 = "01_rowid 2name"
        'param4 = "order by _rowid"
        'ret_code = get_table(param1, param2, param3, param4)
        'If ret_code = 1 Then
        '    Return
        'End If
        'ReDim client_table(last_rowid, 2)
        'client_table = table_array
        'param2 = "Scheme"
        'param3 = "01_rowid 2name"
        'param4 = "order by _rowid"
        'ret_code = get_table(param1, param2, param3, param4)
        'If ret_code = 1 Then
        '    Return
        'End If
        'sch_rows = last_rowid
        'ReDim sch_table(sch_rows, 2)
        'sch_table = table_array

    End Sub

    Private Sub paymentsbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles paymentsbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        param1 = "onestep"
        mode = "B"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                         " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                         " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                         " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                         " and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                         " or empPhone is not null or empFax is not null) " & _
                         " and debt_balance > 1.00 and (debtPaid > 0.00 or arrange_broken = 'Y')"
        produce_dialler_file(param2)
        Me.Close()
        'old_paymentsbtn()
    End Sub
    'Private Sub old_paymentsbtn()
    '    param2 = "Debtor"
    '    param3 = "01_rowid 2name_title 3name_forename 4name_surname" _
    '          & " 5address 6clientschemeID 7schemeID 8add_phone 9add_fax" _
    '          & "10debt_balance 11clientID 12arrange_next 13name2"
    '    param4 = "where  status = 'L' and status_hold = 'N' and status_open_closed = 'O'" _
    '           & " and bail_current = 'N' and (add_phone is not null or add_fax is not null) " & _
    '           " and (debtPaid > 0 or arrange_broken = 'Y')"
    '    ret_code = get_table(param1, param2, param3, param4)

    '    If ret_code = 1 Then
    '        Return
    '    End If
    '    Dim debt_rows As Integer = no_of_rows
    '    Dim debt_table = table_array

    '    Dim outline As String = ""
    '    Dim outfile As String = "Our_Reference,Title,Forename,Surname,Address,Client,Scheme,TelNumber," & _
    '     "SecondNumber,Balance,ClientID,Name2" & vbNewLine
    '    Dim col_value As String = ""
    '    Dim phone_no As String = ""
    '    Dim idx, idx2, idx3 As Integer
    '    Dim phone_array(debt_rows) As String
    '    Dim outline_array(debt_rows) As String
    '    Dim outline_idx As Integer = 0
    '    Dim sch_no As String = ""
    '    Dim cl_no As String = ""
    '    For idx = 1 To debt_rows
    '        ProgressBar1.Value = (idx / debt_rows) * 100
    '        For idx2 = 1 To 13
    '            Dim col_result As String = ""

    '            col_value = Trim(debt_table(idx, idx2))
    '            If idx2 = 6 Then  'clientschemeID
    '                If cs_table(col_value, 4) <> 2 Then  'Branch has to be 2
    '                    outline = ""
    '                    Exit For
    '                End If
    '                cl_no = cs_table(col_value, 2)
    '                sch_no = cs_table(col_value, 3)
    '                col_result = Trim(client_table(cl_no, 2))
    '            ElseIf idx2 = 7 Then
    '                col_result = Trim(sch_table(sch_no, 2))
    '            ElseIf idx2 = 8 Then
    '                If phone_no = col_value And phone_no.Length > 0 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                phone_no = col_value
    '                If Microsoft.VisualBasic.Right(phone_no, 1) = Chr(10) _
    '                    Or Microsoft.VisualBasic.Right(phone_no, 1) = Chr(13) Then
    '                    phone_no = Microsoft.VisualBasic.Left(phone_no, phone_no.Length - 1)
    '                End If
    '                If Microsoft.VisualBasic.Right(phone_no, 1) = Chr(10) _
    '                    Or Microsoft.VisualBasic.Right(phone_no, 1) = Chr(13) Then
    '                    phone_no = Microsoft.VisualBasic.Left(phone_no, phone_no.Length - 1)
    '                End If
    '                col_result = phone_no
    '            ElseIf idx2 = 9 Then
    '                col_result = col_value
    '                If Microsoft.VisualBasic.Right(col_result, 1) = Chr(10) _
    '                    Or Microsoft.VisualBasic.Right(col_result, 1) = Chr(13) Then
    '                    col_result = Microsoft.VisualBasic.Left(col_result, col_result.Length - 1)
    '                End If
    '                If Microsoft.VisualBasic.Right(col_result, 1) = Chr(10) _
    '                                        Or Microsoft.VisualBasic.Right(col_result, 1) = Chr(13) Then
    '                    col_result = Microsoft.VisualBasic.Left(col_result, col_result.Length - 1)
    '                End If
    '            ElseIf idx2 = 10 Then
    '                col_result = Format(col_value, "fixed")
    '            ElseIf idx2 = 11 Then
    '                col_result = cl_no
    '                'ignore next due date test - cases are live
    '                'ElseIf idx2 = 12 Then
    '                '    Dim col_date As Date
    '                '    Try
    '                '        col_date = CDate(col_value)
    '                '    Catch ex As Exception
    '                '        outline = ""
    '                '        Exit For
    '                '    End Try
    '                '    If col_date >= Now Then
    '                '        outline = ""
    '                '        Exit For
    '                '    End If
    '                '    If DateDiff("d", Now, col_date) >= 0 Or _
    '                '        DateDiff("d", Now, col_date) < -7 Then
    '                '        outline = ""
    '                '        Exit For
    '                '    End If
    '            ElseIf idx2 = 13 Then
    '                col_result = col_value
    '            Else
    '                For idx3 = 1 To Len(col_value)
    '                    If Mid(col_value, idx3, 1) = vbCr Or _
    '                        Mid(col_value, idx3, 1) = "," Or _
    '                       Mid(col_value, idx3, 1) = vbLf Then
    '                        col_result = col_result & " "
    '                    Else
    '                        col_result = col_result & Mid(col_value, idx3, 1)
    '                    End If
    '                Next
    '            End If
    '            If idx2 = 8 Then
    '                Continue For
    '            End If
    '            If idx2 = 9 Then
    '                If phone_no.Length = 0 Then
    '                    If col_result.Length = 0 Then
    '                        outline = ""
    '                        Exit For
    '                    End If
    '                    phone_no = col_result
    '                    outline = outline & phone_no & "," & "" & ","
    '                Else
    '                    outline = outline & phone_no & "," & col_result & ","
    '                End If
    '            Else
    '                If idx2 <> 12 And idx2 <> 14 Then
    '                    outline = outline & col_result & ","
    '                End If
    '            End If
    '        Next
    '        If Len(outline) > 0 Then
    '            outline_array(outline_idx) = outline
    '            phone_array(outline_idx) = phone_no
    '            outline_idx += 1
    '            outline = ""
    '        End If
    '    Next

    '    'sort arrays by phone numbers
    '    ProgressBar1.Value = 5
    '    Array.Sort(phone_array, outline_array, 0, outline_idx)
    '    phone_no = ""
    '    For idx = 0 To outline_idx - 1
    '        ProgressBar1.Value = (idx / outline_idx) * 100
    '        If phone_no = phone_array(idx) Then
    '            Continue For
    '        End If
    '        outline = outline_array(idx)
    '        If Len(outline) > 0 Then
    '            outfile = outfile & outline & vbNewLine
    '        End If
    '        phone_no = phone_array(idx)
    '    Next
    '    ProgressBar1.Value = 100
    '    'write out file
    '    With SaveFileDialog1
    '        .Title = "Save file"
    '        .Filter = "CSV files |*.csv"
    '        .DefaultExt = ".csv"
    '        .OverwritePrompt = True
    '        .InitialDirectory = "c:\temp"
    '        .FileName = "dialler.csv"
    '    End With

    '    Dim filepath As String = SaveFileDialog1.FileName
    '    If Len(outfile) = 0 Then
    '        MessageBox.Show("There are no cases selected")
    '    Else
    '        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
    '        End If
    '    End If
    '    Me.Close()
    'End Sub

    Private Sub clntbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clntbtn.Click
        collect_cases = True
        choose_client()
    End Sub
    Private Sub choose_client()
        disable_buttons()

        clientfrm.ShowDialog()
        'Dim client_name As String = client_table(selected_client, 2)
        If collect_cases Then
            If selected_client < 1 Then
                MsgBox("No client has been selected")
                enable_buttons()
                Exit Sub
            End If
        End If
        
        If selected_client = 1805 Then
            DVLA_case = True
        Else
            DVLA_case = False
        End If
        If MsgBox("Create file for this client?", MsgBoxStyle.YesNo, selected_client_name) = MsgBoxResult.No Then
            enable_buttons()
            Exit Sub
        End If
        ccmt_offence_val = 0
        If selected_client = 909 Then
            If MsgBox("Choose a specific category for CCMT campaign?", MsgBoxStyle.YesNo, "Choose specific category") = MsgBoxResult.Yes Then
                categoryfrm.ShowDialog()
                If ccmt_offence_val <> 0 Then
                    select_ccmt_campaigns()
                Else
                    MsgBox("selection cancelled")
                End If
                Me.Close()
                Exit Sub
            End If
        End If

        'allow scheme to be entered
        If MsgBox("Choose a specific scheme?", MsgBoxStyle.YesNo, "Scheme selection?") = MsgBoxResult.Yes Then
            schemefrm.ShowDialog()
        End If

        If selected_client = 909 Then
            select_legal_services()
            Exit Sub
        End If

        'allow stage to be entered
        If MsgBox("Choose a specific stage?", MsgBoxStyle.YesNo, "Stage selection?") = MsgBoxResult.Yes Then
            If selected_client = 1275 Then  'HMRC
                bailiff_stages.ShowDialog()
                If no_stages_found Then
                    Me.Close()
                    Exit Sub
                End If
            Else
                If collect_cases Then
                    mode = "C"
                    collect_stage.ShowDialog()
                    Me.Close()
                    Exit Sub
                Else
                    bailiff_stages.ShowDialog()
                    If no_stages_found Then
                        Me.Close()
                        Exit Sub
                    End If
                End If
            End If
        End If

        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "C"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, _createdDate, arrange_next, statusCode, LaststageDate, bailiffID, bail_current from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and (add_phone is not null or add_fax is not null " & _
                                " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub
    'Private Sub old_client()
    '    param1 = "onestep"
    '    param2 = "Debtor"
    '    param3 = "01_rowid 2name_title 3name_forename 4name_surname" _
    '          & " 5address 6clientschemeID 7schemeID 8add_phone 9add_fax" _
    '          & "10debt_balance 11clientID 12name2 13last_stageID 14_createdDate"
    '    param4 = "where  status = 'L' and status_hold = 'N' and status_open_closed = 'O'" _
    '           & " and bail_current = 'N' and (add_phone is not null or add_fax is not null)"
    '    ret_code = get_table(param1, param2, param3, param4)
    '    If ret_code = 1 Then
    '        Return
    '    End If
    '    Dim debt_rows As Integer = no_of_rows
    '    Dim debt_table = table_array

    '    Dim outline As String = ""
    '    Dim outfile As String = "Our_Reference,Title,Forename,Surname,Address,Client,Scheme,TelNumber," & _
    '      "SecondNumber,Balance,ClientID,Name2" & vbNewLine
    '    Dim col_value As String = ""
    '    Dim phone_no As String = ""
    '    Dim idx, idx2, idx3 As Integer
    '    Dim phone_array(debt_rows) As String
    '    Dim outline_array(debt_rows) As String
    '    Dim outline_idx As Integer = 0
    '    Dim sch_no As String = ""
    '    Dim cl_no As String = ""
    '    For idx = 1 To debt_rows
    '        ProgressBar1.Value = (idx / debt_rows) * 100
    '        For idx2 = 1 To 14
    '            Dim col_result As String = ""
    '            col_value = Trim(debt_table(idx, idx2))
    '            If idx2 = 6 Then  'clientschemeID
    '                'check if csid selected
    '                Dim csid_idx As Integer
    '                Dim csid_found As Boolean = False
    '                If csid_rows > 0 Then
    '                    For csid_idx = 0 To csid_rows
    '                        If csid_table(csid_idx) = col_value Then
    '                            csid_found = True
    '                        End If
    '                    Next
    '                    If csid_found = False Then
    '                        outline = ""
    '                        Exit For
    '                    End If
    '                End If
    '                If cs_table(col_value, 4) <> 2 Then  'Branch has to be 2 
    '                    outline = ""
    '                    Exit For
    '                End If
    '                cl_no = cs_table(col_value, 2)
    '                sch_no = cs_table(col_value, 3)
    '                'check if client number is one required
    '                If cl_no <> selected_client Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                col_result = Trim(client_table(cl_no, 2))
    '            ElseIf idx2 = 7 Then
    '                col_result = Trim(sch_table(sch_no, 2))
    '            ElseIf idx2 = 8 Then
    '                If phone_no = col_value And phone_no.Length > 0 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                'If col_value.Length < 4 Then
    '                '    outline = ""
    '                '    Exit For
    '                'End If
    '                phone_no = col_value
    '                If Microsoft.VisualBasic.Right(phone_no, 1) = Chr(10) _
    '                    Or Microsoft.VisualBasic.Right(phone_no, 1) = Chr(13) Then
    '                    phone_no = Microsoft.VisualBasic.Left(phone_no, phone_no.Length - 1)
    '                End If
    '                If Microsoft.VisualBasic.Right(phone_no, 1) = Chr(10) _
    '                    Or Microsoft.VisualBasic.Right(phone_no, 1) = Chr(13) Then
    '                    phone_no = Microsoft.VisualBasic.Left(phone_no, phone_no.Length - 1)
    '                End If
    '                col_result = phone_no
    '            ElseIf idx2 = 9 Then
    '                col_result = col_value
    '                If Microsoft.VisualBasic.Right(col_result, 1) = Chr(10) _
    '                    Or Microsoft.VisualBasic.Right(col_result, 1) = Chr(13) Then
    '                    col_result = Microsoft.VisualBasic.Left(col_result, col_result.Length - 1)
    '                End If
    '                If Microsoft.VisualBasic.Right(col_result, 1) = Chr(10) _
    '                                        Or Microsoft.VisualBasic.Right(col_result, 1) = Chr(13) Then
    '                    col_result = Microsoft.VisualBasic.Left(col_result, col_result.Length - 1)
    '                End If
    '            ElseIf idx2 = 10 Then
    '                col_result = Format(col_value, "fixed")
    '            ElseIf idx2 = 11 Then
    '                col_result = cl_no
    '            ElseIf idx2 = 12 Then
    '                col_result = col_value
    '            ElseIf idx2 = 13 Then  'last-stageID
    '                'check letter sent at least 5 days ago
    '                If InStr(col_value, "Letter") > 0 Then
    '                    param2 = "select _createdDate from Note " & _
    '                    " where debtorID = " & debt_table(idx, 1) & _
    '                    " and type = 'Letter'" & _
    '                    " order by _rowid"
    '                    Dim note_dataset As DataSet = get_dataset(param1, param2)
    '                    If no_of_rows > 0 Then
    '                        Dim note_date As Date = note_dataset.Tables(0).Rows(0).Item(0)
    '                        If DateDiff(DateInterval.Day, note_date, Now) < 5 Then
    '                            outline = ""
    '                            Exit For
    '                        End If
    '                    End If
    '                End If
    '            ElseIf idx2 = 14 Then  'createdDate
    '                'check at least 5 days old
    '                Dim created_date As Date = col_value
    '                If DateDiff("d", col_value, Now) < 5 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '            Else
    '                For idx3 = 1 To Len(col_value)
    '                    If Mid(col_value, idx3, 1) = vbCr Or _
    '                        Mid(col_value, idx3, 1) = "," Or _
    '                       Mid(col_value, idx3, 1) = vbLf Then
    '                        col_result = col_result & " "
    '                    Else
    '                        col_result = col_result & Mid(col_value, idx3, 1)
    '                    End If
    '                Next
    '            End If
    '            If idx2 = 8 Then
    '                Continue For
    '            End If
    '            If idx2 = 9 Then
    '                If phone_no.Length = 0 Then
    '                    If col_result.Length = 0 Then
    '                        outline = ""
    '                        Exit For
    '                    End If
    '                    phone_no = col_result
    '                    outline = outline & phone_no & "," & "" & ","
    '                Else
    '                    outline = outline & phone_no & "," & col_result & ","
    '                End If
    '            Else
    '                If idx2 <> 13 And idx2 <> 14 Then
    '                    outline = outline & col_result & ","
    '                End If
    '            End If
    '        Next
    '        If Len(outline) > 0 Then
    '            outline_array(outline_idx) = outline
    '            phone_array(outline_idx) = phone_no
    '            outline_idx += 1
    '            outline = ""
    '        End If
    '    Next
    '    'sort arrays by phone numbers
    '    Array.Sort(phone_array, outline_array, 0, outline_idx)
    '    phone_no = ""
    '    ProgressBar1.Value = 5
    '    For idx = 0 To outline_idx - 1
    '        ProgressBar1.Value = (idx / outline_idx) * 100
    '        If phone_no = phone_array(idx) Then
    '            Continue For
    '        End If
    '        outline = outline_array(idx)
    '        If Len(outline) > 0 Then
    '            outfile = outfile & outline & vbNewLine
    '        End If
    '        phone_no = phone_array(idx)
    '    Next
    '    'write out file
    '    With SaveFileDialog1
    '        .Title = "Save file"
    '        .Filter = "CSV files |*.csv"
    '        .DefaultExt = ".csv"
    '        .OverwritePrompt = True
    '        .InitialDirectory = "c:\temp"
    '        .FileName = "dialler.csv"
    '    End With
    '    ProgressBar1.Value = 100
    '    Dim filepath As String = SaveFileDialog1.FileName
    '    If Len(outfile) = 0 Then
    '        MessageBox.Show("There are no cases selected")
    '    Else
    '        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
    '        End If
    '    End If
    '    Me.Close()
    'End Sub
    Private Sub select_ccmt_campaigns()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "E"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                        " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                      " last_stageID, add_postcode, status_nextdate, _createdDate, statusCode  from Debtor" & _
                        " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                        " and bail_current = 'N' and offenceValue = " & ccmt_offence_val & _
                        " and debt_balance > 5 and (add_phone is not null or add_fax is not null " & _
                        " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub
    Private Sub select_legal_services()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "C"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                        " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                      " last_stageID, add_postcode, status_nextdate, _createdDate, statusCode  from Debtor" & _
                        " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                        " and bail_current = 'N' and isCompany = 'N' and arrange_broken = 'Y' " & _
                        " and debt_balance > 5 and (add_phone is not null or add_fax is not null " & _
                        " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub
    Private Sub hitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()

        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "H"
        Dim sevendays_ago As Date = DateAdd(DateInterval.Day, -7, Now)
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                 " and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                 " or empPhone is not null or empFax is not null) and debt_balance > 2.5" & _
                 " and clientschemeID <> 265 and clientschemeID <> 266 " & _
                 " and debt_fees > 22.5 and clientschemeID <> 95 and clientschemeID <> 96" & _
                 " and last_date < '" & Format(sevendays_ago, "yyyy-MM-dd") & "'"
        produce_dialler_file(param2)
        Me.Close()
        'oldhitbtn()
    End Sub
    'Private Sub oldhitbtn()
    '    postcodefrm.Out_of_areaTableAdapter.Fill(postcodefrm.FeesSQLDataSet.Out_of_area)
    '    'get stage names
    '    param2 = "Stage"
    '    param3 = "01_rowid 2name"
    '    param4 = "order by _rowid"
    '    ret_code = get_table(param1, param2, param3, param4)
    '    If ret_code = 1 Then
    '        Return
    '    End If
    '    Dim stage_array = table_array

    '    Dim sevendays_ago As Date = DateAdd(DateInterval.Day, -7, Now)
    '    param1 = "onestep"
    '    param2 = "Debtor"
    '    param3 = "01_rowid 2name_title 3name_forename 4name_surname" _
    '          & " 5address 6clientschemeID 7schemeID 8add_phone 9add_fax" _
    '          & "10debt_balance 11clientID 12arrange_next 13name2 14add_postcode 15last_stageID"
    '    param4 = "where  status = 'L' and status_hold = 'N' and status_open_closed = 'O'" _
    '           & " and clientschemeID <> 265 and clientschemeID <> 266 " _
    '           & " and bail_current = 'N' and (add_phone is not null or add_fax is not null) and debt_balance > 2.5" _
    '           & " and debt_fees > 22.5 and clientschemeID <> 95 and clientschemeID <> 96" _
    '           & " and last_date < '" & Format(sevendays_ago, "yyyy.MM.dd") & "'"
    '    ret_code = get_table(param1, param2, param3, param4)

    '    If ret_code = 1 Then
    '        Return
    '    End If
    '    Dim debt_rows As Integer = no_of_rows
    '    Dim debt_table = table_array

    '    Dim outline As String = ""
    '    Dim outfile As String = "Our_Reference,Title,Forename,Surname,Address,Client,Scheme,TelNumber," & _
    '      "SecondNumber,Balance,ClientID,Name2" & vbNewLine
    '    Dim col_value As String = ""
    '    Dim phone_no As String = ""
    '    Dim idx, idx2, idx3 As Integer
    '    Dim phone_array(debt_rows) As String
    '    Dim outline_array(debt_rows) As String
    '    Dim outline_idx As Integer = 0

    '    For idx = 1 To debt_rows
    '        ProgressBar1.Value = (idx / debt_rows) * 100
    '        Dim last_stageid As Integer = debt_table(idx, 15)

    '        'If Trim(stage_array(last_stageid, 2)) <> "(Return)" _

    '        'And Trim(stage_array(last_stageid, 2)) <> "FurtherVanAttendance" _
    '        'And Trim(stage_array(last_stageid, 2)) <> "Van Notice sent" _


    '        If Trim(stage_array(last_stageid, 2)) <> "FurtherVanAttendance" And _
    '        Trim(stage_array(last_stageid, 2)) <> "Van Attendance" And _
    '        Trim(stage_array(last_stageid, 2)) <> "AwaitingVanApproval" And _
    '        Trim(stage_array(last_stageid, 2)) <> "(Return)" Then
    '            Continue For
    '        End If
    '        Dim sch_no As String = ""
    '        Dim cl_no As String = ""
    '        Dim debtor As Integer
    '        For idx2 = 1 To 14
    '            Dim col_result As String = ""
    '            col_value = Trim(debt_table(idx, idx2))
    '            If idx2 = 1 Then
    '                debtor = col_value
    '            End If
    '            If idx2 = 6 Then  'clientschemeID
    '                If cs_table(col_value, 4) <> 1 Then  'Branch has to be 1
    '                    outline = ""
    '                    Exit For
    '                End If
    '                cl_no = cs_table(col_value, 2)
    '                'exclude special hit team cases
    '                If cl_no = 50 Or cl_no = 54 Or cl_no = 82 Or cl_no = 141 Then 'Liverpool/Manchester/Nottm/Trafford
    '                    If Trim(stage_array(last_stageid, 2)) = "FurtherVanAttendance" Then
    '                        outline = ""
    '                        Exit For
    '                    End If
    '                End If
    '                'van attendance and awaiting van approval only for cornwall
    '                If cl_no <> 949 And cl_no <> 955 And cl_no <> 163 And cl_no <> 160 And _
    '                cl_no <> 211 And cl_no <> 153 And cl_no <> 154 And cl_no <> 648 And cl_no <> 155 And _
    '                cl_no <> 161 And cl_no <> 381 And cl_no <> 628 Then
    '                    If Trim(stage_array(last_stageid, 2)) = "Van Attendance" Or _
    '                    Trim(stage_array(last_stageid, 2)) = "AwaitingVanApproval" Then
    '                        outline = ""
    '                        Exit For
    '                    End If
    '                End If
    '                sch_no = cs_table(col_value, 3)
    '                'ignore road traffic schenes
    '                If sch_no = 41 Or sch_no = 676 Or sch_no = 773 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                col_result = Trim(client_table(cl_no, 2))
    '            ElseIf idx2 = 7 Then
    '                col_result = Trim(sch_table(sch_no, 2))
    '                'ignore NNDR case unless at return stage
    '                If Microsoft.VisualBasic.Left(col_result, 4) = "NNDR" _
    '                And Trim(stage_array(last_stageid, 2)) <> "(Return)" Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '            ElseIf idx2 = 8 Then
    '                If col_value.Length < 5 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                If phone_no = col_value And phone_no.Length > 0 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                phone_no = col_value
    '                col_result = col_value
    '            ElseIf idx2 = 9 Then
    '                If phone_no.Length = 0 Then
    '                    phone_no = col_value
    '                End If
    '                col_result = col_value
    '            ElseIf idx2 = 10 Then
    '                col_result = Format(col_value, "fixed")
    '            ElseIf idx2 = 11 Then
    '                col_result = cl_no
    '            ElseIf idx2 = 12 Then
    '                Continue For
    '            ElseIf idx2 = 13 Then
    '                col_result = col_value
    '            ElseIf idx2 = 14 Then
    '                'check postcode sector
    '                If col_value.Length > 0 Then
    '                    If found_pc_sector(col_value) = True Then
    '                        outline = ""
    '                        Exit For
    '                    End If
    '                End If
    '            Else
    '                For idx3 = 1 To Len(col_value)
    '                    If Mid(col_value, idx3, 1) = vbCr Or _
    '                        Mid(col_value, idx3, 1) = "," Or _
    '                       Mid(col_value, idx3, 1) = vbLf Then
    '                        col_result = col_result & " "
    '                    Else
    '                        col_result = col_result & Mid(col_value, idx3, 1)
    '                    End If
    '                Next
    '            End If
    '            If idx2 <> 12 And idx2 <> 14 Then
    '                outline = outline & col_result & ","
    '            End If
    '        Next
    '        If Len(outline) > 0 Then
    '            'check that debtor does not just have a levy fee - must have van as well
    '            param2 = "select type from Fee where debtorID = " & debtor & _
    '            " and fee_amount > 0"
    '            Dim fee_dataset As DataSet = get_dataset(param1, param2)
    '            Dim fee_idx As Integer
    '            Dim levy_found As Boolean = False
    '            Dim van_fees_found As Boolean = False
    '            For fee_idx = 0 To no_of_rows - 1
    '                Dim fee_type As String = LCase(fee_dataset.Tables(0).Rows(fee_idx).Item(0))
    '                If InStr(fee_type, "lev") > 0 Then
    '                    levy_found = True
    '                ElseIf InStr(fee_type, "van") > 0 Then
    '                    van_fees_found = True
    '                End If
    '            Next
    '            If levy_found And Not van_fees_found Then
    '                outline = ""
    '            Else
    '                outline_array(outline_idx) = outline
    '                phone_array(outline_idx) = phone_no
    '                outline_idx += 1
    '                outline = ""
    '            End If
    '        End If
    '    Next

    '    'sort arrays by phone numbers
    '    Array.Sort(phone_array, outline_array, 0, outline_idx)
    '    phone_no = ""
    '    ProgressBar1.Value = 5
    '    For idx = 0 To outline_idx - 1
    '        ProgressBar1.Value = (idx / outline_idx) * 100
    '        If phone_no = phone_array(idx) Then
    '            Continue For
    '        End If
    '        outline = outline_array(idx)
    '        If Len(outline) > 0 Then
    '            outfile = outfile & outline & vbNewLine
    '        End If
    '        phone_no = phone_array(idx)
    '    Next
    '    ProgressBar1.Value = 100
    '    'write out file
    '    With SaveFileDialog1
    '        .Title = "Save file"
    '        .Filter = "CSV files |*.csv"
    '        .DefaultExt = ".csv"
    '        .OverwritePrompt = True
    '        .InitialDirectory = "c:\temp"
    '        .FileName = "dialler.csv"
    '    End With

    '    Dim filepath As String = SaveFileDialog1.FileName
    '    If Len(outfile) = 0 Then
    '        MessageBox.Show("There are no cases selected")
    '    Else
    '        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
    '        End If
    '    End If
    '    Me.Close()
    'End Sub

    Private Sub arrbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles arrbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "A"
        Dim three_months_ago As Date = DateAdd(DateInterval.Month, -3, Now)
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                 " where status_open_closed = 'O' and status = 'A' and status_hold = 'N'" & _
                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                 " or empPhone is not null or empFax is not null)and arrange_started < '" & three_months_ago.ToString("yyyy-MM-dd") & "'"

        produce_dialler_file(param2)
        Me.Close()
        'old_arrbrn()
    End Sub
    'Private Sub old_arrbrn()

    '    Dim three_months_ago As Date = DateAdd(DateInterval.Month, -3, Now)
    '    param1 = "onestep"
    '    param2 = "Debtor"
    '    param3 = "01_rowid 2name_title 3name_forename 4name_surname" _
    '          & " 5address 6clientschemeID 7schemeID 8add_phone 9add_fax" _
    '          & "10debt_balance 11clientID 12arrange_next 13name2"
    '    param4 = "where  status = 'A' and status_hold = 'N' and status_open_closed = 'O'" _
    '           & " and bail_current = 'N' and (add_phone is not null or add_fax is not null) and arrange_started < '" & Format(three_months_ago, "yyyy.MM.dd") & "'"
    '    ret_code = get_table(param1, param2, param3, param4)

    '    If ret_code = 1 Then
    '        Return
    '    End If
    '    Dim debt_rows As Integer = no_of_rows
    '    Dim debt_table = table_array

    '    Dim outline As String = ""
    '    Dim outfile As String = "Our_Reference,Title,Forename,Surname,Address,Client,Scheme,TelNumber," & _
    '     "SecondNumber,Balance,ClientID,Name2" & vbNewLine
    '    Dim col_value As String = ""
    '    Dim phone_no As String = ""
    '    Dim idx, idx2, idx3 As Integer
    '    Dim phone_array(debt_rows) As String
    '    Dim outline_array(debt_rows) As String
    '    Dim outline_idx As Integer = 0
    '    Dim sch_no As String = ""
    '    Dim cl_no As String = ""
    '    For idx = 1 To debt_rows
    '        ProgressBar1.Value = (idx / debt_rows) * 100
    '        For idx2 = 1 To 13
    '            Dim col_result As String = ""

    '            col_value = Trim(debt_table(idx, idx2))
    '            If idx2 = 6 Then  'clientschemeID
    '                If cs_table(col_value, 4) <> 2 Then  'Branch has to be 2
    '                    outline = ""
    '                    Exit For
    '                End If
    '                cl_no = cs_table(col_value, 2)
    '                sch_no = cs_table(col_value, 3)
    '                col_result = Trim(client_table(cl_no, 2))
    '            ElseIf idx2 = 7 Then
    '                col_result = Trim(sch_table(sch_no, 2))
    '            ElseIf idx2 = 8 Then
    '                If phone_no = col_value And phone_no.Length > 0 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                phone_no = col_value
    '                col_result = col_value
    '            ElseIf idx2 = 9 Then
    '                If phone_no.Length = 0 Then
    '                    phone_no = col_value
    '                End If
    '                col_result = col_value
    '            ElseIf idx2 = 10 Then
    '                col_result = Format(col_value, "fixed")
    '            ElseIf idx2 = 11 Then
    '                col_result = cl_no
    '            ElseIf idx2 = 12 Then
    '            ElseIf idx2 = 13 Then
    '                col_result = col_value
    '            Else
    '                For idx3 = 1 To Len(col_value)
    '                    If Mid(col_value, idx3, 1) = vbCr Or _
    '                        Mid(col_value, idx3, 1) = "," Or _
    '                       Mid(col_value, idx3, 1) = vbLf Then
    '                        col_result = col_result & " "
    '                    Else
    '                        col_result = col_result & Mid(col_value, idx3, 1)
    '                    End If
    '                Next
    '            End If
    '            If idx2 <> 12 Then
    '                outline = outline & col_result & ","
    '            End If
    '        Next
    '        If Len(outline) > 0 Then
    '            outline_array(outline_idx) = outline
    '            phone_array(outline_idx) = phone_no
    '            outline_idx += 1
    '            outline = ""
    '        End If
    '    Next

    '    'sort arrays by phone numbers
    '    Array.Sort(phone_array, outline_array, 0, outline_idx)
    '    phone_no = ""
    '    ProgressBar1.Value = 5
    '    For idx = 0 To outline_idx - 1
    '        ProgressBar1.Value = (idx / outline_idx) * 100
    '        If phone_no = phone_array(idx) Then
    '            Continue For
    '        End If
    '        outline = outline_array(idx)
    '        If Len(outline) > 0 Then
    '            outfile = outfile & outline & vbNewLine
    '        End If
    '        phone_no = phone_array(idx)
    '    Next
    '    ProgressBar1.Value = 100
    '    'write out file
    '    With SaveFileDialog1
    '        .Title = "Save file"
    '        .Filter = "CSV files |*.csv"
    '        .DefaultExt = ".csv"
    '        .OverwritePrompt = True
    '        .InitialDirectory = "c:\temp"
    '        .FileName = "dialler.csv"
    '    End With

    '    Dim filepath As String = SaveFileDialog1.FileName
    '    If Len(outfile) = 0 Then
    '        MessageBox.Show("There are no cases selected")
    '    Else
    '        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
    '        End If
    '    End If
    '    Me.Close()
    'End Sub

    Private Sub agentbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles agentbtn.Click
        Dim resp As Object
        Try
            resp = InputBox("Enter agent number", "Select agent")
            If resp Is "" Then
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("agent number is invalid")
            Exit Sub
        End Try
        'get agent name from onestep
        Try
            agent_no = resp
        Catch ex As Exception
            MsgBox("agent number is invalid")
            Exit Sub
        End Try
        param1 = "onestep"
        param2 = "select name_fore, name_sur from Bailiff where _rowid = " & agent_no
        Dim agent_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox(agent_no & " does not exist on onestep")
            Exit Sub
        End If
        Dim agent_name As String = ""
        Dim forename As String = ""
        Try
            forename = Trim(agent_dataset.Tables(0).Rows(0).Item(0))
        Catch ex As Exception
            forename = ""
        End Try
        agent_name = forename & " " & Trim(agent_dataset.Tables(0).Rows(0).Item(1))
        If MsgBox("Extract cases allocated to " & agent_name, MsgBoxStyle.OkCancel, "Agent Confirmation") = MsgBoxResult.Ok Then
            agent_cases()
        Else
            MsgBox("selection of cases cancelled")
            Exit Sub
        End If
    End Sub
    Private Sub agent_cases()
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5

        mode = "L"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                " where status_open_closed = 'O' and status <> 'C' and status_hold = 'N'" & _
                                " and bail_current = 'Y' and (add_phone is not null or add_fax is not null " & _
                                " or empPhone is not null or empFax is not null) and bailiffID = " & agent_no

        produce_dialler_file(param2)
        Me.Close()
        'old_agent()
    End Sub
    'Private Sub old_agent()

    '    param1 = "onestep"
    '    param2 = "Debtor"
    '    param3 = "01_rowid 2name_title 3name_forename 4name_surname" _
    '          & " 5address 6clientschemeID 7schemeID 8add_phone 9add_fax" _
    '          & "10debt_balance 11clientID 12arrange_next 13name2"
    '    param4 = "where  status <> 'C' and status_hold = 'N' and status_open_closed = 'O'" _
    '           & " and bail_current = 'Y' and (add_phone is not null or add_fax is not null) and bailiffID = " & agent_no
    '    ret_code = get_table(param1, param2, param3, param4)

    '    If ret_code = 1 Then
    '        Return
    '    End If
    '    Dim debt_rows As Integer = no_of_rows
    '    Dim debt_table = table_array

    '    Dim outline As String = ""
    '    Dim outfile As String = "Our_Reference,Title,Forename,Surname,Address,Client,Scheme,TelNumber," & _
    '     "SecondNumber,Balance,ClientID,Name2" & vbNewLine
    '    Dim col_value As String = ""
    '    Dim phone_no As String = ""
    '    Dim idx, idx2, idx3 As Integer
    '    Dim phone_array(debt_rows) As String
    '    Dim outline_array(debt_rows) As String
    '    Dim outline_idx As Integer = 0
    '    Dim sch_no As String = ""
    '    Dim cl_no As String = ""
    '    For idx = 1 To debt_rows
    '        ProgressBar1.Value = (idx / debt_rows) * 100
    '        For idx2 = 1 To 13
    '            Dim col_result As String = ""

    '            col_value = Trim(debt_table(idx, idx2))
    '            If idx2 = 6 Then  'clientschemeID
    '                'If cs_table(col_value, 4) <> 2 Then  'Branch has to be 2
    '                '    outline = ""
    '                '    Exit For
    '                'End If
    '                cl_no = cs_table(col_value, 2)
    '                sch_no = cs_table(col_value, 3)
    '                col_result = Trim(client_table(cl_no, 2))
    '            ElseIf idx2 = 7 Then
    '                col_result = Trim(sch_table(sch_no, 2))
    '            ElseIf idx2 = 8 Then
    '                If phone_no = col_value And phone_no.Length > 0 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                phone_no = col_value
    '                col_result = col_value
    '            ElseIf idx2 = 9 Then
    '                If phone_no.Length = 0 Then
    '                    phone_no = col_value
    '                End If
    '                col_result = col_value
    '            ElseIf idx2 = 10 Then
    '                col_result = Format(col_value, "fixed")
    '            ElseIf idx2 = 11 Then
    '                col_result = cl_no
    '            ElseIf idx2 = 12 Then
    '                'Dim col_date As Date
    '                'Try
    '                '    col_date = CDate(col_value)
    '                'Catch ex As Exception
    '                '    outline = ""
    '                '    Exit For
    '                'End Try
    '                'If col_date >= Now Then
    '                '    outline = ""
    '                '    Exit For
    '                'End If
    '                'If DateDiff("d", Now, col_date) >= 0 Or _
    '                '    DateDiff("d", Now, col_date) < -7 Then
    '                '    outline = ""
    '                '    Exit For
    '                'End If
    '            ElseIf idx2 = 13 Then
    '                col_result = col_value
    '            Else
    '                For idx3 = 1 To Len(col_value)
    '                    If Mid(col_value, idx3, 1) = vbCr Or _
    '                        Mid(col_value, idx3, 1) = "," Or _
    '                       Mid(col_value, idx3, 1) = vbLf Then
    '                        col_result = col_result & " "
    '                    Else
    '                        col_result = col_result & Mid(col_value, idx3, 1)
    '                    End If
    '                Next
    '            End If
    '            If idx2 <> 12 Then
    '                outline = outline & col_result & ","
    '            End If
    '        Next
    '        If Len(outline) > 0 Then
    '            outline_array(outline_idx) = outline
    '            phone_array(outline_idx) = phone_no
    '            outline_idx += 1
    '            outline = ""
    '        End If
    '    Next

    '    'sort arrays by phone numbers
    '    Array.Sort(phone_array, outline_array, 0, outline_idx)
    '    phone_no = ""
    '    ProgressBar1.Value = 5
    '    For idx = 0 To outline_idx - 1
    '        ProgressBar1.Value = (idx / outline_idx) * 100
    '        If phone_no = phone_array(idx) Then
    '            Continue For
    '        End If
    '        outline = outline_array(idx)
    '        If Len(outline) > 0 Then
    '            outfile = outfile & outline & vbNewLine
    '        End If
    '        phone_no = phone_array(idx)
    '    Next
    '    ProgressBar1.Value = 100
    '    'write out file
    '    With SaveFileDialog1
    '        .Title = "Save file"
    '        .Filter = "CSV files |*.csv"
    '        .DefaultExt = ".csv"
    '        .OverwritePrompt = True
    '        .InitialDirectory = "c:\temp"
    '        .FileName = "dialler.csv"
    '    End With

    '    Dim filepath As String = SaveFileDialog1.FileName
    '    If Len(outfile) = 0 Then
    '        MessageBox.Show("There are no cases selected")
    '    Else
    '        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
    '        End If
    '    End If
    '    Me.Close()
    'End Sub

    Private Sub ooabtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ooabtn.Click
        Me.Hide()
        ooafrm.Show()
    End Sub
    Sub disable_buttons()
        adhoc90btn.Enabled = False
        br1_compbtn.Enabled = False
        br1_enfbtn.Enabled = False
        br1_postenfbtn.Enabled = False
        mar_compbtn.Enabled = False
        mar_enfbtn.Enabled = False
        mar_postenfbtn.Enabled = False
        sw_compbtn.Enabled = False
        sw_enfbtn.Enabled = False
        sw_postenfbtn.Enabled = False
        southendbtn.Enabled = False
        ret_pendingbtn.Enabled = False
        SNNDRbtn.Enabled = False
        RNNDRBtn.Enabled = False
        MNNdrBtn.Enabled = False
        swcompbtn.Enabled = False
        swfebtn.Enabled = False
        swfenfbtn.Enabled = False
        swfrbtn.Enabled = False
        swrem1btn.Enabled = False
        swrem2btn.Enabled = False
        swretbtn.Enabled = False
        swrunexcbtn.Enabled = False
        swpc1btn.Enabled = False
        swpc2btn.Enabled = False

        Mexceptionbtn.Enabled = False
        exceptionbtn.Enabled = False
        allhbopbtn.Enabled = False
        allsdbtn.Enabled = False
        allftabtn.Enabled = False
        eonS2.Enabled = False
        EONS1B.Enabled = False
        eonS1A.Enabled = False
        outboundbtn.Enabled = False
        brokenbtn.Enabled = False
        softtracebtn.Enabled = False
        hello4to8btn.Enabled = False
        hello8plusbtn.Enabled = False
        prioritybtn.Enabled = False
        top20btn.Enabled = False
        paymentsbtn.Enabled = False

        clntbtn.Enabled = False
        arrbtn.Enabled = False
        exitbtn.Enabled = False
        agentbtn.Enabled = False
        ooabtn.Enabled = False

        otherbtn.Enabled = False

        returnbtn.Enabled = False
        cmec_ooabtn.Enabled = False
        cmecbtn.Enabled = False

        bail_retnbtn.Enabled = False

        hmrcbtn.Enabled = False
        feesbtn.Enabled = False
        lsc_camp1btn.Enabled = False
        lsc_camp2btn.Enabled = False
        lsc_defaultedbtn.Enabled = False
        lsc_fdcbtn.Enabled = False
        lsc_crystalbtn.Enabled = False
        rtn_not_cmecbtn.Enabled = False
        cco_btn.Enabled = False
        arrow_sch_grpbtn.Enabled = False
        cpw_schbtn.Enabled = False
        outcallbtn.Enabled = False
        vulnerbtn.Enabled = False
        UUBtn.Enabled = False
        tma_rbtn.Enabled = False
        hbop_mbtn.Enabled = False
        tmabtn.Enabled = False
        hmrcagebtn.Enabled = False
        precom1btn.Enabled = False
        precom2btn.Enabled = False
        fvanbtn.Enabled = False
        rtdfvanbtn.Enabled = False
        fsbtn.Enabled = False
        dvlabtn.Enabled = False
        arrow_by_agebtn.Enabled = False
        lowellbtn.Enabled = False
        dvlabrokenbtn.Enabled = False
        slbtn.Enabled = False
        etcbtn.Enabled = False
        awbtn.Enabled = False
        etc_brokenbtn.Enabled = False
        listbtn.Enabled = False
        segmentbtn.Enabled = False
        appealbtn.Enabled = False
        compbtn7.Enabled = False
        remindbtn.Enabled = False
        remind23btn.Enabled = False
        precomL1btn.Enabled = False
        retnbtn.Enabled = False
        hmrc_batchbtn.Enabled = False
        finrembtn.Enabled = False
        finenfbtn.Enabled = False
        Mcompbtn.Enabled = False
        Mremindbtn.Enabled = False
        MfinalWbtn.Enabled = False
        lowellnotvulbtn.Enabled = False
        SLNotvul.Enabled = False
        LAANotvul.Enabled = False
        HMRCnotvul.Enabled = False
        enfletterbtn.Enabled = False
        precommL2btn.Enabled = False
        MenfLetterbtn.Enabled = False
        MfinalWbtn.Enabled = False
        Mfinenfbtn.Enabled = False
        MfinRemindbtn.Enabled = False
        MpcommL1btn.Enabled = False
        MprecommL2.Enabled = False
        Mretnbtn.Enabled = False
        furtherenfbtn.Enabled = False
        Mfurtherenfbtn.Enabled = False
        noasentbtn.Enabled = False
        slcs2letterbtn.Enabled = False
        slcs3btn.Enabled = False
        hmrcintrobtn.Enabled = False
        hmrcl2btn.Enabled = False
        hmrcl3btn.Enabled = False
        hmrcl4btn.Enabled = False
        hmrcsw1btn.Enabled = False
        hmrcsw2btn.Enabled = False
        laal1btn.Enabled = False
        laa2btn.Enabled = False
        laa3btn.Enabled = False
        lowells1lbtn.Enabled = False
        lowells2btn.Enabled = False
        lowells3btn.Enabled = False
        lowell90btn.Enabled = False
        adhocbabtn.Enabled = False
        adhoc90btn.Enabled = False
        uusl1btn.Enabled = False
        uusl2btn.Enabled = False
        uusl3btn.Enabled = False
        uusl4btn.Enabled = False
    End Sub
    Sub enable_buttons()
        adhoc90btn.Enabled = True
        br1_compbtn.Enabled = True
        br1_enfbtn.Enabled = True
        br1_postenfbtn.Enabled = True
        sw_compbtn.Enabled = True
        sw_enfbtn.Enabled = True
        sw_postenfbtn.Enabled = True
        southendbtn.Enabled = True
        ret_pendingbtn.Enabled = True
        SNNDRbtn.Enabled = True
        RNNDRBtn.Enabled = True
        MNNdrBtn.Enabled = True
        swcompbtn.Enabled = True
        swfebtn.Enabled = True
        swfenfbtn.Enabled = True
        swfrbtn.Enabled = True
        swrem2btn.Enabled = True
        swrem1btn.Enabled = True
        swretbtn.Enabled = True
        swrunexcbtn.Enabled = True
        swpc1btn.Enabled = True
        swpc2btn.Enabled = True

        Mexceptionbtn.Enabled = True
        exceptionbtn.Enabled = True
        allhbopbtn.Enabled = True
        allsdbtn.Enabled = True
        allftabtn.Enabled = True
        eonS2.Enabled = True
        EONS1B.Enabled = True
        eonS1A.Enabled = True
        outboundbtn.Enabled = True
        brokenbtn.Enabled = True
        softtracebtn.Enabled = True
        hello4to8btn.Enabled = True
        hello8plusbtn.Enabled = True
        prioritybtn.Enabled = True
        top20btn.Enabled = True
        paymentsbtn.Enabled = True
        clntbtn.Enabled = True
        arrbtn.Enabled = True
        exitbtn.Enabled = True
        agentbtn.Enabled = True
        ooabtn.Enabled = True

        otherbtn.Enabled = True

        returnbtn.Enabled = True
        cmec_ooabtn.Enabled = True
        cmecbtn.Enabled = True

        bail_retnbtn.Enabled = True
        hmrcbtn.Enabled = True
        feesbtn.Enabled = True
        lsc_camp1btn.Enabled = True
        lsc_camp2btn.Enabled = True
        lsc_defaultedbtn.Enabled = True
        lsc_fdcbtn.Enabled = True
        lsc_crystalbtn.Enabled = True
        rtn_not_cmecbtn.Enabled = True
        cco_btn.Enabled = True
        arrow_sch_grpbtn.Enabled = True
        cpw_schbtn.Enabled = True
        outcallbtn.Enabled = True
        vulnerbtn.Enabled = True
        UUBtn.Enabled = True
        tma_rbtn.Enabled = True
        hbop_mbtn.Enabled = True
        tmabtn.Enabled = True
        hmrcagebtn.Enabled = True
        precom1btn.Enabled = True
        precom2btn.Enabled = True
        fvanbtn.Enabled = True
        rtdfvanbtn.Enabled = True
        fsbtn.Enabled = True
        dvlabtn.Enabled = True
        arrow_by_agebtn.Enabled = True
        lowellbtn.Enabled = True
        dvlabrokenbtn.Enabled = True
        slbtn.Enabled = True
        etcbtn.Enabled = True
        awbtn.Enabled = True
        etc_brokenbtn.Enabled = True
        listbtn.Enabled = True
        segmentbtn.Enabled = True
        appealbtn.Enabled = True
        compbtn7.Enabled = True
        remindbtn.Enabled = True
        remind23btn.Enabled = True
        precomL1btn.Enabled = True
        retnbtn.Enabled = True
        hmrc_batchbtn.Enabled = True
        finrembtn.Enabled = True
        finenfbtn.Enabled = True
        Mcompbtn.Enabled = True
        Mremindbtn.Enabled = True
        MfinalWbtn.Enabled = True
        lowellnotvulbtn.Enabled = True
        SLNotvul.Enabled = True
        LAANotvul.Enabled = True
        HMRCnotvul.Enabled = True
        enfletterbtn.Enabled = True
        precommL2btn.Enabled = True
        MenfLetterbtn.Enabled = True
        MfinRemindbtn.Enabled = True
        Mfinenfbtn.Enabled = True
        MfinalWbtn.Enabled = True
        MpcommL1btn.Enabled = True
        MprecommL2.Enabled = True
        Mretnbtn.Enabled = True
        furtherenfbtn.Enabled = True
        Mfurtherenfbtn.Enabled = True
        noasentbtn.Enabled = True
        slcs2letterbtn.Enabled = True
        hmrcintrobtn.Enabled = True
        hmrcl2btn.Enabled = True
        slcs3btn.Enabled = True
        hmrcl3btn.Enabled = True
        hmrcl4btn.Enabled = True
        hmrcsw1btn.Enabled = True
        hmrcsw2btn.Enabled = True
        laal1btn.Enabled = True
        laa2btn.Enabled = True
        laa3btn.Enabled = True
        lowells1lbtn.Enabled = True
        lowells2btn.Enabled = True
        lowells3btn.Enabled = True
        lowell90btn.Enabled = True
        adhocbabtn.Enabled = True
        adhoc90btn.Enabled = True
        uusl1btn.Enabled = True
        uusl2btn.Enabled = True
        uusl3btn.Enabled = True
        uusl4btn.Enabled = True
    End Sub

    Private Sub csabtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "K"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
        'old_csa()
    End Sub
    'Private Sub old_csa()

    '    param1 = "onestep"

    '    'get all CSA clients
    '    param2 = "select _rowid from Client where name like '%CSA%'"
    '    Dim cl_dataset As DataSet = get_dataset(param1, param2)
    '    If no_of_rows = 0 Then
    '        MsgBox("No CSA clients found")
    '        Exit Sub
    '    End If
    '    'now get all csids
    '    Dim idx, idx2, idx3 As Integer
    '    Dim cl_rows As Integer = no_of_rows
    '    Dim csid_rows As Integer = 0
    '    Dim csid_array(20) As Integer
    '    Dim phone_no As String = ""
    '    Dim phone_array(1) As String
    '    Dim outline_array(1) As String
    '    Dim outline_idx As Integer = 0
    '    Dim outline As String
    '    Dim outfile As String = ""
    '    For idx = 0 To cl_rows - 1
    '        param2 = "select _rowid from ClientScheme where clientID = " & cl_dataset.Tables(0).Rows(idx).Item(0)
    '        Dim csid_dataset As DataSet = get_dataset(param1, param2)
    '        For idx2 = 0 To no_of_rows - 1
    '            csid_array(csid_rows) = csid_dataset.Tables(0).Rows(idx2).Item(0)
    '            csid_rows += 1
    '        Next
    '    Next
    '    Dim csid_idx As Integer
    '    outline_idx = 0
    '    outline = ""
    '    Dim tot_rows As Integer = 0
    '    For csid_idx = 0 To csid_rows
    '        param2 = "Debtor"
    '        param3 = "01_rowid 2name_title 3name_forename 4name_surname" _
    '              & " 5address 6clientschemeID 7schemeID 8add_phone 9add_fax" _
    '              & "10debt_balance 11clientID 12name2"
    '        param4 = "where  status = 'L' and status_hold = 'N' and status_open_closed = 'O'" _
    '               & " and bail_current = 'N' and (add_phone is not null or add_fax is not null)" _
    '               & " and clientschemeID = " & csid_array(csid_idx)
    '        ret_code = get_table(param1, param2, param3, param4)
    '        If ret_code = 1 Then
    '            Return
    '        End If
    '        Dim debt_rows As Integer = no_of_rows

    '        tot_rows += no_of_rows
    '        ReDim Preserve outline_array(tot_rows)

    '        Dim debt_table = table_array
    '        outfile = "Our_Reference,Title,Forename,Surname,Address,Client,Scheme,TelNumber," & _
    '          "SecondNumber,Balance,ClientID,Name2" & vbNewLine
    '        Dim col_value As String = ""
    '        phone_no = ""
    '        ReDim Preserve phone_array(tot_rows)

    '        Dim sch_no As String = ""
    '        Dim cl_no As String = ""
    '        For idx = 1 To debt_rows
    '            ProgressBar1.Value = (idx / debt_rows) * 100
    '            For idx2 = 1 To 12
    '                Dim col_result As String = ""
    '                col_value = Trim(debt_table(idx, idx2))
    '                If idx2 = 1 Then
    '                    col_result = col_value
    '                    'check that 7 day letter sent > 5days ago or van on way letter sent
    '                    param2 = " select _createdDate, text from Note where type = 'Letter'" & _
    '                    " and debtorID = " & col_result
    '                    Dim note_dataset As DataSet = get_dataset(param1, param2)
    '                    If no_of_rows = 0 Then
    '                        outline = ""
    '                        Exit For
    '                    End If
    '                    Dim letter_found As Boolean = False
    '                    For idx3 = 0 To no_of_rows - 1
    '                        Dim note_text As String = note_dataset.Tables(0).Rows(idx3).Item(1)
    '                        If InStr(note_text, "VanOnWay") > 0 Then
    '                            letter_found = True
    '                            Exit For
    '                        End If
    '                        If InStr(note_text, "7Day") > 0 Then
    '                            Dim note_date As Date = note_dataset.Tables(0).Rows(idx3).Item(0)
    '                            If DateDiff(DateInterval.Day, note_date, Now) > 5 Then
    '                                letter_found = True
    '                                Exit For
    '                            End If
    '                        End If
    '                    Next
    '                    If letter_found = False Then
    '                        outline = ""
    '                        Exit For
    '                    End If
    '                ElseIf idx2 = 6 Then  'clientschemeID
    '                    cl_no = cs_table(col_value, 2)
    '                    sch_no = cs_table(col_value, 3)
    '                    col_result = Trim(client_table(cl_no, 2))
    '                ElseIf idx2 = 7 Then
    '                    col_result = Trim(sch_table(sch_no, 2))
    '                ElseIf idx2 = 8 Then
    '                    If phone_no = col_value And phone_no.Length > 0 Then
    '                        outline = ""
    '                        Exit For
    '                    End If
    '                    phone_no = col_value
    '                    col_result = col_value
    '                ElseIf idx2 = 9 Then
    '                    col_result = col_value
    '                    If phone_no.Length = 0 Then
    '                        phone_no = col_value
    '                    End If
    '                ElseIf idx2 = 10 Then
    '                    col_result = Format(col_value, "fixed")
    '                ElseIf idx2 = 11 Then
    '                    col_result = cl_no
    '                ElseIf idx2 = 12 Then
    '                    col_result = col_value
    '                Else
    '                    For idx3 = 1 To Len(col_value)
    '                        If Mid(col_value, idx3, 1) = vbCr Or _
    '                            Mid(col_value, idx3, 1) = "," Or _
    '                           Mid(col_value, idx3, 1) = vbLf Then
    '                            col_result = col_result & " "
    '                        Else
    '                            col_result = col_result & Mid(col_value, idx3, 1)
    '                        End If
    '                    Next
    '                End If
    '                outline = outline & col_result & ","
    '            Next
    '            If Len(outline) > 0 Then
    '                outline_array(outline_idx) = outline
    '                phone_array(outline_idx) = phone_no
    '                outline_idx += 1
    '                outline = ""
    '            End If
    '        Next
    '    Next
    '    'sort arrays by phone numbers
    '    Array.Sort(phone_array, outline_array, 0, outline_idx)
    '    phone_no = ""
    '    ProgressBar1.Value = 5
    '    For idx = 0 To outline_idx - 1
    '        ProgressBar1.Value = (idx / outline_idx) * 100
    '        If phone_no = phone_array(idx) Then
    '            Continue For
    '        End If
    '        outline = outline_array(idx)
    '        If Len(outline) > 0 Then
    '            outfile = outfile & outline & vbNewLine
    '        End If
    '        phone_no = phone_array(idx)
    '    Next
    '    'write out file
    '    With SaveFileDialog1
    '        .Title = "Save file"
    '        .Filter = "CSV files |*.csv"
    '        .DefaultExt = ".csv"
    '        .OverwritePrompt = True
    '        .InitialDirectory = "c:\temp"
    '        .FileName = "dialler.csv"
    '    End With
    '    ProgressBar1.Value = 100
    '    Dim filepath As String = SaveFileDialog1.FileName
    '    If Len(outfile) = 0 Then
    '        MessageBox.Show("There are no cases selected")
    '    Else
    '        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
    '        End If
    '    End If
    '    Me.Close()
    'End Sub

    Private Sub spec_hitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()

        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "S"
        Dim sevendays_ago As Date = DateAdd(DateInterval.Day, -7, Now)
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                 " and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                 " or empPhone is not null or empFax is not null) and debt_balance > 2.5" & _
                 " and clientschemeID <> 265 and clientschemeID <> 266 " & _
                 " and debt_fees > 22.5 and clientschemeID <> 95 and clientschemeID <> 96" & _
                 " and last_date < '" & Format(sevendays_ago, "yyyy-MM-dd") & "'"
        produce_dialler_file(param2)
        Me.Close()
        'oldspec_hitbtn()
    End Sub
    'Private Sub oldspec_hitbtn()
    '    postcodefrm.Out_of_areaTableAdapter.Fill(postcodefrm.FeesSQLDataSet.Out_of_area)
    '    'get stage names
    '    param2 = "Stage"
    '    param3 = "01_rowid 2name"
    '    param4 = "order by _rowid"
    '    ret_code = get_table(param1, param2, param3, param4)
    '    If ret_code = 1 Then
    '        Return
    '    End If
    '    Dim stage_array = table_array

    '    Dim sevendays_ago As Date = DateAdd(DateInterval.Day, -7, Now)
    '    param1 = "onestep"
    '    param2 = "Debtor"
    '    param3 = "01_rowid 2name_title 3name_forename 4name_surname" _
    '          & " 5address 6clientschemeID 7schemeID 8add_phone 9add_fax" _
    '          & "10debt_balance 11clientID 12arrange_next 13name2 14add_postcode 15last_stageID"
    '    param4 = "where  status = 'L' and status_hold = 'N' and status_open_closed = 'O'" _
    '          & " and bail_current = 'N' and (add_phone is not null or add_fax is not null) and debt_balance > 2.5" _
    '           & " and debt_fees > 22.5" _
    '           & " and last_date < '" & Format(sevendays_ago, "yyyy.MM.dd") & "'"
    '    ret_code = get_table(param1, param2, param3, param4)

    '    If ret_code = 1 Then
    '        Return
    '    End If
    '    Dim debt_rows As Integer = no_of_rows
    '    Dim debt_table = table_array

    '    Dim outline As String = ""
    '    Dim outfile As String = "Our_Reference,Title,Forename,Surname,Address,Client,Scheme,TelNumber," & _
    '      "SecondNumber,Balance,ClientID,Name2" & vbNewLine
    '    Dim col_value As String = ""
    '    Dim phone_no As String = ""
    '    Dim idx, idx2, idx3 As Integer
    '    Dim phone_array(debt_rows) As String
    '    Dim outline_array(debt_rows) As String
    '    Dim outline_idx As Integer = 0

    '    For idx = 1 To debt_rows
    '        ProgressBar1.Value = (idx / debt_rows) * 100
    '        Dim last_stageid As Integer = debt_table(idx, 15)

    '        'If Trim(stage_array(last_stageid, 2)) <> "(Return)" _
    '        'And Trim(stage_array(last_stageid, 2)) <> "AwaitingVanApproval" _
    '        'And Trim(stage_array(last_stageid, 2)) <> "FurtherVanAttendance" _
    '        'And Trim(stage_array(last_stageid, 2)) <> "Van Notice sent" _
    '        'And Trim(stage_array(last_stageid, 2)) <> "Van Attendance" Then

    '        If Trim(stage_array(last_stageid, 2)) <> "FurtherVanAttendance" _
    '        And Trim(stage_array(last_stageid, 2)) <> "Van Attendance" Then
    '            Continue For
    '        End If
    '        Dim sch_no As String = ""
    '        Dim cl_no As String = ""
    '        Dim debtor As Integer
    '        For idx2 = 1 To 14
    '            Dim col_result As String = ""
    '            col_value = Trim(debt_table(idx, idx2))
    '            If idx2 = 1 Then
    '                debtor = col_value
    '            End If
    '            If idx2 = 6 Then  'clientschemeID
    '                If cs_table(col_value, 4) <> 1 Then  'Branch has to be 1
    '                    outline = ""
    '                    Exit For
    '                End If
    '                cl_no = cs_table(col_value, 2)
    '                If cl_no <> 50 And cl_no <> 54 And cl_no <> 82 And cl_no <> 141 Then 'Manchester/Lpool/Nottm/Trafford
    '                    outline = ""
    '                    Exit For
    '                End If
    '                sch_no = cs_table(col_value, 3)
    '                'ignore road traffic schenes
    '                If sch_no = 41 Or sch_no = 676 Or sch_no = 773 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                col_result = Trim(client_table(cl_no, 2))
    '            ElseIf idx2 = 7 Then
    '                col_result = Trim(sch_table(sch_no, 2))
    '            ElseIf idx2 = 8 Then
    '                If col_value.Length < 5 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                If phone_no = col_value And phone_no.Length > 0 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                phone_no = col_value
    '                col_result = col_value
    '            ElseIf idx2 = 9 Then
    '                If phone_no.Length = 0 Then
    '                    phone_no = col_value
    '                End If
    '                col_result = col_value
    '            ElseIf idx2 = 10 Then
    '                col_result = Format(col_value, "fixed")
    '            ElseIf idx2 = 11 Then
    '                col_result = cl_no
    '            ElseIf idx2 = 12 Then
    '                Continue For
    '            ElseIf idx2 = 13 Then
    '                col_result = col_value
    '            ElseIf idx2 = 14 Then
    '                'check postcode sector
    '                If col_value.Length > 0 Then
    '                    If found_pc_sector(col_value) = True Then
    '                        outline = ""
    '                        Exit For
    '                    End If
    '                End If
    '            Else
    '                For idx3 = 1 To Len(col_value)
    '                    If Mid(col_value, idx3, 1) = vbCr Or _
    '                        Mid(col_value, idx3, 1) = "," Or _
    '                       Mid(col_value, idx3, 1) = vbLf Then
    '                        col_result = col_result & " "
    '                    Else
    '                        col_result = col_result & Mid(col_value, idx3, 1)
    '                    End If
    '                Next
    '            End If
    '            If idx2 <> 12 And idx2 <> 14 Then
    '                outline = outline & col_result & ","
    '            End If
    '        Next
    '        If Len(outline) > 0 Then
    '            'check that debtor does not just have a levy fee - must have van as well
    '            param2 = "select type from Fee where debtorID = " & debtor & _
    '            " and fee_amount > 0"
    '            Dim fee_dataset As DataSet = get_dataset(param1, param2)
    '            Dim fee_idx As Integer
    '            Dim levy_found As Boolean = False
    '            Dim van_fees_found As Boolean = False
    '            For fee_idx = 0 To no_of_rows - 1
    '                Dim fee_type As String = LCase(fee_dataset.Tables(0).Rows(fee_idx).Item(0))
    '                If InStr(fee_type, "lev") > 0 Then
    '                    levy_found = True
    '                ElseIf InStr(fee_type, "van") > 0 Then
    '                    van_fees_found = True
    '                End If
    '            Next
    '            If levy_found And Not van_fees_found Then
    '                outline = ""
    '            Else
    '                outline_array(outline_idx) = outline
    '                phone_array(outline_idx) = phone_no
    '                outline_idx += 1
    '                outline = ""
    '            End If
    '        End If
    '    Next

    '    'sort arrays by phone numbers
    '    Array.Sort(phone_array, outline_array, 0, outline_idx)
    '    phone_no = ""
    '    ProgressBar1.Value = 5
    '    For idx = 0 To outline_idx - 1
    '        ProgressBar1.Value = (idx / outline_idx) * 100
    '        If phone_no = phone_array(idx) Then
    '            Continue For
    '        End If
    '        outline = outline_array(idx)
    '        If Len(outline) > 0 Then
    '            outfile = outfile & outline & vbNewLine
    '        End If
    '        phone_no = phone_array(idx)
    '    Next
    '    ProgressBar1.Value = 100
    '    'write out file
    '    With SaveFileDialog1
    '        .Title = "Save file"
    '        .Filter = "CSV files |*.csv"
    '        .DefaultExt = ".csv"
    '        .OverwritePrompt = True
    '        .InitialDirectory = "c:\temp"
    '        .FileName = "dialler.csv"
    '    End With

    '    Dim filepath As String = SaveFileDialog1.FileName
    '    If Len(outfile) = 0 Then
    '        MessageBox.Show("There are no cases selected")
    '    Else
    '        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
    '        End If
    '    End If
    '    Me.Close()
    'End Sub

    Private Sub otherbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles otherbtn.Click
        mode = "O"
        collect_stage.ShowDialog()
    End Sub

    Private Sub maintbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'clientupdfrm.ShowDialog()
    End Sub

    'Private Sub brokenbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    disable_buttons()
    '    ProgressBar1.Visible = True
    '    ProgressBar1.Value = 5
    '    param1 = "onestep"
    '    param2 = "Debtor"
    '    param3 = "01_rowid 2name_title 3name_forename 4name_surname" _
    '          & " 5address 6clientschemeID 7schemeID 8add_phone 9add_fax" _
    '          & "10debt_balance 11clientID 12arrange_next 13name2"
    '    param4 = "where  status = 'L' and status_hold = 'N' and status_open_closed = 'O'" _
    '           & " and bail_current = 'N' and (add_phone is not null or add_fax is not null)" _
    '           & " and arrange_broken = 'Y' and debtPaid = 0"
    '    ret_code = get_table(param1, param2, param3, param4)

    '    If ret_code = 1 Then
    '        Return
    '    End If
    '    Dim debt_rows As Integer = no_of_rows
    '    Dim debt_table = table_array

    '    Dim outline As String = ""
    '    Dim outfile As String = "Our_Reference,Title,Forename,Surname,Address,Client,Scheme,TelNumber," & _
    '     "SecondNumber,Balance,ClientID,Name2" & vbNewLine
    '    Dim col_value As String = ""
    '    Dim phone_no As String = ""
    '    Dim idx, idx2, idx3 As Integer
    '    Dim phone_array(debt_rows) As String
    '    Dim outline_array(debt_rows) As String
    '    Dim outline_idx As Integer = 0
    '    Dim sch_no As String = ""
    '    Dim cl_no As String = ""
    '    For idx = 1 To debt_rows
    '        ProgressBar1.Value = (idx / debt_rows) * 100
    '        For idx2 = 1 To 13
    '            Dim col_result As String = ""

    '            col_value = Trim(debt_table(idx, idx2))
    '            If idx2 = 6 Then  'clientschemeID
    '                If cs_table(col_value, 4) <> 2 Then  'Branch has to be 2
    '                    outline = ""
    '                    Exit For
    '                End If
    '                cl_no = cs_table(col_value, 2)
    '                sch_no = cs_table(col_value, 3)
    '                col_result = Trim(client_table(cl_no, 2))
    '            ElseIf idx2 = 7 Then
    '                col_result = Trim(sch_table(sch_no, 2))
    '            ElseIf idx2 = 8 Then
    '                If phone_no = col_value And phone_no.Length > 0 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                phone_no = col_value
    '                If Microsoft.VisualBasic.Right(phone_no, 1) = Chr(10) _
    '                    Or Microsoft.VisualBasic.Right(phone_no, 1) = Chr(13) Then
    '                    phone_no = Microsoft.VisualBasic.Left(phone_no, phone_no.Length - 1)
    '                End If
    '                If Microsoft.VisualBasic.Right(phone_no, 1) = Chr(10) _
    '                    Or Microsoft.VisualBasic.Right(phone_no, 1) = Chr(13) Then
    '                    phone_no = Microsoft.VisualBasic.Left(phone_no, phone_no.Length - 1)
    '                End If
    '                col_result = phone_no
    '            ElseIf idx2 = 9 Then
    '                col_result = col_value
    '                If Microsoft.VisualBasic.Right(col_result, 1) = Chr(10) _
    '                    Or Microsoft.VisualBasic.Right(col_result, 1) = Chr(13) Then
    '                    col_result = Microsoft.VisualBasic.Left(col_result, col_result.Length - 1)
    '                End If
    '                If Microsoft.VisualBasic.Right(col_result, 1) = Chr(10) _
    '                                        Or Microsoft.VisualBasic.Right(col_result, 1) = Chr(13) Then
    '                    col_result = Microsoft.VisualBasic.Left(col_result, col_result.Length - 1)
    '                End If
    '            ElseIf idx2 = 10 Then
    '                col_result = Format(col_value, "fixed")
    '            ElseIf idx2 = 11 Then
    '                col_result = cl_no
    '            ElseIf idx2 = 13 Then
    '                col_result = col_value
    '            Else
    '                For idx3 = 1 To Len(col_value)
    '                    If Mid(col_value, idx3, 1) = vbCr Or _
    '                        Mid(col_value, idx3, 1) = "," Or _
    '                       Mid(col_value, idx3, 1) = vbLf Then
    '                        col_result = col_result & " "
    '                    Else
    '                        col_result = col_result & Mid(col_value, idx3, 1)
    '                    End If
    '                Next
    '            End If
    '            If idx2 = 8 Then
    '                Continue For
    '            End If
    '            If idx2 = 9 Then
    '                If phone_no.Length = 0 Then
    '                    If col_result.Length = 0 Then
    '                        outline = ""
    '                        Exit For
    '                    End If
    '                    phone_no = col_result
    '                    outline = outline & phone_no & "," & "" & ","
    '                Else
    '                    outline = outline & phone_no & "," & col_result & ","
    '                End If
    '            Else
    '                If idx2 <> 12 And idx2 <> 14 Then
    '                    outline = outline & col_result & ","
    '                End If
    '            End If
    '        Next
    '        If Len(outline) > 0 Then
    '            outline_array(outline_idx) = outline
    '            phone_array(outline_idx) = phone_no
    '            outline_idx += 1
    '            outline = ""
    '        End If
    '    Next

    '    'sort arrays by phone numbers
    '    ProgressBar1.Value = 5
    '    Array.Sort(phone_array, outline_array, 0, outline_idx)
    '    phone_no = ""
    '    For idx = 0 To outline_idx - 1
    '        ProgressBar1.Value = (idx / outline_idx) * 100
    '        If phone_no = phone_array(idx) Then
    '            Continue For
    '        End If
    '        outline = outline_array(idx)
    '        If Len(outline) > 0 Then
    '            outfile = outfile & outline & vbNewLine
    '        End If
    '        phone_no = phone_array(idx)
    '    Next
    '    ProgressBar1.Value = 100
    '    'write out file
    '    With SaveFileDialog1
    '        .Title = "Save file"
    '        .Filter = "CSV files |*.csv"
    '        .DefaultExt = ".csv"
    '        .OverwritePrompt = True
    '        .InitialDirectory = "c:\temp"
    '        .FileName = "dialler.csv"
    '    End With

    '    Dim filepath As String = SaveFileDialog1.FileName
    '    If Len(outfile) = 0 Then
    '        MessageBox.Show("There are no cases selected")
    '    Else
    '        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
    '        End If
    '    End If
    '    Me.Close()
    'End Sub

    Private Sub rtdbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "R"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                 " and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                 " or empPhone is not null or empFax is not null) and debt_balance > 2.5"

        produce_dialler_file(param2)
        Me.Close()
        'old_rtd()
    End Sub
    'Private Sub old_rtd()

    '    postcodefrm.Out_of_areaTableAdapter.Fill(postcodefrm.FeesSQLDataSet.Out_of_area)
    '    'get stage names
    '    param2 = "Stage"
    '    param3 = "01_rowid 2name"
    '    param4 = "order by _rowid"
    '    ret_code = get_table(param1, param2, param3, param4)
    '    If ret_code = 1 Then
    '        Return
    '    End If
    '    Dim stage_array = table_array

    '    param1 = "onestep"
    '    param2 = "Debtor"
    '    param3 = "01_rowid 2name_title 3name_forename 4name_surname" _
    '          & " 5address 6clientschemeID 7schemeID 8add_phone 9add_fax" _
    '          & "10debt_balance 11clientID 12arrange_next 13name2 14add_postcode 15last_stageID 16status_nextdate"
    '    param4 = "where  status = 'L' and status_hold = 'N' and status_open_closed = 'O'" _
    '           & " and bail_current = 'N'  and (add_phone is not null or add_fax is not null) and debt_balance > 2.5"
    '    ret_code = get_table(param1, param2, param3, param4)

    '    If ret_code = 1 Then
    '        Return
    '    End If
    '    Dim debt_rows As Integer = no_of_rows
    '    Dim debt_table = table_array

    '    Dim outline As String = ""
    '    Dim outfile As String = "Our_Reference,Title,Forename,Surname,Address,Client,Scheme,TelNumber," & _
    '      "SecondNumber,Balance,ClientID,Name2" & vbNewLine
    '    Dim col_value As String = ""
    '    Dim phone_no As String = ""
    '    Dim idx, idx2, idx3 As Integer
    '    Dim phone_array(debt_rows) As String
    '    Dim outline_array(debt_rows) As String
    '    Dim outline_idx As Integer = 0

    '    For idx = 1 To debt_rows
    '        ProgressBar1.Value = (idx / debt_rows) * 100
    '        Dim last_stageid As Integer = debt_table(idx, 15)

    '        If Trim(stage_array(last_stageid, 2)) <> "FurtherVanAttendance" And _
    '        Trim(stage_array(last_stageid, 2)) <> "Van Attendance" And _
    '        Trim(stage_array(last_stageid, 2)) <> "F C - Bailiff" And _
    '        Trim(stage_array(last_stageid, 2)) <> "AwaitingVanApproval" And _
    '        Trim(stage_array(last_stageid, 2)) <> "(Return)" Then
    '            Continue For
    '        End If
    '        'check next date is null except for return
    '        If Trim(stage_array(last_stageid, 2)) <> "(Return)" Then
    '            Dim nextdate As Date
    '            Dim date_found As Boolean = True
    '            Try
    '                nextdate = debt_table(idx, 16)
    '            Catch ex As Exception
    '                date_found = False
    '            End Try
    '            If date_found = True Then
    '                Continue For
    '            End If
    '        End If
    '        Dim sch_no As String = ""
    '        Dim cl_no As String = ""
    '        Dim debtor As Integer
    '        For idx2 = 1 To 14
    '            Dim col_result As String = ""
    '            col_value = Trim(debt_table(idx, idx2))
    '            If idx2 = 1 Then
    '                debtor = col_value
    '            End If
    '            If idx2 = 6 Then  'clientschemeID
    '                If cs_table(col_value, 4) <> 1 Then  'Branch has to be 1
    '                    outline = ""
    '                    Exit For
    '                End If
    '                cl_no = cs_table(col_value, 2)
    '                sch_no = cs_table(col_value, 3)
    '                'ignore any not road traffic schenes
    '                If sch_no <> 41 And sch_no <> 676 And sch_no <> 773 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                col_result = Trim(client_table(cl_no, 2))
    '            ElseIf idx2 = 7 Then
    '                col_result = Trim(sch_table(sch_no, 2))
    '            ElseIf idx2 = 8 Then
    '                If col_value.Length < 5 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                If phone_no = col_value And phone_no.Length > 0 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                phone_no = col_value
    '                col_result = col_value
    '            ElseIf idx2 = 9 Then
    '                If phone_no.Length = 0 Then
    '                    phone_no = col_value
    '                End If
    '                col_result = col_value
    '            ElseIf idx2 = 10 Then
    '                col_result = Format(col_value, "fixed")
    '            ElseIf idx2 = 11 Then
    '                col_result = cl_no
    '            ElseIf idx2 = 12 Then
    '                Continue For
    '            ElseIf idx2 = 13 Then
    '                col_result = col_value
    '            ElseIf idx2 = 14 Then
    '                'check postcode sector
    '                If col_value.Length > 0 Then
    '                    If found_pc_sector(col_value) = True Then
    '                        outline = ""
    '                        Exit For
    '                    End If
    '                End If
    '            Else
    '                For idx3 = 1 To Len(col_value)
    '                    If Mid(col_value, idx3, 1) = vbCr Or _
    '                        Mid(col_value, idx3, 1) = "," Or _
    '                       Mid(col_value, idx3, 1) = vbLf Then
    '                        col_result = col_result & " "
    '                    Else
    '                        col_result = col_result & Mid(col_value, idx3, 1)
    '                    End If
    '                Next
    '            End If
    '            If idx2 <> 12 And idx2 <> 14 Then
    '                outline = outline & col_result & ","
    '            End If
    '        Next
    '        If Len(outline) > 0 Then
    '            outline_array(outline_idx) = outline
    '            phone_array(outline_idx) = phone_no
    '            outline_idx += 1
    '            outline = ""
    '        End If
    '    Next

    '    'sort arrays by phone numbers
    '    Array.Sort(phone_array, outline_array, 0, outline_idx)
    '    phone_no = ""
    '    ProgressBar1.Value = 5
    '    For idx = 0 To outline_idx - 1
    '        ProgressBar1.Value = (idx / outline_idx) * 100
    '        If phone_no = phone_array(idx) Then
    '            Continue For
    '        End If
    '        outline = outline_array(idx)
    '        If Len(outline) > 0 Then
    '            outfile = outfile & outline & vbNewLine
    '        End If
    '        phone_no = phone_array(idx)

    '    Next
    '    ProgressBar1.Value = 100
    '    'write out file
    '    With SaveFileDialog1
    '        .Title = "Save file"
    '        .Filter = "CSV files |*.csv"
    '        .DefaultExt = ".csv"
    '        .OverwritePrompt = True
    '        .FileName = "dialler.csv"
    '    End With

    '    Dim filepath As String = SaveFileDialog1.FileName
    '    If Len(outfile) = 0 Then
    '        MessageBox.Show("There are no cases selected")
    '    Else
    '        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
    '        End If
    '    End If
    '    Me.Close()
    'End Sub

    Private Sub returnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles returnbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "X"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                " last_stageID, add_postcode, status_nextdate, D._createdDate from Debtor D, stage S" & _
                                " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and S.name  = '(Return)'" & _
                                 " and D.last_stageID=S._rowID" & _
                                " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub cmec_ooabtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmec_ooabtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "G"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub cmecbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmecbtn.Click
        disable_buttons()
        cmecstagefrm.ShowDialog()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "I"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                 " last_stageID, add_postcode, status_nextdate, _createdDate, arrange_broken from Debtor" & _
                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    'Private Sub auditbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles auditbtn.Click
    '    disable_buttons()
    '    Dim resp As Object
    '    Try
    '        resp = InputBox("Enter agent number", "Select agent")
    '        If resp Is "" Then
    '            Exit Sub
    '        End If
    '    Catch ex As Exception
    '        MsgBox("agent number is invalid")
    '        Exit Sub
    '    End Try
    '    'get agent name from onestep
    '    Try
    '        agent_no = resp
    '    Catch ex As Exception
    '        MsgBox("agent number is invalid")
    '        Exit Sub
    '    End Try
    '    param1 = "onestep"
    '    param2 = "select name_fore, name_sur from Bailiff where _rowid = " & agent_no
    '    Dim agent_dataset As DataSet = get_dataset(param1, param2)
    '    If no_of_rows = 0 Then
    '        MsgBox(agent_no & " does not exist on onestep")
    '        Exit Sub
    '    End If
    '    Dim agent_name As String = ""
    '    Dim agent_forename As String = ""
    '    Try
    '        agent_forename = Trim(agent_dataset.Tables(0).Rows(0).Item(0))
    '    Catch ex As Exception
    '        agent_forename = ""
    '    End Try
    '    agent_name = agent_forename & " " & Trim(agent_dataset.Tables(0).Rows(0).Item(1))
    '    If MsgBox("Bailiff audit for " & agent_name, MsgBoxStyle.OkCancel, "Agent Confirmation") <> MsgBoxResult.Ok Then
    '        MsgBox("Bailiff audit cancelled")
    '        Me.Close()
    '        Exit Sub
    '    End If
    '    ProgressBar1.Visible = True
    '    ProgressBar1.Value = 5
    '    Dim outline_array(1000) As String
    '    Dim outline_idx As Integer = 0
    '    Dim phone_array(1000) As String
    '    Dim three_months_ago As Date = DateAdd(DateInterval.Month, -3, Now)
    '    'get all cases allocated to agent in last 3 months
    '    param2 = "select debtorID from Visit where bailiffID = " & agent_no & _
    '    " and  date_allocated >= '" & Format(three_months_ago, "yyyy-MM-dd") & "'" & _
    '    " and date_visited >= date_allocated"
    '    Dim visit_dataset As DataSet = get_dataset("onestep", param2)
    '    If no_of_rows = 0 Then
    '        MsgBox("No cases allocated last 3 months for agent no = " & agent_no)
    '        Me.Close()
    '        Exit Sub
    '    End If
    '    Dim visit_rows As Integer = no_of_rows
    '    Dim idx As Integer
    '    For idx = 0 To visit_rows - 1
    '        ProgressBar1.Value = (idx / visit_rows) * 100
    '        Application.DoEvents()
    '        Dim debtor As Integer = visit_dataset.Tables(0).Rows(idx).Item(0)
    '        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
    '              " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
    '              " last_stageID, add_postcode, status_nextdate, _createdDate, arrange_broken from Debtor" & _
    '              " where _rowid = " & debtor & _
    '              " and (add_phone is not null or add_fax is not null " & _
    '              " or empPhone is not null or empFax is not null)"
    '        Dim debtor_dataset As DataSet = get_dataset("onestep", param2)
    '        If no_of_rows = 0 Then
    '            Continue For
    '        End If
    '        Dim csid As Integer = debtor_dataset.Tables(0).Rows(0).Item(5)
    '        param2 = "select branchID, clientID, schemeID from ClientScheme where _rowid = " & csid
    '        Dim csid_dataset As DataSet = get_dataset("onestep", param2)
    '        If no_of_rows <> 1 Then
    '            MsgBox("Unable to read clientScheme for csid = " & csid)
    '            Exit Sub
    '        End If
    '        Dim cl_ID = csid_dataset.Tables(0).Rows(0).Item(1)
    '        If cl_ID = 1 Or cl_ID = 2 Or cl_ID = 24 Then
    '            Continue For
    '        End If
    '        Dim branchID As Integer = csid_dataset.Tables(0).Rows(0).Item(0)
    '        Dim sch_ID As Integer = csid_dataset.Tables(0).Rows(0).Item(2)
    '        param2 = "select name from Client where _rowid = " & cl_ID
    '        Dim cl_dataset As DataSet = get_dataset("onestep", param2)
    '        If no_of_rows = 0 Then
    '            MsgBox("Unable to read client for clID = " & cl_ID)
    '            Exit Sub
    '        End If
    '        Dim cl_name As String = Trim(cl_dataset.Tables(0).Rows(0).Item(0))

    '        param2 = "select name from Scheme where _rowid = " & sch_ID
    '        Dim sch_dataset As DataSet = get_dataset("onestep", param2)
    '        If no_of_rows = 0 Then
    '            MsgBox("Unable to read Scheme for Sch_ID = " & sch_ID)
    '            Exit Sub
    '        End If
    '        Dim sch_name As String = Trim(sch_dataset.Tables(0).Rows(0).Item(0))
    '        Dim ph1, ph2, ph3, ph4 As String
    '        Dim title, forename, surname, name2, address As String
    '        Try
    '            title = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
    '        Catch ex As Exception
    '            title = ""
    '        End Try
    '        Try
    '            forename = Trim(debtor_dataset.Tables(0).Rows(0).Item(2))
    '        Catch ex As Exception
    '            forename = ""
    '        End Try
    '        Try
    '            surname = Trim(debtor_dataset.Tables(0).Rows(0).Item(3))
    '        Catch ex As Exception
    '            surname = ""
    '        End Try
    '        Try
    '            address = Trim(debtor_dataset.Tables(0).Rows(0).Item(4))
    '        Catch ex As Exception
    '            address = ""
    '        End Try
    '        Try
    '            name2 = Trim(debtor_dataset.Tables(0).Rows(0).Item(11))
    '        Catch ex As Exception
    '            name2 = ""
    '        End Try
    '        Try
    '            ph1 = Trim(debtor_dataset.Tables(0).Rows(0).Item(6))
    '        Catch ex As Exception
    '            ph1 = ""
    '        End Try
    '        Try
    '            ph2 = Trim(debtor_dataset.Tables(0).Rows(0).Item(7))
    '        Catch ex As Exception
    '            ph2 = ""
    '        End Try
    '        Try
    '            ph3 = Trim(debtor_dataset.Tables(0).Rows(0).Item(8))
    '        Catch ex As Exception
    '            ph3 = ""
    '        End Try
    '        Try
    '            ph4 = Trim(debtor_dataset.Tables(0).Rows(0).Item(9))
    '        Catch ex As Exception
    '            ph4 = ""
    '        End Try

    '        'shuffle phone numbers along
    '        If ph1.Length < 4 Then
    '            ph1 = ph2
    '            ph2 = ph3
    '            ph3 = ph4
    '            ph4 = ""
    '            If ph1.Length < 4 Then
    '                ph1 = ph2
    '                ph2 = ph3
    '                ph3 = ""
    '            End If
    '            If ph1.Length < 4 Then
    '                ph1 = ph2
    '                ph2 = ""
    '            End If
    '        End If
    '        If ph2.Length < 4 Then
    '            ph2 = ph3
    '            ph3 = ph4
    '            ph4 = ""
    '            If ph2.Length < 4 Then
    '                ph2 = ph3
    '                ph3 = ""
    '            End If
    '        Else
    '            If ph3.Length < 4 Then
    '                ph3 = ph4
    '                ph4 = ""
    '            End If
    '        End If
    '        'remove chr(10) and chr(13) 
    '        If ph1.Length < 4 Then
    '            Continue For
    '        End If
    '        ph1 = remove_chars(ph1, True)
    '        ph2 = remove_chars(ph2, True)
    '        ph3 = remove_chars(ph3, True)
    '        ph4 = remove_chars(ph4, True)
    '        surname = remove_chars(surname, False)
    '        forename = remove_chars(forename, False)
    '        title = remove_chars(title, False)
    '        address = remove_chars(address, False)
    '        name2 = remove_chars(name2, False)
    '        Dim created_date As Date = CDate(debtor_dataset.Tables(0).Rows(0).Item(15))
    '        outline = outline & debtor & "," & title & "," & forename & "," & surname & "," & _
    '        address & "," & cl_name & "," & sch_name & "," & _
    '        ph1 & "," & ph2 & "," & ph3 & "," & ph4 & "," & _
    '        Format(debtor_dataset.Tables(0).Rows(0).Item(10), "fixed") & "," & _
    '        cl_ID & "," & name2 & "," & Format(created_date, "d/MM/yyyy")
    '        Try
    '            outline_array(outline_idx) = outline
    '        Catch ex As Exception
    '            ReDim Preserve outline_array(outline_idx + 200)
    '            ReDim Preserve phone_array(outline_idx + 200)
    '            outline_array(outline_idx) = outline
    '        End Try

    '        phone_array(outline_idx) = ph1
    '        outline_idx += 1
    '        outline = ""
    '    Next
    '    Array.Sort(phone_array, outline_array, 0, outline_idx)
    '    Dim phone_no As String = ""
    '    For idx = 0 To outline_idx - 1
    '        'ToolStripProgressBar1.Value = (idx / outline_idx) * 100
    '        If phone_no = phone_array(idx) Then
    '            Continue For
    '        End If
    '        outline = outline_array(idx)
    '        If Len(outline) > 0 Then
    '            Try
    '                outfile = outfile & outline & vbNewLine
    '            Catch ex As Exception
    '                MsgBox("Limit of cases reached")
    '                Exit For
    '            End Try

    '        End If
    '        phone_no = phone_array(idx)
    '    Next
    '    'write out file
    '    With collect_stage.SaveFileDialog1
    '        .Title = "Save file"
    '        .Filter = "CSV files |*.csv"
    '        .DefaultExt = ".csv"
    '        .OverwritePrompt = True
    '        .FileName = "dialler.csv"
    '    End With
    '    Dim filepath As String = collect_stage.SaveFileDialog1.FileName
    '    If Len(outfile) = 0 Then
    '        MessageBox.Show("There are no cases selected")
    '    Else
    '        If collect_stage.SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            My.Computer.FileSystem.WriteAllText(collect_stage.SaveFileDialog1.FileName, outfile, False, ascii)
    '        End If
    '    End If
    '    selected_stage_name = ""
    '    collect_stage.ToolStripProgressBar1.Visible = False

    '    Me.Close()
    'End Sub

    Private Sub enf_clientbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        collect_cases = False
        choose_client()
    End Sub

    Private Sub bail_returnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bail_retnbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "Y"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                " last_stageID, add_postcode, status_nextdate, _createdDate, " & _
                                " bail_current, bailiffID, status from Debtor" & _
                                " where status_open_closed = 'O' and status_hold = 'N'" & _
                                " and (status = 'C' or status = 'L')" & _
                                " and debt_balance > 1.00 and (add_phone is not null or add_fax is not null " & _
                                " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub nndrbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "Z"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub bail_top_clbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'allow scheme to be entered
        selected_client = 0
        If MsgBox("Choose a specific scheme?", MsgBoxStyle.YesNo, "Scheme selection?") = MsgBoxResult.Yes Then
            schemefrm.ShowDialog()
        End If

        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "W"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub stackedbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "U"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                 " last_stageID, add_postcode, status_nextdate, _createdDate, linkID from Debtor" & _
                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                " and bail_current = 'Y' and bailiffID = 72 and (add_phone is not null or add_fax is not null " & _
                " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub hmrcbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hmrcbtn.Click
        disable_buttons()
        'hmrc_or_DVLA_case = True
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        param1 = "onestep"
        mode = "D"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                         " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                         " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                         " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                         " and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                         " or empPhone is not null or empFax is not null) " & _
                         " and debt_balance > 1.00 and arrange_broken = 'Y'"
        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub feesbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles feesbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "J"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                " and debt_amount=0 and debt_costs = 0 and debt_fees > 0 and bail_current = 'N'" & _
                                " and (add_phone is not null or add_fax is not null " & _
                                " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub


    Private Sub lsc_camp1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsc_camp1btn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "1"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, _createdDate, arrange_next, statusCode, inPrison from Debtor" & _
                  " where status_open_closed = 'O' and status = 'A' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub lsc_camp2btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsc_camp2btn.Click
        'payment due today
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "2"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, _createdDate, arrange_next, statusCode, inPrison from Debtor" & _
                  " where status_open_closed = 'O' and status = 'A' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and arrange_next = '" & Format(Now, "yyyy-MM-dd") & "'" & _
                   " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub lsc_defaultedbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsc_defaultedbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "3"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, _createdDate, arrange_next, statusCode, inPrison from Debtor" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub lsc_fdcbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsc_fdcbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "4"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, _createdDate, arrange_next, statusCode, inPrison from Debtor" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                   " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub lsc_crystalbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lsc_crystalbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "5"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, _createdDate, arrange_next, statusCode, inPrison from Debtor" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub rtn_not_cmecbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rtn_not_cmecbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "6"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                " last_stageID, add_postcode, status_nextdate, _createdDate, " & _
                " bail_current, bailiffID, status from Debtor" & _
                " where status_open_closed = 'O' and status_hold = 'N'" & _
                " and (status = 'C' or status = 'L')" & _
                " and debt_balance > 1.00 and (add_phone is not null or add_fax is not null " & _
                " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub cco_btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cco_btn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "7"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                 " last_stageID, add_postcode, status_nextdate, _createdDate, arrange_next, statusCode,inPrison from Debtor" & _
                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                 " and debt_balance > 1.00 and bail_current = 'N'" & _
                 " and (add_phone is not null or add_fax is not null " & _
                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub levy_van1btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "8"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                 " last_stageID, add_postcode, status_nextdate, _createdDate, " & _
                 " bail_current, bailiffID, status, statusCode, arrangeBrokenCount from Debtor" & _
                 " where status_open_closed = 'O' and status_hold = 'N'" & _
                 " and status = 'L' and bail_current = 'N' and debtPaid = 0" & _
                 " and debt_balance > 1.00 and (add_phone is not null or add_fax is not null " & _
                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub levy_van2btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "9"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                 " last_stageID, add_postcode, status_nextdate, _createdDate, " & _
                 " bail_current, bailiffID, status, statusCode, arrangeBrokenCount from Debtor" & _
                 " where status_open_closed = 'O' and status_hold = 'N'" & _
                 " and status = 'L' and bail_current = 'N'" & _
                 " and debt_balance > 1.00 and (add_phone is not null or add_fax is not null " & _
                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub levy_van3btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "10"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                 " last_stageID, add_postcode, status_nextdate, _createdDate, " & _
                 " bail_current, bailiffID, status, statusCode, arrangeBrokenCount from Debtor" & _
                 " where status_open_closed = 'O' and status_hold = 'N'" & _
                 " and status = 'L' and bail_current = 'N' and debtPaid > 0" & _
                 " and debt_balance > 1.00 and (add_phone is not null or add_fax is not null " & _
                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub empbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "11"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                 " last_stageID, add_postcode, status_nextdate, _createdDate, " & _
                 " bail_current, bailiffID, status, statusCode, arrangeBrokenCount from Debtor" & _
                 " where status_open_closed = 'O' and status_hold = 'N'" & _
                 " and status = 'L' and bail_current = 'N' " & _
                 " and debt_balance > 1.00 and (add_phone is not null or add_fax is not null " & _
                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()

    End Sub

    Private Sub arrow_sch_grpbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles arrow_sch_grpbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        arrow_sch_cbox.Enabled = True
        sch_grplbl.Visible = True
    End Sub

    Private Sub arrow_sch_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles arrow_sch_cbox.SelectedIndexChanged
        If arrow_sch_cbox.SelectedIndex = -1 Then
            MsgBox("Select scheme group")
        Else
            sch_group = arrow_sch_cbox.Text
            collect_cases = True
            arrow_sch_cbox.Enabled = False
            mode = "12"
            If MsgBox("Choose a specific stage?", MsgBoxStyle.YesNo, "Stage selection?") = MsgBoxResult.Yes Then

                collect_stage.ShowDialog()
                Me.Close()
            Else
                param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                         " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                         " last_stageID, add_postcode, status_nextdate, _createdDate, " & _
                         " bail_current, bailiffID, status, statusCode, arrangeBrokenCount from Debtor" & _
                         " where status_open_closed = 'O' and status_hold = 'N'" & _
                         " and status = 'L' and bail_current = 'N' " & _
                         " and debt_balance > 1.00 and (add_phone is not null or add_fax is not null " & _
                         " or empPhone is not null or empFax is not null)"
                produce_dialler_file(param2)
            End If
            Me.Close()
        End If
    End Sub

    Private Sub cpw_schbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cpw_schbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        cpw_sch_cbox.Enabled = True
        cpw_schlbl.Visible = True

    End Sub

    Private Sub cpw_sch_cbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cpw_sch_cbox.SelectedIndexChanged
        If cpw_sch_cbox.SelectedIndex = -1 Then
            MsgBox("Select scheme group")
        Else
            sch_group = cpw_sch_cbox.Text
            collect_cases = True
            cpw_sch_cbox.Enabled = False
            mode = "13"
            If MsgBox("Choose a specific stage?", MsgBoxStyle.YesNo, "Stage selection?") = MsgBoxResult.Yes Then

                collect_stage.ShowDialog()
                Me.Close()
            Else
                param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                         " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                         " last_stageID, add_postcode, status_nextdate, _createdDate, " & _
                         " bail_current, bailiffID, status, statusCode, arrangeBrokenCount from Debtor" & _
                         " where status_open_closed = 'O' and status_hold = 'N'" & _
                         " and status = 'L' and bail_current = 'N' " & _
                         " and debt_balance > 1.00 and (add_phone is not null or add_fax is not null " & _
                         " or empPhone is not null or empFax is not null)"
                produce_dialler_file(param2)
            End If
            Me.Close()
        End If
    End Sub

    Private Sub outcallbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles outcallbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "14"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, _createdDate, arrange_next, statusCode, " & _
                  " bail_current, bailiffID from Debtor" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 0" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub vulnerbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles vulnerbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "15"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, _createdDate, arrange_next, statusCode from Debtor" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 0 and bail_current = 'N'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub UUBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UUBtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        UUCbox.Visible = True
        UUlbl.Visible = True
    End Sub

    Private Sub UUCbox_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UUCbox.SelectedIndexChanged
        If UUCbox.SelectedIndex = -1 Then
            MsgBox("Select scheme group")
        Else
            UU_selected_index = UUCbox.SelectedIndex
            sch_group = UUCbox.Text
            collect_cases = True
            UUCbox.Enabled = False
            mode = "UU"
            If MsgBox("Choose a specific stage?", MsgBoxStyle.YesNo, "Stage selection?") = MsgBoxResult.Yes Then

                collect_stage.ShowDialog()
                Me.Close()
            Else
                param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                         " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                         " last_stageID, add_postcode, status_nextdate, _createdDate, " & _
                         " bail_current, bailiffID, status, statusCode, arrangeBrokenCount from Debtor" & _
                         " where status_open_closed = 'O' and status_hold = 'N'" & _
                         " and status = 'L' and bail_current = 'N' " & _
                         " and debt_balance > 1.00 and (add_phone is not null or add_fax is not null " & _
                         " or empPhone is not null or empFax is not null)"
                produce_dialler_file(param2)
            End If
            Me.Close()
        End If
    End Sub

    Private Sub tma_rbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tma_rbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "16"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub hbop_mbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hbop_mbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "17"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub leeds_tmabtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "18"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                " and clientSchemeID = 3607 and debt_fees >= 149 and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub tmabtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmabtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "19"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub hmrcagebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles hmrcagebtn.Click
        disable_buttons()
        'hmrc_or_DVLA_case = True
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        cancelled = False
        Agefrm.ShowDialog()
        If cancelled Then
            enable_buttons()
            MsgBox("Report not produced")
            Exit Sub
        End If
        mode = "20"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub precom1btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles precom1btn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5

        mode = "21"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub precom2btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles precom2btn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5

        mode = "22"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub fvanbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fvanbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5

        mode = "23"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub rtdfvanbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rtdfvanbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5

        mode = "24"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub fsbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles fsbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5

        mode = "25"
        If MsgBox("Choose a specific stage?", MsgBoxStyle.YesNo, "Stage selection?") = MsgBoxResult.Yes Then

            collect_stage.ShowDialog()
            Me.Close()
        Else
            param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                     " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                     " last_stageID, add_postcode, status_nextdate, _createdDate, " & _
                     " bail_current, bailiffID, status, statusCode, arrangeBrokenCount from Debtor" & _
                     " where status_open_closed = 'O' and status_hold = 'N'" & _
                     " and status = 'L' and bail_current = 'N' " & _
                     " and debt_balance > 1.00 and (add_phone is not null or add_fax is not null " & _
                     " or empPhone is not null or empFax is not null)"
            produce_dialler_file(param2)
            Me.Close()
        End If
    End Sub

    Private Sub dvlabtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dvlabtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        cancelled = False
        DVLA_case = True
        Agefrm.ShowDialog()
        If cancelled Then
            enable_buttons()
            MsgBox("Report not produced")
            Exit Sub
        End If
        mode = "26"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub arrow_by_casebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles arrow_by_agebtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        cancelled = False
        Agefrm.ShowDialog()
        If cancelled Then
            enable_buttons()
            MsgBox("Report not produced")
            Exit Sub
        End If
        mode = "27"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub lowellbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lowellbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        cancelled = False
        Agefrm.ShowDialog()
        If cancelled Then
            enable_buttons()
            MsgBox("Report not produced")
            Exit Sub
        End If
        mode = "28"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dvlabrokenbtn.Click
        'dvla broken arrangements
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "29"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)" & _
                                 " and arrange_broken = 'Y'"

        produce_dialler_file(param2)
        Me.Close()
    End Sub


    Private Sub slbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles slbtn.Click
        'student loans
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "30"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub etcbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles etcbtn.Click
        'HMRC ETC
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        Agefrm.ShowDialog()
        If cancelled Then
            enable_buttons()
            MsgBox("Report not produced")
            Exit Sub
        End If
        mode = "31"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub awbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles awbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        cancelled = False
        Agefrm.ShowDialog()
        If cancelled Then
            enable_buttons()
            MsgBox("Report not produced")
            Exit Sub
        End If
        mode = "32"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub etc_brokenbtn_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles etc_brokenbtn.Click
        'HMRC ETC broken arrangements
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "33"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)" & _
                                 " and arrange_broken = 'Y'"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub listbtn_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles listbtn.Click
        'from onestep list
        Dim listID As Integer = 0
        Try
            listID = InputBox("Enter onestep LISTID", "Onestep ListID")
        Catch ex As Exception

        End Try
        If listID = 0 Then
            MsgBox("Invalid listID - " & listID)
            Exit Sub
        End If
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "34"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor, listdetail " & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)" & _
                                 " and _rowID = objectRowID " & _
                                 " and listID = " & listID

        produce_dialler_file(param2)
        Me.Close()
    End Sub


    Private Sub segmentbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles segmentbtn.Click
        'HMRC ETC  by segment
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        Dim segment As String = ""
        segment = InputBox("Enter segment", "Segment")
        If segment = "" Then
            enable_buttons()
            MsgBox("Report not produced")
            Exit Sub
        End If
        mode = "34"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and client_batch = '" & segment & "'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub apbtn(sender As System.Object, e As System.EventArgs) Handles appealbtn.Click
        'appeals
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "35"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, _createdDate, arrange_next, statusCode, inPrison from Debtor" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and clientschemeID = 2109" & _
                   " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub


    Private Sub compbtn7_Click(sender As System.Object, e As System.EventArgs) Handles compbtn7.Click
        'compliance letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "36"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Compliance Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub remindbtn_Click(sender As System.Object, e As System.EventArgs) Handles remindbtn.Click
        'reminder letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "37"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Reminder Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub remind23btn_Click(sender As System.Object, e As System.EventArgs) Handles remind23btn.Click
        'final warning letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "38"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LastStageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Final Warning Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub precombtn_Click(sender As System.Object, e As System.EventArgs) Handles precomL1btn.Click
        'pre comm letter 1
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "39"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, bail_current, bailiffID, LaststageDate, debt_fees from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                  " and (S.name  like 'Pre Comm Letter 1' or " & _
                  "   S.name  = 'TMA Letter 1')" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub retnbtn_Click(sender As System.Object, e As System.EventArgs) Handles retnbtn.Click
        'returns
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "40"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, bail_current, bailiffID, LaststageDate, debt_fees from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = '(Return)'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub hmrc_batchbtn_Click(sender As System.Object, e As System.EventArgs) Handles hmrc_batchbtn.Click

        Dim batchNo As Integer
        Try
            batchNo = InputBox("Enter Batch Number", "Batch Number")
        Catch ex As Exception
            batchNo = 0
        End Try
        If batchNo = 0 Then
            MsgBox("No batch number selected")
            Exit Sub
        End If
        disable_buttons()
        mode = "41"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and client_batch = " & batchNo & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub finrembtn_Click(sender As System.Object, e As System.EventArgs) Handles finrembtn.Click
        'final reminder letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "42"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Final Reminder'" & _
                  " and D.debt_fees > 0" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub finenfbtn_Click(sender As System.Object, e As System.EventArgs) Handles finenfbtn.Click
        'final enforcement
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "43"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Final Enforcement'" & _
                  " and D.debt_fees > 0" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub


    Private Sub Mcompbtn_Click(sender As System.Object, e As System.EventArgs) Handles Mcompbtn.Click
        'Marston compliance letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "44"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Compliance Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub Mremindbtn_Click(sender As System.Object, e As System.EventArgs) Handles Mremindbtn.Click
        'Marston reminder letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "45"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Reminder Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub MfinalWbtn_Click(sender As System.Object, e As System.EventArgs) Handles MfinalWbtn.Click
        'Marston final warning letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "46"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LastStageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Final Warning Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub lowellnotvulbtn_Click(sender As System.Object, e As System.EventArgs) Handles lowellnotvulbtn.Click
        'Lowell not vulnerable
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "47"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and isVulnerable = 'N' and isSpecial = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub SLNotvul_Click(sender As System.Object, e As System.EventArgs) Handles SLNotvul.Click
        'Student Loans IDS not vulnerable
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "48"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and isVulnerable = 'N' and isSpecial = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub LAANotvul_Click(sender As System.Object, e As System.EventArgs) Handles LAANotvul.Click
        'DMI LAA IDS not vulnerable
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "49"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and isVulnerable = 'N' and isSpecial = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub HMRCnotvul_Click(sender As System.Object, e As System.EventArgs) Handles HMRCnotvul.Click
        'HMRC DMI not vulnerable
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "50"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                                 " and isVulnerable = 'N' and isSpecial = 'N'" & _
                                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub enfletterbtn_Click(sender As System.Object, e As System.EventArgs) Handles enfletterbtn.Click
        'enforcement letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "51"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LastStageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Enforcement Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub precommL2btn_Click(sender As System.Object, e As System.EventArgs) Handles precommL2btn.Click
        'pre comm letter 2 and TMA letter 2
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "52"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, bail_current, bailiffID, LaststageDate, debt_fees from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                 " and (S.name  like 'Pre Comm Letter 2' or " & _
                  "   S.name  = 'TMA Letter 2')" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub MenfLetterbtn_Click(sender As System.Object, e As System.EventArgs) Handles MenfLetterbtn.Click
        'Marston enforcement letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "53"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Enforcement Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub MfinRemindbtn_Click(sender As System.Object, e As System.EventArgs) Handles MfinRemindbtn.Click
        'Marston final reminder letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "54"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Final Reminder'" & _
                  " and D.debt_fees > 0" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub Mfinenfbtn_Click(sender As System.Object, e As System.EventArgs) Handles Mfinenfbtn.Click
        'Marston final enforcement
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "55"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Final Enforcement'" & _
                  " and D.debt_fees > 0 " & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub MpcommL1btn_Click(sender As System.Object, e As System.EventArgs) Handles MpcommL1btn.Click
        'Marston precomm letter 1
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "56"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, bail_current, bailiffID, LaststageDate, debt_fees from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Pre Comm Letter 1'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub MprecommL2_Click(sender As System.Object, e As System.EventArgs) Handles MprecommL2.Click
        'Marston precomm letter 2
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "57"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, bail_current, bailiffID, LaststageDate, debt_fees from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Pre Comm Letter 2'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub Mretnbtn_Click(sender As System.Object, e As System.EventArgs) Handles Mretnbtn.Click
        'Marston returns
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "58"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, bail_current, bailiffID, LaststageDate, debt_fees from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = '(Return)'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub furtherenfbtn_Click(sender As System.Object, e As System.EventArgs) Handles furtherenfbtn.Click
        'further enforcement
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "59"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Further Enforcement'" & _
                  " and D.debt_fees > 0" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub Mfurtherenfbtn_Click(sender As System.Object, e As System.EventArgs) Handles Mfurtherenfbtn.Click
        'Marston further enforcement
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "60"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Further Enforcement'" & _
                  " and D.debt_fees > 0" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub noasentbtn_Click(sender As System.Object, e As System.EventArgs) Handles noasentbtn.Click
        'SLC IDS NOA Sent
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "61"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'NOA Sent'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub slcs2letterbtn_Click(sender As System.Object, e As System.EventArgs) Handles slcs2letterbtn.Click
        'SLC stage 2 letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "62"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Stage 2 letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub slcs3btn_Click(sender As System.Object, e As System.EventArgs) Handles slcs3btn.Click
        'SLC stage 3 letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "63"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Stage 3 letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub hmrcintrobtn_Click(sender As System.Object, e As System.EventArgs) Handles hmrcintrobtn.Click
        'hmrc ids introduction letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "64"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Introduction Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub hmrcl2btn_Click(sender As System.Object, e As System.EventArgs) Handles hmrcl2btn.Click
        'hmrc ids letter 2
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "65"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'HMRC Letter 2'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub hmrcl3btn_Click(sender As System.Object, e As System.EventArgs) Handles hmrcl3btn.Click
        'hmrc ids letter 3
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "66"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'HMRC Letter 3'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub hmrcl4btn_Click(sender As System.Object, e As System.EventArgs) Handles hmrcl4btn.Click
        'hmrc ids letter 4
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "67"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'HMRC Letter 4'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub hmrcsw1btn_Click(sender As System.Object, e As System.EventArgs) Handles hmrcsw1btn.Click
        'hmrc ids 1st sweep
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "68"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'HMRC Letter 4'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub hmrcsw2btn_Click(sender As System.Object, e As System.EventArgs) Handles hmrcsw2btn.Click
        'hmrc ids 2nd sweep
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "69"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'HMRC Letter 4'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub laal1btn_Click(sender As System.Object, e As System.EventArgs) Handles laal1btn.Click
        'LAA IDS DMI letter 1
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "70"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'LAA DMI Letter 1'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub laa2btn_Click(sender As System.Object, e As System.EventArgs) Handles laa2btn.Click
        'LAA IDS DMI letter 2
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "71"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'LAA DMI Letter 2'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub laa3btn_Click(sender As System.Object, e As System.EventArgs) Handles laa3btn.Click
        'LAA IDS DMI letter 3
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "72"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'LAA DMI Letter 3'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub lowells1lbtn_Click(sender As System.Object, e As System.EventArgs) Handles lowells1lbtn.Click
        'Lowell stage 1 letter sent
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "73"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Stage 1 Letter Sent'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub lowells2btn_Click(sender As System.Object, e As System.EventArgs) Handles lowells2btn.Click
        'Lowell stage 2 letter sent
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "74"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Stage 2 Letter Sent'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub lowells3btn_Click(sender As System.Object, e As System.EventArgs) Handles lowells3btn.Click
        'Lowell stage 3 letter sent
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "75"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Stage 3 Letter Sent'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub lowell90btn_Click(sender As System.Object, e As System.EventArgs) Handles lowell90btn.Click
        'Lowell 90 day review
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "76"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D" & _
                  " where status_open_closed = 'O' and status = 'A' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub hmrcbabtn_Click(sender As System.Object, e As System.EventArgs) Handles adhocbabtn.Click
        'HMRC/SLC IDS/LAA IDS Broken arrangements
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "77"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, clientscheme CS" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and D.clientschemeID = CS._rowID" & _
                  " and CS.branchID in (8,23,26,27,29)" & _
                  " and arrangeBrokenCount > 0" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub adhoc90btn_Click(sender As System.Object, e As System.EventArgs) Handles adhoc90btn.Click
        'HMRC/SLC IDS/LAA IDS 90 day review
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "78"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, clientscheme CS" & _
                  " where status_open_closed = 'O' and D.status = 'A' and status_hold = 'N'" & _
                  " and D.clientschemeID = CS._rowID" & _
                  " and CS.branchID in (8,23,26,27,29)" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub uusl1btn_Click(sender As System.Object, e As System.EventArgs) Handles uusl1btn.Click
        'UU stage letter 1
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "79"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Stage Letter 1'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub uusl2btn_Click(sender As System.Object, e As System.EventArgs) Handles uusl2btn.Click
        'UU stage letter 2
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "80"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Stage Letter 2'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub uusl3btn_Click(sender As System.Object, e As System.EventArgs) Handles uusl3btn.Click
        'UU stage letter 3
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "81"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Stage Letter 3'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub uusl4btn_Click(sender As System.Object, e As System.EventArgs) Handles uusl4btn.Click
        'UU stage letter 4
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "82"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Stage Letter 4'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub hello4to8_Click(sender As System.Object, e As System.EventArgs) Handles hello4to8btn.Click
        'Hello letter 4-8 days 
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "83"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Hello Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub hello8plusbtn_Click(sender As System.Object, e As System.EventArgs) Handles hello8plusbtn.Click
        'Hello letter 8+ days 
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "84"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Hello Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub outboundbtn_Click(sender As System.Object, e As System.EventArgs) Handles outboundbtn.Click
        'Outbound Contact Strategy 
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "85"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Outbound Contact Strategy'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub softtracebtn_Click(sender As System.Object, e As System.EventArgs) Handles softtracebtn.Click
        'Soft trace letter 
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "86"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Soft Trace Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub brokenbtn_Click(sender As System.Object, e As System.EventArgs) Handles brokenbtn.Click
        'Broken Arrangement 
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "87"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and arrange_broken = 'Y'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub eonS1A_Click(sender As System.Object, e As System.EventArgs) Handles eonS1A.Click
        'EON stgae 1 Letter 3-6 days
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "88"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Stage 1 Letter'" & _
                 " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub EONS1B_Click(sender As System.Object, e As System.EventArgs) Handles EONS1B.Click
        'EON stage 1 Letter 7-11 days
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "89"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Stage 1 Letter'" & _
                 " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub eonS2_Click(sender As System.Object, e As System.EventArgs) Handles eonS2.Click
        'EON stage 2 Letter 3+ days
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "90"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Stage 2 Letter'" & _
                 " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub eonSetLetter_Click(sender As System.Object, e As System.EventArgs)
        'EON Settlement Letter 6+ days
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "91"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Settlement letter'" & _
                 " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub allhbopbtn_Click(sender As System.Object, e As System.EventArgs) Handles allhbopbtn.Click
        'ALL HBOP
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "92"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, clientscheme CS,scheme S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and CS.schemeID = S._rowID" & _
                  " and D.clientschemeID = CS._rowID" & _
                  " and S.work_type = 18" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub allftabtn_Click(sender As System.Object, e As System.EventArgs) Handles allftabtn.Click
        'ALL FTA
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "93"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, clientscheme CS,scheme S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and CS.schemeID = S._rowID" & _
                  " and D.clientschemeID = CS._rowID" & _
                  " and S.work_type = 17" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub allsdbtn_Click(sender As System.Object, e As System.EventArgs) Handles allsdbtn.Click
        'ALL FTA
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "94"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, arrange_started from Debtor D, clientscheme CS,scheme S" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and CS.schemeID = S._rowID" & _
                  " and D.clientschemeID = CS._rowID" & _
                  " and S.work_type = 13" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub exceptionbtn_Click(sender As System.Object, e As System.EventArgs) Handles exceptionbtn.Click
        'check stage has been selected
        If stagecbox.SelectedIndex = -1 Then
            MsgBox("Please select a stage from the list")
        End If
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "95"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = '" & stagecbox.SelectedItem & "'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub Mexceptionbtn_Click(sender As System.Object, e As System.EventArgs) Handles Mexceptionbtn.Click
        'check stage has been selected
        If Mstagecbox.SelectedIndex = -1 Then
            MsgBox("Please select a stage from the list")
        End If
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "96"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = '" & Mstagecbox.SelectedItem & "'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

   

    Private Sub swcompbtn_Click(sender As System.Object, e As System.EventArgs) Handles swcompbtn.Click
        'swift compliance letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = 100
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Compliance Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub swfwbtn_Click(sender As System.Object, e As System.EventArgs)
        'Swift final warning letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "101"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LastStageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Final Warning Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub swfrbtn_Click(sender As System.Object, e As System.EventArgs) Handles swfrbtn.Click
        'Swift final reminder letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "102"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Final Reminder'" & _
                  " and D.debt_fees > 0" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub swfebtn_Click(sender As System.Object, e As System.EventArgs) Handles swfebtn.Click
        'Swift final enforcement
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "103"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Final Enforcement'" & _
                  " and D.debt_fees > 0" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub swpc2btn_Click(sender As System.Object, e As System.EventArgs) Handles swpc2btn.Click
        'Swift pre comm letter 2 and TMA letter 2
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "104"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, bail_current, bailiffID, LaststageDate, debt_fees from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                 " and (S.name  like 'Pre Comm Letter 2' or " & _
                  "   S.name  = 'TMA Letter 2')" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub swrembtn_Click(sender As System.Object, e As System.EventArgs) Handles swrem1btn.Click
        'Swift reminder letter 1
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "105"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Reminder Letter 1'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub swrem2btn_Click(sender As System.Object, e As System.EventArgs) Handles swrem2btn.Click
        'Swift reminder letter 2
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "106"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Reminder Letter 2'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub swenfbtn_Click(sender As System.Object, e As System.EventArgs) Handles swenfbtn.Click
        'Swift enforcement letter
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "107"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LastStageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Enforcement Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub swfenfbtn_Click(sender As System.Object, e As System.EventArgs) Handles swfenfbtn.Click
        'Swift further enforcement
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "108"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Further Enforcement'" & _
                  " and D.debt_fees > 0" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub swpc1btn_Click(sender As System.Object, e As System.EventArgs) Handles swpc1btn.Click
        'Swift pre comm letter 1
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "109"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, bail_current, bailiffID, LaststageDate, debt_fees from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                  " and (S.name  like 'Pre Comm Letter 1' or " & _
                  "   S.name  = 'TMA Letter 1')" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub swretbtn_Click(sender As System.Object, e As System.EventArgs) Handles swretbtn.Click
        'Swift returns
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "110"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, bail_current, bailiffID, LaststageDate, debt_fees from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = '(Return)'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub swrunexcbtn_Click(sender As System.Object, e As System.EventArgs) Handles swrunexcbtn.Click
        'Swift check stage has been selected
        If swexcombo.SelectedIndex = -1 Then
            MsgBox("Please select a stage from the list")
        End If
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "111"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = '" & swexcombo.SelectedItem & "'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub RNNDRBtn_Click(sender As System.Object, e As System.EventArgs) Handles RNNDRBtn.Click
        'rossendales NNDR
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "112"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Compliance Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub MNNdrBtn_Click(sender As System.Object, e As System.EventArgs) Handles MNNdrBtn.Click
        'Marston NNDR
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "113"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Compliance Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub SNNDRbtn_Click(sender As System.Object, e As System.EventArgs) Handles SNNDRbtn.Click
        'Swift NNDR
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "114"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D, stage S" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'N'" & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.name  = 'Compliance Letter'" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

  
    Private Sub ret_pendingbtn_Click(sender As System.Object, e As System.EventArgs) Handles ret_pendingbtn.Click
        'Return pending 1/2/10
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "115"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate from Debtor D" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 and bail_current = 'Y'" & _
                  " and bailiffID in (3031,3032,3033) " & _
                   " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub southendbtn_Click(sender As System.Object, e As System.EventArgs) Handles southendbtn.Click
        'Southend
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "116"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, bailiffID, bail_current from Debtor D" & _
                  " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                  " and debt_balance > 1.00 " & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub sw_compbtn_Click(sender As System.Object, e As System.EventArgs) Handles sw_compbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "117"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, bailiffID, bail_current from Debtor D, stage S, clientscheme CS" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and D.clientschemeID=CS._rowID" & _
                  " and CS.branchID = 24" & _
                  " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.sort < 350" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub sw_enfbtn_Click(sender As System.Object, e As System.EventArgs) Handles sw_enfbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "118"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, bailiffID, bail_current from Debtor D, stage S, clientscheme CS" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and D.clientschemeID=CS._rowID" & _
                  " and CS.branchID = 24" & _
                  " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.sort >= 350 and S.sort < 600" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub sw_postenfbtn_Click(sender As System.Object, e As System.EventArgs) Handles sw_postenfbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "119"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, bailiffID, bail_current from Debtor D, stage S, clientscheme CS" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and D.clientschemeID=CS._rowID" & _
                  " and CS.branchID = 24" & _
                  " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.sort >= 600" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub mar_compbtn_Click(sender As System.Object, e As System.EventArgs) Handles mar_compbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "120"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, bailiffID, bail_current from Debtor D, stage S, clientscheme CS" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and D.clientschemeID=CS._rowID" & _
                  " and CS.branchID = 10" & _
                  " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.sort < 350" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub mar_enfbtn_Click(sender As System.Object, e As System.EventArgs) Handles mar_enfbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "121"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, bailiffID, bail_current from Debtor D, stage S, clientscheme CS" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and D.clientschemeID=CS._rowID" & _
                  " and CS.branchID = 10" & _
                  " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.sort >= 350 and S.sort < 600" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub mar_postenfbtn_Click(sender As System.Object, e As System.EventArgs) Handles mar_postenfbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "122"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, bailiffID, bail_current from Debtor D, stage S, clientscheme CS" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and D.clientschemeID=CS._rowID" & _
                  " and CS.branchID = 10" & _
                  " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.sort >= 600" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub br1_compbtn_Click(sender As System.Object, e As System.EventArgs) Handles br1_compbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "123"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, bailiffID, bail_current from Debtor D, stage S, clientscheme CS" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and D.clientschemeID=CS._rowID" & _
                  " and CS.branchID = 1" & _
                    " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.sort < 350" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub br1_enfbtn_Click(sender As System.Object, e As System.EventArgs) Handles br1_enfbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "124"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, bailiffID, bail_current from Debtor D, stage S, clientscheme CS" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and D.clientschemeID=CS._rowID" & _
                  " and CS.branchID = 1" & _
                   " and debt_balance > 1.00 " & _
                   " and D.last_stageID = S._rowID" & _
                  " and S.sort >= 350 and S.sort < 600" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub br1_postenfbtn_Click(sender As System.Object, e As System.EventArgs) Handles br1_postenfbtn.Click
        disable_buttons()
        ProgressBar1.Visible = True
        ProgressBar1.Value = 5
        mode = "125"
        param2 = "select D._rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                  " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                  " last_stageID, add_postcode, status_nextdate, D._createdDate, arrange_next, statusCode, LaststageDate, bailiffID, bail_current from Debtor D, stage S, clientscheme CS" & _
                  " where status_open_closed = 'O' and D.status = 'L' and status_hold = 'N'" & _
                  " and D.clientschemeID=CS._rowID" & _
                  " and CS.branchID = 1" & _
                  " and debt_balance > 1.00 " & _
                  " and D.last_stageID = S._rowID" & _
                  " and S.sort >= 600" & _
                  " and (add_phone is not null or add_fax is not null " & _
                  " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
    End Sub

    Private Sub adhocClientbtn_Click(sender As System.Object, e As System.EventArgs) Handles adhocClientbtn.Click
        collect_cases = False
        choose_client()
    End Sub
End Class

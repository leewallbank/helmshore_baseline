<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class schemefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.sel_ind = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.sch_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.sch_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FeesSQLDataSet = New dialler.FeesSQLDataSet()
        Me.Dialler_bailiff_clientsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Dialler_bailiff_clientsTableAdapter = New dialler.FeesSQLDataSetTableAdapters.Dialler_bailiff_clientsTableAdapter()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Dialler_bailiff_clientsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.sel_ind, Me.sch_no, Me.sch_name})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(410, 523)
        Me.DataGridView1.TabIndex = 0
        '
        'sel_ind
        '
        Me.sel_ind.HeaderText = "Selection"
        Me.sel_ind.Name = "sel_ind"
        Me.sel_ind.Width = 60
        '
        'sch_no
        '
        Me.sch_no.HeaderText = "Scheme No"
        Me.sch_no.Name = "sch_no"
        Me.sch_no.Visible = False
        '
        'sch_name
        '
        Me.sch_name.HeaderText = "Scheme"
        Me.sch_name.Name = "sch_name"
        Me.sch_name.Width = 250
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dialler_bailiff_clientsBindingSource
        '
        Me.Dialler_bailiff_clientsBindingSource.DataMember = "Dialler_bailiff_clients"
        Me.Dialler_bailiff_clientsBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'Dialler_bailiff_clientsTableAdapter
        '
        Me.Dialler_bailiff_clientsTableAdapter.ClearBeforeFill = True
        '
        'schemefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(410, 523)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "schemefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Select schemes as required"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Dialler_bailiff_clientsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents sel_ind As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents sch_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents sch_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FeesSQLDataSet As dialler.FeesSQLDataSet
    Friend WithEvents Dialler_bailiff_clientsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dialler_bailiff_clientsTableAdapter As dialler.FeesSQLDataSetTableAdapters.Dialler_bailiff_clientsTableAdapter
End Class

Public Class ooafrm

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub createbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles createbtn.Click
        ProgressBar1.Value = 5

        mode = "F"
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                 " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                 " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                 " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                 " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                 " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
        'old_create()
    End Sub
    'Private Sub old_create()
    '    'get stage names
    '    param2 = "Stage"
    '    param3 = "01_rowid 2name"
    '    param4 = "order by _rowid"
    '    ret_code = get_table(param1, param2, param3, param4)
    '    If ret_code = 1 Then
    '        Return
    '    End If
    '    Dim stage_array = table_array

    '    'load all postcode sectors into dataset
    '    postcodefrm.Out_of_areaTableAdapter.Fill(postcodefrm.FeesSQLDataSet.Out_of_area)

    '    param1 = "onestep"
    '    param2 = "Debtor"
    '    param3 = "01_rowid 2name_title 3name_forename 4name_surname" _
    '          & " 5address 6clientschemeID 7schemeID 8add_phone 9add_fax" _
    '          & "10debt_balance 11clientID 12arrange_next 13name2 14add_postcode 15last_stageID"
    '    param4 = "where  status = 'L' and status_hold = 'N' and status_open_closed = 'O'" _
    '             & " and bail_current = 'N' and (add_phone is not null or add_fax is not null)"
    '    ret_code = get_table(param1, param2, param3, param4)
    '    If ret_code = 1 Then
    '        Return
    '    End If
    '    Dim debt_rows As Integer = no_of_rows
    '    Dim debt_table = table_array

    '    Dim outline As String = ""
    '    Dim outfile As String = "Our_Reference,Title,Forename,Surname,Address,Client,Scheme,TelNumber," & _
    '      "SecondNumber,Balance,ClientID,Name2" & vbNewLine
    '    Dim col_value As String = ""
    '    Dim phone_no As String = ""
    '    Dim idx, idx2, idx3 As Integer
    '    Dim phone_array(debt_rows) As String
    '    Dim outline_array(debt_rows) As String
    '    Dim outline_idx As Integer = 0
    '    Dim nndr_case As Boolean
    '    For idx = 1 To debt_rows
    '        ProgressBar1.Value = (idx / debt_rows) * 100
    '        Dim last_stageid As Integer = debt_table(idx, 15)
    '        Dim sch_no As String = ""
    '        Dim cl_no As String = ""
    '        For idx2 = 1 To 15
    '            Dim col_result As String = ""
    '            col_value = Trim(debt_table(idx, idx2))
    '            If idx2 = 6 Then  'clientschemeID
    '                If cs_table(col_value, 4) <> 1 Then  'Branch has to be 1
    '                    outline = ""
    '                    Exit For
    '                End If
    '                cl_no = cs_table(col_value, 2)
    '                'ignore savills
    '                If cl_no = 55 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                sch_no = cs_table(col_value, 3)
    '                col_result = Trim(client_table(cl_no, 2))
    '            ElseIf idx2 = 7 Then
    '                col_result = Trim(sch_table(sch_no, 2))
    '                'ignore NNDR case unles at return stage
    '                If Microsoft.VisualBasic.Left(col_result, 4) = "NNDR" Then
    '                    nndr_case = True
    '                Else
    '                    nndr_case = False
    '                End If
    '            ElseIf idx2 = 8 Then
    '                If col_value.Length < 5 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                If phone_no = col_value And phone_no.Length > 0 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                phone_no = col_value
    '                col_result = col_value
    '            ElseIf idx2 = 9 Then
    '                col_result = col_value
    '                'taken out 24.6.2011
    '                'If phone_no.Length > 0 Then
    '                'phone_no = col_value
    '                'End If
    '            ElseIf idx2 = 10 Then
    '                col_result = Format(col_value, "fixed")
    '            ElseIf idx2 = 11 Then
    '                col_result = cl_no
    '            ElseIf idx2 = 12 Then
    '                Continue For
    '            ElseIf idx2 = 13 Then
    '                col_result = col_value
    '            ElseIf idx2 = 14 Then
    '                'check postcode sector
    '                If col_value.Length = 0 Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '                If found_pc_sector(col_value) = False Then
    '                    outline = ""
    '                    Exit For
    '                End If
    '            ElseIf idx2 = 15 Then ' stage
    '                If nndr_case And Trim(stage_array(last_stageid, 2)) <> "(Return)" Then
    '                    outline = ""
    '                    Exit For
    '                End If

    '            Else
    '                For idx3 = 1 To Len(col_value)
    '                    If Mid(col_value, idx3, 1) = vbCr Or _
    '                        Mid(col_value, idx3, 1) = "," Or _
    '                       Mid(col_value, idx3, 1) = vbLf Then
    '                        col_result = col_result & " "
    '                    Else
    '                        col_result = col_result & Mid(col_value, idx3, 1)
    '                    End If
    '                Next
    '            End If
    '            If idx2 <> 12 And idx2 <> 14 Then
    '                outline = outline & col_result & ","
    '            End If
    '        Next
    '        If Len(outline) > 0 Then
    '            outline_array(outline_idx) = outline
    '            phone_array(outline_idx) = phone_no
    '            outline_idx += 1
    '            outline = ""
    '        End If
    '    Next

    '    'sort arrays by phone numbers
    '    Array.Sort(phone_array, outline_array, 0, outline_idx)
    '    phone_no = ""
    '    ProgressBar1.Value = 5
    '    For idx = 0 To outline_idx - 1
    '        ProgressBar1.Value = (idx / outline_idx) * 100
    '        If phone_no = phone_array(idx) Then
    '            Continue For
    '        End If
    '        outline = outline_array(idx)
    '        If Len(outline) > 0 Then
    '            outfile = outfile & outline & vbNewLine
    '        End If
    '        phone_no = phone_array(idx)
    '    Next
    '    ProgressBar1.Value = 100
    '    'write out file
    '    With SaveFileDialog1
    '        .Title = "Save file"
    '        .Filter = "CSV files |*.csv"
    '        .DefaultExt = ".csv"
    '        .OverwritePrompt = True
    '        .InitialDirectory = "c:\temp"
    '        .FileName = "dialler.csv"
    '    End With

    '    Dim filepath As String = SaveFileDialog1.FileName
    '    If Len(outfile) = 0 Then
    '        MessageBox.Show("There are no cases selected")
    '    Else
    '        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
    '            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
    '        End If
    '    End If
    '    Me.Close()
    'End Sub

    Private Sub ooafrm_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        mainform.Close()
    End Sub

    Private Sub ooafrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub maintainbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles maintainbtn.Click
        postcodefrm.ShowDialog()
    End Sub
End Class
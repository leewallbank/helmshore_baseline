﻿Imports System.IO

Public Class Form1

    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub createbtn_Click(sender As System.Object, e As System.EventArgs) Handles createbtn.Click
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD506report = New RD506
        Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(rundtp.Value)
        SetCurrentValuesForParameterField1(RD506report, myArrayList1)

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD506report)
        Dim filename As String = ""
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RD506 Cases with same client ref.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            createbtn.Enabled = False
            exitbtn.Enabled = False

            filename = SaveFileDialog1.FileName
            RD506report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelWorkbook, filename)
            RD506report.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
        Me.Close()

    End Sub
End Class

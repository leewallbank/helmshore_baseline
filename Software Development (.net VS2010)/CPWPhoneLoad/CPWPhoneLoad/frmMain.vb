﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain
    Dim debtorArray(9999, 4) As Integer
    Private InputFilePath As String, FileName As String, FileExt As String
    Dim phone1Report As String = "DebtorID,PhoneNumber1,PhoneNumber2,Emp Phone1,Emp Phone2" & vbNewLine
    'Dim phone2Report As String = "DebtorID,PhoneNumber2" & vbNewLine
    'Dim phone3Report As String = "DebtorID,Emp Phone1" & vbNewLine
    'Dim phone4Report As String = "DebtorID,Emp Phone2" & vbNewLine
    Dim phoneMax As Integer = 0

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            btnProcessFile.Enabled = False
            Dim FileDialog As New OpenFileDialog
            Dim OutputFile As String = ""
            Dim totValue As Decimal = 0
            Dim totCases As Integer = 0

            Dim InputLine As String
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim filecontents(,) As String = InputFromExcel(FileDialog.FileName)
            ProgressBar.Maximum = UBound(filecontents)
            Dim rowMax As Integer = UBound(filecontents)
            For rowIDX = 1 To rowMax
                Try
                    ProgressBar.Value = rowIDX
                Catch ex As Exception

                End Try
                Application.DoEvents()
                InputLine = filecontents(rowIDX, 0)
                If InputLine.Length = 0 Then
                    Continue For
                End If
                Try
                    Dim arrowKey As String = filecontents(rowIDX, 0)
                    'get debtorID
                    Dim debt_dt As New DataTable
                    LoadDataTable("DebtRecovery", "select D._rowID, D.add_phone, D.add_fax, D.empPhone, D.empFax " & _
                                  " from Debtor D, clientscheme CS " & _
                                  " where D.client_ref = '" & arrowKey & "'" & _
                                  " and D.clientschemeID = CS._rowID" & _
                                  " and D.status_open_closed = 'O'" & _
                                  " and CS.clientID =1414", debt_dt, False)
                    For Each debtRow In debt_dt.Rows
                        Dim debtorID As Integer = debtRow(0)
                        Dim addPhone As String = ""
                        Dim addFax As String = ""
                        Dim empPhone As String = ""
                        Dim empFax As String = ""
                        Try
                            addPhone = Trim(debtRow(1))
                        Catch ex As Exception

                        End Try
                        Try
                            addFax = Trim(debtRow(2))
                        Catch ex As Exception

                        End Try
                        Try
                            empPhone = Trim(debtRow(3))
                        Catch ex As Exception

                        End Try
                        Try
                            empFax = Trim(debtRow(4))
                        Catch ex As Exception

                        End Try
                        For ColIDX = 1 To 4
                            Dim phoneNumber As String = filecontents(rowIDX, ColIDX)
                            If IsNumeric(phoneNumber) Then
                                'see if already on onestep
                                If phoneNumber <> addPhone And phoneNumber <> addFax And _
                                    phoneNumber <> empPhone And phoneNumber <> empFax Then
                                    Dim reportNo As Integer = 0
                                    reportNo = checkDebtor(debtorID, addPhone, addFax, empPhone, empFax)
                                    Select Case reportNo
                                        Case 1
                                            phone1Report &= debtorID & "," & phoneNumber & ",,," & vbNewLine
                                        Case 2
                                            phone1Report &= debtorID & ",," & phoneNumber & ",," & vbNewLine
                                        Case 3
                                            phone1Report &= debtorID & ",,," & phoneNumber & "," & vbNewLine
                                        Case 4
                                            phone1Report &= debtorID & ",,,," & phoneNumber & vbNewLine
                                    End Select
                                End If
                            End If
                        Next
                    Next
                Catch ex As Exception
                    HandleException(ex)
                End Try

            Next
            WriteFile(InputFilePath & FileName & "_phone.txt", phone1Report)
            'WriteFile(InputFilePath & FileName & "_phone2.txt", phone2Report)
            'WriteFile(InputFilePath & FileName & "_Empphone1.txt", phone3Report)
            'WriteFile(InputFilePath & FileName & "_Empphone2.txt", phone4Report)
            MsgBox("Files saved")
            Me.Close()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
    Private Function checkDebtor(ByVal DebtorID As Integer, ByVal addPhone As String, ByVal addFax As String, ByVal empPhone As String, ByVal empfax As String) As Integer
        Dim reportID As Integer = 1
        'see if case has already been processed
        For phoneIDX = 1 To phoneMax
            If debtorArray(phoneIDX, 1) = DebtorID Then
                reportID = debtorArray(phoneIDX, 2)
                For repIDX = reportID To 4
                    Select Case repIDX
                        Case 1
                            If addFax = "" Then
                                reportID = 2
                                debtorArray(phoneIDX, 2) = reportID
                                Exit For
                            End If
                        Case 2
                            If empPhone = "" Then
                                reportID = 3
                                debtorArray(phoneIDX, 2) = reportID
                                Exit For
                            End If
                        Case 3
                            If empfax = "" Then
                                reportID = 4
                                debtorArray(phoneIDX, 2) = reportID
                                Exit For
                            End If
                        Case 4
                            reportID = 5
                            debtorArray(phoneIDX, 2) = reportID
                            Exit For
                    End Select
                Next
                Exit For
            End If
        Next
        If reportID = 1 Then
            phoneMax += 1
            debtorArray(phoneMax, 1) = DebtorID
            If addPhone = "" Then
                reportID = 1
            ElseIf addFax = "" Then
                reportID = 2
            ElseIf empPhone = "" Then
                reportID = 3
            ElseIf empfax = "" Then
                reportID = 4
            Else
                reportID = 5
            End If
            debtorArray(phoneMax, 2) = reportID

        End If

        Return (reportID)
    End Function

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & "_preprocess.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_preprocess.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & "_nonWRP_4200.txt") Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_nonWRP_4200.txt" & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private TDXCMGData As New TDXCMGData
    Private InputFilePath As String, FileName As String, FileExt As String

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim LineNumber As Integer, NewCases As Integer = 0, ColumbusCases As Integer = 0
            Dim OutputLine As String
            Dim AuditLog As String, ErrorLog As String = "", InputLineArray() As String, DebtorID As String

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            lblReadingFile.Visible = True
            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents)

            For Each OldFile As String In Directory.GetFiles(InputFilePath, FileName & "_*.txt")
                File.Delete(OldFile)
            Next OldFile

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                If LineNumber = 1 Or LineNumber = UBound(FileContents) + 1 Then Continue For ' skip header and trailer line

                OutputLine = ""
                InputLine = InputLine.Replace("|", "") ' added out TS. Request ref 104346
                InputLineArray = Split(InputLine, ",", """")  ' added out TS. Request ref 104346

                If UBound(InputLineArray) + 1 < 119 Or UBound(InputLineArray) + 1 > 120 Then ' if the last column is blank, the array will be one element shorter
                    ErrorLog &= "Unexpected line length of " & (UBound(InputLineArray) + 1).ToString & " items found at line number " & LineNumber.ToString & ". Line not loaded." & vbCrLf
                    Continue For
                End If

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                If InputLineArray(12) <> "PRAW" And InputLineArray(12) <> "PRAWL" Then

                    DebtorID = TDXCMGData.GetDebtorID(InputLineArray(1))

                    If DebtorID <> "" Then

                        OutputLine &= DebtorID & "|"

                        OutputLine &= InputLineArray(0) & "|"

                        OutputLine &= ToNote(InputLineArray(4), "Client name", ";")(0)
                        OutputLine &= ToNote(InputLineArray(7), "Client Brand", ";")(0)
                        OutputLine &= ToNote(InputLineArray(9), "Assignment ID", ";")(0)
                        OutputLine &= ToNote(InputLineArray(10), "Assignment Type Code", ";")(0)
                        OutputLine &= ToNote(InputLineArray(12), "Segment Name", ";")(0)

                        OutputLine &= vbCrLf

                        NewCases += 1

                    Else
                        ErrorLog &= "Cannot find open DebtorID for client ref " & InputLineArray(1).Replace("""", "") & " at line number " & LineNumber.ToString & vbCrLf
                    End If

                Else

                    ColumbusCases += 1

                End If

                AppendToFile(InputFilePath & FileName & "_PreProcessed.txt", OutputLine)

            Next InputLine

            InputLineArray = FileContents(0).Split(",") ' Check header record case count

            AuditLog = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
            AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

            AuditLog &= "Number of new cases: " & NewCases.ToString & vbCrLf
            AuditLog &= "Number of Columbus cases: " & ColumbusCases.ToString & vbCrLf

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
                MessageBox.Show("Errors found.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

            MessageBox.Show("Pre-processing complete.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class

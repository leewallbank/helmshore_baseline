﻿Imports System.IO
Imports commonlibrary

Public Class frmMain

    Private Sub btnSplitfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSplitfile.Click
        Dim FileDialog As New OpenFileDialog
        Dim InputFilePath As String, FileName As String, FileExt As String
        Dim OutputFile As String = ""
        Dim LineNumber As Integer, FileCount As Integer = 1, RowCount As Integer = 0

        Try
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)

            If UBound(FileContents) = 0 Then
                MsgBox("No file contents to split", vbOKOnly)
                Return
            End If

            If chkHasHeader.Checked Then ' If and Else added TS 21/May/2013
                RowCount = 0
            Else
                RowCount = 1
            End If

            ProgressBar.Maximum = UBound(FileContents)

            For Each InputLine As String In FileContents
                OutputFile &= InputLine & vbCrLf
                ProgressBar.Value = LineNumber
                LineNumber += 1
                RowCount += 1
                Application.DoEvents() ' without this line, the button disappears until processing is complete

                If RowCount > CInt(txtNumRows.Text) Then ' Not equal so as not to include the header

                    WriteFile(InputFilePath & FileName & "_File" & FileCount.ToString & FileExt, OutputFile)
                    FileCount += 1

                    If chkHasHeader.Checked Then ' If and Else added TS 21/May/2013
                        OutputFile = FileContents(0) & vbCrLf ' Add header again
                    Else
                        OutputFile = ""
                    End If
                    RowCount = 1
                End If

            Next InputLine

            WriteFile(InputFilePath & FileName & "_File" & FileCount.ToString & FileExt, OutputFile)

            MsgBox("Split complete" & vbCrLf & FileCount.ToString & " file" & If(FileCount > 1, "s", "") & " created" & vbCrLf & If(chkHasHeader.Checked, (LineNumber - 1).ToString, LineNumber.ToString) & " data lines", vbOKOnly, "Complete")

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.btnSplitfile = New System.Windows.Forms.Button()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.txtNumRows = New System.Windows.Forms.TextBox()
        Me.lblNumRows = New System.Windows.Forms.Label()
        Me.chkHasHeader = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'btnSplitfile
        '
        Me.btnSplitfile.Location = New System.Drawing.Point(86, 114)
        Me.btnSplitfile.Name = "btnSplitfile"
        Me.btnSplitfile.Size = New System.Drawing.Size(120, 30)
        Me.btnSplitfile.TabIndex = 0
        Me.btnSplitfile.Text = "Split File"
        Me.btnSplitfile.UseVisualStyleBackColor = True
        '
        'ProgressBar
        '
        Me.ProgressBar.Location = New System.Drawing.Point(30, 182)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(233, 26)
        Me.ProgressBar.TabIndex = 1
        '
        'txtNumRows
        '
        Me.txtNumRows.Location = New System.Drawing.Point(167, 29)
        Me.txtNumRows.Name = "txtNumRows"
        Me.txtNumRows.Size = New System.Drawing.Size(55, 20)
        Me.txtNumRows.TabIndex = 2
        Me.txtNumRows.Text = "500"
        '
        'lblNumRows
        '
        Me.lblNumRows.AutoSize = True
        Me.lblNumRows.Location = New System.Drawing.Point(68, 32)
        Me.lblNumRows.Name = "lblNumRows"
        Me.lblNumRows.Size = New System.Drawing.Size(91, 13)
        Me.lblNumRows.TabIndex = 3
        Me.lblNumRows.Text = "Data lines per file:"
        '
        'chkHasHeader
        '
        Me.chkHasHeader.AutoSize = True
        Me.chkHasHeader.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.chkHasHeader.Checked = True
        Me.chkHasHeader.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkHasHeader.Location = New System.Drawing.Point(97, 65)
        Me.chkHasHeader.Name = "chkHasHeader"
        Me.chkHasHeader.Size = New System.Drawing.Size(98, 17)
        Me.chkHasHeader.TabIndex = 4
        Me.chkHasHeader.Text = "File has header"
        Me.chkHasHeader.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 241)
        Me.Controls.Add(Me.chkHasHeader)
        Me.Controls.Add(Me.lblNumRows)
        Me.Controls.Add(Me.txtNumRows)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.btnSplitfile)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Text file splitter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSplitfile As System.Windows.Forms.Button
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents txtNumRows As System.Windows.Forms.TextBox
    Friend WithEvents lblNumRows As System.Windows.Forms.Label
    Friend WithEvents chkHasHeader As System.Windows.Forms.CheckBox

End Class

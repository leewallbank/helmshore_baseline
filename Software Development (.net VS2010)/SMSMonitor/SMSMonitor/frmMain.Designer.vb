﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.cmdReset = New System.Windows.Forms.Button()
        Me.lblSentCount = New System.Windows.Forms.Label()
        Me.lblLastReset = New System.Windows.Forms.Label()
        Me.tmrRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.lblSent = New System.Windows.Forms.Label()
        Me.lblWaiting = New System.Windows.Forms.Label()
        Me.lblWaitingCount = New System.Windows.Forms.Label()
        Me.lblFailed = New System.Windows.Forms.Label()
        Me.lblFailedCount = New System.Windows.Forms.Label()
        Me.lblSim = New System.Windows.Forms.Label()
        Me.lblSimNum = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'cmdReset
        '
        Me.cmdReset.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdReset.Location = New System.Drawing.Point(75, 213)
        Me.cmdReset.Name = "cmdReset"
        Me.cmdReset.Size = New System.Drawing.Size(142, 47)
        Me.cmdReset.TabIndex = 0
        Me.cmdReset.Text = "Reset"
        Me.cmdReset.UseVisualStyleBackColor = True
        '
        'lblSentCount
        '
        Me.lblSentCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSentCount.Location = New System.Drawing.Point(155, 13)
        Me.lblSentCount.Name = "lblSentCount"
        Me.lblSentCount.Size = New System.Drawing.Size(91, 30)
        Me.lblSentCount.TabIndex = 1
        Me.lblSentCount.Text = "0"
        Me.lblSentCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblLastReset
        '
        Me.lblLastReset.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLastReset.Location = New System.Drawing.Point(46, 173)
        Me.lblLastReset.Name = "lblLastReset"
        Me.lblLastReset.Size = New System.Drawing.Size(200, 30)
        Me.lblLastReset.TabIndex = 2
        Me.lblLastReset.Text = "Label1"
        Me.lblLastReset.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tmrRefresh
        '
        Me.tmrRefresh.Enabled = True
        Me.tmrRefresh.Interval = 60000
        '
        'lblSent
        '
        Me.lblSent.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSent.Location = New System.Drawing.Point(46, 13)
        Me.lblSent.Name = "lblSent"
        Me.lblSent.Size = New System.Drawing.Size(91, 30)
        Me.lblSent.TabIndex = 3
        Me.lblSent.Text = "Sent:"
        Me.lblSent.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWaiting
        '
        Me.lblWaiting.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWaiting.Location = New System.Drawing.Point(46, 53)
        Me.lblWaiting.Name = "lblWaiting"
        Me.lblWaiting.Size = New System.Drawing.Size(91, 30)
        Me.lblWaiting.TabIndex = 5
        Me.lblWaiting.Text = "Waiting:"
        Me.lblWaiting.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblWaitingCount
        '
        Me.lblWaitingCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWaitingCount.Location = New System.Drawing.Point(155, 53)
        Me.lblWaitingCount.Name = "lblWaitingCount"
        Me.lblWaitingCount.Size = New System.Drawing.Size(91, 30)
        Me.lblWaitingCount.TabIndex = 4
        Me.lblWaitingCount.Text = "0"
        Me.lblWaitingCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblFailed
        '
        Me.lblFailed.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFailed.Location = New System.Drawing.Point(46, 93)
        Me.lblFailed.Name = "lblFailed"
        Me.lblFailed.Size = New System.Drawing.Size(91, 30)
        Me.lblFailed.TabIndex = 7
        Me.lblFailed.Text = "Failed:"
        Me.lblFailed.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFailedCount
        '
        Me.lblFailedCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFailedCount.Location = New System.Drawing.Point(155, 93)
        Me.lblFailedCount.Name = "lblFailedCount"
        Me.lblFailedCount.Size = New System.Drawing.Size(91, 30)
        Me.lblFailedCount.TabIndex = 6
        Me.lblFailedCount.Text = "0"
        Me.lblFailedCount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSim
        '
        Me.lblSim.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSim.Location = New System.Drawing.Point(46, 133)
        Me.lblSim.Name = "lblSim"
        Me.lblSim.Size = New System.Drawing.Size(91, 30)
        Me.lblSim.TabIndex = 9
        Me.lblSim.Text = "Sim #:"
        Me.lblSim.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblSimNum
        '
        Me.lblSimNum.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSimNum.Location = New System.Drawing.Point(155, 133)
        Me.lblSimNum.Name = "lblSimNum"
        Me.lblSimNum.Size = New System.Drawing.Size(91, 30)
        Me.lblSimNum.TabIndex = 8
        Me.lblSimNum.Text = "0"
        Me.lblSimNum.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 271)
        Me.Controls.Add(Me.lblSim)
        Me.Controls.Add(Me.lblSimNum)
        Me.Controls.Add(Me.lblFailed)
        Me.Controls.Add(Me.lblFailedCount)
        Me.Controls.Add(Me.lblWaiting)
        Me.Controls.Add(Me.lblWaitingCount)
        Me.Controls.Add(Me.lblSent)
        Me.Controls.Add(Me.lblLastReset)
        Me.Controls.Add(Me.lblSentCount)
        Me.Controls.Add(Me.cmdReset)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SMS Monitor"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdReset As System.Windows.Forms.Button
    Friend WithEvents lblSentCount As System.Windows.Forms.Label
    Friend WithEvents lblLastReset As System.Windows.Forms.Label
    Friend WithEvents tmrRefresh As System.Windows.Forms.Timer
    Friend WithEvents lblSent As System.Windows.Forms.Label
    Friend WithEvents lblWaiting As System.Windows.Forms.Label
    Friend WithEvents lblWaitingCount As System.Windows.Forms.Label
    Friend WithEvents lblFailed As System.Windows.Forms.Label
    Friend WithEvents lblFailedCount As System.Windows.Forms.Label
    Friend WithEvents lblSim As System.Windows.Forms.Label
    Friend WithEvents lblSimNum As System.Windows.Forms.Label

End Class

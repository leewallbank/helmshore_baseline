﻿Public Class clsSMSData
    Private SMSDS As New DataSet
    Private SMSStatusDV As DataView

    Public Sub New()
        SMSDS.Tables.Add("SMSStatus")
    End Sub

    Protected Overrides Sub Finalize()
        If Not IsNothing(SMSStatusDV) Then SMSStatusDV.Dispose()

        If Not IsNothing(SMSDS) Then SMSDS.Dispose() : SMSDS = Nothing
        MyBase.Finalize()
    End Sub

    Public ReadOnly Property SMSStatusDataView() As DataView
        Get
            SMSStatusDataView = SMSStatusDV
        End Get
    End Property

    Public Sub GetSMSStatus(ByVal ResetDatetime As DateTime)
        Dim Sql As String

        Try
            Sql = "SELECT IFNULL(SUM(CASE WHEN status = 'S' THEN 1 ELSE 0 END), 0) AS Sent " & _
                  "     , IFNULL(SUM(CASE WHEN status = 'W' THEN 1 ELSE 0 END), 0) AS Waiting " & _
                  "     , IFNULL(SUM(CASE WHEN status = 'F' THEN 1 ELSE 0 END), 0) AS Failed " & _
                  "FROM Message " & _
                  "WHERE ( status = 'S' AND timeLastSent > '" & ResetDatetime.ToString("yyyy-MM-dd HH:mm:ss") & "') " & _
                  "   OR  status IN ('W','F')"

            LoadDataTable(Sql, SMSDS, "SMSStatus", False)

            SMSStatusDV = New DataView(SMSDS.Tables("SMSStatus"))
            SMSStatusDV.AllowDelete = False
            SMSStatusDV.AllowNew = False

        Catch ex As Exception
            frmMain.tmrRefresh.Enabled = False
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub GetSMSStatus(ByVal ResetDatetime As DateTime, ByVal RefreshDatetime As DateTime)
        Dim Sql As String

        Try
            Sql = "SELECT IFNULL(SUM(CASE WHEN status = 'S' THEN 1 ELSE 0 END), 0) AS Sent " & _
                  "     , IFNULL(SUM(CASE WHEN status = 'W' THEN 1 ELSE 0 END), 0) AS Waiting " & _
                  "     , IFNULL(SUM(CASE WHEN status = 'F' THEN 1 ELSE 0 END), 0) AS Failed " & _
                  "FROM Message " & _
                  "WHERE ( status = 'S' AND timeLastSent > '" & ResetDatetime.ToString("yyyy-MM-dd HH:mm:ss") & "') " & _
                  "   OR ( status IN ('W','F') AND senderDate <= '" & RefreshDatetime.ToString("yyyy-MM-dd HH:mm:ss") & "')"

            LoadDataTable(Sql, SMSDS, "SMSStatus", False)

            SMSStatusDV = New DataView(SMSDS.Tables("SMSStatus"))
            SMSStatusDV.AllowDelete = False
            SMSStatusDV.AllowNew = False

        Catch ex As Exception
            frmMain.tmrRefresh.Enabled = False
            MsgBox(ex.ToString)
        End Try
    End Sub
End Class

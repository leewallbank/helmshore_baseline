﻿Imports System.Xml
Imports System.ServiceProcess
Imports System.Configuration
Imports System.Collections.Specialized
Imports System.Net.Mail

Public Class frmMain

    Dim AppSettings As NameValueCollection
    Dim SMS As New clsSMSData
    Dim Threshold1MsgSent As Boolean = False, Threshold2MsgSent As Boolean = False
    Dim SMSProcess As Process
    Dim loXMLDoc As XmlDocument = New XmlDocument
    Dim loNode As XmlNode
    Dim LastRefresh? As DateTime = Nothing

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        AppSettings = ConfigurationManager.AppSettings()

        'StartProcess()

        loXMLDoc.Load("appsettings.config")
        loNode = loXMLDoc.SelectSingleNode("descendant::appSettings/LastRefresh")
        lblLastReset.Text = loNode.InnerText
        loNode = loXMLDoc.SelectSingleNode("descendant::appSettings/SimNum")
        lblSimNum.Text = loNode.InnerText

        RefreshTotal()

    End Sub

    Private Sub RefreshTotal()
        Try
            'If Not IsNothing(LastRefresh) Then
            '    SMS.GetSMSStatus(lblLastReset.Text, LastRefresh)
            '    If CInt(lblWaitingCount.Text) > 0 And CInt(lblWaitingCount.Text) = SMS.SMSStatusDataView.Table.Rows(0).Item("Waiting") Then MsgBox("Nothing sent since last refresh.", vbCritical + vbOKOnly, Me.Text)
            'End If

            SMS.GetSMSStatus(lblLastReset.Text)

            lblSentCount.Text = SMS.SMSStatusDataView.Table.Rows(0).Item("Sent").ToString
            lblWaitingCount.Text = SMS.SMSStatusDataView.Table.Rows(0).Item("Waiting").ToString
            lblFailedCount.Text = SMS.SMSStatusDataView.Table.Rows(0).Item("Failed").ToString

            If CInt(lblSentCount.Text) >= CInt(AppSettings.Item("Threshold2")) Then
                lblSentCount.ForeColor = Color.Red
                ' SMSProcess.Kill()
                'If Not Threshold2MsgSent Then SendMail(AppSettings.Item("Threshold2Subject"), AppSettings.Item("Threshold2Message"))
                Threshold2MsgSent = True
            ElseIf CInt(lblSentCount.Text) >= CInt(AppSettings.Item("Threshold1")) Then
                lblSentCount.ForeColor = Color.Yellow
                'If Not Threshold1MsgSent Then SendMail(AppSettings.Item("Threshold1Subject"), AppSettings.Item("Threshold1Message"))
                Threshold1MsgSent = True
            Else
                lblSentCount.ForeColor = Color.Black
            End If

            loXMLDoc.Load("appsettings.config")
            loNode = loXMLDoc.SelectSingleNode("descendant::appSettings/LastRefresh")
            lblLastReset.Text = loNode.InnerText

            loNode = loXMLDoc.SelectSingleNode("descendant::appSettings/SimNum")
            lblSimNum.Text = loNode.InnerText

            LastRefresh = Now

            If SMS.SMSStatusDataView.Table.Rows(0).Item("Failed") >= 20 Then Me.Activate()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub cmdReset_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdReset.Click
        Try
            If MsgBox("Are you sure?", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, Me.Text) = MsgBoxResult.No Then Return

            Cursor = Cursors.WaitCursor

            loXMLDoc.Load("appsettings.config")
            loNode = loXMLDoc.SelectSingleNode("descendant::appSettings/LastRefresh")
            loNode.InnerText = Now

            loNode = loXMLDoc.SelectSingleNode("descendant::appSettings/SimNum")
            loNode.InnerText = CInt(loNode.InnerText) + 1
            If loNode.InnerText = "11" Then loNode.InnerText = "1"

            loXMLDoc.Save("appsettings.config")

            RefreshTotal()

            'StartProcess()

            Threshold1MsgSent = False
            Threshold2MsgSent = False

            Cursor = Cursors.Default
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub tmrRefresh_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmrRefresh.Tick
        RefreshTotal()
    End Sub

    Private Sub SendMail(ByVal Subject As String, ByVal Body As String)
        Try
            Dim Message As MailMessage = New MailMessage(AppSettings.Item("MailFrom"), AppSettings.Item("MailTo"))
            Dim SmtpClient As New SmtpClient(AppSettings.Item("SmtpClient").ToString)

            Message.Subject = Subject
            Message.Body = Body
            SmtpClient.Send(Message)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub StartProcess()
        Try
            Dim Processes() As System.Diagnostics.Process = System.Diagnostics.Process.GetProcessesByName(AppSettings.Item("ProcessName").ToString)
            If Processes.Count = 0 Then
                SMSProcess = Process.Start(AppSettings.Item("ProcessPath").ToString & AppSettings.Item("ProcessName").ToString)
            Else
                SMSProcess = Processes(0)
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub
End Class

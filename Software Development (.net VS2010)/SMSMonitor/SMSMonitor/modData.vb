﻿Imports System.Configuration
Imports System.Data
Imports System.Data.Odbc

Module modData
    Public DBCon As New OdbcConnection

    Public Sub ConnectDb()
        Try
            If Not IsNothing(DBCon) Then
                'This is only necessary following an exception...
                If DBCon.State = ConnectionState.Open Then DBCon.Close()
            End If

            DBCon.ConnectionString = ConfigurationManager.ConnectionStrings("SMSMonitor").ConnectionString
            DBCon.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings("SMSMonitor").ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings("SMSMonitor").ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub

    Public Sub DisconnectDb()
        DBCon.Close()
        DBCon.Dispose()
    End Sub

    Public Sub ClearDataTable(ByRef DataSet As DataSet, ByVal DataTable As String)
        DataSet.Tables(DataTable).Clear()
    End Sub

    Public Sub LoadDataTable(ByVal Sql As String, ByRef DataSet As DataSet, ByVal DataTable As String, Optional ByVal Preserve As Boolean = False)
        Try
            Dim da As New OdbcDataAdapter(Sql, DBCon)

            ConnectDb()

            If Preserve = False Then ClearDataTable(DataSet, DataTable)

            da.Fill(DataSet, DataTable)

            da.Dispose()
            da = Nothing

            DisconnectDb()

        Catch ex As Exception
            MsgBox("An error has occurred - the error message is:" & Chr(13) & Chr(13) & ex.Message)
        End Try

    End Sub

End Module


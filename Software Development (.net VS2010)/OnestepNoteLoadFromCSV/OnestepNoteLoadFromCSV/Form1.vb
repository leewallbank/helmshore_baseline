﻿Imports CommonLibrary
Imports System.IO
Public Class Form1

    Private InputFilePath As String, FileName As String, FileExt As String
    Private Sub readbtn_Click(sender As System.Object, e As System.EventArgs) Handles readbtn.Click
        Dim FileDialog As New OpenFileDialog
        Dim OutputFile As String = ""
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        exitbtn.Enabled = False
        readbtn.Enabled = False

        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)

        Dim excel_file_contents(,) As String = InputFromExcel(FileDialog.FileName)
        Dim tag(146) As String
        For rowIDX = 0 To UBound(excel_file_contents)

            Dim debtorID As Integer
            If rowIDX > 0 Then
                Try
                    debtorID = excel_file_contents(rowIDX, 0)
                Catch ex As Exception
                    Continue For
                End Try
            End If
            'write out row
            For colIdx = 0 To 146
               
                Dim field As String = excel_file_contents(rowIDX, colIdx)
                field = Replace(field, ",", " ")
                If field = "" Then
                    Continue For
                End If
                Select Case colIdx
                    Case 8, 9, 12, 13, 14, 15, 16, 17, 18, 36, 37, 43, 46, 47, 48, 49, 51, 52, 53, 54, 55, 56, 57, 62, 65, 84, 129, 132
                        'first row contains the tags
                        If rowIDX = 0 Then
                            'save tags
                            tag(colIdx) = field
                            Continue For
                        Else
                            OutputFile &= debtorID & "|" & tag(colIdx) & ":" & field & ";" & vbNewLine
                        End If
                End Select
            Next

            'get any open links
            Dim debtdt As New DataTable
            LoadDataTable("DebtRecovery", "select D2._rowID from debtor D, debtor D2 " & _
                          " where D.linkID = D2.linkID " & _
                          " and D2.status_open_closed = 'O'" & _
                          " and D._rowID = " & debtorID & _
                          " and D._rowID <> D2._rowID", debtdt, False)
            For Each debtRow In debtdt.Rows
                Dim linkDebtorID As Integer = debtRow(0)
                'write out a new line
                For colIdx = 1 To 146
                    Dim field As String = excel_file_contents(rowIDX, colIdx)
                    field = Replace(field, ",", " ")
                    If field = "" Then
                        Continue For
                    End If
                    Select Case colIdx
                        Case 8, 9, 12, 13, 14, 15, 16, 17, 18, 36, 37, 43, 46, 47, 48, 49, 51, 52, 53, 54, 55, 56, 57, 62, 65, 84, 129, 132
                            OutputFile &= linkDebtorID & "|" & tag(colIdx) & ":" & field & ";" & vbNewLine
                    End Select
                Next
            Next
        Next
        Dim outFileName As String = InputFilePath & "NoteLoadReport.txt"
        My.Computer.FileSystem.WriteAllText(outFileName, OutputFile, False)
        MsgBox("Report Created")
        Me.Close()
    End Sub

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

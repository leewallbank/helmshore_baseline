﻿Imports CommonLibrary
'Imports System.Configuration
'Imports System.IO
Public Class MainForm
    Dim bail_dt As New DataTable
    Dim null_date As Date = CDate("Jan 1, 1900")
    Dim errorFile As String = ""
    Dim fourtyDaysAgo As Date = DateAdd(DateInterval.Day, -40, Now)
    'Dim eightyDaysAgo As Date = DateAdd(DateInterval.Day, -80, Now)


    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim clientArray As Object()
        ConnectDb2("DebtRecoveryLocal")
        ConnectDb2Fees("FeesSQL")
        Dim start_date As Date = CDate("2014 Apr 6")
        Dim upd_txt As String
        

        'get all entries on BAt where de-allocation date < allocation date and not null
        Dim bail22_dt As New DataTable
        LoadDataTable2("FeesSQL", "SELECT bail_ID, bail_debtorID, bail_seq_no, bail_allocation_date " & _
                                            "FROM BailiffAllocationTable " & _
                                            "where bail_de_allocation_found_date <> '" & null_date & "'" & _
                                            " and bail_de_allocation_found_date < bail_allocation_date", bail22_dt, False)
        For Each bail22Row In bail22_dt.Rows
            Dim bailID As Integer = bail22Row(0)
            Dim debtorID As Integer = bail22Row(1)
            Dim seqNo As Integer = bail22Row(2)
            Dim bailAllocationdate As Date = bail22Row(3)
            'get correct de-allocation date

            Dim bail23_dt As New DataTable
            LoadDataTable2("DebtRecoveryLocal", "SELECT bailiffID, bail_allocated, bail_current, return_date, status_open_closed " & _
                                           " FROM debtor" & _
                                           " WHERE _rowID = " & debtorID, bail23_dt, False)
            If bail23_dt.Rows.Count = 0 Then
                Continue For
            End If
            'see if still allocated
            If bail23_dt.Rows(0).Item(0) = bailID And bail23_dt.Rows(0).Item(2) = "Y" And bail23_dt.Rows(0).Item(1) = bailAllocationdate Then
                upd_txt = "update BailiffAllocationTable set bail_de_allocation_found_date = '" & null_date & "'" & _
                        " where bail_seq_no = " & seqNo
                update_sql(upd_txt)
            Else
                'use debtor allocation date if after allocation date
                If Format(bail23_dt.Rows(0).Item(1), "yyyy-MM-dd") > Format(bailAllocationdate, "yyyy-MM-dd") Then
                    upd_txt = "update BailiffAllocationTable set bail_de_allocation_found_date = '" & Format(bail23_dt.Rows(0).Item(1), "yyyy-MM-dd") & "'" & _
                            " where bail_seq_no = " & seqNo
                    update_sql(upd_txt)
                Else
                    'get from notes
                    Dim note_dt As New DataTable
                    LoadDataTable2("DebtRecoveryLocal", "SELECT _createdDate,type " & _
                                                        "FROM note " & _
                                                        "WHERE (type = 'Removed agent'" & _
                                                        " or type = 'Allocated')" & _
                                                        " and DebtorID = " & debtorID & _
                                                        " and _createdDate > '" & Format(bailAllocationdate, "yyyy-MM-dd") & "'" & _
                                                        " order by _rowID", note_dt, False)
                    Dim noteFound As Boolean = False
                    For Each noteRow In note_dt.Rows
                        If Format(bailAllocationdate, "yyyy-MM-dd") = Format(noteRow(0), "yyyy-MM-dd") Then
                            Continue For
                        End If
                        upd_txt = "update BailiffAllocationTable set bail_de_allocation_found_date = '" & Format(noteRow(0), "yyyy-MM-dd") & "'" & _
                          " where bail_seq_no = " & seqNo
                        update_sql(upd_txt)
                        noteFound = True
                        Exit For
                    Next
                    If noteFound Then
                        Continue For
                    End If
                    'use return date if case is closed
                    'if return date before allocation date delete entry
                    If bail23_dt.Rows(0).Item(4) = "C" Then
                        If Format(bail23_dt.Rows(0).Item(3), "yyyy-MM-dd") < Format(bailAllocationdate, "yyyy-MM-dd") Then
                            upd_txt = "delete from BailiffAllocationTable " & _
                                      " WHERE bail_seq_no = " & seqNo
                            update_sql(upd_txt)
                        Else
                            'use return date
                            upd_txt = "update BailiffAllocationTable set bail_de_allocation_found_date = '" & Format(bail23_dt.Rows(0).Item(3), "yyyy-MM-dd") & "'" & _
                              " where bail_seq_no = " & seqNo
                            update_sql(upd_txt)
                        End If
                    Else
                        'case open so use today
                        upd_txt = "update BailiffAllocationTable set bail_de_allocation_found_date = '" & Format(Now, "yyyy-MM-dd") & "'" & _
                             " where bail_seq_no = " & seqNo
                        update_sql(upd_txt)
                    End If

                End If
            End If
        Next



        Dim bail2_dt As New DataTable
        'get all entries on BAT where de-allocation found date is null
        LoadDataTable2("FeesSQL", "SELECT bail_ID, bail_debtorID, bail_seq_no, bail_allocation_date " & _
                                             "FROM BailiffAllocationTable " & _
                                             "where bail_de_allocation_found_date = '" & null_date & "'", bail2_dt, False)
        For Each bail2Row In bail2_dt.Rows
            Dim bailID As Integer = bail2Row(0)
            Dim debtorID As Integer = bail2Row(1)
            Dim seqNo As Long = bail2Row(2)
            'now see if another exists for same case and bailiff ID but different seq No
            Dim bail3_dt As New DataTable
            LoadDataTable2("FeesSQL", "SELECT bail_seq_no " & _
                                            "FROM BailiffAllocationTable " & _
                                            "WHERE bail_de_allocation_found_date = '" & null_date & "'" & _
                                            " AND bail_ID = " & bailID & _
                                            " AND bail_debtorID = " & debtorID & _
                                            " AND bail_seq_no <> " & seqNo, bail3_dt, False)
            Dim rowFound As Boolean = False
            For Each bail3Row In bail3_dt.Rows
                Dim seqNo2 As Long = bail3Row(0)
                Dim updSeqNo As Long
                If seqNo > seqNo2 Then
                    updSeqNo = seqNo
                Else
                    updSeqNo = seqNo2
                End If
                rowFound = True
                upd_txt = "delete from BailiffAllocationTable " & _
                   " WHERE bail_seq_no = " & updSeqNo
                update_sql(upd_txt)
            Next
            If rowFound = False Then
                'see if later allocation entry exists in BAT
                Dim bail4_dt As New DataTable
                LoadDataTable2("FeesSQL", "SELECT bail_seq_no, bail_allocation_date " & _
                                                "FROM BailiffAllocationTable " & _
                                                "WHERE bail_debtorID = " & debtorID & _
                                                " AND bail_allocation_date > '" & Format(bail2Row(3), "yyyy-MM-dd") & "'" & _
                                                " order by bail_allocation_date", bail4_dt, False)
                Dim bail4RowFound As Boolean = False
                For Each bail4Row In bail4_dt.Rows
                    bail4RowFound = True
                    upd_txt = "update BailiffAllocationTable set bail_de_allocation_found_date = '" & Format(bail4Row(1), "yyyy-MM-dd") & "'" & _
                    " where bail_seq_no = " & seqNo
                    update_sql(upd_txt)
                    Exit For
                Next
                If bail4RowFound = False Then
                    'use today's date if not still allocated!
                    Dim debtorarray As Object
                    debtorarray = GetSQLResultsArray2("DebtRecoveryLocal", "SELECT bailiffID, bail_current " & _
                                                   "FROM Debtor " & _
                                                   "WHERE _rowid= " & debtorID)
                    If debtorarray(1) = "N" Or debtorarray(0) <> bailID Then
                        upd_txt = "update BailiffAllocationTable set bail_de_allocation_found_date = '" & Format(Now, "yyyy-MM-dd") & "'" & _
                 " where bail_seq_no = " & seqNo
                        update_sql(upd_txt)
                    End If

                End If
            End If
        Next


        'get all allocated cases

        Dim endDate As Date = CDate(Format(Now, "MMM dd, yyyy" & " 00:00:00"))
        Dim debt_dt As New DataTable
        LoadDataTable2("DebtRecoveryLocal", "SELECT D._rowid, D.clientSchemeID, D.bailiffID, D.last_stageID, D.bail_allocated " & _
                                                "FROM debtor D, bailiff B " & _
                                                "WHERE D.status_open_closed = 'O' " & _
                                                " and B._rowID = D.bailiffID " & _
                                                " and B.add_mobile_name <> 'none' " & _
                                                " AND D.bail_allocated >= '" & Format(start_date, "yyyy-MM-dd") & "'" & _
                                                " AND D.bail_allocated < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                                                " AND D._rowid > 99 AND D.bail_current = 'Y'" & _
                                                " order by D._rowID", debt_dt, False)

        For Each Debtrow In debt_dt.Rows
            Dim bailID As Integer = Debtrow.Item(2)
            Dim csID As Integer = Debtrow.item(1)
            clientArray = GetSQLResultsArray2("DebtRecoveryLocal", "SELECT clientID " & _
                                                    "FROM clientScheme " & _
                                                    "WHERE _rowid= " & csID)
            Dim clientID As Integer = clientArray(0)
            If clientID = 1 Or clientID = 2 Or clientID = 24 Then
                Continue For
            End If
            Dim debtorID As Integer = Debtrow.item(0)
            Dim bail_allocated As Date = Debtrow.item(4)
            'check if already on table
            Dim fees_dt As New DataTable
            LoadDataTable2("FeesSQL", "SELECT bail_allocation_date, bail_de_alloc_for_remit, bail_seq_no, bail_de_allocation_found_date " & _
                                                   "FROM BailiffAllocationTable " & _
                                                   "WHERE bail_debtorID = " & debtorID & _
                                                   " AND bail_ID = " & bailID, fees_dt, False)
            Dim row_found As Boolean = False
            For Each feeRow In fees_dt.Rows
                Dim allocationDate As Date = feeRow(0)
                If Format(allocationDate, "yyyy-MM-dd") = Format(bail_allocated, "yyyy-MM-dd") Then
                    row_found = True
                    Exit For
                End If
                Dim deAllocationdate As Date
                Try
                    deAllocationdate = feeRow(3)
                    If Format(deAllocationdate, "yyyy-MM-dd") = Format(null_date, "yyyy-MM-dd") Then
                        row_found = True
                        Exit For
                    End If
                Catch ex As Exception
                    row_found = True
                    Exit For
                End Try
            Next
            If Not row_found Then
                Dim stageID As Integer = Debtrow.item(3)
                Dim stagename As String
                stagename = GetSQLResults2("DebtRecoveryLocal", "SELECT name " & _
                                                       "FROM Stage " & _
                                                       "WHERE _rowid= " & stageID)
                upd_txt = "insert into BailiffAllocationTable (bail_ID, bail_debtorID, " & _
                    "bail_allocation_date, bail_de_alloc_for_remit, bail_stage_name, bail_de_allocation_found_date)" & _
                    "values (" & bailID & "," & debtorID & ",'" & Format(bail_allocated, "yyyy-MM-dd") & "'," & 0 & ",'" &
                    stagename & "','" & Format(null_date, "yyyy-MM-dd") & "')"
                If bail_allocated > Now Then
                    'MsgBox("future bailiff allocated for case " & debtorID)
                    errorFile &= "future bailiff allocated for case " & debtorID & vbNewLine
                End If
                update_sql(upd_txt)
            End If
        Next

        'MsgBox("done - remove later")
        If errorFile <> "" Then
            WriteFile("\\ross-helm-fp001\Rossendales Shared\FutureAllocationdates\StoreBailiffError3" & Format(Now, "yyyy-MM-dd") & ".txt", errorFile)
        End If
        Me.Close()
    End Sub
End Class

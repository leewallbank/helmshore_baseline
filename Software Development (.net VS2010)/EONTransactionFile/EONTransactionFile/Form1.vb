﻿Imports commonlibrary
Public Class Form1

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim transactionFile As String = ""
        Dim env_str As String = ""
        Dim prod_run As Boolean = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If
        Dim clientID As Integer = 1938
        If prod_run = False Then
            MsgBox("TEST RUN ONLY")
            clientID = 1662
        End If
        Dim startDate As Date = DateAdd(DateInterval.Day, -1, Now)
        Dim endDate As Date = Now
      
        'header
        Dim filename As String = "ADJ-EON-ROS-" & Format(Now, "yyyyMMdd") & ".csv"
        Dim tot_val As Decimal
        Dim tot_vol As Integer

        Dim header As String = "ADJ,EON," & Format(Now, "dd/MM/yyyy") & "," & filename & ","

        'get all debt changes
        Dim debt_dt As New DataTable
        LoadDataTable("DebtRecovery", "select D._rowID, D.client_ref, N.text from debtor D, clientscheme CS, note N" & _
                      " where N.debtorID = D._rowID" & _
                      " and D.clientschemeID=CS._rowID" & _
                      " and N.type = 'Debt Changed'" & _
                      " and N._createdDate >='" & Format(startDate, "yyyy-MM-dd") & "'" & _
                      " and N._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and CS.clientID = " & clientID, debt_dt, False)

        For Each DebtRow In debt_dt.Rows
            Dim debtorID As Integer = DebtRow(0)
            Dim clientRef As String = DebtRow(1)
            Dim noteText As String = DebtRow(2)
            Dim transactionValue As Decimal
            Dim startIDX As Integer = InStr(noteText, "by")
            Dim endIDX As Integer = InStr(noteText, "was")
            Try
                transactionValue = Mid(noteText, startIDX + 2, endIDX - startIDX - 2)
            Catch ex As Exception
                Continue For
            End Try
            endIDX = InStr(noteText, "now")
            Dim testText As String = Microsoft.VisualBasic.Right(noteText, noteText.Length - endIDX - 3)
            endIDX = InStr(testText, ".")
            Dim balance As Decimal
            Try
                balance = Microsoft.VisualBasic.Left(testText, endIDX + 2)
            Catch ex As Exception

            End Try
            Dim adjustmentType As String = ""
            If InStr(noteText, "Increased") > 0 Then
                adjustmentType = "PBA"
            ElseIf InStr(noteText, "Reduced") > 0 Then
                adjustmentType = "NBA"
            Else
                MsgBox("XX")
            End If
            transactionFile &= clientRef & "," & debtorID & "," & balance & "," & Format(startDate, "dd/MM/yyyy") & "," & transactionValue & "," & _
                Format(startDate, "dd/MM/yyyy") & "," & adjustmentType & ",,,GBP,ROS," & vbNewLine
            tot_val += transactionValue
            tot_vol += 1


        Next

        'now get all payments for yesterday
        Dim pay_dt As New DataTable
        LoadDataTable("DebtRecovery", "select D._rowID, D.client_ref, P.amount, P.status, PT.desc_short from debtor D, clientscheme CS, payment P, payType PT" & _
                      " where P.debtorID = D._rowID" & _
                      " and D.clientschemeID=CS._rowID" & _
                      " and P.amount_typeID = PT._rowID " & _
                      " and P.amount <> 0" & _
                      " and P._createdDate >='" & Format(startDate, "yyyy-MM-dd") & "'" & _
                      " and P._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                     " and CS.clientID = " & clientID, pay_dt, False)
        For Each PayRow In pay_dt.Rows
            Dim debtorID As Integer = PayRow(0)
            Dim clientRef As String = PayRow(1)
            Dim transactionValue As Decimal = PayRow(2)
            Dim paymentStatus As String = PayRow(3)
            Dim paymentType As String = PayRow(4)
            Dim adjustmentType As String = ""
            Select Case paymentStatus
                Case "W", "R"
                    adjustmentType = "PAY"
                Case "B", "C"
                    adjustmentType = "PRV"
            End Select
            Dim balance As Decimal
            balance = GetSQLResults("DebtRecovery", "select sum(F.fee_amount-F.remited_fee) from fee F" & _
                                  " where F.debtorID=" & debtorID & _
                                  " and F.fee_remit_col=1")
            Dim paymentMethodType As String
            Select Case paymentType
                Case "S/Ord"
                    paymentMethodType = "STO"
                Case "DD"
                    paymentMethodType = "DD"
                Case "Chq"
                    paymentMethodType = "CHQ"
                Case "CC", "C/C"
                    paymentMethodType = "CC"
                Case "DD", "D/C"
                    paymentMethodType = "DC"
                Case "Cash"
                    paymentMethodType = "CSH"
                Case Else
                    paymentMethodType = "OTH"
            End Select
            If transactionValue < 0 Then
                adjustmentType = "PRV"
                transactionValue = transactionValue * -1
            End If
            transactionFile &= clientRef & "," & debtorID & "," & balance & "," & Format(startDate, "dd/MM/yyyy") & "," & transactionValue & "," & _
               Format(startDate, "dd/MM/yyyy") & "," & adjustmentType & "," & paymentMethodType & "," & ",GBP,ROS," & vbNewLine
            tot_val += transactionValue
            tot_vol += 1
        Next
        'add header to file
        header &= tot_val & "," & tot_vol & vbNewLine
        transactionFile = header & transactionFile & "ROS" & vbNewLine
        filename = "C:/AATemp/" & filename
        My.Computer.FileSystem.WriteAllText(filename, transactionFile, False)

        MsgBox("Completed - remove later")
        Me.Close()
    End Sub
End Class

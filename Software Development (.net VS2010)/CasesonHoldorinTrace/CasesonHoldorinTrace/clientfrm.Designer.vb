﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class clientfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cl_dgv = New System.Windows.Forms.DataGridView()
        Me.clName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.clRef = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label2 = New System.Windows.Forms.Label()
        CType(Me.cl_dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cl_dgv
        '
        Me.cl_dgv.AllowUserToAddRows = False
        Me.cl_dgv.AllowUserToDeleteRows = False
        Me.cl_dgv.AllowUserToOrderColumns = True
        Me.cl_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.cl_dgv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.clName, Me.clID, Me.clRef})
        Me.cl_dgv.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cl_dgv.Location = New System.Drawing.Point(0, 0)
        Me.cl_dgv.Name = "cl_dgv"
        Me.cl_dgv.ReadOnly = True
        Me.cl_dgv.Size = New System.Drawing.Size(674, 262)
        Me.cl_dgv.TabIndex = 6
        '
        'clName
        '
        Me.clName.HeaderText = "Client Name"
        Me.clName.Name = "clName"
        Me.clName.ReadOnly = True
        Me.clName.Width = 250
        '
        'clID
        '
        Me.clID.HeaderText = "Client ID"
        Me.clID.Name = "clID"
        Me.clID.ReadOnly = True
        '
        'clRef
        '
        Me.clRef.HeaderText = "Ref"
        Me.clRef.Name = "clRef"
        Me.clRef.ReadOnly = True
        Me.clRef.Width = 200
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(138, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(138, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Double-Click to select client"
        '
        'clientfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(674, 262)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cl_dgv)
        Me.Name = "clientfrm"
        Me.Text = "clientfrm"
        CType(Me.cl_dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cl_dgv As System.Windows.Forms.DataGridView
    Friend WithEvents clName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents clRef As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class

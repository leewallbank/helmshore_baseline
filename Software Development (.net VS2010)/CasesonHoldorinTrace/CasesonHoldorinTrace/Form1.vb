Imports CommonLibrary

Public Class Form1
   
    Dim record, record2, file2, file3, record3 As String
    Dim hold_table(30, 5)
    Dim trace_table(30, 2)
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

       
    End Sub
    Private Sub remove_chrs(ByRef in_field As String)
        Dim idx2 As Integer
        Dim out_field As String = ""
        For idx2 = 1 To Microsoft.VisualBasic.Len(in_field)
            If Mid(in_field, idx2, 1) <> Chr(9) And _
              Mid(in_field, idx2, 1) <> Chr(10) And _
            Mid(in_field, idx2, 1) <> Chr(13) Then
                out_field = out_field & Mid(in_field, idx2, 1)
            End If
        Next
        in_field = out_field
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim startDate As Date = CDate("Apr 1," & Format(Now, "yyyy"))
        fromDTP.Value = startDate

    End Sub

    Private Sub run_report()
        exitbtn.Enabled = False
        selClientbtn.Enabled = False
        ProgressBar1.Value = 5
        Application.DoEvents()
        If MsgBox("Run report for " & selectedClientName & "?", MsgBoxStyle.YesNo, "Run report") = MsgBoxResult.No Then
            Me.Close()
            Exit Sub
        End If
        file2 = "DebtorID,Client,Scheme,Hold Started,Hold Ended,Client Request,Agent,Hold reason,Date case loaded" & vbNewLine
        file3 = "DebtorID,Client,Scheme,Trace Started,Trace Ended,Date case loaded" & vbNewLine
        'get list of client schemes for selected client
        Dim CS_dt As New DataTable
        LoadDataTable("DebtRecovery", "select CS._rowID, S.name from clientScheme CS, scheme S " & _
                      " where branchID = 1 " & _
                      " and S._rowID = CS.schemeID" & _
                      " and clientID =" & selectedClientID, CS_dt, False)

        For Each CSRow In CS_dt.Rows
            Dim sch_name As String = CSRow(1)
            'get all cases for CSID
            Dim CSID As Integer = CSRow(0)
            Dim debt_dt As New DataTable
            LoadDataTable("DebtRecovery", "select _rowid, status, debt_balance, arrange_amount, arrange_interval, _createdDate from Debtor" & _
            " where clientSchemeID = " & CSID & _
            " and _createdDate >='" & Format(fromDTP.Value, "yyyy-MM-dd") & "'" & _
            " and _createdDate <='" & Format(toDTP.Value, "yyyy-MM-dd") & "'", debt_dt, False)

            Dim hold_idx, trace_idx As Integer
            Dim idx As Integer
            For Each debtRow In debt_dt.Rows
                idx += 1
                Try
                    ProgressBar1.Value = (idx / debt_dt.Rows.Count) * 100
                Catch ex As Exception
                    idx = 0
                End Try
                Application.DoEvents()
                'get all notes
                Dim debtorID As Integer = debtRow(0)
                Dim created_date As Date = debtRow(5)
                hold_idx = 0
                trace_idx = 0
                Dim idx4 As Integer
                For idx4 = 0 To 30
                    trace_table(idx4, 1) = Nothing
                    trace_table(idx4, 2) = Nothing
                Next
                Dim note_dt As New DataTable
                LoadDataTable("Debtrecovery", "select type, text, _createdDate, _createdBy from Note where debtorID = " & debtorID & _
                " order by _rowid", note_dt, False)
                record2 = ""
                record3 = ""
                For Each noteRow In note_dt.Rows
                    Dim note_type As String = noteRow(0)
                    If note_type = "Trace" Then
                        Try
                            If trace_table(trace_idx, 1) = Format(noteRow(2), "dd/MM/yyyy") Then
                                trace_table(trace_idx, 2) = Format(noteRow(2), "dd/MM/yyyy")
                            End If
                        Catch ex As Exception

                        End Try

                        trace_idx += 1
                        trace_table(trace_idx, 1) = Format(noteRow(2), "dd/MM/yyyy")
                    End If
                    If note_type = "Off trace" Then
                        trace_table(trace_idx, 2) = Format(noteRow(2), "dd/MM/yyyy")
                    End If
                    If note_type = "Hold" Then
                        If Microsoft.VisualBasic.Left(noteRow(1), 3) <> "for" Then
                            Continue For
                        End If
                        If InStr(noteRow(1), "days") = 0 Then
                            Continue For
                        End If
                        If hold_idx > 0 Then
                            If noteRow(2) < CDate(hold_table(hold_idx, 3)) Then
                                hold_table(hold_idx, 3) = noteRow(2)
                            End If
                        End If
                        hold_idx += 1
                        hold_table(hold_idx, 1) = Format(noteRow(2), "dd/MM/yyyy")
                        If InStr(noteRow(1), "client request") > 0 Then
                            hold_table(hold_idx, 2) = "Y"
                        Else
                            hold_table(hold_idx, 2) = "N"
                        End If
                        hold_table(hold_idx, 4) = noteRow(3)
                        hold_table(hold_idx, 5) = noteRow(1)
                        hold_table(hold_idx, 5) = Replace(hold_table(hold_idx, 5), ",", " ")
                        remove_chrs(hold_table(hold_idx, 5))
                        'get hold days
                        Dim end_idx As Integer = InStr(noteRow(1), "days")
                        Dim days_str As String = Mid(noteRow(1), 4, end_idx - 4)
                        Dim days As Integer
                        Try
                            days = days_str
                        Catch ex As Exception
                            days = Nothing
                        End Try
                        Dim hold_end_date As Date = Nothing
                        If days <> Nothing Then
                            hold_end_date = DateAdd(DateInterval.Day, days, hold_table(hold_idx, 1))
                        End If
                        hold_table(hold_idx, 3) = Format(hold_end_date, "dd/MM/yyyy")
                    End If
                    If note_type = "Off hold" Then
                        hold_table(hold_idx, 3) = Format(noteRow(2))
                    End If
                Next
                Dim status As String = debtRow(1)

                For idx4 = hold_idx To 1 Step -1
                    Dim hold_start As Date = hold_table(idx4, 1)
                    Dim hold_end As Date
                    Try
                        hold_end = hold_table(idx4, 3)
                    Catch ex As Exception
                        hold_end = Nothing
                    End Try
                    record2 = record2 & debtorID & "," & selectedClientName & "," & sch_name & "," & _
                                           Format(hold_start, "dd/MM/yyyy") & ","
                    If hold_end = Nothing Then
                        record2 = record2 & ","
                    Else
                        record2 = record2 & Format(hold_end, "dd/MM/yyyy") & ","
                    End If
                    record2 = record2 & hold_table(idx4, 2) & "," & hold_table(idx4, 4) & "," & _
                    hold_table(idx4, 5) & "," & Format(created_date, "dd/MM/yyyy") & vbNewLine
                Next
                For idx4 = trace_idx To 1 Step -1
                    Dim trace_start_date As Date = trace_table(idx4, 1)
                    Dim trace_end_date As Date = trace_table(idx4, 2)
                    record3 = record3 & debtorID & "," & selectedClientName & "," & sch_name & "," & _
                                           Format(trace_start_date, "dd/MM/yyyy") & ","
                    If Format(trace_end_date, "yyyy") = 1 Then
                        record3 = record3 & ","
                    Else
                        record3 = record3 & Format(trace_end_date, "dd/MM/yyyy") & ","
                    End If
                    record3 = record3 & Format(created_date, "dd/MM/yyyy") & vbNewLine
                Next
                If record2.Length > 0 Then
                    file2 = file2 & record2
                End If
                If record3.Length > 0 Then
                    file3 = file3 & record3
                End If
            Next
        Next

        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "TXT files |*.txt"
            .DefaultExt = ".txt"
            .OverwritePrompt = True
            .FileName = selectedClientName & "_holds.csv"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Dim start_idx As Integer = InStr(SaveFileDialog1.FileName, "_")
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file2, False)
            SaveFileDialog1.FileName = Microsoft.VisualBasic.Left(SaveFileDialog1.FileName, start_idx) & "trace.csv"
            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, file3, False)
            MsgBox("reports written")
        Else
            MsgBox("No reports written")
        End If

    End Sub

    Private Sub selClientbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles selClientbtn.Click
        clientfrm.ShowDialog()
        run_report()
        Me.Close()
    End Sub
End Class

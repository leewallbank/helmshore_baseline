Public Class maintainfrm

    Private Sub maintainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Split_namesTableAdapter.Fill(Me.Split_namesDataSet.split_names)
        orig_no_rows = DataGridView1.RowCount
    End Sub

    Private Sub add_names()
        Dim idx As Integer
        Dim split_name As String

        For idx = orig_no_rows To DataGridView1.RowCount - 1
            split_name = DataGridView1.Rows(idx - 1).Cells(1).Value
            Try
                Me.Split_namesTableAdapter.InsertQuery(split_name)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        Next
    End Sub

    
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        add_names()
        Me.Close()
    End Sub

    Private Sub DataGridView1_CellValidated1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValidated
        DataGridView1.Rows(e.RowIndex).ErrorText = String.Empty
    End Sub

    Private Sub DataGridView1_RowEnter1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter
        saved_name_id = DataGridView1.Rows(e.RowIndex).Cells(0).Value
    End Sub

    Private Sub DataGridView1_RowsRemoved1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles DataGridView1.RowsRemoved
        Try
            Me.Split_namesTableAdapter.DeleteQuery(saved_name_id)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class

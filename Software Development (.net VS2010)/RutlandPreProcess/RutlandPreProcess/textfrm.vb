Public Class textfrm
    Dim orig_text_line
    Private Sub textfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        orig_text_line = text_line
        TextBox1.Text = text_line
        
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub resetbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles resetbtn.Click
        TextBox1.Text = orig_text_line
        TextBox1.Focus()
    End Sub


    Private Sub TextBox1_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles TextBox1.Validating
        
        Dim idx As Integer
        Dim pipe_count As Integer = 0
        text_line = TextBox1.Text
        For idx = 1 To Microsoft.VisualBasic.Len(text_line)
            If Mid(text_line, idx, 1) = "|" Then
                pipe_count += 1
            End If
        Next
        If pipe_count = 0 Then
            ErrorProvider1.SetError(TextBox1, "Enter a pipe between name and address")
            e.Cancel = True
        ElseIf pipe_count > 2 Then
            ErrorProvider1.SetError(TextBox1, "Enter ONLY ONE OR TWO pipes between name/TA name and address")
            e.Cancel = True
        Else
            ErrorProvider1.SetError(TextBox1, "")
        End If
        If rep_tot_cases > 0 Then
            If case_no > rep_tot_cases Then
                case_no = rep_tot_cases
            End If
            ProgressBar1.Value = case_no / rep_tot_cases * 100
        End If
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Private Sub splitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles splitbtn.Click
        maintainfrm.ShowDialog()
        text_line = check_names(text_line)
        TextBox1.Text = text_line
        TextBox1.Focus()
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged

    End Sub
End Class
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class maintainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.NameidDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SplitnameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SplitnamesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.exitbtn = New System.Windows.Forms.Button()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitnamesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Split_namesDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NameidDataGridViewTextBoxColumn, Me.SplitnameDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.SplitnamesBindingSource
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(424, 443)
        Me.DataGridView1.TabIndex = 0
        '
        'NameidDataGridViewTextBoxColumn
        '
        Me.NameidDataGridViewTextBoxColumn.DataPropertyName = "name_id"
        Me.NameidDataGridViewTextBoxColumn.HeaderText = "name_id"
        Me.NameidDataGridViewTextBoxColumn.Name = "NameidDataGridViewTextBoxColumn"
        Me.NameidDataGridViewTextBoxColumn.ReadOnly = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(308, 151)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 1
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'maintainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(424, 443)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "maintainfrm"
        Me.Text = "maintain names"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SplitnamesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Split_namesDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Split_namesDataSet As RutlandPreProcess.split_namesDataSet
    Friend WithEvents SplitnamesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Split_namesTableAdapter As split_namesDataSetTableAdapters.split_namesTableAdapter
    Friend WithEvents NameidDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SplitnameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents exitbtn As System.Windows.Forms.Button
End Class

﻿Imports CommonLibrary
Imports System.Configuration
Imports System.Xml.Schema
Imports System.IO

Public Class Form1
    Dim xml_valid As Boolean = True
    Dim auditFile As String = ""
    Dim error_no As Integer = 0
    Dim case_no As Integer = 0
    Dim prod_run As Boolean = False
    Dim fileDate As Date = Now
    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        run_report()
    End Sub
    Private Sub run_report()

        ConnectDb2("DebtRecovery")
        'ConnectDb2Fees("StudentLoans")
        'when adding student loans update prod config file as well.
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If
        Dim filename As String = "ADFRossendales" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".xml"
        Dim activity_file As String = "R:\Home Office CGI File\" & filename
        auditFile = "R:/Home Office CGI File/TDX_Activity_audit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        If Not prod_run Then
            activity_file = "H:\temp\" & filename
            auditFile = "H:/temp/TDXHO_Activity_audit" & Format(fileDate, "yyyyMMdd") & "T" & Format(fileDate, "HHmmss") & ".txt"
        End If
        write_audit("report started at " & Now, False)
        Dim writer As New Xml.XmlTextWriter(activity_file, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)
        writer.Formatting = Xml.Formatting.Indented
        writer.WriteStartElement("ActvtyData")
        writer.WriteAttributeString("schemaVersion", "1.0")
        writer.WriteAttributeString("xmlns", "ActivityData")
        writer.WriteStartElement("MsgHdr")
        writer.WriteAttributeString("xmlns", "")
        writer.WriteElementString("MsgTp", "ADF")
        writer.WriteElementString("FlNm", filename)
        writer.WriteElementString("FlDt", Format(fileDate, "yyyy-MM-dd") & "T" & Format(fileDate, "HH:mm:ss"))
        writer.WriteElementString("DCACd", "Rossendales")
        writer.WriteEndElement()  'MsgHdr
        writer.WriteStartElement("Accts")
        writer.WriteAttributeString("xmlns", "")
        Dim startDate As Date
        Dim endDate As Date
        If Weekday(Now) = 7 Then 'Saturday
            endDate = CDate(Format(Now, "MMM") & " " & Format(Now, "dd") & ", " & Year(Now) & " 16:00:00")
        Else
            endDate = DateAdd(DateInterval.Day, -Weekday(Now), Now)
        End If


        endDate = CDate(Format(endDate, "MMM d, yyyy" & " 16:00:00"))

        startDate = DateAdd(DateInterval.Day, -7, endDate)


        'END DATE CHANGED
        'endDate = DateAdd(DateInterval.Day, 1, Now)

        'get all CSIDs for ETC HMRC
        Dim cs_dt As New DataTable
        Dim clientCode As String = "HomeOffice"
        LoadDataTable2("DebtRecovery", "select _rowid " & _
                      " from clientScheme" & _
                      " where clientID =1885 ", cs_dt, False)
        For Each csRow In cs_dt.Rows
            Dim CSID As Integer = csRow(0)
            'now get cases
            Dim debt_dt As New DataTable
            LoadDataTable2("DebtRecovery", "Select D._rowid, client_ref, status, arrange_started, return_codeID, debt_balance, return_date, bail_current, bail_allocated, bailiffID" & _
                            " status_open_closed, return_date" & _
                           " from Debtor D, Note N" & _
                           " where clientschemeID = " & CSID & _
                           " and N.DebtorID = D._rowID" & _
                           " and (debtPaid > 0 " & _
                           " or N._createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "')" & _
                           " group by 1,2,3,4,5,6,7,8,9,10" & _
                           " order by D._rowid", debt_dt, False)
            For Each debtRow In debt_dt.Rows
                Dim lastNoteRowid As Integer = 0
                Dim lastNoteType As String = ""
                Dim xmlWritten As Boolean = False
                Dim debtorID As Integer = debtRow(0)
                Dim clientRef As String = debtRow(1)
                Dim phone_dt As New DataTable
                Dim phoneActionOpen As Boolean = False

                Dim caseStatus As String = debtRow(2)
                Dim arrangeStarted As Date
                Try
                    arrangeStarted = debtRow(3)
                Catch ex As Exception
                    arrangeStarted = Nothing
                End Try
                Dim totalBalance As Decimal = debtRow(5)

                Dim cancelledNote As String = ""
                'get all notes for last week
                Dim note_dt As New DataTable
                LoadDataTable2("DebtRecovery", " select type, text, _createdDate, _createdBy, _rowid" & _
                              " from Note" & _
                              " where debtorID = " & debtorID & _
                              " and _createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                              " and _createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                              " order by _rowID", note_dt, False)

                For Each noteRow In note_dt.Rows
                    Dim noteType As String = noteRow(0)
                    Dim noteText As String = noteRow(1)
                    Dim noteDate As Date = noteRow(2)
                    Dim noteAgent As String = noteRow(3)
                    Dim noteRowid As Integer = noteRow(4)
                    Select Case noteType
                        Case "Allocated"
                            'T54418
                            'See If APY alleged payment made
                            If InStr(noteText, ":3784") Then
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "UNA")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "APY")
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            End If
                            'agent 2197 -query
                            If InStr(noteText, ":2197") Then
                                If Not xmlWritten Then
                                    writer.WriteStartElement("Acct")
                                    writer.WriteElementString("AcctID", clientRef)
                                    writer.WriteElementString("ClntCd", clientCode)
                                    writer.WriteStartElement("Actns")
                                    xmlWritten = True
                                End If
                                writer.WriteStartElement("Actn")
                                writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Tp", "UNA")
                                writer.WriteStartElement("Actvts")
                                writer.WriteStartElement("Actvty")
                                writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                writer.WriteElementString("Cd", "DIS")
                                writer.WriteEndElement()  'acvty
                                writer.WriteEndElement()  'actvts
                                writer.WriteEndElement()  'actn
                            End If
                            If InStr(noteText, "Field Collector") = 0 Then
                                Continue For
                            End If
                            If Not xmlWritten Then
                                writer.WriteStartElement("Acct")
                                writer.WriteElementString("AcctID", clientRef)
                                writer.WriteElementString("ClntCd", clientCode)
                                writer.WriteStartElement("Actns")
                                xmlWritten = True
                            End If
                            writer.WriteStartElement("Actn")
                            writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Tp", "STC")
                            writer.WriteEndElement()  'actn
                        Case "Arrangement"
                            If noteRowid <= lastNoteRowid + 6 Then
                                Continue For
                            End If
                            If Not xmlWritten Then
                                writer.WriteStartElement("Acct")
                                writer.WriteElementString("AcctID", clientRef)
                                writer.WriteElementString("ClntCd", clientCode)
                                writer.WriteStartElement("Actns")
                                xmlWritten = True
                            End If
                            Dim testText As String
                            If InStr(noteText, " payment ") = 0 Then
                                Continue For
                            End If
                            Dim startIDX As Integer
                            writer.WriteStartElement("Actn")
                            writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            'T54418 add PPO and PPS
                            writer.WriteElementString("Tp", "UNA")
                            'writer.WriteElementString("Tp", "PPO")
                            writer.WriteStartElement("Actvts")
                            writer.WriteStartElement("Actvty")
                            writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Cd", "PPS")
                            writer.WriteEndElement()  'acvty

                            writer.WriteStartElement("Actvty")
                            writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            'T54418 use PTP for one-off else PAR
                            startIDX = InStr(noteText, "every")
                            If startIDX = 0 Then
                                writer.WriteElementString("Cd", "PTP")
                            Else
                                writer.WriteElementString("Cd", "PAR")
                            End If

                            writer.WriteStartElement("PymtPln")
                            writer.WriteElementString("CrtnD", Format(noteDate, "yyyy-MM-dd"))
                            Dim firstPaymentDate As Date = Nothing
                            Dim firstPaymentAmount As Decimal = 0
                            Dim regPaymentAmount As Decimal = 0
                            Dim paymentFrequency As String = "P"
                            startIDX = InStr(noteText, "Initial payment of ")
                            If startIDX > 0 Then
                                testText = Microsoft.VisualBasic.Right(noteText, noteText.Length - startIDX - 19 + 1)
                                startIDX = InStr(testText, ".")
                                If startIDX > 0 Then
                                    Try
                                        firstPaymentAmount = Microsoft.VisualBasic.Left(testText, startIDX + 2)
                                    Catch ex As Exception
                                        write_audit(debtorID & " first payment amount??", False)
                                    End Try
                                    startIDX = InStr(testText, "due on")
                                    If startIDX > 0 Then
                                        testText = Mid(testText, startIDX + 6, 13)
                                        Try
                                            firstPaymentDate = CDate(Trim(testText))
                                        Catch ex As Exception
                                            write_audit(debtorID & " first payment due on??", False)
                                        End Try
                                    End If
                                End If
                            Else
                                startIDX = InStr(noteText, "Whole payment of ")
                                If startIDX > 0 Then
                                    testText = Microsoft.VisualBasic.Right(noteText, noteText.Length - startIDX - 17 + 1)
                                    startIDX = InStr(testText, ".")
                                    If startIDX > 0 Then
                                        Try
                                            firstPaymentAmount = Microsoft.VisualBasic.Left(testText, startIDX + 2)
                                        Catch ex As Exception
                                            write_audit(debtorID & " whole payment amount??", False)

                                        End Try
                                        startIDX = InStr(testText, "due on")
                                        If startIDX > 0 Then
                                            testText = Mid(testText, startIDX + 6, 13)
                                            Try
                                                firstPaymentDate = CDate(Trim(testText))
                                            Catch ex As Exception
                                                write_audit(debtorID & " first payment due on??", False)
                                            End Try
                                        End If
                                    End If
                                End If
                            End If
                            startIDX = InStr(noteText, "every")
                            Dim arrangementDays As Integer
                            If startIDX > 0 Then
                                Dim followIDX As Integer = InStr(noteText, "followed by")
                                If followIDX > 0 Then
                                    noteText = Microsoft.VisualBasic.Right(noteText, noteText.Length - followIDX - 11)
                                    startIDX = InStr(noteText, "every")
                                End If
                                Dim endIDX As Integer
                                endIDX = InStr(noteText, "days")
                                Try
                                    regPaymentAmount = Microsoft.VisualBasic.Left(noteText, startIDX - 1)
                                Catch ex As Exception
                                    write_audit(debtorID & " regular payment amount is??", False)
                                End Try
                                If endIDX > 0 Then
                                    Try
                                        arrangementDays = Mid(noteText, startIDX + 5, endIDX - startIDX - 5)
                                    Catch ex As Exception
                                        write_audit(debtorID & " arrangement days??", False)
                                    End Try
                                    If arrangementDays >= 29 Then
                                        paymentFrequency = "M"
                                    ElseIf arrangementDays >= 13 Then
                                        paymentFrequency = "F"
                                    Else
                                        paymentFrequency = "W"
                                    End If
                                End If
                                startIDX = InStr(noteText, "First payment due on")
                                If startIDX > 0 And firstPaymentDate = Nothing Then
                                    testText = Mid(noteText, startIDX + 20, 13)
                                    Try
                                        firstPaymentDate = CDate(Trim(testText))
                                    Catch ex As Exception
                                        write_audit(debtorID & "First payment due on is???", False)
                                    End Try
                                End If
                            End If
                            Dim numPymnts As Integer = 0
                            writer.WriteElementString("FstPymtD", Format(firstPaymentDate, "yyyy-MM-dd"))


                            If regPaymentAmount > 0 Then
                                Dim testPymnts As Integer = 0
                                testPymnts = Int((totalBalance - firstPaymentAmount) / regPaymentAmount) + 1
                                If testPymnts > 0 Then
                                    numPymnts += testPymnts
                                End If
                            End If
                            Dim lastPymtDate As Date = Nothing
                            Try
                                lastPymtDate = DateAdd(DateInterval.Day, numPymnts * arrangementDays, firstPaymentDate)
                            Catch ex As Exception

                            End Try


                            writer.WriteElementString("LstPymtD", Format(lastPymtDate, "yyyy-MM-dd"))

                            writer.WriteElementString("RegPymtAmt", Format(regPaymentAmount, "0.00"))
                            If firstPaymentAmount > 0 Then
                                numPymnts += 1
                                writer.WriteElementString("FstPymtAmt", Format(firstPaymentAmount, "0.00"))
                            End If
                            If firstPaymentAmount = 0 And regPaymentAmount = 0 Then
                                write_audit(debtorID & "First and reg payments are zero???", False)
                            End If
                            If numPymnts > 0 Then
                                writer.WriteElementString("NumPymts", Format(numPymnts, "0"))
                            End If

                            writer.WriteElementString("PymtFrqCd", paymentFrequency)

                            writer.WriteEndElement()  'PymtPln
                            writer.WriteEndElement()  'acvty
                            writer.WriteEndElement()  'actvts
                            writer.WriteEndElement()  'actn
                        Case "Broken"
                            If Not xmlWritten Then
                                writer.WriteStartElement("Acct")
                                writer.WriteElementString("AcctID", clientRef)
                                writer.WriteElementString("ClntCd", clientCode)
                                writer.WriteStartElement("Actns")
                                xmlWritten = True
                            End If
                            writer.WriteStartElement("Actn")
                            writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Tp", "UNA")
                            writer.WriteStartElement("Actvts")
                            writer.WriteStartElement("Actvty")
                            writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            'T54418 if payment received after arrangement started then ARB else PRB
                            Dim pay2_dt As New DataTable
                            LoadDataTable2("DebtRecovery", "Select count(_rowid)" & _
                                          " from Payment" & _
                                          " where debtorID = " & debtorID & _
                                          " and (status = 'W' or status = 'R')" & _
                                          " and date >= '" & Format(arrangeStarted, "yyyy-MM-dd") & "'", pay2_dt, False)
                            If pay2_dt.Rows(0).Item(0) = 0 Then
                                writer.WriteElementString("Cd", "PRB")
                            Else
                                writer.WriteElementString("Cd", "ARB")
                            End If

                            writer.WriteEndElement()  'acvty
                            writer.WriteEndElement()  'actvts
                            writer.WriteEndElement()  'actn
                        Case "Cancelled"
                            cancelledNote = noteText
                        Case "Clear arrange"
                            'T54418
                            If Not xmlWritten Then
                                writer.WriteStartElement("Acct")
                                writer.WriteElementString("AcctID", clientRef)
                                writer.WriteElementString("ClntCd", clientCode)
                                writer.WriteStartElement("Actns")
                                xmlWritten = True
                            End If
                            writer.WriteStartElement("Actn")
                            writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Tp", "UNA")
                            writer.WriteStartElement("Actvts")
                            writer.WriteStartElement("Actvty")
                            writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Cd", "PPX")

                            writer.WriteEndElement()  'acvty
                            writer.WriteEndElement()  'actvts
                            writer.WriteEndElement()  'actn
                        Case "Complaint"
                            If Not xmlWritten Then
                                writer.WriteStartElement("Acct")
                                writer.WriteElementString("AcctID", clientRef)
                                writer.WriteElementString("ClntCd", clientCode)
                                writer.WriteStartElement("Actns")
                                xmlWritten = True
                            End If
                            writer.WriteStartElement("Actn")
                            writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Tp", "UNA")
                            writer.WriteStartElement("Actvts")
                            writer.WriteStartElement("Actvty")
                            writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Cd", "CPT")
                            writer.WriteEndElement()  'acvty
                            writer.WriteEndElement()  'actvts
                            writer.WriteEndElement()  'actn
                        Case "Note"
                            Dim noteStart As String = UCase(Microsoft.VisualBasic.Left(noteText, 3))
                            If Mid(noteText, 4, 1) <> " " Then
                                If noteText = "VoiceSage Contact Message Sent" Then
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "SMS")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "MEL")
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Else
                                    Continue For
                                End If
                            End If
                            Select Case noteStart
                                Case "ACL"
                                    'account closed picked up by remit
                                Case "AMM"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "PCM")
                                    'writer.WriteStartElement("Actvts")
                                    'writer.WriteStartElement("Actvty")
                                    'writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("OutTp", "NOA")
                                    writer.WriteElementString("Notes", noteText)
                                    'writer.WriteEndElement()  'acvty
                                    'writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "ANM"
                                    'MsgBox("Check ANM " & debtorID)
                                    'If Not xmlWritten Then
                                    '    writer.WriteStartElement("Acct")
                                    '    writer.WriteElementString("AcctID", clientref)
                                    '    writer.WriteElementString("ClntCd","HMRC ETC")
                                    '    writer.WriteStartElement("Actns")
                                    '    xmlWritten = True
                                    'End If
                                    'writer.WriteStartElement("Actn")
                                    'writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    'writer.WriteElementString("Tp", "PCM")
                                    'writer.WriteStartElement("Actvts")
                                    'writer.WriteStartElement("Actvty")
                                    'writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    'writer.WriteElementString("Cd", "PCM")
                                    'writer.WriteEndElement()  'acvty
                                    'writer.WriteEndElement()  'actvts
                                    'writer.WriteEndElement()  'actn
                                Case "ARB"
                                    'MsgBox(debtorID & " use broken note or add code for " & noteStart)
                                Case "BDN"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "PCM")
                                    'writer.WriteStartElement("Actvts")
                                    'writer.WriteStartElement("Actvty")
                                    'writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("OutTp", "BDN")
                                    writer.WriteElementString("Notes", noteText)
                                    'writer.WriteEndElement()  'acvty
                                    'writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "DIS"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "UNA")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "DIS")
                                    'writer.WriteElementString("Notes", noteText)
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "EMS"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "EMS")
                                    writer.WriteEndElement()  'actn
                                Case "IAE"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "UNA")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "IAE")
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "INB"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "INC")
                                    writer.WriteEndElement()  'actn
                                Case "INC"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "INC")
                                    writer.WriteEndElement()  'actn
                                Case "INE"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "INE")
                                    writer.WriteEndElement()  'actn
                                Case "INL"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "INL")
                                    writer.WriteEndElement()  'actn
                                Case "MEL"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "PCM")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "MEL")
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "NOA"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "PCM")
                                    'writer.WriteStartElement("Actvts")
                                    'writer.WriteStartElement("Actvty")
                                    'writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("OutTp", "NOA")
                                    writer.WriteElementString("Notes", noteText)
                                    ' writer.WriteEndElement()  'acvty
                                    'writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "ONH"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "UNA")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "ONH")
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "ONP"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "UNA")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "ONP")
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "PAR"
                                    'ignore
                                Case "PAY"
                                    'ignore
                                Case "PCM"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "PCM")
                                    'writer.WriteStartElement("Actvts")
                                    'writer.WriteStartElement("Actvty")
                                    'writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("OutTp", "PCA")
                                    writer.WriteElementString("Notes", noteText)
                                    'writer.WriteEndElement()  'acvty
                                    'writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "PTP"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "UNA")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "PTP")
                                    'writer.WriteElementString("Notes", noteText)
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "QRR"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "UNA")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "QRR")
                                    'writer.WriteElementString("Notes", noteText)
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "RPC"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "PCM")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "RPC")
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "RTP"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "UNA")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "RTP")
                                    ' writer.WriteElementString("Notes", noteText)
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "SET"
                                    'If Microsoft.VisualBasic.Left(noteText, 3) = "SET" Then
                                    '    If InStr(noteText, "rrang") = 0 Then
                                    '        MsgBox(debtorID & " add code for " & noteStart)
                                    '    End If
                                    'End If
                                Case "SIF"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "UNA")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "SET")
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "SMS"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "SMS")
                                    writer.WriteEndElement()  'actn
                                Case "SRE"
                                    'MsgBox(debtorID & " add code for " & noteStart)
                                Case "TPP"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "UNA")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "TPP")
                                    ' writer.WriteElementString("Notes", noteText)
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "WNO"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "PCM")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "WPH")
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                Case "WPC"
                                    If Not xmlWritten Then
                                        writer.WriteStartElement("Acct")
                                        writer.WriteElementString("AcctID", clientRef)
                                        writer.WriteElementString("ClntCd", clientCode)
                                        writer.WriteStartElement("Actns")
                                        xmlWritten = True
                                    End If
                                    writer.WriteStartElement("Actn")
                                    writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Tp", "PCM")
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", "WPC")
                                    writer.WriteEndElement()  'acvty
                                    writer.WriteEndElement()  'actvts
                                    writer.WriteEndElement()  'actn
                                    'Case Else
                                    '    If noteRowid <> lastNoteRowid + 1 Then
                                    '        If Mid(noteText, 4, 1) = " " Then
                                    '            Dim testText As String = Trim(noteStart)
                                    '            testText = Replace(testText, " ", "")
                                    '            If testText.Length = 3 Then
                                    '                MsgBox(debtorID & " Note " & noteText)
                                    '            End If
                                    '        End If
                                    '    End If
                            End Select
                        Case "Letter"
                            If noteRowid <= lastNoteRowid + 6 And
                                lastNoteType = "Letter" Then
                                Continue For
                            End If
                            If Not xmlWritten Then
                                writer.WriteStartElement("Acct")
                                writer.WriteElementString("AcctID", clientRef)
                                writer.WriteElementString("ClntCd", clientCode)
                                writer.WriteStartElement("Actns")
                                xmlWritten = True
                            End If
                            writer.WriteStartElement("Actn")
                            writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Tp", "LSE")
                            writer.WriteEndElement()  'actn
                        Case "Off trace"
                            If Not xmlWritten Then
                                writer.WriteStartElement("Acct")
                                writer.WriteElementString("AcctID", clientRef)
                                writer.WriteElementString("ClntCd", clientCode)
                                writer.WriteStartElement("Actns")
                                xmlWritten = True
                            End If
                            writer.WriteStartElement("Actn")
                            writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Tp", "DTP")
                            writer.WriteStartElement("Actvts")
                            writer.WriteStartElement("Actvty")
                            writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Cd", "DTC")
                            writer.WriteEndElement()  'acvty
                            writer.WriteEndElement()  'actvts
                            writer.WriteEndElement()  'actn
                        Case "Phone"
                            'ignore phone as notes have phone call details - otherwise may be duplicated
                            'If Not xmlWritten Then
                            '    writer.WriteStartElement("Acct")
                            '    writer.WriteElementString("AcctID", clientref)
                            '    writer.WriteElementString("ClntCd","HMRC ETC")
                            '    writer.WriteStartElement("Actns")
                            '    xmlWritten = True
                            'End If
                            'writer.WriteStartElement("Actn")
                            'Dim textDate As Date
                            'Dim startText As Integer = InStr(noteText, "called on")
                            'Dim testText As String = noteText
                            'If startText = 0 Then
                            '    MsgBox("Phone date?")
                            'Else
                            '    testText = Microsoft.VisualBasic.Right(testText, testText.Length - startText - 9)
                            '    startText = InStr(testText, "-")
                            '    If startText = 0 Then
                            '        testText = Replace(testText, "at ", "")
                            '        Try
                            '            textDate = CDate(testText)
                            '        Catch ex As Exception
                            '            MsgBox("Phone date2?")
                            '        End Try
                            '    Else
                            '        testText = Microsoft.VisualBasic.Left(testText, startText - 1)
                            '        testText = Replace(testText, "at ", "")
                            '        Try
                            '            textDate = CDate(testText)
                            '        Catch ex As Exception
                            '            MsgBox("Phone date3?")
                            '        End Try
                            '    End If
                            'End If
                            'writer.WriteElementString("ActnTS", Format(textDate, "yyyy-MM-dd") & "T" & Format(textDate, "HH:mm:ss"))
                            'writer.WriteElementString("Tp", "PCM")
                            'writer.WriteStartElement("Actvts")
                            'writer.WriteStartElement("Actvty")
                            'writer.WriteElementString("Dt", Format(textDate, "yyyy-MM-dd") & "T" & Format(textDate, "HH:mm:ss"))
                            'If InStr(noteText, "Number Unobtainable") > 0 Then
                            '    writer.WriteElementString("Cd", "BDN")
                            'ElseIf InStr(noteText, "MsgLF") > 0 Then
                            '    writer.WriteElementString("Cd", "MEL")
                            'ElseIf InStr(noteText, "RPC") > 0 Then
                            '    writer.WriteElementString("Cd", "RPC")
                            'ElseIf InStr(noteText, "Call Terminated By Caller") > 0 Then
                            '    writer.WriteElementString("Cd", "PCA")
                            'ElseIf InStr(noteText, "No Answer") > 0 Then
                            '    writer.WriteElementString("Cd", "NOA")
                            'ElseIf InStr(noteText, "WNO") > 0 Then
                            '    writer.WriteElementString("Cd", "WPH")
                            'ElseIf InStr(noteText, "Wrong Number") > 0 Then
                            '    writer.WriteElementString("Cd", "WPH")
                            'ElseIf InStr(noteText, "PCM") > 0 Then
                            '    writer.WriteElementString("Cd", "PCA")
                            'ElseIf InStr(UCase(noteText), "ANSWER MACHINE") > 0 Then
                            '    writer.WriteElementString("Cd", "NOA")
                            'ElseIf InStr(UCase(noteText), "TRANSFER") > 0 Then
                            '    'ignore
                            'ElseIf InStr(noteText, "Left Message with third party") > 0 Then
                            '    writer.WriteElementString("Cd", "MEL")
                            'ElseIf InStr(noteText, "Absconded") > 0 Then
                            '    writer.WriteElementString("Cd", "WPC")
                            'Else
                            '    MsgBox("new phone type " & debtorID & " " & noteText)
                            'End If

                            ''agent name?
                            'writer.WriteEndElement()  'Actvty
                            'writer.WriteEndElement()  'Actvts
                            'writer.WriteEndElement()  'actn
                        Case "SMS to debtor"
                            If Not xmlWritten Then
                                writer.WriteStartElement("Acct")
                                writer.WriteElementString("AcctID", clientRef)
                                writer.WriteElementString("ClntCd", clientCode)
                                writer.WriteStartElement("Actns")
                                xmlWritten = True
                            End If
                            writer.WriteStartElement("Actn")
                            writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Tp", "SMS")
                            writer.WriteEndElement()  'actn
                        Case "Trace"
                            If Not xmlWritten Then
                                writer.WriteStartElement("Acct")
                                writer.WriteElementString("AcctID", clientRef)
                                writer.WriteElementString("ClntCd", clientCode)
                                writer.WriteStartElement("Actns")
                                xmlWritten = True
                            End If
                            writer.WriteStartElement("Actn")
                            writer.WriteElementString("ActnTS", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Tp", "DTP")
                            writer.WriteStartElement("Actvts")
                            writer.WriteStartElement("Actvty")
                            writer.WriteElementString("Dt", Format(noteDate, "yyyy-MM-dd") & "T" & Format(noteDate, "HH:mm:ss"))
                            writer.WriteElementString("Cd", "DTR")
                            writer.WriteEndElement()  'acvty
                            writer.WriteEndElement()  'actvts
                            writer.WriteEndElement()  'actn
                        Case Else
                            'If noteType <> "Stage" And noteType <> "Status changed" And noteType <> "Hold" And _
                            '     noteType <> "Unlinked" And noteType <> "Enf. Agent note" And noteType <> "Linked" And _
                            '     noteType <> "Removed agent" And noteType <> "Off hold" And _
                            '    noteType <> "Defer arrange" And noteType <> "Moved" And noteType <> "Clear arrange" And _
                            '    noteType <> "Client note" And noteType <> "Resolved" And noteType <> "Debt Changed" And _
                            '    noteType <> "Documents" And noteType <> "Address" And noteType <> "Employment" Then
                            '    MsgBox(debtorID & " New note type" & noteType)
                            'End If
                    End Select
                    lastNoteRowid = noteRowid
                    lastNoteType = noteType
                Next

                'now get all Payments for last week
                Dim pay_dt As New DataTable
                'T54418 add payment type and source
                LoadDataTable2("DebtRecovery", "Select amount, date, _rowid, amount_typeID, amount_sourceID" & _
                              " from Payment" & _
                              " where debtorID = " & debtorID & _
                              " and (status = 'W' or status = 'R')" & _
                              " and _createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                             " and _createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'", pay_dt, False)
                Dim firstPayment As Boolean = True
                For Each payRow In pay_dt.Rows
                    Dim payDate As Date = payRow(1)
                    Dim payAmount As Decimal = payRow(0)
                    Dim payRowid As Integer = payRow(2)
                    Dim payType As String = ""
                    Dim onlinePayment As Boolean = False
                    If (payRow(3) = 3 Or payRow(3) = 7) And payRow(4) = 49 Then
                        onlinePayment = True
                    End If
                    Dim firstPayRowid As Integer
                    If Not xmlWritten Then
                        writer.WriteStartElement("Acct")
                        writer.WriteElementString("AcctID", clientRef)
                        writer.WriteElementString("ClntCd", clientCode)
                        writer.WriteStartElement("Actns")
                        xmlWritten = True
                    End If
                    firstPayment = False
                    writer.WriteStartElement("Actn")
                    writer.WriteElementString("ActnTS", Format(payDate, "yyyy-MM-dd") & "T" & Format(payDate, "HH:mm:ss"))
                    writer.WriteElementString("Tp", "UNA")
                    writer.WriteStartElement("Actvts")
                    writer.WriteStartElement("Actvty")
                    writer.WriteElementString("Dt", Format(payDate, "yyyy-MM-dd") & "T" & Format(payDate, "HH:mm:ss"))
                    'T54418
                    If onlinePayment Then
                        writer.WriteElementString("Cd", "ONP")
                    Else
                        writer.WriteElementString("Cd", "PAY")
                    End If
                    If caseStatus = "A" Then

                        Dim Array As Object()
                        Try
                            Array = GetSQLResultsArray2("DebtRecovery", "SELECT min(_rowid) " & _
                                                "FROM payment  " & _
                                                "WHERE debtorID = " & debtorID & _
                                                " and date >= '" & Format(arrangeStarted, "yyyy-MM-dd") & "'")
                            firstPayRowid = Array(0)
                        Catch ex As Exception
                            firstPayRowid = 0
                        End Try

                    End If
                    If caseStatus = "A" Then
                        If payDate < arrangeStarted Then
                            payType = "PAR"
                        Else
                            'is this first payment in arrangement?
                            firstPayRowid = GetSQLResults2("DebtRecovery", "SELECT min(_rowid) " & _
                                                "FROM payment  " & _
                                                "WHERE debtorID = " & debtorID & _
                                                " and date >= '" & Format(arrangeStarted, "yyyy-MM-dd") & "'")

                            If firstPayRowid = payRowid Then
                                payType = "AST"
                            Else
                                payType = "PAR"
                            End If
                        End If
                    ElseIf caseStatus = "S" Or caseStatus = "F" Then
                        'pif if this is last payment
                        Dim lastPayRowid As Integer = GetSQLResults2("DebtRecovery", "SELECT max(_rowid) " & _
                                                "FROM payment  " & _
                                                "WHERE debtorID = " & debtorID)
                        If payRowid = lastPayRowid Then
                            payType = "PIF"
                        Else
                            payType = "PAR"
                        End If

                    ElseIf caseStatus = "C" Then
                        'check for settlement
                        Try
                            If debtRow(4) = 30 Or debtRow(4) = 80 Then
                                payType = "SET"
                            End If
                        Catch ex As Exception
                            payType = "PAR"
                        End Try
                    Else
                        payType = "PAR"
                    End If
                    writer.WriteStartElement("Pymt")
                    writer.WriteElementString("PymtTp", payType)
                    writer.WriteElementString("Val", Format(payAmount, "0.00"))
                    Dim payMethod As String = "OTH"
                    Select Case payRow(3)
                        Case 13, 65
                            payMethod = "BAC"
                        Case 3, 57
                            payMethod = "CC"
                        Case 1, 4, 51, 52, 61, 67
                            payMethod = "CHQ"
                        Case 2, 66
                            payMethod = "CSH"
                        Case 7, 58
                            payMethod = "DC"
                        Case 60
                            payMethod = "DD"
                        Case 11, 50
                            payMethod = "GRO"
                    End Select
                    writer.WriteElementString("Mtd", payMethod)
                    writer.WriteEndElement()  'pymt
                    writer.WriteEndElement()  'acvty
                    writer.WriteEndElement()  'actvts
                    writer.WriteEndElement()  'actn
                Next 'payment
                'see if case returned last week
                If caseStatus = "C" Or caseStatus = "S" Then
                    Dim returnDate As Date = Nothing
                    Try
                        returnDate = debtRow(6)
                    Catch ex As Exception

                    End Try
                    If returnDate <> Nothing Then
                        If returnDate >= startDate _
                           And returnDate <= endDate Then
                            If Not xmlWritten Then
                                writer.WriteStartElement("Acct")
                                writer.WriteElementString("AcctID", clientRef)
                                writer.WriteElementString("ClntCd", clientCode)
                                writer.WriteStartElement("Actns")
                                xmlWritten = True
                            End If
                            writer.WriteStartElement("Actn")
                            writer.WriteElementString("ActnTS", Format(returnDate, "yyyy-MM-dd") & "T" & Format(returnDate, "HH:mm:ss"))
                            'T54418()
                            Dim action As String = "UNA"
                            Dim activity As String = ""
                            If caseStatus = "C" Then
                                If debtRow(4) = 76 Then
                                    action = "MAR"
                                End If
                                If debtRow(4) = 102 Then
                                    activity = "FDF"
                                End If
                                If debtRow(4) = 24 Then
                                    activity = "INS"
                                End If
                                If debtRow(4) = 11 Then
                                    activity = "DCS"
                                End If
                            End If

                            If action = "MAR" Then
                                writer.WriteElementString("Tp", "MAR")
                                writer.WriteStartElement("LtRet")
                                writer.WriteElementString("RtdGA", "true")
                                writer.WriteEndElement()  'LtRet
                                writer.WriteStartElement("Actvts")
                            Else
                                writer.WriteElementString("Tp", "UNA")
                                If activity <> "" Then
                                    writer.WriteStartElement("Actvts")
                                    writer.WriteStartElement("Actvty")
                                    writer.WriteElementString("Dt", Format(returnDate, "yyyy-MM-dd") & "T" & Format(returnDate, "HH:mm:ss"))
                                    writer.WriteElementString("Cd", activity)
                                    writer.WriteEndElement()  'acvty
                                    'writer.WriteEndElement()  'actvts
                                    'writer.WriteEndElement()  'actn
                                Else
                                    writer.WriteStartElement("Actvts")
                                End If
                            End If

                            writer.WriteStartElement("Actvty")
                            writer.WriteElementString("Dt", Format(returnDate, "yyyy-MM-dd") & "T" & Format(returnDate, "HH:mm:ss"))
                            writer.WriteElementString("Cd", "ACL")

                            Dim closureReason As String = ""
                            If caseStatus = "C" Then
                                Dim startIDX As Integer = InStr(cancelledNote, "Note:")
                                If startIDX > 0 Then
                                    closureReason = Microsoft.VisualBasic.Right(cancelledNote, cancelledNote.Length - startIDX - 5)
                                End If
                            Else
                                If caseStatus = "S" Then
                                    closureReason = "PaidinFull"
                                End If
                            End If
                            'If closureReason <> "" Then
                            '    writer.WriteElementString("Notes", closureReason)
                            'End If
                            writer.WriteEndElement()  'acvty
                            writer.WriteEndElement()  'actvts
                            writer.WriteEndElement()  'actn
                        End If
                    End If
                End If

                If xmlWritten Then
                    case_no += 1
                    writer.WriteEndElement()  'actns
                    writer.WriteEndElement()  'acct
                    'If case_no >= 10 Then
                    '    Exit For
                    'End If
                End If
            Next  'debtorID
        Next  'CSID

        writer.Close()

        'validate using xsd
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(activity_file)
        myDocument.Schemas.Add("ActivityData", "R:\vb.net\TDX XSD\Activity_Data.xsd")
        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
        write_audit(error_no & " - errors in report finished at " & Now, False)
        If error_no = 0 Then
            If prod_run Then
                'copy file to home office crystal reports on R drive
                Dim newFilename As String = "R:/Home Office Crystal Reports/" & Path.GetFileName(activity_file)
                My.Computer.FileSystem.CopyFile(activity_file, newFilename)

            End If
        End If
        Me.Close()
    End Sub
    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_audit("Error: " & e.Message, True)
            Case XmlSeverityType.Warning
                write_audit("Warning " & e.Message, True)
        End Select
    End Sub
    Private Sub write_audit(ByVal audit_message As String, ByVal errorMessage As Boolean)
        If errorMessage Then
            error_no += 1
        End If
        audit_message = audit_message & vbNewLine
        My.Computer.FileSystem.WriteAllText(auditFile, audit_message, True)
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        run_report()
    End Sub
End Class

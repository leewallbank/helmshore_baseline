﻿Imports System.Xml.Schema
Imports System.IO
Imports commonlibrary

Public Class EON
    Dim xml_valid As Boolean = True
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim new_file As String = ""
    Dim CSID As Integer
    Dim prod_run As Boolean = False
    Dim placements_processed, total_volume As Integer
    Dim tot4987 As Integer = 0
    Dim tot4988 As Integer = 0
    Dim tot4989 As Integer = 0
    Dim tot4990 As Integer = 0
    Dim tot5052 As Integer = 0

    Dim val4987 As Decimal = 0
    Dim val4988 As Decimal = 0
    Dim val4989 As Decimal = 0
    Dim val4990 As Decimal = 0
    Dim val5052 As Decimal = 0

    Dim placementFile As String = ""
    Dim total_value, total_balance, case_balance As Decimal
    Dim ackFileName As String
    Dim filename, auditFile, errorfile, errorFileName, placementFile_4987, placementFile_4988, placementFile_4989 As String
    Dim placementFile_4990, placementFile_5052, queryFile, acknowledgementFile As String

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        processXMLbtn.Enabled = False
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        End If



    End Sub
    Private Sub validate_xml_file()
        'open file as text first to remove any £ signs
        Dim txt_doc As String
        txt_doc = My.Computer.FileSystem.ReadAllText(OpenFileDialog1.FileName)
        txt_doc = Replace(txt_doc, "£", "")
        My.Computer.FileSystem.WriteAllText(OpenFileDialog1.FileName, txt_doc, False)
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(OpenFileDialog1.FileName)
        Dim XSDFile As String = "R:\vb.net\EON XSD\EON Placement.xsd"
        Try
            myDocument.Schemas.Add("", XSDFile)
        Catch ex As Exception
            errorfile = "File: " & OpenFileDialog1.FileName & "has failed validation with XSD" & vbNewLine
            errorfile &= "XSD file: " & XSDFile & vbNewLine
            errorfile &= "Reason: " & ex.ToString
            My.Computer.FileSystem.WriteAllText(errorFileName, errorfile, False)
            xml_valid = False
            Exit Sub
        End Try

        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
    End Sub
    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_error("Error: " & e.Message)
            Case XmlSeverityType.Warning
                write_error("Warning " & e.Message)
        End Select
    End Sub
    Private Sub write_error(ByVal error_message As String)
        Static Dim error_no As Integer
        error_no += 1
        error_message = error_message & vbNewLine
        If error_no = 1 Then
            My.Computer.FileSystem.WriteAllText(errorFileName, error_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(errorFileName, error_message, True)
        End If

    End Sub

    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub processbtn_Click(sender As System.Object, e As System.EventArgs) Handles processXMLbtn.Click
        With OpenFileDialog1
            .Title = "Read XML file"
            .Filter = "XML file|*.xml"
            .FileName = ""
            .CheckFileExists = True
        End With
        Dim xml_dataSet As New DataSet()
        Dim doc As New Xml.XmlDocument()

        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("File not opened")
            Me.Close()
            Exit Sub
        End If
        Dim InputFilePath As String = Path.GetDirectoryName(OpenFileDialog1.FileName)
        InputFilePath &= "\"
        filename = Path.GetFileNameWithoutExtension(OpenFileDialog1.FileName)
        errorFileName = InputFilePath & filename & "_EON_error.txt"
        validate_xml_file()
        If xml_valid = False Then
            MsgBox("XML file has failed validation - see error file")
            If MsgBox("Do you want to continue processing the file?", MsgBoxStyle.YesNo) = MsgBoxResult.No Then
                Me.Close()
                Exit Sub
            End If
        End If
        Dim fieldValue As String
        Dim comments As String = ""
        Dim rdr_name As String = ""
        Dim TelCount As Integer = 0
        Dim TelNo1 As String = ""
        Dim TelNo2 As String = ""
        Dim TelWork As String = ""
        Dim TelType As String = ""
        Dim fuelType As String = ""
        queryFile &= "BY,HOLD CODE,NOTES,ACTION,DATE CREATED" & vbNewLine
        Dim queries As Integer = 0
        Dim reader As New Xml.XmlTextReader(OpenFileDialog1.FileName)
        reader.Read()
        ProgressBar1.Value = 5
        Dim record_count As Integer = 0
        If prod_run = False Then
            MsgBox("TEST RUN ONLY")
        End If
        Dim majorErrorFound As Boolean = False
        Dim validationErrorFound As Boolean = False
        placementFile_4987 = "CLIENT_REFERENCE|DEBT_EXTERNAL_ID|SEGMENT_CODE|SUB_SEGMENT_CODE|PLACEMENT_TYPE|PRINCIPAL_BALANCE|FEE_BALANCE|" & _
            "BALANCE|DEBT_TYPE|DEBTOR_TYPE|DEBTOR_NAME_1|DEBTOR_NAME_2|LAST_PAYMENT_DATE|LAST_PAYMENT_VALUE|ORIGINAL_BALANCE|CURRENCY_CODE|" & _
            "CURRENT ADDRESS|SUPPLY ADDRESS|TEL NO1|TEL NO2|TEL WORK|EMAIL|FUEL TYPE|COMMENTS" & vbNewLine
        placementFile_4988 = placementFile_4987
        placementFile_4989 = placementFile_4987
        placementFile_4990 = placementFile_4987
        placementFile_5052 = placementFile_4987
        Dim segmentCode As String = ""
        Dim subSegmentCode As String = ""
        Dim client_ref As String = ""
        While (reader.ReadState <> Xml.ReadState.EndOfFile)
            If reader.NodeType > 1 Then
                reader.Read()
                Continue While
            End If
            Try
                rdr_name = reader.Name
            Catch
                Continue While
            End Try

            'Note ReadElementContentAsString moves focus to next element so don't need read
            ProgressBar1.Value = record_count
            Application.DoEvents()
            Select Case rdr_name
                Case "PLACEMENT"
                    reader.Read()
                    While reader.Name <> "PLACEMENT"
                        rdr_name = reader.Name
                        If reader.NodeType > 1 Then
                            reader.Read()
                            Continue While
                        End If
                        rdr_name = reader.Name
                        Select Case rdr_name
                            Case "FILE"
                                reader.Read()
                                While reader.Name <> "FILE"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    rdr_name = reader.Name
                                    Select Case rdr_name
                                        Case "CLIENT_CODE"
                                            fieldValue = reader.ReadElementContentAsString
                                            auditFile &= "Client Code:" & fieldValue & vbNewLine
                                        Case "AGENT_CODE"
                                            fieldValue = reader.ReadElementContentAsString
                                            auditFile &= "Agent Code:" & fieldValue & vbNewLine
                                        Case "VOLUME"
                                            fieldValue = reader.ReadElementContentAsString
                                            auditFile &= "Volume:" & fieldValue & vbNewLine
                                            total_volume = fieldValue
                                        Case "VALUE"
                                            fieldValue = reader.ReadElementContentAsString
                                            auditFile &= "Value:" & fieldValue & vbNewLine
                                            total_value = fieldValue
                                        Case "FILE_NAME"
                                            fieldValue = reader.ReadElementContentAsString
                                            auditFile &= "File Name:" & fieldValue & vbNewLine
                                        Case "FILE_TYPE"
                                            fieldValue = reader.ReadElementContentAsString
                                            auditFile &= "File Type:" & fieldValue & vbNewLine
                                            If fieldValue <> "PLA" Then
                                                errorfile &= "File Type is " & fieldValue & " should be PLA" & vbNewLine
                                                MsgBox("File type is not PLA - see error file")
                                                majorErrorFound = True
                                                Exit While
                                            End If
                                        Case "DIRECTION"
                                            fieldValue = reader.ReadElementContentAsString
                                            auditFile &= "Direction:" & fieldValue & vbNewLine
                                        Case "DATE_CREATED"
                                            fieldValue = reader.ReadElementContentAsString
                                            auditFile &= "Date Created:" & fieldValue & vbNewLine
                                    End Select
                                    reader.Read()
                                End While
                                If majorErrorFound Then
                                    Exit While
                                End If
                            Case "SENDER"
                                reader.Read()
                                While reader.Name <> "SENDER"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    rdr_name = reader.Name
                                    Select Case rdr_name
                                        Case "MEMBER_CODE"
                                            fieldValue = reader.ReadElementContentAsString
                                            auditFile &= "MEMBER Code:" & fieldValue & vbNewLine
                                    End Select
                                    reader.Read()
                                End While
                            Case "DEBTS"
                                reader.Read()
                                While reader.Name <> "DEBTS"
                                    rdr_name = reader.Name
                                    If reader.NodeType > 1 Then
                                        reader.Read()
                                        Continue While
                                    End If
                                    rdr_name = reader.Name
                                    Select Case rdr_name
                                        Case "DEBT"
                                            reader.Read()
                                            segmentCode = ""
                                            subSegmentCode = ""
                                            client_ref = ""
                                            While reader.Name <> "DEBT"
                                                rdr_name = reader.Name
                                                If reader.NodeType > 1 Then
                                                    reader.Read()
                                                    Continue While
                                                End If
                                                rdr_name = reader.Name

                                                Select Case rdr_name
                                                    Case "CLIENT_REFERENCE"
                                                        fieldValue = reader.ReadElementContentAsString
                                                       
                                                        comments = ""
                                                        client_ref = fieldValue
                                                        'Dim include_case As Boolean = False
                                                        'While include_case = False
                                                        '    'ignore if already on onestep
                                                        '    Dim param2 As String = "Select _rowID from debtor where client_ref = '" & client_ref & "'"
                                                        '    get_dataset("onestep", param2)
                                                        '    If no_of_rows > 0 Then
                                                        '        'loop to next client ref
                                                        '        reader.Read()
                                                        '        While reader.Name <> "CLIENT_REFERENCE"
                                                        '            rdr_name = reader.Name
                                                        '            If reader.NodeType > 1 Then
                                                        '                reader.Read()
                                                        '                Continue While
                                                        '            End If
                                                        '            reader.Read()
                                                        '            rdr_name = reader.Name
                                                        '        End While
                                                        '        fieldValue = reader.ReadElementContentAsString
                                                        '        client_ref = fieldValue
                                                        '        If client_ref = "014210672320" Then
                                                        '            Exit While
                                                        '        End If
                                                        '    Else
                                                        '        include_case = True
                                                        '    End If
                                                        'End While
                                                        'If client_ref = "014210672320" Then
                                                        '    Exit While
                                                        'End If
                                                        placementFile &= fieldValue & "|"
                                                        placements_processed += 1
                                                        TelCount = 0
                                                        TelNo1 = ""
                                                        TelNo2 = ""
                                                        TelWork = ""
                                                        TelType = ""
                                                    Case "DEBT_EXTERNAL_ID"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                    Case "SEGMENT_CODE"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        segmentCode = fieldValue
                                                        placementFile &= fieldValue & "|"
                                                    Case "SUB_SEGMENT_CODE"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        subSegmentCode = fieldValue
                                                        placementFile &= fieldValue & "|"
                                                    Case "PLACEMENT_TYPE"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                    Case "PRINCIPAL_BALANCE"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                    Case "FEE_BALANCE"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                    Case "BALANCE"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                        total_balance += fieldValue
                                                        case_balance = fieldValue
                                                    Case "DEBT_TYPE"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                    Case "DEBTOR_TYPE"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                    Case "DEBTOR_NAME_1"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                    Case "DEBTOR_NAME_2"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                    Case "LAST_PAYMENT_DATE"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                    Case "LAST_PAYMENT_VALUE"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                    Case "ORIGINAL_BALANCE"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                    Case "CURRENCY_CODE"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                    Case "ADDRESSES"
                                                        Dim CADaddress As String = ""
                                                        Dim SADAddress As String = ""
                                                        reader.Read()
                                                        While reader.Name <> "ADDRESSES"
                                                            rdr_name = reader.Name
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            rdr_name = reader.Name
                                                            Select Case rdr_name
                                                                Case "ADDRESS"
                                                                    reader.Read()
                                                                    Dim addressType As String = ""
                                                                    Dim address As String = ""
                                                                    While reader.Name <> "ADDRESS"
                                                                        rdr_name = reader.Name
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        rdr_name = reader.Name
                                                                        Select Case rdr_name
                                                                            Case "ADDRESS_TYPE"
                                                                                addressType = reader.ReadElementContentAsString
                                                                            Case "ADDRESS_LINE1"
                                                                                fieldValue = Trim(reader.ReadElementContentAsString)
                                                                                If fieldValue <> "" Then
                                                                                    address &= fieldValue & ","
                                                                                End If
                                                                            Case "ADDRESS_LINE2"
                                                                                fieldValue = Trim(reader.ReadElementContentAsString)
                                                                                If fieldValue <> "" Then
                                                                                    address &= fieldValue & ","
                                                                                End If
                                                                            Case "ADDRESS_LINE3"
                                                                                fieldValue = Trim(reader.ReadElementContentAsString)
                                                                                If fieldValue <> "" Then
                                                                                    address &= fieldValue & ","
                                                                                End If
                                                                            Case "ADDRESS_LINE4"
                                                                                fieldValue = Trim(reader.ReadElementContentAsString)
                                                                                If fieldValue <> "" Then
                                                                                    address &= fieldValue & ","
                                                                                End If
                                                                            Case "POSTCODE"
                                                                                fieldValue = Trim(reader.ReadElementContentAsString)
                                                                                If fieldValue <> "" Then
                                                                                    address &= fieldValue
                                                                                End If
                                                                        End Select
                                                                        reader.Read()
                                                                    End While
                                                                    If addressType = "CAD" Then
                                                                        CADaddress = address
                                                                    ElseIf addressType = "SAD" Then
                                                                        SADAddress = address
                                                                    Else
                                                                        comments &= ToNote(address, addressType)(0)
                                                                    End If
                                                            End Select
                                                            reader.Read()
                                                        End While
                                                        placementFile &= CADaddress & "|" & SADAddress & "|"
                                                    Case "TELEPHONES"
                                                        reader.Read()
                                                        While reader.Name <> "TELEPHONES"
                                                            rdr_name = reader.Name
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            rdr_name = reader.Name
                                                            Select Case rdr_name
                                                                Case "TELEPHONE"
                                                                    reader.Read()
                                                                    While reader.Name <> "TELEPHONE"
                                                                        rdr_name = reader.Name
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        rdr_name = reader.Name
                                                                        Select Case rdr_name
                                                                            Case "TELEPHONE_TYPE"
                                                                                TelType = reader.ReadElementContentAsString
                                                                            Case "TELEPHONE_NUMBER"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                If fieldValue <> "" Then
                                                                                    If TelType = "WORK" Then
                                                                                        TelWork = fieldValue
                                                                                    Else
                                                                                        Select Case TelCount
                                                                                            Case 0
                                                                                                TelNo1 = fieldValue
                                                                                            Case 1
                                                                                                TelNo2 = fieldValue
                                                                                            Case Is > 1
                                                                                                comments &= ToNote(fieldValue, "Tel")(0)
                                                                                        End Select
                                                                                        TelCount += 1
                                                                                    End If
                                                                                End If
                                                                        End Select
                                                                        reader.Read()
                                                                    End While
                                                            End Select
                                                            reader.Read()
                                                        End While
                                                    Case "QUERIES"
                                                        reader.Read()
                                                        While reader.Name <> "QUERIES"
                                                            rdr_name = reader.Name
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            rdr_name = reader.Name
                                                            Dim queryBy As String = ""
                                                            Dim queryHoldCode As String = ""
                                                            Dim queryNotes As String = ""
                                                            Dim queryAction As String = ""
                                                            Dim queryDateCreated As String = ""
                                                            Select Case rdr_name
                                                                Case "QUERY"
                                                                    reader.Read()
                                                                    While reader.Name <> "QUERY"
                                                                        rdr_name = reader.Name
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        rdr_name = reader.Name
                                                                        Select Case rdr_name
                                                                            Case "BY"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                queryBy = fieldValue
                                                                            Case "HOLD_CODE"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                queryHoldCode = fieldValue
                                                                            Case "NOTES"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                queryNotes = fieldValue
                                                                            Case "ACTION"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                queryAction = fieldValue
                                                                            Case "DATE_CREATED"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                queryDateCreated = fieldValue
                                                                        End Select
                                                                        reader.Read()
                                                                    End While
                                                                    queryFile &= queryBy & "," & queryHoldCode & "," & queryNotes & "," & queryAction & "," & queryDateCreated & vbNewLine
                                                                    queries += 1
                                                            End Select
                                                            reader.Read()
                                                        End While
                                                    Case "DEBT_SUNDRY"
                                                        reader.Read()
                                                        While reader.Name <> "DEBT_SUNDRY"
                                                            rdr_name = reader.Name
                                                            If reader.NodeType > 1 Then
                                                                reader.Read()
                                                                Continue While
                                                            End If
                                                            rdr_name = reader.Name
                                                            Select Case rdr_name
                                                                Case "ACCOUNT"
                                                                    reader.Read()
                                                                    While reader.Name <> "ACCOUNT"
                                                                        rdr_name = reader.Name
                                                                        If reader.NodeType > 1 Then
                                                                            reader.Read()
                                                                            Continue While
                                                                        End If
                                                                        rdr_name = reader.Name
                                                                        Select Case rdr_name
                                                                            Case "COMMENT"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                If fieldValue <> "" Then
                                                                                    comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                End If
                                                                            Case "VISIT_TYPE"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                If fieldValue <> "" Then
                                                                                    comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                End If
                                                                            Case "BALANCE_DATE"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                If fieldValue <> "" Then
                                                                                    comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                End If
                                                                            Case "DATE_LETTER_SENT"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                If fieldValue <> "" Then
                                                                                    comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                End If
                                                                            Case "FUEL"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                If fieldValue <> "" Then
                                                                                    comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                End If
                                                                                Select Case fieldValue
                                                                                    Case "E"
                                                                                        fuelType = "Electric"
                                                                                    Case "G"
                                                                                        fuelType = "Gas"
                                                                                    Case "O"
                                                                                        fuelType = "Dual Fuel"
                                                                                End Select
                                                                            Case "PAYMENT_METHOD"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                If fieldValue <> "" Then
                                                                                    comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                End If
                                                                            Case "SUPPLIER"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                If fieldValue <> "" Then
                                                                                    comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                End If
                                                                            Case "ACCOUNT_START_DATE"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                If fieldValue <> "" Then
                                                                                    comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                End If
                                                                            Case "PRODUCTS"
                                                                                reader.Read()
                                                                                While reader.Name <> "PRODUCTS"
                                                                                    rdr_name = reader.Name
                                                                                    If reader.NodeType > 1 Then
                                                                                        reader.Read()
                                                                                        Continue While
                                                                                    End If
                                                                                    rdr_name = reader.Name
                                                                                    Select Case rdr_name
                                                                                        Case "PRODUCT"
                                                                                            reader.Read()
                                                                                            While reader.Name <> "PRODUCT"
                                                                                                rdr_name = reader.Name
                                                                                                If reader.NodeType > 1 Then
                                                                                                    reader.Read()
                                                                                                    Continue While
                                                                                                End If
                                                                                                rdr_name = reader.Name
                                                                                                Select Case rdr_name
                                                                                                    Case "TYPE"
                                                                                                        fieldValue = reader.ReadElementContentAsString
                                                                                                        If fieldValue <> "" Then
                                                                                                            comments &= ToNote(fieldValue, "PRODUCT TYPE")(0)
                                                                                                        End If
                                                                                                    Case "PRODUCT_START_DATE"
                                                                                                        fieldValue = reader.ReadElementContentAsString
                                                                                                        If fieldValue <> "" Then
                                                                                                            comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                                        End If
                                                                                                    Case "PRODUCT_END_DATE"
                                                                                                        fieldValue = reader.ReadElementContentAsString
                                                                                                        If fieldValue <> "" Then
                                                                                                            comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                                        End If
                                                                                                    Case "TERMINATION_REASON"
                                                                                                        fieldValue = reader.ReadElementContentAsString
                                                                                                        If fieldValue <> "" Then
                                                                                                            comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                                        End If
                                                                                                    Case "HAS_STAYWARM"
                                                                                                        fieldValue = reader.ReadElementContentAsString
                                                                                                        If fieldValue <> "" Then
                                                                                                            comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                                        End If
                                                                                                    Case "VULNERABLE"
                                                                                                        fieldValue = reader.ReadElementContentAsString
                                                                                                        If fieldValue <> "" Then
                                                                                                            comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                                        End If
                                                                                                    Case "PRODUCT_REFERENCE"
                                                                                                        fieldValue = reader.ReadElementContentAsString
                                                                                                        If fieldValue <> "" Then
                                                                                                            comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                                        End If
                                                                                                    Case "METERS"
                                                                                                        reader.Read()
                                                                                                        While reader.Name <> "METERS"
                                                                                                            'T107874 handle blank meters tag
                                                                                                            If reader.Name = "PRODUCT" Then
                                                                                                                Exit While
                                                                                                            End If
                                                                                                            rdr_name = reader.Name
                                                                                                            If reader.NodeType > 1 Then
                                                                                                                reader.Read()
                                                                                                                Continue While
                                                                                                            End If
                                                                                                            rdr_name = reader.Name
                                                                                                            Select Case rdr_name
                                                                                                                Case "METER"
                                                                                                                    fieldValue = reader.Item(0)

                                                                                                                Case "MPAN_MPR"
                                                                                                                    fieldValue = reader.ReadElementContentAsString
                                                                                                                    comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                                                Case "REGISTERS"
                                                                                                                    reader.Read()
                                                                                                                    While reader.Name <> "REGISTERS"
                                                                                                                        rdr_name = reader.Name
                                                                                                                        If reader.NodeType > 1 Then
                                                                                                                            reader.Read()
                                                                                                                            Continue While
                                                                                                                        End If
                                                                                                                        rdr_name = reader.Name
                                                                                                                        Dim reading_id As String = ""
                                                                                                                        Dim reading_note As String = ""
                                                                                                                        Select Case rdr_name
                                                                                                                            Case "REGISTER"
                                                                                                                                reader.Read()
                                                                                                                                While reader.Name <> "REGISTER"
                                                                                                                                    rdr_name = reader.Name
                                                                                                                                    If reader.NodeType > 1 Then
                                                                                                                                        reader.Read()
                                                                                                                                        Continue While
                                                                                                                                    End If
                                                                                                                                    rdr_name = reader.Name
                                                                                                                                    Select Case rdr_name
                                                                                                                                        Case "READINGS"
                                                                                                                                            reader.Read()
                                                                                                                                            While reader.Name <> "READINGS"
                                                                                                                                                rdr_name = reader.Name
                                                                                                                                                If reader.NodeType > 1 Then
                                                                                                                                                    reader.Read()
                                                                                                                                                    Continue While
                                                                                                                                                End If
                                                                                                                                                rdr_name = reader.Name
                                                                                                                                                Select Case rdr_name
                                                                                                                                                    Case "READING"
                                                                                                                                                        reading_id = reader.Item(0)
                                                                                                                                                        reading_id = "Reading " & reading_id
                                                                                                                                                        reader.Read()
                                                                                                                                                    Case "READING_DATE"
                                                                                                                                                        fieldValue = reader.ReadElementContentAsString
                                                                                                                                                        If fieldValue <> "" Then
                                                                                                                                                            reading_note &= "Reading date:" & fieldValue & ";"
                                                                                                                                                        End If
                                                                                                                                                    Case "READING_TYPE"
                                                                                                                                                        fieldValue = reader.ReadElementContentAsString
                                                                                                                                                        If fieldValue <> "" Then
                                                                                                                                                            reading_note &= "Reading Type:" & fieldValue & ";"
                                                                                                                                                        End If
                                                                                                                                                    Case "VALUE"
                                                                                                                                                        fieldValue = reader.ReadElementContentAsString
                                                                                                                                                        If fieldValue <> "" Then
                                                                                                                                                            reading_note &= "Value:" & fieldValue
                                                                                                                                                        End If
                                                                                                                                                        comments &= ToNote(reading_note, reading_id)(0)
                                                                                                                                                        reading_note = ""
                                                                                                                                                End Select
                                                                                                                                            End While
                                                                                                                                        Case Else
                                                                                                                                            reader.Read()
                                                                                                                                    End Select
                                                                                                                                End While
                                                                                                                        End Select
                                                                                                                    End While
                                                                                                            End Select
                                                                                                            reader.Read()
                                                                                                        End While
                                                                                                End Select
                                                                                                'T107874
                                                                                                If reader.Name <> "PRODUCT" Then
                                                                                                    reader.Read()
                                                                                                End If
                                                                                            End While
                                                                                    End Select
                                                                                    reader.Read()
                                                                                End While
                                                                            Case "BILL_HISTORY"
                                                                                reader.Read()
                                                                                While reader.Name <> "BILL_HISTORY"
                                                                                    rdr_name = reader.Name
                                                                                    If reader.NodeType > 1 Then
                                                                                        reader.Read()
                                                                                        Continue While
                                                                                    End If
                                                                                    rdr_name = reader.Name
                                                                                    Select Case rdr_name
                                                                                        Case "BILL"
                                                                                            fieldValue = reader.Item(0)
                                                                                            If fieldValue <> "" Then
                                                                                                comments &= ToNote(fieldValue, "ID")(0)
                                                                                            End If
                                                                                        Case "DATE"
                                                                                            fieldValue = reader.ReadElementContentAsString
                                                                                            If fieldValue <> "" Then
                                                                                                comments &= ToNote(fieldValue, "BILL DATE")(0)
                                                                                            End If
                                                                                        Case "TYPE"
                                                                                            fieldValue = reader.ReadElementContentAsString
                                                                                            If fieldValue <> "" Then
                                                                                                comments &= ToNote(fieldValue, "BILL TYPE")(0)
                                                                                            End If
                                                                                        Case "TOTAL_CHARGES"
                                                                                            fieldValue = reader.ReadElementContentAsString
                                                                                            If fieldValue <> "" Then
                                                                                                comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                            End If
                                                                                        Case "BALANCE"
                                                                                            fieldValue = reader.ReadElementContentAsString
                                                                                            If fieldValue <> "" Then
                                                                                                comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                            End If
                                                                                    End Select
                                                                                    reader.Read()
                                                                                End While
                                                                            Case "LAST_PAY"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                If fieldValue <> "" Then
                                                                                    comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                End If
                                                                            Case "LAST_PAY_DATE"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                If fieldValue <> "" Then
                                                                                    comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                End If
                                                                            Case "CONTACT_INFORMATION"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                If fieldValue <> "" Then
                                                                                    comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                End If
                                                                            Case "LARGE_LETTER_BRAILLE"
                                                                                fieldValue = reader.ReadElementContentAsString
                                                                                If fieldValue <> "" Then
                                                                                    comments &= ToNote(fieldValue, rdr_name)(0)
                                                                                End If
                                                                            Case Else
                                                                                MsgBox("What is this tag?" & rdr_name)
                                                                                reader.Read()
                                                                        End Select
                                                                        reader.Read()
                                                                    End While
                                                            End Select
                                                            reader.Read()
                                                        End While
                                                    Case "EMAIL"
                                                        placementFile &= TelNo1 & "|" & TelNo2 & "|" & TelWork & "|"
                                                        fieldValue = reader.ReadElementContentAsString
                                                        placementFile &= fieldValue & "|"
                                                    Case Else
                                                        MsgBox("What is this tag?" & rdr_name)
                                                        reader.Read()
                                                End Select
                                                reader.Read()
                                            End While

                                    End Select
                                    placementFile &= fuelType & "|" & comments & vbNewLine
                                    reader.Read()
                                    Dim csid As Integer = 0
                                    Select Case segmentCode
                                        Case "EONSMCOSBL", "EONSMCOSBM", "EONSMCOSBH"
                                            If subSegmentCode = "EONSMCOSBL1P" Or subSegmentCode = "EONSMCOSB2P" Or
                                                subSegmentCode = "EONSMCOSBM1P" Or subSegmentCode = "EONSMCOSBH1P" Then
                                                csid = 4987
                                            End If
                                        Case "EONSMCOTBL", "EONSMCOTBM", "EONSMCOTBH"
                                            If subSegmentCode = "EONSMCOTBL1P" Or subSegmentCode = "EONSMCOTB2P" Or
                                                 subSegmentCode = "EONSMCOTBM1P" Or subSegmentCode = "EONSMCOTBH1P" Then
                                                csid = 4988
                                            End If
                                        Case "EONRMCOSBL", "EONRMCOSBM", "EONRMCOSBH"
                                            If subSegmentCode = "EONRMCOSBL1P" Or subSegmentCode = "EONRMCOSB2P" Or subSegmentCode = "EONRMCOSB3P" Or
                                                subSegmentCode = "EONRMCOSBM1P" Or subSegmentCode = "EONRMCOSBH1P" Then
                                                csid = 4989
                                            End If
                                        Case "EONRMCOTNF"
                                            If subSegmentCode = "EONRMCOTNF1P" Or subSegmentCode = "EONRMCOTNF2P" Then
                                                csid = 4990
                                            End If
                                        Case "EONSMCOTNF"
                                            If subSegmentCode = "EONSMCOTNF2P" Then
                                                csid = 5052
                                            End If
                                    End Select

                                    If csid = 0 Then
                                        MsgBox("Require Mapping for segment code= " & segmentCode & " and Sub segment code = " & subSegmentCode & " for client-ref = " & client_ref)
                                        errorfile &= "Require Mapping for segment code= " & segmentCode & " and Sub segment code = " & subSegmentCode & " for client-ref = " & client_ref & vbNewLine
                                        validationErrorFound = True
                                    End If
                                    Select Case csid
                                        Case 4987
                                            tot4987 += 1
                                            placementFile_4987 &= placementFile
                                            val4987 += case_balance
                                        Case 4988
                                            tot4988 += 1
                                            placementFile_4988 &= placementFile
                                            val4988 += case_balance
                                        Case 4989
                                            tot4989 += 1
                                            placementFile_4989 &= placementFile
                                            val4989 += case_balance
                                        Case 4990
                                            tot4990 += 1
                                            placementFile_4990 &= placementFile
                                            val4990 += case_balance
                                        Case 5052
                                            tot5052 += 1
                                            placementFile_5052 &= placementFile
                                            val5052 += case_balance
                                    End Select
                                    placementFile = ""
                                    case_balance = 0
                                End While

                            Case Else
                                MsgBox("What is this tag?" & rdr_name)
                                reader.Read()
                        End Select
                        reader.Read()
                    End While
                    If majorErrorFound Then
                        Exit While
                    End If
                Case Else
                    MsgBox("What is this tag?" & rdr_name)
                    reader.Read()
            End Select
        End While

        'write out files
        'If queryRequestFound Then
        '    filename = toClientName & "\wagy023o_QueryRequest " & Format(Now, "dd.MM.yyyy") & " .txt"
        '    My.Computer.FileSystem.WriteAllText(filename, queryRequestFile, False, ascii)
        'End If

        'If queryResponseFound Then
        '    filename = toClientName & "\wagy023o_QueryResponse " & Format(Now, "dd.MM.yyyy") & " .txt"
        '    My.Computer.FileSystem.WriteAllText(filename, queryResponseFile, False, ascii)
        'End If
        'If infoFound Then
        '    filename = toClientName & "\wagy023o_InformationStatus " & Format(Now, "dd.MM.yyyy") & " .xls"
        '    My.Computer.FileSystem.WriteAllText(filename, informationStatusFile, False)
        'End If

        'If dirPaycases > 0 Then
        '    filename = toClientName & "\wagy023o_DirectPayment " & Format(Now, "dd.MM.yyyy") & " .xls"
        '    My.Computer.FileSystem.WriteAllText(filename, directPaymentFile, False)
        'End If

        Dim continueRegardless As Boolean = True


        If majorErrorFound Or validationErrorFound Then
            My.Computer.FileSystem.WriteAllText(errorFileName, errorfile, False)
            MsgBox("Errors found - see reports")
        Else
            'audit file
            If total_balance <> total_value Then
                If MsgBox("Warning check audit file - totals do not balance - carry on Y/N?", MsgBoxStyle.YesNo, "Carry on?") = MsgBoxResult.No Then
                    continueRegardless = False
                End If
                auditFile &= " Header Total value = " & total_value & vbNewLine
                auditFile &= " Sum of balances = " & total_balance & vbNewLine
            Else
                If placements_processed <> total_volume Then
                    If MsgBox("Warning check audit file - totals do not balance - carry on?", MsgBoxStyle.YesNo, "Carry on?") = MsgBoxResult.No Then
                        continueRegardless = False
                    End If
                    auditFile &= " Header Total volume = " & total_volume & vbNewLine
                    auditFile &= " No of cases = " & placements_processed & vbNewLine
                End If
            End If
            If continueRegardless Then
                'Acknowledgement file
                'read in file and update value and volume on row 2, cols 2 and 3
                Dim InputLineArray() As String
                Dim FileContents() As String = System.IO.File.ReadAllLines(ackFileName)
                'now rename original file
                Try
                    My.Computer.FileSystem.RenameFile(ackFileName, "original_acknowledgement.csv")
                Catch ex As Exception

                End Try


                Dim lineNumber As Integer
                acknowledgementFile = ""
                'T108167 add timestamp to ack file name
                'look for last hyphen
                Dim idx As Integer
                For idx = ackFileName.Length To 1 Step -1
                    If Mid(ackFileName, idx, 1) = "-" Then
                        Exit For
                    End If
                Next
                ackFileName = Microsoft.VisualBasic.Left(ackFileName, idx)
                Dim runtime As Date = Now
                ackFileName &= Format(runtime, "ddMMyyyyHHmmss") & ".csv"

                For Each InputLine As String In FileContents
                    InputLineArray = InputLine.Split(",")
                    lineNumber += 1
                    If lineNumber = 2 Then
                        InputLineArray(2) = total_value
                        InputLineArray(3) = total_volume
                    End If
                    'write out new acknowledgement file
                    Select Case lineNumber
                        Case 1
                            For colIDX = 0 To 2
                                If colIDX < 2 Then
                                    acknowledgementFile &= InputLineArray(colIDX) & ","
                                Else
                                    acknowledgementFile &= Format(runtime, "yyyy-MM-dd HH:mm:ss") & ","
                                End If
                            Next
                            acknowledgementFile &= Path.GetFileName(ackFileName) & vbNewLine
                        Case 2
                            For colIDX = 0 To 2
                                acknowledgementFile &= InputLineArray(colIDX) & ","
                            Next
                            acknowledgementFile &= InputLineArray(3) & vbNewLine
                        Case 3
                            acknowledgementFile &= "ROS" & vbNewLine
                    End Select

                Next
               
                My.Computer.FileSystem.WriteAllText(ackFileName, acknowledgementFile, False, ascii)
            End If
        End If

        auditFile &= placements_processed & " placements processed at " & Format(Now, "dd/MMM/yyyy hh:mm:ss") & vbNewLine & vbNewLine
        If tot4987 > 0 Then
            auditFile &= "CSID 4987 cases = " & tot4987 & " value = " & val4987 & vbNewLine
        End If
        If tot4988 > 0 Then
            auditFile &= "CSID 4988 cases = " & tot4988 & " value = " & val4988 & vbNewLine
        End If
        If tot4989 > 0 Then
            auditFile &= "CSID 4989 cases = " & tot4989 & " value = " & val4989 & vbNewLine
        End If
        If tot4990 > 0 Then
            auditFile &= "CSID 4990 cases = " & tot4990 & " value = " & val4990 & vbNewLine
        End If
        If tot5052 > 0 Then
            auditFile &= "CSID 5052 cases = " & tot5052 & " value = " & val5052 & vbNewLine
        End If
        auditFile &= "Number of Queries:" & queries & vbNewLine


        If queries > 0 Then
            Dim queryfilename As String = InputFilePath & filename & "_Queries_" & Format(Now, "yyyy.MM.dd") & ".csv"
            My.Computer.FileSystem.WriteAllText(queryfilename, queryFile, False)
        End If

        Dim auditfilename As String = InputFilePath & filename & "_Audit_" & Format(Now, "yyyy.MM.dd") & ".txt"
        My.Computer.FileSystem.WriteAllText(auditfilename, auditFile, False)
        If continueRegardless Then
            If tot4987 > 0 Then
                Dim placementfilename As String = InputFilePath & filename & "_PreProcess_4987_" & Format(Now, "yyyy.MM.dd") & ".txt"
                My.Computer.FileSystem.WriteAllText(placementfilename, placementFile_4987, False)
            End If
            If tot4988 > 0 Then
                Dim placementfilename As String = InputFilePath & filename & "_PreProcess_4988_" & Format(Now, "yyyy.MM.dd") & ".txt"
                My.Computer.FileSystem.WriteAllText(placementfilename, placementFile_4988, False)
            End If
            If tot4989 > 0 Then
                Dim placementfilename As String = InputFilePath & filename & "_PreProcess_4989_" & Format(Now, "yyyy.MM.dd") & ".txt"
                My.Computer.FileSystem.WriteAllText(placementfilename, placementFile_4989, False)
            End If
            If tot4990 > 0 Then
                Dim placementfilename As String = InputFilePath & filename & "_PreProcess_4990_" & Format(Now, "yyyy.MM.dd") & ".txt"
                My.Computer.FileSystem.WriteAllText(placementfilename, placementFile_4990, False)
            End If
            If tot5052 > 0 Then
                Dim placementfilename As String = InputFilePath & filename & "_PreProcess_5052_" & Format(Now, "yyyy.MM.dd") & ".txt"
                My.Computer.FileSystem.WriteAllText(placementfilename, placementFile_5052, False)
            End If
        End If
        MsgBox("Files saved")

        Me.Close()
    End Sub
    Private Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, Optional ByVal NoteLength As Integer = 250) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 2) ' + 2 is for :;
        Dim PreviousLength As Integer
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = (NoteTag & ":" & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)) & ";").PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function
    Private Function getCaseID(ByVal clientRef As String) As Integer
        Dim debtorID As Integer = 0
        debtorID = GetSQLResults("DebtRecovery", "SELECT D._rowID " & _
                                                   "FROM debtor D, clientscheme CS  " & _
                                                   "WHERE client_ref = '" & clientRef & "'" & _
                                                   " AND D.clientschemeID = CS._rowID " & _
                                                   " AND CS.clientID = 1662")

        Return debtorID
    End Function

    Private Sub ackbtn_Click(sender As System.Object, e As System.EventArgs) Handles ackbtn.Click
        With OpenFileDialog1
            .Title = "Read CSV file"
            .Filter = "CSV file|*.csv"
            .FileName = ""
            .CheckFileExists = True
        End With
        If OpenFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("File not opened")
            Me.Close()
            Exit Sub
        End If
        ackFileName = OpenFileDialog1.FileName
        processXMLbtn.Enabled = True
    End Sub
End Class

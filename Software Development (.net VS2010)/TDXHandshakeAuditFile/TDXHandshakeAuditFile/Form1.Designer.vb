﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.readbtn = New System.Windows.Forms.Button()
        Me.createbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.SuspendLayout()
        '
        'readbtn
        '
        Me.readbtn.Location = New System.Drawing.Point(69, 47)
        Me.readbtn.Name = "readbtn"
        Me.readbtn.Size = New System.Drawing.Size(107, 23)
        Me.readbtn.TabIndex = 0
        Me.readbtn.Text = "Read in File"
        Me.readbtn.UseVisualStyleBackColor = True
        '
        'createbtn
        '
        Me.createbtn.Location = New System.Drawing.Point(69, 107)
        Me.createbtn.Name = "createbtn"
        Me.createbtn.Size = New System.Drawing.Size(107, 23)
        Me.createbtn.TabIndex = 1
        Me.createbtn.Text = "Create Audit file"
        Me.createbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(197, 209)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.createbtn)
        Me.Controls.Add(Me.readbtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TDX Handshake Audit File"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents readbtn As System.Windows.Forms.Button
    Friend WithEvents createbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog

End Class

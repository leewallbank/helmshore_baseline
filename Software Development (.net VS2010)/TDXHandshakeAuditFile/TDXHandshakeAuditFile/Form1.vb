﻿Imports System.IO

Public Class Form1
    Public ascii As New System.Text.ASCIIEncoding()
    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        createbtn.Enabled = False
    End Sub

    Private Sub readbtn_Click(sender As System.Object, e As System.EventArgs) Handles readbtn.Click
        Dim FileDialog As New OpenFileDialog
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        Dim InputFilePath As String = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        Dim FileName As String = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        Dim newFilename As String = InputFilePath & FileName & "_Audit.txt"
        Dim FileExt As String = Path.GetExtension(FileDialog.FileName)

        createbtn.Enabled = True
        Dim InputLineArray() As String
        Dim value As Decimal = 0
        Dim volume As Integer = 0
        Dim outfile As String
        Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
        For Each InputLine As String In FileContents
            InputLineArray = InputLine.Split(",")
            Try
                value = InputLineArray(3)
            Catch ex As Exception

            End Try
            Try
                volume = InputLineArray(4)
            Catch ex As Exception

            End Try
            Exit For
        Next
        'Transaction/NFU/Recall
        Dim fileType As String = ""
        If InStr(FileName, "NFU") > 0 Then
            fileType = "NFU"
        ElseIf InStr(FileName, "Recall") > 0 Then
            fileType = "Recall"
        ElseIf InStr(FileName, "Trans") > 0 Then
            fileType = "Transaction"
        ElseIf InStr(FileName, "Update") > 0 Then
            fileType = "Update"
        End If
        outfile = "File Type: " & fileType & vbNewLine
        outfile &= "File Name: " & FileName & FileExt & vbNewLine
        outfile &= "Volume Loaded: " & volume & vbNewLine
        outfile &= "Value Loaded: " & value & vbNewLine
        outfile &= "Exceptions: 0" & vbNewLine

        My.Computer.FileSystem.WriteAllText(newFilename, outfile, False, ascii)
        MsgBox("Audit file saved")
        Me.Close()
    End Sub
End Class

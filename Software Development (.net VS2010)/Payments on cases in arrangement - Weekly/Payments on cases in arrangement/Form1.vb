﻿Public Class Form1
    Dim upd_txt As String
    Dim arrStartDate As Date
    Dim arrEndDate As Date
    Dim lastdebtCosts As Decimal
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("FeesSQL")

        Dim highDate As Date = CDate("Jan 1, 2100 00:00:00")
        Dim startDate As Date = DateAdd(DateInterval.Day, -6 - Weekday(Now), Now)
        startDate = CDate(Format(startDate, "yyyy-MM-dd") & " 00:00:00")
        Dim endDate As Date = DateAdd(DateInterval.Day, 7, startDate)


        upd_txt = "delete from PaymentsOnArrangementsWeekly where arr_date >= '" & Format(startDate, "MMM dd, yyyy") & "'"
        update_sql(upd_txt)

        'get all arrangements set up last week
        Dim arr_dt As New DataTable
        LoadDataTable2("DebtRecovery", "select DebtorID, type, N._createdDate, N._createdBy, debt_amount,debt_costs, status, text from Note N, debtor D " & _
                       " where type in ('Arrangement','Payment plan','Broken','Clear arrange')" & _
                       " and N._createdDate >= '" & Format(startDate, "yyyy-MM-dd") & "'" & _
                       " and N._createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                       " and D._rowID = N.DebtorID " & _
                       " order by DebtorID, N._rowID", arr_dt, False)

        arrEndDate = highDate
        Dim lastDebtorID As Integer = 0
        Dim lastAgent As String = ""
        Dim lastStatus As String = ""
        Dim lastfrequency As Integer
        For Each ArrRow In arr_dt.Rows
            Dim debtorID As Integer = ArrRow(0)
            Dim agent As String
            Try
                agent = ArrRow(3)
            Catch ex As Exception
                agent = ""
            End Try
            Dim status As String = ArrRow(6)
            If debtorID <> lastDebtorID Then
                'new debtor so check any payments for last debtor
                If lastDebtorID > 0 Then
                    arrEndDate = endDate
                    If arrStartDate < arrEndDate Then
                        'look for payments in date range
                        get_payments(lastDebtorID, lastAgent, lastStatus, lastfrequency)
                    End If
                End If
                lastDebtorID = debtorID
                lastAgent = agent
                arrStartDate = highDate
                lastdebtCosts = ArrRow(4) + ArrRow(5)
                lastStatus = status
            End If

            If ArrRow(1) = "Arrangement" Or ArrRow(1) = "Payment plan" Then
                'get frequency
                Dim frequency As Integer = 0
                Dim startIDX As Integer = InStr(ArrRow(7), "every")
                If startIDX > 0 Then
                    Dim endIDX As Integer = InStr(ArrRow(7), "days")
                    If endIDX > startIDX Then
                        Try
                            frequency = Mid(ArrRow(7), startIDX + 5, endIDX - startIDX - 5)
                        Catch ex As Exception
                            Continue For
                        End Try

                    End If
                End If
                If frequency = 0 And Microsoft.VisualBasic.Left(ArrRow(7), 5) <> "Whole" Then
                    Continue For
                End If
                If arrStartDate < highDate Then
                    arrEndDate = ArrRow(2)
                    'look for payments in date range
                    get_payments(debtorID, lastAgent, status, frequency)
                End If
                arrStartDate = ArrRow(2)
                lastAgent = agent
                lastfrequency = frequency
            Else
                'Broken
                arrEndDate = ArrRow(2)
                If arrStartDate < arrEndDate Then
                    'look for payments in date range
                    get_payments(debtorID, lastAgent, status, lastfrequency)
                End If
                arrStartDate = highDate
            End If
        Next

        'don't forget last one
        arrEndDate = endDate
        If arrStartDate < arrEndDate Then
            'get payments in range
            get_payments(lastDebtorID, lastAgent, lastStatus, lastfrequency)
        End If
        Me.Close()
    End Sub
    Private Sub get_payments(ByVal linkdebtorID As Integer, ByVal linkAgent As String, ByVal linkStatus As String, ByVal frequency As Integer)
        'see if any payments in date range
        Dim pay_dt As New DataTable
        Dim agentID As Integer = 0
        LoadDataTable2("DebtRecovery", "select _rowID, date, split_debt, split_costs" & _
                       " from payment " & _
                       " where status in('W','R')" & _
                       " and amount <> 0" & _
                       " and Date >= '" & Format(arrStartDate, "yyyy-MM-dd") & "'" & _
                        " and DebtorID = " & linkdebtorID & _
                        " order by date", pay_dt, False)
        For Each payrow In pay_dt.Rows
            Dim payDate As Date = payrow(1)
            If Format(payDate, "yyyy-MM-dd") >= Format(arrEndDate, "yyyy-MM-dd") Then
                Exit For
            End If
            ''ignore any payment more than 31 days after arrangement start
            'If DateDiff(DateInterval.Day, arrStartDate, payDate) > 31 Then
            '    Exit For
            'End If
            Dim paymentID As Integer = payrow(0)
            If agentID = 0 Then
                agentID = GetSQLResults2("DebtRecovery", "SELECT _rowID " & _
                                                   "FROM bailiff  " & _
                                                   "WHERE login_name = '" & linkAgent & "'")
            End If
            Dim TotalFees As Decimal
            TotalFees = GetSQLResults2("DebtRecovery", "select sum(fee_amount) from Fee " & _
                                     " where DebtorID = " & linkdebtorID & _
                                     " and fee_remit_col > 2")

            'get client balance at point payment was made
            'Dim PayArray As Object()
            ''ONE row ONLY
            'PayArray = GetSQLResultsArray2("DebtRecovery", "select sum(split_debt), sum(split_costs)" & _
            '           " from payment P " & _
            '           " where status ='R'" & _
            '           " and _rowid < " & paymentID & _
            '            " and DebtorID = " & linkdebtorID)
            'Try
            '    clientBalance = lastdebtCosts - PayArray(0) - PayArray(1)
            'Catch ex As Exception
            '    clientBalance = lastdebtCosts
            'End Try
            'ignore if pif and this is last payment and arrangement is same day
            'If (linkStatus = "F" Or linkStatus = "S") And Format(arrStartDate, "yyyy-MM-dd") = Format(payDate, "yyyy-MM-dd") Then
            '    Dim paidAmount As Decimal = payrow(2) + payrow(3)
            '    If clientBalance - paidAmount = 0 Then
            '        '        Continue For
            '    End If
            'End If
            'NOTE clintbalance is actually total fees
            upd_txt = "insert into PaymentsOnArrangementsWeekly values(" & _
                  paymentID & ",'" & Format(arrStartDate, "yyyy-MM-dd") & "'," & agentID & "," & TotalFees & "," & frequency & ")"
            update_sql(upd_txt)
        Next

    End Sub
End Class

﻿Imports System.IO
Imports System.Text

Public Class frmMain
    Private PaymentData As New clsPaymentData

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        dtpModifiedDate.Value = DateAdd(DateInterval.Day, -1, Now)

    End Sub

    Private Sub cmdCreateFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCreateFile.Click
        Try
            Dim WarningCases As New ArrayList
            Dim Line As String = "", Filename As String, Filedate As String, ProductCode As String = ""
            Dim FolderBrowserDialog As New FolderBrowserDialog
            Dim FileCount As Integer = 0

            PaymentData.GetUpdates(dtpModifiedDate.Value)

            Filename = "RossendalesCasePayments_"
            Filedate = "_" & DateTime.Today.ToString("yyyyMMdd") & ".txt"

            If FolderBrowserDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then

                For Each DataRow As DataRow In PaymentData.PaymentDataView.Table.Rows
                    If ProductCode <> "" And ProductCode <> DataRow.Item("defaultCourtCode") Then
                        WriteFile(FolderBrowserDialog.SelectedPath & "\" & Filename & ProductCode & Filedate, Line)
                        FileCount += 1
                        Line = ""
                    End If

                    If Line <> "" Then Line &= vbCrLf ' Do this at the start of the loop as the last line must not have a CrLf after it

                    For Each DataColumn As DataColumn In DataRow.Table.Columns
                        If Not {"defaultCourtCode", "PaymentID"}.Contains(DataColumn.ColumnName) Then ' defaultCourtCode is only to be used as part of the filename and PaymentID is for logging
                            If Not IsDBNull(DataRow(DataColumn)) Then Line &= UCase(DataRow(DataColumn).ToString)
                        
                            ' If the column is not the last add a pipe. Need to ensure that any ignored columns are not at the end, otherwise this will add a pipe at the end
                            If DataColumn.Ordinal < DataRow.ItemArray.Count - 1 Then Line &= "|"
                        End If
                    Next DataColumn

                    ProductCode = DataRow.Item("defaultCourtCode")

                    If PaymentData.OutputExists(DataRow.Item("DebtorID").ToString, DataRow.Item("NoteID").ToString) Then
                        If Not WarningCases.Contains(DataRow.Item("DebtorID").ToString) Then WarningCases.Add(DataRow.Item("DebtorID").ToString) ' Check so we don't load duplicates
                    End If

                    PaymentData.LogOutput(DataRow.Item("DebtorID").ToString, DataRow.Item("PaymentID").ToString)
                Next DataRow

                If Line <> "" Then
                    WriteFile(FolderBrowserDialog.SelectedPath & "\" & Filename & ProductCode & Filedate, Line)
                    FileCount += 1
                End If

                If WarningCases.Count > 0 Then MsgBox("The following case" & If(WarningCases.Count = 1, "", "s") & " have already been exported:" & vbCrLf & String.Join(vbCrLf, WarningCases.ToArray(GetType(String))), vbOKOnly + vbExclamation, Me.Text)

                MsgBox(FileCount.ToString & " file" & If(FileCount = 1, "", "s") & " saved", vbOKOnly + vbInformation, Me.Text)
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Line As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Line)
        End Using
    End Sub

End Class

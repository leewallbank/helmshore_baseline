﻿Imports CommonLibrary

Public Class clsPaymentData

    Private Payment As New DataTable
    Private PaymentDV As DataView

    Public ReadOnly Property PaymentDataView() As DataView
        Get
            PaymentDataView = PaymentDV
        End Get
    End Property

    Public Sub GetUpdates(ByVal ModDate As Date)
        Dim Sql As String

        Try
            ' Load note changes
            Sql = "SELECT cs.defaultCourtCode " & _
                  "     , p._rowID AS PaymentID " & _
                  "     , p.debtorID " & _
                  "     , p.date " & _
                  "     , p.amount " & _
                  "FROM Payment AS p " & _
                  "INNER JOIN Debtor AS d ON p.debtorID = d._rowid " & _
                  "INNER JOIN ClientScheme AS cs ON d.clientschemeID = cs._rowid " & _
                  "WHERE p.date >= '" & ModDate.ToString("yyyy-MM-dd") & "'" & _
                  "  AND p.status = 'R' " & _
                  "  AND p.clientSchemeID IN (3162, 3163, 3164, 3167, 3168, 3174, 3175, 3176, 3177, 3178) " & _
                  "  AND p.amount_sourceID <> 85 -- CCI Legal"

            LoadDataTable("DebtRecovery", Sql, Payment, False)

            PaymentDV = New DataView(Payment)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub LogOutput(ByVal DebtorID As String, ByVal PaymentID As String)
        Dim Sql As String

        Try
            Sql = "EXEC dbo.LogCCIOutput " & DebtorID & ", 'P', " & PaymentID

            ExecStoredProc("StudentLoans", Sql)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Function OutputExists(ByVal DebtorID As String, ByVal NoteID As String) As Boolean
        OutputExists = GetSQLResults("StudentLoans", "EXEC dbo.CCIOutputExists " & DebtorID & ", 'N', '" & NoteID & "'")
    End Function
End Class

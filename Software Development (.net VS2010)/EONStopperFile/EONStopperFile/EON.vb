﻿Imports System.Xml.Schema
Imports System.IO
Imports commonlibrary

Public Class EON
    Dim ascii As New System.Text.ASCIIEncoding()
    Dim new_file As String = ""
    Dim CSID As Integer
    Dim prod_run As Boolean = False
    Dim filename As String
    Dim lastStopperDate As Date

    Private Sub Form1_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Dim env_str As String = ""
        prod_run = False
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        If env_str = "Prod" Then
            prod_run = True
        Else
            MsgBox("test run only")
        End If
        'get latest date from eon_stopper_file

        Try
            lastStopperDate = GetSQLResults("FeesSQL", "select eon_stopper_date from eon_stopper_file" & _
                                            " where eon_stopper_seqID = (select max(eon_stopper_seqID) from eon_stopper_file)")
        Catch ex As Exception
            lastStopperDate = CDate("Jan 1, 2017")
        End Try
        rundtpdate.Value = lastStopperDate
        rundtptime.Value = lastStopperDate
    End Sub
   
    Private Sub exitbtn_Click(sender As System.Object, e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub processbtn_Click(sender As System.Object, e As System.EventArgs) Handles processbtn.Click
        Dim changeDate As Date = CDate("Jun 5 2017, 10:05:00")
        Dim changeDate2 As Date = CDate("Jun 12 2017, 10:05:00")
        Dim newStopperDate As Date = Now
        lastStopperDate = CDate(Format(rundtpdate.Value, "yyyy-MM-dd") & " " & Format(rundtptime.Value, "HH:mm:ss"))
        'need to go back a further 4 days for Successful cases
        Dim succLastStopperDate As Date = DateAdd(DateInterval.Day, -4, lastStopperDate)

        'get all cases cancelled within dates
        Dim debt_dt As New DataTable
        Dim recordCount As Integer
        Dim totalBalance As Decimal = 0
        Dim outFile As String = ""
        LoadDataTable("DebtRecovery", " select client_ref, offence_number, return_codeID, D._rowID, CS._rowID, return_date, D.status from debtor D, clientscheme CS" & _
                      " where D.clientschemeID = CS._rowID" & _
                      " and CS.clientID = 1938" & _
                      " and D.status in ('C','S')" & _
                      " and return_date > '" & Format(succLastStopperDate, "yyyy-MM-dd HH:mm:ss") & "'" & _
                      " and return_date < '" & Format(newStopperDate, "yyyy-MM-dd HH:mm:ss") & "'", debt_dt, False)

        '" and LD.ListID = 201577 " & _
        '" and LD.objectRowID = D._rowID", debt_dt, False)

        'ONLY select Successful cases that are 4 days after remit to allow time for payment to be notified


        For Each debtRow In debt_dt.Rows
            Dim debtorID As Integer = debtRow(3)
            Dim CSID As Integer = debtRow(4)
            Dim clientRef As String = debtRow(0)

            Dim offenceNumber As String = debtRow(1)
            Dim CaseStatus As String = debtRow(6)
            Dim returnCodeID As Integer = 0
            Dim returnDate As Date = debtRow(5)
            If CaseStatus = "C" Then
                returnCodeID = debtRow(2)
                'if settlement (30 and 80) ignore if less than 4 days since return date
                If returnCodeID = 30 Or returnCodeID = 80 Then
                    If DateDiff(DateInterval.Day, returnDate, newStopperDate) < 4 Then
                        Continue For
                    End If
                    'start date time is 12.6.17 10:05:00 = change date
                    If Format(returnDate, "yyyy-MM-dd HH:mm:ss") < Format(changeDate2, "yyyy-MM-dd HH:mm:ss") Then
                        Continue For
                    End If
                End If
            End If

            If CaseStatus = "S" Then
                'ignore if less than 4 days since return date
                If DateDiff(DateInterval.Day, returnDate, newStopperDate) < 4 Then
                    Continue For
                End If
                'start date time is 5.6.17 10:05:00 = change date
                If Format(returnDate, "yyyy-MM-dd HH:mm:ss") < Format(changeDate, "yyyy-MM-dd HH:mm:ss") Then
                    Continue For
                End If
            Else
                'cancelled cases
                'ignore if before lastStopperDate
                If Format(returnDate, "yyyy-MM-dd HH:mm:ss") < Format(lastStopperDate, "yyyy-MM-dd HH:mm;ss") Then
                    Continue For
                End If
            End If
            recordCount += 1
            'get client balance
            Dim clientbalance As Decimal
            Dim clbalArray As Object()
            clbalArray = GetSQLResultsArray("DebtRecovery", "select sum(fee_amount-remited_fee) from fee" & _
                                     " where fee_remit_col=1" & _
                                    " and DebtorID = " & debtorID)
            Try
                clientbalance = clbalArray(0)
            Catch ex As Exception
                clientbalance = 0
            End Try

            totalBalance += clientbalance
            Dim closureCode As String = ""
            If CaseStatus = "S" Or CaseStatus = "F" Then
                closureCode = "PIF"
            Else
                Try
                    closureCode = GetSQLResults("Debtrecovery", "select overrideShort from clientschemereturn " & _
                                              " where clientschemeID = " & CSID & _
                                              " and returnID = " & returnCodeID)
                Catch ex As Exception

                End Try
            End If
            outFile &= offenceNumber & "," & clientRef & "," & clientbalance & "," & Format(returnDate, "dd/MM/yyyy") & "," & closureCode & vbNewLine
        Next

        'do header and trailer
        Dim fileName As String = "CLO-EON-ROS-" & Format(Now, "ddMMyyyy") & ".csv"
        Dim headerRecord As String = "CLO,EON," & Format(Now, "dd/MM/yyyy") & "," & fileName & "," & totalBalance & "," & recordCount & vbNewLine
        outFile = headerRecord & outFile

        Dim trailerRecord As String = "ROS"
        outFile &= trailerRecord

        'update stopper date
        If prod_run Then
            Dim upd_txt As String = "insert into eon_stopper_file (eon_stopper_date) values ('" & Format(newStopperDate, "yyyy-MM-dd HH:mm:ss") & "')"

            update_sql(upd_txt)
        End If

        'write out file
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "CSV files |*.csv"
            .DefaultExt = ".csv"
            .OverwritePrompt = True
            .FileName = fileName
        End With
        If Not SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            MsgBox("No file name selected for save ")
            Return
        End If
        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outFile, False, ascii)
        MsgBox("report saved")
        Me.Close()
    End Sub
    Private Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, Optional ByVal NoteLength As Integer = 250) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 2) ' + 2 is for :;
        Dim PreviousLength As Integer
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = (NoteTag & ":" & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)) & ";").PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function
    Private Function getCaseID(ByVal clientRef As String) As Integer
        Dim debtorID As Integer = 0
        debtorID = GetSQLResults("DebtRecovery", "SELECT D._rowID " & _
                                                   "FROM debtor D, clientscheme CS  " & _
                                                   "WHERE client_ref = '" & clientRef & "'" & _
                                                   " AND D.clientschemeID = CS._rowID " & _
                                                   " AND CS.clientID = 1662")

        Return debtorID
    End Function
End Class

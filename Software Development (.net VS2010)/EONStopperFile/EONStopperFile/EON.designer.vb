﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EON
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.processbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.rundtpdate = New System.Windows.Forms.DateTimePicker()
        Me.rundtptime = New System.Windows.Forms.DateTimePicker()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.SuspendLayout()
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 208)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(152, 23)
        Me.ProgressBar1.TabIndex = 0
        '
        'processbtn
        '
        Me.processbtn.Location = New System.Drawing.Point(57, 137)
        Me.processbtn.Name = "processbtn"
        Me.processbtn.Size = New System.Drawing.Size(130, 23)
        Me.processbtn.TabIndex = 1
        Me.processbtn.Text = "Run Stopper File"
        Me.processbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(197, 208)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(68, 57)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(123, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Last Run Date and Time"
        '
        'rundtpdate
        '
        Me.rundtpdate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.rundtpdate.Location = New System.Drawing.Point(12, 73)
        Me.rundtpdate.Name = "rundtpdate"
        Me.rundtpdate.Size = New System.Drawing.Size(102, 20)
        Me.rundtpdate.TabIndex = 4
        '
        'rundtptime
        '
        Me.rundtptime.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.rundtptime.Location = New System.Drawing.Point(142, 73)
        Me.rundtptime.Name = "rundtptime"
        Me.rundtptime.Size = New System.Drawing.Size(102, 20)
        Me.rundtptime.TabIndex = 5
        '
        'EON
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 262)
        Me.Controls.Add(Me.rundtptime)
        Me.Controls.Add(Me.rundtpdate)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.processbtn)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Name = "EON"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "EON Stopper File"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents processbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents rundtpdate As System.Windows.Forms.DateTimePicker
    Friend WithEvents rundtptime As System.Windows.Forms.DateTimePicker
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog

End Class

﻿Imports CommonLibrary
Imports System.IO
'test client 24 changed to 1736 JEB 9.4.2013
Public Class frmMain
    Private TDXHMRCData As New clsTDXHMRCData
    Private InputFilePath As String, FileName As String, FileExt As String
    Private ClientID As String() = {"1883", "1736"}

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        '     Try

        If cboClient.SelectedIndex = -1 Then
            MessageBox.Show("Please select a client.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return
        End If

        Dim FileDialog As New OpenFileDialog
        Dim LineNumber As Integer, DebtNumber As Integer, DebtColsStart As Integer
        Dim OutputFile As String
        Dim AuditLog As String, ErrorLog As String = "", InputLineArray() As String, DebtNotes As String, DebtorID As String
        Dim CaseBalance As Decimal = 0, TotalBalance As Decimal = 0, LoadedCaseBalance As Decimal = 0, ParseValueDecimal As Decimal
        Dim BalanceDiscrepancies As New ArrayList

        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)

        lblReadingFile.Visible = True
        Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
        lblReadingFile.Visible = False

        ProgressBar.Maximum = UBound(FileContents)

        For Each OldFile As String In Directory.GetFiles(InputFilePath, FileName & "_*.txt")
            File.Delete(OldFile)
        Next OldFile

        For Each InputLine As String In FileContents
            ProgressBar.Value = LineNumber
            LineNumber += 1
            If LineNumber = 1 Or LineNumber = UBound(FileContents) + 1 Then Continue For ' skip header and trailer line

            CaseBalance = 0
            DebtNotes = ""
            OutputFile = "" ' added TS 14/May/2013
            InputLineArray = InputLine.Split(",")

            If UBound(InputLineArray) <> 255 Then ' changed from 219 TS 01/Aug/2014 27566
                ErrorLog &= "Unexpected line length of " & (UBound(InputLineArray) + 1).ToString & " items found at line number " & LineNumber.ToString & ". Line not loaded." & vbCrLf
                Continue For
            End If

            Application.DoEvents() ' without this line, the button disappears until processing is complete
            'For DebtNumber = 1 To (UBound(InputLineArray) - 6) / 5 ' changed from 4 TS 01/Aug/2014 27566 Commented out TS 10/Nov/2014 Request ref 34781
            For DebtNumber = 1 To InputLineArray(5)
                DebtColsStart = 6 + ((DebtNumber - 1) * 5) ' changed from 4 TS 01/Aug/2014 27566

                'If InputLineArray(DebtColsStart) = "" Then Exit For

                If InputLineArray(DebtColsStart + 2).Replace("""", "") <> "" And Decimal.TryParse(InputLineArray(DebtColsStart + 2), ParseValueDecimal) Then
                    CaseBalance += CDec(InputLineArray(DebtColsStart + 2).Replace("""", ""))
                Else
                    ErrorLog &= "Missing work item value for work item " & DebtNumber.ToString & "  at line number " & LineNumber.ToString & vbCrLf
                End If

                DebtNotes &= String.Join("", ToNote(InputLineArray(DebtColsStart).Replace("""", ""), "Tax Year Due " & DebtNumber.ToString.PadLeft(2, "0"), ";"))
                DebtNotes &= String.Join("", ToNote(InputLineArray(DebtColsStart + 1).Replace("""", ""), "Latest Due Date " & DebtNumber.ToString.PadLeft(2, "0"), ";"))
                DebtNotes &= String.Join("", ToNote(InputLineArray(DebtColsStart + 2).Replace("""", ""), "Collectable Amount of Work Item " & DebtNumber.ToString.PadLeft(2, "0"), ";"))
                DebtNotes &= String.Join("", ToNote(InputLineArray(DebtColsStart + 3).Replace("""", ""), "Debtor 1 Payment Ref " & DebtNumber.ToString.PadLeft(2, "0"), ";"))
                DebtNotes &= String.Join("", ToNote(InputLineArray(DebtColsStart + 4).Replace("""", ""), "Debtor 2 Payment Ref " & DebtNumber.ToString.PadLeft(2, "0"), ";"))

            Next DebtNumber

            If DebtNumber - 1 <> InputLineArray(5).Replace("""", "") Then ErrorLog &= "Expecting " & InputLineArray(5).Replace("""", "") & " debt items, found " & (DebtNumber - 1).ToString & " at line number " & LineNumber.ToString & vbCrLf

            DebtorID = ""

            Dim DebtorDetail As Object() = TDXHMRCData.GetDebtorID(InputLineArray(1).Replace("""", ""), ClientID(cboClient.SelectedIndex))

            If DebtorDetail.Length > 0 Then DebtorID = TDXHMRCData.GetDebtorID(InputLineArray(1).Replace("""", ""), ClientID(cboClient.SelectedIndex))(0)

            If DebtorID <> "" Then

                If DebtorDetail(1) <> 4667 Then

                    LoadedCaseBalance = TDXHMRCData.GetCaseBalance(DebtorID)
                    OutputFile &= DebtorID & "|"

                    If InputLineArray(2) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(2).Replace("""", ""), "Client name", ";"))
                    If InputLineArray(3) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(3).Replace("""", ""), "Debt type", ";"))
                    If InputLineArray(4) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(4).Replace("""", ""), "Tranche ID", ";")) ' Corrected to InputLineArray(4) from 5. Request ref 30031
                    If InputLineArray(5) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(5).Replace("""", ""), "Total number of debts in new business file", ";"))

                    OutputFile &= DebtNotes & vbCrLf
                    TotalBalance += CaseBalance

                    If LoadedCaseBalance <> CaseBalance Then
                        BalanceDiscrepancies.Add({DebtorID, LoadedCaseBalance.ToString, CaseBalance.ToString})
                    End If

                    AppendToFile(InputFilePath & FileName & "_PreProcessed.txt", OutputFile) ' added TS 14/May/2013

                Else

                    TDXHMRCData.GetHHBDDebtorID(InputLineArray(1).Replace("""", ""), ClientID(cboClient.SelectedIndex))

                    LoadedCaseBalance = 0

                    For Each Debtor As DataRow In TDXHMRCData.Debtor.Rows

                        LoadedCaseBalance += TDXHMRCData.GetCaseBalance(Debtor.Item(0).ToString)
                        OutputFile = Debtor.Item(0).ToString & "|"

                        If InputLineArray(2) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(2).Replace("""", ""), "Client name", ";"))
                        If InputLineArray(3) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(3).Replace("""", ""), "Debt type", ";"))
                        If InputLineArray(4) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(4).Replace("""", ""), "Tranche ID", ";")) ' Corrected to InputLineArray(4) from 5. Request ref 30031
                        If InputLineArray(5) <> "" Then OutputFile &= String.Join("", ToNote(InputLineArray(5).Replace("""", ""), "Total number of debts in new business file", ";"))

                        OutputFile &= DebtNotes & vbCrLf

                        AppendToFile(InputFilePath & FileName & "_PreProcessed.txt", OutputFile)

                    Next Debtor

                    TotalBalance += CaseBalance

                    If LoadedCaseBalance <> CaseBalance Then
                        BalanceDiscrepancies.Add({InputLineArray(1).Replace("""", ""), LoadedCaseBalance.ToString, CaseBalance.ToString})
                    End If
                End If

            Else
                ErrorLog &= "Cannot find open DebtorID for client ref " & InputLineArray(1).Replace("""", "") & " at line number " & LineNumber.ToString & vbCrLf
            End If



        Next InputLine

        InputLineArray = FileContents(0).Split(",") ' Check header record case count
        If LineNumber - 2 <> InputLineArray(3).Replace("""", "") Then ErrorLog &= "Expecting " & InputLineArray(3) & " cases, found " & (LineNumber - 2).ToString

        ' WriteFile(InputFilePath & FileName & "_PreProcessed.txt", OutputFile) TS 14/May/2013

        AuditLog = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
        AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
        AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

        AuditLog &= "Number of new cases: " & (UBound(FileContents) - 1).ToString & vbCrLf
        AuditLog &= "Total balance of new cases: " & TotalBalance.ToString

        WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

        If Not BalanceDiscrepancies.Count = 0 Then OutputToExcel(InputFilePath, FileName & "_Balance_discrepancies", "Discrepancies", {"DebtorID", "Loaded balance", "Balance in supplementary file"}, BalanceDiscrepancies)

        If ErrorLog <> "" Then
            WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
            MessageBox.Show("Errors found.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

        If File.Exists(InputFilePath & FileName & "_Balance_discrepancies.xls") Then MessageBox.Show("Balance discrepancies found.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
        MessageBox.Show("Pre-processing complete.", Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)

        btnViewInputFile.Enabled = True
        btnViewOutputFile.Enabled = True
        btnViewLogFile.Enabled = True

        ProgressBar.Value = 0

        'Catch ex As Exception
        '    HandleException(ex)
        'End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
            If File.Exists(InputFilePath & FileName & "_Balance_discrepancies.xls") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Balance_discrepancies.xls")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class

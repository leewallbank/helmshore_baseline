﻿Imports CommonLibrary

Public Class clsScheduleData
    Private Const JobName As String = "Refresh LSC Reporting"
    Private Const ScheduleName As String = "LSC Reporting - ad hoc"
    Private HistoryDT As New DataTable
    Private HistoryDV As New DataView

#Region "Public Properties"
    Public ReadOnly Property HistoryDataView() As DataView
        Get
            HistoryDataView = HistoryDV
        End Get
    End Property
#End Region

    Public Function JobStatus() As Integer
        JobStatus = GetSQLResultsArray("LSCReporting", "EXEC msdb.dbo.sp_help_job @job_name = '" & JobName & "', @job_aspect = 'JOB'")(25)
    End Function

#Region "Public Methods"

    Public Sub GetHistory()
        LoadDataTable("LSCReporting", "SELECT CONVERT(DATETIME, RIGHT(t.run_date,2) + '/' + SUBSTRING(t.run_date,5,2) + '/' + LEFT(t.run_date,4) " & _
                                      "       + ' ' + LEFT(t.run_time,2) + ':' + SUBSTRING(t.run_time,3,2) + ':' + RIGHT(t.run_time,2), 103) AS StartDateTime " & _
                                      "     , LEFT(t.message, CHARINDEX('.', t.message)-1) AS RunStatus  " & _
                                      "     , LEFT(t.run_duration,2) + ':' + SUBSTRING(t.run_duration,3,2) + ':' + RIGHT(t.run_duration,2) AS Duration " & _
                                      "FROM ( " & _
                                      "       SELECT CAST(h.run_date AS VARCHAR) AS run_date " & _
                                      "            , RIGHT('0' + CAST(h.run_time AS VARCHAR), 6) AS run_time " & _
                                      "            , h.message  " & _
                                      "            , RIGHT('00000' + CAST(h.run_duration AS VARCHAR), 6) AS run_duration " & _
                                      "       FROM msdb.dbo.sysjobs AS j " & _
                                      "       INNER JOIN msdb.dbo.sysjobhistory AS h ON j.job_id = h.job_id " & _
                                      "       WHERE j.name = '" & JobName & "' " & _
                                      "         AND h.step_id = 0 " & _
                                      "     ) AS t", HistoryDT)

        HistoryDV = New DataView(HistoryDT)
        HistoryDV.AllowDelete = False
        HistoryDV.AllowNew = False
    End Sub

    Public Function GetNextRunDate() As String
        GetNextRunDate = GetSQLResults("LSCReporting", "SELECT CONVERT(DATETIME, RIGHT(t.next_run_date,2) + '/' + SUBSTRING(t.next_run_date,5,2) + '/' + LEFT(t.next_run_date,4) " & _
                                                       "       + ' ' + LEFT(t.next_run_time,2) + ':' + SUBSTRING(t.next_run_time,3,2) + ':' + RIGHT(t.next_run_time,2), 103) AS NextDateTime " & _
                                                       "FROM ( " & _
                                                       "       SELECT CAST(s.active_start_date AS VARCHAR) AS next_run_date " & _
                                                       "            , RIGHT('0' + CAST(s.active_start_time AS VARCHAR), 6) AS next_run_time " & _
                                                       "       FROM msdb.dbo.sysschedules AS s " & _
                                                       "       WHERE s.name = '" & ScheduleName & "' " & _
                                                       "     ) AS t")
    End Function

    Public Sub Reschedule(RunDateTime As DateTime)
        ExecStoredProc("LSCReporting", "EXEC msdb.dbo.sp_update_schedule @name = '" & ScheduleName & "', @enabled = 1, @active_start_date = " & RunDateTime.ToString("yyyyMMdd") & ", @active_start_time = " & RunDateTime.ToString("HHmmss") & ";")
    End Sub

#End Region


End Class

﻿Public Class frmMain

    Private ScheduleData As New clsScheduleData

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        RefreshStatus()

        dgvHistory.DataSource = ScheduleData.HistoryDataView
        dgvHistory.Sort(dgvHistory.Columns(0), System.ComponentModel.ListSortDirection.Descending)

        ' Set the default next date to the nearest multiple of five minutes
        dtpRunDate.Value = Now.Date.Add(TimeSpan.FromMinutes(Math.Floor((Now.TimeOfDay.TotalMinutes + 5) / 5) * 5))

    End Sub

    Private Sub tmrRefresh_Tick(sender As Object, e As System.EventArgs) Handles tmrRefresh.Tick
        RefreshStatus()
    End Sub

    Private Sub btnReSchedule_Click(sender As Object, e As System.EventArgs) Handles btnReSchedule.Click
        ScheduleData.Reschedule(dtpRunDate.Value)
        GetNextRunDate()
    End Sub

    Private Sub GetNextRunDate()
        Dim NextRunDate As DateTime = ScheduleData.GetNextRunDate

        If NextRunDate < DateTime.Now Then
            lblCurrentSchedule.Text = "Last run was " & NextRunDate.ToString
        Else
            lblCurrentSchedule.Text = "Next run is " & NextRunDate.ToString
        End If

    End Sub

    Private Sub RefreshStatus()
        ScheduleData.GetHistory()
        GetNextRunDate()

        If ScheduleData.JobStatus <> 4 Then
            lblCurrentStatus.Text = "Data warehouse refresh is running"
        Else
            lblCurrentStatus.Text = "Data warehouse refresh is idle"
        End If
    End Sub

    Private Sub dgvHistory_SelectionChanged(sender As Object, e As System.EventArgs) Handles dgvHistory.SelectionChanged
        ' stop any cells being shown as selected
        If Not dgvHistory.CurrentCell Is Nothing Then dgvHistory.CurrentCell = Nothing
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class


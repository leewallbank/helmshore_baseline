﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.dgvHistory = New System.Windows.Forms.DataGridView()
        Me.colStartDateTime = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colDuration = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colRunStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.tmrRefresh = New System.Windows.Forms.Timer(Me.components)
        Me.btnReSchedule = New System.Windows.Forms.Button()
        Me.dtpRunDate = New System.Windows.Forms.DateTimePicker()
        Me.lblCurrentStatus = New System.Windows.Forms.Label()
        Me.lblCurrentSchedule = New System.Windows.Forms.Label()
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvHistory
        '
        Me.dgvHistory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvHistory.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colStartDateTime, Me.colDuration, Me.colRunStatus})
        Me.dgvHistory.Location = New System.Drawing.Point(3, 3)
        Me.dgvHistory.Name = "dgvHistory"
        Me.dgvHistory.ReadOnly = True
        Me.dgvHistory.RowHeadersVisible = False
        Me.dgvHistory.Size = New System.Drawing.Size(278, 164)
        Me.dgvHistory.TabIndex = 0
        Me.dgvHistory.TabStop = False
        '
        'colStartDateTime
        '
        Me.colStartDateTime.DataPropertyName = "StartDateTime"
        Me.colStartDateTime.HeaderText = "Start"
        Me.colStartDateTime.Name = "colStartDateTime"
        Me.colStartDateTime.ReadOnly = True
        '
        'colDuration
        '
        Me.colDuration.DataPropertyName = "Duration"
        Me.colDuration.HeaderText = "Duration"
        Me.colDuration.Name = "colDuration"
        Me.colDuration.ReadOnly = True
        Me.colDuration.Width = 60
        '
        'colRunStatus
        '
        Me.colRunStatus.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.colRunStatus.DataPropertyName = "RunStatus"
        Me.colRunStatus.HeaderText = "Message"
        Me.colRunStatus.Name = "colRunStatus"
        Me.colRunStatus.ReadOnly = True
        '
        'tmrRefresh
        '
        Me.tmrRefresh.Enabled = True
        Me.tmrRefresh.Interval = 10000
        '
        'btnReSchedule
        '
        Me.btnReSchedule.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.btnReSchedule.Location = New System.Drawing.Point(167, 232)
        Me.btnReSchedule.Name = "btnReSchedule"
        Me.btnReSchedule.Size = New System.Drawing.Size(76, 25)
        Me.btnReSchedule.TabIndex = 1
        Me.btnReSchedule.Text = "Reschedule"
        Me.btnReSchedule.UseVisualStyleBackColor = True
        '
        'dtpRunDate
        '
        Me.dtpRunDate.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.dtpRunDate.CustomFormat = "dd/MMM/yy HH:mm"
        Me.dtpRunDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpRunDate.Location = New System.Drawing.Point(42, 234)
        Me.dtpRunDate.Name = "dtpRunDate"
        Me.dtpRunDate.Size = New System.Drawing.Size(119, 20)
        Me.dtpRunDate.TabIndex = 0
        '
        'lblCurrentStatus
        '
        Me.lblCurrentStatus.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCurrentStatus.Location = New System.Drawing.Point(13, 177)
        Me.lblCurrentStatus.Name = "lblCurrentStatus"
        Me.lblCurrentStatus.Size = New System.Drawing.Size(260, 15)
        Me.lblCurrentStatus.TabIndex = 4
        Me.lblCurrentStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCurrentSchedule
        '
        Me.lblCurrentSchedule.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblCurrentSchedule.Location = New System.Drawing.Point(12, 201)
        Me.lblCurrentSchedule.Name = "lblCurrentSchedule"
        Me.lblCurrentSchedule.Size = New System.Drawing.Size(260, 15)
        Me.lblCurrentSchedule.TabIndex = 5
        Me.lblCurrentSchedule.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 264)
        Me.Controls.Add(Me.lblCurrentSchedule)
        Me.Controls.Add(Me.lblCurrentStatus)
        Me.Controls.Add(Me.dtpRunDate)
        Me.Controls.Add(Me.btnReSchedule)
        Me.Controls.Add(Me.dgvHistory)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Data warehouse refresh scheduler"
        CType(Me.dgvHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvHistory As System.Windows.Forms.DataGridView
    Friend WithEvents tmrRefresh As System.Windows.Forms.Timer
    Friend WithEvents colStartDateTime As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDuration As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRunStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnReSchedule As System.Windows.Forms.Button
    Friend WithEvents dtpRunDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblCurrentStatus As System.Windows.Forms.Label
    Friend WithEvents lblCurrentSchedule As System.Windows.Forms.Label

End Class

﻿Imports CommonLibrary
Imports System.IO

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try

            Dim FileDialog As New OpenFileDialog
            Dim InputLineArray() As String
            Dim LineNumber As Integer
            Dim OutputFile As String
            Dim FromDate As Date, ToDate As Date

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents)

            OutputFile = FileContents(0) & vbCrLf & _
                         FileContents(1) & vbCrLf & _
                         FileContents(2) & vbCrLf

            For LineNumber = 3 To UBound(FileContents) - 2
                ProgressBar.Value = LineNumber

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                InputLineArray = Split(FileContents(LineNumber), ",", """")

                DateTime.TryParseExact(InputLineArray(37).Replace("""", ""), "yyyy-MM-dd", Nothing, Globalization.DateTimeStyles.None, FromDate)
                DateTime.TryParseExact(InputLineArray(33).Replace("""", ""), "yyyy-MM-dd", Nothing, Globalization.DateTimeStyles.None, ToDate)

                If FromDate > ToDate Then MsgBox("Invalid offence date for client ref " & InputLineArray(12)) 'InputLineArray(33) = CDate(InputLineArray(33)).AddYears(1).ToString()

                OutputFile &= String.Join(",", InputLineArray) & ",""" & InputLineArray(45).Replace("""", "") & "/" & InputLineArray(43).Replace("""", "").PadLeft(4, "0") & """" & vbCrLf

            Next LineNumber

            OutputFile &= FileContents(UBound(FileContents) - 1) & vbCrLf & _
                          FileContents(UBound(FileContents)) & vbCrLf

            WriteFile(InputFilePath & FileName & "_PreProcessed" & FileExt, OutputFile)

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed" & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function Split(ByVal Expression As String, ByVal Delimiter As String, ByVal Qualifier As String) As String()
        Dim ReturnValues() As String = Nothing
        Try

            Dim QualifierState As Boolean = False
            Dim StartIndex As Integer = 0
            Dim Values As New System.Collections.ArrayList

            For CharIndex As Integer = 0 To Expression.Length - 1
                If Not Qualifier Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Qualifier.Length), Qualifier) = 0 Then
                    QualifierState = Not QualifierState
                ElseIf Not QualifierState AndAlso Not Delimiter Is Nothing AndAlso String.Compare(Expression.Substring(CharIndex, Delimiter.Length), Delimiter) = 0 Then
                    Values.Add(Expression.Substring(StartIndex, CharIndex - StartIndex))
                    StartIndex = CharIndex + 1
                End If
            Next CharIndex

            If StartIndex < Expression.Length Then
                Values.Add(Expression.Substring(StartIndex, Expression.Length - StartIndex))
            End If

            ReDim ReturnValues(Values.Count - 1)
            Values.CopyTo(ReturnValues)


        Catch ex As Exception
            HandleException(ex)
        End Try

        Return ReturnValues
    End Function
End Class

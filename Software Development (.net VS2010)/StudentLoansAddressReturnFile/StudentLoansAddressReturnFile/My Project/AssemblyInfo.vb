﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Student Loans Address Return File")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Rossendales")> 
<Assembly: AssemblyProduct("StudentLoansAddressReturnFile")> 
<Assembly: AssemblyCopyright("Copyright © Rossendales 2015")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("2c61a4db-79b5-495c-91c9-f6975afe012c")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.2.0.0")> 
<Assembly: AssemblyFileVersion("1.2.0.0")> 

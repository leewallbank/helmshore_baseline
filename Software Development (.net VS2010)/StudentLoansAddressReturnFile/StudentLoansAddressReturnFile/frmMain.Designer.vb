﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.dgvMain = New System.Windows.Forms.DataGridView()
        Me.cmdCreateFile = New System.Windows.Forms.Button()
        Me.cmdValidate = New System.Windows.Forms.Button()
        Me.dtpModifiedDateFrom = New System.Windows.Forms.DateTimePicker()
        Me.lblModifiedDate = New System.Windows.Forms.Label()
        Me.cmdRefresh = New System.Windows.Forms.Button()
        Me.cmsDGV = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.cmdLoadFile = New System.Windows.Forms.Button()
        Me.cboTarget = New System.Windows.Forms.ComboBox()
        Me.lblTarget = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.dtpModifiedDateTo = New System.Windows.Forms.DateTimePicker()
        CType(Me.dgvMain, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvMain
        '
        Me.dgvMain.AllowDrop = True
        Me.dgvMain.AllowUserToAddRows = False
        Me.dgvMain.AllowUserToDeleteRows = False
        Me.dgvMain.AllowUserToResizeRows = False
        Me.dgvMain.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvMain.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvMain.Location = New System.Drawing.Point(6, 8)
        Me.dgvMain.MultiSelect = False
        Me.dgvMain.Name = "dgvMain"
        Me.dgvMain.RowHeadersVisible = False
        Me.dgvMain.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvMain.Size = New System.Drawing.Size(821, 302)
        Me.dgvMain.TabIndex = 0
        '
        'cmdCreateFile
        '
        Me.cmdCreateFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCreateFile.Location = New System.Drawing.Point(725, 325)
        Me.cmdCreateFile.Name = "cmdCreateFile"
        Me.cmdCreateFile.Size = New System.Drawing.Size(101, 30)
        Me.cmdCreateFile.TabIndex = 1
        Me.cmdCreateFile.Text = "Create File"
        Me.cmdCreateFile.UseVisualStyleBackColor = True
        '
        'cmdValidate
        '
        Me.cmdValidate.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.cmdValidate.Location = New System.Drawing.Point(485, 325)
        Me.cmdValidate.Name = "cmdValidate"
        Me.cmdValidate.Size = New System.Drawing.Size(101, 30)
        Me.cmdValidate.TabIndex = 2
        Me.cmdValidate.Text = "Validate"
        Me.cmdValidate.UseVisualStyleBackColor = True
        '
        'dtpModifiedDateFrom
        '
        Me.dtpModifiedDateFrom.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtpModifiedDateFrom.Location = New System.Drawing.Point(93, 316)
        Me.dtpModifiedDateFrom.Name = "dtpModifiedDateFrom"
        Me.dtpModifiedDateFrom.Size = New System.Drawing.Size(126, 20)
        Me.dtpModifiedDateFrom.TabIndex = 3
        '
        'lblModifiedDate
        '
        Me.lblModifiedDate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblModifiedDate.AutoSize = True
        Me.lblModifiedDate.Location = New System.Drawing.Point(12, 320)
        Me.lblModifiedDate.Name = "lblModifiedDate"
        Me.lblModifiedDate.Size = New System.Drawing.Size(73, 13)
        Me.lblModifiedDate.TabIndex = 4
        Me.lblModifiedDate.Text = "Modified from:"
        '
        'cmdRefresh
        '
        Me.cmdRefresh.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cmdRefresh.Location = New System.Drawing.Point(245, 325)
        Me.cmdRefresh.Name = "cmdRefresh"
        Me.cmdRefresh.Size = New System.Drawing.Size(101, 30)
        Me.cmdRefresh.TabIndex = 5
        Me.cmdRefresh.Text = "Refresh"
        Me.cmdRefresh.UseVisualStyleBackColor = True
        '
        'cmsDGV
        '
        Me.cmsDGV.Name = "cmsDGV"
        Me.cmsDGV.Size = New System.Drawing.Size(61, 4)
        '
        'cmdLoadFile
        '
        Me.cmdLoadFile.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.cmdLoadFile.Location = New System.Drawing.Point(365, 325)
        Me.cmdLoadFile.Name = "cmdLoadFile"
        Me.cmdLoadFile.Size = New System.Drawing.Size(101, 30)
        Me.cmdLoadFile.TabIndex = 6
        Me.cmdLoadFile.Text = "Load File"
        Me.cmdLoadFile.UseVisualStyleBackColor = True
        '
        'cboTarget
        '
        Me.cboTarget.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboTarget.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTarget.FormattingEnabled = True
        Me.cboTarget.ItemHeight = 13
        Me.cboTarget.Items.AddRange(New Object() {"SLC", "CCI"})
        Me.cboTarget.Location = New System.Drawing.Point(660, 331)
        Me.cboTarget.Name = "cboTarget"
        Me.cboTarget.Size = New System.Drawing.Size(59, 21)
        Me.cboTarget.TabIndex = 7
        Me.cboTarget.Visible = False
        '
        'lblTarget
        '
        Me.lblTarget.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTarget.AutoSize = True
        Me.lblTarget.Location = New System.Drawing.Point(613, 334)
        Me.lblTarget.Name = "lblTarget"
        Me.lblTarget.Size = New System.Drawing.Size(41, 13)
        Me.lblTarget.TabIndex = 8
        Me.lblTarget.Text = "Target:"
        Me.lblTarget.Visible = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(65, 346)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(19, 13)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "to:"
        '
        'dtpModifiedDateTo
        '
        Me.dtpModifiedDateTo.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dtpModifiedDateTo.Location = New System.Drawing.Point(93, 342)
        Me.dtpModifiedDateTo.Name = "dtpModifiedDateTo"
        Me.dtpModifiedDateTo.Size = New System.Drawing.Size(126, 20)
        Me.dtpModifiedDateTo.TabIndex = 9
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(839, 367)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dtpModifiedDateTo)
        Me.Controls.Add(Me.lblTarget)
        Me.Controls.Add(Me.cboTarget)
        Me.Controls.Add(Me.cmdRefresh)
        Me.Controls.Add(Me.cmdLoadFile)
        Me.Controls.Add(Me.lblModifiedDate)
        Me.Controls.Add(Me.dtpModifiedDateFrom)
        Me.Controls.Add(Me.cmdValidate)
        Me.Controls.Add(Me.cmdCreateFile)
        Me.Controls.Add(Me.dgvMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.Name = "frmMain"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Student Loans Addresses"
        CType(Me.dgvMain, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvMain As System.Windows.Forms.DataGridView
    Friend WithEvents cmdCreateFile As System.Windows.Forms.Button
    Friend WithEvents cmdValidate As System.Windows.Forms.Button
    Friend WithEvents dtpModifiedDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblModifiedDate As System.Windows.Forms.Label
    Friend WithEvents cmdRefresh As System.Windows.Forms.Button
    Friend WithEvents cmsDGV As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents cmdLoadFile As System.Windows.Forms.Button
    Friend WithEvents cboTarget As System.Windows.Forms.ComboBox
    Friend WithEvents lblTarget As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpModifiedDateTo As System.Windows.Forms.DateTimePicker

End Class

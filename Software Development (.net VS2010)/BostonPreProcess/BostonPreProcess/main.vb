Public Class mainform



    Private Sub openbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openbtn.Click
        Dim filetext As String
        Dim fileok As Boolean = True
        filetext = ""
        With OpenFileDialog1
            .Title = "Open file"
            .Filter = "Text files | *.txt"
            .FileName = ""
            .CheckFileExists = True
        End With

        If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            Try
                filename = OpenFileDialog1.FileName
                filetext = My.Computer.FileSystem.ReadAllText(filename)
            Catch ex As Exception
                MsgBox("Unable to read file")
                fileok = False
            End Try
        Else
            fileok = False
        End If
        If fileok = True Then
            TextBox1.Text = "File Opened"
            reformbtn.Enabled = True
        Else
            TextBox1.Text = "File NOT Opened"
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainform_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        reformbtn.Enabled = False
        viewbtn.Enabled = False
        errbtn.Enabled = False
    End Sub

    Private Sub reformbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles reformbtn.Click
        'do manipulation here
        Dim cases As Integer
        Dim tot_debt As Decimal
        Dim file, outline As String
        Dim linetext As String = ""
        Dim line(0) As String
        Dim propaddr As String = ""
        Dim curraddr As String = ""
        Dim name As String = ""
        Dim name2 As String = ""
        Dim lonum As String = ""
        Dim lostring As String = ""
        Dim idx As Integer
        Dim lines As Integer = 0
        Dim debt_amt As Decimal
        Dim lodate, fromdate, todate As Date
        Dim clref As String = ""
        Dim addrline As String = ""
        Dim amtstring As String = ""
        'read file into array
        file = My.Computer.FileSystem.ReadAllText(filename)

        For idx = 1 To Len(file) - 1
            If Mid(file, idx, 2) = vbNewLine Then
                ReDim Preserve line(lines)
                line(lines) = linetext
                linetext = ""
                lines += 1
            Else
                linetext = linetext + Mid(file, idx, 1)
            End If
        Next

        'write out headings
        outline = "Client ref|Name|Name2|DebtAddress|Current Address|LO Number|LO Date|From date|To date|Debt Amount" & vbNewLine
        outfile = outline

        'look for Account Number
        For idx = 0 To lines - 1
            Dim start_idx As Integer = InStr(line(idx), "Account No :")
            If start_idx > 0 Then
                clref = Trim(Microsoft.VisualBasic.Right(line(idx), (line(idx).Length - start_idx - 12)))
                If clref.Length < 1 Then
                    errorfile = errorfile & "Line  " & idx & " - Client Ref is invalid -"
                End If
                cases += 1
                'get summons no
                start_idx = 0
                While start_idx = 0
                    idx += 1
                    start_idx = InStr(line(idx), "Case No:")
                End While
                lonum = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - start_idx - 8))
                'look for Re:
                start_idx = 0
                While start_idx = 0
                    idx += 1
                    start_idx = InStr(line(idx), "Re:")
                End While
                'name is on next line
                idx += 1
                'ignore first 2 chars (0D0A)
                name = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 2))
                'name 2 may be on next line
                idx += 1
                name2 = ""
                Try
                    'ignore first 2 chars (0D0A)
                    name2 = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 2))
                Catch ex As Exception

                End Try
                If LCase(Microsoft.VisualBasic.Left(name2, 2)) = "mr" Or _
                    LCase(Microsoft.VisualBasic.Left(name2, 2)) = "ms" Or _
                    LCase(Microsoft.VisualBasic.Left(name2, 4)) = "miss" Then
                    'see if third name on next line
                    Dim name3 As String = Trim(Microsoft.VisualBasic.Right(line(idx + 1), line(idx + 1).Length - 2))
                    If LCase(Microsoft.VisualBasic.Left(name3, 2)) = "mr" Or _
                   LCase(Microsoft.VisualBasic.Left(name3, 2)) = "ms" Or _
                   LCase(Microsoft.VisualBasic.Left(name3, 4)) = "miss" Then
                        name2 &= ";" & name3
                        idx += 1
                        'current address is on next line
                    End If
                    'current address is on next line
                Else
                    'no second name this is first line of address
                    name2 = ""
                    idx -= 1
                End If
                'current address is on next lines up to liability
                start_idx = 0
                idx += 1
                While start_idx = 0
                    'ignore first 2 chars (0D0A)
                    Dim line_txt As String = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 2))
                    If line_txt.Length > 2 Then
                        If curraddr <> "" Then
                            curraddr &= ","
                        End If
                        curraddr &= line_txt
                    End If
                    idx += 1
                    start_idx = InStr(line(idx), "Liability Order")
                End While

                'get liability order date
                start_idx = 0
                While start_idx = 0
                    idx += 1
                    start_idx = InStr(line(idx), "Liability Order Made")
                End While
                lostring = Trim(Mid(line(idx), start_idx + 20, 15))
                If Not IsDate(lostring) Then
                    errorfile = errorfile & "Line  " & idx & " - LO Date not valid - " _
                                                 & lostring & vbNewLine
                Else
                    lodate = lostring
                End If

                'get debt amount from total
                start_idx = 0
                While start_idx = 0
                    idx += 1
                    start_idx = InStr(line(idx), "Total    ")
                End While
                amtstring = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - start_idx - 6))
                'ignore first character as currency sign
                amtstring = Microsoft.VisualBasic.Right(amtstring, amtstring.Length - 1)
                If Not IsNumeric(amtstring) Then
                    errorfile = errorfile & "Line  " & idx & " - Warrant amount not numeric - " _
                             & amtstring & vbNewLine
                Else
                    debt_amt = amtstring
                End If
                tot_debt += debt_amt
                'get start and end dates of liability
                start_idx = 0
                While start_idx = 0
                    idx += 1
                    start_idx = InStr(line(idx), "Period of Liability")
                End While
                'dates are on next line
                idx += 1
                start_idx = InStr(line(idx), ".")
                Try
                    fromdate = Mid(line(idx), start_idx - 2, 10)
                Catch ex As Exception
                    errorfile = errorfile & "Line  " & idx & " - From date is invalid - " & vbNewLine
                End Try
                Try
                    todate = Mid(line(idx), start_idx + 11, 12)
                Catch ex As Exception
                    errorfile = errorfile & "Line  " & idx & " - To date is invalid - " & vbNewLine
                End Try

                'get debt address
                start_idx = 0
                While start_idx = 0
                    idx += 1
                    start_idx = InStr(line(idx), "Property")
                End While
                'debt address is on next lines up to dashes or "Account No :"
                start_idx = 0
                idx += 1
                While start_idx = 0
                    'ignore first 2 chars (0D0A)
                    Dim line_txt As String
                    Try
                        line_txt = Trim(Microsoft.VisualBasic.Right(line(idx), line(idx).Length - 2))
                    Catch ex As Exception
                        Exit While
                    End Try

                    If line_txt.Length > 2 Then
                        If propaddr <> "" Then
                            propaddr &= ","
                        End If
                        propaddr &= line_txt
                    End If
                    idx += 1
                    start_idx = InStr(line(idx), "-----") + InStr(line(idx), "Account No :")
                End While



                'save case in outline
                outfile = outfile & clref & "|" & name & "|" & name2 & "|" & propaddr & "|" _
                & curraddr & "|" & lonum & "|" & lodate & "|" & _
                 fromdate & "|" & todate & "|" & debt_amt & "|" & vbNewLine
                name = ""
                propaddr = ""
                curraddr = ""
                lonum = ""
                lodate = Nothing
                debt_amt = Nothing
                fromdate = Nothing
                todate = Nothing
            End If
        Next
        
        viewbtn.Enabled = True
        Dim idx4 As Integer
        Dim filename_prefix As String = ""
        For idx4 = Len(filename) To 1 Step -1
            If Mid(filename, idx4, 1) = "." Then
                filename_prefix = Microsoft.VisualBasic.Left(filename, idx4 - 1)
                Exit For
            End If
        Next
        My.Computer.FileSystem.WriteAllText(filename_prefix & "_preprocess.txt", outfile, False)

        'write out error file
        If errorfile <> Nothing Then
            My.Computer.FileSystem.WriteAllText(filename_prefix & "_error.txt", errorfile, False)
            errbtn.Enabled = True
            TextBox1.Text = "Errors found"
        Else
            TextBox1.Text = "No errors found"
            MsgBox("Cases = " & cases & vbNewLine &
              "Total debt = " & tot_debt)
            Me.Close()
        End If
       
    End Sub

    Private Sub viewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles viewbtn.Click
        viewform.ShowDialog()

    End Sub

    Private Sub errbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles errbtn.Click
        errors.Show()
    End Sub
End Class

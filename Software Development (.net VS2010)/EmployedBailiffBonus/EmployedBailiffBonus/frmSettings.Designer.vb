﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSettings))
        Me.dgvTarget = New System.Windows.Forms.DataGridView()
        Me.TBand = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TCompletedCaseLoad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TLevyObtained = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TSignedWalkingPossession = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TAmountCollected = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvBonus = New System.Windows.Forms.DataGridView()
        Me.BBand = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BCompletedCaseLoad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BLevyObtained = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BSignedWalkingPossession = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BAmountCollected = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.cmdSave = New System.Windows.Forms.Button()
        CType(Me.dgvTarget, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvBonus, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvTarget
        '
        Me.dgvTarget.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTarget.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.TBand, Me.TCompletedCaseLoad, Me.TLevyObtained, Me.TSignedWalkingPossession, Me.TAmountCollected})
        Me.dgvTarget.Location = New System.Drawing.Point(12, 23)
        Me.dgvTarget.Name = "dgvTarget"
        Me.dgvTarget.RowHeadersVisible = False
        Me.dgvTarget.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvTarget.Size = New System.Drawing.Size(323, 264)
        Me.dgvTarget.TabIndex = 0
        '
        'TBand
        '
        Me.TBand.DataPropertyName = "Band"
        Me.TBand.HeaderText = "Band"
        Me.TBand.Name = "TBand"
        Me.TBand.Width = 60
        '
        'TCompletedCaseLoad
        '
        Me.TCompletedCaseLoad.DataPropertyName = "CompletedCaseLoad"
        Me.TCompletedCaseLoad.HeaderText = "Completed"
        Me.TCompletedCaseLoad.Name = "TCompletedCaseLoad"
        Me.TCompletedCaseLoad.Width = 60
        '
        'TLevyObtained
        '
        Me.TLevyObtained.DataPropertyName = "LevyObtained"
        Me.TLevyObtained.HeaderText = "Levies"
        Me.TLevyObtained.Name = "TLevyObtained"
        Me.TLevyObtained.Width = 60
        '
        'TSignedWalkingPossession
        '
        Me.TSignedWalkingPossession.DataPropertyName = "SignedWalkingPossession"
        Me.TSignedWalkingPossession.HeaderText = "SWP"
        Me.TSignedWalkingPossession.Name = "TSignedWalkingPossession"
        Me.TSignedWalkingPossession.Width = 60
        '
        'TAmountCollected
        '
        Me.TAmountCollected.DataPropertyName = "AmountCollected"
        Me.TAmountCollected.HeaderText = "Collected"
        Me.TAmountCollected.Name = "TAmountCollected"
        Me.TAmountCollected.Width = 80
        '
        'dgvBonus
        '
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvBonus.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvBonus.ColumnHeadersHeight = 43
        Me.dgvBonus.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.BBand, Me.BCompletedCaseLoad, Me.BLevyObtained, Me.BSignedWalkingPossession, Me.BAmountCollected})
        Me.dgvBonus.Location = New System.Drawing.Point(343, 23)
        Me.dgvBonus.Name = "dgvBonus"
        Me.dgvBonus.RowHeadersVisible = False
        Me.dgvBonus.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvBonus.Size = New System.Drawing.Size(243, 264)
        Me.dgvBonus.TabIndex = 1
        '
        'BBand
        '
        Me.BBand.DataPropertyName = "Band"
        Me.BBand.HeaderText = "Band"
        Me.BBand.Name = "BBand"
        Me.BBand.Visible = False
        Me.BBand.Width = 60
        '
        'BCompletedCaseLoad
        '
        Me.BCompletedCaseLoad.DataPropertyName = "CompletedCaseLoad"
        Me.BCompletedCaseLoad.HeaderText = "Completed"
        Me.BCompletedCaseLoad.Name = "BCompletedCaseLoad"
        Me.BCompletedCaseLoad.Width = 60
        '
        'BLevyObtained
        '
        Me.BLevyObtained.DataPropertyName = "LevyObtained"
        Me.BLevyObtained.HeaderText = "Levies"
        Me.BLevyObtained.Name = "BLevyObtained"
        Me.BLevyObtained.Width = 60
        '
        'BSignedWalkingPossession
        '
        Me.BSignedWalkingPossession.DataPropertyName = "SignedWalkingPossession"
        Me.BSignedWalkingPossession.HeaderText = "SWP"
        Me.BSignedWalkingPossession.Name = "BSignedWalkingPossession"
        Me.BSignedWalkingPossession.Width = 60
        '
        'BAmountCollected
        '
        Me.BAmountCollected.DataPropertyName = "AmountCollected"
        Me.BAmountCollected.HeaderText = "Collected"
        Me.BAmountCollected.Name = "BAmountCollected"
        Me.BAmountCollected.Width = 60
        '
        'cmdCancel
        '
        Me.cmdCancel.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.cmdCancel.Location = New System.Drawing.Point(358, 306)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(100, 30)
        Me.cmdCancel.TabIndex = 17
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'cmdSave
        '
        Me.cmdSave.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.cmdSave.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.cmdSave.Location = New System.Drawing.Point(150, 306)
        Me.cmdSave.Name = "cmdSave"
        Me.cmdSave.Size = New System.Drawing.Size(100, 30)
        Me.cmdSave.TabIndex = 16
        Me.cmdSave.Text = "Save"
        Me.cmdSave.UseVisualStyleBackColor = True
        '
        'frmSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(600, 353)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdSave)
        Me.Controls.Add(Me.dgvBonus)
        Me.Controls.Add(Me.dgvTarget)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmSettings"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Targets and bonuses"
        CType(Me.dgvTarget, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvBonus, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvTarget As System.Windows.Forms.DataGridView
    Friend WithEvents dgvBonus As System.Windows.Forms.DataGridView
    Friend WithEvents BBand As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BCompletedCaseLoad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BLevyObtained As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BSignedWalkingPossession As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents BAmountCollected As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdSave As System.Windows.Forms.Button
    Friend WithEvents TBand As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TCompletedCaseLoad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TLevyObtained As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TSignedWalkingPossession As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TAmountCollected As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

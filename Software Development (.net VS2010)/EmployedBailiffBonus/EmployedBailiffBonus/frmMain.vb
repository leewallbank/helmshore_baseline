﻿Imports System.ComponentModel
Imports System.Diagnostics
Imports System.IO
Imports CommonLibrary

Public Class frmMain
    Private Bonus As New clsBonusData
    Private WithEvents RefreshDB As New BackgroundWorker
    Dim QuarterStartMonth As New Dictionary(Of Integer, Integer)

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Bonus.ImportBailiffs()
            Bonus.GetBailiffs()

            QuarterStartMonth.Add(1, 11)
            QuarterStartMonth.Add(2, 2)
            QuarterStartMonth.Add(3, 2)
            QuarterStartMonth.Add(4, 2)
            QuarterStartMonth.Add(5, 5)
            QuarterStartMonth.Add(6, 5)
            QuarterStartMonth.Add(7, 5)
            QuarterStartMonth.Add(8, 8)
            QuarterStartMonth.Add(9, 8)
            QuarterStartMonth.Add(10, 8)
            QuarterStartMonth.Add(11, 11)
            QuarterStartMonth.Add(12, 11)

            dgvEligible.DataSource = Bonus.EligibleBailiff
            dgvIneligible.DataSource = Bonus.IneligibleBailiff

            dtpCalcMonth.Value = dtpCalcMonth.Value.AddDays(-dtpCalcMonth.Value.Day + 1).AddMonths(-1)

            If {2, 5, 8, 11}.Contains(Now.Month) Then
                radMonthly.Checked = False
                radQuarterly.Checked = True
            Else
                radMonthly.Checked = True
                radQuarterly.Checked = False
            End If

            lblTo.Visible = radQuarterly.Checked
            lblMonthEnd.Visible = radQuarterly.Checked
            SetDates()

            AddHandler dgvEligible.RowValidated, AddressOf dgvEligible_RowValidated
            AddHandler dgvIneligible.RowValidated, AddressOf dgvIneligible_RowValidated

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnRemoveBailiff_Click(sender As Object, e As System.EventArgs) Handles btnRemoveBailiff.Click
        'http://social.msdn.microsoft.com/Forums/windows/en-US/7db968a9-38be-45fc-9dbf-e283f39d9e8c/datagridview-scrolling-to-selected-row?forum=winformsdatacontrols
        Try
            Dim ERowPos As Integer = dgvEligible.FirstDisplayedScrollingRowIndex
            Dim ERowSel As Integer = dgvEligible.SelectedRows(0).Index, EBailiffIDSel As Integer = dgvEligible.SelectedRows(0).Cells(0).Value

            Bonus.SetBailiff(dgvEligible.SelectedRows(0).Cells(0).Value, If(String.IsNullOrEmpty(dgvEligible.SelectedRows(0).Cells(2).Value.ToString), "", CDate(dgvEligible.SelectedRows(0).Cells(2).Value).ToString("dd/MMM/yyyy")), 0)

            dgvEligible.FirstDisplayedScrollingRowIndex = If(ERowPos = 0, 0, ERowPos - 1)
            dgvEligible.Rows(If(ERowSel = 0, 0, ERowSel - 1)).Selected = True

            For Each Row As DataGridViewRow In dgvIneligible.Rows
                If Row.Cells(0).Value = EBailiffIDSel Then
                    Row.Selected = True

                    Dim HalfWay As Integer = (dgvEligible.DisplayedRowCount(False) / 2)
                    If dgvIneligible.FirstDisplayedScrollingRowIndex + HalfWay > Row.Index OrElse (dgvIneligible.FirstDisplayedScrollingRowIndex + dgvIneligible.DisplayedRowCount(False) - HalfWay) <= Row.Index Then dgvIneligible.FirstDisplayedScrollingRowIndex = Math.Max(Row.Index - HalfWay, 0)

                End If
            Next Row

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnAddBailiff_Click(sender As Object, e As System.EventArgs) Handles btnAddBailiff.Click
        Try
            Dim IRowPos As Integer = dgvIneligible.FirstDisplayedScrollingRowIndex
            Dim IRowSel As Integer = dgvIneligible.SelectedRows(0).Index, IBailiffIDSel As Integer = dgvIneligible.SelectedRows(0).Cells(0).Value

            Bonus.SetBailiff(dgvIneligible.SelectedRows(0).Cells(0).Value, If(String.IsNullOrEmpty(dgvIneligible.SelectedRows(0).Cells(2).Value.ToString), "", CDate(dgvIneligible.SelectedRows(0).Cells(2).Value).ToString("dd/MMM/yyyy")), 1)

            dgvIneligible.FirstDisplayedScrollingRowIndex = If(IRowPos = 0, 0, IRowPos - 1)
            dgvIneligible.Rows(If(IRowSel = 0, 0, IRowSel - 1)).Selected = True

            For Each Row As DataGridViewRow In dgvEligible.Rows
                If Row.Cells(0).Value = IBailiffIDSel Then
                    Row.Selected = True

                    Dim HalfWay As Integer = (dgvEligible.DisplayedRowCount(False) / 2)
                    If dgvEligible.FirstDisplayedScrollingRowIndex + HalfWay > Row.Index OrElse (dgvEligible.FirstDisplayedScrollingRowIndex + dgvEligible.DisplayedRowCount(False) - HalfWay) <= Row.Index Then dgvEligible.FirstDisplayedScrollingRowIndex = Math.Max(Row.Index - HalfWay, 0)

                End If
            Next Row

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvEligible_CellValidating(ByVal sender As Object, ByVal e As DataGridViewCellValidatingEventArgs) Handles dgvEligible.CellValidating
        Try
            If Not dgvEligible.Columns(e.ColumnIndex).Name.Equals("EStartDate") Then Return

            If Not String.IsNullOrEmpty(e.FormattedValue.ToString()) AndAlso Not IsDate(e.FormattedValue.ToString()) Then
                MsgBox("Please enter a valid start date", vbCritical + vbOKOnly, Me.Text)
                e.Cancel = True
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvEligible_RowValidated(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) 'Handles dgvEligible.RowValidated
        Try
            UpdateStartDate(dgvEligible.Rows(e.RowIndex), 1)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvIneligible_CellValidating(ByVal sender As Object, ByVal e As DataGridViewCellValidatingEventArgs) Handles dgvIneligible.CellValidating
        Try
            If Not dgvIneligible.Columns(e.ColumnIndex).Name.Equals("IStartDate") Then Return

            If Not String.IsNullOrEmpty(e.FormattedValue.ToString()) AndAlso Not IsDate(e.FormattedValue.ToString()) Then
                MsgBox("Please enter a valid start date", vbCritical + vbOKOnly, Me.Text)
                e.Cancel = True
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvIneligible_RowValidated(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) 'Handles dgvIneligible.RowValidated
        Try
            UpdateStartDate(dgvIneligible.Rows(e.RowIndex), 0)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub UpdateStartDate(Row As DataGridViewRow, Eligible As Integer)
        Dim ERowPos As Integer = dgvEligible.FirstDisplayedScrollingRowIndex, IRowPos As Integer = dgvIneligible.FirstDisplayedScrollingRowIndex
        Dim ERowSel As Integer = dgvEligible.SelectedRows(0).Index, IRowSel As Integer = dgvIneligible.SelectedRows(0).Index

        Bonus.SetBailiff(Row.Cells(0).Value, If(String.IsNullOrEmpty(Row.Cells(2).Value.ToString), "", CDate(Row.Cells(2).Value).ToString("dd/MMM/yyyy")), Eligible)

        dgvEligible.FirstDisplayedScrollingRowIndex = ERowPos
        dgvIneligible.FirstDisplayedScrollingRowIndex = IRowPos

        dgvEligible.Rows(ERowSel).Selected = True
        dgvIneligible.Rows(IRowSel).Selected = True
    End Sub

    Private Sub btnCalcBonus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCalcBonus.Click
        Try
            lblProgress.Text = "Updating from Debt Recovery"

            PrgMain.Style = ProgressBarStyle.Marquee

            RefreshDB.RunWorkerAsync()

            dgvEligible.Enabled = False
            dgvIneligible.Enabled = False
            btnCalcBonus.Enabled = False

            Do Until Not RefreshDB.IsBusy
                Application.DoEvents()
            Loop

            PrgMain.Style = ProgressBarStyle.Blocks
            lblProgress.Text = ""

            MsgBox("Calculation complete.", vbOKOnly, Me.Text)

            GenerateReport()

            dgvEligible.Enabled = True
            dgvIneligible.Enabled = True
            btnCalcBonus.Enabled = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub RefreshDB_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles RefreshDB.DoWork
        Try
            Bonus.ImportFromDebtRecovery(dtpCalcMonth.Value, CDate(lblMonthEnd.Tag))
            Bonus.ImportCarryOver(If(radMonthly.Checked, 1, 3), dtpCalcMonth.Value)
            Bonus.CalculateBonus(If(radMonthly.Checked, 1, 3), dtpCalcMonth.Value)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radMonthly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radMonthly.CheckedChanged
        Try
            lblTo.Visible = radQuarterly.Checked
            lblMonthEnd.Visible = radQuarterly.Checked
            SetDates()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dtpCalcMonth_CloseUp(ByVal sender As Object, ByVal e As System.EventArgs) Handles dtpCalcMonth.CloseUp
        Try
            SetDates()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetDates()
        Try
            If radQuarterly.Checked Then
                If dtpCalcMonth.Value.Month > 1 Then
                    dtpCalcMonth.Value = dtpCalcMonth.Value.AddMonths(QuarterStartMonth(dtpCalcMonth.Value.Month) - dtpCalcMonth.Value.Month).AddDays(-dtpCalcMonth.Value.Day + 1)
                Else
                    dtpCalcMonth.Value = dtpCalcMonth.Value.AddMonths(QuarterStartMonth(dtpCalcMonth.Value.Month) - dtpCalcMonth.Value.Month - 12).AddDays(-dtpCalcMonth.Value.Day + 1)
                End If

                lblMonthEnd.Tag = dtpCalcMonth.Value.AddMonths(3).AddDays(-1).ToString("dd/MMM/yyyy")
            Else
                dtpCalcMonth.Value = dtpCalcMonth.Value.AddDays(-dtpCalcMonth.Value.Day + 1)
                lblMonthEnd.Tag = dtpCalcMonth.Value.AddMonths(1).AddDays(-1).ToString("dd/MMM/yyyy")
            End If
            lblMonthEnd.Text = CDate(lblMonthEnd.Tag).ToString("MMMM yyyy")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnSetTargetsBonus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSetTargetsBonus.Click
        Try
            frmSettings.ShowDialog()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GenerateReport()

        Dim FolderBrowserDialog As New FolderBrowserDialog
        FolderBrowserDialog.Description = "Select where the output spreadsheet will be saved."
        FolderBrowserDialog.ShowNewFolderButton = True

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        If FolderBrowserDialog.ShowDialog() <> Windows.Forms.DialogResult.OK Then Return
        Dim Path As String = FolderBrowserDialog.SelectedPath & "\", FileName As String = "EmployedBailiffs_" & CDate(dtpCalcMonth.Value).ToString("MMMyy")

        If lblMonthEnd.Visible Then FileName &= "_" & CDate(lblMonthEnd.Text).ToString("MMMyy")

        Bonus.GetBailiffPerformance()

        Dim ExcelApp As Object, Workbook As Object, Worksheet As Object
        Dim ColumnIndex As Integer, RowIndex As Integer
        Dim ColumnSpec As String(,) = {{"BailiffID", 12}, _
                                       {"Bailiff Name", 25}, _
                                       {"Completed Case Load", 12}, _
                                       {"Minimum Target", 12}, _
                                       {"Variance", 9}, _
                                       {"Completed Case Load Band", 15}, _
                                       {"Levy Obtained Actual", 15}, _
                                       {"Minimum Target", 10}, _
                                       {"Variance from min", 10}, _
                                       {"% against Completed Cases", 12}, _
                                       {"% against min caseload Target", 12}, _
                                       {"Levy Obtained Band", 10}, _
                                       {"SWP Actual", 11}, _
                                       {"Minimum Target", 12}, _
                                       {"Variance", 9}, _
                                       {"% SWP against Levy actual", 9}, _
                                       {"% against min target", 10}, _
                                       {"Signed Walking Possession Band", 12}, _
                                       {"Amount Collected", 10}, _
                                       {"Min Target", 9}, _
                                       {"Variance", 9}, _
                                       {"Amount Collected Band", 9}, _
                                       {"Bonus", 8} _
                                      }

        ExcelApp = CreateObject("Excel.Application")
        Workbook = ExcelApp.Workbooks.Add()
        Worksheet = Workbook.Sheets(1)
        Try
            ExcelApp.DisplayAlerts = False

            For Each Sheet As Object In Workbook.Worksheets
                If Sheet.Index > 1 Then Sheet.Delete()
            Next Sheet

            Worksheet.Name = "Bailiff performance"
            Worksheet.Range(Worksheet.Cells(1, 1), Worksheet.Cells(1, UBound(ColumnSpec) + 1)).Font.Bold = True
            Worksheet.Rows("1:1").RowHeight = 60
            Worksheet.Range(Worksheet.Cells(1, 1), Worksheet.Cells(1, UBound(ColumnSpec) + 1)).WrapText = True
            Worksheet.Range(Worksheet.Cells(1, 1), Worksheet.Cells(Bonus.BailiffPerformance.Rows.Count + 1, UBound(ColumnSpec) + 1)).BORDERS.Weight = 2

            For ColumnIndex = 1 To UBound(ColumnSpec) + 1
                Worksheet.Cells(1, ColumnIndex) = ColumnSpec(ColumnIndex - 1, 0)
                Worksheet.Columns(ColumnIndex).EntireColumn.ColumnWidth = CInt(ColumnSpec(ColumnIndex - 1, 1))

                Select Case ColumnIndex
                    Case 3, 7, 13, 19, 20
                        Worksheet.Cells(1, ColumnIndex).Interior.Color = RGB(220, 230, 241)
                        Workbook.Colors(Worksheet.Cells(1, ColumnIndex).Interior.ColorIndex) = RGB(220, 230, 241)

                    Case 6, 12, 18, 22
                        Worksheet.Cells(1, ColumnIndex).Interior.Color = RGB(197, 217, 241)
                        Workbook.Colors(Worksheet.Cells(1, ColumnIndex).Interior.ColorIndex) = RGB(197, 217, 241)
                End Select

                For RowIndex = 1 To Bonus.BailiffPerformance.Rows.Count
                    Application.DoEvents()
                    Worksheet.Cells(RowIndex + 1, ColumnIndex) = Bonus.BailiffPerformance(RowIndex - 1)(ColumnIndex - 1)

                    Select Case ColumnIndex
                        Case 5, 9, 15, 21
                            If Bonus.BailiffPerformance(RowIndex - 1)(ColumnIndex - 1) < 0 Then
                                Worksheet.Cells(RowIndex + 1, ColumnIndex).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red)
                            ElseIf Bonus.BailiffPerformance(RowIndex - 1)(ColumnIndex - 1) > 0 Then
                                Worksheet.Cells(RowIndex + 1, ColumnIndex).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green)
                            End If

                        Case 10, 11
                            Worksheet.Cells(RowIndex + 1, ColumnIndex).NumberFormat = "#0.00"
                            If Bonus.BailiffPerformance(RowIndex - 1)(ColumnIndex - 1) < Bonus.LevyMinTgtPerc Then
                                Worksheet.Cells(RowIndex + 1, ColumnIndex).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red)
                            ElseIf Bonus.BailiffPerformance(RowIndex - 1)(ColumnIndex - 1) > Bonus.LevyMinTgtPerc Then
                                Worksheet.Cells(RowIndex + 1, ColumnIndex).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green)
                            End If

                        Case 16, 17
                            Worksheet.Cells(RowIndex + 1, ColumnIndex).NumberFormat = "#0.00"

                            If Bonus.BailiffPerformance(RowIndex - 1)(ColumnIndex - 1) < Bonus.SWPMinTgtPerc Then
                                Worksheet.Cells(RowIndex + 1, ColumnIndex).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Red)
                            ElseIf Bonus.BailiffPerformance(RowIndex - 1)(ColumnIndex - 1) > Bonus.SWPMinTgtPerc Then
                                Worksheet.Cells(RowIndex + 1, ColumnIndex).Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Green)
                            End If

                        Case 19, 20, 21
                            Worksheet.Cells(RowIndex + 1, ColumnIndex).NumberFormat = "#0.00"

                    End Select

                Next RowIndex
            Next ColumnIndex

            Worksheet.Columns.Autofit()

            Directory.CreateDirectory(Path)
            Workbook.SaveAs(Path & FileName, 39) ' 39  Is Excel version 8. A bit old but more likely to be compatible

        Catch ex As Exception
            HandleException(ex)

        Finally
            CloseComObject(ExcelApp.ActiveWorkbook)
            ExcelApp.ActiveWorkbook.Close()
            ExcelApp.Quit()
            CloseComObject(Worksheet)
            Worksheet = Nothing
            CloseComObject(Workbook)
            Workbook = Nothing
            CloseComObject(ExcelApp)
            ExcelApp = Nothing

            System.Diagnostics.Process.Start(Path & FileName & ".xls")

            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try
    End Sub

End Class

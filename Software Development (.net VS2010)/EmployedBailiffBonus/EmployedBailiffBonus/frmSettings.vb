﻿Public Class frmSettings

    Private Settings As New clsBonusData


    Private Sub frmSettings_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        RefreshSettings()
    End Sub

    Private Sub RefreshSettings()
        Settings.GetBonuses()
        Settings.GetTargets()

        dgvBonus.DataSource = Settings.BonusLevel
        dgvTarget.DataSource = Settings.TargetLevel
    End Sub

    Private Sub cmdSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSave.Click
        For Each dr As DataRowView In dgvTarget.DataSource
            If dr.Row.RowState = DataRowState.Modified Then Settings.SetTarget(dr.Item("Band"), dr.Item("CompletedCaseLoad"), dr.Item("LevyObtained"), dr.Item("SignedWalkingPossession"), dr.Item("AmountCollected"))
        Next dr
        For Each dr As DataRowView In dgvBonus.DataSource
            If dr.Row.RowState = DataRowState.Modified Then Settings.SetBonus(dr.Item("Band"), dr.Item("CompletedCaseLoad"), dr.Item("LevyObtained"), dr.Item("SignedWalkingPossession"), dr.Item("AmountCollected"))
        Next dr

        MsgBox("Changes saved.", MsgBoxStyle.OkOnly, "Settings")

        RemoveHandler Me.FormClosing, AddressOf frmSettings_FormClosing

        Me.Close()
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Dim HasChanges As Boolean = False

        For Each dr As DataRowView In dgvTarget.DataSource
            If dr.Row.RowState = DataRowState.Modified Then HasChanges = True
        Next dr
        For Each dr As DataRowView In dgvBonus.DataSource
            If dr.Row.RowState = DataRowState.Modified Then HasChanges = True
        Next dr

        If HasChanges AndAlso MsgBox("Discard changes?", vbYesNo + vbDefaultButton2, "Settings") = MsgBoxResult.Yes Then
            Me.Close()
        ElseIf Not HasChanges Then
            Me.Close()
        End If

    End Sub

    Private Sub frmSettings_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim HasChanges As Boolean = False

        For Each dr As DataRowView In dgvTarget.DataSource
            If dr.Row.RowState = DataRowState.Modified Then HasChanges = True
        Next dr
        For Each dr As DataRowView In dgvBonus.DataSource
            If dr.Row.RowState = DataRowState.Modified Then HasChanges = True
        Next dr

        If HasChanges AndAlso MsgBox("Discard changes?", vbYesNo + vbDefaultButton2, "Settings") = MsgBoxResult.No Then
            e.Cancel = True
        End If
    End Sub

End Class
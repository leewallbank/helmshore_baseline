﻿Imports CommonLibrary

Public Class clsBonusData
    Private Bailiff As New DataTable, Bonus As New DataTable, Target As New DataTable, Performance As New DataTable
    Private EligibleBailiffDV As DataView, IneligibleBailiffDV As DataView, BonusDV As DataView, TargetDV As DataView

    Public ReadOnly Property EligibleBailiff() As DataView
        Get
            EligibleBailiff = EligibleBailiffDV
        End Get
    End Property

    Public ReadOnly Property IneligibleBailiff() As DataView
        Get
            IneligibleBailiff = IneligibleBailiffDV
        End Get
    End Property

    Public ReadOnly Property BonusLevel() As DataView
        Get
            BonusLevel = BonusDV
        End Get
    End Property

    Public ReadOnly Property TargetLevel() As DataView
        Get
            TargetLevel = TargetDV
        End Get
    End Property

    Public ReadOnly Property BailiffPerformance() As DataTable
        Get
            BailiffPerformance = Performance
        End Get
    End Property

    Public ReadOnly Property LevyMinTgtPerc As Decimal
        Get
            LevyMinTgtPerc = CDec(Target.Select("Band = 0")(0)("LevyObtained"))
        End Get
    End Property

    Public ReadOnly Property SWPMinTgtPerc As Decimal
        Get
            SWPMinTgtPerc = CDec(Target.Select("Band = 0")(0)("SignedWalkingPossession"))
        End Get
    End Property

    Public Sub ImportBailiffs()
        ExecStoredProc("BailiffRemuneration", "EXEC stg.ImportEmployedBailiff")
    End Sub

    Public Sub SetBailiff(ByVal BailiffID As String, ByVal StartDate As String, ByVal Eligible As Boolean)

        If StartDate = "" Then
            StartDate = "NULL"
        Else
            StartDate = "'" & StartDate & "'"
        End If

        ExecStoredProc("BailiffRemuneration", "EXEC dbo.SetEmployedBailiff " & BailiffID & ", " & StartDate & "," & Eligible.ToString)
        GetBailiffs()
    End Sub

    Public Sub ImportFromDebtRecovery(ByVal StartDate As Date, ByVal EndDate As Date)
        ExecStoredProc("BailiffRemuneration", "EXEC stg.ImportFromDebtRecovery '" & StartDate.ToString("dd/MMM/yyyy") & "', '" & EndDate.ToString("dd/MMM/yyyy") & "'", 0)
    End Sub

    Public Sub ImportCarryOver(ByVal PeriodMonths As Integer, ByVal StartDate As Date)
        ExecStoredProc("BailiffRemuneration", "EXEC stg.ImportCarryOver " & PeriodMonths.ToString & ", '" & StartDate.ToString("dd/MMM/yyyy") & "'", 0)
    End Sub

    Public Sub ImportRemittedFees(ByVal StartDate As Date, ByVal EndDate As Date)
        ExecStoredProc("BailiffRemuneration", "EXEC stg.ImportRemittedFees '" & StartDate.ToString("dd/MMM/yyyy") & "', '" & EndDate.ToString("dd/MMM/yyyy") & "'", 0)
    End Sub

    Public Sub CalculateBonus(ByVal PeriodMonths As Integer, ByVal StartDate As Date)
        ExecStoredProc("BailiffRemuneration", "EXEC dbo.BuildBailiffPerformance " & PeriodMonths.ToString & ", '" & StartDate.ToString("dd/MMM/yyyy") & "'")
    End Sub

    Public Sub GetBailiffs()
        Dim Sql As String

        Try

            Sql = "SELECT BailiffID " & _
                  "     , BailiffName " & _
                  "     , StartDate " & _
                  "     , EligibleForBonus " & _
                  "FROM dbo.EmployedBailiff "

            LoadDataTable("BailiffRemuneration", Sql, Bailiff, False)

            EligibleBailiffDV = New DataView(Bailiff)
            EligibleBailiffDV.AllowNew = False
            EligibleBailiffDV.RowFilter = "EligibleForBonus = 1"

            IneligibleBailiffDV = New DataView(Bailiff)
            IneligibleBailiffDV.AllowNew = False
            IneligibleBailiffDV.RowFilter = "EligibleForBonus = 0"

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub GetBonuses()
        Dim Sql As String

        Try

            Sql = "EXEC dbo.GetBonus"

            LoadDataTable("BailiffRemuneration", Sql, Bonus, False)

            BonusDV = New DataView(Bonus)
            BonusDV.AllowNew = False
            
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub GetTargets()
        Dim Sql As String

        Try

            Sql = "EXEC dbo.GetTarget"

            LoadDataTable("BailiffRemuneration", Sql, Target, False)

            TargetDV = New DataView(Target)
            TargetDV.AllowNew = False

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub GetBailiffPerformance()
        Dim Sql As String

        Try

            Sql = "EXEC dbo.GetBailiffPerformance"

            LoadDataTable("BailiffRemuneration", Sql, Performance, False)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Public Sub SetBonus(ByVal Band As Integer, ByVal CompletedCaseLoad As Decimal, ByVal LevyObtained As Decimal, ByVal SignedWalkingPossession As Decimal, ByVal AmountCollected As Decimal)

        ExecStoredProc("BailiffRemuneration", "EXEC dbo.SetBonus " & Band.ToString & "," & CompletedCaseLoad.ToString & "," & LevyObtained.ToString & "," & SignedWalkingPossession.ToString & "," & AmountCollected.ToString, 600)

    End Sub

    Public Sub SetTarget(ByVal Band As Integer, ByVal CompletedCaseLoad As Integer, ByVal LevyObtained As Decimal, ByVal SignedWalkingPossession As Decimal, ByVal AmountCollected As Decimal)

        ExecStoredProc("BailiffRemuneration", "EXEC dbo.SetTarget " & Band.ToString & "," & CompletedCaseLoad.ToString & "," & LevyObtained.ToString & "," & SignedWalkingPossession.ToString & "," & AmountCollected.ToString, 600)

    End Sub

    Public Sub New()
        GetTargets()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.dgvEligible = New System.Windows.Forms.DataGridView()
        Me.EBailiffID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EBailiffName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EStartDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EEligibleForBonus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgvIneligible = New System.Windows.Forms.DataGridView()
        Me.IBailiffID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IBailiffName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IStartDate = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IEligibleForBonus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblEligible = New System.Windows.Forms.Label()
        Me.lblIneligible = New System.Windows.Forms.Label()
        Me.btnCalcBonus = New System.Windows.Forms.Button()
        Me.PrgMain = New System.Windows.Forms.ProgressBar()
        Me.lblProgress = New System.Windows.Forms.Label()
        Me.radMonthly = New System.Windows.Forms.RadioButton()
        Me.radQuarterly = New System.Windows.Forms.RadioButton()
        Me.dtpCalcMonth = New System.Windows.Forms.DateTimePicker()
        Me.lblMonthEnd = New System.Windows.Forms.Label()
        Me.lblTo = New System.Windows.Forms.Label()
        Me.btnSetTargetsBonus = New System.Windows.Forms.Button()
        Me.btnRemoveBailiff = New System.Windows.Forms.Button()
        Me.btnAddBailiff = New System.Windows.Forms.Button()
        CType(Me.dgvEligible, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvIneligible, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvEligible
        '
        Me.dgvEligible.AllowDrop = True
        Me.dgvEligible.AllowUserToAddRows = False
        Me.dgvEligible.AllowUserToDeleteRows = False
        Me.dgvEligible.AllowUserToResizeColumns = False
        Me.dgvEligible.AllowUserToResizeRows = False
        Me.dgvEligible.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgvEligible.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEligible.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.EBailiffID, Me.EBailiffName, Me.EStartDate, Me.EEligibleForBonus})
        Me.dgvEligible.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvEligible.Location = New System.Drawing.Point(11, 38)
        Me.dgvEligible.MultiSelect = False
        Me.dgvEligible.Name = "dgvEligible"
        Me.dgvEligible.RowHeadersVisible = False
        Me.dgvEligible.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvEligible.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvEligible.Size = New System.Drawing.Size(270, 309)
        Me.dgvEligible.TabIndex = 0
        '
        'EBailiffID
        '
        Me.EBailiffID.DataPropertyName = "BailiffID"
        Me.EBailiffID.HeaderText = "ID"
        Me.EBailiffID.Name = "EBailiffID"
        Me.EBailiffID.ReadOnly = True
        Me.EBailiffID.Width = 40
        '
        'EBailiffName
        '
        Me.EBailiffName.DataPropertyName = "BailiffName"
        Me.EBailiffName.HeaderText = "Name"
        Me.EBailiffName.Name = "EBailiffName"
        Me.EBailiffName.ReadOnly = True
        Me.EBailiffName.Width = 130
        '
        'EStartDate
        '
        Me.EStartDate.DataPropertyName = "StartDate"
        Me.EStartDate.HeaderText = "Start date"
        Me.EStartDate.Name = "EStartDate"
        Me.EStartDate.Width = 80
        '
        'EEligibleForBonus
        '
        Me.EEligibleForBonus.DataPropertyName = "EligibleForBonus"
        Me.EEligibleForBonus.HeaderText = "Eligible For Bonus"
        Me.EEligibleForBonus.Name = "EEligibleForBonus"
        Me.EEligibleForBonus.Visible = False
        '
        'dgvIneligible
        '
        Me.dgvIneligible.AllowDrop = True
        Me.dgvIneligible.AllowUserToAddRows = False
        Me.dgvIneligible.AllowUserToDeleteRows = False
        Me.dgvIneligible.AllowUserToResizeColumns = False
        Me.dgvIneligible.AllowUserToResizeRows = False
        Me.dgvIneligible.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.dgvIneligible.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvIneligible.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IBailiffID, Me.IBailiffName, Me.IStartDate, Me.IEligibleForBonus})
        Me.dgvIneligible.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvIneligible.Location = New System.Drawing.Point(335, 38)
        Me.dgvIneligible.MultiSelect = False
        Me.dgvIneligible.Name = "dgvIneligible"
        Me.dgvIneligible.RowHeadersVisible = False
        Me.dgvIneligible.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvIneligible.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvIneligible.Size = New System.Drawing.Size(270, 264)
        Me.dgvIneligible.TabIndex = 1
        '
        'IBailiffID
        '
        Me.IBailiffID.DataPropertyName = "BailiffID"
        Me.IBailiffID.HeaderText = "ID"
        Me.IBailiffID.Name = "IBailiffID"
        Me.IBailiffID.ReadOnly = True
        Me.IBailiffID.Width = 40
        '
        'IBailiffName
        '
        Me.IBailiffName.DataPropertyName = "BailiffName"
        Me.IBailiffName.HeaderText = "Name"
        Me.IBailiffName.Name = "IBailiffName"
        Me.IBailiffName.ReadOnly = True
        Me.IBailiffName.Width = 130
        '
        'IStartDate
        '
        Me.IStartDate.DataPropertyName = "StartDate"
        Me.IStartDate.HeaderText = "Start date"
        Me.IStartDate.Name = "IStartDate"
        Me.IStartDate.Width = 80
        '
        'IEligibleForBonus
        '
        Me.IEligibleForBonus.DataPropertyName = "EligibleForBonus"
        Me.IEligibleForBonus.HeaderText = "Eligible For Bonus"
        Me.IEligibleForBonus.Name = "IEligibleForBonus"
        Me.IEligibleForBonus.Visible = False
        '
        'lblEligible
        '
        Me.lblEligible.AutoSize = True
        Me.lblEligible.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEligible.Location = New System.Drawing.Point(8, 13)
        Me.lblEligible.Name = "lblEligible"
        Me.lblEligible.Size = New System.Drawing.Size(112, 16)
        Me.lblEligible.TabIndex = 2
        Me.lblEligible.Text = "Eligible Bailiffs"
        '
        'lblIneligible
        '
        Me.lblIneligible.AutoSize = True
        Me.lblIneligible.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIneligible.Location = New System.Drawing.Point(332, 13)
        Me.lblIneligible.Name = "lblIneligible"
        Me.lblIneligible.Size = New System.Drawing.Size(123, 16)
        Me.lblIneligible.TabIndex = 3
        Me.lblIneligible.Text = "Ineligible Bailiffs"
        '
        'btnCalcBonus
        '
        Me.btnCalcBonus.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCalcBonus.Location = New System.Drawing.Point(642, 215)
        Me.btnCalcBonus.Name = "btnCalcBonus"
        Me.btnCalcBonus.Size = New System.Drawing.Size(150, 30)
        Me.btnCalcBonus.TabIndex = 0
        Me.btnCalcBonus.Text = "Calculate Bonus"
        Me.btnCalcBonus.UseVisualStyleBackColor = True
        '
        'PrgMain
        '
        Me.PrgMain.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PrgMain.Location = New System.Drawing.Point(611, 317)
        Me.PrgMain.Name = "PrgMain"
        Me.PrgMain.Size = New System.Drawing.Size(212, 28)
        Me.PrgMain.TabIndex = 5
        '
        'lblProgress
        '
        Me.lblProgress.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblProgress.Location = New System.Drawing.Point(611, 279)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(212, 23)
        Me.lblProgress.TabIndex = 6
        Me.lblProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'radMonthly
        '
        Me.radMonthly.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radMonthly.AutoSize = True
        Me.radMonthly.Location = New System.Drawing.Point(640, 38)
        Me.radMonthly.Name = "radMonthly"
        Me.radMonthly.Size = New System.Drawing.Size(62, 17)
        Me.radMonthly.TabIndex = 8
        Me.radMonthly.TabStop = True
        Me.radMonthly.Text = "Monthly"
        Me.radMonthly.UseVisualStyleBackColor = True
        '
        'radQuarterly
        '
        Me.radQuarterly.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.radQuarterly.AutoSize = True
        Me.radQuarterly.Location = New System.Drawing.Point(640, 59)
        Me.radQuarterly.Name = "radQuarterly"
        Me.radQuarterly.Size = New System.Drawing.Size(67, 17)
        Me.radQuarterly.TabIndex = 9
        Me.radQuarterly.TabStop = True
        Me.radQuarterly.Text = "Quarterly"
        Me.radQuarterly.UseVisualStyleBackColor = True
        '
        'dtpCalcMonth
        '
        Me.dtpCalcMonth.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpCalcMonth.CustomFormat = "MMMM yyyy"
        Me.dtpCalcMonth.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpCalcMonth.Location = New System.Drawing.Point(637, 82)
        Me.dtpCalcMonth.Name = "dtpCalcMonth"
        Me.dtpCalcMonth.Size = New System.Drawing.Size(150, 20)
        Me.dtpCalcMonth.TabIndex = 10
        '
        'lblMonthEnd
        '
        Me.lblMonthEnd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMonthEnd.AutoSize = True
        Me.lblMonthEnd.Location = New System.Drawing.Point(639, 135)
        Me.lblMonthEnd.Name = "lblMonthEnd"
        Me.lblMonthEnd.Size = New System.Drawing.Size(39, 13)
        Me.lblMonthEnd.TabIndex = 11
        Me.lblMonthEnd.Text = "Label1"
        '
        'lblTo
        '
        Me.lblTo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblTo.AutoSize = True
        Me.lblTo.Location = New System.Drawing.Point(639, 112)
        Me.lblTo.Name = "lblTo"
        Me.lblTo.Size = New System.Drawing.Size(16, 13)
        Me.lblTo.TabIndex = 14
        Me.lblTo.Text = "to"
        '
        'btnSetTargetsBonus
        '
        Me.btnSetTargetsBonus.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSetTargetsBonus.Location = New System.Drawing.Point(395, 317)
        Me.btnSetTargetsBonus.Name = "btnSetTargetsBonus"
        Me.btnSetTargetsBonus.Size = New System.Drawing.Size(150, 30)
        Me.btnSetTargetsBonus.TabIndex = 15
        Me.btnSetTargetsBonus.Text = "Set Targets / Bonus"
        Me.btnSetTargetsBonus.UseVisualStyleBackColor = True
        '
        'btnRemoveBailiff
        '
        Me.btnRemoveBailiff.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRemoveBailiff.Location = New System.Drawing.Point(293, 94)
        Me.btnRemoveBailiff.Name = "btnRemoveBailiff"
        Me.btnRemoveBailiff.Size = New System.Drawing.Size(30, 30)
        Me.btnRemoveBailiff.TabIndex = 16
        Me.btnRemoveBailiff.Text = " >"
        Me.btnRemoveBailiff.UseVisualStyleBackColor = True
        '
        'btnAddBailiff
        '
        Me.btnAddBailiff.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAddBailiff.Location = New System.Drawing.Point(293, 135)
        Me.btnAddBailiff.Name = "btnAddBailiff"
        Me.btnAddBailiff.Size = New System.Drawing.Size(30, 30)
        Me.btnAddBailiff.TabIndex = 17
        Me.btnAddBailiff.Text = "<"
        Me.btnAddBailiff.UseVisualStyleBackColor = True
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(839, 359)
        Me.Controls.Add(Me.btnAddBailiff)
        Me.Controls.Add(Me.btnRemoveBailiff)
        Me.Controls.Add(Me.btnSetTargetsBonus)
        Me.Controls.Add(Me.lblTo)
        Me.Controls.Add(Me.lblMonthEnd)
        Me.Controls.Add(Me.dtpCalcMonth)
        Me.Controls.Add(Me.radQuarterly)
        Me.Controls.Add(Me.radMonthly)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.PrgMain)
        Me.Controls.Add(Me.btnCalcBonus)
        Me.Controls.Add(Me.lblIneligible)
        Me.Controls.Add(Me.lblEligible)
        Me.Controls.Add(Me.dgvIneligible)
        Me.Controls.Add(Me.dgvEligible)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Employed Bailiff Bonus"
        CType(Me.dgvEligible, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvIneligible, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvEligible As System.Windows.Forms.DataGridView
    Friend WithEvents dgvIneligible As System.Windows.Forms.DataGridView
    Friend WithEvents lblEligible As System.Windows.Forms.Label
    Friend WithEvents lblIneligible As System.Windows.Forms.Label
    Friend WithEvents btnCalcBonus As System.Windows.Forms.Button
    Friend WithEvents PrgMain As System.Windows.Forms.ProgressBar
    Friend WithEvents lblProgress As System.Windows.Forms.Label
    Friend WithEvents radMonthly As System.Windows.Forms.RadioButton
    Friend WithEvents radQuarterly As System.Windows.Forms.RadioButton
    Friend WithEvents dtpCalcMonth As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblMonthEnd As System.Windows.Forms.Label
    Friend WithEvents lblTo As System.Windows.Forms.Label
    Friend WithEvents btnSetTargetsBonus As System.Windows.Forms.Button
    Friend WithEvents EBailiffID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EBailiffName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EStartDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EEligibleForBonus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IBailiffID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IBailiffName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IStartDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IEligibleForBonus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnRemoveBailiff As System.Windows.Forms.Button
    Friend WithEvents btnAddBailiff As System.Windows.Forms.Button

End Class

﻿Imports System.Configuration
Imports System.IO
Imports System.Xml.Schema
Imports CommonLibrary

Public Class frmMain
    Private Returns As New clsReturnsData

    Private SourceRow As Integer, SourceCol As Integer
    Private DateFrom As DateTime, DateTo As DateTime ' Added TS 11/Mar/2015. Request ref 44285
    Private Const DCACode As String = "AGY023"
    Private Const AgencyShortName As String = "ROSS"
    Private ErrorNo As Integer
    Private AuditFile As String

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        ' commented out TS 12/Mar/2015. Request ref 44285
        'If Now.DayOfWeek = DayOfWeek.Monday Then
        '    dtpDateFrom.Value = DateAdd(DateInterval.Day, -2, Now)
        '    dtpDateTo.Value = Now
        'Else
        '    dtpDateFrom.Value = Now
        '    dtpDateTo.Value = dtpDateFrom.Value
        'End If

        ' added TS 12/Mar/2015. Request ref 44285
        Returns.GetLastRunDetails()

        txtFileSequenceNumber.Text = Returns.LastReturnDetails.FileSequence + 1
        dtpDateFrom.Value = Returns.LastReturnDetails.DateTo.AddDays(1)
        dtpDateTo.Value = Today.AddDays(-1)
        ' end of new section

        cmsDGV.Items.Add("Cut")
        cmsDGV.Items.Add("Copy")
        cmsDGV.Items.Add("Paste")

        btnCreateReturns.Enabled = False
    End Sub

    Private Sub dgvMain_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMain.CellValueChanged
        Try
            If Not IsDBNull(dgvMain(e.ColumnIndex, e.RowIndex).Value) Then dgvMain(e.ColumnIndex, e.RowIndex).Value = LTrim(dgvMain(e.ColumnIndex, e.RowIndex).Value)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvMain.MouseDown
        Try
            Dim p As Point = dgvMain.PointToClient(Control.MousePosition)
            Dim hit As DataGridView.HitTestInfo = dgvMain.HitTest(p.X, p.Y)

            SourceRow = hit.RowIndex
            SourceCol = hit.ColumnIndex

            If hit.Type = DataGridViewHitTestType.Cell And e.Button = MouseButtons.Left Then
                dgvMain.BeginEdit(False)
                If Not dgvMain(hit.ColumnIndex, hit.RowIndex).IsInEditMode Then
                    dgvMain.DoDragDrop(dgvMain(hit.ColumnIndex, hit.RowIndex).Value, DragDropEffects.Copy Or DragDropEffects.Move)
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvMain.MouseUp
        Try
            If e.Button = MouseButtons.Right Then cmsDGV.Show(sender, e.Location)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvMain.DragEnter
        Try
            If (e.Data.GetDataPresent(DataFormats.Text)) Then
                e.Effect = DragDropEffects.Copy
            Else
                e.Effect = DragDropEffects.None
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvMain.DragDrop
        Try
            Dim p As System.Drawing.Point = dgvMain.PointToClient(Control.MousePosition)
            Dim hit As DataGridView.HitTestInfo = dgvMain.HitTest(p.X, p.Y)

            If hit.Type = DataGridViewHitTestType.Cell AndAlso SourceRow = hit.RowIndex AndAlso Not dgvMain(hit.ColumnIndex, hit.RowIndex).ReadOnly Then ' Check rows as we do not want data moving from one case to another
                dgvMain(SourceCol, SourceRow).Value = ""
                ' if drop position is in the left hand half of the cell, insert data before the existing contents, else insert after
                If p.X < dgvMain.GetCellDisplayRectangle(hit.ColumnIndex, hit.RowIndex, False).Left + (dgvMain.Columns(hit.ColumnIndex).Width / 2) Then
                    dgvMain(hit.ColumnIndex, hit.RowIndex).Value = e.Data.GetData(DataFormats.Text).ToString & dgvMain(hit.ColumnIndex, hit.RowIndex).Value
                Else
                    dgvMain(hit.ColumnIndex, hit.RowIndex).Value &= e.Data.GetData(DataFormats.Text).ToString
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub frmMain_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If dgvMain.SelectedCells.Count = 0 Then Return

        Try
            Dim ClickCell As DataGridViewCell = dgvMain.SelectedCells(0)
            If e.Control Then
                Select Case e.KeyCode
                    Case Keys.C
                        Clipboard.SetText(ClickCell.Value, TextDataFormat.Text)
                    Case Keys.V
                        If Not ClickCell.ReadOnly Then ClickCell.Value = Clipboard.GetText(TextDataFormat.Text)
                    Case Keys.X
                        If Not ClickCell.ReadOnly Then
                            Clipboard.SetText(ClickCell.Value)
                            ClickCell.Value = ""
                        End If
                End Select
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dtpRemitDate_CloseUp(ByVal sender As Object, ByVal e As System.EventArgs)
        RefreshData()
    End Sub

    Private Sub cmdRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        lblNoData.Visible = False
        RefreshData()
    End Sub

    Private Sub cmsDGV_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsDGV.ItemClicked
        Try
            Dim ClickCell As DataGridViewCell = dgvMain.SelectedCells(0)
            Select Case e.ClickedItem.Text
                Case "Cut"
                    If Not ClickCell.ReadOnly Then
                        Clipboard.SetText(ClickCell.Value)
                        ClickCell.Value = ""
                    End If
                Case "Copy"
                    Clipboard.SetText(ClickCell.Value, TextDataFormat.Text)
                Case "Paste"
                    If Not ClickCell.ReadOnly Then ClickCell.Value = Clipboard.GetText(TextDataFormat.Text)
            End Select

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        Try
            If DataValid() Then MsgBox("Data is valid.", vbOKOnly + vbInformation, Me.Text)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub RefreshData()
        Try
            Me.Cursor = Cursors.WaitCursor

            ' Added TS 11/Mar/2015. Request ref 44285
            ' These are preserved at the point of refresh in case the datetimepickers are changed between refresh and file production
            DateFrom = dtpDateFrom.Value
            DateTo = dtpDateTo.Value

            Returns.GetAddresses(dtpDateFrom.Value, dtpDateTo.Value)
            Returns.GetDebtors(dtpDateFrom.Value, dtpDateTo.Value)

            With dgvMain
                .DataSource = Returns.Addresses

                For Each DataColumn As DataGridViewTextBoxColumn In .Columns
                    DataColumn.SortMode = DataGridViewColumnSortMode.NotSortable
                Next DataColumn

                If .Columns.Contains("DebtorID") Then .Columns("DebtorID").ReadOnly = True
                .Columns("name_title").Visible = False
                .Columns("name_fore").Visible = False
                .Columns("name_sur").Visible = False

            End With

            DataValid()

            'txtFileSequenceNumber.Text = Returns.GetNextFileSequence ' added TS 05/Aug/2014. Request ref 27729. Commented out 12/Mar/2015
            Returns.GetLastRunDetails() ' added TS 12/Mar/2015. Request ref 44285
            txtFileSequenceNumber.Text = Returns.LastReturnDetails.FileSequence + 1 ' added TS 12/Mar/2015. Request ref 44285

            Me.Cursor = Cursors.Default
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Function DataValid() As Boolean
        Dim ColLoopCount As Integer, RowLoopCount As Integer, MaxSize As Integer
        Dim ClipboardText As String = Nothing

        DataValid = True

        Try
            dgvMain.EndEdit()

            For Each DataColumn As DataGridViewTextBoxColumn In dgvMain.Columns
                ColLoopCount = DataColumn.Index

                MaxSize = Returns.Addresses.Table.Columns(ColLoopCount).ExtendedProperties("MaxLength")
                For RowLoopCount = 0 To dgvMain.RowCount - 1
                    With dgvMain(ColLoopCount, RowLoopCount)
                        Select Case dgvMain.Columns(ColLoopCount).Name
                            Case "DOB"
                                Dim DateTest As DateTime
                                If .Value <> "" And (.Value.ToString.Length > MaxSize Or Date.TryParseExact(.Value, "yyyyMMdd", Globalization.CultureInfo.CurrentCulture, Globalization.DateTimeStyles.None, DateTest) = False) Then
                                    .Style.BackColor = Color.Red
                                    DataValid = False
                                Else
                                    .Style.BackColor = Color.White
                                End If
                            Case "NI" ' added TS 26/Mar/2013
                                If Not IsDBNull(.Value) AndAlso .Value <> "" AndAlso Not IsNINumber(.Value) Then
                                    .Style.BackColor = Color.Red
                                    DataValid = False
                                Else
                                    .Style.BackColor = Color.White
                                End If
                            Case Else ' The majority of fields are validate by length only
                                If .Value.ToString.Length > MaxSize Then
                                    .Style.BackColor = Color.Red
                                    DataValid = False
                                Else
                                    .Style.BackColor = Color.White
                                End If
                        End Select
                    End With
                Next RowLoopCount
            Next DataColumn

            ' This section added TS 02/Sep/2014. Request ref 29548
            For Each ReturnCase As DataRowView In Returns.Debtors
                If IsDBNull(ReturnCase.Item("clientReturnNumber")) Then
                    If Not IsNothing(ClipboardText) Then ClipboardText &= ","
                    ClipboardText &= ReturnCase.Item("DebtorID")
                    DataValid = False
                End If
            Next ReturnCase

            If Not IsNothing(ClipboardText) Then
                Clipboard.SetText(ClipboardText, TextDataFormat.Text)
                MsgBox("Cases have missing return codes." & vbCrLf & "IDs copied to clipboard.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, Me.Text)
            End If
            ' End of new section

            If Not DataValid Then MsgBox("Invalid data.", vbOKOnly + vbCritical, Me.Text)

            btnCreateReturns.Enabled = DataValid

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        Return DataValid
    End Function

    Private Sub btnCreateReturns_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateReturns.Click
        Dim ExceptionLog As String = ""
        Dim RecordCount As Integer = 0, LastReturnNumber As Integer = -1

        ' Try

        'If Returns.Debtors.Count = 0 Then
        '    MsgBox("No cases found")
        '    Return
        'End If

        Dim FolderBrowserDialog As New FolderBrowserDialog

        If FolderBrowserDialog.ShowDialog() <> Windows.Forms.DialogResult.OK Then
            Return
        End If

        ' Now start creating the file
        Dim FileName As String = "01_" & DateTime.Today.ToString("yyyyMMdd") & "_" & "AGY023I.xml"

        AuditFile = FolderBrowserDialog.SelectedPath & "\Audit.txt"
        Dim ReturnsFile As New Xml.XmlTextWriter(FolderBrowserDialog.SelectedPath & "\" & FileName, System.Text.Encoding.UTF8)      'text.Encoding.Unicode)
        ReturnsFile.Formatting = Xml.Formatting.Indented

        ReturnsFile.WriteStartDocument()

        ReturnsFile.WriteStartElement("AgencyReturnsToDM")

        ReturnsFile.WriteAttributeString("xmlns", "http://www.fico.com/xml/debtmanager/data/v1_0")
        ReturnsFile.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")

        ReturnsFile.WriteStartElement("HEADER")
        ReturnsFile.WriteElementString("DCA_CODE", DCACode)
        ReturnsFile.WriteElementString("AGENCY_SHORT_NAME", AgencyShortName)
        ReturnsFile.WriteElementString("FILE_DATE", DateTime.Now.ToString("yyyy-MM-dd"))
        ReturnsFile.WriteElementString("TIME_CREATED", DateTime.Now.ToString("HH:mm:ss"))
        ReturnsFile.WriteElementString("SEQ_NBR", txtFileSequenceNumber.Text)
        ReturnsFile.WriteEndElement() ' HEADER

        ReturnsFile.WriteStartElement("DETAILS")

        For Each Debtor As DataRowView In Returns.Debtors

            Dim ClientReturnNumber As Integer ' the following section added TS 12/Jan/2016. Request ref 70720
            If Not Integer.TryParse(Debtor.Item("clientReturnNumber"), ClientReturnNumber) Then
                Clipboard.SetText(Debtor("DebtorID").ToString, TextDataFormat.Text)
                MessageBox.Show("Invalid return number on case " & Debtor("DebtorID").ToString, Me.Text, MessageBoxButtons.OK, MessageBoxIcon.Error)
                Return
            End If ' end of Request ref 70720 section

            If LastReturnNumber <> Debtor.Item("clientReturnNumber") Then
                If LastReturnNumber <> -1 Then ReturnsFile.WriteEndElement()
                ReturnsFile.WriteStartElement("AGENCY_RETURN_CODE_" & Debtor.Item("clientReturnNumber").ToString.PadLeft(2, "0"))
                LastReturnNumber = Debtor.Item("clientReturnNumber")
            End If

            ReturnsFile.WriteStartElement("ACCOUNT_" & Debtor("clientReturnNumber").ToString.PadLeft(2, "0"))

            ReturnsFile.WriteElementString("DebtAccountNumber", Debtor("client_ref"))
            ReturnsFile.WriteElementString("ReturnCode", Debtor("clientReturnNumber"))
            ReturnsFile.WriteElementString("DCACODE", DCACode)
            ReturnsFile.WriteElementString("AgencyShortName", AgencyShortName)

            Select Case Debtor.Item("clientReturnNumber")

                Case 8

                    Returns.Addresses.RowFilter = "DebtorID = " & Debtor("DebtorID")

                    If Returns.Addresses.Item(0).Item("Phone") <> "" Then
                        ReturnsFile.WriteStartElement("Telephone")
                        ReturnsFile.WriteElementString("TelephoneNo", Returns.Addresses.Item(0).Item("Phone"))
                        If Returns.Addresses.Item(0).Item("Phone").ToString.StartsWith("07") Then
                            ReturnsFile.WriteElementString("TelephoneType", "MO")
                        Else
                            ReturnsFile.WriteElementString("TelephoneType", "HO")
                        End If
                        ReturnsFile.WriteEndElement() ' Telephone
                    End If

                    If Returns.Addresses.Item(0).Item("name_title") <> "" Or Returns.Addresses.Item(0).Item("name_fore") <> "" Or Returns.Addresses.Item(0).Item("name_sur") <> "" Then
                        ReturnsFile.WriteStartElement("Name")
                        If Returns.Addresses.Item(0).Item("name_title") <> "" Then
                            ReturnsFile.WriteElementString("Title", Returns.Addresses.Item(0).Item("name_title"))
                        Else
                            ReturnsFile.WriteElementString("Title", "--")
                        End If

                        If Returns.Addresses.Item(0).Item("name_fore") <> "" Then
                            ReturnsFile.WriteElementString("Forename", Returns.Addresses.Item(0).Item("name_fore"))
                        Else
                            ReturnsFile.WriteElementString("Forename", "--")
                        End If

                        If Returns.Addresses.Item(0).Item("name_sur") <> "" Then
                            ReturnsFile.WriteElementString("Surname", Returns.Addresses.Item(0).Item("name_sur"))
                        Else
                            ReturnsFile.WriteElementString("Surname", "--")
                        End If

                        ReturnsFile.WriteEndElement() ' Name
                    End If

                    Dim Address As String = Returns.Addresses.Item(0).Item("ApartmentNumber") & Returns.Addresses.Item(0).Item("HouseName") & Returns.Addresses.Item(0).Item("HouseNumber") & Returns.Addresses.Item(0).Item("SubStreetName") & _
                       Returns.Addresses.Item(0).Item("Street") & Returns.Addresses.Item(0).Item("SubDistrict") & Returns.Addresses.Item(0).Item("District") & Returns.Addresses.Item(0).Item("Town") & _
                       Returns.Addresses.Item(0).Item("County") & Returns.Addresses.Item(0).Item("County") & Returns.Addresses.Item(0).Item("PostCode")

                    If Address.Length > 0 Then

                        ReturnsFile.WriteStartElement("Address")

                        If Returns.Addresses.Item(0).Item("ApartmentNumber") <> "" Then ReturnsFile.WriteElementString("ApartmentNumberOrName", Returns.Addresses.Item(0).Item("ApartmentNumber"))
                        If Returns.Addresses.Item(0).Item("HouseName") <> "" Then ReturnsFile.WriteElementString("HouseName", Returns.Addresses.Item(0).Item("HouseName"))
                        If Returns.Addresses.Item(0).Item("HouseNumber") <> "" Then ReturnsFile.WriteElementString("HouseNumber", Returns.Addresses.Item(0).Item("HouseNumber"))
                        If Returns.Addresses.Item(0).Item("SubStreetName") <> "" Then ReturnsFile.WriteElementString("SubStreetName", Returns.Addresses.Item(0).Item("SubStreetName"))
                        If Returns.Addresses.Item(0).Item("Street") <> "" Then ReturnsFile.WriteElementString("StreetName", Returns.Addresses.Item(0).Item("Street"))
                        If Returns.Addresses.Item(0).Item("SubDistrict") <> "" Then ReturnsFile.WriteElementString("SubDistrictName", Returns.Addresses.Item(0).Item("SubDistrict"))
                        If Returns.Addresses.Item(0).Item("District") <> "" Then ReturnsFile.WriteElementString("VillageOrDistrictName", Returns.Addresses.Item(0).Item("District"))
                        If Returns.Addresses.Item(0).Item("Town") <> "" Then ReturnsFile.WriteElementString("TownName", Returns.Addresses.Item(0).Item("Town"))
                        If Returns.Addresses.Item(0).Item("County") <> "" Then ReturnsFile.WriteElementString("CountyName", Returns.Addresses.Item(0).Item("County"))
                        If Returns.Addresses.Item(0).Item("PostCode") <> "" Then ReturnsFile.WriteElementString("PostalCode", Returns.Addresses.Item(0).Item("PostCode"))

                        ReturnsFile.WriteEndElement() ' Address
                    End If

                    If Returns.Addresses.Item(0).Item("DOB") <> "" Then ReturnsFile.WriteElementString("DateOfBirth", Returns.Addresses.Item(0).Item("DOB"))

                Case 11
                    ReturnsFile.WriteElementString("BalanceToBeWrittenOff", Debtor("debt_balance").ToString)
                    If Not IsDBNull(Debtor("return_details")) Then ReturnsFile.WriteElementString("AdditionalComments", Debtor("return_details"))

                Case 61, 62
                    If Debtor.Item("jsaOffice") <> "" Then
                        ReturnsFile.WriteElementString("BenefitType", Debtor.Item("jsaOffice"))
                    Else
                        ReturnsFile.WriteElementString("BenefitType", "NK")
                    End If

                    If IsNINumber(Debtor("empNI").ToString) Then
                        ReturnsFile.WriteElementString("NINO", Debtor.Item("empNI"))
                    Else
                        ExceptionLog &= "Invalid NI number found on case " & Debtor("DebtorID").ToString & vbCrLf
                    End If

                Case 65, 66
                    If IsDBNull(Debtor("arrange_amount")) Or IsDBNull(Debtor("arrange_interval")) Or IsDBNull(Debtor("arrange_next")) Then
                        ExceptionLog &= "Incomplete arrangement details on case " & Debtor("DebtorID").ToString & ". Case not returned." & vbCrLf
                    Else
                        ReturnsFile.WriteElementString("PayPlanAmount", Debtor("arrange_amount"))

                        Select Case Debtor("AgentID")
                            Case 2358
                                ReturnsFile.WriteElementString("PaymentMethod", "DD")
                            Case 2329
                                ReturnsFile.WriteElementString("PaymentMethod", "PC")
                            Case Else
                                ReturnsFile.WriteElementString("PaymentMethod", "OT")
                        End Select

                        Select Case Debtor.Item("arrange_interval")
                            Case 0 To 7
                                ReturnsFile.WriteElementString("PaymentFrequency", "WE")
                            Case 8 To 14
                                ReturnsFile.WriteElementString("PaymentFrequency", "FO")
                            Case 28
                                ReturnsFile.WriteElementString("PaymentFrequency", "FW")
                            Case Else
                                ReturnsFile.WriteElementString("PaymentFrequency", "MO")
                        End Select

                        Dim PaymentDay As String = CDate(Debtor("arrange_next").ToString).ToString("ddd").ToUpper
                        If PaymentDay = "SAT" Or PaymentDay = "SUN" Then PaymentDay = "MON"
                        ReturnsFile.WriteElementString("PaymentDay", PaymentDay)

                    End If

                Case 82 To 89
                    If Not IsDBNull(Debtor("return_details")) Then ReturnsFile.WriteElementString("AdditionalComments", Debtor("return_details"))

                Case Else

            End Select

            ReturnsFile.WriteEndElement() ' ACCOUNT_

            RecordCount += 1

        Next Debtor

        ReturnsFile.WriteEndElement() ' close the last AGENCY_RETURN_CODE_ block

        ReturnsFile.WriteEndElement() ' DETAILS

        ReturnsFile.WriteStartElement("TRAILER")
        ReturnsFile.WriteElementString("DCACODE", DCACode)
        ReturnsFile.WriteElementString("AgencyShortName", AgencyShortName)
        ReturnsFile.WriteElementString("Agency_Record_Count", RecordCount.ToString)
        ReturnsFile.WriteEndElement() ' TRAILER

        ReturnsFile.WriteEndElement()

        ReturnsFile.WriteEndDocument()

        ReturnsFile.Close()

        Returns.Addresses.RowFilter = "" ' Update screen

        'validate using xsd
        Dim myDocument As New Xml.XmlDocument
        myDocument.Load(FolderBrowserDialog.SelectedPath & "\" & FileName)
        myDocument.Schemas.Add("http://www.fico.com/xml/debtmanager/data/v1_0", "\\ross-helm-fp001\Rossendales Shared\vb.net\UU XSD\TestInboundXSD.xsd")
        Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
        myDocument.Validate(eventHandler)
        WriteAudit(ErrorNo & " - errors in report finished at " & Now, False)
        'If error_no = 0 Then

        If ExceptionLog <> "" Then
            Dim Cancel As Boolean = False
            WriteFile(FolderBrowserDialog.SelectedPath & "\" & "Exceptions.txt", ExceptionLog)

            If MsgBox("Exceptions found. Create file and increment sequence?", vbCritical + vbYesNo + vbDefaultButton2, Me.Text) = vbNo Then
                Cancel = True
                MsgBox("File not created", vbOKOnly, Me.Text)
            End If

            System.Diagnostics.Process.Start(FolderBrowserDialog.SelectedPath & "\Exceptions.txt")

            If Cancel Then Return
        End If

        If ConfigurationManager.AppSettings("Environment") = "Prod" Then ' Added TS 11/Apr/2013
            ' Added DateFrom and DateTo TS 11/Mar/2015. Request ref 44285
            Returns.SetLastFileSequence(txtFileSequenceNumber.Text, Returns.Debtors.Count, DateFrom, DateTo) ' FileSequenceNumber replaced by txtFileSequenceNumber.Text TS 05/Aug/2014. Request ref 27729
        Else
            MsgBox("Test version. Last file sequence not set.", vbInformation + vbOKOnly, Me.Text)
        End If

        MsgBox("Returns file created.", vbOKOnly, Me.Text)

        System.Diagnostics.Process.Start(FolderBrowserDialog.SelectedPath & "\" & FileName)

        'Catch ex As Exception
        '    HandleException(ex)
        'End Try
    End Sub

    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        Select Case e.Severity
            Case XmlSeverityType.Error
                WriteAudit("Error: " & e.Message, True)
            Case XmlSeverityType.Warning
                WriteAudit("Warning " & e.Message, True)
        End Select
    End Sub

    Private Sub WriteAudit(ByVal AuditMessage As String, ByVal ErrorMessage As Boolean)
        If ErrorMessage Then
            ErrorNo += 1
        End If

        My.Computer.FileSystem.WriteAllText(AuditFile, AuditMessage & vbNewLine, True)
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub

    Private Sub txtFileSequenceNumber_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtFileSequenceNumber.Validating
        Try
            If Not IsNumeric(txtFileSequenceNumber.Text) Then
                MsgBox("Please enter a valid file sequence number.", vbOKOnly + vbCritical, Me.Text)
                e.Cancel = True
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class

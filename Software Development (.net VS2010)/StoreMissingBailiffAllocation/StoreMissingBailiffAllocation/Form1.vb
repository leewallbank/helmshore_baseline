﻿Imports CommonLibrary
'Imports System.Configuration
'Imports System.IO
Public Class MainForm
    Dim bail_dt As New DataTable
    Dim null_date As Date = CDate("Jan 1, 1900")
    Dim fourtyDaysAgo As Date = DateAdd(DateInterval.Day, -40, Now)
    'Dim eightyDaysAgo As Date = DateAdd(DateInterval.Day, -80, Now)


    Private Sub MainForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim clientArray As Object()
        ConnectDb2("DebtRecoveryLocal")
        ConnectDb2Fees("FeesSQL")
        Dim start_date As Date = CDate("2014 Apr 6")
        Dim upd_txt As String
        'upd_txt = "delete from BailiffAllocationTable " & _
        '          " WHERE bail_seq_no =789605 "
        'update_sql(upd_txt)
        'delete duplicate rows in BAT
        Dim bailDup_dt As New DataTable
        LoadDataTable2("FeesSQL", "SELECT bail_seq_no, bail_ID, bail_allocation_date, bail_de_allocation_found_date, bail_debtorID " & _
                                        "FROM BailiffAllocationTable " & _
                                        " order by bail_seq_no", bailDup_dt, False)
        For Each bailDupRow In bailDup_dt.Rows
            Dim SeqNo As Long = bailDupRow(0)
            Dim bailID As Integer = bailDupRow(1)
            Dim AllocatedDate As Date = bailDupRow(2)
            Dim deAllocationDate As Date = bailDupRow(3)
            Dim debtorID As Integer = bailDupRow(4)
            'see if duplicate 
            Dim bailDup2_dt As New DataTable
            LoadDataTable2("FeesSQL", "SELECT bail_seq_no, bail_de_allocation_found_date " & _
                                            "FROM BailiffAllocationTable " & _
                                            " where bail_seq_no <> " & SeqNo & _
                                            " and bail_ID = " & bailID & _
                                            " and bail_debtorID = " & debtorID & _
                                            " and bail_allocation_date = '" & Format(AllocatedDate, "yyyy-MM-dd") & "'", bailDup2_dt, False)
            For Each bailDup2Row In bailDup2_dt.Rows
                Dim SeqNo2 As Long = bailDup2Row(0)
                Dim delSeqNo As Integer = SeqNo2
                Dim deAllocationDate2 As Date = bailDup2Row(1)
                If deAllocationDate2 > deAllocationDate Then
                    delSeqNo = SeqNo
                End If
                'delete duplicate
                upd_txt = "delete from BailiffAllocationTable " & _
                   " WHERE bail_seq_no = " & delSeqNo
                update_sql(upd_txt)
            Next
        Next
        'MsgBox("done - remove later")
        Me.Close()
    End Sub
End Class

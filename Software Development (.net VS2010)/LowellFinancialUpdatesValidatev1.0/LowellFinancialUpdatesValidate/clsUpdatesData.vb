﻿Imports commonlibrary

Public Class clsUpdatesData
    Private LastReturnTable As New DataTable

    Public ReadOnly Property LastReturnDetails() As LastReturnDetails
        Get

            LastReturnDetails = Nothing

            Try

                LastReturnDetails = New LastReturnDetails
                LastReturnDetails.FileSequence = LastReturnTable.Rows(0).Item("FileSequence")

            Catch ex As Exception
                HandleException(ex)
            End Try
        End Get
    End Property

    Public Function GetCaseDetails(ByVal ClientRef As String) As Object()

        GetCaseDetails = Nothing

        Try

            GetCaseDetails = GetSQLResultsArray("DebtRecovery", "SELECT d._rowID, d.debt_balance, cs.fee_comm1 " & _
                                                                "FROM debtor AS d " & _
                                                                "INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID " & _
                                                                "WHERE d.client_ref = '" & ClientRef & "' AND cs.clientID = 1815")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Sub GetLastRunDetails()
        Dim Sql As String

        Try
            Sql = "SELECT COUNT(*) AS FileSequence " & _
                  "FROM dbo.LowellInbound " & _
                  "WHERE CreatedDate > DATEADD(Day, DATEDIFF(DAY, 0, GETDATE()), 0) " & _
                  "  AND FileType = 'OFA_CONF'"

            LoadDataTable("FeesSQL", Sql, LastReturnTable)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub SetLastFileSequence(ByVal FileSequence As String, FileName As String, ByVal CasesLoaded As Integer)
        Try

            ExecStoredProc("FeesSQL", " EXEC dbo.SetLowellInbound 'OFA_CONF'," & FileSequence & ", '" & FileName & "', " & CasesLoaded.ToString & ", NULL, NULL")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

Public Class LastReturnDetails
    Public Property FileSequence As Integer
End Class

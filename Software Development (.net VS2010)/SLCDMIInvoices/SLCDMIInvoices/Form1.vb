﻿Imports System.IO

Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        Dim testValue As Decimal
        Try
            testValue = ndir_value.Text
        Catch ex As Exception
            MsgBox("Invalid amount for Non-direct transaction value")
            Exit Sub
        End Try
        Try
            testValue = dir_value.Text
        Catch ex As Exception
            MsgBox("Invalid amount for Direct transaction value")
            Exit Sub
        End Try
        runbtn.Enabled = False
        exitbtn.Enabled = False
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RD253APreport = New RD253AP
        Dim myArrayList1 As ArrayList = New ArrayList()
        RD253APreport.SetParameterValue("start_date", start_dtp.Value)
        RD253APreport.SetParameterValue("end_date", end_dtp.Value)
        RD253APreport.SetParameterValue("trans_name", ndir_filename.Text)
        RD253APreport.SetParameterValue("trans_value", ndir_value.Text)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253APreport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "PDF files |*.pdf"
            .DefaultExt = ".pdf"
            .OverwritePrompt = True
            .FileName = "RD253A TDX SLC non-direct invoice.pdf"
        End With

        If SaveFileDialog1.ShowDialog <> Windows.Forms.DialogResult.OK Then
            MsgBox("Reports not run")
            Exit Sub
        End If
        Dim pathname As String = Path.GetDirectoryName(SaveFileDialog1.FileName) & "\"
        RD253APreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, SaveFileDialog1.FileName)
        RD253APreport.Close()

        Dim RD253ADPreport = New RD253ADP
        RD253ADPreport.SetParameterValue("start_date", start_dtp.Value)
        RD253ADPreport.SetParameterValue("end_date", end_dtp.Value)
        RD253ADPreport.SetParameterValue("trans_name", dir_filename.Text)
        RD253ADPreport.SetParameterValue("trans_value", dir_value.Text)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD253ADPreport)
        Dim filename As String = pathname & "RD253AD TDX SLC direct invoice.pdf"
        RD253ADPreport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD253ADPreport.Close()

        Dim RD448F2report = New RD448F2
        'Dim myArrayList1 As ArrayList = New ArrayList()
        myArrayList1.Add(start_dtp.Value)
        SetCurrentValuesForParameterField1(RD448F2report, myArrayList1)
        myArrayList1.Add(end_dtp.Value)
        SetCurrentValuesForParameterField2(RD448F2report, myArrayList1)
        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RD448F2report)
        filename = pathname & "RD448F2 TDX SLC Fee invoice.pdf"
        RD448F2report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, filename)
        RD448F2report.Close()
        MsgBox("Reports saved")
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        start_dtp.Value = DateAdd(DateInterval.Day, -4 - Weekday(Now), Now)
        end_dtp.Value = DateAdd(DateInterval.Day, -Weekday(Now) + 2, Now)
    End Sub
End Class

﻿Imports System.IO
Imports System.Configuration
Imports CommonLibrary

Public Class frmMain
    Private SevernTrentSelectData As New SevernTrentSelectData
    Const Separator As String = "|"
    Private ConnCode As New Dictionary(Of String, Integer)
    Private AccountType As New Dictionary(Of String, String)

    Private ConnID As String() = {"4861", "4862", "4863"}
    Private InputFilePath As String, PlacementFileName As String, PlacementFileExt As String, ClientSupportFilePath As String
    Private OutputFiles As New ArrayList()
    Private NewCases As New ArrayList()

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        ConnCode.Add("AGY", 0)
        ConnCode.Add("NGY", 1)
        ConnCode.Add("LGY", 2)

        AccountType.Add("ME", "Measured water services (metered)")
        AccountType.Add("UN", "Unmeasured water services (un metered)")
        AccountType.Add("MI", "Mixed water services (metered and un metered)")
        AccountType.Add("TR", "Trade effluent (Industrial Waste)")
        AccountType.Add("SU", "Sundry (Miscellaneous")

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        
        Dim ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String, InputLineArray() As String, ConnKey As String, MBCNotes As String = ""
        Dim Balance As Decimal, TotalNewDebt As Decimal
        Dim NewDebtFile As String ' Dim NewDebtFile(11) As String TS 24/Sep/2014. Request ref 31419
        Dim NewDebtSumm(2, 1) As Decimal
        Dim MissingChargeEndDateFile As New ArrayList()
        Dim LastFileSequence As Long, FileSequence As Long
        Dim LineNumber As Integer, DetailCount As Integer, PreviouslyLoadedCount As Integer = 0 ' added TS 20/Aug/2016. Request ref 82644
        Dim HasHeader As Boolean = False

        Try
            
            Dim FileDialog As New OpenFileDialog
            FileDialog.Filter = "STS placement files|DCA_Placement_*.csv"
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
            PlacementFileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            PlacementFileExt = Path.GetExtension(FileDialog.FileName)
            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            ClientSupportFilePath = InputFilePath & "\To Client Support\"
            InputFilePath &= "\"

            For NewDebtFileCount As Integer = 0 To UBound(NewDebtSumm)
                If File.Exists(InputFilePath & ConnID(NewDebtFileCount) & "_" & PlacementFileName & "_NewDebt.txt") Then File.Delete(InputFilePath & ConnID(NewDebtFileCount) & "_" & PlacementFileName & "_NewDebt.txt")
            Next NewDebtFileCount

            If File.Exists(InputFilePath & PlacementFileName & "_PreviouslyLoaded.txt") Then File.Delete(InputFilePath & PlacementFileName & "_PreviouslyLoaded.txt")

            Dim FileContents() As String = System.IO.File.ReadAllLines(InputFilePath & PlacementFileName & PlacementFileExt)
            ProgressBar.Maximum = UBound(FileContents) + 8

            LastFileSequence = SevernTrentSelectData.GetLastFileSequence()

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                Application.DoEvents() ' without this line, the button disappears until processing is complete

                InputLine.Replace("|", "")
                InputLineArray = InputLine.Split(";")

                Select Case InputLineArray(0)

                    Case "N" ' coded first for performance
                        DetailCount += 1

                        NewDebtFile = ""

                        'Check for mandatory fields first
                        If InputLineArray(4) = "" Then ExceptionLog &= "No account number supplied at line number " & LineNumber.ToString & vbCrLf
                        If InputLineArray(5) = "" Then ExceptionLog &= "No customer name supplied at line number " & LineNumber.ToString & vbCrLf
                        If InputLineArray(12) = "" Then ExceptionLog &= "No customer postcode supplied at line number " & LineNumber.ToString & vbCrLf ' was 10. TS 12/Aug/2015. Request ref 54691
                        If InputLineArray(26) = "" Then ExceptionLog &= "No account balance supplied at line number " & LineNumber.ToString & vbCrLf ' was 22. TS 12/Aug/2015. Request ref 54691
                        If InputLineArray(27) = "" Then ExceptionLog &= "No charge end date supplied at line number " & LineNumber.ToString & vbCrLf ' was 23. TS 12/Aug/2015. Request ref 54691

                        ' Identify the client scheme
                        ConnKey = InputLineArray(1)
                        If ConnCode.ContainsKey(ConnKey) Then

                            If AccountType.ContainsKey(InputLineArray(17)) Then ' was 15. TS 12/Aug/2015. Request ref 54691
                                InputLineArray(17) = AccountType(InputLineArray(17)) ' was 15. TS 12/Aug/2015. Request ref 54691
                            Else
                                ErrorLog &= "Unknown account type of " & InputLineArray(17) & " at line number " & LineNumber.ToString & vbCrLf ' was 15. TS 12/Aug/2015. Request ref 54691
                            End If

                            'NewDebtFile &= Join(InputLineArray, Separator) & Separator commented out TS 07/Sep/2015. Request ref 54691
                            ' This section added TS 07/Sep/2015. Request ref 54691
                            Dim OutputLineArray(UBound(InputLineArray)) As String

                            For LoopCount As Integer = 0 To UBound(OutputLineArray)
                                OutputLineArray(LoopCount) = InputLineArray(LoopCount)
                            Next LoopCount

                            OutputLineArray(6) = ConcatFields({InputLineArray(13), InputLineArray(6), InputLineArray(7), InputLineArray(8), InputLineArray(9)}, " ")

                            OutputLineArray(18) = ConcatFields({InputLineArray(25), InputLineArray(18), InputLineArray(19), InputLineArray(20), InputLineArray(21)}, " ")

                            ' end of new section

                            NewDebtFile = String.Join(Separator, OutputLineArray) & Separator


                            NewDebtFile &= ToNote(InputLineArray(2), "NFA Indicator")(0)
                            NewDebtFile &= ToNote(InputLineArray(3), "FA Indicator")(0)
                            NewDebtFile &= ToNote(InputLineArray(14), "Contact Name")(0) ' was 12. TS 12/Aug/2015. Request ref 54691
                            NewDebtFile &= ToNote(InputLineArray(17), "Account Type")(0) ' was 15. TS 12/Aug/2015. Request ref 54691
                            NewDebtFile &= ToNote(If(InputLineArray(27) <> "", Date.ParseExact(InputLineArray(27), "ddMMyyyy", Nothing).ToString("dd/MMM/yyyy"), ""), "Charge End Date")(0) ' was 23. TS 12/Aug/2015. Request ref 54691
                            NewDebtFile &= ToNote(If(InputLineArray(28) <> "", Date.ParseExact(InputLineArray(28), "ddMMyyyy", Nothing).ToString("dd/MMM/yyyy"), ""), "Charge Start Date")(0) ' was 24. TS 12/Aug/2015. Request ref 54691
                            NewDebtFile &= ToNote(CDec(InputLineArray(29)).ToString, "Current Year Debt")(0) ' was 25. TS 12/Aug/2015. Request ref 54691
                            NewDebtFile &= ToNote(CDec(InputLineArray(30)).ToString, "Previous Year Debt")(0) ' was 26. TS 12/Aug/2015. Request ref 54691
                            NewDebtFile &= ToNote(CDec(InputLineArray(31)).ToString, "Legacy Debt")(0) ' was 27. TS 12/Aug/2015. Request ref 54691
                            NewDebtFile &= ToNote(If(InputLineArray(32) <> "", Date.ParseExact(InputLineArray(32), "ddMMyyyy", Nothing).ToString("dd/MMM/yyyy"), ""), "Latest Bill Date")(0) ' was 28. TS 12/Aug/2015. Request ref 54691
                            NewDebtFile &= ToNote(CDec(InputLineArray(33)).ToString, "Latest Bill Value")(0) ' was 29. TS 12/Aug/2015. Request ref 54691
                            NewDebtFile &= ToNote(If(InputLineArray(34) <> "", Date.ParseExact(InputLineArray(34), "ddMMyyyy", Nothing).ToString("dd/MMM/yyyy"), ""), "Last Payment Date")(0) ' was 30. TS 12/Aug/2015. Request ref 54691
                            NewDebtFile &= ToNote(CDec(InputLineArray(35)).ToString, "Last Payment Amount")(0) ' was 31. TS 12/Aug/2015. Request ref 54691

                            NewDebtFile &= vbCrLf

                            If SevernTrentSelectData.GetCaseLive(InputLineArray(4)) > 0 Then
                                ExceptionLog &= "Client ref " & InputLineArray(4) & " already loaded at line number " & LineNumber.ToString & vbCrLf
                                AppendToFile(InputFilePath & PlacementFileName & "_PreviouslyLoaded.txt", InputLine & vbCrLf) ' added TS 20/Aug/2016. Request ref 82644
                                PreviouslyLoadedCount += 1 ' added TS 20/Aug/2016. Request ref 82644
                            Else ' added TS 20/Aug/2016. Request ref 82644
                                If InputLineArray(27) = "" Then MissingChargeEndDateFile.Add({InputLineArray(4)}) ' was 23. TS 12/Aug/2015. Request ref 54691
                                Balance += InputLineArray(26) ' was 22. TS 12/Aug/2015. Request ref 54691
                                NewDebtSumm(ConnCode(ConnKey), 0) += 1
                                NewDebtSumm(ConnCode(ConnKey), 1) += InputLineArray(26) ' was 22. TS 12/Aug/2015. Request ref 54691
                                NewCases.Add(InputLineArray(4))

                                AppendToFile(InputFilePath & ConnID(ConnCode(ConnKey)) & "_" & PlacementFileName & "_NewDebt.txt", NewDebtFile)
                            End If ' added TS 20/Aug/2016. Request ref 82644
                        Else
                            ErrorLog &= "Unable to identify client scheme at line number " & LineNumber.ToString & ". DCA marker: " & InputLineArray(1) & vbCrLf
                        End If

                    Case "H"
                        HasHeader = True
                        If DetailCount <> 0 Then ErrorLog &= "Header not at the start of section at line number " & LineNumber.ToString & vbCrLf
                        FileSequence = CLng(InputLineArray(4))
                        If FileSequence <= LastFileSequence Then ErrorLog &= "Unexpected file sequence at line number " & LineNumber.ToString & ". Found in file " & FileSequence.ToString & ", last file sequence preprocessed was " & LastFileSequence.ToString & vbCrLf

                    Case "New Charge Type"
                        'ignore column headers

                    Case Else
                        ErrorLog &= "Unknown record type of '" & InputLine.Substring(6, 1) & "'at line number " & LineNumber.ToString & ". Line cannot be processed." & vbCrLf
                End Select
            Next InputLine

            ProgressBar.Value += 1
            AuditLog = "File processed: " & InputFilePath & PlacementFileName & PlacementFileExt & vbCrLf
            AuditLog &= "File sequence number: " & FileSequence & vbCrLf & vbCrLf

            AuditLog &= "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Number of previously loaded cases: " & PreviouslyLoadedCount.ToString & vbCrLf ' added TS 20/Aug/2016. Request ref 82644
            AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            For NewDebtFileCount As Integer = 0 To UBound(NewDebtSumm)
                ProgressBar.Value += 1
                If File.Exists(InputFilePath & ConnID(NewDebtFileCount) & "_" & PlacementFileName & "_NewDebt.txt") Then OutputFiles.Add(InputFilePath & ConnID(NewDebtFileCount) & "_" & PlacementFileName & "_NewDebt.txt")
                AuditLog &= "Clientscheme " & ConnID(NewDebtFileCount) & vbCrLf
                AuditLog &= "Number of new cases: " & NewDebtSumm(NewDebtFileCount, 0).ToString & vbCrLf
                AuditLog &= "Total balance of new cases: " & NewDebtSumm(NewDebtFileCount, 1).ToString & vbCrLf & vbCrLf
                TotalNewDebt += NewDebtSumm(NewDebtFileCount, 1)
            Next NewDebtFileCount

            If ConfigurationManager.AppSettings("Environment") = "Prod" Then
                SevernTrentSelectData.SetLastFileSequence(FileSequence, NewCases.Count, TotalNewDebt)
            Else
                MsgBox("Test version. Last file sequence not set.", vbInformation + vbOKOnly, Me.Text)
            End If

            ProgressBar.Value += 1
            If MissingChargeEndDateFile.Count > 0 Then
                OutputToSeparatedFile(ClientSupportFilePath, PlacementFileName & "_MissingChargeEndDate.txt", "|", Nothing, MissingChargeEndDateFile)
                OutputFiles.Add(ClientSupportFilePath & PlacementFileName & "_MissingChargeEndDate.txt")
            End If

            WriteFile(InputFilePath & PlacementFileName & "_Audit.txt", AuditLog)

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & PlacementFileName & "_Error.txt", ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            If ExceptionLog <> "" Then WriteFile(InputFilePath & PlacementFileName & "_Exception.txt", ExceptionLog)
            If MBCNotes <> "" Then WriteFile(ClientSupportFilePath & PlacementFileName & "_MBCNotes.txt", MBCNotes)

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub

    Private Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, Optional ByVal NoteLength As Integer = 250) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 2) ' + 2 is for :;
        Dim PreviousLength As Integer
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = (NoteTag & ":" & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)) & ";").PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Clipboard.SetText("notepad.exe " & InputFilePath & PlacementFileName & PlacementFileExt)
        If File.Exists(InputFilePath & PlacementFileName & PlacementFileExt) Then System.Diagnostics.Process.Start("wordpad.exe", InputFilePath & PlacementFileName & PlacementFileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If File.Exists(InputFilePath & PlacementFileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & PlacementFileName & "_Audit.txt")
        If File.Exists(InputFilePath & PlacementFileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & PlacementFileName & "_Error.txt")
        If File.Exists(InputFilePath & PlacementFileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & PlacementFileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub
End Class



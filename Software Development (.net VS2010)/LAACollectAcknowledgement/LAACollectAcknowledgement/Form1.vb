﻿Imports CommonLibrary
Imports System.Configuration
Imports System.Xml.Schema
Imports System.IO

Public Class Form1
    Dim xml_valid As Boolean = True
    Dim error_no As Integer
    Dim logfile As String


    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles runbtn.Click
        runbtn.Enabled = False
        exitbtn.Enabled = False
        Dim filename As String = "Agency_placement_S1098_Ack_" & Format(loadDTP.Value, "yyyyMMdd") & ".xml"

        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XML files |*.xml"
            .DefaultExt = ".xml"
            .OverwritePrompt = True
            .FileName = filename
        End With
        If Not SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            MsgBox("No file name selected for save ")
            Return
        End If
        logfile = Path.GetDirectoryName(SaveFileDialog1.FileName) & "\Agency_placement_logfile.txt"
        Dim writer As New Xml.XmlTextWriter(SaveFileDialog1.FileName, System.Text.Encoding.ASCII)      'text.Encoding.Unicode)
        writer.Formatting = Xml.Formatting.Indented
        writer.WriteStartElement("RossAck")

        Dim startDate As Date = CDate(Format(loadDTP.Value, "yyyy, dd MMM") & " 00:00:00")
        Dim endDate As Date = DateAdd(DateInterval.Day, 1, startDate)
        'get csids for cl=1045
        Dim cs_dt As New DataTable
        Dim cases As Integer = 0
        LoadDataTable("DebtRecovery", "select _rowid " & _
                      " from clientScheme" & _
                      " where clientID = 1045", cs_dt, False)
        For Each csRow In cs_dt.Rows
            Dim CSID As Integer = csRow(0)
            'only include certain csids now
            If (CSID < 4431 Or CSID > 4434) And CSID <> 4564 And CSID <> 4581 Then Continue For

            'now get cases
            Dim debt_dt As New DataTable
            LoadDataTable("DebtRecovery", "Select _rowid, client_ref,name_fore, name_sur, address, add_postcode, debt_original" & _
                           " status_open_closed, return_date" & _
                          " from Debtor" & _
                          " where clientschemeID = " & CSID & _
                          " and _createdDate >='" & Format(startDate, "yyyy-MM-dd") & "'" & _
                          " and _createdDate < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                          " order by _rowid", debt_dt, False)
            For Each debtRow In debt_dt.Rows
                cases += 1
                writer.WriteStartElement("CaseDetails")
                Dim debtorID As Integer = debtRow(0)
                writer.WriteElementString("OurRef", debtorID)
                Dim clientRef As String = debtRow(1)
                writer.WriteElementString("ClientRef", clientRef)
                Dim name As String = ""
                Try
                    name = debtRow(2) & " "
                Catch ex As Exception

                End Try
                name &= debtRow(3)
                writer.WriteElementString("Name", name)
                Dim address As String = debtRow(4)
                Dim idx As Integer
                For idx = 1 To address.Length
                    If Mid(address, idx, 1) = Chr(10) Or Mid(address, idx, 1) = Chr(13) Then
                        Exit For
                    End If
                Next
                address = Microsoft.VisualBasic.Left(address, idx - 1)
                writer.WriteElementString("AddessLine1", address)
                Dim postCode As String
                Try
                    postCode = debtRow(5)
                Catch ex As Exception
                    postCode = ""
                End Try

                writer.WriteElementString("Postcode", postCode)

                writer.WriteElementString("DateReceived", Format(loadDTP.Value, "dd/MM/yyyy"))
                Dim originalOS As Decimal = debtRow(6)
                writer.WriteElementString("OriginalOS", Format(originalOS, "0.00"))
                writer.WriteEndElement()  'casedetails
            Next
        Next
        writer.WriteEndElement()  'rossack
        writer.Close()
        If cases = 0 Then
            MsgBox("No cases for " & Format(loadDTP.Value, "dd/MM/yyyy"))
        Else
            'validate using xsd
            Dim myDocument As New Xml.XmlDocument
            myDocument.Load(SaveFileDialog1.FileName)
            myDocument.Schemas.Add("RossAck", "R:\vb.net\LAA Collect XSD\Acknowledgement.xsd")
            Dim eventHandler As Xml.Schema.ValidationEventHandler = New Xml.Schema.ValidationEventHandler(AddressOf ValidationEventHandler)
            myDocument.Validate(eventHandler)
            If error_no = 0 Then
                MsgBox("Finished No errors - Number of cases =  " & cases)
            Else
                MsgBox("Finished - " & error_no & " errors")
            End If
        End If
        Me.Close()
    End Sub
    Private Sub ValidationEventHandler(ByVal sender As Object, ByVal e As Xml.Schema.ValidationEventArgs)
        xml_valid = False
        Select Case e.Severity
            Case XmlSeverityType.Error
                write_error("Error: " & e.Message)
            Case XmlSeverityType.Warning
                write_error("Warning " & e.Message)
        End Select
    End Sub
    Private Sub write_error(ByVal error_message As String)

        error_no += 1
        error_message = error_message & vbNewLine
        If error_no = 1 Then
            My.Computer.FileSystem.WriteAllText(logfile, error_message, False)
        Else
            My.Computer.FileSystem.WriteAllText(logfile, error_message, True)
        End If

    End Sub

End Class

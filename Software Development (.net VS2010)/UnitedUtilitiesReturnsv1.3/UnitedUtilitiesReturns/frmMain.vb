﻿Imports System.Configuration
Imports System.IO
Imports CommonLibrary

Public Class frmMain
    Private Returns As New clsReturnsData

    Private SourceRow As Integer, SourceCol As Integer

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

        If Now.DayOfWeek = DayOfWeek.Monday Then
            dtpDateFrom.Value = DateAdd(DateInterval.Day, -2, Now)
            dtpDateTo.Value = Now
        Else
            dtpDateFrom.Value = Now
            dtpDateTo.Value = dtpDateFrom.Value
        End If

        cmsDGV.Items.Add("Cut")
        cmsDGV.Items.Add("Copy")
        cmsDGV.Items.Add("Paste")


        btnCreateReturns.Enabled = False
    End Sub

    'Private Sub frmMain_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown commented out TS 05/Aug/2014 as users always change the date ranges. Request ref 27729
    '    Try
    '        RefreshData()
    '    Catch ex As Exception
    '        MsgBox(ex.ToString)
    '    End Try
    'End Sub

    Private Sub dgvMain_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvMain.CellValueChanged
        Try
            If Not IsDBNull(dgvMain(e.ColumnIndex, e.RowIndex).Value) Then dgvMain(e.ColumnIndex, e.RowIndex).Value = LTrim(dgvMain(e.ColumnIndex, e.RowIndex).Value)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvMain.MouseDown
        Try
            Dim p As Point = dgvMain.PointToClient(Control.MousePosition)
            Dim hit As DataGridView.HitTestInfo = dgvMain.HitTest(p.X, p.Y)

            SourceRow = hit.RowIndex
            SourceCol = hit.ColumnIndex

            If hit.Type = DataGridViewHitTestType.Cell And e.Button = MouseButtons.Left Then
                dgvMain.BeginEdit(False)
                If Not dgvMain(hit.ColumnIndex, hit.RowIndex).IsInEditMode Then
                    dgvMain.DoDragDrop(dgvMain(hit.ColumnIndex, hit.RowIndex).Value, DragDropEffects.Copy Or DragDropEffects.Move)
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvMain.MouseUp
        Try
            If e.Button = MouseButtons.Right Then cmsDGV.Show(sender, e.Location)
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvMain.DragEnter
        Try
            If (e.Data.GetDataPresent(DataFormats.Text)) Then
                e.Effect = DragDropEffects.Copy
            Else
                e.Effect = DragDropEffects.None
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dgvMain_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles dgvMain.DragDrop
        Try
            Dim p As System.Drawing.Point = dgvMain.PointToClient(Control.MousePosition)
            Dim hit As DataGridView.HitTestInfo = dgvMain.HitTest(p.X, p.Y)

            If hit.Type = DataGridViewHitTestType.Cell AndAlso SourceRow = hit.RowIndex AndAlso Not dgvMain(hit.ColumnIndex, hit.RowIndex).ReadOnly Then ' Check rows as we do not want data moving from one case to another
                dgvMain(SourceCol, SourceRow).Value = ""
                ' if drop position is in the left hand half of the cell, insert data before the existing contents, else insert after
                If p.X < dgvMain.GetCellDisplayRectangle(hit.ColumnIndex, hit.RowIndex, False).Left + (dgvMain.Columns(hit.ColumnIndex).Width / 2) Then
                    dgvMain(hit.ColumnIndex, hit.RowIndex).Value = e.Data.GetData(DataFormats.Text).ToString & dgvMain(hit.ColumnIndex, hit.RowIndex).Value
                Else
                    dgvMain(hit.ColumnIndex, hit.RowIndex).Value &= e.Data.GetData(DataFormats.Text).ToString
                End If
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub frmMain_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown

        If dgvMain.SelectedCells.Count = 0 Then Return

        Try
            Dim ClickCell As DataGridViewCell = dgvMain.SelectedCells(0)
            If e.Control Then
                Select Case e.KeyCode
                    Case Keys.C
                        Clipboard.SetText(ClickCell.Value, TextDataFormat.Text)
                    Case Keys.V
                        If Not ClickCell.ReadOnly Then ClickCell.Value = Clipboard.GetText(TextDataFormat.Text)
                    Case Keys.X
                        If Not ClickCell.ReadOnly Then
                            Clipboard.SetText(ClickCell.Value)
                            ClickCell.Value = ""
                        End If
                End Select
            End If

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub dtpRemitDate_CloseUp(ByVal sender As Object, ByVal e As System.EventArgs)
        RefreshData()
    End Sub

    Private Sub cmdRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefresh.Click
        lblNoData.Visible = False
        RefreshData()
    End Sub

    Private Sub cmsDGV_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsDGV.ItemClicked
        Try
            Dim ClickCell As DataGridViewCell = dgvMain.SelectedCells(0)
            Select Case e.ClickedItem.Text
                Case "Cut"
                    If Not ClickCell.ReadOnly Then
                        Clipboard.SetText(ClickCell.Value)
                        ClickCell.Value = ""
                    End If
                Case "Copy"
                    Clipboard.SetText(ClickCell.Value, TextDataFormat.Text)
                Case "Paste"
                    If Not ClickCell.ReadOnly Then ClickCell.Value = Clipboard.GetText(TextDataFormat.Text)
            End Select

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub cmdValidate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdValidate.Click
        Try
            If DataValid() Then MsgBox("Data is valid.", vbOKOnly + vbInformation, Me.Text)

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub RefreshData()
        Try
            Me.Cursor = Cursors.WaitCursor

            Returns.GetAddresses(dtpDateFrom.Value, dtpDateTo.Value)
            Returns.GetDebtors(dtpDateFrom.Value, dtpDateTo.Value)

            With dgvMain
                .DataSource = Returns.Addresses

                For Each DataColumn As DataGridViewTextBoxColumn In .Columns
                    DataColumn.SortMode = DataGridViewColumnSortMode.NotSortable
                Next DataColumn

                .Columns("DebtorID").ReadOnly = True
                '.Columns("DOB").Visible = False
                '.Columns("NI").Visible = False

            End With

            DataValid()

            txtFileSequenceNumber.Text = Returns.GetNextFileSequence ' added TS 05/Aug/2014. Request ref 27729

            Me.Cursor = Cursors.Default
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Function DataValid() As Boolean
        Dim ColLoopCount As Integer, RowLoopCount As Integer, MaxSize As Integer
        Dim ClipboardText As String = Nothing

        DataValid = True

        Try
            dgvMain.EndEdit()

            For Each DataColumn As DataGridViewTextBoxColumn In dgvMain.Columns
                ColLoopCount = DataColumn.Index

                MaxSize = Returns.Addresses.Table.Columns(ColLoopCount).ExtendedProperties("MaxLength")
                For RowLoopCount = 0 To dgvMain.RowCount - 1
                    With dgvMain(ColLoopCount, RowLoopCount)
                        Select Case dgvMain.Columns(ColLoopCount).Name
                            Case "DOB"
                                Dim DateTest As DateTime
                                If .Value <> "" And (.Value.ToString.Length > MaxSize Or Date.TryParseExact(.Value, "yyyyMMdd", Globalization.CultureInfo.CurrentCulture, Globalization.DateTimeStyles.None, DateTest) = False) Then
                                    .Style.BackColor = Color.Red
                                    DataValid = False
                                Else
                                    .Style.BackColor = Color.White
                                End If
                            Case "NI" ' added TS 26/Mar/2013
                                If Not IsDBNull(.Value) AndAlso .Value <> "" AndAlso Not IsNINumber(.Value) Then
                                    .Style.BackColor = Color.Red
                                    DataValid = False
                                Else
                                    .Style.BackColor = Color.White
                                End If
                            Case Else ' The majority of fields are validate by length only
                                If .Value.ToString.Length > MaxSize Then
                                    .Style.BackColor = Color.Red
                                    DataValid = False
                                Else
                                    .Style.BackColor = Color.White
                                End If
                        End Select
                    End With
                Next RowLoopCount
            Next DataColumn

            ' This section added TS 02/Sep/2014. Request ref 29548
            For Each ReturnCase As DataRowView In Returns.Debtors
                If IsDBNull(ReturnCase.Item("clientReturnNumber")) Then
                    If Not IsNothing(ClipboardText) Then ClipboardText &= ","
                    ClipboardText &= ReturnCase.Item("DebtorID")
                    DataValid = False
                End If
            Next ReturnCase

            If Not IsNothing(ClipboardText) Then
                Clipboard.SetText(ClipboardText, TextDataFormat.Text)
                MsgBox("Cases have missing return codes." & vbCrLf & "IDs copied to clipboard.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, Me.Text)
            End If
            ' End of new section

            If Not DataValid Then MsgBox("Invalid data.", vbOKOnly + vbCritical, Me.Text)

            btnCreateReturns.Enabled = DataValid

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

        Return DataValid
    End Function

    Private Sub btnCreateReturns_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateReturns.Click
        Dim ExceptionLog As String = ""
        Try

            If Returns.Debtors.Count = 0 Then
                MsgBox("No cases found")
                Return
            End If

            Dim FolderBrowserDialog As New FolderBrowserDialog

            If FolderBrowserDialog.ShowDialog() <> Windows.Forms.DialogResult.OK Then
                Return
            End If

            ' Now start creating the file
            Dim FileName As String = DateTime.Today.ToString("yyyyMMdd") & "_" & "wagy023i.txt", CommonFields As String ', FileSequenceNumber As String = Returns.GetNextFileSequence ' commented out TS 05/Aug/2014. Request ref 27729
            Dim ReturnsFile As String = "HDRAGY023" & DateTime.Today.ToString("yyyyMMdd") & txtFileSequenceNumber.Text.PadLeft(6, "0") & Space(233) & vbCrLf ' FileSequenceNumber replaced by txtFileSequenceNumber TS 05/Aug/2014. Request ref 27729

            For Each Debtor As DataRowView In Returns.Debtors

                CommonFields = "DET" & Debtor("client_ref").ToString.PadRight(20) & Debtor("defaultCourtCode").ToString.PadRight(6) & Debtor("clientReturnNumber").ToString.PadLeft(2, "0")

                Select Case Debtor.Item("clientReturnNumber")
                    Case 1
                        If IsDBNull(Debtor.Item("GoneAwayDate")) OrElse Debtor.Item("GoneAwayDate") = "" Then
                            ReturnsFile &= CommonFields & Space(8) & vbCrLf
                        Else
                            ReturnsFile &= CommonFields & CDate(Debtor.Item("GoneAwayDate")).ToString("yyyyMMdd") & vbCrLf
                        End If

                    Case 8

                        Returns.Addresses.RowFilter = "DebtorID = " & Debtor("DebtorID")

                        If IsDBNull(Returns.Addresses.Item(0).Item("NI")) OrElse Returns.Addresses.Item(0).Item("NI") = "" OrElse IsNINumber(Returns.Addresses.Item(0).Item("NI")) Then

                            If Returns.Addresses.Count > 0 Then
                                ReturnsFile &= CommonFields
                                ReturnsFile &= IIf(Not IsDBNull(Returns.Addresses.Item(0).Item("CustomerFullName")), Returns.Addresses.Item(0).Item("CustomerFullName"), "").PadRight(68)
                                ReturnsFile &= IIf(Not IsDBNull(Returns.Addresses.Item(0).Item("ApartmentNumber")), Returns.Addresses.Item(0).Item("ApartmentNumber"), "").PadRight(50)
                                ReturnsFile &= IIf(Not IsDBNull(Returns.Addresses.Item(0).Item("HouseName")), Returns.Addresses.Item(0).Item("HouseName"), "").PadRight(50)
                                ReturnsFile &= IIf(Not IsDBNull(Returns.Addresses.Item(0).Item("HouseNumber")), Returns.Addresses.Item(0).Item("HouseNumber"), "").PadRight(50)
                                ReturnsFile &= IIf(Not IsDBNull(Returns.Addresses.Item(0).Item("SubStreetName")), Returns.Addresses.Item(0).Item("SubStreetName"), "").PadRight(50)
                                ReturnsFile &= IIf(Not IsDBNull(Returns.Addresses.Item(0).Item("Street")), Returns.Addresses.Item(0).Item("Street"), "").PadRight(50)
                                ReturnsFile &= IIf(Not IsDBNull(Returns.Addresses.Item(0).Item("SubDistrict")), Returns.Addresses.Item(0).Item("SubDistrict"), "").PadRight(50)
                                ReturnsFile &= IIf(Not IsDBNull(Returns.Addresses.Item(0).Item("District")), Returns.Addresses.Item(0).Item("District"), "").PadRight(50)
                                ReturnsFile &= IIf(Not IsDBNull(Returns.Addresses.Item(0).Item("Town")), Returns.Addresses.Item(0).Item("Town"), "").PadRight(50)
                                ReturnsFile &= IIf(Not IsDBNull(Returns.Addresses.Item(0).Item("County")), Returns.Addresses.Item(0).Item("County"), "").PadRight(50)
                                ReturnsFile &= IIf(Not IsDBNull(Returns.Addresses.Item(0).Item("PostCode")), Returns.Addresses.Item(0).Item("PostCode"), "").PadRight(8)
                                ReturnsFile &= IIf(Not IsDBNull(Returns.Addresses.Item(0).Item("Phone")), Returns.Addresses.Item(0).Item("Phone").ToString.Replace(" ", ""), "").PadRight(12)
                                ReturnsFile &= IIf(Not IsDBNull(Returns.Addresses.Item(0).Item("AssociateName")), Returns.Addresses.Item(0).Item("AssociateName"), "").PadRight(50)

                                If Not IsDBNull(Returns.Addresses.Item(0).Item("DOB")) AndAlso Returns.Addresses.Item(0).Item("DOB") <> "" Then
                                    'ReturnsFile &= CDate(Returns.Addresses.Item(0).Item("DOB")).ToString("yyyyMMdd").PadRight(8)
                                    ReturnsFile &= Returns.Addresses.Item(0).Item("DOB").ToString.PadRight(8)
                                Else
                                    ReturnsFile &= Space(8)
                                End If

                                If Not IsDBNull(Returns.Addresses.Item(0).Item("NI")) AndAlso Returns.Addresses.Item(0).Item("NI") <> "" Then
                                    ReturnsFile &= Returns.Addresses.Item(0).Item("NI").PadRight(12) & vbCrLf
                                Else
                                    ReturnsFile &= Space(12) & vbCrLf
                                End If

                            Else
                                ReturnsFile &= CommonFields & Space(608) & vbCrLf
                            End If
                        Else
                            ExceptionLog = "Invalid NI found on case " & Debtor("DebtorID").ToString & ". Case not returned."
                        End If

                    Case 11
                        ReturnsFile &= CommonFields & SignedDecimal(Debtor("debtPaid")) & vbCrLf

                    Case 43, 44, 64, 65, 66
                        If IsDBNull(Debtor("arrange_amount")) Or IsDBNull(Debtor("arrange_interval")) Or IsDBNull(Debtor("arrange_next")) Then
                            ExceptionLog = "Incomplete arrangement details on case " & Debtor("DebtorID").ToString & ". Case not returned."
                        Else
                            Dim ArrangeNext As String
                            ReturnsFile &= CommonFields & SignedDecimal(Debtor("arrange_amount"))
                            ArrangeNext = CDate(Debtor("arrange_next").ToString).ToString("ddd").ToUpper
                            If ArrangeNext = "SAT" Or ArrangeNext = "SUN" Then ArrangeNext = "MON"

                            ' Payment day
                            Select Case Debtor.Item("arrange_interval")
                                Case 0 To 7
                                    'ReturnsFile &= "WE" & CDate(Debtor("arrange_next").ToString).ToString("ddd").ToUpper & vbCrLf
                                    ReturnsFile &= "WE" & ArrangeNext & vbCrLf
                                Case 8 To 14
                                    'ReturnsFile &= "FO" & CDate(Debtor("arrange_next").ToString).ToString("ddd").ToUpper & vbCrLf
                                    ReturnsFile &= "FO" & ArrangeNext & vbCrLf
                                Case 28
                                    ReturnsFile &= "FW" & CDate(Debtor("arrange_next").ToString).ToString("0dd").ToUpper & vbCrLf
                                Case Else
                                    ReturnsFile &= "MO" & CDate(Debtor("arrange_next").ToString).ToString("0dd").ToUpper & vbCrLf
                            End Select

                        End If

                    Case 61
                        If IsNINumber(Debtor("empNI").ToString) Then
                            ReturnsFile &= CommonFields & Debtor.Item("empNI").ToString.PadRight(12) & vbCrLf
                        Else
                            ExceptionLog = "Invalid NI number found on case " & Debtor("DebtorID").ToString & ". Case not returned."
                        End If

                    Case 62
                        If String.IsNullOrEmpty(Debtor("AssociateNI").ToString) Or IsNINumber(Debtor("AssociateNI").ToString) Then ' test added TS 12/Apr/2013. Portal task ref 14987
                            ReturnsFile &= CommonFields & Debtor("AssociateName").ToString.PadRight(50) & _
                                           If(IsDBNull(Debtor("AssociateDOB")), Space(8), CDate(Debtor("AssociateDOB")).ToString("yyyyMMdd")) & _
                                           Debtor("AssociateNI").ToString.PadRight(12) & vbCrLf
                        Else
                            ExceptionLog = "Invalid Associate NI number found on case " & Debtor("DebtorID").ToString & ". Case not returned."
                        End If

                    Case 80 To 89, 35 To 42
                        Dim ReturnDetails As String = ""
                        If Not IsDBNull(Debtor("return_details")) Then ReturnDetails = Debtor("return_details")
                        ' Replace in this order so that a CrLf does not become two spaces
                        ReturnDetails = ReturnDetails.Replace(vbCrLf, " ")
                        ReturnDetails = ReturnDetails.Replace(vbCr, " ")
                        ReturnDetails = ReturnDetails.Replace(vbLf, " ")
                        ReturnDetails = ReturnDetails.Replace(vbTab, " ")

                        ReturnsFile &= CommonFields & ReturnDetails.PadRight(500) & vbCrLf

                    Case Else
                        ReturnsFile &= CommonFields & vbCrLf
                End Select

            Next Debtor

            ReturnsFile &= "TRL" & Returns.Debtors.Count.ToString.PadLeft(6, "0")

            'Using Writer As StreamWriter = New StreamWriter(FolderBrowserDialog.SelectedPath & "\" & FileName)
            '    Writer.Write(ReturnsFile)
            'End Using

            'Returns.Addresses.RowFilter = ""

            'Returns.SetLastFileSequence(FileSequenceNumber, Returns.Debtors.Count)

            'If ExceptionLog <> "" Then WriteFile(FolderBrowserDialog.SelectedPath & "\" & "Exceptions.txt", ExceptionLog)

            'MsgBox("Returns file created.", vbOKOnly, Me.Text)

            'System.Diagnostics.Process.Start(FolderBrowserDialog.SelectedPath & "\" & FileName)
            'If ExceptionLog <> "" Then System.Diagnostics.Process.Start(FolderBrowserDialog.SelectedPath & "\Exceptions.txt")

            Returns.Addresses.RowFilter = ""

            If ExceptionLog <> "" Then
                Dim Cancel As Boolean = False
                WriteFile(FolderBrowserDialog.SelectedPath & "\" & "Exceptions.txt", ExceptionLog)

                If MsgBox("Exceptions found. Create file and increment sequence?", vbCritical + vbYesNo + vbDefaultButton2, Me.Text) = vbNo Then
                    Cancel = True
                    MsgBox("File not created", vbOKOnly, Me.Text)
                End If

                System.Diagnostics.Process.Start(FolderBrowserDialog.SelectedPath & "\Exceptions.txt")

                If Cancel Then Return
            End If

            ' The following commented out TS 07/Jan/2014
            'Using Writer As StreamWriter = New StreamWriter(FolderBrowserDialog.SelectedPath & "\" & FileName)
            '    Writer.Write(ReturnsFile)
            'End Using

            WriteFile(FolderBrowserDialog.SelectedPath & "\" & FileName, ReturnsFile) ' added TS 07/Jan/2014

            If ConfigurationManager.AppSettings("Environment") = "Prod" Then ' Added TS 11/Apr/2013
                Returns.SetLastFileSequence(txtFileSequenceNumber.Text, Returns.Debtors.Count) ' FileSequenceNumber replaced by txtFileSequenceNumber.Text TS 05/Aug/2014. Request ref 27729
            Else
                MsgBox("Test version. Last file sequence not set.", vbInformation + vbOKOnly, Me.Text)
            End If

            MsgBox("Returns file created.", vbOKOnly, Me.Text)

            System.Diagnostics.Process.Start(FolderBrowserDialog.SelectedPath & "\" & FileName)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function SignedDecimal(ByVal Value As Decimal) As String
        Dim Result As String

        If Value < 0 Then
            Result = "-"
        ElseIf Value > 0 Then
            Result = "+"
        Else
            Result = " "
        End If

        Result &= Math.Round(Value, 2).ToString("000000.00")

        SignedDecimal = Result

    End Function

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub

    Private Sub txtFileSequenceNumber_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtFileSequenceNumber.Validating
        Try
            If Not IsNumeric(txtFileSequenceNumber.Text) Then
                MsgBox("Please enter a valid file sequence number.", vbOKOnly + vbCritical, Me.Text)
                e.Cancel = True
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
End Class

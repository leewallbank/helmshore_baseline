﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String, ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String
    Private ConnCode As New Dictionary(Of String, Integer)
    Private OutputFiles As New ArrayList()
    Private Const DebtCol As Integer = 19

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim OutputFile As String = ""
        Dim LineNumber As Integer, CaseCount As Integer
        Dim OrigDebtAmount As Decimal

        Try

            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)

            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents) + 2 ' audit file and output file

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                Application.DoEvents() ' without this line, the button disappears until processing is complete

                OutputFile &= InputLine

                If LineNumber > 1 And LineNumber < UBound(FileContents) + 1 Then
                    OutputFile &= "|" & (InputLine.Split("|")(DebtCol) - 30).ToString & "|Please note the original balance has had a reduction of 30 which will be loaded as costs."
                    CaseCount += 1
                    OrigDebtAmount += InputLine.Split("|")(DebtCol)
                ElseIf LineNumber = UBound(FileContents) + 1 Then
                    If UBound(FileContents) - 1 <> InputLine.Split("|")(0) Then ErrorLog &= "Incorrect trailer data record count at line number " & LineNumber.ToString & ". Trailer: " & InputLine.Split("|")(0).ToString & " Actual: " & (UBound(FileContents) - 1).ToString & vbCrLf
                    If InputLine.Split("|")(2) <> Decimal.Round(OrigDebtAmount, 2) Then ErrorLog &= "Incorrect trailer total balance at line number " & LineNumber.ToString & ". Trailer: " & InputLine.Split("|")(2).ToString & " Detail: " & OrigDebtAmount.ToString & vbCrLf
                End If

                OutputFile &= vbCrLf
            Next InputLine

            ProgressBar.Value += 1
            AuditLog = "File processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            ProgressBar.Value += 1

            If OutputFile <> "" Then
                WriteFile(InputFilePath & FileName & "_PreProcessed.txt", OutputFile)
                OutputFiles.Add(InputFilePath & FileName & "_PreProcessed.txt")
                AuditLog &= "Number of new cases: " & CaseCount.ToString & vbCrLf
                AuditLog &= "Total balance of new cases: " & OrigDebtAmount.ToString & vbCrLf & vbCrLf
            End If

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)
            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", ExceptionLog)

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Clipboard.SetText("notepad.exe " & InputFilePath & FileName & FileExt)
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start(InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If AuditLog <> "" And File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If ErrorLog <> "" And File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If ExceptionLog <> "" And File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub
End Class

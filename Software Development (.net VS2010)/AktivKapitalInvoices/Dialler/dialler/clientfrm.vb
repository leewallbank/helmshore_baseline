Public Class clientfrm

    Private Sub clientfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        param1 = "onestep"
        param2 = "select _rowid, name from Client " & _
        " where _rowid <> 1 and _rowid <> 2 and _rowid <> 24 order by name"
        Dim cl_dataset As DataSet = get_dataset(param1, param2)
        Dim idx As Integer

        DataGridView1.Rows.Clear()
        For idx = 0 To no_of_rows - 1
            Dim cl_no As Integer = cl_dataset.Tables(0).Rows(idx).Item(0)
            Dim cl_name As String = Trim(cl_dataset.Tables(0).Rows(idx).Item(1))
            DataGridView1.Rows.Add(cl_no, cl_name)
        Next

    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        Me.Close()
    End Sub

    Private Sub DataGridView1_RowEnter1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter
        selected_client = DataGridView1.Rows(e.RowIndex).Cells(0).Value
        selected_client_name = DataGridView1.Rows(e.RowIndex).Cells(1).Value
    End Sub
End Class
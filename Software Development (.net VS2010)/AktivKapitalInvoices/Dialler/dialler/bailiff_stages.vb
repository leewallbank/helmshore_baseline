Public Class bailiff_stages

    Private Sub bailiff_stages_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        ReDim selected_stages(100)
        bailiff_stages_dg.EndEdit()
        Dim row As DataGridViewRow
        Dim idx As Integer = 0
        selected_stage_rows = 0
        For Each row In bailiff_stages_dg.Rows
            If bailiff_stages_dg.Rows(idx).Cells(0).Value = True Then
                selected_stages(selected_stage_rows) = bailiff_stages_dg.Rows(idx).Cells(1).Value
                selected_stage_rows += 1
            End If
            idx += 1
        Next
    End Sub

    Private Sub bailiff_stages_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim stage_table(100) As String
        Dim csid_idx As Integer
        bailiff_stages_dg.Rows.Clear()
        If csid_rows = 0 Then
            'no schemes selected so just use selected_client
            param2 = "select _rowid from clientScheme where clientID = " & selected_client & _
            " and branchID <> 2"
            Dim cs_dataset As DataSet = get_dataset("onestep", param2)
            For csid_idx = 0 To no_of_rows - 1
                csid_table(csid_idx) = cs_dataset.Tables(0).Rows(csid_idx).Item(0)
                csid_rows += 1
            Next
        End If
        Dim temp_stage_array(200) As String
        Dim temp_stage_rows As Integer = 0
        Dim idx As Integer
        For csid_idx = 0 To csid_rows
            Dim cs_id As Integer
            Try
                cs_id = csid_table(csid_idx)
            Catch ex As Exception
                Continue For
            End Try
            param2 = "select schemeID from clientScheme where _rowid = " & cs_id
            Dim csid_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim sch_id As Integer = csid_dataset.Tables(0).Rows(0).Item(0)
            param2 = "select distinct name from Stage where schemeID = " & sch_id & _
            " order by sort"
            Dim sch_dataset As DataSet = get_dataset("onestep", param2)
            If no_of_rows = 0 Then
                Continue For
            End If
            Dim stage_rows As Integer = no_of_rows
            For idx = 0 To stage_rows - 1
                Dim stage_name As String = sch_dataset.Tables(0).Rows(idx).Item(0)
                temp_stage_array(temp_stage_rows) = stage_name
                temp_stage_rows += 1
            Next
        Next
        no_stages_found = False
        If temp_stage_rows = 0 Then
            MsgBox("No Bailiff stages available")
            no_stages_found = True
            Me.Close()
            Exit Sub
        End If
        ReDim Preserve temp_stage_array(temp_stage_rows - 1)
        Array.Sort(temp_stage_array)
        Dim last_stage_name As String = ""
        For idx = 0 To temp_stage_rows - 1
            Dim stage_name As String = temp_stage_array(idx)
            If stage_name <> last_stage_name Then
                bailiff_stages_dg.Rows.Add(False, stage_name)
            End If
            last_stage_name = stage_name
        Next
    End Sub
End Class
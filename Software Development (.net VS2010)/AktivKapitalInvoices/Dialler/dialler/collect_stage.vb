Public Class collect_stage
    Dim param1, param2, param3, param4, ret_code As String

    Private Sub stage_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        stage_form = False
    End Sub



    Private Sub stage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        stage_form = True
        stagerb1.Checked = True
        ToolStripProgressBar1.Visible = False
        If mode = "P" Then
            stagelbl.Text = "Priority clients file"
        ElseIf mode = "T" Then
            stagelbl.Text = "Top 20 client file"
        ElseIf mode = "O" Then
            stagelbl.Text = "Other clients file"
        ElseIf mode = "C" Then
            stagelbl.Text = "Selected client file"
        ElseIf mode = "12" Then
            stagelbl.Text = "Arrow clients - " & sch_group
        ElseIf mode = "13" Then
            stagelbl.Text = "CPW clients - " & sch_group
        ElseIf mode = "UU" Then
            stagelbl.Text = "United utilities"
        ElseIf mode = "25" Then
            stagelbl.Text = "Financial Services"
        End If

    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub createbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles createbtn.Click

        maintainbtn.Enabled = False
        exitbtn.Enabled = False
        'createbtn.Enabled = False
        'create file for selected stage
        ToolStripProgressBar1.Visible = True
        ToolStripProgressBar1.Value = 5
        selected_stage_name = ""

        If stagerb1.Checked Then
            selected_stage_name = "Stage 1"
        ElseIf stagerb2.Checked Then
            selected_stage_name = "Stage 2"
        ElseIf stagerb3.Checked Then
            selected_stage_name = "Stage 3"
        ElseIf stagerb4.Checked Then
            selected_stage_name = "Stage 4"
        ElseIf stagerb5.Checked Then
            selected_stage_name = "P.S.D"
        ElseIf stagerb6.Checked Then
            selected_stage_name = "Awaiting Doorstep"
        ElseIf letterb1.Checked Then
            selected_stage_name = "Letter"
        End If
        param2 = "select _rowid, name_title, name_fore, name_sur, address, clientschemeID, " & _
                        " add_phone, add_fax, empPhone, empFax, debt_balance, name2, " & _
                        " last_stageID, add_postcode, status_nextdate, _createdDate from Debtor" & _
                        " where status_open_closed = 'O' and status = 'L' and status_hold = 'N'" & _
                        " and debt_balance > 1.00 and bail_current = 'N' and (add_phone is not null or add_fax is not null " & _
                        " or empPhone is not null or empFax is not null)"

        produce_dialler_file(param2)
        Me.Close()
        'old_stage()
    End Sub
    Private Sub old_stage()
        'param1 = "onestep"
        'param2 = "Debtor"
        'param3 = "01_rowid 2name_title 3name_forename 4name_surname" _
        '      & " 5address 6clientschemeID 7schemeID 8add_phone 9add_fax" _
        '      & "10debt_balance 11clientID 12last_stageID 13name2 14_createdDate"
        'param4 = "where  status = 'L' and status_hold = 'N' and status_open_closed = 'O'" _
        '       & " and bail_current = 'N' and (add_phone is not null or add_fax is not null)"
        'ret_code = get_table(param1, param2, param3, param4)

        'If ret_code = 1 Then
        '    Return
        'End If
        'Dim debt_rows As Integer = no_of_rows
        'Dim debt_table = table_array
        ''get all stage names
        'param2 = "Stage"
        'param3 = "01_rowid 2name"
        'param4 = "order by _rowid"
        'ret_code = get_table(param1, param2, param3, param4)
        'If ret_code = 1 Then
        '    Return
        'End If
        'stage_table = table_array

        'outfile = "Our_Reference,Title,Forename,Surname,Address,Client,Scheme,TelNumber," & _
        '"SecondNumber,Balance,ClientID,Name2" & vbNewLine

        'Dim col_value As String = ""
        'Dim phone_no As String = ""
        'Dim idx, idx2, idx3 As Integer
        'Dim phone_array(debt_rows) As String
        'Dim outline_array(debt_rows) As String
        'Dim outline_idx As Integer = 0
        'Dim sch_no As String = ""
        'Dim cl_no As String = ""
        'For idx = 1 To debt_rows
        '    ToolStripProgressBar1.Value = (idx / debt_rows) * 100
        '    For idx2 = 1 To 14
        '        Dim col_result As String = ""

        '        col_value = Trim(debt_table(idx, idx2))
        '        If idx2 = 6 Then  'clientschemeID
        '            'check if csid selected
        '            Dim csid_idx As Integer
        '            If csid_rows > 0 Then
        '                Dim csid_found As Boolean = False
        '                For csid_idx = 0 To csid_rows
        '                    If csid_table(csid_idx) = col_value Then
        '                        csid_found = True
        '                    End If
        '                Next
        '                If csid_found = False Then
        '                    outline = ""
        'Exit For
        '                End If
        '            End If
        'If mode <> "C" And cs_table(col_value, 4) <> 2 Then  'Branch has to be 2
        '    outline = ""
        '    Exit For
        'End If
        'cl_no = cs_table(col_value, 2)
        ''16.05.2001 exclude lsc and VM
        'If mode = "O" And (cl_no = 1045 Or cl_no = 1159) Then
        '    outline = ""
        '    Exit For
        'End If
        'If mode = "C" Then
        '    If cl_no <> selected_client Then
        '        outline = ""
        '        Exit For
        '    End If
        'Else
        ''see if client is in table
        'Me.Dialler_clientsTableAdapter.FillBy(Me.FeesSQLDataSet.Dialler_clients, cl_no)
        'If Me.FeesSQLDataSet.Dialler_clients.Rows.Count = 0 Then
        '    If mode <> "O" Then
        '        outline = ""
        '        Exit For
        '    Else
        '        If cl_no = 1045 Then  'ignore LSC(Collect)
        '            outline = ""
        '            Exit For
        '        End If
        '    End If
        'Else
        '    If Me.FeesSQLDataSet.Dialler_clients.Rows(0).Item(1) = True Then  'priority client
        '        If mode <> "P" Then
        '            outline = ""
        '            Exit For
        '        End If
        '    Else
        'If mode <> "T" Then
        '    outline = ""
        '    Exit For
        'End If
        '    End If
        'End If
        'End If
        'sch_no = cs_table(col_value, 3)
        'scheme no longer being checked
        'If mode <> "C" Then
        '    'check if scheme number is one required
        '    Try
        '        ret_code = dialler.maintainfrm.CollectDiallerSchemeTableAdapter.CheckQuery(sch_no, mode)
        '    Catch ex As Exception
        '        MessageBox.Show(ex.Message)
        '    End Try
        '    If ret_code = 0 Then
        '        outline = ""
        '        Exit For
        '    End If
        'End If
        'col_result = Trim(client_table(cl_no, 2))
        '        ElseIf idx2 = 7 Then
        'col_result = Trim(sch_table(sch_no, 2))
        '        ElseIf idx2 = 8 Then
        'If phone_no = col_value And phone_no.Length > 0 Then
        '    outline = ""
        '    Exit For
        'End If
        'phone_no = col_value
        'If Microsoft.VisualBasic.Right(phone_no, 1) = Chr(10) _
        '    Or Microsoft.VisualBasic.Right(phone_no, 1) = Chr(13) Then
        '    phone_no = Microsoft.VisualBasic.Left(phone_no, phone_no.Length - 1)
        'End If
        'If Microsoft.VisualBasic.Right(phone_no, 1) = Chr(10) _
        '    Or Microsoft.VisualBasic.Right(phone_no, 1) = Chr(13) Then
        '    phone_no = Microsoft.VisualBasic.Left(phone_no, phone_no.Length - 1)
        'End If
        'col_result = phone_no
        '        ElseIf idx2 = 9 Then
        'col_result = col_value
        'If Microsoft.VisualBasic.Right(col_result, 1) = Chr(10) _
        '                        Or Microsoft.VisualBasic.Right(col_result, 1) = Chr(13) Then
        '    col_result = Microsoft.VisualBasic.Left(col_result, col_result.Length - 1)
        'End If
        'If Microsoft.VisualBasic.Right(col_result, 1) = Chr(10) _
        '                        Or Microsoft.VisualBasic.Right(col_result, 1) = Chr(13) Then
        '    col_result = Microsoft.VisualBasic.Left(col_result, col_result.Length - 1)
        'End If
        '        ElseIf idx2 = 10 Then
        'col_result = Format(col_value, "fixed")
        '        ElseIf idx2 = 11 Then
        'col_result = cl_no
        '        ElseIf idx2 = 12 Then
        'col_result = Trim(stage_table(col_value, 2))
        'If InStr(col_result, selected_stage_name) = 0 Then
        '    If selected_stage_name = "Stage 1" Then
        '        If InStr(col_result, "Notice of Assignment") = 0 Then
        '            outline = ""
        '            Exit For
        '        End If
        '    Else
        '        outline = ""
        '        Exit For
        '    End If
        'Else
        'If selected_stage_name = "Stage 1" Or _
        'selected_stage_name = "Letter" Then
        '    'for stage 1 and letter stages, check letter sent at least 5 days ago
        '    param2 = "select _createdDate from Note " & _
        '    " where debtorID = " & debt_table(idx, 1) & _
        '    " and type = 'Letter'" & _
        '    " order by _rowid"
        '    Dim note_dataset As DataSet = get_dataset(param1, param2)
        '    If no_of_rows = 0 Then
        '        outline = ""
        '        Exit For
        '    Else
        'Dim note_date As Date = note_dataset.Tables(0).Rows(0).Item(0)
        'If DateDiff(DateInterval.Day, note_date, Now) < 5 Then
        '    outline = ""
        '    Exit For
        'End If
        '    End If
        'End If
        'End If
        ''if letter stage selected - ignore names with stage in
        'If selected_stage_name = "Letter" Then
        '    If InStr(col_result, "Stage") > 0 Then
        '        outline = ""
        '        Exit For
        '    End If
        'End If
        'col_result = ""
        '        ElseIf idx2 = 13 Then
        'col_result = col_value
        '        ElseIf idx2 = 14 Then  'createdDate
        ''check at least 5 days old
        'Dim created_date As Date = col_value
        'If DateDiff("d", col_value, Now) < 5 Then
        '    outline = ""
        '    Exit For
        'End If
        '        Else
        'For idx3 = 1 To Len(col_value)
        '    If Mid(col_value, idx3, 1) = vbCr Or _
        '        Mid(col_value, idx3, 1) = "," Or _
        '       Mid(col_value, idx3, 1) = vbLf Then
        '        col_result = col_result & " "
        '    Else
        '        col_result = col_result & Mid(col_value, idx3, 1)
        '    End If
        'Next
        '        End If
        ''if 1st phone number blank, move 2nd phone number to 1st number.
        'If idx2 = 8 Then
        '    Continue For
        'End If
        'If idx2 = 9 Then
        '    If phone_no.Length = 0 Then
        '        If col_result.Length = 0 Then
        '            outline = ""
        '            Exit For
        '        End If
        '        phone_no = col_result
        '        outline = outline & phone_no & "," & "" & ","
        '    Else
        '        outline = outline & phone_no & "," & col_result & ","
        '    End If
        'Else
        '    If idx2 <> 12 And idx2 <> 14 Then
        '        outline = outline & col_result & ","
        '    End If
        'End If
        '    Next
        'If Len(outline) > 0 Then
        '    outline_array(outline_idx) = outline
        '    phone_array(outline_idx) = phone_no
        '    outline_idx += 1
        '    outline = ""
        'End If

        'Next
        ''sort arrays by phone numbers
        'ToolStripProgressBar1.Value = 5
        'Array.Sort(phone_array, outline_array, 0, outline_idx)
        'phone_no = ""
        'For idx = 0 To outline_idx - 1
        '    ToolStripProgressBar1.Value = (idx / outline_idx) * 100
        '    If phone_no = phone_array(idx) Then
        '        Continue For
        '    End If
        '    outline = outline_array(idx)
        '    If Len(outline) > 0 Then
        '        outfile = outfile & outline & vbNewLine
        '    End If
        '    phone_no = phone_array(idx)
        'Next
        'write out file
        'With SaveFileDialog1
        '    .Title = "Save file"
        '    .Filter = "CSV files |*.csv"
        '    .DefaultExt = ".csv"
        '    .OverwritePrompt = True
        '    .InitialDirectory = "c:\temp"
        '    .FileName = "dialler.csv"
        'End With
        'ToolStripProgressBar1.Value = 100
        'Dim filepath As String = SaveFileDialog1.FileName
        'If Len(outfile) = 0 Then
        '    MessageBox.Show("There are no cases selected")
        'Else
        '    If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
        '        My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False)
        '    End If
        'End If
        'ToolStripProgressBar1.Visible = False
        'Me.Close()
    End Sub

    Private Sub maintainbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles maintainbtn.Click
        MsgBox("Not available at present - Ask Jeff or DD to amend for now - table dialler_clients")
        Me.Close()

        'maintainfrm.Show()
    End Sub


End Class
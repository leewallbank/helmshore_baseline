<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class cmecstagefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmecstagedg = New System.Windows.Forms.DataGridView
        Me.cbox = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.stagename = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.cmecstagedg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cmecstagedg
        '
        Me.cmecstagedg.AllowUserToAddRows = False
        Me.cmecstagedg.AllowUserToDeleteRows = False
        Me.cmecstagedg.AllowUserToOrderColumns = True
        Me.cmecstagedg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.cmecstagedg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cbox, Me.stagename})
        Me.cmecstagedg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.cmecstagedg.Location = New System.Drawing.Point(0, 0)
        Me.cmecstagedg.Name = "cmecstagedg"
        Me.cmecstagedg.Size = New System.Drawing.Size(303, 266)
        Me.cmecstagedg.TabIndex = 0
        '
        'cbox
        '
        Me.cbox.HeaderText = "Select"
        Me.cbox.Name = "cbox"
        Me.cbox.Width = 50
        '
        'stagename
        '
        Me.stagename.HeaderText = "Stage Name"
        Me.stagename.Name = "stagename"
        Me.stagename.ReadOnly = True
        Me.stagename.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.stagename.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.stagename.Width = 200
        '
        'cmecstagefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(303, 266)
        Me.Controls.Add(Me.cmecstagedg)
        Me.Name = "cmecstagefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Select CMEC stages"
        CType(Me.cmecstagedg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmecstagedg As System.Windows.Forms.DataGridView
    Friend WithEvents cbox As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents stagename As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

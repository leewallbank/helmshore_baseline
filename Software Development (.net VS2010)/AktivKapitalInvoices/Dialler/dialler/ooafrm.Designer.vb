<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ooafrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.createbtn = New System.Windows.Forms.Button
        Me.maintainbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.SuspendLayout()
        '
        'createbtn
        '
        Me.createbtn.Location = New System.Drawing.Point(98, 57)
        Me.createbtn.Name = "createbtn"
        Me.createbtn.Size = New System.Drawing.Size(119, 23)
        Me.createbtn.TabIndex = 0
        Me.createbtn.Text = "Create OOA File"
        Me.createbtn.UseVisualStyleBackColor = True
        '
        'maintainbtn
        '
        Me.maintainbtn.Location = New System.Drawing.Point(98, 133)
        Me.maintainbtn.Name = "maintainbtn"
        Me.maintainbtn.Size = New System.Drawing.Size(119, 23)
        Me.maintainbtn.TabIndex = 1
        Me.maintainbtn.Text = "Maintain Postcodes"
        Me.maintainbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(194, 220)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 2
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(12, 220)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(100, 23)
        Me.ProgressBar1.TabIndex = 3
        '
        'ooafrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 266)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.maintainbtn)
        Me.Controls.Add(Me.createbtn)
        Me.Name = "ooafrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Out of Area"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents createbtn As System.Windows.Forms.Button
    Friend WithEvents maintainbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
End Class

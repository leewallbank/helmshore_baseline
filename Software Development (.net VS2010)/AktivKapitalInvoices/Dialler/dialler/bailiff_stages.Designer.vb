<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class bailiff_stages
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.bailiff_stages_dg = New System.Windows.Forms.DataGridView
        Me.selection = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.stage_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.branch = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.bailiff_stages_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bailiff_stages_dg
        '
        Me.bailiff_stages_dg.AllowUserToAddRows = False
        Me.bailiff_stages_dg.AllowUserToDeleteRows = False
        Me.bailiff_stages_dg.AllowUserToOrderColumns = True
        Me.bailiff_stages_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.bailiff_stages_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.selection, Me.stage_name, Me.branch})
        Me.bailiff_stages_dg.Location = New System.Drawing.Point(0, 0)
        Me.bailiff_stages_dg.Name = "bailiff_stages_dg"
        Me.bailiff_stages_dg.Size = New System.Drawing.Size(428, 508)
        Me.bailiff_stages_dg.TabIndex = 0
        '
        'selection
        '
        Me.selection.HeaderText = "Select"
        Me.selection.Name = "selection"
        Me.selection.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.selection.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.selection.Width = 50
        '
        'stage_name
        '
        Me.stage_name.HeaderText = "Stage"
        Me.stage_name.Name = "stage_name"
        Me.stage_name.ReadOnly = True
        Me.stage_name.Width = 300
        '
        'branch
        '
        Me.branch.HeaderText = "branch"
        Me.branch.Name = "branch"
        Me.branch.Visible = False
        '
        'bailiff_stages
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(450, 520)
        Me.Controls.Add(Me.bailiff_stages_dg)
        Me.Name = "bailiff_stages"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "bailiff_stages"
        CType(Me.bailiff_stages_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents bailiff_stages_dg As System.Windows.Forms.DataGridView
    Friend WithEvents selection As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents stage_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents branch As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

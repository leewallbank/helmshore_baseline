<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class clientupdfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.DiallerclientsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FeesSQLDataSet = New dialler.FeesSQLDataSet
        Me.Dialler_clientsTableAdapter = New dialler.FeesSQLDataSetTableAdapters.Dialler_clientsTableAdapter
        Me.cl_no = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.cl_name = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Priority = New System.Windows.Forms.DataGridViewCheckBoxColumn
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DiallerclientsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.cl_no, Me.cl_name, Me.Priority})
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(0, 0)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(479, 532)
        Me.DataGridView1.TabIndex = 0
        '
        'DiallerclientsBindingSource
        '
        Me.DiallerclientsBindingSource.DataMember = "Dialler_clients"
        Me.DiallerclientsBindingSource.DataSource = Me.FeesSQLDataSet
        '
        'FeesSQLDataSet
        '
        Me.FeesSQLDataSet.DataSetName = "FeesSQLDataSet"
        Me.FeesSQLDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Dialler_clientsTableAdapter
        '
        Me.Dialler_clientsTableAdapter.ClearBeforeFill = True
        '
        'cl_no
        '
        Me.cl_no.HeaderText = "Client No"
        Me.cl_no.Name = "cl_no"
        Me.cl_no.Width = 80
        '
        'cl_name
        '
        Me.cl_name.HeaderText = "Client Name"
        Me.cl_name.Name = "cl_name"
        Me.cl_name.ReadOnly = True
        Me.cl_name.Width = 250
        '
        'Priority
        '
        Me.Priority.HeaderText = "Priority"
        Me.Priority.Name = "Priority"
        '
        'clientupdfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(479, 532)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "clientupdfrm"
        Me.Text = "clientupdfrm"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DiallerclientsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FeesSQLDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents FeesSQLDataSet As dialler.FeesSQLDataSet
    Friend WithEvents DiallerclientsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents Dialler_clientsTableAdapter As dialler.FeesSQLDataSetTableAdapters.Dialler_clientsTableAdapter
    Friend WithEvents cl_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cl_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Priority As System.Windows.Forms.DataGridViewCheckBoxColumn
End Class

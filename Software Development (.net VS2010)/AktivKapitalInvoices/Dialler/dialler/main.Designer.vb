<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainform
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.prioritybtn = New System.Windows.Forms.Button
        Me.top20btn = New System.Windows.Forms.Button
        Me.paymentsbtn = New System.Windows.Forms.Button
        Me.exitbtn = New System.Windows.Forms.Button
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog
        Me.clntbtn = New System.Windows.Forms.Button
        Me.arrbtn = New System.Windows.Forms.Button
        Me.agentbtn = New System.Windows.Forms.Button
        Me.ooabtn = New System.Windows.Forms.Button
        Me.otherbtn = New System.Windows.Forms.Button
        Me.returnbtn = New System.Windows.Forms.Button
        Me.cmec_ooabtn = New System.Windows.Forms.Button
        Me.cmecbtn = New System.Windows.Forms.Button
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.bail_retnbtn = New System.Windows.Forms.Button
        Me.hmrcbtn = New System.Windows.Forms.Button
        Me.feesbtn = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.lsc_camp1btn = New System.Windows.Forms.Button
        Me.lsc_camp2btn = New System.Windows.Forms.Button
        Me.lsc_defaultedbtn = New System.Windows.Forms.Button
        Me.lsc_fdcbtn = New System.Windows.Forms.Button
        Me.lsc_crystalbtn = New System.Windows.Forms.Button
        Me.rtn_not_cmecbtn = New System.Windows.Forms.Button
        Me.cco_btn = New System.Windows.Forms.Button
        Me.Label4 = New System.Windows.Forms.Label
        Me.levy_van1btn = New System.Windows.Forms.Button
        Me.levy_van2btn = New System.Windows.Forms.Button
        Me.levy_van3btn = New System.Windows.Forms.Button
        Me.arrow_sch_grpbtn = New System.Windows.Forms.Button
        Me.arrow_sch_cbox = New System.Windows.Forms.ComboBox
        Me.sch_grplbl = New System.Windows.Forms.Label
        Me.cpw_schbtn = New System.Windows.Forms.Button
        Me.cpw_schlbl = New System.Windows.Forms.Label
        Me.cpw_sch_cbox = New System.Windows.Forms.ComboBox
        Me.outcallbtn = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.vulnerbtn = New System.Windows.Forms.Button
        Me.UUBtn = New System.Windows.Forms.Button
        Me.UUCbox = New System.Windows.Forms.ComboBox
        Me.UUlbl = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.tma_rbtn = New System.Windows.Forms.Button
        Me.hbop_mbtn = New System.Windows.Forms.Button
        Me.tmabtn = New System.Windows.Forms.Button
        Me.hmrcagebtn = New System.Windows.Forms.Button
        Me.precom1btn = New System.Windows.Forms.Button
        Me.precom2btn = New System.Windows.Forms.Button
        Me.fvanbtn = New System.Windows.Forms.Button
        Me.rtdfvanbtn = New System.Windows.Forms.Button
        Me.fsbtn = New System.Windows.Forms.Button
        Me.dvlabtn = New System.Windows.Forms.Button
        Me.arrow_by_agebtn = New System.Windows.Forms.Button
        Me.lowellbtn = New System.Windows.Forms.Button
        Me.dvlabrokenbtn = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'prioritybtn
        '
        Me.prioritybtn.Location = New System.Drawing.Point(37, 52)
        Me.prioritybtn.Name = "prioritybtn"
        Me.prioritybtn.Size = New System.Drawing.Size(86, 23)
        Me.prioritybtn.TabIndex = 0
        Me.prioritybtn.Text = "Priority clients"
        Me.prioritybtn.UseVisualStyleBackColor = True
        '
        'top20btn
        '
        Me.top20btn.Location = New System.Drawing.Point(162, 52)
        Me.top20btn.Name = "top20btn"
        Me.top20btn.Size = New System.Drawing.Size(99, 23)
        Me.top20btn.TabIndex = 1
        Me.top20btn.Text = "Top 20 clients"
        Me.top20btn.UseVisualStyleBackColor = True
        '
        'paymentsbtn
        '
        Me.paymentsbtn.Location = New System.Drawing.Point(286, 96)
        Me.paymentsbtn.Name = "paymentsbtn"
        Me.paymentsbtn.Size = New System.Drawing.Size(110, 23)
        Me.paymentsbtn.TabIndex = 6
        Me.paymentsbtn.Text = "Payment or Broken"
        Me.paymentsbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(714, 637)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 41
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'clntbtn
        '
        Me.clntbtn.Location = New System.Drawing.Point(37, 96)
        Me.clntbtn.Name = "clntbtn"
        Me.clntbtn.Size = New System.Drawing.Size(95, 23)
        Me.clntbtn.TabIndex = 4
        Me.clntbtn.Text = "Choose Client"
        Me.clntbtn.UseVisualStyleBackColor = True
        '
        'arrbtn
        '
        Me.arrbtn.Location = New System.Drawing.Point(433, 96)
        Me.arrbtn.Name = "arrbtn"
        Me.arrbtn.Size = New System.Drawing.Size(137, 23)
        Me.arrbtn.TabIndex = 7
        Me.arrbtn.Text = "Arrangements > 3 months old"
        Me.arrbtn.UseVisualStyleBackColor = True
        '
        'agentbtn
        '
        Me.agentbtn.Location = New System.Drawing.Point(433, 52)
        Me.agentbtn.Name = "agentbtn"
        Me.agentbtn.Size = New System.Drawing.Size(109, 23)
        Me.agentbtn.TabIndex = 3
        Me.agentbtn.Text = "Select Agent"
        Me.agentbtn.UseVisualStyleBackColor = True
        '
        'ooabtn
        '
        Me.ooabtn.Location = New System.Drawing.Point(37, 615)
        Me.ooabtn.Name = "ooabtn"
        Me.ooabtn.Size = New System.Drawing.Size(126, 23)
        Me.ooabtn.TabIndex = 30
        Me.ooabtn.Text = "Out of Area"
        Me.ooabtn.UseVisualStyleBackColor = True
        '
        'otherbtn
        '
        Me.otherbtn.Location = New System.Drawing.Point(286, 52)
        Me.otherbtn.Name = "otherbtn"
        Me.otherbtn.Size = New System.Drawing.Size(86, 23)
        Me.otherbtn.TabIndex = 2
        Me.otherbtn.Text = "Other clients"
        Me.otherbtn.UseVisualStyleBackColor = True
        '
        'returnbtn
        '
        Me.returnbtn.Location = New System.Drawing.Point(162, 96)
        Me.returnbtn.Name = "returnbtn"
        Me.returnbtn.Size = New System.Drawing.Size(99, 43)
        Me.returnbtn.TabIndex = 5
        Me.returnbtn.Text = "Return Stage (incHMRC)"
        Me.returnbtn.UseVisualStyleBackColor = True
        '
        'cmec_ooabtn
        '
        Me.cmec_ooabtn.Location = New System.Drawing.Point(381, 474)
        Me.cmec_ooabtn.Name = "cmec_ooabtn"
        Me.cmec_ooabtn.Size = New System.Drawing.Size(148, 23)
        Me.cmec_ooabtn.TabIndex = 23
        Me.cmec_ooabtn.Text = "CSA/CMEC/CMECS OOA"
        Me.cmec_ooabtn.UseVisualStyleBackColor = True
        '
        'cmecbtn
        '
        Me.cmecbtn.Location = New System.Drawing.Point(216, 474)
        Me.cmecbtn.Name = "cmecbtn"
        Me.cmecbtn.Size = New System.Drawing.Size(137, 23)
        Me.cmecbtn.TabIndex = 22
        Me.cmecbtn.Text = "CSA/CMEC/CMECS"
        Me.cmecbtn.UseVisualStyleBackColor = True
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(216, 637)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(287, 23)
        Me.ProgressBar1.TabIndex = 36
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(34, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 19
        Me.Label1.Text = "COLLECT"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(34, 449)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 13)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "ENFORCEMENT"
        '
        'bail_retnbtn
        '
        Me.bail_retnbtn.Location = New System.Drawing.Point(216, 570)
        Me.bail_retnbtn.Name = "bail_retnbtn"
        Me.bail_retnbtn.Size = New System.Drawing.Size(122, 36)
        Me.bail_retnbtn.TabIndex = 28
        Me.bail_retnbtn.Text = "Return Stage         (Not CMEC)"
        Me.bail_retnbtn.UseVisualStyleBackColor = True
        '
        'hmrcbtn
        '
        Me.hmrcbtn.Location = New System.Drawing.Point(37, 149)
        Me.hmrcbtn.Name = "hmrcbtn"
        Me.hmrcbtn.Size = New System.Drawing.Size(110, 39)
        Me.hmrcbtn.TabIndex = 8
        Me.hmrcbtn.Text = "HMRC Payment or Broken"
        Me.hmrcbtn.UseVisualStyleBackColor = True
        '
        'feesbtn
        '
        Me.feesbtn.Location = New System.Drawing.Point(37, 474)
        Me.feesbtn.Name = "feesbtn"
        Me.feesbtn.Size = New System.Drawing.Size(126, 23)
        Me.feesbtn.TabIndex = 21
        Me.feesbtn.Text = "Fees only Branch 1"
        Me.feesbtn.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(665, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(93, 13)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "LAA CAMPAIGNS"
        '
        'lsc_camp1btn
        '
        Me.lsc_camp1btn.Location = New System.Drawing.Point(646, 78)
        Me.lsc_camp1btn.Name = "lsc_camp1btn"
        Me.lsc_camp1btn.Size = New System.Drawing.Size(143, 23)
        Me.lsc_camp1btn.TabIndex = 31
        Me.lsc_camp1btn.Text = "Payment due 5-13 days"
        Me.lsc_camp1btn.UseVisualStyleBackColor = True
        '
        'lsc_camp2btn
        '
        Me.lsc_camp2btn.Location = New System.Drawing.Point(646, 121)
        Me.lsc_camp2btn.Name = "lsc_camp2btn"
        Me.lsc_camp2btn.Size = New System.Drawing.Size(143, 23)
        Me.lsc_camp2btn.TabIndex = 32
        Me.lsc_camp2btn.Text = "Payment due 14-21 days"
        Me.lsc_camp2btn.UseVisualStyleBackColor = True
        '
        'lsc_defaultedbtn
        '
        Me.lsc_defaultedbtn.Location = New System.Drawing.Point(646, 165)
        Me.lsc_defaultedbtn.Name = "lsc_defaultedbtn"
        Me.lsc_defaultedbtn.Size = New System.Drawing.Size(143, 23)
        Me.lsc_defaultedbtn.TabIndex = 33
        Me.lsc_defaultedbtn.Text = "Defaulted"
        Me.lsc_defaultedbtn.UseVisualStyleBackColor = True
        '
        'lsc_fdcbtn
        '
        Me.lsc_fdcbtn.Location = New System.Drawing.Point(646, 212)
        Me.lsc_fdcbtn.Name = "lsc_fdcbtn"
        Me.lsc_fdcbtn.Size = New System.Drawing.Size(143, 23)
        Me.lsc_fdcbtn.TabIndex = 34
        Me.lsc_fdcbtn.Text = "FDC Cases"
        Me.lsc_fdcbtn.UseVisualStyleBackColor = True
        '
        'lsc_crystalbtn
        '
        Me.lsc_crystalbtn.Location = New System.Drawing.Point(646, 255)
        Me.lsc_crystalbtn.Name = "lsc_crystalbtn"
        Me.lsc_crystalbtn.Size = New System.Drawing.Size(143, 23)
        Me.lsc_crystalbtn.TabIndex = 35
        Me.lsc_crystalbtn.Text = "Crystallised cases"
        Me.lsc_crystalbtn.UseVisualStyleBackColor = True
        '
        'rtn_not_cmecbtn
        '
        Me.rtn_not_cmecbtn.Location = New System.Drawing.Point(381, 570)
        Me.rtn_not_cmecbtn.Name = "rtn_not_cmecbtn"
        Me.rtn_not_cmecbtn.Size = New System.Drawing.Size(122, 23)
        Me.rtn_not_cmecbtn.TabIndex = 29
        Me.rtn_not_cmecbtn.Text = "Return Stage (CMEC)"
        Me.rtn_not_cmecbtn.UseVisualStyleBackColor = True
        '
        'cco_btn
        '
        Me.cco_btn.Location = New System.Drawing.Point(646, 294)
        Me.cco_btn.Name = "cco_btn"
        Me.cco_btn.Size = New System.Drawing.Size(143, 23)
        Me.cco_btn.TabIndex = 36
        Me.cco_btn.Text = "CCO due"
        Me.cco_btn.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(604, 457)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(185, 13)
        Me.Label4.TabIndex = 32
        Me.Label4.Text = "CASES WITH LEVY AND VAN FEES"
        '
        'levy_van1btn
        '
        Me.levy_van1btn.Location = New System.Drawing.Point(619, 482)
        Me.levy_van1btn.Name = "levy_van1btn"
        Me.levy_van1btn.Size = New System.Drawing.Size(153, 23)
        Me.levy_van1btn.TabIndex = 38
        Me.levy_van1btn.Text = "Paid nothing No Broken Arr"
        Me.levy_van1btn.UseVisualStyleBackColor = True
        '
        'levy_van2btn
        '
        Me.levy_van2btn.Location = New System.Drawing.Point(619, 524)
        Me.levy_van2btn.Name = "levy_van2btn"
        Me.levy_van2btn.Size = New System.Drawing.Size(153, 23)
        Me.levy_van2btn.TabIndex = 39
        Me.levy_van2btn.Text = "Broken Arrangement"
        Me.levy_van2btn.UseVisualStyleBackColor = True
        '
        'levy_van3btn
        '
        Me.levy_van3btn.Location = New System.Drawing.Point(619, 562)
        Me.levy_van3btn.Name = "levy_van3btn"
        Me.levy_van3btn.Size = New System.Drawing.Size(153, 23)
        Me.levy_van3btn.TabIndex = 40
        Me.levy_van3btn.Text = "Has Paid"
        Me.levy_van3btn.UseVisualStyleBackColor = True
        '
        'arrow_sch_grpbtn
        '
        Me.arrow_sch_grpbtn.Location = New System.Drawing.Point(162, 149)
        Me.arrow_sch_grpbtn.Name = "arrow_sch_grpbtn"
        Me.arrow_sch_grpbtn.Size = New System.Drawing.Size(109, 39)
        Me.arrow_sch_grpbtn.TabIndex = 9
        Me.arrow_sch_grpbtn.Text = "Arrow Scheme Groups"
        Me.arrow_sch_grpbtn.UseVisualStyleBackColor = True
        '
        'arrow_sch_cbox
        '
        Me.arrow_sch_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.arrow_sch_cbox.Enabled = False
        Me.arrow_sch_cbox.FormattingEnabled = True
        Me.arrow_sch_cbox.Items.AddRange(New Object() {"Barclaycard", "FS", "MBNA", "Mid Val", "Mixed", "PC", "Shop", "Telco", "Terbo"})
        Me.arrow_sch_cbox.Location = New System.Drawing.Point(286, 159)
        Me.arrow_sch_cbox.Name = "arrow_sch_cbox"
        Me.arrow_sch_cbox.Size = New System.Drawing.Size(137, 21)
        Me.arrow_sch_cbox.TabIndex = 10
        '
        'sch_grplbl
        '
        Me.sch_grplbl.AutoSize = True
        Me.sch_grplbl.Location = New System.Drawing.Point(296, 143)
        Me.sch_grplbl.Name = "sch_grplbl"
        Me.sch_grplbl.Size = New System.Drawing.Size(107, 13)
        Me.sch_grplbl.TabIndex = 34
        Me.sch_grplbl.Text = "Select scheme group"
        Me.sch_grplbl.Visible = False
        '
        'cpw_schbtn
        '
        Me.cpw_schbtn.Location = New System.Drawing.Point(433, 149)
        Me.cpw_schbtn.Name = "cpw_schbtn"
        Me.cpw_schbtn.Size = New System.Drawing.Size(124, 39)
        Me.cpw_schbtn.TabIndex = 11
        Me.cpw_schbtn.Text = "CPW Scheme Groups"
        Me.cpw_schbtn.UseVisualStyleBackColor = True
        '
        'cpw_schlbl
        '
        Me.cpw_schlbl.AutoSize = True
        Me.cpw_schlbl.Location = New System.Drawing.Point(450, 195)
        Me.cpw_schlbl.Name = "cpw_schlbl"
        Me.cpw_schlbl.Size = New System.Drawing.Size(107, 13)
        Me.cpw_schlbl.TabIndex = 36
        Me.cpw_schlbl.Text = "Select scheme group"
        Me.cpw_schlbl.Visible = False
        '
        'cpw_sch_cbox
        '
        Me.cpw_sch_cbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cpw_sch_cbox.Enabled = False
        Me.cpw_sch_cbox.FormattingEnabled = True
        Me.cpw_sch_cbox.Items.AddRange(New Object() {"CPW", "Mixed", "Telco", "Terbo", "PC"})
        Me.cpw_sch_cbox.Location = New System.Drawing.Point(433, 210)
        Me.cpw_sch_cbox.Name = "cpw_sch_cbox"
        Me.cpw_sch_cbox.Size = New System.Drawing.Size(137, 21)
        Me.cpw_sch_cbox.TabIndex = 12
        '
        'outcallbtn
        '
        Me.outcallbtn.Location = New System.Drawing.Point(646, 403)
        Me.outcallbtn.Name = "outcallbtn"
        Me.outcallbtn.Size = New System.Drawing.Size(143, 23)
        Me.outcallbtn.TabIndex = 37
        Me.outcallbtn.Text = "Cases to Outcall"
        Me.outcallbtn.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(683, 378)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(75, 13)
        Me.Label5.TabIndex = 37
        Me.Label5.Text = "HIGH COURT"
        '
        'vulnerbtn
        '
        Me.vulnerbtn.Location = New System.Drawing.Point(161, 282)
        Me.vulnerbtn.Name = "vulnerbtn"
        Me.vulnerbtn.Size = New System.Drawing.Size(110, 45)
        Me.vulnerbtn.TabIndex = 16
        Me.vulnerbtn.Text = "Vulnerable Collect/HMRC"
        Me.vulnerbtn.UseVisualStyleBackColor = True
        '
        'UUBtn
        '
        Me.UUBtn.Location = New System.Drawing.Point(280, 207)
        Me.UUBtn.Name = "UUBtn"
        Me.UUBtn.Size = New System.Drawing.Size(109, 45)
        Me.UUBtn.TabIndex = 14
        Me.UUBtn.Text = "United Utilities"
        Me.UUBtn.UseVisualStyleBackColor = True
        '
        'UUCbox
        '
        Me.UUCbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.UUCbox.FormattingEnabled = True
        Me.UUCbox.Items.AddRange(New Object() {"Domestic", "Commercial", "Other"})
        Me.UUCbox.Location = New System.Drawing.Point(280, 255)
        Me.UUCbox.Name = "UUCbox"
        Me.UUCbox.Size = New System.Drawing.Size(121, 21)
        Me.UUCbox.TabIndex = 40
        Me.UUCbox.Visible = False
        '
        'UUlbl
        '
        Me.UUlbl.AutoSize = True
        Me.UUlbl.Location = New System.Drawing.Point(154, 255)
        Me.UUlbl.Name = "UUlbl"
        Me.UUlbl.Size = New System.Drawing.Size(107, 13)
        Me.UUlbl.TabIndex = 41
        Me.UUlbl.Text = "Select scheme group"
        Me.UUlbl.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(34, 413)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(179, 13)
        Me.Label6.TabIndex = 42
        Me.Label6.Text = "TRANSFERRED FROM MARSTON"
        '
        'tma_rbtn
        '
        Me.tma_rbtn.Location = New System.Drawing.Point(277, 413)
        Me.tma_rbtn.Name = "tma_rbtn"
        Me.tma_rbtn.Size = New System.Drawing.Size(75, 23)
        Me.tma_rbtn.TabIndex = 20
        Me.tma_rbtn.Text = "TMA"
        Me.tma_rbtn.UseVisualStyleBackColor = True
        '
        'hbop_mbtn
        '
        Me.hbop_mbtn.Location = New System.Drawing.Point(381, 413)
        Me.hbop_mbtn.Name = "hbop_mbtn"
        Me.hbop_mbtn.Size = New System.Drawing.Size(87, 23)
        Me.hbop_mbtn.TabIndex = 21
        Me.hbop_mbtn.Text = "HBOP - M"
        Me.hbop_mbtn.UseVisualStyleBackColor = True
        '
        'tmabtn
        '
        Me.tmabtn.Location = New System.Drawing.Point(501, 282)
        Me.tmabtn.Name = "tmabtn"
        Me.tmabtn.Size = New System.Drawing.Size(110, 45)
        Me.tmabtn.TabIndex = 19
        Me.tmabtn.Text = "TMA Schemes"
        Me.tmabtn.UseVisualStyleBackColor = True
        '
        'hmrcagebtn
        '
        Me.hmrcagebtn.Location = New System.Drawing.Point(37, 210)
        Me.hmrcagebtn.Name = "hmrcagebtn"
        Me.hmrcagebtn.Size = New System.Drawing.Size(86, 44)
        Me.hmrcagebtn.TabIndex = 12
        Me.hmrcagebtn.Text = "HMRC by Case Age"
        Me.hmrcagebtn.UseVisualStyleBackColor = True
        '
        'precom1btn
        '
        Me.precom1btn.Location = New System.Drawing.Point(37, 518)
        Me.precom1btn.Name = "precom1btn"
        Me.precom1btn.Size = New System.Drawing.Size(126, 23)
        Me.precom1btn.TabIndex = 24
        Me.precom1btn.Text = "Pre Committal Letter 1"
        Me.precom1btn.UseVisualStyleBackColor = True
        '
        'precom2btn
        '
        Me.precom2btn.Location = New System.Drawing.Point(216, 518)
        Me.precom2btn.Name = "precom2btn"
        Me.precom2btn.Size = New System.Drawing.Size(126, 23)
        Me.precom2btn.TabIndex = 25
        Me.precom2btn.Text = "Pre Committal Letter 2"
        Me.precom2btn.UseVisualStyleBackColor = True
        '
        'fvanbtn
        '
        Me.fvanbtn.Location = New System.Drawing.Point(381, 518)
        Me.fvanbtn.Name = "fvanbtn"
        Me.fvanbtn.Size = New System.Drawing.Size(140, 23)
        Me.fvanbtn.TabIndex = 26
        Me.fvanbtn.Text = "Further Van (excl RTD)"
        Me.fvanbtn.UseVisualStyleBackColor = True
        '
        'rtdfvanbtn
        '
        Me.rtdfvanbtn.Location = New System.Drawing.Point(37, 570)
        Me.rtdfvanbtn.Name = "rtdfvanbtn"
        Me.rtdfvanbtn.Size = New System.Drawing.Size(126, 23)
        Me.rtdfvanbtn.TabIndex = 27
        Me.rtdfvanbtn.Text = "Further Van (RTD)"
        Me.rtdfvanbtn.UseVisualStyleBackColor = True
        '
        'fsbtn
        '
        Me.fsbtn.Location = New System.Drawing.Point(277, 283)
        Me.fsbtn.Name = "fsbtn"
        Me.fsbtn.Size = New System.Drawing.Size(86, 44)
        Me.fsbtn.TabIndex = 17
        Me.fsbtn.Text = "Financial Services"
        Me.fsbtn.UseVisualStyleBackColor = True
        '
        'dvlabtn
        '
        Me.dvlabtn.Location = New System.Drawing.Point(37, 283)
        Me.dvlabtn.Name = "dvlabtn"
        Me.dvlabtn.Size = New System.Drawing.Size(86, 44)
        Me.dvlabtn.TabIndex = 15
        Me.dvlabtn.Text = "DVLA by Case Age"
        Me.dvlabtn.UseVisualStyleBackColor = True
        '
        'arrow_by_agebtn
        '
        Me.arrow_by_agebtn.Location = New System.Drawing.Point(161, 207)
        Me.arrow_by_agebtn.Name = "arrow_by_agebtn"
        Me.arrow_by_agebtn.Size = New System.Drawing.Size(109, 39)
        Me.arrow_by_agebtn.TabIndex = 13
        Me.arrow_by_agebtn.Text = "Arrow by Case Age"
        Me.arrow_by_agebtn.UseVisualStyleBackColor = True
        '
        'lowellbtn
        '
        Me.lowellbtn.Location = New System.Drawing.Point(381, 283)
        Me.lowellbtn.Name = "lowellbtn"
        Me.lowellbtn.Size = New System.Drawing.Size(109, 44)
        Me.lowellbtn.TabIndex = 18
        Me.lowellbtn.Text = "Lowell by Case Age"
        Me.lowellbtn.UseVisualStyleBackColor = True
        '
        'dvlabrokenbtn
        '
        Me.dvlabrokenbtn.Location = New System.Drawing.Point(37, 333)
        Me.dvlabrokenbtn.Name = "dvlabrokenbtn"
        Me.dvlabrokenbtn.Size = New System.Drawing.Size(86, 44)
        Me.dvlabrokenbtn.TabIndex = 20
        Me.dvlabrokenbtn.Text = "DVLA Broken Arrangements"
        Me.dvlabrokenbtn.UseVisualStyleBackColor = True
        '
        'mainform
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(845, 691)
        Me.Controls.Add(Me.dvlabrokenbtn)
        Me.Controls.Add(Me.lowellbtn)
        Me.Controls.Add(Me.arrow_by_agebtn)
        Me.Controls.Add(Me.dvlabtn)
        Me.Controls.Add(Me.fsbtn)
        Me.Controls.Add(Me.rtdfvanbtn)
        Me.Controls.Add(Me.fvanbtn)
        Me.Controls.Add(Me.precom2btn)
        Me.Controls.Add(Me.precom1btn)
        Me.Controls.Add(Me.hmrcagebtn)
        Me.Controls.Add(Me.tmabtn)
        Me.Controls.Add(Me.hbop_mbtn)
        Me.Controls.Add(Me.tma_rbtn)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.UUlbl)
        Me.Controls.Add(Me.UUCbox)
        Me.Controls.Add(Me.UUBtn)
        Me.Controls.Add(Me.vulnerbtn)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.outcallbtn)
        Me.Controls.Add(Me.cpw_sch_cbox)
        Me.Controls.Add(Me.cpw_schlbl)
        Me.Controls.Add(Me.cpw_schbtn)
        Me.Controls.Add(Me.sch_grplbl)
        Me.Controls.Add(Me.arrow_sch_cbox)
        Me.Controls.Add(Me.arrow_sch_grpbtn)
        Me.Controls.Add(Me.levy_van3btn)
        Me.Controls.Add(Me.levy_van2btn)
        Me.Controls.Add(Me.levy_van1btn)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cco_btn)
        Me.Controls.Add(Me.rtn_not_cmecbtn)
        Me.Controls.Add(Me.lsc_crystalbtn)
        Me.Controls.Add(Me.lsc_fdcbtn)
        Me.Controls.Add(Me.lsc_defaultedbtn)
        Me.Controls.Add(Me.lsc_camp2btn)
        Me.Controls.Add(Me.lsc_camp1btn)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.feesbtn)
        Me.Controls.Add(Me.hmrcbtn)
        Me.Controls.Add(Me.bail_retnbtn)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.cmecbtn)
        Me.Controls.Add(Me.cmec_ooabtn)
        Me.Controls.Add(Me.returnbtn)
        Me.Controls.Add(Me.otherbtn)
        Me.Controls.Add(Me.ooabtn)
        Me.Controls.Add(Me.agentbtn)
        Me.Controls.Add(Me.arrbtn)
        Me.Controls.Add(Me.clntbtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.paymentsbtn)
        Me.Controls.Add(Me.top20btn)
        Me.Controls.Add(Me.prioritybtn)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "mainform"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Phone Dialler"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents prioritybtn As System.Windows.Forms.Button
    Friend WithEvents top20btn As System.Windows.Forms.Button
    Friend WithEvents paymentsbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents clntbtn As System.Windows.Forms.Button
    Friend WithEvents arrbtn As System.Windows.Forms.Button
    Friend WithEvents agentbtn As System.Windows.Forms.Button
    Friend WithEvents ooabtn As System.Windows.Forms.Button
    Friend WithEvents otherbtn As System.Windows.Forms.Button
    Friend WithEvents returnbtn As System.Windows.Forms.Button
    Friend WithEvents cmec_ooabtn As System.Windows.Forms.Button
    Friend WithEvents cmecbtn As System.Windows.Forms.Button
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents bail_retnbtn As System.Windows.Forms.Button
    Friend WithEvents hmrcbtn As System.Windows.Forms.Button
    Friend WithEvents feesbtn As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lsc_camp1btn As System.Windows.Forms.Button
    Friend WithEvents lsc_camp2btn As System.Windows.Forms.Button
    Friend WithEvents lsc_defaultedbtn As System.Windows.Forms.Button
    Friend WithEvents lsc_fdcbtn As System.Windows.Forms.Button
    Friend WithEvents lsc_crystalbtn As System.Windows.Forms.Button
    Friend WithEvents rtn_not_cmecbtn As System.Windows.Forms.Button
    Friend WithEvents cco_btn As System.Windows.Forms.Button
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents levy_van1btn As System.Windows.Forms.Button
    Friend WithEvents levy_van2btn As System.Windows.Forms.Button
    Friend WithEvents levy_van3btn As System.Windows.Forms.Button
    Friend WithEvents arrow_sch_grpbtn As System.Windows.Forms.Button
    Friend WithEvents arrow_sch_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents sch_grplbl As System.Windows.Forms.Label
    Friend WithEvents cpw_schbtn As System.Windows.Forms.Button
    Friend WithEvents cpw_schlbl As System.Windows.Forms.Label
    Friend WithEvents cpw_sch_cbox As System.Windows.Forms.ComboBox
    Friend WithEvents outcallbtn As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents vulnerbtn As System.Windows.Forms.Button
    Friend WithEvents UUBtn As System.Windows.Forms.Button
    Friend WithEvents UUCbox As System.Windows.Forms.ComboBox
    Friend WithEvents UUlbl As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tma_rbtn As System.Windows.Forms.Button
    Friend WithEvents hbop_mbtn As System.Windows.Forms.Button
    Friend WithEvents tmabtn As System.Windows.Forms.Button
    Friend WithEvents hmrcagebtn As System.Windows.Forms.Button
    Friend WithEvents precom1btn As System.Windows.Forms.Button
    Friend WithEvents precom2btn As System.Windows.Forms.Button
    Friend WithEvents fvanbtn As System.Windows.Forms.Button
    Friend WithEvents rtdfvanbtn As System.Windows.Forms.Button
    Friend WithEvents fsbtn As System.Windows.Forms.Button
    Friend WithEvents dvlabtn As System.Windows.Forms.Button
    Friend WithEvents arrow_by_agebtn As System.Windows.Forms.Button
    Friend WithEvents lowellbtn As System.Windows.Forms.Button
    Friend WithEvents dvlabrokenbtn As System.Windows.Forms.Button

End Class

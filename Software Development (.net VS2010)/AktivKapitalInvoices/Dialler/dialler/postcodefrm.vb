Public Class postcodefrm
    Dim saved_ooa_code As Integer
    Private Sub postcodefrm_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        'insert any new rows in the table
        Dim row As DataGridViewRow
        Dim idx As Integer = 0
        For Each row In DataGridView1.Rows
            Dim ooa_code As Integer
            Dim ooa_name As String
            Try
                ooa_code = DataGridView1.Rows(idx).Cells(0).FormattedValue
            Catch ex As Exception
                idx += 1
                Continue For
            End Try

            If ooa_code = 0 Then
                idx += 1
                Continue For
            End If
            Try
                ooa_name = DataGridView1.Rows(idx).Cells(1).Value
            Catch ex As Exception
                idx += 1
                Continue For
            End Try
            ooa_name = DataGridView1.Rows(idx).Cells(1).Value
            Dim out_of_areatable2 As New FeesSQLDataSet.Out_of_areaDataTable
            Me.Out_of_areaTableAdapter.FillBy(out_of_areatable2, ooa_code)
            Try
                Dim test As Integer = out_of_areatable2.Rows(0).Item(0)
            Catch
                Try
                    Me.Out_of_areaTableAdapter.InsertQuery(ooa_name)
                Catch ex As Odbc.OdbcException
                    MessageBox.Show("Insert of Postcode failed")
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try
            End Try
            idx += 1
        Next
        saved_ooa_code = 0
    End Sub

    Private Sub postcodefrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Out_of_areaTableAdapter.Fill(Me.FeesSQLDataSet.Out_of_area)
    End Sub

    Private Sub DataGridView1_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles DataGridView1.CellValidating

    End Sub

    Private Sub DataGridView1_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellValueChanged
        If e.ColumnIndex = 1 And e.RowIndex >= 0 Then
            Dim ooa_code As Integer
            Dim ooa_name As String
            ooa_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
            ooa_name = DataGridView1.Rows(e.RowIndex).Cells(1).Value
            If DataGridView1.Rows(e.RowIndex).IsNewRow Then Return
            Try
                Me.Out_of_areaTableAdapter.UpdateQuery(ooa_name, ooa_code)
            Catch ex As Odbc.OdbcException
                MessageBox.Show("Duplicate Postcode entered")
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub DataGridView1_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.RowEnter
        saved_ooa_code = DataGridView1.Rows(e.RowIndex).Cells(0).Value
    End Sub

    Private Sub DataGridView1_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles DataGridView1.RowsRemoved
        If e.RowIndex >= 0 And saved_ooa_code > 0 Then
            Try
                Me.Out_of_areaTableAdapter.DeleteQuery(saved_ooa_code)
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub
End Class
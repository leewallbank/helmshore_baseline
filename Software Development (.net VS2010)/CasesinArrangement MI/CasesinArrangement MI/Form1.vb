﻿Imports CommonLibrary
Public Class Form1
    Dim outfile As String = "CaseID,Scheme, Balance OS,Fees OS,Client OS, Comm rate, Arrange Interval,Arrange Started, Arrange Amount" & vbNewLine
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim run_date As Date = ("may 31, 2013")
        'get all cases open at run date then see if it was in an arrangement then
        Dim dt As New DataTable
        '" AND _rowid < 6303876", dt, False)
        LoadDataTable("DebtRecovery", "SELECT _rowid, clientschemeID, debt_amount, debt_costs, debt_fees " & _
                                                "FROM debtor " & _
                                                "WHERE (status_open_closed = 'O' or " & _
                                                " return_date > '" & Format(run_date, "yyyy-MM-dd") & "')" & _
                                                " AND clientschemeID > 2 " & _
                                                " AND _rowid < 7449508", dt, False)
        Dim debtor_count As Integer = dt.Rows.Count
        Dim CSIDArray As Object()
        Dim DebtorRow As DataRow
        For Each DebtorRow In dt.Rows
            Threading.Thread.Sleep(75)
            Dim debtorID As Integer = DebtorRow.Item(0)
            Dim CSID As Integer = DebtorRow.Item(1)
            CSIDArray = GetSQLResultsArray("DebtRecovery", "SELECT branchID, schemeID, fee_comm1 " & _
                                                   "FROM clientScheme " & _
                                                   "WHERE _rowid = " & CSID)
            If CSIDArray(0) <> 2 And CSIDArray(0) <> 8 Then
                Continue For
            End If
            'see if in arrangement at rundate
            Dim dt2 As New DataTable
            LoadDataTable("DebtRecovery", "SELECT type, text, _createdDate " & _
                                                    "FROM note " & _
                                                    "WHERE (type = 'Arrangement'" & _
                                                    " OR type = 'Broken'" & _
                                                    " OR type = 'removed agent')" & _
                                                    " AND debtorID =" & debtorID & _
                                                    " Order by _createdDate, _rowid desc", dt2, False)
            Dim NoteRow As DataRow
            Dim InArrangement As Boolean = False
            Dim Notetype As String = ""
            Dim NoteText As String = ""
            For Each NoteRow In dt2.Rows
                Dim Notedate As Date = NoteRow.Item(2)
                If Format(Notedate, "yyyy-MM-dd") > Format(run_date, "yyyy-MM-dd") Then
                    Exit For
                End If
                Notetype = NoteRow.Item(0)
                If Notetype = "Arrangement" Then
                    InArrangement = True
                    NoteText = NoteRow.Item(1)
                Else
                    InArrangement = False
                End If
            Next
            If Not InArrangement Then
                Continue For
            End If
            'case in arrangement found
            'check balance at rundate
            Dim dt3 As New DataTable
            Dim PaymentArray As Object()
            PaymentArray = GetSQLResultsArray("DebtRecovery", "SELECT sum(split_debt+split_costs), sum(split_fees+split_other+split_van) " & _
                                                   "FROM payment " & _
                                                   "WHERE debtorID = " & debtorID & _
                                                   " AND (status = 'W' or status = 'R')" & _
                                                   " AND status_date <= '" & Format(run_date, "yyyy-MM-dd") & "'")
            Dim paidClientAmount As Decimal
            Try
                paidClientAmount = PaymentArray(0)
            Catch ex As Exception

            End Try
            Dim paidFeesAmount As Decimal
            Try
                paidFeesAmount = PaymentArray(1)
            Catch ex As Exception

            End Try

            Dim debtAmount As Decimal = DebtorRow.Item(2) + DebtorRow.Item(3)
            If paidClientAmount + paidFeesAmount >= debtAmount Then
                Continue For
            End If

            Dim schemeArray As Object()
            Dim schemeID As Integer = CSIDArray(1)
            schemeArray = GetSQLResultsArray("DebtRecovery", "SELECT name " & _
                                                   "FROM scheme " & _
                                                   "WHERE _rowid = " & schemeID)
            Dim schemeName As String = schemeArray(0)
            Dim ClientbalanceOS As Decimal = debtAmount - paidClientAmount

            Dim FeeArray As Object()
            FeeArray = GetSQLResultsArray("DebtRecovery", "SELECT sum(fee_amount) " & _
                                                   "FROM fee " & _
                                                   "WHERE debtorID = " & debtorID & _
                                                   " AND fee_remit_col between 3 and 5 " & _
                                                   " AND date <= '" & Format(run_date, "yyyy-MM-dd") & "'")
            Dim feesOS As Decimal
            Try
                feesOS = FeeArray(0) - paidFeesAmount
            Catch ex As Exception
                feesOS = 0
            End Try
            If feesOS < 0 Then feesOS = 0

            Dim balanceOS As Decimal = feesOS + ClientbalanceOS
            Dim commRate As Decimal = CSIDArray(2)
            Dim arrangeInterval As Integer = 0
            Dim arrangeStarted As Date = Nothing
            Dim arrangeAmount As Decimal = 0
            'get arrangement interval from note
            Dim start_idx As Integer = InStr(NoteText, "every")
            If start_idx > 0 Then
                Dim test_amount As String
                Dim follow_idx As Integer = InStr(NoteText, "followed by")
                If follow_idx = 0 Or follow_idx > start_idx Then
                    test_amount = Microsoft.VisualBasic.Left(NoteText, start_idx - 1)
                Else
                    test_amount = Mid(NoteText, follow_idx + 11, start_idx - follow_idx - 11)
                End If

                Try
                    arrangeAmount = test_amount
                Catch ex As Exception
                    'MsgBox("Invalid arrange amount")
                End Try
                If arrangeAmount = 0 Then
                    Continue For
                End If
                follow_idx = InStr(NoteText, "days")
                If follow_idx > start_idx Then
                    Try
                        arrangeInterval = Mid(NoteText, start_idx + 5, follow_idx - start_idx - 5)
                    Catch ex As Exception
                        MsgBox("what is interval")
                    End Try
                End If
                'get arrangement started date from note
                start_idx = InStr(NoteText, "due on")
                If start_idx > 0 Then
                    Dim date_str As String = Mid(NoteText, start_idx + 6, 13)
                    Try
                        arrangeStarted = CDate(date_str)
                    Catch ex As Exception
                        MsgBox("what is arrange start date")
                    End Try
                End If



                outfile &= debtorID & "," & schemeName & "," & balanceOS & "," & feesOS & "," & ClientbalanceOS & "," &
                    commRate & "," & arrangeInterval & "," & arrangeStarted & "," & arrangeAmount & vbNewLine
            End If
        Next
        WriteFile("H:temp/ArrangementCases.txt", outfile)
        MsgBox("Report finished")
        Me.Close()
    End Sub
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.clientlbl = New System.Windows.Forms.Label()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.clientbtn = New System.Windows.Forms.Button()
        Me.cl_tbox = New System.Windows.Forms.TextBox()
        Me.datebtn = New System.Windows.Forms.Button()
        Me.startdtp = New System.Windows.Forms.DateTimePicker()
        Me.enddtp = New System.Windows.Forms.DateTimePicker()
        Me.startlbl = New System.Windows.Forms.Label()
        Me.endlbl = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.branchbtn = New System.Windows.Forms.Button()
        Me.brrbtn = New System.Windows.Forms.RadioButton()
        Me.selectrbtn = New System.Windows.Forms.RadioButton()
        Me.allrbtn = New System.Windows.Forms.RadioButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.callrbtn = New System.Windows.Forms.RadioButton()
        Me.visitsrbtn = New System.Windows.Forms.RadioButton()
        Me.debtyearcbox = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(350, 413)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 3
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'clientlbl
        '
        Me.clientlbl.AutoSize = True
        Me.clientlbl.Location = New System.Drawing.Point(146, 16)
        Me.clientlbl.Name = "clientlbl"
        Me.clientlbl.Size = New System.Drawing.Size(124, 13)
        Me.clientlbl.TabIndex = 2
        Me.clientlbl.Text = "Enter start of client name"
        Me.clientlbl.Visible = False
        '
        'clientbtn
        '
        Me.clientbtn.Location = New System.Drawing.Point(156, 62)
        Me.clientbtn.Name = "clientbtn"
        Me.clientbtn.Size = New System.Drawing.Size(106, 23)
        Me.clientbtn.TabIndex = 1
        Me.clientbtn.Text = "Select Client"
        Me.clientbtn.UseVisualStyleBackColor = True
        Me.clientbtn.Visible = False
        '
        'cl_tbox
        '
        Me.cl_tbox.Location = New System.Drawing.Point(162, 32)
        Me.cl_tbox.Name = "cl_tbox"
        Me.cl_tbox.Size = New System.Drawing.Size(100, 20)
        Me.cl_tbox.TabIndex = 0
        Me.cl_tbox.Visible = False
        '
        'datebtn
        '
        Me.datebtn.Location = New System.Drawing.Point(146, 106)
        Me.datebtn.Name = "datebtn"
        Me.datebtn.Size = New System.Drawing.Size(122, 23)
        Me.datebtn.TabIndex = 4
        Me.datebtn.Text = "Select Dates"
        Me.datebtn.UseVisualStyleBackColor = True
        '
        'startdtp
        '
        Me.startdtp.Location = New System.Drawing.Point(67, 158)
        Me.startdtp.Name = "startdtp"
        Me.startdtp.Size = New System.Drawing.Size(123, 20)
        Me.startdtp.TabIndex = 5
        Me.startdtp.Visible = False
        '
        'enddtp
        '
        Me.enddtp.Location = New System.Drawing.Point(259, 158)
        Me.enddtp.Name = "enddtp"
        Me.enddtp.Size = New System.Drawing.Size(130, 20)
        Me.enddtp.TabIndex = 6
        Me.enddtp.Visible = False
        '
        'startlbl
        '
        Me.startlbl.AutoSize = True
        Me.startlbl.Location = New System.Drawing.Point(97, 142)
        Me.startlbl.Name = "startlbl"
        Me.startlbl.Size = New System.Drawing.Size(51, 13)
        Me.startlbl.TabIndex = 7
        Me.startlbl.Text = "start date"
        Me.startlbl.Visible = False
        '
        'endlbl
        '
        Me.endlbl.AutoSize = True
        Me.endlbl.Location = New System.Drawing.Point(290, 142)
        Me.endlbl.Name = "endlbl"
        Me.endlbl.Size = New System.Drawing.Size(49, 13)
        Me.endlbl.TabIndex = 8
        Me.endlbl.Text = "end date"
        Me.endlbl.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.branchbtn)
        Me.GroupBox1.Controls.Add(Me.brrbtn)
        Me.GroupBox1.Controls.Add(Me.selectrbtn)
        Me.GroupBox1.Controls.Add(Me.allrbtn)
        Me.GroupBox1.Controls.Add(Me.clientlbl)
        Me.GroupBox1.Controls.Add(Me.cl_tbox)
        Me.GroupBox1.Controls.Add(Me.clientbtn)
        Me.GroupBox1.Location = New System.Drawing.Point(77, 211)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(276, 153)
        Me.GroupBox1.TabIndex = 9
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "All Clients or selection?"
        '
        'branchbtn
        '
        Me.branchbtn.Location = New System.Drawing.Point(156, 108)
        Me.branchbtn.Name = "branchbtn"
        Me.branchbtn.Size = New System.Drawing.Size(106, 23)
        Me.branchbtn.TabIndex = 11
        Me.branchbtn.Text = "Select Branch"
        Me.branchbtn.UseVisualStyleBackColor = True
        Me.branchbtn.Visible = False
        '
        'brrbtn
        '
        Me.brrbtn.AutoSize = True
        Me.brrbtn.Location = New System.Drawing.Point(23, 108)
        Me.brrbtn.Name = "brrbtn"
        Me.brrbtn.Size = New System.Drawing.Size(121, 17)
        Me.brrbtn.TabIndex = 12
        Me.brrbtn.Text = "All Clients by Branch"
        Me.brrbtn.UseVisualStyleBackColor = True
        '
        'selectrbtn
        '
        Me.selectrbtn.AutoSize = True
        Me.selectrbtn.Checked = True
        Me.selectrbtn.Location = New System.Drawing.Point(23, 28)
        Me.selectrbtn.Name = "selectrbtn"
        Me.selectrbtn.Size = New System.Drawing.Size(84, 17)
        Me.selectrbtn.TabIndex = 10
        Me.selectrbtn.TabStop = True
        Me.selectrbtn.Text = "Select Client"
        Me.selectrbtn.UseVisualStyleBackColor = True
        '
        'allrbtn
        '
        Me.allrbtn.AutoSize = True
        Me.allrbtn.Location = New System.Drawing.Point(22, 62)
        Me.allrbtn.Name = "allrbtn"
        Me.allrbtn.Size = New System.Drawing.Size(70, 17)
        Me.allrbtn.TabIndex = 11
        Me.allrbtn.Text = "All Clients"
        Me.allrbtn.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.callrbtn)
        Me.GroupBox2.Controls.Add(Me.visitsrbtn)
        Me.GroupBox2.Location = New System.Drawing.Point(129, 12)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(200, 79)
        Me.GroupBox2.TabIndex = 10
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Calls or Visits"
        '
        'callrbtn
        '
        Me.callrbtn.AutoSize = True
        Me.callrbtn.Checked = True
        Me.callrbtn.Location = New System.Drawing.Point(17, 19)
        Me.callrbtn.Name = "callrbtn"
        Me.callrbtn.Size = New System.Drawing.Size(97, 17)
        Me.callrbtn.TabIndex = 11
        Me.callrbtn.TabStop = True
        Me.callrbtn.Text = "Outbound Calls"
        Me.callrbtn.UseVisualStyleBackColor = True
        '
        'visitsrbtn
        '
        Me.visitsrbtn.AutoSize = True
        Me.visitsrbtn.Location = New System.Drawing.Point(17, 42)
        Me.visitsrbtn.Name = "visitsrbtn"
        Me.visitsrbtn.Size = New System.Drawing.Size(66, 17)
        Me.visitsrbtn.TabIndex = 12
        Me.visitsrbtn.Text = "EA Visits"
        Me.visitsrbtn.UseVisualStyleBackColor = True
        '
        'debtyearcbox
        '
        Me.debtyearcbox.AutoCompleteCustomSource.AddRange(New String() {"2014", "2015"})
        Me.debtyearcbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.debtyearcbox.FormattingEnabled = True
        Me.debtyearcbox.Items.AddRange(New Object() {"2014", "2015", "2016"})
        Me.debtyearcbox.Location = New System.Drawing.Point(116, 388)
        Me.debtyearcbox.Name = "debtyearcbox"
        Me.debtyearcbox.Size = New System.Drawing.Size(68, 21)
        Me.debtyearcbox.TabIndex = 11
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(46, 391)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Debt Year"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(448, 448)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.debtyearcbox)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.endlbl)
        Me.Controls.Add(Me.startlbl)
        Me.Controls.Add(Me.enddtp)
        Me.Controls.Add(Me.startdtp)
        Me.Controls.Add(Me.datebtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Run RA2513 and RA2516 reports"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents clientlbl As System.Windows.Forms.Label
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents clientbtn As System.Windows.Forms.Button
    Friend WithEvents cl_tbox As System.Windows.Forms.TextBox
    Friend WithEvents datebtn As System.Windows.Forms.Button
    Friend WithEvents startdtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents enddtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents startlbl As System.Windows.Forms.Label
    Friend WithEvents endlbl As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents selectrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents allrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents callrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents visitsrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents brrbtn As System.Windows.Forms.RadioButton
    Friend WithEvents branchbtn As System.Windows.Forms.Button
    Friend WithEvents debtyearcbox As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label

End Class

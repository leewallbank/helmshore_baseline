Public Class Form1

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        debtyearcbox.SelectedIndex = 0

    End Sub

    Private Sub schbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        
    End Sub

    Private Sub clbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clientbtn.Click
        clientfrm.ShowDialog()
        If clID = 0 Then
            Exit Sub
        End If
        schemefrm.ShowDialog()
        schemefrm.sch_dg.EndEdit()
        clientbtn.Enabled = False
        disable_btns()

        'select report
        If visitsrbtn.Checked Then
            run_visits_report()
        Else
            run_calls_report()
        End If

        Me.Close()
    End Sub
    Private Sub disable_btns()
        datebtn.Enabled = False
        clientbtn.Enabled = False
        branchbtn.Enabled = False
        exitbtn.Enabled = False
    End Sub

    Private Sub run_visits_report()

        Dim schemesSelected As Boolean = False
        Dim schRow As DataGridViewRow
        For Each schRow In schemefrm.sch_dg.Rows
            If schRow.Cells(0).Value Then
                Dim schID As Integer = schRow.Cells(2).Value
                schemesSelected = True
                Exit For
            End If
        Next

        If schemesSelected Then
            run_RA2513A_report()
        Else
            run_RA2513_report()
        End If
    End Sub
    Private Sub run_calls_report()

        Dim schemesSelected As Boolean = False
        Dim schRow As DataGridViewRow
        For Each schRow In schemefrm.sch_dg.Rows
            If schRow.Cells(0).Value Then
                Dim schID As Integer = schRow.Cells(2).Value
                schemesSelected = True
                Exit For
            End If
        Next

        If schemesSelected Then
            run_RA2516A_report()
        Else
            run_RA2516_report()
        End If
    End Sub
    Private Sub run_RA2513_report()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2513report = New RA2513

        Dim start_date As Date = CDate("Jan 1, 1900")
        Dim end_Date As Date = CDate("Jan 1, 2100")
        If startdtp.Visible Then
            start_date = startdtp.Value
            end_Date = enddtp.Value
        End If
        RA2513report.SetParameterValue("start_date", start_date)
        RA2513report.SetParameterValue("end_date", end_Date)
        Dim debt_year As String = "2014"
        If debtyearcbox.SelectedIndex = 1 Then
            debt_year = "2015"
        End If
        If debtyearcbox.SelectedIndex = 2 Then
            debt_year = "2016"
        End If
        RA2513report.SetParameterValue("debt_year", debt_year)

        Dim startCLID As Integer = clID
        Dim endCLID As Integer = clID
        If clID = 0 Then
            endCLID = 99999
        End If
        RA2513report.SetParameterValue("start_clientID", startCLID)
        RA2513report.SetParameterValue("end_clientID", endCLID)

        Dim start_branchID As Integer = 0
        Dim end_branchID As Integer = 999
        If branch_no > 0 Then
            start_branchID = branch_no
            end_branchID = branch_no
        End If
        RA2513report.SetParameterValue("start_branchID", start_branchID)
        RA2513report.SetParameterValue("end_branchID", end_branchID)

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2513report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2513 EA Visits report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2513report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2513report.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2513A_report()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2513Areport = New RA2513A

        Dim start_date As Date = CDate("Jan 1, 1900")
        Dim end_Date As Date = CDate("Jan 1, 2100")
        If startdtp.Visible Then
            start_date = startdtp.Value
            end_Date = enddtp.Value
        End If
        RA2513Areport.SetParameterValue("start_date", start_date)
        RA2513Areport.SetParameterValue("end_date", end_Date)
        Dim debt_year As String = "2014"
        If debtyearcbox.SelectedIndex = 1 Then
            debt_year = "2015"
        End If
        If debtyearcbox.SelectedIndex = 2 Then
            debt_year = "2016"
        End If
        RA2513Areport.SetParameterValue("debt_year", debt_year)

        Dim csid_idx As Integer
        RA2513Areport.SetParameterValue("clientschemeID2", 0)
        RA2513Areport.SetParameterValue("clientschemeID3", 0)
        RA2513Areport.SetParameterValue("clientschemeID4", 0)
        RA2513Areport.SetParameterValue("clientschemeID5", 0)
        Dim schRow As DataGridViewRow
        For Each schRow In schemefrm.sch_dg.Rows
            If schRow.Cells(0).Value Then
                Dim clientschID As Integer = schRow.Cells(2).Value
                csid_idx += 1
                Select Case csid_idx
                    Case 1 : RA2513Areport.SetParameterValue("clientschemeID1", clientschID)
                    Case 2 : RA2513Areport.SetParameterValue("clientschemeID2", clientschID)
                    Case 3 : RA2513Areport.SetParameterValue("clientschemeID3", clientschID)
                    Case 4 : RA2513Areport.SetParameterValue("clientschemeID4", clientschID)
                    Case 5 : RA2513Areport.SetParameterValue("clientschemeID5", clientschID)
                    Case 6 : MsgBox("First 5 client schemes are being used")
                End Select
            End If
        Next

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2513Areport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2513 EA Visits report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2513Areport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2513Areport.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2516_report()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2516report = New RA2516
        Dim myArrayList1 As ArrayList = New ArrayList()
        Dim start_date As Date = CDate("Jan 1, 1900")
        Dim end_Date As Date = CDate("Jan 1, 2100")
        If startdtp.Visible Then
            start_date = startdtp.Value
            end_Date = enddtp.Value
        End If
        RA2516report.SetParameterValue("start_date", start_date)
        RA2516report.SetParameterValue("end_date", end_Date)

        Dim debt_year As String = "2014"
        If debtyearcbox.SelectedIndex = 1 Then
            debt_year = "2015"
        End If
        If debtyearcbox.SelectedIndex = 2 Then
            debt_year = "2016"
        End If
        RA2516report.SetParameterValue("debt_year", debt_year)

        RA2516report.SetParameterValue("start_clientID", clID)
        RA2516report.SetParameterValue("end_clientID", clID)

        Dim start_branchID As Integer = 0
        Dim end_branchID As Integer = 999
        If branch_no > 0 Then
            start_branchID = branch_no
            end_branchID = branch_no
        End If
        RA2516report.SetParameterValue("start_branchID", start_branchID)
        RA2516report.SetParameterValue("end_branchID", end_branchID)

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2516report)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2516 Outbound Calls report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2516report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2516report.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub run_RA2516A_report()
        Dim myConnectionInfo As CrystalDecisions.Shared.ConnectionInfo = New CrystalDecisions.Shared.ConnectionInfo()
        Dim RA2516Areport = New RA2516A

        Dim start_date As Date = CDate("Jan 1, 1900")
        Dim end_Date As Date = CDate("Jan 1, 2100")
        If startdtp.Visible Then
            start_date = startdtp.Value
            end_Date = enddtp.Value
        End If
        RA2516Areport.SetParameterValue("start_date", start_date)
        RA2516Areport.SetParameterValue("end_date", end_Date)
        Dim debt_year As String = "2014"
        If debtyearcbox.SelectedIndex = 1 Then
            debt_year = "2015"
        End If
        If debtyearcbox.SelectedIndex = 2 Then
            debt_year = "2016"
        End If
        RA2516Areport.SetParameterValue("debt_year", debt_year)

        Dim csid_idx As Integer
        RA2516Areport.SetParameterValue("clientschemeID2", 0)
        RA2516Areport.SetParameterValue("clientschemeID3", 0)
        RA2516Areport.SetParameterValue("clientschemeID4", 0)
        RA2516Areport.SetParameterValue("clientschemeID5", 0)
        Dim schRow As DataGridViewRow
        For Each schRow In schemefrm.sch_dg.Rows
            If schRow.Cells(0).Value Then
                Dim clientschID As Integer = schRow.Cells(2).Value
                csid_idx += 1
                Select Case csid_idx
                    Case 1 : RA2516Areport.SetParameterValue("clientschemeID1", clientschID)
                    Case 2 : RA2516Areport.SetParameterValue("clientschemeID2", clientschID)
                    Case 3 : RA2516Areport.SetParameterValue("clientschemeID3", clientschID)
                    Case 4 : RA2516Areport.SetParameterValue("clientschemeID4", clientschID)
                    Case 5 : RA2516Areport.SetParameterValue("clientschemeID5", clientschID)
                    Case 6 : MsgBox("First 5 client schemes are being used")
                End Select
            End If
        Next

        myConnectionInfo.ServerName = "DebtRecovery"
        myConnectionInfo.DatabaseName = "DebtRecovery"
        myConnectionInfo.UserID = "vbnet"
        myConnectionInfo.Password = "tenbv"
        SetDBLogonForReport(myConnectionInfo, RA2516Areport)
        With SaveFileDialog1
            .Title = "Save file"
            .Filter = "XLS files |*.xls"
            .DefaultExt = ".xls"
            .OverwritePrompt = True
            .FileName = "RA2516 Outbound Calls report.xls"
        End With
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            RA2516Areport.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.ExcelRecord, SaveFileDialog1.FileName)
            RA2516Areport.Close()
            MsgBox("Report saved")
        Else
            MsgBox("Report not saved")
        End If
    End Sub
    Private Sub datebtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles datebtn.Click
        If startlbl.Visible Then
            startlbl.Visible = False
            startdtp.Visible = False
            endlbl.Visible = False
            enddtp.Visible = False
        Else
            startlbl.Visible = True
            startdtp.Visible = True
            endlbl.Visible = True
            enddtp.Visible = True
        End If
    End Sub

    Private Sub selectrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles selectrbtn.CheckedChanged
        clientlbl.Visible = selectrbtn.Checked
        clientbtn.Visible = selectrbtn.Checked
        cl_tbox.Visible = selectrbtn.Checked
    End Sub

    Private Sub allrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles allrbtn.CheckedChanged
        If allrbtn.Checked Then
            If MsgBox("Run for all clients", MsgBoxStyle.YesNo, "Run for ALL clients") = MsgBoxResult.Yes Then
                exitbtn.Enabled = False
                datebtn.Enabled = False
                'select report
                If visitsrbtn.Checked Then
                    run_visits_report()
                Else
                    run_calls_report()
                End If
                Me.Close()
            End If

        End If
    End Sub

    Private Sub brrbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles brrbtn.CheckedChanged
        branchbtn.Visible = brrbtn.Checked

    End Sub

    Private Sub branch_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub branchrbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles branchbtn.Click
        Try
            branch_no = InputBox("Enter branch no", "Enter branch no")
        Catch ex As Exception
            MsgBox("Invalid branch number")
            Exit Sub
        End Try
        disable_btns()
        'Dim schemes As Boolean = False
        'If MsgBox("Do you want breakdown by schemes?", MsgBoxStyle.YesNo, "Scheme Breakdown") = MsgBoxResult.Yes Then
        '    schemes = True
        'End If

        'select report
        If visitsrbtn.Checked Then
            run_visits_report()
        Else
            run_calls_report()
        End If
        Me.Close()
    End Sub

    Private Sub cl_tbox_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cl_tbox.TextChanged

    End Sub
End Class

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class schemefrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.sch_dg = New System.Windows.Forms.DataGridView
        Me.selection = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.SchemeName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.schemeID = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.sch_dg, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'sch_dg
        '
        Me.sch_dg.AllowUserToAddRows = False
        Me.sch_dg.AllowUserToDeleteRows = False
        Me.sch_dg.AllowUserToOrderColumns = True
        Me.sch_dg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.sch_dg.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.selection, Me.SchemeName, Me.schemeID})
        Me.sch_dg.Dock = System.Windows.Forms.DockStyle.Fill
        Me.sch_dg.Location = New System.Drawing.Point(0, 0)
        Me.sch_dg.Name = "sch_dg"
        Me.sch_dg.Size = New System.Drawing.Size(405, 329)
        Me.sch_dg.TabIndex = 0
        '
        'selection
        '
        Me.selection.HeaderText = "Select"
        Me.selection.Name = "selection"
        '
        'SchemeName
        '
        Me.SchemeName.HeaderText = "Scheme"
        Me.SchemeName.Name = "SchemeName"
        Me.SchemeName.ReadOnly = True
        Me.SchemeName.Width = 250
        '
        'schemeID
        '
        Me.schemeID.HeaderText = "Column1"
        Me.schemeID.Name = "schemeID"
        Me.schemeID.Visible = False
        '
        'schemefrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(405, 329)
        Me.Controls.Add(Me.sch_dg)
        Me.Name = "schemefrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Select Schemes"
        CType(Me.sch_dg, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents sch_dg As System.Windows.Forms.DataGridView
    Friend WithEvents selection As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents SchemeName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents schemeID As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

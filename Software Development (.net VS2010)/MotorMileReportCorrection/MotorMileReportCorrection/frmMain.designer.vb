﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.lblReadingFile = New System.Windows.Forms.Label()
        Me.btnViewLogFile = New System.Windows.Forms.Button()
        Me.btnViewOutputFile = New System.Windows.Forms.Button()
        Me.btnViewInputFile = New System.Windows.Forms.Button()
        Me.btnProcessFile = New System.Windows.Forms.Button()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.week_clorbtn = New System.Windows.Forms.RadioButton()
        Me.month_clobtn = New System.Windows.Forms.RadioButton()
        Me.updbtn = New System.Windows.Forms.RadioButton()
        Me.ackbtn = New System.Windows.Forms.RadioButton()
        Me.paybtn = New System.Windows.Forms.RadioButton()
        Me.remit_dtp = New System.Windows.Forms.DateTimePicker()
        Me.remit_lbl = New System.Windows.Forms.Label()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblReadingFile
        '
        Me.lblReadingFile.AutoSize = True
        Me.lblReadingFile.BackColor = System.Drawing.Color.White
        Me.lblReadingFile.Location = New System.Drawing.Point(112, 304)
        Me.lblReadingFile.Name = "lblReadingFile"
        Me.lblReadingFile.Size = New System.Drawing.Size(72, 13)
        Me.lblReadingFile.TabIndex = 23
        Me.lblReadingFile.Text = "Reading file..."
        Me.lblReadingFile.Visible = False
        '
        'btnViewLogFile
        '
        Me.btnViewLogFile.Enabled = False
        Me.btnViewLogFile.Location = New System.Drawing.Point(265, 238)
        Me.btnViewLogFile.Name = "btnViewLogFile"
        Me.btnViewLogFile.Size = New System.Drawing.Size(153, 42)
        Me.btnViewLogFile.TabIndex = 4
        Me.btnViewLogFile.Text = "View &Log File"
        Me.btnViewLogFile.UseVisualStyleBackColor = True
        '
        'btnViewOutputFile
        '
        Me.btnViewOutputFile.Enabled = False
        Me.btnViewOutputFile.Location = New System.Drawing.Point(265, 163)
        Me.btnViewOutputFile.Name = "btnViewOutputFile"
        Me.btnViewOutputFile.Size = New System.Drawing.Size(153, 42)
        Me.btnViewOutputFile.TabIndex = 2
        Me.btnViewOutputFile.Text = "View &Output File"
        Me.btnViewOutputFile.UseVisualStyleBackColor = True
        '
        'btnViewInputFile
        '
        Me.btnViewInputFile.Enabled = False
        Me.btnViewInputFile.Location = New System.Drawing.Point(265, 87)
        Me.btnViewInputFile.Name = "btnViewInputFile"
        Me.btnViewInputFile.Size = New System.Drawing.Size(153, 42)
        Me.btnViewInputFile.TabIndex = 1
        Me.btnViewInputFile.Text = "View &Input File"
        Me.btnViewInputFile.UseVisualStyleBackColor = True
        '
        'btnProcessFile
        '
        Me.btnProcessFile.Location = New System.Drawing.Point(265, 26)
        Me.btnProcessFile.Name = "btnProcessFile"
        Me.btnProcessFile.Size = New System.Drawing.Size(153, 42)
        Me.btnProcessFile.TabIndex = 0
        Me.btnProcessFile.Text = "&Process File"
        Me.btnProcessFile.UseVisualStyleBackColor = True
        '
        'ProgressBar
        '
        Me.ProgressBar.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock
        Me.ProgressBar.Location = New System.Drawing.Point(12, 320)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(406, 22)
        Me.ProgressBar.TabIndex = 18
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.week_clorbtn)
        Me.GroupBox1.Controls.Add(Me.month_clobtn)
        Me.GroupBox1.Controls.Add(Me.updbtn)
        Me.GroupBox1.Controls.Add(Me.ackbtn)
        Me.GroupBox1.Controls.Add(Me.paybtn)
        Me.GroupBox1.Location = New System.Drawing.Point(28, 26)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(200, 208)
        Me.GroupBox1.TabIndex = 24
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Report Type"
        '
        'week_clorbtn
        '
        Me.week_clorbtn.AutoSize = True
        Me.week_clorbtn.Location = New System.Drawing.Point(11, 173)
        Me.week_clorbtn.Name = "week_clorbtn"
        Me.week_clorbtn.Size = New System.Drawing.Size(88, 17)
        Me.week_clorbtn.TabIndex = 4
        Me.week_clorbtn.Text = "Weekly CLO "
        Me.week_clorbtn.UseVisualStyleBackColor = True
        '
        'month_clobtn
        '
        Me.month_clobtn.AutoSize = True
        Me.month_clobtn.Location = New System.Drawing.Point(11, 137)
        Me.month_clobtn.Name = "month_clobtn"
        Me.month_clobtn.Size = New System.Drawing.Size(105, 17)
        Me.month_clobtn.TabIndex = 3
        Me.month_clobtn.Text = "Last Month CLO "
        Me.month_clobtn.UseVisualStyleBackColor = True
        '
        'updbtn
        '
        Me.updbtn.AutoSize = True
        Me.updbtn.Location = New System.Drawing.Point(11, 96)
        Me.updbtn.Name = "updbtn"
        Me.updbtn.Size = New System.Drawing.Size(88, 17)
        Me.updbtn.TabIndex = 2
        Me.updbtn.Text = "UPD (.xls file)"
        Me.updbtn.UseVisualStyleBackColor = True
        '
        'ackbtn
        '
        Me.ackbtn.AutoSize = True
        Me.ackbtn.Location = New System.Drawing.Point(11, 61)
        Me.ackbtn.Name = "ackbtn"
        Me.ackbtn.Size = New System.Drawing.Size(46, 17)
        Me.ackbtn.TabIndex = 1
        Me.ackbtn.Text = "ACK"
        Me.ackbtn.UseVisualStyleBackColor = True
        '
        'paybtn
        '
        Me.paybtn.AutoSize = True
        Me.paybtn.Checked = True
        Me.paybtn.Location = New System.Drawing.Point(11, 25)
        Me.paybtn.Name = "paybtn"
        Me.paybtn.Size = New System.Drawing.Size(46, 17)
        Me.paybtn.TabIndex = 0
        Me.paybtn.TabStop = True
        Me.paybtn.Text = "PAY"
        Me.paybtn.UseVisualStyleBackColor = True
        '
        'remit_dtp
        '
        Me.remit_dtp.Location = New System.Drawing.Point(28, 269)
        Me.remit_dtp.Name = "remit_dtp"
        Me.remit_dtp.Size = New System.Drawing.Size(133, 20)
        Me.remit_dtp.TabIndex = 25
        '
        'remit_lbl
        '
        Me.remit_lbl.AutoSize = True
        Me.remit_lbl.Location = New System.Drawing.Point(36, 253)
        Me.remit_lbl.Name = "remit_lbl"
        Me.remit_lbl.Size = New System.Drawing.Size(60, 13)
        Me.remit_lbl.TabIndex = 26
        Me.remit_lbl.Text = "Remit Date"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(469, 354)
        Me.Controls.Add(Me.remit_lbl)
        Me.Controls.Add(Me.remit_dtp)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.lblReadingFile)
        Me.Controls.Add(Me.btnViewLogFile)
        Me.Controls.Add(Me.btnViewOutputFile)
        Me.Controls.Add(Me.btnViewInputFile)
        Me.Controls.Add(Me.btnProcessFile)
        Me.Controls.Add(Me.ProgressBar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "MotorMile Report Correction"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblReadingFile As System.Windows.Forms.Label
    Friend WithEvents btnViewLogFile As System.Windows.Forms.Button
    Friend WithEvents btnViewOutputFile As System.Windows.Forms.Button
    Friend WithEvents btnViewInputFile As System.Windows.Forms.Button
    Friend WithEvents btnProcessFile As System.Windows.Forms.Button
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents month_clobtn As System.Windows.Forms.RadioButton
    Friend WithEvents updbtn As System.Windows.Forms.RadioButton
    Friend WithEvents ackbtn As System.Windows.Forms.RadioButton
    Friend WithEvents paybtn As System.Windows.Forms.RadioButton
    Friend WithEvents remit_dtp As System.Windows.Forms.DateTimePicker
    Friend WithEvents remit_lbl As System.Windows.Forms.Label
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents week_clorbtn As System.Windows.Forms.RadioButton

End Class

﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration
Imports System.Data.SqlClient


Public Class frmMain
    Dim remitDate As Date = Now
    Public conn As New Odbc.OdbcConnection()
    Private InputFilePath As String, FileName As String, FileExt As String
    Private outFileName As String
    Dim recordType As String = ""
    Dim FileDialog As New OpenFileDialog
    Dim LineNumber As Integer = 0
    Dim lineCount As Integer = 0
    Dim OutputFile As String = ""
    Dim outputLine As String = ""
    Dim AuditLog As String, ErrorLog As String = ""
    Dim upd_txt As String


    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click

        Try
            If month_clobtn.Checked Then
                process_monthly_closures()
            ElseIf week_clorbtn.Checked Then
                process_weekly_closures()
            ElseIf updbtn.Checked Then
                process_update()
            ElseIf paybtn.Checked Then
                process_pay()
            Else
                process_rest()
            End If

            lblReadingFile.Visible = True
            If outFileName <> "" Then
                WriteFile(outFileName, OutputFile)
                FileName = Path.GetFileNameWithoutExtension(outFileName)
                AuditLog = "File pre-processed: " & InputFilePath & FileName & ".txt" & vbCrLf
                AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
                AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

                AuditLog &= "Number of rows: " & lineCount & vbCrLf


                WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

                If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
            End If


            MsgBox("Pre-processing complete.", vbInformation + vbOKOnly, Me.Text)
            If Not month_clobtn.Checked Then
                btnViewInputFile.Enabled = True
            End If
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
    Private Sub process_update()
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        outFileName = "ROSSENDALES_" & Format(Now, "yyyyMMdd_HHmmss")
        FileExt = Path.GetExtension(FileDialog.FileName)
        outFileName = InputFilePath & outFileName & ".UPD"
        Dim excel_file_contents(,) As String = InputFromExcel(FileDialog.FileName)
        Dim upperBound As Integer = UBound(excel_file_contents)
        ProgressBar.Maximum = upperBound
        For lineIdx As Integer = 0 To upperBound
            Try
                ProgressBar.Value = lineIdx
            Catch ex As Exception

            End Try

            Application.DoEvents()
            outputLine = ""
            recordType = excel_file_contents(lineIdx, 0)
            Select Case recordType
                Case "01"
                    'header only has 9 columns - index 0 to 9
                    'tab separated text file required
                    For idx As Integer = 0 To 6
                        outputLine &= excel_file_contents(lineIdx, idx) & vbTab
                    Next
                    'col7 is the time
                    outputLine &= Format(Now, "HH:mm:ss") & vbTab
                    outputLine &= excel_file_contents(lineIdx, 8)
                    lineCount += 1
                Case "04"
                    '23 columns
                    For idx As Integer = 0 To 21
                        outputLine &= excel_file_contents(lineIdx, idx) & vbTab
                    Next
                    outputLine &= excel_file_contents(lineIdx, 22)
                    lineCount += 1
                Case "08"
                    '15 columns
                    For idx As Integer = 0 To 13
                        outputLine &= excel_file_contents(lineIdx, idx) & vbTab
                    Next
                    outputLine &= excel_file_contents(lineIdx, 14)
                    lineCount += 1
                Case "02"
                    '9 columns
                    lineCount += 1
                    For idx As Integer = 0 To 7
                        'replace line count in col 7
                        If idx = 7 Then
                            outputLine &= lineCount & vbTab
                        Else
                            outputLine &= excel_file_contents(lineIdx, idx) & vbTab
                        End If

                    Next
                    outputLine &= excel_file_contents(lineIdx, 8)

                Case Else
                    outputLine = ""
            End Select
            If outputLine <> "" Then
                OutputFile &= outputLine & vbNewLine
            End If
        Next

    End Sub
    Private Sub process_pay()
        Dim recordTotal As Decimal = 0
        lineCount = 0
        Dim trailerRecord As String = ""
        'get all pay files from onestep archives for remit date
        Dim startDate As Date = remit_dtp.Value
        startDate = CDate(Format(startDate, "yyyy, MMM dd") & " 00:00:00")
        Dim endDate As Date = DateAdd(DateInterval.Day, 1, startDate)
        endDate = CDate(Format(endDate, "yyyy, MMM dd") & " 00:00:00")
        'first get all CSIDs for Motormile
        Dim CS_dt As New DataTable
        LoadDataTable("DebtRecovery", "SELECT _rowid " & _
                                                "FROM clientscheme " & _
                                                "WHERE clientID = 1792", CS_dt, False)
        Dim csRrow As DataRow
        For Each csRrow In CS_dt.Rows
            Dim CSID As Integer = csRrow.Item(0)
            'now get remittances for each CSID
            Dim remit_dt As New DataTable
            LoadDataTable("DebtRecovery", "SELECT _rowid " & _
                                           "FROM remit " & _
                                           "WHERE clientschemeID = " & CSID & _
                                           " AND date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                                           " AND date >= '" & Format(startDate, "yyyy-MM-dd") & "'", remit_dt, False)
            Dim remitRow As DataRow

            For Each remitRow In remit_dt.Rows
                Dim remitID As Integer = remitRow.Item(0)
                Dim search_date As Date = startDate
                Dim search_name As String = "Remit-" & remitID & "-Seq-*"
                Dim search_month As String = Format(search_date, "MM") & "_" & Format(search_date, "MMM")
                Dim search_day As String = Format(search_date, "dd") & "_" & Format(search_date, "ddd")
                Dim file_path As String = "O:\DebtRecovery\Archives\" & Format(search_date, "yyyy") & "\" & search_month &
                    "\" & search_day & "\Remittances\" & remitID
                Try
                    For Each foundFile As String In My.Computer.FileSystem.GetFiles(
                  file_path,
                   Microsoft.VisualBasic.FileIO.SearchOption.SearchTopLevelOnly, search_name)
                        Dim FileContents() As String = System.IO.File.ReadAllLines(foundFile)
                        'OutputFile &= My.Computer.FileSystem.ReadAllText(foundFile)
                        For Each InputLine As String In FileContents
                            recordType = Microsoft.VisualBasic.Left(InputLine, 2)
                            Select Case recordType
                                Case "01"
                                    If lineCount = 0 Then
                                        If InputLine <> "" Then
                                            OutputFile &= InputLine & vbNewLine
                                            lineCount += 1
                                        End If
                                    End If

                                Case "02"
                                    'ignore trailer (add one at very end
                                    trailerRecord = InputLine
                                    Dim columns() As String = trailerRecord.Split(vbTab)
                                    recordTotal += columns(8)
                                Case Else
                                    If InputLine <> "" Then
                                        OutputFile &= InputLine & vbNewLine
                                        lineCount += 1
                                    End If
                            End Select
                        Next

                    Next

                Catch ex As Exception

                End Try

            Next
        Next
        If lineCount = 0 Then
            outFileName = ""
            MsgBox("No Pay files found")
        Else
            'add last trailer
            lineCount += 1
            Dim columns() As String = trailerRecord.Split(vbTab)
            'write out replacing column 7 with correct record count and col 8 with correct total
            Dim new_trailer As String = ""
            For trailer_idx = 0 To 8
                Select Case trailer_idx
                    Case Is < 6
                        new_trailer &= columns(trailer_idx) & vbTab
                    Case 7
                        new_trailer &= lineCount & vbTab
                    Case 8
                        new_trailer &= recordTotal
                End Select
            Next
            OutputFile &= new_trailer
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "PAY files |*.PAY"
                .DefaultExt = ".PAY"
                .OverwritePrompt = True
                .FileName = "ROSSENDALES_" & Format(Now, "yyyyMMdd_HHmmss") & ".PAY"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                outFileName = Path.GetFileNameWithoutExtension(SaveFileDialog1.FileName)
                InputFilePath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                InputFilePath &= "\"
                outFileName = InputFilePath & outFileName & ".PAY"
            Else
                MsgBox("Unable to save file")
                outFileName = ""
            End If
        End If

    End Sub
    Private Sub process_monthly_closures()
        Dim recordTotal As Decimal = 0
        lineCount = 0
        Dim trailerRecord As String = ""
        'get all CLO files from onestep archives for last month
        Dim startDate As Date = DateAdd(DateInterval.Month, -1, Now)
        startDate = CDate(Format(startDate, "yyyy, MMM ") & "01 00:00:00")
        Dim endDate As Date = Now
        endDate = CDate(Format(endDate, "yyyy, MMM ") & "01 00:00:00")
        'first get all CSIDs for Motormile
        Dim CS_dt As New DataTable
        LoadDataTable("DebtRecovery", "SELECT _rowid " & _
                                                "FROM clientscheme " & _
                                                "WHERE clientID = 1792", CS_dt, False)
        Dim csRrow As DataRow
        For Each csRrow In CS_dt.Rows
            Dim CSID As Integer = csRrow.Item(0)
            'now get remittances for each CSID
            Dim remit_dt As New DataTable
            LoadDataTable("DebtRecovery", "SELECT _rowid, date " & _
                                           "FROM remit " & _
                                           "WHERE clientschemeID = " & CSID & _
                                           " AND date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                                           " AND date >= '" & Format(startDate, "yyyy-MM-dd") & "'", remit_dt, False)
            Dim remitRow As DataRow

            For Each remitRow In remit_dt.Rows
                Dim remitID As Integer = remitRow.Item(0)
                Dim search_date As Date = remitRow.Item(1)
                Dim search_month As String = Format(search_date, "MM") & "_" & Format(search_date, "MMM")
                Dim search_day As String = Format(search_date, "dd") & "_" & Format(search_date, "ddd")
                Dim file_path As String = "O:\DebtRecovery\Archives\" & Format(search_date, "yyyy") & "\" & search_month &
                    "\" & search_day & "\Remittances\" & remitID
                Try
                    For Each foundFile As String In My.Computer.FileSystem.GetFiles(
                  file_path,
                   Microsoft.VisualBasic.FileIO.SearchOption.SearchTopLevelOnly, "ReturnedCases.txt")
                        Dim FileContents() As String = System.IO.File.ReadAllLines(foundFile)
                        'OutputFile &= My.Computer.FileSystem.ReadAllText(foundFile)
                        For Each InputLine As String In FileContents
                            recordType = Microsoft.VisualBasic.Left(InputLine, 2)
                            Select Case recordType
                                Case "01"
                                    If lineCount = 0 Then
                                        If InputLine <> "" Then
                                            OutputFile &= InputLine & vbNewLine
                                            lineCount += 1
                                        End If
                                    End If

                                Case "02"
                                    'ignore trailer (add one at very end
                                    trailerRecord = InputLine
                                    'Dim columns() As String = trailerRecord.Split(vbTab)
                                    'recordTotal += columns(8)
                                Case Else
                                    'ignore if debtorid already reported
                                    Dim debtorID As Integer

                                    Dim InputLineArray = InputLine.Split(vbTab)
                                    Try
                                        debtorID = InputLineArray(5)
                                    Catch ex As Exception
                                        Continue For
                                    End Try


                                    Dim reportDate As Date = Nothing
                                    Try
                                        reportDate = GetSQLResults("FeesSQL", "select report_date from Motormile_cancelled_cases" & _
                                                                   " WHERE debtorID = " & debtorID)
                                    Catch ex As Exception

                                    End Try
                                    If reportDate <> Nothing Then
                                        Continue For
                                    End If
                                    'get debt amount
                                    Dim debtAmount As Decimal
                                    debtAmount = GetSQLResults("DebtRecovery", "select debt_amount from debtor" & _
                                                                   " WHERE _rowID = " & debtorID)
                                    recordTotal += debtAmount
                                    If InputLine <> "" Then
                                        OutputFile &= InputLine & vbNewLine
                                        lineCount += 1
                                    End If
                            End Select
                        Next

                    Next

                Catch ex As Exception

                End Try

            Next
        Next
        If lineCount = 0 Then
            outFileName = ""
            MsgBox("No CLO files found")
        Else
            'add last trailer
            lineCount += 1
            Dim columns() As String = trailerRecord.Split(vbTab)
            'write out replacing column 7 with correct record count and col 8 with correct total
            Dim new_trailer As String = ""
            For trailer_idx = 0 To 8
                Select Case trailer_idx
                    Case Is < 6
                        Try
                            new_trailer &= columns(trailer_idx) & vbTab
                        Catch ex As Exception

                        End Try
                        Try
                            new_trailer &= columns(trailer_idx) & vbTab
                        Catch ex As Exception

                        End Try

                    Case 7
                        new_trailer &= lineCount & vbTab
                    Case 8
                        new_trailer &= recordTotal
                End Select
            Next
            OutputFile &= new_trailer
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "CLO files |*.CLO"
                .DefaultExt = ".CLO"
                .OverwritePrompt = True
                .FileName = "ROSSENDALES_" & Format(Now, "yyyyMMdd_HHmmss") & ".CLO"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                outFileName = Path.GetFileNameWithoutExtension(SaveFileDialog1.FileName)
                InputFilePath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                InputFilePath &= "\"
                outFileName = InputFilePath & outFileName & ".CLO"
            Else
                MsgBox("Unable to save file")
                outFileName = ""
            End If
        End If

    End Sub

    Private Sub process_weekly_closures()
        ConnectDb2("DebtRecovery")
        ConnectDb2Fees("FeesSQL")
        Dim prod_run As Boolean = False
        Dim env_str As String = ""
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            prod_run = False
        End Try
        Dim recordTotal As Decimal = 0
        lineCount = 0
        Dim trailerRecord As String = ""
        'get all CLO files from onestep archives for last month
        Dim startDate As Date = remit_dtp.Value
        startDate = CDate(Format(startDate, "yyyy, MMM dd") & " 00:00:00")
        Dim endDate As Date = DateAdd(DateInterval.Day, 7, startDate)
        endDate = CDate(Format(endDate, "yyyy, MMM dd") & " 00:00:00")
        'first get all CSIDs for Motormile
        Dim CS_dt As New DataTable
        LoadDataTable2("DebtRecovery", "SELECT _rowid " & _
                                                "FROM clientscheme " & _
                                                "WHERE clientID = 1792", CS_dt, False)
        Dim csRrow As DataRow

        For Each csRrow In CS_dt.Rows
            Dim CSID As Integer = csRrow.Item(0)
            'now get cancelled cases with return date in period
            Dim debtor_dt As New DataTable
            LoadDataTable2("DebtRecovery", "SELECT _rowid, client_ref, prevReference, client_batch, return_date, " & _
                          " return_codeID, debt_amount " & _
                                           "FROM debtor " & _
                                           "WHERE clientschemeID = " & CSID & _
                                           " and status = 'C' " & _
                                           " AND return_date < '" & Format(endDate, "yyyy-MM-dd") & "'" & _
                                           " AND return_date >= '" & Format(startDate, "yyyy-MM-dd") & "'", debtor_dt, False)
            Dim debtorRow As DataRow
            Dim rowCount As Integer = debtor_dt.Rows.Count
            For Each debtorRow In debtor_dt.Rows
                Dim debtorID As Integer = debtorRow.Item(0)
                'save debtorID in motormile_cancelled_cases table if prod run
                If prod_run Then
                    Try
                        upd_txt = "insert into Motormile_cancelled_cases (debtorid, report_date) values (" &
                   debtorID & ",'" & Format(Now, "dd/MMM/yyyy HH:mm:ss") & "')"
                        update_sql(upd_txt)
                    Catch ex As Exception

                    End Try

                End If
                Dim clientRef As String = debtorRow.Item(1)
                Dim prevREf As String = debtorRow.Item(2)
                Dim clientBatch As String = ""
                Try
                    clientBatch = debtorRow.Item(3)
                Catch ex As Exception

                End Try

                Dim returnDate As Date = debtorRow.Item(4)
                Dim returnCodeID As Integer = debtorRow.Item(5)
                Dim debtAmount As Decimal = debtorRow.Item(6)
                lineCount += 1
                If lineCount = 1 Then
                    'write heading
                    OutputFile = "01" & vbTab & "ROSSENDALES" & vbTab & Format(Now, "dd/MM/yyyy") & vbTab &
                        vbTab & vbTab & vbTab & vbTab & "0" & vbTab & "CLO" & vbNewLine
                End If
                'get return reason and codes
                Dim returnReason As String = ""
                Dim returnCode2 As String = Nothing
                Dim returnArray As Object()
                returnArray = GetSQLResultsArray2("DebtRecovery", "SELECT clientReturnCode2, overrideShort " & _
                                                         "FROM clientSchemeReturn  " & _
                                                    "WHERE clientSchemeID =  " & CSID & _
                                                    " AND returnID = " & returnCodeID)
                Try
                    returnCode2 = returnArray(0)
                    returnReason = returnArray(1)
                Catch ex As Exception

                End Try


                recordTotal += debtAmount
                OutputFile &= "10" & vbTab & "ROSSENDALES" & vbTab & Format(returnDate, "dd/MM/yyyy") & vbTab &
                clientRef & vbTab & prevREf & vbTab & debtorID & vbTab & clientBatch & vbTab &
                returnCode2 & vbTab & returnReason & vbNewLine
            Next
        Next
        If lineCount = 0 Then
            outFileName = ""
            MsgBox("No cancelled cases found")
        Else
            'add trailer
            lineCount += 2
            Dim trailer As String = "02" & vbTab & "ROSSENDALES" & vbTab & Format(Now, "dd/MM/yyyy") & vbTab & _
                vbTab & vbTab & vbTab & lineCount & vbTab & recordTotal & vbNewLine

            OutputFile &= trailer
            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "CLO files |*.CLO"
                .DefaultExt = ".CLO"
                .OverwritePrompt = True
                .FileName = "ROSSENDALES_" & Format(Now, "yyyyMMdd_HHmmss") & ".CLO"
            End With
            If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
                outFileName = Path.GetFileNameWithoutExtension(SaveFileDialog1.FileName)
                InputFilePath = Path.GetDirectoryName(SaveFileDialog1.FileName)
                InputFilePath &= "\"
                outFileName = InputFilePath & outFileName & ".CLO"
            Else
                MsgBox("Unable to save file")
                outFileName = ""
            End If
        End If

    End Sub
    Private Sub process_rest()
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        outFileName = "ROSSENDALES_" & Format(Now, "yyyyMMdd_HHmmss")
        FileExt = Path.GetExtension(FileDialog.FileName)
        If updbtn.Checked Then
            outFileName = InputFilePath & outFileName & ".UPD"
        Else
            outFileName = InputFilePath & outFileName & FileExt
        End If

        Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
        lblReadingFile.Visible = False
        ProgressBar.Maximum = UBound(FileContents)

        For Each InputLine As String In FileContents
            Try
                ProgressBar.Value = LineNumber
            Catch ex As Exception

            End Try

            Application.DoEvents()
            LineNumber += 1

            If LineNumber > UBound(FileContents) + 1 Then
                Continue For
            End If

            recordType = Microsoft.VisualBasic.Left(InputLine, 2)
            Select Case recordType
                Case "01"
                    outputLine = InputLine
                Case "02"
                    'trailer
                    Dim columns() As String = InputLine.Split(vbTab)
                    Dim recordCount As String = columns(7)
                    'write out replacing column 7 with correct total
                    outputLine = Replace(InputLine, columns(7), CStr(LineNumber))
                    lineCount = LineNumber
                Case Else
                    outputLine = InputLine
            End Select

            OutputFile &= outputLine & vbNewLine

        Next InputLine

    End Sub
    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try
            If updbtn.Checked Then
                If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")
            Else
                If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try

            If File.Exists(outFileName) Then System.Diagnostics.Process.Start(outFileName)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'last Saturday of last month
        remitDate = Now
        remitDate = CDate(Format(remitDate, "yyyy MMM ") & " 01 00:00:00")
        Dim satFound As Boolean = False
        While Not satFound
            remitDate = DateAdd(DateInterval.Day, -1, remitDate)
            If remitDate.DayOfWeek = 6 Then
                satFound = True
            End If
        End While

        remit_dtp.Value = remitDate
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed_wrp.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFil_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub clobtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles month_clobtn.CheckedChanged

    End Sub

    Private Sub week_clorbtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles week_clorbtn.CheckedChanged
        remit_lbl.Visible = week_clorbtn.Checked
        remit_dtp.Visible = week_clorbtn.Checked
        If remit_dtp.Value = remitdate Then
            remit_dtp.Value = DateAdd(DateInterval.Day, -Weekday(Now) - 6, Now)
        End If
        If week_clorbtn.Checked Then
            remit_lbl.Text = "Week Commencing Date"
        End If
    End Sub

    Private Sub paybtn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles paybtn.CheckedChanged
        remit_lbl.Visible = paybtn.Checked
        remit_dtp.Visible = paybtn.Checked
        If paybtn.Checked Then
            remit_lbl.Text = "Remit Date"
        End If
    End Sub
    Sub update_sql(ByVal upd_txt As String)
        Try
            If conn.State = ConnectionState.Closed Then
                Connect_sqlDb()
            End If
            Dim ODBCCMD As Odbc.OdbcCommand

            'Define attachment to database table specifics
            ODBCCMD = New Odbc.OdbcCommand
            With ODBCCMD
                .Connection = conn
                .CommandText = upd_txt
                .ExecuteNonQuery()
            End With

            'Clean up the connection
            ODBCCMD = Nothing

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Public Sub Connect_sqlDb()

        Try
            If Not IsNothing(conn) Then
                'This is only necessary following an exception...
                If conn.State = ConnectionState.Open Then conn.Close()
            End If
            conn.ConnectionString = ""
            conn.ConnectionString = ConfigurationManager.ConnectionStrings("FeesSQL").ConnectionString
            conn.Open()

        Catch ex As Exception
            Dim sDbName As String

            sDbName = Mid(ConfigurationManager.ConnectionStrings("FeesSQL").ConnectionString, _
                InStr(ConfigurationManager.ConnectionStrings("feesSQL").ConnectionString, "Data Source=") + 12)

            'Strip any characters off the end of the database name
            If InStr(sDbName, ";") <> 0 Then sDbName = Mid(sDbName, 1, InStr(sDbName, ";") - 1)

            MsgBox("Unable to connect to database '" & sDbName & "'", MsgBoxStyle.Critical)
            Throw ex
        End Try
    End Sub

End Class

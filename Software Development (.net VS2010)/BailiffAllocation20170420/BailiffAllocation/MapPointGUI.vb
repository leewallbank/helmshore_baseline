﻿Public Class MapPointGUI

    'http://www.mapforums.com/sitemap/t-5830.html

    Private hwMPC As IntPtr = IntPtr.Zero
    Private hwMessageFrame As IntPtr

    Private Const WM_COMMAND As Int32 = &H111
    Private Const WM_MAPPOINT_BEGIN_SCRIBBLE As Int32 = 58205
    Private Const WM_MAPPOINT_BEGIN_LINE As Int32 = 58206
    Private Const WM_MAPPOINT_BEGIN_ARROW As Int32 = 58207
    Private Const WM_MAPPOINT_BEGIN_FREEFORM As Int32 = 58208
    Private Const WM_MAPPOINT_BEGIN_RECTANGLE As Int32 = 58209
    Private Const WM_MAPPOINT_BEGIN_OVAL As Int32 = 58210
    Private Const WM_MAPPOINT_BEGIN_MEASURE As Int32 = &HE370
    Private Const WM_MAPPOINT_BEGIN_RADIUS As Int32 = &HE372
    Private Const WM_MAPPOINT_BEGIN_TEXTBOX As Int32 = 58211
    Private Const WM_MAPPOINT_DRAW_PUSHPIN As Int32 = 58202

    Private Declare Auto Function FindWindowEx Lib "user32" Alias "FindWindowEx" (ByVal hwParent As IntPtr, ByVal hwChildAfter As IntPtr, ByVal strClassName As String, ByVal strWindowCaption As String) As IntPtr
    Private Declare Auto Function GetParentWindow Lib "user32" Alias "GetParent" (ByVal hwChild As IntPtr) As IntPtr
    Private Declare Auto Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As IntPtr, ByVal wMsg As Int32, ByVal wParam As Int32, ByVal lParam As Int32) As Int32

    Public Event MPCError(ByVal strErrorMsg As String)

    Public Sub New(ByRef MPC As AxMapPoint.AxMappointControl)
        If MPC.Handle.ToInt32 = 0 Then
            RaiseEvent MPCError("MapPointControl was not initialized before calling xMapPointGui.New()")
            Exit Sub
        End If
        hwMPC = GetParentWindow(FindWindowEx(MPC.Handle, IntPtr.Zero, vbNullString, vbNullString))
        Dim hwTemp = FindWindowEx(hwMPC, IntPtr.Zero, vbNullString, "")
        hwMessageFrame = FindWindowEx(hwTemp, IntPtr.Zero, vbNullString, "")
    End Sub

    Public Sub BeginScribbleTool()
        SendMessage(hwMessageFrame, WM_COMMAND, WM_MAPPOINT_BEGIN_SCRIBBLE, 0)
    End Sub

    Public Sub BeginLineTool()
        SendMessage(hwMessageFrame, WM_COMMAND, WM_MAPPOINT_BEGIN_LINE, 0)
    End Sub

    Public Sub BeginArrowTool()
        SendMessage(hwMessageFrame, WM_COMMAND, WM_MAPPOINT_BEGIN_ARROW, 0)
    End Sub

    Public Sub BeginFreeFormTool()
        SendMessage(hwMessageFrame, WM_COMMAND, WM_MAPPOINT_BEGIN_FREEFORM, 0)
    End Sub

    Public Sub BeginRectangleTool()
        SendMessage(hwMessageFrame, WM_COMMAND, WM_MAPPOINT_BEGIN_RECTANGLE, 0)
    End Sub

    Public Sub BeginOvalTool()
        SendMessage(hwMessageFrame, WM_COMMAND, WM_MAPPOINT_BEGIN_OVAL, 0)
    End Sub

    Public Sub BeginRadiusTool()
        SendMessage(hwMessageFrame, WM_COMMAND, WM_MAPPOINT_BEGIN_RADIUS, 0)
    End Sub

    Public Sub BeginTextBoxTool()
        SendMessage(hwMessageFrame, WM_COMMAND, WM_MAPPOINT_BEGIN_TEXTBOX, 0)
    End Sub

    Public Sub BeginMeasureTool()
        SendMessage(hwMessageFrame, WM_COMMAND, WM_MAPPOINT_BEGIN_MEASURE, 0)
    End Sub

    Public Sub BeginDrawPushPinTool()
        SendMessage(hwMessageFrame, WM_COMMAND, WM_MAPPOINT_DRAW_PUSHPIN, 0)
    End Sub
End Class

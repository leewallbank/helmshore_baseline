﻿Imports CommonLibrary
Imports System.Configuration

Module modCommon

    Public DataColumnsList() As String = {"Total", "Period1", "Period2", "Period3", "Period4", "Period5", "Period6", "Period7", "Month1", "Month2", "Month3", "Month4", "Month5", "Month6", "Month7", "Month8", "Month9", "Month10", "Month11", "Month12", "YearPlus", "Day0", "Day1", "Day2", "Day3", "Day4", "Day5", "Day6", "Day7", "Day8", "Day9", "Day10", "Day11", "Day12", "Day13", "Week1", "Week2", "Week3", "Week4", "Month4to6", "Month7Plus", "Quantity"} 'Used for validation within events
    Public RefreshScroll As Boolean = True ' needed as chkTopClients will change the number of rows in dgvClientName
    Public UserCompany As String
    Public UserCompanyID As Integer
    Public StartScreen As String
    Public UserCanRefresh As Boolean

    Sub Main()
        Application.EnableVisualStyles()

        GetStartConfig()

        Select Case StartScreen
            Case "C"
                frmCaseSummary.ShowDialog()
            Case "B"
                frmBailiffSummary.ShowDialog()
            Case "P"
                frmPostEnforcementSummary.ShowDialog()
            Case "U"
                frmEAUpdateSummary.ShowDialog()
        End Select
    End Sub

    Public Sub PreRefresh(ByRef Control As Control)
        Control.Cursor = Cursors.WaitCursor
        Control.SuspendLayout()
    End Sub

    Public Sub PostRefresh(ByRef Control As Control)
        Control.ResumeLayout()
        Control.Cursor = Cursors.Default
    End Sub

    Public Sub FormatGridColumns(ByRef DataGridView As DataGridView, Optional ByVal StageList As String() = Nothing)
        If IsNothing(StageList) Then StageList = {}

        Dim PeriodNames As String() = GetPeriodNames(String.Join(vbTab, StageList))

        For Each Column As DataGridViewColumn In DataGridView.Columns
            Select Case Column.Name
                Case "StageName"
                    Column.Width = 140
                    Column.HeaderText = "Stage"
                Case "ClientName"
                    Column.Width = 170
                    Column.HeaderText = "Client"
                Case "WorkType"
                    Column.Width = 90
                    Column.HeaderText = "Work type"
                Case "LinkedPIF"
                    Column.Width = 70
                    Column.HeaderText = "Linked PIF"
                Case "SchemeName"
                    Column.Width = 100
                    Column.HeaderText = "Scheme"
                Case "Allocated"
                    Column.Width = 55
                Case "Levied"
                    Column.Width = 55
                Case "EnforcementFeesApplied"
                    Column.Width = 80
                    Column.HeaderText = "Enforcement Fees"
                Case "Payment"
                    Column.Width = 60
                Case "NumVisits"
                    Column.Width = 55
                    Column.HeaderText = "# Visits"
                Case "AddConfirmed"
                    Column.Width = 60
                    Column.HeaderText = "Confirmed"
                Case "ArrangementBroken"
                    Column.Width = 60
                    Column.HeaderText = "Broken"
                Case "InYear"
                    Column.Width = 55
                    Column.HeaderText = "In Year"
                Case "DebtYear"
                    Column.Width = 55
                    Column.HeaderText = "Year"
                Case "BailiffName"
                    Column.Width = 120
                    Column.HeaderText = "Bailiff"
                Case "PostcodeArea"
                    If DataGridView.Name = "dgvSummary" Then
                        Column.Width = 55
                        Column.HeaderText = "Area"
                    ElseIf DataGridView.Name = "dgvAllocationBreakdown" Then
                        Column.Width = 55
                        Column.HeaderText = "Area"
                    Else
                        Column.Visible = False
                    End If
                Case "HoursBetweenVisits"
                    Column.Width = 60
                    Column.HeaderText = "Hours"
                Case "Total"
                    Column.Width = 55
                Case "Period1"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(0)
                Case "Period2"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(1)
                Case "Period3"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(2)
                Case "Period4"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(3)
                Case "Period5"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(4)
                Case "Period6"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(5)
                Case "Period7"
                    Column.Width = 55
                    Column.HeaderText = PeriodNames(6)
                Case "Month1"
                    Column.Width = 55
                Case "Month2"
                    Column.Width = 55
                Case "Month3"
                    Column.Width = 55
                Case "Month4"
                    Column.Width = 55
                Case "Month5"
                    Column.Width = 55
                Case "Month6"
                    Column.Width = 55
                Case "Month7"
                    Column.Width = 55
                Case "Month8"
                    Column.Width = 55
                Case "Month9"
                    Column.Width = 55
                Case "Month10"
                    Column.Width = 55
                Case "Month11"
                    Column.Width = 55
                Case "Month12"
                    Column.Width = 55
                Case "YearPlus"
                    Column.Width = 55
                Case "Day0"
                    Column.Width = 55
                    Column.HeaderText = Today.ToString("ddd dd")
                Case "Day1"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-1).ToString("ddd dd")
                Case "Day2"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-2).ToString("ddd dd")
                Case "Day3"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-3).ToString("ddd dd")
                Case "Day4"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-4).ToString("ddd dd")
                Case "Day5"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-5).ToString("ddd dd")
                Case "Day6"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-6).ToString("ddd dd")
                Case "Day7"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-7).ToString("ddd dd")
                Case "Day8"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-8).ToString("ddd dd")
                Case "Day9"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-9).ToString("ddd dd")
                Case "Day10"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-10).ToString("ddd dd")
                Case "Day11"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-11).ToString("ddd dd")
                Case "Day12"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-12).ToString("ddd dd")
                Case "Day13"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-13).ToString("ddd dd")
                Case "Day14"
                    Column.Width = 55
                    Column.HeaderText = Today.AddDays(-14).ToString("ddd dd")
                Case "DebtorID"
                    Column.Width = 80
                    Column.HeaderText = "Case ID"
                Case "LinkID"
                    Column.Width = 70
                    Column.HeaderText = "Link ID"
                Case "Warning"
                    Column.Width = 55
                Case "add_postcode"
                    Column.Width = 70
                    Column.HeaderText = "Postcode"
                Case "CreatedDate"
                    Column.Width = 105
                    Column.HeaderText = "Created date"
                Case "debt_balance"
                    Column.Width = 60
                    Column.HeaderText = "Balance"
                Case "FeesOutstanding"
                    Column.Width = 70
                    Column.HeaderText = "Os fees"
                Case "LastAction"
                    Column.Width = 105
                    Column.HeaderText = "Last action"
                Case "MoveToEnforcementDate"
                    Column.Width = 105
                    Column.HeaderText = "Move on date"
                Case "BailiffID"
                    Column.Visible = False
                Case "name_fore"
                    Column.Width = 70
                    Column.HeaderText = "Forename"
                Case "name_sur"
                    Column.Width = 70
                    Column.HeaderText = "Surname"
                Case "BailiffType"
                    Column.Width = 60
                    Column.HeaderText = "Type"
                Case "CasesAllocated"
                    Column.Width = 60
                    Column.HeaderText = "Allocated"
                Case "MaxCases"
                    Column.Width = 50
                    Column.HeaderText = "Max"
                Case "LastAllocation"
                    Column.HeaderText = "Last Allocation"
                Case "BailiffAllocatedDate"
                    Column.Width = 70
                    Column.HeaderText = "Allocated"
                Case "LODate"
                    Column.Width = 70
                    Column.HeaderText = "LO date"
                Case "ResidencyScore"
                    Column.Width = 80
                    Column.HeaderText = "Res score"
                Case "add_phone" ' added TS 06/May/2013
                    Column.Width = 80
                    Column.HeaderText = "Telephone 1"
                Case "add_fax" ' added TS 06/May/2013
                    Column.Width = 80
                    Column.HeaderText = "Telephone 2"
                Case "add_os_easting"
                    Column.Visible = False
                Case "add_os_northing"
                    Column.Visible = False
                Case "LinkedCase"
                    Column.Visible = False
                Case "FirstTwoVisitsPAWithinTwoDays" ' added TS 24/May/2013
                    Column.Width = 80
                    Column.HeaderText = "Visit 2 < 2"
                Case "NumberOfVisits" ' added TS 13/Sep/2013
                    Column.Width = 70
                    Column.HeaderText = "# Visits"
                Case "NumberOfVisitsPA" ' added TS 24/May/2013
                    Column.Width = 80
                    Column.HeaderText = "# Visits"
                Case "AllVisitsPAWithinTwentyDays" ' added TS 24/May/2013
                    Column.Width = 85
                    Column.HeaderText = "Visits < 20"
                Case "DataSet"
                    Column.Visible = False ' added TS 03/Feb/2014
                Case "status_nextdate"
                    Column.Width = 105
                    Column.HeaderText = "Next stage"
                Case "PaymentStage"
                    Column.Width = 80
                    Column.HeaderText = "Payment"
                Case "LinkedPayment"
                    Column.Width = 70
                    Column.HeaderText = "Linked Pay"
                Case "EAName" ' following added TS 07/Jul/2015. Request ref 54224
                    Column.Width = 120
                    Column.HeaderText = "Agent"
                Case "NOI" ' following added TS 05/Aug/2015. Request ref 51775
                    Column.Width = 70
                Case "AuditCheck" ' following added TS 05/Aug/2015. Request ref 51775
                    Column.Width = 70
                    Column.HeaderText = "Audit"
                Case "GoneAway" ' following added TS 01/Oct/2015. Request ref 51775
                    Column.Width = 70
                    Column.HeaderText = "Gone away"
                Case "VisitAfterPIF" ' following added TS 01/Oct/2015. Request ref 51775
                    Column.Width = 70
                    Column.HeaderText = "V after PIF"
                Case "AuditCheckBy" ' following added TS 01/Oct/2015. Request ref 51775
                    Column.Width = 100
                    Column.HeaderText = "User"
                Case "UpdateDatetime" ' following added TS 08/Dec/2015. Request ref 67499
                    Column.Width = 100
                    Column.HeaderText = "Updated"
                Case "Week1" ' the following six added TS 10/Dec/2015. Request ref 66902
                    Column.Width = 55
                Case "Week2"
                    Column.Width = 55
                Case "Week3"
                    Column.Width = 55
                Case "Week4"
                    Column.Width = 55
                Case "Month4to6"
                    Column.Width = 55
                    Column.HeaderText = "Mth 4-6"
                Case "Month7Plus"
                    Column.Width = 55
                    Column.HeaderText = "Mth 7+" ' end of 66902 section
                Case "Period" ' following added TS 28/Jun/2016. Request ref 83034
                    Column.Width = 100
                Case "Quantity"
                    Column.Width = 100
                    Column.HeaderText = "Total" ' end of 83034 section
            End Select

        Next Column
    End Sub

    Public Function FormOpen(ByVal Formname As String) As Boolean
        FormOpen = False

        For Each frm As Form In Application.OpenForms
            If Formname = frm.Name Then FormOpen = True
        Next

    End Function

    Private Sub GetStartConfig()
        Dim Config() As Object = GetSQLResultsArray("BailiffAllocation", "EXEC dbo.GetStartConfig")

        UserCompany = Config(0)
        StartScreen = Config(1)
        UserCanRefresh = Config(2)

        ' This section added TS 13/Oct/2014. Request ref 31856
        Select Case UserCompany
            Case "Rossendales"
                UserCompanyID = 1
            Case "Marston"
                UserCompanyID = 2
            Case "CMG"
                UserCompanyID = 3
            Case Else
                UserCompanyID = -1
        End Select

    End Sub

    Private Function GetPeriodNames(ByVal StageList As String) As String()
        GetPeriodNames = GetSQLResults("BailiffAllocation", "EXEC dbo.GetPeriodNames '" & StageList & "'").Split(",")
    End Function

    Public Sub SetFormIcon(ByRef Sender As Form)
        Select Case UserCompany
            Case "Rossendales"
                Sender.Icon = My.Resources.REBUS
                Sender.Text = "REBUS - " & Sender.Text
            Case "Marston"
                Sender.Icon = My.Resources.Marston
                'If Sender.Name = "frmCaseSummary" Then
                '    Sender.Controls("cboCompany").Visible = False
                '    Sender.Controls("dgvClientName").Top = 7
                '    Sender.Controls("dgvClientName").Height = 297

                'ElseIf Sender.Name = "frmBailiffSummary" Then
                '    Sender.Controls("cboCompany").Visible = False
                '    Sender.Controls("dgvBailiffName").Top = 7
                '    Sender.Controls("dgvBailiffName").Height = 275

                'ElseIf Sender.Name = "frmPostEnforcementSummary" Then
                '    Sender.Controls("cboCompany").Visible = False
                '    Sender.Controls("dgvLastVisitBailiffName").Top = 7
                '    Sender.Controls("dgvLastVisitBailiffName").Height = 282

                'ElseIf Sender.Name = "frmEAUpdateSummary" Then
                '    Sender.Controls("cboCompany").Visible = False
                '    Sender.Controls("dgvEAName").Top = 7
                '    Sender.Controls("dgvEAName").Height = 283
                'End If
            Case "CMG"
                Sender.Icon = My.Resources.Marston
                'If Sender.Name = "frmCaseSummary" Then
                '    Sender.Controls("cboCompany").Visible = False
                '    Sender.Controls("dgvClientName").Top = 7
                '    Sender.Controls("dgvClientName").Height = 297

                'ElseIf Sender.Name = "frmBailiffSummary" Then
                '    Sender.Controls("cboCompany").Visible = False
                '    Sender.Controls("dgvBailiffName").Top = 7
                '    Sender.Controls("dgvBailiffName").Height = 275

                'ElseIf Sender.Name = "frmPostEnforcementSummary" Then
                '    Sender.Controls("cboCompany").Visible = False
                '    Sender.Controls("dgvLastVisitBailiffName").Top = 7
                '    Sender.Controls("dgvLastVisitBailiffName").Height = 282

                'ElseIf Sender.Name = "frmEAUpdateSummary" Then
                '    Sender.Controls("cboCompany").Visible = False
                '    Sender.Controls("dgvEAName").Top = 7
                '    Sender.Controls("dgvEAName").Height = 283
                'End If
        End Select

    End Sub
End Module

﻿Imports CommonLibrary

Public Class frmStartup

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        GetStartConfig()

        Select Case StartScreen
            Case "C"
                frmCaseSummary.ShowDialog()
            Case "B"
                frmBailiffSummary.ShowDialog()
            Case "P"
                frmPostEnforcementSummary.ShowDialog()
            Case "U"
                frmEAUpdateSummary.ShowDialog()
            Case "R"
                frmAllocationByRegionSummary.ShowDialog()
        End Select

        Me.Hide()
    End Sub

    Private Sub GetStartConfig()
        Dim Config() As Object = GetSQLResultsArray("BailiffAllocation", "EXEC dbo.GetStartConfig")

        UserCompany = Config(0)
        StartScreen = Config(1)
        UserCanRefresh = Config(2)

        ' This section added TS 13/Oct/2014. Request ref 31856
        Select Case UserCompany
            Case "Rossendales"
                UserCompanyID = 1
            Case "Marston"
                UserCompanyID = 2
            Case "CMG"
                UserCompanyID = 3
            Case Else
                UserCompanyID = -1
        End Select

    End Sub

    Private Sub frmStartup_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        Me.Hide()
        If Not FormOpen("frmCaseSummary") And Not FormOpen("frmBailiffSummary") And Not FormOpen("frmPostEnforcementSummary") And Not FormOpen("frmEAUpdateSummary") Then
            Application.Exit()
        End If

    End Sub

End Class
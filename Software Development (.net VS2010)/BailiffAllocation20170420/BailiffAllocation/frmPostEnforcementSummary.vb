﻿Imports CommonLibrary

Public Class frmPostEnforcementSummary


    Private SummaryData As New clsPostEnforcementSummaryData
    Private ListData As New clsListData

    Private StageGridState As clsGridState
    Private ClientGridState As clsGridState
    Private SchemeNameGridState As clsGridState
    Private PaymentStageGridState As clsGridState
    Private CGAGridState As clsGridState
    Private ResidencyScoreGridState As clsGridState
    Private NumberOfVisitsAddressGridState As clsGridState
    Private AddConfirmedGridState As clsGridState
    Private PostcodeAreaGridState As clsGridState
    Private ArrangementBrokenFourteenDaysGridState As clsGridState
    Private LinkedPIFGridState As clsGridState
    Private DebtYearGridState As clsGridState
    Private InYearGridState As clsGridState
    Private LastVisitBailiffNameGridState As clsGridState
    Private OutOfHoursVisitGridState As clsGridState
    Private HoursBetweenVisitsGridState As clsGridState
    Private AddressChangedGridState As clsGridState
    Private LastVisitBailiffTypeGridState As clsGridState

    Private ColSort As String

    Private ParamList As String

    Private RightClickColIdx As Integer ' Request ref 64648

#Region "New and open"

    Public Sub New()
        Try
            InitializeComponent()

            SetFormIcon(Me)

            cmdRefreshDB.Visible = UserCanRefresh

            ParamList = UserCompanyID.ToString & ",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0," & BalanceType() & ",14" ' i.e. top level ' BalanceType added TS 12/Feb/2015. Request ref 40282

            ' Now instantiate these
            StageGridState = New clsGridState(dgvStageName)
            ClientGridState = New clsGridState(dgvClientName)
            SchemeNameGridState = New clsGridState(dgvSchemeName)
            PaymentStageGridState = New clsGridState(dgvPaymentStage)
            CGAGridState = New clsGridState(dgvCGA)
            ResidencyScoreGridState = New clsGridState(dgvResidencyScore)
            NumberOfVisitsAddressGridState = New clsGridState(dgvNumberOfVisitsAddress)
            AddConfirmedGridState = New clsGridState(dgvAddConfirmed)
            PostcodeAreaGridState = New clsGridState(dgvPostcodeArea)
            ArrangementBrokenFourteenDaysGridState = New clsGridState(dgvArrangementBrokenFourteenDays)
            LinkedPIFGridState = New clsGridState(dgvLinkedPIF)
            DebtYearGridState = New clsGridState(dgvDebtYear)
            InYearGridState = New clsGridState(dgvInYear)
            LastVisitBailiffNameGridState = New clsGridState(dgvLastVisitBailiffName)
            OutOfHoursVisitGridState = New clsGridState(dgvOutOfHoursVisit)
            HoursBetweenVisitsGridState = New clsGridState(dgvHoursBetweenVisits)
            AddressChangedGridState = New clsGridState(dgvAddressChanged)
            LastVisitBailiffTypeGridState = New clsGridState(dgvLastVisitBailiffType)

            AddControlHandlers()

            cmsSummary.Items.Add("View Cases")
            cmsSummary.Items.Add("Copy")
            cmsSummary.Items.Add("Select All")
            cmsSummary.Items.Add("Select Column") ' Request ref 64648

            'cmsList.Items.Add("Set warning levels") commented out TS 12/Sep/2016
            cmsList.Items.Add("Regions")
            cmsList.Items.Add("Copy")
            cmsList.Items.Add("Select All")

            cmsForm.Items.Add("Allocation View")
            cmsForm.Items.Add("Bailiff View")
            cmsForm.Items.Add("EA Update View")
            cmsForm.Items.Add("Regional View")

            'ListData.GetConsortiums(UserCompanyID)

            'Dim Consortium As ToolStripMenuItem = Convert.ChangeType(cmsList.Items(0), GetType(ToolStripMenuItem))

            'For Each Row As DataRow In ListData.ConsortiumDataView.ToTable(True, "Consortium").Rows
            '    Dim SubMenuItem As ToolStripMenuItem = New ToolStripMenuItem(Row.Item(0).ToString)

            '    AddHandler SubMenuItem.Click, AddressOf ConsortiumContextSubMenu_Click
            '    Consortium.DropDownItems.Add(SubMenuItem)
            'Next Row

            ' Added TS 09/Feb/2015. Request ref 38930
            ListData.GetPostcodeAreaRegions()

            Dim Region As ToolStripMenuItem = Convert.ChangeType(cmsList.Items(1), GetType(ToolStripMenuItem)) ' Items(2) changed to Items(1) TS 16/Nov/2015. Request ref 63535

            For Each Row As DataRow In ListData.PostcodeAreaRegionDataView.ToTable(True, "RegionDesc").Rows
                Dim RegionSubMenuItem As ToolStripMenuItem = New ToolStripMenuItem(Row.Item(0).ToString)

                AddHandler RegionSubMenuItem.Click, AddressOf RegionContextSubMenu_Click
                Region.DropDownItems.Add(RegionSubMenuItem)
            Next Row
            ' end of 38930

            cboBailiffEmp.SelectedIndex = 0

            SummaryData.GetSummary(ParamList, True)
            dgvSummary.DataSource = SummaryData.SummaryDataView

            FormatGridColumns(dgvSummary)

            ListData.GetPostEnforcementList(ParamList)
            SetListGrids()
            FormatListColumns()

            cboPeriodType.DataSource = SummaryData.PeriodTypeDataView
            cboPeriodType.ValueMember = "PeriodTypeID"
            cboPeriodType.DisplayMember = "PeriodTypeDesc"

            cboCompany.DataSource = SummaryData.CompanyDataView
            cboCompany.ValueMember = "CompanyID"
            cboCompany.DisplayMember = "CompanyDesc"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmSummary_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            RemoveSelectionHandlers()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmCaseSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        Try
            If e.Button = MouseButtons.Right Then
                cmsForm.Show(sender, e.Location)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmSummary_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            HighlightWarnings()
            SetSelections() 'Arrays will be nothing so clear all selections - by default the first row in a datagridview is always selected
            dgvSummary.ClearSelection()

            radAbs.Checked = True
            cboCompany.Text = UserCompany
            chkTopClients.Checked = False
            cboPeriodType.SelectedValue = 14 ' changed from 1 TS 18/Dec/2013. Changed from 8 TS 08/Dec/2015

            AddHandler radAbs.CheckedChanged, AddressOf radAbs_CheckedChanged
            AddHandler chkTopClients.CheckedChanged, AddressOf chkTopClients_CheckedChanged

            AddSelectionHandlers()

            SetcmdRefreshTip()

            ' Commented out TS 12/Sep/2016. Request ref 90026
            ' This is duplicated from FormatListColumns as that sub runs before the form is shown
            'For Each Row As DataGridViewRow In dgvClientName.Rows
            '    SummaryData.UnderAllocatedClientDataView.RowFilter = "ClientName = '" & Row.Cells("ClientName").Value & "'"
            '    If SummaryData.UnderAllocatedClientDataView.Count > 0 Then
            '        Row.DefaultCellStyle.ForeColor = Color.Red
            '    Else
            '        Row.DefaultCellStyle.ForeColor = Color.Black
            '    End If
            'Next Row
            ' end of 90026 section

            ' This section added TS 08/Dec/2015. Request ref 66905
            dgvLastVisitBailiffName.Columns(0).ToolTipText = "The last EA to visit."
            dgvInYear.Columns(0).ToolTipText = "Was the case loaded in this financial year?"
            dgvPaymentStage.Columns(0).ToolTipText = "Has there been a payment in the last year if so at what stage?"
            dgvLinkedPIF.Columns(0).ToolTipText = "Where a linked case has PIF in the last year."
            dgvArrangementBrokenFourteenDays.Columns(0).ToolTipText = "Has an arrangement broken in the last fourteen days?"
            dgvNumberOfVisitsAddress.Columns(0).ToolTipText = "The number of visits since the last address change."
            dgvOutOfHoursVisit.Columns(0).ToolTipText = "Has there been a visit out of hours i.e. Sat/Sun or between 1730 and 0830?"
            dgvHoursBetweenVisits.Columns(0).ToolTipText = "The hours difference between the earliest and latest call on the case regardless of date."
            dgvAddressChanged.Columns(0).ToolTipText = "Has the address ever been changed?"
            ' end of 66905 section

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "Refresh"

    Private Sub RefreshGrid()
        Try
            ParamList = cboCompany.SelectedValue.ToString & ","
            ParamList &= GetParam(dgvStageName, "StageName") & ","
            ParamList &= GetParam(dgvClientName, "ClientName") & ","
            ParamList &= GetParam(dgvSchemeName, "SchemeName") & ","
            ParamList &= GetParam(dgvPaymentStage, "PaymentStage") & ","
            ParamList &= GetParam(dgvPostcodeArea, "PostcodeArea") & ","
            ParamList &= GetParam(dgvCGA, "CGA") & ","
            ParamList &= GetParam(dgvLinkedPIF, "LinkedPIF") & ","
            ParamList &= GetParam(dgvArrangementBrokenFourteenDays, "ArrangementBrokenFourteenDays") & ","
            ParamList &= GetParam(dgvAddConfirmed, "AddConfirmed") & ","
            ParamList &= GetParam(dgvNumberOfVisitsAddress, "NumberOfVisitsAddress") & ","
            ParamList &= GetParam(dgvResidencyScore, "ResidencyScore") & ","
            ParamList &= GetParam(dgvDebtYear, "DebtYear") & ","
            ParamList &= GetParam(dgvInYear, "InYear") & ","
            ParamList &= GetParam(dgvLastVisitBailiffName, "LastVisitBailiffName") & ","
            ParamList &= GetParam(dgvOutOfHoursVisit, "OutOfHoursVisit") & ","
            ParamList &= GetParam(dgvHoursBetweenVisits, "HoursBetweenVisits") & ","
            ParamList &= GetParam(dgvAddressChanged, "AddressChanged") & ","
            ParamList &= GetParam(dgvLastVisitBailiffType, "LastVisitBailiffType") & ","
            ParamList &= cboBailiffEmp.SelectedIndex & ","

            ParamList &= chkTopClients.Checked.ToString

            ParamList &= "," & BalanceType() ' added TS 12/Feb/2015. Request ref 40282

            ParamList &= "," & cboPeriodType.SelectedValue.ToString

            PreRefresh(Me)

            RemoveSelectionHandlers()

            GetSelections()

            SummaryData.GetSummary(ParamList, radAbs.Checked)
            dgvSummary.DataSource = SummaryData.SummaryDataView

            FormatGridColumns(dgvSummary)
            HighlightWarnings()

            ListData.GetPostEnforcementList(ParamList)
            SetListGrids()
            FormatListColumns()

            SetSelections()

            AddSelectionHandlers()

            PostRefresh(Me)

            dgvSummary.ClearSelection() ' The first cell always get selected

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function GetParam(ByRef DataGrid As DataGridView, ByVal ColumnName As String) As String
        GetParam = Nothing

        Try
            ' Used when selection criteria change
            Dim Param As String = Nothing

            For Each dr As DataGridViewRow In DataGrid.SelectedRows
                If Not IsNothing(Param) Then Param += vbTab
                Param += dr.Cells(ColumnName).Value.ToString
            Next dr

            If IsNothing(Param) Then
                Param = "null"
            Else
                Param = "'" & Param.Replace("'", "''''") & "'" ' Portal task ref 16716
            End If

            Return Param

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function GetParam(ByVal ParamColumn As String, ByRef Cell As DataGridViewCell, ByRef ListDataGridView As DataGridView)
        GetParam = Nothing

        Try
            ' Used when detail for a particular cell in the summary grid is retrieved
            Dim Param As String = ""

            If IsNothing(Cell.DataGridView.Columns(ParamColumn)) Then
                ' The column may not be present as only one criteria is applicable
                If ListDataGridView.SelectedRows.Count = 1 Then
                    ' Param = "'" & ListDataGridView.SelectedRows(0).Cells(0).Value & "'"
                    Param = "'" & CStr(ListDataGridView.SelectedRows(0).Cells(0).Value).Replace("'", "''''") & "'" ' Portal task ref 16716
                Else
                    Param = "null"
                End If
            Else
                Param = "'" & CStr(Cell.DataGridView.Item(ParamColumn, Cell.RowIndex).Value).Replace("'", "''''") & "'" ' Portal task ref 16716
            End If

            Return Param

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function
#End Region

#Region "Grid selection events"

    Private Sub cboCompany_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboCompany.SelectedValueChanged
        Try
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    CType(Control, DataGridView).FirstDisplayedScrollingRowIndex = 0
                    CType(Control, DataGridView).ClearSelection()
                End If
            Next Control

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboPeriodType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboPeriodType.SelectedValueChanged
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboDisplaySet_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboDisplaySet.SelectedValueChanged
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ' This is needed to make mousewheel scroll list items
            sender.Select()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                GetSelections()
                RemoveSelectionHandlers()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                SetSelections()
                AddSelectionHandlers()
                'HighlightUnderAllocation() commented out TS 12/Sep/2016. Request ref 90026
            End If

            If e.Button = MouseButtons.Right Then
                'If sender.name <> "dgvClientName" And sender.name <> "dgvSchemeName" Then cmsList.Items(0).Visible = False Else cmsList.Items(0).Visible = True ' Commented out TS 16/Nov/2015. Request ref 63535
                'If sender.name <> "dgvClientName" Then cmsList.Items(0).Visible = False Else cmsList.Items(0).Visible = True ' Items(1) changed to Items(0) TS 16/Nov/2015. Request ref 63535. Commented out TS 12/Sep/2016. Request ref 90026
                If sender.name <> "dgvPostcodeArea" Then cmsList.Items(0).Visible = False Else cmsList.Items(0).Visible = True ' added TS 09/Feb/2015. Request ref 38930 Items(2) changed to Items(1) TS 16/Nov/2015. Request ref 63535. 1 changed to 0. Request ref 90026
                cmsList.Show(sender, e.Location)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvSummary.DataBindingComplete
        Try
            ' This needs to be handled differently. I'm not sure why but doing this later causes events to fire. Recommended practice on the following url
            ' http://msdn.microsoft.com/en-us/library/system.windows.forms.datagridviewcolumn.visible.aspx

            If dgvSummary.Columns.Contains("RowID") Then dgvSummary.Columns("RowID").Visible = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseDown
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
                dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected = True
            End If

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                ColSort = SummaryData.SummaryDataView.Sort
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseUp
        Try
            ' New section. Request ref 64648
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            RightClickColIdx = hti.ColumnIndex

            If RightClickColIdx <> -1 AndAlso DataColumnsList.Contains(dgvSummary.Columns(RightClickColIdx).Name) Then
                cmsSummary.Items(3).Visible = True
            Else
                cmsSummary.Items(3).Visible = False
            End If

            ' end of request ref 64648 section

            If e.Button = MouseButtons.Right Then cmsSummary.Show(dgvSummary, e.Location)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                If ColSort <> "" Then SummaryData.SummaryDataView.Sort += "," & ColSort
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSummary.SelectionChanged
        Try
            Dim TotalCases As Integer = 0

            If dgvSummary.SelectedCells.Count = 0 Or radAbs.Checked = False Then
                lblSummary.Text = ""
            Else
                For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                    If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then TotalCases += Cell.Value
                Next Cell
                lblSummary.Text = TotalCases.ToString & " case" & If(TotalCases > 1, "s", "")
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ShowDetail()
        Try
            Dim DetailParamList As String
            Dim Detail As New diaCaseDetail(Me)

            PreRefresh(Me)

            For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then
                    DetailParamList = cboCompany.SelectedValue.ToString & ","
                    DetailParamList &= GetParam("StageName", Cell, dgvStageName) & ","
                    DetailParamList &= GetParam("ClientName", Cell, dgvClientName) & ","
                    DetailParamList &= GetParam("SchemeName", Cell, dgvSchemeName) & ","
                    DetailParamList &= GetParam("PaymentStage", Cell, dgvPaymentStage) & ","
                    DetailParamList &= GetParam("PostcodeArea", Cell, dgvPostcodeArea) & ","
                    DetailParamList &= GetParam("CGA", Cell, dgvCGA) & ","
                    DetailParamList &= GetParam("LinkedPIF", Cell, dgvLinkedPIF) & ","
                    DetailParamList &= GetParam("ArrangementBrokenFourteenDays", Cell, dgvArrangementBrokenFourteenDays) & ","
                    DetailParamList &= GetParam("AddConfirmed", Cell, dgvAddConfirmed) & ","
                    DetailParamList &= GetParam("NumberOfVisitsAddress", Cell, dgvNumberOfVisitsAddress) & ","
                    DetailParamList &= GetParam("ResidencyScore", Cell, dgvResidencyScore) & ","
                    DetailParamList &= GetParam("DebtYear", Cell, dgvDebtYear) & ","
                    DetailParamList &= GetParam("InYear", Cell, dgvInYear) & ","
                    DetailParamList &= GetParam("LastVisitBailiffName", Cell, dgvLastVisitBailiffName) & ","
                    DetailParamList &= GetParam("OutOfHoursVisit", Cell, dgvOutOfHoursVisit) & ","
                    DetailParamList &= GetParam("HoursBetweenVisits", Cell, dgvHoursBetweenVisits) & ","
                    DetailParamList &= GetParam("AddressChanged", Cell, dgvAddressChanged) & ","
                    DetailParamList &= GetParam("LastVisitBailiffType", Cell, dgvLastVisitBailiffType) & ","
                    DetailParamList &= cboBailiffEmp.SelectedIndex & ","

                    DetailParamList &= chkTopClients.Checked.ToString

                    DetailParamList &= "," & BalanceType()  ' added TS 12/Feb/2015. Request ref 40282

                    DetailParamList &= "," & cboPeriodType.SelectedValue.ToString

                    Detail.AddCases(DetailParamList, dgvSummary.Columns(Cell.ColumnIndex).Name)

                End If
            Next Cell

            PostRefresh(Me)

            If Detail.CaseCount > 0 Then Detail.ShowDialog() ' Portal task ref 16716
            Detail.Dispose()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub


#End Region

#Region "'All' and 'Clear' button click events"

    Private Sub cmdStageAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageAll.Click
        Try
            dgvStageName.SelectAll()
            StageGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdStageClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageClear.Click
        Try
            dgvStageName.ClearSelection()
            StageGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientAll.Click
        Try
            dgvClientName.SelectAll()
            ClientGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientClear.Click
        Try
            dgvClientName.ClearSelection()
            ClientGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdSchemeNameAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeNameAll.Click
        Try
            dgvSchemeName.SelectAll()
            SchemeNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdSchemeNameClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeNameClear.Click
        Try
            dgvSchemeName.ClearSelection()
            SchemeNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCGAAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCGAAll.Click
        Try
            dgvCGA.SelectAll()
            CGAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCGAClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCGAClear.Click
        Try
            dgvCGA.ClearSelection()
            CGAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdResidencyScoreAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdResidencyScoreAll.Click
        Try
            dgvResidencyScore.SelectAll()
            ResidencyScoreGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdResidencyScoreClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdResidencyScoreClear.Click
        Try
            dgvResidencyScore.ClearSelection()
            ResidencyScoreGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPaymentStageAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPaymentStageAll.Click
        Try
            dgvPaymentStage.SelectAll()
            PaymentStageGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPaymentStageClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPaymentStageClear.Click
        Try
            dgvPaymentStage.ClearSelection()
            PaymentStageGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdNumberOfVisitsAddressAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNumberOfVisitsAddressAll.Click
        Try
            dgvNumberOfVisitsAddress.SelectAll()
            NumberOfVisitsAddressGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdNumberOfVisitsAddressClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNumberOfVisitsAddressClear.Click
        Try
            dgvNumberOfVisitsAddress.ClearSelection()
            NumberOfVisitsAddressGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAddConfirmedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfirmedAll.Click
        Try
            dgvAddConfirmed.SelectAll()
            AddConfirmedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAddConfirmedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfirmedClear.Click
        Try
            dgvAddConfirmed.ClearSelection()
            AddConfirmedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeAreaAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaAll.Click
        Try
            dgvPostcodeArea.SelectAll()
            PostcodeAreaGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeAreaClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaClear.Click
        Try
            dgvPostcodeArea.ClearSelection()
            PostcodeAreaGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdArrangementBrokenFourteenDaysAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdArrangementBrokenFourteenDaysAll.Click
        Try
            dgvArrangementBrokenFourteenDays.SelectAll()
            ArrangementBrokenFourteenDaysGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdArrangementBrokenFourteenDaysClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdArrangementBrokenFourteenDaysClear.Click
        Try
            dgvArrangementBrokenFourteenDays.ClearSelection()
            ArrangementBrokenFourteenDaysGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLinkedPIFAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLinkedPIFAll.Click
        Try
            dgvLinkedPIF.SelectAll()
            LinkedPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLinkedPIFClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLinkedPIFClear.Click
        Try
            dgvLinkedPIF.ClearSelection()
            LinkedPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdDebtYearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDebtYearAll.Click
        Try
            dgvDebtYear.SelectAll()
            DebtYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdDebtYearClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDebtYearClear.Click
        Try
            dgvDebtYear.ClearSelection()
            DebtYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdInYearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdInYearAll.Click
        Try
            dgvInYear.SelectAll()
            InYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdInYearClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdInYearClear.Click
        Try
            dgvInYear.ClearSelection()
            InYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLastVisitBailiffNameAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLastVisitBailiffNameAll.Click
        Try
            dgvLastVisitBailiffName.SelectAll()
            LastVisitBailiffNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLastVisitBailiffNameClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLastVisitBailiffNameClear.Click
        Try
            dgvLastVisitBailiffName.ClearSelection()
            LastVisitBailiffNameGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdOutOfOursVisitAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOutOfHoursVisitAll.Click
        Try
            dgvOutOfHoursVisit.SelectAll()
            OutOfHoursVisitGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdOutOfHoursVisitClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdOutOfHoursVisitClear.Click
        Try
            dgvOutOfHoursVisit.ClearSelection()
            OutOfHoursVisitGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdHoursBetweenVisitsAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdHoursBetweenVisitsAll.Click
        Try
            dgvHoursBetweenVisits.SelectAll()
            HoursBetweenVisitsGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdHoursBetweenVisitsClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdHoursBetweenVisitsClear.Click
        Try
            dgvHoursBetweenVisits.ClearSelection()
            HoursBetweenVisitsGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAddressChangedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddressChangedAll.Click
        Try
            dgvAddressChanged.SelectAll()
            AddressChangedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAddressChangedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddressChangedClear.Click
        Try
            dgvAddressChanged.ClearSelection()
            AddressChangedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLastVisitBailiffTypeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLastVisitBailiffTypeAll.Click
        Try
            dgvLastVisitBailiffType.SelectAll()
            LastVisitBailiffTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLastVisitBailiffTypeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLastVisitBailiffTypeClear.Click
        Try
            dgvLastVisitBailiffType.ClearSelection()
            LastVisitBailiffTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "Handlers"

    Private Sub RemoveSelectionHandlers()
        Try
            ' Need to find a way to do these at once but these SelectionChanged event is specific to the datagridview class and not control - the collection on the form
            RemoveHandler dgvStageName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvCGA.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvResidencyScore.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPaymentStage.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvNumberOfVisitsAddress.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvAddConfirmed.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvArrangementBrokenFourteenDays.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvLinkedPIF.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvDebtYear.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvInYear.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvLastVisitBailiffName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvOutOfHoursVisit.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvHoursBetweenVisits.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvAddressChanged.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvLastVisitBailiffType.SelectionChanged, AddressOf dgvSelectionChanged

            RemoveHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            RemoveHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
            RemoveHandler cboBailiffEmp.SelectedValueChanged, AddressOf cboBailiffEmp_SelectedValueChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddSelectionHandlers()
        Try
            AddHandler dgvStageName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvCGA.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvResidencyScore.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPaymentStage.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvNumberOfVisitsAddress.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvAddConfirmed.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvArrangementBrokenFourteenDays.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvLinkedPIF.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvDebtYear.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvInYear.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvLastVisitBailiffName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvOutOfHoursVisit.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvHoursBetweenVisits.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvAddressChanged.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvLastVisitBailiffType.SelectionChanged, AddressOf dgvSelectionChanged

            AddHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            AddHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
            AddHandler cboBailiffEmp.SelectedValueChanged, AddressOf cboBailiffEmp_SelectedValueChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddControlHandlers()
        Try
            ' All these grids and buttons call the same events so no point declaring them all separately
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    AddHandler Control.MouseDown, AddressOf dgvMouseDown
                    AddHandler Control.MouseUp, AddressOf dgvMouseUp
                    AddHandler Control.MouseEnter, AddressOf dgvMouseEnter
                    'AddHandler Control.MouseHover, AddressOf dgvMouseHover
                ElseIf TypeOf Control Is ComboBox Or TypeOf Control Is Button Or TypeOf Control Is RadioButton Or TypeOf Control Is CheckBox Then
                    'AddHandler Control.MouseHover, AddressOf dgvMouseHover
                End If
            Next Control

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "DataGridView methods"

    Private Sub SetListGrids()
        Try
            dgvStageName.DataSource = ListData.StageName
            StageGridState.SetSort()

            dgvClientName.DataSource = ListData.ClientName
            ClientGridState.SetSort()

            dgvSchemeName.DataSource = ListData.SchemeName
            SchemeNameGridState.SetSort()

            dgvCGA.DataSource = ListData.CGA
            CGAGridState.SetSort()

            dgvResidencyScore.DataSource = ListData.ResidencyScore
            ResidencyScoreGridState.SetSort()

            dgvPaymentStage.DataSource = ListData.PaymentStage
            PaymentStageGridState.SetSort()

            dgvNumberOfVisitsAddress.DataSource = ListData.NumberOfVisitsAddress
            NumberOfVisitsAddressGridState.SetSort()

            dgvAddConfirmed.DataSource = ListData.AddConfirmed
            AddConfirmedGridState.SetSort()

            dgvPostcodeArea.DataSource = ListData.PostcodeArea
            PostcodeAreaGridState.SetSort()

            dgvArrangementBrokenFourteenDays.DataSource = ListData.ArrangementBrokenFourteenDays
            ArrangementBrokenFourteenDaysGridState.SetSort()

            dgvLinkedPIF.DataSource = ListData.LinkedPIF
            LinkedPIFGridState.SetSort()

            dgvDebtYear.DataSource = ListData.DebtYear
            DebtYearGridState.SetSort()

            dgvInYear.DataSource = ListData.InYear
            InYearGridState.SetSort()

            dgvLastVisitBailiffName.DataSource = ListData.LastVisitBailiffName
            LastVisitBailiffNameGridState.SetSort()

            dgvOutOfHoursVisit.DataSource = ListData.OutOfHoursVisit
            OutOfHoursVisitGridState.SetSort()

            dgvHoursBetweenVisits.DataSource = ListData.HoursBetweenVisits
            HoursBetweenVisitsGridState.SetSort()

            dgvAddressChanged.DataSource = ListData.AddressChanged
            AddressChangedGridState.SetSort()

            dgvLastVisitBailiffType.DataSource = ListData.LastVisitBailiffType
            LastVisitBailiffTypeGridState.SetSort()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub FormatListColumns()
        Try
            ' This is slightly different to settings in modCommon as we want some of the fields wider than they need to be for presentation
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    If Control.Name <> "dgvSummary" Then ' This is not a list grid
                        For Each Column In CType(Control, DataGridView).Columns
                            Select Case Column.Name
                                Case "Total"
                                    Column.Width = 40
                                Case "StageName"
                                    Column.Width = 88
                                    Column.HeaderText = "Stage"
                                Case "ClientName"
                                    Column.Width = 100
                                    Column.HeaderText = "Client"
                                Case "WorkType"
                                    Column.Width = 88
                                    Column.HeaderText = "Work Type"
                                Case "SchemeName"
                                    Column.Width = 100
                                    Column.HeaderText = "Scheme"
                                Case "LinkedPIF"
                                    Column.Width = 70
                                    Column.HeaderText = "Linked PIF"
                                Case "PhoneNumber"
                                    Column.Width = 70
                                Case "CGA"
                                    Column.Width = 70
                                Case "ResidencyScore"
                                    Column.Width = 70
                                    Column.HeaderText = "Res score"
                                Case "PaymentStage"
                                    Column.Width = 70
                                Case "NumberOfVisitsAddress"
                                    Column.Width = 63
                                Case "AddConfirmed"
                                    Column.Width = 76
                                    Column.HeaderText = "Confirmed"
                                Case "ArrangementBrokenFourteenDays"
                                    Column.Width = 70
                                    Column.HeaderText = "Broken"
                                Case "PostcodeArea"
                                    Column.Width = 88
                                    Column.HeaderText = "Area"
                                Case "DebtYear"
                                    Column.Width = 55
                                    Column.HeaderText = "Year"
                                Case "InYear"
                                    Column.Width = 70
                                Case "LinkedPayment"
                                    Column.Width = 70
                                    Column.HeaderText = "Linked Pay"
                                Case "LastVisitBailiffName"
                                    Column.width = 88
                                    Column.HeaderText = "Name"
                                Case "OutOfHoursVisit"
                                    Column.Width = 70
                                    Column.HeaderText = "OOH"
                                Case "HoursBetweenVisits"
                                    Column.width = 70
                                    Column.headertext = "Hours"
                                Case "AddressChanged"
                                    Column.Width = 76
                                Case "LastVisitBailiffType"
                                    Column.Width = 70
                                    Column.HeaderText = "EA Type"
                                Case Else
                                    Column.Width = 40
                            End Select
                        Next Column
                    End If
                End If
            Next Control

            'SummaryData.GetUnderAllocatedClients() commented out TS 12/Sep/2016. Request ref 90026

            'HighlightUnderAllocation() commented out TS 12/Sep/2016. Request ref 90026

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    'Private Sub HighlightUnderAllocation() commented out TS 12/Sep/2016. Request ref 90026
    '    Try
    '        For Each Row As DataGridViewRow In dgvClientName.Rows
    '            SummaryData.UnderAllocatedClientDataView.RowFilter = "ClientName = '" & Row.Cells("ClientName").Value & "'"
    '            If SummaryData.UnderAllocatedClientDataView.Count > 0 Then
    '                Row.DefaultCellStyle.ForeColor = Color.Red
    '            Else
    '                Row.DefaultCellStyle.ForeColor = Color.Black
    '            End If
    '        Next Row

    '    Catch ex As Exception
    '        HandleException(ex)
    '    End Try
    'End Sub

    Private Sub HighlightWarnings()
        Try
            Dim RowIndex As Integer

            For Each DataRow As DataRow In SummaryData.WarningSummaryDataView.ToTable.Rows
                ' Find the row with warnings on the datagridview
                For Each GridRow As DataGridViewRow In dgvSummary.Rows
                    If GridRow.Cells("RowID").Value = DataRow.Item("RowID") Then RowIndex = GridRow.Index
                Next GridRow

                ' Loop through periods and highlight if any warnings exist
                For Each Item As DataColumn In DataRow.Table.Columns
                    If DataColumnsList.Contains(Item.ColumnName) AndAlso DataRow.Item(Item.ColumnName) > 0 Then
                        dgvSummary.Rows(RowIndex).Cells(Item.ColumnName).Style.ForeColor = Color.Red
                    End If
                Next Item
            Next DataRow

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub GetSelections()
        Try
            StageGridState.GetState()
            ClientGridState.GetState()
            SchemeNameGridState.GetState()
            CGAGridState.GetState()
            ResidencyScoreGridState.GetState()
            PaymentStageGridState.GetState()
            NumberOfVisitsAddressGridState.GetState()
            AddConfirmedGridState.GetState()
            PostcodeAreaGridState.GetState()
            ArrangementBrokenFourteenDaysGridState.GetState()
            LinkedPIFGridState.GetState()
            DebtYearGridState.GetState()
            InYearGridState.GetState()
            LastVisitBailiffNameGridState.GetState()
            OutOfHoursVisitGridState.GetState()
            HoursBetweenVisitsGridState.GetState()
            AddressChangedGridState.GetState()
            LastVisitBailiffTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetSelections()
        Try
            StageGridState.SetState()
            ClientGridState.SetState()
            SchemeNameGridState.SetState()
            CGAGridState.SetState()
            ResidencyScoreGridState.SetState()
            PaymentStageGridState.SetState()
            NumberOfVisitsAddressGridState.SetState()
            AddConfirmedGridState.SetState()
            PostcodeAreaGridState.SetState()
            ArrangementBrokenFourteenDaysGridState.SetState()
            LinkedPIFGridState.SetState()
            DebtYearGridState.SetState()
            InYearGridState.SetState()
            LastVisitBailiffNameGridState.SetState()
            OutOfHoursVisitGridState.SetState()
            HoursBetweenVisitsGridState.SetState()
            AddressChangedGridState.SetState()
            LastVisitBailiffTypeGridState.SetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Additional functionality"

    Private Sub cmdClearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClearAll.Click
        Try
            RemoveSelectionHandlers()

            cmdStageClear_Click(sender, New System.EventArgs)
            cmdClientClear_Click(sender, New System.EventArgs)
            cmdSchemeNameClear_Click(sender, New System.EventArgs)
            cmdCGAClear_Click(sender, New System.EventArgs)
            cmdResidencyScoreClear_Click(sender, New System.EventArgs)
            cmdPaymentStageClear_Click(sender, New System.EventArgs)
            cmdNumberOfVisitsAddressClear_Click(sender, New System.EventArgs)
            cmdAddConfirmedClear_Click(sender, New System.EventArgs)
            cmdPostcodeAreaClear_Click(sender, New System.EventArgs)
            cmdArrangementBrokenFourteenDaysClear_Click(sender, New System.EventArgs)
            cmdLinkedPIFClear_Click(sender, New System.EventArgs)
            cmdDebtYearClear_Click(sender, New System.EventArgs)
            cmdInYearClear_Click(sender, New System.EventArgs)
            cmdLastVisitBailiffNameClear_Click(sender, New System.EventArgs)
            cmdOutOfHoursVisitClear_Click(sender, New System.EventArgs)
            cmdHoursBetweenVisitsClear_Click(sender, New System.EventArgs)
            cmdAddressChangedClear_Click(sender, New System.EventArgs)
            cmdLastVisitBailiffTypeClear_Click(sender, New System.EventArgs)

            AddSelectionHandlers()

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radAbs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles radAbs.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radPerc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPerc.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub chkTopClients_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles chkTopClients.CheckedChanged
        Try
            RefreshScroll = False

            If chkTopClients.Checked Then
                ' Not ideal but we need to refresh the client list before the main refresh to exclude any clients selected that would not be in the top list
                RemoveSelectionHandlers()

                GetSelections()
                ListData.GetClientList(ParamList)
                SetSelections()

                AddSelectionHandlers()
            End If

            RefreshGrid()

            RefreshScroll = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.Click
        Try
            If MsgBox("This could take several minutes, are you sure?", vbYesNo + vbDefaultButton2 + vbQuestion) = vbYes Then
                'RefreshDBClicked = True
                PreRefresh(Me)
                SummaryData.RefreshDatabase()
                RefreshGrid()
                PostRefresh(Me)
                MsgBox("Refresh complete.", vbOKOnly + vbInformation)
                SetcmdRefreshTip()
                'RefreshDBClicked = False
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.MouseLeave
        Try
            ' on XP machines there is a bug that prevents the tooltip showing a second time. This addresses the problem.
            ToolTip1.Active = False
            ToolTip1.Active = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetcmdRefreshTip()
        Try
            SummaryData.GetLastLoad()
            ToolTip1.SetToolTip(cmdRefreshDB, "Last refreshed at " & SummaryData.LastLoadDataView.Item(0).Item(0).ToString & " by " & SummaryData.LastLoadDataView.Item(0).Item(1).ToString)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsSummary_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsSummary.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "View Cases"
                    ShowDetail()
                Case "Copy"
                    dgvSummary.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    If Not dgvSummary.GetClipboardContent Is Nothing Then Clipboard.SetDataObject(dgvSummary.GetClipboardContent())
                Case "Select All"
                    dgvSummary.SelectAll()
                Case "Select Column" ' Request ref 64648

                    RemoveHandler dgvSummary.SelectionChanged, AddressOf dgvSummary_SelectionChanged ' Remove the handler to avoid refreshing the lists on the first select and recalculating the total for every select

                    If Not My.Computer.Keyboard.CtrlKeyDown Then dgvSummary.ClearSelection()

                    For Each DataRow As DataGridViewRow In dgvSummary.Rows
                        If DataRow.Index = dgvSummary.Rows.Count - 1 Then ' Before selecting the last cell reinstatate the handler so that the total is recalculated
                            AddHandler dgvSummary.SelectionChanged, AddressOf dgvSummary_SelectionChanged
                        End If

                        DataRow.Cells(RightClickColIdx).Selected = True
                    Next DataRow

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsList_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsList.ItemClicked
        Try
            ' Slightly more complicated than the summary grid as this event fires for all other datagridviews.
            Select Case e.ClickedItem.Text
                ' the following commented out TS 12/Sep/2016
                'Case "Set warning levels"
                '    Dim dia As New diaClientAllocationLevels(SummaryData.ClientAllocationLevelDataView)
                '    dia.ShowDialog()
                '    SummaryData.GetClientAllocationLevels()
                '    If dia.DialogResult = DialogResult.OK Then FormatListColumns()
                '    dia.Dispose()

                Case "Copy"
                    sender.SourceControl.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    If Not sender.SourceControl.GetClipboardContent() Is Nothing Then Clipboard.SetDataObject(sender.SourceControl.GetClipboardContent())

                Case "Select All"
                    sender.SourceControl.SelectAll()

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsForm_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsForm.ItemClicked
        Try
            ' The dispose is required to clear any forms that have called others. Dispose show changed from ShowDialog request ref 75670
            Select Case e.ClickedItem.Text
                Case "Allocation View"
                    If FormOpen("frmCaseSummary") Then
                        frmCaseSummary.Activate()
                    Else
                        frmCaseSummary.Dispose()
                        frmCaseSummary.Show()
                    End If
                Case "Bailiff View"
                    If FormOpen("frmBailiffSummary") Then
                        frmBailiffSummary.Activate()
                    Else
                        frmBailiffSummary.Dispose()
                        frmBailiffSummary.Show()
                    End If
                Case "EA Update View"
                    If FormOpen("frmEAUpdateSummary") Then
                        frmEAUpdateSummary.Activate()
                    Else
                        frmEAUpdateSummary.Dispose()
                        frmEAUpdateSummary.Show()
                    End If
                Case "Regional View"
                    If FormOpen("frmAllocationByRegionSummary") Then
                        frmAllocationByRegionSummary.Activate()
                    Else
                        frmAllocationByRegionSummary.Dispose()
                        frmAllocationByRegionSummary.Show()
                    End If
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    'Private Sub ConsortiumContextSubMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim ScrollbarSet As Boolean = False

    '        RemoveSelectionHandlers()

    '        dgvClientName.ClearSelection()
    '        dgvSchemeName.ClearSelection()

    '        ListData.ConsortiumDataView.RowFilter = "Consortium = '" & CType(sender, ToolStripItem).Text & "'"

    '        For Each ConsortiumDataRow As DataRow In ListData.ConsortiumDataView.ToTable.Rows
    '            For Each GridDataRow As DataGridViewRow In dgvClientName.Rows
    '                If GridDataRow.Cells("ClientName").Value = ConsortiumDataRow.Item("ClientName") Then
    '                    GridDataRow.Selected = True
    '                    If Not ScrollbarSet Then
    '                        dgvClientName.FirstDisplayedScrollingRowIndex = GridDataRow.Index
    '                        ScrollbarSet = True
    '                    End If
    '                End If
    '            Next GridDataRow

    '            ScrollbarSet = False

    '            For Each GridDataRow As DataGridViewRow In dgvSchemeName.Rows
    '                If GridDataRow.Cells("SchemeName").Value = ConsortiumDataRow.Item("SchemeName") Then
    '                    GridDataRow.Selected = True
    '                    If Not ScrollbarSet Then
    '                        dgvSchemeName.FirstDisplayedScrollingRowIndex = GridDataRow.Index
    '                        ScrollbarSet = True
    '                    End If
    '                End If
    '            Next GridDataRow
    '        Next ConsortiumDataRow

    '        AddSelectionHandlers()
    '        RefreshGrid()

    '    Catch ex As Exception
    '        HandleException(ex)
    '    End Try
    'End Sub

    Private Sub RegionContextSubMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' added TS 09/Feb/2015. Request ref 38930
        Try
            Dim ScrollbarSet As Boolean = False

            RemoveSelectionHandlers()

            ListData.PostcodeAreaRegionDataView.RowFilter = "RegionDesc = '" & CType(sender, ToolStripItem).Text & "'"

            For Each PostcodeAreaRegionDataRow As DataRow In ListData.PostcodeAreaRegionDataView.ToTable.Rows
                For Each GridDataRow As DataGridViewRow In dgvPostcodeArea.Rows
                    If GridDataRow.Cells("PostcodeArea").Value = PostcodeAreaRegionDataRow.Item("PostcodeArea") Then
                        GridDataRow.Selected = True
                        If Not ScrollbarSet Then
                            dgvPostcodeArea.FirstDisplayedScrollingRowIndex = GridDataRow.Index
                            ScrollbarSet = True
                        End If
                    End If
                Next GridDataRow
            Next PostcodeAreaRegionDataRow

            AddSelectionHandlers()
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radLow_Click(sender As Object, e As System.EventArgs) Handles radLow.Click
        Try
            If radLow.Checked Then
                radLow.Checked = False
            Else
                radLow.Checked = True
                radHigh.Checked = False
            End If

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radHigh_Click(sender As Object, e As System.EventArgs) Handles radHigh.Click
        Try
            If radHigh.Checked Then
                radHigh.Checked = False
            Else
                radHigh.Checked = True
                radLow.Checked = False
            End If

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function BalanceType() As String
        BalanceType = "A"

        Try
            If radLow.Checked Then
                BalanceType = "L"
            ElseIf radHigh.Checked Then
                BalanceType = "H"
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    'Private Function GetStages() As String()
    '    GetStages = Nothing

    '    Try
    '        Dim SelectedSorts As New List(Of String)


    '        For Each Item As DataGridViewRow In dgvStageName.SelectedRows
    '            SelectedSorts.Add(Item.Cells("StageName").Value.ToString)
    '        Next Item

    '        If Not IsNothing(SelectedSorts) Then GetStages = SelectedSorts.ToArray

    '    Catch ex As Exception
    '        HandleException(ex)
    '    End Try
    'End Function

    Private Sub cboBailiffEmp_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboBailiffEmp.SelectedValueChanged
        Try
            RefreshScroll = False

            ' Not ideal but we need to refresh the bailiff list before the main refresh to exclude any bailiffs selected that would not be in the top list
            RemoveSelectionHandlers()

            cmdLastVisitBailiffNameClear_Click(sender, New System.EventArgs) ' Added TS 15/Apr/2016. Request ref 79559

            GetSelections()
            ListData.GetLastVisitBailiffNameList(ParamList)
            SetSelections()

            AddSelectionHandlers()

            RefreshGrid()

            RefreshScroll = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region



    'Private Sub dgvMouseHover(sender As Object, e As System.EventArgs)

    '    If Not RefreshDBClicked Then Return

    '    If Not ListData.IsRefreshRunning Then Return

    '    If Not RefreshRunningMsgShown Then

    '        Me.Enabled = False

    '        MsgBox("Refresh running.", vbOKOnly + vbInformation)
    '        RefreshRunningMsgShown = True
    '        RefreshCheckTimer.Enabled = True
    '    End If
    'End Sub

    'Private Sub RefreshCheckTimer_Tick(sender As Object, e As System.EventArgs) Handles RefreshCheckTimer.Tick

    '    If ListData.IsRefreshRunning Then Return

    '    MsgBox("Refresh complete.", vbOKOnly + vbInformation)
    '    RefreshRunningMsgShown = False
    '    RefreshCheckTimer.Enabled = False
    '    Me.Enabled = True
    'End Sub

End Class



﻿Imports CommonLibrary
Imports System.IO
Imports System.Configuration

Public Class frmMain
    Private InputFilePath As String, FileName As String, FileExt As String


    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            ConnectDb2("DebtRecovery")
            Dim prod_run As Boolean = False
            Dim env_str As String = ""
            Try
                env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
            Catch ex As Exception
                prod_run = False
            End Try
            If env_str = "Prod" Then prod_run = True

            Dim filedialog As New OpenFileDialog

            If Not filedialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(filedialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(filedialog.FileName)
            FileExt = Path.GetExtension(filedialog.FileName)
           

            Dim errorNo As Integer = 0
            Dim openNo, closedNo As Integer

            Dim AuditLog As String, ErrorLog As String = ""
            Dim OpenOutFile As String = ""
            Dim closedOutFile As String = ""
            lblReadingFile.Visible = True
            'read excel file - ZERO based
            Dim FileContents() As String = System.IO.File.ReadAllLines(filedialog.FileName)


            ProgressBar.Maximum = UBound(FileContents)
            Dim inputLineArray() As String
            Dim LineNumber As Integer = 0
            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                Application.DoEvents()
                LineNumber += 1
                inputLineArray = InputLine.Split(",")
                Dim amountstr As String = Trim(inputLineArray(7))
                Dim amount As Decimal
                Try
                    amount = amountstr
                Catch ex As Exception
                    Continue For
                End Try
                Dim clientRef As String = inputLineArray(1)
                Dim accountID As String = ""
                Try
                    accountID = inputLineArray(0)
                Catch ex As Exception
                    Continue For
                End Try
                If accountID.Length < 5 Then
                    errorNo += 1
                    Continue For
                End If
                Dim changedate As Date
                Try
                    changedate = inputLineArray(8)
                Catch ex As Exception
                    changedate = Nothing
                End Try
                'get debtorID from onestep
                Dim dt As New DataTable
                LoadDataTable2("DebtRecovery", "SELECT D._rowid, status_open_closed  " & _
                                                        "FROM debtor D, clientscheme CS  " & _
                                                        "WHERE client_ref = '" & clientRef & "'" & _
                                                        " and D.clientschemeID = CS._rowID" & _
                                                        " and offenceLocation = '" & accountID & "'" & _
                                                        " and CS.branchID in (23,25,26,27) ", dt, False)
                Dim debtorFound As Boolean = False
                For Each debtRow In dt.Rows
                    Dim debtorID As Integer = debtRow(0)

                    Dim openClosed As String = debtRow(1)
                    If openClosed = "O" Then
                        OpenOutFile &= debtorID & "|" & amount & "|"
                        If changedate = Nothing Then
                            OpenOutFile &= vbNewLine
                        Else
                            OpenOutFile &= Format(changedate, "yyyy-MM-dd") & vbNewLine
                        End If
                        openNo += 1
                    Else
                        closedOutFile &= debtorID & "|" & amount & "|"
                        If changedate = Nothing Then
                            closedOutFile &= vbNewLine
                        Else
                            closedOutFile &= Format(changedate, "yyyy-MM-dd") & vbNewLine
                        End If
                        closedNo += 1
                    End If
                    debtorFound = True
                    Exit For
                Next
                If debtorFound = False Then
                    errorNo += 1
                End If
            Next
          
            lblReadingFile.Visible = False
            ' WriteFile(InputFilePath & FileName & "_PreProcessed.txt", OutputFile)

            AuditLog = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Date pre-processed: " & DateTime.Now.ToString & vbCrLf
            AuditLog &= "By: " & Environment.UserName & vbCrLf & vbCrLf

            AuditLog &= "Open   Amount changes: " & openNo & vbNewLine
            AuditLog &= "Closed Amount changes: " & closedNo & vbNewLine
            AuditLog &= "Errors: " & errorNo & vbNewLine
            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            If openNo > 0 Then WriteFile(InputFilePath & FileName & "_OpenChanges.txt", OpenOutFile)
            If closedNo > 0 Then WriteFile(InputFilePath & FileName & "_ClosedChanges.txt", closedOutFile)
            If ErrorLog <> "" Then WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)

            MsgBox("Pre-processing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            ' btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", """" & InputFilePath & FileName & "_PreProcessed.txt """"")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try

            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class

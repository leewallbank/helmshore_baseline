﻿Imports CommonLibrary
Imports System.IO
Imports System.Text.RegularExpressions

Public Class frmMain

    Private InputFilePath As String, FileName As String, FileExt As String

    Private Const Separator As String = "|"


    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        btnViewInputFile.Enabled = False
        btnViewLogFile.Enabled = False
        btnViewOutputFile.Enabled = False

    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog

            Dim NewDebtNotes As String = "", NewDebtLine(28) As String
            Dim AuditLog As String
            Dim FileContents(,) As String, ErrorLog As String = ""
            Dim NewDebtFile As String = ""
            Dim NewDebtSumm(1) As Decimal ' 0 Case count, 1 Total balance
            Dim TotalIntChargesColPos As Integer = -1

            btnViewInputFile.Enabled = False
            btnViewLogFile.Enabled = False
            btnViewOutputFile.Enabled = False

            FileDialog.Filter = "New business files|*.xls;*.xlsx|All files (*.*)|*.*"
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim LogHeader As String = "File pre-processed: " & InputFilePath & FileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf
            Dim OutputFileName As String = InputFilePath & FileName & "_Preprocessed.txt"

            If File.Exists(OutputFileName) Then File.Delete(OutputFileName)
            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then File.Delete(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then File.Delete(InputFilePath & FileName & "_Error.txt")

            lblReadingFile.Visible = True
            Application.DoEvents()
            FileContents = InputFromExcel(FileDialog.FileName)

            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents) + 2  ' plus audit file and output

            For RowNum = 1 To UBound(FileContents)

                Application.DoEvents() ' without this line, the button disappears until processing is complete
                ProgressBar.Value = RowNum
                NewDebtNotes = ""

                If String.IsNullOrEmpty(FileContents(RowNum, 0)) Then Continue For

                ' Strip out any separators in the data
                For ColNum As Integer = 0 To UBound(FileContents, 2)
                    FileContents(RowNum, ColNum) = FileContents(RowNum, ColNum).Replace(Separator, "")
                Next ColNum

                ' Read columns
                For ColNum As Integer = 0 To 28
                    Select Case ColNum
                        Case 18
                            ' Concatenate address fields
                            NewDebtLine(19) = ConcatFields({ConcatFields({FileContents(RowNum, 18), FileContents(RowNum, 19)}, " "), FileContents(RowNum, 20), FileContents(RowNum, 21), FileContents(RowNum, 22), FileContents(RowNum, 23)}, ",")

                        Case 19 To 23
                            ' leave these blank - covered in 18

                        Case Else
                            NewDebtLine(ColNum) = FileContents(RowNum, ColNum)
                    End Select
                Next ColNum

                ' Now build up notes
                NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 4), "Warrant Produced", ";"))
                NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 9), "Vehicle Type", ";"))
                NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 11), "Zone Name", ";"))
                NewDebtNotes &= String.Join("", ToNote(FileContents(RowNum, 13), "Offence Description", ";"))

                NewDebtFile = Join(NewDebtLine, Separator) & Separator & NewDebtNotes & vbCrLf

                AppendToFile(OutputFileName, NewDebtFile)

                NewDebtSumm(0) += 1
                NewDebtSumm(1) += CDec(FileContents(RowNum, 24))

            Next RowNum

            ' Update audit log
            AuditLog = LogHeader & "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Number of new cases: " & NewDebtSumm(0).ToString & vbCrLf
            AuditLog &= "Total balance of new cases: " & NewDebtSumm(1).ToString & vbCrLf

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)

            ProgressBar.Value += 1

            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", LogHeader & ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            ProgressBar.Value = ProgressBar.Maximum

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try
            If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("excel.exe", """" & InputFilePath & FileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try
            If File.Exists(InputFilePath & FileName & "_Preprocessed.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Preprocessed.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try
            If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
            If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
            If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function InputFromExcel(ByVal FileName As String) As String(,)

        InputFromExcel = Nothing

        Try

            Dim TempTable As New DataTable
            ' HDR=NO to not skip the first line
            Dim xlsConn As System.Data.OleDb.OleDbConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FileName & ";Extended Properties=""Excel 12.0 Xml;IMEX=1;HDR=NO;TypeGuessRows = 0;ImportMixedTypes = Text;""")
            xlsConn.Open()
            Dim xlsComm As System.Data.OleDb.OleDbDataAdapter = New System.Data.OleDb.OleDbDataAdapter("select * from [" & xlsConn.GetSchema("Tables").Rows(0)("TABLE_NAME") & "]", xlsConn)

            xlsComm.Fill(TempTable)
            xlsConn.Close()

            Dim Output As String(,)
            ReDim Output(TempTable.Rows.Count - 1, TempTable.Columns.Count - 1)

            For RowIdx As Integer = 0 To TempTable.Rows.Count - 1
                For ColIdx As Integer = 0 To TempTable.Columns.Count - 1
                    Application.DoEvents()
                    Output(RowIdx, ColIdx) = TempTable.Rows(RowIdx).Item(ColIdx).ToString
                Next ColIdx
            Next RowIdx

            Return Output

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

End Class


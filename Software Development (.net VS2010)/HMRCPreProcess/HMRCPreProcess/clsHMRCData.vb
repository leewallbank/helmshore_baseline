﻿Imports CommonLibrary

Public Class clsHMRCData
    Private ErrorCode As String() = {"110", "111"}
    Private ClientID As String = 1275
    Private DTCaseBatch As DataTable = New DataTable
    Private DTTranche As DataTable = New DataTable
    Private DTHODDebtLevel As DataTable = New DataTable

    Public ReadOnly Property CaseBatch As DataTable
        Get
            CaseBatch = DTCaseBatch
        End Get
    End Property

    Public ReadOnly Property Tranche As DataTable
        Get
            Tranche = DTTranche
        End Get
    End Property

    Public ReadOnly Property HODDebtLevel As DataTable
        Get
            HODDebtLevel = DTHODDebtLevel
        End Get
    End Property
    Public Function AddHMRCCaseBatchDetail(TrancheID As String, RecordSequence As Integer, ClientRef As String, RowError As String()) As Boolean

        AddHMRCCaseBatchDetail = False

        Try
            Dim SQL As String = "INSERT INTO dbo.HMRCCaseBatchDetail (TrancheID, RecordSequence, CreatedDate, CreatedBy, ClientRef, RejectRecord, ErrorCode, ErrorDesc) " & _
                   "VALUES (" & TrancheID & ", " & RecordSequence.ToString & ", GETDATE(), SYSTEM_USER, '" & ClientRef & "'"

            If Not RowError Is Nothing Then
                SQL &= ", 'Y'"
            Else
                SQL &= ", 'N'"
            End If

            If Not RowError Is Nothing AndAlso CInt(RowError(0)) >= 0 Then
                SQL &= ", '" & ErrorCode(CInt(RowError(0))) & "', '" & RowError(1) & "')"
            Else
                SQL &= ",NULL, NULL)"
            End If

            ExecStoredProc("FeesSQL", SQL)

            AddHMRCCaseBatchDetail = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Function UpdateCaseBatchDetail(ByVal TrancheID As String, RecordSequence As String) As Boolean

        UpdateCaseBatchDetail = False

        Try
            Dim Results As Object() = GetSQLResultsArray("DebtRecovery", "SELECT d._rowID, d.debt_original " & _
                                                         "FROM debtor AS d " & _
                                                         "INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID " & _
                                                         "WHERE d.prevReference = '" & RecordSequence & "' " & _
                                                         "  AND d.status_open_closed = 'O' " & _
                                                         "  AND d.status = 'L' " & _
                                                         "  AND d.client_batch = '" & TrancheID & "'" & _
                                                         "  AND cs.clientID = " & ClientID)

            ' The following code was used for the Tranche 1049 issue
            'Dim Results As Object() = GetSQLResultsArray("DebtRecovery", "SELECT d._rowID, d.debt_original " & _
            '                                 "FROM debtor AS d " & _
            '                                 "INNER JOIN ClientScheme AS cs ON d.ClientSchemeID = cs._rowID " & _
            '                                 "WHERE d.prevReference = '" & RecordSequence & "' " & _
            '                                 "  AND d.client_batch = '" & TrancheID & "'" & _
            '                                 "  AND cs.clientID = " & ClientID)

            If Results.Length > 0 Then
                UpdateCaseBatchDetail = True
                ExecStoredProc("FeesSQL", "UPDATE dbo.HMRCCaseBatchDetail SET DebtorID = " & Results(0).ToString & " WHERE TrancheID = " & TrancheID & " AND RecordSequence = '" & RecordSequence & "'")
                frmMain.TotalLoadedCasesCount += 1
                frmMain.TotalLoadedCasesValue += CDec(Results(1))
            Else
                UpdateCaseBatchDetail = False
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Function AddNewBatch(TrancheID As String, AccountsLoaded As Integer, AccountsRejected As Integer, HeaderValidation As String, TrailerValidation As String, ColumnsValidation As String, RejectPercentage As Double, FileType As String, AccountsRejectedValue As Decimal, HMRCFinancialYear As Integer, BatchRejected As String) As Boolean

        AddNewBatch = False

        Try
            Dim SQL As String = "INSERT INTO dbo.HMRCCaseBatch ( TrancheID, CreatedDate, CreatedBy, AccountsLoaded, AccountsRejected, HeaderValidation, TrailerValidation" & _
                                                               ", ColumnsValidation, RejectPercentage, FileType, AccountsLoadedValue, AccountsRejectedValue, HMRCFinancialYear, BatchRejected) " & _
                                "VALUES ('" & TrancheID & "', GETDATE(), SYSTEM_USER, 0, " & AccountsRejected.ToString & ", '" & HeaderValidation & "', '" & TrailerValidation & _
                                                               "', '" & ColumnsValidation & "', " & RejectPercentage.ToString & ", '" & FileType & "', 0, " & AccountsRejectedValue & ", " & HMRCFinancialYear & ",'" & BatchRejected & "')"

            ExecStoredProc("FeesSQL", SQL)

            AddNewBatch = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Function UpdateCaseBatch(ByVal TrancheID As String) As Boolean

        UpdateCaseBatch = False

        Try
            Dim SQL As String = "UPDATE dbo.HMRCCaseBatch SET AccountsLoaded = " & frmMain.TotalLoadedCasesCount & ", AccountsLoadedValue = " & frmMain.TotalLoadedCasesValue & " WHERE TrancheID = '" & TrancheID & "'"

            ExecStoredProc("FeesSQL", SQL)

            UpdateCaseBatch = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Sub GetCaseBatch(ByVal TrancheID As String)

        Try
            Dim SQL As String = "SELECT 'ROSS' + CAST(d.DebtorID AS VARCHAR) AS DebtorID, d.ClientRef, d.TrancheID, d.RecordSequence, d.ErrorCode, d.ErrorDesc, b.BatchRejected " & _
                                "FROM dbo.HMRCCaseBatchDetail AS d " & _
                                "INNER JOIN dbo.HMRCCaseBatch AS b ON d.TrancheID = b.TrancheID " & _
                                "WHERE d.TrancheID = " & TrancheID & " " & _
                                "ORDER BY d.RecordSequence"

            ' The following code was used for the Tranche 1049 issue
            'Dim SQL As String = "SELECT 'ROSS' + CAST(d.DebtorID AS VARCHAR) AS DebtorID, d.ClientRef, d.TrancheID, d.RecordSequence, d.ErrorCode, d.ErrorDesc, b.BatchRejected " & _
            '        "FROM dbo.HMRCCaseBatchDetail AS d " & _
            '        "INNER JOIN dbo.HMRCCaseBatch AS b ON d.TrancheID = b.TrancheID " & _
            '        "WHERE d.TrancheID = " & TrancheID & " " & _
            '        "  AND EXISTS ( SELECT t.DebtorID FROM Tranche1049 AS t WHERE t.DebtorID = d.DebtorID) " & _
            '        "ORDER BY d.RecordSequence"

            LoadDataTable("FeesSQL", SQL, DTCaseBatch)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetHODDebtLevel()

        Try
            Dim SQL As String = "SELECT HOD, CAST(MinimumDebt AS VARCHAR) AS MinimumDebt, MaximumDebt " & _
                                "FROM dbo.HODDebtLevel " & _
                                "ORDER BY HOD"

            LoadDataTable("FeesSQL", SQL, DTHODDebtLevel)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetTrancheList()

        Try
            Dim SQL As String = "SELECT DISTINCT TrancheID, DATEADD(dd, 0, DATEDIFF(dd, 0, CreatedDate)) " & _
                                "FROM dbo.HMRCCaseBatchDetail " & _
                                "ORDER BY DATEADD(dd, 0, DATEDIFF(dd, 0, CreatedDate)) DESC"

            LoadDataTable("FeesSQL", SQL, DTTranche)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub SetHODDebtLevel(ByVal HOD As String, ByVal MinimumDebt As String, ByVal MaximumDebt As String)
        Try
            ExecStoredProc("FeesSQL", "UPDATE dbo.HODDebtLevel SET MinimumDebt = " & MinimumDebt & ", MaximumDebt = " & MaximumDebt & " WHERE HOD = '" & HOD & "'", 600)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Function GetTrancheIDCount(ByVal TrancheID As String) As Integer

        GetTrancheIDCount = 0

        Try
            GetTrancheIDCount += CInt(GetSQLResults("FeesSQL", "SELECT COUNT(*) " & _
                                                               "FROM dbo.HMRCCaseBatchDetail " & _
                                                               "WHERE TrancheID = " & TrancheID))

            GetTrancheIDCount += CInt(GetSQLResults("FeesSQL", "SELECT COUNT(*) " & _
                                                               "FROM dbo.HMRCCaseBatch " & _
                                                               "WHERE TrancheID = " & TrancheID))

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Public Function ClearBatch(ByVal TrancheID As String) As Boolean

        ClearBatch = False

        Try
            ExecStoredProc("FeesSQL", "DELETE " & _
                                      "FROM dbo.HMRCCaseBatchDetail " & _
                                      "WHERE TrancheID = " & TrancheID)

            ExecStoredProc("FeesSQL", "DELETE " & _
                                      "FROM dbo.HMRCCaseBatch " & _
                                      "WHERE TrancheID = " & TrancheID)

            ClearBatch = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

End Class


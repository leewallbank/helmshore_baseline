﻿Imports CommonLibrary
Imports System.IO
Imports System.Text.RegularExpressions

Public Class frmMain

#Region "Declarations"

    Private cboTrancheIDChanged As Boolean = False

    Private Structure FileDetail
        Public Filename As String
        Public HOD As String
        Public CaseCount As Integer
        Public WorkItemsCount As Integer
        Public TotalBalance As Decimal
    End Structure

    Private NewDebtFile(0) As FileDetail
    Private HMRCData As New clsHMRCData
    Private FilePath As String, VersionNum As String, InputFileName As String, FileExt As String, ErrorLog As String, FileType As String, TrancheID As String
    Private FileNum As Integer
    Private InputLineArray() As String

    Private TotalErrorCasesValue As Decimal
    Private TotalErrorCasesCount As Integer, TotalErrorWorkItemsCount As Integer

    Public TotalLoadedCasesValue As Decimal
    Public TotalLoadedCasesCount As Integer

    Private RowValid As Boolean, PlacementFileValid As Boolean

    ' File attributes
    Private Const DCAID As String = "7"
    Private Const NumOfFixedCols As Integer = 57 ' in the input file
    Private Const NumOfWorkFlowItemCols As Integer = 5 ' in the input file
    Private Const Separator As String = "|" ' the same separator is for input and output files

    Private HODCodes() As String = {"K", "D", "P", "B", "N", "C"}

    ' Column positions - all zero based
    Private Const DCAIDPos As Integer = 0
    Private Const TrancheIDPos As Integer = 1
    Private Const HouseholdRefPos As Integer = 3
    Private Const HODPos As Integer = 4
    Private Const Debtor1NINOPos As Integer = 5
    Private Const Debtor1DOBPos As Integer = 6
    Private Const Debtor1TitlePos As Integer = 7
    Private Const Debtor1ForenamesPos As Integer = 8
    Private Const Debtor1SurnamePos As Integer = 9
    Private Const Debtor1NameLine2Pos As Integer = 10
    Private Const Debtor1AddressLine1Pos As Integer = 11
    Private Const Debtor1AddressLine2Pos As Integer = 12
    Private Const Debtor1AddressLine3Pos As Integer = 13
    Private Const Debtor1AddressLine4Pos As Integer = 14
    Private Const Debtor1PostcodePos As Integer = 15
    Private Const Debtor1ContactTelNo1Pos As Integer = 16
    Private Const Debtor1ContactTelNo2Pos As Integer = 17
    Private Const Debtor1ContactTelNo3Pos As Integer = 18
    Private Const Debtor1RecoveryTitlePos As Integer = 19
    Private Const Debtor1RecoveryForenamesPos As Integer = 20
    Private Const Debtor1RecoverySurnamePos As Integer = 21
    Private Const Debtor1RecoveryNameLine2Pos As Integer = 22
    Private Const Debtor1RecoveryAddressLine1Pos As Integer = 23
    Private Const Debtor1RecoveryAddressLine2Pos As Integer = 24
    Private Const Debtor1RecoveryAddressLine3Pos As Integer = 25
    Private Const Debtor1RecoveryAddressLine4Pos As Integer = 26
    Private Const Debtor1RecoveryPostcodePos As Integer = 27
    Private Const Debtor1RecoveryTelNoPos As Integer = 28
    Private Const Debtor1BusTelNoPos As Integer = 29
    Private Const Debtor2NINOPos As Integer = 30
    Private Const Debtor2DOBPos As Integer = 31
    Private Const Debtor2TitlePos As Integer = 32
    Private Const Debtor2ForenamesPos As Integer = 33
    Private Const Debtor2SurnamePos As Integer = 34
    Private Const Debtor2NameLine2Pos As Integer = 35
    Private Const Debtor2AddressLine1Pos As Integer = 36
    Private Const Debtor2AddressLine2Pos As Integer = 37
    Private Const Debtor2AddressLine3Pos As Integer = 38
    Private Const Debtor2AddressLine4Pos As Integer = 39
    Private Const Debtor2PostcodePos As Integer = 39
    Private Const Debtor2ContactTelNo1Pos As Integer = 41
    Private Const Debtor2ContactTelNo2Pos As Integer = 42
    Private Const Debtor2ContactTelNo3Pos As Integer = 43
    Private Const Debtor2RecoveryTitlePos As Integer = 44
    Private Const Debtor2RecoveryForenamesPos As Integer = 45
    Private Const Debtor2RecoverySurnamePos As Integer = 46
    Private Const Debtor2RecoveryNameLine2Pos As Integer = 47
    Private Const Debtor2RecoveryAddressLine1Pos As Integer = 48
    Private Const Debtor2RecoveryAddressLine2Pos As Integer = 49
    Private Const Debtor2RecoveryAddressLine3Pos As Integer = 50
    Private Const Debtor2RecoveryAddressLine4Pos As Integer = 51
    Private Const Debtor2RecoveryPostcodePos As Integer = 52
    Private Const Debtor2RecoveryTelNoPos As Integer = 53
    Private Const Debtor2BusTelNoPos As Integer = 54
    Private Const TotalDebtPos As Integer = 55
    Private Const NumWorkItemsPos As Integer = 56
    Private Const PaymentRef01Pos As Integer = 60

    Private FixedColumnHeader() As String = {"DCA ID", "Tranche ID", "Segment ID", "Ddebt Reference Number", "HOD", "Debtor 1 NINO", "Debtor 1 DOB", "Debtor 1 Title", "Debtor 1 Forenames", "Debtor 1 Surname", _
                                             "Debtor 1 Name Line 2", "Debtor 1 Address Line 1", "Debtor 1 Address Line 2", "Debtor 1 Address Line 3", "Debtor 1 Address Line 4", "Debtor 1 Postcode", "Debtor 1 Contact Tel No 1", "Debtor 1 Contact Tel No 2", "Debtor 1 Contact Tel No 3", "Debtor 1 Recovery Title", _
                                             "Debtor 1 Recovery Forenames", "Debtor 1 Recovery Surname", "Debtor 1 Recovery Name Line 2", "Debtor 1 Recovery Address Line 1", "Debtor 1 Recovery Address Line 2", "Debtor 1 Recovery Address Line 3", "Debtor 1 Recovery Address Line 4", "Debtor 1 Recovery Postcode", "Debtor 1 Recovery Tel No", "Debtor 1 Business Tel No", _
                                             "Debtor 2 NINO", "Debtor 2 DOB", "Debtor 2 Title", "Debtor 2 Forenames", "Debtor 2 Surname", "Debtor 2 Name Line 2", "Debtor 2 Address Line 1", "Debtor 2 Address Line 2", "Debtor 2 Address Line 3", "Debtor 2 Address Line 4", _
                                             "Debtor 2 Postcode", "Debtor 2 Contact Tel No 1", "Debtor 2 Contact Tel No 2", "Debtor 2 Contact Tel No 3", "Debtor 2 Recovery Title", "Debtor 2 Recovery Forenames", "Debtor 2 Recovery Surname", "Debtor 2 Recovery Name Line 2", "Debtor 2 Recovery Address Line 1", "Debtor 2 Recovery Address Line 2", _
                                             "Debtor 2 Recovery Address Line 3", "Debtor 2 Recovery Address Line 4", "Debtor 2 Recovery Postcode", "Debtor 2 Recovery Tel No", "Debtor 2 Business Tel No", "Total Debt Outstanding", "Number Of Work Items"}

    Private WorkItemHeader() As String = {"Tax Year Due ", "Latest Due Date ", "Collectible Amount of Work Item ", "Debtor 1 Payment Ref ", "Debtor 2 Payment Ref "}

    Private Const LineTerminator As String = vbLf

#End Region

    Public Sub New()
        Try
            ' This call is required by the designer.
            InitializeComponent()

            ' Add any initialization after the InitializeComponent() call.
            btnViewInputFile.Enabled = False
            btnViewLogFile.Enabled = False
            btnViewOutputFile.Enabled = False

            txtMaxLinesInOutputFile.Text = My.Settings.MaxLinesInOutputFile.ToString
            txtBatchRejectPercentageThreshold.Text = My.Settings.BatchRejectPercentageThreshold.ToString

            HMRCData.GetTrancheList()

            HMRCData.GetHODDebtLevel()
            dgvDebtLevels.DataSource = HMRCData.HODDebtLevel

            cboTrancheID.DataSource = HMRCData.Tranche
            cboTrancheID.DisplayMember = "TrancheID"
            cboTrancheID.ValueMember = "TrancheID"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmMain_Shown(sender As Object, e As System.EventArgs) Handles Me.Shown
        AddHandler dgvDebtLevels.CellValidating, AddressOf dgvDebtLevels_CellValidating
    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Try
            Dim FileDialog As New OpenFileDialog
            Dim DebtColsStart As Integer, TotalValidCasesCount As Integer, TotalValidWorkItemsCount As Integer, CaseWorkItemsCount As Integer, LineNumber As Integer
            Dim HODFileNum(UBound(HODCodes)) As Integer
            Dim NewDebtNotes As String = "", NewWorkItemNotes As String = "", AuditLog As String
            Dim NewDebtLine(NumOfFixedCols) As String, RowError(1) As String
            Dim CaseBalance As Decimal, TotalBalance As Decimal, RejectionPerc As Decimal
            Dim Debtor2Required As Boolean = False, ManifestFileValid As Boolean
            Dim BatchRejected As String = "N"
            Dim HODDebtLevel As DataView = New DataView(HMRCData.HODDebtLevel)

            For HODNum As Integer = 0 To UBound(HODCodes)
                HODFileNum(HODNum) = -1
            Next HODNum

            btnViewInputFile.Enabled = False
            btnViewLogFile.Enabled = False
            btnViewOutputFile.Enabled = False

            rtxtErrorLog.Text = ""
            ErrorLog = ""
            TotalErrorCasesCount = 0
            TotalErrorWorkItemsCount = 0
            TotalErrorCasesValue = 0
            LineNumber = 0
            TotalLoadedCasesCount = 0
            TotalLoadedCasesValue = 0
            ReDim NewDebtFile(0) ' Clear the array
            FileNum = 0

            FileType = ""
            TrancheID = ""

            FileDialog.Filter = "HMRC placement files|DCA.DEBT_PLACEMENT.H.*.dat|All files (*.*)|*.*" '" & Microsoft.VisualBasic.Right("00000" & DCAID, 5) & ".
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            If Not MsgBox("Selected file " & FileDialog.FileName, MsgBoxStyle.OkCancel + MsgBoxStyle.DefaultButton2, Me.Text) = MsgBoxResult.Ok Then Return

            FilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            InputFileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            For Each OldFile As String In Directory.GetFiles(FilePath, "*" & InputFileName & "*.txt")
                File.Delete(OldFile)
            Next OldFile

            Dim LogHeader As String = "File pre-processed: " & FilePath & InputFileName & FileExt & vbCrLf & "Date pre-processed: " & DateTime.Now.ToString & vbCrLf & "By: " & Environment.UserName & vbCrLf & vbCrLf

            lblReadingFile.Visible = True
            Application.DoEvents()

            ManifestFileValid = True
            PlacementFileValid = True

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)

            If File.Exists(FileDialog.FileName.Replace("DEBT_PLACEMENT", "MANIFEST")) Then
                Dim ManifestFileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName.Replace("DEBT_PLACEMENT", "MANIFEST"))
                Dim ManifestFileLine() As String = ManifestFileContents(0).Split(Separator)
                If ManifestFileLine(2) <> UBound(FileContents) + 1 Then
                    MsgBox("Record count in manifest file differs from placement file", MsgBoxStyle.Critical, Me.Text)
                    ManifestFileValid = False
                End If
                If ManifestFileLine(3) <> DCAID Then
                    MsgBox("Wrong DCA ID found manifest file", MsgBoxStyle.Critical, Me.Text)
                    ManifestFileValid = False
                End If
            Else
                MsgBox("No manifest file found", MsgBoxStyle.Critical, Me.Text)
                ManifestFileValid = False
            End If

            NewDebtFile(0).Filename = OutputFilename()
            lblReadingFile.Visible = False

            ProgressBar.Maximum = UBound(FileContents) + 2  ' plus audit file and output file minus header and trailer rows

            For Each InputLine As String In FileContents ' skip header and trailer lines
                Application.DoEvents() ' without this line, the button disappears until processing is complete

                If Not PlacementFileValid Then Exit For

                ProgressBar.Value = LineNumber
                LineNumber += 1
                InputLineArray = InputLine.Split(Separator)

                For LoopCount As Integer = 0 To UBound(InputLineArray) ' Trim all array elements
                    InputLineArray(LoopCount) = InputLineArray(LoopCount).Trim
                Next LoopCount

                RowValid = True
                CaseBalance = 0
                CaseWorkItemsCount = 0
                Array.Clear(NewDebtLine, 0, NewDebtLine.Length)
                NewDebtNotes = ""
                NewWorkItemNotes = ""
                RowError = Nothing

                ' Validate the number of columns
                If UBound(InputLineArray) < NumOfFixedCols - 1 Then AddToErrorLog("Insufficient number of columns (" & (UBound(InputLineArray) + 1).ToString & ") found at row no. " & LineNumber.ToString & ". Row not loaded.", True)

                ' Check valid work item make up
                If (UBound(InputLineArray) - (NumOfFixedCols - 1)) Mod NumOfWorkFlowItemCols <> 0 Then AddToErrorLog("Incomplete number of work items columns found at row no. " & LineNumber.ToString & ". Row not loaded.", True)

                If RowValid Then
                    ' Find the relevant HOD
                    HODDebtLevel.RowFilter = "HOD = '" & InputLineArray(HODPos) & "'"

                    'Validate fixed columns
                    For ColNum As Integer = 0 To NumOfFixedCols - 1
                        If Not RowError Is Nothing OrElse Not PlacementFileValid Then Exit For

                        Select Case ColNum
                            Case DCAIDPos
                                If String.IsNullOrEmpty(InputLineArray(ColNum)) Then RowError = {"0", "Invalide Tranch ID, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}
                                If TrancheID = "" Then
                                    TrancheID = InputLineArray(TrancheIDPos) ' This should always be the first data row. Coded this way to cater for the first data row being invalid
                                    If TrancheIDExists(TrancheID) Then
                                        If MsgBox("Batch " & TrancheID & " has already been pre processed. Overwrite?", MsgBoxStyle.Question + MsgBoxStyle.YesNo + MsgBoxStyle.DefaultButton2, Me.Text) = MsgBoxResult.Yes Then
                                            HMRCData.ClearBatch(TrancheID)
                                        Else
                                            AddToErrorLog("Batch already loaded", False)
                                        End If
                                    End If
                                End If

                                If String.IsNullOrEmpty(InputLineArray(ColNum)) OrElse InputLineArray(ColNum) <> DCAID Then RowError = {"0", "Invalid DCA ID, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}

                            Case HouseholdRefPos
                                ' Need to validate HOD first as this determines the valid HouseholdRef format
                                If Not HODCodes.Contains(InputLineArray(HODPos)) Then
                                    RowError = {"0", "Invalid HOD, column no. = " & (HODPos + 1).ToString & ", row no. = " & LineNumber.ToString}
                                    'Else
                                    'If FileType = "" Then FileType = HOD(InputLineArray(HODPos)) ' This should always be the first data row. Coded this way to cater for the first data row being invalid
                                    '    If String.IsNullOrEmpty(InputLineArray(ColNum)) Then RowError = {"0", "Invalid Customer Ref, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}

                                    '    If RowError Is Nothing Then
                                    '        If InputLineArray(HODPos) = "N" Then
                                    '            Debtor2Required = True
                                    '            If InputLineArray(ColNum).Length <> 33 OrElse Not (IsPaymentRef(InputLineArray(HODPos), InputLineArray(ColNum).Substring(0, 16)) OrElse InputLineArray(HouseholdRefPos).Substring(16, 1) = "/" OrElse IsPaymentRef(InputLineArray(HODPos), InputLineArray(ColNum).Substring(17, 16))) Then RowError = {"1", "Invalid Customer Ref, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}
                                    '        Else
                                    '            Debtor2Required = False
                                    '            If Not IsPaymentRef(InputLineArray(HODPos), InputLineArray(ColNum)) Then RowError = {"0", "Invalid Customer Ref, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}
                                    '        End If
                                    '    End If
                                End If

                                'Case Debtor1NINOPos
                                '    If Not String.IsNullOrEmpty(InputLineArray(ColNum)) AndAlso Not IsNINumber(InputLineArray(ColNum)) Then RowError = {"1", "Invalid Debtor1 NI no., column No. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}

                            Case Debtor1DOBPos
                                If Not String.IsNullOrEmpty(InputLineArray(ColNum)) Then
                                    '   MsgBox(InputLineArray(ColNum).Substring(6, 2) & "-" & InputLineArray(ColNum).Substring(4, 2) & "-" & InputLineArray(ColNum).Substring(0, 4))
                                    If IsDate(InputLineArray(ColNum).Substring(6, 2) & "-" & InputLineArray(ColNum).Substring(4, 2) & "-" & InputLineArray(ColNum).Substring(0, 4)) Then
                                        If CDate(InputLineArray(ColNum).Substring(6, 2) & "-" & InputLineArray(ColNum).Substring(4, 2) & "-" & InputLineArray(ColNum).Substring(0, 4)) >= DateTime.Now.AddYears(-16) Then RowError = {"0", "Debtor1 < 16 years of age, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}
                                    Else
                                        RowError = {"0", "Invalid DOB, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}
                                    End If
                                End If

                            Case Debtor1SurnamePos
                                If String.IsNullOrEmpty(InputLineArray(ColNum)) Then RowError = {"0", "Invalid Debtor1Surname, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}

                            Case Debtor1AddressLine1Pos
                                If String.IsNullOrEmpty(InputLineArray(ColNum)) Then RowError = {"0", "Invalid Debtor1AddressLine1, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}

                                'Case Debtor2NINOPos
                                '    If Debtor2Required AndAlso Not String.IsNullOrEmpty(InputLineArray(ColNum)) AndAlso Not Not IsNINumber(InputLineArray(ColNum)) Then RowError = {"1", "Invalid Debtor2 NI no., column No. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}

                            Case Debtor2DOBPos
                                If Not String.IsNullOrEmpty(InputLineArray(ColNum)) Then
                                    If IsDate(InputLineArray(ColNum)) Then
                                        If CDate(InputLineArray(ColNum)) >= DateTime.Now.AddYears(-16) Then RowError = {"0", "Debtor2 < 16 years of age, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}
                                    Else
                                        RowError = {"0", "Invalid Debtor2 DOB, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}
                                    End If
                                End If

                            Case Debtor2SurnamePos
                                If Debtor2Required AndAlso String.IsNullOrEmpty(InputLineArray(ColNum)) Then RowError = {"0", "Invalid Debtor2Surname, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}

                            Case Debtor2AddressLine1Pos
                                If Debtor2Required AndAlso String.IsNullOrEmpty(InputLineArray(ColNum)) Then RowError = {"0", "Invalid Debtor2AddressLine1, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}

                            Case TotalDebtPos
                                If String.IsNullOrEmpty(InputLineArray(ColNum)) Then RowError = {"0", "No outstanding debt, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}
                                If RowError Is Nothing AndAlso Not IsNumeric(InputLineArray(ColNum)) Then RowError = {"0", "Invalid outstanding debt, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}
                                If RowError Is Nothing AndAlso (CDec(InputLineArray(ColNum)) < HODDebtLevel.ToTable.Rows(0).Item("MinimumDebt")) Then RowError = {"1", "Outstanding debt out of specified range, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}
                                If RowError Is Nothing AndAlso (CDec(InputLineArray(ColNum)) > HODDebtLevel.ToTable.Rows(0).Item("MaximumDebt")) Then RowError = {"1", "Outstanding debt out of specified range, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}

                            Case NumWorkItemsPos
                                If String.IsNullOrEmpty(InputLineArray(ColNum)) Then RowError = {"0", "No number of workitems, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}
                                If RowError Is Nothing AndAlso Not IsNumeric(InputLineArray(ColNum)) Then RowError = {"0", "Invalid number of work items, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}
                                If RowError Is Nothing AndAlso CInt(InputLineArray(ColNum)) <= 0 Then RowError = {"0", "Invalid number of work items, column no. = " & (ColNum + 1).ToString & ", row no. = " & LineNumber.ToString}

                        End Select
                    Next ColNum

                    ' Validate where at least one field is populated in a collection
                    ' Claimant 1 address lines 2, 3, 4 and Postcode
                    If RowError Is Nothing And Join({InputLineArray(Debtor1AddressLine2Pos), InputLineArray(Debtor1AddressLine3Pos), InputLineArray(Debtor1AddressLine4Pos), InputLineArray(Debtor1PostcodePos)}, "").Length = 0 Then RowError = {"1", "Invalid claimant 1 address, row no. = " & LineNumber.ToString}
                    ' Claimant 2 address lines 2, 3, 4 and Postcode
                    If Debtor2Required And RowError Is Nothing And Join({InputLineArray(Debtor2AddressLine2Pos), InputLineArray(Debtor2AddressLine3Pos), InputLineArray(Debtor2AddressLine4Pos), InputLineArray(Debtor2PostcodePos)}, "").Length = 0 Then RowError = {"1", "Invalid claimant 2 address, row no. = " & LineNumber.ToString}

                    If RowError Is Nothing Then
                        ' Work through and build notes for work items, validating at the same time. Notes are built regardless of validation for speed.
                        For WorkItemNum As Integer = 1 To InputLineArray(NumWorkItemsPos)
                            DebtColsStart = NumOfFixedCols + ((WorkItemNum - 1) * NumOfWorkFlowItemCols)

                            CaseWorkItemsCount += 1

                            For WorkItemCol As Integer = 0 To NumOfWorkFlowItemCols - 1 ' loop through the five work item columns and add to notes
                                ' Exit loop if errors found. No point in continuing as the note will not be added to the case
                                If Not RowError Is Nothing Then Exit For

                                ' Start validating
                                If WorkItemCol = 0 And String.IsNullOrEmpty(InputLineArray(DebtColsStart + WorkItemCol)) Then RowError = {"1", "Tax year = NULL, column no. = " & (DebtColsStart + WorkItemCol + 1).ToString & ", row no. = " & LineNumber.ToString}

                                If WorkItemCol = 2 Then
                                    ' Validate collectable amount
                                    If String.IsNullOrEmpty(InputLineArray(DebtColsStart + WorkItemCol)) Then RowError = {"1", "Collectable amount of work item = NULL, column no. = " & (DebtColsStart + WorkItemCol + 1).ToString & ", row no. = " & LineNumber.ToString}
                                    If RowError Is Nothing AndAlso Not IsNumeric(InputLineArray(DebtColsStart + WorkItemCol)) Then RowError = {"1", "Invalid Currency Value, column no. = " & (DebtColsStart + WorkItemCol + 1).ToString & ", row no. = " & LineNumber.ToString}
                                    If RowError Is Nothing AndAlso CDec(InputLineArray(DebtColsStart + WorkItemCol)) <= 0 Then RowError = {"1", "Collectable amount of work item < = 0, column no. = " & (DebtColsStart + WorkItemCol + 1).ToString & ", row no. = " & LineNumber.ToString}
                                    If RowError Is Nothing Then CaseBalance += CDec(InputLineArray(DebtColsStart + 2))
                                End If

                                ' Validate payment refs
                                If WorkItemCol = 3 And Not IsPaymentRef(InputLineArray(HODPos), InputLineArray(DebtColsStart + WorkItemCol)) Then RowError = {"1", "Invalid " & WorkItemHeader(WorkItemCol) & WorkItemNum.ToString("00") & ", row no. = " & LineNumber.ToString}
                                If WorkItemCol = 4 And Debtor2Required And Not IsPaymentRef(InputLineArray(HODPos), InputLineArray(DebtColsStart + WorkItemCol)) Then RowError = {"1", "Invalid " & WorkItemHeader(WorkItemCol) & WorkItemNum.ToString("00") & ", row no. = " & LineNumber.ToString}

                                ' Build up notes 
                                ' IF statement added TS 12/Aug/2014. Request ref 27169
                                If Not String.IsNullOrEmpty(InputLineArray(DebtColsStart + WorkItemCol)) Then NewWorkItemNotes &= String.Join("", ToNote(" " & InputLineArray(DebtColsStart + WorkItemCol), WorkItemHeader(WorkItemCol) & WorkItemNum.ToString("00"), ";")) ' Replace the # in the column header with the work item number

                            Next WorkItemCol
                        Next WorkItemNum

                        If RowError Is Nothing AndAlso Not IsNumeric(InputLineArray(TotalDebtPos)) Then RowError = {"1", "Invalid total debt, column no. = " & TotalDebtPos + 1 & ", row no. = " & LineNumber.ToString}
                        If RowError Is Nothing AndAlso CaseBalance <> CDec(InputLineArray(TotalDebtPos)) Then RowError = {"1", "Total debt <> work items, column no. = " & TotalDebtPos + 1 & ", row no. = " & LineNumber.ToString}
                        If RowError Is Nothing AndAlso Not IsNumeric(InputLineArray(NumWorkItemsPos)) Then RowError = {"1", "Invalid number of work items, column no. = " & TotalDebtPos + 1 & ", row no. = " & LineNumber.ToString}
                    End If
                End If
                If Not RowError Is Nothing Then AddToErrorLog(RowError(1), True)

                ' Now insert into the SQL Server database
                If Not HMRCData.AddHMRCCaseBatchDetail(TrancheID, ProgressBar.Value + 1, InputLineArray(HouseholdRefPos), RowError) Then AddToErrorLog("Unable to add record in FeesSQL.HMRCCaseBatchDetail, row no. = " & (LineNumber + 1).ToString, True)

                If RowValid Then
                    ' Start to build the output file
                    For ColNum As Integer = 0 To NumOfFixedCols - 1
                        Select Case ColNum

                            Case Debtor1TitlePos, Debtor2TitlePos
                                ' Concatenate title and the three name lines
                                NewDebtLine(ColNum) = ConcatFields({InputLineArray(ColNum).ToString, InputLineArray(ColNum + 1).ToString, InputLineArray(ColNum + 2).ToString, InputLineArray(ColNum + 3).ToString}, " ")

                            Case Debtor1ForenamesPos, Debtor1SurnamePos, Debtor1NameLine2Pos, Debtor2ForenamesPos, Debtor2SurnamePos, Debtor2NameLine2Pos
                                ' leave the field blank - catered for in DebtorXTitlePos

                            Case Debtor1AddressLine1Pos
                                NewDebtLine(ColNum) = ConcatFields({InputLineArray(ColNum).ToString _
                                                                  , InputLineArray(ColNum + 1).ToString _
                                                                  , InputLineArray(ColNum + 2).ToString _
                                                                  , InputLineArray(ColNum + 3).ToString _
                                                                  , InputLineArray(ColNum + 4).ToString} _
                                                                  , ","
                                                                  )

                                NewDebtNotes &= String.Join("", ToNote(ConcatFields({InputLineArray(ColNum).ToString _
                                                                                   , InputLineArray(ColNum + 1).ToString _
                                                                                   , InputLineArray(ColNum + 2).ToString _
                                                                                   , InputLineArray(ColNum + 3).ToString _
                                                                                   , InputLineArray(ColNum + 4).ToString} _
                                                                                   , ", "
                                                                                   ) _
                                                                      , "Debtor 1 Address" _
                                                                      , ";" _
                                                                      ) _
                                                           )

                            Case Debtor1RecoveryAddressLine1Pos
                                NewDebtNotes &= String.Join("", ToNote(ConcatFields({InputLineArray(ColNum).ToString _
                                                                                   , InputLineArray(ColNum + 1).ToString _
                                                                                   , InputLineArray(ColNum + 2).ToString _
                                                                                   , InputLineArray(ColNum + 3).ToString _
                                                                                   , InputLineArray(ColNum + 4).ToString} _
                                                                                   , ", "
                                                                                   ) _
                                                                      , "Debtor 1 Recovery Address" _
                                                                      , ";" _
                                                                      ) _
                                                           )

                            Case Debtor2AddressLine1Pos
                                NewDebtNotes &= String.Join("", ToNote(ConcatFields({InputLineArray(ColNum).ToString _
                                                                                   , InputLineArray(ColNum + 1).ToString _
                                                                                   , InputLineArray(ColNum + 2).ToString _
                                                                                   , InputLineArray(ColNum + 3).ToString _
                                                                                   , InputLineArray(ColNum + 4).ToString} _
                                                                                   , ", "
                                                                                   ) _
                                                                      , "Debtor 2 Address" _
                                                                      , ";" _
                                                                      ) _
                                                           )

                            Case Debtor2RecoveryAddressLine1Pos
                                NewDebtNotes &= String.Join("", ToNote(ConcatFields({InputLineArray(ColNum).ToString _
                                                                                   , InputLineArray(ColNum + 1).ToString _
                                                                                   , InputLineArray(ColNum + 2).ToString _
                                                                                   , InputLineArray(ColNum + 3).ToString _
                                                                                   , InputLineArray(ColNum + 4).ToString} _
                                                                                   , ", "
                                                                                   ) _
                                                                      , "Debtor 2 Recovery Address" _
                                                                      , ";" _
                                                                      ) _
                                                           )

                            Case Debtor1AddressLine2Pos, Debtor1AddressLine3Pos, Debtor1RecoveryAddressLine2Pos, Debtor2AddressLine2Pos, Debtor2RecoveryAddressLine2Pos, Debtor1RecoveryAddressLine3Pos, Debtor2AddressLine3Pos, Debtor2RecoveryAddressLine3Pos, Debtor1AddressLine4Pos, Debtor1RecoveryAddressLine4Pos, Debtor2AddressLine4Pos, Debtor2RecoveryAddressLine4Pos, Debtor1PostcodePos, Debtor1RecoveryPostcodePos, Debtor2PostcodePos, Debtor2RecoveryPostcodePos
                                ' leave the field blank - catered for in Debtor1AddressLine1Pos, Debtor1RecoveryAddressLine1Pos, Debtor2AddressLine1Pos, Debtor2RecoveryAddressLine1Pos

                            Case Debtor1ContactTelNo1Pos, Debtor2ContactTelNo1Pos
                                If Not String.IsNullOrEmpty(InputLineArray(ColNum)) Then NewDebtNotes &= String.Join("", ToNote(" " & InputLineArray(ColNum), FixedColumnHeader(ColNum), ";"))

                                Dim TelNums() As String = ConcatFields({InputLineArray(ColNum), InputLineArray(ColNum + 1), InputLineArray(ColNum + 2)}, ",").Replace(" ", "").Split(",") ' Remove any spaces. ConcatFields then Split results in an array with no Nothing elements
                                For TelNumCount As Integer = 0 To UBound(TelNums)
                                    NewDebtLine(ColNum + TelNumCount) = TelNums(TelNumCount)
                                Next TelNumCount

                            Case Debtor1RecoveryTitlePos
                                NewDebtNotes &= String.Join("", ToNote(ConcatFields({InputLineArray(ColNum).ToString.Trim, InputLineArray(ColNum + 1).ToString.Trim, InputLineArray(ColNum + 2).ToString.Trim, InputLineArray(ColNum + 3).ToString.Trim}, " "), "Debtor 1 Recovery Name", ";"))

                            Case Debtor2RecoveryTitlePos
                                NewDebtNotes &= String.Join("", ToNote(ConcatFields({InputLineArray(ColNum).ToString.Trim, InputLineArray(ColNum + 1).ToString.Trim, InputLineArray(ColNum + 2).ToString.Trim, InputLineArray(ColNum + 3).ToString.Trim}, " "), "Debtor 2 Recovery Name", ";"))

                            Case Debtor1RecoveryForenamesPos, Debtor1RecoverySurnamePos, Debtor1RecoveryNameLine2Pos, Debtor2RecoveryForenamesPos, Debtor2RecoverySurnamePos, Debtor2RecoveryNameLine2Pos
                                ' leave the field blank - catered for in Debtor1RecoveryTitlePos, Debtor2RecoveryTitlePos

                            Case Debtor1ContactTelNo2Pos, Debtor1ContactTelNo3Pos, Debtor1RecoveryTelNoPos, Debtor1BusTelNoPos, Debtor2ContactTelNo2Pos, Debtor2ContactTelNo3Pos, Debtor2RecoveryTelNoPos, Debtor2BusTelNoPos
                                ' add to notes
                                If Not String.IsNullOrEmpty(InputLineArray(ColNum)) Then NewDebtNotes &= String.Join("", ToNote(" " & InputLineArray(ColNum), FixedColumnHeader(ColNum), ";"))

                            Case Else
                                NewDebtLine(ColNum) = InputLineArray(ColNum).Trim
                        End Select
                    Next ColNum

                    NewDebtLine(NumOfFixedCols) = InputLineArray(PaymentRef01Pos) ' Add the first payment reference for debtor 1

                    FileNum = HODFileNum(Array.IndexOf(HODCodes, InputLineArray(HODPos)))

                    If FileNum = -1 And UBound(NewDebtFile) = 0 And NewDebtFile(0).CaseCount = 0 Then ' only applies to the first valid line in the input file
                        FileNum = 0
                        HODFileNum(Array.IndexOf(HODCodes, InputLineArray(HODPos))) = FileNum
                        NewDebtFile(FileNum).HOD = HOD(InputLineArray(HODPos))
                    End If

                    If FileNum = -1 OrElse NewDebtFile(FileNum).CaseCount = My.Settings.MaxLinesInOutputFile Then ' for the first time a HOD is used or when an existing file has reached its maximum permissible size
                        FileNum = UBound(NewDebtFile) + 1
                        ReDim Preserve NewDebtFile(FileNum)
                        HODFileNum(Array.IndexOf(HODCodes, InputLineArray(HODPos))) = FileNum
                        NewDebtFile(FileNum).Filename = OutputFilename()
                        NewDebtFile(FileNum).HOD = HOD(InputLineArray(HODPos))
                    End If

                    AppendToFile(FilePath & NewDebtFile(FileNum).HOD & "_" & NewDebtFile(FileNum).Filename, LineNumber.ToString & Separator & Join(NewDebtLine, Separator) & Separator & NewDebtNotes & NewWorkItemNotes & vbCrLf)

                    ' Update running totals
                    TotalValidCasesCount += 1
                    NewDebtFile(FileNum).CaseCount += 1
                    TotalValidWorkItemsCount += CaseWorkItemsCount
                    NewDebtFile(FileNum).WorkItemsCount += CaseWorkItemsCount
                    TotalBalance += CaseBalance
                    NewDebtFile(FileNum).TotalBalance += CaseBalance
                End If ' RowValid

            Next InputLine

            ProgressBar.Value = ProgressBar.Maximum

            ' Validate totals
            If TotalValidCasesCount = 0 Then AddToErrorLog("No valid cases found in file.", False)
            If TotalValidWorkItemsCount = 0 Then AddToErrorLog("No valid work items found in file.", False)
            If TotalBalance = 0 Then AddToErrorLog("No valid debt stated found in file.", False)

            If TotalErrorCasesCount + TotalValidCasesCount > 0 Then
                RejectionPerc = CDec(Math.Round((TotalErrorCasesCount * 100) / (TotalErrorCasesCount + TotalValidCasesCount), 2))
            Else
                RejectionPerc = 0
            End If

            ' Update audit log
            AuditLog = LogHeader & "Number of new cases: " & TotalValidCasesCount.ToString & vbCrLf
            AuditLog &= "Number of new work items: " & TotalValidWorkItemsCount.ToString & vbCrLf
            AuditLog &= "Total balance of new cases: " & TotalBalance.ToString & vbCrLf & vbCrLf
            AuditLog &= "Rejected cases: " & TotalErrorCasesCount.ToString & vbCrLf
            AuditLog &= "Case rejection: " & RejectionPerc.ToString & "%" & vbCrLf
            AuditLog &= vbCrLf & vbCrLf & "Files created: " & (UBound(NewDebtFile) + 1).ToString & vbCrLf

            If NewDebtFile(0).CaseCount > 0 Then ' Check to see if any output files have been created
                For FileLoopCount = 0 To UBound(NewDebtFile)
                    AuditLog &= vbCrLf & vbCrLf & "File " & (FileLoopCount + 1).ToString & " - " & NewDebtFile(FileLoopCount).HOD & vbCrLf & vbCrLf
                    AuditLog &= "Number of new cases: " & NewDebtFile(FileLoopCount).CaseCount.ToString & vbCrLf
                    AuditLog &= "Number of new work items: " & NewDebtFile(FileLoopCount).WorkItemsCount.ToString & vbCrLf
                    AuditLog &= "Total balance of new cases: " & NewDebtFile(FileLoopCount).TotalBalance.ToString & vbCrLf
                Next FileLoopCount
            End If

            WriteFile(FilePath & InputFileName & "_Audit.txt", AuditLog)

            'ProgressBar.Value += 1

            If ErrorLog <> "" Then WriteFile(FilePath & InputFileName & "_Error.txt", LogHeader & ErrorLog)

            If Not ManifestFileValid Then
                MsgBox("The batch has been rejected due to an invalid manifest file", MsgBoxStyle.Critical, Me.Text)
                BatchRejected = "Y"
            ElseIf RejectionPerc >= My.Settings.BatchRejectPercentageThreshold Then
                MsgBox(Decimal.Round(RejectionPerc).ToString & "% could not be loaded.", MsgBoxStyle.Critical, Me.Text)
                MsgBox("The batch has been rejected. Please see the error log for details", MsgBoxStyle.Critical, Me.Text)
                BatchRejected = "Y"
            Else
                MsgBox("Value loaded £" & TotalBalance.ToString, MsgBoxStyle.Information, Me.Text)
                MsgBox("Value rejected £" & TotalErrorCasesValue.ToString, MsgBoxStyle.Information, Me.Text)
                MsgBox("Batch successfully processed.", MsgBoxStyle.Information, Me.Text)
                btnViewOutputFile.Enabled = True
            End If

            If Not HMRCData.AddNewBatch(TrancheID, TotalValidCasesCount, TotalErrorCasesCount, "Y", "Y", "Y", RejectionPerc, FileType, TotalErrorCasesValue, HMRCFinancialYear, BatchRejected) Then AddToErrorLog("Unable to add record in FeesSQL.HMRCCaseBatch", False)

            cboTrancheID.SelectedValue = TrancheID
            cboTrancheIDChanged = False

            btnViewInputFile.Enabled = True
            btnViewLogFile.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnReconcile_Click(sender As Object, e As System.EventArgs) Handles btnReconcile.Click
        Try
            Dim UpdateLine(26) As String

            If FilePath = "" Or cboTrancheIDChanged Then ' If a file has not been pre processed or cboTrancheID has changed then prompt for a new output folder
                Dim FolderBrowserDialog As New FolderBrowserDialog

                If FolderBrowserDialog.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    FilePath = FolderBrowserDialog.SelectedPath & "\"
                Else
                    Return
                End If
            End If

            TrancheID = cboTrancheID.SelectedValue
            Dim UpdateFileName As String = FilePath & "ReturnFiles\" & "DCA.DEBT_UPDATE.D." & DCAID.PadLeft(5, "0") & "." & DateTime.Today.ToString("yyyyMMdd") & ".dat"

            ' Delete the file if it exists
            If File.Exists(FilePath & UpdateFileName & FileExt) Then System.Diagnostics.Process.Start("notepad.exe", """" & FilePath & UpdateFileName & FileExt & """")
            If Directory.Exists(FilePath & "ReturnFiles\") Then Directory.Delete(FilePath & "ReturnFiles\", True)

            HMRCData.GetCaseBatch(TrancheID)

            ProgressBar.Value = 0 ' added v1.2. Request ref 38121
            ProgressBar.Minimum = 0 ' added v1.2. Request ref 38121
            ProgressBar.Maximum = HMRCData.CaseBatch.Rows.Count ' added v1.2. Request ref 38121

            ' Update records in the database
            For Each Account As DataRow In HMRCData.CaseBatch.Rows
                ProgressBar.Value += 1 ' added v1.2. Request ref 38121
                Application.DoEvents() ' added v1.2. Request ref 38121
                HMRCData.UpdateCaseBatchDetail(TrancheID, Account.Item("RecordSequence").ToString)
            Next Account

            HMRCData.GetCaseBatch(TrancheID) ' needs to run again to get the case IDs

            ' Populate update file using records from the database
            For Each NewCase As DataRow In HMRCData.CaseBatch.Rows
                Array.Clear(UpdateLine, 0, UpdateLine.Length)
                UpdateLine(0) = DCAID
                UpdateLine(1) = "D"
                UpdateLine(2) = If(NewCase.Item("BatchRejected") = "Y" Or Not IsDBNull(NewCase.Item("ErrorCode")), "ROSS_REJECT", NewCase.Item("DebtorID").ToString)
                UpdateLine(3) = NewCase.Item("ClientRef").ToString
                UpdateLine(4) = NewCase.Item("TrancheID").ToString

                UpdateLine(19) = Date.Today.ToString("yyyyMMdd")
                If Not IsDBNull(NewCase.Item("ErrorCode")) Then
                    UpdateLine(20) = NewCase.Item("ErrorCode").ToString
                Else
                    If NewCase.Item("BatchRejected") <> "Y" Then
                        UpdateLine(20) = "109" ' Success code
                    Else
                        UpdateLine(20) = "111" ' Error code other even though the case was valid
                    End If
                End If

                If Not IsDBNull(NewCase.Item("ErrorDesc")) Then UpdateLine(22) = NewCase.Item("ErrorDesc").ToString

                AppendToFile(UpdateFileName, String.Join(Separator, UpdateLine) & vbLf)
            Next NewCase

            CreateManifestFile(FilePath & "ReturnFiles\")

            CreateZipFile(FilePath & "DCA.FILE_SET.D." & DCAID.PadLeft(5, "0") & "." & DateTime.Today.ToString("yyyyMMdd") & ".zip", FilePath & "ReturnFiles\")

            Process.Start("explorer.exe", "/select," & FilePath & "DCA.FILE_SET.D." & DCAID.PadLeft(5, "0") & "." & DateTime.Today.ToString("yyyyMMdd") & ".zip")

            HMRCData.UpdateCaseBatch(TrancheID)

            MsgBox("Reconciliation successfully processed.", MsgBoxStyle.Information, Me.Text) ' added v1.2. Request ref 38121
            ProgressBar.Value = 0

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddToErrorLog(ErrorMessage As String, RowLevel As Boolean)
        Try
            ErrorLog &= ErrorMessage & vbCrLf
            rtxtErrorLog.AppendText(ErrorMessage & vbCrLf)
            If RowLevel Then
                TotalErrorCasesCount += 1
                If IsNumeric(InputLineArray(NumWorkItemsPos)) Then TotalErrorWorkItemsCount += CDec(InputLineArray(NumWorkItemsPos))
                If IsNumeric(InputLineArray(TotalDebtPos)) Then TotalErrorCasesValue += CDec(InputLineArray(TotalDebtPos))
                RowValid = False
            Else
                PlacementFileValid = False
                MsgBox(ErrorMessage, MsgBoxStyle.Critical, Me.Text)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboTranche_SelectedValueChanged(sender As Object, e As System.EventArgs) Handles cboTrancheID.SelectedValueChanged
        cboTrancheIDChanged = True
    End Sub


#Region "View buttons"

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Try
            If File.Exists(FilePath & InputFileName & FileExt) Then System.Diagnostics.Process.Start("notepad.exe", """" & FilePath & InputFileName & FileExt & """")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewOutputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        Try
            For FileNum As Integer = 0 To UBound(NewDebtFile)
                If File.Exists(NewDebtFile(FileNum).Filename) Then System.Diagnostics.Process.Start(NewDebtFile(FileNum).Filename)
            Next FileNum

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub btnViewLogFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFile.Click
        Try
            If File.Exists(FilePath & InputFileName & "_Audit.txt") Then System.Diagnostics.Process.Start(FilePath & InputFileName & "_Audit.txt")
            If File.Exists(FilePath & InputFileName & "_Error.txt") Then System.Diagnostics.Process.Start(FilePath & InputFileName & "_Error.txt")
            If File.Exists(FilePath & InputFileName & "_Exception.txt") Then System.Diagnostics.Process.Start(FilePath & InputFileName & "_Exception.txt")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Private Functions"

    Private Function IsPaymentRef(HODIndicator As String, RefNo As String) As Boolean

        IsPaymentRef = False

        Try

            Select Case HODIndicator

                Case "K"    ' The format of the SA reference is 9999999999 ' Note that HMRC dropped the K after initial testing
                    IsPaymentRef = Regex.IsMatch(RefNo, "^[0-9]{10}$")

                Case "D"    ' NINO
                    IsPaymentRef = IsNINumber(RefNo)

                Case "P"    ' PAYE PAYMENT REFERENCE  -   For .HOD_TYPE 'P'
                    '   Reference field – Total 18 Characters
                    '   The format of the BROCS reference is 999PC99999999YYMMA
                    '   This is constructed as follows:
                    '       999 = District Number (.DIST_NO - 3 characters)
                    '       P = File Identifier (LAN_WORK_ITEM.FILE_IND, always ‘P’ – 1 character)
                    '       C = Check Character (.HOD_CHECK_CHAR – 1 character)
                    '       99999999 = Register Number (.HOD_REG_NO – 8 characters, if less than 8 this field should be preceded by zeros)
                    '       YY = PAYE Year (3rd and 4th characters of LAN_WORK_ITEM.ASN – 2 characters, this should be from the ASN with the earliest outstanding year)
                    '       MM = Month Number (LAN_WORK_ITEM.INSTAL_NO) – 2 characters
                    '       A = Indicator (derived from LAN_WORK_ITEM.INSTAL_NO, values 01 –12 will have indicator value of ‘0’ and 13 or greater will have indicator of ‘A’ – 1 character.

                    ' IMPORTANT - Note example in first file was 992PB00003178 1213
                    IsPaymentRef = Regex.IsMatch(RefNo, "^[0-9]{3}[P]{1}[A-Z]{1}[0-9]{8}[ ]{1}[0-9]{4}$")
                    'IsPaymentRef = Regex.IsMatch(RefNo, "^[0-9]{3}[P]{1}[A-Z]{1}[0-9]{9}[ ]{1}[0-9]{4}$")

                Case "B"    ' VAT   VAT: 9 numerics format 999 9999 99
                    IsPaymentRef = Regex.IsMatch(RefNo, "^[0-9]{3}[ ][0-9]{4}[ ][0-9]{2}$")

                Case "N"
                    IsPaymentRef = Regex.IsMatch(RefNo, "^[A-Z]{2}[0-9]{12}[A-Z]{2}$")

                Case "C"    ' Not getting this debt stream in the first instance
                    'IsPaymentRef = Regex.IsMatch(RefNo, "^[0-9]{10}$") TS 1/Jul/2014
                    IsPaymentRef = Regex.IsMatch(RefNo, "^[0-9]{10}[A]{1}[0-9]{5}[A]{1}$")

                Case Else
                    IsPaymentRef = False

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function HOD(HODIndicator As String) As String

        HOD = ""

        Try

            Select Case HODIndicator

                Case "K"
                    HOD = "SA"

                Case "D"
                    HOD = "NIC2"

                Case "P"
                    HOD = "PAYE"  ' or NIC2 Penalty!!!!!!!! (HMRC Phil Lee has advised we shouldn't get NIC2 Penalty 05.09.2012

                Case "B"
                    HOD = "VAT"

                Case "N"
                    HOD = "TAX Credits"

                Case "C"
                    HOD = "COTAX"

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function HMRCFinancialYear() As Integer

        HMRCFinancialYear = -1

        Try

            HMRCFinancialYear = DateTime.Today.AddMonths(-3).Year

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function TrancheIDExists(TrancheID As String) As Boolean

        TrancheIDExists = False

        Try

            If HMRCData.GetTrancheIDCount(TrancheID) > 0 Then TrancheIDExists = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function OutputFilename() As String
        OutputFilename = InputFileName & "_" & (FileNum + 1).ToString.PadLeft(3, "0") & "_PreProcessed.txt"
    End Function

#End Region

#Region "Validate settings"

    Private Sub txtMaxLinesInOutputFile_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtMaxLinesInOutputFile.Validating
        Try
            Dim ErrorMsg As String = ""

            If Not IsNumeric(txtMaxLinesInOutputFile.Text) Then
                ErrorMsg = "Please enter a valid number of lines."
            ElseIf CInt(txtMaxLinesInOutputFile.Text) <= 0 Then
                ErrorMsg = "Please enter a positve number of lines."
            End If

            If ErrorMsg = "" Then
                My.Settings.Item("MaxLinesInOutputFile") = CInt(txtMaxLinesInOutputFile.Text)
            Else
                e.Cancel = True
                MsgBox(ErrorMsg, MsgBoxStyle.Critical, Me.Text)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub txtBatchRejectPercentageThreshold_Validating(sender As Object, e As System.ComponentModel.CancelEventArgs) Handles txtBatchRejectPercentageThreshold.Validating
        Try
            Dim ErrorMsg As String = ""

            If Not IsNumeric(txtBatchRejectPercentageThreshold.Text) Then
                ErrorMsg = "Please enter a valid percentage threshold."
            ElseIf CInt(txtBatchRejectPercentageThreshold.Text) <= 0 Then
                ErrorMsg = "Threshold cannot be less than zero."
            ElseIf CInt(txtBatchRejectPercentageThreshold.Text) >= 100 Then
                ErrorMsg = "Threshold must be less than 100."
            End If

            If ErrorMsg = "" Then
                My.Settings.Item("BatchRejectPercentageThreshold") = CDec(txtBatchRejectPercentageThreshold.Text)
            Else
                e.Cancel = True
                MsgBox(ErrorMsg, MsgBoxStyle.Critical, Me.Text)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvDebtLevels_CellValidating(sender As Object, e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) 'Handles dgvDebtLevels.CellValidating
        Try
            Dim ErrorMsg As String = ""

            If Not IsNumeric(dgvDebtLevels.Rows(e.RowIndex).Cells(1).EditedFormattedValue) Then
                ErrorMsg = "Please enter a valid minimum debt value."
            ElseIf CInt(dgvDebtLevels.Rows(e.RowIndex).Cells(1).EditedFormattedValue) <= 0 Then
                ErrorMsg = "Minimum debt cannot be less than zero."
            ElseIf CInt(dgvDebtLevels.Rows(e.RowIndex).Cells(2).EditedFormattedValue) < CInt(dgvDebtLevels.Rows(e.RowIndex).Cells(1).EditedFormattedValue) Then
                ErrorMsg = "Minimum debt cannot be greater than maximum debt."
            ElseIf Not IsNumeric(dgvDebtLevels.Rows(e.RowIndex).Cells(2).EditedFormattedValue) Then
                ErrorMsg = "Please enter a valid maximum debt value."
            ElseIf CInt(dgvDebtLevels.Rows(e.RowIndex).Cells(2).EditedFormattedValue) < 0 Then
                ErrorMsg = "Maximum debt cannot be less than zero."
            ElseIf CInt(dgvDebtLevels.Rows(e.RowIndex).Cells(2).EditedFormattedValue) < CInt(dgvDebtLevels.Rows(e.RowIndex).Cells(1).EditedFormattedValue) Then
                ErrorMsg = "Maximum debt cannot be less than minimum debt."
            End If

            If ErrorMsg <> "" Then
                e.Cancel = True
                MsgBox(ErrorMsg, MsgBoxStyle.Critical, Me.Text)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvDebtLevels_Validated(sender As Object, e As System.EventArgs) Handles dgvDebtLevels.Validated
        For Each dr As DataRow In HMRCData.HODDebtLevel.Rows
            If dr.RowState = DataRowState.Modified Then HMRCData.SetHODDebtLevel(dr.Item("HOD").ToString, dr.Item("MinimumDebt").ToString, dr.Item("MaximumDebt").ToString)
        Next dr
    End Sub


    Private Sub CreateManifestFile(ByVal OutputFolder As String)
        Try
            Dim ManifestLine As String = ""
            Dim ManifestFileName As String = OutputFolder & "DCA.MANIFEST.D." & DCAID.PadLeft(5, "0") & "." & Date.Now.ToString("yyyyMMdd") & ".dat"

            If File.Exists(ManifestFileName) Then File.Delete(ManifestFileName)

            For Each File As IO.FileInfo In New IO.DirectoryInfo(OutputFolder).GetFiles("DCA.*.D." & DCAID.PadLeft(5, "0") & ".*.dat")

                ManifestLine = String.Join(Separator, {File.Name _
                                                      , IO.File.ReadAllBytes(File.FullName).Length.ToString _
                                                      , IO.File.ReadAllLines(File.FullName).Length.ToString _
                                                      , DCAID _
                                                      , "D" _
                                                      , TrancheID _
                                                      , "" _
                                                      , Date.Now.ToString("yyyyMMdd")
                                                      } _
                                          ) & LineTerminator

                AppendToFile(ManifestFileName, ManifestLine)

            Next File

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region




End Class


﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.lblRejectionThreshold = New System.Windows.Forms.Label()
        Me.lblOutputLines = New System.Windows.Forms.Label()
        Me.txtBatchRejectPercentageThreshold = New System.Windows.Forms.TextBox()
        Me.txtMaxLinesInOutputFile = New System.Windows.Forms.TextBox()
        Me.rtxtErrorLog = New System.Windows.Forms.RichTextBox()
        Me.btnReconcile = New System.Windows.Forms.Button()
        Me.lblReadingFile = New System.Windows.Forms.Label()
        Me.btnViewLogFile = New System.Windows.Forms.Button()
        Me.btnViewOutputFile = New System.Windows.Forms.Button()
        Me.btnViewInputFile = New System.Windows.Forms.Button()
        Me.btnProcessFile = New System.Windows.Forms.Button()
        Me.ProgressBar = New System.Windows.Forms.ProgressBar()
        Me.btnViewOutputFil = New System.Windows.Forms.Button()
        Me.cboTrancheID = New System.Windows.Forms.ComboBox()
        Me.lblTrancheID = New System.Windows.Forms.Label()
        Me.dgvDebtLevels = New System.Windows.Forms.DataGridView()
        Me.colHOD = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMinimumDebt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colMaximumDebt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvDebtLevels, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblRejectionThreshold
        '
        Me.lblRejectionThreshold.AutoSize = True
        Me.lblRejectionThreshold.Location = New System.Drawing.Point(17, 232)
        Me.lblRejectionThreshold.Name = "lblRejectionThreshold"
        Me.lblRejectionThreshold.Size = New System.Drawing.Size(105, 13)
        Me.lblRejectionThreshold.TabIndex = 55
        Me.lblRejectionThreshold.Text = "Batch rejection perc:"
        Me.lblRejectionThreshold.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblOutputLines
        '
        Me.lblOutputLines.AutoSize = True
        Me.lblOutputLines.Location = New System.Drawing.Point(11, 206)
        Me.lblOutputLines.Name = "lblOutputLines"
        Me.lblOutputLines.Size = New System.Drawing.Size(111, 13)
        Me.lblOutputLines.TabIndex = 54
        Me.lblOutputLines.Text = "Max num lines per file:"
        Me.lblOutputLines.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtBatchRejectPercentageThreshold
        '
        Me.txtBatchRejectPercentageThreshold.Location = New System.Drawing.Point(127, 229)
        Me.txtBatchRejectPercentageThreshold.Name = "txtBatchRejectPercentageThreshold"
        Me.txtBatchRejectPercentageThreshold.Size = New System.Drawing.Size(77, 20)
        Me.txtBatchRejectPercentageThreshold.TabIndex = 51
        '
        'txtMaxLinesInOutputFile
        '
        Me.txtMaxLinesInOutputFile.Location = New System.Drawing.Point(127, 203)
        Me.txtMaxLinesInOutputFile.Name = "txtMaxLinesInOutputFile"
        Me.txtMaxLinesInOutputFile.Size = New System.Drawing.Size(77, 20)
        Me.txtMaxLinesInOutputFile.TabIndex = 50
        '
        'rtxtErrorLog
        '
        Me.rtxtErrorLog.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.rtxtErrorLog.Location = New System.Drawing.Point(210, 10)
        Me.rtxtErrorLog.Name = "rtxtErrorLog"
        Me.rtxtErrorLog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.rtxtErrorLog.Size = New System.Drawing.Size(764, 531)
        Me.rtxtErrorLog.TabIndex = 49
        Me.rtxtErrorLog.Text = ""
        '
        'btnReconcile
        '
        Me.btnReconcile.Location = New System.Drawing.Point(12, 460)
        Me.btnReconcile.Name = "btnReconcile"
        Me.btnReconcile.Size = New System.Drawing.Size(192, 42)
        Me.btnReconcile.TabIndex = 48
        Me.btnReconcile.Text = "&Reconcile"
        Me.btnReconcile.UseVisualStyleBackColor = True
        '
        'lblReadingFile
        '
        Me.lblReadingFile.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblReadingFile.BackColor = System.Drawing.Color.White
        Me.lblReadingFile.Location = New System.Drawing.Point(285, 552)
        Me.lblReadingFile.Name = "lblReadingFile"
        Me.lblReadingFile.Size = New System.Drawing.Size(416, 13)
        Me.lblReadingFile.TabIndex = 47
        Me.lblReadingFile.Text = "Reading file..."
        Me.lblReadingFile.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblReadingFile.Visible = False
        '
        'btnViewLogFile
        '
        Me.btnViewLogFile.Enabled = False
        Me.btnViewLogFile.Location = New System.Drawing.Point(12, 154)
        Me.btnViewLogFile.Name = "btnViewLogFile"
        Me.btnViewLogFile.Size = New System.Drawing.Size(192, 42)
        Me.btnViewLogFile.TabIndex = 46
        Me.btnViewLogFile.Text = "View &Log File"
        Me.btnViewLogFile.UseVisualStyleBackColor = True
        '
        'btnViewOutputFile
        '
        Me.btnViewOutputFile.Enabled = False
        Me.btnViewOutputFile.Location = New System.Drawing.Point(12, 106)
        Me.btnViewOutputFile.Name = "btnViewOutputFile"
        Me.btnViewOutputFile.Size = New System.Drawing.Size(192, 42)
        Me.btnViewOutputFile.TabIndex = 45
        Me.btnViewOutputFile.Text = "View &Output File"
        Me.btnViewOutputFile.UseVisualStyleBackColor = True
        '
        'btnViewInputFile
        '
        Me.btnViewInputFile.Enabled = False
        Me.btnViewInputFile.Location = New System.Drawing.Point(12, 58)
        Me.btnViewInputFile.Name = "btnViewInputFile"
        Me.btnViewInputFile.Size = New System.Drawing.Size(192, 42)
        Me.btnViewInputFile.TabIndex = 43
        Me.btnViewInputFile.Text = "View &Input File"
        Me.btnViewInputFile.UseVisualStyleBackColor = True
        '
        'btnProcessFile
        '
        Me.btnProcessFile.Location = New System.Drawing.Point(12, 10)
        Me.btnProcessFile.Name = "btnProcessFile"
        Me.btnProcessFile.Size = New System.Drawing.Size(192, 42)
        Me.btnProcessFile.TabIndex = 41
        Me.btnProcessFile.Text = "&Process File"
        Me.btnProcessFile.UseVisualStyleBackColor = True
        '
        'ProgressBar
        '
        Me.ProgressBar.AccessibleRole = System.Windows.Forms.AccessibleRole.Clock
        Me.ProgressBar.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ProgressBar.Location = New System.Drawing.Point(12, 547)
        Me.ProgressBar.Name = "ProgressBar"
        Me.ProgressBar.Size = New System.Drawing.Size(962, 22)
        Me.ProgressBar.TabIndex = 42
        '
        'btnViewOutputFil
        '
        Me.btnViewOutputFil.Enabled = False
        Me.btnViewOutputFil.Location = New System.Drawing.Point(12, 106)
        Me.btnViewOutputFil.Name = "btnViewOutputFil"
        Me.btnViewOutputFil.Size = New System.Drawing.Size(192, 42)
        Me.btnViewOutputFil.TabIndex = 44
        Me.btnViewOutputFil.Text = "View &Output File"
        Me.btnViewOutputFil.UseVisualStyleBackColor = True
        '
        'cboTrancheID
        '
        Me.cboTrancheID.FormattingEnabled = True
        Me.cboTrancheID.Location = New System.Drawing.Point(127, 509)
        Me.cboTrancheID.Name = "cboTrancheID"
        Me.cboTrancheID.Size = New System.Drawing.Size(77, 21)
        Me.cboTrancheID.TabIndex = 58
        '
        'lblTrancheID
        '
        Me.lblTrancheID.AutoSize = True
        Me.lblTrancheID.Location = New System.Drawing.Point(72, 512)
        Me.lblTrancheID.Name = "lblTrancheID"
        Me.lblTrancheID.Size = New System.Drawing.Size(50, 13)
        Me.lblTrancheID.TabIndex = 59
        Me.lblTrancheID.Text = "Tranche:"
        '
        'dgvDebtLevels
        '
        Me.dgvDebtLevels.AllowUserToAddRows = False
        Me.dgvDebtLevels.AllowUserToDeleteRows = False
        Me.dgvDebtLevels.AllowUserToResizeColumns = False
        Me.dgvDebtLevels.AllowUserToResizeRows = False
        Me.dgvDebtLevels.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDebtLevels.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvDebtLevels.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDebtLevels.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colHOD, Me.colMinimumDebt, Me.colMaximumDebt})
        Me.dgvDebtLevels.Location = New System.Drawing.Point(12, 255)
        Me.dgvDebtLevels.Name = "dgvDebtLevels"
        Me.dgvDebtLevels.RowHeadersVisible = False
        Me.dgvDebtLevels.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.dgvDebtLevels.Size = New System.Drawing.Size(192, 133)
        Me.dgvDebtLevels.TabIndex = 60
        '
        'colHOD
        '
        Me.colHOD.DataPropertyName = "HOD"
        Me.colHOD.HeaderText = "HOD"
        Me.colHOD.Name = "colHOD"
        Me.colHOD.ReadOnly = True
        Me.colHOD.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colHOD.Width = 50
        '
        'colMinimumDebt
        '
        Me.colMinimumDebt.DataPropertyName = "MinimumDebt"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle2.Format = "N2"
        Me.colMinimumDebt.DefaultCellStyle = DataGridViewCellStyle2
        Me.colMinimumDebt.HeaderText = "Minimum"
        Me.colMinimumDebt.Name = "colMinimumDebt"
        Me.colMinimumDebt.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colMinimumDebt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colMinimumDebt.Width = 70
        '
        'colMaximumDebt
        '
        Me.colMaximumDebt.DataPropertyName = "MaximumDebt"
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.colMaximumDebt.DefaultCellStyle = DataGridViewCellStyle3
        Me.colMaximumDebt.HeaderText = "Maximum"
        Me.colMaximumDebt.Name = "colMaximumDebt"
        Me.colMaximumDebt.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.colMaximumDebt.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        Me.colMaximumDebt.Width = 70
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 578)
        Me.Controls.Add(Me.dgvDebtLevels)
        Me.Controls.Add(Me.lblTrancheID)
        Me.Controls.Add(Me.cboTrancheID)
        Me.Controls.Add(Me.lblRejectionThreshold)
        Me.Controls.Add(Me.lblOutputLines)
        Me.Controls.Add(Me.txtBatchRejectPercentageThreshold)
        Me.Controls.Add(Me.txtMaxLinesInOutputFile)
        Me.Controls.Add(Me.rtxtErrorLog)
        Me.Controls.Add(Me.btnReconcile)
        Me.Controls.Add(Me.lblReadingFile)
        Me.Controls.Add(Me.btnViewLogFile)
        Me.Controls.Add(Me.btnViewOutputFile)
        Me.Controls.Add(Me.btnViewInputFile)
        Me.Controls.Add(Me.btnProcessFile)
        Me.Controls.Add(Me.ProgressBar)
        Me.Controls.Add(Me.btnViewOutputFil)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "HMRC Placement file preprocessor"
        CType(Me.dgvDebtLevels, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblRejectionThreshold As System.Windows.Forms.Label
    Friend WithEvents lblOutputLines As System.Windows.Forms.Label
    Friend WithEvents txtBatchRejectPercentageThreshold As System.Windows.Forms.TextBox
    Friend WithEvents txtMaxLinesInOutputFile As System.Windows.Forms.TextBox
    Friend WithEvents rtxtErrorLog As System.Windows.Forms.RichTextBox
    Friend WithEvents btnReconcile As System.Windows.Forms.Button
    Friend WithEvents lblReadingFile As System.Windows.Forms.Label
    Friend WithEvents btnViewLogFile As System.Windows.Forms.Button
    Friend WithEvents btnViewOutputFile As System.Windows.Forms.Button
    Friend WithEvents btnViewInputFile As System.Windows.Forms.Button
    Friend WithEvents btnProcessFile As System.Windows.Forms.Button
    Friend WithEvents ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents btnViewOutputFil As System.Windows.Forms.Button
    Friend WithEvents cboTrancheID As System.Windows.Forms.ComboBox
    Friend WithEvents lblTrancheID As System.Windows.Forms.Label
    Friend WithEvents dgvDebtLevels As System.Windows.Forms.DataGridView
    Friend WithEvents colHOD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colMinimumDebt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colMaximumDebt As System.Windows.Forms.DataGridViewTextBoxColumn

End Class

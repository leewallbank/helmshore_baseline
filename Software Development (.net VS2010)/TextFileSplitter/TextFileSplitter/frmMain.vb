﻿Imports System.IO
Imports commonlibrary

Public Class frmMain

    Private Sub btnSplitfile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSplitfile.Click
        Dim FileDialog As New OpenFileDialog
        Dim InputFilePath As String, FileName As String, FileExt As String
        Dim OutputLine As String = "", FileHeader As String = "", InputLine As String
        Dim LineNumber As Integer, FileCount As Integer = 1, RowCount As Integer = 0

        Try
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName) & "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            If chkHasHeader.Checked Then ' If and Else added TS 21/May/2013
                RowCount = -1
            Else
                RowCount = 0
            End If

            Dim sr As StreamReader = New StreamReader(FileDialog.FileName)

            ProgressBar.Style = ProgressBarStyle.Marquee

            Do While sr.Peek() >= 0
                RowCount += 1
                LineNumber += 1
                OutputLine = ""
                InputLine = sr.ReadLine

                If LineNumber = 1 And chkHasHeader.Checked Then FileHeader = InputLine

                Application.DoEvents() ' without this line, the button disappears until processing is complete

                If RowCount > CInt(txtNumRows.Text) Then ' Not equal so as not to include the header

                    FileCount += 1

                    If chkHasHeader.Checked Then ' If and Else added TS 21/May/2013
                        OutputLine = FileHeader & vbCrLf ' Add header again
                    Else
                        OutputLine = ""
                    End If
                    RowCount = 1
                End If

                OutputLine &= InputLine

                If RowCount < CInt(txtNumRows.Text) And sr.Peek >= 0 Then OutputLine &= vbCrLf ' add a new line unless at the end of the file

                AppendToFile(InputFilePath & FileName & "_File" & FileCount.ToString & FileExt, OutputLine)

            Loop

            sr.Close()

            ProgressBar.Style = ProgressBarStyle.Blocks

            MsgBox("Split complete" & vbCrLf & FileCount.ToString & " file" & If(FileCount > 1, "s", "") & " created" & vbCrLf & If(chkHasHeader.Checked, (LineNumber - 1).ToString, LineNumber.ToString) & " data lines", vbOKOnly, "Complete")

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

End Class

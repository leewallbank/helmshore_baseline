﻿Imports CommonLibrary

Public Class clsNottinghamData

    Public Function GetOutstandingClientBalance(ByVal ClientRef As String) As String
        ' Added TS 13/Sep/2013. Portal task ref 16885
        GetOutstandingClientBalance = GetSQLResults("DebtRecovery", "SELECT IFNULL(SUM(f.fee_amount - f.remited_fee),0) AS OutstandingClientBalance " & _
                                                                    "FROM debtor AS d " & _
                                                                    "INNER JOIN fee AS f ON d._rowID = f.DebtorID " & _
                                                                    "WHERE d.client_ref = '" & ClientRef & "' " & _
                                                                    "  AND d.status_open_closed = 'O' " & _
                                                                    "  AND f.fee_remit_col IN (1,2) " & _
                                                                    "  AND d.clientschemeID = 4380")

                                
    End Function

End Class


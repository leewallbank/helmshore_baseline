﻿Imports System.IO
Imports System.Collections
Imports System.Configuration
Imports CommonLibrary
Imports System.Globalization


Public Class frmMain
    Private NottinghamData As New clsNottinghamData
    'Private ConnID As String() = {"1894", "3479"} ' Standard CTax, Small balances
    Private ConnID As String() = {"4380", "4382"} ' Standard CTax, Small balances request 21512
    Private InputFilePath As String, FileName As String, FileExt As String
    Private OutputFiles As New ArrayList()
    Private NewCases As New ArrayList()
    Private Const DebtCol As Integer = 18
    Private Const Threshold As Decimal = 120

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        
    End Sub

    Private Sub btnProcessFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessFile.Click
        Dim FileDialog As New OpenFileDialog
        Dim ErrorLog As String = "", ExceptionLog As String = "", AuditLog As String = "", ConnNum As Integer, InputLineArray() As String, InputLineToCheckArray() As String, NewDebtFile(1) As String
        Dim NewDebtSumm(1, 1) As Decimal, TotalNewDebt As Decimal
        Dim RelatedDebt As Integer, LineNumber As Integer, LineNumberToCheck As Integer

        Try
            If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return

            InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
            InputFilePath &= "\"
            FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
            FileExt = Path.GetExtension(FileDialog.FileName)

            Dim FileContents() As String = System.IO.File.ReadAllLines(FileDialog.FileName)
            ProgressBar.Maximum = UBound(FileContents) + 3 ' log files and two output files

            For Each InputLine As String In FileContents
                ProgressBar.Value = LineNumber
                LineNumber += 1
                Application.DoEvents() ' without this line, the button disappears until processing is complete
                InputLineArray = InputLine.Split("|")

                Select LineNumber
                    Case 1, 2
                        ' skip the first two header lines
                        Continue For

                    Case UBound(FileContents) + 1
                        ' validate the footer
                        If InputLineArray(1) <> NewDebtSumm(0, 0) + NewDebtSumm(1, 0) Then ErrorLog &= "Footer contains " & InputLineArray(1) & " cases, pre processed " & (NewDebtSumm(0, 0) + NewDebtSumm(1, 0)).ToString & vbCrLf

                    Case Else
                        ' pre process the line

                        LineNumberToCheck = 0
                        RelatedDebt = 0
                        ConnNum = 0

                        If IsNumeric(InputLineArray(DebtCol)) Then ' DebtOriginal
                            If InputLineArray(18) <= Threshold Then ' No point in checking for related debt if the current debt is already over the threshold
                                For Each InputLineToCheck As String In FileContents
                                    LineNumberToCheck += 1
                                    InputLineToCheckArray = InputLineToCheck.Split("|")
                                    If InputLineArray(0) = InputLineToCheckArray(0) And LineNumber <> LineNumberToCheck Then RelatedDebt += InputLineToCheckArray(DebtCol)
                                Next InputLineToCheck

                                RelatedDebt += NottinghamData.GetOutstandingClientBalance(InputLineArray(0)) ' Added TS 9/Sep/2013. Portal task ref 16885

                                If InputLineArray(18) + RelatedDebt <= Threshold Then ConnNum = 1 ' If the debt on the current case and related cases does not exceed the threshold switch clientscheme
                            End If

                            NewDebtSumm(ConnNum, 1) += InputLineArray(DebtCol)
                        Else
                            ErrorLog &= "Cannot read debt amount at line number " & LineNumber.ToString & ". Value in file is '" & InputLineArray(DebtCol) & "'. Cannot check for related debt and cannot add to clientscheme " & ConnID(ConnNum) & " balance summary." & vbCrLf
                        End If

                        NewDebtFile(ConnNum) &= InputLine & vbCrLf
                        NewDebtSumm(ConnNum, 0) += 1

                End Select

            Next InputLine

            AuditLog = "File processed: " & InputFilePath & FileName & FileExt & vbCrLf
            AuditLog &= "Errors found: " & (ErrorLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= "Exceptions found: " & (ExceptionLog.Split(vbCrLf).Count - 1).ToString & vbCrLf
            AuditLog &= LineNumber.ToString & " lines processed at " & DateTime.Now.ToString("dd/MMM/yyyy HH:mm:ss") & vbCrLf & vbCrLf

            For NewDebtFileCount As Integer = 0 To UBound(NewDebtFile)

                ProgressBar.Value += 1

                If NewDebtFile(NewDebtFileCount) <> "" Then
                    WriteFile(InputFilePath & ConnID(NewDebtFileCount) & "_" & FileName & "_NewDebt.txt", NewDebtFile(NewDebtFileCount))
                    OutputFiles.Add(InputFilePath & ConnID(NewDebtFileCount) & "_" & FileName & "_NewDebt.txt")
                    AuditLog &= "Clientscheme " & ConnID(NewDebtFileCount) & vbCrLf
                    AuditLog &= "Number of new cases: " & NewDebtSumm(NewDebtFileCount, 0).ToString & vbCrLf
                    AuditLog &= "Total balance of new cases: " & NewDebtSumm(NewDebtFileCount, 1).ToString & vbCrLf & vbCrLf
                    TotalNewDebt += NewDebtSumm(NewDebtFileCount, 1)
                End If

            Next NewDebtFileCount

            ProgressBar.Value += 1

            WriteFile(InputFilePath & FileName & "_Audit.txt", AuditLog)
            If ErrorLog <> "" Then
                WriteFile(InputFilePath & FileName & "_Error.txt", ErrorLog)
                MsgBox("Errors found.", vbCritical, Me.Text)
            End If

            If ExceptionLog <> "" Then WriteFile(InputFilePath & FileName & "_Exception.txt", ExceptionLog)

            MsgBox("Preprocessing complete.", vbInformation + vbOKOnly, Me.Text)

            btnViewInputFile.Enabled = True
            btnViewOutputFile.Enabled = True
            btnViewLogFiles.Enabled = True

            ProgressBar.Value = 0

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
    End Sub

    Private Sub WriteFile(ByVal FileName As String, ByVal Content As String)
        Using Writer As StreamWriter = New StreamWriter(FileName)
            Writer.Write(Content)
        End Using
    End Sub

    Private Function ToNote(ByVal FreeText As String, ByVal NoteTag As String, Optional ByVal NoteLength As Integer = 250) As String()
        Dim Length As Integer = NoteLength - (NoteTag.Length + 2) ' + 2 is for :;
        Dim PreviousLength As Integer
        Dim Note As String() = New String(Math.Floor(FreeText.Length / NoteLength)) {} 'Use floor as arrays are zero based

        If FreeText = "" Then
            Note(0) = ""
        Else
            For NoteCount = 0 To UBound(Note)
                PreviousLength = NoteCount * Length
                Note(NoteCount) = (NoteTag & ":" & FreeText.Substring(PreviousLength, If((FreeText.Length - PreviousLength) < Length, FreeText.Length - PreviousLength, Length)) & ";").PadRight(NoteLength, " ")
            Next NoteCount
        End If

        Return Note
    End Function

    Private Sub btnViewInputFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewInputFile.Click
        Clipboard.SetText("notepad.exe " & InputFilePath & FileName & FileExt)
        If File.Exists(InputFilePath & FileName & FileExt) Then System.Diagnostics.Process.Start("wordpad.exe", InputFilePath & FileName & FileExt)
    End Sub

    Private Sub btnViewLogFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewLogFiles.Click
        If File.Exists(InputFilePath & FileName & "_Audit.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Audit.txt")
        If File.Exists(InputFilePath & FileName & "_Error.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Error.txt")
        If File.Exists(InputFilePath & FileName & "_Exception.txt") Then System.Diagnostics.Process.Start(InputFilePath & FileName & "_Exception.txt")
    End Sub

    Private Sub btnViewOutputFiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewOutputFile.Click
        For Each Filename As String In OutputFiles
            If File.Exists(Filename) Then System.Diagnostics.Process.Start(Filename)
        Next Filename
    End Sub
End Class

﻿Imports CommonLibrary
Public Class Form1

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim start_date As Date = CDate("Feb 1, 2012")
        'get multiple rows - datatable
        Dim dt As New DataTable
        Dim feeReport As String = "CaseID,Date case closed, Fee type, Recovered Date, Recovered Fee,Bailiff,Reason" & vbNewLine
        LoadDataTable("DebtRecovery", "SELECT _rowid, clientSchemeID, return_date " & _
                                                "FROM debtor " & _
                                                "WHERE _rowid= 6480168" & _
                                                " AND status_open_closed = 'C'" & _
                                                "AND return_date >= " & Format(start_date, "yyyy-MM-dd") & _
                                                " AND status = 'S'", dt, False)

        For Each row In dt.Rows
            Dim debtID As Integer = row.Item(0)
            Dim CSID As Integer = row.item(1)
            Dim returnDate As Date = row.item(2)
            'ignore test clients
            Dim Array As Object()
            Array = GetSQLResultsArray("DebtRecovery", "SELECT clientID, schemeID " & _
                                                    "FROM clientScheme " & _
                                                    "WHERE _rowid= " & CSID)
            Dim clientID As Integer = Array(0)
            If clientID = 1 Or clientID = 2 Or clientID = 24 Then Continue For

            'check scheme is CTAX
            Dim schemeID As Integer = Array(1)
            Array = GetSQLResultsArray("DebtRecovery", "SELECT work_type " & _
                                                    "FROM Scheme " & _
                                                    "WHERE _rowid= " & schemeID)
            Dim work_type As Integer = Array(0)
            If work_type <> 2 Then Continue For

            'now get fees
            Dim feedt As New DataTable
            LoadDataTable("DebtRecovery", "SELECT _rowid, type " & _
                                                    "FROM fee " & _
                                                    "WHERE debtorID= " & debtID & _
                                                    " AND fee_amount <> 0" & _
                                                    " AND fee_remit_col > 2" & _
                                                    " AND remited_fee <> 0", feedt, False)
            For Each feeRow In feedt.Rows
                Dim feeId As Integer = feeRow(0)
                Dim feeType As String = feeRow(1)
                Dim bailID As Integer
                Dim reason As String = ""
                'now get recovered fees
                Dim recovdt As New DataTable
                LoadDataTable("FeesSQL", "SELECT Amount, RemitedDate " & _
                                                        "FROM FeePayments " & _
                                                        "WHERE feeID = " & feeId, recovdt, False)
                For Each RecovRow In recovdt.Rows
                    Dim feeAmount As Decimal = RecovRow(0)
                    Dim remitedDate As Date = RecovRow(1)

                    Dim visitdt As New DataTable
                    LoadDataTable("DebtRecovery", "SELECT date_visited, bailiffID " & _
                                                            "FROM visit " & _
                                                            "WHERE debtorID= " & debtID & _
                                                            " Order by date_visited desc", visitdt, False)
                    For Each visitRow In visitdt.Rows
                        Dim visitDate As Date = visitRow(0)
                        If Format(visitDate, "yyyy-MM-dd") > Format(remitedDate, "yyyy-MM-dd") Then Continue For
                        Dim daysDiff As Integer = DateDiff(DateInterval.Day, visitDate, remitedDate)
                        'see if visit within 39 days of remitted date
                        If daysDiff < 39 Then
                            bailID = visitRow(1)
                            reason = "remitted within 39 days of visit"
                            feeReport &= debtID & "," & Format(returnDate, "dd/MM/yyyy") & "," & feeType & "," & Format(remitedDate, "dd/MM/yyyy") & "," & Format(feeAmount, "f") & "," & bailID & "," & reason & vbNewLine
                            Exit For
                        Else
                            'see if in arrangement at time of payment remit
                            Dim bailiffFound As Boolean = False
                            Dim notedt As New DataTable
                            LoadDataTable("DebtRecovery", "SELECT type, _createdDate " & _
                                                                    "FROM Note " & _
                                                                    "WHERE debtorID= " & debtID & _
                                                                     " AND (type = 'Arrangement' or type = 'Broken')" & _
                                                                    " Order by _rowid desc", notedt, False)
                            For Each noteRow In notedt.Rows
                                Dim noteDate As Date = noteRow(1)
                                If Format(noteDate, "yyyy-MM-dd") > Format(remitedDate, "yyyy-MM-dd") Then Continue For
                                Dim noteType As String = noteRow(0)
                                If noteType = "Broken" Then
                                    bailID = 0
                                    reason = "Broken Arrangement"
                                    bailiffFound = True
                                    feeReport &= debtID & "," & Format(returnDate, "dd/MM/yyyy") & "," & feeType & "," & Format(remitedDate, "dd/MM/yyyy") & "," & Format(feeAmount, "f") & "," & bailID & "," & reason & vbNewLine
                                    Exit For
                                End If
                                'see if bailiff visit within 10 days of arrangement set up
                                Dim visit2dt As New DataTable
                                LoadDataTable("DebtRecovery", "SELECT date_visited, bailiffID " & _
                                                                        "FROM visit " & _
                                                                        "WHERE debtorID= " & debtID & _
                                                                        " Order by date_visited desc", visit2dt, False)

                                For Each visit2Row In visit2dt.Rows
                                    Dim visit2Date As Date
                                    Try
                                        visit2Date = visit2Row(0)
                                    Catch ex As Exception
                                        Continue For
                                    End Try
                                    If Format(visit2Date, "yyyy-MM-dd") > Format(noteDate, "yyyy-MM-dd") Then Continue For
                                    bailID = visit2Row(1)
                                    Dim daysDiff2 As Integer = DateDiff(DateInterval.Day, visit2Date, noteDate)
                                    'arrangement set up within 10 days of visit
                                    If daysDiff2 < 11 Then
                                        'is remit within 39 days of arrangement set up
                                        Dim daysDiff3 As Integer = DateDiff(DateInterval.Day, noteDate, remitedDate)
                                        If daysDiff3 < 39 Then
                                            reason = "In Arrangement"
                                            bailiffFound = True
                                            feeReport &= debtID & "," & Format(returnDate, "dd/MM/yyyy") & "," & feeType & "," & Format(remitedDate, "dd/MM/yyyy") & "," & Format(feeAmount, "f") & "," & bailID & "," & reason & vbNewLine
                                        Else
                                            'check no phone calls between visit and payment
                                            Dim note2dt As New DataTable
                                            LoadDataTable("DebtRecovery", "SELECT _createdDate " & _
                                                                                    "FROM Note " & _
                                                                                    "WHERE debtorID= " & debtID & _
                                                                                     " AND type = 'Phone'" & _
                                                                                    " Order by _rowid desc", note2dt, False)
                                            Dim phoneFound As Boolean = False
                                            For Each note2Row In note2dt.Rows
                                                Dim note2Date As Date = note2Row(0)
                                                If Format(note2Date, "yyyy-MM-dd") > Format(remitedDate, "yyyy-MM-dd") Then Continue For
                                                If Format(note2Date, "yyyy-MM-dd") < Format(visit2Date, "yyyy-MM-dd") Then
                                                    Exit For
                                                End If
                                                phoneFound = True
                                                Exit For
                                            Next
                                            If phoneFound Then
                                                reason = "Phone call"
                                                bailiffFound = True
                                                bailID = 0
                                                feeReport &= debtID & "," & Format(returnDate, "dd/MM/yyyy") & "," & feeType & "," & Format(remitedDate, "dd/MM/yyyy") & "," & Format(feeAmount, "f") & "," & bailID & "," & reason & vbNewLine
                                            Else
                                                reason = "In Arrangement cont"
                                                bailiffFound = True
                                                feeReport &= debtID & "," & Format(returnDate, "dd/MM/yyyy") & "," & feeType & "," & Format(remitedDate, "dd/MM/yyyy") & "," & Format(feeAmount, "f") & "," & bailID & "," & reason & vbNewLine
                                            End If
                                        End If
                                        Exit For
                                    End If
                                Next
                                If bailiffFound Then
                                    Exit For
                                End If
                            Next
                            If Not bailiffFound Then
                                bailID = 0
                                reason = "Not in Arrangement"
                                bailiffFound = True
                                feeReport &= debtID & "," & Format(returnDate, "dd/MM/yyyy") & "," & feeType & "," & Format(remitedDate, "dd/MM/yyyy") & "," & Format(feeAmount, "f") & "," & bailID & "," & reason & vbNewLine
                            End If
                        End If
                        Exit For
                    Next
                    
                Next
            Next
        Next
        WriteFile("H:\temp\Recovered_fees.txt", feeReport)
        MsgBox("Completed")
        Me.Close()
    End Sub
End Class

﻿Imports CommonLibrary
Imports System.IO
Public Class Form1

    Private InputFilePath As String, FileName As String, FileExt As String
    Private Sub readbtn_Click(sender As System.Object, e As System.EventArgs) Handles readbtn.Click
        Dim FileDialog As New OpenFileDialog
        Dim OutputFile As String = ""
        If Not FileDialog.ShowDialog() = System.Windows.Forms.DialogResult.OK Then Return
        exitbtn.Enabled = False
        readbtn.Enabled = False
        Dim progressbar As New ProgressBar
        

        InputFilePath = Path.GetDirectoryName(FileDialog.FileName)
        InputFilePath &= "\"
        FileName = Path.GetFileNameWithoutExtension(FileDialog.FileName)
        FileExt = Path.GetExtension(FileDialog.FileName)

        Dim excel_file_contents(,) As String = InputFromExcel(FileDialog.FileName)

        For rowIDX = 0 To UBound(excel_file_contents)
            'write out row
            For colIdx = 0 To 146
                Dim field As String = excel_file_contents(rowIDX, colIdx)
                field = Replace(field, ",", " ")
                Select Case colIdx
                    Case 0, 8, 9, 12, 13, 14, 15, 16, 17, 18, 36, 37, 38, 43, 46, 47, 48, 49, 51, 52, 53, 54, 55, 56, 57, 62, 65, 70, 84, 129, 132
                        OutputFile &= field & ","
                End Select
            Next
            OutputFile &= vbNewLine
            Dim debtorID As Integer
            Try
                debtorID = excel_file_contents(rowIDX, 0)
            Catch ex As Exception
                Continue For
            End Try

            'get any open links
            Dim debtdt As New DataTable
            LoadDataTable("DebtRecovery", "select D2._rowID from debtor D, debtor D2 " & _
                          " where D.linkID = D2.linkID " & _
                          " and D2.status_open_closed = 'O'" & _
                          " and D._rowID = " & debtorID & _
                          " and D._rowID <> D2._rowID", debtdt, False)
            For Each debtRow In debtdt.Rows
                Dim linkDebtorID As Integer = debtRow(0)
                'write out a new line
                OutputFile &= linkDebtorID & ","
                For colIdx = 1 To 146
                    Dim field As String = excel_file_contents(rowIDX, colIdx)
                    field = Replace(field, ",", " ")
                    Select Case colIdx
                        Case 0, 8, 9, 12, 13, 14, 15, 16, 17, 18, 36, 37, 38, 43, 46, 47, 48, 49, 51, 52, 53, 54, 55, 56, 57, 62, 65, 70, 84, 129, 132
                            OutputFile &= field & ","
                    End Select
                Next
                OutputFile &= vbNewLine
            Next
        Next
        Dim outFileName As String = InputFilePath & "Trace_Report.csv"
        My.Computer.FileSystem.WriteAllText(outFileName, OutputFile, False)
        MsgBox("Report Created")
        Me.Close()
    End Sub
End Class

﻿Imports CommonLibrary

Public Class frmCaseSummary
    Private Map As frmMap
    Private SummaryData As New clsCaseSummaryData
    Private ListData As New clsListData
    ' Private Map As frmMap
    ' These cannot be instantiated here as the datagridviews have not been instantiated at this point but they need to be declared here to achieve the right scope...

    Private StageGridState As clsGridState
    Private ClientGridState As clsGridState
    Private WorkTypeGridState As clsGridState
    Private SchemeGridState As clsGridState
    Private AllocatedGridState As clsGridState
    Private PaymentGridState As clsGridState
    Private LeviedGridState As clsGridState
    Private VanFeesAppliedGridState As clsGridState
    Private VisitedGridState As clsGridState
    Private AddConfirmedGridState As clsGridState
    Private CoveredGridState As clsGridState
    Private PostcodeAreaGridState As clsGridState
    Private ArrangementBrokenGridState As clsGridState
    Private DebtYearGridState As clsGridState
    Private LinkedPIFGridState As clsGridState

    Private ColSort As String

    Private ParamList As String = "null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,0,2" ' i.e. top level changed the default to 2 for PeriodType

    'Private RefreshDBClicked As Boolean = False
    'Private RefreshRunningMsgShown As Boolean = False

#Region "New and open"

    Public Sub New()
        Try
            InitializeComponent()

            SetFormIcon(Me)

            ' Now instantiate these
            '   Map = New frmMap(dgvSummary)
            StageGridState = New clsGridState(dgvStageName)
            ClientGridState = New clsGridState(dgvClientName)
            WorkTypeGridState = New clsGridState(dgvWorkType)
            SchemeGridState = New clsGridState(dgvSchemeName)
            AllocatedGridState = New clsGridState(dgvAllocated)
            PaymentGridState = New clsGridState(dgvPayment)
            LeviedGridState = New clsGridState(dgvLevied)
            VanFeesAppliedGridState = New clsGridState(dgvVanFeesApplied)
            VisitedGridState = New clsGridState(dgvVisited)
            AddConfirmedGridState = New clsGridState(dgvAddConfirmed)
            CoveredGridState = New clsGridState(dgvCovered)
            PostcodeAreaGridState = New clsGridState(dgvPostcodeArea)
            ArrangementBrokenGridState = New clsGridState(dgvArrangementBroken)
            DebtYearGridState = New clsGridState(dgvDebtYear)
            LinkedPIFGridState = New clsGridState(dgvLinkedPIF)

            AddControlHandlers()

            cmsSummary.Items.Add("View Cases")
            cmsSummary.Items.Add("Map Cases")
            cmsSummary.Items.Add("Copy")
            cmsSummary.Items.Add("Select All")

            cmsList.Items.Add("Consortiums...")
            cmsList.Items.Add("Set warning levels")
            cmsList.Items.Add("Copy")
            cmsList.Items.Add("Select All")

            cmsForm.Items.Add("Bailiff View")

            SummaryData.GetConsortiums()
            Dim Consortium As ToolStripMenuItem = Convert.ChangeType(cmsList.Items(0), GetType(ToolStripMenuItem))

            For Each Row As DataRow In SummaryData.ConsortiumDataView.ToTable(True, "Consortium").Rows
                Dim SubMenuItem As ToolStripMenuItem = New ToolStripMenuItem(Row.Item(0).ToString)

                AddHandler SubMenuItem.Click, AddressOf ConsortiumContextSubMenu_Click
                Consortium.DropDownItems.Add(SubMenuItem)
            Next Row

            SummaryData.GetSummary(ParamList, True, "P")
            dgvSummary.DataSource = SummaryData.SummaryDataView

            FormatGridColumns(dgvSummary, GetStages)

            ListData.GetCaseList(ParamList)
            SetListGrids()
            FormatListColumns()

            cboPeriodType.DataSource = SummaryData.PeriodTypeDataView
            cboPeriodType.ValueMember = "PeriodTypeID"
            cboPeriodType.DisplayMember = "PeriodTypeDesc"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmSummary_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            RemoveSelectionHandlers()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmCaseSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        Try
            If e.Button = MouseButtons.Right Then
                cmsForm.Show(sender, e.Location)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmSummary_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            HighlightWarnings()
            SetSelections() 'Arrays will be nothing so clear all selections - by default the first row in a datagridview is always selected
            dgvSummary.ClearSelection()

            radAbs.Checked = True
            chkTopClients.Checked = False
            cboPeriodType.SelectedValue = 2 ' changed from 1 TS 18/Dec/2013
            cboDisplaySet.Text = "Periods"

            AddHandler radAbs.CheckedChanged, AddressOf radAbs_CheckedChanged
            AddHandler chkTopClients.CheckedChanged, AddressOf chkTopClients_CheckedChanged

            AddSelectionHandlers()

            SetcmdRefreshTip()

            ' This is duplicated from FormatListColumns as that sub runs before the form is shown
            For Each Row As DataGridViewRow In dgvClientName.Rows
                SummaryData.UnderAllocatedClientDataView.RowFilter = "ClientName = '" & Row.Cells("ClientName").Value & "'"
                If SummaryData.UnderAllocatedClientDataView.Count > 0 Then
                    Row.DefaultCellStyle.ForeColor = Color.Red
                Else
                    Row.DefaultCellStyle.ForeColor = Color.Black
                End If
            Next Row

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "Refresh"

    Private Sub RefreshGrid()
        Try
            ParamList = GetParam(dgvStageName, "StageName") & ","
            ParamList += GetParam(dgvClientName, "ClientName") & ","
            ParamList += GetParam(dgvWorkType, "WorkType") & ","
            ParamList += GetParam(dgvSchemeName, "SchemeName") & ","
            ParamList += GetParam(dgvAllocated, "Allocated") & ","
            ParamList += GetParam(dgvLevied, "Levied") & ","
            ParamList += GetParam(dgvVanFeesApplied, "VanFeesApplied") & ","
            ParamList += GetParam(dgvPayment, "Payment") & ","
            ParamList += GetParam(dgvVisited, "Visited") & ","
            ParamList += GetParam(dgvAddConfirmed, "AddConfirmed") & ","
            ParamList += GetParam(dgvCovered, "Covered") & ","
            ParamList += GetParam(dgvPostcodeArea, "PostcodeArea") & ","
            ParamList += GetParam(dgvArrangementBroken, "ArrangementBroken") & ","
            ParamList += GetParam(dgvDebtYear, "DebtYear") & ","
            ParamList += GetParam(dgvLinkedPIF, "LinkedPIF") & ","

            ParamList += chkTopClients.Checked.ToString
            ParamList += "," & cboPeriodType.SelectedValue.ToString

            PreRefresh(Me)

            RemoveSelectionHandlers()

            GetSelections()

            SummaryData.GetSummary(ParamList, radAbs.Checked, cboDisplaySet.Text.Substring(0, 1))
            dgvSummary.DataSource = SummaryData.SummaryDataView

            FormatGridColumns(dgvSummary, GetStages)
            HighlightWarnings()

            ListData.GetCaseList(ParamList)
            SetListGrids()
            FormatListColumns()

            SetSelections()

            AddSelectionHandlers()

            PostRefresh(Me)

            dgvSummary.ClearSelection() ' The first cell always get selected

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function GetParam(ByRef DataGrid As DataGridView, ByVal ColumnName As String) As String
        GetParam = Nothing

        Try
            ' Used when selection criteria change
            Dim Param As String = Nothing

            For Each dr As DataGridViewRow In DataGrid.SelectedRows
                If Not IsNothing(Param) Then Param += vbTab
                Param += dr.Cells(ColumnName).Value
            Next dr

            If IsNothing(Param) Then
                Param = "null"
            Else
                Param = "'" & Param.Replace("'", "''''") & "'" ' Portal task ref 16716
            End If

            Return Param

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function GetParam(ByVal ParamColumn As String, ByRef Cell As DataGridViewCell, ByRef ListDataGridView As DataGridView)
        GetParam = Nothing

        Try
            ' Used when detail for a particular cell in the summary grid is retrieved
            Dim Param As String = ""

            If IsNothing(Cell.DataGridView.Columns(ParamColumn)) Then
                ' The column may not be present as only one criteria is applicable
                If ListDataGridView.SelectedRows.Count = 1 Then
                    ' Param = "'" & ListDataGridView.SelectedRows(0).Cells(0).Value & "'"
                    Param = "'" & ListDataGridView.SelectedRows(0).Cells(0).Value.Replace("'", "''''") & "'" ' Portal task ref 16716
                Else
                    Param = "null"
                End If
            Else
                Param = "'" & Cell.DataGridView.Item(ParamColumn, Cell.RowIndex).Value.Replace("'", "''''") & "'" ' Portal task ref 16716
            End If

            Return Param

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function
#End Region

#Region "Grid selection events"

    Private Sub cboPeriodType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboPeriodType.SelectedValueChanged
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboDisplaySet_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboDisplaySet.SelectedValueChanged
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ' This is needed to make mousewheel scroll list items
            sender.Select()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                GetSelections()
                RemoveSelectionHandlers()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                SetSelections()
                AddSelectionHandlers()
                HighlightUnderAllocation()
            End If

            If e.Button = MouseButtons.Right Then
                If sender.name <> "dgvClientName" And sender.name <> "dgvSchemeName" Then cmsList.Items(0).Visible = False Else cmsList.Items(0).Visible = True
                If sender.name <> "dgvClientName" Then cmsList.Items(1).Visible = False Else cmsList.Items(1).Visible = True
                cmsList.Show(sender, e.Location)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvSummary.DataBindingComplete
        Try
            ' This needs to be handled differently. I'm not sure why but doing this later causes events to fire. Recommended practice on the following url
            ' http://msdn.microsoft.com/en-us/library/system.windows.forms.datagridviewcolumn.visible.aspx

            If dgvSummary.Columns.Contains("RowID") Then dgvSummary.Columns("RowID").Visible = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseDown
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
                dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected = True
            End If

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                ColSort = SummaryData.SummaryDataView.Sort
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseUp
        Try
            If e.Button = MouseButtons.Right Then cmsSummary.Show(dgvSummary, e.Location)

            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                If ColSort <> "" Then SummaryData.SummaryDataView.Sort += "," & ColSort
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSummary.SelectionChanged
        Try
            Dim TotalCases As Integer = 0

            If dgvSummary.SelectedCells.Count = 0 Or radAbs.Checked = False Then
                lblSummary.Text = ""
            Else
                For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                    If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then TotalCases += Cell.Value
                Next Cell
                lblSummary.Text = TotalCases.ToString & " case" & If(TotalCases > 1, "s", "")
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ShowDetail()
        Try
            Dim DetailParamList As String
            Dim Detail As New diaCaseDetail(Me)

            PreRefresh(Me)

            For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then
                    DetailParamList = GetParam("StageName", Cell, dgvStageName) & ","
                    DetailParamList += GetParam("ClientName", Cell, dgvClientName) & ","
                    DetailParamList += GetParam("WorkType", Cell, dgvWorkType) & ","
                    DetailParamList += GetParam("SchemeName", Cell, dgvSchemeName) & ","
                    DetailParamList += GetParam("Allocated", Cell, dgvAllocated) & ","
                    DetailParamList += GetParam("Levied", Cell, dgvLevied) & ","
                    DetailParamList += GetParam("VanFeesApplied", Cell, dgvVanFeesApplied) & ","
                    DetailParamList += GetParam("Payment", Cell, dgvPayment) & ","
                    DetailParamList += GetParam("Visited", Cell, dgvVisited) & ","
                    DetailParamList += GetParam("AddConfirmed", Cell, dgvAddConfirmed) & ","
                    DetailParamList += GetParam("Covered", Cell, dgvCovered) & ","
                    DetailParamList += GetParam("PostcodeArea", Cell, dgvPostcodeArea) & ","
                    DetailParamList += GetParam("ArrangementBroken", Cell, dgvArrangementBroken) & ","
                    DetailParamList += GetParam("DebtYear", Cell, dgvDebtYear) & ","
                    DetailParamList += GetParam("LinkedPIF", Cell, dgvLinkedPIF) & ","

                    DetailParamList += chkTopClients.Checked.ToString

                    DetailParamList += "," & cboPeriodType.SelectedValue.ToString

                    Detail.AddCases(DetailParamList, dgvSummary.Columns(Cell.ColumnIndex).Name)

                End If
            Next Cell

            PostRefresh(Me)

            If Detail.CaseCount > 0 Then Detail.ShowDialog() ' Portal task ref 16716
            Detail.Dispose()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ShowMap()
        Try
            Dim DetailParamList As String

            PreRefresh(Me)

            If IsNothing(map) OrElse map.IsDisposed Then map = New frmMap(dgvSummary)

            For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then
                    DetailParamList = GetParam("StageName", Cell, dgvStageName) & ","
                    DetailParamList += GetParam("ClientName", Cell, dgvClientName) & ","
                    DetailParamList += GetParam("WorkType", Cell, dgvWorkType) & ","
                    DetailParamList += GetParam("SchemeName", Cell, dgvSchemeName) & ","
                    DetailParamList += GetParam("Allocated", Cell, dgvAllocated) & ","
                    DetailParamList += GetParam("Levied", Cell, dgvLevied) & ","
                    DetailParamList += GetParam("VanFeesApplied", Cell, dgvVanFeesApplied) & ","
                    DetailParamList += GetParam("Payment", Cell, dgvPayment) & ","
                    DetailParamList += GetParam("Visited", Cell, dgvVisited) & ","
                    DetailParamList += GetParam("AddConfirmed", Cell, dgvAddConfirmed) & ","
                    DetailParamList += GetParam("Covered", Cell, dgvCovered) & ","
                    DetailParamList += GetParam("PostcodeArea", Cell, dgvPostcodeArea) & ","
                    DetailParamList += GetParam("ArrangementBroken", Cell, dgvArrangementBroken) & ","
                    DetailParamList += GetParam("DebtYear", Cell, dgvDebtYear) & ","
                    DetailParamList += GetParam("LinkedPIF", Cell, dgvLinkedPIF) & ","

                    DetailParamList += chkTopClients.Checked.ToString

                    DetailParamList += "," & cboPeriodType.SelectedValue.ToString

                    Map.AddCases(DetailParamList, dgvSummary.Columns(Cell.ColumnIndex).Name)

                End If
            Next Cell

            ' Get the cases added ready to plot - this is where the different datasets are named
            map.GetCasesForMap()

            PostRefresh(Me)

            Map.Show()
            '   Map = Nothing

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "'All' and 'Clear' button click events"

    Private Sub cmdStageAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageAll.Click
        Try
            dgvStageName.SelectAll()
            StageGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdStageClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageClear.Click
        Try
            dgvStageName.ClearSelection()
            StageGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientAll.Click
        Try
            dgvClientName.SelectAll()
            ClientGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientClear.Click
        Try
            dgvClientName.ClearSelection()
            ClientGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdWorkTypeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWorkTypeAll.Click
        Try
            dgvWorkType.SelectAll()
            WorkTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdWorkTypeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWorkTypeClear.Click
        Try
            dgvWorkType.ClearSelection()
            WorkTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdSchemeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeAll.Click
        Try
            dgvSchemeName.SelectAll()
            SchemeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdSchemeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeClear.Click
        Try
            dgvSchemeName.ClearSelection()
            SchemeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAllocatedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAllocatedAll.Click
        Try
            dgvAllocated.SelectAll()
            AllocatedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAllocatedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAllocatedClear.Click
        Try
            dgvAllocated.ClearSelection()
            AllocatedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLeviedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLeviedAll.Click
        Try
            dgvLevied.SelectAll()
            LeviedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLeviedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLeviedClear.Click
        Try
            dgvLevied.ClearSelection()
            LeviedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdVanFeesAppliedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVanFeesAppliedAll.Click
        Try
            dgvVanFeesApplied.SelectAll()
            VanFeesAppliedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdVanFeesAppliedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVanFeesAppliedClear.Click
        Try
            dgvVanFeesApplied.ClearSelection()
            VanFeesAppliedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPaymentAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPaymentAll.Click
        Try
            dgvPayment.SelectAll()
            PaymentGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPaymentClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPaymentClear.Click
        Try
            dgvPayment.ClearSelection()
            PaymentGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdVisitedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVisitedAll.Click
        Try
            dgvVisited.SelectAll()
            VisitedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdVisitedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdVisitedClear.Click
        Try
            dgvVisited.ClearSelection()
            VisitedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAddConfirmedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfirmedAll.Click
        Try
            dgvAddConfirmed.SelectAll()
            AddConfirmedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAddConfirmedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfirmedClear.Click
        Try
            dgvAddConfirmed.ClearSelection()
            AddConfirmedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCoveredAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCoveredAll.Click
        Try
            dgvCovered.SelectAll()
            CoveredGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCoveredClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCoveredClear.Click
        Try
            dgvCovered.ClearSelection()
            CoveredGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeAreaAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaAll.Click
        Try
            dgvPostcodeArea.SelectAll()
            PostcodeAreaGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeAreaClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaClear.Click
        Try
            dgvPostcodeArea.ClearSelection()
            PostcodeAreaGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdArrangementBrokenAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdArrangementBrokenAll.Click
        Try
            dgvArrangementBroken.SelectAll()
            ArrangementBrokenGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdArrangementBrokenClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdArrangementBrokenClear.Click
        Try
            dgvArrangementBroken.ClearSelection()
            ArrangementBrokenGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdDebtYearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDebtYearAll.Click
        Try
            dgvDebtYear.SelectAll()
            DebtYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdDebtYearClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDebtYearClear.Click
        Try
            dgvDebtYear.ClearSelection()
            DebtYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLinkedPIFAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLinkedPIFAll.Click
        Try
            dgvLinkedPIF.SelectAll()
            LinkedPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLinkedPIFClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLinkedPIFClear.Click
        Try
            dgvLinkedPIF.ClearSelection()
            LinkedPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "Handlers"

    Private Sub RemoveSelectionHandlers()
        Try
            ' Need to find a way to do these at once but these SelectionChanged event is specific to the datagridview class and not control - the collection on the form
            RemoveHandler dgvStageName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvWorkType.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvAllocated.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvLevied.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvVanFeesApplied.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPayment.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvVisited.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvAddConfirmed.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvCovered.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvArrangementBroken.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvDebtYear.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvLinkedPIF.SelectionChanged, AddressOf dgvSelectionChanged

            RemoveHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
            RemoveHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddSelectionHandlers()
        Try
            AddHandler dgvStageName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvWorkType.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvAllocated.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvLevied.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvVanFeesApplied.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPayment.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvVisited.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvAddConfirmed.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvCovered.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvArrangementBroken.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvDebtYear.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvLinkedPIF.SelectionChanged, AddressOf dgvSelectionChanged

            AddHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
            AddHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddControlHandlers()
        Try
            ' All these grids and buttons call the same events so no point declaring them all separately
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    AddHandler Control.MouseDown, AddressOf dgvMouseDown
                    AddHandler Control.MouseUp, AddressOf dgvMouseUp
                    AddHandler Control.MouseEnter, AddressOf dgvMouseEnter
                    'AddHandler Control.MouseHover, AddressOf dgvMouseHover
                ElseIf TypeOf Control Is ComboBox Or TypeOf Control Is Button Or TypeOf Control Is RadioButton Or TypeOf Control Is CheckBox Then
                    'AddHandler Control.MouseHover, AddressOf dgvMouseHover
                End If
            Next Control

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "DataGridView methods"

    Private Sub SetListGrids()
        Try
            dgvStageName.DataSource = ListData.StageName
            StageGridState.SetSort()

            dgvClientName.DataSource = ListData.ClientName
            ClientGridState.SetSort()

            dgvWorkType.DataSource = ListData.WorkType
            WorkTypeGridState.SetSort()

            dgvSchemeName.DataSource = ListData.SchemeName
            SchemeGridState.SetSort()

            dgvAllocated.DataSource = ListData.Allocated
            AllocatedGridState.SetSort()

            dgvLevied.DataSource = ListData.Levied
            LeviedGridState.SetSort()

            dgvVanFeesApplied.DataSource = ListData.VanFeesApplied
            VanFeesAppliedGridState.SetSort()

            dgvPayment.DataSource = ListData.Payment
            PaymentGridState.SetSort()

            dgvVisited.DataSource = ListData.Visited
            VisitedGridState.SetSort()

            dgvAddConfirmed.DataSource = ListData.AddConfirmed
            AddConfirmedGridState.SetSort()

            dgvCovered.DataSource = ListData.Covered
            CoveredGridState.SetSort()

            dgvPostcodeArea.DataSource = ListData.PostcodeArea
            PostcodeAreaGridState.SetSort()

            dgvArrangementBroken.DataSource = ListData.ArrangementBroken
            ArrangementBrokenGridState.SetSort()

            dgvDebtYear.DataSource = ListData.DebtYear
            DebtYearGridState.SetSort()

            dgvLinkedPIF.DataSource = ListData.LinkedPIF
            LinkedPIFGridState.SetSort()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub FormatListColumns()
        Try
            ' This is slightly different to settings in modCommon as we want some of the fields wider than they need to be for presentation
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    If Control.Name <> "dgvSummary" Then ' This is not a list grid
                        For Each Column In CType(Control, DataGridView).Columns
                            Select Case Column.Name
                                Case "Total"
                                    Column.Width = 40
                                Case "StageName"
                                    Column.Width = 100
                                    Column.HeaderText = "Stage"
                                Case "ClientName"
                                    Column.Width = 100
                                    Column.HeaderText = "Client"
                                Case "WorkType"
                                    Column.Width = 84
                                    Column.HeaderText = "Work Type"
                                Case "LinkedPIF"
                                    Column.Width = 84
                                    Column.HeaderText = "Linked PIF"
                                Case "SchemeName"
                                    Column.Width = 100
                                    Column.HeaderText = "Scheme"
                                Case "Allocated"
                                    Column.Width = 84
                                Case "Levied"
                                    Column.Width = 90
                                Case "VanFeesApplied"
                                    Column.Width = 120
                                    Column.HeaderText = "Van Fees"
                                Case "Payment"
                                    Column.Width = 70
                                Case "Visited"
                                    Column.Width = 70
                                Case "AddConfirmed"
                                    Column.Width = 70
                                    Column.HeaderText = "Confirmed"
                                Case "ArrangementBroken"
                                    Column.Width = 90
                                    Column.HeaderText = "Broken"
                                Case "Covered"
                                    Column.Width = 90
                                Case "PostcodeArea"
                                    Column.Width = 55
                                    Column.HeaderText = "Area"
                                Case "DebtYear"
                                    Column.Width = 55
                                    Column.HeaderText = "Year"
                                Case Else
                                    Column.Width = 40
                            End Select
                        Next Column
                    End If
                End If
            Next Control

            SummaryData.GetUnderAllocatedClients()

            HighlightUnderAllocation()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub HighlightUnderAllocation()
        Try
            For Each Row As DataGridViewRow In dgvClientName.Rows
                SummaryData.UnderAllocatedClientDataView.RowFilter = "ClientName = '" & Row.Cells("ClientName").Value & "'"
                If SummaryData.UnderAllocatedClientDataView.Count > 0 Then
                    Row.DefaultCellStyle.ForeColor = Color.Red
                Else
                    Row.DefaultCellStyle.ForeColor = Color.Black
                End If
            Next Row

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub HighlightWarnings()
        Try
            Dim RowIndex As Integer

            For Each DataRow As DataRow In SummaryData.WarningSummaryDataView.ToTable.Rows
                ' Find the row with warnings on the datagridview
                For Each GridRow As DataGridViewRow In dgvSummary.Rows
                    If GridRow.Cells("RowID").Value = DataRow.Item("RowID") Then RowIndex = GridRow.Index
                Next GridRow

                ' Loop through periods and highlight if any warnings exist
                For Each Item As DataColumn In DataRow.Table.Columns
                    If DataColumnsList.Contains(Item.ColumnName) AndAlso DataRow.Item(Item.ColumnName) > 0 Then
                        dgvSummary.Rows(RowIndex).Cells(Item.ColumnName).Style.ForeColor = Color.Red
                    End If
                Next Item
            Next DataRow

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub GetSelections()
        Try
            StageGridState.GetState()
            ClientGridState.GetState()
            WorkTypeGridState.GetState()
            SchemeGridState.GetState()
            AllocatedGridState.GetState()
            LeviedGridState.GetState()
            VanFeesAppliedGridState.GetState()
            PaymentGridState.GetState()
            VisitedGridState.GetState()
            AddConfirmedGridState.GetState()
            CoveredGridState.GetState()
            PostcodeAreaGridState.GetState()
            ArrangementBrokenGridState.GetState()
            DebtYearGridState.GetState()
            LinkedPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetSelections()
        Try
            StageGridState.SetState()
            ClientGridState.SetState()
            WorkTypeGridState.SetState()
            SchemeGridState.SetState()
            AllocatedGridState.SetState()
            LeviedGridState.SetState()
            VanFeesAppliedGridState.SetState()
            PaymentGridState.SetState()
            VisitedGridState.SetState()
            AddConfirmedGridState.SetState()
            CoveredGridState.SetState()
            PostcodeAreaGridState.SetState()
            ArrangementBrokenGridState.SetState()
            DebtYearGridState.SetState()
            LinkedPIFGridState.SetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Additional functionality"

    Private Sub cmdClearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClearAll.Click
        Try
            RemoveSelectionHandlers()

            cmdStageClear_Click(sender, New System.EventArgs)
            cmdClientClear_Click(sender, New System.EventArgs)
            cmdWorkTypeClear_Click(sender, New System.EventArgs)
            cmdSchemeClear_Click(sender, New System.EventArgs)
            cmdAllocatedClear_Click(sender, New System.EventArgs)
            cmdLeviedClear_Click(sender, New System.EventArgs)
            cmdVanFeesAppliedClear_Click(sender, New System.EventArgs)
            cmdPaymentClear_Click(sender, New System.EventArgs)
            cmdVisitedClear_Click(sender, New System.EventArgs)
            cmdAddConfirmedClear_Click(sender, New System.EventArgs)
            cmdCoveredClear_Click(sender, New System.EventArgs)
            cmdPostcodeAreaClear_Click(sender, New System.EventArgs)
            cmdArrangementBrokenClear_Click(sender, New System.EventArgs)
            cmdDebtYearClear_Click(sender, New System.EventArgs)
            cmdLinkedPIFClear_Click(sender, New System.EventArgs)

            AddSelectionHandlers()

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radAbs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles radAbs.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radPerc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPerc.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub chkTopClients_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles chkTopClients.CheckedChanged
        Try
            RefreshScroll = False

            If chkTopClients.Checked Then
                ' Not ideal but we need to refresh the client list before the main refresh to exclude any clients selected that would not be in the top list
                RemoveSelectionHandlers()

                GetSelections()
                ListData.GetClientList(ParamList)
                SetSelections()

                AddSelectionHandlers()
            End If

            RefreshGrid()

            RefreshScroll = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.Click
        Try
            If MsgBox("This could take several minutes, are you sure?", vbYesNo + vbDefaultButton2 + vbQuestion) = vbYes Then
                'RefreshDBClicked = True
                PreRefresh(Me)
                SummaryData.RefreshDatabase()
                RefreshGrid()
                PostRefresh(Me)
                MsgBox("Refresh complete.", vbOKOnly + vbInformation)
                SetcmdRefreshTip()
                'RefreshDBClicked = False
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.MouseLeave
        Try
            ' on XP machines there is a bug that prevents the tooltip showing a second time. This addresses the problem.
            ToolTip1.Active = False
            ToolTip1.Active = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetcmdRefreshTip()
        Try
            SummaryData.GetLastLoad()
            ToolTip1.SetToolTip(cmdRefreshDB, "Last refreshed at " & SummaryData.LastLoadDataView.Item(0).Item(0).ToString & " by " & SummaryData.LastLoadDataView.Item(0).Item(1).ToString)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsSummary_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsSummary.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "View Cases"
                    ShowDetail()
                Case "Map Cases"
                    ShowMap()
                Case "Copy"
                    dgvSummary.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    Clipboard.SetDataObject(dgvSummary.GetClipboardContent())
                Case "Select All"
                    dgvSummary.SelectAll()
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsList_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsList.ItemClicked
        Try
            ' Slightly more complicated than the summary grid as this event fires for all other datagridviews.
            Select Case e.ClickedItem.Text
                Case "Set warning levels"
                    Dim dia As New diaClientAllocationLevels(SummaryData.ClientAllocationLevelDataView)
                    dia.ShowDialog()
                    SummaryData.GetClientAllocationLevels()
                    If dia.DialogResult = DialogResult.OK Then FormatListColumns()
                    dia.Dispose()

                Case "Copy"
                    sender.SourceControl.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    Clipboard.SetDataObject(sender.SourceControl.GetClipboardContent())

                Case "Select All"
                    sender.SourceControl.SelectAll()

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsForm_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsForm.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "Bailiff View"
                    If FormOpen("frmBailiffSummary") Then
                        frmBailiffSummary.Activate()
                    Else
                        frmBailiffSummary.ShowDialog()
                    End If
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ConsortiumContextSubMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ScrollbarSet As Boolean = False

            RemoveSelectionHandlers()

            SummaryData.ConsortiumDataView.RowFilter = "Consortium = '" & CType(sender, ToolStripItem).Text & "'"

            For Each ConsortiumDataRow As DataRow In SummaryData.ConsortiumDataView.ToTable.Rows
                For Each GridDataRow As DataGridViewRow In dgvClientName.Rows
                    If GridDataRow.Cells("ClientName").Value = ConsortiumDataRow.Item("ClientName") Then
                        GridDataRow.Selected = True
                        If Not ScrollbarSet Then
                            dgvClientName.FirstDisplayedScrollingRowIndex = GridDataRow.Index
                            ScrollbarSet = True
                        End If
                    End If
                Next GridDataRow

                ScrollbarSet = False

                For Each GridDataRow As DataGridViewRow In dgvSchemeName.Rows
                    If GridDataRow.Cells("SchemeName").Value = ConsortiumDataRow.Item("SchemeName") Then
                        GridDataRow.Selected = True
                        If Not ScrollbarSet Then
                            dgvSchemeName.FirstDisplayedScrollingRowIndex = GridDataRow.Index
                            ScrollbarSet = True
                        End If
                    End If
                Next GridDataRow
            Next ConsortiumDataRow

            AddSelectionHandlers()
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function GetStages() As String()
        GetStages = Nothing

        Try
            Dim SelectedSorts As New List(Of String)


            For Each Item As DataGridViewRow In dgvStageName.SelectedRows
                SelectedSorts.Add(Item.Cells("StageName").Value.ToString)
            Next Item

            If Not IsNothing(SelectedSorts) Then GetStages = SelectedSorts.ToArray

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function
#End Region



    'Private Sub dgvMouseHover(sender As Object, e As System.EventArgs)

    '    If Not RefreshDBClicked Then Return

    '    If Not ListData.IsRefreshRunning Then Return

    '    If Not RefreshRunningMsgShown Then

    '        Me.Enabled = False

    '        MsgBox("Refresh running.", vbOKOnly + vbInformation)
    '        RefreshRunningMsgShown = True
    '        RefreshCheckTimer.Enabled = True
    '    End If
    'End Sub

    'Private Sub RefreshCheckTimer_Tick(sender As Object, e As System.EventArgs) Handles RefreshCheckTimer.Tick

    '    If ListData.IsRefreshRunning Then Return

    '    MsgBox("Refresh complete.", vbOKOnly + vbInformation)
    '    RefreshRunningMsgShown = False
    '    RefreshCheckTimer.Enabled = False
    '    Me.Enabled = True
    'End Sub
End Class

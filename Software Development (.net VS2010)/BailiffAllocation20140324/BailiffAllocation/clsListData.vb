﻿Imports CommonLibrary

Public Class clsListData

    Private StageNameDT As New DataTable
    Private ClientNameDT As New DataTable
    Private WorkTypeDT As New DataTable
    Private SchemeNameDT As New DataTable
    Private AllocatedDT As New DataTable
    Private LeviedDT As New DataTable
    Private VanFeesAppliedDT As New DataTable
    Private PaymentDT As New DataTable
    Private VisitedDT As New DataTable
    Private AddConfirmedDT As New DataTable
    Private CoveredDT As New DataTable
    Private PostcodeAreaDT As New DataTable
    Private ArrangementBrokenDT As New DataTable
    Private DebtYearDT As New DataTable
    Private LinkedPIFDT As New DataTable ' added TS 214/Oct/2013. Portal task ref 17216

    Private BailiffNameDT As New DataTable
    Private BailiffTypeDT As New DataTable
    Private StatusNameDT As New DataTable
    Private FirstTwoVisitsPAWithinTwoDaysDT As New DataTable
    Private NumberOfVisitsPADT As New DataTable
    Private AllVisitsPAWithinTwentyDaysDT As New DataTable

    Private StageNameDV As DataView
    Private ClientNameDV As DataView
    Private WorkTypeDV As DataView
    Private SchemeNameDV As DataView
    Private AllocatedDV As DataView
    Private LeviedDV As DataView
    Private VanFeesAppliedDV As DataView
    Private PaymentDV As DataView
    Private VisitedDV As DataView
    Private AddConfirmedDV As DataView
    Private CoveredDV As DataView
    Private PostcodeAreaDV As DataView
    Private ArrangementBrokenDV As DataView
    Private DebtYearDV As DataView
    Private LinkedPIFDV As New DataView

    Private BailiffNameDV As New DataView
    Private BailiffTypeDV As New DataView
    Private StatusNameDV As New DataView
    Private FirstTwoVisitsPAWithinTwoDaysDV As New DataView
    Private NumberOfVisitsPADV As New DataView
    Private AllVisitsPAWithinTwentyDaysDV As New DataView

#Region "Public properties"

    Public ReadOnly Property StageName() As DataView
        Get
            StageName = StageNameDV
        End Get
    End Property

    Public ReadOnly Property ClientName() As DataView
        Get
            ClientName = ClientNameDV
        End Get
    End Property

    Public ReadOnly Property WorkType() As DataView
        Get
            WorkType = WorkTypeDV
        End Get
    End Property

    Public ReadOnly Property SchemeName() As DataView
        Get
            SchemeName = SchemeNameDV
        End Get
    End Property

    Public ReadOnly Property Allocated() As DataView
        Get
            Allocated = AllocatedDV
        End Get
    End Property

    Public ReadOnly Property Levied() As DataView
        Get
            Levied = LeviedDV
        End Get
    End Property

    Public ReadOnly Property VanFeesApplied() As DataView
        Get
            VanFeesApplied = VanFeesAppliedDV
        End Get
    End Property

    Public ReadOnly Property Payment() As DataView
        Get
            Payment = PaymentDV
        End Get
    End Property

    Public ReadOnly Property Visited() As DataView
        Get
            Visited = VisitedDV
        End Get
    End Property

    Public ReadOnly Property AddConfirmed() As DataView
        Get
            AddConfirmed = AddConfirmedDV
        End Get
    End Property

    Public ReadOnly Property Covered() As DataView
        Get
            Covered = CoveredDV
        End Get
    End Property

    Public ReadOnly Property PostcodeArea() As DataView
        Get
            PostcodeArea = PostcodeAreaDV
        End Get
    End Property

    Public ReadOnly Property ArrangementBroken() As DataView
        Get
            ArrangementBroken = ArrangementBrokenDV
        End Get
    End Property

    Public ReadOnly Property DebtYear() As DataView
        Get
            DebtYear = DebtYearDV
        End Get
    End Property

    Public ReadOnly Property LinkedPIF() As DataView
        Get
            LinkedPIF = LinkedPIFDV
        End Get
    End Property

    ' Bailiff screen specific lists
    Public ReadOnly Property BailiffName() As DataView
        Get
            BailiffName = BailiffNameDV
        End Get
    End Property

    Public ReadOnly Property BailiffType() As DataView
        Get
            BailiffType = BailiffTypeDV
        End Get
    End Property

    Public ReadOnly Property StatusName() As DataView
        Get
            StatusName = StatusNameDV
        End Get
    End Property

    Public ReadOnly Property FirstTwoVisitsPAWithinTwoDays() As DataView
        Get
            FirstTwoVisitsPAWithinTwoDays = FirstTwoVisitsPAWithinTwoDaysDV
        End Get
    End Property

    Public ReadOnly Property NumberOfVisitsPA() As DataView
        Get
            NumberOfVisitsPA = NumberOfVisitsPADV
        End Get
    End Property

    Public ReadOnly Property AllVisitsPAWithinTwentyDays() As DataView
        Get
            AllVisitsPAWithinTwentyDays = AllVisitsPAWithinTwentyDaysDV
        End Get
    End Property

    Public ReadOnly Property xIsRefreshRunning() As Boolean
        Get
            xIsRefreshRunning = GetSQLResults("BailiffAllocation", "EXEC dbo.IsRefreshRunning")
        End Get
    End Property

#End Region

#Region "Public methods"

    Public Sub GetCaseList(ByVal ParamList As String)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'StageName'", StageNameDT)
            StageNameDV = New DataView(StageNameDT)
            StageNameDV.AllowDelete = False
            StageNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'ClientName'", ClientNameDT)
            ClientNameDV = New DataView(ClientNameDT)
            ClientNameDV.AllowDelete = False
            ClientNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'WorkType'", WorkTypeDT)
            WorkTypeDV = New DataView(WorkTypeDT)
            WorkTypeDV.AllowDelete = False
            WorkTypeDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'SchemeName'", SchemeNameDT)
            SchemeNameDV = New DataView(SchemeNameDT)
            SchemeNameDV.AllowDelete = False
            SchemeNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'Allocated'", AllocatedDT)
            AllocatedDV = New DataView(AllocatedDT)
            AllocatedDV.AllowDelete = False
            AllocatedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'Levied'", LeviedDT)
            LeviedDV = New DataView(LeviedDT)
            LeviedDV.AllowDelete = False
            LeviedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'VanFeesApplied'", VanFeesAppliedDT)
            VanFeesAppliedDV = New DataView(VanFeesAppliedDT)
            VanFeesAppliedDV.AllowDelete = False
            VanFeesAppliedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'Payment'", PaymentDT)
            PaymentDV = New DataView(PaymentDT)
            PaymentDV.AllowDelete = False
            PaymentDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'Visited'", VisitedDT)
            VisitedDV = New DataView(VisitedDT)
            VisitedDV.AllowDelete = False
            VisitedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'AddConfirmed'", AddConfirmedDT)
            AddConfirmedDV = New DataView(AddConfirmedDT)
            AddConfirmedDV.AllowDelete = False
            AddConfirmedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'Covered'", CoveredDT)
            CoveredDV = New DataView(CoveredDT)
            CoveredDV.AllowDelete = False
            CoveredDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'PostcodeArea'", PostcodeAreaDT)
            PostcodeAreaDV = New DataView(PostcodeAreaDT)
            PostcodeAreaDV.AllowDelete = False
            PostcodeAreaDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'ArrangementBroken'", ArrangementBrokenDT)
            ArrangementBrokenDV = New DataView(ArrangementBrokenDT)
            ArrangementBrokenDV.AllowDelete = False
            ArrangementBrokenDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'DebtYear'", DebtYearDT)
            DebtYearDV = New DataView(DebtYearDT)
            DebtYearDV.AllowDelete = False
            DebtYearDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'LinkedPIF'", LinkedPIFDT)
            LinkedPIFDV = New DataView(LinkedPIFDT)
            LinkedPIFDV.AllowDelete = False
            LinkedPIFDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetBailiffList(ByVal ParamList As String)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'BailiffName'", BailiffNameDT)
            BailiffNameDV = New DataView(BailiffNameDT)
            BailiffNameDV.AllowDelete = False
            BailiffNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'BailiffType'", BailiffTypeDT)
            BailiffTypeDV = New DataView(BailiffTypeDT)
            BailiffTypeDV.AllowDelete = False
            BailiffTypeDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'ClientName'", ClientNameDT)
            ClientNameDV = New DataView(ClientNameDT)
            ClientNameDV.AllowDelete = False
            ClientNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'WorkType'", WorkTypeDT)
            WorkTypeDV = New DataView(WorkTypeDT)
            WorkTypeDV.AllowDelete = False
            WorkTypeDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'SchemeName'", SchemeNameDT)
            SchemeNameDV = New DataView(SchemeNameDT)
            SchemeNameDV.AllowDelete = False
            SchemeNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'StatusName'", StatusNameDT)
            StatusNameDV = New DataView(StatusNameDT)
            StatusNameDV.AllowDelete = False
            StatusNameDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'Levied'", LeviedDT)
            LeviedDV = New DataView(LeviedDT)
            LeviedDV.AllowDelete = False
            LeviedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'Payment'", PaymentDT)
            PaymentDV = New DataView(PaymentDT)
            PaymentDV.AllowDelete = False
            PaymentDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'Visited'", VisitedDT)
            VisitedDV = New DataView(VisitedDT)
            VisitedDV.AllowDelete = False
            VisitedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'AddConfirmed'", AddConfirmedDT)
            AddConfirmedDV = New DataView(AddConfirmedDT)
            AddConfirmedDV.AllowDelete = False
            AddConfirmedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'VanFeesApplied'", VanFeesAppliedDT)
            VanFeesAppliedDV = New DataView(VanFeesAppliedDT)
            VanFeesAppliedDV.AllowDelete = False
            VanFeesAppliedDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'PostcodeArea'", PostcodeAreaDT)
            PostcodeAreaDV = New DataView(PostcodeAreaDT)
            PostcodeAreaDV.AllowDelete = False
            PostcodeAreaDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'FirstTwoVisitsPAWithinTwoDays'", FirstTwoVisitsPAWithinTwoDaysDT)
            FirstTwoVisitsPAWithinTwoDaysDV = New DataView(FirstTwoVisitsPAWithinTwoDaysDT)
            FirstTwoVisitsPAWithinTwoDaysDV.AllowDelete = False
            FirstTwoVisitsPAWithinTwoDaysDV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'NumberOfVisitsPA'", NumberOfVisitsPADT)
            NumberOfVisitsPADV = New DataView(NumberOfVisitsPADT)
            NumberOfVisitsPADV.AllowDelete = False
            NumberOfVisitsPADV.AllowNew = False

            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'AllVisitsPAWithinTwentyDays'", AllVisitsPAWithinTwentyDaysDT)
            AllVisitsPAWithinTwentyDaysDV = New DataView(AllVisitsPAWithinTwentyDaysDT)
            AllVisitsPAWithinTwentyDaysDV.AllowDelete = False
            AllVisitsPAWithinTwentyDaysDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetClientList(ByVal ParamList As String)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetList " & ParamList & ",'ClientName'", ClientNameDT)
            ClientNameDV = New DataView(ClientNameDT)
            ClientNameDV.AllowDelete = False
            ClientNameDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub GetBailiffNameList(ByVal ParamList As String)
        Try
            LoadDataTable("BailiffAllocation", "EXEC dbo.GetBailiffList " & ParamList & ",'BailiffName'", BailiffNameDT)
            BailiffNameDV = New DataView(BailiffNameDT)
            BailiffNameDV.AllowDelete = False
            BailiffNameDV.AllowNew = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

    'Protected Overrides Sub Finalize()

    '    If Not IsNothing(StageNameDV) Then StageNameDV.Dispose()
    '    If Not IsNothing(ClientNameDV) Then ClientNameDV.Dispose()
    '    If Not IsNothing(WorkTypeDV) Then WorkTypeDV.Dispose()
    '    If Not IsNothing(SchemeNameDV) Then SchemeNameDV.Dispose()
    '    If Not IsNothing(AllocatedDV) Then AllocatedDV.Dispose()
    '    If Not IsNothing(LeviedDV) Then LeviedDV.Dispose()
    '    If Not IsNothing(VanFeesAppliedDV) Then VanFeesAppliedDV.Dispose()
    '    If Not IsNothing(PaymentDV) Then PaymentDV.Dispose()
    '    If Not IsNothing(VisitedDV) Then VisitedDV.Dispose()
    '    If Not IsNothing(AddConfirmedDV) Then AddConfirmedDV.Dispose()
    '    If Not IsNothing(CoveredDV) Then CoveredDV.Dispose()
    '    If Not IsNothing(PostcodeAreaDV) Then PostcodeAreaDV.Dispose()
    '    If Not IsNothing(ArrangementBrokenDV) Then ArrangementBrokenDV.Dispose()
    '    If Not IsNothing(DebtYearDV) Then DebtYearDV.Dispose()
    '    If Not IsNothing(LinkedPIFDV) Then LinkedPIFDV.Dispose()

    '    MyBase.Finalize()
    'End Sub

End Class

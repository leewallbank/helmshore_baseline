﻿Imports CommonLibrary
Public Class Form1

    Public ascii As New System.Text.ASCIIEncoding()

    Private Sub clntbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clntbtn.Click
        If client_tbox.Text.Length < 1 Then
            MsgBox("Enter start of client name")
        Else
            client_tbox.Text = Trim(client_tbox.Text)
            clientfrm.ShowDialog()

            With SaveFileDialog1
                .Title = "Save file"
                .Filter = "CSV files |*.csv"
                .DefaultExt = ".csv"
                .OverwritePrompt = True
                .FileName = "ClientReviewMeeting_" & selectedClientName & ".csv"
            End With
            If Not SaveFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                MsgBox("No file name selected for save ")
                Return
            End If

            exitbtn.Enabled = False
            clntbtn.Enabled = False
            outfile &= selectedClientName & vbNewLine
            outfile &= ","
            ProgressBar1.Maximum = 180
            ProgressBar1.Value = 10
            Application.DoEvents()
            'get all schemes with cases
            Dim dt_cs As New DataTable
            LoadDataTable2("DebtRecovery", "SELECT _rowid, schemeID " & _
                                                    "FROM clientScheme " & _
                                                    "WHERE clientID= " & selectedClientNo & _
                                                    " and branchID in (2,26)", dt_cs, False)
            NoOfSchemes = 0
            For Each row In dt_cs.Rows
                Dim CSID As Integer = row.item(0)
                Dim schID As Integer = row.item(1)
                Dim schemeName As String
                schemeName = GetSQLResults2("DebtRecovery", "SELECT name " & _
                                               "FROM Scheme " & _
                                               "WHERE _rowID= " & schID)
                NoOfSchemes += 1
                If NoOfSchemes > UBound(cs_table) Then
                    ReDim Preserve cs_table(NoOfSchemes + 10)
                    ReDim Preserve case_table(NoOfSchemes + 10)
                    ReDim Preserve paid_table(NoOfSchemes + 10)
                    ReDim Preserve gross_table(NoOfSchemes + 10)
                End If
                cs_table(NoOfSchemes) = CSID
                outfile &= schemeName & ","
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 20
            Application.DoEvents()
            'commission rates
            outfile &= "Commission rate" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim commrate As Decimal
                commrate = GetSQLResults2("DebtRecovery", "SELECT fee_comm1 " & _
                                               "FROM clientScheme " & _
                                               "WHERE _rowID= " & cs_table(rowIDX))
                outfile &= Format(commrate, "0.00") & ","
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 30
            Application.DoEvents()
            'no of cases
            outfile &= "Number of cases referred" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim NoofCases As Integer
                NoofCases = GetSQLResults2("DebtRecovery", "SELECT count(_rowid) " & _
                                               "FROM Debtor " & _
                                               "WHERE clientschemeID= " & cs_table(rowIDX))
                outfile &= NoofCases & ","
                case_table(rowIDX) = NoofCases
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 40
            Application.DoEvents()
            'gross value
            outfile &= "Gross Value" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim grossvalue As Decimal
                grossvalue = GetSQLResults2("DebtRecovery", "SELECT sum(debt_amount+debt_costs) " & _
                                               "FROM Debtor " & _
                                               "WHERE clientschemeID= " & cs_table(rowIDX))
                outfile &= Format(grossvalue, "0.00") & ","
                gross_table(rowIDX) = grossvalue
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 50
            Application.DoEvents()
            'last referral date
            outfile &= "Last Referral Date" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim lastReferralDate As Date = Nothing
                Try
                    lastReferralDate = GetSQLResults2("DebtRecovery", "SELECT max(_createdDate) " & _
                                               "FROM Debtor " & _
                                               "WHERE clientschemeID= " & cs_table(rowIDX))
                Catch ex As Exception

                End Try
                If lastReferralDate = Nothing Then
                    outfile &= ","
                Else
                    outfile &= Format(lastReferralDate, "dd/MM/yyyy") & ","
                End If

            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 60
            Application.DoEvents()
            'Average Balance
            outfile &= "Average Balance" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim debtBalance As Decimal = 0
                debtBalance = GetSQLResults2("DebtRecovery", "SELECT sum(debt_balance) " & _
                                           "FROM Debtor " & _
                                           "WHERE clientschemeID= " & cs_table(rowIDX))
                If case_table(rowIDX) = 0 Then
                    outfile &= "0.00,"
                Else
                    outfile &= Format(debtBalance / case_table(rowIDX), "0.00") & ","
                End If
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 70
            Application.DoEvents()
            '% with tel number
            outfile &= "%age of cases with tel numbers" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim phoneNumbers As Integer = 0
                phoneNumbers = GetSQLResults2("DebtRecovery", "SELECT distinct count(_rowid) " & _
                                           "FROM Debtor " & _
                                           "WHERE clientschemeID= " & cs_table(rowIDX) & _
                                       " and (add_phone is not null or add_fax is not null)")
                If case_table(rowIDX) = 0 Then
                    outfile &= "0.0,"
                Else
                    outfile &= Format((phoneNumbers / case_table(rowIDX)) * 100, "0.0") & ","
                End If

            Next
            outfile &= vbNewLine

            'Agreement type in place
            outfile &= "Agreement type in place" & vbNewLine

            ProgressBar1.Value = 80
            Application.DoEvents()
            'Collections
            outfile &= "Collections" & vbNewLine
            outfile &= "Monies Remitted" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim remitted As Decimal = 0
                remitted = GetSQLResults2("DebtRecovery", "SELECT sum(amount) " & _
                                           "FROM Payment " & _
                                           "WHERE clientschemeID= " & cs_table(rowIDX) & _
                                       " and status = 'R'")
                outfile &= Format(remitted, "0.00") & ","
                paid_table(rowIDX) = remitted
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 90
            Application.DoEvents()
            'Monies Waiting
            outfile &= "Monies Waiting" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim waiting As Decimal = 0
                waiting = GetSQLResults2("DebtRecovery", "SELECT sum(amount) " & _
                                           "FROM Payment " & _
                                           "WHERE clientschemeID= " & cs_table(rowIDX) & _
                                       " and status = 'W'")
                outfile &= Format(waiting, "0.00") & ","
                paid_table(rowIDX) += waiting
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 100
            Application.DoEvents()

            'Gross Collection %
            outfile &= "Gross Collection %" & ","
            For rowIDX = 1 To NoOfSchemes
                If gross_table(rowIDX) = 0 Then
                    outfile &= "0.0,"
                Else
                    outfile &= Format((paid_table(rowIDX) / gross_table(rowIDX)) * 100, "0.0") & ","
                End If
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 110
            Application.DoEvents()
            'Net Collection %
            outfile &= "Net Collection %" & ","
            'assume total paid / (gross value - cash value on no contact, gone away and client request)
            For rowIDX = 1 To NoOfSchemes
                Dim cashvalue As Decimal = 0
                cashvalue = GetSQLResults2("DebtRecovery", "SELECT sum(d.debt_balance) " & _
                                           "FROM Debtor AS d " & _
                                           " INNER JOIN codereturns as c on d.return_codeID = c._rowid" & _
                                           " WHERE d.clientschemeID= " & cs_table(rowIDX) & _
                                       " and d.status = 'C'" & _
                                       " and (c.fee_category = 1 or c.fee_category = 2)")
                If gross_table(rowIDX) - cashvalue = 0 Then
                    outfile &= "0.0,"
                Else
                    outfile &= Format((paid_table(rowIDX) / (gross_table(rowIDX) - cashvalue)) * 100, "0.0") & ","
                End If
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 120
            Application.DoEvents()
            'Closed Account Information
            outfile &= "Closed Account Information" & vbNewLine
            outfile &= "Activity Exhausted Volume" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim exhaustedVol As Integer = 0
                exhaustedVol = GetSQLResults2("DebtRecovery", "SELECT count(_rowid) " & _
                                           "FROM Debtor " & _
                                           "WHERE clientschemeID= " & cs_table(rowIDX) & _
                                       " and status = 'C'" & _
                                       " and (return_codeID = 33 or return_codeID = 78)")
                outfile &= Format(exhaustedVol, "0") & ","
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 130
            Application.DoEvents()
            outfile &= "Client Request Volume" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim clientReqVol As Integer = 0
                clientReqVol = GetSQLResults2("DebtRecovery", "SELECT count(d._rowid) " & _
                                           "FROM Debtor AS d " & _
                                           " INNER JOIN codereturns as c on d.return_codeID = c._rowid" & _
                                           " WHERE d.clientschemeID= " & cs_table(rowIDX) & _
                                       " and d.status = 'C'" & _
                                       " and c.fee_category = 2")
                outfile &= Format(clientReqVol, "0") & ","
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 140
            Application.DoEvents()
            outfile &= "Gone Away Volume" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim goneAwayVol As Integer = 0
                goneAwayVol = GetSQLResults2("DebtRecovery", "SELECT count(d._rowid) " & _
                                           "FROM Debtor AS d " & _
                                           " INNER JOIN codereturns as c on d.return_codeID = c._rowid" & _
                                           " WHERE d.clientschemeID= " & cs_table(rowIDX) & _
                                       " and d.status = 'C'" & _
                                       " and c.fee_category = 1")
                outfile &= Format(goneAwayVol, "0") & ","
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 150
            Application.DoEvents()
            outfile &= "Account Status Information" & vbNewLine
            outfile &= "Arrangement Volume" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim agreements As Integer = 0
                agreements = GetSQLResults2("DebtRecovery", "SELECT count(_rowid) " & _
                                           "FROM Debtor " & _
                                           "WHERE clientschemeID= " & cs_table(rowIDX) & _
                                       " and status = 'A'")

                outfile &= Format(agreements, "0") & ","
            Next
            outfile &= vbNewLine

            'T131219 get value of cases in arrangement
            ProgressBar1.Value = 155
            Application.DoEvents()
            outfile &= "Arrangement Value" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim agreements_val As Decimal = 0
                agreements_val = GetSQLResults2("DebtRecovery", "SELECT sum(F.fee_amount - F.remited_fee) " & _
                                           "FROM Debtor D, Fee F " & _
                                           "WHERE D.clientschemeID = " & cs_table(rowIDX) & _
                                           " and F.DebtorID=D._rowid" & _
                                           " and F.fee_remit_col = 1" & _
                                           " and D.status = 'A'")

                outfile &= Format(agreements_val, "0.0") & ","
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 160
            Application.DoEvents()
            outfile &= "Paid in Full Volume" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim pifVol As Integer = 0
                pifVol = GetSQLResults2("DebtRecovery", "SELECT count(_rowid) " & _
                                           "FROM Debtor " & _
                                           "WHERE clientschemeID= " & cs_table(rowIDX) & _
                                       " and (status = 'F' or status = 'S')")
                outfile &= Format(pifVol, "0") & ","
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 170
            Application.DoEvents()
            outfile &= "Trace Volume" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim traceVol As Integer = 0
                traceVol = GetSQLResults2("DebtRecovery", "SELECT count(_rowid) " & _
                                           "FROM Debtor " & _
                                           "WHERE clientschemeID= " & cs_table(rowIDX) & _
                                       " and status = 'T' and status_open_closed = 'O'")
                outfile &= Format(traceVol, "0") & ","
            Next
            outfile &= vbNewLine

            ProgressBar1.Value = 180
            Application.DoEvents()
            outfile &= "Total Open" & ","
            For rowIDX = 1 To NoOfSchemes
                Dim openVol As Integer = 0
                openVol = GetSQLResults2("DebtRecovery", "SELECT count(_rowid) " & _
                                           "FROM Debtor " & _
                                           "WHERE clientschemeID= " & cs_table(rowIDX) & _
                                       " and status_open_closed = 'O'")
                outfile &= Format(openVol, "0") & ","
            Next
            outfile &= vbNewLine
            outfile &= "Comments" & vbNewLine

            My.Computer.FileSystem.WriteAllText(SaveFileDialog1.FileName, outfile, False, ascii)
            MsgBox("Report saved")
            Me.Close()
        End If
    End Sub

    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        ConnectDb2("DebtRecovery")
    End Sub
End Class

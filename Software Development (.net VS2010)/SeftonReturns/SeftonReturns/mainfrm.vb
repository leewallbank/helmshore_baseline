Public Class mainfrm
    Dim first_letter As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim days As Integer = Weekday(Now)
        date_picker.Value = DateAdd(DateInterval.Day, -days, Now)
    End Sub

    Private Sub csbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cl_btn.Click
        first_letter = InputBox("Enter first letters of client", "First Letters of Client")
        If first_letter.Length = 0 Then
            MsgBox("NO Letter entered")
            Exit Sub
        End If
        param1 = "onestep"
        param2 = "Select _rowid, name from Client where name like '" & first_letter & "%" & "'" & _
        " order by name"
        Dim client_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("There are no clients starting with " & first_letter)
            Exit Sub
        End If
        Dim idx As Integer
        cl_rows = no_of_rows - 1
        For idx = 0 To cl_rows
            If idx = UBound(client_array, 2) Then
                ReDim Preserve client_array(2, idx + 20)
            End If
            client_array(1, idx) = client_dataset.Tables(0).Rows(idx).Item(0)
            client_array(2, idx) = Trim(client_dataset.Tables(0).Rows(idx).Item(1))
        Next
        clientfrm.ShowDialog()
        If selected_csid = 0 Then
            MsgBox("No client scheme has been selected")
            Exit Sub
        Else
            If MsgBox("Client selected is " & client_name & " scheme name is " & scheme_name, MsgBoxStyle.YesNo, "Client scheme verification") = MsgBoxResult.No Then
                Exit Sub
            End If
        End If
        date_picker.Enabled = True
        Label1.Text = client_name
        retnbtn.Enabled = True
    End Sub

    Private Sub retnbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles retnbtn.Click
        retn_month = Format(date_picker.Value, "MM_MMM")
        retn_day = Format(date_picker.Value, "dd_ddd")
        'get remittance number for selected client scheme
        Dim files_found As Boolean = False
        param2 = "select _rowid from Remit where clientschemeID = " & selected_csid & _
             " and date = '" & Format(date_picker.Value, "yyyy-MM-dd") & "'"
        Dim remit_dataset As DataSet = get_dataset(param1, param2)
        If no_of_rows = 0 Then
            MsgBox("No remit found")
            Exit Sub
        End If
        Dim remit_no As Integer = remit_dataset.Tables(0).Rows(0).Item(0)
        file_path = "O:\DebtRecovery\Archives\" & Format(date_picker.Value, "yyyy") & "\" & retn_month & "\" & _
                               retn_day & "\Remittances\" & Format(remit_no, "#") & "\Returns\"

        Dim file_name, client_ref As String
        Dim cf_ga As String = ""
        Dim cf_cg As String = ""
        Dim cf_nb As String = ""
        Dim cf_nc As String = ""
        Dim cf_as As String = ""
        Dim cf_all As String = ""
        'NO LONGER NEED RETURNS SEPARATING INTO FOLDERS!
        'create directories for returns
        'Dim dir_name As String = file_path & "Nulla Bona"
        'Try
        '    Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
        '    If System.IO.Directory.Exists(dir_name) = False Then
        '        di = System.IO.Directory.CreateDirectory(dir_name)
        '    Else
        '        System.IO.Directory.Delete(dir_name, True)
        '        di = System.IO.Directory.CreateDirectory(dir_name)
        '    End If
        'Catch ex As Exception
        '    MsgBox("Unable to create folder")
        '    End
        'End Try
        'dir_name = file_path & "Gone Away"
        'Try
        '    Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
        '    If System.IO.Directory.Exists(dir_name) = False Then
        '        di = System.IO.Directory.CreateDirectory(dir_name)
        '    Else
        '        System.IO.Directory.Delete(dir_name, True)
        '        di = System.IO.Directory.CreateDirectory(dir_name)
        '    End If
        'Catch ex As Exception
        '    MsgBox("Unable to create folder")
        '    End
        'End Try
        'dir_name = file_path & "As Requested"
        'Try
        '    Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
        '    If System.IO.Directory.Exists(dir_name) = False Then
        '        di = System.IO.Directory.CreateDirectory(dir_name)
        '    Else
        '        System.IO.Directory.Delete(dir_name, True)
        '        di = System.IO.Directory.CreateDirectory(dir_name)
        '    End If
        'Catch ex As Exception
        '    MsgBox("Unable to create folder")
        '    End
        'End Try
        'dir_name = file_path & "Client Guidelines"
        'Try
        '    Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
        '    If System.IO.Directory.Exists(dir_name) = False Then
        '        di = System.IO.Directory.CreateDirectory(dir_name)
        '    Else
        '        System.IO.Directory.Delete(dir_name, True)
        '        di = System.IO.Directory.CreateDirectory(dir_name)
        '    End If
        'Catch ex As Exception
        '    MsgBox("Unable to create folder")
        '    End
        'End Try
        'dir_name = file_path & "No Contact"
        'Try
        '    Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
        '    If System.IO.Directory.Exists(dir_name) = False Then
        '        di = System.IO.Directory.CreateDirectory(dir_name)
        '    Else
        '        System.IO.Directory.Delete(dir_name, True)
        '        di = System.IO.Directory.CreateDirectory(dir_name)
        '    End If
        'Catch ex As Exception
        '    MsgBox("Unable to create folder")
        '    End
        'End Try
        Dim debtor, retn_codeid As Integer

        For Each foundFile As String In My.Computer.FileSystem.GetFiles _
            (file_path, FileIO.SearchOption.SearchAllSubDirectories, "*.pdf")
            'get debtor number
            files_found = True
            Dim dashIDX As Integer = InStr(foundFile, "-")
            debtor = Mid(foundFile, dashIDX + 1, 8)
            'get return code for this debtor
            param2 = "select return_codeID, client_ref, status_open_closed from Debtor " & _
            " where _rowid = " & debtor
            Dim debtor_dataset As DataSet = get_dataset(param1, param2)
            If no_of_rows = 0 Then
                MsgBox("Unable to find case number" & debtor)
                Exit Sub
            End If
            Dim status_open_closed As String = debtor_dataset.Tables(0).Rows(0).Item(2)
            If status_open_closed = "O" Then
                Continue For
            End If
            Try
                retn_codeid = debtor_dataset.Tables(0).Rows(0).Item(0)
            Catch ex As Exception
                retn_codeid = 0
            End Try
            'get returns category
            param2 = "select fee_category from CodeReturns where _rowid = " & retn_codeid
            Dim retn_ds As DataSet = get_dataset("onestep", param2)
            Dim retn_cat As Integer = retn_ds.Tables(0).Rows(0).Item(0)
            If InStr(scheme_name, "NNDR") > 0 Then
                retn_cat = retn_cat * 11
            End If
            'get retn_group 
            client_ref = Trim(debtor_dataset.Tables(0).Rows(0).Item(1))
            cf_all = cf_all & client_ref & "-" & debtor & ".pdf," & client_ref & "," & retn_cat & vbNewLine
            'Dim retn_group As String = ""
            'Select Case retn_cat
            '    Case 1
            '        retn_group = "Gone Away"
            '        cf_ga = cf_ga & client_ref & "-" & debtor & ".pdf," & client_ref & vbNewLine
            '    Case 2
            '        retn_group = "As Requested"
            '        cf_as = cf_as & client_ref & "-" & debtor & ".pdf," & client_ref & vbNewLine
            '    Case 3
            '        retn_group = "Nulla Bona"
            '        cf_nb = cf_nb & client_ref & "-" & debtor & ".pdf," & client_ref & vbNewLine
            '    Case 4
            '        retn_group = "Client Guidelines"
            '        cf_cg = cf_cg & client_ref & "-" & debtor & ".pdf," & client_ref & vbNewLine
            '    Case 6
            '        retn_group = "No Contact"
            '        cf_nc = cf_nc & client_ref & "-" & debtor & ".pdf," & client_ref & vbNewLine
            'End Select
            'If retn_group = "" Then
            '    MsgBox("Return code not found for case number " & debtor)
            'End If

            'file_name = file_path & retn_group & "\" & client_ref & "-" & debtor & ".pdf"
            'My.Computer.FileSystem.CopyFile(foundFile, file_name)
        Next
        'write control files
        'now only one control file
        'If cf_ga <> "" Then
        '    file_name = file_path & "Gone Away\cf_gone_away.txt"
        '    My.Computer.FileSystem.WriteAllText(file_name, cf_ga, False)
        'End If

        'If cf_as <> "" Then
        '    file_name = file_path & "As Requested\cf_as_requested.txt"
        '    My.Computer.FileSystem.WriteAllText(file_name, cf_as, False)
        'End If

        'If cf_nb <> "" Then
        '    file_name = file_path & "Nulla Bona\cf_nulla_bona.txt"
        '    My.Computer.FileSystem.WriteAllText(file_name, cf_nb, False)
        'End If

        'If cf_cg <> "" Then
        '    file_name = file_path & "Client Guidelines\cf_client_guidelines.txt"
        '    My.Computer.FileSystem.WriteAllText(file_name, cf_cg, False)
        'End If

        'If cf_nc <> "" Then
        '    file_name = file_path & "No Contact\cf_no_contact.txt"
        '    My.Computer.FileSystem.WriteAllText(file_name, cf_nc, False)
        'End If
        If cf_all <> "" Then
            file_name = file_path & "Returns.txt"
            My.Computer.FileSystem.WriteAllText(file_name, cf_all, False)
        End If
        If files_found Then
            'MsgBox("All reports moved to directories")
            MsgBox("Control file saved")
        Else
            MsgBox("No reports were found")
        End If
        Me.Close()
    End Sub

End Class

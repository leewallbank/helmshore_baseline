﻿Imports CommonLibrary

Public Class diaBailiff
    Private BailiffData As New clsBailiffData

    Public Sub AddCases(ByVal ParamList() As String)
        Try
            PreRefresh(Me)

            BailiffData.GetBailiffs(ParamList)
            dgvBailiff.DataSource = BailiffData.BailiffDataView

            FormatGridColumns(dgvBailiff)

            PostRefresh(Me)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Public Sub New()
        Try
            ' This call is required by the designer.
            InitializeComponent()

            SetFormIcon(Me)

            cmsBailiff.Items.Add("View Cases")

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvBailiff_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvBailiff.MouseDown
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvBailiff.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
                dgvBailiff.Rows(hti.RowIndex).Selected = True
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvBailiff_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvBailiff.MouseUp
        If e.Button = MouseButtons.Right Then cmsBailiff.Show(dgvBailiff, e.Location)
    End Sub

    Private Sub cmsBailiff_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsBailiff.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "View Cases"
                    Dim Detail As New diaCaseDetail(Me)
                    Detail.Text += " for " & dgvBailiff.SelectedRows(0).Cells("name_fore").Value & " " & dgvBailiff.SelectedRows(0).Cells("name_sur").Value
                    Detail.AddCasesByBailiff(dgvBailiff.SelectedRows(0).Cells("BailiffID").Value)
                    Detail.Show()
                    Detail = Nothing
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

End Class
Imports CommonLibrary
Public Class clientfrm

    Private Sub clientfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim client_dt As New DataTable
        LoadDataTable("DebtRecoveryLocal", "Select _rowid, name, ref from Client " & _
                      " where name like '" & first_letters & "%" & "'" & _
                      " order by name", client_dt, False)
        If client_dt.Rows.Count = 0 Then
            MsgBox("There are no clients starting with " & first_letters)
            Exit Sub
        End If
        clientDG.Rows.Clear()
        For Each clientRow In client_dt.Rows
            Dim cl_id As Integer = clientRow(0)
            Dim cl_name As String = clientRow(1)
            Dim cl_ref As String = clientRow(2)
            clientDG.Rows.Add(cl_id, cl_name, cl_ref)
        Next
    End Sub
End Class
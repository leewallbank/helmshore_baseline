﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class emailfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.emailDG = New System.Windows.Forms.DataGridView()
        Me.findbtn = New System.Windows.Forms.Button()
        Me.add_seq_no = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.add_clientID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.add_client_name = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.add_work_type = New System.Windows.Forms.DataGridViewComboBoxColumn()
        Me.add_email = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.add_last_date = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.emailDG, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'emailDG
        '
        Me.emailDG.AllowUserToDeleteRows = False
        Me.emailDG.AllowUserToOrderColumns = True
        Me.emailDG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.emailDG.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.add_seq_no, Me.add_clientID, Me.add_client_name, Me.add_work_type, Me.add_email, Me.add_last_date})
        Me.emailDG.Dock = System.Windows.Forms.DockStyle.Fill
        Me.emailDG.Location = New System.Drawing.Point(0, 0)
        Me.emailDG.Name = "emailDG"
        Me.emailDG.Size = New System.Drawing.Size(846, 589)
        Me.emailDG.TabIndex = 0
        '
        'findbtn
        '
        Me.findbtn.Location = New System.Drawing.Point(542, 0)
        Me.findbtn.Name = "findbtn"
        Me.findbtn.Size = New System.Drawing.Size(115, 23)
        Me.findbtn.TabIndex = 2
        Me.findbtn.Text = "Get client number"
        Me.findbtn.UseVisualStyleBackColor = True
        '
        'add_seq_no
        '
        Me.add_seq_no.HeaderText = "SeqNo"
        Me.add_seq_no.Name = "add_seq_no"
        Me.add_seq_no.Visible = False
        '
        'add_clientID
        '
        Me.add_clientID.HeaderText = "Client ID"
        Me.add_clientID.Name = "add_clientID"
        Me.add_clientID.Width = 50
        '
        'add_client_name
        '
        Me.add_client_name.HeaderText = "Client Name"
        Me.add_client_name.Name = "add_client_name"
        Me.add_client_name.ReadOnly = True
        Me.add_client_name.Width = 250
        '
        'add_work_type
        '
        Me.add_work_type.HeaderText = "Work Type"
        Me.add_work_type.Items.AddRange(New Object() {"CTAX", "NNDR", "RTD", "Other"})
        Me.add_work_type.Name = "add_work_type"
        Me.add_work_type.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.add_work_type.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.add_work_type.Width = 80
        '
        'add_email
        '
        Me.add_email.HeaderText = "Email Address"
        Me.add_email.Name = "add_email"
        Me.add_email.Width = 250
        '
        'add_last_date
        '
        Me.add_last_date.HeaderText = "Last Date Sent"
        Me.add_last_date.Name = "add_last_date"
        Me.add_last_date.ReadOnly = True
        Me.add_last_date.Width = 150
        '
        'emailfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(846, 589)
        Me.Controls.Add(Me.findbtn)
        Me.Controls.Add(Me.emailDG)
        Me.Name = "emailfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Maintain Email Addresses"
        CType(Me.emailDG, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents emailDG As System.Windows.Forms.DataGridView
    Friend WithEvents findbtn As System.Windows.Forms.Button
    Friend WithEvents add_seq_no As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents add_clientID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents add_client_name As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents add_work_type As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents add_email As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents add_last_date As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

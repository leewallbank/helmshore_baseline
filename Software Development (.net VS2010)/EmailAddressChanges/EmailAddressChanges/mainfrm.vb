Imports System.Collections
Imports CommonLibrary
Public Class mainfrm
    Dim startDate, endDate As Date
    Dim subject As String = ""
    Dim fromaddress As String
    Dim toaddress As String = ""
    Dim cc1 As String = ""
    Dim body, attachment As String
    Dim error_found As Boolean = False
    Dim sendALL As Boolean
    Dim cc_all As String
    Dim update_txt As String
    Private Sub exitbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitbtn.Click
        Me.Close()
    End Sub

    Private Sub runbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles maintainrbtn.Click
        emailfrm.ShowDialog()

    End Sub
    Sub send_email()
        Try
            If email(toaddress, fromaddress, subject, body, attachment, cc_all) = 0 Then
                Me.Close()
            Else
                MsgBox("See error log at H:\EmailAddressChanges\error_log")
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
            Exit Sub
        End Try
    End Sub

    Private Sub mainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        startDate = DateAdd(DateInterval.Month, -1, CDate(Format(Now, "MMM") & " 1," & Format(Now, "yyyy") & " 00:00:00"))
        monthLbl.Text = "Send address changes for " & Format(startDate, "MMM yyyy")
        endDate = CDate(Format(Now, "MMM") & ", 01 " & Format(Now, "yyyy"))
    End Sub

   
    Private Sub sendNewbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sendNewbtn.Click
        sendALL = False
        If MsgBox("Send emails not yet sent for last month?", MsgBoxStyle.YesNo, "Send emails not yet sent") = MsgBoxResult.No Then
            MsgBox("No emails sent")
            Exit Sub
        End If
        process_emails()
    End Sub
    Private Sub process_emails()
        error_path = "H:\EmailAddressChanges\"
        error_file = error_path & "error_log_" & Format(Now, "dd.MM.yyyy.HH.mm.ss") & ".txt"
        Dim env_str As String = ""
        Try
            env_str = System.Configuration.ConfigurationManager.AppSettings("Environment").ToString
        Catch ex As Exception
            env_str = "Test"
        End Try
        'delete folder containing last reports
        Dim dir_name As String = "H:emailAddressChanges\reports"
        Try
            Dim di As IO.DirectoryInfo = New IO.DirectoryInfo(dir_name)
            If System.IO.Directory.Exists(dir_name) = False Then
                di = System.IO.Directory.CreateDirectory(dir_name)
            Else
                System.IO.Directory.Delete(dir_name, True)
                di = System.IO.Directory.CreateDirectory(dir_name)
            End If
        Catch ex As Exception
            MsgBox("Unable to create reports folder")
            Exit Sub
            End
        End Try
        sendALLBtn.Enabled = False
        sendNewbtn.Enabled = False
        maintainrbtn.Enabled = False
        exitbtn.Enabled = False
        Dim oWSH = CreateObject("WScript.Shell")
        Dim log_user As String = oWSH.ExpandEnvironmentStrings("%USERNAME%")
        Dim ccaddress As String = log_user & "@rossendales.com"
        cc_all = InputBox("Send a CC for emails to ", "EMAIL to send CC to. Blank out for no CC", ccaddress)
        Dim yesterday As Date = DateAdd(DateInterval.Day, -1, Now)

        'check clients and work type for address changes
        Dim client_dt As New DataTable
        Dim clientName As String = ""
        LoadDataTable("FeesSQL", " select add_seq_no, add_clientID, add_workType, add_email, add_date_sent" & _
                      " from EmailAddressChanges" & _
                      " order by add_seq_no", client_dt, False)

        Dim addressChange As Boolean = False
        Dim outfile As String = "Case Number,Client Reference,LO Date, Summons No, Old case address,New Case Address" & vbNewLine
        For Each clientRow In client_dt.Rows
            Dim seqNo As Integer = clientRow(0)
            Dim clientID As Integer = clientRow(1)

            Dim schemeType As String = clientRow(2)
            Dim email As String = clientRow(3)

            Dim dateSent As Date = clientRow(4)
            If Not sendALL Then
                If Format(dateSent, "yyyy-MM-dd") >= Format(endDate, "yyyy-MM-dd") Then
                    Continue For
                End If
            End If
            Dim debt_dt As New DataTable
            attachment = ""
            LoadDataTable("DebtRecoveryLocal", "SELECT d._rowid, d.client_ref, d.address, n.text, n._createddate, s.work_type, n.type, d.offence_court, d.offence_number, C.name " & _
                                                    "FROM debtor as d" & _
                                                    " INNER JOIN Note as n ON d._rowid = n.debtorID " & _
                                                    " INNER JOIN clientScheme as cs ON d.clientSchemeID = cs._rowID" & _
                                                    " INNER JOIN scheme as S ON cs.schemeID = s._rowID" & _
                                                    " INNER JOIN client as C ON cs.clientID = C._rowID" & _
                                                    " and d.status_open_closed = 'O'" & _
                                                    " and not(d.status in ('C','F','S'))" & _
                                                   " and n._createdDate >= ' " & Format(startDate, "yyyy-MM-dd") & "'" & _
                                                   " and n._createdDate < ' " & Format(endDate, "yyyy-MM-dd") & "'" & _
                                                    " and cs.clientID = " & clientID, debt_dt, False)
            Dim debtRows As Integer = debt_dt.Rows.Count
            Dim row As DataRow
            Dim idx As Integer = 0
            ProgressBar1.Maximum = debt_dt.Rows.Count
            For Each row In debt_dt.Rows
                idx += 1
                Try
                    ProgressBar1.Value = idx
                Catch ex As Exception
                    idx = 0
                End Try

                Application.DoEvents()
                Dim noteType As String = row(6)
                If noteType <> "Address" Then
                    Continue For
                End If
                Dim workType As Integer = row(5)
                Select Case schemeType
                    Case "CTAX"
                        If workType <> 2 Then
                            Continue For
                        End If
                    Case "NNDR"
                        If workType <> 3 Then
                            Continue For
                        End If
                    Case "RTD"
                        If workType <> 16 And workType <> 20 Then
                            Continue For
                        End If
                    Case "Other"
                        If workType = 2 Or workType = 3 Or workType = 16 Or workType = 20 Then
                            Continue For
                        End If
                End Select
                Dim debtorID As Integer = row(0)
                Dim clientREf As String = row(1)
                Dim createdDate As Date = row(4)
                Dim LODate As Date
                Try
                    LODate = row(7)
                Catch ex As Exception
                    LODate = Nothing
                End Try
                Dim SummonsNo As String = ""
                Try
                    SummonsNo = row(8)
                Catch ex As Exception

                End Try

                clientName = row(9)
                'GET old address
                Dim oldAddress As String = row.Item(3)
                If Microsoft.VisualBasic.Left(oldAddress, 13) = "changed from:" Then
                    oldAddress = Microsoft.VisualBasic.Right(oldAddress, oldAddress.Length - 13)
                Else
                    Continue For
                End If
                Dim newAddress As String = row.Item(2)
                'remove carriage returns from new address
                Dim amendedNewAddress As String = ""
                For idx = 1 To newAddress.Length
                    If Mid(newAddress, idx, 1) = Chr(10) Or Mid(newAddress, idx, 1) = Chr(13) Then
                        amendedNewAddress &= ", "
                    Else
                        amendedNewAddress &= Mid(newAddress, idx, 1)
                    End If
                Next
                addressChange = True
                'remove commas
                oldAddress = Replace(oldAddress, ",", " ")
                amendedNewAddress = Replace(amendedNewAddress, ",", " ")
                outfile &= debtorID & ",  " & clientREf & ","
                If LODate = Nothing Then
                    outfile &= ","
                Else
                    outfile &= Format(LODate, "dd/MM/yyyy") & ","
                End If
                outfile &= SummonsNo & "," & oldAddress & "," & amendedNewAddress & vbNewLine
               
                attachment = "H:emailAddressChanges\reports\" & clientName & "_" & schemeType & ".csv"
                WriteFile(attachment, outfile)
            Next
            If addressChange Then
                addressChange = False
                fromaddress = "crystal@rossendales.com"
                If env_str <> "Prod" Then
                    toaddress = "JBlundell@rossendales.com"
                Else
                    toaddress = email
                End If
                subject = "#XNO Change Contact for " & clientName & "  " & schemeType
                body = "Good Morning"
                If Format(Now, "HH") >= 12 Then
                    body = "Good afternoon"
                End If
                body &= vbNewLine
                body &= "Please find attached list of address changes for last month"
                body &= vbNewLine & vbNewLine & "Regards" & vbNewLine
                body &= "Client Liaison Team" & vbNewLine & _
                        "        Rossendales" & vbNewLine & _
                        "        PO Box 324" & vbNewLine & _
                        "        Rossendale" & vbNewLine & _
                        "        BB4 0GE   " & vbNewLine & _
                        "Tel: 01706 830323" & vbNewLine
                send_email()
                outfile = "Case Number,Client Reference,LO Date, Summons No, Old case address,New Case Address" & vbNewLine
                'update date on table
                update_txt = "update EmailAddressChanges" & _
                    " set add_date_sent = '" & Format(Now, "yyyy-MMM-dd") & "'" & _
                    " where add_seq_no = " & seqNo
                update_sql(update_txt)
            End If
        Next
        MsgBox("All emails sent")
        Me.Close()
    End Sub

    Private Sub sendALLBtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles sendALLBtn.Click
        sendALL = True
        If MsgBox("Send ALL emails for last month?", MsgBoxStyle.YesNo, "Send ALL emails") = MsgBoxResult.No Then
            MsgBox("No emails sent")
            Exit Sub
        End If
        process_emails()
    End Sub
End Class

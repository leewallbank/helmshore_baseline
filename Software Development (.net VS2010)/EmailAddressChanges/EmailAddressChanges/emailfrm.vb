﻿Imports CommonLibrary
Public Class emailfrm

    Private Sub Maintainfrm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        emailDG.Rows.Clear()
        'get all rows
        Dim client_dt As New DataTable
        Dim lastCLID As Integer = 0
        Dim lastClientName As String = ""
        LoadDataTable("FeesSQL", " select add_seq_no, add_clientID, add_workType, add_email, add_date_sent" & _
                      " from EmailAddressChanges" & _
                      " order by add_clientID", client_dt, False)
        For Each clientRow In client_dt.Rows
            Dim seqNo As Integer = clientRow(0)
            Dim clientID As Integer = clientRow(1)
            Dim workType As String = clientRow(2)
            Dim email As String = ""
            Try
                email = clientRow(3)
            Catch ex As Exception

            End Try
            Dim dateSent As Date
            Try
                dateSent = clientRow(4)
            Catch ex As Exception
                dateSent = nullDate
            End Try
            Dim clientName As String
            If clientID = lastCLID Then
                clientName = lastClientName
            Else
                clientName = GetSQLResults("DebtRecoveryLocal", "select name from Client " & _
                                                                            " where _rowid = " & clientID)
            End If
           
            If dateSent = nullDate Then
                emailDG.Rows.Add(seqNo, clientID, clientName, workType, email)
            Else
                emailDG.Rows.Add(seqNo, clientID, clientName, workType, email, Format(dateSent, "dd/MMM/yyyy"))
            End If
            lastCLID = clientID
            lastClientName = clientName
        Next
    End Sub

    Private Sub emailDG_CellValidating(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellValidatingEventArgs) Handles emailDG.CellValidating
        If e.ColumnIndex = 1 Then
            Try
                If e.FormattedValue = 0 Then
                    Exit Sub
                End If
            Catch ex As Exception
                Exit Sub
            End Try
            If saved_seq_no = 0 Then
                emailDG.Rows(e.RowIndex).Cells(2).Value = GetSQLResults("DebtRecoveryLocal", "select name from Client " & _
                                                                            " where _rowid = " & e.FormattedValue)
                emailDG.Rows(e.RowIndex).Cells(3).Value = "CTAX"
            End If

        End If
        If e.ColumnIndex = 4 Then
            If InStr(e.FormattedValue, "@") = 0 Then
                MsgBox("Email address has to contain @")
                e.Cancel = True
            Else
                If InStr(e.FormattedValue.ToString, """") > 0 Then
                    MsgBox("Email address must not contain quotes")
                    e.Cancel = True
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub findbtn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles findbtn.Click
        first_letters = InputBox("Enter first part of client name", "Select client")
        first_letters = Trim(first_letters)
        first_letters = UCase(Microsoft.VisualBasic.Left(first_letters, 1)) & _
        Microsoft.VisualBasic.Right(first_letters, first_letters.Length - 1)
        If first_letters.Length = 0 Then
            MsgBox("NO Letters entered")
            Exit Sub
        End If
        clientfrm.ShowDialog()
    End Sub

    Private Sub emailDG_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles emailDG.CellValueChanged
        If e.RowIndex < 0 Then
            Exit Sub
        End If
        Dim clientID As String = emailDG.Rows(e.RowIndex).Cells(1).Value
        Dim workType As String = emailDG.Rows(e.RowIndex).Cells(3).Value
        Dim email As String = emailDG.Rows(e.RowIndex).Cells(4).Value
        'If e.ColumnIndex = 4 Then
        '    Dim date_str As String = emailDG.Rows(e.RowIndex).Cells(4).Value
        '    If date_str.Length > 0 Then
        '        'update date
        '    End If

        'End If
        If e.ColumnIndex = 1 Then
            If saved_seq_no = 0 Then
                Try
                    upd_txt = "Insert into EmailAddressChanges (add_clientID, add_workType, add_email, add_date_sent)" & _
                        "values(" & clientID & ",'" & "CTAX" & "','" & email & "','" & nullDate & "')"
                    update_sql(upd_txt)
                Catch ex As Exception
                    MsgBox(ex.Message)
                End Try
                'get seq_no
                saved_seq_no = GetSQLResults("FeesSQL", "select max(add_seq_no) from EmailAddressChanges")
            Else
                'delete if clientID =0
                If clientID = 0 Then
                    upd_txt = "delete from EmailAddressChanges" & _
                                       " where add_seq_no = " & saved_seq_no
                    update_sql(upd_txt)
                Else
                    upd_txt = "Update EmailAddressChanges set add_clientID = " & clientID & _
                                        " where add_seq_no = " & saved_seq_no
                    update_sql(upd_txt)
                    Dim clientName As String = GetSQLResults("DebtRecoveryLocal", "select name from Client " & _
                                                                                " where _rowid = " & clientID)
                    emailDG.Rows(e.RowIndex).Cells(2).Value = clientName
                End If
               
            End If
        End If
        If e.ColumnIndex = 3 And saved_seq_no > 0 Then
            Try
                upd_txt = "Update EmailAddressChanges set add_workType = '" & workType & "'" & _
                    " where add_seq_no = " & saved_seq_no
                update_sql(upd_txt)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
        If e.ColumnIndex = 4 And saved_seq_no > 0 Then
            Try
                upd_txt = "Update EmailAddressChanges set add_email = '" & email & "'" & _
                    " where add_seq_no = " & saved_seq_no
                update_sql(upd_txt)
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Private Sub emailDG_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles emailDG.RowEnter
       
        saved_email = emailDG.Rows(e.RowIndex).Cells(4).Value

        saved_work_type = emailDG.Rows(e.RowIndex).Cells(3).Value
        saved_seq_no = emailDG.Rows(e.RowIndex).Cells(0).Value
        saved_clientID = emailDG.Rows(e.RowIndex).Cells(1).Value
        'If saved_clientID <> 0 Then
        '    emailDG.Rows(e.RowIndex).Cells(1).ReadOnly = True
        'End If
    End Sub

End Class
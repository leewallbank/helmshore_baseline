<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mainfrm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.maintainrbtn = New System.Windows.Forms.Button()
        Me.exitbtn = New System.Windows.Forms.Button()
        Me.sendALLBtn = New System.Windows.Forms.Button()
        Me.sendNewbtn = New System.Windows.Forms.Button()
        Me.monthLbl = New System.Windows.Forms.Label()
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.SuspendLayout()
        '
        'maintainrbtn
        '
        Me.maintainrbtn.Location = New System.Drawing.Point(95, 71)
        Me.maintainrbtn.Name = "maintainrbtn"
        Me.maintainrbtn.Size = New System.Drawing.Size(121, 23)
        Me.maintainrbtn.TabIndex = 0
        Me.maintainrbtn.Text = "Maintain Email List"
        Me.maintainrbtn.UseVisualStyleBackColor = True
        '
        'exitbtn
        '
        Me.exitbtn.Location = New System.Drawing.Point(271, 287)
        Me.exitbtn.Name = "exitbtn"
        Me.exitbtn.Size = New System.Drawing.Size(75, 23)
        Me.exitbtn.TabIndex = 3
        Me.exitbtn.Text = "Exit"
        Me.exitbtn.UseVisualStyleBackColor = True
        '
        'sendALLBtn
        '
        Me.sendALLBtn.Location = New System.Drawing.Point(95, 181)
        Me.sendALLBtn.Name = "sendALLBtn"
        Me.sendALLBtn.Size = New System.Drawing.Size(173, 23)
        Me.sendALLBtn.TabIndex = 2
        Me.sendALLBtn.Text = "send ALL emails for last month"
        Me.sendALLBtn.UseVisualStyleBackColor = True
        '
        'sendNewbtn
        '
        Me.sendNewbtn.Location = New System.Drawing.Point(95, 127)
        Me.sendNewbtn.Name = "sendNewbtn"
        Me.sendNewbtn.Size = New System.Drawing.Size(146, 23)
        Me.sendNewbtn.TabIndex = 1
        Me.sendNewbtn.Text = "send emails not yet sent"
        Me.sendNewbtn.UseVisualStyleBackColor = True
        '
        'monthLbl
        '
        Me.monthLbl.AutoSize = True
        Me.monthLbl.Location = New System.Drawing.Point(92, 20)
        Me.monthLbl.Name = "monthLbl"
        Me.monthLbl.Size = New System.Drawing.Size(39, 13)
        Me.monthLbl.TabIndex = 4
        Me.monthLbl.Text = "Label1"
        '
        'ProgressBar1
        '
        Me.ProgressBar1.Location = New System.Drawing.Point(31, 287)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(210, 23)
        Me.ProgressBar1.TabIndex = 5
        '
        'mainfrm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(358, 363)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.monthLbl)
        Me.Controls.Add(Me.sendNewbtn)
        Me.Controls.Add(Me.sendALLBtn)
        Me.Controls.Add(Me.exitbtn)
        Me.Controls.Add(Me.maintainrbtn)
        Me.Name = "mainfrm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Address change report"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents maintainrbtn As System.Windows.Forms.Button
    Friend WithEvents exitbtn As System.Windows.Forms.Button
    Friend WithEvents sendALLBtn As System.Windows.Forms.Button
    Friend WithEvents sendNewbtn As System.Windows.Forms.Button
    Friend WithEvents monthLbl As System.Windows.Forms.Label
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar

End Class

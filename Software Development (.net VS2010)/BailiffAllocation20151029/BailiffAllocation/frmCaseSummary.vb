﻿Imports CommonLibrary

Public Class frmCaseSummary
    Private Map As frmMap
    Private SummaryData As New clsCaseSummaryData
    Private ListData As New clsListData
    ' Private Map As frmMap
    ' These cannot be instantiated here as the datagridviews have not been instantiated at this point but they need to be declared here to achieve the right scope...

    Private StageGridState As clsGridState
    Private ClientGridState As clsGridState
    Private WorkTypeGridState As clsGridState
    Private SchemeGridState As clsGridState
    Private AllocatedGridState As clsGridState
    Private PaymentGridState As clsGridState
    Private CGAGridState As clsGridState
    Private EnforcementFeesAppliedGridState As clsGridState
    Private NumberOfVisitsGridState As clsGridState
    Private AddConfirmedGridState As clsGridState
    Private InYearGridState As clsGridState
    Private PostcodeAreaGridState As clsGridState
    Private ArrangementBrokenGridState As clsGridState
    Private DebtYearGridState As clsGridState
    Private LinkedPIFGridState As clsGridState

    Private ColSort As String

    Private ParamList As String

    'Private RefreshDBClicked As Boolean = False
    'Private RefreshRunningMsgShown As Boolean = False

#Region "New and open"

    Public Sub New()
        Try
            InitializeComponent()

            SetFormIcon(Me)

            cmdRefreshDB.Visible = UserCanRefresh

            ParamList = UserCompanyID.ToString & ",null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null," & BalanceType() & ",8" ' i.e. top level ' BalanceType added TS 12/Feb/2015. Request ref 40282

            ' Now instantiate these
            '   Map = New frmMap(dgvSummary)
            StageGridState = New clsGridState(dgvStageName)
            ClientGridState = New clsGridState(dgvClientName)
            WorkTypeGridState = New clsGridState(dgvWorkType)
            SchemeGridState = New clsGridState(dgvSchemeName)
            AllocatedGridState = New clsGridState(dgvAllocated)
            PaymentGridState = New clsGridState(dgvPayment)
            CGAGridState = New clsGridState(dgvCGA)
            EnforcementFeesAppliedGridState = New clsGridState(dgvEnforcementFeesApplied)
            NumberOfVisitsGridState = New clsGridState(dgvNumberOfVisits)
            AddConfirmedGridState = New clsGridState(dgvAddConfirmed)
            InYearGridState = New clsGridState(dgvInYear)
            PostcodeAreaGridState = New clsGridState(dgvPostcodeArea)
            ArrangementBrokenGridState = New clsGridState(dgvArrangementBroken)
            DebtYearGridState = New clsGridState(dgvDebtYear)
            LinkedPIFGridState = New clsGridState(dgvLinkedPIF)

            AddControlHandlers()

            cmsSummary.Items.Add("View Cases")
            cmsSummary.Items.Add("Map Cases")
            cmsSummary.Items.Add("Copy")
            cmsSummary.Items.Add("Select All")

            cmsList.Items.Add("Consortiums...")
            cmsList.Items.Add("Set warning levels")
            cmsList.Items.Add("Regions")
            cmsList.Items.Add("Copy")
            cmsList.Items.Add("Select All")

            cmsForm.Items.Add("Bailiff View")
            cmsForm.Items.Add("Post Enforcement View")
            cmsForm.Items.Add("EA Update View")

            ListData.GetConsortiums(UserCompanyID)

            Dim Consortium As ToolStripMenuItem = Convert.ChangeType(cmsList.Items(0), GetType(ToolStripMenuItem))

            For Each Row As DataRow In ListData.ConsortiumDataView.ToTable(True, "Consortium").Rows
                Dim ConsortiumSubMenuItem As ToolStripMenuItem = New ToolStripMenuItem(Row.Item(0).ToString)

                AddHandler ConsortiumSubMenuItem.Click, AddressOf ConsortiumContextSubMenu_Click
                Consortium.DropDownItems.Add(ConsortiumSubMenuItem)
            Next Row

            ' Added TS 09/Feb/2015. Request ref 38930
            ListData.GetPostcodeAreaRegions()

            Dim Region As ToolStripMenuItem = Convert.ChangeType(cmsList.Items(2), GetType(ToolStripMenuItem))

            For Each Row As DataRow In ListData.PostcodeAreaRegionDataView.ToTable(True, "RegionDesc").Rows
                Dim RegionSubMenuItem As ToolStripMenuItem = New ToolStripMenuItem(Row.Item(0).ToString)

                AddHandler RegionSubMenuItem.Click, AddressOf RegionContextSubMenu_Click
                Region.DropDownItems.Add(RegionSubMenuItem)
            Next Row
            ' end of 38930

            SummaryData.GetSummary(ParamList, True, "P")
            dgvSummary.DataSource = SummaryData.SummaryDataView

            FormatGridColumns(dgvSummary, GetStages)

            ListData.GetCaseList(ParamList)
            SetListGrids()
            FormatListColumns()

            cboPeriodType.DataSource = SummaryData.PeriodTypeDataView
            cboPeriodType.ValueMember = "PeriodTypeID"
            cboPeriodType.DisplayMember = "PeriodTypeDesc"

            cboCompany.DataSource = SummaryData.CompanyDataView
            cboCompany.ValueMember = "CompanyID"
            cboCompany.DisplayMember = "CompanyDesc"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmSummary_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Try
            RemoveSelectionHandlers()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmCaseSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles Me.MouseUp
        Try
            If e.Button = MouseButtons.Right Then
                cmsForm.Show(sender, e.Location)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub frmSummary_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            HighlightWarnings()
            SetSelections() 'Arrays will be nothing so clear all selections - by default the first row in a datagridview is always selected
            dgvSummary.ClearSelection()

            radAbs.Checked = True
            cboCompany.Text = UserCompany
            chkTopClients.Checked = False
            cboPeriodType.SelectedValue = 8 ' changed from 1 TS 18/Dec/2013
            cboDisplaySet.Text = "Periods"

            AddHandler radAbs.CheckedChanged, AddressOf radAbs_CheckedChanged
            AddHandler chkTopClients.CheckedChanged, AddressOf chkTopClients_CheckedChanged

            AddSelectionHandlers()

            SetcmdRefreshTip()

            ' This is duplicated from FormatListColumns as that sub runs before the form is shown
            For Each Row As DataGridViewRow In dgvClientName.Rows
                SummaryData.UnderAllocatedClientDataView.RowFilter = "ClientName = '" & Row.Cells("ClientName").Value & "'"
                If SummaryData.UnderAllocatedClientDataView.Count > 0 Then
                    Row.DefaultCellStyle.ForeColor = Color.Red
                Else
                    Row.DefaultCellStyle.ForeColor = Color.Black
                End If
            Next Row

            SetStageColour() ' Added TS 24/Jul/2014 26706

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "Refresh"

    Private Sub RefreshGrid()
        Try
            ParamList = cboCompany.SelectedValue.ToString & ","
            ParamList &= GetParam(dgvStageName, "StageName") & ","
            ParamList &= GetParam(dgvClientName, "ClientName") & ","
            ParamList &= GetParam(dgvWorkType, "WorkType") & ","
            ParamList &= GetParam(dgvSchemeName, "SchemeName") & ","
            ParamList &= GetParam(dgvAllocated, "Allocated") & ","
            ParamList &= GetParam(dgvCGA, "CGA") & ","
            ParamList &= GetParam(dgvEnforcementFeesApplied, "EnforcementFeesApplied") & ","
            ParamList &= GetParam(dgvPayment, "Payment") & ","
            ParamList &= GetParam(dgvNumberOfVisits, "NumberOfVisits") & ","
            ParamList &= GetParam(dgvAddConfirmed, "AddConfirmed") & ","
            ParamList &= GetParam(dgvInYear, "InYear") & ","
            ParamList &= GetParam(dgvPostcodeArea, "PostcodeArea") & ","
            ParamList &= GetParam(dgvArrangementBroken, "ArrangementBroken") & ","
            ParamList &= GetParam(dgvDebtYear, "DebtYear") & ","
            ParamList &= GetParam(dgvLinkedPIF, "LinkedPIF") & ","

            ParamList &= chkTopClients.Checked.ToString

            ParamList &= "," & BalanceType() ' added TS 12/Feb/2015. Request ref 40282

            ParamList &= "," & cboPeriodType.SelectedValue.ToString

            PreRefresh(Me)

            RemoveSelectionHandlers()

            GetSelections()

            SummaryData.GetSummary(ParamList, radAbs.Checked, cboDisplaySet.Text.Substring(0, 1))
            dgvSummary.DataSource = SummaryData.SummaryDataView

            FormatGridColumns(dgvSummary, GetStages)
            HighlightWarnings()

            ListData.GetCaseList(ParamList)
            SetListGrids()
            FormatListColumns()

            SetSelections()

            AddSelectionHandlers()

            PostRefresh(Me)

            dgvSummary.ClearSelection() ' The first cell always get selected

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function GetParam(ByRef DataGrid As DataGridView, ByVal ColumnName As String) As String
        GetParam = Nothing

        Try
            ' Used when selection criteria change
            Dim Param As String = Nothing

            For Each dr As DataGridViewRow In DataGrid.SelectedRows
                If Not IsNothing(Param) Then Param += vbTab
                Param += dr.Cells(ColumnName).Value
            Next dr

            If IsNothing(Param) Then
                Param = "null"
            Else
                Param = "'" & Param.Replace("'", "''''") & "'" ' Portal task ref 16716
            End If

            Return Param

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function GetParam(ByVal ParamColumn As String, ByRef Cell As DataGridViewCell, ByRef ListDataGridView As DataGridView)
        GetParam = Nothing

        Try
            ' Used when detail for a particular cell in the summary grid is retrieved
            Dim Param As String = ""

            If IsNothing(Cell.DataGridView.Columns(ParamColumn)) Then
                ' The column may not be present as only one criteria is applicable
                If ListDataGridView.SelectedRows.Count = 1 Then
                    ' Param = "'" & ListDataGridView.SelectedRows(0).Cells(0).Value & "'"
                    Param = "'" & ListDataGridView.SelectedRows(0).Cells(0).Value.Replace("'", "''''") & "'" ' Portal task ref 16716
                Else
                    Param = "null"
                End If
            Else
                Param = "'" & Cell.DataGridView.Item(ParamColumn, Cell.RowIndex).Value.Replace("'", "''''") & "'" ' Portal task ref 16716
            End If

            Return Param

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function
#End Region

#Region "Grid selection events"

    Private Sub cboCompany_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboCompany.SelectedValueChanged
        Try
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    CType(Control, DataGridView).FirstDisplayedScrollingRowIndex = 0
                    CType(Control, DataGridView).ClearSelection()
                End If
            Next Control

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboPeriodType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboPeriodType.SelectedValueChanged
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cboDisplaySet_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) ' Handles cboDisplaySet.SelectedValueChanged
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            ' This is needed to make mousewheel scroll list items
            sender.Select()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                GetSelections()
                RemoveSelectionHandlers()
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvMouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                SetSelections()
                AddSelectionHandlers()
                HighlightUnderAllocation()
            End If

            If e.Button = MouseButtons.Right Then
                If sender.name <> "dgvClientName" And sender.name <> "dgvSchemeName" Then cmsList.Items(0).Visible = False Else cmsList.Items(0).Visible = True
                If sender.name <> "dgvClientName" Then cmsList.Items(1).Visible = False Else cmsList.Items(1).Visible = True
                If sender.name <> "dgvPostcodeArea" Then cmsList.Items(2).Visible = False Else cmsList.Items(2).Visible = True ' added TS 09/Feb/2015. Request ref 38930
                cmsList.Show(sender, e.Location)
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_DataBindingComplete(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewBindingCompleteEventArgs) Handles dgvSummary.DataBindingComplete
        Try
            ' This needs to be handled differently. I'm not sure why but doing this later causes events to fire. Recommended practice on the following url
            ' http://msdn.microsoft.com/en-us/library/system.windows.forms.datagridviewcolumn.visible.aspx

            If dgvSummary.Columns.Contains("RowID") Then dgvSummary.Columns("RowID").Visible = False

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseDown
        Try
            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If e.Button = Windows.Forms.MouseButtons.Right AndAlso hti.Type = DataGridViewHitTestType.Cell AndAlso Not dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected Then
                dgvSummary.Item(hti.ColumnIndex, hti.RowIndex).Selected = True
            End If

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                ColSort = SummaryData.SummaryDataView.Sort
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSummary.MouseUp
        Try
            If e.Button = MouseButtons.Right Then cmsSummary.Show(dgvSummary, e.Location)

            Dim hti As DataGridView.HitTestInfo = sender.HitTest(e.X, e.Y)

            If hti.Type = DataGridViewHitTestType.ColumnHeader Then
                If ColSort <> "" Then SummaryData.SummaryDataView.Sort += "," & ColSort
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub dgvSummary_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSummary.SelectionChanged
        Try
            Dim TotalCases As Integer = 0

            If dgvSummary.SelectedCells.Count = 0 Or radAbs.Checked = False Then
                lblSummary.Text = ""
            Else
                For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                    If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then TotalCases += Cell.Value
                Next Cell
                lblSummary.Text = TotalCases.ToString & " case" & If(TotalCases > 1, "s", "")
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ShowDetail()
        Try
            Dim DetailParamList As String
            Dim Detail As New diaCaseDetail(Me)

            PreRefresh(Me)

            For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then
                    DetailParamList = cboCompany.SelectedValue.ToString & ","
                    DetailParamList &= GetParam("StageName", Cell, dgvStageName) & ","
                    DetailParamList &= GetParam("ClientName", Cell, dgvClientName) & ","
                    DetailParamList &= GetParam("WorkType", Cell, dgvWorkType) & ","
                    DetailParamList &= GetParam("SchemeName", Cell, dgvSchemeName) & ","
                    DetailParamList &= GetParam("Allocated", Cell, dgvAllocated) & ","
                    DetailParamList &= GetParam("CGA", Cell, dgvCGA) & ","
                    DetailParamList &= GetParam("EnforcementFeesApplied", Cell, dgvEnforcementFeesApplied) & ","
                    DetailParamList &= GetParam("Payment", Cell, dgvPayment) & ","
                    DetailParamList &= GetParam("NumberOfVisits", Cell, dgvNumberOfVisits) & ","
                    DetailParamList &= GetParam("AddConfirmed", Cell, dgvAddConfirmed) & ","
                    DetailParamList &= GetParam("InYear", Cell, dgvInYear) & ","
                    DetailParamList &= GetParam("PostcodeArea", Cell, dgvPostcodeArea) & ","
                    DetailParamList &= GetParam("ArrangementBroken", Cell, dgvArrangementBroken) & ","
                    DetailParamList &= GetParam("DebtYear", Cell, dgvDebtYear) & ","
                    DetailParamList &= GetParam("LinkedPIF", Cell, dgvLinkedPIF) & ","

                    DetailParamList &= chkTopClients.Checked.ToString

                    DetailParamList &= "," & BalanceType()  ' added TS 12/Feb/2015. Request ref 40282

                    DetailParamList &= "," & cboPeriodType.SelectedValue.ToString

                    Detail.AddCases(DetailParamList, dgvSummary.Columns(Cell.ColumnIndex).Name)

                End If
            Next Cell

            PostRefresh(Me)

            If Detail.CaseCount > 0 Then Detail.ShowDialog() ' Portal task ref 16716
            Detail.Dispose()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ShowMap()
        Try
            Dim DetailParamList As String

            PreRefresh(Me)

            If IsNothing(map) OrElse map.IsDisposed Then map = New frmMap(dgvSummary)

            For Each Cell As DataGridViewCell In dgvSummary.SelectedCells
                If DataColumnsList.Contains(dgvSummary.Columns(Cell.ColumnIndex).Name) Then
                    DetailParamList = cboCompany.SelectedValue.ToString & ","
                    DetailParamList &= GetParam("StageName", Cell, dgvStageName) & ","
                    DetailParamList &= GetParam("ClientName", Cell, dgvClientName) & ","
                    DetailParamList &= GetParam("WorkType", Cell, dgvWorkType) & ","
                    DetailParamList &= GetParam("SchemeName", Cell, dgvSchemeName) & ","
                    DetailParamList &= GetParam("Allocated", Cell, dgvAllocated) & ","
                    DetailParamList &= GetParam("CGA", Cell, dgvCGA) & ","
                    DetailParamList &= GetParam("EnforcementFeesApplied", Cell, dgvEnforcementFeesApplied) & ","
                    DetailParamList &= GetParam("Payment", Cell, dgvPayment) & ","
                    DetailParamList &= GetParam("NumberOfVisits", Cell, dgvNumberOfVisits) & ","
                    DetailParamList &= GetParam("AddConfirmed", Cell, dgvAddConfirmed) & ","
                    DetailParamList &= GetParam("InYear", Cell, dgvInYear) & ","
                    DetailParamList &= GetParam("PostcodeArea", Cell, dgvPostcodeArea) & ","
                    DetailParamList &= GetParam("ArrangementBroken", Cell, dgvArrangementBroken) & ","
                    DetailParamList &= GetParam("DebtYear", Cell, dgvDebtYear) & ","
                    DetailParamList &= GetParam("LinkedPIF", Cell, dgvLinkedPIF) & ","

                    DetailParamList &= chkTopClients.Checked.ToString

                    DetailParamList &= "," & BalanceType()  ' added TS 12/Feb/2015. Request ref 40282

                    DetailParamList &= "," & cboPeriodType.SelectedValue.ToString

                    Map.AddCases(DetailParamList, dgvSummary.Columns(Cell.ColumnIndex).Name)

                End If
            Next Cell

            ' Get the cases added ready to plot - this is where the different datasets are named
            map.GetCasesForMap()

            PostRefresh(Me)

            Map.Show()
            '   Map = Nothing

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "'All' and 'Clear' button click events"

    Private Sub cmdStageAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageAll.Click
        Try
            dgvStageName.SelectAll()
            StageGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdStageClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdStageClear.Click
        Try
            dgvStageName.ClearSelection()
            StageGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientAll.Click
        Try
            dgvClientName.SelectAll()
            ClientGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdClientClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClientClear.Click
        Try
            dgvClientName.ClearSelection()
            ClientGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdWorkTypeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWorkTypeAll.Click
        Try
            dgvWorkType.SelectAll()
            WorkTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdWorkTypeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdWorkTypeClear.Click
        Try
            dgvWorkType.ClearSelection()
            WorkTypeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdSchemeAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeAll.Click
        Try
            dgvSchemeName.SelectAll()
            SchemeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdSchemeClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSchemeClear.Click
        Try
            dgvSchemeName.ClearSelection()
            SchemeGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAllocatedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAllocatedAll.Click
        Try
            dgvAllocated.SelectAll()
            AllocatedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAllocatedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAllocatedClear.Click
        Try
            dgvAllocated.ClearSelection()
            AllocatedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCGAAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCGAAll.Click
        Try
            dgvCGA.SelectAll()
            CGAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdCGAClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCGAClear.Click
        Try
            dgvCGA.ClearSelection()
            CGAGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdEnforcementFeesAppliedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEnforcementFeesAppliedAll.Click
        Try
            dgvEnforcementFeesApplied.SelectAll()
            EnforcementFeesAppliedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdEnforcementFeesAppliedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdEnforcementFeesAppliedClear.Click
        Try
            dgvEnforcementFeesApplied.ClearSelection()
            EnforcementFeesAppliedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPaymentAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPaymentAll.Click
        Try
            dgvPayment.SelectAll()
            PaymentGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPaymentClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPaymentClear.Click
        Try
            dgvPayment.ClearSelection()
            PaymentGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdNumberOfVisitsAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNumberOfVisitsAll.Click
        Try
            dgvNumberOfVisits.SelectAll()
            NumberOfVisitsGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdNumberOfVisitsClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdNumberOfVisitsClear.Click
        Try
            dgvNumberOfVisits.ClearSelection()
            NumberOfVisitsGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAddConfirmedAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfirmedAll.Click
        Try
            dgvAddConfirmed.SelectAll()
            AddConfirmedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdAddConfirmedClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdAddConfirmedClear.Click
        Try
            dgvAddConfirmed.ClearSelection()
            AddConfirmedGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdInYearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdInYearAll.Click
        Try
            dgvInYear.SelectAll()
            InYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdInYearClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdInYearClear.Click
        Try
            dgvInYear.ClearSelection()
            InYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeAreaAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaAll.Click
        Try
            dgvPostcodeArea.SelectAll()
            PostcodeAreaGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdPostcodeAreaClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdPostcodeAreaClear.Click
        Try
            dgvPostcodeArea.ClearSelection()
            PostcodeAreaGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdArrangementBrokenAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdArrangementBrokenAll.Click
        Try
            dgvArrangementBroken.SelectAll()
            ArrangementBrokenGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdArrangementBrokenClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdArrangementBrokenClear.Click
        Try
            dgvArrangementBroken.ClearSelection()
            ArrangementBrokenGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdDebtYearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDebtYearAll.Click
        Try
            dgvDebtYear.SelectAll()
            DebtYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdDebtYearClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdDebtYearClear.Click
        Try
            dgvDebtYear.ClearSelection()
            DebtYearGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLinkedPIFAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLinkedPIFAll.Click
        Try
            dgvLinkedPIF.SelectAll()
            LinkedPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdLinkedPIFClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdLinkedPIFClear.Click
        Try
            dgvLinkedPIF.ClearSelection()
            LinkedPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub
#End Region

#Region "Handlers"

    Private Sub RemoveSelectionHandlers()
        Try
            ' Need to find a way to do these at once but these SelectionChanged event is specific to the datagridview class and not control - the collection on the form
            RemoveHandler dgvStageName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvWorkType.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvAllocated.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvCGA.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvEnforcementFeesApplied.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPayment.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvNumberOfVisits.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvAddConfirmed.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvInYear.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvArrangementBroken.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvDebtYear.SelectionChanged, AddressOf dgvSelectionChanged
            RemoveHandler dgvLinkedPIF.SelectionChanged, AddressOf dgvSelectionChanged

            RemoveHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            RemoveHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
            RemoveHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddSelectionHandlers()
        Try
            AddHandler dgvStageName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvClientName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvWorkType.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvSchemeName.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvAllocated.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvCGA.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvEnforcementFeesApplied.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPayment.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvNumberOfVisits.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvAddConfirmed.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvInYear.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvPostcodeArea.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvArrangementBroken.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvDebtYear.SelectionChanged, AddressOf dgvSelectionChanged
            AddHandler dgvLinkedPIF.SelectionChanged, AddressOf dgvSelectionChanged

            AddHandler cboCompany.SelectedValueChanged, AddressOf cboCompany_SelectedValueChanged
            AddHandler cboPeriodType.SelectedValueChanged, AddressOf cboPeriodType_SelectedValueChanged
            AddHandler cboDisplaySet.SelectedValueChanged, AddressOf cboDisplaySet_SelectedValueChanged

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub AddControlHandlers()
        Try
            ' All these grids and buttons call the same events so no point declaring them all separately
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView And Control.Name <> "dgvSummary" Then
                    AddHandler Control.MouseDown, AddressOf dgvMouseDown
                    AddHandler Control.MouseUp, AddressOf dgvMouseUp
                    AddHandler Control.MouseEnter, AddressOf dgvMouseEnter
                    'AddHandler Control.MouseHover, AddressOf dgvMouseHover
                ElseIf TypeOf Control Is ComboBox Or TypeOf Control Is Button Or TypeOf Control Is RadioButton Or TypeOf Control Is CheckBox Then
                    'AddHandler Control.MouseHover, AddressOf dgvMouseHover
                End If
            Next Control

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "DataGridView methods"

    Private Sub SetListGrids()
        Try
            dgvStageName.DataSource = ListData.StageName
            StageGridState.SetSort()

            dgvClientName.DataSource = ListData.ClientName
            ClientGridState.SetSort()

            dgvWorkType.DataSource = ListData.WorkType
            WorkTypeGridState.SetSort()

            dgvSchemeName.DataSource = ListData.SchemeName
            SchemeGridState.SetSort()

            dgvAllocated.DataSource = ListData.Allocated
            AllocatedGridState.SetSort()

            dgvCGA.DataSource = ListData.CGA
            CGAGridState.SetSort()

            dgvEnforcementFeesApplied.DataSource = ListData.EnforcementFeesApplied
            EnforcementFeesAppliedGridState.SetSort()

            dgvPayment.DataSource = ListData.Payment
            PaymentGridState.SetSort()

            dgvNumberOfVisits.DataSource = ListData.NumberOfVisits
            NumberOfVisitsGridState.SetSort()

            dgvAddConfirmed.DataSource = ListData.AddConfirmed
            AddConfirmedGridState.SetSort()

            dgvInYear.DataSource = ListData.InYear
            InYearGridState.SetSort()

            dgvPostcodeArea.DataSource = ListData.PostcodeArea
            PostcodeAreaGridState.SetSort()

            dgvArrangementBroken.DataSource = ListData.ArrangementBroken
            ArrangementBrokenGridState.SetSort()

            dgvDebtYear.DataSource = ListData.DebtYear
            DebtYearGridState.SetSort()

            dgvLinkedPIF.DataSource = ListData.LinkedPIF
            LinkedPIFGridState.SetSort()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub FormatListColumns()
        Try
            ' This is slightly different to settings in modCommon as we want some of the fields wider than they need to be for presentation
            For Each Control As Control In Me.Controls
                If TypeOf Control Is DataGridView Then
                    If Control.Name <> "dgvSummary" Then ' This is not a list grid
                        For Each Column In CType(Control, DataGridView).Columns
                            Select Case Column.Name
                                Case "Total"
                                    Column.Width = 40
                                Case "StageName"
                                    Column.Width = 121
                                    Column.HeaderText = "Stage"
                                Case "ClientName"
                                    Column.Width = 100
                                    Column.HeaderText = "Client"
                                Case "WorkType"
                                    Column.Width = 100
                                    Column.HeaderText = "Work Type"
                                Case "LinkedPIF"
                                    Column.Width = 70
                                    Column.HeaderText = "Linked PIF"
                                Case "SchemeName"
                                    Column.Width = 100
                                    Column.HeaderText = "Scheme"
                                Case "Allocated"
                                    Column.Width = 70
                                Case "CGA"
                                    Column.Width = 70
                                Case "EnforcementFeesApplied"
                                    Column.Width = 70
                                    Column.HeaderText = "Enf Fees"
                                Case "Payment"
                                    Column.Width = 70
                                Case "NumberOfVisits"
                                    Column.Width = 55
                                Case "AddConfirmed"
                                    Column.Width = 70
                                    Column.HeaderText = "Confirmed"
                                Case "ArrangementBroken"
                                    Column.Width = 70
                                    Column.HeaderText = "Broken"
                                Case "InYear"
                                    Column.Width = 73
                                Case "PostcodeArea"
                                    Column.Width = 55
                                    Column.HeaderText = "Area"
                                Case "DebtYear"
                                    Column.Width = 55
                                    Column.HeaderText = "Year"
                                Case Else
                                    Column.Width = 40
                            End Select
                        Next Column
                    End If
                End If
            Next Control

            SummaryData.GetUnderAllocatedClients()

            HighlightUnderAllocation()

            SetStageColour() ' Added TS 24/Jul/2014 26706

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub HighlightUnderAllocation()
        Try
            For Each Row As DataGridViewRow In dgvClientName.Rows
                SummaryData.UnderAllocatedClientDataView.RowFilter = "ClientName = '" & Row.Cells("ClientName").Value & "'"
                If SummaryData.UnderAllocatedClientDataView.Count > 0 Then
                    Row.DefaultCellStyle.ForeColor = Color.Red
                Else
                    Row.DefaultCellStyle.ForeColor = Color.Black
                End If
            Next Row

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub HighlightWarnings()
        Try
            Dim RowIndex As Integer

            For Each DataRow As DataRow In SummaryData.WarningSummaryDataView.ToTable.Rows
                ' Find the row with warnings on the datagridview
                For Each GridRow As DataGridViewRow In dgvSummary.Rows
                    If GridRow.Cells("RowID").Value = DataRow.Item("RowID") Then RowIndex = GridRow.Index
                Next GridRow

                ' Loop through periods and highlight if any warnings exist
                For Each Item As DataColumn In DataRow.Table.Columns
                    If DataColumnsList.Contains(Item.ColumnName) AndAlso DataRow.Item(Item.ColumnName) > 0 Then
                        dgvSummary.Rows(RowIndex).Cells(Item.ColumnName).Style.ForeColor = Color.Red
                    End If
                Next Item
            Next DataRow

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetStageColour()
        Try
            For Each Row As DataGridViewRow In dgvStageName.Rows
                Select Case Row.Cells("StageName").Value.ToString.Substring(0, 3)
                    Case "050", "075"
                        Row.DefaultCellStyle.ForeColor = Color.Orange

                    Case "150", "200", "250", "300"
                        Row.DefaultCellStyle.ForeColor = Color.Purple

                    Case "350", "500", "550"
                        Row.DefaultCellStyle.ForeColor = Color.Green

                    Case "525"
                        Row.DefaultCellStyle.ForeColor = Color.DodgerBlue

                End Select

            Next Row

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub GetSelections()
        Try
            StageGridState.GetState()
            ClientGridState.GetState()
            WorkTypeGridState.GetState()
            SchemeGridState.GetState()
            AllocatedGridState.GetState()
            CGAGridState.GetState()
            EnforcementFeesAppliedGridState.GetState()
            PaymentGridState.GetState()
            NumberOfVisitsGridState.GetState()
            AddConfirmedGridState.GetState()
            InYearGridState.GetState()
            PostcodeAreaGridState.GetState()
            ArrangementBrokenGridState.GetState()
            DebtYearGridState.GetState()
            LinkedPIFGridState.GetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetSelections()
        Try
            StageGridState.SetState()
            ClientGridState.SetState()
            WorkTypeGridState.SetState()
            SchemeGridState.SetState()
            AllocatedGridState.SetState()
            CGAGridState.SetState()
            EnforcementFeesAppliedGridState.SetState()
            PaymentGridState.SetState()
            NumberOfVisitsGridState.SetState()
            AddConfirmedGridState.SetState()
            InYearGridState.SetState()
            PostcodeAreaGridState.SetState()
            ArrangementBrokenGridState.SetState()
            DebtYearGridState.SetState()
            LinkedPIFGridState.SetState()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

#End Region

#Region "Additional functionality"

    Private Sub cmdClearAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdClearAll.Click
        Try
            RemoveSelectionHandlers()

            cmdStageClear_Click(sender, New System.EventArgs)
            cmdClientClear_Click(sender, New System.EventArgs)
            cmdWorkTypeClear_Click(sender, New System.EventArgs)
            cmdSchemeClear_Click(sender, New System.EventArgs)
            cmdAllocatedClear_Click(sender, New System.EventArgs)
            cmdCGAClear_Click(sender, New System.EventArgs)
            cmdEnforcementFeesAppliedClear_Click(sender, New System.EventArgs)
            cmdPaymentClear_Click(sender, New System.EventArgs)
            cmdNumberOfVisitsClear_Click(sender, New System.EventArgs)
            cmdAddConfirmedClear_Click(sender, New System.EventArgs)
            cmdInYearClear_Click(sender, New System.EventArgs)
            cmdPostcodeAreaClear_Click(sender, New System.EventArgs)
            cmdArrangementBrokenClear_Click(sender, New System.EventArgs)
            cmdDebtYearClear_Click(sender, New System.EventArgs)
            cmdLinkedPIFClear_Click(sender, New System.EventArgs)

            AddSelectionHandlers()

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radAbs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles radAbs.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radPerc_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPerc.CheckedChanged
        Try
            RefreshGrid()
            If radAbs.Checked Then dgvSummary.DefaultCellStyle.Format = "" Else dgvSummary.DefaultCellStyle.Format = "N2"

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub chkTopClients_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) 'Handles chkTopClients.CheckedChanged
        Try
            RefreshScroll = False

            If chkTopClients.Checked Then
                ' Not ideal but we need to refresh the client list before the main refresh to exclude any clients selected that would not be in the top list
                RemoveSelectionHandlers()

                GetSelections()
                ListData.GetClientList(ParamList)
                SetSelections()

                AddSelectionHandlers()
            End If

            RefreshGrid()

            RefreshScroll = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.Click
        Try
            If MsgBox("This could take several minutes, are you sure?", vbYesNo + vbDefaultButton2 + vbQuestion) = vbYes Then
                'RefreshDBClicked = True
                PreRefresh(Me)
                SummaryData.RefreshDatabase()
                RefreshGrid()
                PostRefresh(Me)
                MsgBox("Refresh complete.", vbOKOnly + vbInformation)
                SetcmdRefreshTip()
                'RefreshDBClicked = False
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmdRefreshDB_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdRefreshDB.MouseLeave
        Try
            ' on XP machines there is a bug that prevents the tooltip showing a second time. This addresses the problem.
            ToolTip1.Active = False
            ToolTip1.Active = True

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub SetcmdRefreshTip()
        Try
            SummaryData.GetLastLoad()
            ToolTip1.SetToolTip(cmdRefreshDB, "Last refreshed at " & SummaryData.LastLoadDataView.Item(0).Item(0).ToString & " by " & SummaryData.LastLoadDataView.Item(0).Item(1).ToString)

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsSummary_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsSummary.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "View Cases"
                    ShowDetail()
                Case "Map Cases"
                    ShowMap()
                Case "Copy"
                    dgvSummary.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    Clipboard.SetDataObject(dgvSummary.GetClipboardContent())
                Case "Select All"
                    dgvSummary.SelectAll()
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsList_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsList.ItemClicked
        Try
            ' Slightly more complicated than the summary grid as this event fires for all other datagridviews.
            Select Case e.ClickedItem.Text
                Case "Set warning levels"
                    Dim dia As New diaClientAllocationLevels(SummaryData.ClientAllocationLevelDataView)
                    dia.ShowDialog()
                    SummaryData.GetClientAllocationLevels()
                    If dia.DialogResult = DialogResult.OK Then FormatListColumns()
                    dia.Dispose()

                Case "Copy"
                    sender.SourceControl.ClipboardCopyMode = DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
                    Clipboard.SetDataObject(sender.SourceControl.GetClipboardContent())

                Case "Select All"
                    sender.SourceControl.SelectAll()

            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub cmsForm_ItemClicked(ByVal sender As Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles cmsForm.ItemClicked
        Try
            Select Case e.ClickedItem.Text
                Case "Bailiff View"
                    If FormOpen("frmBailiffSummary") Then
                        frmBailiffSummary.Activate()
                    Else
                        frmBailiffSummary.ShowDialog()
                    End If
                Case "Post Enforcement View"
                    If FormOpen("frmPostEnforcementSummary") Then
                        frmPostEnforcementSummary.Activate()
                    Else
                        frmPostEnforcementSummary.ShowDialog()
                    End If
                Case "EA Update View"
                    If FormOpen("frmEAUpdateSummary") Then
                        frmEAUpdateSummary.Activate()
                    Else
                        frmEAUpdateSummary.ShowDialog()
                    End If
            End Select

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub ConsortiumContextSubMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ScrollbarSet As Boolean = False

            RemoveSelectionHandlers()

            dgvClientName.ClearSelection()
            dgvSchemeName.ClearSelection()

            ListData.ConsortiumDataView.RowFilter = "Consortium = '" & CType(sender, ToolStripItem).Text & "'"

            For Each ConsortiumDataRow As DataRow In ListData.ConsortiumDataView.ToTable.Rows
                For Each GridDataRow As DataGridViewRow In dgvClientName.Rows
                    If GridDataRow.Cells("ClientName").Value = ConsortiumDataRow.Item("ClientName") Then
                        GridDataRow.Selected = True
                        If Not ScrollbarSet Then
                            dgvClientName.FirstDisplayedScrollingRowIndex = GridDataRow.Index
                            ScrollbarSet = True
                        End If
                    End If
                Next GridDataRow

                ScrollbarSet = False

                For Each GridDataRow As DataGridViewRow In dgvSchemeName.Rows
                    If GridDataRow.Cells("SchemeName").Value = ConsortiumDataRow.Item("SchemeName") Then
                        GridDataRow.Selected = True
                        If Not ScrollbarSet Then
                            dgvSchemeName.FirstDisplayedScrollingRowIndex = GridDataRow.Index
                            ScrollbarSet = True
                        End If
                    End If
                Next GridDataRow
            Next ConsortiumDataRow

            AddSelectionHandlers()
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub RegionContextSubMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ' added TS 09/Feb/2015. Request ref 38930
        Try
            Dim ScrollbarSet As Boolean = False

            RemoveSelectionHandlers()

            ListData.PostcodeAreaRegionDataView.RowFilter = "RegionDesc = '" & CType(sender, ToolStripItem).Text & "'"

            For Each PostcodeAreaRegionDataRow As DataRow In ListData.PostcodeAreaRegionDataView.ToTable.Rows
                For Each GridDataRow As DataGridViewRow In dgvPostcodeArea.Rows
                    If GridDataRow.Cells("PostcodeArea").Value = PostcodeAreaRegionDataRow.Item("PostcodeArea") Then
                        GridDataRow.Selected = True
                        If Not ScrollbarSet Then
                            dgvPostcodeArea.FirstDisplayedScrollingRowIndex = GridDataRow.Index
                            ScrollbarSet = True
                        End If
                    End If
                Next GridDataRow
            Next PostcodeAreaRegionDataRow

            AddSelectionHandlers()
            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radLow_Click(sender As Object, e As System.EventArgs) Handles radLow.Click
        Try
            If radLow.Checked Then
                radLow.Checked = False
            Else
                radLow.Checked = True
                radHigh.Checked = False
            End If

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Sub radHigh_Click(sender As Object, e As System.EventArgs) Handles radHigh.Click
        Try
            If radHigh.Checked Then
                radHigh.Checked = False
            Else
                radHigh.Checked = True
                radLow.Checked = False
            End If

            RefreshGrid()

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Sub

    Private Function BalanceType() As String
        BalanceType = "A"

        Try
            If radLow.Checked Then
                BalanceType = "L"
            ElseIf radHigh.Checked Then
                BalanceType = "H"
            End If

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function

    Private Function GetStages() As String()
        GetStages = Nothing

        Try
            Dim SelectedSorts As New List(Of String)


            For Each Item As DataGridViewRow In dgvStageName.SelectedRows
                SelectedSorts.Add(Item.Cells("StageName").Value.ToString)
            Next Item

            If Not IsNothing(SelectedSorts) Then GetStages = SelectedSorts.ToArray

        Catch ex As Exception
            HandleException(ex)
        End Try
    End Function
#End Region



    'Private Sub dgvMouseHover(sender As Object, e As System.EventArgs)

    '    If Not RefreshDBClicked Then Return

    '    If Not ListData.IsRefreshRunning Then Return

    '    If Not RefreshRunningMsgShown Then

    '        Me.Enabled = False

    '        MsgBox("Refresh running.", vbOKOnly + vbInformation)
    '        RefreshRunningMsgShown = True
    '        RefreshCheckTimer.Enabled = True
    '    End If
    'End Sub

    'Private Sub RefreshCheckTimer_Tick(sender As Object, e As System.EventArgs) Handles RefreshCheckTimer.Tick

    '    If ListData.IsRefreshRunning Then Return

    '    MsgBox("Refresh complete.", vbOKOnly + vbInformation)
    '    RefreshRunningMsgShown = False
    '    RefreshCheckTimer.Enabled = False
    '    Me.Enabled = True
    'End Sub


End Class

